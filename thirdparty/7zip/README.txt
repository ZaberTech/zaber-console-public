The LzmaAlone project is used to compile the lzma.dll that ships in
the bin folder of the Zaber Console distributions.

The providence and license for the source code is uncertain; it should
not be included in any open source distributions until verified.

This software is free. Modification and redistribution are permitted 
upon meeting the terms of the licenses.

The original portions of this software are released under the Apache 2.0
license. Some portions of the source code that were adapted from external
sources, and some third-party dependencies, are covered by other licenses.
For more detail and the complete text of the Apache 2.0 license, 
please see [ZaberConsoleGUI/license.txt](ZaberConsoleGUI/license.txt).

This software is distributed "as-is" with no warranty, and liability 
is disclaimed; use at your own risk.

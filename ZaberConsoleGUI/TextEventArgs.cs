﻿using System;

namespace ZaberConsole
{
	/// <summary>
	///     Used to describe an event with a block of text.
	/// </summary>
	public class TextEventArgs : EventArgs
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Create a new instance.
		/// </summary>
		/// <param name="text">The text associated with this event</param>
		public TextEventArgs(string text)
		{
			Text = text;
		}


		/// <summary>
		///     Gets the text associated with this event.
		/// </summary>
		public string Text { get; }

		#endregion
	}
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using Zaber;

namespace ZaberConsole
{
	public class SqliteDeviceDictionary : SqliteDictionary, IDictionary<Tuple<int, FirmwareVersion>, DeviceType>
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Intended to quietly serve as the DeviceTypeMap for a
		///     ZaberPortFacade, this class lazily fetches data from the
		///     SQLite database.
		/// </summary>
		/// <param name="aFilename">The name of the SQLite database file.</param>
		public SqliteDeviceDictionary(string aFilename)
			: base(aFilename)
		{
			_databaseFileName = aFilename;
			_cache = new Dictionary<Tuple<int, FirmwareVersion>, DeviceType>();
		}


		public void Clear() => _cache.Clear();


		public void Add(KeyValuePair<Tuple<int, FirmwareVersion>, DeviceType> aItem)
			=> throw new NotSupportedException("Collection is read-only.");


		public void Add(Tuple<int, FirmwareVersion> aKey, DeviceType aValue)
			=> throw new NotSupportedException("Collection is read-only.");


		public bool Contains(KeyValuePair<Tuple<int, FirmwareVersion>, DeviceType> aItem)
			=> ContainsKey(aItem.Key) && aItem.Value.Equals(this[aItem.Key]);


		public bool ContainsKey(Tuple<int, FirmwareVersion> aKey) => MapFirmwareVersion(aKey.Item1, aKey.Item2) != null;


		public void CopyTo(KeyValuePair<Tuple<int, FirmwareVersion>, DeviceType>[] aDest, int aIndex)
			=> throw new NotImplementedException();


		public IEnumerator<KeyValuePair<Tuple<int, FirmwareVersion>, DeviceType>> GetEnumerator()
		{
			using (var command = new SQLiteCommand(
				"SELECT DeviceId, MajorVersion, MinorVersion, Build FROM Console_Devices",
				_connection))
			{
				using (var reader = command.ExecuteReader())
				{
					var majorColumn = reader.GetOrdinal("MajorVersion");
					var minorColumn = reader.GetOrdinal("MinorVersion");
					var buildColumn = reader.GetOrdinal("Build");
					var idColumn = reader.GetOrdinal("DeviceId");

					while (reader.Read())
					{
						var build = reader.IsDBNull(buildColumn) ? 0 : reader.GetInt32(buildColumn);
						var firmwareVersion = new FirmwareVersion(reader.GetInt32(majorColumn),
																  reader.GetInt32(minorColumn),
																  build);

						var key = new Tuple<int, FirmwareVersion>(reader.GetInt32(idColumn), firmwareVersion);

						yield return new KeyValuePair<Tuple<int, FirmwareVersion>, DeviceType>(key, this[key]);
					}
				}
			}
		}


		public bool Remove(KeyValuePair<Tuple<int, FirmwareVersion>, DeviceType> aItem)
			=> throw new NotSupportedException("Collection is read-only.");


		public bool Remove(Tuple<int, FirmwareVersion> aKey)
			=> throw new NotSupportedException("Collection is read-only.");


		public bool TryGetValue(Tuple<int, FirmwareVersion> aKey, out DeviceType aValue)
		{
			try
			{
				aValue = this[aKey];
				return true;
			}
			catch (KeyNotFoundException)
			{
				aValue = null;
				return false;
			}
		}


		public DeviceType this[Tuple<int, FirmwareVersion> aKey]
		{
			get
			{
				if (_cache.ContainsKey(aKey))
				{
					return _cache[aKey];
				}

				var deviceId = aKey.Item1;
				var realFirmwareVersion = aKey.Item2;
				var firmwareVersionWrapped = MapFirmwareVersion(deviceId, realFirmwareVersion);
				if (!firmwareVersionWrapped.HasValue)
				{
					throw new KeyNotFoundException("Device's FW version is not in the device database and could not be mapped to one that is.");
				}
				var firmwareVersion = firmwareVersionWrapped.Value;

				string productId = null;
				DeviceType deviceType = null;

				using (var deviceReader = GetDeviceProductReader(deviceId, firmwareVersion))
				{
					if (!deviceReader.Read())
					{
						throw new KeyNotFoundException();
					}

					// TODO (Soleil 2019/09): The peripheral map assigned below can hold a lock on the
					// database file and should be disposed on port close. This means DeviceType should
					// be IDisposable, and it means examining every usage of DeviceType in the program.
					// I'm deferring this for now - see ticket #6053. I believe the worst consequence is
					// that one old copy of the device database may persist on disk until the program
					// is restarted.
					var periDict = new SqlitePeripheralDictionary(_databaseFileName, deviceId, firmwareVersion, realFirmwareVersion)
					{
						// Awkward - the dimensions are per file but we have two different classes wrapping the same file.
						_dimensionIdMap = _dimensionIdMap 
					};

					deviceType = new DeviceType
					{
						DeviceId = deviceId,
						FirmwareVersion = firmwareVersion,
						RealFirmwareVersion = realFirmwareVersion,
						Name = (string) deviceReader["Name"],
						PeripheralMap = periDict
					};

					productId = ((long) deviceReader["Id"]).ToString();
				}

				if ((deviceId == 952) // 952 is the T-MCA.
				|| (deviceId == 901) // 901 is the T-CD1000.
				|| (deviceId == 902)) // 902 is the T-CD2500.
				{
					// FW5 does not support the "get peripheral ID" command,
					// so it's impossible for us to know what peripheral is
					// connected to a T-series controller. Controllers don't 
					// have their own set of commands in the database; the 
					// "safe mode" peripheral contains the set of commands for
					// the device. Downside: no UOMs. Upside: commands list!
					try
					{
						deviceType.Commands = deviceType.PeripheralMap[0].Commands;

						// Don't use the "safe mode" text in the name because it's
						// still possible to put the device into unsafe modes.
					}
					catch (KeyNotFoundException)
					{
						// Move on if there is no safe mode peripheral.
					}
				}
				else
				{
					deviceType.Commands = GetCommandList(productId);
				}

				var converters = GetProductUnitConversions(productId, deviceType.Commands);
				deviceType.MotionType =
					DetermineMotionType(converters.Select(c => c.ReferenceUnit.Dimension), deviceType.Commands);
				deviceType.UnitConverter.Parent = UnitConverter;
				foreach (var converter in converters.Where(c => c.Scale.HasValue))
				{
					deviceType.DeviceTypeUnitConversionInfo[converter.ReferenceUnit.Dimension] = converter;
				}

				deviceType.TuningData = PopulateTuningData(productId);
				deviceType.Capabilities = ReadCapabilities(productId);

				// Cache it for next time.
				_cache[aKey] = deviceType;
				return deviceType;
			}

			set => throw new NotSupportedException("Collection is read-only.");
		}


		public int Count
		{
			get
			{
				using (var cmd = new SQLiteCommand("SELECT count(*) FROM Console_Devices", _connection))
				{
					return (int) (long) cmd.ExecuteScalar();
				}
			}
		}


		public ICollection<Tuple<int, FirmwareVersion>> Keys
		{
			get
			{
				using (var cmd = new SQLiteCommand("SELECT DeviceId, MajorVersion, MinorVersion "
											   + "FROM Console_Devices",
												   _connection))
				{
					using (var reader = cmd.ExecuteReader())
					{
						var keys = new List<Tuple<int, FirmwareVersion>>();
						while (reader.Read())
						{
							var firmwareVersion =
								((int) (long) reader["MajorVersion"] * 100)
							+ (int) (long) reader["MinorVersion"];
							keys.Add(new Tuple<int, FirmwareVersion>((int) (long) reader["DeviceId"],
																	 new FirmwareVersion(firmwareVersion)));
						}

						return keys;
					}
				}
			}
		}


		/// <summary>
		///     Retrieves all values from the database.
		/// </summary>
		/// <remarks>
		///     If possible, don't use this. One of the strengths of this class
		///     is its lazy lookup, grabbing only the DeviceTypes necessary.
		///     Calling this method will read the entire database into memory,
		///     which will be slow and self-defeating.
		/// </remarks>
		public ICollection<DeviceType> Values => Keys.Select(key => this[key]).ToList();

		#endregion

		#region -- Interfaces --

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

		#endregion

		#region -- Data --

		private readonly Dictionary<Tuple<int, FirmwareVersion>, DeviceType> _cache;
		private readonly string _databaseFileName;

		#endregion
	}
}

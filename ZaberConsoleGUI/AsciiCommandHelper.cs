﻿using System.Collections.Generic;
using System.Linq;
using Zaber;

namespace ZaberConsole
{
	/// <summary>
	///     Helper class for performing calculations and transformations of ASCII command trees.
	/// </summary>
	public static class AsciiCommandHelper
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Generate all possible variations of a set of commands.
		/// </summary>
		/// <param name="aCommandList">Set of commands to expand.</param>
		/// <returns>
		///     All possible variations of the given commands, as strings with {x} substitution points
		///     for parameters.
		/// </returns>
		public static IEnumerable<string> ListAsciiCommandCompletions(IEnumerable<CommandInfo> aCommandList)
		{
			var commands = new List<string>();

			if (null != aCommandList)
			{
				foreach (var command in aCommandList)
				{
					if (command is ASCIICommandInfo info)
					{
						commands.AddRange(EnumeratePossibilities(info));
					}
					else if (command is SettingInfo si && !string.IsNullOrEmpty(si.TextCommand))
					{
						commands.Add("get " + si.TextCommand);
						if (si.IsReadOnlySetting)
						{
							commands.Add("set " + si.TextCommand + " [x]");
						}
					}
				}
			}

			return commands.Distinct().OrderBy(c => c);
		}

		#endregion

		#region -- Non-Public Methods --

		private static IEnumerable<string> EnumeratePossibilities(ASCIICommandInfo aCommand)
			=> EnumeratePossibilities(string.Empty, aCommand.Nodes.ToList());


		private static IEnumerable<string> EnumeratePossibilities(string aPrefix,
																  IList<ASCIICommandNode> aRemainingNodes)
		{
			var node = aRemainingNodes?.FirstOrDefault();

			if (node != null)
			{
				var remainingNodes = aRemainingNodes.Skip(1).ToList();
				var space = string.IsNullOrEmpty(aPrefix) ? string.Empty : " ";
				var newPrefixes = new List<string>();

				if (node.IsParameter)
				{
					if (1 == node.ParamType.Arity)
					{
						if (node.IsEnumerableParameter)
						{
							newPrefixes.AddRange(node.ParamType.EnumValues);
						}
						else
						{
							newPrefixes.Add($"[{node.NodeText}]");
						}
					}
					else
					{
						newPrefixes.Add($"[{node.NodeText}...]");
					}
				}
				else
				{
					newPrefixes.Add(node.NodeText);
				}

				foreach (var prefix in newPrefixes)
				{
					var newPrefix = $"{aPrefix}{space}{prefix}";
					foreach (var cmd in EnumeratePossibilities(newPrefix, remainingNodes))
					{
						yield return cmd;
					}
				}
			}
			else if (!string.IsNullOrEmpty(aPrefix))
			{
				yield return aPrefix; // Base case; no more nodes.
			}
		}

		#endregion
	}
}

﻿using System;
using System.Windows;
using System.Xml.Serialization;

namespace ZaberConsole
{
	[Serializable]
	public class WindowSettings
	{
		#region -- Public Methods & Properties --

		#region -- Initialization and upgrades --

		public WindowSettings()
		{
			// default to an invalid location
			Location = new Point(int.MinValue, int.MinValue);
			WindowState = WindowVisibilityState.Normal;
		}

		#endregion

		#endregion

		#region -- Serialized properties --

		public enum WindowVisibilityState
		{
			Maximized,
			Minimized,
			Normal
		}

		public Point Location { get; set; }

		public Size Size { get; set; }

		public WindowVisibilityState WindowState { get; set; }

		public string MonitorName { get; set; }

		public int[] SplitterDistances { get; set; }

		public int[] SplitterSizes { get; set; }

		public string[] UserMeasurements { get; set; }

		#endregion

		#region -- Properties bound from XAML --

		// Xaml bindings cannot update members of value types, so these
		// properties are needed to correctly update the serialized values.
		[XmlIgnore]
		public double WindowWidth
		{
			get => Size.Width;
			set => Size = new Size(value, Size.Height);
		}


		[XmlIgnore]
		public double WindowHeight
		{
			get => Size.Height;
			set => Size = new Size(Size.Width, value);
		}


		[XmlIgnore]
		public double WindowLeft
		{
			get => Location.X;
			set => Location = new Point(value, Location.Y);
		}


		[XmlIgnore]
		public double WindowTop
		{
			get => Location.Y;
			set => Location = new Point(Location.X, value);
		}


		[XmlIgnore]
		public WindowState WindowStateWpf
		{
			get
			{
				switch (WindowState)
				{
					case WindowVisibilityState.Maximized:
						return System.Windows.WindowState.Maximized;
					case WindowVisibilityState.Minimized:
						return System.Windows.WindowState.Minimized;
					default:
						return System.Windows.WindowState.Normal;
				}
			}
			set
			{
				switch (value)
				{
					case System.Windows.WindowState.Maximized:
						WindowState = WindowVisibilityState.Maximized;
						break;
					case System.Windows.WindowState.Minimized:
						WindowState = WindowVisibilityState.Minimized;
						break;
					default:
						WindowState = WindowVisibilityState.Normal;
						break;
				}
			}
		}

		#endregion

		#region -- Helper methods --

		public void CenterOnMainScreen()
		{
			if (WindowVisibilityState.Normal == WindowState)
			{
				// Clamp window size to size of primary screen.
				if (WindowWidth > SystemParameters.PrimaryScreenWidth)
				{
					WindowWidth = SystemParameters.PrimaryScreenWidth;
				}

				if (WindowHeight > SystemParameters.PrimaryScreenHeight)
				{
					WindowHeight = SystemParameters.PrimaryScreenHeight;
				}

				// Center window on primary screen.
				WindowLeft = (SystemParameters.PrimaryScreenWidth - WindowWidth) / 2;
				WindowTop = (SystemParameters.PrimaryScreenHeight - WindowHeight) / 2;
			}
		}


		public void EnsureWindowVisible()
		{
			if (WindowVisibilityState.Normal == WindowState)
			{
				if (WindowWidth > SystemParameters.VirtualScreenWidth)
				{
					WindowWidth = SystemParameters.VirtualScreenWidth;
				}

				if (WindowHeight > SystemParameters.VirtualScreenHeight)
				{
					WindowHeight = SystemParameters.VirtualScreenHeight;
				}

				// If any part of the window is too far offscreen, center it.
				// Centering rather than clamping avoids cases where the window could
				// still be offscreen because of non-rectangular multi-monitor layouts.
				var screenRight = SystemParameters.VirtualScreenLeft + SystemParameters.VirtualScreenWidth;
				var screenBottom = SystemParameters.VirtualScreenTop + SystemParameters.VirtualScreenHeight;

				if (((WindowLeft + (WindowWidth / 2)) < SystemParameters.VirtualScreenLeft)
				|| ((WindowLeft + (WindowWidth / 2)) > screenRight)
				|| (WindowTop < SystemParameters.VirtualScreenTop)
				|| ((WindowTop + (WindowHeight / 2)) > screenBottom))
				{
					WindowLeft = ((SystemParameters.VirtualScreenLeft + screenRight) / 2) - (WindowWidth / 2);
					WindowTop = ((SystemParameters.VirtualScreenTop + screenBottom) / 2) - (WindowHeight / 2);
				}
			}
		}

		#endregion
	}
}

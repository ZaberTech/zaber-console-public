using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using log4net;
using Zaber;
using ZaberConsole.Dialogs.Calibration;

namespace ZaberConsole
{
	/// <summary>
	///     Helper class for high-level device interaction tasks.
	/// </summary>
	public class DeviceHelper
	{
		#region -- Public data --

		/// <summary>
		///     Severity enumeration for use with ProgressReportCallbacks.
		/// </summary>
		public enum ReportType
		{
			/// <summary>
			///     Messages to display to the user if there is visible output somewhere.
			/// </summary>
			Info,

			/// <summary>
			///     Messages that don't stop progress but should be brought to user attention, ie via a popup.
			/// </summary>
			Warning,

			/// <summary>
			///     Messages that do stop progress and should display an error.
			/// </summary>
			Error
		}


		/// <summary>
		///     Callback type for functions that do large amounts of communications with devices.
		/// </summary>
		/// <param name="aProgress">How much of the work is done. Ranges from 0 to 1.</param>
		/// <param name="aMessage">Status message for the user, or error message.</param>
		/// <param name="aReportType">Severity of the message.</param>
		public delegate void ProgressReportCallback(float aProgress, string aMessage, ReportType aReportType);

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Read all of the calibration tables from the device. Only works in ASCII mode.
		/// </summary>
		/// <param name="aConversation">Conversation with the device to list tables on.</param>
		/// <param name="aCallback">Invoked to report progress and error messages. May be invoked on random threads.</param>
		/// <returns>List of calibration tables with all data populated.</returns>
		public static IEnumerable<CalibrationTableVM> ReadAllCalibrationTables(Conversation aConversation, ProgressReportCallback aCallback)
		{
			var result = new List<CalibrationTableVM>();
			aCallback(0.0f, null, ReportType.Info);

			var device = aConversation?.Device;

			if (!CheckNotNull(device, aCallback)
			|| !CheckSingleDevice(device, aCallback)
			|| !CheckBootloader(device, aCallback)
			|| !CheckFirmwareVersion(device, new FirmwareVersion(6, 22), aCallback))
			{
				return result;
			}

			// Try "calibration list" first.
			var error = false;
			var listSucceeded = true;
			var serials = new List<uint>();
			aCallback(0.0f, null, ReportType.Info);
			try
			{
				DeviceListener.ReceiveListResponse(
					aConversation,
					aTopic => { device.Send("calibration list", aTopic.MessageId); },
					aMessage =>
					{
						if (aMessage.IsError)
						{
							error = true;
						}

						if (!error)
						{
							if (MessageType.Comment == aMessage.MessageType)
							{
								var matches = Regex.Matches(aMessage.Body, "serial ([0-9]+)");
								if ((1 == matches.Count) && (2 == matches[0].Groups.Count))
								{
									serials.Add(uint.Parse(matches[0].Groups[1].Value, CultureInfo.InvariantCulture));
								}
								else
								{
									listSucceeded = false;
									_log.Error($"Could not parse calibration list payload '{aMessage.Body}'.");
								}
							}
						}
					},
					new TimeoutTimer {Timeout = 250});
			}
			catch (ErrorResponseException)
			{
				error = true;
			}

			if (!listSucceeded)
			{
				aCallback(1.0f, "The device response contained unexpected data.", ReportType.Error);
			}
			else if (!error) 
			{
				// The "calibration list" command was accepted; read all the listed tables.
				// They are keyed by peripheral serial number in this case, the the command
				// output provided all the serial numbers.
				foreach (var serialNo in serials)
				{
					var table = ReadCalibrationTable(aConversation, serialNo, aCallback);
					if (null != table)
					{
						result.Add(table);
					} 
				}

				aCallback(1.0f, null, ReportType.Info);
			}
			else // "calibration list" was rejected; this must be an integrated device or a FW7 device.
			{
				int calibrationType = 0;
				try
				{
					var response = aConversation.Request("get calibration.type");
					if (!response.IsError)
					{
						calibrationType = (int) response.NumericData;
					}
				}
				catch (ErrorResponseException)
				{
					_log.Warn("The device rejected the 'get calibration.type' command.");
					aCallback(1.0f, "The device responded with an error.", ReportType.Error);
					return result;
				}

				// 0 means no calibration data.
				if (0 != calibrationType) 
				{
					// For these cases the calibration table is not really keyed by serial number, but
					// we will store the peripheral or device serial number with it for user reference.
					uint serialNo = 0;
					DeviceMessage response = null;
					if (device.DeviceType.Commands.Any(cmd => (cmd is SettingInfo si) && ("peripheral.serial" == si.TextCommand)))
					{
						// We should always associate the data with the peripheral serial number
						// if it exists.
						response = aConversation.Request("get peripheral.serial");
					}
					else if ((device.FirmwareVersion >= 700) && (device.AxisNumber > 0) && !device.DeviceType.Capabilities.Contains(Capability.AutoDetect))
					{
						// For FW7 controllers connected to legacy peripherals there may not be
						// a peripheral serial number, so default to the axis number to provide at least
						// some information about where the data came from.
						serialNo = (uint)device.AxisNumber;
					}
					else
					{
						// For integrated devices, associate the data with the device serial number.
						response = aConversation.Request("get system.serial");
					}

					if ((null != response) && !response.IsError)
					{
						serialNo = (uint)response.NumericData;
					}

					var table = ReadCalibrationTable(aConversation, null, aCallback);
					if (table != null)
					{
						table.SerialNumber = serialNo;
						table.CalibrationType = calibrationType;
						result.Add(table);
					}
				}

				aCallback(1.0f, null, ReportType.Info);
			}

			return result;
		}


		/// <summary>
		///     Read a calibration table from a device.
		/// </summary>
		/// <param name="aConversation">Device conversation to use for communication.</param>
		/// <param name="aSerialNumber">Serial number of the table to read, if the tables are
		/// indexed by serial number on this device.</param>
		/// <param name="aCallback">Progress report callback.</param>
		/// <returns>The contents of the calibration table, with metadata.</returns>
		public static CalibrationTableVM ReadCalibrationTable(Conversation aConversation, uint? aSerialNumber,
															  ProgressReportCallback aCallback)
		{
			aCallback(0.0f, null, ReportType.Info);

			var device = aConversation?.Device;

			if (!CheckNotNull(device, aCallback)
			|| !CheckSingleDevice(device, aCallback)
			|| !CheckBootloader(device, aCallback)
			|| !CheckFirmwareVersion(device, new FirmwareVersion(6, 22), aCallback))
			{
				return null;
			}

			// Data storage commands on a smart peripheral can be slow to respond.
			var timeout = (device.IsPeripheral 
			               && (device.FirmwareVersion.Major >= 7) 
			               && device.DeviceType.Capabilities.Contains(Capability.AutoDetect)) 
				? 5000 : 250;

			try
			{
				_log.Info("Starting calibration table transfer.");

				var result = new CalibrationTableVM
				{
					Description = "Retrieved from " + device.Description,
					SerialNumber = aSerialNumber ?? 0
				};

				var requestStr = "calibration print";
				if (aSerialNumber.HasValue)
				{
					requestStr += " " + aSerialNumber.Value;
				}

				var data = new List<int>();

				try
				{
					DeviceListener.ReceiveListResponse(
						aConversation,
						aTopic => { aConversation.Device.Send(requestStr, aTopic.MessageId); },
						aMessage =>
						{
							if (!aMessage.IsError)
							{
								if (MessageType.Response == aMessage.MessageType)
								{
									result.CalibrationType = (int)aMessage.NumericValues[0];
									if (aMessage.NumericValues.Count > 1)
									{
										result.TableWidth = (int) aMessage.NumericValues[1];
										if (aMessage.NumericValues.Count > 2)
										{
											result.TableOffset = (int) aMessage.NumericValues[2];
										}
										else
										{
											result.TableOffset = 0;
										}
									}
								}
								else if (MessageType.Comment == aMessage.MessageType)
								{
									var matches = Regex.Matches(aMessage.Body,
										"data (-?[0-9]+) (-?[0-9]+) (-?[0-9]+) (-?[0-9]+)");
									if ((1 == matches.Count) && (5 == matches[0].Groups.Count))
									{
										for (var i = 0; i < 4; ++i)
										{
											data.Add(
												int.Parse(matches[0].Groups[i + 1].Value,
													CultureInfo.InvariantCulture));
										}

										aCallback(data.Count / 64.0f, null, ReportType.Info);
									}
								}
							}
						},
						new TimeoutTimer {Timeout = timeout});

					if (0 == result.CalibrationType)
					{
						aCallback(1.0f, "No table found.", ReportType.Info);
						return null;
					}

					result.Data = new ObservableCollection<int>(data);

					_log.Info("Calibration table transferred successfully.");
					aCallback(1.0f, "Table retrieved successfully.", ReportType.Info);

					return result;
				}
				catch (ErrorResponseException)
				{
					aCallback(1.0f, "Error while reading calibration data.", ReportType.Error);
				}
			}
			catch (Exception aException)
			{
				_log.Error("Exception thrown while reading calibration table.", aException);
				aCallback(1.0f, Properties.Resources.STR_CALIBRATION_TRANSFER_ERROR, ReportType.Error);
			}

			return null;
		}


		public static void WriteCalibrationTable(Conversation aConversation, CalibrationTableVM aTable,
												 ProgressReportCallback aCallback)
		{
			aCallback(0.0f, null, ReportType.Info);

			var device = aConversation?.Device;

			if (!CheckNotNull(device, aCallback)
			|| !CheckSingleDevice(device, aCallback)
			|| !CheckBootloader(device, aCallback)
			|| !CheckFirmwareVersion(device, new FirmwareVersion(6, 22), aCallback))
			{
				return;
			}

			if (aTable is null)
			{
				aCallback(1.0f, Properties.Resources.STR_CALIBRATION_NULL_TABLE, ReportType.Error);
				return;
			}

			if (1 != aTable.DataVersion)
			{
				aCallback(1.0f, string.Format(Properties.Resources.STR_CALIBRATION_UNSUPPORTED_TABLE_VERSION, aTable.DataVersion), ReportType.Error);
				return;
			}

			if (device.DeviceType.FirmwareVersion.Major < 7)
			{
				if (aTable.TableOffset != 0)
				{
					aCallback(1.0f, Properties.Resources.STR_CALIBRATION_OFFSET_NOT_SUPPORTED, ReportType.Warning);
					return;
				}

				if (device.AxisNumber > 0)
				{
					aCallback(1.0f, Properties.Resources.STR_CALIBRATION_NOT_SUPPORTED_FW6_PERIPHERAL, ReportType.Warning);
					return;
				}
			}

			try
			{
				_log.Info("Starting calibration table transfer.");

				var requestStr = "calibration load start";
				if (device.IsController && (device.FirmwareVersion.Major < 7)) 
				{
					// FW6 controller - tables are indexed by peripheral serial number.
					requestStr += " " + aTable.SerialNumber;
				}

				requestStr += $" {aTable.CalibrationType} {aTable.TableWidth}";

				if (device.DeviceType.FirmwareVersion.Major >= 7)
				{
					requestStr += $" {aTable.TableOffset}";
				}

				var response = aConversation.Request(requestStr);
				var toSend = (int)response.NumericData;

				if (toSend > aTable.Data.Count)
				{
					throw new InvalidOperationException(string.Format(Properties.Resources.STR_CALIBRATION_TABLE_SIZE_MISMATCH, toSend, aTable.Data.Count));
				}

				aCallback(0.1f, null, ReportType.Info);

				var index = 0;
				while (toSend > 0)
				{
					requestStr = "calibration load";
					for (var i = 0; i < 4; ++i)
					{
						requestStr += " " + aTable.Data[index];
						index++;
					}

					response = aConversation.Request(requestStr);
					toSend = (int)response.NumericData;

					aCallback(1.0f - (0.9f * (toSend / (float) aTable.Data.Count)), null, ReportType.Info);
				}

				_log.Info("Calibration table transferred successfully.");
				aCallback(1.0f, "Calibration table transferred successfully.", ReportType.Info);
			}
			catch (Exception aException)
			{
				var err = aException as ErrorResponseException;
				if ((null != err) 
					&& (("FULL" == err.Response.TextData) || ("ALREADYEXISTS" == err.Response.TextData)))
				{
					_log.Error("Device calibration memory is full.", aException);
					aCallback(1.0f, Properties.Resources.STR_CALIBRATION_DEVICE_FULL, ReportType.Warning);
				}
				else if ((null != err)
					&& (("BADCOMMAND" == err.Response.TextData) || ("BADDATA" == err.Response.TextData)))
				{
					_log.Error("Device rejected the calibration command.", aException);
					aCallback(1.0f, Properties.Resources.STR_CALIBRATION_REJECTED, ReportType.Warning);
				}
				else
				{
					_log.Error("Exception thrown while transferring calibration table.", aException);
					aCallback(1.0f, Properties.Resources.STR_CALIBRATION_TRANSFER_ERROR, ReportType.Error);
				}
			}
		}


		/// <summary>
		///    Returns the list of axes that have user calibration data applied. This method must
		///    be used on a controller; use on a peripheral or integrated device will result in error.
		/// </summary>
		/// <param name="aConversation">Conversation for the controller to query.</param>
		/// <returns>Zero or more 1-based axes numbers, in ascending order, where the
		/// axis has stored calibration data.</returns>
		public static IEnumerable<int> ListCalibratedAxes(Conversation aConversation)
		{
			if (aConversation is null)
			{
				throw new ArgumentNullException(nameof(aConversation));
			}

			var device = aConversation.Device;

			if (!device.IsController)
			{
				throw new ArgumentException("This method only works on controllers.");
			}

			var calibratedAxes = new List<int>();
			try
			{
				var response = aConversation.Request("get calibration.type");
				response.ForEachNumericValue((aIndex, aCalibrationType) =>
				{
					if (aCalibrationType.HasValue && (aCalibrationType.Value > 0))
					{
						calibratedAxes.Add(aIndex + 1);
					}
				});
			}
			catch (ErrorResponseException)
			{
				// Eat error responses because if the device rejected this command,
				// it must not support calibration or not have any axes that can be
				// calibrated.
			}

			return calibratedAxes;
		}

		#endregion

		#region -- Non-Public Methods --

		private static bool CheckNotNull(object aDevice, ProgressReportCallback aCallback)
		{
			if (aDevice is null)
			{
				aCallback(1.0f, Properties.Resources.STR_CALIBRATION_MUST_SELECT_DEVICE, ReportType.Error);
				return false;
			}

			return true;
		}


		private static bool CheckSingleDevice(ZaberDevice aDevice, ProgressReportCallback aCallback)
		{
			if (!aDevice.IsSingleDevice || aDevice is DeviceCollection)
			{
				aCallback(1.0f, Properties.Resources.STR_CALIBRATION_MUST_SELECT_SINGLE_DEVICE, ReportType.Error);
				return false;
			}

			return true;
		}


		private static bool CheckBootloader(ZaberDevice aDevice, ProgressReportCallback aCallback)
		{
			if (aDevice.IsInBootloaderMode)
			{
				aCallback(1.0f, Properties.Resources.STR_CALIBRATION_BOOTLOADER_MODE, ReportType.Error);
				return false;
			}

			return true;
		}


		private static bool CheckFirmwareVersion(ZaberDevice aDevice, FirmwareVersion aMinVersion,
												 ProgressReportCallback aCallback)
		{
			if (aDevice.FirmwareVersion < aMinVersion)
			{
				aCallback(1.0f, string.Format(Properties.Resources.STR_CALIBRATION_FW_UNSUPPORTED, aDevice.FirmwareVersion), ReportType.Error);
				return false;
			}

			return true;
		}

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace ZaberConsole
{
	/// <summary>
	///     File system helper functions for Zaber Console.
	/// </summary>
	public static class FileUtil
	{
		#region -- Public data --

		/// <summary>
		///     Mode control for CopyDirectory - determines how file conflicts are resolved if
		///     a destination file already exists and is different from the corresponding source file.
		/// </summary>
		public enum CollisionResolveMode
		{
			/// <summary>
			///     Do not copy files or create directories - just return an collection of possibly conflicting file names.
			/// </summary>
			PreviewCollisions,

			/// <summary>
			///     Assume yes for prompts and force-overwrite all conflicting files.
			/// </summary>
			ForceOverwrite,

			/// <summary>
			///     Assume no for prompts and only copy non-conflicting files.
			/// </summary>
			SkipCollisions
		}

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Recursively copies all files from a source directory to a destination directory. Optionally prompts
		///     user whether or not to overwrite existing files that are different. Also optionally does no action
		///     but finds all files that would result in a user prompt.
		/// </summary>
		/// <comments>Does not copy hidden files.</comments>
		/// <param name="aSource">The directory to copy from.</param>
		/// <param name="aDest">The directory to copy to.</param>
		/// <param name="aResolutionMode">How to resolve comflicts.</param>
		/// <returns>
		///     A set of conflicting files (files that aready exist in the destination and are
		///     different from the corresponding source files) if
		///     <cref>ZaberConsole.FileUtil.CollisionResolveMode.PreviewCollisions</cref> is used,
		///     or nothing otherwise.
		/// </returns>
		/// <exception cref="SecurityException">
		///     Program does not have sufficient permissions to overwrite a file.
		/// </exception>
		/// <exception cref="ArgumentNullException">
		///     Destination path string is null.
		/// </exception>
		/// <exception cref="DirectoryNotFoundException">
		///     Source or destination directory does not exist or is not a directory.
		/// </exception>
		/// <exception cref="PathTooLongException">
		///     The source or destination path after recursive expansion is longer than 248 directory characters or 260 filename
		///     characters.
		/// </exception>
		/// <exception cref="NotSupportedException">
		///     There are invalid characters in the source or desination path string.
		/// </exception>
		/// <exception cref="IOException">
		///     A filename to be copied already exists as a directory in the destination.
		/// </exception>
		/// <exception cref="UnauthorizedAccessException">
		///     Program does not have sufficient permissions to overwrite a file.
		/// </exception>
		public static IEnumerable<string> CopyDirectory(string aSource, string aDest,
														CollisionResolveMode aResolutionMode)
		{
			var conflicts = new List<string>();

			// Create the destination folder if missing.
			if (!Directory.Exists(aDest) && (aResolutionMode != CollisionResolveMode.PreviewCollisions))
			{
				Directory.CreateDirectory(aDest);
			}

			var dirInfo = new DirectoryInfo(aSource);

			// Copy all files.
			foreach (var fileInfo in dirInfo.GetFiles())
			{
				if (!IsHidden(fileInfo))
				{
					var destFile = Path.Combine(aDest, fileInfo.Name);
					if (File.Exists(destFile))
					{
						// We don't need to copy if the source and dest files are the same, but
						// if they are different we should either copy or prompt the user.
						if (!FilesAreEqual(new FileInfo(destFile), fileInfo))
						{
							switch (aResolutionMode)
							{
								case CollisionResolveMode.PreviewCollisions:
									conflicts.Add(destFile);
									break;

								case CollisionResolveMode.ForceOverwrite:
									fileInfo.CopyTo(destFile, true);
									break;
							}
						}
					}
					else if (aResolutionMode != CollisionResolveMode.PreviewCollisions)
					{
						fileInfo.CopyTo(destFile, true);
					}
				}
			}

			// Recursively copy all sub-directories.
			foreach (var subDirectoryInfo in dirInfo.GetDirectories())
			{
				if (!IsHidden(subDirectoryInfo))
				{
					conflicts.AddRange(CopyDirectory(subDirectoryInfo.FullName,
													 Path.Combine(aDest, subDirectoryInfo.Name),
													 aResolutionMode));
				}
			}

			return conflicts;
		}


		/// <summary>
		///     Determine if two files have the same content.
		/// </summary>
		/// <param name="aFile1">Path to the first file.</param>
		/// <param name="aFile2">Path to the second file.</param>
		/// <returns>True if both files have the same data, or false otherwise.</returns>
		public static bool FilesAreEqual(FileInfo aFile1, FileInfo aFile2)
		{
			if (aFile1.Length != aFile2.Length)
			{
				return false;
			}

			using (var stream1 = aFile1.OpenRead())
			{
				using (var stream2 = aFile2.OpenRead())
				{
					const int bufferSize = 4096;
					var buffer1 = new byte[bufferSize];
					var buffer2 = new byte[bufferSize];

					var bytesRead = 0;
					do
					{
						bytesRead = stream1.Read(buffer1, 0, bufferSize);
						stream2.Read(buffer2, 0, bufferSize);

						for (var i = 0; i < bytesRead; i++)
						{
							if (buffer1[i] != buffer2[i])
							{
								return false;
							}
						}
					} while (bytesRead != 0);
				}
			}

			return true;
		}


		/// <summary>
		///     Helper to determine if a file is hidden at the file system level.
		/// </summary>
		/// <param name="aFile">FileInfo structure about the file</param>
		/// <returns>True if this is a hidden file.</returns>
		public static bool IsHidden(FileSystemInfo aFile)
		{
			var isFileHidden = (aFile.Attributes & FileAttributes.Hidden) != 0;
			return isFileHidden;
		}


		/// <summary>
		///     Get the MD5 hash of a file's content.
		/// </summary>
		/// <param name="aPath">Absolute or relative path to the file.</param>
		/// <returns>an MD5 hash of the file. Exceptions are thown if the file can't be read.</returns>
		public static byte[] GetFileHash(string aPath)
		{
			byte[] hash = null;

			using (var md5 = MD5.Create())
			{
				using (var stream = File.OpenRead(aPath))
				{
					stream.Position = 0L;
					hash = md5.ComputeHash(stream);
				}
			}

			return hash;
		}


		/// <summary>
		///     Get the MD5 hash of a file's content as a hex-formatted string.
		/// </summary>
		/// <param name="aPath">Relative or absolute path to the file to read.</param>
		/// <returns>
		///     A string containing a hexadecimal printout of the
		///     file's content hash. Exceptions are thrown on error.
		/// </returns>
		public static string GetFileHashAsString(string aPath)
		{
			var hash = GetFileHash(aPath);

			var sb = new StringBuilder();
			foreach (var b in hash)
			{
				sb.AppendFormat("{0:X2}", b);
			}

			return sb.ToString();
		}


		/// <summary>
		///     Determines whether file exists and can be written.
		/// </summary>
		/// <param name="path">File path.</param>
		/// <returns>True if file exists and can be written. False otherwise.</returns>
		public static bool CanBeWritten(string path)
		{
			try
			{
				using (File.OpenWrite(path))
				{
					return true;
				}
			}
			catch (IOException)
			{
				return false;
			}
			catch (UnauthorizedAccessException)
			{
				return false;
			}
		}


		/// <summary>
		///     Returns file path within specified directory.
		///     If file is not in the directory, returns its full path.
		/// </summary>
		public static string GetPathInDirectory(DirectoryInfo directory, FileInfo file)
			=> GetPathInDirectory(directory.FullName, file.FullName);


		/// <summary>
		///     Returns file path within specified directory.
		///     If file is not in the directory, returns its full path.
		/// </summary>
		public static string GetPathInDirectory(string directory, string file)
		{
			if (!IsInDirectory(directory, file))
			{
				return file;
			}

			var directoryPath = NormalizePath(directory);
			var filePath = NormalizePath(file);

			return filePath.Substring(directoryPath.Length + 1);
		}


		/// <summary>
		///     Checks whether file is in the directory. Checks only path not existence of file.
		/// </summary>
		/// <returns>True if file is in the directory, false otherwise.</returns>
		public static bool IsInDirectory(DirectoryInfo directory, FileInfo file)
			=> IsInDirectory(directory.FullName, file.FullName);


		/// <summary>
		///     Checks whether file is in the directory. Checks only path not existence of file.
		/// </summary>
		/// <returns>True if file is in the directory, false otherwise.</returns>
		public static bool IsInDirectory(string directory, string file)
		{
			var directoryPath = NormalizePath(directory) + Path.DirectorySeparatorChar;
			var filePath = NormalizePath(file);

			return filePath.StartsWith(directoryPath, StringComparison.OrdinalIgnoreCase);
		}


		/// <summary>
		///     Turns path into canonical representation.
		///     Unifies slashes, dedup slashes, trims slashes.
		/// </summary>
		/// <returns>Canonical path.</returns>
		public static string NormalizePath(string path)
		{
			var unfiedSlashes = new Uri(path).LocalPath;
			var withoutDuplicates = Regex.Replace(unfiedSlashes,
												  $"\\{Path.DirectorySeparatorChar}+",
												  Path.DirectorySeparatorChar.ToString());
			var withoutTrailing = withoutDuplicates.TrimEnd(Path.DirectorySeparatorChar);
			return withoutTrailing;
		}


		/// <summary>
		/// Determines if a given path points to a specified base directory or something
		/// contained in that directory. Does not actually check the file system for
		/// existence of anything.
		/// </summary>
		/// <param name="aBaseDir">Directory name to test for being a prefix of the other path.</param>
		/// <param name="aPath">Path to test for membership or identity with the base dir.</param>
		/// <returns>True if the path is or is contained by the base dir.</returns>
		public static bool IsInOrBelowDirectory(string aBaseDir, string aPath)
		{
			if (string.IsNullOrEmpty(aPath))
			{
				return false;
			}

			var result = false;

			var pathInfo = new DirectoryInfo(Path.GetDirectoryName(aPath));
			var rootInfo = new DirectoryInfo(aBaseDir.TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar));

			while (pathInfo != null)
			{
				if (pathInfo.FullName == rootInfo.FullName)
				{
					result = true;
					break;
				}
				else
				{
					pathInfo = pathInfo.Parent;
				}
			}

			return result;
		}

		#endregion
	}
}

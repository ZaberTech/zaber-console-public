﻿using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.Threading;
using Zaber.Threading;
using ZaberConsole.Properties;

namespace ZaberConsole
{
	public partial class DatabaseUpdaterVM
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Starts a background task to see if a new database is available,
		///     given the path to the one currently in use.
		/// </summary>
		/// <param name="aDatabasePath">Path to the existing database file.</param>
		public void CheckForUpdates(string aDatabasePath)
		{
			var timeStampForUpdateCheck = File.GetLastWriteTimeUtc(aDatabasePath);
			#if DEV || TEST
			// DEV is bundled with the public database to reduce size, but DEV users
			// should always download a new internal database after installing.
			if (Configuration.IsInProgramInstallDir(aDatabasePath))
			{
				timeStampForUpdateCheck = DateTime.MinValue;
			}
			#endif

			CheckForUpdates(timeStampForUpdateCheck);
		}


		/// <summary>
		///     Starts a background task to see if a newer database is available.
		/// </summary>
		/// <param name="aCurrentDbTimestamp">Detect if the downloadable database is newer than this date.</param>
		public void CheckForUpdates(DateTime aCurrentDbTimestamp)
		{
			if (!_enabled)
			{
				_log.Error("Database update check skipped due to initialization error.");
				return;
			}

			lock (_syncRoot)
			{
				if (_worker is null)
				{
					_updateIsAvailable = false;
					_worker = BackgroundWorkerManager.CreateWorker();
				}
				else
				{
					return;
				}
			}

			_worker.DoWork += (aSender, aArgs) =>
			{
				aArgs.Result = false;
				_log.Info(
					$"Checking for database updates using {aCurrentDbTimestamp} as database last-modified timestamp.");

				_previousState = CurrentState;
				_dispatcher.BeginInvoke(() =>
				{
					CurrentState = State.CheckingForUpdates;
					ProgressMaximum = 30.0;
					Progress = 1.0;
				});

				// Make a fake progress bar with a timer since there is no progress
				// for a web header check and it will be either very fast or time out.
				EventHandler progressUpdateCallback = (__, _) =>
				{
					_dispatcher.BeginInvoke(() => { Progress += 1.0; });
				};

				_timer.Tick += progressUpdateCallback;
				_timer.Interval = TimeSpan.FromMilliseconds(100);

				try
				{
					_timer.Start();
					var startTime = DateTime.Now;
					var onlineTimestamp = GetOnlineDatabaseTimestamp();
					if (onlineTimestamp > aCurrentDbTimestamp)
					{
						aArgs.Result = true;
					}

					if (_worker.CancellationPending)
					{
						aArgs.Cancel = true;
						return;
					}

					// Make sure the update check takes at least half a second 
					// so it looks like something happened. If the button changes
					// state too fast it looks like a glitch.
					while ((DateTime.Now - startTime).TotalMilliseconds < 500)
					{
						Thread.Sleep(100);
					}
				}
				finally
				{
					_timer.Stop();
					_timer.Tick -= progressUpdateCallback;
				}
			};

			_worker.RunWorkerCompleted += (aSender, aArgs) =>
			{
				lock (_syncRoot)
				{
					_worker = null;
				}

				if (aArgs.Cancelled)
				{
					_log.Warn("Update check was cancelled by user.");
					_dispatcher.BeginInvoke(() => { CurrentState = _previousState; });
				}
				else if (aArgs.Error != null)
				{
					_log.Warn("Checking for a database update failed.", aArgs.Error);
					_dispatcher.BeginInvoke(() =>
					{
						CurrentState = State.Error;
						ErrorMessage = string.Format(Resources.ERR_DB_UPDATE_CHECK_FAILED, aArgs.Error.Message);
					});
				}
				else
				{
					_updateIsAvailable = (bool) aArgs.Result;
					if (_updateIsAvailable)
					{
						_log.Info("A new database is available for download.");
						_dispatcher.BeginInvoke(() =>
						{
							CurrentState = State.UpdateAvailable;
							if (_settings.AutomaticallyDownloadUpdates)
							{
								StartDownload();
							}
						});
					}
					else
					{
						_log.Info("The current database is up to date.");
						_dispatcher.BeginInvoke(() => { CurrentState = State.UpToDate; });
					}
				}
			};

			_worker.WorkerSupportsCancellation = true;

			_worker.Run();
		}

		#endregion

		#region -- Non-Public Methods --

		private DateTime GetOnlineDatabaseTimestamp()
		{
			var req = WebRequest.Create(_downloadUrl);
			req.Method = "HEAD";

			if (Settings.Default.DisableWebProxy)
			{
				// If the user doesn't need a web proxy, this speeds things up by 1-2 seconds.
				req.Proxy = null;
			}

			if (req is HttpWebRequest httpReq)
			{
				var assemblyName = Assembly.GetExecutingAssembly().GetName();
				httpReq.UserAgent = string.Format("{0}/{1}", assemblyName.Name, assemblyName.Version);
			}

			_log.Info($"Checking for a new database version from {_downloadUrl}.");
			using (var response = req.GetResponse())
			{
				if (response is HttpWebResponse webResponse)
				{
					return webResponse.LastModified;
				}

				if (response is FileWebResponse fileResponse)
				{
					var path = fileResponse.ResponseUri.LocalPath;
					return File.GetLastWriteTimeUtc(path);
				}

				throw new InvalidOperationException($"Don't know how to get time stamp of {_downloadUrl}!");
			}
		}


		/// <summary>
		///     Begin downloading a database update if not already in progress.
		/// </summary>
		private void StartDownload()
		{
			lock (_syncRoot)
			{
				if (_worker is null)
				{
					_worker = BackgroundWorkerManager.CreateWorker();
				}
				else
				{
					_log.Warn("Database download is already in progress.");
					return;
				}
			}

			_worker.WorkerSupportsCancellation = true;

			_worker.DoWork += (aSender, aArgs) =>
			{
				aArgs.Result = null;
				_previousState = CurrentState;

				_dispatcher.BeginInvoke(() => { CurrentState = State.Downloading; });

				var srcName = DownloadDatabase();
				var destName = CreateNewDatabaseName();
				File.Move(srcName, destName);
				_updateIsAvailable = false;
				_log.Info($"New database file is available at {destName}.");
				if (_worker.CancellationPending)
				{
					aArgs.Cancel = true;
					_log.Warn($"Deleting {destName} because the download was aborted.");
					TempFileManager.RegisterForDelete(destName);
					TempFileManager.TryDeleteFiles();
				}
				else
				{
					aArgs.Result = destName;
				}
			};

			_worker.RunWorkerCompleted += (aSender, aArgs) =>
			{
				lock (_syncRoot)
				{
					_worker = null;
				}

				if (aArgs.Cancelled || aArgs.Error is OperationCanceledException)
				{
					_dispatcher.BeginInvoke(() => { CurrentState = _previousState; });

					ShowStatusMessage?.Invoke(this, Resources.STR_DB_DOWNLOAD_CANCELLED, STATUS_DURATION);
				}
				else if (aArgs.Error != null)
				{
					_log.Warn("Database download was aborted by an error:", aArgs.Error);
					_dispatcher.BeginInvoke(() =>
					{
						CurrentState = State.Error;
						ErrorMessage = string.Format(Resources.ERR_DB_DOWNLOAD_FAILED, aArgs.Error.Message);
					});
				}
				else
				{
					LatestDatabasePath = (string) aArgs.Result;
					_settings.ImportedDatabasePath = null;
					_dispatcher.BeginInvoke(() =>
					{
						OnPropertyChanged(nameof(UsingImportedDatabase));
						CurrentState = State.UpdateReady;
					});

					_eventLog.LogEvent("Database downloaded", $"File timestamp = {File.GetLastWriteTimeUtc(LatestDatabasePath)}");
				}
			};

			_worker.WorkerSupportsCancellation = true;

			_worker.Run();
		}


		/// Downloads the database to a temp file. Throws an exception on error.
		/// Also updates the button progress bar as it goes. Returns the path to the temp file
		/// on success.
		private string DownloadDatabase()
		{
			_dispatcher.BeginInvoke(() => { Progress = 0; });

			var req = WebRequest.Create(_downloadUrl);

			if (Settings.Default.DisableWebProxy)
			{
				// If the user doesn't need a web proxy, this speeds things up by 1-2 seconds.
				req.Proxy = null;
			}

			if (req is HttpWebRequest httpReq)
			{
				var assemblyName = Assembly.GetExecutingAssembly().GetName();
				httpReq.UserAgent = string.Format("{0}/{1}", assemblyName.Name, assemblyName.Version);
			}

			_log.Info($"Downloading new database version from {_downloadUrl}.");
			var response = req.GetResponse();

			var bytesToDownload = response.ContentLength;
			_dispatcher.BeginInvoke(() =>
			{
				// Estimate the decompressed file size at ten times the compressed size.
				ProgressMaximum = 11 * bytesToDownload;
			});

			// Download the online file to a temporary file.
			var tempFileName = Path.GetTempFileName();
			Stream stream = null;
			FileStream lzmaFile = null;
			try
			{
				stream = response.GetResponseStream();
				lzmaFile = File.Open(tempFileName, FileMode.Truncate);
				var buffer = new byte[8 * 1024];
				int len;
				var lastProgressUpdate = DateTime.Now;
				var totalRead = 0;
				while ((len = stream.Read(buffer, 0, buffer.Length)) > 0)
				{
					lzmaFile.Write(buffer, 0, len);
					totalRead += len;

					if ((DateTime.Now - lastProgressUpdate).TotalMilliseconds > 100) // Avoid spamming UI updates.
					{
						lastProgressUpdate = DateTime.Now;
						_dispatcher.BeginInvoke(() => { Progress = totalRead; });
					}

					if (_worker.CancellationPending)
					{
						throw new OperationCanceledException("Database download was cancelled.");
					}
				}
			}
			catch (Exception)
			{
				// Clean up temp file on error or cancel.
				if (null != lzmaFile)
				{
					lzmaFile.Close();
					lzmaFile.Dispose();
					TempFileManager.RegisterForDelete(tempFileName);
					TempFileManager.TryDeleteFiles();
					lzmaFile = null;
				}

				throw;
			}
			finally
			{
				stream?.Close();
				if (response is IDisposable disposable)
				{
					disposable.Dispose();
				}
			}

			_log.Info("Downloaded successfully. Decompressing database...");

			// We need to give LzmaDecompress a FileStream because it requires
			// a seekable input stream, which a NetworkStream is not.
			FileStream sqliteFile = null;
			var decompressedFileName = Path.GetTempFileName();

			// The LZMA decompresser implementation currently doesn't report progress, so 
			// instead we'll watch the file size from a UI thread timer callback.
			EventHandler timerUpdateCallback = (__, _) =>
			{
				_dispatcher.BeginInvoke(() =>
				{
					try
					{
						Progress = bytesToDownload + new FileInfo(decompressedFileName).Length;
					}
					catch (IOException)
					{
					} // Can happen if decompression is cancelled.
				});
			};

			_dispatcher.BeginInvoke(() => { CurrentState = State.Decompressing; });

			try
			{
				lzmaFile.Flush();
				lzmaFile.Seek(0, SeekOrigin.Begin);
				sqliteFile = File.Create(decompressedFileName);

				_timer.Interval = TimeSpan.FromMilliseconds(100);
				_timer.Tick += timerUpdateCallback;
				_timer.Start();

				var decompressionThread = ThreadFactory.CreateThread(() =>
				{
					CompressionHelper.LzmaDecompress(
						lzmaFile,
						sqliteFile);
				});

				decompressionThread.Start();

				while (decompressionThread.IsAlive)
				{
					Thread.Sleep(100);

					if (_worker.CancellationPending)
					{
						decompressionThread.Abort();
					}
				}

				if (_worker.CancellationPending)
				{
					throw new OperationCanceledException("Database decompression was cancelled.");
				}
			}
			catch (Exception)
			{
				// If the thread was aborted while decompressing the file, clean up
				// the incomplete file.
				try
				{
					sqliteFile?.Close();
					sqliteFile = null;
					File.Delete(decompressedFileName);
				}
				catch (IOException aIoException)
				{
					_log.Error(
						"Thread was aborted during database download and there was an error cleaning up the partial download.",
						aIoException);
				}

				throw;
			}
			finally
			{
				lzmaFile?.Close();
				sqliteFile?.Close();
				_timer.Stop();
				_timer.Tick -= timerUpdateCallback;
			}

			_log.Info("Decompression successful.");
			_dispatcher.BeginInvoke(() => { Progress = ProgressMaximum; });

			// Clean up temp file. 
			TempFileManager.RegisterForDelete(tempFileName);
			TempFileManager.TryDeleteFiles();

			return decompressedFileName; // Caller is responsible for moving or deleting this file.
		}

		#endregion
	}
}

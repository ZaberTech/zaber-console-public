﻿using System;

namespace ZaberConsole.Ports
{
	/// <summary>
	///     Attribute to specify what class serves as the settings for a port factory.
	/// </summary>
	public class PortSettingsTypeAttribute : Attribute
	{
		#region -- Public Methods & Properties --

		public PortSettingsTypeAttribute(Type aSettingsType)
		{
			if (!typeof(PortSettings).IsAssignableFrom(aSettingsType) || aSettingsType.IsAbstract)
			{
				throw new ArgumentException(
					$"Settings type must be an instantiable subclass of {nameof(PortSettings)}.");
			}

			SettingsType = aSettingsType;
		}


		public Type SettingsType { get; }

		#endregion
	}
}

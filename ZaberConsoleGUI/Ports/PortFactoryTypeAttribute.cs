﻿using System;

namespace ZaberConsole.Ports
{
	/// <summary>
	///     Attribute used to match saved user port settings to the factory type that
	///     interprets the settings and creates the ports.
	/// </summary>
	public class PortFactoryTypeAttribute : Attribute
	{
		#region -- Public Methods & Properties --

		public PortFactoryTypeAttribute(Type aFactoryType)
		{
			if (!typeof(IPortFactory).IsAssignableFrom(aFactoryType) || aFactoryType.IsAbstract)
			{
				throw new ArgumentException(
					"Type provided to PortTypeAttribute must implement IPortFactory and be instantiable.");
			}

			FactoryType = aFactoryType;
		}


		/// <summary>
		///     Type of the port factory - must be a concrete type that implements IPortFactory;
		/// </summary>
		public Type FactoryType { get; }

		#endregion
	}
}

﻿using System;
using System.ComponentModel;
using Zaber;
using Zaber.Ports;
using ZaberWpfToolbox;

namespace ZaberConsole.Ports
{
	[DisplayName(PORT_TYPE_NAME)]
	[PortSettingsType(typeof(TcpPortSettings))]
	public class TcpPortFactory : ObservableObject, IPortFactory
	{
		#region -- Public data --

		public const string PORT_TYPE_NAME = "TCP/IP";

		#endregion

		#region -- Public Methods & Properties --

		public TcpPortFactory()
		{
			_settings = new TcpPortSettings();
		}


		/// <inheritdoc cref="IPortFactory.CreatePort" />
		public IZaberPort CreatePort() => new TcpPort(_settings.HostName, _settings.PortNumber);


		/// <inheritdoc cref="IPortFactory.CreateRawPort" />
		public IRawPort CreateRawPort() => new RawTcpPort(_settings.HostName, _settings.PortNumber);


		/// <inheritdoc cref="IPortFactory.Settings" />
		public PortSettings Settings
		{
			get => _settings;
			set
			{
				if (value is null)
				{
					throw new ArgumentNullException($"{nameof(Settings)} cannot be null");
				}

				if (value is TcpPortSettings tcpSettings)
				{
					_settings = tcpSettings;
					OnPropertyChanged(nameof(HostName));
					OnPropertyChanged(nameof(PortNumber));
				}
				else
				{
					throw new ArgumentException(
						$"Wrong settings type supplied: {value.GetType().Name} should be {typeof(TcpPortSettings).Name}.");
				}
			}
		}

		/// <summary>
		///     Hostname or IP address to connect to.
		/// </summary>
		public string HostName
		{
			get => _settings.HostName;
			set
			{
				if (_settings.HostName != value)
				{
					_settings.HostName = value;
					OnPropertyChanged(nameof(HostName));
				}
			}
		}


		/// <summary>
		///     Port number to connect to.
		/// </summary>
		public int PortNumber
		{
			get => _settings.PortNumber;
			set
			{
				if (_settings.PortNumber != value)
				{
					_settings.PortNumber = value;
					OnPropertyChanged(nameof(PortNumber));
				}
			}
		}

		#endregion

		#region -- Data --

		private TcpPortSettings _settings;

		#endregion
	}
}

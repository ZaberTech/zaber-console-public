﻿namespace ZaberConsole.Ports
{
	/// <summary>
	///     Connection settings for TCP/IP ports.
	/// </summary>
	[PortFactoryType(typeof(TcpPortFactory))]
	public class TcpPortSettings : PortSettings
	{
		#region -- Public data --

		public const string DEFAULT_HOST_NAME = "localhost";
		public const int DEFAULT_PORT_NUMBER = 0x5A41; // "ZC"

		#endregion

		#region -- Public Methods & Properties --

		/// <inheritdoc cref="PortSettings.Clone" />
		public override PortSettings Clone() => MemberwiseClone() as PortSettings;


		/// <inheritdoc cref="PortSettings.DataEquals" />
		public override bool DataEquals(object obj)
		{
			if (base.DataEquals(obj))
			{
				if (obj is TcpPortSettings settings)
				{
					return (HostName == settings.HostName) && (PortNumber == settings.PortNumber);
				}
			}

			return false;
		}


		/// <summary>
		///     The hostname or IP address to connect to.
		/// </summary>
		public string HostName { get; set; } = DEFAULT_HOST_NAME;


		/// <summary>
		///     The port number to connect to at the remote end.
		/// </summary>
		public int PortNumber { get; set; } = DEFAULT_PORT_NUMBER;

		#endregion
	}
}

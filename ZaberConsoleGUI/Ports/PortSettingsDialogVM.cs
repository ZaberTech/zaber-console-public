﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmDialogs.ViewModels;
using Zaber.Ports;
using ZaberConsole.Properties;
using ZaberWpfToolbox;

namespace ZaberConsole.Ports
{
	/// <summary>
	///     ViewModel for the popup dialog for editing port connections.
	/// </summary>
	public class PortSettingsDialogVM : ObservableObject, IUserDialogViewModel
	{
		#region -- Events --

		public event EventHandler DialogClosing;

		public event EventHandler BringToFront;

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Initialize the dialog with a list of available port factory types.
		/// </summary>
		/// <param name="aFactoryTypeList">
		///     List of port types the user can select
		///     when creating a new connection.
		/// </param>
		public PortSettingsDialogVM(IEnumerable<Type> aFactoryTypeList)
		{
			_dispatcher = DispatcherHelper.Dispatcher;

			foreach (var type in aFactoryTypeList)
			{
				var attr = type.GetFirstAttributeOfType<DisplayNameAttribute>();
				if (null != attr)
				{
					_factoryTypesByDisplayName[attr.DisplayName] = type;
					_factoryDisplayNames[type] = attr.DisplayName;
				}
			}
		}


		public void RequestClose() => DialogClosing?.Invoke(this, EventArgs.Empty);

		public void RequestBringToFront() => BringToFront?.Invoke(this, EventArgs.Empty);


		/// <summary>
		///     Return value for the creator of the dialog - true if the Save button
		///     was pressed, false if the dialog was closed any other way.
		/// </summary>
		public bool SaveChanges { get; private set; }


		/// <summary>
		///     List of previously created connections. This should be a deep-copied list
		///     rather than a direct reference to the saved user settings, so that the content
		///     can be reverted when the dialog is cancelled.
		/// </summary>
		public ObservableCollection<PortSettings> PortList
		{
			get => _portList;
			set => Set(ref _portList, value, nameof(PortList));
		}


		/// <summary>
		///     Currently selected connection - must be an instance present in <see cref="PortList" />.
		/// </summary>
		public PortSettings SelectedPort
		{
			get => _selectedPort;
			set
			{
				Set(ref _selectedPort, value, nameof(SelectedPort));
				IsDropDownOpen = false;
				IsNewConnection = false;
				ConnectionTestResult = null;
				UpdateFactoryType();
			}
		}


		public IPortFactory SettingsEditor
		{
			get => _settingsEditor;
			set => Set(ref _settingsEditor, value, nameof(SettingsEditor));
		}


		public bool IsDropDownOpen
		{
			get => _isDropDownOpen;
			set => Set(ref _isDropDownOpen, value, nameof(IsDropDownOpen));
		}


		public bool IsNewConnection
		{
			get => _isNewConnection;
			private set => Set(ref _isNewConnection, value, nameof(IsNewConnection));
		}


		public IEnumerable<Type> FactoryTypes => _factoryDisplayNames.Keys.OrderBy(t => t.Name);


		public Type SelectedFactoryType
		{
			get => _selectedFactoryType;
			set
			{
				Set(ref _selectedFactoryType, value, nameof(SelectedFactoryType));
				UpdateEditControls();
			}
		}


		public bool IsTestInProgress
		{
			get => _isTestInProgress;
			private set => Set(ref _isTestInProgress, value, nameof(IsTestInProgress));
		}


		public bool? ConnectionTestResult
		{
			get => _connectionTestResult;
			private set => Set(ref _connectionTestResult, value, nameof(ConnectionTestResult));
		}


		public string ConnectionTestMessage
		{
			get => _connectionTestMessage;
			private set => Set(ref _connectionTestMessage, value, nameof(ConnectionTestMessage));
		}


		public ICommand SaveButtonCommand
			=> OnDemandRelayCommand(ref _saveCommand,
									_ =>
									{
										IsDropDownOpen = false;
										SaveChanges = true;
										RequestClose();
									});


		public ICommand CancelButtonCommand
			=> OnDemandRelayCommand(ref _cancelCommand,
									_ =>
									{
										IsDropDownOpen = false;
										SaveChanges = false;
										RequestClose();
									});


		public ICommand AddButtonCommand
			=> OnDemandRelayCommand(ref _addButtonCommand,
									_ =>
									{
										IsDropDownOpen = false;
										var factoryType =
											SelectedFactoryType
										?? _factoryDisplayNames.Keys.First();
										SelectedPort = AddNewSettings(factoryType);
										IsNewConnection = true;
									});


		public ICommand DeleteButtonCommand
			=> OnDemandRelayCommand(ref _deleteButtonCommand,
									_ =>
									{
										IsDropDownOpen = false;
										if (null != SelectedPort)
										{
											var selectionIndex =
												PortList.IndexOf(SelectedPort);
											PortList.Remove(SelectedPort);
											selectionIndex =
												Math.Min(selectionIndex,
														 PortList.Count - 1);
											SelectedPort = selectionIndex >= 0
												? PortList[selectionIndex]
												: null;
										}
									});


		public ICommand TestButtonCommand
			=> OnDemandRelayCommand(ref _testButtonCommand,
									_ =>
									{
										ConnectionTestResult = null;
										IsTestInProgress = true;
										ConnectionTestMessage = "Trying to connect...";
										Task.Factory.StartNew(() => TestConnection(SettingsEditor));
									});

		public Exception Exception { get; set; }

		public bool IsModal => true;

		#endregion

		#region -- Non-Public Methods --

		private void UpdateFactoryType()
		{
			var attr = SelectedPort?.GetType().GetFirstAttributeOfType<PortFactoryTypeAttribute>();
			SelectedFactoryType = attr?.FactoryType;
		}


		private void UpdateEditControls()
		{
			if (SelectedFactoryType is null)
			{
				SettingsEditor = null;
			}
			else
			{
				SettingsEditor = Activator.CreateInstance(SelectedFactoryType) as IPortFactory;
				var typeAttr = SelectedFactoryType.GetFirstAttributeOfType<PortSettingsTypeAttribute>();
				if ((null != SelectedPort) && typeAttr.SettingsType.IsAssignableFrom(SelectedPort.GetType()))
				{
					SettingsEditor.Settings = SelectedPort;
				}
				else
				{
					// User has changed the factory type after creating a new connection.
					var _previousPort = SelectedPort;
					SelectedPort = AddNewSettings(SelectedFactoryType);
					PortList.Remove(_previousPort);
					IsNewConnection = true;
				}
			}
		}


		private PortSettings AddNewSettings(Type aFactoryType)
		{
			var typeAttr = aFactoryType.GetFirstAttributeOfType<PortSettingsTypeAttribute>();
			var newSettings = Activator.CreateInstance(typeAttr.SettingsType) as PortSettings;
			PortList.Add(newSettings);
			return newSettings;
		}


		private void TestConnection(IPortFactory aFactory)
		{
			var result = true;
			string message = null;
			IRawPort port = null;

			try
			{
				port = aFactory?.CreateRawPort();
				var results = DeviceDetector.DetectProtocolAndBaudRate(port);
				switch (results.Item1)
				{
					case Protocol.ASCII:
						message = Resources.STR_CONNECTION_TEST_ASCII;
						break;
					case Protocol.Binary:
						message = Resources.STR_CONNECTION_TEST_BINARY;
						break;
					default:
						message = Resources.STR_CONNECTION_TEST_NO_DEVICES;
						break;
				}
			}
			catch (Exception aException)
			{
				result = false;
				message = string.Format(Resources.STR_CONNECTION_TEST_ERROR, aException.Message);
			}
			finally
			{
				port?.Close();
				if (port is IDisposable disposable)
				{
					disposable.Dispose();
				}
			}

			_dispatcher.BeginInvoke(() =>
			{
				ConnectionTestResult = result;
				ConnectionTestMessage = message;
				IsTestInProgress = false;
			});
		}

		#endregion

		#region -- Data --

		private readonly IDictionary<string, Type> _factoryTypesByDisplayName = new Dictionary<string, Type>();
		private readonly IDictionary<Type, string> _factoryDisplayNames = new Dictionary<Type, string>();

		private readonly IDispatcher _dispatcher;

		private ObservableCollection<PortSettings> _portList;
		private PortSettings _selectedPort;
		private RelayCommand _saveCommand;
		private RelayCommand _cancelCommand;
		private RelayCommand _addButtonCommand;
		private RelayCommand _deleteButtonCommand;
		private RelayCommand _testButtonCommand;
		private bool _isDropDownOpen;
		private bool _isNewConnection;
		private bool _isTestInProgress;
		private bool? _connectionTestResult;
		private string _connectionTestMessage;
		private Type _selectedFactoryType;
		private IPortFactory _settingsEditor;

		#endregion
	}
}

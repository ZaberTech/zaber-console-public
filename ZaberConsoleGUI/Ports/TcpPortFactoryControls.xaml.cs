﻿namespace ZaberConsole.Ports
{
	/// <summary>
	///     Interaction logic for TcpPortFactoryControls.xaml
	/// </summary>
	public partial class TcpPortFactoryControls
	{
		#region -- Public Methods & Properties --

		public TcpPortFactoryControls()
		{
			InitializeComponent();
		}

		#endregion
	}
}

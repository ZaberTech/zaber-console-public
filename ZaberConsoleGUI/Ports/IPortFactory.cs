﻿using Zaber;
using Zaber.Ports;

namespace ZaberConsole.Ports
{
	/// <summary>
	///     Interface for VM classes that know how to instantiate port types.
	/// </summary>
	public interface IPortFactory
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Instantiate a port of the supported type and initialize it with
		///     the current settings.
		/// </summary>
		/// <returns>
		///     An implementation of <see cref="IZaberPort" /> configured to
		///     connect to the currently selected destination.
		/// </returns>
		IZaberPort CreatePort();


		/// <summary>
		///     Instatiate a low-level version of the port that the device detector
		///     can use to detect protocol and baud rate. The caller is responsible for
		///     disposing of the returned object if it implements IDisposable.
		/// </summary>
		/// <returns>
		///     An implementation of <see cref="IRawPort" /> configured to
		///     connect to the currently selected destination.
		/// </returns>
		IRawPort CreateRawPort();


		/// <summary>
		///     Current settings for the factory to use. This should be a concrete
		///     subclass of <see cref="PortSettings" />. Setting this will update
		///     the controls and should be done when loading saved user settings.
		///     Getting this will return an instance that reflects the current
		///     state of the controls.
		/// </summary>
		PortSettings Settings { get; set; }

		#endregion
	}
}

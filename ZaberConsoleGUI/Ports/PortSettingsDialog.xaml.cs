﻿using System.Windows;

namespace ZaberConsole.Ports
{
	/// <summary>
	///     Interaction logic for PortSettingsDialog.xaml
	/// </summary>
	public partial class PortSettingsDialog : Window
	{
		#region -- Public Methods & Properties --

		public PortSettingsDialog()
		{
			InitializeComponent();
		}

		#endregion
	}
}

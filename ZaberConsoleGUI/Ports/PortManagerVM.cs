using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Zaber.Ports;
using ZaberConsole.Properties;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Dialogs;

namespace ZaberConsole.Ports
{
	/// <summary>
	///     Manages saved user connections, displays connection edit dialogs
	///     and informs the application what kind of port to open.
	/// </summary>
	public class PortManagerVM : ObservableObject, IDialogClient
	{
		#region -- Public data --

		/// <summary>
		///     Type definition for handlers of settings changed events.
		/// </summary>
		public delegate void PortSettingsChangedHandler();

		#endregion

		#region -- Events --

		/// <summary>
		///     Event fired when the selected port changes, its settings change, or
		///     the type of port changes.
		/// </summary>
		public event PortSettingsChangedHandler SelectedPortSettingsChanged;

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Initialize the port manager with the recently used port list that it will
		///     work from. Note the manager will update this list in place, so passing
		///     in a referece to the instance in the application settings will update them
		///     automatically.
		/// </summary>
		/// <param name="aSavedConnections">List of recently used port connections.</param>
		/// <param name="aFactoryTypeList">List of available implementations of <see cref="IPortFactory" />.</param>
		public PortManagerVM(PortSettingsCollection aSavedConnections, IEnumerable<Type> aFactoryTypeList)
		{
			_savedConnections = aSavedConnections;
			_factoryTypes = aFactoryTypeList.ToList();
			UpdateDisplayedConnections();
			UpdatePortFactory();
		}


		/// <summary>
		///     Make the last selection index in the saved settings match the currently selected port.
		/// </summary>
		public void SaveCurrentSettingsToRecentList()
		{
			if (SelectedConnection is null)
			{
				_savedConnections.LastSelectedIndex = -1;
			}
			else
			{
				if (!_savedConnections.RecentConnections.Contains(SelectedConnection))
				{
					_savedConnections.RecentConnections.Add(SelectedConnection);
				}

				_savedConnections.LastSelectedIndex = _savedConnections.RecentConnections.IndexOf(SelectedConnection);
			}
		}


		/// <summary>
		///     Display information about the current protocol and baud rate.
		/// </summary>
		/// <param name="aMessage">Connection info to display, or null if the port is closed.</param>
		public void ShowConnectionDetails(string aMessage)
		{
			_connectionDetails = aMessage;
			UpdateSelectedConnectionInfo();
		}


		/// <summary>
		///     Refresh the list of serial ports available on the computer.
		/// </summary>
		public void RefreshSerialPorts() => UpdateDisplayedConnections();


		/// <summary>
		///     Factory for the type of port associated with the currently selected connection, or
		///     null if nothing is selected.
		/// </summary>
		public IPortFactory PortFactory
		{
			get => _portFactory;
			private set => Set(ref _portFactory, value, nameof(PortFactory));
		}


		public bool IsDropDownOpen
		{
			get => _isDropDownOpen;
			set => Set(ref _isDropDownOpen, value, nameof(IsDropDownOpen));
		}


		public ObservableCollection<PortSettings> DisplayedConnections
		{
			get => _displayedConnections;
			private set => Set(ref _displayedConnections, value, nameof(DisplayedConnections));
		}


		public PortSettings SelectedConnection
		{
			get => _selectedConnection;
			set
			{
				Set(ref _selectedConnection, value, nameof(SelectedConnection));
				IsDropDownOpen = false;
				UpdateSelectedConnectionInfo();
				UpdatePortFactory();
			}
		}


		public string SelectedConnectionInfo
		{
			get => _selectedConnectionInfo;
			private set => Set(ref _selectedConnectionInfo, value, nameof(SelectedConnectionInfo));
		}


		public ICommand ManageConnectionsCommand
			=> OnDemandRelayCommand(ref _manageConnectionsCommand,
									_ =>
									{
										var selectedIndex =
											DisplayedConnections.IndexOf(SelectedConnection);
										var dialogConnections =
											new ObservableCollection<PortSettings>(
												DisplayedConnections.Select(ps => ps.Clone()));
										var dialogSelection =
											selectedIndex >= 0
												? dialogConnections[selectedIndex]
												: null;
										var dialog =
											new PortSettingsDialogVM(_factoryTypes)
											{
												PortList = dialogConnections,
												SelectedPort = dialogSelection
											};

										RequestDialogOpen?.Invoke(this, dialog);

										if (dialog.SaveChanges)
										{
											_savedConnections.RecentConnections =
												dialog.PortList.ToList();
											_savedConnections.LastSelectedIndex =
												_savedConnections
												.RecentConnections.IndexOf(dialog.SelectedPort);
											SelectedConnection =
												dialog.SelectedPort;
											UpdateDisplayedConnections();
										}
									});

		#endregion

		#region-- IDialogClient implementation --

		public event RequestDialogChange RequestDialogOpen;
		#pragma warning disable CS0067 // Event never used
		public event RequestDialogChange RequestDialogClose;
		#pragma warning restore CS0067

		#endregion


		private void UpdateDisplayedConnections()
		{
			var localSerialPorts = new HashSet<string>(RS232Port.GetSerialPortNames());

			// If the last selected port was a serial port that no longer exists, remove it from
			// the saved settings.
			var lastSelectedSettings = SelectedConnection ?? _savedConnections.GetLastSelectedSettings();
			if (lastSelectedSettings is SerialPortSettings lastSelectedSerialPort)
			{
				if (!localSerialPorts.Contains(lastSelectedSerialPort.SerialPortName))
				{
					_savedConnections.LastSelectedIndex = -1;
					_savedConnections.RecentConnections.Remove(lastSelectedSerialPort);
					lastSelectedSettings = null;
				}
			}

			// Add all remaining saved connections, avoid duplicating the ones that
			// refer to existing serial ports, and strip out ones that refer to missing serial ports.
			var newList = new List<PortSettings>();
			foreach (var savedConnection in _savedConnections.RecentConnections)
			{
				if (savedConnection is SerialPortSettings serialSettings)
				{
					if (localSerialPorts.Contains(serialSettings.SerialPortName))
					{
						newList.Add(savedConnection);
						localSerialPorts.Remove(serialSettings.SerialPortName);
					}
				}
				else
				{
					newList.Add(savedConnection);
				}
			}

			// Add auto-generated connections for any existing serial ports not mentioned in the saved settings.
			foreach (var portName in localSerialPorts)
			{
				newList.Add(new SerialPortSettings
				{
					Name = $"Serial port {portName}",
					SerialPortName = portName
				});
			}

			DisplayedConnections = new ObservableCollection<PortSettings>(newList.OrderBy(s => s.Name));
			SelectedConnection = lastSelectedSettings;

			// Make the saved settings mirror the updated list. We allow generated port settings
			// into the user settings so that saving the identity of the last selected port will
			// work when it is a generated one.
			_savedConnections.RecentConnections = DisplayedConnections.ToList();
			_savedConnections.LastSelectedIndex = _savedConnections.RecentConnections.IndexOf(lastSelectedSettings);
		}


		private void UpdatePortFactory()
		{
			var changed = false;

			if (SelectedConnection is null)
			{
				changed = null != PortFactory;
				PortFactory = null;
			}
			else
			{
				var factoryTypeAttr = SelectedConnection.GetType().GetFirstAttributeOfType<PortFactoryTypeAttribute>();
				if (factoryTypeAttr is null)
				{
					throw new InvalidProgramException(Resources.STR_PORT_FACTORY_MISSING);
				}

				if (PortFactory is null || (PortFactory.GetType() != factoryTypeAttr.FactoryType))
				{
					changed = true;
					PortFactory = Activator.CreateInstance(factoryTypeAttr.FactoryType) as IPortFactory;
					if (PortFactory is null)
					{
						throw new InvalidProgramException(Resources.STR_PORT_FACTORY_WRONG_TYPE);
					}
				}
				else
				{
					changed = !PortFactory.Settings.DataEquals(SelectedConnection);
				}

				PortFactory.Settings = SelectedConnection;
			}

			if (changed)
			{
				SelectedPortSettingsChanged?.Invoke();
			}
		}


		private void UpdateSelectedConnectionInfo()
		{
			if (SelectedConnection is null)
			{
				SelectedConnectionInfo = null;
			}
			else if (_connectionDetails is null)
			{
				SelectedConnectionInfo = SelectedConnection?.Name;
			}
			else
			{
				SelectedConnectionInfo = SelectedConnection.Name + " " + _connectionDetails;
			}
		}


		private readonly PortSettingsCollection _savedConnections;
		private readonly IEnumerable<Type> _factoryTypes;

		private ObservableCollection<PortSettings> _displayedConnections;
		private PortSettings _selectedConnection;
		private string _selectedConnectionInfo;
		private string _connectionDetails;
		private bool _isDropDownOpen;
		private RelayCommand _manageConnectionsCommand;

		private IPortFactory _portFactory;
	}
}

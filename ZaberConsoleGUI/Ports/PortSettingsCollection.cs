﻿using System.Collections.Generic;

namespace ZaberConsole.Ports
{
	/// <summary>
	///     Collection of settings for all recent connections saved in user settings.
	/// </summary>
	public class PortSettingsCollection
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Gets the last selected port settings.
		/// </summary>
		/// <returns>Last selected settings, or null if no valid selection was saved.</returns>
		public PortSettings GetLastSelectedSettings()
		{
			if ((LastSelectedIndex >= 0) && (LastSelectedIndex < RecentConnections.Count))
			{
				return RecentConnections[LastSelectedIndex];
			}

			return null;
		}


		/// <summary>
		///     All saved connection settings.
		/// </summary>
		public List<PortSettings> RecentConnections { get; set; } = new List<PortSettings>();


		/// <summary>
		///     Index of the last selected settings within <see cref="RecentConnections" />.
		/// </summary>
		public int LastSelectedIndex { get; set; } = -1;

		#endregion
	}
}

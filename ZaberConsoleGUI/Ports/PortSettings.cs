﻿using System.Xml.Serialization;

namespace ZaberConsole.Ports
{
	/// <summary>
	///     Base class saved user settings for port connections.
	/// </summary>
	[XmlInclude(typeof(SerialPortSettings))] // TODO (Soleil 2019/07): This is backwards. Ticket #5858.
	[XmlInclude(typeof(TcpPortSettings))]
	public abstract class PortSettings
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Create a deep copy of this object.
		/// </summary>
		/// <returns>A copy of the object that has the same value but with no shared references.</returns>
		public abstract PortSettings Clone();


		/// <summary>
		///     Compare settings data for equality.
		/// </summary>
		/// <param name="obj">Object to compare to</param>
		/// <returns>True if both objects are the same type and all properties have the same value.</returns>
		public virtual bool DataEquals(object obj)
		{
			if (obj is PortSettings settings)
			{
				return Name == settings.Name;
			}

			return false;
		}


		/// <summary>
		///     User-assigned name for the connection.
		/// </summary>
		public string Name { get; set; }

		#endregion
	}
}

﻿namespace ZaberConsole.Ports
{
	/// <summary>
	///     Connection settings for serial ports.
	/// </summary>
	[PortFactoryType(typeof(SerialPortFactory))]
	public class SerialPortSettings : PortSettings
	{
		#region -- Public Methods & Properties --

		/// <inheritdoc cref="PortSettings.Clone" />
		public override PortSettings Clone() => MemberwiseClone() as PortSettings;


		/// <inheritdoc cref="PortSettings.DataEquals" />
		public override bool DataEquals(object obj)
		{
			if (base.DataEquals(obj))
			{
				if (obj is SerialPortSettings settings)
				{
					return SerialPortName == settings.SerialPortName;
				}
			}

			return false;
		}


		/// <summary>
		///     The operating system's name for the serial port to use.
		/// </summary>
		public string SerialPortName { get; set; }

		#endregion
	}
}

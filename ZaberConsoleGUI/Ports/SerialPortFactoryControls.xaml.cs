﻿namespace ZaberConsole.Ports
{
	/// <summary>
	///     Interaction logic for SerialPortFactoryControls.xaml
	/// </summary>
	public partial class SerialPortFactoryControls
	{
		#region -- Public Methods & Properties --

		public SerialPortFactoryControls()
		{
			InitializeComponent();
		}

		#endregion
	}
}

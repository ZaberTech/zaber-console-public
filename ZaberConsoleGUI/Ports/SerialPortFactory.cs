﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO.Ports;
using System.Linq;
using Zaber;
using Zaber.Ports;
using ZaberWpfToolbox;

namespace ZaberConsole.Ports
{
	[DisplayName(PORT_TYPE_NAME)]
	[PortSettingsType(typeof(SerialPortSettings))]
	public class SerialPortFactory : ObservableObject, IPortFactory
	{
		#region -- Public data --

		public const string PORT_TYPE_NAME = "Serial";

		#endregion

		#region -- Initialization --

		public SerialPortFactory()
		{
			_settings = new SerialPortSettings();
			RefreshPortList();
		}


		public void RefreshPortList()
		{
			var ports = SerialPort.GetPortNames().ToList();
			ports.Sort();
			AvailablePorts = new ObservableCollection<string>(ports);
			if (!ports.Contains(PortDisplayName))
			{
				PortDisplayName = ports.FirstOrDefault();
			}
		}

		#endregion

		#region -- IPortFactory interface --

		/// <inheritdoc cref="IPortFactory.Settings" />
		public PortSettings Settings
		{
			get => _settings;
			set
			{
				if (value is null)
				{
					throw new ArgumentNullException($"{nameof(Settings)} cannot be null");
				}

				if (value is SerialPortSettings serialSettings)
				{
					_settings = serialSettings;
					OnPropertyChanged(nameof(PortDisplayName));
				}
				else
				{
					throw new ArgumentException(
						$"Wrong settings type supplied: {value.GetType().Name} should be {typeof(SerialPortSettings).Name}.");
				}
			}
		}


		/// <inheritdoc cref="IPortFactory.CreatePort" />
		public IZaberPort CreatePort()
			=> new TSeriesPort(new SerialPort(_settings.SerialPortName), new PacketConverter());


		/// <inheritdoc cref="IPortFactory.CreateRawPort" />
		public IRawPort CreateRawPort() => new RawRS232Port(new SerialPort(_settings.SerialPortName));

		#endregion

		#region -- View data --

		/// <summary>
		///     Currently selected port name.
		/// </summary>
		public string PortDisplayName
		{
			get => _settings.SerialPortName;
			set
			{
				if (AvailablePorts.Contains(value))
				{
					_settings.SerialPortName = value;
				}

				OnPropertyChanged(nameof(PortDisplayName));
			}
		}


		/// <summary>
		///     List of available serial ports.
		/// </summary>
		public ObservableCollection<string> AvailablePorts
		{
			get => _ports;
			set => Set(ref _ports, value, nameof(AvailablePorts));
		}

		#endregion

		#region -- Fields --

		private ObservableCollection<string> _ports = new ObservableCollection<string>();
		private SerialPortSettings _settings;

		#endregion
	}
}

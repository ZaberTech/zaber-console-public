﻿using System;
using System.IO;

namespace ZaberConsole
{
	/// <summary>
	///     A StringWriter that raises an event every time it is written to
	///     instead of recording the text in a buffer.
	/// </summary>
	/// <remarks>
	///     The internal buffer should always be empty, so
	///     the ToString() method will return an empty string.
	/// </remarks>
	internal class TextWriterWatcher : StringWriter
	{
		#region -- Events --

		/// <summary>
		///     Raised whenever something is written.
		/// </summary>
		/// <remarks>
		///     The actual text that was written is contained in the event args.
		/// </remarks>
		public event EventHandler<TextEventArgs> Written;

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Raises the <see cref="Written" /> event with a single character.
		/// </summary>
		/// <param name="value">The character to write.</param>
		public override void Write(char value) => OnWritten(value.ToString());


		/// <summary>
		///     Raises the <see cref="Written" /> event with a string.
		/// </summary>
		/// <param name="value">The string to write.</param>
		public override void Write(string value) => OnWritten(value);


		/// <summary>
		///     Raises the <see cref="Written" /> event with a subarray of
		///     characters.
		/// </summary>
		/// <param name="buffer">The character array to write data from.</param>
		/// <param name="index">Starting index in the buffer.</param>
		/// <param name="count">The number of characters to write.</param>
		/// <exception cref="System.ArgumentException">
		///     The buffer length minus index is less than count.
		/// </exception>
		/// <exception cref="System.ArgumentNullException">
		///     The buffer parameter is null.
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		///     index or count is negative.
		/// </exception>
		public override void Write(char[] buffer, int index, int count) => OnWritten(new string(buffer, index, count));

		#endregion

		#region -- Non-Public Methods --

		private void OnWritten(string text)
		{
			if (Written != null)
			{
				Written(this, new TextEventArgs(text));
			}
		}

		#endregion
	}
}

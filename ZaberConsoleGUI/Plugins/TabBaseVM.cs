﻿using ZaberWpfToolbox;

namespace ZaberConsole.Plugins
{
	/// <summary>
	///     Convenience base class to use for VMs placed in a collection of tabs.
	/// </summary>
	public class TabBaseVM : ObservableObject
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Get or set the name of the tab control associated with this VM instance.
		/// </summary>
		public string TabName
		{
			get => _tabName;
			set => Set(ref _tabName, value, nameof(TabName));
		}

		#endregion

		#region -- Data --

		private string _tabName = "Default Tab Name";

		#endregion
	}
}

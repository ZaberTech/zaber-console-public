﻿using System;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins
{
	public class PluginHostTabVM : PluginTabVM
	{
		#region -- Public Methods & Properties --

		public PluginHostTabVM(ObservableObject aControlVM)
		{
			if (null == aControlVM)
			{
				throw new ArgumentException("Plugin VM is null.");
			}

			ControlVM = aControlVM;
		}


		public ObservableObject ControlVM { get; }

		#endregion
	}
}

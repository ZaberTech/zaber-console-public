﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Reflection;
using System.Windows;
using log4net;
using Zaber;
using Zaber.PlugIns;

namespace ZaberConsole.Plugins
{
	// Helper for the PluginManager to find exports from ZC plugin DLLs.
	// Represents what is known about a DLL, including the main executable itself.
	public class PluginAssemblyInfo
	{
		#region -- Public Methods & Properties --

		public PluginAssemblyInfo(Assembly aAssembly, string aFileName, PlugInManager aManager)
		{
			Assembly = aAssembly;
			AssemblyFilePath = aFileName;

			// Find plugin types.
			var pluginTypes = new List<PluginTypeInfo>();
			PluginTypes = pluginTypes;
			try
			{
				AssemblyVersion = aAssembly.GetName().Version ?? new Version("0.0.0.0");
				foreach (var type in aAssembly.GetTypes().Where(IsPlugIn))
				{
					pluginTypes.Add(new PluginTypeInfo(this, type, aManager));
				}
			}
			catch (ReflectionTypeLoadException aException)
			{
				foreach (var loaderException in aException.LoaderExceptions)
				{
					_log.Error("Loader exception", loaderException);
				}
			}

			// Find resource dictionaries.
			var ac = new AssemblyCatalog(aAssembly);
			var container = new CompositionContainer(ac);
			container.ComposeParts(this);

			// Check for error conditions.
			if (0 == pluginTypes.Count)
			{
				var requiredVersion = typeof(IZaberPort).Assembly.GetName().Version;

				var isWrongZaberLibraryLoaded = (from referencedAssembly in aAssembly.GetReferencedAssemblies()
												 where referencedAssembly.Name.Equals("Zaber")
												 let referencedVersion = referencedAssembly.Version
												 where !requiredVersion.Equals(referencedVersion)
												 select requiredVersion).Any();

				var isWrongToolboxLibraryLoaded = (from referencedAssembly in aAssembly.GetReferencedAssemblies()
												   where referencedAssembly.Name.Equals("ZaberWpfToolbox")
												   let referencedVersion = referencedAssembly.Version
												   where !requiredVersion.Equals(referencedVersion)
												   select requiredVersion).Any();

				string message = null;
				if (isWrongZaberLibraryLoaded)
				{
					message = "Wrong version of Zaber.dll loaded; make sure it's not anywhere in the plugin folders.";
				}

				if (isWrongToolboxLibraryLoaded)
				{
					message =
						"Wrong version of ZaberWpfToolbox.dll loaded; make sure it's not anywhere in the plugin folders.";
				}

				message = message ?? $"No plug in types found in assembly {AssemblyFilePath}.";

				_log.Warn(message);
			}
		}


		public override string ToString() => AssemblyFilePath;


		// Full path to the DLL.
		public string AssemblyFilePath { get; }

		// The loaded assembly itself.
		public Assembly Assembly { get; }

		// The version number of the assembly. Defaults to 0.0.0.0 if not found.
		public Version AssemblyVersion { get; }


		// XAML Resource Dictionaries exported by the assembly - these should
		// be merged with the top-level application resource dictionary on plugin load
		// and removed on plugin unload.
		[ImportMany(PlugInAttribute.PLUGIN_RESOURCE_DICTIONARY, typeof(ResourceDictionary))]
		public IEnumerable<ResourceDictionary> Resources { get; set; }


		// Plugin types found in the assembly - ie classes with the plugin attribute.
		public IEnumerable<PluginTypeInfo> PluginTypes { get; }

		#endregion

		#region -- Non-Public Methods --

		private static bool IsPlugIn(Type aType)
		{
			var attributes = aType.GetCustomAttributes(typeof(PlugInAttribute), true);
			return attributes.Length > 0;
		}

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		#endregion
	}
}

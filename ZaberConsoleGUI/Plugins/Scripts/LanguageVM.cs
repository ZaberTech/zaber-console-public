﻿using Zaber.PlugIns;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.Scripts
{
	public class LanguageVM : ObservableObject
	{
		#region -- Public Methods & Properties --

		public LanguageVM(ScriptLanguage aLanguge)
		{
			Language = aLanguge;
			Name = Language.Name;
			_checked = false;
		}


		// These properties do not need to be observable since they are not updated.
		public string Name { get; }

		public string ShortcutName { get; set; }

		public ScriptLanguage Language { get; }


		public bool Checked
		{
			get => _checked;
			set => Set(ref _checked, value, nameof(Checked));
		}

		#endregion

		#region -- Data --

		private bool _checked;

		#endregion
	}
}

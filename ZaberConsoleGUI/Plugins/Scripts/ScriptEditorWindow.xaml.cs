﻿using System.Windows;

namespace ZaberConsole.Plugins.Scripts
{
	/// <summary>
	///     Interaction logic for ScriptEditorWindow.xaml
	/// </summary>
	public partial class ScriptEditorWindow : Window
	{
		#region -- Public Methods & Properties --

		public ScriptEditorWindow()
		{
			InitializeComponent();
		}

		#endregion
	}
}

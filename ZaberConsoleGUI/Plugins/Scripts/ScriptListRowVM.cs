﻿using System;
using System.IO;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.Scripts
{
	public class ScriptListRowVM : ObservableObject, IComparable<ScriptListRowVM>
	{
		#region -- Public Methods & Properties --

		#region -- IComparable implementation --

		public int CompareTo(ScriptListRowVM aOther)
		{
			if (IsExample != aOther.IsExample)
			{
				return IsExample ? 1 : -1;
			}

			return string.Compare(Name, aOther.Name, StringComparison.Ordinal);
		}

		#endregion

		#region -- View-bound data --

		public string DisplayName => Name + (IsExample ? " (Example)" : "");

		#endregion

		#endregion

		#region -- Setup and Context --

		public ScriptListRowVM(FileInfo aScriptFile, DirectoryInfo aScriptsFolder, bool isExample)
		{
			ScriptFile = aScriptFile ?? throw new ArgumentNullException(nameof(aScriptFile));
			ScriptsFolder = aScriptsFolder;
			IsExample = isExample;
			Name = FileUtil.GetPathInDirectory(aScriptsFolder, aScriptFile);

			Runner = new ScriptRunner();
		}


		public string Name { get; }

		public FileInfo ScriptFile { get; }

		public DirectoryInfo ScriptsFolder { get; }

		public bool IsExample { get; }

		public ScriptRunner Runner { get; }

		#endregion
	}
}

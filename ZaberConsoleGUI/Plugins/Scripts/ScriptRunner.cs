﻿using System;
using System.Reflection;
using System.Threading;
using log4net;
using Zaber;
using Zaber.PlugIns;

namespace ZaberConsole.Plugins.Scripts
{
	public class ScriptRunner
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Run the plug in and wait for it to complete.
		/// </summary>
		/// <exception cref="ScriptException">
		///     A problem occurred running the
		///     script.
		/// </exception>
		public void Run()
		{
			_workerThread = new Thread(RunWorkerThread)
			{
				IsBackground = true,
				Name = "Script runner thread"
			};

			_workerException = null;
			_workerThread.Start();
			_workerThread.Join();
			_workerThread = null;

			// Eat thread abort exceptions to avoid an annoying popup when the Cancel button is pressed.
			if (null != _workerException)
			{
				if (_workerException is ThreadAbortException)
				{
					_log.Info("Script thread abort exception.", _workerException);
				}
				else
				{
					throw new ScriptException(_workerException.Message, _workerException);
				}
			}
		}


		/// <summary>
		///     Cancel the plugin that's executing on another thread.
		/// </summary>
		/// <exception cref="InvalidOperationException">
		///     The PlugIn was not
		///     running, and so could not be cancelled.
		/// </exception>
		public void Cancel(IZaberPort aPort)
		{
			if (ScriptPlugin != null)
			{
				// We can't check to see if the plugin has already been cancelled, but
				// if the worker thread is still around then it *probably* hasn't - see
				// the logic in Run(). The worst case here should be that the plugin
				// gets cancelled twice; If the worker thread is null then failing to
				// cancel the plugin should be OK because it already exited on its own.
				if (_workerThread != null)
				{
					ScriptPlugin.Cancel();
				}
			}

			var workerThread = _workerThread;
			if ((workerThread != null) && !workerThread.Join(1000))
			{
				_log.Info("User cancelled script execution. Aborting thread.");
				lock (aPort.SendLock)
				{
					workerThread.Abort();
				}
			}
		}


		public IPlugIn ScriptPlugin { get; set; }


		/// <summary>
		///     Gets a flag showing whether the script is currently running.
		/// </summary>
		public bool IsRunning => null != _workerThread;

		#endregion

		#region -- Non-Public Methods --

		private void RunWorkerThread()
		{
			try
			{
				ScriptPlugin.Run();
			}
			catch (Exception ex)
			{
				_workerException = ex;
			}
		}

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private Thread _workerThread;
		private Exception _workerException;

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using log4net;
using Zaber;
using Zaber.PlugIns;
using Zaber.Telemetry;
using Zaber.Threading;
using ZaberConsole.Properties;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Dialogs;
using ZaberWpfToolbox.Logging;

namespace ZaberConsole.Plugins.Scripts
{
	[PlugIn(Name = "Scripts", Description = "Edit and run device control scripts.")]
	public class ScriptListVM : ObservableObject, IDialogClient, ScriptEditorWindowVM.IParent
	{
		#region -- Public Methods & Properties --

		public ScriptListVM()
		{
			var asmPath = Assembly.GetExecutingAssembly().Location;

			var applicationScripts = Path.Combine(Path.GetDirectoryName(asmPath), "Scripts");
			_applicationScriptsDirectory = new DirectoryInfo(applicationScripts);
			_appTemplateFinder = new FileFinder(Path.Combine(applicationScripts, "Templates"));

			_dispatcher = Dispatcher.CurrentDispatcher;
			SetTemplateFinder();
			BuildScriptsGrid();
			OutputText.MaxLength = Settings.Default.LogMaxLength;
		}


		/// <summary>
		///     Invoked by the plugin manager when the window is closing.
		/// </summary>
		[PlugInMethod(PlugInMethodAttribute.EventType.PluginDeactivated)]
		public void OnWindowClosing()
		{
			if (_scriptEditorVM != null)
			{
				_scriptEditorVM.RequestClose();
			}
		}


		[PlugInProperty]
		public Conversation Conversation
		{
			get => _conversation;
			set
			{
				_conversation = value;
				OnPropertyChanged(nameof(CanRunOrCancel));
			}
		}


		[PlugInProperty]
		public ZaberPortFacade PortFacade
		{
			get => _portFacade;
			set
			{
				if (null != _portFacade)
				{
					_portFacade.Opened -= PortFacade_OpenedOrClosed;
					_portFacade.Closed -= PortFacade_OpenedOrClosed;
				}

				_portFacade = value;

				if (null != _portFacade)
				{
					_portFacade.Opened += PortFacade_OpenedOrClosed;
					_portFacade.Closed += PortFacade_OpenedOrClosed;
				}
			}
		}


		/// <summary>
		///     Gets or sets the text writer that output messages can be written to.
		/// </summary>
		/// <remarks>
		///     Example messages are starting a script, ending a script, and output
		///     from a script. The text contains a prefix to show it came from
		///     a script.
		/// </remarks>
		[PlugInProperty]
		public TextWriter Output { get; set; }

		public ObservableCollection<ScriptListRowVM> Rows
		{
			get => _rows;
			set => Set(ref _rows, value, nameof(Rows));
		}


		public ScriptListRowVM SelectedRow
		{
			get => _selectedRow;
			set
			{
				Set(ref _selectedRow, value, nameof(SelectedRow));
				OnPropertyChanged(nameof(CanRunOrCancel));
				OnPropertyChanged(nameof(IsSelectedRowDeletable));
			}
		}

		public bool IsSelectedRowDeletable => (_selectedRow != null) && !_selectedRow.IsExample;


		/// <summary>
		///     Property for the views to use to display the current script folder. This exists
		///     to make it easier to send property change notifications.
		/// </summary>
		public string CurrentScriptFolder => Settings.Default.ScriptsFolder;


		public bool OpenScriptFolderEnabled
		{
			get => _openScriptFolderEnabled;
			set => Set(ref _openScriptFolderEnabled, value, nameof(OpenScriptFolderEnabled));
		}


		public bool IsPortOpen => PortFacade?.IsOpen ?? false;


		/// <summary>
		///     Set to true when a script requests ineractive input during a run.
		/// </summary>
		public bool IsInteractive
		{
			get => _isInteractive;
			set => Set(ref _isInteractive, value, nameof(IsInteractive));
		}


		public LogControlVM OutputText { get; } = new LogControlVM();


		public string InputText
		{
			get => _inputText;
			set => Set(ref _inputText, value, nameof(InputText));
		}


		public bool InputTextHasFocus
		{
			get => _inputTextHasFocus;
			set => Set(ref _inputTextHasFocus, value, nameof(InputTextHasFocus));
		}


		public string RunButtonLabel
		{
			get => _runButtonLabel;
			set => Set(ref _runButtonLabel, value, nameof(RunButtonLabel));
		}


		public bool IsRunning
		{
			get => _isRunning;
			set => Set(ref _isRunning, value, nameof(IsRunning));
		}


		public bool CanRunOrCancel => IsPortOpen
								  && (null != Conversation)
								  && (0 == Conversation.Device.AxisNumber)
								  && (null != SelectedRow);


		/// <summary>
		///     Checkbox value for whether or not scripts in sub-folders are shown in the list.
		/// </summary>
		public bool ShowScriptsInSubFolders
		{
			get => Settings.Default.ScriptList_ShowScriptsInSubFolders;
			set
			{
				Settings.Default.ScriptList_ShowScriptsInSubFolders = value;
				OnPropertyChanged(nameof(ShowScriptsInSubFolders));
				BuildScriptsGrid();
			}
		}


		public bool ShowExamples
		{
			get => Settings.Default.ScriptList_ShowExamples;
			set
			{
				Settings.Default.ScriptList_ShowExamples = value;
				OnPropertyChanged(nameof(ShowExamples));
				BuildScriptsGrid();
			}
		}


		public ICommand InputEnterKeyCommand => new RelayCommand(_ => OnInputEnterKeyPressed());


		public ICommand RunOrCancelCommand => new RelayCommand(_ => RunOrCancelScript(SelectedRow));


		public ICommand EditNewCommand => new RelayCommand(_ => ShowScriptEditor(null));


		public ICommand EditRowCommand => new RelayCommand(_ => ShowScriptEditor(SelectedRow));


		public ICommand DeleteScriptCommand
			=> OnDemandRelayCommand(ref _deleteScriptCommand,
									aRow =>
									{
										var selectedScriptVM = aRow as ScriptListRowVM;
										if (null == selectedScriptVM)
										{
											return;
										}

										DeleteScript(selectedScriptVM);
									});


		public ICommand RefreshScriptsCommand => new RelayCommand(_ => BuildScriptsGrid());


		public ICommand OpenScriptFolderCommand => new RelayCommand(_ => ChooseScriptsFolder());

		#endregion

		#region -- Interfaces --

		void ScriptEditorWindowVM.IParent.NewFile() => BuildScriptsGrid();


		DirectoryInfo ScriptEditorWindowVM.IParent.GetUserScriptDirectory()
		{
			if (!CheckScriptsFolder())
			{
				return null;
			}

			return new DirectoryInfo(Settings.Default.ScriptsFolder);
		}


		IFileFinder ScriptEditorWindowVM.IParent.GetTemplateFinder() => _templateFinder;

		#endregion

		#region -- Non-Public Methods --

		private void PortFacade_OpenedOrClosed(object sender, EventArgs e) => _dispatcher.BeginInvoke(new Action(() =>
		{
			OnPropertyChanged(nameof(IsPortOpen));
			OnPropertyChanged(nameof(CanRunOrCancel));
		}));


		private void OnEditScript(object aSender, EventArgs aArgs)
		{
			var view = (ScriptListRowVM) aSender;
			ShowScriptEditor(view);
		}


		private void RunOrCancelScript(ScriptListRowVM aScript)
		{
			_dispatcher.Invoke(new Action(() => { IsInteractive = false; }));

			var view = aScript;
			if (null != view.Runner.ScriptPlugin)
			{
				view.Runner.Cancel(PortFacade.Port);
				_eventLog.LogEvent("Script cancelled");
			}
			else
			{
				_dispatcher.Invoke(new Action(() => { OutputText.Text = string.Empty; }));

				if (ZaberPortFacadeState.Open != PortFacade.CurrentState)
				{
					var mbvm = new MessageBoxParams
					{
						Message = "The port is currently closed. Most scripts require the port to be "
							  + "open in order to work properly. Do you still want to run this script?",
						Caption = "Warning",
						Buttons = MessageBoxButton.YesNo,
						Icon = MessageBoxImage.Warning
					};

					RequestDialogOpen?.Invoke(this, mbvm);

					if (MessageBoxResult.Yes != mbvm.Result)
					{
						return;
					}
				}

				if ((null == Conversation) || (0 == Conversation.Device.AxisNumber))
				{
					// Not an axis conversation.

					try
					{
						var script = new Script(view.ScriptFile, _templateFinder);
						var plugIn = script.Build();

						plugIn.PortFacade = PortFacade;
						plugIn.Conversation = Conversation;
						_scriptInputStream = new BlockingStream(new MemoryStream());
						_scriptInputStream.Blocked += ScriptInputStream_Blocked;
						_scriptInputReader = new StreamReader(_scriptInputStream);
						plugIn.Input = _scriptInputReader;
						_scriptInputWriter = new StreamWriter(_scriptInputStream) { AutoFlush = true };

						var watcher = new TextWriterWatcher();
						watcher.Written += OnScriptOutputWritten;
						plugIn.Output = watcher;
						view.Runner.ScriptPlugin = plugIn;
						var scriptWorker = BackgroundWorkerManager.CreateWorker();
						scriptWorker.DoWork += BackgroundWorker_DoWork;
						scriptWorker.RunWorkerCompleted += BackgroundWorker_RunWorkerCompleted;
						OutputWrite("started " + view.Name);
						_dispatcher.BeginInvoke(new Action(() =>
						{
							RunButtonLabel = "Cancel";
							IsRunning = true;
						}));
						scriptWorker.Run(view);

						_eventLog.LogEvent("Run script", view.ScriptFile.Name);
					}
					catch (Exception ex)
					{
						if ((null != view) && (null != view.Runner.ScriptPlugin))
						{
							view.Runner.ScriptPlugin.Dispose();
							view.Runner.ScriptPlugin = null;
						}

						DisplayError("Problem executing script", ex);
					}
				}
				else
				{
					// An axis is selected.
					var mbvm = new MessageBoxParams
					{
						Message = "Cannot run script on an axis. Please select a device.",
						Caption = "Execute script",
						Buttons = MessageBoxButton.OK,
						Icon = MessageBoxImage.Error
					};

					RequestDialogOpen?.Invoke(this, mbvm);
				}
			}
		}


		/// <summary>
		///     Deletes a script file, first confirming that it isn't open in the editor and
		///     that the user really wants to delete the file. Also handles reporting file system
		///     errors deleting the file. Triggers a refresh of the script list.
		/// </summary>
		/// <param name="aScript">Info about the script to delete.</param>
		private void DeleteScript(ScriptListRowVM aScript)
		{
			if (null == aScript)
			{
				return;
			}

			MessageBoxParams mbvm = null;
			_log.InfoFormat("Answering request to delete script " + aScript.ScriptFile.FullName);

			if (aScript.ScriptFile.FullName == _scriptEditorVM?.CurrentScriptFile?.FullName)
			{
				_log.Info("Couldn't delete script because it is open in the editor.");
				mbvm = MessageBoxParams.CreateErrorNotification("Can't delete script",
																"The selected script is currently open in the script editor. Please close the file before deleting it.");
				RequestDialogOpen?.Invoke(this, mbvm);
				return;
			}

			mbvm = MessageBoxParams.CreateYesNoPrompt("Confirm delete",
													  $"Are you sure you want to delete the script {aScript.ScriptFile.FullName}?");
			RequestDialogOpen?.Invoke(this, mbvm);
			if (MessageBoxResult.Yes != mbvm.Result)
			{
				_log.Info("User cancelled script delete when prompted to confirm.");
				return;
			}

			try
			{
				aScript.ScriptFile.Delete();
				_log.Info("Script deleted successfully.");
			}
			catch (IOException aException)
			{
				_log.Info("There was an error deleting the script file.", aException);
				mbvm = MessageBoxParams.CreateErrorNotification("Error deleting file",
																"There was an error deleting the file: "
															+ aException.Message);
				RequestDialogOpen?.Invoke(this, mbvm);
			}

			BuildScriptsGrid();
		}


		private void ScriptInputStream_Blocked(object aSender, EventArgs aArgs) => _dispatcher.BeginInvoke(
			new Action(() =>
			{
				IsInteractive = true;
				_dispatcher.BeginInvoke(new Action(() => { InputTextHasFocus = true; }));
			}));


		private void OnInputEnterKeyPressed()
		{
			IsInteractive = false;
			Output.WriteLine(">" + InputText + "\r\n");
			OutputText.AppendLine(">" + InputText);
			_scriptInputWriter.WriteLine(InputText);
			InputText = string.Empty;
		}


		private void OnScriptOutputWritten(object aSender, TextEventArgs aArgs) => OutputWrite(aArgs.Text, false);


		private void OutputWrite(string text, bool endLine = true)
		{
			Output.WriteLine("script> " + text.Replace(Environment.NewLine, "\\n"));

			if (endLine)
			{
				OutputText.AppendLine(text);
			}
			else
			{
				OutputText.Append(text);
			}
		}


		private void ScriptEditorForm_FormClosed(object aSender, EventArgs aArgs)
		{
			_scriptEditorVM = null;
			OpenScriptFolderEnabled = true;
		}


		private void SetTemplateFinder()
		{
			var templateFinders = new List<IFileFinder>();

			var localTemplates = new DirectoryInfo(Path.Combine(Settings.Default.ScriptsFolder, "Templates"));
			if (localTemplates.Exists)
			{
				templateFinders.Add(new FileFinder(localTemplates));
			}

			templateFinders.Add(_appTemplateFinder);

			_templateFinder = new JointFileFinder(templateFinders);
		}


		/// <summary>
		///     Populate the scriptViewBindingSource with scripts.
		/// </summary>
		private bool BuildScriptsGrid()
		{
			var result = true;
			var settings = Settings.Default;
			Rows.Clear();

			var scripts = new List<ScriptListRowVM>();
			foreach (var view in Rows)
			{
				if (null != view.Runner.ScriptPlugin)
				{
					// keep running scripts around so they can be cancelled.
					var index = scripts.BinarySearch(view);
					if (index < 0)
					{
						scripts.Insert(~index, view);
					}
				}
			}

			var newRows = new ObservableCollection<ScriptListRowVM>();

			string errorMsg = null;

			try
			{
				if (Directory.Exists(settings.ScriptsFolder))
				{
					var scriptsFolder = new DirectoryInfo(settings.ScriptsFolder);
					BuildScriptsList(scriptsFolder, scriptsFolder, scripts, false);
				}

				if (ShowExamples)
				{
					BuildScriptsList(_applicationScriptsDirectory, _applicationScriptsDirectory, scripts, true);
				}
			}
			catch (IOException aException)
			{
				errorMsg = "Cannot read content script folder or one of its sub-folders.";
				_log.Error(errorMsg, aException);
				scripts = new List<ScriptListRowVM>();
				result = false;
			}
			catch (UnauthorizedAccessException aException)
			{
				errorMsg = "The selected script folder or one of its sub-folders is not accessible.";
				_log.Error("Cannot access script root directory because it or a subdirectory is not accessible.",
						   aException);
				scripts = new List<ScriptListRowVM>();
				result = false;
			}

			foreach (var view in scripts)
			{
				newRows.Add(view);
			}

			Rows = newRows;

			if (null != errorMsg)
			{
				var mbvm = new MessageBoxParams
				{
					Buttons = MessageBoxButton.OK,
					Caption = "Scripts tab",
					Icon = MessageBoxImage.Warning,
					Message = errorMsg
				};

				_dispatcher.BeginInvoke(new Action(() => { RequestDialogOpen?.Invoke(this, mbvm); }));
			}

			return result;
		}


		private void BuildScriptsList(DirectoryInfo aScriptDirectory, DirectoryInfo aTargetDirectory,
									  List<ScriptListRowVM> aScripts, bool isExample)
		{
			if (null == _scriptExtensions)
			{
				_scriptExtensions = new HashSet<string>();
				foreach (var lang in ScriptLanguage.FindAll())
				{
					foreach (var ext in lang.UniqueExtensions)
					{
						var e = ext;
						if (!e.StartsWith("."))
						{
							e = "." + e;
						}

						_scriptExtensions.Add(e.ToLower());
					}
				}

				_scriptExtensions.Add(".txt");
			}

			// List all files that aren't hidden and have a supported extension.
			try
			{
				foreach (var scriptFile in aTargetDirectory.GetFiles())
				{
					if (!FileUtil.IsHidden(scriptFile) && _scriptExtensions.Contains(scriptFile.Extension.ToLower()))
					{
						var view = new ScriptListRowVM(scriptFile, aScriptDirectory, isExample);

						var index = aScripts.BinarySearch(view);
						if (index < 0)
						{
							aScripts.Insert(~index, view);
						}
					}
				}
			}
			catch (PathTooLongException)
			{
				_log.WarnFormat("Could not find all scripts under '{0}' because the path is too long.",
								aTargetDirectory.FullName);
			}

			// Recursively search all sub-directories.
			if (ShowScriptsInSubFolders)
			{
				try
				{
					foreach (var sub in aTargetDirectory.GetDirectories())
					{
						var skip = (aScriptDirectory == aTargetDirectory)
							   && sub.Name.Equals("templates", StringComparison.InvariantCultureIgnoreCase);

						if (!skip && !FileUtil.IsHidden(sub))
						{
							BuildScriptsList(aScriptDirectory, sub, aScripts, isExample);
						}
					}
				}
				catch (PathTooLongException)
				{
					_log.WarnFormat("Not searching for scripts under '{0}' because the path is too long.",
									aTargetDirectory.FullName);
				}
			}
		}


		private bool CheckScriptsFolder()
		{
			if (Directory.Exists(Settings.Default.ScriptsFolder))
			{
				return true;
			}

			// If the scripts folder doesn't exist, but the recommended folder 
			// ("[My ]Documents\Zaber Console Scripts") does, then take that
			// as the scripts folder.
			var defaultScriptFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
								  + "\\Zaber Console Scripts";
			if (Directory.Exists(defaultScriptFolder))
			{
				SetScriptsFolder(defaultScriptFolder);

				return true;
			}

			// Offer to create the scripts folder.
			var mbvm = new MessageBoxParams
			{
				Message = "A folder could not be found at "
					  + defaultScriptFolder
					  + "."
					  + Environment.NewLine
					  + Environment.NewLine
					  + "Would you like Zaber Console to "
					  + "create a new \"Zaber Console Scripts\" folder for you?",
				Caption = "Could not find scripts folder",
				Buttons = MessageBoxButton.YesNo,
				Icon = MessageBoxImage.Question
			};

			RequestDialogOpen?.Invoke(this, mbvm);

			if (MessageBoxResult.Yes == mbvm.Result)
			{
				Directory.CreateDirectory(defaultScriptFolder);
				Settings.Default.ScriptsFolder = defaultScriptFolder;
				_dispatcher.BeginInvoke(new Action(() => { OnPropertyChanged(nameof(CurrentScriptFolder)); }));

				return true;
			}

			return ChooseScriptsFolder();
		}


		private bool ChooseScriptsFolder()
		{
			var dlgVM = new FolderBrowserDialogParams
			{
				SelectedPath = Settings.Default.ScriptsFolder,
				Description = "Please choose a folder to hold your script files.",
				ShowNewFolderButton = true
			};

			var pathError = false;
			try
			{
				RequestDialogOpen?.Invoke(this, dlgVM);
			}
			catch (PathTooLongException)
			{
				pathError = true;
			}
			catch (TargetInvocationException aException)
			{
				if (aException.InnerException is PathTooLongException)
				{
					pathError = true;
				}
				else
				{
					throw;
				}
			}

			if (pathError)
			{
				var mbvm = new MessageBoxParams
				{
					Buttons = MessageBoxButton.OK,
					Caption = "Path too long",
					Icon = MessageBoxImage.Error,
					Message =
						"The selected path was too long. This program does not support paths longer than 260 characters. Keeping the previously selected path."
				};

				RequestDialogOpen?.Invoke(this, mbvm);

				return false;
			}

			if (dlgVM.DialogResult != true)
			{
				return false;
			}

			SetScriptsFolder(dlgVM.SelectedPath);

			return true;
		}


		private void SetScriptsFolder(string path)
		{
			var changed = Settings.Default.ScriptsFolder != path;
			Settings.Default.ScriptsFolder = path;

			if (changed)
			{
				SetTemplateFinder();
				BuildScriptsGrid();

				_dispatcher.BeginInvoke(new Action(() => { OnPropertyChanged(nameof(CurrentScriptFolder)); }));
			}
		}


		private void DisplayError(string aCaption, Exception aException)
		{
			_log.Error(aCaption, aException);

			_dispatcher.BeginInvoke(new Action(() =>
			{
				var mbvm = new MessageBoxParams
				{
					Message = aException.Message,
					Caption = aCaption,
					Buttons = MessageBoxButton.OK,
					Icon = MessageBoxImage.Error
				};

				RequestDialogOpen?.Invoke(this, mbvm);
			}));
		}


		private void ShowScriptEditor(ScriptListRowVM aScript)
		{
			if (null == _scriptEditorVM)
			{
				_scriptEditorVM = new ScriptEditorWindowVM(this) { PortFacade = PortFacade };

				_scriptEditorVM.DialogClosing += ScriptEditorForm_FormClosed;

				RequestDialogOpen?.Invoke(this, _scriptEditorVM);

				_eventLog.LogEvent("Open script editor");
			}
			else
			{
				_scriptEditorVM.RequestBringToFront();
			}

			if (null != aScript)
			{
				_scriptEditorVM.OpenScript(aScript.ScriptFile, aScript.ScriptsFolder, aScript.IsExample);
			}

			// If the user refuses to set a script directory, scriptEditorForm
			// will not be opened and will therefore be null. Check for it.
			if (null != _scriptEditorVM)
			{
				_scriptEditorVM.SelectConversation(_conversation);
				OpenScriptFolderEnabled = false;
			}
		}


		private void BackgroundWorker_DoWork(object aSender, DoWorkEventArgs aArgs)
		{
			var view = (ScriptListRowVM) aArgs.Argument;
			aArgs.Result = view;

			// Don't interrupt a user script by invalidating.
			var oldValue = PortFacade.IsInvalidateEnabled;
			PortFacade.IsInvalidateEnabled = false;

			try
			{
				view.Runner.Run();
			}
			catch (Exception aException)
			{
				var msg = string.Format("script> aborted {0}: {1}",
										view != null ? view.Name : "script",
										aException.Message);
				OutputWrite(msg);
				DisplayError("Script execution failed", aException);
			}

			Thread.Sleep(50); // Let responses get added to log on another thread.
			OutputWrite("completed " + view.Name);

			// Restore the user's invalidation setting.
			PortFacade.IsInvalidateEnabled = oldValue;

			_dispatcher.BeginInvoke(new Action(() => { IsInteractive = false; }));
		}


		private void BackgroundWorker_RunWorkerCompleted(object aSender, RunWorkerCompletedEventArgs aArgs)
		{
			var view = (ScriptListRowVM) aArgs.Result;

			// If the user cancels a script which threw an exception or failed
			// to run for other reasons, the view.PlugIn will be null.
			if (view.Runner.ScriptPlugin != null)
			{
				view.Runner.ScriptPlugin.Dispose();
			}

			view.Runner.ScriptPlugin = null;

			_dispatcher.BeginInvoke(new Action(() =>
			{
				RunButtonLabel = "Run";
				IsRunning = false;
			}));
		}

		#endregion

		#region -- IDialogClient implementation --

		/// <summary>
		///     Implementing VM should fire this event when it wants the main window
		///     to open a dialog using the VM it supplies.
		/// </summary>
		public event RequestDialogChange RequestDialogOpen;

		/// <summary>
		///     Implementing VM should fire this event if it wants to programmatically
		///     close the dialog it previously opened.
		/// </summary>
		#pragma warning disable CS0067 // Event is never used.
		public event RequestDialogChange RequestDialogClose;
		#pragma warning restore CS0067

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Script tab");

		private ZaberPortFacade _portFacade;
		private ObservableCollection<ScriptListRowVM> _rows = new ObservableCollection<ScriptListRowVM>();
		private ScriptListRowVM _selectedRow;
		private bool _openScriptFolderEnabled = true;
		private bool _isInteractive;
		private BlockingStream _scriptInputStream;
		private StreamReader _scriptInputReader;
		private StreamWriter _scriptInputWriter;
		private string _inputText = string.Empty;
		private bool _inputTextHasFocus;
		private string _runButtonLabel = "Run";
		private bool _isRunning;
		private HashSet<string> _scriptExtensions;

		private readonly Dispatcher _dispatcher;

		private Conversation _conversation;

		private ScriptEditorWindowVM _scriptEditorVM;
		private readonly DirectoryInfo _applicationScriptsDirectory;

		private RelayCommand _deleteScriptCommand;

		private JointFileFinder _templateFinder;
		private readonly FileFinder _appTemplateFinder;

		#endregion
	}
}

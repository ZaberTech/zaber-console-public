﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Timers;
using System.Windows;
using System.Windows.Input;
using log4net;
using MvvmDialogs.ViewModels;
using Zaber;
using Zaber.PlugIns;
using Zaber.Telemetry;
using Zaber.Threading;
using ZaberConsole.Properties;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Dialogs;
using ZaberWpfToolbox.Logging;

namespace ZaberConsole.Plugins.Scripts
{
	public class ScriptEditorWindowVM : ObservableObject, IUserDialogViewModel, IDialogClient
	{
		#region -- Setup / context --

		public ScriptEditorWindowVM(IParent parent)
		{
			_parent = parent;

			_scriptWorker = BackgroundWorkerManager.CreateWorker();
			_scriptWorker.DoWork += ScriptWorker_DoWork;
			_scriptWorker.RunWorkerCompleted += ScriptWorker_RunWorkerCompleted;

			_scriptOutputTimer = new Timer();
			_scriptOutputTimer.Elapsed += ScriptOutputTimer_Tick;

			CreateInput();

			_defaultTemplateDisplay = TemplateText;

			BuildConversationList();
			BuildLanguageMenu();

			// Upgrade window settings to WPF format.
			var windowSettings = Settings.Default.ScriptEditorWindowSettings;
			if (null == windowSettings)
			{
				windowSettings = new WindowSettings();
				Settings.Default.ScriptEditorWindowSettings = windowSettings;
				windowSettings.WindowWidth = 800;
				windowSettings.WindowHeight = 600;
				windowSettings.CenterOnMainScreen();
			}

			if (null == windowSettings.UserMeasurements)
			{
				windowSettings.UserMeasurements = new string[2];
				if (null != windowSettings.SplitterDistances)
				{
					windowSettings.UserMeasurements[0] = windowSettings.SplitterDistances[0] + "*";
					windowSettings.UserMeasurements[1] =
						(windowSettings.WindowHeight - windowSettings.SplitterDistances[0]) + "*";
				}
				else
				{
					windowSettings.UserMeasurements[0] = "2*";
					windowSettings.UserMeasurements[1] = "1*";
				}
			}

			windowSettings.EnsureWindowVisible();

			// Start with a blank script.
			OpenScriptFile(null, null, false);

			OutputText.MaxLength = Settings.Default.LogMaxLength;
		}


		public ZaberPortFacade PortFacade
		{
			get => _port;
			set
			{
				if (null != _port)
				{
					_port.Opened -= PortFacade_OpenedOrClosed;
					_port.Closed -= PortFacade_OpenedOrClosed;
				}

				_port = value;

				if (null != _port)
				{
					_port.Opened += PortFacade_OpenedOrClosed;
					_port.Closed += PortFacade_OpenedOrClosed;
				}

				BuildConversationList();
			}
		}

		#endregion

		#region -- Public API --

		public void SelectConversation(Conversation aConversation)
		{
			foreach (var vm in Conversations)
			{
				if (aConversation == vm.Conversation)
				{
					SelectedConversation = vm;
					return;
				}
			}

			SelectedConversation = Conversations.FirstOrDefault();
		}


		public void OpenScript(FileInfo aFile, DirectoryInfo aScriptFolder, bool isReadOnly)
		{
			if (!PromptToSaveIfDirty())
			{
				return;
			}

			OpenScriptFile(aFile, aScriptFolder, isReadOnly);
		}


		/// <summary>
		///     Information about the file currently open in the script editor, if any.
		///     Note there may be a valid script typed into the editor that hasn't been saved yet.
		/// </summary>
		public FileInfo CurrentScriptFile => _script.File;

		#endregion

		#region -- View-bound properties --

		public string WindowTitle
		{
			get => _windowTitle;
			set => Set(ref _windowTitle, value, nameof(WindowTitle));
		}


		public string DeprecationNotice => (IsPortOpen && !PortFacade.Port.IsAsciiMode)
			? Resources.LABEL_SCRIPTING_DEPRECATED_BINARY
			: Resources.LABEL_SCRIPTING_DEPRECATED_ASCII;


		public ObservableCollection<ConversationVM> Conversations
		{
			get => _conversations;
			set => Set(ref _conversations, value, nameof(Conversations));
		}


		public ConversationVM SelectedConversation
		{
			get => _selectedConversation;
			set => Set(ref _selectedConversation, value, nameof(SelectedConversation));
		}


		public bool ConversationSelectionEnabled
		{
			get => _conversationSelectionEnabled;
			set => Set(ref _conversationSelectionEnabled, value, nameof(ConversationSelectionEnabled));
		}


		public string RunButtonLabel
		{
			get => _runButtonLabel;
			set => Set(ref _runButtonLabel, value, nameof(RunButtonLabel));
		}


		public bool InputEnabled
		{
			get => _inputEnabled;
			set => Set(ref _inputEnabled, value, nameof(InputEnabled));
		}


		public LogControlVM OutputText { get; } = new LogControlVM();


		public int SelectedTabIndex
		{
			get => _selectedTabIndex;
			set => Set(ref _selectedTabIndex, value, nameof(SelectedTabIndex));
		}


		public string TemplateText
		{
			get => _templateText;
			set => Set(ref _templateText, value, nameof(TemplateText));
		}


		public string HelpText => Resources.ScriptEditorHelpText;


		public bool IsPortOpen => PortFacade?.IsOpen ?? false;


		public int[] ErrorLines
		{
			get => _errorLines;
			set => Set(ref _errorLines, value, nameof(ErrorLines));
		}


		public ICommand SelectLanguageCommand => new RelayCommand(aArg => SelectLanguage(((LanguageVM) aArg).Language));


		public ICommand RunOrCancelCommand => new RelayCommand(_ => OnRunOrCancel());

		#region ---- File menu stuff ----

		public ICommand OpenScriptCommand => new RelayCommand(_ => OnOpenScript());


		public ICommand SaveScriptCommand => new RelayCommand(_ => OnSaveScript(), _ => _canSave);


		public ICommand SaveScriptAsCommand => new RelayCommand(_ => OnSaveScriptAs());


		public ICommand NewScriptCommand => new RelayCommand(_ => OnNewScript());


		public ICommand ExploreScriptFolderCommand
			=> new RelayCommand(_ => Process.Start(Settings.Default.ScriptsFolder));


		public ICommand CloseWindowCommand => new RelayCommand(_ => RequestClose());


		public ICommand LanguageShortcutCommand
		{
			get
			{
				if (null == _languageShortcutCommand)
				{
					_languageShortcutCommand = new RelayCommand(aLangIndex =>
					{
						var i = (int) aLangIndex;
						if ((i >= 0) && (i < Languages.Count))
						{
							SelectedLanguage = Languages[i];
						}
					});
				}

				return _languageShortcutCommand;
			}
		}

		#endregion

		#region --- Language menu stuff ----

		public ObservableCollection<LanguageVM> Languages
		{
			get => _languages;
			set => Set(ref _languages, value, nameof(Languages));
		}


		public LanguageVM SelectedLanguage
		{
			get => _selectedLanguage;
			set
			{
				Set(ref _selectedLanguage, value, nameof(SelectedLanguage));
				OnPropertyChanged(nameof(SelectedLanguageName));

				foreach (var language in Languages)
				{
					language.Checked = language == _selectedLanguage;
				}
			}
		}

		#endregion

		#region ---- Help menu stuff ----

		public ICommand SelectTabCommand => new RelayCommand(aIndex =>
		{
			SelectedTabIndex = (int) aIndex;
			_eventLog.LogEvent("Viewed help", 2 == SelectedTabIndex ? "Template tab" : "Help tab");
		});


		public ICommand LaunchChmCommand => new RelayCommand(aCommand =>
		{
			var path = (string) aCommand;
			System.Windows.Forms.Help.ShowHelp(null, "Help/SDKs/Zaber.chm", path);
			_eventLog.LogEvent("Viewed help", "Scripting API reference");
		});


		public ICommand LaunchCommand => new RelayCommand(aCommand =>
		{
			var path = (string) aCommand;

			// If we're launching a relative file path, canonicalize it relative to the executable.
			if (!path.ToLower().Contains("http"))
			{
				path = path.Replace('/', Path.DirectorySeparatorChar).Replace('\\', Path.DirectorySeparatorChar);
				if (!Path.IsPathRooted(path))
				{
					var appDir = AppDomain.CurrentDomain.BaseDirectory;
					path = Path.Combine(appDir, path);
				}
			}

			Process.Start(path);
			_eventLog.LogEvent("Viewed help", path);
		});

		#endregion

		#region ---- Editor control stuff ----

		public string SelectedLanguageName => _scintillaLanguageNameMap[SelectedLanguage.Name];


		public string ScriptText
		{
			get => _scriptText;
			set => Set(ref _scriptText, value, nameof(ScriptText));
		}


		// It feels weird to be using an ICommand for this. Is there a better way?
		public ICommand DocumentChangedCommand => new RelayCommand(aCode => OnDocumentChanged((int) aCode));

		#endregion

		#region -- Script input and output tab stuff --

		public string InputText
		{
			get => _inputText;
			set => Set(ref _inputText, value, nameof(InputText));
		}


		public bool InputTextHasFocus
		{
			get => _inputTextHasFocus;
			set => Set(ref _inputTextHasFocus, value, nameof(InputTextHasFocus));
		}


		public ICommand InputEnterKeyCommand => new RelayCommand(_ => OnInputEnterKeyPressed());

		#endregion

		#endregion

		#region -- IUserDialogViewModel implementation --

		public bool IsModal => false;


		public event EventHandler DialogClosing;

		public event EventHandler BringToFront;


		public void RequestClose()
		{
			if (!PromptToSaveIfDirty())
			{
				return;
			}

			Settings.Default.ScriptLanguage = _selectedLanguage.Name;
			if (null == Settings.Default.ScriptEditorWindowSettings)
			{
				Settings.Default.ScriptEditorWindowSettings = new WindowSettings();
			}

			PortFacade.Opened -= PortFacade_OpenedOrClosed;
			PortFacade.Closed -= PortFacade_OpenedOrClosed;
			_closed = true;
			DialogClosing?.Invoke(this, null);
		}


		public void RequestBringToFront() => BringToFront?.Invoke(this, null);


		/// <summary>
		///     Any exception that was unhandled by the dialog itself.
		/// </summary>
		public Exception Exception { get; set; }

		#endregion

		#region -- IDialogClient implementation --

		public event RequestDialogChange RequestDialogOpen;
		#pragma warning disable CS0067 // Event is never used.
		public event RequestDialogChange RequestDialogClose;
		#pragma warning restore CS0067

		#endregion

		#region -- Functionality --

		private void CreateInput()
		{
			_scriptInputStream = new BlockingStream(new MemoryStream());
			_scriptInputStream.Blocked += ScriptInputStream_Blocked;
			_scriptInputReader = new StreamReader(_scriptInputStream);
			_scriptInputWriter = new StreamWriter(_scriptInputStream) { AutoFlush = true };
			_isInputRequested = false;
		}


		private void ScriptInputStream_Blocked(object aSender, EventArgs aArgs) => _isInputRequested = true;


		private void BuildConversationList()
		{
			var oldSelection = SelectedConversation?.Conversation;
			var newList = new ObservableCollection<ConversationVM>();

			if (null != PortFacade)
			{
				foreach (var conversation in PortFacade.AllConversations)
				{
					if (conversation.Device.AxisNumber == 0)
					{
						var view = new ConversationVM(conversation);
						newList.Add(view);
					}
				}
			}

			Conversations = newList;

			SelectConversation(oldSelection);
		}


		private void PortFacade_OpenedOrClosed(object aSender, EventArgs aArgs) => _dispatcher.BeginInvoke(() =>
		{
			Conversations.Clear();
			BuildConversationList();
			EnableButtons();
			OnPropertyChanged(nameof(IsPortOpen));
			OnPropertyChanged(nameof(DeprecationNotice));
		});


		/// <summary>
		///     Fill the "Language" toolstrip menu with the languages supported.
		/// </summary>
		private void BuildLanguageMenu()
		{
			var languages = ScriptLanguage.FindAll();
			var menuItems = new ObservableCollection<LanguageVM>();
			foreach (var language in languages)
			{
				_languageMap[language.Name] = language;
				var vm = new LanguageVM(language);

				if (menuItems.Count < 10)
				{
					vm.ShortcutName = "Ctrl-" + menuItems.Count;
				}

				menuItems.Add(vm);
			}

			Languages = menuItems;

			foreach (var menuItem in menuItems)
			{
				if (menuItem.Name.Equals(Settings.Default.ScriptLanguage))
				{
					SelectLanguage(menuItem.Language);
					break;
				}

				if ("csharp" == menuItem.Language.Name)
				{
					SelectLanguage(menuItem.Language);
				}
			}

			if (null == SelectedLanguage)
			{
				SelectLanguage(Languages.FirstOrDefault()?.Language);
			}
		}


		private void SelectLanguage(ScriptLanguage aLanguage)
		{
			foreach (var vm in Languages)
			{
				if (vm.Language.Name == aLanguage.Name)
				{
					SelectedLanguage = vm;
				}
			}

			if (null != _script)
			{
				_script.Language = SelectedLanguage.Language;
			}

			_eventLog.LogEvent("Language selected", SelectedLanguage?.Name ?? "None");
		}


		private void EnableButtons()
		{
			ConversationSelectionEnabled = (null != PortFacade) && PortFacade.IsOpen && !_scriptWorker.IsBusy;
			RunButtonLabel = _scriptWorker.IsBusy ? "Cancel Sc_ript" : "_Run Script";
			InputEnabled = _scriptWorker.IsBusy;
		}


		private void ScriptWorker_DoWork(object aSender, DoWorkEventArgs aArgs)
		{
			// Don't interrupt a user script by invalidating.
			var oldValue = PortFacade.IsInvalidateEnabled;
			PortFacade.IsInvalidateEnabled = false;

			try
			{
				_runner.Run();
				_dispatcher.BeginInvoke(() => { ErrorLines = null; });
			}
			catch (Exception aException)
			{
				var ce = aException as CompilerException;
				var ex = aException;
				while ((null == ce) && (null != ex.InnerException))
				{
					ex = ex.InnerException;
					ce = ex as CompilerException;
				}

				if (null != ce)
				{
					var lines = new HashSet<int>();
					if (null != ce.ErrorLineNumbers)
					{
						lines = new HashSet<int>(ce.ErrorLineNumbers);
					}

					var lineList = lines.ToList();
					lineList.Sort();
					_dispatcher.BeginInvoke(() => { ErrorLines = lineList.ToArray(); });
				}

				DisplayError("Error executing your script", aException);
			}

			// Restore the user's invalidation setting.
			PortFacade.IsInvalidateEnabled = oldValue;
		}


		private void DisplayError(string aCaption, Exception aException)
		{
			_log.Error(aCaption, aException);

			var mbvm = new MessageBoxParams
			{
				Message = aException.Message,
				Caption = aCaption,
				Buttons = MessageBoxButton.OK,
				Icon = MessageBoxImage.Error
			};

			RequestDialogOpen?.Invoke(this, mbvm);
		}


		private void ScriptWorker_RunWorkerCompleted(object aSender, RunWorkerCompletedEventArgs aArgs)
		{
			if (null != _runner.ScriptPlugin)
			{
				_runner.ScriptPlugin.Dispose();
				_runner.ScriptPlugin = null;
			}

			if (!_closed)
			{
				CreateInput(); // for next run
				_scriptOutputTimer.Enabled = false;
				ScriptOutputTimer_Tick(_scriptOutputTimer, EventArgs.Empty);
				EnableButtons();
			}
		}


		private void OnOpenScript()
		{
			if (!PromptToSaveIfDirty())
			{
				return;
			}

			var userScriptFolder = _parent.GetUserScriptDirectory();
			if (userScriptFolder == null)
			{
				return;
			}

			var dialog = new FileBrowserDialogParams();

			ConfigureFileDialog(dialog, userScriptFolder);

			RequestDialogOpen?.Invoke(this, dialog);

			if (dialog.DialogResult == true)
			{
				var fileInfo = ValidateOpenFileName(dialog.Filename, userScriptFolder);
				if (fileInfo != null)
				{
					OpenScriptFile(fileInfo, userScriptFolder, false);
				}
			}
		}


		private void OnNewScript()
		{
			if (!PromptToSaveIfDirty())
			{
				return;
			}

			OpenScriptFile(null, null, false);
		}


		private FileInfo ValidateOpenFileName(string aFilename, DirectoryInfo userScriptDirectory)
		{
			var fileInfo = new FileInfo(aFilename);
			if (FileUtil.IsInDirectory(userScriptDirectory.FullName, aFilename))
			{
				return fileInfo;
			}

			var mbvm = new MessageBoxParams
			{
				Message =
					fileInfo.Name
				+ " cannot be opened because it is not in "
				+ "the script folder. Do you want to copy it to the script folder?",
				Caption = "Open Script",
				Buttons = MessageBoxButton.YesNo,
				Icon = MessageBoxImage.Question
			};

			RequestDialogOpen?.Invoke(this, mbvm);

			if (MessageBoxResult.Yes != mbvm.Result)
			{
				return null;
			}

			var copyInfo = new FileInfo(Path.Combine(userScriptDirectory.FullName, fileInfo.Name));
			if (copyInfo.Exists)
			{
				mbvm.Message = "There is already a file named "
						   + fileInfo.Name
						   + " in the script folder. Do you want to overwrite it?";
				mbvm.Result = MessageBoxResult.None;

				RequestDialogOpen?.Invoke(this, mbvm);

				if (MessageBoxResult.Yes != mbvm.Result)
				{
					return null;
				}
			}

			fileInfo.CopyTo(copyInfo.FullName, true);

			_parent.NewFile();

			return copyInfo;
		}


		private void OpenScriptFile(FileInfo aFile, DirectoryInfo aScriptFolder, bool isReadOnly)
		{
			_runner = new ScriptRunner();

			string errorMessage = null;
			try
			{
				if (null == aFile)
				{
					_script = new Script(_selectedLanguage.Language, _parent.GetTemplateFinder());

					_canSave = true;
					_scriptName = "Untitled";
					_eventLog.LogEvent("Created new script");
				}
				else
				{
					_script = new Script(aFile, _parent.GetTemplateFinder());

					SelectLanguage(ScriptLanguage.FindByExtension(aFile));

					_canSave = !isReadOnly && FileUtil.CanBeWritten(aFile.FullName);
					_scriptName = FileUtil.GetPathInDirectory(aScriptFolder, aFile);
					_eventLog.LogEvent("Opened file", aFile.Name);
				}

				ScriptText = _script.Text;
				UpdateTemplateDisplay();
				EnableButtons();
			}
			catch (Exception aException)
			{
				errorMessage = "There was an error opening the script file: " + aException.Message;
				_log.Error("Error opening script file " + aFile.FullName, aException);
			}

			if (null != errorMessage)
			{
				var mbvm = new MessageBoxParams
				{
					Message = errorMessage,
					Caption = "Error opening script",
					Icon = MessageBoxImage.Error,
					Buttons = MessageBoxButton.OK
				};

				RequestDialogOpen?.Invoke(this, mbvm);
			}
		}


		private void ConfigureFileDialog(FileBrowserDialogParams aDialogVM, DirectoryInfo aStartingFolder,
										 string fileName = null)
		{
			aDialogVM.Filter = _selectedLanguage.Language.FileFilter + "|All files (*.*)|*.*";
			aDialogVM.FilterIndex = 1;

			if (null != aStartingFolder)
			{
				aDialogVM.InitialDirectory = aStartingFolder.FullName;
			}

			if (fileName != null)
			{
				aDialogVM.Filename = fileName;
			}
		}


		/// <summary>
		///     If there is text in the script editor and it hasn't been saved to a file,
		///     prompt the user to save it.
		/// </summary>
		/// <returns>True if it is OK to discard the data in the script editor.</returns>
		internal bool PromptToSaveIfDirty()
		{
			var isConfirmed = true;
			if (_script?.IsDirty == true)
			{
				RequestBringToFront();

				var mbvm = new MessageBoxParams
				{
					Message = $"Save changes to {_scriptName}?",
					Caption = "Confirm",
					Buttons = MessageBoxButton.YesNoCancel,
					Icon = MessageBoxImage.Question
				};

				RequestDialogOpen?.Invoke(this, mbvm);
				isConfirmed = MessageBoxResult.Cancel != mbvm.Result;
				if (MessageBoxResult.Yes == mbvm.Result)
				{
					OnSaveScript();

					if (_script.IsDirty)
					{
						isConfirmed = false;
					}
				}
			}

			if (isConfirmed && (null != _runner) && _runner.IsRunning)
			{
				RequestBringToFront();

				var mbvm = new MessageBoxParams
				{
					Message = "Stop running current script?",
					Caption = "Confirm",
					Buttons = MessageBoxButton.OKCancel,
					Icon = MessageBoxImage.Question
				};

				RequestDialogOpen?.Invoke(this, mbvm);

				isConfirmed = MessageBoxResult.Cancel != mbvm.Result;
				if (isConfirmed)
				{
					_runner.Cancel(PortFacade.Port);
				}
			}

			return isConfirmed;
		}


		private void SetWindowTitle()
		{
			var dirtyIndicator = _script.IsDirty ? "*" : string.Empty;

			var title = $"Script Editor - {_scriptName}{dirtyIndicator}";
			WindowTitle = title;
		}


		private void OnSaveScript()
		{
			if (_script.File == null)
			{
				OnSaveScriptAs();
			}
			else
			{
				onSaveScriptExistingFile();
			}
		}


		private void OnSaveScriptAs()
		{
			var dialog = new FileBrowserDialogParams();

			var userScriptFolder = _parent.GetUserScriptDirectory();
			if (userScriptFolder == null)
			{
				return;
			}

			ConfigureFileDialog(dialog, userScriptFolder, _scriptName);
			dialog.CheckFileExists = false;
			dialog.Action = FileBrowserDialogParams.FileActionType.Save;

			string saveError = null;
			try
			{
				RequestDialogOpen?.Invoke(this, dialog);
			}
			catch (Exception aException)
			{
				if (aException is PathTooLongException || aException.InnerException is PathTooLongException)
				{
					saveError =
						"The selected path is too long; this program only works with paths up to 260 characters long. Please try again and select a shorter path.";
				}
				else
				{
					saveError = "There was an error saving the file: " + aException.Message;
				}

				_log.Error(saveError, aException);
			}

			if ((null == saveError) && (dialog.DialogResult == true))
			{
				FileInfo newFile = null;
				try
				{
					newFile = new FileInfo(dialog.Filename);
					_script.SaveAs(newFile);
				}
				catch (Exception aException)
				{
					saveError = "There was an error saving the file: " + aException.Message;
					_log.Error(saveError, aException);
				}

				if (null == saveError)
				{
					if (FileUtil.IsInDirectory(userScriptFolder, newFile))
					{
						ScriptText = ""; // forces reevaluation when loading from new file
						OpenScriptFile(newFile, userScriptFolder, false);
						_parent.NewFile();
					}
					else
					{
						var mbvm = new MessageBoxParams
						{
							Message = "The file has been saved, but the script editor "
								  + "has to close because it can only edit files in "
								  + "the script folder or its subfolders.",
							Caption = "Save Script As",
							Buttons = MessageBoxButton.OK,
							Icon = MessageBoxImage.Information
						};

						RequestDialogOpen?.Invoke(this, mbvm);

						RequestClose();
					}
				}
			}

			if (null != saveError)
			{
				ShowSaveError(saveError);
			}
		}


		private void onSaveScriptExistingFile()
		{
			try
			{
				_script.Save();
			}
			catch (Exception aException)
			{
				var saveError = "There was an error saving the file: " + aException.Message;
				_log.Error(saveError, aException);

				ShowSaveError(saveError);
				return;
			}

			SetWindowTitle();
		}


		private void ShowSaveError(string aSaveError)
		{
			var mbvm = new MessageBoxParams
			{
				Message = aSaveError,
				Caption = "Error saving file",
				Buttons = MessageBoxButton.OK,
				Icon = MessageBoxImage.Error
			};

			RequestDialogOpen?.Invoke(this, mbvm);
		}


		private void OnRunOrCancel()
		{
			if (_scriptWorker.IsBusy)
			{
				CancelScript();
			}
			else
			{
				RunScript();
			}

			EnableButtons();
		}


		private void RunScript()
		{
			OutputText.Text = string.Empty;

			try
			{
				_runner.ScriptPlugin = _script.Build();
				_dispatcher.BeginInvoke(() => { ErrorLines = null; });
			}
			catch (CompilerException ce)
			{
				var lines = new HashSet<int>();
				if (null != ce.ErrorLineNumbers)
				{
					lines = new HashSet<int>(ce.ErrorLineNumbers);
				}

				var lineList = lines.ToList();
				lineList.Sort();
				_dispatcher.BeginInvoke(() => { ErrorLines = lineList.ToArray(); });

				_log.Error("Failed to build script.", ce);

				var mbvm = new MessageBoxParams
				{
					Message = "There was a problem building your script. "
						  + "Please see the \"Output\" tab in the script editor "
						  + "window for more detail.",
					Caption = "Could not build your script",
					Buttons = MessageBoxButton.OK,
					Icon = MessageBoxImage.Error
				};

				RequestDialogOpen?.Invoke(this, mbvm);

				return; // Bail early, since the script can't be run if it isn't built.
			}
			catch (Exception ex)
			{
				DisplayError("Problem building your script", ex);
				return;
			}
			finally
			{
				DisplayCompilerErrors();
			}

			_runner.ScriptPlugin.PortFacade = PortFacade;
			var view = SelectedConversation;
			_runner.ScriptPlugin.Conversation = view?.Conversation;
			_runner.ScriptPlugin.Input = _scriptInputReader;
			var watcher = new TextWriterWatcher();
			watcher.Written += ScriptOutputWritten;
			_runner.ScriptPlugin.Output = watcher;
			_isScriptOutputStarted = false;
			_scriptOutputTimer.Enabled = true;

			try
			{
				_scriptWorker.Run();
				EnableButtons();
				_eventLog.LogEvent("Script run");
			}
			catch (Exception ex)
			{
				if (null != _runner.ScriptPlugin)
				{
					_runner.ScriptPlugin.Dispose();
					_runner.ScriptPlugin = null;
				}

				DisplayError("Script execution failed.", ex);
			}
		}


		private void ScriptOutputWritten(object aSender, TextEventArgs aArgs)
		{
			lock (_scriptIoLocker)
			{
				if (null == _scriptOutput)
				{
					_scriptOutput = new StringWriter();
				}

				_scriptOutput.Write(aArgs.Text);
			}
		}


		private void DisplayCompilerErrors()
		{
			var writer = new StringWriter();
			foreach (CompilerError error in _script.CompilerErrors)
			{
				if (Settings.Default.ScriptEditorShowWarnings || !error.IsWarning)
				{
					writer.WriteLine("{0} at line {1} column {2}", error.ErrorText, error.Line, error.Column);
				}
			}

			OutputText.Append(writer.ToString());
		}


		private void CancelScript()
		{
			_scriptInputStream.IsBlockingEnabled = false;
			_runner.Cancel(PortFacade.Port);
			_eventLog.LogEvent("Script cancelled");
		}


		private void ScriptOutputTimer_Tick(object aSender, EventArgs aArgs)
		{
			lock (_scriptIoLocker)
			{
				if (null != _scriptOutput)
				{
					var text = _scriptOutput.ToString();
					DisplayScriptOutput(text);
					_scriptOutput.Close();
					_scriptOutput = null;
				}
			}

			if (_isInputRequested)
			{
				_dispatcher.BeginInvoke(() =>
				{
					SelectedTabIndex = 0;
					InputTextHasFocus = true;
				});

				_isInputRequested = false;
			}
		}


		private void DisplayScriptOutput(string aMessage)
		{
			if (!_isScriptOutputStarted && (OutputText.Text.Length > 0))
			{
				OutputText.Append("\r\n---\r\n");
			}

			_isScriptOutputStarted = true;
			OutputText.Append(aMessage);
		}


		private void OnInputEnterKeyPressed()
		{
			DisplayScriptOutput(">" + InputText + "\r\n");
			_scriptInputWriter.WriteLine(InputText);
			InputText = string.Empty;
		}


		private void OnDocumentChanged(int aModificationType)
		{
			// See http://www.scintilla.org/ScintillaDoc.html#SCN_MODIFIED for
			// info on the modificationType flag.

			// We only care about the last two bytes.
			var modificationType = aModificationType & 0xFF;

			// User inserted (0x11) or deleted (0x12) text in the document. 
			if ((0x11 == modificationType) || (0x12 == modificationType))
			{
				UpdateTemplateDisplay();
			}
		}


		private void UpdateTemplateDisplay()
		{
			_script.Text = ScriptText;
			var templateDisplay = _script.Template.Text;
			if (0 == templateDisplay.Length)
			{
				templateDisplay = _defaultTemplateDisplay;
			}

			TemplateText = templateDisplay;
			SetWindowTitle();
		}

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Script editor");

		private string _templateText = Resources.DefaultTemplateText;
		private ObservableCollection<ConversationVM> _conversations = new ObservableCollection<ConversationVM>();
		private ConversationVM _selectedConversation;
		private ObservableCollection<LanguageVM> _languages = new ObservableCollection<LanguageVM>();
		private bool _conversationSelectionEnabled;
		private string _runButtonLabel = "_Run Script";
		private bool _inputEnabled = true;
		private string _scriptText = string.Empty;
		private string _windowTitle = "Script Editor - Untitled";
		private string _inputText = string.Empty;
		private bool _inputTextHasFocus;
		private int _selectedTabIndex;
		private ICommand _languageShortcutCommand;

		private readonly IParent _parent;
		private ZaberPortFacade _port;
		private readonly IDispatcher _dispatcher = DispatcherHelper.Dispatcher;
		private readonly IBackgroundWorker _scriptWorker;
		private readonly Timer _scriptOutputTimer;
		private bool _closed;

		private ScriptRunner _runner;
		private Script _script;
		private string _scriptName;
		private bool _canSave;

		private readonly Dictionary<string, ScriptLanguage> _languageMap =
			new Dictionary<string, ScriptLanguage>();

		private LanguageVM _selectedLanguage;
		private readonly string _defaultTemplateDisplay;
		private BlockingStream _scriptInputStream;
		private bool _isScriptOutputStarted;
		private StreamWriter _scriptInputWriter;
		private StreamReader _scriptInputReader;
		private bool _isInputRequested;
		private readonly object _scriptIoLocker = new object();
		private StringWriter _scriptOutput;
		private int[] _errorLines;


		// <summary>
		// Used to change between detected compiler-supported language names
		// and Scintilla's special language name codes for use with the
		// scintilla.ConfigurationManager.Language property.
		// </summary>
		private readonly Dictionary<string, string> _scintillaLanguageNameMap =
			new Dictionary<string, string>
			{
				{ "c++", "cpp" },
				{ "csharp", "cs" },
				{ "javascript", "js" },
				{ "IronPython", "python" },
				{ "visualbasic", "vbscript" }
			};

		#endregion

		public interface IParent
		{
			#region -- Public Methods & Properties --

			void NewFile();

			DirectoryInfo GetUserScriptDirectory();

			IFileFinder GetTemplateFinder();

			#endregion
		}
	}
}

using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace ZaberConsole
{
	/// <summary>
	///     This exception is thrown by the worker thread to wrap any exceptions thrown
	///     by the script.
	/// </summary>
	[Serializable]
	public class ScriptException : Exception
	{
		#region -- Public Methods & Properties --

		public ScriptException()
			: base(DEFAULT_MESSAGE)
		{
		}


		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="message">The message to display</param>
		public ScriptException(string message)
			: base(message)
		{
		}


		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="message">The message to display</param>
		/// <param name="ex">The exception that caused this one</param>
		public ScriptException(string message, Exception ex)
			: base(message, ex)
		{
		}


		/// <summary>
		///     Initializes a new instance with serialized
		///     data.
		/// </summary>
		/// <param name="info">
		///     The System.Runtime.Serialization.SerializationInfo that holds the serialized
		///     object data about the exception being thrown.
		/// </param>
		/// <param name="context">
		///     The System.Runtime.Serialization.StreamingContext that contains contextual
		///     information about the source or destination.
		/// </param>
		protected ScriptException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}


		/// <summary>
		///     Sets the System.Runtime.Serialization.SerializationInfo
		///     with information about the exception.
		/// </summary>
		/// <param name="info">
		///     The System.Runtime.Serialization.SerializationInfo that holds the serialized
		///     object data about the exception being thrown.
		/// </param>
		/// <param name="context">
		///     The System.Runtime.Serialization.StreamingContext that contains contextual
		///     information about the source or destination.
		/// </param>
		/// <exception cref="ArgumentNullException">The info parameter is a null reference.</exception>
		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}

			base.GetObjectData(info, context);
		}

		#endregion

		#region -- Data --

		private const string DEFAULT_MESSAGE =
			"Script failed.";

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Windows.Forms;
using log4net;
using MvvmDialogs.ViewModels;
using Zaber.PlugIns;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Dialogs;

namespace ZaberConsole.Plugins
{
	// Container for one plugin class instance. Associates the tab (if any) with the plugin
	// instance (which may or may not be the same object as the tab). Responsible for
	// invoking plugin methods and setting plugin properties under direction of the Plugin Manager.
	public class PluginInstance : IDialogClient
	{
		#region -- Public API --

		public PluginInstance(PlugInManager aManager, PluginTypeInfo aPluginType)
		{
			Manager = aManager;
			TypeInfo = aPluginType;
			var type = TypeInfo.Type;

			if (typeof(PluginTabVM).IsAssignableFrom(type))
			{
				// Plugin is also a tab.
				Tab = Activator.CreateInstance(type) as PluginTabVM;
				Instance = Tab;
			}
			else if (typeof(INotifyPropertyChanged).IsAssignableFrom(type))
			{
				// Plugin is a VM for a tab. Wrap it in a WPF host tab so its UI gets displayed.
				Instance = Activator.CreateInstance(type);
				Tab = new PluginHostTabVM((ObservableObject) Instance);
			}
			else if (typeof(Control).IsAssignableFrom(type))
			{
				// Plugin is a legacy WinForms tab. Wrap it in a WinForms host tab.
				Instance = Activator.CreateInstance(type);
				Tab = new WinFormsHostTabVM((Control) Instance);
			}
			else
			{
				// Plugin has no user interface.
				Instance = Activator.CreateInstance(type);
				Tab = null;
			}

			var dlgClient = Instance as IDialogClient;
			if (null != dlgClient)
			{
				dlgClient.RequestDialogOpen += ForwardDialogOpenRequest;
				dlgClient.RequestDialogClose += ForwardDialogCloseRequest;
			}

			if (null != Tab)
			{
				Tab.Plugin = this;
				Tab.TabName = aPluginType.DisplayName;
			}

			if (Instance == null)
			{
				throw new ArgumentException("instance is null");
			}

			AddPluginHooks(type, Instance);
		}


		// Sets a plugin property on this instance only.
		public void SetProperty(object aValue, Type aPropertyType, string aPropertyName)
		{
			foreach (var setter in _propertySetters)
			{
				try
				{
					setter.Set(aValue, aPropertyType, aPropertyName);
				}
				catch (Exception aException)
				{
					_log.Warn(
						$"Error setting plugin property {aPropertyType.Name} to {aValue?.ToString() ?? "<null>"}.",
						aException);
					throw;
				}
			}
		}


		// Invoked a plugin method on this instance only.
		public void InvokeMethod(PlugInMethodAttribute.EventType aEventType)
		{
			#if DEV || TEST

			// Sanity check the order of events.
			switch (aEventType)
			{
				case PlugInMethodAttribute.EventType.PluginActivated:
					if (PlugInMethodAttribute.EventType.PluginDeactivated != _lastEvent)
					{
						throw new InvalidProgramException("Plugin deactivation event can only be followed by activation.");
					}
					break;
				case PlugInMethodAttribute.EventType.PluginDeactivated:
					if ((PlugInMethodAttribute.EventType.PluginHidden != _lastEvent) && (PlugInMethodAttribute.EventType.PluginActivated != _lastEvent))
					{
						throw new InvalidProgramException("Plugin must be activated or hidden before being deactivated.");
					}
					break;
				case PlugInMethodAttribute.EventType.PluginShown:
					if ((PlugInMethodAttribute.EventType.PluginHidden != _lastEvent) && (PlugInMethodAttribute.EventType.PluginActivated != _lastEvent))
					{
						throw new InvalidProgramException("Plugin must be activated or hidden before being shown.");
					}
					break;
				case PlugInMethodAttribute.EventType.PluginHidden:
					if (PlugInMethodAttribute.EventType.PluginShown != _lastEvent)
					{
						throw new InvalidProgramException("Plugin must be shown before being hidden.");
					}
					break;
				default:
					throw new InvalidProgramException($"Unhandled event type {aEventType}");
			}

			_lastEvent = aEventType;
			#endif

			foreach (var invoker in _methodInvokers)
			{
				try
				{
					invoker.Invoke(aEventType);
				}
				catch (Exception aException)
				{
					_log.Warn($"Error invoking plugin event {aEventType}.", aException);
					throw;
				}
			}
		}


		public override string ToString() => "Instance of " + TypeInfo;


		// Forwards docking manager close commands to the plugin manager.
		public void Deactivate() => Manager.Deactivate(this);


		// Reference to the plugin manager.
		public PlugInManager Manager { get; }


		// Information about the type of this plugin.
		public PluginTypeInfo TypeInfo { get; }


		// The actual plugin instance.
		public object Instance { get; }


		// The plugin's user interface, if any.
		public PluginTabVM Tab { get; }

		#endregion

		#region --  IDialogClient implementation --

		public event RequestDialogChange RequestDialogOpen;

		public event RequestDialogChange RequestDialogClose;

		#endregion

		#region -- Child IDialogClient handling --

		private void ForwardDialogOpenRequest(object aSender, IDialogViewModel aVM)
			=> RequestDialogOpen?.Invoke(aSender, aVM);


		private void ForwardDialogCloseRequest(object aSender, IDialogViewModel aVM)
			=> RequestDialogClose?.Invoke(aSender, aVM);

		#endregion

		#region -- Private functionality --

		private void AddPluginHooks(Type aType, object aPlugin)
		{
			foreach (var property in aType.GetProperties())
			{
				var customAttributes = property.GetCustomAttributes(typeof(PlugInPropertyAttribute), true);

				var isPlugInProperty = customAttributes.Length > 0;
				if (!isPlugInProperty)
				{
					continue;
				}

				_propertySetters.Add(
					new PlugInPropertySetter(((PlugInPropertyAttribute) customAttributes[0]).Name, aPlugin, property));
			}


			foreach (var method in aType.GetMethods(BindingFlags.Instance | BindingFlags.Public))
			{
				var customAttributes = method.GetCustomAttributes(typeof(PlugInMethodAttribute), true);

				if (0 == customAttributes.Length)
				{
					continue;
				}

				_methodInvokers.Add(
					new PluginMethodInvoker(((PlugInMethodAttribute) customAttributes[0]).Event, aPlugin, method));
			}
		}

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private readonly List<PlugInPropertySetter> _propertySetters = new List<PlugInPropertySetter>();
		private readonly List<PluginMethodInvoker> _methodInvokers = new List<PluginMethodInvoker>();

		#if DEV || TEST
		private PlugInMethodAttribute.EventType _lastEvent = PlugInMethodAttribute.EventType.PluginDeactivated;
		#endif

		#endregion
	}
}

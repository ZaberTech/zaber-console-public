using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Threading;
using Xceed.Wpf.AvalonDock.Layout;
using Xceed.Wpf.AvalonDock.Layout.Serialization;
using Zaber;
using Zaber.PlugIns;
using ZaberConsole.Behaviors;
using ZaberConsole.Plugins.CommandList;
using ZaberConsole.Plugins.Help;
using ZaberConsole.Plugins.Options;
using ZaberConsole.Plugins.Scripts;
using ZaberConsole.Plugins.SettingsList;
using ZaberConsole.Plugins.Terminal;
using ZaberConsole.Properties;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Dialogs;
using ZaberWpfToolbox.Logging;

namespace ZaberConsole.Plugins
{
	public partial class PlugInManager : ObservableObject, IPlugInManager, IDialogClient
	{
		#region -- Public Methods & Properties --

		public void LoadPlugins()
		{
			if (_assemblies.Count > 0)
			{
				throw new InvalidProgramException("Plugin Manager should only be initialized once.");
			}

			LoadPluginsInternal();
		}


		internal static bool WindowLayoutSaved { get; private set; }

		#endregion

		#region -- Non-Public Methods --

		// Fix up failures of plugin assemblies to locate Zaber-provided DLLs.
		private static Assembly CurrentDomain_AssemblyResolve(object aSender, ResolveEventArgs aArgs)
		{
			var name = new AssemblyName(aArgs.Name);

			if (name.Name == "ZaberWpfToolbox")
			{
				return typeof(LogControlVM).Assembly;
			}

			if (name.Name == "Zaber")
			{
				return typeof(IZaberPort).Assembly;
			}

			return null;
		}


		// Does initial loading of plugins at program start.
		private void LoadPluginsInternal()
		{
			var settings = Settings.Default;

			// Find all DLLs that might be plugins.
			// This means search the bundled Zaber_plugins directory and the user-selected plugin
			// directory for all DLLs, and find all plugin types and relevant resource types in them.
			_assemblies.Add(new PluginAssemblyInfo(Assembly.GetExecutingAssembly(), null, this));
			_assemblies.Add(new PluginAssemblyInfo(typeof(IZaberPort).Assembly, null, this));
			_assemblies.AddRange(LoadPluginAssemblies());

			// Look for duplicate plugins and select one version of each.
			ResolvePluginCollisions();

			foreach (var asm in _assemblies)
			{
				// Merge in XAML resouces from each plugin assembly.
				foreach (var dict in asm.Resources)
				{
					ApplicationResourceDictionary.MergedDictionaries.Add(dict);
				}

				// Generate a list of all plugin types.
				_pluginTypes.AddRange(asm.PluginTypes);
			}

			// Initialize default output watcher and plugin manager references for the plugins.
			var watcher = new TextWriterWatcher();
			watcher.Written += OutputWritten;
			SetProperty(watcher, typeof(TextWriter));
			SetProperty(this, typeof(IPlugInManager));

			// Initialize plugins from user settings and create always-active plugins.
			PopulatePluginsFromAppSettings();
		}


		// Loads all plugin assemblies at program start.
		private IEnumerable<PluginAssemblyInfo> LoadPluginAssemblies()
		{
			foreach (var assemblyFile in FindPlugInFiles())
			{
				Assembly assembly;
				try
				{
					assembly = Assembly.LoadFrom(assemblyFile);
				}
				catch (Exception ex)
				{
					// If there's a problem with the DLL,
					// don't try to load tabs from it.
					_log.Error("Failed to load assembly " + assemblyFile, ex);

					var sb = new StringBuilder();
					sb.AppendLine("Encountered an error loading " + assemblyFile + ".");
					sb.AppendLine();
					sb.AppendLine();
					sb.AppendLine();

					var di1 = new DirectoryInfo("Zaber_plugins");
					var di2 = new DirectoryInfo(Path.GetDirectoryName(assemblyFile));
					if (di1.FullName == di2.FullName) // Zaber-bundled plugins
					{
						sb.AppendLine(
							"Please see the Application Log for the error details, and contact Zaber support.");
					}
					else
					{
						sb.AppendLine(
							"If this is a custom plugin, please see the Application Log for the complete error message.");
						sb.Append(
							"You can also check the Zaber Console release notes to see if there have been changes ");
						sb.AppendLine(
							"that affect custom plugins: https://www.zaber.com/wiki/Software/Zaber_Console/Release_notes");
					}


					var mbvm = new MessageBoxParams
					{
						Message = sb.ToString(),
						Caption = "Error loading plugins",
						Buttons = MessageBoxButton.OK,
						Icon = MessageBoxImage.Warning
					};

					RequestDialogOpen?.Invoke(this, mbvm);
					continue;
				}

				yield return new PluginAssemblyInfo(assembly, assemblyFile, this);
			}
		}


		private static IEnumerable<string> FindPlugInFiles()
		{
			foreach (var filter in CreatePlugInFilters())
			{
				var files = filter.Filter(filter.PlugInsFolder.GetFiles("*.dll", SearchOption.AllDirectories));

				foreach (var file in files)
				{
					yield return file.FullName;
				}
			}
		}


		private static IEnumerable<PlugInFilter> CreatePlugInFilters()
		{
			var bundledFolder = new DirectoryInfo("Zaber_plugins");
			if (bundledFolder.Exists)
			{
				yield return new PlugInFilter(bundledFolder);
			}

			var plugInsFolderName = Settings.Default.PlugInsFolder;
			if (!string.IsNullOrEmpty(plugInsFolderName))
			{
				var plugInsFolder = new DirectoryInfo(plugInsFolderName);
				if (plugInsFolder.Exists)
				{
					yield return new PlugInFilter(plugInsFolder);
				}
			}
		}


		// Finds possible plugin collisions, selects the best in each case, and warns the user.
		private void ResolvePluginCollisions()
		{
			var pluginsByName = new Dictionary<string, List<PluginTypeInfo>>();

			// Find out how many versions of each plugin we have.
			foreach (var info in _assemblies)
			{
				foreach (var pluginType in info.PluginTypes)
				{
					if (!pluginsByName.ContainsKey(pluginType.QualifiedName))
					{
						pluginsByName.Add(pluginType.QualifiedName, new List<PluginTypeInfo>());
					}

					pluginsByName[pluginType.QualifiedName].Add(pluginType);
				}
			}

			// Select one version of each, and warn user about the collisions.
			var collisions = false;
			var sb = new StringBuilder();
			sb.Append("Warning: Multiple versions of the following plugins were detected. ");
			sb.Append("This means you have multiple copies of the same plugin DLL between the ");
			sb.Append("Zaber Console install location and your custom plugins folder. ");
			sb.AppendLine(
				"You should close Zaber Console and remove all but the latest version of the DLLs listed below.");
			sb.AppendLine();
			sb.Append(
				"The ones marked with an asterisk are the ones Zaber Console has selected to load in this session, ");
			sb.AppendLine("based on version number or file location.");
			sb.AppendLine();

			foreach (var kvp in pluginsByName)
			{
				var name = kvp.Key;
				var versionList = kvp.Value;
				if (versionList.Count > 1) // There were collisions.
				{
					collisions = true;

					// Search for best version.
					var bestVersion = versionList.Max(pi => pi.AssemblyInfo.AssemblyVersion);
					var pluginsWithBestVersion =
						versionList.Where(pi => pi.AssemblyInfo.AssemblyVersion == bestVersion).ToList();

					// If there is a clear winner, pick that.
					PluginTypeInfo winner = null;
					if (1 == pluginsWithBestVersion.Count)
					{
						winner = pluginsWithBestVersion[0];
					}
					else
					{
						// Prefer assemblies we've already selected.
						winner = pluginsWithBestVersion
							 .Where(info => _assemblies.Contains(info.AssemblyInfo))
							 .FirstOrDefault();

						// If there are multiple best version numbers and one of them is in the app dir, select that.
						if (null == winner)
						{
							winner = pluginsWithBestVersion
								 .Where(info => info.AssemblyInfo.AssemblyFilePath.Contains("Zaber_plugins"))
								 .FirstOrDefault();
						}

						// Otherwise just pick the first one found.
						if (null == winner)
						{
							winner = pluginsWithBestVersion.First();
						}
					}

					// Report and remove the non-winning collision victims.
					foreach (var pluginInfo in versionList)
					{
						if (pluginInfo == winner)
						{
							sb.Append("* ");
						}
						else
						{
							_assemblies.Remove(pluginInfo.AssemblyInfo);
						}

						sb.AppendLine($"{pluginInfo.Type.Name} from {pluginInfo.AssemblyInfo.AssemblyFilePath}");
						sb.AppendLine();
					}
				}
			}

			if (collisions)
			{
				var mbvm = new CustomMessageBoxVM(sb.ToString(), "Duplicate plugins found", null, "OK");
				RequestDialogOpen?.Invoke(this, mbvm);
			}
		}


		// Instantiates user-selected or default plugins from all loaded assemblies, at program startup.
		private void PopulatePluginsFromAppSettings()
		{
			Tabs.Clear();
			var pluginTypesByName = _pluginTypes.ToDictionary(pt => pt.QualifiedName);
			var errors = false;

			// Set up the actually displayed tabs in a default order or in the order
			// specified in saved user settings.
			var activePluginNames = Settings.Default.ActivePlugins ?? new StringCollection();

			// Carry over legacy setting.
			if (null != Settings.Default.TabList)
			{
				foreach (var name in Settings.Default.TabList)
				{
					var newName = name;

					// Upgrade old classnames for build-in tabs.
					if (_oldTabToNewTabNameMap.ContainsKey(name))
					{
						newName = _oldTabToNewTabNameMap[name];
					}

					// Remove directory names for external plugins, unless it matters.
					else if ((Path.GetFileName(newName) != newName) && !pluginTypesByName.ContainsKey(newName))
					{
						newName = Path.GetFileName(newName);
					}

					if (!activePluginNames.Contains(name) && !activePluginNames.Contains(newName))
					{
						activePluginNames.Add(newName);
					}
				}

				// Add tabs that were never named in the old Zaber Console settings, but always shown.
				activePluginNames.AddIfUnique(_appAssemblyName + "." + typeof(OptionsVM).Name);
				activePluginNames.AddIfUnique(_appAssemblyName + "." + typeof(HelpTabVM).Name);
				activePluginNames.AddIfUnique(_appAssemblyName + "." + typeof(TerminalPluginVM).Name);

				// Carry over the last selected tab if possible.
				_legacySelectedTabIndex = Settings.Default.MainTabSelectedIndex;
			}

			// Invalidate legacy settings.
			Settings.Default.TabList = null;
			Settings.Default.MainTabSelectedIndex = -1;

			if (0 == activePluginNames.Count)
			{
				activePluginNames.AddRange(GetDefaultPluginList().ToArray());
			}

			// Add entries for plugin types that are always active, if they're not already in the list.
			var alwaysActivePluginTypes = _pluginTypes.Where(pt => pt.AlwaysActive);
			foreach (var pt in alwaysActivePluginTypes)
			{
				if (!activePluginNames.Contains(pt.QualifiedName))
				{
					activePluginNames.Add(pt.QualifiedName);
				}
			}

			// Attempt to instantiate the listed plugins. 
			var failed = new StringCollection();
			Settings.Default.ActivePlugins = new StringCollection();
			foreach (var name in activePluginNames)
			{
				PluginTypeInfo typeInfo = null;

				var fileName = name;
				var asmName = fileName.Substring(0, fileName.LastIndexOf(".")) ?? "<unknown>";
				var pluginName = fileName.Substring(fileName.LastIndexOf(".") + 1) ?? "<unknown>";

				if (pluginTypesByName.TryGetValue(fileName, out typeInfo))
				{
					var instance = _pluginInstances.Where(pi => typeInfo.Type.IsAssignableFrom(pi.TypeInfo.Type))
												.FirstOrDefault();
					if (null == instance)
					{
						instance = InstantiatePlugin(typeInfo);

						if (null != instance)
						{
							// If we created a log window, adopt it for log output.
							if (typeof(LogControlVM).IsAssignableFrom(typeInfo.Type))
							{
								LogControl = instance.Instance as LogControlVM;
							}
						}
						else
						{
							errors |= !_deprecatedTabs.Contains(pluginName);
							failed.Add(name);
							var msg =
								$"Your previous session included a plugin named {pluginName} from assembly {asmName}. "
							+ "This plugin type was found for there was an error initialzing it; other messages in this log may indicate why.";
							_log.Error(msg);
						}
					}

					if ((null != instance) && (null != instance.Tab))
					{
						Tabs.Add(instance.Tab);
						Settings.Default.ActivePlugins.AddIfUnique(typeInfo.QualifiedName);
					}
				}
				else
				{
					errors |= !_deprecatedTabs.Contains(pluginName);
					failed.Add(name);
					var msg = $"Your previous session included a plugin named {pluginName} from assembly {asmName}. "
						  + "This plugin type was not found in any of the currently available plugin assemblies; "
						  + "have you changed your plugin folder or deleted plugins?";
					_log.Error(msg);
				}
			}

			// Remove plugins that failed to load from the user settings list.
			foreach (var name in failed)
			{
				activePluginNames.Remove(name);
			}

			// Remove any plugins that are active but shouldn't be.
			var extraInstances = new List<PluginInstance>();
			foreach (var instance in _pluginInstances)
			{
				if (!activePluginNames.Contains(instance.TypeInfo.QualifiedName))
				{
					if (!instance.TypeInfo.AlwaysActive)
					{
						extraInstances.Add(instance);
					}
				}
			}

			foreach (var instance in extraInstances)
			{
				DeinstantiatePlugin(instance);
			}

			if (errors)
			{
				var mbvm = new MessageBoxParams
				{
					Message = "There were errors while loading plugins; some plugin types were not instantiated. "
						  + " Check the Application Log for more information.",
					Caption = "Error Loading Plugins",
					Buttons = MessageBoxButton.OK,
					Icon = MessageBoxImage.Exclamation
				};

				Dispatcher.CurrentDispatcher.BeginInvoke(new Action(() => { RequestDialogOpen?.Invoke(this, mbvm); }));
			}
		}


		// Instantiates a plugin, sets up events and properties and adds to the active plugin lists.
		private PluginInstance InstantiatePlugin(PluginTypeInfo aType)
		{
			PluginInstance instance = null;

			try
			{
				instance = aType.Instantiate();
			}
			catch (Exception ex)
			{
				var msg =
					$"Failed to create plugin {aType.Type.Name} from assembly {aType.AssemblyInfo.AssemblyFilePath}.";
				_log.Error(msg, ex);
			}

			if (null != instance)
			{
				foreach (var pair in _lastProperties)
				{
					instance.SetProperty(pair.Value, pair.Key.Item1, pair.Key.Item2);
				}

				instance.RequestDialogOpen += ForwardDialogOpenRequest;
				instance.RequestDialogClose += ForwardDialogCloseRequest;

				instance.InvokeMethod(PlugInMethodAttribute.EventType.PluginActivated);

				_pluginInstances.Add(instance);

				if (null == Settings.Default.ActivePlugins)
				{
					Settings.Default.ActivePlugins = new StringCollection();
				}

				Settings.Default.ActivePlugins.AddIfUnique(instance.TypeInfo.QualifiedName);
			}

			return instance;
		}


		// Deinstantiates a plugin if allowed, removes tabs and instance from appropriate lists,
		// and fires appropriate events. Disposes the plugin if it is IDisposable.
		private void DeinstantiatePlugin(PluginInstance aInstance)
		{
			if (aInstance.TypeInfo.AlwaysActive)
			{
				return;
			}

			if (null != aInstance.Tab)
			{
				aInstance.Tab.IsVisible = false;

				var index = Tabs.IndexOf(aInstance.Tab);
				if (index >= 0)
				{
					Tabs.Remove(aInstance.Tab);
					if (aInstance.Tab == ActiveTab)
					{
						if (index < Tabs.Count)
						{
							ActiveTab = Tabs[index];
						}
						else
						{
							ActiveTab = Tabs.LastOrDefault();
						}
					}
				}
			}

			aInstance.InvokeMethod(PlugInMethodAttribute.EventType.PluginDeactivated);
			_pluginInstances.Remove(aInstance);
			Settings.Default.ActivePlugins.Remove(aInstance.TypeInfo.QualifiedName);
			if (aInstance.Instance is IDisposable)
			{
				((IDisposable) aInstance.Instance).Dispose();
			}
		}


		private IEnumerable<string> GetDefaultPluginList()
		{
			var result = new List<string>();

			var pluginType = _pluginTypes.Where(pt => pt.DisplayName.Contains("Simple")).FirstOrDefault();
			if (null != pluginType)
			{
				result.Add(pluginType.QualifiedName);
			}

			// Set up defaults.
			// Force the order of the tabs on first load.
			result.Add(_appAssemblyName + "." + typeof(CommandListVM).Name);
			result.Add(_appAssemblyName + "." + typeof(SettingListVM).Name);
			result.Add(_appAssemblyName + "." + typeof(ScriptListVM).Name);
			result.Add(_appAssemblyName + "." + typeof(OptionsVM).Name);
			result.Add(_appAssemblyName + "." + typeof(HelpTabVM).Name);
			result.Add(_appAssemblyName + "." + typeof(TerminalPluginVM).Name);

			return result;
		}


		// Callback from the docking manager via the WindowLayoutSerializer to load the previous
		// window layout. The layout is serialized in an XML file in the user's appdata directory.
		// We control the location of the file but not its content.
		private void LoadWindowLayout(WindowLayoutSerializer.DeserializeDelegate aCallback)
		{
			// Load the file if it exists.
			string layoutText = null;
			if (File.Exists(LAYOUT_FILE))
			{
				layoutText = File.ReadAllText(LAYOUT_FILE);
			}

			// Use the serialized layout if present; else stick with defaults set up by LoadPlugins().
			if (!string.IsNullOrEmpty(layoutText))
			{
				LoadPreviousLayout(aCallback, layoutText);
			}
			else if (_legacySelectedTabIndex.HasValue) // Carry over last selected tab index if upgrading settings.
			{
				var lastTab = _legacySelectedTabIndex.Value;
				if ((lastTab > 0) && (lastTab < Tabs.Count))
				{
					ActiveTab = Tabs[lastTab];
				}
			}

			// Update the Show context menu content.
			OnPropertyChanged(nameof(ShowableTabs));
		}


		private void LoadPreviousLayout(WindowLayoutSerializer.DeserializeDelegate aCallback, string layoutText)
		{
			PluginTabVM lastSelectedMainTab = null;
			LayoutContent lastSelectedContent = null;

			var requiredTypes = _pluginTypes.Where(pt => pt.AlwaysActive && pt.HasUI).ToList();

			// There was a previous window layout in the file. We pass this to the WindowLayoutSerializer
			// using the callback it provided, and it will in turn invoke the callback code below to
			// resolve serialized tab identifiers into the correct tab VM instances.
			try
			{
				var uniquePlugins = new HashSet<object>();

				aCallback?.Invoke(layoutText,
								  (aSender, aArgs) =>
								  {
									  var selectedTab = FindTabContentBySerializedId(aArgs, uniquePlugins, requiredTypes, out var selectedContent);
									  if ((null != selectedTab) && (null != selectedContent))
									  {
										  lastSelectedMainTab = selectedTab;
										  lastSelectedContent = selectedContent;
									  }
								  });
			}
			catch (Exception aException)
			{
				_log.Warn("There was an error while loading the previous window layout.", aException);
			}

			// NOTE: Programmatically altering the content of Tabs after this point can cause 
			// floating windows to disappear or dock themselves.
			// Removing tabs is OK, and adding tabs to the end of the list might be safe.

			// Verify that all the required types were loaded, and reset the layout if not.
			if (requiredTypes.Count > 0)
			{
				PopulatePluginsFromAppSettings();
			}

			if ((null != lastSelectedMainTab) && Tabs.Contains(lastSelectedMainTab))
			{
				// Secondary hack to ensure the last selected tab remains the selected
				// tab and in its correct position on window load with no floating windows:
				// Set the selected tab in a timer callback after everything else is done.
				// I hate this.
				var timer = new DispatcherTimer();
				timer.Interval = new TimeSpan(0, 0, 0, 0, 100);
				timer.Tick += (aSender, aArgs) =>
				{
					timer.Stop();
					ActiveTab = lastSelectedMainTab;
					lastSelectedContent.IsSelected = true;
				};
				timer.Start();
			}
		}


		private PluginTabVM FindTabContentBySerializedId(LayoutSerializationCallbackEventArgs aArgs, HashSet<object> aUniquePlugins,
														 List<PluginTypeInfo> aRequiredTypes, out LayoutContent aLastSelectedContent)
		{
			PluginTabVM lastSelectedMainTab = null;
			var uniqueId = aArgs.Model.ContentId;
			object viewModel = null;
			aLastSelectedContent = null;

			// Try to find an existing plugin instance with matching UI identifier.
			var instance = _pluginInstances
					   .Where(pi => (null != pi.Tab) && (pi.Tab.UniqueIdentifier == uniqueId))
					   .FirstOrDefault();

			// If none found, infer the type name from the identifier, try to find an existing instance of that type.
			var typeName = uniqueId;
			if (null == instance)
			{
				instance = _pluginInstances
					   .Where(pi => pi.TypeInfo.QualifiedName == uniqueId)
					   .FirstOrDefault();
			}

			// If still no match, try to find a matching plugin type and instantiate it.
			PluginTabVM tab = null;
			if (null == instance)
			{
				var type = _pluginTypes
					   .Where(pt => pt.QualifiedName == typeName)
					   .FirstOrDefault();
				if (null != type)
				{
					instance = InstantiatePlugin(type);
				}
			}

			if (null != instance)
			{
				tab = instance.Tab;
			}

			if ((null != tab) && !Tabs.Contains(tab))
			{
				Tabs.Add(tab);
			}

			viewModel = tab;

			if (viewModel == null)
			{
				aArgs.Cancel = true;
			}
			else if (!aUniquePlugins.Add(viewModel))
			{
				aArgs.Cancel = true;
				_log.Warn($"There was a duplicate instance of {instance.TypeInfo}.");
			}
			else
			{
				aArgs.Content = viewModel;

				if (aArgs.Model.IsSelected
				&& !aArgs.Model.IsFloating
				&& (-1 == aArgs.Model.PreviousContainerIndex))
				{
					// Hack to prevent AvalonDock from moving the last selected tab in the main
					// docking area to the leftmost position. Selected status should not alter
					// tab order.
					lastSelectedMainTab = tab;
					aLastSelectedContent = aArgs.Model;
					aArgs.Model.IsSelected = false;
				}

				// Hack to clear maximized state from non-floating tabs, else if they're undocked
				// they will spontaneously maximize at next program start.
				if (!aArgs.Model.IsFloating)
				{
					aArgs.Model.IsMaximized = false;
				}
			}

			if (null != instance)
			{
				aRequiredTypes.Remove(instance.TypeInfo);
			}

			return lastSelectedMainTab;
		}


		private void SaveWindowLayout(string aSerializedData)
		{
			// Window layout serialization happens after user settings are saved, so
			// we have to manually save them in a different file. This has the advantage
			// of making it easy to reset the settings by deleting the file.
			if (File.Exists(LAYOUT_FILE))
			{
				File.Delete(LAYOUT_FILE);
			}

			if (!string.IsNullOrEmpty(aSerializedData))
			{
				File.WriteAllText(LAYOUT_FILE, aSerializedData);
			}

			WindowLayoutSaved = true;
		}

		#endregion

		#region -- Data --

		// Use the name of the current assembly, not necessarily just
		// "ZaberConsole": Could also be "ZaberConsoleTest" or 
		// "ZaberConsoleDev".
		private static readonly string _appAssemblyName = Assembly.GetExecutingAssembly().GetName().Name;

		// Map old tab names to new qualified plugin type names, for upgrade purposes.
		private static readonly Dictionary<string, string> _oldTabToNewTabNameMap = new Dictionary<string, string>
		{
			#if TEST
			{ "ZaberConsoleInstallTest.CommandGrid", _appAssemblyName + "." + typeof(CommandListVM).Name },
			{ "ZaberConsoleInstallTest.SettingGrid", _appAssemblyName + "." + typeof(SettingListVM).Name },
			{ "ZaberConsoleInstallTest.ScriptGrid", _appAssemblyName + "." + typeof(Scripts.ScriptListVM).Name },
			{ "ZaberConsoleInstallTest.MessageLogPluginVM", _appAssemblyName + "." + typeof(TerminalPluginVM).Name },
			#endif
			#if DEV
			{ "ZaberConsoleDev.CommandGrid", _appAssemblyName + "." + typeof(CommandListVM).Name },
			{ "ZaberConsoleDev.SettingGrid", _appAssemblyName + "." + typeof(SettingListVM).Name },
			{ "ZaberConsoleDev.ScriptGrid", _appAssemblyName + "." + typeof(Scripts.ScriptListVM).Name },
			{ "ZaberConsoleDev.MessageLogPluginVM", _appAssemblyName + "." + typeof(TerminalPluginVM).Name },
			#endif
			{ "ZaberConsole.CommandGrid", _appAssemblyName + "." + typeof(CommandListVM).Name },
			{ "ZaberConsole.SettingGrid", _appAssemblyName + "." + typeof(SettingListVM).Name },
			{ "ZaberConsole.ScriptGrid", _appAssemblyName + "." + typeof(ScriptListVM).Name },
			{ "ZaberConsole.MessageLogPluginVM", _appAssemblyName + "." + typeof(TerminalPluginVM).Name }
		};


		// List of deprecated plugin names for which a failed to load dialog should not appear.
		private static readonly List<string> _deprecatedTabs = new List<string>
		{
			"AdvancedCommandTabVM",
			"MessageLogPluginVM"
		};

		#endregion
	}
}

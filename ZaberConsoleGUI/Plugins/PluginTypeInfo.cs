﻿using System;
using System.Windows.Forms;
using Zaber.PlugIns;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins
{
	// Container for information about one plugin class type.
	public class PluginTypeInfo
	{
		#region -- Public Methods & Properties --

		public PluginTypeInfo(PluginAssemblyInfo aAssembly, Type aType, PlugInManager aManager)
		{
			Manager = aManager;
			AssemblyInfo = aAssembly;
			Type = aType;

			var plugInAttribute = (PlugInAttribute) aType.GetCustomAttributes(typeof(PlugInAttribute), true)[0];
			DisplayName = plugInAttribute.Name ?? aType.Name;
			AlwaysActive = plugInAttribute.AlwaysActive;
			Dockable = plugInAttribute.Dockable;
			Description = plugInAttribute.Description;
			QualifiedName = aAssembly.Assembly.GetName().Name + "." + aType.Name;
		}


		// Create an instance of this plugin. Does not prevent creating multiple instances!
		public PluginInstance Instantiate() => new PluginInstance(Manager, this);


		public override string ToString() => QualifiedName;


		// Reference to the plugin manager for forwarding events.
		public PlugInManager Manager { get; }

		// Information about the assembly this type came from.
		public PluginAssemblyInfo AssemblyInfo { get; }

		// The class type of the plugin.
		public Type Type { get; }

		// The type name used in user settings plugin lists.
		public string QualifiedName { get; }

		// If true, at least one instance of this type must always be active.
		public bool AlwaysActive { get; }

		// If true, the user interface for this type can be torn off and docked.
		public bool Dockable { get; }

		// The user-readable name of this plugin.
		public string DisplayName { get; }

		// The optional user-readable description of this plugin.
		public string Description { get; }

		// True if this plugin type is recognized as having a user interface.
		public bool HasUI => typeof(PluginTabVM).IsAssignableFrom(Type)
						 || typeof(ObservableObject).IsAssignableFrom(Type)
						 || typeof(Control).IsAssignableFrom(Type);

		#endregion
	}
}

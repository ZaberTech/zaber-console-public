﻿using System;
using System.Windows.Forms;

namespace ZaberConsole.Plugins
{
	public class WinFormsHostTabVM : PluginTabVM
	{
		#region -- Public Methods & Properties --

		public WinFormsHostTabVM(Control aControl)
		{
			if (null == aControl)
			{
				throw new ArgumentException("Plugin is not a Windows Forms Control.");
			}

			Control = aControl;

			// Hack to force correct resizing.
			Control.Font = Control.Font;
		}


		public Control Control { get; }

		#endregion
	}
}

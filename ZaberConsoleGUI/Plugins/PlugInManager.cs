﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using log4net;
using MvvmDialogs.ViewModels;
using Zaber;
using Zaber.PlugIns;
using Zaber.Telemetry;
using ZaberConsole.Behaviors;
using ZaberConsole.Dialogs;
using ZaberConsole.Properties;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Dialogs;
using ZaberWpfToolbox.Logging;

namespace ZaberConsole.Plugins
{
	public partial class PlugInManager : ObservableObject, IPlugInManager, IDialogClient
	{
		#region -- Initialization and context --

		public delegate void StatusMessageHandler(object aSender, string aMessage, int aDuration);

		/// <summary>
		///     Event invoked when a plugin wants to log a status message.
		/// </summary>
		public event StatusMessageHandler StatusMessageReceived;


		public PlugInManager()
		{
			AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
		}


		public LogControlVM LogControl { get; set; }


		/// <summary>
		///     The entry point of the application must set this to the top- or high-level Resource
		///     Dictionary that plugin-supplied Resource Dictionaries should be added to.
		/// </summary>
		public static ResourceDictionary ApplicationResourceDictionary { get; internal set; }


		public ZaberPortFacade PortFacade
		{
			get => _portFacade;
			set
			{
				if (null != _portFacade)
				{
					_portFacade.SelectedConversationChanged -= PortFacade_SelectedConversationChanged;
					_portFacade.Opened -= PortFacade_SelectedConversationChanged;
					_portFacade.Closed -= PortFacade_SelectedConversationChanged;
				}

				_portFacade = value;

				// Pass new port facade and port to all active plugins.
				SetProperty(value, typeof(ZaberPortFacade));
				SetProperty(value?.Port, typeof(IZaberPort));

				if (null != _portFacade)
				{
					_portFacade.SelectedConversationChanged += PortFacade_SelectedConversationChanged;
					_portFacade.Opened += PortFacade_SelectedConversationChanged;
					_portFacade.Closed += PortFacade_SelectedConversationChanged;
				}
			}
		}

		#endregion

		#region -- Public API --

		public void NotifyWindowClosing()
		{
			// Don't use DeinstantiatePlugin() and don't clear tabs else window layout 
			// serialization will produce incorrect output.
			foreach (var instance in _pluginInstances)
			{
				if (null != instance.Tab)
				{
					instance.Tab.IsVisible = false;
				}
			}

			InvokeMethod(PlugInMethodAttribute.EventType.PluginDeactivated);

			foreach (var instance in _pluginInstances)
			{
				var id = instance.Instance as IDisposable;
				id?.Dispose();

				if ((null != instance.Tab) && !ReferenceEquals(instance.Instance, instance.Tab))
				{
					var id2 = instance.Tab as IDisposable;
					id2?.Dispose();
				}
			}

			_pluginInstances.Clear();
		}


		// Displays the plugin options 
		// where users can enable, disable, show and hide
		// plugins, and then applies the changes to the active plugin list.
		public void ShowOptionsDialog()
		{
			// Note this currently handles at most one instance per type.
			var instancesByType = _pluginInstances.ToDictionary(pi => pi.TypeInfo);
			foreach (var type in _pluginTypes)
			{
				if (!instancesByType.ContainsKey(type))
				{
					instancesByType.Add(type, null);
				}
			}

			// Populate a list of possible instances, ordered by type.
			var types = new List<PluginTypeInfo>(_pluginTypes);
			types.Sort((a, b) => a.DisplayName.CompareTo(b.DisplayName));

			var dialogVM = new PluginOptionsDialogVM();
			foreach (var type in types)
			{
				var instance = instancesByType[type];
				var options = new PluginInstanceOptionsVM(type, Tabs.Contains(instance?.Tab));
				dialogVM.Plugins.Add(options);
			}

			// Show the dialog.
			dialogVM.SelectedPlugin = dialogVM.Plugins.FirstOrDefault();
			RequestDialogOpen?.Invoke(this, dialogVM);
			if (!dialogVM.Result)
			{
				return;
			}

			// Apply changes made by the user in the dialog.
			_pluginInstances = new List<PluginInstance>();
			Settings.Default.ActivePlugins.Clear();

			foreach (var options in dialogVM.Plugins)
			{
				var pluginType = options.PluginType;
				instancesByType.TryGetValue(pluginType, out var instance);

				// If this plugin type is supposed to be inactive, shut down any existing instance.
				if (!options.Visible && !pluginType.AlwaysActive)
				{
					if (null != instance)
					{
						DeinstantiatePlugin(instance);
					}
				}
				else
				{
					// If this plugin type is supposed to be active, make sure it is and add it to the new list.
					if (instance is null)
					{
						instance = InstantiatePlugin(pluginType);
					}
					else
					{
						_pluginInstances.Add(instance);
						Settings.Default.ActivePlugins.AddIfUnique(pluginType.QualifiedName);
					}

					if (null != instance)
					{
						// Ensure tab visiblity matches user selection.
						if (null != instance.Tab)
						{
							if (options.Visible | (pluginType.HasUI && pluginType.AlwaysActive))
							{
								if (!Tabs.Contains(instance.Tab))
								{
									Tabs.Add(instance.Tab);
								}
							}
							else
							{
								Tabs.Remove(instance.Tab);
							}
						}
					}
				}
			}

			OnPropertyChanged(nameof(ShowableTabs));
		}


		/// <summary>
		///     Invoke the <ref>SettingsResetEvent</ref>.
		/// </summary>
		/// <param name="aInvoker">Optional; object invoking the event. Defaults to the event manager.</param>
		/// <param name="aArgs">Optional event arguments. Defaults to null.</param>
		public void FireSettingsResetEvent(object aInvoker = null, EventArgs aArgs = null)
			=> SettingsResetEvent?.Invoke(aInvoker ?? this, aArgs);


		// Deactivate a plugin and hide its tab.
		internal void Deactivate(PluginInstance aPlugin)
		{
			if ((null != aPlugin.Tab) && aPlugin.Tab.CanClose)
			{
				Tabs.Remove(aPlugin.Tab);
				OnPropertyChanged(nameof(ShowableTabs));
				_eventLog.LogEvent("Tab closed", aPlugin.TypeInfo.DisplayName);
			}

			DeinstantiatePlugin(aPlugin);
		}


		/// <summary>
		///     Show the tab for a plugin type, activating the plugin if necessary.
		/// </summary>
		/// <param name="aType">Type of plugin to activate.</param>
		/// <param name="aFocus">Focus the tab after activating it. Defaults to true.</param>
		internal void ShowPluginType(PluginTypeInfo aType, bool aFocus = true)
		{
			if (aType is null)
			{
				return;
			}

			// If there is already an active tab, show it.
			var existing = Tabs.Where(t => aType.Type == t.Plugin.TypeInfo.Type).FirstOrDefault();
			if (null != existing)
			{
				if (aFocus)
				{
					ActiveTab = existing;
				}

				return;
			}

			// If there is an instance we can show, do that.
			PluginTabVM tab = null;
			var instance = _pluginInstances.Where(pi => aType == pi.TypeInfo).FirstOrDefault();
			if (null != instance)
			{
				tab = instance.Tab;
			}
			else if (aType.HasUI)
			{
				instance = InstantiatePlugin(aType);
				if (null != instance)
				{
					tab = instance.Tab;
					_eventLog.LogEvent("Tab opened", tab.TabName);
				}
			}

			if (null != tab)
			{
				Tabs.Add(tab);
				if (aFocus)
				{
					ActiveTab = tab;
				}

				OnPropertyChanged(nameof(ShowableTabs));
			}
		}


		// Find active plugins of a given type.
		public IEnumerable<T> FindActivePlugins<T>()
			=> _pluginInstances.Where(pi => typeof(T).IsAssignableFrom(pi.TypeInfo.Type)).Select(pi => (T) pi.Instance);

		#endregion

		#region -- View-Bound Properties --

		public ObservableCollection<PluginTabVM> Tabs
		{
			get => _tabs;
			set => Set(ref _tabs, value, nameof(Tabs));
		}


		// Setting this will force the given tab to become the active tab.
		public PluginTabVM ActiveTab
		{
			get => _activeTab;
			set
			{
				if (!ReferenceEquals(_activeTab, value))
				{
					_activeTab = value;
					OnPropertyChanged(nameof(ActiveTab));
					_eventLog.LogEvent("Tab activated", value is null ? "<none>" : value.TabName);
				}
			}
		}


		// List of tabs that are currently not visible but can be shown.
		// Used to populate the docking manager's "show..." menu.
		public IEnumerable<PluginTypeInfo> ShowableTabs
		{
			get
			{
				var instancesByType = _pluginInstances.ToDictionary(pi => pi.TypeInfo);
				foreach (var type in _pluginTypes)
				{
					if (!instancesByType.ContainsKey(type))
					{
						instancesByType.Add(type, null);
					}
				}

				var result = new List<PluginTypeInfo>();

				foreach (var type in _pluginTypes)
				{
					var showable = false;

					if (type.HasUI)
					{
						var instance = instancesByType[type];
						showable = (null == instance) || !Tabs.Contains(instance.Tab);
					}

					if (showable)
					{
						result.Add(type);
					}
				}

				return result;
			}
		}


		// Used by the docking manager to load the previous window layout. Called on control load.
		public ICommand LoadLayoutCommand
			=> new RelayCommand(aCallback => LoadWindowLayout(aCallback as WindowLayoutSerializer.DeserializeDelegate));


		// Used by the docking manager to save the previous window layout. Called on control unload.
		public ICommand SaveLayoutCommand
			=> new RelayCommand(aSerializedData => SaveWindowLayout(aSerializedData as string));


		// Used by the docking manager's "Show..." context menu.
		public ICommand ShowPluginTypeCommand
			=> new RelayCommand(aPluginType => ShowPluginType(aPluginType as PluginTypeInfo));

		#endregion

		#region -- Functionality --

		private void OutputWritten(object aSender, TextEventArgs aArgs)
			=> LogControl?.Append(aArgs.Text);


		// Pass conversation selection changes to all active plugins.
		private void PortFacade_SelectedConversationChanged(object sender, EventArgs e)
		{
			SetProperty(PortFacade.SelectedConversation, typeof(Conversation));
			SetProperty(PortFacade.SelectedConversation?.Device, typeof(ZaberDevice));
		}

		#endregion

		#region -- IPlugInManager Members --

		/// <inheritdoc cref="IPlugInManager.SetProperty(object, Type)" />
		/// />
		public void SetProperty(object aValue, Type aType)
			=> SetProperty(aValue, aType, null);


		/// <inheritdoc cref="IPlugInManager.SetProperty(object, Type, string)" />
		/// />
		public void SetProperty(object aValue, Type aType, string aName)
		{
			foreach (var plugin in _pluginInstances)
			{
				plugin.SetProperty(aValue, aType, aName);
			}

			_lastProperties[new Tuple<Type, string>(aType, aName)] = aValue;
		}


		/// <inheritdoc cref="IPlugInManager.InvokeMethod(PlugInMethodAttribute.EventType)" />
		/// />
		public void InvokeMethod(PlugInMethodAttribute.EventType aEventType)
		{
			foreach (var plugin in _pluginInstances)
			{
				plugin.InvokeMethod(aEventType);
			}
		}


		/// <inheritdoc cref="IPlugInManager.ShowPluginTab(Type, bool)" />
		/// />
		public void ShowPluginTab(Type aPluginType, bool aFocus = true)
		{
			var info = _pluginTypes.Where(i => aPluginType == i.Type).FirstOrDefault();
			ShowPluginType(info, aFocus);
		}


		/// <inheritdoc cref="IPlugInManager.ShowPluginTab(string, bool)" />
		/// />
		public void ShowPluginTab(string aPluginName, bool aFocus = true)
		{
			var info = _pluginTypes.Where(i => aPluginName == i.DisplayName).FirstOrDefault();
			ShowPluginType(info, aFocus);
		}


		/// <inheritdoc cref="IPlugInManager.GetActivePluginTypes" />
		/// />
		public IEnumerable<Type> GetActivePluginTypes()
		{
			var result = new HashSet<Type>();

			foreach (var pi in _pluginInstances)
			{
				if (null != pi.Tab)
				{
					result.Add(pi.TypeInfo.Type);
				}
			}

			return result;
		}


		/// <inheritdoc cref="IPlugInManager.GetVisiblePluginTypes" />
		/// />
		public IEnumerable<Type> GetVisiblePluginTypes()
		{
			var result = new HashSet<Type>();

			foreach (var pi in _pluginInstances)
			{
				if ((null != pi.Tab) && pi.Tab.IsVisible)
				{
					result.Add(pi.TypeInfo.Type);
				}
			}

			return result;
		}


		/// <summary>
		///     Scan all known plugins for instantiable classes that are assignable to
		///     the given type. Normally used to find plugged-in implementations of an
		///     interface. This also scans the main program assemblies.
		/// </summary>
		/// <param name="aInterfaceType">Type to find implementations of.</param>
		/// <returns>
		///     Zero or more types that are instantiable and are assignable
		///     to the specified type. Each distinct type is guaranteed to appear only
		///     once. Ordering is not specified.
		/// </returns>
		public IEnumerable<Type> FindConcreteImplementationsOf(Type aInterfaceType)
		{
			var types = new HashSet<Type>();

			foreach (var asm in _assemblies)
			{
				foreach (var t in asm.Assembly.GetExportedTypes())
				{
					if (!t.IsAbstract && aInterfaceType.IsAssignableFrom(t))
					{
						types.Add(t);
					}
				}
			}

			return types;
		}


		/// <inheritdoc cref="IPlugInManager.SettingsResetEvent" />
		/// />
		public event EventHandler SettingsResetEvent;


		/// <inheritdoc cref="IPlugInManager.ShowStatusText(object, string, int)" />
		/// />
		public void ShowStatusText(object aOwner, string aMessage, int aDuration = 0)
			=> StatusMessageReceived?.Invoke(aOwner, aMessage, aDuration);

		#endregion

		#region --  IDialogClient implementation --

		public event RequestDialogChange RequestDialogOpen;

		public event RequestDialogChange RequestDialogClose;

		#endregion

		#region -- Child IDialogClient handling --

		private void ForwardDialogOpenRequest(object aSender, IDialogViewModel aVM)
			=> RequestDialogOpen?.Invoke(aSender, aVM);


		private void ForwardDialogCloseRequest(object aSender, IDialogViewModel aVM)
			=> RequestDialogClose?.Invoke(aSender, aVM);

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Plugin manager");

		#if DEV || TEST
		private static readonly string LAYOUT_FILE = Path.Combine(
										Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
										@"Zaber Technologies",
										@"Zaber Console",
										@"AvalonWindowLayout_DEV.xml");
		#else
		private static readonly string LAYOUT_FILE = Path.Combine(
			Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
			@"Zaber Technologies",
			@"Zaber Console",
			@"AvalonWindowLayout.xml");
		#endif

		private ZaberPortFacade _portFacade;
		private ObservableCollection<PluginTabVM> _tabs = new ObservableCollection<PluginTabVM>();
		private PluginTabVM _activeTab;
		private int? _legacySelectedTabIndex;

		private readonly List<PluginAssemblyInfo> _assemblies = new List<PluginAssemblyInfo>();
		private readonly List<PluginTypeInfo> _pluginTypes = new List<PluginTypeInfo>();
		private List<PluginInstance> _pluginInstances = new List<PluginInstance>();

		private readonly Dictionary<Tuple<Type, string>, object> _lastProperties =
			new Dictionary<Tuple<Type, string>, object>();

		#endregion
	}
}

﻿using System.Windows.Input;
using Zaber.PlugIns;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins
{
	// Base class for plugin UI tabs. Plugins can inherit from this. If they do not, they
	// will be wrapped in a subclass of this.
	public abstract class PluginTabVM : TabBaseVM
	{
		#region -- Public Methods & Properties --

		// Plugin manager tracking information for the plugin that owns this tab.
		public PluginInstance Plugin { get; set; }

		// Unique identifier for the tab instance. Used by the plugin manager.
		public string UniqueIdentifier => Plugin.TypeInfo.QualifiedName;


		// True if it is possible to close this tab.
		public bool CanClose => !Plugin.TypeInfo.AlwaysActive;


		// True if this tab can be undocked from the main window.
		public bool CanFloat => Plugin.TypeInfo.Dockable;


		// Command invoked by the context menu deactivate option.
		public ICommand DeactivateCommand => new RelayCommand(_ => { Plugin.Deactivate(); },
															  _ => CanClose);


		// Control visibility information from the docking manager.
		public bool IsVisible
		{
			get => _visible;
			set
			{
				if (value != _visible)
				{
					if (value)
					{
						Plugin.InvokeMethod(PlugInMethodAttribute.EventType.PluginShown);
					}
					else
					{
						Plugin.InvokeMethod(PlugInMethodAttribute.EventType.PluginHidden);
					}

					_visible = value;
					OnPropertyChanged(nameof(IsVisible));
				}
			}
		}

		#endregion

		#region -- Data --

		private bool _visible;

		#endregion
	}
}

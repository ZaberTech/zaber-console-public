﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows.Input;
using log4net;
using MvvmDialogs.ViewModels;
using Zaber;
using Zaber.PlugIns;
using Zaber.Telemetry;
using Zaber.Threading;
using ZaberConsole.Plugins.CommandList;
using ZaberConsole.Plugins.Joystick.Key;
using ZaberConsole.Plugins.Joystick.Stick;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Dialogs;
using Timer = System.Timers.Timer;

namespace ZaberConsole.Plugins.Joystick
{
	/// <summary>
	///     The main class that does all of the top level logic for the plugin.
	///     This class queries the joystick on startup, has all of the underlying commands,
	///     and changes the views of the plug-in when needed. This class handles the port as well.
	/// </summary>
	[PlugIn(Name = "Joystick", Description = "Joystick configuration helper.")]
	public class JoystickPluginVM : ObservableObject, IDialogClient
	{
		#region -- Public Methods & Properties --

		public JoystickPluginVM()
		{
			//WPF data binding errors slow down zaber console, we want to avoid those.
			//Initially the plugin is bound to these dummy objects.
			Joysticks.Add(new JoystickVM(null, Protocol.None));
			Joysticks.Add(new JoystickVM(null, Protocol.None));
			Joysticks.Add(new JoystickVM(null, Protocol.None));
			Joysticks.Add(new JoystickVM(null, Protocol.None));
		}

		#endregion

		#region -- Setup --

		/// <summary>
		///     The port facade for this class is set through the <see cref="PlugInManager" />.
		/// </summary>
		[PlugInProperty]
		public ZaberPortFacade PortFacade
		{
			get => _portFacade;
			set
			{
				if (null != _portFacade)
				{
					_portFacade.Closed -= OnPortFacadeClosed;
					_portFacade.Opened -= OnPortFacadeOpened;
				}

				_portFacade = value;

				if (null != _portFacade)
				{
					_portFacade.Closed += OnPortFacadeClosed;
					_portFacade.Opened += OnPortFacadeOpened;
				}
			}
		}

		/// <summary>
		///     One way the plugin is initialized is by selecting a joystick conversation through
		///     the top left window.
		/// </summary>
		[PlugInProperty]
		public Conversation Conversation
		{
			get => _conversation;
			set
			{
				//Conversation gets set to null when the port is closed.
				if (null == value)
				{
					_conversation = null;
					return;
				}

				_dispatcher.BeginInvoke(() =>
				{
					//Make sure to refresh the plugin first when switching conversations
					ClosePlugin();

					if (!(value.Device.DeviceType.IsJoystick && value.Device.IsSingleDevice)
					&& (null != _conversation))
					{
						DisplayInformation = UNSELECTED_MSG;
					}

					_conversation = value;

					//Check if an X-JOY or a T-JOY is a selected conversation and if the tab is currently selected.
					if (value.Device.DeviceType.IsJoystick && value.Device.IsSingleDevice && _isPluginSelected)
					{
						_originalConversationTimeout = Conversation.Timeout;
						Conversation.Timeout = 1000;
						DisplayInformation = "Click on a key or the joystick button in the left panel.";

						//Initialize everything.
						if (_protocol == Protocol.ASCII)
						{
							InitializePluginAscii();
						}
						else
						{
							InitializePluginBinary();
						}
					}

					UpdateVisibility();
				});
			}
		}


		/// <summary>
		///     Populates the binary command list and designates what the protocol version
		///     the plugin should use.
		/// </summary>
		/// <param name="sender">not used</param>
		/// <param name="e">not used</param>
		private void OnPortFacadeOpened(object aSender, EventArgs e) => _dispatcher.BeginInvoke(() =>
		{
			//Make the joystick control/key control invisible and
			//deselect any buttons.
			if (KeyDisplay != null)
			{
				KeyDisplay = null;
				MakeInvisible();
				foreach (var button in XJoyPanel.Buttons)
				{
					button.IsSelected = false;
				}
			}

			if (_portFacade.Port.IsAsciiMode)
			{
				_protocol = Protocol.ASCII;
			}
			else
			{
				_protocol = Protocol.Binary;
			}

			DisplayInformation = UNSELECTED_MSG;
		});


		/// <summary>
		///     This method is called when the "Joystick" tab is selected through Zaber Console.
		/// </summary>
		[PlugInMethod(PlugInMethodAttribute.EventType.PluginShown)]
		public void OnTabShown()
		{
			if (PortFacade.IsOpen)
			{
				DisplayInformation = UNSELECTED_MSG;
			}

			_isPluginSelected = true;

			//This check is needed for when a plugin is instantiated via the options tab
			//and the port facade is already open.
			if (null != PortFacade)
			{
				if (PortFacade.Port.IsAsciiMode)
				{
					_protocol = Protocol.ASCII;
				}
				else
				{
					_protocol = Protocol.Binary;
				}
			}

			if ((null != Conversation)
			&& Conversation.Device.DeviceType.IsJoystick
			&& Conversation.Device.IsSingleDevice)
			{
				DisplayInformation = "Click on a key or the joystick button in the left panel.";
				_originalConversationTimeout = Conversation.Timeout;

				if (_protocol == Protocol.ASCII)
				{
					InitializePluginAscii();
				}
				else
				{
					InitializePluginBinary();
				}
			}

			UpdateVisibility();
		}


		/// <summary>
		///     This method is called when the plugin is hidden through closing the tab in Zaber Console.
		/// </summary>
		[PlugInMethod(PlugInMethodAttribute.EventType.PluginHidden)]
		public void OnTabHidden()
		{
			_isPluginSelected = false;
			ClosePlugin();
			UpdateVisibility();
		}


		/// <summary>
		///     Make the plugin visible, initialize all relevant members and
		///     start querying the joystick when in ASCII mode.
		/// </summary>
		private void InitializePluginAscii()
		{
			//Make sure all subscriptions are unsubscribed 
			Conversation.Device.MessageReceived -= Joystick_MessageReceived;
			Conversation.Device.Port.DataPacketReceived -= DataPacket_Received;

			//Listen to the X-JOY
			Conversation.Device.MessageReceived += Joystick_MessageReceived;
			_connectedDevices = new List<ZaberDevice>(_portFacade.Devices);

			//Initialize the lockstep/virtual axis detection routine
			foreach (var connectedDevice in _connectedDevices)
			{
				VirtualAxesCollection.Add(connectedDevice.DeviceNumber, new Dictionary<int, string>());
				LockstepCollection.Add(connectedDevice.DeviceNumber, new Dictionary<int, string>());
			}

			//Populate containers which hold X-JOY data
			Keys.Clear();

			for (var i = 0; i < 10; i++)
			{
				Keys.Add(new KeyVM(i, _protocol));
			}

			var Joystick0 = new JoystickVM("99", _protocol);
			var Joystick1 = new JoystickVM("1", _protocol);
			var Joystick2 = new JoystickVM("2", _protocol);
			var Joystick3 = new JoystickVM("3", _protocol);

			Joysticks[0] =
				Joystick0; //dummy joystick to get indices to line up correctly and allow for foreach iteration
			Joysticks[1] = Joystick1;
			Joysticks[2] = Joystick2;
			Joysticks[3] = Joystick3;

			foreach (var joystick in Joysticks)
			{
				joystick.InversionChanged += ChangeInversionGraphic;
				joystick.ConnectedDevices = _connectedDevices;
				joystick.VirtualAxesCollection = _virtualAxesCollection;
				joystick.LockstepCollection = _lockstepCollection;
			}

			///This variable is just a counter used to count and determine the right key messages to parse.
			///<see cref="ProcessKeyInfo"/> uses this counter.
			InitialCounter = 0;

			//Start executing the device queries on a separate thread.
			var worker = BackgroundWorkerManager.CreateWorker();
			worker.DoWork += (aSender, aArgs) => { InitialQueryAscii(); };
			worker.RunWorkerCompleted += (aSender, aArgs) => { Conversation.Timeout = _originalConversationTimeout; };

			XJoyPanel = new XJoyPanelVM(this, _protocol);
			worker.Run();
			XJoyPanel.RequestDialogOpen += RelayDialogOpen;
		}


		/// <summary>
		///     Same as <see cref="InitializePluginAscii" /> but for Binary.
		/// </summary>
		private void InitializePluginBinary()
		{
			if (_binaryCommands.Count == 0)
			{
				FetchBinaryCommands();
			}

			_connectedDevices = new List<ZaberDevice>(_portFacade.Devices);

			//Make sure all subscriptions are unsubscribed or else reopening the plugin doesn't work
			Conversation.Device.MessageReceived -= Joystick_MessageReceived;
			Conversation.Device.Port.DataPacketReceived -= DataPacket_Received;
			Conversation.Device.Port.DataPacketReceived += DataPacket_Received;

			//Populate containers which hold X-JOY data
			Keys.Clear();
			for (var i = 0; i < 10; i++)
			{
				var toAdd = new KeyVM(i, _protocol)
				{
					ConnectedDevices = _connectedDevices,
					BinaryCommands = _binaryCommands
				};

				Keys.Add(toAdd);
			}

			var Joystick0 = new JoystickVM("99", _protocol); //dummy
			var Joystick1 = new JoystickVM("1", _protocol);
			var Joystick2 = new JoystickVM("2", _protocol);
			var Joystick3 = new JoystickVM("3", _protocol);

			Joysticks[0] =
				Joystick0; //dummy joystick to get indices to line up correctly and allow for foreach iteration
			Joysticks[1] = Joystick1;
			Joysticks[2] = Joystick2;
			Joysticks[3] = Joystick3;

			foreach (var joystick in Joysticks)
			{
				joystick.InversionChanged += ChangeInversionGraphic;
				joystick.ConnectedDevices = _connectedDevices;
			}

			InitialCounter = 0;

			//Start executing the device queries on a separate thread.
			var worker = BackgroundWorkerManager.CreateWorker();
			worker.DoWork += (aSender, aArgs) => { InitialQueryBinary(); };
			worker.RunWorkerCompleted += (aSender, aArgs) => { Conversation.Timeout = _originalConversationTimeout; };

			XJoyPanel = new XJoyPanelVM(this, _protocol);
			worker.Run();
			XJoyPanel.RequestDialogOpen += RelayDialogOpen;
		}


		/// <summary>
		///     Used to parse the initial batch of queries and populate the command grids.
		/// </summary>
		private void ProcessKeyInfo()
		{
			int keyEvent;
			int targetKey;

			//_messageIdToKeyMap contains mappings from message ID's to the integers
			//0-32. The integers 0-32 can be manipulated via the integer arithmetic to determine
			//key events and key numbers.
			foreach (var messageID in _messageIdToKeyMap.Keys)
			{
				var i = _messageIdToKeyMap[messageID];

				//The 1st key info message will be key 1 keyEvent 1, the 4th will be
				//key 1 keyEvent 4. The 26th will be key 7 keyEvent 2 and so forth.
				keyEvent = i % 4;
				targetKey = (i / 4) + 1;

				if (keyEvent == 0)
				{
					keyEvent = 4;
					targetKey = i / 4;
				}

				//_messageIDcontainer contains a list of strings like "cmd 02 1 home" which is a list of the
				//command information for each key.
				Keys[targetKey].ParseKeyInfoAscii(_messageIDcontainer[messageID], keyEvent);
			}

			_messageIdToKeyMap.Clear();
			_messageIDcontainer.Clear();
			InitialCounter = 0;
		}


		private void OnPortFacadeClosed(object aSender, EventArgs e) => _dispatcher.BeginInvoke(() =>
		{
			DisplayInformation = DISCONNECTED_MSG;
			ControlsEnabled = false;
			ClosePlugin();
		});


		/// <summary>
		///     Clears the data this plugin queries for that isn't
		///     necessary for when the plugin is invisible. Some data members should not be cleared
		///     due to databinding reasons e.g. Keys or Joysticks.
		/// </summary>
		private void ClosePlugin()
		{
			VirtualAxesCollection.Clear();
			LockstepCollection.Clear();
			_messageIdToKeyMap.Clear();
			_messageIDcontainer.Clear();
			IsQuerying = false;
			DeselectTabs();
			InitialCounter = 0;
		}


		/// <summary>
		///     Helper function for determining whether the user should see the plugin's interfaceable features,
		///     such as the buttons or the data.
		/// </summary>
		private void UpdateVisibility()
		{
			if ((PortFacade == null) || (Conversation == null))
			{
				ControlsEnabled = false;
				return;
			}

			//Only show the visible controls if the conversation is a joystick device,
			//and the plug in is currently selected.
			if (Conversation.Device.DeviceType.IsJoystick && Conversation.Device.IsSingleDevice && _isPluginSelected)
			{
				ControlsEnabled = true;
				AxisControlsEnabled = Conversation.Device.DeviceType.Commands
					.Where(cmd => (int)Command.SetAxisDeviceNumber == cmd.Number)
					.Any();
			}
			else
			{
				ControlsEnabled = false;
			}
		}

		#endregion

		#region --Device Message Handling--

		/// <summary>
		///     Handles options and key information queries when the user plugin decides
		///     to query the joystick for key information. This comes on a separate thread,
		///     so all paths that modify the view will have to call the dispatcher from this method.
		///     Key querying is handled via events because key information in ASCII is returned
		///     as multiple messages. There's no way to extract multiple messages through one request
		///     using the conversation model.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Joystick_MessageReceived(object aSender, DeviceMessageEventArgs e)
		{
			if (!IsQuerying)
			{
				if (e.DeviceMessage.Request != null)
				{
					var commandInfo = e.DeviceMessage.Request.Body.Split(' ');
					var mainCommand = commandInfo[0];

					//Check if the command is changing a setting.
					if (mainCommand.Equals("set"))
					{
						ChangeSettingsMethod(e);
					}
				}

				//If alerts are enabled, we show a little message when the key is pressed.
				//The alert device message body is "key (keyNumber) (eventNumber)"
				if ((e.DeviceMessage.MessageType == MessageType.Alert) && e.DeviceMessage.Body.Contains("key"))
				{
					var alertInfo = e.DeviceMessage.Body.Split(' ');
					var keyNumber = int.Parse(alertInfo[1], CultureInfo.InvariantCulture);
					var eventNumber = int.Parse(alertInfo[2], CultureInfo.InvariantCulture);

					_dispatcher.BeginInvoke(() => { Keys[keyNumber].KeyCommandGrids[eventNumber].EventFired = true; });
				}
			}

			//Key command processing, assigns a mapping from
			//the message ID (which the plugin turns on) of the device message
			//to a number from 1-32 (8 keys, 4 events on each key)
			//which is to be later processed.
			else
			{
				int messageID = e.DeviceMessage.MessageId;

				// Each key has an associated alert status message,
				// So each alert message should map from a message id to a key.
				// The message containing "alerts" comes first, and may be followed by
				// info messages using the same message ID.
				if (e.DeviceMessage.Body.Contains("alerts") || _messageIDcontainer.ContainsKey(messageID))
				{
					// Tell the main query function that we are still receiving key info.
					_waitForJoystickMessage.Set();

					if (!_messageIDcontainer.ContainsKey(messageID))
					{
						// This is the combined key + event index. It is zeroed before sending key info commands.
						InitialCounter++;
						_messageIdToKeyMap.Add(messageID, InitialCounter);

						_messageIDcontainer.Add(messageID, new List<string>());
					}

					_messageIDcontainer[messageID].Add(e.DeviceMessage.Body);
				}
			}

			//If the josytick plugin itself issues a bad command, then a the ErrorOccured flag will be set
			//and the controls choose what message to display accordingly. On the joystick it's something like "Invalid Commnand".
			//An example of this occuring is when the user enters a large number on a joystick field and clicks "Write".
			if (e.DeviceMessage.Body.Contains("RJ") && !_errorFreeQueryingMode)
			{
				_dispatcher.BeginInvoke(() => { ErrorOccured = true; });
			}
		}


		/// <summary>
		///     Used for handling Binary key information data packets.
		///     Data packets are routed through the device to generate device messages, which
		///     is what our joystick hooks up to in ASCII mode. However, in binary, key information
		///     returns from the device that the command is addressed to. This means
		///     that there may not be an associated device for every data packet, which means we have to
		///     hook up to this event to handle all of the messages.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void DataPacket_Received(object aSender, DataPacketEventArgs e)
		{
			if (IsQuerying)
			{
				BinaryCounter++;
				InitialCounter++;

				//e.g. key query no. 1 corresponds to key # 1
				//event number 1. Key Query no. 16 corresponds to
				//key 4, event number 4.
				var keyEvent = InitialCounter % 4;
				var targetKey = (InitialCounter / 4) + 1;
				if (keyEvent == 0)
				{
					keyEvent = 4;
					targetKey = InitialCounter / 4;
				}

				//This event is fired on a separate thread so we have to 
				//use the dispatcher.
				_dispatcher.BeginInvoke(() =>
				{
					Keys[targetKey].ParseKeyInfoBinary(e.DataPacket, keyEvent);
					KeySelected = KeySelected; //this is needed to refresh the view

					//KeySelected should never be 0. That would make the dummy KeyVM visible.
					if (KeySelected == 0)
					{
						KeyDisplay = null;
					}
				});

				//Specify the _packetsReceivedBeforeFinish field to automatically
				//set a flag when the querying is done.
				if (BinaryCounter >= PacketsReceivedBeforeFinish)
				{
					IsQuerying = false;
					InitialCounter = 0;
					BinaryCounter = 0;
					PacketsReceivedBeforeFinish = 0;
				}
			}

			//Counting how many packets were received. This is used
			//in some places to make sure that the querying has been finished.
			else
			{
				BinaryCounter++;

				if (BinaryCounter >= PacketsReceivedBeforeFinish)
				{
					BinaryCounter = 0;
					PacketsReceivedBeforeFinish = 0;
				}
			}
		}


		/// <summary>
		///     The comm.alert settings and joy.debug settings are changed from receiving
		///     device messages as opposed to other joystick settings which are changed via
		///     read write.
		/// </summary>
		/// <param name="e">The device message received from the joystick.</param>
		private void ChangeSettingsMethod(DeviceMessageEventArgs e)
		{
			var commandInfo = e.DeviceMessage.Request.Body.Split(' ');
			if (e.DeviceMessage.Body.Contains("OK"))
			{
				if (commandInfo[1].Equals("comm.alert"))
				{
					if (commandInfo[2].Equals("1"))
					{
						XJoyPanel.AlertsOn = true;

						foreach (var key in Keys)
						{
							_dispatcher.BeginInvoke(() => { key.CommAlertsOn = true; });
						}
					}
					else
					{
						XJoyPanel.AlertsOn = false;

						//Dispatcher is needed to display a little triangle on the command grid
						//if comm.alert is off but the key alerts are on.
						foreach (var key in Keys)
						{
							_dispatcher.BeginInvoke(() => { key.CommAlertsOn = false; });
						}
					}
				}

				if (commandInfo[1].Equals("joy.debug"))
				{
					XJoyPanel.DebugOn = commandInfo[2] == "1";
				}
			}
		}


		/// <summary>
		///     This method populates all of the joystick data by querying the device
		///     on start-up. Joystick and Key information is all populated.
		/// </summary>
		private void InitialQueryAscii()
		{
			lock (_backgroundWorkerLock)
			{
				///IsQueryingVisible is databound to a datatrigger that needs
				///to be toggled for it to work.
				IsQueryingVisible = true;
				IsQueryingVisible = false;

				//Virtual Axes and Lockstep querying might cause a querying error, turn this on
				//so that we don't cause the "Error Querying" message to pop up.
				_errorFreeQueryingMode = true;
				IsQuerying = true;

				//The command "key (key#) (event#) info" is sent.
				//"key 4 1" returns the information of Event 1 for key 4
				for (var i = 1; i < (NUMBER_OF_XJOY_KEYS + 1); i++)
				{
					for (var j = 1; j < (NUMBER_OF_KEY_EVENTS + 1); j++)
					{
						_conversation.Request(_conversation.Device.FormatAsciiCommand("key {0} {1} info", i, j));
					}
				}

				// Event gets reset (through Joystick_MessageReceived) everytime a device message is fired from the port.
				// We know that querying key commands is finished when the timer expires. 
				// The Numbers used for waiting are just heuristic magic numbers.
				while (_waitForJoystickMessage.WaitOne(300))
				{ }

				IsQuerying = false;

				_dispatcher.BeginInvoke(() =>
				{
					ProcessKeyInfo();
					KeySelected =
						KeySelected; //this is needed to refresh the view, a hacky way of calling OnPropertyChanged which is needed for some reason.

					//KeySelected should never be 0. That would make the dummy KeyVM visible.
					if (KeySelected == 0)
					{
						KeyDisplay = null;
					}
				});

				//Request the joystick information and the settings information. This information
				//can be nicely fit inside of one string return value.
				var JoystickInfo1 = _conversation.Request("joystick 1 info").Body;
				var JoystickInfo2 = _conversation.Request("joystick 2 info").Body;
				var JoystickInfo3 = _conversation.Request("joystick 3 info").Body;
				var alertsEnabled = _conversation.Request("get comm.alert").TextData;
				var debugEnabled = _conversation.Request("get joy.debug").TextData;

				_dispatcher.BeginInvoke(() =>
				{
					Joysticks[1].ParseJoystickInfo(JoystickInfo1);
					Joysticks[2].ParseJoystickInfo(JoystickInfo2);
					Joysticks[3].ParseJoystickInfo(JoystickInfo3);
				});

				//A value of 1 means on, a value of 0 means off.
				XJoyPanel.DebugOn = debugEnabled == "1";

				if (alertsEnabled.Equals("1"))
				{
					foreach (var key in Keys)
					{
						_dispatcher.BeginInvoke(() => { key.CommAlertsOn = true; });
					}

					XJoyPanel.AlertsOn = true;
				}
				else
				{
					//Dispatcher is needed because a data trigger in the key command grid uses 
					//the property CommAlertsOn to alert the user if alerts are on/off.
					foreach (var key in Keys)
					{
						_dispatcher.BeginInvoke(() => { key.CommAlertsOn = false; });
					}

					XJoyPanel.AlertsOn = false;
				}

				//query Virtual/Lockstep information
				foreach (var device in _connectedDevices)
				{
					if (device.DeviceNumber != 0)
					{
						GetVirtualLockstepForDevice(device);
					}
				}

				_errorFreeQueryingMode = false;
			}
		}


		/// <summary>
		///     This method populates all of the relevant joystick data on start-up.
		///     Equivalent to <see cref="InitialQueryAscii" /> except used in binary.
		/// </summary>
		private void InitialQueryBinary()
		{
			lock (_backgroundWorkerLock)
			{
				IsQueryingVisible = true;
				IsQueryingVisible = false;

				//We must turn IsInvalidateEnabled off since Binary key info messages
				//may come from invalid device numbers.
				var oldValueIsInvalidateEnabled = _portFacade.IsInvalidateEnabled;
				_portFacade.IsInvalidateEnabled = false;

				//Read the joystick info
				ReadJoystickInfoBodyBinary();
				int numberOfKeys;

				//Start Key Querying, make sure we start after the last request is sent.
				if (Conversation.Device.DeviceType.DeviceId == TJOY3_DEVICE_ID)
				{
					PacketsReceivedBeforeFinish = 20; //4 Key Infos for 5 keys.
					numberOfKeys = NUMBER_OF_TJOY_KEYS;
				}
				else // Assuming 8 keys. TODO: Need a way to find out how many keys really exist.
				{
					PacketsReceivedBeforeFinish = 32; //4 Key information requests each for 8 keys.
					numberOfKeys = NUMBER_OF_XJOY_KEYS;
				}

				//Always set the querying flag when we start querying for key information.
				IsQuerying = true;

				///Responses are handled via <see cref="DataPacket_Received(object, DataPacketEventArgs)"/>
				for (var i = 1; i < (numberOfKeys + 1); i++)
				{
					for (var j = 1; j < (NUMBER_OF_KEY_EVENTS + 1); j++)
					{
						//The data for this command corresponds to a key event and a key.
						//E.g. data 81 represents key 8, event 1. Data 54 represents key 5, event 4.
						var data = (i * 10) + j;
						Conversation.Send(Command.ReturnEventInstruction, data);
					}
				}

				while (IsQuerying)
				{
				}

				;

				_portFacade.ClearDeferredInvalidation();
				_portFacade.IsInvalidateEnabled = oldValueIsInvalidateEnabled;
			}
		}

		#endregion

		#region --Virtual Axis / Lockstep Detection --

		/// <summary>
		///     Gets all of the virtual axis information for the device that the
		///     joystick axis is currently targeting.
		///     IMPORTANT : I am assuming that in the future, when we have controllers
		///     that, for example, allow 2 virtual axes, only virtual indices 1
		///     and 2 will be allowed.
		/// </summary>
		/// <param name="device">The device the joystick axis is targeting.</param>
		private void GetVirtualLockstepForDevice(ZaberDevice device)
		{
			//if the joystick isn't targeting a device.
			if (device == null)
			{
				return;
			}

			var conversation = PortFacade.GetConversation(device.DeviceNumber, device.AxisNumber);

			DeviceMessage virtualMessage = null;
			DeviceMessage lockstepMessage = null;

			var virtualIsBad = false;
			var lockstepIsBad = false;

			//We query the devices via to see how many virtual axes it can possbily hold. 
			//If the device can hold 2 virtual axes, then we still query virtual indices 1 and 2
			//for information. If it holds none, then we don't query at all. 
			try
			{
				//throws an exception when we request something that is rejected.
				virtualMessage = conversation.Request("get virtual.numvirtual");
			}

			catch
			{
				virtualIsBad = true;
			}

			try
			{
				lockstepMessage = conversation.Request("get lockstep.numgroups");
			}

			catch
			{
				lockstepIsBad = true;
			}

			if (!virtualIsBad)
			{
				//The return value of "get virtual.numvirtual" indicates how many indices we should query. 
				var numVirtual = (int) virtualMessage.NumericData;

				for (var i = 1; i <= numVirtual; i++)
				{
					var virtualInfo = conversation.Request($"virtual {i} info");
					ProcessVirtual(i, virtualInfo);
				}
			}

			if (!lockstepIsBad)
			{
				//The return value of "get lockstep.numgroups" indicates how many indicies we should query.
				var numLockstep = (int) lockstepMessage.NumericData;

				for (var i = 1; i <= numLockstep; i++)
				{
					var lockstepInfo = conversation.Request($"lockstep {i} info");
					ProcessLockstep(i, lockstepInfo);
				}
			}
		}


		/// <summary>
		///     Puts the virtual info into the collection and refreshes the view for the joystick.
		/// </summary>
		/// <param name="message">The device message.</param>
		private void ProcessVirtual(int virtualAxis, DeviceMessage message)
		{
			int deviceNumber = message.DeviceNumber;

			//The virtual axis is disabled
			if (message.Body.Contains("disabled"))
			{
				VirtualAxesCollection[deviceNumber][virtualAxis] = "Not Set-up";
				return;
			}

			var response = message.Body.Split(' ');
			var axis1 = response[3];
			var axis2 = response[4];
			string virtualInformation;

			//The response looks like "OK IDLE WR 1 2 30 6000000" 
			//or "OK IDLE WR 2 1 1 5 45000" depending on the type of virtual axis.

			if (7 == response.Length)
			{
				//Virtual axis based on Angle
				var angle = response[5];
				virtualInformation = "Axes " + axis1 + " , " + axis2 + " (Angle : " + angle + " degrees)";
			}
			else if (8 == response.Length)
			{
				//Virtual axis based on ratio.
				var multiplier1 = response[5];
				var multiplier2 = response[6];
				virtualInformation =
					"Axes " + axis1 + " , " + axis2 + " (Ratio : " + multiplier1 + " to " + multiplier2 + ")";
			}
			else
			{
				throw new Exception("Virtual info response not caught");
			}

			VirtualAxesCollection[deviceNumber][virtualAxis] = virtualInformation;

			//Change the virtual axis targeting information through the UI thread.
			_dispatcher.BeginInvoke(() =>
			{
				foreach (var joystick in Joysticks)
				{
					joystick.SetVirtualAxisInfo();
				}
			});
		}


		/// <summary>
		///     The lockstep equivalent of <see cref="ProcessVirtual(DeviceMessage)" />
		/// </summary>
		/// <param name="message"></param>
		private void ProcessLockstep(int lockstepGroup, DeviceMessage message)
		{
			var deviceNumber = (int) message.DeviceNumber;

			if (message.Body.Contains("disabled"))
			{
				//no lockstep detected
				LockstepCollection[deviceNumber][lockstepGroup] = "Not Set-up";

				return;
			}

			//The response looks like "OK IDLE -- 1 2 500 0"
			var response = message.Body.Split(' ');
			var axis1 = response[3];
			var axis2 = response[4];

			var lockstepInformation = "Axes " + axis1 + " , " + axis2;

			LockstepCollection[deviceNumber][lockstepGroup] = lockstepInformation;

			_dispatcher.BeginInvoke(() =>
			{
				foreach (var joystick in Joysticks)
				{
					joystick.SetLockstepInfo();
				}
			});
		}

		#endregion

		#region -- Commands and Methods for Writing and Reading --   

		//All ICommands are bound to buttons.

		/// <summary>
		///     Fired when the big round joystick button is clicked.
		/// </summary>
		public ICommand ClickedJoystickButtonCommand => new RelayCommand(thisButton => OnJoystickClicked(thisButton));

		/// <summary>
		///     Fired when the smaller key buttons are clicked.
		/// </summary>
		public ICommand ClickedKeyButtonCommand => new RelayCommand(thisButton => OnKeyClicked(thisButton));

		/// <summary>
		///     Fired when the "Write" button on the joystick control is clicked.
		/// </summary>
		public ICommand WriteJoystickInfoCommand => new RelayCommand(aJoystickVM => WriteJoystickInfo(aJoystickVM));

		/// <summary>
		///     Fired when the "Read" button of the joystick control is clicked.
		/// </summary>
		public ICommand ReadJoystickInfoCommand => new RelayCommand(aJoystickVM => ReadJoystickInfo(aJoystickVM));

		/// <summary>
		///     Fired when the "Read" button of the key control is clicked.
		/// </summary>
		public ICommand ReadKeyInfoCommand => new RelayCommand(aKeyVM => ReadKeyInfo(aKeyVM));

		/// <summary>
		///     Fired when the "Write" button of the key control is clicked.
		/// </summary>
		public ICommand WriteKeyInfoCommand => new RelayCommand(aKeyVM => WriteKeyInfo(aKeyVM));


		/// <summary>
		///     Hooked up to the <see cref="XJoyPanel" /> RequestDialogOpen events
		///     and bubbles this event upwards.
		/// </summary>
		/// <param name="sender">Not used</param>
		/// <param name="e">The IDialogViewModel to be opened.</param>
		private void RelayDialogOpen(object aSender, IDialogViewModel e) => RequestDialogOpen?.Invoke(this, e);


		/// <summary>
		///     Reads one joystick axis' info from the device on a different helper thread.
		/// </summary>
		/// <param name="sender">A JoystickVM's whose information will be populated.</param>
		/// <param name="e"></param>
		private void ReadJoystickInfo(object aSender)
		{
			//Prevents spamming of this command.
			if (NumberOfBackgroundWorkers > MAX_NUMBER_BACKGROUND_WORKERS)
			{
				return;
			}

			var aJoystick = aSender as JoystickVM;
			var worker = BackgroundWorkerManager.CreateWorker();

			worker.RunWorkerCompleted += delegate { NumberOfBackgroundWorkers--; };
			NumberOfBackgroundWorkers++;

			if (_protocol == Protocol.ASCII)
			{
				worker.DoWork += delegate
				{
					ReadSingleAxisBodyAscii(int.Parse(aJoystick.JoystickAxisNumber, CultureInfo.InvariantCulture));
				};
			}
			else
			{
				worker.DoWork += delegate { ReadSingleAxisBodyBinary(aJoystick); };
			}

			worker.Run();
		}


		/// <summary>
		///     Write one joystick axis' info to the device on a different helper thread.
		/// </summary>
		/// <param name="sender">A JoystickVM's whose information will be populated.</param>
		/// <param name="e"></param>
		private void WriteJoystickInfo(object aSender)
		{
			if (NumberOfBackgroundWorkers > MAX_NUMBER_BACKGROUND_WORKERS)
			{
				return;
			}

			var aJoystickVM = aSender as JoystickVM;
			var worker = BackgroundWorkerManager.CreateWorker();

			worker.RunWorkerCompleted += delegate { NumberOfBackgroundWorkers--; };
			NumberOfBackgroundWorkers++;

			if (_protocol == Protocol.ASCII)
			{
				worker.DoWork += delegate { WriteJoystickInfoBodyAscii(aJoystickVM); };
			}
			else
			{
				worker.DoWork += delegate { WriteJoystickInfoBodyBinary(aJoystickVM); };
			}

			worker.Run();

			_eventLog.LogEvent("Write joystick axis settings", $"Axis {aJoystickVM.JoystickAxisNumber}");
		}


		/// <summary>
		///     Reads one key's info from a different thread.
		/// </summary>
		/// <param name="sender">A KeyVM's whose information will be populated.</param>
		/// <param name="e"></param>
		private void ReadKeyInfo(object aSender)
		{
			if (NumberOfBackgroundWorkers > MAX_NUMBER_BACKGROUND_WORKERS)
			{
				return;
			}

			var aKey = aSender as KeyVM;
			var worker = BackgroundWorkerManager.CreateWorker();

			worker.RunWorkerCompleted += delegate { NumberOfBackgroundWorkers--; };
			NumberOfBackgroundWorkers++;

			if (_protocol == Protocol.ASCII)
			{
				worker.DoWork += delegate { ReadKeyInfoBodyAscii(aKey); };
			}
			else
			{
				worker.DoWork += delegate { ReadKeyInfoBodyBinary(aKey); };
			}

			worker.Run();
		}


		/// <summary>
		///     Write one key's info to the device on a different thread.
		/// </summary>
		/// <param name="sender">A KeyVM whose information will be populated.</param>
		/// <param name="e"></param>
		private void WriteKeyInfo(object aSender)
		{
			if (NumberOfBackgroundWorkers > MAX_NUMBER_BACKGROUND_WORKERS)
			{
				return;
			}

			var aKey = aSender as KeyVM;
			var worker = BackgroundWorkerManager.CreateWorker();

			worker.RunWorkerCompleted += delegate { NumberOfBackgroundWorkers--; };
			NumberOfBackgroundWorkers++;

			if (_protocol == Protocol.ASCII)
			{
				worker.DoWork += delegate { WriteKeyInfoBodyAscii(aKey); };
			}
			else
			{
				worker.DoWork += delegate { WriteKeyInfoBodyBinary(aKey); };
			}

			worker.Run();

			_eventLog.LogEvent("Write key settings", $"Key {aKey.ID}");
		}


		/// <summary>
		///     ASCII: Reads one joystick axis' information and parses it.
		/// </summary>
		/// <param name="joystickAxis">The axis of the joystick you want to read to.</param>
		private void ReadSingleAxisBodyAscii(int joystickAxis)
		{
			lock (_backgroundWorkerLock)
			{
				var JoystickInfo = _conversation
							   .Request(_conversation.Device.FormatAsciiCommand("joystick {0} info", joystickAxis))
							   .Body;

				//We use Invoke instead of BeginInvoke because GetVirtualLockstepForDevice uses
				//the TargetDevice that the joystick is currently targeting, which is changed in the below method.
				_dispatcher.Invoke(() => { Joysticks[joystickAxis].ParseJoystickInfo(JoystickInfo); });

				//Querying for lockstep and virtual may cause errors.
				_errorFreeQueryingMode = true;
				GetVirtualLockstepForDevice(Joysticks[joystickAxis].TargetDevice);
				_errorFreeQueryingMode = false;
			}
		}


		/// <summary>
		///     Binary : Reads one joystick axis' information and parses it.
		/// </summary>
		/// <param name="aJoystick">The joystick axis, either axis 1, 2 or 3.</param>
		private void ReadSingleAxisBodyBinary(JoystickVM aJoystick)
		{
			lock (_backgroundWorkerLock)
			{
				var joystickInformation = new string[4];

				//To get binary settings we have to set the active axis to the joystick's axis, then request 
				//a few settings via Command.ReturnSetting.
				Conversation.Request(Command.SetActiveAxis,
									 int.Parse(aJoystick.JoystickAxisNumber, CultureInfo.InvariantCulture));
				var targetDevice = Conversation.Request(Command.ReturnSetting, (byte) Command.SetAxisDeviceNumber)
											.NumericData.ToString();
				var controlSpeed = Conversation.Request(Command.ReturnSetting, (byte) Command.SetAxisVelocityProfile)
											.NumericData.ToString();
				var maxSpeed = Conversation.Request(Command.ReturnSetting, (byte) Command.SetAxisVelocityScale)
										.NumericData.ToString();
				var inversion = Conversation.Request(Command.ReturnSetting, (byte) Command.SetAxisInversion)
										 .NumericData.ToString();

				joystickInformation[0] = targetDevice;
				joystickInformation[1] = controlSpeed;
				joystickInformation[2] = maxSpeed;
				joystickInformation[3] = inversion;

				//Parse it on the main thread.
				_dispatcher.BeginInvoke(() => { aJoystick.ParseJoystickInfo(joystickInformation); });
			}
		}


		/// <summary>
		///     ASCII : Write the current key's 4 command events to the joystick.
		/// </summary>
		/// <param name="aKey">The key you're writing to</param>
		private void WriteKeyInfoBodyAscii(KeyVM aKey)
		{
			lock (_backgroundWorkerLock)
			{
				for (var i = 1; i < (NUMBER_OF_KEY_EVENTS + 1); i++)
				{
					//Return a list of formatted commands from the grid. Clear the key grids, and then
					//add each command that was returned from the grid.

					var grid = aKey.KeyCommandGrids[i];
					var allCommands = grid.FormatCommands();
					Conversation.Request(Conversation.Device.FormatAsciiCommand("key {0} {1} clear", aKey.ID, i));

					foreach (var command in allCommands)
					{
						var parts = command.Split(new[] { ' ' }, 2, StringSplitOptions.RemoveEmptyEntries);
						Conversation.Request(
							Conversation.Device.FormatAsciiCommand("key {0} {1} add {2} {3}",
																   aKey.ID,
																   i,
																   parts[0],
																   parts[1]));
					}

					int alert;
					alert = grid.AlertsEnabled ? 1 : 0;
					Conversation.Request(
						Conversation.Device.FormatAsciiCommand("key {0} {1} alert {2}", aKey.ID, i, alert));
				}

				//Read the key after writing.
				ReadKeyInfoBodyAscii(aKey);
			}
		}


		/// <summary>
		///     ASCII : Read the key information from the joystick to Zaber console. This was made public just
		///     for the ClearKeysWindowVM which shouldn't use the multithreaded wrapper command for this body
		///     because we want that "Clear" button press to delay the main thread. This is
		///     a messy way to do it, TODO find a better way.
		/// </summary>
		/// <param name="aKeyVM">The key you're reading from.</param>
		public void ReadKeyInfoBodyAscii(KeyVM aKey)
		{
			lock (_backgroundWorkerLock)
			{
				//We want the initial counter to be 28 for key 8,
				//0 for key 1, 4 for key 2, etc
				InitialCounter = (int.Parse(aKey.ID, CultureInfo.InvariantCulture) - 1) * 4;

				IsQuerying = true;

				for (var i = 1; i < (NUMBER_OF_KEY_EVENTS + 1); i++)
				{
					_conversation.Request(_conversation.Device.FormatAsciiCommand("key {0} {1} info", aKey.ID, i));
				}

				// Event gets reset (through Joystick_MessageReceived) everytime a device message is fired from the port.
				// We know that querying key commands is finished when the timer expires. 
				// The Numbers used for waiting are just heuristic magic numbers.
				while (_waitForJoystickMessage.WaitOne(300))
				{ }

				IsQuerying = false;

				///use Invoke instead of BeginInvoke because BeginInvoke would allow
				///another ReadKeyInfo thread to acquire this lock and do things before key infos are processed.
				///This bug was found by clicking the read button multiple times which would do weird things to the key grids.
				_dispatcher.Invoke(() =>
				{
					foreach (var grid in aKey.KeyCommandGrids)
					{
						grid.ClearCommands();
					}

					ProcessKeyInfo();
				});
			}
		}


		/// <summary>
		///     ASCII : Writes a joystick axis' information in this tab to the actual device.
		///     Note: This method was made public because <see cref="JoystickConfig.JoystickConfigurationVM" />
		///     needs to use the synchronous version of this method.
		/// </summary>
		/// <param name="aJoystickVM">The joystick axis you're writing to.</param>
		/// <returns>True on success; false if there was an error.</returns>
		public bool WriteJoystickInfoBodyAscii(JoystickVM aJoystickVM)
		{
			var success = true;

			lock (_backgroundWorkerLock)
			{
				try
				{
					var device = _conversation.Device;
					if (aJoystickVM.ViewInfo.IsTargetingNormalAxis)
					{
						_conversation.Request(device.FormatAsciiCommand("joystick {0} target {1} {2}",
																		aJoystickVM.JoystickAxisNumber,
																		aJoystickVM.ViewInfo.TargetDeviceNumber,
																		aJoystickVM.ViewInfo.TargetAxis));
					}
					else if (aJoystickVM.ViewInfo.IsTargetingVirtualAxis)
					{
						_conversation.Request(device.FormatAsciiCommand("joystick {0} target {1} {2} {3}",
																		aJoystickVM.JoystickAxisNumber,
																		aJoystickVM.ViewInfo.TargetDeviceNumber,
																		"virtual",
																		aJoystickVM.ViewInfo.VirtualIndex));
					}
					else if (aJoystickVM.ViewInfo.IsTargetingLockstep)
					{
						_conversation.Request(device.FormatAsciiCommand("joystick {0} target {1} {2} {3}",
																		aJoystickVM.JoystickAxisNumber,
																		aJoystickVM.ViewInfo.TargetDeviceNumber,
																		"lockstep",
																		aJoystickVM.ViewInfo.LockstepIndex));
					}

					_conversation.Request(device.FormatAsciiCommand("joystick {0} speedprofile {1}",
																	aJoystickVM.JoystickAxisNumber,
																	aJoystickVM.SpeedProfile));
					_conversation.Request(device.FormatAsciiCommand("joystick {0} maxspeed {1}",
																	aJoystickVM.JoystickAxisNumber,
																	aJoystickVM.ViewInfo.MaxSpeed));
					_conversation.Request(device.FormatAsciiCommand("joystick {0} invert {1}",
																	aJoystickVM.JoystickAxisNumber,
																	aJoystickVM.Inversion));
					_conversation.Request(device.FormatAsciiCommand("joystick {0} resolution {1}",
																	aJoystickVM.JoystickAxisNumber,
																	aJoystickVM.ViewInfo.Resolution));


					//Re-read after writing to update the view information.
					ReadSingleAxisBodyAscii(int.Parse(aJoystickVM.JoystickAxisNumber, CultureInfo.InvariantCulture));
				}
				catch (ErrorResponseException aErrorException)
				{
					success = false;
					_log.Error("Failed to write joystick axis settings; a command was rejected.", aErrorException);
				}
				catch (FormatException aFormatException)
				{
					success = false;
					_log.Error("Saved joystick axis settings were invalid.", aFormatException);
				}
				catch (InvalidDataException aDataException)
				{
					success = false;
					_log.Error("Could not find command formatting info needed to restore some joystick settings.", aDataException);
				}
			}

			return success;
		}


		/// <summary>
		///     Binary : Reading key information from the device to zaber console.
		/// </summary>
		/// <param name="aKey">The key you're reading from.</param>
		private void ReadKeyInfoBodyBinary(KeyVM aKey)
		{
			lock (_backgroundWorkerLock)
			{
				//Invoke synchronously. We don't want to proceed before the grids are empty.
				_dispatcher.Invoke(() =>
				{
					foreach (var grid in aKey.KeyCommandGrids)
					{
						grid.ClearCommands();
					}
				});

				//We can't have invalidate on because responses might come from invalid devices.
				var oldValueValidate = _portFacade.IsInvalidateEnabled;
				_portFacade.IsInvalidateEnabled = false;

				var keyID = int.Parse(aKey.ID, CultureInfo.InvariantCulture);
				BinaryCounter = 0;
				InitialCounter = (keyID - 1) * 4; //the initial counter is operated on using integer division

				//and the modulus operator to get the key event/key information.
				///<see cref="DataPacket_Received(object, DataPacketEventArgs)"/>
				PacketsReceivedBeforeFinish = 4;
				IsQuerying = true;

				for (var j = 1; j < 5; j++)
				{
					var data = (keyID * 10) + j;
					Conversation.Device.Send(Command.ReturnEventInstruction, data, (byte) data);
				}

				while (IsQuerying)
				{
				}

				;

				_portFacade.ClearDeferredInvalidation();
				_portFacade.IsInvalidateEnabled = oldValueValidate;
			}
		}


		/// Binary : Reads ALL axes of the joystick in binary. Only used on startup.
		/// </summary>
		private void ReadJoystickInfoBodyBinary()
		{
			lock (_backgroundWorkerLock)
			{
				//There are 15 packets that are received as a result of requests from this method.
				PacketsReceivedBeforeFinish = 15;

				var joystickInformation = new List<string[]>();

				for (var i = 1; i < 4; i++)
				{
					joystickInformation.Add(new string[4]);
					Conversation.Request(Command.SetActiveAxis, i);
					var targetDevice = Conversation.Request(Command.ReturnSetting, (byte) Command.SetAxisDeviceNumber)
												.NumericData.ToString();
					var controlSpeed = Conversation
								   .Request(Command.ReturnSetting, (byte) Command.SetAxisVelocityProfile)
								   .NumericData.ToString();
					var maxSpeed = Conversation.Request(Command.ReturnSetting, (byte) Command.SetAxisVelocityScale)
											.NumericData.ToString();
					var inversion = Conversation.Request(Command.ReturnSetting, (byte) Command.SetAxisInversion)
											 .NumericData.ToString();

					joystickInformation[i - 1][0] = targetDevice;
					joystickInformation[i - 1][1] = controlSpeed;
					joystickInformation[i - 1][2] = maxSpeed;
					joystickInformation[i - 1][3] = inversion;
				}

				///IMPORTANT : We don't want to start parsing information before the
				///queries are all done because this method is called from  <see cref="InitialQueryBinary"/>
				///right before we query the key information. The responses from the above query might overlap
				///with the responses from the key query, which would make our keys show invalid information.
				while (PacketsReceivedBeforeFinish > 0)
				{
				}

				;

				_dispatcher.BeginInvoke(() =>
				{
					for (var i = 1; i < 4; i++)
					{
						Joysticks[i].ParseJoystickInfo(joystickInformation[i - 1]);
					}
				});
			}
		}


		/// <summary>
		///     Binary : Writes one key's key command information to the joystick.
		/// </summary>
		/// <param name="aKey">The key you're writing to.</param>
		private void WriteKeyInfoBodyBinary(KeyVM aKey)
		{
			lock (_backgroundWorkerLock)
			{
				//We can't have invalidate on because responses might come from invalid devices.
				var oldValueValidate = _portFacade.IsInvalidateEnabled;
				_portFacade.IsInvalidateEnabled = false;

				var keyDataFirstDigit = int.Parse(aKey.ID, CultureInfo.InvariantCulture) * 10;

				//8 Packets are received from the device, 4 key events 
				//that send a "Load Event Instruction" command, and
				//the actual command.
				PacketsReceivedBeforeFinish = 8;

				for (var i = 1; i < (NUMBER_OF_KEY_EVENTS + 1); i++)
				{
					var commandToSend = aKey.KeyCommandGrids[i].BinaryInfo;
					try
					{
						var device = byte.Parse(commandToSend.TargetDeviceNumber, CultureInfo.InvariantCulture);
						var command = byte.Parse(commandToSend.BinaryCommand, CultureInfo.InvariantCulture);
						var data = int.Parse(commandToSend.BinaryData, CultureInfo.InvariantCulture);

						Conversation.Device.Send(Command.LoadEventInstruction, keyDataFirstDigit + i);
						PortFacade.Port.Send(device, (Command) command, data);
					}

					catch
					{
						//other parse errors, such as an empty data cause an error to be sent.
						Conversation.Device.Send(Command.Error, 0);
						Conversation.Device.Send(Command.Error, 0);

						_dispatcher.BeginInvoke(() => { ErrorOccured = true; });

						//Stop writing.
						return;
					}
				}

				while (PacketsReceivedBeforeFinish != 0)
				{
				}

				//Parse the information that was written to the port.
				_dispatcher.BeginInvoke(() =>
				{
					for (var i = 1; i < 5; i++)
					{
						//Manufacture the information sent to the device into data packets.
						//We do this instead re-reading the key because devices that move as a result of writing
						//may send back messages which may get accidentally interpreted as the data from a return command.
						var binaryInfoToParse = new DataPacket
						{
							DeviceNumber =
								byte.Parse(aKey.KeyCommandGrids[i].BinaryInfo.TargetDeviceNumber,
										   CultureInfo.InvariantCulture),
							Command =
								(Command) byte.Parse(aKey.KeyCommandGrids[i].BinaryInfo.BinaryCommand,
													 CultureInfo.InvariantCulture),
							NumericData = decimal.Parse(aKey.KeyCommandGrids[i].BinaryInfo.BinaryData,
														CultureInfo.InvariantCulture)
						};

						aKey.KeyCommandGrids[i].ClearCommands();
						aKey.ParseKeyInfoBinary(binaryInfoToParse, i);
					}
				});

				_portFacade.ClearDeferredInvalidation();
				_portFacade.IsInvalidateEnabled = oldValueValidate;
			}
		}


		/// <summary>
		///     Binary : Writes a single joystick axis' information to the joystick.
		/// </summary>
		/// <param name="aJoystick"></param>
		private void WriteJoystickInfoBodyBinary(JoystickVM aJoystick)
		{
			lock (_backgroundWorkerLock)
			{
				try
				{
					Conversation.Request(Command.SetActiveAxis,
										 int.Parse(aJoystick.JoystickAxisNumber, CultureInfo.InvariantCulture));
					Conversation.Request(Command.SetAxisDeviceNumber,
										 int.Parse(aJoystick.ViewInfo.TargetDeviceNumber,
												   CultureInfo.InvariantCulture));
					Conversation.Request(Command.SetAxisVelocityProfile,
										 int.Parse(aJoystick.SpeedProfile, CultureInfo.InvariantCulture));
					Conversation.Request(Command.SetAxisVelocityScale,
										 int.Parse(aJoystick.ViewInfo.MaxSpeed, CultureInfo.InvariantCulture));
					Conversation.Request(Command.SetAxisInversion,
										 int.Parse(aJoystick.Inversion, CultureInfo.InvariantCulture));

					ReadSingleAxisBodyBinary(aJoystick);
				}

				//catch parse exceptions such as a string being too long or empty.
				//This is a data trigger so it needs to dispatched to the main thread.
				catch (Exception e)
				{
					_log.Error("Failed to write some settings to joystick.", e);

					_dispatcher.BeginInvoke(() => { ErrorOccured = true; });
				}
			}
		}


		/// <summary>
		///     Reads all 3 axis of the joystick and selects it.
		///     Allows user to modify the joystick settings.
		/// </summary>
		/// <param name="sender">The big round button's ButtonVM.</param>
		/// <param name="e">Null.</param>
		private void OnJoystickClicked(object aButton)
		{
			JoystickVisible = true;
			KeyVisible = false;

			if (!(aButton is ButtonVM))
			{
				throw new ArgumentException();
			}

			var Button = aButton as ButtonVM;

			//Each key has a ButtonVM whose ButtonID corresponds to the key,
			//the big joystick button has a ButtonID of 9. Deselect all of the buttons
			//and highlight(select) the one that was clicked.
			foreach (var button in XJoyPanel.Buttons)
			{
				button.IsSelected = false;
			}

			Button.IsSelected = true;
			KeySelected = Button.ButtonID;

			//Reread the joystick information.
			for (var i = 1; i < 4; i++)
			{
				ReadJoystickInfo(Joysticks[i]);
			}
		}


		/// <summary>
		///     Reads the selected key. Makes the view correspond to the appropriate selected key.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnKeyClicked(object aButton)
		{
			JoystickVisible = false;
			KeyVisible = true;

			if (!(aButton is ButtonVM))
			{
				throw new ArgumentException();
			}

			var Button = aButton as ButtonVM;
			KeySelected = Button.ButtonID;

			//Deselect all of the buttons
			//and highlight(select) the one that was clicked.
			foreach (var button in XJoyPanel.Buttons)
			{
				button.IsSelected = false;
			}

			Button.IsSelected = true;
			KeySelected = Button.ButtonID;

			//Reread the information.
			ReadKeyInfo(KeyDisplay);
		}


		/// <summary>
		///     Sets which plus signs and arrows are shown when a joystick axis is selected.
		///     Called when an inversion event is fired from a joystick inversion checkbox.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		/// n
		private void ChangeInversionGraphic(object senderJoystick, EventArgs e)
		{
			var aJoystick = senderJoystick as JoystickVM;
			var joystickAxis = int.Parse(aJoystick.JoystickAxisNumber, CultureInfo.InvariantCulture);

			XJoyPanel.ChangeInversionGraphic(aJoystick);
		}


		/// <summary>
		///     Utility function for remembering the last selected tab.
		/// </summary>
		private void DeselectTabs()
		{
			for (var i = 0; i < 4; i++)
			{
				if (SelectedTab[i])
				{
					_lastSelectedAxis = i;
				}

				SelectedTab[i] = false;
			}
		}


		/// <summary>
		///     Get's all of the binary commands that should be available
		///     in the binary mode.
		/// </summary>
		private void FetchBinaryCommands()
		{
			//Obtain all of the binary commands.
			var commands = new List<CommandInfoVM>();
			foreach (var commandInfo in _portFacade.DefaultDeviceType.Commands)
			{
				if (commandInfo.TextCommand == null)
				{
					commands.Add(new CommandInfoVM(commandInfo, null));
				}
			}

			//Make sure that the list is alphabetically ordered.
			commands.Sort((commandA, commandB) => string.CompareOrdinal(commandA.CommandName, commandB.CommandName));
			_binaryCommands.Clear();

			foreach (var vm in commands)
			{
				_binaryCommands.Add(vm);
			}

			//Make sure to add the error command.
			var errorCommand = new CommandInfo
			{
				Name = "Error",
				Command = Command.Error
			};

			var errorCommandVM = new CommandInfoVM(errorCommand, null);
			_binaryCommands.Add(errorCommandVM);
		}


		/// <summary>
		///     Utility function for making all views that pertain to joystick settings invisible.
		/// </summary>
		private void MakeInvisible()
		{
			JoystickVisible = false;
			KeyVisible = false;

			DeselectTabs();
		}


		/// <summary>
		///     Utility function that's called when we want to show the old view
		///     without forgetting our last selection.
		/// </summary>
		private void MakeVisible()
		{
			if (KeyDisplay == null)
			{
				JoystickVisible = false;
				KeyVisible = false;

				return;
			}

			//Button # 9 is the joystick.
			if (KeySelected == 9)
			{
				JoystickVisible = true;
				XJoyPanel.Buttons[9].IsSelected = true;
			}
			else
			{
				KeyVisible = true;
				XJoyPanel.Buttons[KeySelected].IsSelected = true;
			}
		}

		#endregion

		#region -- Getters and Setters --

		/// <summary>
		///     The displayed bound KeyVM.
		/// </summary>
		public KeyVM KeyDisplay
		{
			get => _keyDisplay;

			set => Set(ref _keyDisplay, value, nameof(KeyDisplay));
		}

		/// <summary>
		///     The selected key, used for determining view properties.
		/// </summary>
		public int KeySelected
		{
			get => _keySelected;

			set
			{
				Set(ref _keySelected, value, nameof(KeySelected));

				//Automatically set the correct key view when this is changed.
				KeyDisplay = Keys[KeySelected];
			}
		}

		/// <summary>
		///     Holds 4 JoystickVM's. The first one is a dummy to line indicies, index 1 coresponds to joystick axis 1 and etc.
		/// </summary>
		public ObservableCollection<JoystickVM> Joysticks
		{
			get => _joysticks;
			set => Set(ref _joysticks, value, nameof(Joysticks));
		}

		/// <summary>
		///     Holds 10 keys. Index 0 is a dummy key to line up indices, and index 9 is another dummy key
		///     as a special key for the joystick's views.
		/// </summary>
		public ObservableCollection<KeyVM> Keys
		{
			get => _keys;
			set => Set(ref _keys, value, nameof(Keys));
		}

		/// <summary>
		///     Dictates whether any part of this plugin should be visible at all.
		/// </summary>
		public bool ControlsEnabled
		{
			get => _controlsEnabled;
			set
			{
				Set(ref _controlsEnabled, value, nameof(ControlsEnabled));

				if (value == false)
				{
					MakeInvisible();
				}
				else
				{
					MakeVisible();
				}
			}
		}


		/// <summary>
		///     Used to disable the axis controls if an ENG joystick without a stick is selected.
		/// </summary>
		public bool AxisControlsEnabled
		{
			get => _axisControlsEnabled;
			set
			{
				Set(ref _axisControlsEnabled, value, nameof(AxisControlsEnabled));
			}
		}


		/// <summary>
		///     Displays the joystick settings if true.
		/// </summary>
		public bool JoystickVisible
		{
			get => _joystickVisible;
			set
			{
				Set(ref _joystickVisible, value, nameof(JoystickVisible));

				//to remember the last selected tab.
				DeselectTabs();

				if (value)
				{
					KeyVisible = false;
					SelectedTab[_lastSelectedAxis] = true;
				}
			}
		}

		/// <summary>
		///     Displays the key settings if true.
		/// </summary>
		public bool KeyVisible
		{
			get => _keyVisible;
			set
			{
				Set(ref _keyVisible, value, nameof(KeyVisible));

				if (value)
				{
					JoystickVisible = false;
				}
			}
		}

		/// <summary>
		///     Which joystick axis is selected. 4 items, first index is a dummy.
		/// </summary>
		public ObservableCollection<bool> SelectedTab
		{
			get => _selectedTab;

			set => Set(ref _selectedTab, value, nameof(SelectedTab));
		}

		/// <summary>
		///     A bank of virtual axes. The first key is the device number,
		///     the value is a dictionary whose key is the virtual axis index.
		///     For example, device # 2 (the first key) has 2 possible virtual axes,
		///     index 1 -> Virtual axis with angle of 28.5, and index 2 -> virtual axis
		///     with ratio of 1:5.
		/// </summary>
		private Dictionary<int, Dictionary<int, string>> VirtualAxesCollection
		{
			get => _virtualAxesCollection;

			set => Set(ref _virtualAxesCollection, value, nameof(VirtualAxesCollection));
		}

		/// <summary>
		///     A bank of lockstep configs. The first key is the device number,
		///     the value is a dictionary whose key is the lockstep index.
		/// </summary>
		private Dictionary<int, Dictionary<int, string>> LockstepCollection
		{
			get => _lockstepCollection;

			set => Set(ref _lockstepCollection, value, nameof(LockstepCollection));
		}

		/// <summary>
		///     The X-JOY button resembling panel that is shown on the left side of the plugin.
		/// </summary>
		public XJoyPanelVM XJoyPanel
		{
			get => _xJoyPanel;

			set => Set(ref _xJoyPanel, value, nameof(XJoyPanel));
		}

		/// <summary>
		///     The text that helps guide users when the plugin isn't fully instantiated.
		/// </summary>
		public string DisplayInformation
		{
			get => _displayInformation;

			set => Set(ref _displayInformation, value, nameof(DisplayInformation));
		}

		/// <summary>
		///     When this is true, a brief notification appears signalling an error on the view.
		/// </summary>
		public bool ErrorOccured
		{
			get => _errorOccured;

			set
			{
				Set(ref _errorOccured, value, nameof(ErrorOccured));

				//We want to toggle this flag, never leave it to true.
				//The storyboard action for popping up a message is triggered
				//on a false to true flag.
				if (value)
				{
					ErrorOccured = false;
				}
			}
		}

		/// <summary>
		///     This is the visible query message only, does not affect underlying behavior.
		///     Shows a little "Querying" blurb on the JoystickControl during the initial query.
		/// </summary>
		public bool IsQueryingVisible
		{
			get => _isQueryingVisible;

			set => _dispatcher.Invoke(() => { Set(ref _isQueryingVisible, value, nameof(IsQueryingVisible)); });
		}

		/// <summary>
		///     This is the flag that tells this class to start parsing key info messages.
		///     This is used in both binary and ascii to parse key info messages via
		///     <see cref="Joystick_MessageReceived(object, DeviceMessageEventArgs)" /> and
		///     <see cref="DataPacket_Received(object, DataPacketEventArgs)" />
		/// </summary>
		private bool IsQuerying
		{
			get => _isQueryingMember;

			set
			{
				_isQueryingMember = value;

				// We need message IDs enabled while querying the key commands. 
				if (Conversation != null)
				{
					if (value)
					{
						_previousAsciiMessageIdState = Conversation.Device.AreAsciiMessageIdsEnabled;
						Conversation.Device.AreAsciiMessageIdsEnabled = true;
					}
					else
					{
						// Restore user message ID state.
						if (_previousAsciiMessageIdState.HasValue)
						{
							Conversation.Device.AreAsciiMessageIdsEnabled = _previousAsciiMessageIdState.Value;
							_previousAsciiMessageIdState = null;
						}
					}
				}
			}
		}

		/// <summary>
		///     Readonly fields that can be checked to see if the plugin is querying or not.
		/// </summary>
		public int NumberOfBackgroundWorkers { get; private set; }

		public int PacketsReceivedBeforeFinish { get; private set; }

		public int BinaryCounter { get; private set; }

		public int InitialCounter { get; private set; }

		#endregion

		#region -- Data --

		private const string DISCONNECTED_MSG = "Connect a Zaber joystick and open the port to start using this tab.";
		private const string UNSELECTED_MSG = "Select a Zaber joystick device in the upper panel.";

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Joystick tab");

		private Protocol _protocol;

		private bool _isPluginSelected;

		//members that dictate the visibility view logic.
		private bool _controlsEnabled;
		private bool _axisControlsEnabled;
		private bool _joystickVisible;
		private bool _keyVisible;
		private int _keySelected;
		private int _lastSelectedAxis;
		private string _displayInformation = DISCONNECTED_MSG;
		private bool _errorOccured;
		private bool? _previousAsciiMessageIdState;

		//member variables that represent data. some data will be bound to the views e.g. _keyDisplay and the 
		private KeyVM _keyDisplay;
		private XJoyPanelVM _xJoyPanel;

		private ObservableCollection<bool> _selectedTab = new ObservableCollection<bool>
		{
			false,
			true,
			false,
			false
		};

		private ObservableCollection<JoystickVM> _joysticks = new ObservableCollection<JoystickVM>();
		private ObservableCollection<KeyVM> _keys = new ObservableCollection<KeyVM>();
		private ZaberPortFacade _portFacade;
		private Conversation _conversation;

		private readonly IDispatcher
			_dispatcher =
				DispatcherHelper.Dispatcher; //this is just a wrapper interface to allow us to mock the dispatcher.

		private List<ZaberDevice> _connectedDevices = new List<ZaberDevice>();

		private Dictionary<int, Dictionary<int, string>> _virtualAxesCollection =
			new Dictionary<int, Dictionary<int, string>>();

		private Dictionary<int, Dictionary<int, string>> _lockstepCollection =
			new Dictionary<int, Dictionary<int, string>>();

		private readonly ObservableCollection<CommandInfoVM>
			_binaryCommands = new ObservableCollection<CommandInfoVM>();

		//member variables for querying information.
		private bool _isQueryingVisible;
		private bool _isQueryingMember;

		private int _originalConversationTimeout = 100;

		//Maps from a message ID to a number that has the key event and key # encoded into it.
		private readonly Dictionary<int, int> _messageIdToKeyMap = new Dictionary<int, int>();

		//Maps from a message id to a list of ASCII key commands.
		private readonly Dictionary<int, List<string>> _messageIDcontainer = new Dictionary<int, List<string>>();
		private readonly object _backgroundWorkerLock = new object();

		private bool
			_errorFreeQueryingMode; //when this is false, errors will be shown when the user manages to get this plugin to send rejected commands.

		private AutoResetEvent _waitForJoystickMessage = new AutoResetEvent(false);

		public event RequestDialogChange RequestDialogOpen;
		#pragma warning disable CS0067 // Event is never used.
		public event RequestDialogChange RequestDialogClose;
		#pragma warning restore CS0067

		//constants
		public const int TJOY3_DEVICE_ID = 3003;
		public const int MAX_NUMBER_BACKGROUND_WORKERS = 9; // Do not reduce this number below 9.
		public const int NUMBER_OF_XJOY_KEYS = 8;
		public const int NUMBER_OF_TJOY_KEYS = 5;
		public const int NUMBER_OF_KEY_EVENTS = 4;

		#endregion
	}
}

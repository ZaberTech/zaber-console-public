﻿using MvvmDialogs.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Zaber;
using ZaberWpfToolbox;

/// <summary>
/// The modal window that asks users whether they want to proceed with
/// the restore command or not.
/// </summary>
namespace ZaberConsole.Plugins.Joystick
{
    public class RestoreModalWindowVM : IUserDialogViewModel
    {
        public RestoreModalWindowVM(JoystickPluginVM parent, Protocol protocol)
        {
            _protocol = protocol;
            _conversation = parent.Conversation;
            _parent = parent;
        }

        public bool IsModal
        {
            get
            {
                return true;
            }
        }

        public ICommand CloseWindowCommand
        {
            get { return new RelayCommand(_ => RequestClose()); }
        }

        public ICommand RestoreJoystickCommand
        {
            get { return new RelayCommand(_ => RestoreJoystick()); }
        }

        public void RequestBringToFront()
        {
            BringToFront?.Invoke(this, null);
        }

        public void RequestClose()
        {
            DialogClosing?.Invoke(this, null);
        }

        public void RestoreJoystick()
        {
            //throws a Zaber.RequestTimeoutException 
            try
            {
                if (_protocol == Protocol.ASCII)
                {
                    _conversation.Request("system restore");
                }

                else
                {
                    _conversation.Request(Command.RestoreSettings);
                }

                string portName = _parent.PortFacade.Port.PortName;
                _parent.PortFacade.Close();
                _parent.PortFacade.Open(portName);
            }
            catch
            {
                //refresh the plugin
                string portName = _parent.PortFacade.Port.PortName;
                _parent.PortFacade.Close();
                _parent.PortFacade.Open(portName);
            }

            finally
            {
                RequestClose();
            }
        }

        public event EventHandler BringToFront;
        public event EventHandler DialogClosing;

        private JoystickPluginVM _parent;
        private Protocol _protocol;
        private Conversation _conversation;
    }
}

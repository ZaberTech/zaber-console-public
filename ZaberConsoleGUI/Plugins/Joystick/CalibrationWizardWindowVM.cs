﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using MvvmDialogs.ViewModels;
using Zaber;
using Zaber.Telemetry;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Dialogs;

namespace ZaberConsole.Plugins.Joystick
{
	/// <summary>
	///     This is the calibration wizard's view model.
	/// </summary>
	public class CalibrationWizardWindowVM : ObservableObject, IUserDialogViewModel, IDialogClient
	{
		#region -- Public Methods & Properties --

		public CalibrationWizardWindowVM(Protocol protocol, Conversation conversation)
		{
			_protocol = protocol;
			_conversation = conversation;
		}

		#endregion

		#region --Commands and Private Methods--

		/// <summary>
		///     Sends a request to the joystick to start calibrating.
		/// </summary>
		public ICommand CalibrationCommand => new RelayCommand(_ => CalibrationMethod());

		/// <summary>
		///     Saves the calibration settings.
		/// </summary>
		public ICommand SaveCommand => new RelayCommand(_ => SaveMethod());

		/// <summary>
		///     Closes this window.
		/// </summary>
		public ICommand ExitCommand => new RelayCommand(_ => ExitMethod());


		/// <summary>
		///     Invoked when the user clicks "Start Calibrating".
		/// </summary>
		private void CalibrationMethod()
		{
			NotCalibrating = false;
			CanCalibrate = false;
			CanSave = true;

			if (DeadbandsToggled)
			{
				ShowDeadbandsInstruction = true;
			}

			else
			{
				ShowLimitsInstruction = true;
			}

			if (_protocol == Protocol.ASCII)
			{
				if (DeadbandsToggled)
				{
					_conversation.Request("joystick calibrate deadbands start");
				}

				if (LimitsToggled)
				{
					_conversation.Request("joystick calibrate limits start");
				}
			}

			if (_protocol == Protocol.Binary)
			{
				if (DeadbandsToggled)
				{
					_conversation.Request(Command.SetCalibrationMode, 2);
				}

				if (LimitsToggled)
				{
					_conversation.Request(Command.SetCalibrationMode, 1);
				}
			}
		}


		/// <summary>
		///     Save the current calibration settings onto the device.
		/// </summary>
		private void SaveMethod()
		{
			NotCalibrating = true;
			CanCalibrate = true;
			CanSave = false;
			QuerySaved = true;
			ShowLimitsInstruction = false;
			ShowDeadbandsInstruction = false;

			if (_protocol == Protocol.ASCII)
			{
				if (DeadbandsToggled)
				{
					_conversation.Request("joystick calibrate deadbands save");
				}

				if (LimitsToggled)
				{
					_conversation.Request("joystick calibrate limits save");
				}
			}

			if (_protocol == Protocol.Binary)
			{
				_conversation.Request(Command.SetCalibrationMode, 0);
			}

			_eventLog.LogEvent("Calibration wizard used");
		}


		/// <summary>
		///     We pop up a modal dialog to ask the user to confirm if they want
		///     to exit in the midst of calibrating.
		/// </summary>
		private void ExitMethod()
		{
			if (!NotCalibrating)
			{
				var calibrationModalWindow = new CustomMessageBoxVM(
					"Your joystick device is currently calibrating. If you exit this window, The calibration process will stop and the settings will be saved.",
					"Cancel Calibration",
					"OK",
					"Cancel");

				calibrationModalWindow.DialogClosing += (anObject, anEventArgs) =>
				{
					if (MessageBoxResult.Yes == (MessageBoxResult) calibrationModalWindow.DialogResult)
					{
						SaveMethod();
					}

					if (MessageBoxResult.No == (MessageBoxResult) calibrationModalWindow.DialogResult)
					{
						CancelEvent.Cancel = true;
					}
				};

				RequestDialogOpen?.Invoke(this, calibrationModalWindow);
			}

			else
			{
				RequestClose();
			}
		}

		#endregion

		#region --Properties--

		/// <summary>
		///     This window is modal.
		/// </summary>
		public bool IsModal => true;


		/// <summary>
		///     Close this dialog.
		/// </summary>
		public void RequestClose() => DialogClosing?.Invoke(this, null);


		/// <summary>
		///     Bring this dialog to the front.
		/// </summary>
		public void RequestBringToFront() => BringToFront?.Invoke(this, null);


		/// <summary>
		///     If the "Introduction" toggle button is toggled.
		/// </summary>
		public bool IntroductionToggled
		{
			get => _introductionToggled;

			set
			{
				Set(ref _introductionToggled, value, nameof(IntroductionToggled));
				CanCalibrate = IntroductionToggled ? false : true;
			}
		}

		/// <summary>
		///     If the "Limits" toggle button is toggled.
		/// </summary>
		public bool LimitsToggled
		{
			get => _limitsToggled;

			set
			{
				Set(ref _limitsToggled, value, nameof(LimitsToggled));
				CanCalibrate = IntroductionToggled ? false : true;
			}
		}

		/// <summary>
		///     If the "Deadbands" toggle button is toggled.
		/// </summary>
		public bool DeadbandsToggled
		{
			get => _deadbandsToggled;

			set
			{
				Set(ref _deadbandsToggled, value, nameof(DeadbandsToggled));
				CanCalibrate = IntroductionToggled ? false : true;
			}
		}

		/// <summary>
		///     Specifies whether the user can click on the calibrate button.
		/// </summary>
		public bool CanCalibrate
		{
			get => _canCalibrate;

			set => Set(ref _canCalibrate, value, nameof(CanCalibrate));
		}

		/// <summary>
		///     Specifices whether the user is not calibrating.
		///     Disables and enables the navigation buttons.
		/// </summary>
		public bool NotCalibrating
		{
			get => _notCalibrating;

			set => Set(ref _notCalibrating, value, nameof(NotCalibrating));
		}

		/// <summary>
		///     Specifies whether the save button is enabled.
		/// </summary>
		public bool CanSave
		{
			get => _canSave;

			set => Set(ref _canSave, value, nameof(CanSave));
		}

		/// <summary>
		///     Fires a pop-up message if the the calibration was saved.
		/// </summary>
		public bool QuerySaved
		{
			get => _querySaved;

			set
			{
				Set(ref _querySaved, value, nameof(QuerySaved));

				//This functions only as a data trigger for the true value, so if we want it to trigger again we always have to set 
				//it to false right afterwards.
				if (value)
				{
					QuerySaved = false;
				}
			}
		}

		/// <summary>
		///     If the instructions for calibrating limits are visible.
		/// </summary>
		public bool ShowLimitsInstruction
		{
			get => _showLimitsInstruction;

			set => Set(ref _showLimitsInstruction, value, nameof(ShowLimitsInstruction));
		}

		/// <summary>
		///     If the instructions for calibrating deadbands are visible.
		/// </summary>
		public bool ShowDeadbandsInstruction
		{
			get => _showDeadbandsInstruction;

			set => Set(ref _showDeadbandsInstruction, value, nameof(ShowDeadbandsInstruction));
		}

		/// <summary>
		///     Very hacky solution to make the wizard close after the modal window pops up. Pass a CancelEventArgs from the
		///     attached behavior
		///     found in PassCloseEventBehavior.cs to this VM so that this event is cancelable. Trying to handle this
		///     event from an attached behavior does not work because ExitMethod() will be called after the the event is handled.
		/// </summary>
		public CancelEventArgs CancelEvent { get; set; }


		/// <summary>
		///     Any exception that was unhandled by the dialog itself.
		/// </summary>
		public Exception Exception { get; set; }

		#endregion

		#region --Data--

		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Joystick tab");

		private bool _showLimitsInstruction;
		private bool _showDeadbandsInstruction;
		private bool _querySaved;
		private bool _canSave;
		private bool _notCalibrating = true;
		private bool _canCalibrate;
		private bool _introductionToggled = true;
		private bool _limitsToggled;
		private bool _deadbandsToggled;
		private readonly Protocol _protocol;
		private readonly Conversation _conversation;

		public event EventHandler DialogClosing;

		public event EventHandler BringToFront;

		public event RequestDialogChange RequestDialogOpen;
		#pragma warning disable CS0067 // Event is never used.
		public event RequestDialogChange RequestDialogClose;
		#pragma warning restore CS0067

		#endregion
	}
}

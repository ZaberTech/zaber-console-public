﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using MvvmDialogs.ViewModels;
using Zaber;
using Zaber.Telemetry;
using ZaberConsole.Plugins.Joystick.Key;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.Joystick
{
	/// <summary>
	///     The VM for the window that does the key clearing.
	/// </summary>
	public class ClearKeysWindowVM : ObservableObject, IUserDialogViewModel
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Constructor that gets reference to the plugin
		///     to access its conversation.
		/// </summary>
		/// <param name="parentPlugin">The parent plugin whose keys will be cleared.</param>
		/// <param name="protocol">ASCII or Binary.</param>
		public ClearKeysWindowVM(JoystickPluginVM parentPlugin, Protocol protocol)
		{
			for (var i = 1; i < 9; i++)
			{
				KeysToDelete.Add(i, false);
			}

			_parentPlugin = parentPlugin;
			_protocol = protocol;
		}

		#endregion

		#region --Commands and Private Methods--

		/// <summary>
		///     Clears the selected keys.
		/// </summary>
		public ICommand ClearCommand
		{
			get
			{
				if (_protocol == Protocol.ASCII)
				{
					return new RelayCommand(_ => ClearAscii());
				}

				return new RelayCommand(_ => ClearBinary());
			}
		}

		/// <summary>
		///     Closes this window.
		/// </summary>
		public ICommand CloseCommand => new RelayCommand(_ => RequestClose());

		/// <summary>
		///     Selects or deselects all of the checkboxes.
		/// </summary>
		public ICommand SelectAllCommand => new RelayCommand(isChecked => SelectAll(isChecked));


		/// <summary>
		///     Select all or deselect all of the checkboxes
		///     depending on the state of the checkbox that toggled this.
		/// </summary>
		/// <param name="sender"></param>
		private void SelectAll(object sender)
		{
			var isChecked = (bool) sender;

			if (isChecked)
			{
				for (var i = 1; i < 9; i++)
				{
					KeysToDelete[i] = true;
				}
			}

			else
			{
				for (var i = 1; i < 9; i++)
				{
					KeysToDelete[i] = false;
				}
			}

			//manually notify changes to the window
			OnPropertyChanged(nameof(KeysToDelete));
		}


		/// <summary>
		///     Clear the Binary key commands.
		/// </summary>
		private void ClearBinary()
		{
			var numKeys = 8;

			if (_parentPlugin.Conversation.Device.DeviceType.DeviceId == JoystickPluginVM.TJOY3_DEVICE_ID)
			{
				numKeys = JoystickPluginVM.NUMBER_OF_TJOY_KEYS;
			}

			for (var i = 1; i < (numKeys + 1); i++)
			{
				if (KeysToDelete[i])
				{
					for (var j = 1; j < 5; j++)
					{
						var binaryInfo = _parentPlugin.Keys[i].KeyCommandGrids[j].BinaryInfo;

						//Setting a key to 255 Error 0 is the same as clearing it, effectively.
						binaryInfo.BinaryCommand = ((byte) Command.Error).ToString();
						binaryInfo.BinaryData = "0";
						binaryInfo.TargetDeviceNumber = KeyCommandGridVM.ALL_DEVICES;
					}

					_parentPlugin.WriteKeyInfoCommand.Execute(_parentPlugin.Keys[i]);
				}
			}

			for (var i = 1; i < 9; i++)
			{
				KeysToDelete[i] = false;
			}

			IsChecked = false;

			//manually notify changes to the window
			OnPropertyChanged(nameof(KeysToDelete));
			OnPropertyChanged(nameof(IsChecked));

			_eventLog.LogEvent("Clear keys dialog used");
		}


		/// <summary>
		///     Clear the key commands in ASCII.
		/// </summary>
		private void ClearAscii()
		{
			if (!KeysToDelete.ContainsValue(false))
			{
				_parentPlugin.Conversation.Request("key clear all");

				for (var i = 1; i < 9; i++)
				{
					//We use ReadKeyInfoBody instead of ReadKeyInfo because it is synchronous. It
					//is better to have a delay after clearing.
					_parentPlugin.ReadKeyInfoBodyAscii(_parentPlugin.Keys[i]);
				}
			}

			else
			{
				for (var i = 1; i < 9; i++)
				{
					if (KeysToDelete[i])
					{
						for (var j = 1; j < 5; j++)
						{
							_parentPlugin.Conversation.Request("key " + i + " " + j + " clear");
						}

						_parentPlugin.ReadKeyInfoBodyAscii(_parentPlugin.Keys[i]);
					}
				}
			}

			for (var i = 1; i < 9; i++)
			{
				KeysToDelete[i] = false;
			}

			IsChecked = false;

			//manually notify changes
			OnPropertyChanged(nameof(KeysToDelete));
			OnPropertyChanged(nameof(IsChecked));

			_eventLog.LogEvent("Clear keys dialog used");
		}

		#endregion

		#region --IUserDialogViewModel Implementation--

		public bool IsModal => true;

		public event EventHandler BringToFront;

		public event EventHandler DialogClosing;


		public void RequestBringToFront() => BringToFront?.Invoke(this, null);


		public void RequestClose() => DialogClosing?.Invoke(this, null);


		/// <summary>
		///     Any exception that was unhandled by the dialog itself.
		/// </summary>
		public Exception Exception { get; set; }

		#endregion

		#region --Data and Properties--

		/// <summary>
		///     Maps the key number to a boolean of whether
		///     the key should be cleared or not.
		/// </summary>
		public Dictionary<int, bool> KeysToDelete { get; set; } = new Dictionary<int, bool>();

		/// <summary>
		///     The "Select All" checkbox's state.
		/// </summary>
		public bool IsChecked { get; set; }

		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Joystick tab");

		private readonly Protocol _protocol;
		private readonly JoystickPluginVM _parentPlugin;

		#endregion
	}
}

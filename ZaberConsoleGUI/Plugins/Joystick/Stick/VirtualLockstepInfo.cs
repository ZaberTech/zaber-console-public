﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.Joystick.Stick
{
    class VirtualLockstepInfo : ObservableObject
    {

        public string Device
        {
            get { return _device; }
            set
            {
                Set(ref _device, value, () => Device);
            }
        }

        public string Name
        {
            get {; return _name; }
            set
            {
                Set(ref _name, value, () => Name);
            }
        }

        public string Axes
        {
            get {; return _axes; }
            set
            {
                Set(ref _axes, value, () => Axes);
            }
        }

        public string AngleOrRatio
        {
            get {; return _angleOrRatio; }
            set
            {
                Set(ref _angleOrRatio, value, () => AngleOrRatio);
            }
        }

        public string ID
        {
            get {; return _id; }
            set
            {
                Set(ref _id, value, () => ID);
            }
        }

        public string Type
        {
            get {; return _type; }
            set
            {
                Set(ref _type, value, () => Type);
            }
        }

        private string _name = "Descriptive Axis Name";
        private string _angleOrRatio = "45 degrees / 1:5";
        private string _axes = "Reference : 1, Secondary : 2";
        private string _device = "X-LRM";
        private string _id = "1";
        private string _type = "Virtual";
    }
}

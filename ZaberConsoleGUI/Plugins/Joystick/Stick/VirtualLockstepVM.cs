﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.Joystick.Stick
{
    public class VirtualLockstepVM : ObservableObject
    {

        public VirtualLockstepGridVM VirtualLockstepGridVM
        {
            get { return _virtualLockstepGridVM; }
            set
            {
                Set(ref _virtualLockstepGridVM, value, () => VirtualLockstepGridVM);
            }
        }

        private VirtualLockstepGridVM _virtualLockstepGridVM = new VirtualLockstepGridVM();
    }
}

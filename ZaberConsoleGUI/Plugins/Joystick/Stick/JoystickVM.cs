﻿using System;
using System.Collections.Generic;
using Zaber;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.Joystick.Stick
{
	/// <summary>
	///     This class represents all of the information and logic behind the joystick control.
	/// </summary>
	public class JoystickVM : ObservableObject
	{
		#region -- Public Methods & Properties --

		public JoystickVM(string JoystickAxis, Protocol protocol)
		{
			_joystickAxisNumber = JoystickAxis;
			ControlSpeeds.Add("Linear");
			ControlSpeeds.Add("Squared (Default)");
			ControlSpeeds.Add("Cubed");

			if (protocol == Protocol.ASCII)
			{
				IsAsciiMode = true;
			}

			else
			{
				IsAsciiMode = false;
				TargetVirtualAxisInfo = "Not Available in Binary";
				TargetAxisDeviceName = "Not Available in Binary";
				TargetLockstepInfo = "Not Available in Binary";
			}
		}

		#endregion

		#region --Public Methods--

		/// <summary>
		///     Parses an ASCII joystick info response into data.
		/// </summary>
		/// <param name="response">
		///     A string such as "OK IDLE -- 2 1 10000 2 1 50".
		///     Look at the ASCII protocol manual for explanation of values.
		/// </param>
		public void ParseJoystickInfo(string response)
		{
			//expect a response like "OK IDLE -- 2 1 10000 2 1 50"
			//                                   ^ 11th index.
			var coreInfo = response.Substring(11);
			var joystickInfo = coreInfo.Split(' ');
			OldInfo = new JoystickData(true, joystickInfo);
			ViewInfo = new JoystickData(false, joystickInfo);

			ViewInfo.PropertyChanged += ViewInfo_PropertyChanged;
			ViewInfo.InversionChanged += ViewInfo_InversionChanged;
			ViewInfo.SmartTargetEvent += SmartTarget;

			//These event aren't hooked up until after initialization, so we have to manually invoke them.
			GetDeviceInfo();
			ViewInfo_PropertyChanged(null, null);
			ViewInfo_InversionChanged(null, null);

			LastReadTime = DateTime.Now.ToShortTimeString();
		}


		/// <summary>
		///     Parses a Binary joystick information response into data.
		/// </summary>
		/// <param name="binaryInfo">
		///     The data in a string array. The data is in order as:
		///     Target Device #, Speed profile, Max speed, and inversion.
		/// </param>
		public void ParseJoystickInfo(string[] binaryInfo)
		{
			//Populate the _oldInfo field and the ViewInfo property.
			OldInfo = new JoystickData(true, binaryInfo);
			ViewInfo = new JoystickData(false, binaryInfo);

			ViewInfo.PropertyChanged += ViewInfo_PropertyChanged;
			ViewInfo.InversionChanged += ViewInfo_InversionChanged;
			ViewInfo.SmartTargetEvent += SmartTarget;

			OldInfo.Resolution = "N/A";
			ViewInfo.Resolution = "N/A";

			//These event aren't hooked up until after initialization, so we have to manually invoke them.
			GetDeviceInfo();
			ViewInfo_PropertyChanged(null, null);
			ViewInfo_InversionChanged(null, null);

			LastReadTime = DateTime.Now.ToShortTimeString();
		}


		/// <summary>
		///     The event handler for the SmartTargetEvent in the <see cref="ViewInfo" />
		/// </summary>
		/// <param name="sender">Not used</param>
		/// <param name="e">Holds a SmartTargetMethod argument that is used to determine what method to call.</param>
		private void SmartTarget(object aSender, SmartTargetArgs aSmartTargetArg)
		{
			//We don't care about axis smart targeting in Binary mode.
			if ((SmartTargetMethod.Axis == aSmartTargetArg.MethodType) && IsAsciiMode)
			{
				GetAxisInfo();
			}

			if (SmartTargetMethod.Device == aSmartTargetArg.MethodType)
			{
				GetDeviceInfo();
			}

			if ((SmartTargetMethod.Virtual == aSmartTargetArg.MethodType) && IsAsciiMode)
			{
				SetVirtualAxisInfo();
			}

			if ((SmartTargetMethod.Lockstep == aSmartTargetArg.MethodType) && IsAsciiMode)
			{
				SetLockstepInfo();
			}
		}


		/// <summary>
		///     Bubbles the InversionChanged event received from the <see cref="ViewInfo" /> up to the JoystickPluginVM.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ViewInfo_InversionChanged(object sender, EventArgs e) => InversionChanged?.Invoke(this, null);


		/// <summary>
		///     Event Handler for the PropertyChanged event from the <see cref="ViewInfo" />. This
		///     Calls the <see cref="JoystickDataEquals(JoystickData, JoystickData)" /> method
		///     to automatically change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ViewInfo_PropertyChanged(object sender, EventArgs e)
		{
			if (!ViewInfo.IsOldData)
			{
				HasUnsavedChanges = !JoystickDataEquals(ViewInfo, OldInfo);
			}
		}


		/// <summary>
		///     Sets the target virtual axis information.
		/// </summary>
		public void SetVirtualAxisInfo()
		{
			if (!_connectedDeviceFound || (null == ViewInfo.VirtualIndex) || AllDevicesTargeted)
			{
				return;
			}

			//If the target device # isn't a connected device, the virtual axis information is set to
			//"Not Set-up" by default through GetDeviceInfo().
			if (!VirtualAxesCollection.ContainsKey(_targetDevice.DeviceNumber))
			{
				return;
			}

			var deviceVirtualCollection = VirtualAxesCollection[_targetDevice.DeviceNumber];

			if (deviceVirtualCollection.ContainsKey(int.Parse(ViewInfo.VirtualIndex)))
			{
				TargetVirtualAxisInfo = deviceVirtualCollection[int.Parse(ViewInfo.VirtualIndex)];
			}

			else
			{
				TargetVirtualAxisInfo = "Not Set-up";
			}
		}


		/// <summary>
		///     Sets the smart detection for the lockstep axis.
		/// </summary>
		public void SetLockstepInfo()
		{
			if (!_connectedDeviceFound || (ViewInfo.LockstepIndex == null) || AllDevicesTargeted)
			{
				return;
			}

			//If the target device # isn't a connected device, the lockstep information is set to
			//"Not Set-up" by default through GetDeviceInfo().
			if (!LockstepCollection.ContainsKey(_targetDevice.DeviceNumber))
			{
				return;
			}

			var deviceLockstepCollection = LockstepCollection[_targetDevice.DeviceNumber];

			if (deviceLockstepCollection.ContainsKey(int.Parse(ViewInfo.LockstepIndex)))
			{
				TargetLockstepInfo = deviceLockstepCollection[int.Parse(ViewInfo.LockstepIndex)];
			}

			else
			{
				TargetLockstepInfo = "Not Set-up";
			}
		}

		#endregion

		#region --Private Methods--

		/// <summary>
		///     Connected device detection method. Checks if the device number has an associated
		///     connected device and displays the name.
		/// </summary>
		private void GetDeviceInfo()
		{
			if (ViewInfo.TargetDeviceNumber == null)
			{
				_connectedDeviceFound = false;
				return;
			}

			//Try and find the target device name.
			var deviceNumberInt = int.Parse(ViewInfo.TargetDeviceNumber);
			_connectedDeviceFound = false;

			foreach (var zaberDevice in ConnectedDevices)
			{
				if (zaberDevice.DeviceNumber == deviceNumberInt)
				{
					_connectedDeviceFound = true;
					TargetDeviceName = zaberDevice.DeviceType.Name;
					_targetDevice = zaberDevice;
				}
			}

			//No device found; we set all of the axis and target device information.
			if (!_connectedDeviceFound)
			{
				TargetDeviceName = "Not Connected";
				_targetDevice = null;
				if (IsAsciiMode)
				{
					TargetAxisDeviceName = "Not Connected";
					TargetVirtualAxisInfo = "Not Set-up";
					TargetLockstepInfo = "Not Set-up";
					AllDevicesTargeted = false;
				}
			}

			else
			{
				if (_targetDevice.DeviceNumber == 0)
				{
					AllDevicesTargeted = true;
				}

				else
				{
					AllDevicesTargeted = false;

					//We don't want to worry about axis info in Binary.
					//It should always be "Not Available in Binary"
					if (IsAsciiMode)
					{
						GetAxisInfo();
						SetVirtualAxisInfo();
						SetLockstepInfo();
					}
				}
			}
		}


		/// <summary>
		///     Sets the correct axis smart detection for the connected device.
		/// </summary>
		private void GetAxisInfo()
		{
			var axisFound = false;

			if ((ViewInfo.TargetAxis == null) || AllDevicesTargeted)
			{
				return;
			}

			if (_connectedDeviceFound)
			{
				//We want single axis devices to display "Single Axis Device" instead of "All Axes" 
				//The _targetDevice.Axes contains an "All Axes" axis that will be targeted otherwise.
				if (!int.TryParse(ViewInfo.TargetAxis, out var viewTargetAxis) || (0 == viewTargetAxis))
				{
					TargetAxisDeviceName = _targetDevice.Axes.Count == 1 ? "Single Axis Device" : "All Axes";
					return;
				}

				//Try and iterate through the ZaberDevice.Axes collection to find a match.
				foreach (var axis in _targetDevice.Axes)
				{
					if (axis.AxisNumber == viewTargetAxis)
					{
						TargetAxisDeviceName = axis.DeviceType.Name;
						axisFound = true;

						if (_targetDevice.Axes.Count == 1)
						{
							TargetAxisDeviceName = "Single Axis Device";
						}
					}
				}

				if (!axisFound)
				{
					TargetAxisDeviceName = "Not Connected";
				}
			}
		}


		/// <summary>
		///     This method is used to convert between a selected speed and a number to actually write
		///     to the joystick via the "joystick speedprofile 1|2|3" command.
		/// </summary>
		/// <param name="selectedSpeed"></param>
		/// <returns></returns>
		public static string ConvertToNumber(string selectedSpeed)
		{
			if (selectedSpeed == null)
			{
				return null;
			}

			if (selectedSpeed == "Linear")
			{
				return "1";
			}

			if (selectedSpeed == "Squared (Default)")
			{
				return "2";
			}

			if (selectedSpeed == "Cubed")
			{
				return "3";
			}

			throw new ArgumentException(selectedSpeed + "Not valid");
		}


		/// <summary>
		///     Determines if the <see cref="JoystickVM.HasUnsavedChanges" />
		///     flag should be set.
		/// </summary>
		/// <param name="ViewInfo">The <see cref="JoystickVM.ViewInfo" /></param>
		/// <param name="oldInfo">The <see cref="JoystickVM.OldInfo" /></param>
		/// <returns>True if the data are equal, false otherwise.</returns>
		private bool JoystickDataEquals(JoystickData ViewInfo, JoystickData oldInfo)
		{
			//Compare the old information to the view's information. 
			ViewInfo.TargetDeviceChanged = ViewInfo.TargetDeviceNumber != oldInfo.TargetDeviceNumber;
			ViewInfo.ControlSpeedChanged = ViewInfo.SpeedSelected != oldInfo.SpeedSelected;
			ViewInfo.MaxSpeedChanged = ViewInfo.MaxSpeed != oldInfo.MaxSpeed;
			ViewInfo.InvertChanged = ViewInfo.IsInverted != oldInfo.IsInverted;
			ViewInfo.ResolutionChanged = ViewInfo.Resolution != oldInfo.Resolution;

			//3 sets of logical blocks. We want to check if the viewinfo and the oldinfo are targeting
			//the same axes. If they are, we check further to see if the target axis has actually changed.
			if (ViewInfo.IsTargetingVirtualAxis == oldInfo.IsTargetingVirtualAxis)
			{
				if (ViewInfo.IsTargetingVirtualAxis)
				{
					ViewInfo.TargetVirtualChanged = ViewInfo.VirtualIndex != oldInfo.VirtualIndex;
				}

				else
				{
					ViewInfo.TargetVirtualChanged = false;
				}
			}

			else
			{
				ViewInfo.TargetVirtualChanged = true;
			}

			if (ViewInfo.IsTargetingLockstep == oldInfo.IsTargetingLockstep)
			{
				if (ViewInfo.IsTargetingLockstep)
				{
					ViewInfo.TargetLockstepChanged = ViewInfo.LockstepIndex != oldInfo.LockstepIndex;
				}

				else
				{
					ViewInfo.TargetLockstepChanged = false;
				}
			}

			else
			{
				ViewInfo.TargetLockstepChanged = true;
			}

			if (ViewInfo.IsTargetingNormalAxis == oldInfo.IsTargetingNormalAxis)
			{
				if (ViewInfo.IsTargetingNormalAxis)
				{
					ViewInfo.TargetAxisChanged = ViewInfo.TargetAxis != oldInfo.TargetAxis;
				}

				else
				{
					ViewInfo.TargetAxisChanged = false;
				}
			}

			else
			{
				ViewInfo.TargetAxisChanged = true;
			}

			//Return false if any of these changed flags are true.
			if (ViewInfo.TargetDeviceChanged
			|| ViewInfo.ControlSpeedChanged
			|| ViewInfo.MaxSpeedChanged
			|| ViewInfo.InvertChanged
			|| ViewInfo.ResolutionChanged
			|| ViewInfo.TargetVirtualChanged
			|| ViewInfo.TargetLockstepChanged
			|| ViewInfo.TargetAxisChanged)
			{
				return false;
			}

			return true;
		}

		#endregion

		#region --Getters and Setters--

		/// <summary>
		///     The speed profile (linear, squared, cubed) of a joystick mapped
		///     to a number so that it can be written to the joystick.
		/// </summary>
		public string SpeedProfile => ConvertToNumber(ViewInfo.SpeedSelected);

		/// <summary>
		///     A list of speed profiles (linear, squared, cubed) that will be
		///     bound to the combobox.
		/// </summary>
		public List<string> ControlSpeeds
		{
			get => _controlSpeeds;
			set => Set(ref _controlSpeeds, value, nameof(ControlSpeeds));
		}

		/// <summary>
		///     Which joystick axis these settings correspond to, i.e horizontal, vertical, and circular joystick movements.
		///     Not to be confused with <see cref="JoystickData.TargetAxis" />which represents the axis number of the device this
		///     device is targeting.
		/// </summary>
		public string JoystickAxisNumber
		{
			get => _joystickAxisNumber;

			set => Set(ref _joystickAxisNumber, value, nameof(JoystickAxisNumber));
		}

		/// <summary>
		///     The name of the targeted device that the smart targeting
		///     mechanism will display.
		/// </summary>
		public string TargetDeviceName
		{
			get => _targetDeviceName;

			set => Set(ref _targetDeviceName, value, nameof(TargetDeviceName));
		}

		/// <summary>
		///     The name of the targeted device's axis that the smart targeting
		///     mechanism will display.
		/// </summary>
		public string TargetAxisDeviceName
		{
			get => _targetAxisDeviceName;

			set => Set(ref _targetAxisDeviceName, value, nameof(TargetAxisDeviceName));
		}

		/// <summary>
		///     The information of the device's virtual axis
		///     this joystick is targeting.
		/// </summary>
		public string TargetVirtualAxisInfo
		{
			get => _targetVirtualAxisInfo;

			set => Set(ref _targetVirtualAxisInfo, value, nameof(TargetVirtualAxisInfo));
		}

		/// <summary>
		///     The information of the device's lockstep
		///     that this joystick is targeting.
		/// </summary>
		public string TargetLockstepInfo
		{
			get => _targetLockstepInfo;

			set => Set(ref _targetLockstepInfo, value, nameof(TargetLockstepInfo));
		}

		/// <summary>
		///     The actual zaber device that this
		///     joystick is targeting.
		/// </summary>
		public ZaberDevice TargetDevice
		{
			get => _targetDevice;

			set => Set(ref _targetDevice, value, nameof(TargetDevice));
		}

		/// <summary>
		///     All of the connected devices that is set when this view model is initialized.
		/// </summary>
		public List<ZaberDevice> ConnectedDevices { get; set; }

		/// <summary>
		///     A collection of virtual axes that is obtained by querying
		///     the devices on reading. The key is the device number,
		///     the value is a mapping of virtual indices to information.
		/// </summary>
		public Dictionary<int, Dictionary<int, string>> VirtualAxesCollection { get; set; }

		/// <summary>
		///     A collection of lockstep axes that is obtained by querying
		///     the devices on reading. The key is the device number,
		///     the value is a mapping of lockstep indices to information.
		/// </summary>
		public Dictionary<int, Dictionary<int, string>> LockstepCollection { get; set; }

		/// <summary>
		///     The value that should be written to the joystick to specify the
		///     inversion of the joystick. We are mapping a true or false value to a
		///     number that represents the joystick inversion state when written.
		/// </summary>
		public string Inversion
		{
			get
			{
				//Blame the ASCII and Binary protocol for this madness, not me
				if (ViewInfo.IsInverted)
				{
					if (IsAsciiMode)
					{
						return "1";
					}

					return "-1";
				}

				if (IsAsciiMode)
				{
					return "0";
				}

				return "1";
			}
		}

		/// <summary>
		///     Sets whether the smart targeting
		///     should display "All Devices".
		/// </summary>
		private bool AllDevicesTargeted
		{
			get => _allDevicesTargeted;

			set
			{
				if (value)
				{
					if (IsAsciiMode)
					{
						TargetAxisDeviceName = "All Devices";
						TargetLockstepInfo = "All Devices";
						TargetVirtualAxisInfo = "All Devices";
					}
				}

				_allDevicesTargeted = value;
			}
		}

		/// <summary>
		///     Determines whether or not "Unwritten Changes" will appear
		///     as a warning on the display.
		/// </summary>
		public bool HasUnsavedChanges
		{
			get => _hasUnsavedChanges;

			set => Set(ref _hasUnsavedChanges, value, nameof(HasUnsavedChanges));
		}

		/// <summary>
		///     The time of last reading that will be displayed on the view.
		/// </summary>
		public string LastReadTime
		{
			get => _lastReadTime;

			set => Set(ref _lastReadTime, value, nameof(LastReadTime));
		}

		/// <summary>
		///     Whether this device is in ASCII mode of in Binary mode.
		/// </summary>
		public bool IsAsciiMode { get; set; }

		/// <summary>
		///     The JoystickData that is pulled from the joystick when it is read. This will be
		///     compared against the <see cref="OldInfo" /> to determine if unsaved changes have occured.
		/// </summary>
		public JoystickData OldInfo { get; private set; } = new JoystickData(true, null);

		/// <summary>
		///     Holds all of the viewbound data that corresponds to the joystick settings. Will be compared
		///     against <see cref="OldInfo" /> to determine if unsaved changes have occured.
		/// </summary>
		public JoystickData ViewInfo
		{
			get => _ViewInfo;

			set => Set(ref _ViewInfo, value, nameof(ViewInfo));
		}

		#endregion

		#region --Data--

		public event EventHandler InversionChanged;

		//Initialize these two so that we don't have to do null checks on all methods
		//that initially rely on these.
		private JoystickData _ViewInfo = new JoystickData(true, null);

		private List<string> _controlSpeeds = new List<string>();
		private string _joystickAxisNumber;
		private string _targetLockstepInfo = "Not Set-up";
		private string _targetVirtualAxisInfo = "Not Set-up";
		private string _targetAxisDeviceName = "Not Connected";
		private string _targetDeviceName;

		private bool _connectedDeviceFound;
		private ZaberDevice _targetDevice;
		private bool _allDevicesTargeted;
		private bool _hasUnsavedChanges;
		private string _lastReadTime;

		#endregion
	}
}

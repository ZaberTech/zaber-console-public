﻿using System;
using System.Collections.Generic;
using System.Reflection;
using log4net;
using ZaberWpfToolbox;

/// <summary>
/// Encapsulates all of the information that a single joystick axis would hold.
/// </summary>
namespace ZaberConsole.Plugins.Joystick.Stick
{
	public class JoystickData : ObservableObject
	{
		#region --Public Method and Constructor--

		/// <summary>
		///     Constructor : Sets the data for this class.
		/// </summary>
		/// <param name="isOldData">
		///     Whether this is the old data that was previously read or the new view bound data that can be
		///     changed.
		/// </param>
		/// <param name="joystickInfo">Contains all of the joystick info to be parsed.</param>
		public JoystickData(bool isOldData, string[] joystickInfo)
		{
			if (joystickInfo == null)
			{
				return;
			}

			IsOldData = isOldData;

			//Normal axes have 6 pieces of information,
			//device #, axis #, resolution, maxspeed, speed profile, and inversion.
			if (joystickInfo.Length == 6)
			{
				ParseNormalAxis(joystickInfo);
			}

			//virtual/lockstep axes have 7 pieces. The above 6 + whether it is virtual or lockstep.
			else if (joystickInfo.Length == 7)
			{
				if (Array.Exists(joystickInfo, token => token == "virtual"))
				{
					ParseVirtualAxis(joystickInfo);
				}

				else if (Array.Exists(joystickInfo, token => token == "lockstep"))
				{
					ParseLockstepAxis(joystickInfo);
				}
			}

			//Binary joystick information only has 4 fields. device #, maxspeed, speed profile, and inversion.
			else
			{
				ParseBinaryInfo(joystickInfo);
			}
		}


		/// <summary>
		///     Parses a string array which represents a binary joystick's information.
		/// </summary>
		/// <param name="joystickInfo">
		///     A string array of size 4 with indices representing
		///     target device number, speed profile, maxspeed, and inversion in that order.
		///     <param></param>
		public void ParseBinaryInfo(string[] joystickInfo)
		{
			var inversion = joystickInfo[3];
			var controlSpeed = joystickInfo[1];

			//This is defined in the binary protocol manual for "Set Axis Inversion"
			if (inversion.Equals("-1"))
			{
				IsInverted = true;
			}
			else
			{
				IsInverted = false;
			}

			//This too is defined in the binary protocol manual for "Set Velocity Profile"
			if (controlSpeed.Equals("1"))
			{
				SpeedSelected = "Linear";
			}
			else if (controlSpeed.Equals("3"))
			{
				SpeedSelected = "Cubed";
			}
			else // should equal "2"
			{
				SpeedSelected = "Squared (Default)";
			}

			TargetDeviceNumber = joystickInfo[0];
			MaxSpeed = joystickInfo[2];
		}

		#endregion

		#region --Private Methods--

		/// <summary>
		///     Parse a normal joystick axis in ASCII that isn't targeting lockstep or virtual.
		/// </summary>
		/// <param name="JoystickInfo">The information in a string array.</param>
		private void ParseNormalAxis(string[] JoystickInfo)
		{
			IsTargetingNormalAxis = true;
			IsTargetingVirtualAxis = false;
			IsTargetingLockstep = false;

			TargetDeviceNumber = JoystickInfo[0];
			TargetAxis = JoystickInfo[1];
			MaxSpeed = JoystickInfo[2];
			ConvertToSpeed(JoystickInfo[3]);
			if (JoystickInfo[4] == "0")
			{
				IsInverted = false;
			}

			else
			{
				IsInverted = true;
			}

			Resolution = JoystickInfo[5];
		}


		/// <summary>
		///     Same as <see cref="ParseNormalAxis(string[])" /> but for virtual axes.
		/// </summary>
		/// <param name="JoystickInfo"></param>
		private void ParseVirtualAxis(string[] JoystickInfo)
		{
			IsTargetingNormalAxis = false;
			IsTargetingVirtualAxis = true;
			IsTargetingLockstep = false;

			TargetDeviceNumber = JoystickInfo[0];
			VirtualIndex = JoystickInfo[2];
			MaxSpeed = JoystickInfo[3];
			ConvertToSpeed(JoystickInfo[4]);
			if (JoystickInfo[5] == "0")
			{
				IsInverted = false;
			}

			else
			{
				IsInverted = true;
			}

			Resolution = JoystickInfo[6];
		}


		/// <summary>
		///     Same as <see cref="ParseNormalAxis(string[])" /> but with Lockstep axes.
		/// </summary>
		/// <param name="JoystickInfo"></param>
		private void ParseLockstepAxis(string[] JoystickInfo)
		{
			IsTargetingNormalAxis = false;
			IsTargetingVirtualAxis = false;
			IsTargetingLockstep = true;

			TargetDeviceNumber = JoystickInfo[0];
			LockstepIndex = JoystickInfo[2];
			MaxSpeed = JoystickInfo[3];
			ConvertToSpeed(JoystickInfo[4]);
			if (JoystickInfo[5] == "0")
			{
				IsInverted = false;
			}

			else
			{
				IsInverted = true;
			}

			Resolution = JoystickInfo[6];
			;
		}


		/// <summary>
		///     Our speed profile information comes back as an integer 1-3. This method
		///     maps that value to the actual description.
		/// </summary>
		/// <param name="Speed"></param>
		private void ConvertToSpeed(string Speed)
		{
			if (Speed == "1")
			{
				SpeedSelected = "Linear";
				return;
			}

			if (Speed == "2")
			{
				SpeedSelected = "Squared (Default)";
				return;
			}

			if (Speed == "3")
			{
				SpeedSelected = "Cubed";
				return;
			}

			_log.Warn($"Invalid speed setting '{Speed}' was defaulted to Squared.");
			SpeedSelected = "Squared (Default)";
		}

		#endregion

		#region -- Properties --

		// We invoke InfoChanged on every data setter
		// because the JoystickVM will handle this event
		// to determine if changes have been made.

		public bool IsOldData { get; }


		/// <summary>
		///     The resolution (sensitivity) of the joystick.
		/// </summary>
		public string Resolution
		{
			get => _resolution;
			set
			{
				//In binary mode we allow characters other than numbers to display N/A
				if (value.Equals("N/A"))
				{
					Set(ref _resolution, value, nameof(Resolution));
					return;
				}

				Set(ref _resolution, value, nameof(Resolution));
			}
		}


		/// <summary>
		///     Whether this joystick is inverted or not.
		/// </summary>
		public bool IsInverted
		{
			get => _isInverted;
			set
			{
				Set(ref _isInverted, value, nameof(IsInverted));

				if (!IsOldData)
				{
					InversionChanged?.Invoke(this, null);
				}
			}
		}


		/// <summary>
		///     What the speed profile is of this joystick, i.e "Squared", "Linear", "Cubed"
		/// </summary>
		public string SpeedSelected
		{
			get => _speedSelected;
			set => Set(ref _speedSelected, value, nameof(SpeedSelected));
		}


		/// <summary>
		///     The Device Number the joystick is targeting.
		/// </summary>
		public string TargetDeviceNumber
		{
			get => _targetDeviceNumber;
			set
			{
				Set(ref _targetDeviceNumber, value, nameof(TargetDeviceNumber));

				if (!IsOldData)
				{
					SmartTargetEvent?.Invoke(null, new SmartTargetArgs(SmartTargetMethod.Device));
				}
			}
		}


		/// <summary>
		///     What normal axis of the device this joystick is targeting.
		/// </summary>
		public string TargetAxis
		{
			get => _axis;
			set
			{
				if (null == value)
				{
					return;
				}

				Set(ref _axis, value, nameof(TargetAxis));
				if (!IsOldData)
				{
					SmartTargetEvent?.Invoke(null, new SmartTargetArgs(SmartTargetMethod.Axis));
				}
			}
		}


		/// <summary>
		///     The index of the virtual axis this joystick is targeting.
		/// </summary>
		public string VirtualIndex
		{
			get => _virtualIndex;
			set
			{
				if (null == value)
				{
					return;
				}

				Set(ref _virtualIndex, value, nameof(VirtualIndex));

				if (!IsOldData)
				{
					SmartTargetEvent?.Invoke(null, new SmartTargetArgs(SmartTargetMethod.Virtual));
				}
			}
		}


		/// <summary>
		///     The index of the lockstep axis this joystick is targeting.
		/// </summary>
		public string LockstepIndex
		{
			get => _lockstepIndex;
			set
			{
				if (null == value)
				{
					return;
				}

				Set(ref _lockstepIndex, value, nameof(LockstepIndex));

				if (!IsOldData)
				{
					SmartTargetEvent?.Invoke(null, new SmartTargetArgs(SmartTargetMethod.Lockstep));
				}
			}
		}


		/// <summary>
		///     The maxspeed this joystick will control devices with.
		/// </summary>
		public string MaxSpeed
		{
			get => _maxspeed;

			set => Set(ref _maxspeed, value, nameof(MaxSpeed));
		}


		/// <summary>
		///     If this joystick is targeting a normal axis, a virtual axis, or a lockstep axis in ASCII.
		/// </summary>
		public bool IsTargetingNormalAxis
		{
			get => _isTargetingNormalAxis;
			set => Set(ref _isTargetingNormalAxis, value, nameof(IsTargetingNormalAxis));
		}


		/// <summary>
		///     If this joystick is targeting a normal axis, a virtual axis, or a lockstep axis in ASCII.
		/// </summary>
		public bool IsTargetingVirtualAxis
		{
			get => _isTargetingVirtualAxis;
			set => Set(ref _isTargetingVirtualAxis, value, nameof(IsTargetingVirtualAxis));
		}


		/// <summary>
		///     If this joystick is targeting a normal axis, a virtual axis, or a lockstep axis in ASCII.
		/// </summary>
		public bool IsTargetingLockstep
		{
			get => _isTargetingLockstep;
			set => Set(ref _isTargetingLockstep, value, nameof(IsTargetingLockstep));
		}


		/// Below methods are set when changes occur.
		/// The control corresponding to these properties will be highlighted red on the view.
		/// These are set in
		/// <see cref="JoystickDataComparer.Equals(JoystickData, JoystickData)" />

		public bool TargetDeviceChanged
		{
			get => _targetDeviceChanged;

			set => Set(ref _targetDeviceChanged, value, nameof(TargetDeviceChanged));
		}

		public bool TargetAxisChanged
		{
			get => _targetAxisChanged;

			set => Set(ref _targetAxisChanged, value, nameof(TargetAxisChanged));
		}

		public bool TargetVirtualChanged
		{
			get => _targetVirtualChanged;

			set => Set(ref _targetVirtualChanged, value, nameof(TargetVirtualChanged));
		}

		public bool TargetLockstepChanged
		{
			get => _targetLockstepChanged;

			set => Set(ref _targetLockstepChanged, value, nameof(TargetLockstepChanged));
		}

		public bool ControlSpeedChanged
		{
			get => _controlSpeedChanged;

			set => Set(ref _controlSpeedChanged, value, nameof(ControlSpeedChanged));
		}

		public bool MaxSpeedChanged
		{
			get => _maxSpeedChanged;

			set => Set(ref _maxSpeedChanged, value, nameof(MaxSpeedChanged));
		}

		public bool InvertChanged
		{
			get => _invertChanged;

			set => Set(ref _invertChanged, value, nameof(InvertChanged));
		}

		public bool ResolutionChanged
		{
			get => _resolutionChanged;

			set => Set(ref _resolutionChanged, value, nameof(ResolutionChanged));
		}

		#endregion

		#region --Data--

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private bool _targetDeviceChanged;
		private bool _targetAxisChanged;
		private bool _targetVirtualChanged;
		private bool _targetLockstepChanged;
		private bool _controlSpeedChanged;
		private bool _maxSpeedChanged;
		private bool _invertChanged;
		private bool _resolutionChanged;

		public event EventHandler<SmartTargetArgs> SmartTargetEvent;

		public event EventHandler InversionChanged;

		private string _speedSelected;
		private List<string> _controlSpeeds = new List<string>();
		private string _targetDeviceNumber = "1";
		private string _axis = "1";
		private string _virtualIndex = "1";
		private string _lockstepIndex = "1";
		private string _maxspeed;
		private bool _isInverted;
		private bool _isTargetingNormalAxis;
		private bool _isTargetingVirtualAxis;
		private bool _isTargetingLockstep;
		private string _resolution;

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZaberConsole.Plugins.Joystick.Stick
{
    /// <summary>
    /// Class used to pass information about whether the event is invoked from a lockstep chance or virtual
    /// axis change.
    /// 
    /// TODO Enums would be better
    /// </summary>
    class AxisArgs : EventArgs
    {
        public AxisArgs(string axisType)
        {
            AxisType = axisType;
        }

        public string AxisType
        {
            get; set;
        }
    }
}

﻿using MvvmDialogs.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.Joystick.Stick
{
    class AddVirtualLockstepVM : ObservableObject, IUserDialogViewModel
    {
        public bool IsModal
        {
            get
            {
                return false;
            }
        }

        public event EventHandler DialogClosing;

        public void RequestClose()
        {
            DialogClosing?.Invoke(this, null);
        }
    }
}

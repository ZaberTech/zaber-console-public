﻿namespace ZaberConsole.Plugins.Joystick.Stick
{
	public enum SmartTargetMethod
	{
		Virtual,
		Axis,
		Lockstep,
		Device
	}
}
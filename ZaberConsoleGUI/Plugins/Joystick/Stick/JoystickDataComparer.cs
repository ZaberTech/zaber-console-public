﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.Joystick.Stick
{
    /// <summary>
    /// Static helper class with one method for determining equality between two JoystickData.
    /// Used in <see cref="JoystickVM.InfoChanged(object, EventArgs)"/> to determine if
    /// changes have occured and then sets the appropriate flags if changes have occured.
    /// </summary>
    public static class JoystickDataComparer 
    {
        /// <summary>
        /// Determines if the <see cref="JoystickVM.HasUnsavedChanges"/>
        /// flag should be set.
        /// </summary>
        /// <param name="ViewInfo">The <see cref="JoystickVM.ViewInfo"/></param>
        /// <param name="oldInfo">The <see cref="JoystickVM.OldInfo"/></param>
        /// <returns></returns>
        public static bool Equals(JoystickData ViewInfo, JoystickData oldInfo)
        {
            //Compare the old information to the view's information. 
            ViewInfo.TargetDeviceChanged =( ViewInfo.TargetDeviceNumber != oldInfo.TargetDeviceNumber);
            ViewInfo.ControlSpeedChanged = (ViewInfo.SpeedSelected != oldInfo.SpeedSelected);
            ViewInfo.MaxSpeedChanged = (ViewInfo.MaxSpeed != oldInfo.MaxSpeed);
            ViewInfo.InvertChanged = (ViewInfo.IsInverted != oldInfo.IsInverted);
            ViewInfo.ResolutionChanged = (ViewInfo.Resolution != oldInfo.Resolution);
            
            //3 sets of logical blocks. We want to check if the viewinfo and the oldinfo are targeting
            //the same axes. If they are, we check further to see if the target axis has actually changed.
            if(ViewInfo.IsTargetingVirtualAxis == oldInfo.IsTargetingVirtualAxis)
            {
                if(ViewInfo.IsTargetingVirtualAxis == true)
                {
                    ViewInfo.TargetVirtualChanged = (ViewInfo.VirtualIndex != oldInfo.VirtualIndex);
                }

                else
                {
                    ViewInfo.TargetVirtualChanged = false;
                }
            }

            else
            {
                ViewInfo.TargetVirtualChanged = true;
            }

            if (ViewInfo.IsTargetingLockstep == oldInfo.IsTargetingLockstep)
            {
                if(ViewInfo.IsTargetingLockstep == true)
                {
                    ViewInfo.TargetLockstepChanged = (ViewInfo.LockstepIndex != oldInfo.LockstepIndex);
                }

                else
                {
                    ViewInfo.TargetLockstepChanged = false;
                }
            }

            else
            {
                ViewInfo.TargetLockstepChanged = true;
            }

            if (ViewInfo.IsTargetingNormalAxis == oldInfo.IsTargetingNormalAxis)
            {
                if(ViewInfo.IsTargetingNormalAxis == true)
                {
                    ViewInfo.TargetAxisChanged = (ViewInfo.TargetAxis != oldInfo.TargetAxis);
                }

                else
                {
                    ViewInfo.TargetAxisChanged = false;
                }
            }

            else
            {
                ViewInfo.TargetAxisChanged = true;
            }

            if( ViewInfo.TargetDeviceChanged ||
                ViewInfo.ControlSpeedChanged ||
                ViewInfo.MaxSpeedChanged ||
                ViewInfo.InvertChanged ||
                ViewInfo.ResolutionChanged ||
                ViewInfo.TargetVirtualChanged ||
                ViewInfo.TargetLockstepChanged ||
                ViewInfo.TargetAxisChanged)
            {
                return false;
            }

            else
            {
                return true;
            }             
        }
    }
}

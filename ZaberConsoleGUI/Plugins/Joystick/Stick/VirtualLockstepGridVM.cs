﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.Joystick.Stick
{
    public class VirtualLockstepGridVM : ObservableObject
    {
        public VirtualLockstepGridVM()
        {
            VirtualLockstepCollection.Add(new VirtualLockstepInfo());
        }

        public ObservableCollection<ObservableObject> VirtualLockstepCollection
        {
            get { return _virtualLockstepCollection; }
            set
            {
                Set(ref _virtualLockstepCollection, value, () => VirtualLockstepCollection);
            }
        }

        ObservableCollection<ObservableObject> _virtualLockstepCollection = new ObservableCollection<ObservableObject>();

    }
}

﻿using System;

/// <summary>
/// This class is used to determine
/// which smart targeting method should be called
/// in <see cref="JoystickVM.SmartTarget(object, SmartTargetArgs)"/>
/// </summary>
namespace ZaberConsole.Plugins.Joystick.Stick
{
	public class SmartTargetArgs : EventArgs
	{
		#region -- Public Methods & Properties --

		public SmartTargetArgs(SmartTargetMethod aMethodType)
		{
			MethodType = aMethodType;
		}


		public SmartTargetMethod MethodType { get; set; }

		#endregion
	}
}

﻿using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.Joystick
{
	/// <summary>
	///     Represents the state and the data of a clickable button
	///     on the left panel of the joystick plugin.
	/// </summary>
	public class ButtonVM : ObservableObject
	{
		#region -- Public Methods & Properties --

		public ButtonVM(int ButtonID)
		{
			this.ButtonID = ButtonID;
			_isSelected = false;
		}


		/// <summary>
		///     1 for Key # 1, 2 for key # 2, etc.
		///     The joystick has a ButtonID of 9.
		/// </summary>
		public int ButtonID
		{
			get => _buttonID;

			set => Set(ref _buttonID, value, nameof(ButtonID));
		}

		/// <summary>
		///     The button will be coloured red when it is selected.
		/// </summary>
		public bool IsSelected
		{
			get => _isSelected;

			set => Set(ref _isSelected, value, nameof(IsSelected));
		}

		#endregion

		#region -- Data --

		private int _buttonID;
		private bool _isSelected;

		#endregion
	}
}

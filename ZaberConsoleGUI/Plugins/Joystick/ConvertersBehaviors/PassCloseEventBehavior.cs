﻿using System.ComponentModel;
using System.Windows;

using Microsoft.Xaml.Behaviors;

namespace ZaberConsole.Plugins.Joystick.ConvertersBehaviors
{
	/// <summary>
	///     This event is a way to deal with closing a window through a modal dialog.
	///     We pass the CancelEventArgs (which can be canceled) to the VM so that the VM controls the
	///     cancelling.
	/// </summary>
	public class PassCloseEventBehavior : Behavior<Window>
	{
		#region -- Public data --

		public static readonly DependencyProperty PassCloseEventProperty =
			DependencyProperty.Register("PassCloseEvent",
										typeof(bool),
										typeof(PassCloseEventBehavior),
										new FrameworkPropertyMetadata(false));

		#endregion

		#region -- Public Methods & Properties --

		public bool PassCloseEvent
		{
			get => (bool) GetValue(PassCloseEventProperty);
			set => SetValue(PassCloseEventProperty, value);
		}

		#endregion

		#region -- Non-Public Methods --

		protected override void OnAttached() => AssociatedObject.Closing += AssociatedObject_Closing;


		private void AssociatedObject_Closing(object sender, CancelEventArgs e)
		{
			//We could probably make this general to an interface.
			var calibrationWizardVM = AssociatedObject.DataContext as CalibrationWizardWindowVM;
			calibrationWizardVM.CancelEvent = e;
		}

		#endregion
	}
}

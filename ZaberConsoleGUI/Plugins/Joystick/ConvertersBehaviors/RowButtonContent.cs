﻿using System;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

using Microsoft.Xaml.Behaviors;

using ZaberConsole.Plugins.Joystick.Key;

namespace ZaberConsole.Plugins.Joystick.ConvertersBehaviors
{
	/// <summary>
	///     Changes a row button icon to a plus or minus sign.
	///     Used for handling that XAML parse exception that comes with
	///     inserting an image as the content of a button in a datagrid.
	///     For some reason, WPF decides to throw a XAML parse exception the first time
	///     the image gets loaded, but not afterwards. I can't figure out why.
	///     Also function allows us to do some xaml code programatically.
	/// </summary>
	public sealed class RowButtonContent : Behavior<ContentControl>
	{
		#region -- Non-Public Methods --

		protected override void OnAttached()
		{
			base.OnAttached();

			AssociatedObject.Loaded += Initialize;
		}


		private void Initialize(object sender, EventArgs e)
		{
			try
			{
				var dataInfo = AssociatedObject.DataContext as KeyCommandInfo;

				if (dataInfo.IsAddCommand)
				{
					var plusSign = new Image();
					var plus_sign = new BitmapImage();
					plus_sign.BeginInit();
					plus_sign.UriSource = new Uri(
						"pack://application:,,,/ZaberWpfToolbox;component/Resources/Icons/plus_sign.png",
						UriKind.Absolute);

					plus_sign.EndInit();
					plusSign.Source = plus_sign;
					AssociatedObject.Content = plusSign;
				}

				else
				{
					var minusSign = new Image();
					var imageUri = new Uri(
						"pack://application:,,,/ZaberWpfToolbox;component/Resources/Icons/minus_sign.png",
						UriKind.Absolute);
					var minus_sign = new BitmapImage(imageUri);

					minusSign.Source = minus_sign;
					AssociatedObject.Content = minusSign;
				}
			}
			catch (Exception)
			{
			}
		}

		#endregion
	}
}

﻿using GongSolutions.Wpf.DragDrop;
using ZaberConsole.Plugins.Joystick.Key;

namespace ZaberConsole.Plugins.Joystick.ConvertersBehaviors
{
	/// <summary>
	///     Custom drag handler used in the ASCII key command grid
	///     that overrides the default to disallow dragging of the add
	///     command row.
	/// </summary>
	public class GridDragHandler : DefaultDragHandler
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Start dragging a KeyCommandInfo for
		///     the ASCII key command grid.
		/// </summary>
		/// <param name="dragInfo">The information for the dragged object.</param>
		public override void StartDrag(IDragInfo dragInfo)
		{
			var dragData = dragInfo.SourceItem as KeyCommandInfo;

			//Don't allow a row to be dragged if it is currently editing
			//or if it is the add command row.
			if (dragData.IsAddCommand || dragData.IsEditing)
			{
			}

			else
			{
				base.StartDrag(dragInfo);
			}
		}

		#endregion
	}
}

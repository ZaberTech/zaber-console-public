﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using ZaberConsole.Plugins.Joystick.Key;

namespace ZaberConsole.Plugins.Joystick.ConvertersBehaviors
{
	/// <summary>
	///     Provides extended support for drag drop operation
	///     And editing of textboxes and Labels.
	/// </summary>
	public static class KeyCommandGridMouseHelper
	{
		#region -- Public data --

		/// <summary>
		///     This dependency property allows the user to scroll a scrollbar
		///     while dragging an item.  See
		///     http://stackoverflow.com/questions/10733581/scrolling-while-dragging-and-dropping-wpf
		/// </summary>
		public static readonly DependencyProperty ScrollOnDragDropProperty =
			DependencyProperty.RegisterAttached("ScrollOnDragDrop",
												typeof(bool),
												typeof(KeyCommandGridMouseHelper),
												new PropertyMetadata(false, HandleScrollOnDragDropChanged));

		/// <summary>
		///     This dependency property handles clicking to edit textboxes
		///     in the key datagrid.
		/// </summary>
		public static readonly DependencyProperty ClickEditProperty =
			DependencyProperty.RegisterAttached("ClickEdit",
												typeof(bool),
												typeof(KeyCommandGridMouseHelper),
												new PropertyMetadata(false, HandleClickEditChanged));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Helper function - finds the first visual child of a certain type
		///     associated with a dependency object via recursion.
		/// </summary>
		/// <typeparam name="T">The child type to find.</typeparam>
		/// <param name="depObj">The parent object to look from.</param>
		/// <returns></returns>
		public static T GetFirstVisualChild<T>(DependencyObject depObj) where T : DependencyObject
		{
			if (depObj != null)
			{
				for (var i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
				{
					var child = VisualTreeHelper.GetChild(depObj, i);
					if ((child != null) && child is T)
					{
						return (T) child;
					}

					var childItem = GetFirstVisualChild<T>(child);
					if (childItem != null)
					{
						return childItem;
					}
				}
			}

			return null;
		}


		/// <summary>
		///     Helper function - finds the first visual parent of certain type associated
		///     with the dependency object via recursion.
		/// </summary>
		/// <typeparam name="T">The parent type to find</typeparam>
		/// <param name="child"> The child object to look from.</param>
		/// <returns></returns>
		public static T FindVisualParent<T>(DependencyObject child)
			where T : DependencyObject
		{
			// get parent item
			var parentObject = VisualTreeHelper.GetParent(child);

			// we’ve reached the end of the tree
			if (parentObject == null)
			{
				return null;
			}

			// check if the parent matches the type we’re looking for
			var parent = parentObject as T;
			if (parent != null)
			{
				return parent;
			}

			// use recursion to proceed with next level
			return FindVisualParent<T>(parentObject);
		}


		/// <summary>
		///     Used to find a specific child DependencyObject from a hit test.
		/// </summary>
		/// <typeparam name="T">The dependency object to search for</typeparam>
		/// <param name="reference">A reference element</param>
		/// <param name="point">The mouse point</param>
		/// <returns></returns>
		public static T GetChildFromPoint<T>(UIElement reference, Point point)
			where T : DependencyObject
		{
			var element = reference.InputHitTest(point) as DependencyObject;

			if (element == null)
			{
				return null;
			}

			if (element is T)
			{
				return (T) element;
			}

			return GetFirstVisualChild<T>(element);
		}


		/// <summary>
		///     Used to find a specific parent from a hit test.
		/// </summary>
		/// <typeparam name="T">The dependency object to search for</typeparam>
		/// <param name="reference">A reference element</param>
		/// <param name="point">The mouse point</param>
		/// <returns></returns>
		public static T GetParentFromPoint<T>(UIElement reference, Point point)
			where T : DependencyObject
		{
			var element = reference.InputHitTest(point) as DependencyObject;

			if (element == null)
			{
				return null;
			}

			if (element is T)
			{
				return (T) element;
			}

			return FindVisualParent<T>(element);
		}


		public static bool GetScrollOnDragDrop(DependencyObject element)
		{
			if (element == null)
			{
				throw new ArgumentNullException("element");
			}

			return (bool) element.GetValue(ScrollOnDragDropProperty);
		}


		public static void SetScrollOnDragDrop(DependencyObject element, bool value)
		{
			if (element == null)
			{
				throw new ArgumentNullException("element");
			}

			element.SetValue(ScrollOnDragDropProperty, value);
		}


		public static bool GetClickEdit(DependencyObject element)
		{
			if (element == null)
			{
				throw new ArgumentNullException("'element' was null.");
			}

			return (bool) element.GetValue(ClickEditProperty);
		}


		public static void SetClickEdit(DependencyObject element, bool value)
		{
			if (element == null)
			{
				throw new ArgumentNullException("'element' was null.");
			}

			element.SetValue(ClickEditProperty, value);
		}

		#endregion

		#region -- Non-Public Methods --

		private static void HandleScrollOnDragDropChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var container = d as FrameworkElement;

			if (d == null)
			{
				return;
			}

			Unsubscribe(container);

			if (true.Equals(e.NewValue))
			{
				Subscribe(container);
			}
		}


		private static void Subscribe(FrameworkElement container)
			=> container.PreviewDragOver += OnContainerPreviewDragOver;


		private static void OnContainerPreviewDragOver(object sender, DragEventArgs e)
		{
			var container = sender as FrameworkElement;

			if (container == null)
			{
				return;
			}

			var scrollViewer = GetFirstVisualChild<ScrollViewer>(container);

			if (scrollViewer == null)
			{
				return;
			}

			double tolerance = 30;
			var verticalPos = e.GetPosition(container).Y;
			var offset = 0.02;

			if (verticalPos < tolerance) // Top of visible list? 
			{
				scrollViewer.ScrollToVerticalOffset(scrollViewer.VerticalOffset - offset); //Scroll up. 
			}
			else if (verticalPos > (container.ActualHeight - tolerance)) //Bottom of visible list? 
			{
				scrollViewer.ScrollToVerticalOffset(scrollViewer.VerticalOffset + offset); //Scroll down.     
			}
		}


		private static void Unsubscribe(FrameworkElement container)
			=> container.PreviewDragOver -= OnContainerPreviewDragOver;


		/// <summary>
		///     Hook up the left mouse up event to the datagrid.
		/// </summary>
		/// <param name="d">The datagrid.</param>
		/// <param name="e">We don't use e.</param>
		private static void HandleClickEditChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var container = d as DataGrid;

			if (d == null)
			{
				return;
			}

			container.PreviewMouseLeftButtonUp -= StartEditing;

			if (true.Equals(e.NewValue))
			{
				container.PreviewMouseLeftButtonUp += StartEditing;
			}
		}


		/// <summary>
		///     When we click away from the editing text box, we want it to lose focus and stop editing.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private static void OnEditLostFocus(object sender, RoutedEventArgs e)
		{
			//Below members are useful for debugging.
			//var scope = FocusManager.GetFocusScope(textbox);
			//var logicalFocus = FocusManager.GetFocusedElement(scope);
			//var keyboardFocus = Keyboard.FocusedElement; 

			if ((null == _textbox) || (null == _label))
			{
				return;
			}

			_textbox.Visibility = Visibility.Collapsed;
			_label.Visibility = Visibility.Visible;

			_rowData.IsEditing = false;
			_textbox.LostFocus -= OnEditLostFocus;
		}


		/// <summary>
		///     Change the visibility of the Label and textbox associated with
		///     the datagrid cell so that the textbox will become editable.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private static void StartEditing(object sender, MouseButtonEventArgs e)
		{
			var container = sender as DataGrid;

			//Search both directions, child and parent, to get the datagrid row.
			var row = GetParentFromPoint<DataGridRow>(container, e.GetPosition(container));
			row = row ?? GetChildFromPoint<DataGridRow>(container, e.GetPosition(container));
			var keyboardFocus = Keyboard.FocusedElement;

			//We're alerady editing if we are focused on the same textbox or if didn't click on a row.
			if ((keyboardFocus == _textbox) || (row == null))
			{
				return;
			}

			//We don't want to do anything if the clicked row is the row for adding commands.
			if ((row.DataContext as KeyCommandInfo).IsAddCommand)
			{
				return;
			}

			_rowData = row.DataContext as KeyCommandInfo;

			//If we click on another row.
			if ((_textbox != null) && (_label != null)) //let the textbox lose focus before re-trying
			{
				if ((_textbox.Visibility == Visibility.Visible) && (_label.Visibility == Visibility.Collapsed))
				{
					OnEditLostFocus(null, null);
					StartEditing(sender, e);
				}
			}

			//Search both directions for the datagridcell.
			var dataGridCell = GetParentFromPoint<DataGridCell>(container, e.GetPosition(container));
			dataGridCell = dataGridCell ?? GetChildFromPoint<DataGridCell>(container, e.GetPosition(container));

			_label = GetFirstVisualChild<Label>(dataGridCell);
			_textbox = GetFirstVisualChild<TextBox>(dataGridCell);

			if ((null == _label) || (null == dataGridCell))
			{
				return;
			}

			_textbox = GetFirstVisualChild<TextBox>(dataGridCell);
			_textbox.LostFocus += OnEditLostFocus;

			_textbox.Visibility = Visibility.Visible;
			_label.Visibility = Visibility.Collapsed;
			var scope = FocusManager.GetFocusScope(_textbox);

			FocusManager.SetFocusedElement(scope, _textbox);
			Keyboard.Focus(_textbox);
			_rowData.IsEditing = true;
		}

		#endregion

		#region -- Data --

		private static TextBox _textbox;
		private static Label _label;
		private static KeyCommandInfo _rowData;

		#endregion
	}
}

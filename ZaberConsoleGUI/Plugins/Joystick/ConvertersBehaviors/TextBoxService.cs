﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ZaberConsole.Plugins.Joystick.ConvertersBehaviors
/// <summary>
/// The code for this class was adapted from 
///http://stackoverflow.com/questions/1346707/validation-in-textbox-in-wpf
/// and is not covered by the same license as regular Zaber code. The applicable
/// license is CC BY-SA 3.0: http://creativecommons.org/licenses/by-sa/3.0/
/// </summary>

/// <summary>
/// Allows TextBoxes to filter out non-numeric values. Used for the
/// joystick and key textboxes that don't allow non-numeric values.
/// </summary>
{
    public static class TextBoxService
    {
        /// <summary>
        /// TextBox Attached Dependency Property
        /// </summary>
        public static readonly DependencyProperty IsNumericOnlyProperty = DependencyProperty.RegisterAttached(
           "IsNumericOnly",
           typeof(bool),
           typeof(TextBoxService),
           new UIPropertyMetadata(false, OnIsNumericOnlyChanged));

        /// <summary>
        /// Gets the IsNumericOnly property.  This dependency property indicates the text box only allows numeric or not.
        /// </summary>
        /// <param name="d"><see cref="DependencyObject"/> to get the property from</param>
        /// <returns>The value of the StatusBarContent property</returns>
        public static bool GetIsNumericOnly(DependencyObject d)
        {
            return (bool)d.GetValue(IsNumericOnlyProperty);
        }

        /// <summary>
        /// Sets the IsNumericOnly property.  This dependency property indicates the text box only allows numeric or not.
        /// </summary>
        /// <param name="d"><see cref="DependencyObject"/> to set the property on</param>
        /// <param name="value">value of the property</param>
        public static void SetIsNumericOnly(DependencyObject d, bool value)
        {
            d.SetValue(IsNumericOnlyProperty, value);
        }

        /// <summary>
        /// Handles changes to the IsNumericOnly property.
        /// </summary>
        /// <param name="d"><see cref="DependencyObject"/> that fired the event</param>
        /// <param name="e">A <see cref="DependencyPropertyChangedEventArgs"/> that contains the event data.</param>
        private static void OnIsNumericOnlyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            bool isNumericOnly = (bool)e.NewValue;

            TextBox textBox = d as TextBox;
            Label label = d as Label;

            if( label == null && textBox == null)
            {
                throw new ArgumentException("TextBoxService on a control that's neither a textbox or a label");
            }

            if (isNumericOnly && label != null)
            {
                label.PreviewTextInput += BlockNonDigitCharacters;
                label.PreviewKeyDown += ReviewKeyDown;
            }
            else if (isNumericOnly && textBox != null)
            {
                textBox.PreviewTextInput += BlockNonDigitCharacters;
                textBox.PreviewKeyDown += ReviewKeyDown;
            }

            else if (!isNumericOnly && textBox != null)
            {
                textBox.PreviewTextInput -= BlockNonDigitCharacters;
                textBox.PreviewKeyDown -= ReviewKeyDown;
            }

            else
            {
                label.PreviewTextInput -= BlockNonDigitCharacters;
                label.PreviewKeyDown -= ReviewKeyDown;
            }
        }

        /// <summary>
        /// Disallows non-digit character.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">An <see cref="TextCompositionEventArgs"/> that contains the event data.</param>
        private static void BlockNonDigitCharacters(object sender, TextCompositionEventArgs e)
        {
            foreach (char ch in e.Text)
            {
                if (!Char.IsDigit(ch))
                {
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// Disallows a space key.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">An <see cref="KeyEventArgs"/> that contains the event data.</param>
        private static void ReviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Space)
            {
                // Disallow the space key, which doesn't raise a PreviewTextInput event.
                e.Handled = true;
            }
        }
    }
}
﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

using Microsoft.Xaml.Behaviors;

using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.Joystick.ConvertersBehaviors
{
	/// <summary>
	///     Behavior for the datagrid containing the rows to gain focus when being clicked on. This allows the user to easily
	///     deselect a text box.
	/// </summary>
	internal class LoseFocusBehavior : Behavior<UIElement>
	{
		#region -- Non-Public Methods --

		protected override void OnAttached()
		{
			base.OnAttached();
			AssociatedObject.PreviewMouseDown += AssociatedObject_PreviewMouseDown;
		}


		protected override void OnDetaching()
		{
			AssociatedObject.PreviewMouseDown -= AssociatedObject_PreviewMouseDown;
			base.OnDetaching();
		}


		private void AssociatedObject_PreviewMouseDown(object sender, MouseEventArgs e)

		{
			var container = sender as DataGrid;
			var keyboardFocus = Keyboard.FocusedElement;

			//Check if the user clicked on a textbox.
			var clickedTextBox =
				KeyCommandGridMouseHelper.GetParentFromPoint<TextBox>(container, e.GetPosition(container));
			var parent = KeyCommandGridMouseHelper.FindVisualParent<BaseControl>(container);

			//if the user clicked on different spot from the focused textbox we lose focus.
			if (keyboardFocus is TextBox && (keyboardFocus != clickedTextBox))
			{
				AssociatedObject.Focus();
			}

			else if (keyboardFocus != clickedTextBox)
			{
				parent.Focus();
			}
		}

		#endregion
	}
}

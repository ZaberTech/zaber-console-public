﻿using System.Collections.ObjectModel;
using GongSolutions.Wpf.DragDrop;
using ZaberConsole.Plugins.Joystick.Key;

namespace ZaberConsole.Plugins.Joystick.ConvertersBehaviors
{
	/// <summary>
	///     Custom drop handler that overrides some of the
	///     normal behavior that allows dragging ASCII key command grids to work.
	/// </summary>
	public class GridDropHandler : DefaultDropHandler
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     We don't want to able to insert at index 0 since
		///     the add command row always occupies that slot.
		/// </summary>
		/// <param name="dropInfo">The target we'll drop to.</param>
		public override void DragOver(IDropInfo dropInfo)
		{
			if (dropInfo.InsertIndex < 1)
			{
				return;
			}

			base.DragOver(dropInfo);
		}


		/// <summary>
		///     The drop action.
		/// </summary>
		/// <param name="dropInfo">The drop target.</param>
		public override void Drop(IDropInfo dropInfo)
		{
			base.Drop(dropInfo);

			//If we're dragging from our grid into a different grid.
			if (dropInfo.TargetCollection != dropInfo.DragInfo.SourceCollection)
			{
				//The default behavior is the copy the object over. We need to 
				//erase the old object.
				var draggedCollection = dropInfo.DragInfo.SourceCollection as ObservableCollection<KeyCommandInfo>;
				var draggedItem = dropInfo.DragInfo.SourceItem as KeyCommandInfo;

				draggedCollection.Remove(draggedItem);
			}
		}

		#endregion
	}
}

﻿using System;
using System.Collections;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ZaberConsole.Plugins.Joystick.ConvertersBehaviors
{
	/// <summary>
	///     A naive implementation of drag and drop as attached behaviors.
	///     Included as a starting point in case the used library, gong-wpf-dragdrop
	///     becomes deprecated.
	/// </summary>
	public class VisualHelper
	{
		#region -- Public data --

		public static readonly DependencyProperty EnableRowsMoveProperty =
			DependencyProperty.RegisterAttached("EnableRowsMove",
												typeof(bool),
												typeof(VisualHelper),
												new PropertyMetadata(false, EnableRowsMoveChanged));

		public static readonly DependencyProperty AttachLineToGridProperty =
			DependencyProperty.RegisterAttached("AttachLineToGrid",
												typeof(bool),
												typeof(VisualHelper),
												new PropertyMetadata(false, AttachLineToGridChanged));

		#endregion

		#region -- Public Methods & Properties --

		public static bool GetEnableRowsMove(DataGrid obj) => (bool) obj.GetValue(EnableRowsMoveProperty);


		public static void SetEnableRowsMove(DataGrid obj, bool value) => obj.SetValue(EnableRowsMoveProperty, value);


		public static bool GetAttachLineToGrid(Line obj) => (bool) obj.GetValue(AttachLineToGridProperty);


		public static void SetAttachLineToGrid(Line obj, bool value) => obj.SetValue(AttachLineToGridProperty, value);


		public static T FindVisualParent<T>(DependencyObject child)
			where T : DependencyObject
		{
			// get parent item
			var parentObject = VisualTreeHelper.GetParent(child);

			// we’ve reached the end of the tree
			if (parentObject == null)
			{
				return null;
			}

			// check if the parent matches the type we’re looking for
			var parent = parentObject as T;
			if (parent != null)
			{
				return parent;
			}

			// use recursion to proceed with next level
			return FindVisualParent<T>(parentObject);
		}


		public static T TryFindFromPoint<T>(UIElement reference, Point point)
			where T : DependencyObject
		{
			var element = reference.InputHitTest(point) as DependencyObject;
			if (element == null)
			{
				return null;
			}

			if (element is T)
			{
				return (T) element;
			}

			return FindVisualParent<T>(element);
		}

		#endregion

		#region -- Non-Public Methods --

		private static void SetLineHeight(Line obj, double value)
		{
			if (_changeLine)
			{
				obj.Y1 = value;
				obj.Y2 = value;
				obj.X2 = _rowWidth;
			}
		}


		private static void EnableRowsMoveChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var grid = d as DataGrid;
			if (grid == null)
			{
				return;
			}

			if ((bool) e.NewValue)
			{
				grid.PreviewMouseLeftButtonDown += OnMouseLeftButtonDown;
				grid.PreviewMouseLeftButtonUp += OnMouseLeftButtonUp;
				grid.PreviewMouseMove += OnMouseMove;
			}
			else
			{
				grid.PreviewMouseLeftButtonDown -= OnMouseLeftButtonDown;
				grid.PreviewMouseLeftButtonUp -= OnMouseLeftButtonUp;
				grid.PreviewMouseMove -= OnMouseMove;
			}
		}


		private static void AttachLineToGridChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var line = d as Line;

			if (line == null)
			{
				return;
			}

			_changeLine = true;
			_thisLine = line;

			line.X1 = 0;
			line.X2 = 0;
		}


		private static void OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			//find datagrid row by mouse point position
			var row = TryFindFromPoint<DataGridRow>((UIElement) sender, e.GetPosition(sender as DataGridRow));

			if ((row == null) || row.IsEditing || _mouseDown)
			{
				return;
			}

			draggedRow = row;
			targetRow = row;
			_mouseDown = true;
		}


		private static void OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			if (draggedRow == null)
			{
				draggedRow = null;
				_mouseDown = false;
				SetLineHeight(_thisLine, 0);
				return;
			}

			ExchangeItems(sender, targetRow);

			draggedRow = null;

			SetLineHeight(_thisLine, 0);
			_mouseDown = false;
		}


		private static void OnMouseMove(object sender, MouseEventArgs e)
		{
			var row = TryFindFromPoint<DataGridRow>((UIElement) sender, e.GetPosition(sender as DataGrid));

			if ((row == null) || row.IsEditing || !_mouseDown)
			{
				return;
			}

			if (row != draggedRow)
			{
				var dp = sender as DependencyObject;

				var rowRelativePoint = e.GetPosition(row);
				Point mousePoint;

				if (rowRelativePoint.Y > (row.ActualHeight / 2))
				{
					mousePoint = new Point(0, row.ActualHeight);
					_isBottomRow = true;
				}

				else
				{
					mousePoint = new Point(0, 0);
					_isBottomRow = false;
				}

				var parent = FindVisualParent<DataGrid>(dp);
				var absPoint = row.TranslatePoint(mousePoint, parent);
				SetLineHeight(_thisLine, absPoint.Y);

				draggedRow.IsSelected = true;
				targetRow = row;
				_rowWidth = row.ActualWidth;
			}
		}


		private static void ExchangeItems(object sender, object targetItem)
		{
			if (draggedRow == null)
			{
				return;
			}

			if ((targetItem != null) && !ReferenceEquals(draggedRow, targetRow))
			{
				var list = (sender as DataGrid).ItemsSource as IList;
				if (list == null)
				{
					throw new ApplicationException(
						"EnableRowsMoveProperty requires the ItemsSource property of DataGrid to be at least IList inherited collection. Use ObservableCollection to have movements reflected in UI.");
				}

				//get target index
				var targetIndex = list.IndexOf(targetRow.Item);
				var draggedIndex = list.IndexOf(draggedRow.Item);

				if (draggedIndex == targetIndex)
				{
					return;
				}

				if (draggedIndex > targetIndex)
				{
					targetIndex++;
				}

				if (!_isBottomRow)
				{
					if (targetIndex != 0)
					{
						targetIndex--;
					}
				}

				else
				{
					if (targetIndex == 0)
					{
						targetIndex++;
					}
				}

				//remove the source from the list
				list.Remove(draggedRow.Item);

				//move source at the target's location
				list.Insert(targetIndex, draggedRow.Item);
			}
		}

		#endregion

		#region -- Data --

		private static bool _mouseDown;
		private static bool _changeLine;
		private static bool _isBottomRow;
		private static Line _thisLine;
		private static double _rowWidth;

		private static DataGridRow draggedRow;
		private static DataGridRow targetRow;

		#endregion
	}
}

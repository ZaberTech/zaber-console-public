﻿using System.Windows;
using System.Windows.Input;

using Microsoft.Xaml.Behaviors;

namespace ZaberConsole.Plugins.Joystick.ConvertersBehaviors

	/// <summary>
	/// Attached behavior to make a scroller bubble its event upwards.
	/// The code for this class was adapted from 
	/// http://stackoverflow.com/questions/14348517/child-elements-of-scrollviewer-preventing-scrolling-with-mouse-wheel
	/// and is not covered by the same license as regular Zaber code. The applicable
	/// license is CC BY-SA 3.0: http://creativecommons.org/licenses/by-sa/3.0/
	/// </summary>
{
	// Used on sub-controls of an expander to bubble the mouse wheel scroll event up 
	public sealed class BubbleScrollEvent : Behavior<UIElement>
	{
		#region -- Non-Public Methods --

		protected override void OnAttached()
		{
			base.OnAttached();
			AssociatedObject.PreviewMouseWheel += AssociatedObject_PreviewMouseWheel;
		}


		protected override void OnDetaching()
		{
			AssociatedObject.PreviewMouseWheel -= AssociatedObject_PreviewMouseWheel;
			base.OnDetaching();
		}


		private void AssociatedObject_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
		{
			e.Handled = true;
			var e2 = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
			e2.RoutedEvent = UIElement.MouseWheelEvent;
			AssociatedObject.RaiseEvent(e2);
		}

		#endregion
	}
}

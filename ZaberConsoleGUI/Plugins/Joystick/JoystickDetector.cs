﻿using System;
using System.Linq;
using System.Windows;
using Zaber;
using Zaber.PlugIns;
using ZaberConsole.Properties;
using ZaberWpfToolbox.Dialogs;
using ZaberWpfToolbox.Settings;

namespace ZaberConsole.Plugins.Joystick
{
	/// <summary>
	///     Invisible plugin that looks for joystick devices and prompts the user to display
	///     the joystick setup tab when there is one.
	/// </summary>
	[PlugIn(Name = "Joystick Detector",
		AlwaysActive = true,
		Dockable = false,
		Description = "Detects joystick devices and prompts to display the setup interface.")]
	public class JoystickDetector : IDialogClient
	{
		#region -- Plugin implementation --

		/// <summary>
		///     The active port.
		/// </summary>
		[PlugInProperty]
		public ZaberPortFacade PortFacade
		{
			get => _port;
			set
			{
				if (null != _port)
				{
					_port.Opened -= OnPortOpened;
				}

				_port = value;

				if (null != _port)
				{
					_port.Opened += OnPortOpened;
				}
			}
		}


		/// <summary>
		///     Reference to the plugin manager so we can check available tabs.
		/// </summary>
		[PlugInProperty]
		public IPlugInManager PluginManager { get; set; }

		#endregion

		#region -- IDialogClient implementation --

		/// <summary>
		///     Invoked to display a popup dialog.
		/// </summary>
		public event RequestDialogChange RequestDialogOpen;

		/// <summary>
		///     Invoked to programmatically close a popup dialog.
		/// </summary>
		#pragma warning disable CS0067 // Event is never used.
		public event RequestDialogChange RequestDialogClose;
		#pragma warning restore CS0067

		#endregion

		#region -- Functionality --

		// Called when the port is opened, after devices have been detected.
		private void OnPortOpened(object aSender, EventArgs aArgs)
		{
			if ((null == _port) || (null == PluginManager))
			{
				return;
			}

			// Don't do anything if the servo tab is already visible.
			if (PluginManager.GetVisiblePluginTypes().Contains(typeof(JoystickPluginVM)))
			{
				return;
			}

			// Find the do-not-show-again settings for this dialog.
			var settings = Settings.Default.DoNotShowAgainSettings;
			if (null == settings)
			{
				settings = new DoNotShowAgainSettingsCollection();
				Settings.Default.DoNotShowAgainSettings = settings;
			}

			var setting = settings.FindOrCreate(SETTING_NAME);
			if (setting.ShowDialog)
			{
				if (!_port.AllConversations.Any(conversation
													=> conversation.Device.DeviceType.IsJoystick
												   && conversation.Device.IsSingleDevice))
				{
					return;
				}

				var mbvm = new CustomMessageBoxVM(
					"Zaber Console has detected that you have a joystick device connected.Do you want to enable the Joystick Set-up Helper tab?",
					"Show joystick tab?",
					"Yes",
					"No");
				mbvm.DoNotShowAgainSettings = setting;
				mbvm.DialogResult = MessageBoxResult.No;

				RequestDialogOpen?.Invoke(this, mbvm);

				// Show the tab if the user said OK.
				if (MessageBoxResult.Yes == (MessageBoxResult) mbvm.DialogResult)
				{
					PluginManager.ShowPluginTab(typeof(JoystickPluginVM));
				}
			}
		}

		#endregion

		#region -- State information --

		private const string SETTING_NAME = "EnableJoystickWindow";

		private ZaberPortFacade _port;

		#endregion
	}
}

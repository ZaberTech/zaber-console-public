﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using Zaber;
using ZaberConsole.Plugins.CommandList;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.Joystick.Key
{
	/// <summary>
	///     This class represents the data behind one key on the joystick
	///     and is data bound to the right panel display when a key button is clicked
	///     on the left panel.
	/// </summary>
	public class KeyVM : ObservableObject
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Constructor for the KeyVM which represents one key's information.
		/// </summary>
		/// <param name="ID">The key # of the key, from 1-8.</param>
		/// <param name="protocol">Whether the key is in ASCII or Binary.</param>
		public KeyVM(int ID, Protocol protocol)
		{
			var KeyCommandGrid0 =
				new KeyCommandGridVM(0) { GridName = "Shouldn't be visible" }; //add dummy so indices match up
			var KeyCommandGrid1 = new KeyCommandGridVM(1) { GridName = GRID_1_NAME };
			var KeyCommandGrid2 = new KeyCommandGridVM(2) { GridName = GRID_2_NAME };
			var KeyCommandGrid3 = new KeyCommandGridVM(3) { GridName = GRID_3_NAME };
			var KeyCommandGrid4 = new KeyCommandGridVM(4) { GridName = GRID_4_NAME };

			KeyCommandGrids.Add(KeyCommandGrid0);
			KeyCommandGrids.Add(KeyCommandGrid1);
			KeyCommandGrids.Add(KeyCommandGrid2);
			KeyCommandGrids.Add(KeyCommandGrid3);
			KeyCommandGrids.Add(KeyCommandGrid4);

			this.ID = ID.ToString();

			foreach (var grid in KeyCommandGrids)
			{
				grid.SelectionChanged += SelectionChangedMethod;
				grid.CommandsChangedEvent += CommandsChangedMethod;
			}

			if (protocol == Protocol.ASCII)
			{
				IsAsciiMode = true;
			}

			else
			{
				IsAsciiMode = false;
			}
		}

		#endregion

		#region --Public Methods--

		/// <summary>
		///     Parses all of the information for one key event and populates the grid for that key event.
		/// </summary>
		/// <param name="commandList">
		///     Each string represents one command. The first string describes
		///     whether the key alert is enabled or not.
		/// </param>
		/// <param name="keyEvent">an event from 1-4</param>
		public void ParseKeyInfoAscii(List<string> commandList, int keyEvent)
		{
			var targetGrid = KeyCommandGrids[keyEvent];

			if (commandList[0].Contains("enabled"))
			{
				targetGrid.PreviousAlertsEnabled = true;
				targetGrid.AlertsEnabled = true;
			}

			else
			{
				targetGrid.PreviousAlertsEnabled = false;
				targetGrid.AlertsEnabled = false;
			}

			for (var i = 1; i < commandList.Count; i++)
			{
				//each command in commandList looks like "cmd 02 1 home"
				var commandInformation = commandList[i].Split(' ');
				var commandParams = commandInformation.Length;
				var toAdd = new KeyCommandInfo();

				// Detect old firmware case that did not have the "cmd" prefix to disambiguate.
				var idx = 1;
				if (("cmd" != commandInformation[0])
				&& int.TryParse(commandInformation[0],
								NumberStyles.Integer,
								CultureInfo.InvariantCulture,
								out var dummy)
				&& int.TryParse(commandInformation[1], NumberStyles.Integer, CultureInfo.InvariantCulture, out dummy))
				{
					idx = 0;
				}

				toAdd.TargetDeviceNumber = commandInformation[idx];
				toAdd.TargetAxis = commandInformation[idx + 1];

				for (var j = idx + 2; j < commandParams; j++)
				{
					toAdd.CommandName += commandInformation[j];
					toAdd.CommandName += " ";
				}

				toAdd.CommandName = toAdd.CommandName.Remove(toAdd.CommandName.Length - 1);
				targetGrid.AddCommandAscii(toAdd);
			}

			LastReadTime = DateTime.Now.ToShortTimeString();
		}


		/// <summary>
		///     Populates the binary layout for each key event. Used by the
		///     read button and the initial query.
		/// </summary>
		/// <param name="message">A datapacket representing the response from a Return Event Instruction command.</param>
		/// <param name="keyEvent">The key event that the command was queried from</param>
		public void ParseKeyInfoBinary(DataPacket message, int keyEvent)
		{
			var targetgrid = KeyCommandGrids[keyEvent];

			var commandInfo = new KeyCommandInfo
			{
				TargetDeviceNumber = message.DeviceNumber.ToString(),
				BinaryCommand = ((int) message.Command).ToString(),
				BinaryData = message.NumericData.ToString()
			};

			//KeyCommands should only have 1 command, the null add command row.
			if (targetgrid.KeyCommands.Count > 1)
			{
				throw new ArgumentException("Binary Grid has more than 1 command.");
			}

			targetgrid.AddCommandBinary(commandInfo);

			LastReadTime = DateTime.Now.ToShortTimeString();
		}

		#endregion

		#region --Event Handling--

		/// <summary>
		///     Called to make sure that only one command grid command can be selected at a time.
		/// </summary>
		/// <param name="aSender">The KeyCommandGridVM this event came from.</param>
		/// <param name="e"></param>
		private void SelectionChangedMethod(object aSender, EventArgs e)
		{
			var sentCommandGrid = (KeyCommandGridVM) aSender;
			_selectedItem = sentCommandGrid.SelectedItem;

			foreach (var commandGrid in KeyCommandGrids)
			{
				if (commandGrid != sentCommandGrid)
				{
					commandGrid.SelectedItem = null;
				}
			}
		}


		/// <summary>
		///     Called from each command grid anytime a command has been changed.
		/// </summary>
		/// <param name="aGrid"></param>
		/// <param name="e"></param>
		private void CommandsChangedMethod(object aGrid, EventArgs e)
		{
			var gridInfo = aGrid as KeyCommandGridVM;
			_changedCommandGrids[gridInfo.ID] = gridInfo.HasUnsavedChanges;

			//If any of the 4 grids have unsaved changes, then the key has unsaved changes as well.
			if (_changedCommandGrids.ContainsValue(true))
			{
				HasUnsavedChanges = true;
			}
			else
			{
				HasUnsavedChanges = false;
			}
		}

		#endregion

		#region --Getters and Setters--

		/// <summary>
		///     Whether comm.alert is on or off for this joystick. When comm.alert is off and
		///     the key alert is on, a little yellow triangle appears.
		/// </summary>
		public bool CommAlertsOn
		{
			get => _commAlertsOn;

			set => Set(ref _commAlertsOn, value, nameof(CommAlertsOn));
		}

		/// <summary>
		///     The time at which the settings displayed were last read.
		/// </summary>
		public string LastReadTime
		{
			get => _lastReadTime;

			set => Set(ref _lastReadTime, value, nameof(LastReadTime));
		}

		/// <summary>
		///     The actual grids that hold the data. Both ASCII and Binary data
		///     are held in here.
		/// </summary>
		public ObservableCollection<KeyCommandGridVM> KeyCommandGrids
		{
			get => _keyCommandGrids;

			set => Set(ref _keyCommandGrids, value, nameof(KeyCommandGrids));
		}


		/// <summary>
		///     Which key # this is from 1-8.
		/// </summary>
		public string ID
		{
			get => _id;
			set => Set(ref _id, value, nameof(ID));
		}

		/// <summary>
		///     All of the connected devices currently attached to the computer.
		/// </summary>
		public List<ZaberDevice> ConnectedDevices
		{
			get => _connectedDevices;

			set
			{
				_connectedDevices = value;

				//Tunnel the new reference down to the children grids
				foreach (var grid in KeyCommandGrids)
				{
					grid.ConnectedDevices = value;
				}
			}
		}

		/// <summary>
		///     All of the Binary commands that are available for the devices on the chain.
		/// </summary>
		public ObservableCollection<CommandInfoVM> BinaryCommands
		{
			get => _binaryCommands;

			set
			{
				_binaryCommands = value;

				//Tunnel the new reference down to the children grids
				foreach (var grid in KeyCommandGrids)
				{
					grid.BinaryCommands = value;
				}
			}
		}

		/// <summary>
		///     Which datagridrow is actually selected. We make sure
		///     that only 1 row can be selected at a time.
		/// </summary>
		public KeyCommandInfo SelectedItem
		{
			get => _selectedItem;

			set => Set(ref _selectedItem, value, nameof(SelectedItem));
		}

		/// <summary>
		///     Whether one of the grids on the key has an unsaved change.
		/// </summary>
		public bool HasUnsavedChanges
		{
			get => _hasUnsavedChanges;

			set => Set(ref _hasUnsavedChanges, value, nameof(HasUnsavedChanges));
		}

		/// <summary>
		///     Binding in xaml for which type of key grid to display.
		/// </summary>
		public bool IsAsciiMode { get; set; }

		#endregion

		#region --Data--

		private bool
			_commAlertsOn =
				true; //start true so that the alert triangles don't turn on then turn off when initially querying.

		private ObservableCollection<KeyCommandGridVM> _keyCommandGrids = new ObservableCollection<KeyCommandGridVM>();
		private KeyCommandInfo _selectedItem;
		private bool _hasUnsavedChanges;
		private string _id;

		private readonly Dictionary<int, bool> _changedCommandGrids = new Dictionary<int, bool>
		{
			{ 1, false },
			{ 2, false },
			{ 3, false },
			{ 4, false }
		};

		private string _lastReadTime;
		private List<ZaberDevice> _connectedDevices;
		private ObservableCollection<CommandInfoVM> _binaryCommands;

		public const string GRID_1_NAME = "Press (Event 1)";
		public const string GRID_2_NAME = "Quick Release (Event 2)";
		public const string GRID_3_NAME = "Hold 2 Seconds (Event 3)";
		public const string GRID_4_NAME = "Release after Hold (Event 4)";

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ZaberConsole.Plugins.Joystick.Key
{
	/// <summary>
	///     Specialty class used for comparing key command information. Used by
	///     KeyCommandGrid.cs.
	/// </summary>
	public class KeyInfoComparer : IEqualityComparer<KeyCommandInfo>
	{
		#region -- Public Methods & Properties --

		public bool Equals(KeyCommandInfo x, KeyCommandInfo y)
		{
			bool sameAxis;
			bool sameCommand;
			bool sameTargetNumber;
			bool sameBinaryData;
			bool sameBinaryCommand;

			sameAxis = x.TargetAxis == y.TargetAxis;
			sameTargetNumber = x.TargetDeviceNumber == y.TargetDeviceNumber;
			sameBinaryData = x.BinaryData == y.BinaryData;
			sameBinaryCommand = x.BinaryCommand == y.BinaryCommand;

			//Split into tokens and compare so that end spaces don't matter.
			if ((null == y.CommandName) || (null == x.CommandName))
			{
				sameCommand = y.CommandName == x.CommandName;
			}

			else
			{
				var thisCommand = x.CommandName.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
				var thatCommand = y.CommandName.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

				sameCommand = thisCommand.SequenceEqual(thatCommand);
			}

			return sameAxis && sameCommand && sameTargetNumber && sameBinaryCommand && sameBinaryData;
		}


		/// <summary>
		///     Probably not needed as this class is
		///     never put in a dictionary.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public int GetHashCode(KeyCommandInfo obj) => throw new NotImplementedException();

		#endregion
	}
}

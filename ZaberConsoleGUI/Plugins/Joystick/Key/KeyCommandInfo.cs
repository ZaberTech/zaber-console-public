﻿using System;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.Joystick.Key
{
	/// <summary>
	///     Represents the data behind both an ASCII
	///     and a Binary key command on the joystick.
	/// </summary>
	public class KeyCommandInfo : ObservableObject
	{
		#region -- Events --

		public event EventHandler CommandChanged;

		#endregion

		#region -- Public Methods & Properties --

		public KeyCommandInfo()
		{
			IsAddCommand = false;
		}


		/// <summary>
		///     Indicates whether this commands is the first row in ASCII mode
		///     that can add commands.
		/// </summary>
		public bool IsAddCommand
		{
			get => _isAddCommand;

			set => Set(ref _isAddCommand, value, nameof(IsAddCommand));
		}

		/// <summary>
		///     What device number key command is targeting. Used in ASCII and Binary.
		/// </summary>
		public string TargetDeviceNumber
		{
			get => _targetDevice;
			set
			{
				//don't invoke CommandChanged when this hasn't been initialized yet.
				if (null == TargetDeviceNumber)
				{
					Set(ref _targetDevice, value, nameof(TargetDeviceNumber));
				}

				else
				{
					Set(ref _targetDevice, value, nameof(TargetDeviceNumber));
					CommandChanged?.Invoke(this, null);
				}
			}
		}

		/// <summary>
		///     What the ASCII command name of the command is.
		/// </summary>
		public string CommandName
		{
			get => _command;

			set
			{
				//don't invoke CommandChanged when this hasn't been initialized yet.
				if (null == CommandName)
				{
					Set(ref _command, value, nameof(CommandName));
				}

				else
				{
					Set(ref _command, value, nameof(CommandName));
					CommandChanged?.Invoke(this, null);
				}
			}
		}

		/// <summary>
		///     Which normal device axis that this device is targeting.
		/// </summary>
		public string TargetAxis
		{
			get => _targetAxis;

			set
			{
				if (null == TargetAxis)
				{
					Set(ref _targetAxis, value, nameof(TargetAxis));
				}

				else
				{
					Set(ref _targetAxis, value, nameof(TargetAxis));
					CommandChanged?.Invoke(this, null);
				}
			}
		}

		/// <summary>
		///     The command number of the binary command. For example, Move Relative
		///     corresponds to command number 21.
		/// </summary>
		public string BinaryCommand
		{
			get => _binaryCommand;

			set
			{
				Set(ref _binaryCommand, value, nameof(BinaryCommand));
				CommandChanged?.Invoke(this, null);
			}
		}

		/// <summary>
		///     The data sent through the binary command.
		/// </summary>
		public string BinaryData
		{
			get => _binaryData;

			set
			{
				if (null == BinaryData)
				{
					Set(ref _binaryData, value, nameof(BinaryData));
				}

				else
				{
					Set(ref _binaryData, value, nameof(BinaryData));
					CommandChanged?.Invoke(this, null);
				}
			}
		}

		/// <summary>
		///     Whether this key command info has been changed.
		/// </summary>
		public bool HasBeenChanged
		{
			get => _hasBeenChanged;

			set => Set(ref _hasBeenChanged, value, nameof(HasBeenChanged));
		}

		public bool IsEditing { get; set; } = false;

		#endregion

		#region -- Data --

		private bool _hasBeenChanged;
		private string _command;
		private string _targetDevice;
		private string _targetAxis;
		private string _binaryCommand;
		private string _binaryData;
		private bool _isAddCommand;

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Zaber;
using ZaberConsole.Plugins.CommandList;
using ZaberConsole.Plugins.Joystick.ConvertersBehaviors;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.Joystick.Key
{
	/// <summary>
	///     View model representing the grid that contains all of the
	///     commands and their information as well as the row that adds
	///     key commands.
	///     In Binary, this class is used the same, but <see cref="KeyCommands" /> is not used, just <see cref="BinaryInfo" />.
	///     This is a bit of a hacky solution, will refactor if time permits.
	/// </summary>
	public class KeyCommandGridVM : ObservableObject
	{
		#region -- Events --

		public event EventHandler SelectionChanged;

		public event EventHandler CommandsChangedEvent;

		#endregion

		#region -- Public Methods & Properties --

		public KeyCommandGridVM(int id)
		{
			ID = id;

			//in ASCII _addCommandRow is the first row you use for
			//adding commands.
			//in Binary it doesn't do anything.
			_addCommandRow = new KeyCommandInfo();
			_addCommandRow.IsAddCommand = true;

			KeyCommands.Add(_addCommandRow);
			PreviousReadCommands.Add(_addCommandRow);

			DropHandler = new GridDropHandler();
			DragHandler = new GridDragHandler();

			_keyCommands.CollectionChanged += InfoChangedMethod;
		}


		/// <summary>
		///     Method for taking the command information
		///     from this grid and returning them as a list
		///     of strings that resemble the original command.
		///     Should only be used in ASCII.
		/// </summary>
		/// <param name="returnOldInformation">
		///     Set to true if we want to return
		///     the previously read information
		/// </param>
		/// <returns>
		///     A list of strings in the form of
		///     "(device#) (axis#) (commandname)"
		/// </returns>
		public List<string> FormatCommands(bool returnOldInformation = false)
		{
			var FormattedCommands = new List<string>();
			IEnumerable<KeyCommandInfo> gridToFormat;

			if (returnOldInformation)
			{
				gridToFormat = PreviousReadCommands;
			}
			else
			{
				gridToFormat = KeyCommands;
			}

			foreach (var commandInfo in gridToFormat)
			{
				//make sure that we don't return the add command                
				if (commandInfo.IsAddCommand != true)
				{
					var ToAdd = commandInfo.TargetDeviceNumber
							+ " "
							+ commandInfo.TargetAxis
							+ " "
							+ commandInfo.CommandName;
					FormattedCommands.Add(ToAdd);
				}
			}

			return FormattedCommands;
		}


		/// <summary>
		///     Wrapper command for clearing the command grid for initialization.
		///     Adds the commandInfo to the end of the grid, detects for changes on
		///     that commandInfo and doesn't cause unsaved changes.
		/// </summary>
		/// <param name="commandInfo"></param>
		public void AddCommandAscii(KeyCommandInfo commandInfo)
		{
			//Note that the below method seems to deep copy the commandInfo's strings into commandInfoDeepCopy.
			//My original assumption was that it was a shallow copy, but for some reason
			//when I modify one of the string fields in commandInfoDeepCopy, it doesn't refer to commandInfo's string field.

			var commandInfoDeepCopy = new KeyCommandInfo
			{
				TargetAxis = commandInfo.TargetAxis,
				CommandName = commandInfo.CommandName,
				TargetDeviceNumber = commandInfo.TargetDeviceNumber
			};

			//Add the copies to their respective containers, which will be compared against to see if unsaved changes
			//have occured.

			PreviousReadCommands.Add(commandInfoDeepCopy);
			KeyCommands.Add(commandInfo);

			commandInfo.CommandChanged += InfoChangedMethod;
		}


		/// <summary>
		///     The same as <see cref="AddCommandAscii(KeyCommandInfo)" />. Used
		///     for when the port is in binary mode.
		/// </summary>
		/// <param name="binaryCommand"></param>
		public void AddCommandBinary(KeyCommandInfo binaryCommand)
		{
			IsEmptyCommand = false;

			var commandInfoDeepCopy = new KeyCommandInfo
			{
				BinaryCommand = binaryCommand.BinaryCommand,
				TargetDeviceNumber = binaryCommand.TargetDeviceNumber,
				BinaryData = binaryCommand.BinaryData
			};

			//Add the copies to their respective containers, which will be compared against to see if unsaved changes
			//have occured.
			PreviousReadCommands.Add(commandInfoDeepCopy);
			KeyCommands.Add(binaryCommand);

			binaryCommand.CommandChanged += InfoChangedMethod;

			//Since BinaryInfo is only a getter, we need to manually call onpropertychanged.
			OnPropertyChanged(nameof(BinaryInfo));
		}


		/// <summary>
		///     Wrapper command for clearing the command grid for initialization. Preserves the
		///     add command row, and doesn't cause unsaved changes.
		/// </summary>
		public void ClearCommands()
		{
			//populate pervious read commands first so that equality comparisons fired
			//as a result of KeyCommands.CollectionChanged works.
			PreviousReadCommands.Clear();
			PreviousReadCommands.Add(_addCommandRow);

			KeyCommands.Clear();
			KeyCommands.Add(_addCommandRow);
		}


		/// <summary>
		///     Determines if there have been changes since last read or write,
		///     also updates the smart targeting information in Binary.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public void InfoChangedMethod(object sender, EventArgs e)
		{
			//AlertsEnabled and PreviousAlertsEnabled both default to false if in binary.
			HasUnsavedChanges = !(KeyGridSequenceEqual(PreviousReadCommands, KeyCommands, new KeyInfoComparer())
							  && (PreviousAlertsEnabled == AlertsEnabled));

			//Needs to be updated manually to make the grid text appear red.
			OnPropertyChanged(nameof(HasUnsavedChanges));

			//KeyVM hooks onto this event to see if it should display the
			//unsaved changes red text.
			CommandsChangedEvent?.Invoke(this, null);

			//Binary smart detection for device name and command.
			if ((BinaryInfo == null)
			|| (BinaryInfo.TargetDeviceNumber == null)
			|| (BinaryInfo.BinaryCommand == null)
			|| (ConnectedDevices == null))
			{
				return;
			}

			byte targetingDeviceNumber;
			byte commandNumber;

			//Don't do anything if the number is larger than a byte. Error handling occurs when
			//the user clicks "Write" and handled in the JoystickPluginVM's WriteKeyInfoBodyBinary method.
			try
			{
				targetingDeviceNumber = byte.Parse(BinaryInfo.TargetDeviceNumber);
				commandNumber = byte.Parse(BinaryInfo.BinaryCommand);
			}

			catch
			{
				return;
			}

			//Smart target which device the command is targeting.
			TargetDeviceNameBinary = "Device Not Currently Connected";
			foreach (var device in ConnectedDevices)
			{
				if (device.DeviceNumber == targetingDeviceNumber)
				{
					TargetDeviceNameBinary = device.DeviceType.Name;
				}
			}

			//Make the combo box's selected command correspond to the command number.
			foreach (var info in BinaryCommands)
			{
				if (commandNumber == (byte) info.CommandInfo.Command)
				{
					SelectedCommandBinary = info;
					break;
				}

				SelectedCommandBinary = null;
			}

			//Empty command
			if ((((int) Command.Error).ToString() == BinaryInfo.BinaryCommand)
			&& ("0" == BinaryInfo.BinaryData)
			&& (ALL_DEVICES == BinaryInfo.TargetDeviceNumber))
			{
				IsEmptyCommand = true;
			}
		}


		/// <summary>
		///     Using a modified version of .NET's implementation of Enumerable.SequenceEqual
		///     http://referencesource.microsoft.com/#System.Core/System/Linq/Enumerable.cs,577032c8811e20d3
		///     Used for indicating which individual key commands have been modified.
		/// </summary>
		/// <typeparam name="TSource"></typeparam>
		/// <param name="previousCommands">The previously read commands.</param>
		/// <param name="currentCommands"> The new commands to compare to the previously read commands.</param>
		/// <param name="comparer">A custom comparer for key commands.</param>
		/// <returns></returns>
		public static bool KeyGridSequenceEqual(IEnumerable<KeyCommandInfo> previousCommands,
												IEnumerable<KeyCommandInfo> currentCommands,
												IEqualityComparer<KeyCommandInfo> comparer)
		{
			var toReturn = true;

			if (comparer == null)
			{
				throw new ArgumentNullException("This method should not be using a null comparer.");
			}

			if (previousCommands == null)
			{
				throw new ArgumentNullException("KeyGridSequenceEqual's first parameter is null.");
			}

			if (currentCommands == null)
			{
				throw new ArgumentNullException("KeyGridSequenceEqual's second parameter is null.");
			}

			using (var e1 = previousCommands.GetEnumerator())
			{
				using (var e2 = currentCommands.GetEnumerator())
				{
					while (e1.MoveNext())
					{
						//If the size of the commands are different, 
						//or the actual command compared is different we return false for not equal
						//and tag all of the commands that are not equal.
						if (!e2.MoveNext() || !comparer.Equals(e1.Current, e2.Current))
						{
							toReturn = false;

							if (e2.Current != null)
							{
								e2.Current.HasBeenChanged = true;
							}
						}

						else
						{
							e2.Current.HasBeenChanged = false;
						}
					}

					while (e2.MoveNext())
					{
						toReturn = false;
						e2.Current.HasBeenChanged = true;
					}
				}
			}

			return toReturn;
		}

		#endregion

		#region -- Data --

		internal const string ALL_DEVICES = "255";
		private readonly string _commandDetection = "";
		private readonly KeyCommandInfo _addCommandRow;

		private bool _eventFired;
		private bool _isEmptyCommand;
		private bool _addingError;
		private ObservableCollection<CommandInfoVM> _binaryCommands;
		private ObservableCollection<KeyCommandInfo> _keyCommands = new ObservableCollection<KeyCommandInfo>();
		private string _gridName;
		private bool _alertsEnabled;
		private KeyCommandInfo _selectedItem;
		private string _targetDeviceNameBinary;
		private CommandInfoVM _selectedCommandBinary;

		#endregion

		#region -- Commands and Private Methods --

		public ICommand AddRowCommand => new RelayCommand(aKeyCommandInfo => AddRow(aKeyCommandInfo, null));


		public ICommand DeleteRowCommand => new RelayCommand(aKeyCommandInfo => DeleteRow(aKeyCommandInfo, null));


		/// <summary>
		///     Adds a new key command in ASCII mode.
		/// </summary>
		/// <param name="aRow">The null key command, or KeyCommands[0].</param>
		/// <param name="e"></param>
		private void AddRow(object aRow, EventArgs e)
		{
			var aKeyCommandInfo = aRow as KeyCommandInfo;

			//This can be sent from a KeyCommandInfo that isn't an add command. We must check for that.
			if (!aKeyCommandInfo.IsAddCommand)
			{
				return;
			}

			//The information must be at least populated
			if ((aKeyCommandInfo.TargetDeviceNumber == null) || (aKeyCommandInfo.CommandName == null))
			{
				AddingError = true;
				AddingError = false;
				return;
			}

			//Check the device number and axis constraints.
			if ((long.Parse(aKeyCommandInfo.TargetDeviceNumber) > 99)
			|| ((aKeyCommandInfo.TargetAxis != null) && (long.Parse(aKeyCommandInfo.TargetAxis) > 9)))
			{
				AddingError = true;
				AddingError = false;
				return;
			}

			var commandInfoCopy = new KeyCommandInfo
			{
				CommandName = aKeyCommandInfo.CommandName,
				TargetDeviceNumber = aKeyCommandInfo.TargetDeviceNumber
			};

			//If no axis was entered, we enter a 0 by default.
			if (aKeyCommandInfo.TargetAxis != null)
			{
				commandInfoCopy.TargetAxis = aKeyCommandInfo.TargetAxis;
			}

			else
			{
				commandInfoCopy.TargetAxis = "0";
			}

			KeyCommands.Add(commandInfoCopy);

			aKeyCommandInfo.TargetAxis = null;
			aKeyCommandInfo.CommandName = null;
			aKeyCommandInfo.TargetDeviceNumber = null;
		}


		/// <summary>
		///     Executed when the red minus sign is clicked, removes the command.
		/// </summary>
		/// <param name="aRow">The key command to remove.</param>
		/// <param name="e"></param>
		private void DeleteRow(object aRow, EventArgs e)
		{
			var aKeyCommandInfo = aRow as KeyCommandInfo;
			if (aKeyCommandInfo.IsAddCommand)
			{
				return;
			}

			KeyCommands.Remove(aKeyCommandInfo);
		}


		/// <summary>
		///     Executed when the "Clear" button is clicked.
		/// </summary>
		/// <param name="sender">Not used.</param>
		/// <param name="e">Not used.</param>
		private void ClearBinaryInfo(object sender, EventArgs e)
		{
			BinaryInfo.BinaryCommand = ((int) Command.Error).ToString();
			BinaryInfo.BinaryData = "0";
			BinaryInfo.TargetDeviceNumber = ALL_DEVICES;
		}

		#endregion

		#region -- Getters and Setters--

		/// <summary>
		///     Clears the binary command if the "Clear" button is clicked.
		/// </summary>
		public ICommand ClearBinaryInfoCommand => new RelayCommand(_ => ClearBinaryInfo(null, null));

		/// <summary>
		///     A collection of KeyCommandInfo is bound to the datagrid in ASCII.
		///     Don't modify this directly, use the wrapper commands AddCommands, AddCommandsBinary, and ClearCommands.
		///     The first index of this collection is ALWAYS a null key command that is for adding rows in ASCII.
		///     In binary, the first index of this collection is just a null key command that is never touched.
		/// </summary>
		public ObservableCollection<KeyCommandInfo> KeyCommands
		{
			get => _keyCommands;
			set => Set(ref _keyCommands, value, nameof(KeyCommands));
		}

		/// <summary>
		///     The device name of device the binary command is targeting.
		/// </summary>
		public string TargetDeviceNameBinary
		{
			get => _targetDeviceNameBinary;
			set => Set(ref _targetDeviceNameBinary, value, nameof(TargetDeviceNameBinary));
		}

		/// <summary>
		///     The name of this key event, e.g. ("Key Event 1 : Press")
		/// </summary>
		public string GridName
		{
			get => _gridName;
			set => Set(ref _gridName, value, nameof(GridName));
		}

		/// <summary>
		///     Whether the Alerts check box is ticked in ASCII.
		/// </summary>
		public bool AlertsEnabled
		{
			get => _alertsEnabled;
			set
			{
				Set(ref _alertsEnabled, value, nameof(AlertsEnabled));
				InfoChangedMethod(this, null);
			}
		}

		/// <summary>
		///     The key event of this grid, from 1-4.
		/// </summary>
		public int ID { get; }

		/// <summary>
		///     The selected row. Only 1 row can be selected at a time.
		/// </summary>
		public KeyCommandInfo SelectedItem
		{
			get => _selectedItem;
			set
			{
				Set(ref _selectedItem, value, nameof(SelectedItem));

				if (value != null)
				{
					SelectionChanged?.Invoke(this, null);
				}
			}
		}

		/// <summary>
		///     Custom drophandler that is bound for dragging and dropping.
		/// </summary>
		public GridDropHandler DropHandler { get; set; }

		/// <summary>
		///     Custom drag handler that is bound for dragging and dropping.
		/// </summary>
		public GridDragHandler DragHandler { get; set; }

		/// <summary>
		///     The binary key information for this event.
		/// </summary>
		public KeyCommandInfo BinaryInfo
		{
			get
			{
				if (KeyCommands.Count > 1)
				{
					//The KeyCommands collection in binary always should only contain 2 keycommands,
					//a null KeyCommandInfo on index 0 and the actual binary info on index 1. 
					//The null KeyCommandInfo is for the ASCII add command row.
					return KeyCommands[1];
				}

				//This is done to fight against WPF data binding errors which may lag the program.
				return new KeyCommandInfo();
			}
		}

		/// <summary>
		///     Whether this grid has unsaved changes.
		/// </summary>
		public bool HasUnsavedChanges { get; set; }

		/// <summary>
		///     Used to check if settings have changed.
		/// </summary>
		public bool PreviousAlertsEnabled { get; set; }

		/// <summary>
		///     A list of all connected devices for smart targeting.
		/// </summary>
		public List<ZaberDevice> ConnectedDevices { get; set; }

		/// <summary>
		///     A list of binary commands that the user can select from.
		/// </summary>
		public ObservableCollection<CommandInfoVM> BinaryCommands
		{
			get => _binaryCommands;

			set => Set(ref _binaryCommands, value, nameof(BinaryCommands));
		}

		/// <summary>
		///     The selected binary command corresponding to the command number.
		/// </summary>
		public CommandInfoVM SelectedCommandBinary
		{
			get => _selectedCommandBinary;

			set
			{
				if (value == SelectedCommandBinary)
				{
					return;
				}

				Set(ref _selectedCommandBinary, value, nameof(SelectedCommandBinary));

				if (SelectedCommandBinary != null)
				{
					//Start editing commands again if the command was cleared previously
					if (IsEmptyCommand && (value.CommandInfo.Command != Command.Error))
					{
						IsEmptyCommand = false;
						BinaryInfo.BinaryCommand = ((byte) SelectedCommandBinary.CommandInfo.Command).ToString();
						BinaryInfo.BinaryData = "0";
						BinaryInfo.TargetDeviceNumber = "0";
					}

					if (BinaryInfo.BinaryCommand != null)
					{
						//Don't allow infinite setter recursion.
						if ((int) SelectedCommandBinary.CommandInfo.Command == int.Parse(BinaryInfo.BinaryCommand))
						{
							return;
						}

						BinaryInfo.BinaryCommand = ((byte) SelectedCommandBinary.CommandInfo.Command).ToString();
					}
				}
			}
		}

		/// <summary>
		///     Shows an error notification when true.
		/// </summary>
		public bool AddingError
		{
			get => _addingError;

			set => Set(ref _addingError, value, nameof(AddingError));
		}

		/// <summary>
		///     This is a special property which indicates if the command values are
		///     Device # 255, Error Command, Data Value 0. We represent this as the empty command in binary.
		/// </summary>
		public bool IsEmptyCommand
		{
			get => _isEmptyCommand;

			set
			{
				Set(ref _isEmptyCommand, value, nameof(IsEmptyCommand));

				//Make the combobox blank when we clear the binary command.
				if (value)
				{
					SelectedCommandBinary = null;
				}
			}
		}

		/// <summary>
		///     The empty string that is bound to the textbox
		///     when the command is cleared. After this textbox is changed,
		///     the user can start editing the command again.
		/// </summary>
		public string CommandDetectionDevice
		{
			get => _commandDetection;

			set
			{
				IsEmptyCommand = false;
				BinaryInfo.BinaryCommand = "0";
				BinaryInfo.BinaryData = "0";
				BinaryInfo.TargetDeviceNumber = value;
			}
		}

		/// <summary>
		///     The empty string that is bound to the textbox
		///     when the command is cleared. After this textbox is changed,
		///     the user can start editing the command again.
		/// </summary>
		public string CommandDetectionData
		{
			get => _commandDetection;

			set
			{
				IsEmptyCommand = false;
				BinaryInfo.BinaryCommand = "0";
				BinaryInfo.BinaryData = value;
				BinaryInfo.TargetDeviceNumber = "0";
			}
		}

		/// <summary>
		///     The empty string that is bound to the textbox
		///     when the command is cleared. After this textbox is changed,
		///     the user can start editing the command again.
		/// </summary>
		public string CommandDetectionCommand
		{
			get => _commandDetection;

			set
			{
				IsEmptyCommand = false;
				BinaryInfo.BinaryCommand = value;
				BinaryInfo.BinaryData = "0";
				BinaryInfo.TargetDeviceNumber = "0";
			}
		}

		/// <summary>
		///     Makes a little green message appear when the key is pressed
		///     and alerts are enabled.
		/// </summary>
		public bool EventFired
		{
			get => _eventFired;

			set
			{
				Set(ref _eventFired, value, nameof(EventFired));

				if (value)
				{
					EventFired = false;
				}
			}
		}

		/// <summary>
		///     Returns the perviously read commands
		/// </summary>
		public List<KeyCommandInfo> PreviousReadCommands { get; } = new List<KeyCommandInfo>();

		#endregion
	}
}

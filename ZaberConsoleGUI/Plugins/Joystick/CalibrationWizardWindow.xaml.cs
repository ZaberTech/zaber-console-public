﻿using System.Windows;

namespace ZaberConsole.Plugins.Joystick
{
	/// <summary>
	///     Interaction logic for CalibrationWizardWindow.xaml
	/// </summary>
	public partial class CalibrationWizardWindow : Window
	{
		#region -- Public Methods & Properties --

		public CalibrationWizardWindow()
		{
			InitializeComponent();
		}

		#endregion
	}
}

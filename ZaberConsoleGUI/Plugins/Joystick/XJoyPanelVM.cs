﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Input;
using MvvmDialogs.ViewModels;
using Zaber;
using Zaber.Telemetry;
using ZaberConsole.Plugins.Joystick.JoystickConfig;
using ZaberConsole.Plugins.Joystick.Stick;
using ZaberConsole.Properties;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Dialogs;

namespace ZaberConsole.Plugins.Joystick
{
	/// <summary>
	///     Handles all of the view logic for the left panel of the plugin as well as
	///     handling a the joy.debug and comm.alert settings.
	/// </summary>
	public class XJoyPanelVM : ObservableObject, IDialogClient
	{
		#region --Public Methods and Constructor--

		/// <summary>
		///     Initializes important data for this class.
		/// </summary>
		/// <param name="parent">The parent plugin. We need reference to a few of its members.</param>
		/// <param name="protocol">Whether the device is currently in ASCII or Binary mode.</param>
		public XJoyPanelVM(JoystickPluginVM parent, Protocol protocol)
		{
			_selectedTab = parent.SelectedTab;
			_conversation = parent.Conversation;
			_clearKeysWindow = new ClearKeysWindowVM(parent, protocol);

			_parent = parent;
			_protocol = protocol;

			for (var i = 0; i < 10; i++)
			{
				Buttons.Add(new ButtonVM(i));
			}

			//Makes sure the inversion is correct when the plugin is reinitialized while viewing
			//joystick information.
			for (var i = 1; i < 4; i++)
			{
				ChangeInversionGraphic(parent.Joysticks[i]);
			}

			if (protocol == Protocol.ASCII)
			{
				IsAsciiMode = true;
			}

			else
			{
				IsAsciiMode = false;
			}

			if (parent.Conversation.Device.DeviceType.DeviceId == 3003)
			{
				IsXJOY = false;
			}

			_restoreModalWindow = new CustomMessageBoxVM(
				"Are you sure you want to restore the joystick? This will delete all of your current settings.",
				"Restore Joystick",
				"OK",
				"Cancel");
			_restoreModalWindow.IsModal = true;
			_restoreModalWindow.DialogClosing += RestoreModalWindow_DialogClosing;
		}


		/// <summary>
		///     Changes the visibility bindings of the arrows and plus signs on the joystick.
		/// </summary>
		/// <param name="aJoystick"></param>
		public void ChangeInversionGraphic(JoystickVM aJoystick)
		{
			var joystickAxis = int.Parse(aJoystick.JoystickAxisNumber);
			InvertedAxes[joystickAxis] = aJoystick.ViewInfo.IsInverted;
		}

		#endregion

		#region --Commands--

		/// <summary>
		///     Opens the Clear Keys Window.
		/// </summary>
		public ICommand ShowClearWindowCommand => new RelayCommand(_ => ShowClearWindow());

		/// <summary>
		///     Opens a modal window that prompts the user if they want to restore their joystick settings.
		/// </summary>
		public ICommand OpenRestoreWindowCommand
			=> new RelayCommand(_ => RequestDialogOpen?.Invoke(this, _restoreModalWindow));

		/// <summary>
		///     Shows the help manual for this tab.
		/// </summary>
		public ICommand ViewJoystickPluginHelpCommand => new RelayCommand(_ => ViewJoystickPluginHelp());

		/// <summary>
		///     Opens the calibration wizard for calibrating the joystick.
		/// </summary>
		public ICommand OpenCalibrationWizardCommand => new RelayCommand(_ =>
		{
			var wizardWindow = new CalibrationWizardWindowVM(_protocol, _conversation);
			RequestDialogOpen?.Invoke(this, wizardWindow);
			wizardWindow.RequestDialogOpen += WizardWindow_RequestDialogOpen;
		});

		/// <summary>
		///     Opens the saving and loading joystick settings window.
		/// </summary>
		public ICommand OpenJoystickConfigsCommand => new RelayCommand(_ =>
		{
			var joystickConfigs = new JoystickConfigurationVM(_parent, _protocol);
			RequestDialogOpen?.Invoke(this, joystickConfigs);
		});

		/// <summary>
		///     Sends an ASCII command to the joystick device.
		/// </summary>
		public ICommand SendAsciiCommand => new RelayCommand(commandString =>
		{
			var asciiCommand = commandString as string;
			_conversation.Send(asciiCommand);
		});

		#endregion

		#region -- Private Methods--

		/// <summary>
		///     Shows the clear keys window.
		/// </summary>
		private void ShowClearWindow() => RequestDialogOpen?.Invoke(this, _clearKeysWindow);


		/// <summary>
		///     Shows the modal window for restoring settings.
		/// </summary>
		/// <param name="sender">Not used.</param>
		/// <param name="e">Not used.</param>
		private void RestoreModalWindow_DialogClosing(object aSender, EventArgs e)
		{
			if (MessageBoxResult.Yes == (MessageBoxResult) _restoreModalWindow.DialogResult)
			{
				RestoreJoystick();
				_eventLog.LogEvent("Restore menu item used");
			}
		}


		/// <summary>
		///     Opens the calibration wizard window.
		/// </summary>
		/// <param name="aSender"></param>
		/// <param name="aVM"></param>
		private void WizardWindow_RequestDialogOpen(object aSender, IDialogViewModel aVM)
			=> RequestDialogOpen?.Invoke(this, aVM);


		/// <summary>
		///     Shows the website for the manual if online mode is enabled
		///     otherwise it shows the offline help manual.
		/// </summary>
		private void ViewJoystickPluginHelp()
		{
			if (Settings.Default.AllowNetworkAccess)
			{
				Process.Start("https://www.zaber.com/wiki/Software/Zaber_Console/Joystick_Setup_Tab");
			}
			else
			{
				var baseUrl = AppDomain.CurrentDomain.BaseDirectory;
				baseUrl = Path.Combine(baseUrl, OFFLINE_JOYSTICK_HELP_PATH);
				baseUrl = new Uri(Path.GetFullPath(baseUrl)).AbsoluteUri;
				Process.Start(baseUrl);
			}

			_eventLog.LogEvent("Show help");
		}


		/// <summary>
		///     Restores the joystick to factory default settings.
		/// </summary>
		private void RestoreJoystick()
		{
			//throws a Zaber.RequestTimeoutException 
			try
			{
				if (_protocol == Protocol.ASCII)
				{
					_conversation.Request("system restore");
				}

				else
				{
					_conversation.Request(Command.RestoreSettings);
				}

				var portName = _parent.PortFacade.Port.PortName;
				_parent.PortFacade.Close();
				_parent.PortFacade.Open(portName);
			}
			catch
			{
				//refresh the plugin
				var portName = _parent.PortFacade.Port.PortName;
				_parent.PortFacade.Close();
				_parent.PortFacade.Open(portName);
			}
		}

		#endregion

		#region --Getters and Setters--

		/// <summary>
		///     Whether comm.alert is on.
		/// </summary>
		public bool AlertsOn
		{
			get => _alertsOn;

			set => Set(ref _alertsOn, value, nameof(AlertsOn));
		}


		/// <summary>
		///     If the device is a T-JOY, buttons 6-8 are greyed out.
		/// </summary>
		public bool IsXJOY { get; set; } = true;


		/// <summary>
		///     Whether joy.debug is on.
		/// </summary>
		public bool DebugOn
		{
			get => _debugOn;

			set => Set(ref _debugOn, value, nameof(DebugOn));
		}


		/// <summary>
		///     10 Buttons are inside this collection. Index 0 is a dummy button,
		///     indices 1-8 are key buttons, and index 9 is a joystick button.
		/// </summary>
		public ObservableCollection<ButtonVM> Buttons
		{
			get => _buttons;

			set => Set(ref _buttons, value, nameof(Buttons));
		}

		/// <summary>
		///     Used for determining inversion graphics. 4 entries, first index is a dummy one.
		/// </summary>
		public ObservableCollection<bool> SelectedTab
		{
			get => _selectedTab;

			set => Set(ref _selectedTab, value, nameof(SelectedTab));
		}

		/// <summary>
		///     Which axes are inverted. 4 entries, first index is a dummy one.
		/// </summary>
		public ObservableCollection<bool> InvertedAxes { get; set; } = new ObservableCollection<bool>
		{
			false,
			false,
			false,
			false
		};

		/// <summary>
		///     Whether this plugin is in ASCII mode or not.
		/// </summary>
		public bool IsAsciiMode { get; set; }

		#endregion

		#region --Data Members--

		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Joystick tab");

		private ObservableCollection<bool> _selectedTab;
		private ObservableCollection<ButtonVM> _buttons = new ObservableCollection<ButtonVM>();
		private bool _alertsOn = true;
		private bool _debugOn = true;

		private ObservableCollection<bool> _notInvertedAxes = new ObservableCollection<bool>
		{
			true,
			true,
			true,
			true
		};

		private readonly Conversation _conversation;
		private readonly Protocol _protocol;
		private readonly JoystickPluginVM _parent;
		private readonly CustomMessageBoxVM _restoreModalWindow;

		public event RequestDialogChange RequestDialogOpen;
		#pragma warning disable CS0067 // Event is never used.
		public event RequestDialogChange RequestDialogClose;
		#pragma warning restore CS0067

		private readonly ClearKeysWindowVM _clearKeysWindow;

		public const string OFFLINE_JOYSTICK_HELP_PATH = "Help/Manuals/Joystick/Joystick_Setup_Tab.html";

		#endregion
	}
}

﻿using System.Windows;

namespace ZaberConsole.Plugins.Joystick
{
	/// <summary>
	///     Interaction logic for ClearKeysWindow.xaml
	/// </summary>
	public partial class ClearKeysWindow : Window
	{
		#region -- Public Methods & Properties --

		public ClearKeysWindow()
		{
			InitializeComponent();
		}

		#endregion
	}
}

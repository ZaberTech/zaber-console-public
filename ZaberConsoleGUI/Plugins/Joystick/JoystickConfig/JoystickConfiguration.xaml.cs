﻿using System.Windows;

namespace ZaberConsole.Plugins.Joystick.JoystickConfig
{
	/// <summary>
	///     Interaction logic for JoystickConfiguration.xaml
	/// </summary>
	public partial class JoystickConfiguration : Window
	{
		#region -- Public Methods & Properties --

		public JoystickConfiguration()
		{
			InitializeComponent();
		}

		#endregion
	}
}

﻿namespace ZaberConsole.Plugins.Joystick.JoystickConfig
{
	/// <summary>
	///     A class with view information bound to the list of configurations.
	/// </summary>
	public class JoystickConfigView
	{
		#region -- Public Methods & Properties --

		public string Description { get; set; }

		public string Date { get; set; }

		public string Protocol { get; set; }

		#endregion
	}
}

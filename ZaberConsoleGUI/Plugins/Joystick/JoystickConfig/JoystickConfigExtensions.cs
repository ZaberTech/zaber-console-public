﻿using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Static class for generating a useful extension method used for parsing. This
/// is useful for iterating through a collection so that 
/// a null collection can be "iterated" through.
/// </summary>
namespace ZaberConsole.Plugins.Joystick.JoystickConfig
{
	public static class JoystickConfigExtensions
	{
		#region -- Public Methods & Properties --

		public static IEnumerable<T> OrEmptyIfNull<T>(this IEnumerable<T> source) => source ?? Enumerable.Empty<T>();

		#endregion
	}
}

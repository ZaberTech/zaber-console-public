﻿using System.Collections.Generic;
using YamlDotNet.Serialization;

namespace ZaberConsole.Plugins.Joystick.JoystickConfig
{
	/// <summary>
	///     The data structure for the deserialized data.
	///     To better understand the .yaml serialization format, save
	///     the current settings on the Saving and Loading window and
	///     check the displayed text. That displayed text is exactly representative of
	///     of the way the text is serialized in the .yaml format.
	/// </summary>
	public class JoystickConfigData
	{
		#region -- Public Methods & Properties --

		public int SerializationVersion { get; set; } = 1;

		public string Date { get; set; }

		public string Description { get; set; }

		public string Protocol { get; set; }

		public Dictionary<string, JoystickAxis> Joysticks { get; set; }

		[YamlMember(Alias = "Binary Key Commands")]
		public Dictionary<string, KeyEventsDataBinary> BinaryKeys { get; set; }

		[YamlMember(Alias = "ASCII Key Commands")]
		public Dictionary<string, KeyEventsDataASCII> ASCIIKeys { get; set; }

		#endregion
	}

	/// <summary>
	///     Has one binary key information for every key event.
	///     There are 8 of these, one for each key.
	/// </summary>
	public class KeyEventsDataBinary
	{
		#region -- Public Methods & Properties --

		//The Alias is how it should appear in the .yaml file.

		[YamlMember(Alias = "Press (Event 1)")]
		public BinaryKeyData Press { get; set; }

		[YamlMember(Alias = "Quick Release (Event 2)")]
		public BinaryKeyData QuickRelease { get; set; }

		[YamlMember(Alias = "Hold 2 Seconds (Event 3)")]
		public BinaryKeyData Hold2Seconds { get; set; }

		[YamlMember(Alias = "Release after Hold (Event 4)")]
		public BinaryKeyData ReleaseAfterHold { get; set; }

		#endregion
	}

	/// <summary>
	///     Represents the information for one binary key.
	/// </summary>
	public class BinaryKeyData
	{
		#region -- Public Methods & Properties --

		[YamlMember(Alias = "Device Number")]
		public string DeviceNumber { get; set; }

		public string Command { get; set; }

		[YamlMember(Alias = "Command Name")]
		public string CommandName { get; set; }

		public string Data { get; set; }

		#endregion
	}

	/// <summary>
	///     Represents the information for one joystick axis.
	/// </summary>
	public class JoystickAxis
	{
		#region -- Public Methods & Properties --

		[YamlMember(Alias = "Device Number")]
		public string DeviceNumber { get; set; }

		[YamlMember(Alias = "Control Speed")]
		public string ControlSpeed { get; set; }

		[YamlMember(Alias = "Max Speed")]
		public string MaxSpeed { get; set; }

		public bool Inverted { get; set; }

		public string Resolution { get; set; }

		[YamlMember(Alias = "Lockstep Index")]
		public string LockstepIndex { get; set; }

		[YamlMember(Alias = "Virtual Index")]
		public string VirtualIndex { get; set; }

		[YamlMember(Alias = "Axis Number")]
		public string AxisNumber { get; set; }

		#endregion
	}

	/// <summary>
	///     Represents one key's key events and alert settings.
	///     Each key event just has a list of ASCII key commands
	///     represented as strings.
	/// </summary>
	public class KeyEventsDataASCII
	{
		#region -- Public Methods & Properties --

		[YamlMember(Alias = "Alert Settings for Events 1-4")]
		public Dictionary<int, bool> AlertsOn { get; set; } = new Dictionary<int, bool>();

		[YamlMember(Alias = "Press (Event 1)")]
		public List<string> Press { get; set; }

		[YamlMember(Alias = "Quick Release (Event 2)")]
		public List<string> QuickRelease { get; set; }

		[YamlMember(Alias = "Hold 2 Seconds (Event 3)")]
		public List<string> Hold2Seconds { get; set; }

		[YamlMember(Alias = "Release after Hold (Event 4)")]
		public List<string> ReleaseAfterHold { get; set; }

		#endregion
	}
}

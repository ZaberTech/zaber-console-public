﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using log4net;
using MvvmDialogs.ViewModels;
using YamlDotNet.Serialization;
using Zaber;
using Zaber.Telemetry;
using ZaberConsole.Plugins.Joystick.Stick;
using ZaberConsole.Properties;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Dialogs;

/// <summary>
/// This class is the VM behind the window for saving and loading
/// joystick configurations.
/// </summary>

namespace ZaberConsole.Plugins.Joystick.JoystickConfig
{
	public class JoystickConfigurationVM : ObservableObject, IUserDialogViewModel, IDialogClient
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Parse all of the information stored at the <see cref="CONFIG_FILE" /> and
		///     initialize this window.
		/// </summary>
		/// <param name="parent">The JoystickPluginVM that we will load to and save from.</param>
		/// <param name="protocol">If the plugin is currently in Binary or ASCII</param>
		public JoystickConfigurationVM(JoystickPluginVM parent, Protocol protocol)
		{
			_parent = parent;
			_protocol = protocol;

			ParseConfigurationFile();
		}


		/// <summary>
		///     Clicking the "Exit" button closes the window.
		/// </summary>
		public ICommand ExitCommand => new RelayCommand(_ => RequestClose());

		/// <summary>
		///     Clicking the "Save Current Settings" button saves the current settings
		///     of the joystick.
		/// </summary>
		public ICommand SaveCurrentSettingsCommand => new RelayCommand(_ => SaveCurrentSettings());

		/// <summary>
		///     Deletes a saved configuration.
		/// </summary>
		public ICommand DeleteCommand => new RelayCommand(_ =>
		{
			var toRemove = _viewToConfigMap[SelectedItem];

			_joystickConfigurations.Remove(toRemove);
			_viewToConfigMap.Remove(SelectedItem);
			VisibleConfigurations.Remove(SelectedItem);
			_eventLog.LogEvent("Saved settings deleted");
		});


		/// <summary>
		///     Command invoked by view button to export selected or all settings to a user-selected file.
		/// </summary>
		public ICommand ExportCommand => new RelayCommand(_ => ExportSettings());


		/// <summary>
		///     Command invoked by view button to import settings from a user-selected file.
		/// </summary>
		public ICommand ImportCommand => new RelayCommand(_ => ImportSettings());


		/// <summary>
		///     Loads a configuration onto the joystick.
		/// </summary>
		public ICommand LoadCommand => new RelayCommand(_ => LoadSettings());


		/// <summary>
		///     The clickable JoystickConfigViews which are bound to the view.
		/// </summary>
		public ObservableCollection<JoystickConfigView> VisibleConfigurations
		{
			get => _visibleConfigurations;

			set => Set(ref _visibleConfigurations, value, nameof(VisibleConfigurations));
		}

		/// <summary>
		///     The title that we will save our joystick config as if it is filled out.
		///     Otherwise we save it as "Settings (time)"
		/// </summary>
		public string SaveConfigTitle
		{
			get => _saveConfigTitle;

			set => Set(ref _saveConfigTitle, value, nameof(SaveConfigTitle));
		}

		/// <summary>
		///     Our bound view item. When changes, we change the display text.
		/// </summary>
		public JoystickConfigView SelectedItem
		{
			get => _selectedItem;

			set
			{
				if (value == null)
				{
					Set(ref _selectedItem, value, nameof(SelectedItem));
					DisplayConfigInformation = null;
					DeleteAllowed = false; //Enables or disables the "Delete" button.
					CorrectProtocol = false; //CorrectProtocol enables or disables the "Load" button.
					return;
				}

				Set(ref _selectedItem, value, nameof(SelectedItem));

				CorrectProtocol = value.Protocol == _protocol.ToString();
				DeleteAllowed = true;

				//Display the YAML information for the selected command.
				var serializer = new Serializer();
				var stringBuilder = new StringBuilder();
				serializer.Serialize(new StringWriter(stringBuilder), _viewToConfigMap[SelectedItem]);

				DisplayConfigInformation = stringBuilder.ToString();
			}
		}

		public bool DeleteAllowed
		{
			get => _deleteAllowed;

			set => Set(ref _deleteAllowed, value, nameof(DeleteAllowed));
		}

		/// <summary>
		///     The displayed yaml text.
		/// </summary>
		public string DisplayConfigInformation
		{
			get => _displayConfigInformation;

			set => Set(ref _displayConfigInformation, value, nameof(DisplayConfigInformation));
		}

		/// <summary>
		///     Whether we can load the configuration based on what our currently
		///     selected port protocol is.
		/// </summary>
		public bool CorrectProtocol
		{
			get => _correctProtocol;

			set => Set(ref _correctProtocol, value, nameof(CorrectProtocol));
		}

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Parse the <see cref="CONFIG_FILE" /> and
		///     display that information to the user.
		/// </summary>
		private void ParseConfigurationFile()
		{
			StringReader configInput;
			List<JoystickConfigData> joystickConfigurations;

			// Deserialize and parse the config file to check for saved configs.
			if (File.Exists(CONFIG_FILE))
			{
				configInput = new StringReader(File.ReadAllText(CONFIG_FILE));
				var deserializer = new Deserializer();
				joystickConfigurations = deserializer.Deserialize<List<JoystickConfigData>>(configInput);
			}

			// If the file doesn't exist we display nothing.
			else
			{
				joystickConfigurations = new List<JoystickConfigData>();
			}

			_joystickConfigurations = joystickConfigurations;

			// Put each configuration detected into an observable collection to display.
			foreach (var configuration in joystickConfigurations)
			{
				if (configuration.SerializationVersion == 1)
				{
					var configInfo = new JoystickConfigView();

					configInfo.Description = configuration.Description;
					configInfo.Date = configuration.Date;
					configInfo.Protocol = configuration.Protocol;

					_viewToConfigMap.Add(configInfo, configuration);
					VisibleConfigurations.Add(configInfo);
				}
				else
				{
					_log.Error(
						$"Serialization format {configuration.SerializationVersion} not recognized when loading joystick configurations.");
				}
			}
		}


		/// <summary>
		///     Loads the joystick with settings from a saved configuration.
		///     We are simply extracting information from the file which is read into
		///     a list of <see cref="JoystickConfigData" />. We write to the joystick
		///     with the extracted information and then re-read everything.
		/// </summary>
		private void LoadSettings()
		{
			if (_protocol == Protocol.ASCII)
			{
				LoadASCIISettings();
			}
			else
			{
				LoadBinarySettingsWarning();
			}

			_eventLog.LogEvent("Write saved settings to the joystick");
		}


		/// <summary>
		///     Loads ASCII settings.
		///     Note : we don't save comm.alert or joy.debug values.
		/// </summary>
		private void LoadASCIISettings()
		{
			for (var i = 1; i < 4; i++)
			{
				//This is our hacky way of accessing dictionary indices. >.NET does not provide
				//generic dictionaries that preserve insertion order, hence this method.
				var joystickData = _viewToConfigMap[SelectedItem].Joysticks["Joystick " + i];

				string joystickInfo;
				string inversion;

				//joystickInfo is the response from a "key info" command.
				//it looks something like "OK IDLE -- 2 1 10000 2 1 50"

				if (joystickData.Inverted)
				{
					inversion = "1";
				}

				else
				{
					inversion = "0";
				}

				if (joystickData.LockstepIndex != null)
				{
					joystickInfo = "OK IDLE -- "
							   + joystickData.DeviceNumber
							   + " "
							   + "lockstep "
							   + joystickData.LockstepIndex
							   + " "
							   + joystickData.MaxSpeed
							   + " "
							   + JoystickVM.ConvertToNumber(joystickData.ControlSpeed)
							   + " "
							   + inversion
							   + " "
							   + joystickData.Resolution;
				}

				else if (joystickData.VirtualIndex != null)
				{
					joystickInfo = "OK IDLE -- "
							   + joystickData.DeviceNumber
							   + " "
							   + "virtual "
							   + joystickData.VirtualIndex
							   + " "
							   + joystickData.MaxSpeed
							   + " "
							   + JoystickVM.ConvertToNumber(joystickData.ControlSpeed)
							   + " "
							   + inversion
							   + " "
							   + joystickData.Resolution;
				}

				//Targeting normal axis.
				else
				{
					joystickInfo = "OK IDLE -- "
							   + joystickData.DeviceNumber
							   + " "
							   + joystickData.AxisNumber
							   + " "
							   + joystickData.MaxSpeed
							   + " "
							   + JoystickVM.ConvertToNumber(joystickData.ControlSpeed)
							   + " "
							   + inversion
							   + " "
							   + joystickData.Resolution;
				}

				///Read this information into the joystickVM, so that we can use
				///<see cref="JoystickPluginVM.WriteJoystickInfoCommand"/>, which is a
				///command that sends joystick information to the device and is already written.
				_parent.Joysticks[i].ParseJoystickInfo(joystickInfo);
			}

			_parent.Conversation.Request("key clear all");

			//Populate the key command grids based on the configuration.
			var keySuccess = true;
			for (var i = 1; i < (JoystickPluginVM.NUMBER_OF_XJOY_KEYS + 1); i++)
			{
				var keyData = _viewToConfigMap[SelectedItem].ASCIIKeys["Key " + i];

				//Use the OrEmptyIfNull extension method because the key event
				//list of commands will be null if there were no commands, not empty.
				try
				{
					foreach (var command in keyData.Press.OrEmptyIfNull())
					{
						var parts = command.Split(new[] { ' ' }, 2, StringSplitOptions.RemoveEmptyEntries);
						_parent.Conversation.Request(_parent.Conversation.Device.FormatAsciiCommand(
														 "key {0} {1} add {2} {3}",
														 i,
														 1,
														 parts[0],
														 parts[1]));
					}

					foreach (var command in keyData.QuickRelease.OrEmptyIfNull())
					{
						var parts = command.Split(new[] { ' ' }, 2, StringSplitOptions.RemoveEmptyEntries);
						_parent.Conversation.Request(_parent.Conversation.Device.FormatAsciiCommand(
														 "key {0} {1} add {2} {3}",
														 i,
														 2,
														 parts[0],
														 parts[1]));
					}

					foreach (var command in keyData.Hold2Seconds.OrEmptyIfNull())
					{
						var parts = command.Split(new[] { ' ' }, 2, StringSplitOptions.RemoveEmptyEntries);
						_parent.Conversation.Request(_parent.Conversation.Device.FormatAsciiCommand(
														 "key {0} {1} add {2} {3}",
														 i,
														 3,
														 parts[0],
														 parts[1]));
					}

					foreach (var command in keyData.ReleaseAfterHold.OrEmptyIfNull())
					{
						var parts = command.Split(new[] { ' ' }, 2, StringSplitOptions.RemoveEmptyEntries);
						_parent.Conversation.Request(_parent.Conversation.Device.FormatAsciiCommand(
														 "key {0} {1} add {2} {3}",
														 i,
														 4,
														 parts[0],
														 parts[1]));
					}

					for (var j = 1; j < (JoystickPluginVM.NUMBER_OF_KEY_EVENTS + 1); j++)
					{
						if (keyData.AlertsOn[j])
						{
							_parent.Conversation.Request(_parent.Conversation.Device.FormatAsciiCommand(
															 "key {0} {1} alert {2}",
															 i,
															 j,
															 1));
						}
						else
						{
							_parent.Conversation.Request(_parent.Conversation.Device.FormatAsciiCommand(
															 "key {0} {1} alert {2}",
															 i,
															 j,
															 0));
						}
					}
				}
				catch (ErrorResponseException aErrorException)
				{
					keySuccess = false;
					_log.Error("Failed to restore key configuration; a command was rejected.", aErrorException);
				}
				catch (FormatException aFormatException)
				{
					keySuccess = false;
					_log.Error("Saved joystick key settings were invalid.", aFormatException);
				}
			}

			//Use the synchronous version of write joystick info because
			//all requests must be written before re-reading everything.
			var axisSuccess = true;
			axisSuccess &= _parent.WriteJoystickInfoBodyAscii(_parent.Joysticks[1]);
			axisSuccess &= _parent.WriteJoystickInfoBodyAscii(_parent.Joysticks[2]);
			axisSuccess &= _parent.WriteJoystickInfoBodyAscii(_parent.Joysticks[3]);

			//Switch conversations to re-initialize the plugin.
			var temp = _parent.Conversation;
			_parent.Conversation = null;
			_parent.Conversation = temp;

			if (!(axisSuccess && keySuccess))
			{
				var message = "There were errors while restoring the joystick configuration. Some ";
				if (!keySuccess)
				{
					if (!axisSuccess)
					{
						message += "keys and axes";
					}
					else
					{
						message += "keys";
					}
				}
				else if (!axisSuccess)
				{
					message += "axes";
				}

				message +=
					" may not have been restored to the intended states; please verify the joystick settings before using it.";

				var mbvm = new MessageBoxParams
				{
					Buttons = MessageBoxButton.OK,
					Caption = "Error restoring configuration",
					Icon = MessageBoxImage.Exclamation,
					Message = message
				};

				RequestDialogOpen?.Invoke(this, mbvm);
			}
		}


		/// <summary>
		///     Opens up a modal dialog to prompt the user if they want to go through
		///     with loading binary settings and warns them that deviecs may move.
		/// </summary>
		private void LoadBinarySettingsWarning()
		{
			var binaryLoadWarning = new CustomMessageBoxVM(
				"WARNING : When binary settings are loaded, all of the saved binary key settings will be sent. If devices are currently connected to the joystick, they may move. We recommend disconnecting the devices downstream of your joystick before you proceed and then reconnect those devices afterwards.",
				"Load Binary Settings",
				"Proceed",
				"Cancel");
			binaryLoadWarning.IsModal = true;

			binaryLoadWarning.DialogClosing += (anObject, aEventArgs) =>
			{
				if (MessageBoxResult.Yes == (MessageBoxResult) binaryLoadWarning.DialogResult)
				{
					LoadBinarySettings();
				}

				if (MessageBoxResult.No == (MessageBoxResult) binaryLoadWarning.DialogResult)
				{
				}
			};

			binaryLoadWarning.DoNotShowAgainSettings =
				Settings.Default.DoNotShowAgainSettings.FindOrCreate("LoadingBinaryJoystickConfigurationWarning");
			RequestDialogOpen?.Invoke(this, binaryLoadWarning);
		}


		/// <summary>
		///     Actually loads the binary settings.
		/// </summary>
		private void LoadBinarySettings()
		{
			var oldValueIsInvalidateEnabled = _parent.PortFacade.IsInvalidateEnabled;
			_parent.PortFacade.IsInvalidateEnabled = false;

			MessageBoxParams mbvm = null;

			try
			{
				for (var i = 1; i < 4; i++)
				{
					var joystickData = _viewToConfigMap[SelectedItem].Joysticks["Joystick " + i];

					var joystickConversation = _parent.Conversation;

					joystickConversation.Request(Command.SetActiveAxis, i);
					joystickConversation.Request(Command.SetAxisDeviceNumber,
												 int.Parse(joystickData.DeviceNumber, CultureInfo.InvariantCulture));
					joystickConversation.Request(Command.SetAxisVelocityProfile,
												 int.Parse(JoystickVM.ConvertToNumber(joystickData.ControlSpeed)));
					joystickConversation.Request(Command.SetAxisVelocityScale,
												 int.Parse(joystickData.MaxSpeed, CultureInfo.InvariantCulture));

					if (joystickData.Inverted)
					{
						joystickConversation.Request(Command.SetAxisInversion, -1);
					}

					else
					{
						joystickConversation.Request(Command.SetAxisInversion, 1);
					}
				}

				for (var i = 1; i < 9; i++)
				{
					//255 Error 0 corresponds to the Empty Command. We send that command
					//if the parsed key event token is null.
					var keyData = _viewToConfigMap[SelectedItem].BinaryKeys["Key " + i];

					_parent.Conversation.Request(Command.LoadEventInstruction, (i * 10) + 1);
					if (keyData.Press == null)
					{
						_parent.PortFacade.Port.Send(255, Command.Error, 0);
					}

					else
					{
						_parent.PortFacade.Port.Send(
							byte.Parse(keyData.Press.DeviceNumber, CultureInfo.InvariantCulture),
							(Command) byte.Parse(keyData.Press.Command, CultureInfo.InvariantCulture),
							int.Parse(keyData.Press.Data, CultureInfo.InvariantCulture));
					}

					_parent.Conversation.Request(Command.LoadEventInstruction, (i * 10) + 2);
					if (keyData.QuickRelease == null)
					{
						_parent.PortFacade.Port.Send(255, Command.Error, 0);
					}

					else
					{
						_parent.PortFacade.Port.Send(
							byte.Parse(keyData.QuickRelease.DeviceNumber, CultureInfo.InvariantCulture),
							(
							Command) byte.Parse(keyData.QuickRelease.Command, CultureInfo.InvariantCulture),
							int.Parse(keyData.QuickRelease.Data, CultureInfo.InvariantCulture));
					}

					_parent.Conversation.Request(Command.LoadEventInstruction, (i * 10) + 3);
					if (keyData.Hold2Seconds == null)
					{
						_parent.PortFacade.Port.Send(255, Command.Error, 0);
					}

					else
					{
						_parent.PortFacade.Port.Send(
							byte.Parse(keyData.Hold2Seconds.DeviceNumber, CultureInfo.InvariantCulture),
							(Command) byte.Parse(keyData.Hold2Seconds.Command, CultureInfo.InvariantCulture),
							int.Parse(keyData.Hold2Seconds.Data, CultureInfo.InvariantCulture));
					}

					_parent.Conversation.Request(Command.LoadEventInstruction, (i * 10) + 4);
					if (keyData.ReleaseAfterHold == null)
					{
						_parent.PortFacade.Port.Send(255, Command.Error, 0);
					}

					else
					{
						_parent.PortFacade.Port.Send(
							byte.Parse(keyData.ReleaseAfterHold.DeviceNumber, CultureInfo.InvariantCulture),
							(Command) byte.Parse(keyData.ReleaseAfterHold.Command,
												 CultureInfo.InvariantCulture),
							int.Parse(keyData.ReleaseAfterHold.Data, CultureInfo.InvariantCulture));
					}
				}
			}
			catch (Exception aException)
			{
				_log.Error("Exception when restoring binary joystick settings.", aException);
				mbvm = new MessageBoxParams
				{
					Buttons = MessageBoxButton.OK,
					Caption = "Error restoring configuration",
					Icon = MessageBoxImage.Exclamation,
					Message =
						"Errors occurred while loading the saved configuration and some joystick settings may not have been restored correctly. Please verify the joystick configuration before using it."
				};
			}
			finally
			{
				Thread.Sleep(250);

				//TODO this is a hacky way of circumventing the fact that
				//the above Port.Send commands have a response that arrives asynchronously.
				//If the response arrives after IsInvalidateEnabled is set to true,
				//Then the port closes and re-opens. This isn't too big of a deal,
				//but a eventually a better way would be nice.

				_parent.PortFacade.ClearDeferredInvalidation();
				_parent.PortFacade.IsInvalidateEnabled = oldValueIsInvalidateEnabled;

				var temp = _parent.Conversation;
				_parent.Conversation = null;
				_parent.Conversation = temp;
			}

			if (null != mbvm)
			{
				RequestDialogOpen?.Invoke(this, mbvm);
			}
		}


		/// <summary>
		///     Save the current settings that were most recently read from the joystick.
		///     IMPORTANT : Most recently read, not the current unsaved settings.
		///     Check JoystickConfigData for the data structure on howsettings are serialized.
		///     We are simply populating those defined data structures in <see cref="JoystickConfigData" /> and
		///     then creating a new JoystickConfigData object to be written to the .yaml file.
		/// </summary>
		private void SaveCurrentSettings()
		{
			var date = DateTime.Now.ToLongDateString();

			//Save the config as "Settings (time)" if they haven't entered anything in "Save As".
			var description = SaveConfigTitle ?? ("Settings " + DateTime.Now.ToShortTimeString());
			string protocol;
			SaveConfigTitle = null;

			///Data structure for keys :
			///Keys => Keys #s => Key Events -- List of Commands.
			///Data structure for joysticks:
			///Joysticks => Joystick Axis # (1,2,3) => Joystick Settings(maxspeed,resolution,etc) -- Number
			///
			var joysticks = new Dictionary<string, JoystickAxis>();
			var keys = new object();
			var currentSettings = new JoystickConfigData();

			if (_protocol == Protocol.ASCII)
			{
				keys = new Dictionary<string, KeyEventsDataASCII>();
				protocol = "ASCII";
				SaveCurrentASCII(keys as Dictionary<string, KeyEventsDataASCII>, joysticks);
			}

			else
			{
				keys = new Dictionary<string, KeyEventsDataBinary>();
				protocol = "Binary";
				SaveCurrentBinary(keys as Dictionary<string, KeyEventsDataBinary>, joysticks);
			}

			currentSettings.Date = date;
			currentSettings.Description = description;
			currentSettings.Protocol = protocol;

			//Only one of these key collections will be non-null
			currentSettings.ASCIIKeys = keys as Dictionary<string, KeyEventsDataASCII>;
			currentSettings.BinaryKeys = keys as Dictionary<string, KeyEventsDataBinary>;
			currentSettings.Joysticks = joysticks;

			var currentViewInfo = new JoystickConfigView
			{
				Date = date,
				Description = description,
				Protocol = protocol
			};

			_joystickConfigurations.Add(currentSettings);
			VisibleConfigurations.Add(currentViewInfo);
			_viewToConfigMap.Add(currentViewInfo, currentSettings);

			_eventLog.LogEvent("Settings saved");
		}


		/// <summary>
		///     Saves the current ASCII key and joystick settings and
		///     serializes them into objects to be written to our .yaml file.
		/// </summary>
		/// <param name="keyCollection">The format in which we will serialize our keys.</param>
		/// <param name="joystickCollection">The format in which we'll serialize our joysticks.</param>
		private void SaveCurrentASCII(Dictionary<string, KeyEventsDataASCII> keyCollection,
									  Dictionary<string, JoystickAxis> joystickCollection)
		{
			///Data structure for keys :
			///Keys => Keys #s => Key Events -- List of Commands.

			foreach (var key in _parent.Keys)
			{
				//I added these two values originally for convenience purposes.
				if ((key.ID == "0") || (key.ID == "9"))
				{
					continue;
				}

				var keyEventCommands = new KeyEventsDataASCII();
				foreach (var grid in key.KeyCommandGrids)
				{
					var commands = grid.FormatCommands(true);
					var alertsOn = grid.PreviousAlertsEnabled;

					switch (grid.ID)
					{
						//If we return an empty list, we will instead to set to null instead so that
						//our .yaml file is more pleasant to look at. We will not see the event instead of seeing
						//something like "Press (Event 1) : []".
						case 1:
							keyEventCommands.Press = commands.Count > 0 ? commands : null;
							keyEventCommands.AlertsOn[1] = alertsOn;
							break;
						case 2:
							keyEventCommands.QuickRelease = commands.Count > 0 ? commands : null;
							keyEventCommands.AlertsOn[2] = alertsOn;
							break;
						case 3:
							keyEventCommands.Hold2Seconds = commands.Count > 0 ? commands : null;
							keyEventCommands.AlertsOn[3] = alertsOn;
							break;
						case 4:
							keyEventCommands.ReleaseAfterHold = commands.Count > 0 ? commands : null;
							keyEventCommands.AlertsOn[4] = alertsOn;
							break;
						default:
							continue;
					}
				}

				keyCollection["Key " + key.ID] = keyEventCommands;
			}

			foreach (var joystick in _parent.Joysticks)
			{
				//There's a dummy joystick for indexing purposes.
				if (joystick.JoystickAxisNumber == "99")
				{
					continue;
				}

				var joystickAxisInformation = new JoystickAxis();
				joystickAxisInformation.DeviceNumber = joystick.OldInfo.TargetDeviceNumber;
				joystickAxisInformation.ControlSpeed = joystick.OldInfo.SpeedSelected;
				joystickAxisInformation.MaxSpeed = joystick.OldInfo.MaxSpeed;
				joystickAxisInformation.Inverted = joystick.OldInfo.IsInverted;
				joystickAxisInformation.Resolution = joystick.OldInfo.Resolution;

				//The joysticks have one of three axis targeting fields that are non-null.
				//That will tell us which axis type it is targeting.
				if (joystick.OldInfo.IsTargetingNormalAxis)
				{
					joystickAxisInformation.AxisNumber = joystick.OldInfo.TargetAxis;
				}

				else if (joystick.OldInfo.IsTargetingVirtualAxis)
				{
					joystickAxisInformation.VirtualIndex = joystick.OldInfo.VirtualIndex;
				}

				else if (joystick.OldInfo.IsTargetingLockstep)
				{
					joystickAxisInformation.LockstepIndex = joystick.OldInfo.LockstepIndex;
				}

				joystickCollection["Joystick " + joystick.JoystickAxisNumber] = joystickAxisInformation;
			}
		}


		/// <summary>
		///     The binary equivalent of
		///     <see cref="SaveCurrentASCII(Dictionary{string, KeyEventsDataASCII}, Dictionary{string, JoystickAxis})" />
		/// </summary>
		/// <param name="keyCollection"></param>
		/// <param name="joystickCollection"></param>
		private void SaveCurrentBinary(Dictionary<string, KeyEventsDataBinary> keyCollection,
									   Dictionary<string, JoystickAxis> joystickCollection)
		{
			///Data structure for keys :
			///Keys => Keys #s => Key Events => Key parameter (command name, data, device number) to value mapping.

			var maxKeys = 8;
			if (_parent.Conversation.Device.DeviceType.DeviceId == JoystickPluginVM.TJOY3_DEVICE_ID)
			{
				maxKeys = JoystickPluginVM.NUMBER_OF_TJOY_KEYS;
			}

			for (var i = 1; i < (maxKeys + 1); i++)
			{
				var key = _parent.Keys[i];

				//I added these two values originally for convenience purposes.

				var keyEventCommands = new KeyEventsDataBinary();

				foreach (var grid in key.KeyCommandGrids)
				{
					BinaryKeyData binaryCommandInfo;

					if (grid.ID == 0)
					{
						continue;
					}

					binaryCommandInfo = new BinaryKeyData
					{
						DeviceNumber = grid.BinaryInfo.TargetDeviceNumber,
						Command = grid.BinaryInfo.BinaryCommand,
						CommandName = grid.SelectedCommandBinary?.Name,
						Data = grid.BinaryInfo.BinaryData
					};

					if (grid.IsEmptyCommand)
					{
						binaryCommandInfo = null;
					}

					switch (grid.ID)
					{
						case 1:
							keyEventCommands.Press = binaryCommandInfo;
							break;
						case 2:
							keyEventCommands.QuickRelease = binaryCommandInfo;
							break;
						case 3:
							keyEventCommands.Hold2Seconds = binaryCommandInfo;
							break;
						case 4:
							keyEventCommands.ReleaseAfterHold = binaryCommandInfo;
							break;
						default:
							continue;
					}
				}

				keyCollection["Key " + key.ID] = keyEventCommands;
			}

			if (maxKeys == 5)
			{
				keyCollection["Key 6"] = new KeyEventsDataBinary();
				keyCollection["Key 7"] = new KeyEventsDataBinary();
				keyCollection["Key 8"] = new KeyEventsDataBinary();
			}

			foreach (var joystick in _parent.Joysticks)
			{
				//Dummy joystick for indexing purposes.
				if (joystick.JoystickAxisNumber == "99")
				{
					continue;
				}

				var joystickAxisInformation = new JoystickAxis();
				joystickAxisInformation.DeviceNumber = joystick.OldInfo.TargetDeviceNumber;
				joystickAxisInformation.ControlSpeed = joystick.OldInfo.SpeedSelected;
				joystickAxisInformation.MaxSpeed = joystick.OldInfo.MaxSpeed;
				joystickAxisInformation.Inverted = joystick.OldInfo.IsInverted;

				joystickCollection["Joystick " + joystick.JoystickAxisNumber] = joystickAxisInformation;
			}
		}

		#endregion

		#region --IUserDialogViewModel Implementation--

		public bool IsModal => true;

		public event EventHandler BringToFront;

		public event EventHandler DialogClosing;

		public event RequestDialogChange RequestDialogOpen;
		#pragma warning disable CS0067 // Event is never used.
		public event RequestDialogChange RequestDialogClose;
		#pragma warning restore CS0067


		public void RequestBringToFront() => BringToFront?.Invoke(this, null);


		/// <summary>
		///     Any exception that was unhandled by the dialog itself.
		/// </summary>
		public Exception Exception { get; set; }


		/// <summary>
		///     Closes the window, and saves all of the information to the JoystickConfiguration.yaml file in AppData.
		/// </summary>
		public void RequestClose()
		{
			var serializer = new Serializer();

			if (File.Exists(CONFIG_FILE))
			{
				File.Delete(CONFIG_FILE);
			}

			//Save the settings to the config file.
			var toWriteToYaml = new StringBuilder();
			var configWriter = new StringWriter(toWriteToYaml);
			serializer.Serialize(configWriter, _joystickConfigurations);

			File.WriteAllText(CONFIG_FILE, configWriter.ToString());
			DialogClosing?.Invoke(this, null);
		}


		private void ExportSettings()
		{
			var exportable = _joystickConfigurations;
			if (null != SelectedItem)
			{
				exportable = new List<JoystickConfigData>();
				exportable.Add(_viewToConfigMap[SelectedItem]);
			}

			if (exportable.Count > 0)
			{
				var dlg = new FileBrowserDialogParams
				{
					AddExtension = true,
					CheckFileExists = false,
					DefaultExtension = ".yaml",
					Filter = "YAML files (*.yaml)|*.yaml|All files (*.*)|*.*",
					InitialDirectory = Settings.Default.LastJoystickExportFolder,
					Title = "Enter Filename to Save",
					Action = FileBrowserDialogParams.FileActionType.Save
				};

				RequestDialogOpen?.Invoke(this, dlg);
				if (dlg.DialogResult.HasValue && dlg.DialogResult.Value)
				{
					var path = dlg.Filename;
					var serializer = new Serializer();
					var toWriteToYaml = new StringBuilder();
					var configWriter = new StringWriter(toWriteToYaml);
					serializer.Serialize(configWriter, exportable);

					File.WriteAllText(path, configWriter.ToString());
					Settings.Default.LastJoystickExportFolder = Path.GetDirectoryName(path);

					_eventLog.LogEvent("Settings exported to file");
				}
			}
			else
			{
				var mbvm = new MessageBoxParams
				{
					Buttons = MessageBoxButton.OK,
					Caption = "Nothing to do.",
					Icon = MessageBoxImage.Asterisk,
					Message = "No configurations selected to export."
				};

				RequestDialogOpen?.Invoke(this, mbvm);
			}
		}


		private void ImportSettings()
		{
			var dlg = new FileBrowserDialogParams
			{
				CheckFileExists = true,
				DefaultExtension = ".yaml",
				Filter = "YAML files (*.yaml)|*.yaml|All files (*.*)|*.*",
				InitialDirectory = Settings.Default.LastJoystickExportFolder,
				Title = "Select a File to Load"
			};

			RequestDialogOpen?.Invoke(this, dlg);
			if (dlg.DialogResult.HasValue && dlg.DialogResult.Value)
			{
				var path = dlg.Filename;

				try
				{
					StringReader configInput;
					List<JoystickConfigData> loaded;
					configInput = new StringReader(File.ReadAllText(path));
					var deserializer = new Deserializer();
					loaded = deserializer.Deserialize<List<JoystickConfigData>>(configInput);

					// Add loaded configurations to list and add VMs for them.
					foreach (var configuration in loaded)
					{
						if (configuration.SerializationVersion == 1)
						{
							_joystickConfigurations.Add(configuration);
							var configInfo = new JoystickConfigView();

							configInfo.Description = configuration.Description;
							configInfo.Date = configuration.Date;
							configInfo.Protocol = configuration.Protocol;

							_viewToConfigMap.Add(configInfo, configuration);
							VisibleConfigurations.Add(configInfo);
						}
						else
						{
							throw new IOException(
								$"Serialization format {configuration.SerializationVersion} not recognized when loading joystick configurations.");
						}
					}

					_eventLog.LogEvent("Settings imported from file");
				}
				catch (Exception aException)
				{
					_log.Error("Error loading joystick configurations from " + path, aException);

					var mbvm = new MessageBoxParams
					{
						Buttons = MessageBoxButton.OK,
						Caption = "Error Loading File",
						Icon = MessageBoxImage.Error,
						Message =
							"There was an error while loading the selected file. See the Application Log for details."
					};

					RequestDialogOpen?.Invoke(this, mbvm);
				}
			}
		}

		#endregion

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Joystick tab");

		private bool _deleteAllowed;
		private bool _correctProtocol;
		private string _displayConfigInformation;
		private string _saveConfigTitle;
		private readonly JoystickPluginVM _parent;
		private JoystickConfigView _selectedItem;

		private readonly Protocol _protocol;

		//This is a collection of viewable joystick configurations with just the relevant information for viewing.
		private ObservableCollection<JoystickConfigView> _visibleConfigurations =
			new ObservableCollection<JoystickConfigView>();

		//This maps from the selected JoystickConfigView to the JoystickConfigData it represents. 
		private readonly Dictionary<JoystickConfigView, JoystickConfigData> _viewToConfigMap =
			new Dictionary<JoystickConfigView, JoystickConfigData>();

		//This is the container for the JoystickConfigData, each representing one joystick set-up.
		private List<JoystickConfigData> _joystickConfigurations;

		internal static string CONFIG_FILE = Path.Combine(
			Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
			@"Zaber Technologies",
			@"Zaber Console",
			@"JoystickConfigurations.yaml");
	}
}

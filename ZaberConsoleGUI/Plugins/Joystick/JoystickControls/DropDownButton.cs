﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;

namespace ZaberConsole.Plugins.Joystick.JoystickControls
{
	/// <summary>
	///     A custom control for the Toggle Button above the X-JOY Panel.
	///     The code for this class was adapted from
	///     http://stackoverflow.com/questions/8958946/how-to-open-a-popup-menu-when-a-button-is-clicked/20710436
	///     and is not covered by the same license as regular Zaber code. The applicable
	///     license is CC BY-SA 3.0: http://creativecommons.org/licenses/by-sa/3.0/
	/// </summary>
	public class DropDownButton : ToggleButton
	{
		#region -- Public data --

		// *** Dependency Properties ***


		public static readonly DependencyProperty DropDownProperty =
			DependencyProperty.Register("DropDown",
										typeof(ContextMenu),
										typeof(DropDownButton),
										new UIPropertyMetadata(null));

		#endregion

		#region -- Public Methods & Properties --

		// *** Constructors *** 


		public DropDownButton()
		{
			// Bind the ToogleButton.IsChecked property to the drop-down's IsOpen property 

			var binding = new Binding("DropDown.IsOpen");
			binding.Source = this;
			SetBinding(IsCheckedProperty, binding);
		}


		// *** Properties *** 

		public ContextMenu DropDown
		{
			get => (ContextMenu) GetValue(DropDownProperty);
			set => SetValue(DropDownProperty, value);
		}

		#endregion

		#region -- Non-Public Methods --

		// *** Overridden Methods *** 


		protected override void OnClick()
		{
			if (DropDown != null)
			{
				// If there is a drop-down assigned to this button, then position and display it 

				DropDown.PlacementTarget = this;
				DropDown.Placement = PlacementMode.Bottom;

				DropDown.IsOpen = true;
			}
		}

		#endregion
	}
}

﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ZaberConsole.Plugins.Joystick.JoystickControls
{
	/// <summary>
	///     This class is adapted from the article
	///     by Tomer Shamam:
	///     http://www.codeproject.com/Articles/28098/A-WPF-Pie-Chart-with-Data-Binding-Support
	///     And is distributed under different terms than Zaber-originated code.
	///     Please see the license.txt file included at the root folder of this library.
	/// </summary>
	internal class ButtonWedge : Shape
	{
		#region -- Public Methods & Properties --

		public static Point ComputeCartesianCoordinate(double angle, double radius)
		{
			// convert to radians
			var angleRad = (Math.PI / 180.0) * (angle - 90);

			var x = radius * Math.Cos(angleRad);
			var y = radius * Math.Sin(angleRad);

			return new Point(x, y);
		}


		protected override Geometry DefiningGeometry
		{
			get
			{
				// Create a StreamGeometry for describing the shape
				var geometry = new StreamGeometry();
				geometry.FillRule = FillRule.EvenOdd;

				using (var context = geometry.Open())
				{
					DrawGeometry(context);
				}

				// Freeze the geometry for performance benefits
				geometry.Freeze();

				return geometry;
			}
		}

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Draws the pie piece
		/// </summary>
		private void DrawGeometry(StreamGeometryContext context)
		{
			var startPoint = new Point(CentreX, CentreY);

			var innerArcStartPoint = ComputeCartesianCoordinate(RotationAngle, InnerRadius);
			innerArcStartPoint.Offset(CentreX, CentreY);

			var innerArcEndPoint = ComputeCartesianCoordinate(RotationAngle + WedgeAngle, InnerRadius);
			innerArcEndPoint.Offset(CentreX, CentreY);

			var outerArcStartPoint = ComputeCartesianCoordinate(RotationAngle, Radius);
			outerArcStartPoint.Offset(CentreX, CentreY);

			var outerArcEndPoint = ComputeCartesianCoordinate(RotationAngle + WedgeAngle, Radius);
			outerArcEndPoint.Offset(CentreX, CentreY);

			var largeArc = WedgeAngle > 180.0;

			if (PushOut > 0)
			{
				var offset = ComputeCartesianCoordinate(RotationAngle + (WedgeAngle / 2), PushOut);
				innerArcStartPoint.Offset(offset.X, offset.Y);
				innerArcEndPoint.Offset(offset.X, offset.Y);
				outerArcStartPoint.Offset(offset.X, offset.Y);
				outerArcEndPoint.Offset(offset.X, offset.Y);
			}

			var outerArcSize = new Size(Radius, Radius);
			var innerArcSize = new Size(InnerRadius, InnerRadius);

			context.BeginFigure(innerArcStartPoint, true, true);
			context.LineTo(outerArcStartPoint, true, true);
			context.ArcTo(outerArcEndPoint, outerArcSize, 0, largeArc, SweepDirection.Clockwise, true, true);
			context.LineTo(innerArcEndPoint, true, true);
			context.ArcTo(innerArcStartPoint, innerArcSize, 0, largeArc, SweepDirection.Counterclockwise, true, true);
		}

		#endregion

		#region dependency properties

		public static readonly DependencyProperty RadiusProperty =
			DependencyProperty.Register("RadiusProperty",
										typeof(double),
										typeof(ButtonWedge),
										new FrameworkPropertyMetadata(0.0,
																	  FrameworkPropertyMetadataOptions.AffectsRender
																  | FrameworkPropertyMetadataOptions.AffectsMeasure));

		/// <summary>
		///     The radius of this pie piece
		/// </summary>
		public double Radius
		{
			get => (double) GetValue(RadiusProperty);
			set => SetValue(RadiusProperty, value);
		}

		public static readonly DependencyProperty PushOutProperty =
			DependencyProperty.Register("PushOutProperty",
										typeof(double),
										typeof(ButtonWedge),
										new FrameworkPropertyMetadata(0.0,
																	  FrameworkPropertyMetadataOptions.AffectsRender
																  | FrameworkPropertyMetadataOptions.AffectsMeasure));

		/// <summary>
		///     The distance to 'push' this pie piece out from the centre.
		/// </summary>
		public double PushOut
		{
			get => (double) GetValue(PushOutProperty);
			set => SetValue(PushOutProperty, value);
		}

		public static readonly DependencyProperty InnerRadiusProperty =
			DependencyProperty.Register("InnerRadiusProperty",
										typeof(double),
										typeof(ButtonWedge),
										new FrameworkPropertyMetadata(0.0,
																	  FrameworkPropertyMetadataOptions.AffectsRender
																  | FrameworkPropertyMetadataOptions.AffectsMeasure));

		/// <summary>
		///     The inner radius of this pie piece
		/// </summary>
		public double InnerRadius
		{
			get => (double) GetValue(InnerRadiusProperty);
			set => SetValue(InnerRadiusProperty, value);
		}

		public static readonly DependencyProperty WedgeAngleProperty =
			DependencyProperty.Register("WedgeAngleProperty",
										typeof(double),
										typeof(ButtonWedge),
										new FrameworkPropertyMetadata(0.0,
																	  FrameworkPropertyMetadataOptions.AffectsRender
																  | FrameworkPropertyMetadataOptions.AffectsMeasure));

		/// <summary>
		///     The wedge angle of this pie piece in degrees
		/// </summary>
		public double WedgeAngle
		{
			get => (double) GetValue(WedgeAngleProperty);
			set
			{
				SetValue(WedgeAngleProperty, value);
				Percentage = value / 360.0;
			}
		}

		public static readonly DependencyProperty RotationAngleProperty =
			DependencyProperty.Register("RotationAngleProperty",
										typeof(double),
										typeof(ButtonWedge),
										new FrameworkPropertyMetadata(0.0,
																	  FrameworkPropertyMetadataOptions.AffectsRender
																  | FrameworkPropertyMetadataOptions.AffectsMeasure));

		/// <summary>
		///     The rotation, in degrees, from the Y axis vector of this pie piece.
		/// </summary>
		public double RotationAngle
		{
			get => (double) GetValue(RotationAngleProperty);
			set => SetValue(RotationAngleProperty, value);
		}

		public static readonly DependencyProperty CentreXProperty =
			DependencyProperty.Register("CentreXProperty",
										typeof(double),
										typeof(ButtonWedge),
										new FrameworkPropertyMetadata(0.0,
																	  FrameworkPropertyMetadataOptions.AffectsRender
																  | FrameworkPropertyMetadataOptions.AffectsMeasure));

		/// <summary>
		///     The X coordinate of centre of the circle from which this pie piece is cut.
		/// </summary>
		public double CentreX
		{
			get => (double) GetValue(CentreXProperty);
			set => SetValue(CentreXProperty, value);
		}

		public static readonly DependencyProperty CentreYProperty =
			DependencyProperty.Register("CentreYProperty",
										typeof(double),
										typeof(ButtonWedge),
										new FrameworkPropertyMetadata(0.0,
																	  FrameworkPropertyMetadataOptions.AffectsRender
																  | FrameworkPropertyMetadataOptions.AffectsMeasure));

		/// <summary>
		///     The Y coordinate of centre of the circle from which this pie piece is cut.
		/// </summary>
		public double CentreY
		{
			get => (double) GetValue(CentreYProperty);
			set => SetValue(CentreYProperty, value);
		}

		public static readonly DependencyProperty PercentageProperty =
			DependencyProperty.Register("PercentageProperty",
										typeof(double),
										typeof(ButtonWedge),
										new FrameworkPropertyMetadata(0.0));

		/// <summary>
		///     The percentage of a full pie that this piece occupies.
		/// </summary>
		public double Percentage
		{
			get => (double) GetValue(PercentageProperty);
			private set => SetValue(PercentageProperty, value);
		}

		public static readonly DependencyProperty PieceValueProperty =
			DependencyProperty.Register("PieceValueProperty",
										typeof(double),
										typeof(ButtonWedge),
										new FrameworkPropertyMetadata(0.0));

		/// <summary>
		///     The value that this pie piece represents.
		/// </summary>
		public double PieceValue
		{
			get => (double) GetValue(PieceValueProperty);
			set => SetValue(PieceValueProperty, value);
		}

		#endregion
	}
}

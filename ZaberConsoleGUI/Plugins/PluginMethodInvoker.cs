﻿using System.Reflection;
using Zaber.PlugIns;

namespace ZaberConsole.Plugins
{
	internal class PluginMethodInvoker
	{
		#region -- Public Methods & Properties --

		public PluginMethodInvoker(PlugInMethodAttribute.EventType aEventType, object aPlugin, MethodInfo aMethodInfo)
		{
			_eventType = aEventType;
			Plugin = aPlugin;
			_method = aMethodInfo;
		}


		public void Invoke(PlugInMethodAttribute.EventType aEventType)
		{
			if (aEventType == _eventType)
			{
				_method.Invoke(Plugin, null);
			}
		}


		public object Plugin { get; }

		#endregion

		#region -- Data --

		private readonly PlugInMethodAttribute.EventType _eventType;
		private readonly MethodInfo _method;

		#endregion
	}
}

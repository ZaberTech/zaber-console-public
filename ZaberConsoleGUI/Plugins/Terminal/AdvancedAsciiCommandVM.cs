﻿using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Windows.Input;
using Zaber;
using Zaber.Telemetry;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.Terminal
{
	public class AdvancedAsciiCommandVM : ObservableObject
	{
		#region -- Public Methods & Properties --

		public AdvancedAsciiCommandVM()
		{
			_dispatcher = DispatcherHelper.Dispatcher;
		}


		public ZaberPortFacade PortFacade { get; set; }


		public Conversation Conversation
		{
			get => _conversation;
			set
			{
				_conversation = value;
				var newList = new ObservableCollection<string>(
					AsciiCommandHelper.ListAsciiCommandCompletions(
						PortFacade?.GetConversation(0)?.Device?.DeviceType?.Commands));
				AutocompleteList = newList;
			}
		}

		public string Command
		{
			get => _command;
			set => Set(ref _command, value, nameof(Command));
		}


		public bool CommandTextHasFocus
		{
			get => _commandTextHasFocus;
			set => Set(ref _commandTextHasFocus, value, nameof(CommandTextHasFocus));
		}


		/// <summary>
		///     List of possible completions for a command being typed in.
		/// </summary>
		public ObservableCollection<string> AutocompleteList
		{
			get => _autocompleteList;
			set => Set(ref _autocompleteList, value, nameof(AutocompleteList));
		}


		/// <summary>
		///     Send command from text field to device.
		/// </summary>
		public ICommand SendTextCommand => new RelayCommand(_ => SendCommand());


		public ICommand FocusCommandTextCommand => new RelayCommand(_ => CommandTextHasFocus = true);

		#endregion

		#region -- Non-Public Methods --

		private void SendCommand()
		{
			_eventLog.LogEvent("Send ASCII commands", Command);
			var commands = Regex.Split(Command, @"\s*;\s*");
			foreach (var command in commands)
			{
				PortFacade.Port.Send(command);
			}
		}

		#endregion

		#region -- Data --

		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Terminal tab");

		private IDispatcher _dispatcher;
		private Conversation _conversation;

		private string _command = string.Empty;
		private bool _commandTextHasFocus;

		private ObservableCollection<string> _autocompleteList;

		#endregion
	}
}

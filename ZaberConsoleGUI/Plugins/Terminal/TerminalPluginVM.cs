using System;
using System.Collections.ObjectModel;
using Zaber;
using Zaber.PlugIns;
using ZaberConsole.Properties;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Logging;

namespace ZaberConsole.Plugins.Terminal
{
	[PlugIn(Name = "Terminal", AlwaysActive = true, Description = "Device communication terminal.")]
	public class TerminalPluginVM : LogControlVM
	{
		#region -- Plugin interface --

		/// <summary>
		///     Reference to the port - used to determine what controls to display and
		///     to forward user commands.
		/// </summary>
		[PlugInProperty]
		public ZaberPortFacade PortFacade
		{
			get => _portFacade;
			set
			{
				if (null != _portFacade)
				{
					_portFacade.Closed -= Port_OpenStateChanged;
					_portFacade.Opened -= Port_OpenStateChanged;
				}

				_portFacade = value;

				if (null != _portFacade)
				{
					_portFacade.Closed += Port_OpenStateChanged;
					_portFacade.Opened += Port_OpenStateChanged;
				}

				Populate();
			}
		}


		/// <summary>
		///     Reference to the Plugin Manager. Used to detect user settings reset events.
		/// </summary>
		[PlugInProperty]
		public IPlugInManager PluginManager
		{
			get => _pluginManager;
			set
			{
				if (null != _pluginManager)
				{
					_pluginManager.SettingsResetEvent -= OnSettingsReset;
				}

				_pluginManager = value;

				if (null != _pluginManager)
				{
					_pluginManager.SettingsResetEvent += OnSettingsReset;
				}
			}
		}


		/// <summary>
		///     Reference to the currently selected device conversation; provided by the Plugin Manager.
		/// </summary>
		[PlugInProperty]
		public Conversation Conversation
		{
			get => _conversation;
			set
			{
				_conversation = value;
				if (null != _asciiVM)
				{
					_asciiVM.Conversation = value;
				}
			}
		}

		#endregion

		#region -- View-Bound Properties --

		/// <summary>
		///     Indicates that the view controls should be enabled.
		/// </summary>
		public bool Enabled
		{
			get => _enabled;
			set => Set(ref _enabled, value, nameof(Enabled));
		}


		/// <summary>
		///     Whether or not the controls to send commands are expanded.
		/// </summary>
		public bool ControlsExpanded
		{
			get => Settings.Default.TerminalControlsExpanded;
			set
			{
				Settings.Default.TerminalControlsExpanded = value;
				OnPropertyChanged(nameof(ControlsExpanded));
			}
		}


		/// <summary>
		///     ViewModels for the protocol-specific command controls to display.
		/// </summary>
		public ObservableCollection<ObservableObject> Controls
		{
			get => _controls;
			set => Set(ref _controls, value, nameof(Controls));
		}

		#endregion

		#region -- Event handlers --

		private void Port_OpenStateChanged(object sender, EventArgs e) => Populate();


		private void Populate() => _dispatcher.BeginInvoke(() =>
		{
			Controls.Clear();
			_asciiVM = null;

			if ((null != PortFacade) && PortFacade.IsOpen)
			{
				if (PortFacade.Port.IsAsciiMode)
				{
					_asciiVM = new AdvancedAsciiCommandVM
					{
						PortFacade = PortFacade,
						Conversation = Conversation
					};

					Controls.Add(_asciiVM);
				}
				else
				{
					Controls.Add(new AdvancedBinaryCommandVM { PortFacade = PortFacade });
					Controls.Add(new AdvancedBinaryRegisterVM { PortFacade = PortFacade });
				}
			}
		});


		private void OnSettingsReset(object aSender, EventArgs aArgs)
		{
			var settings = Settings.Default;
			settings.TerminalControlsExpanded = true;
			OnPropertyChanged(nameof(ControlsExpanded));
		}

		#endregion

		#region -- Fields --

		private readonly IDispatcher _dispatcher = DispatcherHelper.Dispatcher;
		private IPlugInManager _pluginManager;
		private ZaberPortFacade _portFacade;
		private Conversation _conversation;
		private AdvancedAsciiCommandVM _asciiVM;
		private ObservableCollection<ObservableObject> _controls = new ObservableCollection<ObservableObject>();
		private bool _enabled = true;

		#endregion
	}
}

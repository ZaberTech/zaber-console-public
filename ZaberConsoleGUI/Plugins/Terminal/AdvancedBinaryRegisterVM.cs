﻿using System;
using System.Windows.Input;
using System.Windows.Threading;
using Zaber;
using Zaber.Telemetry;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.Terminal
{
	public class AdvancedBinaryRegisterVM : ObservableObject
	{
		#region -- Public Methods & Properties --

		public AdvancedBinaryRegisterVM()
		{
			_dispatcher = Dispatcher.CurrentDispatcher;
		}


		public ZaberPortFacade PortFacade
		{
			get => _portFacade;
			set
			{
				if (null != _portFacade)
				{
					var allDevices = _portFacade.GetDevice(0);
					allDevices.MessageReceived -= OnAllDevicesMessageReceived;
					_portFacade.Port.DataPacketReceived -= OnPortDataPacketReceived;
				}

				_portFacade = value;

				if (null != _portFacade)
				{
					var allDevices = _portFacade.GetDevice(0);
					allDevices.MessageReceived += OnAllDevicesMessageReceived;
					_portFacade.Port.DataPacketReceived += OnPortDataPacketReceived;
				}
			}
		}

		public byte RegisterDeviceNumber
		{
			get => _registerDeviceNumber;
			set => Set(ref _registerDeviceNumber, value, nameof(RegisterDeviceNumber));
		}


		public int RegisterNumber
		{
			get => _registerNumber;
			set => Set(ref _registerNumber, value, nameof(RegisterNumber));
		}


		public int RegisterData
		{
			get => _registerData;
			set => Set(ref _registerData, value, nameof(RegisterData));
		}


		public bool RegisterDeviceNumberHasFocus
		{
			get => _registerDeviceNumberHasFocus;
			set => Set(ref _registerDeviceNumberHasFocus, value, nameof(RegisterDeviceNumberHasFocus));
		}


		public bool RegisterNumberHasFocus
		{
			get => _registerNumberHasFocus;
			set => Set(ref _registerNumberHasFocus, value, nameof(RegisterNumberHasFocus));
		}


		public bool RegisterValueHasFocus
		{
			get => rRegisterValueHasFocus;
			set => Set(ref rRegisterValueHasFocus, value, nameof(RegisterValueHasFocus));
		}


		public ICommand ReadRegisterCommand => new RelayCommand(_ => OnReadRegister());


		public ICommand WriteRegisterCommand => new RelayCommand(_ => OnWriteRegister());


		public ICommand FocusDeviceNumberCommand => new RelayCommand(_ => RegisterDeviceNumberHasFocus = true);


		public ICommand FocusRegisterNumberCommand => new RelayCommand(_ => RegisterNumberHasFocus = true);


		public ICommand FocusRegisterValueCommand => new RelayCommand(_ => RegisterValueHasFocus = true);

		#endregion

		#region -- Non-Public Methods --

		private void OnAllDevicesMessageReceived(object aSender, DeviceMessageEventArgs aArgs)
		{
			if (Command.ReadRegister == aArgs.DeviceMessage.Command)
			{
				_dispatcher.BeginInvoke(new Action(() =>
				{
					if (_isReadingRegister)
					{
						RegisterData = (int) aArgs.DeviceMessage.NumericData;
						_isReadingRegister = false;
					}
					else
					{
						// Multiple answers, clear the field and make the user
						// read the log.
						RegisterData = 0;
					}
				}));
			}
		}


		private void OnPortDataPacketReceived(object aSender, DataPacketEventArgs aArgs)
		{
			if (PortFacade.ContainsDevice(aArgs.DataPacket.DeviceNumber))
			{
				// This will get handled by the allDevices event handler.
				return;
			}

			if (Command.ReadRegister == aArgs.DataPacket.Command)
			{
				var messageArgs = new DeviceMessageEventArgs(new DeviceMessage(aArgs.DataPacket));
				OnAllDevicesMessageReceived(aSender, messageArgs);
			}
		}


		private void OnReadRegister()
		{
			_eventLog.LogEvent("Get Binary register", RegisterNumber.ToString());
			_isReadingRegister = true;
			_portFacade.Port.Send(RegisterDeviceNumber, Command.ReadRegister, RegisterNumber);
		}


		private void OnWriteRegister()
		{
			_eventLog.LogEvent("Set Binary register", $"{RegisterNumber} = {RegisterData}");
			_portFacade.Port.Send(RegisterDeviceNumber, Command.SetActiveRegister, RegisterNumber);
			_portFacade.Port.Send(RegisterDeviceNumber, Command.WriteRegister, RegisterData);
		}

		#endregion

		#region -- Data --

		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Terminal tab");
		private readonly Dispatcher _dispatcher;

		private ZaberPortFacade _portFacade;
		private bool _isReadingRegister;
		private byte _registerDeviceNumber;
		private int _registerNumber;
		private int _registerData;
		private bool _registerDeviceNumberHasFocus;
		private bool _registerNumberHasFocus;
		private bool rRegisterValueHasFocus;

		#endregion
	}
}

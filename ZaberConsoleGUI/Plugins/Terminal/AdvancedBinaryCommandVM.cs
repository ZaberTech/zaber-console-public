﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Zaber;
using Zaber.Telemetry;
using ZaberConsole.Plugins.CommandList;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.Terminal
{
	public class AdvancedBinaryCommandVM : ObservableObject
	{
		#region -- Public Methods & Properties --

		public ZaberPortFacade PortFacade
		{
			get => _portFacade;
			set
			{
				_portFacade = value;
				InitializeAdvancedCommandList();
			}
		}

		public ObservableCollection<CommandInfoVM> AdvancedCommands
		{
			get => _advancedCommands;
			set => Set(ref _advancedCommands, value, nameof(AdvancedCommands));
		}


		public CommandInfoVM SelectedAdvancedCommand
		{
			get => _selectedAdvancedCommand;
			set
			{
				Set(ref _selectedAdvancedCommand, value, nameof(SelectedAdvancedCommand));

				if (null != _selectedAdvancedCommand)
				{
					if (AdvancedCommandNumberHasFocus)
					{
						return; // Don't reset the field while it's being edited.
					}

					if (_selectedAdvancedCommand.CommandName == "Other")
					{
						// If the user selects "Other", clear the number and move the focus.
						_advancedCommandNumber = 0;
						AdvancedCommandNumberHasFocus = true;
					}
					else
					{
						_advancedCommandNumber = (byte) _selectedAdvancedCommand.CommandInfo.Command;
					}

					OnPropertyChanged(nameof(AdvancedCommandNumber)); // Avoid setter recursion.
				}
			}
		}


		public byte AdvancedDeviceNumber
		{
			get => _advancedDeviceNumber;
			set => Set(ref _advancedDeviceNumber, value, nameof(AdvancedDeviceNumber));
		}


		public byte AdvancedCommandNumber
		{
			get => _advancedCommandNumber;
			set
			{
				Set(ref _advancedCommandNumber, value, nameof(AdvancedCommandNumber));

				foreach (var vm in _advancedCommands)
				{
					if (value == (byte) vm.CommandInfo.Command)
					{
						SelectedAdvancedCommand = vm;
						break;
					}
				}
			}
		}


		public bool AdvancedCommandNumberHasFocus
		{
			get => _advancedCommandNumberHasFocus;
			set => Set(ref _advancedCommandNumberHasFocus, value, nameof(AdvancedCommandNumberHasFocus));
		}


		public bool DeviceNumberHasFocus
		{
			get => _deviceNumberHasFocus;
			set => Set(ref _deviceNumberHasFocus, value, nameof(DeviceNumberHasFocus));
		}


		public bool CommandListHasFocus
		{
			get => _commandListHasFocus;
			set => Set(ref _commandListHasFocus, value, nameof(CommandListHasFocus));
		}


		public bool CommandDataHasFocus
		{
			get => _commandDataHasFocus;
			set => Set(ref _commandDataHasFocus, value, nameof(CommandDataHasFocus));
		}


		public int AdvancedData
		{
			get => _advancedData;
			set => Set(ref _advancedData, value, nameof(AdvancedData));
		}


		public ICommand SendButtonCommand => new RelayCommand(_ => OnSendClicked());


		public ICommand EnterPressedCommand => new RelayCommand(_ => OnSendClicked());


		public ICommand FocusDeviceNumberCommand => new RelayCommand(_ => DeviceNumberHasFocus = true);


		public ICommand FocusCommandCommand => new RelayCommand(_ => CommandListHasFocus = true);


		public ICommand FocusCommandNumberCommand => new RelayCommand(_ => AdvancedCommandNumberHasFocus = true);


		public ICommand FocusCommandDataCommand => new RelayCommand(_ => CommandDataHasFocus = true);

		#endregion

		#region -- Non-Public Methods --

		private void OnSendClicked()
		{
			_eventLog.LogEvent("Send Binary command", $"{(Command) AdvancedCommandNumber} {AdvancedData}");
			_portFacade.Port.Send(AdvancedDeviceNumber, (Command) AdvancedCommandNumber, AdvancedData);
		}


		private void InitializeAdvancedCommandList()
		{
			if (_portFacade is null || (_advancedCommands.Count > 0))
			{
				return;
			}

			var commands = new List<CommandInfoVM>();
			foreach (var commandInfo in _portFacade.DefaultDeviceType.Commands)
			{
				if (commandInfo.TextCommand == null)
				{
					commands.Add(new CommandInfoVM(commandInfo, null));
				}
			}

			commands.Sort((commandA, commandB) => string.CompareOrdinal(commandA.CommandName, commandB.CommandName));

			var otherCommand = new CommandInfo { Name = "Other" };

			var otherCommandView = new CommandInfoVM(otherCommand, null);
			commands.Add(otherCommandView);

			_advancedCommands.Clear();
			foreach (var vm in commands)
			{
				_advancedCommands.Add(vm);
			}
		}

		#endregion

		#region -- Data --

		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Terminal tab");

		private ZaberPortFacade _portFacade;

		private ObservableCollection<CommandInfoVM> _advancedCommands = new ObservableCollection<CommandInfoVM>();
		private CommandInfoVM _selectedAdvancedCommand;
		private byte _advancedDeviceNumber;
		private byte _advancedCommandNumber;
		private int _advancedData;
		private bool _advancedCommandNumberHasFocus;
		private bool _deviceNumberHasFocus;
		private bool _commandListHasFocus;
		private bool _commandDataHasFocus;

		#endregion
	}
}

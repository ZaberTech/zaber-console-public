﻿using System.Diagnostics;
using System.Windows.Input;
using Zaber.PlugIns;
using ZaberConsole.Properties;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.Help
{
	[PlugIn(Name = "Help", AlwaysActive = true, Description = "General help for Zaber Console.")]
	public class HelpTabVM : PluginTabVM
	{
		#region -- Public Methods & Properties --

		public string HelpText => Resources.HelpTabHelpText;

		public ICommand ViewOnlineCommand => new RelayCommand(_ => LaunchOnlineManual());

		#endregion

		#region -- Non-Public Methods --

		private void LaunchOnlineManual()
			=> Process.Start("https://www.zaber.com/wiki/Software/Zaber_Console#Using_Zaber_Console");

		#endregion
	}
}

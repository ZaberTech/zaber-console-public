﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Zaber;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.CommandList
{
	/// <summary>
	///     Represents a single node in an ASCII command. This could be a
	///     token or a parameter.
	///     <seealso cref="ASCIICommandInfoVM" />
	/// </summary>
	public class ASCIICommandNodeVM : ObservableObject
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Constructs a VM from an ASCIICommandNode object.
		/// </summary>
		/// <param name="node">
		///     The ASCII command node to get information
		///     from.
		/// </param>
		public ASCIICommandNodeVM(ASCIICommandNode node)
		{
			NodeInfo = node;

			if (null == NodeInfo.ParamType)
			{
				Content = NodeInfo.NodeText;
			}
		}


		/// <summary>
		///     Creates a memberwise clone of the object.
		/// </summary>
		/// <returns>A shallow copy of this Command Node object</returns>
		public ASCIICommandNodeVM Clone()
		{
			var newNode = (ASCIICommandNodeVM) MemberwiseClone();
			newNode.Content = string.Copy(Content);
			return newNode;
		}


		/// <summary>
		///     A reference to the ASCIICommandNode object that constructed the
		///     VM.
		/// </summary>
		public ASCIICommandNode NodeInfo
		{
			get => _node;
			set => Set(ref _node, value, nameof(NodeInfo));
		}


		/// <summary>
		///     A string representing the value in this node. For a parameter,
		///     this will be entered by the user.  For a literal token, it will
		///     simply be the text displayed in the command list.
		/// </summary>
		public string Content
		{
			get => _content;
			set => Set(ref _content, value, nameof(Content));
		}


		/// <summary>
		///     The unit selected by the user for this node. Null if a unit does not
		///     apply or if no unit is selected.
		/// </summary>
		public UnitOfMeasure SelectedUnit
		{
			get => _selectedunit;
			set => Set(ref _selectedunit, value, nameof(SelectedUnit));
		}


		/// <summary>
		///     The potential entries for an enumerated parameter. For anything
		///     else, this list will be null.
		/// </summary>
		public IEnumerable<string> EnumValues => NodeInfo.ParamType.EnumValues;


		/// <summary>
		///     Base unit for unit conversion factors.
		/// </summary>
		public UnitOfMeasure RequestUnit => NodeInfo.RequestUnit;


		/// <summary>
		///     The units which can be used to convert the node data to Zaber
		///     device units, if applicable.
		/// </summary>
		public ObservableCollection<object> Units
		{
			get => _units;
			set => Set(ref _units, value, nameof(Units));
		}

		#endregion

		#region -- Data --

		private ASCIICommandNode _node;
		private string _content = string.Empty;
		private ObservableCollection<object> _units;
		private UnitOfMeasure _selectedunit;

		#endregion
	}
}

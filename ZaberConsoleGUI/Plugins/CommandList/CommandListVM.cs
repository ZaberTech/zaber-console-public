﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using log4net;
using Zaber;
using Zaber.PlugIns;
using ZaberConsole.Properties;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.CommandList
{
	[PlugIn(Name = "Commands", Description = "Select from a menu of device commands.")]
	public class CommandListVM : CommandListBaseVM
	{
		#region -- Public Methods & Properties --

		#region -- Setup --

		public CommandListVM()
		{
			SetLogger(_log);
			_timeoutTimer.Timeout = DefaultTimeout;

			Settings.Default.CommandGridSplitters = UpgradeWindowSettings(Settings.Default.CommandGridSplitters);

			UpdateHideHelpButton();
		}

		#endregion

		#region -- View-Bound Properties --

		/// <summary>
		///     Set this to true to force cursor focus to the autocomplete text box.
		/// </summary>
		public bool AutocompleteHasFocus
		{
			get => _autocompleteHasFocus;
			set
			{
				_autocompleteHasFocus = value;
				OnPropertyChanged(nameof(AutocompleteHasFocus));
			}
		}

		#endregion

		#endregion

		#region -- Plugin interface --

		[PlugInMethod(PlugInMethodAttribute.EventType.PluginShown)]
		public void OnShown()
		{
			_amVisible = true;

			if (IsPortOpenInAsciiMode)
			{
				_dispatcher.BeginInvoke(() => { AutocompleteHasFocus = true; });
			}
		}


		[PlugInMethod(PlugInMethodAttribute.EventType.PluginHidden)]
		public void OnHidden()
		{
			_amVisible = false;

			_dispatcher.BeginInvoke(() => { AutocompleteHasFocus = false; });
		}

		#endregion

		#region -- View data --

		/// <summary>
		///     Display width of the control panel.
		/// </summary>
		public override string LeftColumnWidth
		{
			get => Settings.Default.CommandGridSplitters.UserMeasurements[0];
			set
			{
				Settings.Default.CommandGridSplitters.UserMeasurements[0] = value;
				OnPropertyChanged(nameof(LeftColumnWidth));
			}
		}


		/// <summary>
		///     Display width of the help panel.
		/// </summary>
		public override string RightColumnWidth
		{
			get => Settings.Default.CommandGridSplitters.UserMeasurements[1];
			set
			{
				Settings.Default.CommandGridSplitters.UserMeasurements[1] = value;
				OnPropertyChanged(nameof(RightColumnWidth));
				UpdateHideHelpButton();
			}
		}

		#endregion

		#region -- Event handling --

		protected override void OnPortOpened()
		{
			base.OnPortOpened();
			FilterMode = Settings.Default.CommandListFilterMode;

			if (_amVisible && IsPortOpenInAsciiMode)
			{
				_dispatcher.BeginInvoke(() => { AutocompleteHasFocus = true; });
			}
		}


		protected override void OnPortClosed()
		{
			_conversationMap.Clear();
			Settings.Default.CommandListFilterMode = FilterMode;
			base.OnPortClosed();

			_dispatcher.BeginInvoke(() => { AutocompleteHasFocus = false; });
		}

		#endregion

		#region -- Command list generation --

		protected override void Populate()
		{
			if (PortFacade.IsOpen)
			{
				ListCommands();
			}
			else if (null != ItemList)
			{
				ItemList.SelectedRow = null;
				ItemList.Rows.Clear();
			}
		}


		// Grab the appropriate set of commands for the currently selected Conversation.
		private void ListCommands()
		{
			var device = Conversation?.Device;

			if (device is null)
			{
				return;
			}

			// Generate list of commands to display.
			if (!_conversationMap.TryGetValue(Conversation, out var views))
			{
				views = CreateCommands() ?? new List<CommandInfoVM>();
				_conversationMap[Conversation] = views;
			}

			// Set command visibility according to device access level setting.
			foreach (var view in views)
			{
				var cmd = view.CommandInfo;
				view.VisibilityLevel = GetCommandVisibility(cmd, device.AccessLevel);
			}

			// If we're in ASCII mode and hierarchical commands are available, use the new tree view.
			CommandInfoList newItemList;
			if (IsPortOpenInAsciiMode && views.Any(v => v is ASCIICommandInfoVM))
			{
				// For ASCII mode and if the device is in the database, use the new hierarchical command list.
				newItemList = new CommandTreeVM(IsPortOpenInAsciiMode, device.GetIdentity());
			}
			else
			{
				// For settings, binary mode or safe mode, use the old flat command list.
				newItemList = new CommandGridVM(IsPortOpenInAsciiMode, device.GetIdentity());
			}

			newItemList.Rows = new ObservableCollection<CommandGridRowBaseVM>(views);

			//add event handlers to the list
			newItemList.PropertyChanged += OnItemListPropertyChanged;
			newItemList.OnRowInvoked += ItemList_OnRowInvoked;

			// Set up the new control.
			ItemList = newItemList;
			newItemList.SetViewMode(FilterMode);

			AutocompleteList = new ObservableCollection<string>(
				AsciiCommandHelper.ListAsciiCommandCompletions(
					device.DeviceType?.Commands?.OfType<ASCIICommandInfo>() ?? new ASCIICommandInfo[0]));
		}


		// Create VMs from each CommandInfo object and sort
		public List<CommandInfoVM> CreateCommands()
		{
			var device = Conversation.Device;

			var cmdEntries = device.DeviceType.Commands.Where(cmd => !(cmd.IsSetting || cmd.IsResponseOnly));

			var views = new List<CommandInfoVM>();

			foreach (var cmd in cmdEntries)
			{
				// ASCII entry
				if (cmd is ASCIICommandInfo info)
				{
					var view = new ASCIICommandInfoVM(info, Conversation, _timeoutTimer);
					PopulateUnits(device, view);
					views.Add(view);
				}

				// binary entry
				else
				{
					var view = new CommandInfoVM(cmd, Conversation, _timeoutTimer);
					if (cmd.HasParameters)
					{
						PopulateUnits(device, view);
					}

					views.Add(view);
				}
			}

			views.Sort((viewA, viewB) => string.CompareOrdinal(viewA.Name, viewB.Name)); //sort in above foreach loop?
			return views;
		}


		private CommandVisibility GetCommandVisibility(CommandInfo aCommand, int aAccessLevel)
		{
			// This is the command grid = no settings allowed.
			if (aCommand is SettingInfo || aCommand.IsResponseOnly)
			{
				return CommandVisibility.NEVER;
			}

			// Commands must match the access level.
			if (aAccessLevel < aCommand.AccessLevel)
			{
				return CommandVisibility.NEVER;
			}

			// Do not show commands which protocol does not match port
			if (PortFacade.Port.IsAsciiMode)
			{
				if (null == aCommand.TextCommand)
				{
					return CommandVisibility.NEVER;
				}
			}
			else
			{
				if (null != aCommand.TextCommand)
				{
					return CommandVisibility.NEVER;
				}
			}

			if (!aCommand.IsBasic)
			{
				return CommandVisibility.ADVANCED;
			}

			return CommandVisibility.BASIC;
		}

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private const int DefaultTimeout = 500;

		private readonly Dictionary<Conversation, List<CommandInfoVM>> _conversationMap =
			new Dictionary<Conversation, List<CommandInfoVM>>();

		private readonly TimeoutTimer _timeoutTimer = new TimeoutTimer();
		private readonly IDispatcher _dispatcher = DispatcherHelper.Dispatcher;

		private bool _autocompleteHasFocus;
		private bool _amVisible;

		#endregion
	}
}

﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using log4net;
using Zaber;
using Zaber.Telemetry;
using Zaber.Threading;

namespace ZaberConsole.Plugins.CommandList
{
	/// <summary>
	///     Used to display a command in an editable grid or list of commands or
	///     settings for one device.
	/// </summary>
	public class CommandInfoVM : MeasurementVM
	{
		#region -- Public Methods & Properties --

		#region -- Setup --

		/// <summary>
		///     Create a new instance.
		/// </summary>
		/// <param name="aCommandInfo">the command to represent with this view.</param>
		/// <param name="aConversation">
		///     The Conversation to which to send the
		///     command represented by this view.
		/// </param>
		/// <param name="aTimer">Optional timeout for awaiting command responses.</param>
		public CommandInfoVM(CommandInfo aCommandInfo, Conversation aConversation, TimeoutTimer aTimer = null)
			: base(aConversation)
		{
			CommandInfo = aCommandInfo;
			Name = aCommandInfo.TextCommand ?? aCommandInfo.Name;

			if ((null != aConversation) && !(aConversation.Device?.Port?.IsAsciiMode ?? true))
			{
				var helpMapper = aConversation.Device.HelpSource;
				HelpUrl = helpMapper?.GetCommandHelpPath(((int) aCommandInfo.Command).ToString());
			}

			_timeoutTimer = aTimer;
			if (null == _timeoutTimer)
			{
				_timeoutTimer = new TimeoutTimer { Timeout = Timeout.Infinite };
			}

			_dataVM.HasParameters = aCommandInfo.HasParameters;
			_dataVM.OnSendClicked += SendButtonClicked;
			Data = _dataVM;
			Clear();
		}

		#endregion

		#endregion

		#region -- Public API --

		public CommandInfo CommandInfo { get; }

		public override UnitOfMeasure RequestUnit => CommandInfo.RequestUnit;

		/// <summary>
		///     The unmodified name of the command. Settings will start with "Set" or "Return".
		/// </summary>
		public string CommandName => CommandInfo.Name;


		/// <summary>
		///     ToString() override for display convenience. Returns the command name.
		/// </summary>
		/// <returns></returns>
		public override string ToString() => CommandName;


		/// <summary>
		///     Fired when enter is pressed on a row. Performs the default action, which is to
		///     send the command.
		/// </summary>
		public override void Invoke() => SendButtonClicked(null, null);

		#endregion

		#region -- Functionality --

		private void SendButtonClicked(object aSender, EventArgs aArgs)
		{
			InvokeBeforeCommand();

			ConversationTopic topic = null;
			try
			{
				topic = Request(_dataVM.Value);
			}
			catch (ConversionException e)
			{
				ToolTip = e.Message;
				return;
			}

			if (topic != null)
			{
				var worker = BackgroundWorkerManager.CreateWorker();
				worker.DoWork += WaitForRequest;
				worker.RunWorkerCompleted += RequestCompleted;
				worker.Run(topic);
			}
		}


		protected void WaitForRequest(object sender, DoWorkEventArgs e)
		{
			var request = (ConversationTopic) e.Argument;
			WaitForResponse(request);
			e.Result = request;
		}


		protected void RequestCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			var request = (ConversationTopic) e.Result;
			OnPropertyChanged(nameof(Data));
		}


		/// <summary>
		///     Remove the current value and error text.
		/// </summary>
		public void Clear()
		{
			ToolTip = null;
			_dataVM.Value = string.Empty;
		}


		protected void ProcessResponse(ConversationTopic aTopic)
		{
			if (aTopic is ConversationTopicCollection topics)
			{
				ProcessResponseCollection(topics);
			}
			else
			{
				var error = FormatResponseError(aTopic);
				ToolTip = error;
				if (!string.IsNullOrEmpty(error))
				{
					_log.Info("Error response: " + error);
				}
			}

			aTopic.Dispose();
		}


		private void ProcessResponseCollection(ConversationTopicCollection aTopics)
		{
			var isMultipleResponse = false;
			var display = "";
			var error = "";
			var displayBuilder = new StringBuilder();
			var singleDisplayBuilder = new StringBuilder();
			var errorBuilder = new StringBuilder();

			foreach (var subtopic in aTopics)
			{
				var subdisplay = FormatResponseData(subtopic);
				var suberror = FormatResponseError(subtopic);
				if (displayBuilder.Length == 0)
				{
					display = FormatResponseData(subtopic);
					error = FormatResponseError(subtopic);
					singleDisplayBuilder.Append(display);
				}
				else
				{
					displayBuilder.AppendLine();
					singleDisplayBuilder.AppendLine();
					isMultipleResponse =
						isMultipleResponse || !subdisplay.Equals(display) || !suberror.Equals(error);
				}

				byte deviceNumber;
				string format;

				if (subtopic.Response == null)
				{
					deviceNumber = 0;
					format = "{1}";
				}
				else
				{
					deviceNumber = subtopic.Response.DeviceNumber;
					format = "{0}: {1}";
				}

				displayBuilder.AppendFormat(format, deviceNumber, subdisplay);

				if (suberror.Length > 0)
				{
					if (errorBuilder.Length == 0)
					{
						errorBuilder.AppendLine("Some requests failed:");
					}
					else
					{
						errorBuilder.AppendLine();
					}

					errorBuilder.AppendFormat(format, deviceNumber, suberror);
				}
			}

			ToolTip = isMultipleResponse ? errorBuilder.ToString() : error;
		}


		private string FormatResponseData(ConversationTopic aTopic)
		{
			if (aTopic.IsValid)
			{
				if (aTopic.Response.TextData == null)
				{
					return FormatData(aTopic.Response.NumericData, CommandInfo);
				}

				if ((CommandInfo.ResponseUnit == null)
				|| (CommandInfo.ResponseUnit.MeasurementType == MeasurementType.Other))
				{
					return aTopic.Response.TextData;
				}

				return string.Join(" ", aTopic.Response.NumericValues.Select(value => value != null ? FormatData(value.Value, CommandInfo) : "n/a").ToArray());
			}

			if (IsErrorResponse(aTopic) && IsInvalidCommandOrSetting(aTopic))
			{
				return "n/a";
			}

			return "";
		}


		/// <summary>
		///     Sends a request for this command and waits for a response or until
		///     the <see cref="TimeoutTimer" /> times out.
		/// </summary>
		/// <param name="aDataValue">
		///     A string representation of the data value
		///     to request. Empty string is equivalent to 0.
		/// </param>
		/// <returns>
		///     null if the response was received, or a valid
		///     ConversationTopic if it timed out.
		/// </returns>
		/// <remarks>
		///     Send the ConversationTopic to <see cref="WaitForResponse" />.
		/// </remarks>
		public ConversationTopic Request(string aDataValue)
		{
			decimal parsedValue = 0;
			var isParseSuccessful = true;
			if (aDataValue.Length > 0)
			{
				isParseSuccessful =
					decimal.TryParse(aDataValue, NumberStyles.Float, CultureInfo.CurrentCulture, out parsedValue);
				if (!isParseSuccessful)
				{
					isParseSuccessful = decimal.TryParse(aDataValue,
														 NumberStyles.Float,
														 CultureInfo.InvariantCulture,
														 out parsedValue);
				}

				if (!isParseSuccessful && !Conversation.Device.Port.IsAsciiMode)
				{
					ToolTip = "Invalid number.";
					return null;
				}
			}
			else if (CommandInfo.DataDescription != null)
			{
				ToolTip = "This command requires a data value.";
				return null;
			}

			var measurement = new Measurement(parsedValue, UnitOfMeasure);
			var topic = Conversation.StartTopic();

			if (Conversation.Device.Port.IsAsciiMode)
			{
				var fullCommandName = CommandInfo.TextCommand;

				if ((aDataValue.Length > 0) && isParseSuccessful)
				{
					if (Conversation.Axes.Count == 1)
					{
						//special handling for single axis device with UOM.
						var units = Conversation.Device.Axes[0].CalculateExactData(fullCommandName, measurement);

						_log.InfoFormat("Sending: {0} {1}", fullCommandName, units);
						_eventLog.LogEvent("Sending command", $"{fullCommandName} {units}");

						if (Conversation.Device.AreAsciiMessageIdsEnabled)
						{
							Conversation.Device.SendInUnits(fullCommandName,
															new Measurement(units, UnitOfMeasure.Data),
															topic.MessageId);
						}
						else
						{
							Conversation.Device.SendInUnits(fullCommandName,
															new Measurement(units, UnitOfMeasure.Data));
						}
					}
					else
					{
						_log.InfoFormat("Sending: {0} {1}", fullCommandName, measurement);
						_eventLog.LogEvent("Sending command", $"{fullCommandName} {measurement}");

						if (Conversation.Device.AreAsciiMessageIdsEnabled)
						{
							Conversation.Device.SendInUnits(fullCommandName, measurement, topic.MessageId);
						}
						else
						{
							Conversation.Device.SendInUnits(fullCommandName, measurement);
						}
					}
				}
				else
				{
					var cmdStr = string.Format(CultureInfo.InvariantCulture,
											   aDataValue.Length > 0 ? "{0} {1}" : "{0}",
											   fullCommandName,
											   aDataValue);

					_log.InfoFormat("Sending: {0}", cmdStr);
					_eventLog.LogEvent("Sending command", cmdStr);

					if (Conversation.Device.AreAsciiMessageIdsEnabled)
					{
						Conversation.Send(cmdStr, topic.MessageId);
					}
					else
					{
						Conversation.Send(cmdStr);
					}
				}
			}
			else
			{
				_log.InfoFormat("Sending: {0} {1}", CommandInfo.Command, measurement);
				_eventLog.LogEvent("Sending command", $"{CommandInfo.Command} {measurement}");

				Conversation.Device.SendInUnits(CommandInfo.Command, measurement, topic.MessageId);
			}

			if (topic.Wait(_timeoutTimer))
			{
				ProcessResponse(topic);
				topic = null;
			}
			else
			{
				ToolTip = "";
			}

			return topic;
		}


		/// <summary>
		///     Wait for a request to complete.
		/// </summary>
		/// <param name="aTopic">A topic that was returned by <see cref="Request" />.</param>
		public void WaitForResponse(ConversationTopic aTopic)
		{
			aTopic.Wait();
			ProcessResponse(aTopic);
		}

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(typeof(CommandInfoVM));
		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Commands tab");

		private readonly CommandArgumentsVM _dataVM = new CommandArgumentsVM();
		protected TimeoutTimer _timeoutTimer;

		#endregion
	}
}

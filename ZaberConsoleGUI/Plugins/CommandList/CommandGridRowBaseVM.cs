﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.CommandList
{
	/// <summary>
	///     Base ViewModel for rows in the Zaber Console Command Grid and Settings Grid. Users should
	///     subclass this for the specific type of data they want to display in the Data column, and
	///     set up a globla VM-to-control type association so it will be displayed with their custom
	///     control type in that column.
	/// </summary>
	public class CommandGridRowBaseVM : ObservableObject
	{
		#region -- Non-Public Methods --

		#region -- Protected API --

		protected void InvokeBeforeCommand() => BeforeCommand?.Invoke(this, EventArgs.Empty);

		#endregion

		#endregion

		#region -- Events

		/// <summary>
		///     Invoked when the user clicks the star (favorite) button on a command.
		/// </summary>
		public event EventHandler OnFavoriteChanged;

		/// <summary>
		///     Invoked when the user clicks the send button on a row, before the command is actually sent.
		/// </summary>
		public event EventHandler BeforeCommand;

		#endregion

		#region -- View-bound properties and client API --

		/// <summary>
		///     Content to be displayed in the Name column of the grid.
		/// </summary>
		public string Name
		{
			get => _name;
			set => Set(ref _name, value, nameof(Name));
		}


		/// <summary>
		///     Content to be displayed in the Data column of the grid. This is expected to
		///     be a user-defined VM type and have appropriate control associations set up
		///     by the client application.
		/// </summary>
		public ObservableObject Data
		{
			get => _data;
			set => Set(ref _data, value, nameof(Data));
		}


		/// <summary>
		///     Content for the Units column. A list of unit of measure names.
		///     The instance of this list can be shared with other VM instances;
		///     recommended usage is to just have one global instance of each different
		///     list if there are a small number of them.
		/// </summary>
		public ObservableCollection<object> Units
		{
			get => _units;
			set => Set(ref _units, value, nameof(Units));
		}


		/// <summary>
		///     Currently selected unit in the Units column.
		/// </summary>
		public object SelectedUnit
		{
			get => _selectedUnit;
			set => Set(ref _selectedUnit, value, nameof(SelectedUnit));
		}


		/// <summary>
		///     ToolTip message. If not empty, the row will have a tooltip
		///     dynamically added to display this. Set to null or empty to
		///     remove the tooltip.
		/// </summary>
		public string ToolTip
		{
			get => _toolTip;
			set => Set(ref _toolTip, value, nameof(ToolTip));
		}


		/// <summary>
		///     Used to provide links to online help for commands and settings.
		///     Value should be set by subclasses.
		/// </summary>
		public string HelpUrl { get; set; }


		/// <summary>
		///     Controls whether or not this row is displayed. Defaults to
		///     true; set to false to hide the row without having to remove it
		///     from the row collection.
		/// </summary>
		public bool Visible
		{
			get => _visible;
			set => Set(ref _visible, value, nameof(Visible));
		}


		/// <summary>
		///     Visibility level of the command, according to its basic/advanced property,
		///     the current protocol, and the current command filtering mode.
		/// </summary>
		public CommandVisibility VisibilityLevel
		{
			get => _visibilitylevel;
			set => Set(ref _visibilitylevel, value, nameof(VisibilityLevel));
		}


		/// <summary>
		///     Indicates if the command has been marked as a favorite by the user.
		/// </summary>
		public bool Favorited
		{
			get => _favorited;
			set => Set(ref _favorited, value, nameof(Favorited));
		}


		/// <summary>
		///     Fires the event for starring or unstarring this command.
		/// </summary>
		public ICommand ToggleFavoriteCommand => new RelayCommand(_ =>
		{
			Favorited = !Favorited;
			OnFavoriteChanged?.Invoke(this, null);
		});

		#endregion

		#region -- Data --

		private string _name = string.Empty;
		private ObservableObject _data;
		private ObservableCollection<object> _units = new ObservableCollection<object>();
		private object _selectedUnit;
		private string _toolTip;
		private bool _visible;
		private bool _favorited;
		private CommandVisibility _visibilitylevel;

		#endregion
	}
}

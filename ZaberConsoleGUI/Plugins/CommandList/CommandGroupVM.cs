﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using ZaberConsole.Plugins.SettingsList;

namespace ZaberConsole.Plugins.CommandList
{
	/// <summary>
	///     View model for grouping commands into headings. A
	///     command group element will be constructed when a group of
	///     commands with the same first node is considered too large.
	/// </summary>
	public class CommandGroupVM : CommandGridRowBaseVM
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     All commands under the heading.
		/// </summary>
		public ObservableCollection<CommandGridRowBaseVM> Children
		{
			get => _children;
			set => Set(ref _children, value, nameof(Children));
		}


		/// <summary>
		///     The header name, as seen in the UI.
		/// </summary>
		public string HeaderText
		{
			get => _header;
			set => Set(ref _header, value, nameof(HeaderText));
		}


		/// <summary>
		///     Controls whether or not the groups is expanded.
		/// </summary>
		public bool IsExpanded
		{
			get => _isExpanded;
			set => Set(ref _isExpanded, value, nameof(IsExpanded));
		}


		/// <summary>
		///     The visibility of the heading, as determined by the minimum visibility of
		///     its children.
		/// </summary>
		public new CommandVisibility VisibilityLevel
		{
			get
			{
				var groupChildren =
					from child in Children
					where child is CommandGroupVM
					select (int) ((CommandGroupVM) child).VisibilityLevel;
				var asciiChildren =
					from child in Children where child is ASCIICommandInfoVM select (int) child.VisibilityLevel;
				var asciiSettingChildren =
					from child in Children where child is SettingInfoVM select (int) child.VisibilityLevel;

				return (CommandVisibility) groupChildren
									   .Union(asciiChildren)
									   .Union(asciiSettingChildren)
									   .Aggregate((a, b) => Math.Min(a, b));
			}
		}

		#endregion

		#region -- Data --

		private string _header;
		private bool _isExpanded;
		private ObservableCollection<CommandGridRowBaseVM> _children;

		#endregion
	}
}

﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Zaber;
using ZaberConsole.Plugins.SettingsList;
using ZaberConsole.Properties;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.CommandList
{
	/// <summary>
	///     ViewModel for binary commands and settings.
	/// </summary>
	public class CommandGridVM : CommandInfoList
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Constructor. Initializes favorite settings.
		/// </summary>
		/// <param name="aIsAsciiConnection">Whether the current connection is ASCII or Binary.</param>
		/// <param name="aDeviceIdentity">
		///     The identity of the device whose commands
		///     are being presented, for the purposes of retrieving and storing favorite
		///     commands for this device. Pass null to skip using stored favorites.
		/// </param>
		public CommandGridVM(bool aIsAsciiConnection, DeviceIdentity aDeviceIdentity = null)
			: base(aIsAsciiConnection)
		{
			// Set up saved favorite command settings.
			var settings = Settings.Default.CommandListFavorites;
			var id = aDeviceIdentity;
			if (null != id)
			{
				_favoriteSettings =
					settings.FindOrCreateSettings(id, Settings.Default.CommandsAndSettingsFavoriteKeyMode);
			}
		}


		// Called when the master units change - updates the units on all commands or settings.
		public override void ChangeAllUnits(UnitOfMeasure aNewUnit)
		{
			var derivative = null == aNewUnit ? UnitOfMeasure.Data : aNewUnit.Derivative ?? UnitOfMeasure.Data;
			var secondDerivative = derivative.Derivative ?? UnitOfMeasure.Data;
			foreach (MeasurementVM cmdView in Rows.Where(c => null != (c as MeasurementVM)?.RequestUnit))
			{
				// Master UOMs are always units of position, so we don't need to
				// handle all possible combinations here.
				if (null != cmdView.RequestUnit)
				{
					switch (cmdView.RequestUnit.MeasurementType)
					{
						case MeasurementType.Position:
							cmdView.UnitOfMeasure = aNewUnit;
							break;
						case MeasurementType.Velocity:
							cmdView.UnitOfMeasure = derivative;
							break;
						case MeasurementType.Acceleration:
							cmdView.UnitOfMeasure = secondDerivative;
							break;
						case MeasurementType.Other:
							break;
						case MeasurementType.Current:
							break;
						case MeasurementType.CurrentAC:
							break;
						case MeasurementType.Force:
							break;
						default:
							throw new InvalidEnumArgumentException("Unrecognized MeasurementType.");
					}
				}
			}

			OnPropertyChanged(nameof(Rows));
		}


		/// <inheritdoc cref="CommandInfoList.UpdateHelpUrls" />
		public override void UpdateHelpUrls(IDeviceHelp aHelpMapper)
		{
			foreach (var item in Rows)
			{
				//search for a heading in the protocol with a matching Id to the command
				string newUrl = null;

				if (IsAsciiConnection)
				{
					// The ASCII case only needs to handle commands.
					// This code path is only used when an ASCII device is not in the
					// database and "help commands list" is used.
					newUrl = aHelpMapper.GetCommandHelpPath(item.Name);
				}
				else
				{
					// This control is used for both commands and settings in Binary, so
					// handle both cases separately.
					if (item is CommandInfoVM ci)
					{
						newUrl = aHelpMapper.GetCommandHelpPath(((int) ci.CommandInfo.Command).ToString());
					}
					else if (item is SettingInfoVM si)
					{
						newUrl = aHelpMapper.GetSettingHelpPath(((int) si.SettingInfo.Command).ToString());
					}
				}

				if (!string.IsNullOrEmpty(newUrl))
				{
					item.HelpUrl = newUrl;
				}
			}
		}

		#endregion

		#region -- Non-Public Methods --

		// Called when rows are displayed to determine whether or not they should be marked as favorites,
		// according to previous user settings.
		protected override bool IsFavorited(CommandGridRowBaseVM aItem)
		{
			if (null != _favoriteSettings)
			{
				// This VM is only used for binary commands and settings.
				var setting = aItem as SettingInfoVM;
				var cmd = aItem as CommandInfoVM;

				if (IsAsciiConnection)
				{
					if (null != setting)
					{
						return _favoriteSettings.FavoriteAsciiSettings.Contains(setting.CommandName);
					}

					if (null != cmd)
					{
						return _favoriteSettings.FavoriteAsciiCommands.Contains(cmd.CommandName);
					}
				}
				else
				{
					if (null != setting)
					{
						return _favoriteSettings.FavoriteBinarySettings.Contains((int) setting.SettingInfo.Command);
					}

					if (null != cmd)
					{
						return _favoriteSettings.FavoriteBinaryCommands.Contains((int) cmd.CommandInfo.Command);
					}
				}
			}

			return false;
		}


		// Invoked when a command or setting is favorited or unfavorited by the user. Saves the new
		// state in user settings.
		protected override void OnItemFavoriteStateChanged(CommandGridRowBaseVM aItem)
		{
			if (null != _favoriteSettings)
			{
				// This VM is only used for binary commands and settings.
				var setting = aItem as SettingInfoVM;
				var cmd = aItem as CommandInfoVM;

				// Normally this class is not used with ASCII devices, but it can still occur
				// as a fallback when a device is not in the device database, and with FW6 devices
				// it is possible for actual commands and settings to be listed as a result
				// of sending "help commands list" during detection.
				if (IsAsciiConnection)
				{
					if (null != cmd)
					{
						_favoriteSettings.FavoriteAsciiCommands.Remove(cmd.CommandName);
						if (cmd.Favorited)
						{
							_favoriteSettings.FavoriteAsciiCommands.Add(cmd.CommandName);
						}
					}
					else if (null != setting)
					{
						_favoriteSettings.FavoriteAsciiSettings.Remove(setting.CommandName);
						if (setting.Favorited)
						{
							_favoriteSettings.FavoriteAsciiSettings.Add(setting.CommandName);
						}
					}
				}
				else
				{
					List<int> favorites = null;
					var value = 0;
					if (null != setting)
					{
						favorites = _favoriteSettings.FavoriteBinarySettings;
						value = (int) setting.SettingInfo.Command;
					}
					else if (null != cmd)
					{
						favorites = _favoriteSettings.FavoriteBinaryCommands;
						value = (int) cmd.CommandInfo.Command;
					}

					if (null != favorites)
					{
						favorites.Remove(value);
						if (aItem.Favorited)
						{
							favorites.Add(value);
						}
					}
				}
			}
		}

		#endregion

		#region -- Data --

		private IDispatcher _dispatcher = DispatcherHelper.Dispatcher;
		private readonly FavoriteCommandDeviceSettings _favoriteSettings;

		#endregion
	}
}

﻿using System;
using System.Windows.Input;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.CommandList
{
	public class CommandArgumentsVM : ObservableObject
	{
		#region -- Events --

		#region -- Control events --

		public event EventHandler OnSendClicked;

		#endregion

		#endregion

		#region -- View-bound properties --

		public string Value
		{
			get => _value;
			set => Set(ref _value, value, nameof(Value));
		}


		public string ButtonLabel
		{
			get => _buttonLabel;
			set => Set(ref _buttonLabel, value, nameof(ButtonLabel));
		}


		public bool EnableButton
		{
			get => _enableButton;
			set => Set(ref _enableButton, value, nameof(EnableButton));
		}


		public bool HasParameters
		{
			get => _hasParameters;
			set => Set(ref _hasParameters, value, nameof(HasParameters));
		}


		public ICommand SendCommand => new RelayCommand(_ => OnSendClicked?.Invoke(this, null));

		#endregion

		#region -- Data --

		private string _value = string.Empty;
		private string _buttonLabel = "Send";
		private bool _enableButton = true;
		private bool _hasParameters = true;

		#endregion
	}
}

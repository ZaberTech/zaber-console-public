﻿namespace ZaberConsole.Plugins.CommandList
{
	/// <summary>
	///     The visibility type of a command or setting, which determines whether or not it can be
	///     seen in the UI.
	/// </summary>
	public enum CommandVisibility
	{
		NEVER,
		BASIC,
		ADVANCED
	}
}

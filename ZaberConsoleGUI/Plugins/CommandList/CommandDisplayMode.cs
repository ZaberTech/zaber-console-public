﻿namespace ZaberConsole.Plugins.CommandList
{
	/// <summary>
	///     Enumeration corresponding to the filter radio buttons on the ASCII command tree UI.
	/// </summary>
	public enum CommandDisplayMode
	{
		BASIC,
		ADVANCED,
		FAVORITES
	}
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using HtmlAgilityPack;
using log4net;
using Zaber;
using Zaber.PlugIns;
using Zaber.Telemetry;
using Zaber.Units;
using ZaberConsole.Help;
using ZaberConsole.Plugins.SettingsList;
using ZaberConsole.Properties;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Behaviors;

namespace ZaberConsole.Plugins.CommandList
{
	public abstract class CommandListBaseVM : ObservableObject, WebBrowserEvalBehavior.IWebBrowserController
	{
		#region -- Events --

		#region -- IWebBrowserController implementation --

		public event WebBrowserEvalBehavior.BrowserEvalEventHandler EvalTrigger;

		#endregion

		#endregion

		#region -- Setup --

		/// <summary>
		///     Let subclasses override the logger instance so we can more easily tell
		///     the sources of messages apart.
		/// </summary>
		/// <param name="aLogger">The logger instance to use.</param>
		protected void SetLogger(ILog aLogger) => _log = aLogger;


		/// <summary>
		///     Reference to the Port Facade; provided by the Plugin Manager.
		/// </summary>
		[PlugInProperty]
		public ZaberPortFacade PortFacade
		{
			get => _portFacade;
			set
			{
				if (null != _portFacade)
				{
					_portFacade.Closed -= OnPortFacadeClosed;
					_portFacade.Opened -= OnPortFacadeOpened;
				}

				_portFacade = value;

				if (null != _portFacade)
				{
					_portFacade.Closed += OnPortFacadeClosed;
					_portFacade.Opened += OnPortFacadeOpened;
				}
			}
		}


		/// <summary>
		///     Reference to the currently selected device conversation; provided by the Plugin Manager.
		/// </summary>
		[PlugInProperty]
		public Conversation Conversation
		{
			get => _conversation;
			set
			{
				_conversation = value;

				_dispatcher.BeginInvoke(() =>
				{
					Populate();
					RefreshHelpUrls();
					ItemList?.SetViewMode(FilterMode);
					OnPropertyChanged(nameof(HelpWarningMessage));

					var helpMapper = _conversation?.Device.HelpSource;
					if (null != helpMapper)
					{
						HelpUrl = helpMapper.GetCommandHelpPath(_lastSelectedCommandName);
					}
				});
			}
		}


		/// <summary>
		///     Currently selected master unit of measure for the currently selected device; provided by the Plugin Manager.
		/// </summary>
		[PlugInProperty]
		public UnitOfMeasure MasterUnitOfMeasure
		{
			get => _masterUnitOfMeasure;
			set => ItemList?.ChangeAllUnits(value);
		}

		#endregion

		#region -- View-bound properties --

		/// <summary>
		///     Indicates whether or not the port is currently open. For UI information only.
		/// </summary>
		public bool IsPortOpen => PortFacade?.IsOpen ?? false;

		/// <summary>
		///     Convenience property to determine if the port is set to ASCII mode.
		/// </summary>
		public bool IsPortOpenInAsciiMode => IsPortOpen && (PortFacade?.Port?.IsAsciiMode ?? false);


		/// <summary>
		///     Command list control for the plugin.  If the port is in ASCII mode,
		///     this will be a <see cref="CommandTreeVM" /> or <see cref="SettingTreeVM" />.
		///     Otherwise, it will be a <see cref="CommandGridVM" />.
		/// </summary>
		public CommandInfoList ItemList
		{
			get => _itemList;
			set => Set(ref _itemList, value, nameof(ItemList));
		}


		public string TextFilter
		{
			get => _textFilter;
			set
			{
				Set(ref _textFilter, value, nameof(TextFilter));
				ItemList?.SetViewMode(FilterMode, _textFilter);
			}
		}


		/// <summary>
		///     List of possible completions for a command being typed in.
		/// </summary>
		public ObservableCollection<string> AutocompleteList
		{
			get => _autocompleteList;
			set => Set(ref _autocompleteList, value, nameof(AutocompleteList));
		}


		/// <summary>
		///     Send command from text field to device.
		/// </summary>
		public ICommand SendTextCommand => new RelayCommand(text => SendText(text));


		/// <summary>
		/// Error messages resulting from sending a text command from the autocomplete box.
		/// </summary>
		public string SendTextError
		{
			get => _sendTextError;
			set => Set(ref _sendTextError, value, nameof(SendTextError));
		}


		private void SendText(object text)
		{
			try
			{
				SendTextError = null;
				var cmd = text as string;
				cmd = (cmd ?? string.Empty).Replace("/", "").Trim();
				_eventLog.LogEvent("Send a command from text box", cmd);
				Conversation.Request(cmd);
			}
			catch (ErrorResponseException ere)
			{
				SendTextError = ere.Message;
			}
			catch (ZaberPortErrorException zpe)
			{
				SendTextError = zpe.Message;
			}
			catch (RequestCollectionException rce)
			{
				SendTextError = rce.Message;
			}
			catch (RequestReplacedException rre)
			{
				SendTextError = rre.Message;
			}
			catch (InvalidOperationException ioe)
			{
				SendTextError = ioe.Message;
			}
		}


		/// <summary>
		///     URL for the command help to display in the help browser panel.
		/// </summary>
		public string HelpUrl
		{
			get => _helpUrl;
			set
			{
				_showingDefaultHelp = false;
				var newUrl = value;
				if (null == newUrl)
				{
					newUrl = Conversation?.Device.HelpSource?.DefaultHelpPath;
					_showingDefaultHelp = true;
				}

				// If the help URL does not change, the browser will not fire the document
				// loaded event, meaning we must explicitly re-apply profiling. Since
				// the document is not being reloaded, it should be safe to do this immediately.
				var forceProfile = (_helpUrl == newUrl) && !string.IsNullOrEmpty(newUrl);

				try
				{
					Set(ref _helpUrl, newUrl, nameof(HelpUrl));
				}
				catch (UriFormatException ufe)
				{
					// Handle invalid URLs by logging the error and doing nothing.
					_log.Warn(("Invalid URL encountered in help: " + newUrl) ?? "<null>", ufe);
				}

				OnPropertyChanged(nameof(HelpWarningMessage));

				if (forceProfile)
				{
					ApplyHelpProfiling();
				}
			}
		}


		/// <summary>
		/// User agent header to use when browsing help.
		/// </summary>
		public string UserAgent
			=> "User-Agent: " + Configuration.UserAgent;


		/// <summary>
		///     Currently selected filtering mode for commands and settings.
		/// </summary>
		public CommandDisplayMode FilterMode
		{
			get => _filterMode;
			set
			{
				Set(ref _filterMode, value, nameof(FilterMode));
				ItemList?.SetViewMode(value);
			}
		}


		/// <summary>
		///     Warning message displayed about the help panel if the content may ne outdated.
		/// </summary>
		public string HelpWarningMessage
		{
			get
			{
				var msg = string.Empty;

				if (_showingDefaultHelp && (null != ItemList.SelectedRow))
				{
					msg += "Sorry, there is no help available for the selected item.";
				}

				var url = HelpUrl ?? string.Empty;
				if (url.StartsWith("file:") && !url.Contains("Placeholder"))
				{
					msg += " Offline Help mode; content may be outdated.";
				}

				return msg;
			}
		}


		/// <summary>
		///     Display width of the control panel.
		/// </summary>
		public abstract string LeftColumnWidth { get; set; }


		/// <summary>
		///     Display width of the help panel.
		/// </summary>
		public abstract string RightColumnWidth { get; set; }


		/// <summary>
		///     Label for the button that expands and collapses the help panel.
		/// </summary>
		public string HideHelpButtonLabel
		{
			get => _hideHelpButtonLabel;
			set => Set(ref _hideHelpButtonLabel, value, nameof(HideHelpButtonLabel));
		}


		/// <summary>
		///     Signal to the view to disable tab navigation when the web browser is hidden.
		/// </summary>
		public bool IsHelpVisible
		{
			get => _isHelpVisible;
			set => Set(ref _isHelpVisible, value, nameof(IsHelpVisible));
		}


		/// <summary>
		///     Command invoked by the show/hide help button.
		/// </summary>
		public ICommand HideHelpButtonCommand => new RelayCommand(_ =>
		{
			var left = (GridLength) _gridLengthConverter.ConvertFromString(LeftColumnWidth);
			var right = (GridLength) _gridLengthConverter.ConvertFromString(RightColumnWidth);
			var sum = left.Value + right.Value;
			if (right.Value < (0.05 * sum))
			{
				var half = 0.5 * sum;
				RightColumnWidth = half.ToString() + "*";
				LeftColumnWidth = RightColumnWidth;
			}
			else
			{
				RightColumnWidth = "0*";
				LeftColumnWidth = sum.ToString() + "*";
			}
		});


		/// <summary>
		///     Event to be fired when the help text completes loading.
		/// </summary>
		public ICommand BrowserDocumentLoadedCommand
			=> OnDemandRelayCommand(ref _documentLoadedCommand, _ => { ApplyHelpProfiling(); });

		#endregion

		#region -- Event handling --

		// Subclasses can override this to receive port opened events.
		// This will be called on the UI thread.
		protected virtual void OnPortOpened()
		{
			Populate();
			RefreshHelpUrls();
		}


		// Subclasses can override this to receive port closed events.
		// This will be called on the UI thread.
		protected virtual void OnPortClosed() => ItemList?.Clear();


		// Redirects to cleaner OnPortOpened().
		private void OnPortFacadeOpened(object sender, EventArgs e) => _dispatcher.BeginInvoke(() =>
		{
			OnPropertyChanged(nameof(IsPortOpen));
			OnPropertyChanged(nameof(IsPortOpenInAsciiMode));
			OnPortOpened();
		});


		// Redirects to cleaner OnPortClosed().
		private void OnPortFacadeClosed(object sender, EventArgs e) => _dispatcher.BeginInvoke(() =>
		{
			OnPropertyChanged(nameof(IsPortOpen));
			OnPropertyChanged(nameof(IsPortOpenInAsciiMode));
			OnPortClosed();
		});

		#endregion

		#region -- Functionality --

		// Subclasses must implement this to create the ItemList and populate it.
		// This will be called on the UI thread.
		protected abstract void Populate();


		/// <summary>
		///     Populates the units of a <see cref="MeasurementVM" /> object.
		/// </summary>
		/// <param name="aDevice">The device where the command will be sent.</param>
		/// <param name="aInfo">The command to populate.</param>
		protected void PopulateUnits(ZaberDevice aDevice, MeasurementVM aInfo)
		{
			var measurementType = MeasurementType.Other;

			if (aInfo is ASCIICommandInfoVM)
			{
				// for an ASCII command, populate the units of all applicable nodes
				foreach (var node in (aInfo as ASCIICommandInfoVM).Nodes)
				{
					if (null != node.RequestUnit)
					{
						if (node.NodeInfo.ParamType.IsNumeric)
						{
							measurementType = node.RequestUnit.MeasurementType;
							node.Units =
								new ObservableCollection<object>(
									GetUnits(aDevice, measurementType, node.RequestUnit.Unit?.Dimension));
						}
						else // Don't offer unit conversions for non-numeric parameter types.
						{
							node.Units = new ObservableCollection<object>();
						}
					}
				}
			}
			else if (null != aInfo.RequestUnit)
			{
				measurementType = aInfo.RequestUnit.MeasurementType;
				aInfo.Units =
					new ObservableCollection<object>(GetUnits(aDevice,
															  measurementType,
															  aInfo.RequestUnit.Unit?.Dimension));
			}
		}


		/// <summary>
		///     Helper method for <see cref="PopulateUnits(ZaberDevice, MeasurementVM)" />.
		/// </summary>
		/// <param name="aDevice">The device to get units for.</param>
		/// <param name="aType">The unit type to retrieve (i.e. acceleration, velocity, etc.).</param>
		/// <param name="aDimension">Optional - dimension to use for more future-proof unit identification.</param>
		/// <returns>
		///     A collection of <see cref="UnitOfMeasure" /> objects available to the device
		///     for the specified measurement type.
		/// </returns>
		private ICollection<UnitOfMeasure> GetUnits(ZaberDevice aDevice, MeasurementType aType,
													Dimension aDimension = null)
		{
			if (aDevice is DeviceCollection) // Unit conversions not currently supported for multi-select.
			{
				return new[] { UnitOfMeasure.Data };
			}

			ICollection<UnitOfMeasure> units = null;
			var deviceToGetFrom = aDevice;
			if ((1 == aDevice.Axes.Count) && (0 == aDevice.AxisNumber))
			{
				// Device with single axis, get UoM from axis
				deviceToGetFrom = aDevice.Axes[0];
			}

			// Use dimension if available - more future-proof.
			if (null != aDimension)
			{
				units = deviceToGetFrom.GetUnitsOfMeasure(aDimension);
			}
			else
			{
				units = deviceToGetFrom.GetUnitsOfMeasure(aType);
			}

			return units;
		}


		// Converts splitter position from legacy WinForms settings.
		protected WindowSettings UpgradeWindowSettings(WindowSettings aSettings)
		{
			// Upgrade grid splitter position to WPF format.
			var windowSettings = aSettings;
			if (null == windowSettings)
			{
				windowSettings = new WindowSettings();
				aSettings = windowSettings;
			}

			if (null == windowSettings.UserMeasurements)
			{
				windowSettings.UserMeasurements = new string[2];
				if (null != windowSettings.SplitterDistances)
				{
					windowSettings.UserMeasurements[0] = windowSettings.SplitterDistances[0] + "*";
					windowSettings.UserMeasurements[1] =
						(Settings.Default.ZaberConsoleWindowSettings.WindowWidth - windowSettings.SplitterDistances[0])
					+ "*";
				}
				else
				{
					windowSettings.UserMeasurements[0] = "1*";
					windowSettings.UserMeasurements[1] = "1*";
				}
			}

			return windowSettings;
		}


		protected void UpdateHideHelpButton()
		{
			var left = (GridLength) _gridLengthConverter.ConvertFromString(LeftColumnWidth);
			var right = (GridLength) _gridLengthConverter.ConvertFromString(RightColumnWidth);
			var sum = left.Value + right.Value;
			if (right.Value < (0.05 * sum))
			{
				HideHelpButtonLabel = "Show help";
				IsHelpVisible = false;
			}
			else
			{
				HideHelpButtonLabel = "Hide help";
				IsHelpVisible = true;
			}

			OnPropertyChanged(nameof(HelpWarningMessage));
		}


		// Listen for selection changes to update help URL.
		protected void OnItemListPropertyChanged(object aSender, PropertyChangedEventArgs aArgs)
		{
			if (nameof(ItemList.SelectedRow) == aArgs.PropertyName)
			{
				if (null != ItemList.SelectedRow)
				{
					var selectedName = ItemList.SelectedRow.Name;
					if (_lastSelectedCommandName != selectedName)
					{
						_lastSelectedCommandName = selectedName;
						HelpUrl = ItemList.SelectedRow?.HelpUrl;
					}
				}
			}
		}


		// Listen for enter or double click on rows.
		protected void ItemList_OnRowInvoked(object aSender, EventArgs aArgs)
		{
			if (((CommandInfoList.RowEventArgs) aArgs).SelectedRow is MeasurementVM vm)
			{
				vm.Invoke();
			}
		}

		#endregion

		#region -- Online Help --

		/// <summary>
		///     Apply new profiling data to the displayed help file after the browser finishes loading it.
		/// </summary>
		private void ApplyHelpProfiling()
		{
			var helpMapper = Conversation?.Device.HelpSource as IWebHelpMapper;
			if (null == helpMapper)
			{
				return;
			}

			try
			{
				var ditaVals = helpMapper.GetProfilingData();
				if (ditaVals.HasValue)
				{
					_dispatcher.BeginInvoke(() =>
					{
						try
						{
							EvalTrigger?.Invoke(this, ditaVals.Value.Item1, ditaVals.Value.Item2);
						}
						catch (COMException aException)
						{
							// Eat COMExceptions because they likely mean a JavaScript incompatibility.
							_log.Error("Help profiling error.", aException);
						}
						catch (InvalidOperationException aInvalidOpException)
						{
							// This exception usually means the document hasn't been loaded yet.
							_log.Error("Help profiling error.", aInvalidOpException);
						}

						// Applying profiling can cause the previous scroll position to be wrong,
						// so re-navigate to the same URL again. As long as _redoHelpProfiling
						// remains set to false, it will not trigger an infinite loop.
						OnPropertyChanged(nameof(HelpUrl));
					});
				}
			}
			catch (Exception aException)
			{
				_log.Error("Error while profiling help.", aException);
			}
		}


		/// <summary>
		///     Update command/setting VM help URLs after repopulating them, since the
		///     repopulation process discards pre-existing instances.
		/// </summary>
		private void RefreshHelpUrls() => Task.Factory.StartNew(() =>
		{
			try
			{
				var helpMapper = Conversation?.Device.HelpSource as IWebHelpMapper;
				if (null != helpMapper)
				{
					// This does not need to be done on the UI thread because the
					// command/setting help URL properties are not bound to views anywhere.
					ItemList?.UpdateHelpUrls(helpMapper);
				}
			}
			catch (Exception aException)
			{
				_log.Error("Error refreshing command/setting help URLs.", aException);
			}
		});

		#endregion

		#region -- Data --

		// Members related to loading HTML help files.
		private class BrowserDocumentEventArgs : EventArgs
		{
			#region -- Public Methods & Properties --

			public HtmlDocument Document { get; set; }

			#endregion
		}

		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Commands tab");
		private readonly UnitOfMeasure _masterUnitOfMeasure = UnitOfMeasure.Data;

		// This logger is not static so that subclasses can override it meaningfully.
		private ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private readonly IDispatcher _dispatcher = DispatcherHelper.Dispatcher;
		private ZaberPortFacade _portFacade;
		private Conversation _conversation;
		private CommandDisplayMode _filterMode = CommandDisplayMode.BASIC;
		private CommandInfoList _itemList;
		private string _helpUrl = string.Empty;
		private string _hideHelpButtonLabel;
		private readonly GridLengthConverter _gridLengthConverter = new GridLengthConverter();
		private ObservableCollection<string> _autocompleteList;
		private string _textFilter;
		private bool _isHelpVisible;
		private string _sendTextError;

		private bool _showingDefaultHelp;
		private RelayCommand _documentLoadedCommand;
		private string _lastSelectedCommandName;

		#endregion
	}
}

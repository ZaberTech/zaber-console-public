﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Input;
using Zaber;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.CommandList
{
	/// <summary>
	///     Base class for command or setting list ViewModels.
	/// </summary>
	public abstract class CommandInfoList : ObservableObject
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Initializes class member data.
		/// </summary>
		/// <param name="aIsAsciiConnection">Whether the current connection is ASCII or Binary.</param>
		public CommandInfoList(bool aIsAsciiConnection)
		{
			IsAsciiConnection = aIsAsciiConnection;
		}

		#endregion

		#region -- Public events and types  --

		/// <summary>
		///     Fired when the enter button is pressed on a command.
		/// </summary>
		public event EventHandler OnRowInvoked;


		/// <summary>
		///     Argument class for the OnRowInvoked event.
		/// </summary>
		public class RowEventArgs : EventArgs
		{
			#region -- Public Methods & Properties --

			public CommandGridRowBaseVM SelectedRow { get; set; }

			#endregion
		}

		#endregion

		#region -- Public API --

		/// <summary>
		///     Clears all content from the list and sets the currently selected row to null.
		/// </summary>
		public virtual void Clear()
		{
			SelectedRow = null;
			Rows.Clear();
		}


		/// <summary>
		///     Called when a master unit of measure changes or repopulation occurs.
		///     Subclasses must implement this.
		/// </summary>
		/// <param name="aNewUnit">New unit of measure to apply to all items.</param>
		public abstract void ChangeAllUnits(UnitOfMeasure aNewUnit);


		/// <summary>
		///     Helper to filter commands according to current display mode.
		/// </summary>
		/// <param name="aFilterMode">Enumeration value controlling command filter behavior.</param>
		public virtual void SetViewMode(CommandDisplayMode aFilterMode)
		{
			switch (aFilterMode)
			{
				default: // Deliberate fall-through.
				case CommandDisplayMode.BASIC:
					foreach (var row in Rows)
					{
						row.Visible = CommandVisibility.BASIC == row.VisibilityLevel;
					}

					break;
				case CommandDisplayMode.ADVANCED:
					foreach (var row in Rows)
					{
						row.Visible = CommandVisibility.NEVER != row.VisibilityLevel;
					}

					break;
				case CommandDisplayMode.FAVORITES:
					foreach (var row in Rows)
					{
						row.Visible = row.Favorited && (CommandVisibility.NEVER != row.VisibilityLevel);
					}

					break;
			}
		}


		public virtual void SetViewMode(CommandDisplayMode aFilterMode, string aTextFilter)
		{
			SetViewMode(aFilterMode);

			if (!string.IsNullOrEmpty(aTextFilter))
			{
				foreach (var row in Rows)
				{
					row.Visible &= row.Name.StartsWith(aTextFilter);
				}
			}
		}


		/// <summary>
		///     Update help links for all list items in this control.
		/// </summary>
		/// <param name="aHelpMapper">A source for command or setting help links.</param>
		public abstract void UpdateHelpUrls(IDeviceHelp aHelpMapper);


		/// <summary>
		///     Whether the current connection is ASCII or Binary mode. Used for filtering
		///     commands and settings by protocol.
		/// </summary>
		public bool IsAsciiConnection { get; }

		#endregion

		#region -- View data --

		/// <summary>
		///     The contents of the view.  This may include binary commands
		///     and/or settings, but they will not be grouped and displayed by default.
		/// </summary>
		public ObservableCollection<CommandGridRowBaseVM> Rows
		{
			get => _rows;
			set
			{
				// if resetting the list, remove old event handlers
				if (null != _rows)
				{
					_rows.CollectionChanged -= Rows_CollectionChanged;
					foreach (var row in _rows)
					{
						row.OnFavoriteChanged -= OnItemFavoriteStateChanged;
						row.BeforeCommand -= OnBeforeCommand;
					}
				}

				Set(ref _rows, value, nameof(Rows));

				// Attach new event handlers.
				if (null != _rows)
				{
					_rows.CollectionChanged += Rows_CollectionChanged;
					Rows_CollectionChanged(_rows,
										   new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

					// Apply saved favorites.
					foreach (var row in _rows)
					{
						row.Favorited = IsFavorited(row);
					}
				}
			}
		}


		/// <summary>
		///     The currently selected command or heading in the view, or null if an
		///     element is not selected.
		/// </summary>
		public CommandGridRowBaseVM SelectedRow
		{
			get => _selectedRow;
			set => Set(ref _selectedRow, value, nameof(SelectedRow));
		}


		/// <summary>
		///     Fires the OnEnterPressed event.
		/// </summary>
		public ICommand InvokeRowCommand => new RelayCommand(row =>
		{
			OnRowInvoked?.Invoke(
				this,
				new RowEventArgs
				{
					SelectedRow = (CommandGridRowBaseVM) row
				});
		});

		#endregion

		#region -- Protected API --

		/// <summary>
		///     Subclasses must implement this to determine whether a given item was favorited
		///     in the user settings.
		/// </summary>
		/// <param name="aItem">Item to check.</param>
		/// <returns>True if the item was starred in the previous session.</returns>
		protected abstract bool IsFavorited(CommandGridRowBaseVM aItem);


		/// <summary>
		///     Subclasses must implement this to update user settings when an item's favorite
		///     button is clicked.
		/// </summary>
		/// <param name="aItem">Item to save the new favorited state on.</param>
		protected abstract void OnItemFavoriteStateChanged(CommandGridRowBaseVM aItem);


		/// <summary>
		///     Invoked when the <see cref="Rows" /> collection is added to, removed from
		///     or reset. Ensures that all ASCII command elements have the appropriate event
		///     handlers associated with them and calls the method to group them.
		/// </summary>
		/// <param name="aSender">Unused.</param>
		/// <param name="aArgs">Information associated with the collection change.</param>
		protected virtual void Rows_CollectionChanged(object aSender, NotifyCollectionChangedEventArgs aArgs)
		{
			//all commands in Rows should have the ItemStarred event handler
			if (NotifyCollectionChangedAction.Reset == aArgs.Action)
			{
				//reset event - sender should be new list of rows
				foreach (var row in Rows)
				{
					row.OnFavoriteChanged -= OnItemFavoriteStateChanged;
					row.OnFavoriteChanged += OnItemFavoriteStateChanged;
					row.BeforeCommand -= OnBeforeCommand;
					row.BeforeCommand += OnBeforeCommand;
				}
			}
			else
			{
				if (null != aArgs.OldItems)
				{
					foreach (ASCIICommandInfoVM cmd in aArgs.OldItems)
					{
						cmd.OnFavoriteChanged -= OnItemFavoriteStateChanged;
						cmd.BeforeCommand -= OnBeforeCommand;
					}
				}

				if (null != aArgs.NewItems)
				{
					foreach (ASCIICommandInfoVM cmd in aArgs.NewItems)
					{
						cmd.OnFavoriteChanged -= OnItemFavoriteStateChanged;
						cmd.OnFavoriteChanged += OnItemFavoriteStateChanged;
						cmd.BeforeCommand -= OnBeforeCommand;
						cmd.BeforeCommand += OnBeforeCommand;
					}
				}
			}
		}

		#endregion

		#region -- Private functionality --

		// Callback for the favorite state changes - invokes the protected version.
		private void OnItemFavoriteStateChanged(object aSender, EventArgs aArgs)
		{
			var vm = (CommandGridRowBaseVM) aSender;
			OnItemFavoriteStateChanged(vm);
		}


		// Invoked before a command is sent.
		private void OnBeforeCommand(object sender, EventArgs e)
		{
			// Clear the error states of all command rows.
			foreach (var row in Rows)
			{
				row.ToolTip = null;
			}
		}

		#endregion

		#region -- Data --

		private CommandGridRowBaseVM _selectedRow;
		private ObservableCollection<CommandGridRowBaseVM> _rows;

		#endregion
	}
}

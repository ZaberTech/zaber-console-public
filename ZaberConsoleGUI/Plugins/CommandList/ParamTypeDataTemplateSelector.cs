﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace ZaberConsole.Plugins.CommandList
{
	/// <summary>
	///     Selects which data template to display in the command list for each nodes in an
	///     ASCII command.
	///     <seealso cref="ASCIICommandNodeVM" />
	///     <seealso cref="ASCIICommandInfoVM" />
	///     <seealso cref="CommandTreeVM" />
	/// </summary>
	internal class ParamTypeDataTemplateSelector : DataTemplateSelector
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Selects the appropriate UI element for a single <see cref="ASCIICommandNodeVM" />.
		///     This could be a text box for a parameter or any other framework element.
		/// </summary>
		/// <param name="item">The command node to be represented in the command list.</param>
		/// <param name="container">
		///     The <see cref="FrameworkElement" /> to contain the new UI
		///     element.
		/// </param>
		/// <returns>The <see cref="DataTemplate" /> for the command node.</returns>
		public override DataTemplate SelectTemplate(object item, DependencyObject container)
		{
			var element = container as FrameworkElement;

			if ((element != null) && (item != null) && item is ASCIICommandNodeVM)
			{
				var nodeVM = item as ASCIICommandNodeVM;

				if (null == nodeVM.NodeInfo.ParamType)
				{
					//literal node (TextBlock)
					return element.FindResource("literalNodeTemplate") as DataTemplate;
				}

				if (nodeVM.NodeInfo.ParamType.IsEnumeration)
				{
					//enum node (ComboBox)
					return element.FindResource("enumParamTemplate") as DataTemplate;
				}

				//anything else uses a text box
				return element.FindResource("textParamTemplate") as DataTemplate;
			}

			throw new Exception("Invalid data type for ParamTypeDataSelector");
		}

		#endregion
	}
}

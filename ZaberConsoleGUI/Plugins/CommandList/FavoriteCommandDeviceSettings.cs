﻿using System;
using System.Collections.Generic;
using Zaber.Application;

namespace ZaberConsole.Plugins.CommandList
{
	/// <summary>
	///     Used to serialize the user's favorite commands for one device to the user settings.
	/// </summary>
	[Serializable]
	public class FavoriteCommandDeviceSettings : PerDeviceSettings
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Favorited ASCII commands, stored by CommandName string representation.
		/// </summary>
		public List<string> FavoriteAsciiCommands { get; set; } = new List<string>();

		/// <summary>
		///     List of ASCII command groups that are expanded.
		/// </summary>
		public List<string> ExpandedAsciiGroups { get; set; } = new List<string>();

		/// <summary>
		///     Favorited ASCII settings, stored by string representation.
		/// </summary>
		public List<string> FavoriteAsciiSettings { get; set; } = new List<string>();

		/// <summary>
		///     List of ASCII setting groups that are expanded.
		/// </summary>
		public List<string> ExpandedAsciiSettingGroups { get; set; } = new List<string>();

		/// <summary>
		///     Favorited binary commands, stored by command number representation.
		/// </summary>
		public List<int> FavoriteBinaryCommands { get; set; } = new List<int>();

		/// <summary>
		///     Favorited binary settings, stored by setting number.
		/// </summary>
		public List<int> FavoriteBinarySettings { get; set; } = new List<int>();

		#endregion
	}
}

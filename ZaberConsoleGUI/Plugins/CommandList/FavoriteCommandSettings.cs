﻿using System;
using Zaber;
using Zaber.Application;

namespace ZaberConsole.Plugins.CommandList
{
	/// <summary>
	///     Used to save and restore user's favorited commands across sessions.
	/// </summary>
	[Serializable]
	public class FavoriteCommandSettings : PerDeviceSettingsCollection<FavoriteCommandDeviceSettings>
	{
		#region -- Public Methods & Properties --

		public FavoriteCommandDeviceSettings FindOrCreateSettings(DeviceIdentity aDevice,
																  FavoriteSettingsKeyMode aKeyMode)
		{
			var mask = DeviceIdentityMask.All;

			switch (aKeyMode)
			{
				case FavoriteSettingsKeyMode.BySerialOrType:
					mask = DeviceIdentityMask.SerialNumber
					   | DeviceIdentityMask.DeviceId
					   | DeviceIdentityMask.PeripheralId;
					break;

				case FavoriteSettingsKeyMode.ByType:
					mask = DeviceIdentityMask.DeviceId | DeviceIdentityMask.PeripheralId;
					break;

				case FavoriteSettingsKeyMode.Global:
				default: // Deliberate fall-through.
					mask = DeviceIdentityMask.None;
					break;
			}

			return base.FindOrCreateSettings(aDevice, mask);
		}

		#endregion
	}
}

﻿using Zaber;

namespace ZaberConsole.Plugins.CommandList
{
	/// <summary>
	///     Functionality common to CommandInfoVM and SettingInfoVM.
	/// </summary>
	public abstract class MeasurementVM : CommandGridRowBaseVM
	{
		#region -- Public Methods & Properties --

		#region -- Setup --

		/// <summary>
		///     Create a new instance.
		/// </summary>
		/// <param name="aConversation">
		///     The conversation to query for step size.
		/// </param>
		protected MeasurementVM(Conversation aConversation)
		{
			Conversation = aConversation;
			UnitOfMeasure = UnitOfMeasure.Data;
		}

		#endregion

		#endregion

		#region -- Public API --

		/// <summary>
		///     Gets the conversation around the device that this view represents.
		/// </summary>
		public Conversation Conversation { get; }


		public abstract UnitOfMeasure RequestUnit { get; }


		/// <summary>
		///     The unit of measure to convert the raw data to.
		/// </summary>
		public UnitOfMeasure UnitOfMeasure
		{
			get => SelectedUnit as UnitOfMeasure;
			set => SelectedUnit = value;
		}


		/// <summary>
		///     Perform whatever the default action for this item is.
		/// </summary>
		public virtual void Invoke()
		{
		}

		#endregion

		#region -- Protected API --

		/// <summary>
		///     Check if the response was an error that means the command or setting
		///     is not supported by the device.
		/// </summary>
		/// <param name="aTopic">Contains an error response.</param>
		/// <returns>
		///     True if the command or setting is not supported, otherwise
		///     false.
		/// </returns>
		internal static bool IsInvalidCommandOrSetting(ConversationTopic aTopic)
		{
			var isInvalidCommandOrSetting =
				(aTopic.Response != null)
			&& (((int) aTopic.Response.NumericData == (int) ZaberError.CommandInvalid)
			|| ((int) aTopic.Response.NumericData == (int) ZaberError.SettingInvalid));
			return isInvalidCommandOrSetting;
		}


		/// <summary>
		///     Check if the response is an error.
		/// </summary>
		/// <param name="aSubTopic">Contains the response.</param>
		/// <returns>True if the response is an error, otherwise false.</returns>
		internal static bool IsErrorResponse(ConversationTopic aSubTopic)
			=> (aSubTopic.Response != null) && (aSubTopic.Response.Command == Command.Error);


		/// <summary>
		///     Format a data value in another unit of measure.
		/// </summary>
		/// <param name="aRawData">The raw data value.</param>
		/// <returns>
		///     A formatted number in the current unit of measure.
		/// </returns>
		protected string FormatData(decimal aRawData, CommandInfo aCommand = null)
		{
			// Enable UOM for single axis devices. 
			// The controller conversation contains no UOM information.
			if (Conversation.Axes.Count == 1)
			{
				return Conversation.Axes[0].Device.FormatData(aRawData, UnitOfMeasure, aCommand);
			}

			return Conversation.Device.FormatData(aRawData, UnitOfMeasure, aCommand);
		}


		protected static string FormatResponseError(ConversationTopic aTopic)
		{
			if (aTopic.IsValid)
			{
				return "";
			}

			if (aTopic.ReplacementResponse != null)
			{
				return $"Request was replaced by {aTopic.ReplacementResponse.Command} command.";
			}

			if (aTopic.ZaberPortError != ZaberPortError.None)
			{
				return $"{aTopic.ZaberPortError} error occurred.";
			}

			if ((aTopic.Response != null) && (aTopic.Response.Command == Command.Error))
			{
				if (IsInvalidCommandOrSetting(aTopic))
				{
					return "";
				}

				var errorCode = (ZaberError) (int) aTopic.Response.NumericData;
				var explanation = errorCode.GetDescription();
				if (string.IsNullOrEmpty(explanation))
				{
					return $"Error occurred: {errorCode}.";
				}

				return $"Error occurred: {errorCode}. {explanation}";
			}

			return "Invalid response.";
		}

		#endregion
	}
}

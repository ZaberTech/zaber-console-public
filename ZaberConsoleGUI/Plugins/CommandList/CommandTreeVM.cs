﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Input;
using Zaber;
using ZaberConsole.Properties;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.CommandList
{
	/// <summary>
	///     Heirarchical view model for displaying and sending ASCII commands.
	/// </summary>
	public class CommandTreeVM : CommandInfoList
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor - initializes base class and initializes favorited
		///     commands and expanded groups from user settings.
		/// </summary>
		/// <param name="aIsAsciiConnection">Whether the current connection is ASCII or Binary.</param>
		/// <param name="aDeviceIdentity">
		///     The identity of the device whose commands
		///     are being presented, for the purposes of retrieving and storing favorite
		///     commands for this device. Pass null to skip using stored favorites.
		/// </param>
		public CommandTreeVM(bool aIsAsciiConnection, DeviceIdentity aDeviceIdentity = null)
			: base(aIsAsciiConnection)
		{
			// Set up saved favorite command settings.
			var settings = Settings.Default.CommandListFavorites;
			var id = aDeviceIdentity;
			if (null != id)
			{
				_favoriteSettings =
					settings.FindOrCreateSettings(id, Settings.Default.CommandsAndSettingsFavoriteKeyMode);
			}
		}


		/// <summary>
		///     Sets the "mode" of the view, which determines the commands that can be seen
		///     by the user, and updates the UI.
		/// </summary>
		/// <param name="aMode">The desired <see cref="CommandDisplayMode" /> to view.</param>
		/// <seealso
		///     cref="SetViewModeGrouped(CommandDisplayMode, System.Collections.Generic.IEnumerable{ZaberConsole.Plugins.CommandList.CommandGridRowBaseVM})" />
		public override void SetViewMode(CommandDisplayMode aMode) => SetViewModeGrouped(aMode, GroupedRows);


		public override void SetViewMode(CommandDisplayMode aMode, string textFilter)
			=> SetViewModeGrouped(aMode, GroupedRows, textFilter);


		// Called when the master units change - updates the units on all
		// commands or settings.
		public override void ChangeAllUnits(UnitOfMeasure aNewUnit)
		{
			var derivative = null == aNewUnit ? UnitOfMeasure.Data : aNewUnit.Derivative ?? UnitOfMeasure.Data;
			var secondDerivative = derivative.Derivative ?? UnitOfMeasure.Data;
			foreach (MeasurementVM cmdView in Rows)
			{
				var asciiCmd = cmdView as ASCIICommandInfoVM;
				if (null != asciiCmd)
				{
					foreach (var nodeView in asciiCmd.Nodes)
					{
						if (null != nodeView.RequestUnit)
						{
							// Master UOMs are always units of position, so we don't need to
							// handle all possible combinations here.
							switch (nodeView.RequestUnit.MeasurementType)
							{
								case MeasurementType.Position:
									nodeView.SelectedUnit = aNewUnit;
									break;
								case MeasurementType.Velocity:
									nodeView.SelectedUnit = derivative;
									break;
								case MeasurementType.Acceleration:
									nodeView.SelectedUnit = secondDerivative;
									break;
								case MeasurementType.Other:
									break;
								case MeasurementType.Current:
									break;
								case MeasurementType.CurrentAC:
									break;
								case MeasurementType.Force:
									break;
								default:
									throw new InvalidEnumArgumentException("Unrecognized MeasurementType.");
							}
						}
					}
				}
				else if (null != cmdView.RequestUnit)
				{
					// Master UOMs are always units of position, so we don't need to
					// handle all possible combinations here.
					switch (cmdView.RequestUnit.MeasurementType)
					{
						case MeasurementType.Position:
							cmdView.UnitOfMeasure = aNewUnit;
							break;
						case MeasurementType.Velocity:
							cmdView.UnitOfMeasure = derivative;
							break;
						case MeasurementType.Acceleration:
							cmdView.UnitOfMeasure = secondDerivative;
							break;
						case MeasurementType.Other:
							break;
						case MeasurementType.Current:
							break;
						case MeasurementType.CurrentAC:
							break;
						case MeasurementType.Force:
							break;
						default:
							throw new InvalidEnumArgumentException("Unrecognized MeasurementType.");
					}
				}
			}

			OnPropertyChanged(nameof(Rows));
		}


		/// <inheritdoc cref="CommandInfoList.UpdateHelpUrls" />
		public override void UpdateHelpUrls(IDeviceHelp aHelpMapper)
		{
			foreach (var item in Rows.OfType<ASCIICommandInfoVM>())
			{
				// get first literal nodes in the command - a parameter entry will not
				// be included in a protocol manual heading
				var firstLiterals = item.CommandInfo.Nodes.Select(node => node.NodeText).ToList();
				var newUrl = aHelpMapper.GetCommandHelpPath(string.Join(" ", firstLiterals));

				if (!string.IsNullOrEmpty(newUrl))
				{
					item.HelpUrl = newUrl;
				}
			}
		}


		/// <summary>
		///     A collection which has been filtered to only include ASCII commands.
		///     <see cref="CommandGroupVM" /> objects are included to serve as group
		///     headings.
		/// </summary>
		public ObservableCollection<CommandGridRowBaseVM> GroupedRows
		{
			get => _groupedrows;
			private set => Set(ref _groupedrows, value, nameof(GroupedRows));
		}


		/// <summary>
		///     Command fired when the selected element in the view is changed.
		/// </summary>
		public ICommand ItemChangedCommand => new RelayCommand(SelectedItemChanged);

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Invoked when a command is favorited by the user. Toggles the favorited state.
		/// </summary>
		/// <param name="aSender">The ASCII command element to favorite or unfavorite.</param>
		/// <param name="aArgs">Unused.</param>
		protected override void OnItemFavoriteStateChanged(CommandGridRowBaseVM aItem)
		{
			var vm = aItem as ASCIICommandInfoVM;
			if ((null != vm) && (null != _favoriteSettings))
			{
				_favoriteSettings.FavoriteAsciiCommands.Remove(vm.CommandName);
				if (vm.Favorited)
				{
					_favoriteSettings.FavoriteAsciiCommands.Add(vm.CommandName);
				}
			}
		}


		protected override bool IsFavorited(CommandGridRowBaseVM aItem)
		{
			var cmd = aItem as ASCIICommandInfoVM;
			if ((null != cmd) && (null != _favoriteSettings))
			{
				return _favoriteSettings.FavoriteAsciiCommands.Contains(cmd.CommandName);
			}

			return false;
		}


		/// <summary>
		///     Invoked when the <see cref="Rows" /> collection is added to, removed from
		///     or reset. Ensures that all ASCII command elements have the appropriate event
		///     handlers associated with them and calls the method to group them.
		/// </summary>
		/// <param name="aSender">Unused.</param>
		/// <param name="aArgs">Information associated with the collection change.</param>
		protected override void Rows_CollectionChanged(object aSender, NotifyCollectionChangedEventArgs aArgs)
		{
			base.Rows_CollectionChanged(aSender, aArgs);
			GroupedRows = GroupCommands(Rows.OfType<ASCIICommandInfoVM>());
		}


		// Called when a group property changes - used to track the group expanded/collapsed state in settings.
		private void Group_PropertyChanged(object aGroup, PropertyChangedEventArgs aArgs)
		{
			var vm = aGroup as CommandGroupVM;
			var settings = _favoriteSettings;
			if ((null != vm) && (null != settings))
			{
				settings.ExpandedAsciiGroups.Remove(vm.HeaderText);
				if (vm.IsExpanded)
				{
					settings.ExpandedAsciiGroups.Add(vm.HeaderText);
				}
			}
		}


		/// <summary>
		///     Sets the <see cref="SelectedRow" /> property of the view.
		/// </summary>
		/// <param name="aRow">The newly selected ASCII command or group object.</param>
		private void SelectedItemChanged(object aRow)
		{
			if (aRow is CommandGroupVM)
			{
				SelectedRow = aRow as CommandGroupVM;
			}
			else if (aRow is ASCIICommandInfoVM)
			{
				SelectedRow = aRow as ASCIICommandInfoVM;
			}
			else
			{
				SelectedRow = null;
			}
		}


		/// <summary>
		///     Recursive helper method for the <see cref="SetViewMode(CommandDisplayMode)" />
		///     function.  Sets the visibilities of the commands in a collection depending on
		///     the desired view mode.
		/// </summary>
		/// <param name="aMode">The desired <see cref="CommandDisplayMode" /> to view.</param>
		/// <param name="aRows">The root collection of commands to set visible or invisible.</param>
		private bool SetViewModeGrouped(CommandDisplayMode aMode, IEnumerable<CommandGridRowBaseVM> aRows,
										string aTextFilter = "")
		{
			// condition on which the row will be displayed (parameter is the visibility
			// level of the row)
			Predicate<CommandGridRowBaseVM> rowCondition;
			switch (aMode)
			{
				case CommandDisplayMode.BASIC:
					rowCondition = row => CommandVisibility.BASIC == row.VisibilityLevel;
					break;
				case CommandDisplayMode.ADVANCED:
					rowCondition = row => CommandVisibility.NEVER != row.VisibilityLevel;
					break;
				case CommandDisplayMode.FAVORITES:
					rowCondition = row => row.Favorited && (CommandVisibility.NEVER != row.VisibilityLevel);
					break;
				default:
					rowCondition = row => true;
					break;
			}

			var groupVisible = false;
			foreach (var row in aRows)
			{
				groupVisible |= isRowVisible(aMode, row, rowCondition, aTextFilter);
			}

			return groupVisible;
		}


		private bool isRowVisible(CommandDisplayMode aMode, CommandGridRowBaseVM aRow,
								  Predicate<CommandGridRowBaseVM> aRowCondition, string aTextFilter)
		{
			if (aRow is CommandGroupVM)
			{
				aRow.Visible = SetViewModeGrouped(aMode, (aRow as CommandGroupVM).Children, aTextFilter);
			}
			else
			{
				aRow.Visible = aRowCondition(aRow);
			}

			if (!string.IsNullOrEmpty(aTextFilter))
			{
				if (aRow is CommandGroupVM)
				{
					aRow.Visible = (aRow as CommandGroupVM).HeaderText.StartsWith(aTextFilter.Split()[0]);
					(aRow as CommandGroupVM).IsExpanded = aRow.Visible;
				}
				else
				{
					aRow.Visible = !rowShouldGetFiltered(aRow, aTextFilter);
				}
			}
			else if (aRow is CommandGroupVM)
			{
				(aRow as CommandGroupVM).IsExpanded = false;
			}

			return aRow.Visible;
		}


		private bool rowShouldGetFiltered(CommandGridRowBaseVM aRow, string aTextFilter)
		{
			var rowArgs = aRow.Name.Split();
			var textFilterArgs = aTextFilter.Split();

			if (rowArgs.Length < textFilterArgs.Length)
			{
				return true;
			}

			for (var i = 0; i < textFilterArgs.Length; ++i)
			{
				if (Regex.IsMatch(rowArgs[i], "^\\{.*?\\}$"))
				{
					continue;
				}

				if (((1 < textFilterArgs.Length)
				 && (i < (textFilterArgs.Length - 1))
				 && !rowArgs[i].Equals(textFilterArgs[i]))
				|| !rowArgs[i].StartsWith(textFilterArgs[i]))
				{
					return true;
				}
			}

			return false;
		}


		/// <summary>
		///     Takes a list of ASCII commands and constructs headings if some commands have
		///     the same first nodes. Used to set the <see cref="GroupedRows" /> property which
		///     is ultimately seen in the view.
		/// </summary>
		/// <param name="aCommands">The ASCII commands to group.</param>
		/// <returns>A grouped collection of ASCII commands and headings.</returns>
		private ObservableCollection<CommandGridRowBaseVM> GroupCommands(IEnumerable<ASCIICommandInfoVM> aCommands)
		{
			var newCollection = new ObservableCollection<CommandGridRowBaseVM>();

			var groups = aCommands.GroupBy(cmd => cmd.CommandInfo.Nodes.First().NodeText)
							   .ToDictionary(grp => grp.Key, grp => grp.ToList());

			foreach (var group in groups)
			{
				// Group commands if a threshold count has been reached.
				if (group.Value.Count >= NUM_COMMANDS_BEFORE_GROUP)
				{
					// Group
					var newGroup = new CommandGroupVM();
					newGroup.Children = new ObservableCollection<CommandGridRowBaseVM>(group.Value);
					newGroup.HeaderText = group.Key;

					// Restore expanded state from settings.
					newGroup.IsExpanded = _favoriteSettings?.ExpandedAsciiGroups.Contains(newGroup.HeaderText) ?? false;

					newGroup.PropertyChanged += Group_PropertyChanged;
					newCollection.Add(newGroup);
				}
				else
				{
					// Add as individual nodes.
					foreach (var node in group.Value)
					{
						newCollection.Add(node);
					}
				}
			}

			return newCollection;
		}

		#endregion

		#region -- Data --

		private const int NUM_COMMANDS_BEFORE_GROUP = 3;
		private readonly FavoriteCommandDeviceSettings _favoriteSettings;

		private IDispatcher _dispatcher = DispatcherHelper.Dispatcher;
		private ObservableCollection<CommandGridRowBaseVM> _groupedrows;

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows.Input;
using log4net;
using Zaber;
using Zaber.Threading;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.CommandList
{
	/// <summary>
	///     Represents one row in the <see cref="CommandTree" /> view.
	/// </summary>
	public class ASCIICommandInfoVM : CommandInfoVM
	{
		#region -- Events --

		/// <summary>
		///     Invoked when the user clicks the Send button on a command.
		/// </summary>
		public event EventHandler OnSendClicked;

		#endregion

		#region -- Setup --

		/// <summary>
		///     Constructs a view model for an ASCIICommandInfo object.
		///     <seealso cref="CommandInfoVM" />
		/// </summary>
		/// <param name="aCommandInfo">the command to represent with this view.</param>
		/// <param name="aConversation">
		///     The Conversation to which to send the
		///     command represented by this view.
		/// </param>
		/// <param name="aTimer">Optional timeout for awaiting command responses.</param>
		public ASCIICommandInfoVM(ASCIICommandInfo aCommandInfo, Conversation aConversation,
								  TimeoutTimer aTimeoutTimer = null)
			: base(aCommandInfo, aConversation, aTimeoutTimer)
		{
			_command = aCommandInfo;

			// Create VM objects from all nodes
			_nodes = new ObservableCollection<ASCIICommandNodeVM>();
			foreach (var node in aCommandInfo.Nodes)
			{
				var count = 1;
				if (node.ParamType?.Arity != null)
				{
					// If arity has a specific value, we repeat the param
					// node that many times.
					count = node.ParamType.Arity.Value;
				}

				while (count > 0)
				{
					var uiNode = new ASCIICommandNodeVM(node);

					// If the arity is variable we create only one UI node at first
					// but add a callback for when the value changes.
					if ((null != node.ParamType) && !node.ParamType.Arity.HasValue)
					{
						uiNode.PropertyChanged += OnVariableArityParamChanged;
					}

					_nodes.Add(uiNode);
					count--;
				}
			}

			OnSendClicked += SendButtonClicked;
		}


		/// <summary>
		///     Constructs a shallow copy of the object, and shallow copies all
		///     <see cref="ASCIICommandNodeVM" /> objects along with it.
		/// </summary>
		/// <returns>Copies of the ASCIICommandInfoVM object and each of its nodes.</returns>
		public ASCIICommandInfoVM(ASCIICommandInfoVM aOther)
			: base(aOther.CommandInfo, aOther.Conversation, aOther._timeoutTimer)
		{
			var nodeVMs = aOther.Nodes.Select(node => node.Clone());
			Nodes = new ObservableCollection<ASCIICommandNodeVM>(nodeVMs);

			_command = aOther.CommandInfo;

			OnSendClicked += SendButtonClicked;
		}

		#endregion

		#region -- View-Bound Properties --

		/// <summary>
		///     A reference to the ASCIICommandInfo object used by this view model.
		/// </summary>
		public new ASCIICommandInfo CommandInfo
		{
			get => _command;
			set => Set(ref _command, value, nameof(_command));
		}


		/// <summary>
		///     A string representation of the command.
		/// </summary>
		public new string Name => _command.Name;


		/// <summary>
		///     UI elements to be inserted into the tree row.
		/// </summary>
		public ObservableCollection<ASCIICommandNodeVM> Nodes
		{
			get => _nodes;
			set => Set(ref _nodes, value, nameof(Nodes));
		}


		/// <summary>
		///     Fires the event to send this command to the device.
		/// </summary>
		public ICommand SendCommand => new RelayCommand(_ => OnSendClicked?.Invoke(this, null));

		#endregion

		#region -- Functionality --

		/// <summary>
		///     Invoking the row by pressing enter or double-clicking will send the selected command
		///     to the device.
		/// </summary>
		public override void Invoke() => OnSendClicked?.Invoke(this, null);


		/// <summary>
		///     Sends this command to the device using the content of each node as a
		///     parameter.
		/// </summary>
		/// <param name="aSender">The object attempting to send the command to the device.</param>
		/// <param name="aArgs">Data associated with the event that triggered this method. </param>
		private void SendButtonClicked(object aSender, EventArgs aArgs)
		{
			InvokeBeforeCommand();

			// get the entered values for all nodes in the command
			var paramNodes = Nodes
						 .Where(node => (null != node.NodeInfo.ParamType) && !string.IsNullOrEmpty(node.Content))
						 .ToList();
			var commandNodes = paramNodes.Select(node => node.NodeInfo).Distinct().ToList();
			var nodeContents = paramNodes.Select(node => new ParameterData(commandNodes.IndexOf(node.NodeInfo),
																		   node.Content,
																		   node.NodeInfo?.ParamType,
																		   node.SelectedUnit));

			var topic = Request(nodeContents.ToArray());

			if (null != topic)
			{
				var worker = BackgroundWorkerManager.CreateWorker();
				worker.DoWork += WaitForRequest;
				worker.RunWorkerCompleted += RequestCompleted;
				worker.Run(topic);
			}
		}


		/// <summary>
		///     Sends an ASCII command to the device, and waits for a response.
		/// </summary>
		/// <param name="aParams">All parameters required to send the command.</param>
		/// <returns>
		///     The <see cref="ConversationTopic" /> used to send the command if the device does
		///     not respond in time, otherwise null.
		/// </returns>
		private ConversationTopic Request(IEnumerable<ParameterData> aParams)
		{
			var topic = Conversation.StartTopic();

			var cmdFormat = Name;
			var paramsToUse = aParams.ToList();

			if (1 == Conversation.Axes.Count)
			{
				// special handling for single-axis device with UOM
				// calculate data from axis, then send to device
				var nodeIndex = 0;
				var newParams = new List<ParameterData>();
				foreach (var node in paramsToUse)
				{
					// If the user-entered text is a number and has a unit specified, do unit conversion and formatting on it.
					if ((null != node.SourceUnit) && node.TryParseData(out var val))
					{
						var measurement = new Measurement(val, node.SourceUnit);
						var value = Conversation.Device.Axes[0].CalculateExactData(cmdFormat, measurement, nodeIndex);
						newParams.Add(new ParameterData(node.ParameterIndex,
														node.ParamType?.FormatNumeric(value)
													?? ((int) Math.Round(value)).ToString(),
														null,
														null));
					}
					else // No unit or couldn't parse number - pass through unmodified.
					{
						newParams.Add(new ParameterData(node.ParameterIndex, node.Data, node.ParamType, null));
					}

					nodeIndex += 1;
				}

				paramsToUse = newParams;
			}

			_log.InfoFormat("Sending: '{0}' ({1})",
							cmdFormat,
							string.Join(", ",
										paramsToUse.Select(
											pd => (pd.Data + pd.SourceUnit?.Abbreviation) ?? string.Empty)));

			if (Conversation.Device.AreAsciiMessageIdsEnabled)
			{
				Conversation.Device.SendFormatInUnits(cmdFormat, paramsToUse, topic.MessageId);
			}
			else
			{
				Conversation.Device.SendFormatInUnits(cmdFormat, paramsToUse);
			}

			if (topic.Wait(_timeoutTimer))
			{
				ProcessResponse(topic);
				topic = null;
			}
			else
			{
				ToolTip = string.Empty;
			}

			return topic;
		}


		// Invoked when the value of a parameter with variable arity is changed,
		// so we can add or remove a blank control for the user to enter more values.
		private void OnVariableArityParamChanged(object aNodeVM, PropertyChangedEventArgs aArgs)
		{
			var index = -1;
			ASCIICommandNode cmdNode = null;
			if (nameof(ASCIICommandNodeVM.Content) == aArgs.PropertyName)
			{
				if (aNodeVM is ASCIICommandNodeVM node)
				{
					index = _nodes.IndexOf(node);
					cmdNode = node.NodeInfo;
				}
			}

			if ((index < 0) || cmdNode is null)
			{
				return;
			}

			// Approach: Find all adjacent UI nodes that reference the same parameter,
			// and collapse them so all the ones with values are adjacent in the
			// original order, then add one blank one after that group.
			// This code only runs for params without a specific
			// arity so we don't need to worry about capping the quantity.

			// UI nodes referencing the same command node is used to detect
			// adjacent copies of the same parameter.

			while ((index > 0) && (_nodes[index - 1].NodeInfo == cmdNode))
			{
				index--;
			}

			var changed = false;
			var insertOffset = 1; // Index currently points at a relevant node, so insert the blankone later.
			while ((index <= (_nodes.Count - 1)) && (_nodes[index].NodeInfo == cmdNode))
			{
				if (string.IsNullOrEmpty(_nodes[index].Content))
				{
					_nodes.RemoveAt(index);
					changed = true;
				}
				else
				{
					index++;
				}

				insertOffset = 0; // Index now points after a relevant node, so insert the blank here.
			}

			if (!cmdNode.ParamType.Arity.HasValue)
			{
				var newNode = new ASCIICommandNodeVM(cmdNode);
				newNode.PropertyChanged += OnVariableArityParamChanged;
				_nodes.Insert(index + insertOffset, newNode);
				changed = true;
			}

			if (changed)
			{
				OnPropertyChanged(nameof(Nodes));
			}
		}

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private ASCIICommandInfo _command;
		private ObservableCollection<ASCIICommandNodeVM> _nodes;

		#endregion
	}
}

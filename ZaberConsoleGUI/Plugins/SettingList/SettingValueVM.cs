﻿using System;
using System.Windows.Input;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.SettingsList
{
	public class SettingValueVM : ObservableObject
	{
		#region -- Events --

		public event EventHandler OnReadClicked;

		public event EventHandler OnWriteClicked;

		public event EventHandler OnEnterPressed;

		#endregion

		#region -- Public Methods & Properties --

		public string Value
		{
			get => _value;
			set => Set(ref _value, value, nameof(Value));
		}


		public string ReadButtonLabel
		{
			get => _readbuttonLabel;
			set => Set(ref _readbuttonLabel, value, nameof(ReadButtonLabel));
		}


		public string WriteButtonLabel
		{
			get => _writeButtonLabel;
			set => Set(ref _writeButtonLabel, value, nameof(WriteButtonLabel));
		}


		public bool IsWritable
		{
			get => _writable;
			set => Set(ref _writable, value, nameof(IsWritable));
		}


		public bool EnableButtons
		{
			get => _enableButtons;
			set => Set(ref _enableButtons, value, nameof(EnableButtons));
		}


		public ICommand ReadCommand => new RelayCommand(_ => OnReadClicked?.Invoke(this, null));


		public ICommand WriteCommand => new RelayCommand(_ => OnWriteClicked?.Invoke(this, null));


		public ICommand UpdateSettingCommand => new RelayCommand(_ => OnEnterPressed?.Invoke(this, null));

		#endregion

		#region -- Data --

		private string _value = string.Empty;
		private string _readbuttonLabel = "Read";
		private string _writeButtonLabel = "Write";
		private bool _writable = true;
		private bool _enableButtons = true;

		#endregion
	}
}

﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using log4net;
using Zaber;
using Zaber.Telemetry;
using Zaber.Threading;
using ZaberConsole.Plugins.CommandList;

namespace ZaberConsole.Plugins.SettingsList
{
	/// <summary>
	///     ViewModel for one setting in the setting list control. Handles device communication for value changes.
	/// </summary>
	public class SettingInfoVM : MeasurementVM
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Create a new instance.
		/// </summary>
		/// <param name="aSettingInfo">the command to represent with this view.</param>
		/// <param name="aConversation">
		///     The Conversation to which to send the
		///     command represented by this view.
		/// </param>
		/// <param name="aTimer">Optional timeout for awaiting command responses.</param>
		public SettingInfoVM(SettingInfo aSettingInfo, Conversation aConversation, TimeoutTimer aTimer = null)
			: base(aConversation)
		{
			SettingInfo = aSettingInfo;

			var name = aSettingInfo.TextCommand;
			if (null == name)
			{
				if (aSettingInfo.Name.StartsWith("Set "))
				{
					name = aSettingInfo.Name.Substring(4);
				}
				else
				{
					if (aSettingInfo.IsReadOnlySetting && aSettingInfo.Name.StartsWith("Return "))
					{
						name = aSettingInfo.Name.Substring(7);
					}
					else
					{
						name = aSettingInfo.Name;
					}
				}
			}

			Name = name;

			if (null != aConversation)
			{
				var helpMapper = aConversation.Device.HelpSource;

				if (aConversation.Device.Port.IsAsciiMode)
				{
					HelpUrl = helpMapper?.GetSettingHelpPath(name);
				}
				else
				{
					HelpUrl = helpMapper?.GetSettingHelpPath(((int) aSettingInfo.Command).ToString());
				}
			}

			_timeoutTimer = aTimer;
			if (null == _timeoutTimer)
			{
				_timeoutTimer = new TimeoutTimer { Timeout = Timeout.Infinite };
			}

			Data = _dataVM;
			_dataVM.OnReadClicked += OnReadClicked;
			_dataVM.OnWriteClicked += OnWriteClicked;
			_dataVM.OnEnterPressed += OnEnterPressed;

			_dataVM.IsWritable = !(aSettingInfo is ReadOnlySettingInfo);

			Clear();
			ResetButtons();
		}


		/// <summary>
		///     Fired when enter is pressed on a row. Performs the default action, which is to
		///     read or write the setting.
		/// </summary>
		public override void Invoke() => OnEnterPressed(null, null);


		/// <summary>
		///     Sends a request to read this setting with the current data value and
		///     waits for a response or until the <see cref="TimeoutTimer" /> times out.
		/// </summary>
		/// <returns>
		///     null if the response was received, or a valid ConversationTopic if
		///     it timed out.
		/// </returns>
		/// <remarks>
		///     Send the ConversationTopic to <see cref="WaitForResponse" />.
		/// </remarks>
		public ConversationTopic Refresh()
		{
			var topic = Conversation.StartTopic();

			if (Conversation.Device.Port.IsAsciiMode)
			{
				if (Conversation.Device.AreAsciiMessageIdsEnabled)
				{
					Conversation.Device.Send("get " + SettingInfo.TextCommand, topic.MessageId);
				}
				else
				{
					Conversation.Device.Send("get " + SettingInfo.TextCommand);
				}
			}
			else if (SettingInfo.IsReadOnlySetting)
			{
				Conversation.Device.Send(SettingInfo.Command, 0, topic.MessageId);
			}
			else
			{
				Conversation.Device.Send(Command.ReturnSetting, (int) SettingInfo.Command, topic.MessageId);
			}

			if (topic.Wait(_timeoutTimer))
			{
				ProcessResponse(topic);
				topic = null;
			}
			else
			{
				ToolTip = "No response from the device.";
			}

			return topic;
		}


		// <summary>
		// Remove the current value and error text.
		// </summary>
		public void Clear()
		{
			ToolTip = null;
			var conversations = Conversation as ConversationCollection;
			if (conversations == null)
			{
				_dataVM.Value = string.Empty;
			}
			else
			{
				var sb = new StringBuilder();

				using (var topics = conversations.StartTopicCollection())
				{
					for (var i = 0; i < (topics.Count - 1); i++)
					{
						sb.AppendLine();
					}

					topics.Cancel();
				}

				_dataVM.Value = sb.ToString();
			}
		}


		// <summary>
		// Sends a request for this command and waits for a response or until
		// the <see cref="TimeoutTimer"/> times out.
		// </summary>
		// <param name="aDataValue">A string representation of the data value 
		// to request. Empty string is equivalent to 0.</param>
		// <returns>null if the response was received, or a valid 
		// ConversationTopic if it timed out.</returns>
		// <remarks>
		// Send the ConversationTopic to <see cref="WaitForResponse"/>.
		// </remarks>
		public ConversationTopic Request(string aDataValue)
		{
			if (!_dataVM.IsWritable)
			{
				ToolTip = "This setting is read-only.";
				return null;
			}

			decimal parsedValue = 0;
			var isParseSuccessful = true;
			if (aDataValue.Length > 0)
			{
				isParseSuccessful = decimal.TryParse(aDataValue, NumberStyles.Float, CultureInfo.CurrentCulture, out parsedValue);
				if (!isParseSuccessful)
				{
					isParseSuccessful = decimal.TryParse(aDataValue,
														 NumberStyles.Float,
														 CultureInfo.InvariantCulture,
														 out parsedValue);
				}

				if (!isParseSuccessful && !Conversation.Device.Port.IsAsciiMode)
				{
					ToolTip = "Invalid number.";
					return null;
				}
			}
			else if (SettingInfo.DataDescription != null)
			{
				ToolTip = "A data value is required.";
				return null;
			}

			var measurement = new Measurement(parsedValue, UnitOfMeasure);
			var topic = Conversation.StartTopic();

			if (Conversation.Device.Port.IsAsciiMode)
			{
				var fullCommandName = "set " + SettingInfo.TextCommand;

				if ((aDataValue.Length > 0) && isParseSuccessful && (SettingInfo.ParamType?.IsNumeric ?? false))
				{
					if (Conversation.Axes.Count == 1)
					{
						// Special handling for single axis device with UOM: Use axis unit converter.
						var units = Conversation.Device.Axes[0].CalculateExactData(fullCommandName, measurement);

						_log.InfoFormat("Sending: {0} {1}", fullCommandName, units);
						Conversation.Device.SendInUnits(fullCommandName,
														new Measurement(units, UnitOfMeasure.Data),
														topic.MessageId,
														SettingInfo.ParamType);
					}
					else
					{
						_log.InfoFormat("Sending: {0} {1}", fullCommandName, measurement);
						Conversation.Device.SendInUnits(fullCommandName, measurement, topic.MessageId, SettingInfo.ParamType);
					}
				}
				else
				{
					var cmdStr = string.Format(CultureInfo.InvariantCulture,
											   aDataValue.Length > 0 ? "{0} {1}" : "{0}",
											   fullCommandName,
											   aDataValue);

					_log.InfoFormat("Sending: {0}", cmdStr);

					Conversation.Send(cmdStr, topic.MessageId);
				}
			}
			else
			{
				_log.InfoFormat("Sending: {0} {1}", SettingInfo.Command, measurement);
				Conversation.Device.SendInUnits(SettingInfo.Command, measurement, topic.MessageId);
			}

			if (topic.Wait(_timeoutTimer))
			{
				ProcessResponse(topic);

				if (SettingInfo.TextCommand != null)
				{
					_dataVM.Value = aDataValue;
				}

				topic = null;
			}
			else
			{
				_dataVM.WriteButtonLabel = "Writing";
				ToolTip = null;
			}

			return topic;
		}


		// <summary>
		// Wait for a request to complete.
		// </summary>
		// <param name="aTopic">A topic that was returned by <see cref="Request"/>.</param>
		public void WaitForResponse(ConversationTopic aTopic)
		{
			aTopic.Wait();
			ProcessResponse(aTopic);
		}


		/// <summary>
		///     The SettingInfo associated with this VM. May be a ReadOnlySettingInfo.
		/// </summary>
		public SettingInfo SettingInfo { get; }


		/// <summary>
		///     The unmodified name of the command. Settings will start with "Set" or "Return".
		/// </summary>
		public string CommandName => SettingInfo.Name;


		/// <summary>
		///     Currently selected unit of measure for this setting.
		/// </summary>
		public override UnitOfMeasure RequestUnit => SettingInfo.RequestUnit;

		#endregion

		#region -- Non-Public Methods --

		private void OnReadClicked(object aSender, EventArgs aArgs)
		{
			Refresh();
			_eventLog.LogEvent("Read setting", $"{SettingInfo.TextCommand} in {UnitOfMeasure}");
		}


		private void OnWriteClicked(object aSender, EventArgs aArgs)
		{
			ConversationTopic topic = null;
			try
			{
				topic = Request(_dataVM.Value);
			}
			catch (ConversionException e)
			{
				ToolTip = e.Message;
				return;
			}

			if (topic != null)
			{
				var worker = BackgroundWorkerManager.CreateWorker();
				worker.DoWork += WaitForRequest;
				worker.RunWorkerCompleted += RequestCompleted;
				worker.Run(topic);
				_eventLog.LogEvent("Write setting", $"{SettingInfo.TextCommand} = {_dataVM.Value} {UnitOfMeasure}");
			}
		}


		private void OnEnterPressed(object aSender, EventArgs aArgs)
		{
			if (!_dataVM.IsWritable || string.IsNullOrEmpty(_dataVM.Value))
			{
				OnReadClicked(aSender, aArgs);
			}
			else
			{
				OnWriteClicked(aSender, aArgs);
			}
		}


		private void WaitForRequest(object aSender, DoWorkEventArgs aArgs)
		{
			var request = (ConversationTopic) aArgs.Argument;
			WaitForResponse(request);
			aArgs.Result = request;
		}


		private void RequestCompleted(object aSender, RunWorkerCompletedEventArgs aArgs)
		{
			var request = (ConversationTopic) aArgs.Result;
			OnPropertyChanged(nameof(Data));
		}


		private void ResetButtons()
		{
			var deviceAccessLevel = Conversation?.Device?.AccessLevel ?? int.MaxValue;

			_dataVM.ReadButtonLabel = string.Empty;
			if (deviceAccessLevel >= SettingInfo.ReadAccessLevel)
			{
				_dataVM.ReadButtonLabel = "Read";
			}

			_dataVM.IsWritable = true;
			if (SettingInfo.IsReadOnlySetting)
			{
				_dataVM.IsWritable = false;
			}
			else
			{
				if (deviceAccessLevel >= SettingInfo.AccessLevel)
				{
					_dataVM.WriteButtonLabel = "Write";
				}
				else
				{
					_dataVM.WriteButtonLabel = string.Empty;
					_dataVM.IsWritable = false;
				}
			}
		}


		private void ProcessResponse(ConversationTopic aTopic)
		{
			ResetButtons();

			if (aTopic is ConversationTopicCollection topics)
			{
				ProcessResponseCollection(topics);
			}
			else
			{
				_dataVM.Value = FormatResponseData(aTopic, SettingInfo);
				var error = FormatResponseError(aTopic);
				ToolTip = error;
				if (!string.IsNullOrEmpty(error))
				{
					_log.Info("Error response: " + error);
				}
			}

			aTopic.Dispose();
		}


		private void ProcessResponseCollection(ConversationTopicCollection aTopics)
		{
			var isMultipleResponse = false;
			var display = "";
			var error = "";
			var displayBuilder = new StringBuilder();
			var singleDisplayBuilder = new StringBuilder();
			var errorBuilder = new StringBuilder();

			foreach (var subtopic in aTopics)
			{
				var subdisplay = FormatResponseData(subtopic, SettingInfo);
				var suberror = FormatResponseError(subtopic);
				if (displayBuilder.Length == 0)
				{
					display = FormatResponseData(subtopic, SettingInfo);
					error = FormatResponseError(subtopic);
					singleDisplayBuilder.Append(display);
				}
				else
				{
					displayBuilder.AppendLine();
					singleDisplayBuilder.AppendLine();
					isMultipleResponse =
						isMultipleResponse || !subdisplay.Equals(display) || !suberror.Equals(error);
				}

				byte deviceNumber;
				string format;

				if (subtopic.Response == null)
				{
					deviceNumber = 0;
					format = "{1}";
				}
				else
				{
					deviceNumber = subtopic.Response.DeviceNumber;
					format = "{0}: {1}";
				}

				displayBuilder.AppendFormat(format, deviceNumber, subdisplay);

				if (suberror.Length > 0)
				{
					if (errorBuilder.Length == 0)
					{
						errorBuilder.AppendLine("Some requests failed:");
					}
					else
					{
						errorBuilder.AppendLine();
					}

					errorBuilder.AppendFormat(format, deviceNumber, suberror);
				}
			}

			_dataVM.Value = isMultipleResponse ? displayBuilder.ToString() : singleDisplayBuilder.ToString();

			ToolTip = isMultipleResponse ? errorBuilder.ToString() : error;
		}


		private string FormatResponseData(ConversationTopic aTopic, CommandInfo aCommand)
		{
			if (aTopic.IsValid)
			{
				if (aTopic.Response.TextData == null)
				{
					return FormatData(aTopic.Response.NumericData, aCommand);
				}

				if ((SettingInfo.ResponseUnit == null)
				|| ((SettingInfo.ResponseUnit.MeasurementType == MeasurementType.Other)
				&& !SettingInfo.ResponseUnitScale.HasValue))
				{
					return aTopic.Response.TextData;
				}

				return string.Join(" ", aTopic.Response.NumericValues.Select(value => value != null ? FormatData(value.Value, SettingInfo) : "n/a").ToArray());
			}

			if (IsErrorResponse(aTopic) && IsInvalidCommandOrSetting(aTopic))
			{
				return "n/a";
			}

			return "";
		}

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Settings tab");

		private readonly SettingValueVM _dataVM = new SettingValueVM();
		private readonly TimeoutTimer _timeoutTimer;

		#endregion
	}
}

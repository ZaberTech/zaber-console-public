﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Windows.Input;
using log4net;
using Zaber;
using Zaber.PlugIns;
using Zaber.Telemetry;
using ZaberConsole.Plugins.CommandList;
using ZaberConsole.Properties;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.SettingsList
{
	/// <summary>
	///     Top-level ViewModel for the Settings plugin.
	/// </summary>
	[PlugIn(Name = "Settings", Description = "Read and write values from a menu of device settings.")]
	public class SettingListVM : CommandListBaseVM
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor - initializes tomeouts and upgrades settings.
		/// </summary>
		public SettingListVM()
		{
			SetLogger(_log);
			_timeoutTimer.Timeout = DefaultTimeout;
			Settings.Default.SettingGridSplitters = UpgradeWindowSettings(Settings.Default.SettingGridSplitters);
			UpdateHideHelpButton();
		}


		/// <summary>
		///     Command invoked by the Read All button.
		/// </summary>
		public ICommand ReadAllCommand => new RelayCommand(_ =>
		{
			_eventLog.LogEvent("Read all settings");

			foreach (SettingInfoVM vm in ItemList.Rows)
			{
				if (vm.Visible)
				{
					vm.Refresh();
				}
			}
		});

		/// <summary>
		///     Display width of the control panel.
		/// </summary>
		public override string LeftColumnWidth
		{
			get => Settings.Default.SettingGridSplitters.UserMeasurements[0];
			set
			{
				Settings.Default.SettingGridSplitters.UserMeasurements[0] = value;
				OnPropertyChanged(nameof(LeftColumnWidth));
			}
		}


		/// <summary>
		///     Display width of the help panel.
		/// </summary>
		public override string RightColumnWidth
		{
			get => Settings.Default.SettingGridSplitters.UserMeasurements[1];
			set
			{
				Settings.Default.SettingGridSplitters.UserMeasurements[1] = value;
				OnPropertyChanged(nameof(RightColumnWidth));
				UpdateHideHelpButton();
			}
		}

		#endregion

		#region -- Non-Public Methods --

		protected override void OnPortOpened() => FilterMode = Settings.Default.SettingListFilterMode;


		protected override void OnPortClosed()
		{
			Settings.Default.SettingListFilterMode = FilterMode;
			_conversationMap.Clear();
		}


		protected override void Populate()
		{
			if (PortFacade.IsOpen)
			{
				ListSettings();
			}
			else if (null != ItemList)
			{
				ItemList.SelectedRow = null;
				ItemList.Rows.Clear();
			}
		}


		// Grab the appropriate set of settings for the currently selected Conversation.
		private void ListSettings()
		{
			if (Conversation == null)
			{
				return;
			}

			var device = Conversation.Device;

			if (!_conversationMap.TryGetValue(Conversation, out var views))
			{
				views = CreateSettings();
				_conversationMap[Conversation] = views;
			}

			foreach (var view in views)
			{
				var cmd = view.SettingInfo;
				view.VisibilityLevel = IsSettingDisplayed(cmd, device.AccessLevel);
			}

			if (PortFacade.Port.IsAsciiMode)
			{
				ItemList = new SettingTreeVM(PortFacade.Port.IsAsciiMode, Conversation.Device.GetIdentity());
			}
			else
			{
				ItemList = new CommandGridVM(PortFacade.Port.IsAsciiMode, Conversation.Device.GetIdentity());
			}

			//add event handling for item list
			ItemList.PropertyChanged += OnItemListPropertyChanged;
			ItemList.OnRowInvoked += ItemList_OnRowInvoked;

			ItemList.Rows = new ObservableCollection<CommandGridRowBaseVM>(views);
		}


		private List<SettingInfoVM> CreateSettings()
		{
			var device = Conversation.Device;

			var views = device.DeviceType.Commands.Where(cmd => cmd.IsSetting)
						   .Select(cmd => new SettingInfoVM((SettingInfo) cmd, Conversation, _timeoutTimer))
						   .ToList();

			views.Sort((viewA, viewB) => string.CompareOrdinal(viewA.Name, viewB.Name));

			// Populate units of measure.
			foreach (var view in views)
			{
				PopulateUnits(device, view);
			}

			return views;
		}


		private CommandVisibility IsSettingDisplayed(SettingInfo cmd, int aAccessLevel)
		{
			// Settings are displayed if the device's access level matches either the read or write level
			// of the setting.
			if (aAccessLevel < cmd.ReadAccessLevel)
			{
				if (cmd is ReadOnlySettingInfo)
				{
					return CommandVisibility.NEVER;
				}

				// Being able to write a setting implies being able to read it.
				if (aAccessLevel < cmd.AccessLevel)
				{
					return CommandVisibility.NEVER;
				}
			}

			// Do not show commands which protocol does not match port
			if (PortFacade.Port.IsAsciiMode)
			{
				if (null == cmd.TextCommand)
				{
					return CommandVisibility.NEVER;
				}
			}
			else
			{
				if (null != cmd.TextCommand)
				{
					return CommandVisibility.NEVER;
				}
			}

			// Hide optional command if Show All checkbox is not selected
			if (!cmd.IsBasic)
			{
				return CommandVisibility.ADVANCED;
			}

			return CommandVisibility.BASIC;
		}

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Settings tab");

		private const int DefaultTimeout = 500;

		private readonly Dictionary<Conversation, List<SettingInfoVM>> _conversationMap =
			new Dictionary<Conversation, List<SettingInfoVM>>();

		private readonly TimeoutTimer _timeoutTimer = new TimeoutTimer();

		#endregion
	}
}

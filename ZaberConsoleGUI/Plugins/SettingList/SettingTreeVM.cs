﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using Zaber;
using ZaberConsole.Plugins.CommandList;
using ZaberConsole.Properties;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.SettingsList
{
	/// <summary>
	///     Heirarchical view model for displaying and writing ASCII settings.
	/// </summary>
	public class SettingTreeVM : CommandInfoList
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor - initializes base class and initializes favorited
		///     settings and expanded groups from user settings.
		/// </summary>
		/// <param name="aIsAsciiConnection">Whether the current connection is ASCII or Binary.</param>
		/// <param name="aDeviceIdentity">
		///     The identity of the device whose settings
		///     are being presented, for the purposes of retrieving and storing favorite
		///     settings for this device. Pass null to skip using stored favorites.
		/// </param>
		public SettingTreeVM(bool aIsAsciiConnection, DeviceIdentity aDeviceIdentity = null)
			: base(aIsAsciiConnection)
		{
			// Set up saved favorite setting settings. These are shared with the command list.
			var settings = Settings.Default.CommandListFavorites;
			var id = aDeviceIdentity;
			if (null != id)
			{
				_favoriteSettings =
					settings.FindOrCreateSettings(id, Settings.Default.CommandsAndSettingsFavoriteKeyMode);
			}
		}


		// Called when the master units change - updates the units on all
		// commands or settings.
		public override void ChangeAllUnits(UnitOfMeasure aNewUnit)
		{
			var derivative = null == aNewUnit ? UnitOfMeasure.Data : aNewUnit.Derivative ?? UnitOfMeasure.Data;
			var secondDerivative = derivative.Derivative ?? UnitOfMeasure.Data;
			foreach (MeasurementVM cmdView in Rows)
			{
				var newUnit = cmdView.UnitOfMeasure;
				if (null != cmdView.RequestUnit)
				{
					// Master UOMs are always units of position, so we don't need to
					// handle all possible combinations here.
					switch (cmdView.RequestUnit.MeasurementType)
					{
						case MeasurementType.Position:
							newUnit = aNewUnit;
							break;
						case MeasurementType.Velocity:
							newUnit = derivative;
							break;
						case MeasurementType.Acceleration:
							newUnit = secondDerivative;
							break;
						case MeasurementType.Other:
							break;
						case MeasurementType.Current:
							break;
						case MeasurementType.CurrentAC:
							break;
						case MeasurementType.Force:
							break;
						default:
							throw new InvalidEnumArgumentException("Unrecognized MeasurementType.");
					}
				}

				if (newUnit != cmdView.UnitOfMeasure && cmdView.Units.Contains(newUnit))
				{
					cmdView.UnitOfMeasure = newUnit;
				}
			}

			OnPropertyChanged(nameof(Rows));
		}


		/// <inheritdoc cref="CommandInfoList.UpdateHelpUrls" />
		public override void UpdateHelpUrls(IDeviceHelp aHelpMapper)
		{
			foreach (var item in Rows)
			{
				//search for a heading in the protocol with a matching Id to the command
				var newUrl = aHelpMapper.GetSettingHelpPath(item.Name);
				if (!string.IsNullOrEmpty(newUrl))
				{
					item.HelpUrl = newUrl;
				}
			}
		}


		/// <summary>
		///     Sets the "mode" of the view, which determines the commands that can be seen
		///     by the user, and updates the UI.
		/// </summary>
		/// <param name="aMode">The desired <see cref="CommandDisplayMode" /> to view.</param>
		/// <seealso
		///     cref="SetViewModeGrouped(CommandDisplayMode, System.Collections.Generic.IEnumerable{ZaberConsole.Plugins.CommandList.CommandGridRowBaseVM})" />
		public override void SetViewMode(CommandDisplayMode aMode)
		{
			base.SetViewMode(aMode);
			SetViewModeGrouped(aMode, GroupedRows);
		}


		/// <summary>
		///     A collection which has been filtered to only include ASCII settings.
		///     <see cref="CommandGroupVM" /> objects are included to serve as group
		///     headings.
		/// </summary>
		public ObservableCollection<CommandGridRowBaseVM> GroupedRows
		{
			get => _groupedrows;
			private set => Set(ref _groupedrows, value, nameof(GroupedRows));
		}


		/// <summary>
		///     Command fired when the selected element in the view is changed.
		/// </summary>
		public ICommand ItemChangedCommand => new RelayCommand(SelectedItemChanged);

		#endregion

		#region -- Non-Public Methods --

		// Determines if a setting was favorited in the user settings.
		protected override bool IsFavorited(CommandGridRowBaseVM aItem)
		{
			var setting = aItem as SettingInfoVM;
			if ((null != setting) && (null != _favoriteSettings))
			{
				return _favoriteSettings.FavoriteAsciiSettings.Contains(setting.CommandName);
			}

			return false;
		}


		/// <summary>
		///     Invoked when a command is favorited by the user. Toggles the favorited state.
		/// </summary>
		/// <param name="aSender">The ASCII command element to favorite or unfavorite.</param>
		/// <param name="aArgs">Unused.</param>
		protected override void OnItemFavoriteStateChanged(CommandGridRowBaseVM aItem)
		{
			var vm = aItem as SettingInfoVM;
			if ((null != vm) && (null != _favoriteSettings))
			{
				_favoriteSettings.FavoriteAsciiSettings.Remove(vm.CommandName);
				if (vm.Favorited)
				{
					_favoriteSettings.FavoriteAsciiSettings.Add(vm.CommandName);
				}
			}
		}


		/// <summary>
		///     Invoked when the <see cref="Rows" /> collection is added to, removed from
		///     or reset. Ensures that all ASCII setting elements have the appropriate event
		///     handlers associated with them and calls the method to group them.
		/// </summary>
		/// <param name="aSender">Unused.</param>
		/// <param name="aArgs">Information associated with the collection change.</param>
		protected override void Rows_CollectionChanged(object aSender, NotifyCollectionChangedEventArgs aArgs)
		{
			base.Rows_CollectionChanged(aSender, aArgs);
			GroupedRows = GroupSettings(Rows.OfType<SettingInfoVM>());
		}


		/// <summary>
		///     Sets the <see cref="SelectedRow" /> property of the view.
		/// </summary>
		/// <param name="aRow">The newly selected ASCII command or group object.</param>
		private void SelectedItemChanged(object aRow)
		{
			if (aRow is CommandGroupVM)
			{
				SelectedRow = aRow as CommandGroupVM;
			}
			else if (aRow is SettingInfoVM)
			{
				SelectedRow = aRow as SettingInfoVM;
			}
			else
			{
				SelectedRow = null;
			}
		}


		// Called when a group property changes - used to track the group expanded/collapsed state in settings.
		private void Group_PropertyChanged(object aGroup, PropertyChangedEventArgs aArgs)
		{
			var vm = aGroup as CommandGroupVM;
			var settings = _favoriteSettings;
			if ((null != vm) && (null != settings))
			{
				settings.ExpandedAsciiSettingGroups.Remove(vm.HeaderText);
				if (vm.IsExpanded)
				{
					settings.ExpandedAsciiSettingGroups.Add(vm.HeaderText);
				}
			}
		}


		/// <summary>
		///     Recursive helper method for the <see cref="SetViewMode(CommandDisplayMode)" />
		///     function.  Sets the visibilities of the commands in a collection depending on
		///     the desired view mode.
		/// </summary>
		/// <param name="aMode">The desired <see cref="CommandDisplayMode" /> to view.</param>
		/// <param name="aRows">The root collection of commands to set visible or invisible.</param>
		private bool SetViewModeGrouped(CommandDisplayMode aMode, IEnumerable<CommandGridRowBaseVM> aRows)
		{
			// condition on which the row will be displayed (parameter is the visibility
			// level of the row)
			Predicate<CommandGridRowBaseVM> rowCondition;
			switch (aMode)
			{
				case CommandDisplayMode.BASIC:
					rowCondition = row => CommandVisibility.BASIC == row.VisibilityLevel;
					break;
				case CommandDisplayMode.ADVANCED:
					rowCondition = row => CommandVisibility.NEVER != row.VisibilityLevel;
					break;
				case CommandDisplayMode.FAVORITES:
					rowCondition = row => row.Favorited && (CommandVisibility.NEVER != row.VisibilityLevel);
					break;
				default:
					rowCondition = row => false;
					break;
			}

			var groupVisible = false;
			foreach (var row in aRows)
			{
				if (row is CommandGroupVM)
				{
					row.Visible = SetViewModeGrouped(aMode, (row as CommandGroupVM).Children);
				}
				else
				{
					row.Visible = rowCondition(row);
				}

				groupVisible |= row.Visible;
			}

			return groupVisible;
		}


		/// <summary>
		///     Takes a list of ASCII settings and constructs headings if some settings have
		///     the same first node, split by periods. Used to set the <see cref="GroupedRows" /> property which
		///     is ultimately seen in the view.
		/// </summary>
		/// <param name="aSettings">The ASCII settings to group.</param>
		/// <returns>A grouped collection of ASCII settings and headings.</returns>
		private ObservableCollection<CommandGridRowBaseVM> GroupSettings(IEnumerable<SettingInfoVM> aSettings)
		{
			var newCollection = new ObservableCollection<CommandGridRowBaseVM>();

			Func<SettingInfoVM, string> groupNamer = aSetting =>
			{
				var parts = aSetting.CommandName.Split('.');
				if ((null != parts) && (parts.Length > 0))
				{
					return parts[0];
				}

				return aSetting.CommandName;
			};

			var groups = aSettings.GroupBy(groupNamer).ToDictionary(grp => grp.Key, grp => grp.ToList());

			foreach (var group in groups)
			{
				// Group commands if a threshold count has been reached.
				if (group.Value.Count >= NUM_SETTINGS_BEFORE_GROUP)
				{
					// Group
					var newGroup = new CommandGroupVM();
					newGroup.Children = new ObservableCollection<CommandGridRowBaseVM>(group.Value);
					newGroup.HeaderText = group.Key;

					// Restore expanded state from settings.
					newGroup.IsExpanded = _favoriteSettings?.ExpandedAsciiSettingGroups.Contains(newGroup.HeaderText)
									  ?? false;

					newGroup.PropertyChanged += Group_PropertyChanged;
					newCollection.Add(newGroup);
				}
				else
				{
					// Add as individual nodes.
					foreach (var node in group.Value)
					{
						newCollection.Add(node);
					}
				}
			}

			return newCollection;
		}

		#endregion

		#region -- Data --

		private const int NUM_SETTINGS_BEFORE_GROUP = 3;
		private readonly FavoriteCommandDeviceSettings _favoriteSettings;

		private IDispatcher _dispatcher = DispatcherHelper.Dispatcher;
		private ObservableCollection<CommandGridRowBaseVM> _groupedrows;

		#endregion
	}
}

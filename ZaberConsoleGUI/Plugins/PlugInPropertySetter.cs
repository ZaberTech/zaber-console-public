﻿using System;
using System.Reflection;

namespace ZaberConsole.Plugins
{
	/// <summary>
	///     Holds details about a plugin's property that needs to be set.
	/// </summary>
	internal class PlugInPropertySetter
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Initializes an object with all the information about a plugin's
		///     property.
		/// </summary>
		/// <param name="aPropertyname">The name of the property. May be null.</param>
		/// <param name="aPlugin">The plug in object that exposes the property.</param>
		/// <param name="aPropertyInfo">
		///     The reflection information about the
		///     property.
		/// </param>
		public PlugInPropertySetter(string aPropertyname, object aPlugin, PropertyInfo aPropertyInfo)
		{
			_propertyName = aPropertyname;
			_plugin = aPlugin;
			_propertyInfo = aPropertyInfo;
		}


		public void Set(object aValue, Type aType, string aPropertyName)
		{
			if (string.Equals(_propertyName, aPropertyName) && _propertyInfo.PropertyType.IsAssignableFrom(aType))
			{
				_propertyInfo.SetValue(_plugin, aValue, null);
			}
		}

		#endregion

		#region -- Data --

		private readonly string _propertyName;
		private readonly object _plugin;
		private readonly PropertyInfo _propertyInfo;

		#endregion
	}
}

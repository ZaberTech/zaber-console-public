﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using log4net;
using Zaber;
using Zaber.PlugIns;
using Zaber.Ports;
using Zaber.Telemetry;
using ZaberConsole.Properties;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Dialogs;
using ZaberWpfToolbox.Dialogs.ErrorReporter;

namespace ZaberConsole.Plugins.Options
{
	[PlugIn(Name = "Options", AlwaysActive = true, Dockable = false, Description = "Zaber Console program settings.")]
	public class OptionsVM : PluginTabVM, IDialogClient
	{
		#region -- Public data --

		public class ErrorReporterButtonEnabledReporter : IObserver<bool>
		{
			#region -- Public Methods & Properties --

			public ErrorReporterButtonEnabledReporter(OptionsVM aOptionsVM)
			{
				_optionsVM = aOptionsVM;
			}


			public virtual void Subscribe(IObservable<bool> aProvider) => _unsubscriber = aProvider.Subscribe(this);


			public virtual void Unsubscribe() => _unsubscriber.Dispose();


			public void OnCompleted()
			{
			}


			public void OnError(Exception aError)
			{
			}


			public void OnNext(bool aErrorReportDialogOpen)
				=> _optionsVM.ErrorReporterButtonEnabled = !aErrorReportDialogOpen;

			#endregion

			#region -- Data --

			private readonly OptionsVM _optionsVM;

			private IDisposable _unsubscriber;

			#endregion
		}

		public ErrorReporterButtonEnabledReporter ErrorReporterButtonEnabledReporterInstance;

		#endregion

		#region -- Events --

		public event RequestDialogChange RequestDialogOpen;

		public event RequestDialogChange RequestDialogClose;

		public delegate void DevicePollSettingsChangedHandler();

		public event DevicePollSettingsChangedHandler DevicePollSettingsChanged;

		#endregion

		#region -- Public Methods & Properties --

		public OptionsVM()
		{
			ErrorReporterButtonEnabledReporterInstance = new ErrorReporterButtonEnabledReporter(this);
			ErrorReporterButtonEnabledReporterInstance.Subscribe(
				ErrorReportDialogVM.ErrorReporterDialogOpenProviderInstance);
		}


		/// <summary>
		///     Populate all relevant control values from user settings.
		///     Does nothing if the Port is not set.
		/// </summary>
		public void LoadFromSettings()
		{
			if (null == PortFacade)
			{
				return;
			}

			var settings = Settings.Default;

			QueryDevices = settings.EnableDeviceQuery && !settings.IsQuerySilent;
			QueryDevicesSilently = settings.EnableDeviceQuery && settings.IsQuerySilent;
			DoNotQueryDevices = !settings.EnableDeviceQuery;
			KeepListCurrent = settings.IsPortInvalidateEnabled;
			ValidateDeviceNumbers = settings.AreDeviceNumbersValidated;

			LogSize = (uint) settings.LogMaxLength;

			SendChecksums = settings.AreChecksumsSent;

			RetryCount = (uint) settings.RetryCount;
			OnlineMode = settings.AllowNetworkAccess;
			DisableWebProxy = settings.DisableWebProxy;

			BaudRate = settings.ManualBaudRate;
			Protocol = settings.ManualProtocol;

			if (PortFacade.IsOpen && PortFacade.Port.IsAsciiMode)
			{
				// RM6579 - Always use message IDs in ASCII if supported.
				AreMessageIdsEnabled = true;
			}
		}


		[PlugInProperty]
		public ZaberPortFacade PortFacade
		{
			get => _port;
			set
			{
				if (_port != null)
				{
					_port.Opened -= portFacade_Opened;
					_port.Closed -= portFacade_Closed;
					_port.SelectedConversationChanged -= _portFacade_SelectedConversationChanged;
				}

				_port = value;
				if (_port != null)
				{
					_port.Opened += portFacade_Opened;
					_port.Closed += portFacade_Closed;
					_port.SelectedConversationChanged += _portFacade_SelectedConversationChanged;
				}
			}
		}


		// This is hooked up manually from MainWindowVM because we need more than the
		// IPluginManager interface, which is what the PluginProperty attribute would allow us.
		public PlugInManager PlugInManager
		{
			get => _manager;
			set
			{
				if (null != _manager)
				{
					_manager.SettingsResetEvent -= OnSettingsReset;
				}

				_manager = value;

				if (null != _manager)
				{
					_manager.SettingsResetEvent += OnSettingsReset;
				}
			}
		}

		/// <summary>
		///     Gets a flag to say whether devices should be queried when the
		///     serial port is opened.
		/// </summary>
		public bool EnableDeviceQuery => !DoNotQueryDevices;

		public bool QueryDevices
		{
			get => _queryDevices;
			set
			{
				Set(ref _queryDevices, value, nameof(QueryDevices));

				if (value)
				{
					Settings.Default.EnableDeviceQuery = true;
					Settings.Default.IsQuerySilent = false;
					_eventLog.LogEvent("Query mode", "Automatic query");
				}

				PortFacade.ClearDeferredInvalidation();
				UpdatePortSettings();
			}
		}


		public bool QueryDevicesSilently
		{
			get => _queryDevicesSilently;
			set
			{
				Set(ref _queryDevicesSilently, value, nameof(QueryDevicesSilently));

				if (value)
				{
					Settings.Default.EnableDeviceQuery = true;
					Settings.Default.IsQuerySilent = true;
					_eventLog.LogEvent("Query mode", "Automatic silent query");
				}

				PortFacade.ClearDeferredInvalidation();
				UpdatePortSettings();
			}
		}


		public bool DoNotQueryDevices
		{
			get => _doNotQueryDevices;
			set
			{
				Set(ref _doNotQueryDevices, value, nameof(DoNotQueryDevices));

				if (value)
				{
					Settings.Default.EnableDeviceQuery = false;
					_eventLog.LogEvent("Query mode", "Query disabled");
				}

				PortFacade.ClearDeferredInvalidation();
				UpdatePortSettings();
			}
		}


		public IEnumerable<Protocol> Protocols => new[] { Protocol.ASCII, Protocol.Binary };


		public Protocol Protocol
		{
			get => _protocol;
			set
			{
				Set(ref _protocol, value, nameof(Protocol));
				OnPropertyChanged(nameof(PortProtocol));
				Settings.Default.ManualProtocol = _protocol;
			}
		}


		public Protocol PortProtocol
		{
			get
			{
				if ((null != PortFacade?.Port) && PortFacade.Port.IsOpen)
				{
					return PortFacade.Port.IsAsciiMode ? Protocol.ASCII : Protocol.Binary;
				}

				return Protocol;
			}
		}


		public IEnumerable<int> BaudRates => RawRS232Port.SupportedSerialBaudRates;


		public int BaudRate
		{
			get => _baudRate;
			set
			{
				Set(ref _baudRate, value, nameof(BaudRate));
				Settings.Default.ManualBaudRate = _baudRate;
				OnPropertyChanged(nameof(PortBaudRate));
			}
		}


		public int PortBaudRate => PortFacade?.BaudRate ?? BaudRate;


		public bool KeepListCurrent
		{
			get => _keepListCurrent;
			set
			{
				Set(ref _keepListCurrent, value, nameof(KeepListCurrent));
				Settings.Default.IsPortInvalidateEnabled = value;
				PortFacade.IsInvalidateEnabled = value && EnableDeviceQuery;
				_eventLog.LogEvent("Keep device list up to date", value.ToString());
			}
		}


		public bool ValidateDeviceNumbers
		{
			get => _validateDeviceNumbers;
			set
			{
				Set(ref _validateDeviceNumbers, value, nameof(ValidateDeviceNumbers));
				Settings.Default.AreDeviceNumbersValidated = value;
				PortFacade.AreDeviceNumbersValidated = value && EnableDeviceQuery;
				_eventLog.LogEvent("Validate device numbers", value.ToString());
			}
		}


		public bool SendChecksums
		{
			get => _sendChecksums;
			set
			{
				Set(ref _sendChecksums, value, nameof(SendChecksums));
				Settings.Default.AreChecksumsSent = SendChecksums;
				_eventLog.LogEvent("Send checksums", value.ToString());
			}
		}


		public bool OnlineMode
		{
			get => Settings.Default.AllowNetworkAccess;
			set
			{
				Settings.Default.AllowNetworkAccess = value;
				OnPropertyChanged(nameof(OnlineMode));
				_eventLog.LogEvent("Allow network access", value.ToString());
			}
		}


		public bool DisableWebProxy
		{
			get => _disableWebProxy;
			set
			{
				Set(ref _disableWebProxy, value, nameof(DisableWebProxy));
				Settings.Default.DisableWebProxy = value;
			}
		}


		public uint RetryCount
		{
			get => _retryCount;
			set
			{
				Set(ref _retryCount, value, nameof(RetryCount));
				Settings.Default.RetryCount = (int) value;
				PortFacade.GetConversation(0).RetryCount = (int) value;
			}
		}


		public uint LogSize
		{
			get => _logSize;
			set
			{
				Set(ref _logSize, value, nameof(LogSize));
				Settings.Default.LogMaxLength = (int) value;
			}
		}


		public bool IsPortOpen => (null != PortFacade) && PortFacade.IsOpen;


		public bool AreMessageIdsEnabled
		{
			get => (null != PortFacade) && PortFacade.AreMessageIdsEnabled;
			set
			{
				SetMessageIdsEnabled(value);
				OnPropertyChanged(nameof(AreMessageIdsEnabled));
				UpdatePortSettings();
				_eventLog.LogEvent("Message ID enable", value.ToString());
			}
		}


		public bool ErrorReporterButtonEnabled
		{
			get => _errorReporterButtonEnabled;
			set => Set(ref _errorReporterButtonEnabled, value, nameof(ErrorReporterButtonEnabled));
		}


		public FavoriteSettingsKeyMode CommandsAndSettingsKeyMode
		{
			get => Settings.Default.CommandsAndSettingsFavoriteKeyMode;
			set
			{
				if (Settings.Default.CommandsAndSettingsFavoriteKeyMode != value)
				{
					Settings.Default.CommandsAndSettingsFavoriteKeyMode = value;
					OnPropertyChanged(nameof(CommandsAndSettingsKeyMode));
					if (IsPortOpen && !DoNotQueryDevices)
					{
						_log.Info("Invalidating port because favorites keying mode was changed.");
						PortFacade.Invalidate();
					}
				}

				_eventLog.LogEvent("Favorites key mode", value.ToString());
			}
		}


		/// <summary>
		///     Optional message to add to the About box about the database version.
		/// </summary>
		public string DatabaseDisplayInfo { get; set; } = null;


		/// <summary>
		///     List of selectable polling intervals for auto-refresh.
		/// </summary>
		public IEnumerable<TimeSpan> PollIntervals 
			=> new []
			{
				TimeSpan.FromSeconds(1),
				TimeSpan.FromMilliseconds(500),
				TimeSpan.FromMilliseconds(250),
				TimeSpan.FromMilliseconds(100)
			};


		/// <summary>
		///     Selected device poll interval for auto-refresh.
		/// </summary>
		public TimeSpan SelectedPollInterval
		{
			get => Settings.Default.DevicePollingInterval;
			set
			{
				Settings.Default.DevicePollingInterval = value;
				OnPropertyChanged(nameof(SelectedPollInterval));
				DevicePollSettingsChanged?.Invoke();
			}
		}


		/// <summary>
		///     Whether or not device polling is currently enabled.
		/// </summary>
		public bool IsPollingEnabled
		{
			get => Settings.Default.DevicePollingEnabled;
			set
			{
				Settings.Default.DevicePollingEnabled = value;
				OnPropertyChanged(nameof(IsPollingEnabled));
				DevicePollSettingsChanged?.Invoke();
			}
		}


		public bool ShowPollingMessages
		{
			get => Settings.Default.ShowPollingMessages;
			set
			{
				Settings.Default.ShowPollingMessages = value;
				OnPropertyChanged(nameof(ShowPollingMessages));
				DevicePollSettingsChanged?.Invoke();
			}
		}


		public ICommand ShowApplicationLogCommand 
			=> OnDemandRelayCommand(ref _showAppLogCommand, _ => OnShowApplicationLog());


		public ICommand ShowPluginDialogCommand 
			=> OnDemandRelayCommand(
				ref _showPluginDialogCommand,
				_ =>
				{
					PlugInManager.ShowOptionsDialog();
					_eventLog.LogEvent("Plugin list dialog used");
				});


		public ICommand ShowAboutBoxCommand 
			=> OnDemandRelayCommand(ref _showAboutBoxCommand, _ => OnShowAboutBox());


		public ICommand RestoreDefaultsCommand 
			=> OnDemandRelayCommand(ref _restoreDefaultsCommand, _ =>
			{
				PlugInManager?.FireSettingsResetEvent(this);
				_eventLog.LogEvent("Restore defaults button used");

				var mbvm = new MessageBoxParams
				{
					Message = "User settings have been reset to their default values.",
					Caption = "Reset",
					Icon = MessageBoxImage.Information,
					Buttons = MessageBoxButton.OK
				};

				RequestDialogOpen?.Invoke(this, mbvm);
			});


		public ICommand ReportanErrorCommand 
			=> OnDemandRelayCommand(
				ref _reportErrorCommand,
				_ =>
				{
					var errorReporter = new ErrorReporter();
					errorReporter.RequestDialogOpen += (sender, vm) => { RequestDialogOpen?.Invoke(sender, vm); };
					errorReporter.RequestDialogClose += (sender, vm) => { RequestDialogClose?.Invoke(sender, vm); };
					errorReporter.ShowErrorReportDialog();
				});

		#endregion

		#region -- Non-Public Methods --

		private void portFacade_Opened(object aSender, EventArgs aArgs) => _dispatcher.BeginInvoke(() =>
		{
			OnPropertyChanged(nameof(IsPortOpen));
			OnPropertyChanged(nameof(AreMessageIdsEnabled));
			OnPropertyChanged(nameof(PortBaudRate));
			OnPropertyChanged(nameof(PortProtocol));
			OnPropertyChanged(nameof(IsPollingEnabled));

			if (PortFacade.Port.IsAsciiMode)
			{
				AreMessageIdsEnabled = true;
			}
		});


		private void portFacade_Closed(object aSender, EventArgs aArgs) => _dispatcher.BeginInvoke(() =>
		{
			OnPropertyChanged(nameof(IsPortOpen));
			OnPropertyChanged(nameof(PortBaudRate));
			OnPropertyChanged(nameof(PortProtocol));
			OnPropertyChanged(nameof(IsPollingEnabled));
		});


		private void _portFacade_SelectedConversationChanged(object aSender, EventArgs aArgs)
			=> _dispatcher.BeginInvoke(() =>
			{
				OnPropertyChanged(nameof(IsPortOpen));
				OnPropertyChanged(nameof(AreMessageIdsEnabled));
				OnPropertyChanged(nameof(PortBaudRate));
			});


		private void UpdatePortSettings()
		{
			if (null != PortFacade)
			{
				PortFacade.IsInvalidateEnabled = EnableDeviceQuery && KeepListCurrent;
				PortFacade.AreDeviceNumbersValidated = EnableDeviceQuery && AreMessageIdsEnabled;
			}

			// Try to force all devices to a consistent message ID mode.
			SetMessageIdsEnabled(AreMessageIdsEnabled);
		}


		private void SetMessageIdsEnabled(bool aValue)
		{
			if (null == PortFacade)
			{
				return;
			}

			MessageBoxParams mb = null;

			try
			{
				PortFacade.AreMessageIdsEnabled = aValue;
			}
			catch (Exception aException)
			{
				_log.Error("Error changing message ID mode.", aException);

				var change = aValue ? "enable" : "disable";

				mb = new MessageBoxParams
				{
					Buttons = MessageBoxButton.OK,
					Caption = "Mode change unsuccessful",
					Icon = MessageBoxImage.Warning,
					Message =
						$"An error occurred while attempting to {change} the message ID mode. If you have multiple devices on the serial chain, they may be using inconsistent message ID modes, which is not recommended. Ensure all devices are idle before changing this setting."
				};
			}

			if (null != mb)
			{
				RequestDialogOpen?.Invoke(this, mb);
			}
		}


		private void OnShowApplicationLog()
		{
			var logFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
										   "Zaber Technologies",
										   "Zaber Console",
										   "ZaberConsole.log");

			try
			{
				Process.Start(logFilePath);
			}
			catch (Exception ex)
			{
				var mbvm = new MessageBoxParams
				{
					Message = string.Format("{0}: {1}{2}({3})",
											ex.GetType().Name,
											ex.Message,
											Environment.NewLine,
											logFilePath),
					Caption = "Application Log",
					Buttons = MessageBoxButton.OK,
					Icon = MessageBoxImage.Warning
				};

				RequestDialogOpen?.Invoke(this, mbvm);
			}

			_eventLog.LogEvent("Show application log button used");
		}


		private void OnShowAboutBox()
		{
			if (null != RequestDialogOpen)
			{
				var dlg = new AboutBoxVM();
				dlg.Icon = Application.Current.FindResource("ZaberConsoleIcon") as ImageSource;
				dlg.Description =
					"Zaber Console is a general-purpose tool for configuring and interacting with Zaber motion devices.";

				if (!string.IsNullOrEmpty(DatabaseDisplayInfo))
				{
					dlg.Description += Environment.NewLine + Environment.NewLine + DatabaseDisplayInfo;
				}

				var ver = Assembly.GetEntryAssembly().GetName()?.Version;
				if (null != ver)
				{
					#if DEV || TEST
					var releaseNotesUrl = $"https://iwiki.izaber.com/FWEE/Zaber_Console/Dev_Release_Notes#{ ver }"; 
					#else
					var releaseNotesUrl = $"https://www.zaber.com/wiki/Software/Zaber_Console/Release_notes#{ver}";
					#endif
					dlg.ReleaseNotesUrl = new Uri(releaseNotesUrl);
				}

				RequestDialogOpen.Invoke(this, dlg);

				_eventLog.LogEvent("About box shown");
			}
		}


		private void OnSettingsReset(object aSender, EventArgs aArgs)
		{
			var settings = Settings.Default;
			settings.EnableDeviceQuery = true;
			settings.IsQuerySilent = false;
			settings.IsPortInvalidateEnabled = true;
			settings.AreChecksumsSent = false;
			settings.AreDeviceNumbersValidated = false;
			settings.RetryCount = 0;
			settings.LogMaxLength = 10000;
			settings.AllowNetworkAccess = true;
			settings.DatabaseUpdater.AutomaticallyCheckForUpdates = true;
			settings.DatabaseUpdater.LastExportSettings = new FileBrowserUserSettings();
			settings.DatabaseUpdater.LastImportSettings = new FileBrowserUserSettings();
			settings.SoftwareUpdater.AutomaticallyCheckForUpdates = true;
			settings.DisableWebProxy = false;
			settings.DoNotShowAgainSettings?.Clear();
			settings.CommandListFavorites?.Clear();
			settings.ManualProtocol = Protocol.ASCII;
			settings.ManualBaudRate = 115200;
			LoadFromSettings();
		}

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Options tab");

		private readonly IDispatcher _dispatcher = DispatcherHelper.Dispatcher;

		private bool _queryDevices;
		private bool _queryDevicesSilently;
		private bool _doNotQueryDevices;
		private Protocol _protocol = Protocol.ASCII;
		private int _baudRate = 115200;
		private bool _keepListCurrent;
		private bool _validateDeviceNumbers;
		private bool _sendChecksums;
		private bool _disableWebProxy;
		private bool _errorReporterButtonEnabled = true;
		private uint _retryCount;
		private uint _logSize;
		private ZaberPortFacade _port;
		private PlugInManager _manager;
		private RelayCommand _showAppLogCommand;
		private RelayCommand _showPluginDialogCommand;
		private RelayCommand _showAboutBoxCommand;
		private RelayCommand _restoreDefaultsCommand;
		private RelayCommand _reportErrorCommand;

		#endregion
	}
}

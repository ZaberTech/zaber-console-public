﻿using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.Options
{
	/// <summary>
	///     Interaction logic for OptionsControl.xaml
	/// </summary>
	public partial class OptionsControl : BaseControl
	{
		#region -- Public Methods & Properties --

		public OptionsControl()
		{
			InitializeComponent();
		}

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;

namespace ZaberConsole.Plugins
{
	public class PlugInFilter
	{
		#region -- Public Methods & Properties --

		public PlugInFilter(DirectoryInfo plugInsFolder)
		{
			PlugInsFolder = plugInsFolder;
			versionRegex = new Regex(@"^[\d\.]*$");
		}


		public IEnumerable<FileInfo> Filter(IEnumerable<FileInfo> files)
		{
			var filtered = new Dictionary<string, FileInfo>(); // name -> fileInfo
			foreach (var file in files)
			{
				var plugInName = GetPlugInName(file);
				if (plugInName.Equals("zaber",
									  StringComparison.InvariantCultureIgnoreCase))
				{
					continue;
				}

				FileInfo otherVersion;
				FileInfo laterVersion;
				if (filtered.TryGetValue(plugInName, out otherVersion))
				{
					laterVersion = GetLaterVersion(file, otherVersion);
				}
				else
				{
					laterVersion = file;
				}

				filtered[plugInName] = laterVersion;
			}

			return filtered.Values;
		}


		public string GetPlugInName(FileInfo fileInfo)
		{
			var fullPath = fileInfo.FullName;
			string fileName;
			if (!fullPath.StartsWith(PlugInsFolder.FullName))
			{
				fileName = Path.GetFileNameWithoutExtension(fullPath);
			}
			else
			{
				var folderLength = PlugInsFolder.FullName.Length;
				var extensionLength = Path.GetExtension(fullPath).Length;
				fileName = fullPath.Substring(folderLength + 1,
											  fullPath.Length - extensionLength - folderLength - 1);
			}

			var dashIndex = fileName.LastIndexOf('-');
			if (dashIndex < 0)
			{
				return fileName;
			}

			if (versionRegex.IsMatch(fileName.Substring(dashIndex + 1)))
			{
				return fileName.Substring(0, dashIndex);
			}

			return fileName;
		}


		public DirectoryInfo PlugInsFolder { get; }

		#endregion

		#region -- Non-Public Methods --

		private FileInfo GetLaterVersion(FileInfo a, FileInfo b)
		{
			var piecesA = GetVersionPieces(a);
			var piecesB = GetVersionPieces(b);

			for (var i = 0; i < piecesA.Length; i++)
			{
				if (i >= piecesB.Length)
				{
					return a;
				}

				var diff = piecesA[i] - piecesB[i];
				if (diff < 0)
				{
					return b;
				}

				if (diff > 0)
				{
					return a;
				}
			}

			return b;
		}


		private int[] GetVersionPieces(FileInfo file)
		{
			var name = Path.GetFileNameWithoutExtension(file.Name);
			var dashIndex = name.LastIndexOf('-');
			if (dashIndex < 0)
			{
				return new int[] { };
			}

			var version = name.Substring(dashIndex + 1);
			var stringPieces = version.Split('.');
			var intPieces = new int[stringPieces.Length];
			for (var i = 0; i < stringPieces.Length; i++)
			{
				int.TryParse(stringPieces[i], NumberStyles.Integer, CultureInfo.InvariantCulture, out intPieces[i]);
			}

			return intPieces;
		}

		#endregion

		#region -- Data --

		private readonly Regex versionRegex;

		#endregion
	}
}

﻿using System;
using System.IO;
using System.Windows;
using Zaber;
using Zaber.Threading;
using ZaberConsole.Properties;
using ZaberWpfToolbox.Dialogs;

namespace ZaberConsole
{
	/// <summary>
	///     Main window logic related to database updates.
	/// </summary>
	internal partial class MainWindowVM
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Class (also UI component VM) that manages database downloads.
		/// </summary>
		public DatabaseUpdaterVM DatabaseUpdater { get; private set; }

		#endregion

		#region -- Non-Public Methods --

		private void SetupDatabaseUpdater()
		{
			// Upgrade legacy settings for the database updater.
			var updaterSettings = Settings.Default.DatabaseUpdater;
			if (updaterSettings is null)
			{
				Settings.Default.AllowNetworkAccess = Settings.Default.ShouldUpdateDatabase;
				updaterSettings = new DatabaseUpdaterSettings
				{
					AutomaticallyCheckForUpdates = Settings.Default.ShouldUpdateDatabase
				};

				// ShouldUpdateDatabase and the six settings on the RHS below are now deprecated. TODO ticket #6049.
				updaterSettings.LastImportSettings.Directory = Settings.Default.LastDatabaseImportFolder;
				updaterSettings.LastImportSettings.Filename = Settings.Default.LastDatabaseImportFilename;
				updaterSettings.LastImportSettings.FilterIndex = Settings.Default.LastDatabaseImportFilterIndex;
				updaterSettings.LastExportSettings.Directory = Settings.Default.LastDatabaseExportFolder;
				updaterSettings.LastExportSettings.Filename = Settings.Default.LastDatabaseExportFilename;
				updaterSettings.LastExportSettings.FilterIndex = Settings.Default.LastDatabaseExportFilterIndex;

				Settings.Default.DatabaseUpdater = updaterSettings;
			}

			DatabaseUpdater = new DatabaseUpdaterVM(Configuration.AppDataPath,
													Configuration.DatabaseFilename,
													Configuration.DatabaseUrl,
													Configuration.BundledDatabasePath,
													updaterSettings);

			DatabaseUpdater.RequestDialogOpen += Child_RequestDialogOpen;
			DatabaseUpdater.RequestDialogClose += Child_RequestDialogClose;
			DatabaseUpdater.ShowStatusMessage += PluginManager_StatusMessageReceived;
		}


		private void ShutdownDatabaseUpdater()
		{
			DatabaseUpdater.RequestDialogOpen -= Child_RequestDialogOpen;
			DatabaseUpdater.RequestDialogClose -= Child_RequestDialogClose;
			DatabaseUpdater.ShowStatusMessage -= PluginManager_StatusMessageReceived;
			DatabaseUpdater.Shutdown();
		}


		/// <summary>
		///     Load the device database recommended by the updater, unless it is the
		///     same one currently loaded.
		/// </summary>
		private void SwitchToLatestDatabase()
		{
			_latestDatabasePath = DatabaseUpdater.LatestDatabasePath;

			// A new database has been downloaded or we are opening a database at program start.
			// Note this may be the bundled database.
			if (_latestDatabasePath != _deviceDatabase?.FilePath)
			{
				TrySetDatabase(_latestDatabasePath);
			}

			// Opening the downloaded database failed. Fall back to bundled if possible.
			bool fallback = false;
			if ((_deviceDatabase is null) && (_latestDatabasePath != Configuration.BundledDatabasePath))
			{
				TrySetDatabase(Configuration.BundledDatabasePath);
				fallback = true;
			}

			// Report an error if no database opened.
			if (_deviceDatabase is null)
			{
				var mb = new MessageBoxParams
				{
					Buttons = MessageBoxButton.OK,
					Caption = "Device database not available",
					Icon = MessageBoxImage.Error,
					Message = Resources.ERR_DB_OPEN_FAILED
				};

				Child_RequestDialogOpen(this, mb);
			}
			else if (fallback)
			{
				var mb = new MessageBoxParams
				{
					Buttons = MessageBoxButton.OK,
					Caption = "Downloaded device database cannot be opened",
					Icon = MessageBoxImage.Warning,
					Message = Resources.WARN_DB_FALLBACK
				};

				Child_RequestDialogOpen(this, mb);
			}
		}


		private void TrySetDatabase(string aPath)
		{
			_appLog.Info($"Opening database from {aPath}.");
			var db = TryLoadDatabaseFile(aPath);
			if (null != db)
			{
				var prevDB = _deviceDatabase;
				_deviceDatabase = db;

				PortFacade.DefaultDeviceType = new DeviceType
				{
					Commands = _deviceDatabase.GetAdvancedBinaryCommands()
				};

				PortFacade.DeviceTypeMap = _deviceDatabase;
				PortFacade.DeviceHelpProvider = _deviceDatabase;

				prevDB?.Dispose();
				DatabaseUpdater.CleanupOldDatabaseFiles();

				// Output the database hash to the app log for debugging purposes.
				var worker = BackgroundWorkerManager.CreateWorker();
				worker.DoWork += (aSender, aArgs) =>
				{
					var hash = FileUtil.GetFileHashAsString(aPath);
					_appLog.Info($"Opening database {aPath} with hash = {hash}.");
					OptionsTab.DatabaseDisplayInfo = "Device database hash = "
												 + hash
												 + Environment.NewLine
												 + "Database file timestamp = "
												 + File.GetLastWriteTimeUtc(aPath);
				};

				worker.RunWorkerCompleted += (aSender, aArgs) =>
				{
					if (null != aArgs.Error)
					{
						_appLog.Error($"Failed to hash database file {aPath}.", aArgs.Error);
					}
				};

				worker.Run();
			}
			else
			{
				_appLog.Error($"Failed to open new database from {aPath}.");
			}
		}


		// Attempts to load the specified database. Returns null on error.
		private SqliteDeviceDictionary TryLoadDatabaseFile(string aPath)
		{
			SqliteDeviceDictionary deviceDict = null;
			try
			{
				var dbFileInfo = new FileInfo(aPath);
				deviceDict = new SqliteDeviceDictionary(aPath);
				_appLog.InfoFormat("Loaded device database from {0} ({1:n0} bytes)",
								   dbFileInfo.FullName,
								   dbFileInfo.Length);
			}
			catch (Exception e)
			{
				_appLog.Error("There was an error opening database file " + aPath, e);
			}

			return deviceDict;
		}

		#endregion

		#region -- Data --

		private SqliteDeviceDictionary _deviceDatabase;
		private string _latestDatabasePath;

		#endregion
	}
}

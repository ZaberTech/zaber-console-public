﻿using System;
using System.IO;
using System.Threading;

namespace ZaberConsole
{
	/// <summary>
	///     A stream that blocks the reading thread until there is something
	///     available to read.
	/// </summary>
	public class BlockingStream : Stream
	{
		#region -- Events --

		/// <summary>
		///     Raised when the stream is blocked and waiting for input.
		/// </summary>
		public event EventHandler Blocked;

		#endregion

		#region -- Public Methods & Properties --

		public BlockingStream(Stream stream)
		{
			if (!stream.CanSeek)
			{
				throw new ArgumentException("Stream must support seek", "stream");
			}

			_stream = stream;
		}


		public override void Flush()
		{
			lock (_stream)
			{
				_stream.Flush();
				Monitor.PulseAll(_stream);
			}
		}


		public override long Seek(long offset, SeekOrigin origin)
		{
			lock (_stream)
			{
				var res = _stream.Seek(offset, origin);
				Monitor.PulseAll(_stream);
				return res;
			}
		}


		public override void SetLength(long value)
		{
			lock (_stream)
			{
				_stream.SetLength(value);
				Monitor.PulseAll(_stream);
			}
		}


		public override int Read(byte[] buffer, int offset, int count)
		{
			lock (_stream)
			{
				do
				{
					var read = _stream.Read(buffer, offset, count);
					if ((read > 0) || !_isBlockingEnabled)
					{
						return read;
					}

					if (Blocked != null)
					{
						Blocked(this, EventArgs.Empty);
					}

					Monitor.Wait(_stream);
				} while (true);
			}
		}


		public override void Write(byte[] buffer, int offset, int count)
		{
			lock (_stream)
			{
				var currentPosition = _stream.Position;
				_stream.Position = _stream.Length;
				_stream.Write(buffer, offset, count);
				_stream.Position = currentPosition;
				Monitor.PulseAll(_stream);
			}
		}


		/// <summary>
		///     Gets or sets a value enabling the stream to block when
		///     there is no available input for a read method.
		/// </summary>
		public bool IsBlockingEnabled
		{
			get => _isBlockingEnabled;
			set
			{
				lock (_stream)
				{
					_isBlockingEnabled = value;

					Monitor.PulseAll(_stream);
				}
			}
		}

		public override bool CanRead
		{
			get
			{
				lock (_stream)
				{
					return _stream.CanRead;
				}
			}
		}

		public override bool CanSeek
		{
			get
			{
				lock (_stream)
				{
					return _stream.CanSeek;
				}
			}
		}

		public override bool CanWrite
		{
			get
			{
				lock (_stream)
				{
					return _stream.CanWrite;
				}
			}
		}

		public override long Length
		{
			get
			{
				lock (_stream)
				{
					return _stream.Length;
				}
			}
		}

		public override long Position
		{
			get
			{
				lock (_stream)
				{
					return _stream.Position;
				}
			}
			set
			{
				lock (_stream)
				{
					_stream.Position = value;
					Monitor.PulseAll(_stream);
				}
			}
		}

		#endregion

		#region -- Data --

		private readonly Stream _stream;
		private bool _isBlockingEnabled = true;

		#endregion
	}
}

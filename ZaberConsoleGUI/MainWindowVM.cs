using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using log4net;
using MvvmDialogs.ViewModels;
using Zaber;
using Zaber.Ports;
using Zaber.Telemetry;
using Zaber.Threading;
using ZaberConsole.DeviceList;
using ZaberConsole.Dialogs;
using ZaberConsole.Plugins;
using ZaberConsole.Plugins.Options;
using ZaberConsole.Plugins.Terminal;
using ZaberConsole.Ports;
using ZaberConsole.Properties;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Controls;
using ZaberWpfToolbox.Dialogs;

namespace ZaberConsole
{
	internal partial class MainWindowVM : ObservableObject
	{
		#region -- Setup and teardown --

		public event EventHandler MainWindowClosed;


		public MainWindowVM()
		{
			_appLog.Debug("Enter main window VM constructor.");
			_eventLog.LogEvent("Program opened",
							   $"Edition = {Configuration.ProgramName}; version = {Configuration.ProgramVersion}");
			#if DEV
			WindowTitle = "Zaber Console DEV";
			#elif TEST
			WindowTitle = "Zaber Console TEST VERSION - DO NOT DISTRIBUTE";
			#else
			WindowTitle = "Zaber Console";
			#endif
			_dispatcher = Dispatcher.CurrentDispatcher;

			TxIndicatorVM = new IndicatorLightVM
			{
				Text = "Tx",
				ToolTip = "Flashes when serial data is sent."
			};

			RxIndicatorVM = new IndicatorLightVM
			{
				Text = "Rx",
				ToolTip = "Flashes when serial data is received.",
				LitColor = Colors.DeepSkyBlue
			};


			// Ensure the window will be positioned onscreen.
			var windowSettings = Settings.Default.ZaberConsoleWindowSettings;
			if (null == windowSettings)
			{
				windowSettings = new WindowSettings();
				Settings.Default.ZaberConsoleWindowSettings = windowSettings;
				windowSettings.WindowWidth = 800;
				windowSettings.WindowHeight = 600;
				windowSettings.CenterOnMainScreen();
			}

			windowSettings.EnsureWindowVisible();

			// Upgrade the legacy splitter distance settings if needed.
			if (null == windowSettings.UserMeasurements)
			{
				windowSettings.UserMeasurements = new string[4];

				if ((null != windowSettings.SplitterDistances) && (windowSettings.SplitterDistances.Length > 2))
				{
					windowSettings.UserMeasurements[0] = windowSettings.SplitterDistances[2] + "*";
					windowSettings.UserMeasurements[1] =
						(windowSettings.WindowWidth - windowSettings.SplitterDistances[2]) + "*";
					windowSettings.UserMeasurements[2] = windowSettings.SplitterDistances[0] + "*";
					windowSettings.UserMeasurements[3] =
						(windowSettings.WindowHeight - windowSettings.SplitterDistances[0]) + "*";
				}
				else
				{
					for (var i = 0; i < 4; ++i)
					{
						windowSettings.UserMeasurements[i] = "1*";
					}
				}
			}

			SetupDatabaseUpdater();
			SetupSoftwareUpdater();

			_findDevicesCommand = new RelayCommand(
				_ => FindDevicesButton_Click(),
				_ => IsConnectionOpen && !(PortFacade.Port is TcpPort));

			_changeProtocolMenuItem = new MenuItemVM
			{
				Title = Resources.LABEL_CHANGE_PROTOCOL_NONE,
				ToolTip = Resources.TIP_CHANGE_PROTOCOL,
				Command = new RelayCommand(
					_ => QuickChangeProtocol(),
					_ => IsConnectionOpen && !(PortFacade.Port is TcpPort))
			};

			ConnectionMenu = new MenuButtonVM
			{
				ToolTip = Resources.TIP_CONNECTION_MENU,
				IconUrl = CONNECTION_MENU_ICON,
				MenuItems = new ObservableCollection<MenuItemVM>
				{
					new MenuItemVM
					{
						Title = Resources.LABEL_FIND_DEVICES,
						ToolTip = Resources.TIP_FIND_DEVICES,
						Command = _findDevicesCommand
					},
					_changeProtocolMenuItem
				}
			};

			// Hook up the events that allows all HTML link clicks to be intercepted for offline mode.
			WebLinkFilter.RequestDialogOpen += Child_RequestDialogOpen;
			_appLog.Debug("Exit main window VM constructor.");
		}


		private void OnWindowLoaded()
		{
			_appLog.Debug("Enter main window loaded event.");
			var settings = Settings.Default;

			// Initialize device list refresh timer.
			_deviceListUpdateTimer = new DispatcherTimer(DispatcherPriority.Background, _dispatcher)
			{
				Interval = new TimeSpan(0, 0, 0, 0, 100)
			};

			_deviceListUpdateTimer.Tick += DeviceListUpdateTimer_Tick;
			_deviceListUpdateTimer.IsEnabled = true;

			// Load plugins.
			PluginManager.RequestDialogOpen += Child_RequestDialogOpen;
			PluginManager.RequestDialogClose += Child_RequestDialogClose;
			PluginManager.LoadPlugins();
			PluginManager.SettingsResetEvent += OnUserSettingsReset;
			PluginManager.StatusMessageReceived += PluginManager_StatusMessageReceived;
			Terminal = PluginManager.FindActivePlugins<TerminalPluginVM>().FirstOrDefault();

			// Plugin manager should have auto-created the options tab since it's an always-visible plugin.
			OptionsTab = PluginManager.FindActivePlugins<OptionsVM>().FirstOrDefault();
			if (null == OptionsTab)
			{
				throw new InvalidProgramException(
					"The Plugin Manager failed to create the Options tab. This is a critical failure and the program will not function.");
			}

			OptionsTab.PlugInManager = PluginManager;
			OptionsTab.DevicePollSettingsChanged += () => ApplyDevicePollSettings(Settings.Default.DevicePollingEnabled);

			PortFacade = new ZaberPortFacade
			{
				DefaultDeviceType = new DeviceType { Commands = new List<CommandInfo>() },
				DeviceTypeMap = new Dictionary<Tuple<int, FirmwareVersion>, DeviceType>(),

				// TSeriesPort is used here instead of RS232Port for backwards compatibility.
				Port = new TSeriesPort(new SerialPort(), new PacketConverter()),
				QueryTimeout = 1000
			};

			// Apply database imports, downloads or default deployments.
			SwitchToLatestDatabase();
			if (Settings.Default.DatabaseUpdater.AutomaticallyCheckForUpdates)
			{
				DatabaseUpdater.CheckForUpdates(_latestDatabasePath);
			}

			// Start a check for program updates, if enabled.
			if (Settings.Default.SoftwareUpdater.AutomaticallyCheckForUpdates)
			{
				SoftwareUpdater.CheckForUpdates();
			}

			var applyLastPortSelection = false;
			if (settings.SavedConnections is null)
			{
				settings.SavedConnections = new PortSettingsCollection();
				applyLastPortSelection = true;
			}

			var portFactoryTypes = PluginManager.FindConcreteImplementationsOf(typeof(IPortFactory));
			PortManager = new PortManagerVM(settings.SavedConnections, portFactoryTypes);

			// One-time upgrade of old com port selection to new port settings system.
			if (applyLastPortSelection && !string.IsNullOrEmpty(settings.ComPortName))
			{
				PortManager.SelectedConnection =
					PortManager.DisplayedConnections.Where(conn =>
								{
									if (conn is SerialPortSettings serialSettings)
									{
										return serialSettings.SerialPortName == settings.ComPortName;
									}

									return false;
								})
							.FirstOrDefault();
			}

			PortManager.SelectedPortSettingsChanged += PortManager_PortPropertiesChanged;
			PortManager.RequestDialogOpen += Child_RequestDialogOpen;
			PortManager.RequestDialogClose += Child_RequestDialogClose;

			DeviceList.PropertyChanged += DeviceList_SelectionChanged;

			_portFacade.Closed += OnPortFacadeClosed;
			_portFacade.Opened += OnPortFacadeOpened;
			_portFacade.Invalidated += OnPortFacadeInvalidated;
			_portFacade.ClosedUnexpectedly += OnPortClosedUnexpectedly;


			_usbWatcher = new USBSerialPortWatcher();
			_usbWatcher.PortAdded += OnUsbDeviceAdded;
			_usbWatcher.PortRemoved += OnUsbDeviceRemoved;
			_usbWatcher.Start();

			var assemblyName = Assembly.GetExecutingAssembly().GetName();

			Terminal.AppendLine(string.Format("{0} v{1}", assemblyName.Name, assemblyName.Version));

			OptionsTab.PropertyChanged += OptionsVM_PropertyChanged;
			OptionsTab.LoadFromSettings();

			EnableControls();
			OpenCloseButtonHasFocus = true;

			ErrorReporter.SubmitErrorReportsAsync();

			_appLog.Debug("Exit main window loaded event.");
		}


		private void OnWindowClosing()
		{
			_deviceDatabase = null;
			if (PluginManager == null)
			{
				_appLog.Error("PluginManager is null.");
			}

			PluginManager.SettingsResetEvent -= OnUserSettingsReset;
			PluginManager.StatusMessageReceived -= PluginManager_StatusMessageReceived;

			if (PortFacade?.IsOpen ?? false)
			{
				try
				{
					PortFacade.Close();
				}
				catch (Exception aException) // Don't let a port error stop the program from exiting.
				{
					_appLog.Error("Error closing port at program exit time.", aException);
				}
			}

			WebLinkFilter.RequestDialogOpen -= Child_RequestDialogOpen;

			if (PortManager == null)
			{
				_appLog.Error("PortManager is null.");
			}

			PortManager.SelectedPortSettingsChanged -= PortManager_PortPropertiesChanged;
			PortManager.RequestDialogOpen -= Child_RequestDialogOpen;
			PortManager.RequestDialogClose -= Child_RequestDialogClose;

			ShutdownDatabaseUpdater();
			ShutdownSoftwareUpdater();

			PluginManager.NotifyWindowClosing();

			if (DatabaseUpdater == null)
			{
				_appLog.Error("DatabaseUpdater is null.");
			}

			DatabaseUpdater.RequestDialogOpen -= Child_RequestDialogOpen;
			DatabaseUpdater.RequestDialogClose -= Child_RequestDialogClose;

			var settings = Settings.Default;
			if (settings.ZaberConsoleWindowSettings == null)
			{
				settings.ZaberConsoleWindowSettings = new WindowSettings();
			}

			settings.Save();

			if (_usbWatcher == null)
			{
				_appLog.Error("_usbWatcher is null.");
			}

			_usbWatcher?.Stop();
			_usbWatcher.PortAdded -= OnUsbDeviceAdded;
			_usbWatcher.PortRemoved -= OnUsbDeviceRemoved;
			_usbWatcher?.Dispose();

			_appLog.Info("Cleaning up temp files at exit...");
			TempFileManager.TryDeleteFiles();

			_eventLog.LogEvent("Program closed");
		}

		#endregion

		#region -- API --

		public ZaberPortFacade PortFacade
		{
			get => _portFacade;
			set
			{
				if (null != _portFacade)
				{
					var allDevices = _portFacade.GetDevice(0);
					allDevices.MessageReceived -= AllDevices_MessageReceived;
					allDevices.MessageSent -= AllDevices_MessageSent;
					DetachPortEvents(PortFacade.Port);
					PortFacade.PollingDisableTokenSource.ZeroStateChanged -= PollingDisableTokenSource_ZeroStateChanged;
				}

				_portFacade = value;
				PluginManager.PortFacade = value;
				OptionsTab.PortFacade = value; // Have to pass this manually because options is not a plugin.

				if (null != _portFacade)
				{
					var allDevices = _portFacade.GetDevice(0);
					allDevices.MessageReceived += AllDevices_MessageReceived;
					allDevices.MessageSent += AllDevices_MessageSent;
					AttachPortEvents(PortFacade.Port);
					PortFacade.PollingDisableTokenSource.ZeroStateChanged += PollingDisableTokenSource_ZeroStateChanged;
				}
			}
		}

		public Protocol Protocol
		{
			get => _protocol;
			set
			{
				if (_inProtocolSetter)
				{
					return;
				}

				_inProtocolSetter = true; // Lock out event loops.

				_protocol = value;
				var isAsciiMode = value == Protocol.ASCII;
				_portFacade.Port.IsAsciiMode = isAsciiMode;

				_inProtocolSetter = false;
			}
		}


		public OptionsVM OptionsTab { get; private set; }

		#endregion

		#region -- View-bound properties --

		public ObservableCollection<IDialogViewModel> Dialogs { get; } = new ObservableCollection<IDialogViewModel>();


		public TerminalPluginVM Terminal
		{
			get => _terminal;
			set => Set(ref _terminal, value, nameof(Terminal));
		}


		public PlugInManager PluginManager { get; } = new PlugInManager();


		public PortManagerVM PortManager
		{
			get => _portManager;
			private set => Set(ref _portManager, value, nameof(PortManager));
		}


		// No setter because we hook into DeviceListVM events. Change the content
		// of the existing instance instead of replacing it.
		public DeviceListVM DeviceList { get; } = new DeviceListVM();


		public string OpenCloseButtonLabel
		{
			get => _openCloseButtonLabel;
			set => Set(ref _openCloseButtonLabel, value, nameof(OpenCloseButtonLabel));
		}


		public bool OpenCloseButtonEnabled
		{
			get => _openCloseButtonEnabled;
			set => Set(ref _openCloseButtonEnabled, value, nameof(OpenCloseButtonEnabled));
		}


		public bool OpenCloseButtonHasFocus
		{
			get => _openCloseButtonHasFocus;
			set
			{
				// Assignments to this property should always fire the event, even if the
				// value hasn't changed.
				_openCloseButtonHasFocus = value;
				OnPropertyChanged(nameof(OpenCloseButtonHasFocus));
			}
		}


		public bool IsConnectionOpen
		{
			get => _isConnectionOpen;
			set
			{
				Set(ref _isConnectionOpen, value, nameof(IsConnectionOpen));
			}
		}


		// This property doesn't need to be observable since it isn't expected to change after startup.
		public string WindowTitle { get; set; }


		/// <summary>
		///     Set this to true to pop up the "do not query mode" popup over the device list.
		/// </summary>
		public bool ShowEmptyDeviceListHint
		{
			get => _showEmptyDeviceListHint;
			set => Set(ref _showEmptyDeviceListHint, value, nameof(ShowEmptyDeviceListHint));
		}


		public MarqueeVM StatusBarText { get; } = new MarqueeVM();

		public IndicatorLightVM TxIndicatorVM { get; set; }

		public IndicatorLightVM RxIndicatorVM { get; set; }


		public ICommand OpenCloseButtonCommand => new RelayCommand(_ => OpenOrCloseButton_Click());


		public MenuButtonVM ConnectionMenu { get; }


		/// <summary>
		///     Set to true to programmatically trigger window closing.
		/// </summary>
		public bool ForceWindowClose
		{
			get => _forceWindowClose;
			set => Set(ref _forceWindowClose, value, nameof(ForceWindowClose));
		}


		public ICommand WindowClosingCommand
			=> OnDemandRelayCommand(ref _windowClosingCommand,
									_ => MainWindowClosed?.Invoke(this, null),
									_ => { return IsWindowCloseAllowed; });


		public ICommand WindowLoadedCommand => new RelayCommand(_ => OnWindowLoaded());

		#endregion

		#region -- Helper methods and PortFacade callbacks --

		private void OnUsbDeviceAdded(object aSender) => _dispatcher.Invoke(new Action(() =>
		{
			if (ZaberPortFacadeState.Open != _portFacade.CurrentState)
			{
				PortManager.RefreshSerialPorts();
			}
		}));


		private void OnUsbDeviceRemoved(object aSender) => _dispatcher.Invoke(new Action(() =>
		{
			// We check the PortFacade's state instead of "IsOpen"
			// because "IsOpen" will report "false" when a port
			// has been unplugged without being properly closed. 
			if (ZaberPortFacadeState.Open == _portFacade.CurrentState)
			{
				string portName = null;
				if (_portFacade.Port is RS232Port serialPort)
				{
					portName = serialPort.PortName;
				}

				// Determine if the removed device is the USB serial port currently open.
				if ((null != portName) && !Array.Exists(SerialPort.GetPortNames(), s => s.Equals(portName)))
				{
					// If we get here, our port just disappeared! It was likely unplugged.
					try
					{
						PortDisconnected();
					}
					catch (IOException)
					{
						// Expected: the port has disappeared, and so it "no longer exists."
						// Ignore the exception and carry on.
					}
				}
			}

			PortManager.RefreshSerialPorts();
		}));


		private void PortDisconnected()
		{
			var mbvm = new MessageBoxParams
			{
				Message = "The port has been disconnected. Zaber Console will now close the port.",
				Caption = "COM port disconnected",
				Icon = MessageBoxImage.Exclamation,
				Buttons = MessageBoxButton.OK
			};

			Child_RequestDialogOpen(this, mbvm);

			OpenOrCloseButton_Click();
		}


		private void OnPortClosedUnexpectedly(object sender, EventArgs e) => _dispatcher.Invoke(new Action(() =>
		{
			if (ZaberPortFacadeState.Open == _portFacade.CurrentState)
			{
				PortDisconnected();
			}
		}));


		private void OpenOrCloseButton_Click()
		{
			if (ZaberPortFacadeState.Open == _portFacade.CurrentState)
			{
				try
				{
					PortManager.SaveCurrentSettingsToRecentList();
					_portFacade.Close();

					// All the UI update is done on the portFacade.Closed event.
					_eventLog.LogEvent("Port closed");
				}
				catch (Exception aException)
				{
					_appLog.Error("Error closing port.", aException);
					var mbvm = MessageBoxParams.CreateErrorNotification("Error closing port",
																		"There was an unexpected error closing the port; the program may be in an unstable state. It is recommended that you exit and restart.");
					Child_RequestDialogOpen(this, mbvm);
				}
			}
			else
			{
				SwitchToLatestDatabase();
				DatabaseUpdater.NotifyDatabaseInUse(_deviceDatabase.FilePath);

				if (OptionsTab.EnableDeviceQuery)
				{
					if (OptionsTab.QueryDevicesSilently)
					{
						Terminal.AppendLine("Device queries not shown. (See options tab.)");
						Terminal.Enabled = false;
					}

					OpenCloseButtonLabel = "Scanning...";
					OpenCloseButtonEnabled = false;

					Tuple<Protocol, int> protocolAndBaud = null;

					var worker = BackgroundWorkerManager.CreateWorker();
					worker.DoWork += (aSender, aArgs) =>
					{
						UpdatePortType();

						// Detect and set protocol and baud rate in a background thread so UI will update.
						var rawPort = PortManager.PortFactory.CreateRawPort();
						try
						{
							protocolAndBaud = DeviceDetector.DetectProtocolAndBaudRate(rawPort);
						}
						finally
						{
							rawPort.Close();
							if (rawPort is IDisposable disposable)
							{
								disposable.Dispose();
							}
						}
					};

					worker.RunWorkerCompleted += (aSender, aArgs) =>
					{
						try
						{
							if (aArgs.Error != null)
							{
								_appLog.Error(
									$"Exception opening port {PortManager.SelectedConnection?.Name ?? "<null>"}.",
									aArgs.Error);
								throw aArgs.Error;
							}

							if ((protocolAndBaud.Item1 == Protocol.None) || (protocolAndBaud.Item2 == 0))
							{
								Terminal.AppendLine("Didn't find any devices. Opening port anyway.");
								Protocol = OptionsTab.Protocol;
								PortFacade.BaudRate = OptionsTab.BaudRate;
							}
							else
							{
								Protocol = protocolAndBaud.Item1;
								PortFacade.BaudRate = protocolAndBaud.Item2;
							}

							Open();
						}
						catch (FormatException aFormatException)
						{
							// A FormatException is thrown from DetectProtocolAndBaudRate()
							// when it can't make sense of a message it receives.
							EnableControls(); // Set the openOrClose.Text back.
							HandlePortOpenException(aFormatException);
						}
						catch (MismatchedDevicesException aMismatchException)
						{
							HandlePortOpenException(aMismatchException);
							Open();
						}
						catch (UnauthorizedAccessException aUnAuthException)
						{
							EnableControls();
							HandlePortOpenException(aUnAuthException);
						}
						catch (Exception aException)
						{
							HandlePortOpenException(aException);
						}
						finally
						{
							Terminal.Enabled = true;
							EnableControls();
							OpenCloseButtonHasFocus = true;
							_queryMutex.Set();
						}
					};

					worker.Run();
				}
				else // Do not query devices.
				{
					Protocol = OptionsTab.Protocol;
					PortFacade.BaudRate = OptionsTab.BaudRate;
					Terminal.AppendLine("Devices not queried. (See options tab.)");
					try
					{
						_portFacade.OpenWithoutQuery();
					}
					catch (Exception aException)
					{
						HandlePortOpenException(aException);
					}
				}
			}
		}


		/// <summary>
		///     Helper for <see cref="OpenOrCloseButton_Click" />, which has multiple code paths
		///     that open the port, and which all need to check for the same long list of exception
		///     types. This function displays user-friendly error messages for exception types
		///     we know the cause of.
		/// </summary>
		/// <param name="aException">Exception throw by opening the port.</param>
		private void HandlePortOpenException(Exception aException)
		{
			var mbvm = new MessageBoxParams
			{
				Buttons = MessageBoxButton.OK,
				Icon = MessageBoxImage.Exclamation
			};

			if (aException is FormatException)
			{
				mbvm.Message = "An unexpected message was read.\r\n"
						   + "Please make sure that none of your devices are moving "
						   + "or sending messages, then try again.";
				mbvm.Caption = "Error detecting devices";
			}
			else if (aException is MismatchedDevicesException)
			{
				mbvm.Message = "Detected devices at different baud rates "
						   + "or protocols. Not all of them will appear in the device "
						   + "list.\r\nPlease plug your devices in one at a time, "
						   + "and configure them all to the same protocol and baud rate "
						   + "before trying to chain them together.";
				mbvm.Caption = "Could not show all devices";
			}
			else if (aException is MemberAccessException mae)
			{
				// Out-of-date (ie. built for older .NET versions) 
				// plugins which subscribe to the "Opened" event can 
				// throw exceptions when they try to use some members of
				// the Zaber library.
				mbvm.Message = $"The plug-in \"{mae.Source}\" is "
						   + "incompatible with this version of Zaber "
						   + "Console. It was built using a different version of "
						   + "the .NET framework or a different version of the "
						   + "Zaber library.\r\n\r\nPlease see if there is an "
						   + "updated version of the plugin or remove the "
						   + "plug-in from your plug-ins folder, then restart "
						   + "Zaber Console."
						   + (mae.Message != null ? $"\r\n\r\nThe specific error message is: \"{mae.Message}\"" : "");
				mbvm.Caption = "Incompatible plug-in";
			}
			else if (aException is UnauthorizedAccessException)
			{
				mbvm.Message = "Access to the port is denied. It is "
						   + "likely that another process is already using "
						   + "it.\r\nPlease close the port elsewhere and "
						   + "then try again.";
				mbvm.Caption = "Error opening port";
			}
			else if (aException is IOException)
			{
				mbvm.Message =
					"There was a system error opening the serial port. Please ensure you selected the correct serial port and that the cable is connected. Details of the error can be seen in the Application Log accessible from the Options tab.";
				mbvm.Caption = "I/O Error";
			}
			else if (aException is SocketException)
			{
				mbvm.Message = string.Format(Resources.STR_SOCKET_PORT_ERROR, aException.Message);
				mbvm.Caption = "Network Error";
			}
			else
			{
				throw aException;
			}

			if (null != mbvm)
			{
				Child_RequestDialogOpen(this, mbvm);
			}
		}


		private void Open()
		{
			try
			{
				UpdatePortType();
				_portFacade.Open();

				_eventLog.LogEvent("Port opened", $"Port type = {_portFacade.Port.GetType().Name}");
			}
			catch (MemberAccessException) // Handled by the caller.
			{
				throw;
			}
			catch (Exception aException)
			{
				var message = "Error opening port.\r\n"
						  + "Trying to open port without querying devices.\r\n\r\n"
						  + "Error message: "
						  + aException.Message;
				_appLog.Error(message, aException);

				var mbvm = new MessageBoxParams
				{
					Message = message,
					Caption = "Error opening port.",
					Buttons = MessageBoxButton.OK,
					Icon = MessageBoxImage.Exclamation
				};

				Child_RequestDialogOpen(this, mbvm);

				if (_portFacade.IsOpen)
				{
					try
					{
						_portFacade.Close();
					}
					catch (Exception aSecondException) // Don't follow one error with another caused by the first.
					{
						_appLog.Error(
							"Error closing the port after an error opening the port; may have been caused by previous error.",
							aSecondException);
					}
				}

				_portFacade.OpenWithoutQuery();
			}

			// All the UI update is done on the portFacade.Opened event.
		}


		private void FindDevicesButton_Click()
		{
			var reconfigVM = new ReconfigureDialogVM(PortFacade, PortManager.PortFactory.CreateRawPort());
			Child_RequestDialogOpen(this, reconfigVM);

			if (MessageBoxResult.OK == reconfigVM.Result)
			{
				// Close and reopen the port.
				if (PortFacade.IsOpen)
				{
					PortFacade.Close();
				}

				OpenOrCloseButton_Click();
			}
			else if (MessageBoxResult.No == reconfigVM.Result)
			{
				// We use Ignore to signal that no devices were found.
				// Just close the port and leave it at that.
				if (PortFacade.IsOpen)
				{
					PortFacade.Close();
				}
			}
		}


		private void OnPortFacadeOpened(object aSender, EventArgs aArgs)
		{
			if (!_dispatcher.CheckAccess())
			{
				_dispatcher.BeginInvoke(new Action(() => OnPortFacadeOpened(aSender, aArgs)));
				return;
			}

			_appLog.InfoFormat("Opened port {0}.", PortFacade.Port.PortName);
			_masterUnitOfMeasure = UnitOfMeasure.Data; // Ensure master UOM changes will propagate.

			// Force enable message IDs in ASCII - note that device polling relies on this
			// opened callback being called first to enable IDs.
			if (PortFacade.Port.IsAsciiMode)
			{
				_changeProtocolMenuItem.Title = Resources.LABEL_CHANGE_PROTOCOL_BINARY;

				// RM6579 - Always use message IDs in ASCII if supported.
				PortFacade.AreMessageIdsEnabled = true;
			}
			else
			{
				_changeProtocolMenuItem.Title = Resources.LABEL_CHANGE_PROTOCOL_ASCII;
			}

			var newDeviceList = new ObservableCollection<DeviceVM>();
			var inBootloaderMode = new HashSet<byte>();
			var commandsDefaulted = new HashSet<byte>();
			var devicesWithErrorFlags = new HashSet<byte>();
			var safeModeDetected = false;
			foreach (var conversation in PortFacade.AllConversations)
			{
				// We need to get the device through the PortFacade and not 
				// through the conversation to ensure that we get the 
				// ZaberDevice object for the device itself, not its axis.
				var device = PortFacade.GetConversation(conversation.Device.DeviceNumber).Device;

				// Do not create a separate axis line for single-axis integrated devices.
				if ((1 == device.Axes.Count) && (1 == conversation.Device.AxisNumber) && (MotionType.None != device.DeviceType.MotionType))
				{
					continue;
				}
				
				var view = new DeviceVM(_portFacade, conversation);

				view.RequestDialogOpen += Child_RequestDialogOpen;
				view.RequestDialogClose += Child_RequestDialogClose;

				// No other way to detect binary controllers at this level. :(
				if ((conversation.Device.AxisNumber != 0)
				|| (!PortFacade.Port.IsAsciiMode && conversation.Device.DeviceType.ToString().Contains("+")))
				{
					var deviceType = conversation.Device.DeviceType;
					var supportedPeripherals = _deviceDatabase
						.GetSupportedPeripheralNames(deviceType.DeviceId, deviceType.FirmwareVersion);
					view.SetSupportedPeripherals(supportedPeripherals.ToList());

					// Safe mode applies to devices that don't have the auto-detect capability.
					// This will include all FW6 devices, and FW7 devices that lack the capability.
					safeModeDetected |= (0 == deviceType.PeripheralId) 
					                    && !deviceType.Capabilities.Contains(Capability.AutoDetect);
				}

				newDeviceList.Add(view);
				view.RefreshRequired += UpdateResponse;

				if (device.IsInBootloaderMode)
				{
					inBootloaderMode.Add(device.DeviceNumber);
				}

				var lastFlags = device.LastFlagsReceived ?? string.Empty;
				if (lastFlags.StartsWith("F") && (WarningFlags.PeripheralInactive != lastFlags))
				{
					devicesWithErrorFlags.Add(device.DeviceNumber);
				}

				if (!PortFacade.Port.IsAsciiMode && (0 == device.DeviceType.Commands.Count))
				{
					_appLog.WarnFormat(
						"Binary commands were defaulted for device number {0}; DeviceId = {1}, PeripheralId = {2}.",
						device.DeviceNumber,
						device.DeviceType.DeviceId,
						device.DeviceType.PeripheralId);
					device.DeviceType.Commands = PortFacade.DefaultDeviceType.Commands;
					device.DeviceType.PeripheralId = 0; // Otherwise commands will be filtered out.
					if (!(device is DeviceCollection))
					{
						commandsDefaulted.Add(device.DeviceNumber);
					}
				}
			}

			DeviceList.Devices = newDeviceList;
			DeviceList.SelectedDevice = newDeviceList.FirstOrDefault();
			DeviceList.IsAscii = _portFacade.Port.IsAsciiMode;

			EnableControls();

			var connectionDetails = string.Format("({0}{1})",
												  Enum.GetName(typeof(Protocol), Protocol),
												  _portFacade.BaudRate >= 0 ? $" {_portFacade.BaudRate}" : "");
			PortManager.ShowConnectionDetails(connectionDetails);

			// Notify the user if any devices are stuck in bootloader mode.
			if (inBootloaderMode.Count > 0)
			{
				var inBootloaderModeSorted = inBootloaderMode.OrderBy(d => d).ToArray();

				var sb = new StringBuilder();
				if (1 == inBootloaderModeSorted.Length)
				{
					sb.Append($"Device {inBootloaderModeSorted[0]} is ");
				}
				else
				{
					sb.Append("Devices ");
					for (var i = 0; i < (inBootloaderModeSorted.Length - 1); ++i)
					{
						sb.Append($"{inBootloaderModeSorted[i]}, ");
					}

					sb.Append($" and {inBootloaderModeSorted[inBootloaderModeSorted.Length - 1]} are ");
				}

				sb.AppendLine("in bootloader mode, with a reduced command set. "
						  + "This is most likely the result of an interrupted "
						  + "firmware update attempt.");
				sb.AppendLine();
				sb.AppendLine("Issuing a system reset command may rectify this; "
						  + "If not, try updating the firmware again.");


				var mbvm = new MessageBoxParams
				{
					Message = sb.ToString(),
					Caption = "Devices in invalid mode",
					Buttons = MessageBoxButton.OK,
					Icon = MessageBoxImage.Warning
				};

				Child_RequestDialogOpen(this, mbvm);
			}

			// Notify the user if any devices have error flags.
			if (devicesWithErrorFlags.Count > 0)
			{
				var devicesWithErrorFlagsSorted = devicesWithErrorFlags.OrderBy(f => f).ToArray();

				var sb = new StringBuilder();
				if (1 == devicesWithErrorFlagsSorted.Length)
				{
					sb.Append($"Device {devicesWithErrorFlagsSorted[0]} is ");
				}
				else
				{
					sb.Append("Devices ");
					for (var i = 0; i < (devicesWithErrorFlagsSorted.Length - 1); ++i)
					{
						sb.Append($"{devicesWithErrorFlagsSorted[i]}, ");
					}

					sb.Append($" and {devicesWithErrorFlagsSorted[devicesWithErrorFlagsSorted.Length - 1]} are ");
				}

				sb.AppendLine("returning error flags and may not operate until the error conditions "
						  + "are cleared. Use the 'warnings' command to get a full list of "
						  + "condition flags, and see the "
						  + "[Zaber ASCII protocol manual](https://www.zaber.com/protocol-manual#topic_message_format_warning_flags) "
						  + "for their meanings.");

				var mbvm = new CustomMessageBoxVM(sb.ToString(), "Error flags detected")
				{
					DoNotShowAgainSettings =
						Settings.Default.DoNotShowAgainSettings.FindOrCreate("NotifyDevicesHaveErrorFlags"),
					EnableUrlParsing = true
				};

				mbvm.AddOption("OK", 0);

				Child_RequestDialogOpen(this, mbvm);
			}

			// Notify the user of safe mode.
			if (safeModeDetected)
			{
				var mbvm = new CustomMessageBoxVM(Resources.STR_CONTROLLER_IN_SAFE_MODE, "Controller in safe mode")
				{
					EnableUrlParsing = true,
					DoNotShowAgainSettings =
						Settings.Default.DoNotShowAgainSettings.FindOrCreate("ControllerInSafeMode")
				};

				mbvm.AddOption("OK", 0);

				Child_RequestDialogOpen(this, mbvm);
			}

			// Notify the user of devices with defaulted commands lists.
			if (commandsDefaulted.Count > 0)
			{
				var commandsDefaultedSorted = commandsDefaulted.OrderBy(d => d).ToArray();

				var sb = new StringBuilder();
				if (1 == commandsDefaultedSorted.Length)
				{
					sb.Append($"Device {commandsDefaultedSorted[0]} was ");
				}
				else
				{
					sb.Append("Devices ");
					for (var i = 0; i < (commandsDefaultedSorted.Length - 1); ++i)
					{
						sb.Append($"{commandsDefaultedSorted[i]}, ");
					}

					sb.Append($" and {commandsDefaultedSorted[commandsDefaultedSorted.Length - 1]} were ");
				}

				sb.AppendLine("not found in the Device Database. The lists of commands "
						  + "and settings have been set to include all known binary commands "
						  + "and settings; some may not be applicable and will cause errors if used.");

				var mbvm = new CustomMessageBoxVM(sb.ToString(), "Unrecognized Binary Device(s)");
				mbvm.AddOption("OK", 1);
				mbvm.DoNotShowAgainSettings =
					Settings.Default.DoNotShowAgainSettings.FindOrCreate("NotifyBinaryCommandsDefaulted");

				Child_RequestDialogOpen(this, mbvm);
			}

			// Notify the user of device number collisions.
			if (_portFacade.IsCollisionDetected)
			{
				var mbvm = new MessageBoxParams
				{
					Message =
						"Some devices are using the same device number. Would you like to renumber all devices?",
					Caption = "Open Port",
					Buttons = MessageBoxButton.YesNo,
					Icon = MessageBoxImage.Question
				};

				Child_RequestDialogOpen(this, mbvm);

				if (MessageBoxResult.Yes == mbvm.Result)
				{
					if (_portFacade.Port.IsAsciiMode)
					{
						_portFacade.Port.Send("renumber");
					}
					else
					{
						_portFacade.Port.Send(0, Command.Renumber);
					}
				}
			}

			// Show a tooltip directing the user towards the "Find Devices"
			// button if they have a new set of devices connected.
			if (Settings.Default.LastSeenDevices is null)
			{
				Settings.Default.LastSeenDevices = new StringCollection();
			}

			// Pop up dialog if no devices are shown because querying is disabled.
			var showQueryHint = (DeviceList.Devices.Count == 1) && OptionsTab.DoNotQueryDevices;
			if (showQueryHint)
			{
				ShowEmptyDeviceListHint = true;
			}

			var lastSeenDevices = Settings.Default.LastSeenDevices;
			if (!DeviceList.Devices.Select(vm => vm.DeviceType).SequenceEqual(lastSeenDevices.Cast<string>()))
			{
				lastSeenDevices.Clear();
				foreach (var vm in DeviceList.Devices)
				{
					lastSeenDevices.Add(vm.DeviceType);
				}
			}

			ApplyDevicePollSettings(Settings.Default.DevicePollingEnabled);

			_appLog.Debug("End of portFacade_Opened");
		}


		private void OnPortFacadeInvalidated(object sender, EventArgs e)
		{
			_appLog.InfoFormat("Reopen port {0}.", PortFacade.Port.PortName);

			lock (_invalidationMutex)
			{
				if (null != _invalidationWorker)
				{
					return;
				}

				_invalidationWorker = BackgroundWorkerManager.CreateWorker();
			}

			_invalidationWorker.DoWork += (aSender, aArgs) =>
			{
				_queryMutex.Reset();
				_dispatcher.Invoke(new Action(() => OpenOrCloseButton_Click()));
				Thread.Sleep(1000); // Some devices are slow to reinitialize.
				_dispatcher.BeginInvoke(new Action(() => OpenOrCloseButton_Click()));
				_queryMutex.WaitOne();
			};

			_invalidationWorker.RunWorkerCompleted += (aSender, aArgs) =>
			{
				lock (_invalidationMutex)
				{
					_invalidationWorker = null;
				}
			};

			_invalidationWorker.Run();
		}


		private void OnPortFacadeClosed(object sender, EventArgs e)
		{
			if (!_dispatcher.CheckAccess())
			{
				_dispatcher.BeginInvoke(new Action(() => OnPortFacadeClosed(sender, e)));
				return;
			}

			_appLog.Info("Port was closed.");

			StopPolling();

			_changeProtocolMenuItem.Title = Resources.LABEL_CHANGE_PROTOCOL_ASCII;
			PortManager.ShowConnectionDetails(null);
			PortManager.RefreshSerialPorts();
			EnableControls();

			DeviceList.SelectedDevice = null;
			foreach (DeviceVM view in DeviceList.Devices)
			{
				view.Dispose();
			}

			DeviceList.Devices.Clear();
		}


		private void EnableControls()
		{
			IsConnectionOpen = _portFacade.IsOpen;
			OpenCloseButtonEnabled = null != PortManager.SelectedConnection;
			OpenCloseButtonLabel =
				_portFacade.IsOpen
					? "Cl_ose"
					: "_Open";

			_deviceListUpdateTimer.IsEnabled = _portFacade.IsOpen;
			DeviceList.Enabled = _portFacade.IsOpen;
		}


		// Called when user settings are to be reset.
		private void OnUserSettingsReset(object aSender, EventArgs aArgs)
			=> DeviceList?.ResetColumnVisibility();


		// Called when a plugin logs a status message.
		private void PluginManager_StatusMessageReceived(object aSender, string aMessage, int aDuration)
		{
			if (string.IsNullOrEmpty(aMessage))
			{
				StatusBarText.ClearMessages(aSender);
			}
			else
			{
				StatusBarText.AddMessage(aSender, aMessage, aDuration);
			}
		}


		// Called when the user changes selections in the device list grid.
		private void DeviceList_SelectionChanged(object aSender, PropertyChangedEventArgs aArgs)
		{
			if (aArgs.PropertyName == nameof(DeviceList.SelectedDevice))
			{
				var deviceView = DeviceList.SelectedDevice as DeviceVM;
				_portFacade.SelectedConversation = deviceView?.Conversation;
			}
		}


		// Called when view properties are changed on the options tab.
		private void OptionsVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (nameof(OptionsTab.LogSize) == e.PropertyName)
			{
				Terminal.MaxLength = (int) OptionsTab.LogSize;
			}
		}


		// Called when a DeviceVM raises an event.
		private void UpdateResponse(object sender, EventArgs e)
		{
			if (sender is DeviceVM view)
			{
				lock (_deviceViewsToUpdate)
				{
					_deviceViewsToUpdate.Add(view);
				}
			}
		}


		private void DeviceListUpdateTimer_Tick(object aSender, EventArgs aArgs)
		{
			lock (_deviceViewsToUpdate)
			{
				// Propagate unit changes from devices to commands and settings.
				foreach (var viewBase in _deviceViewsToUpdate)
				{
					var view = viewBase as DeviceVM;
					if ((view == DeviceList.SelectedDevice) && (view.SelectedUnit != _masterUnitOfMeasure))
					{
						_masterUnitOfMeasure = view.SelectedUnit;
						PluginManager.SetProperty(_masterUnitOfMeasure, typeof(UnitOfMeasure));
					}
				}

				_deviceViewsToUpdate.Clear();
			}
		}


		private DeviceVM CurrentView => DeviceList.SelectedDevice;


		private void PortManager_PortPropertiesChanged() => _dispatcher.BeginInvoke(new Action(() =>
		{
			_portTypeChanged = true;
			EnableControls();
		}));


		private void UpdatePortType()
		{
			if (_portTypeChanged)
			{
				_portTypeChanged = false;
				DetachPortEvents(_portFacade.Port);
				_portFacade.Port = PortManager.PortFactory.CreatePort();
				AttachPortEvents(_portFacade.Port);
			}
		}


		private void AttachPortEvents(IZaberPort aPort)
		{
			if (null != aPort)
			{
				aPort.ErrorReceived += Port_ErrorReceived;
				aPort.DataPacketSent += Port_DataPacketSent;
				aPort.DataPacketReceived += Port_DataPacketReceived;
			}
		}


		private void DetachPortEvents(IZaberPort aPort)
		{
			if (null != aPort)
			{
				aPort.ErrorReceived -= Port_ErrorReceived;
				aPort.DataPacketSent -= Port_DataPacketSent;
				aPort.DataPacketReceived -= Port_DataPacketReceived;
			}
		}


		private bool IsWindowCloseAllowed
		{
			get
			{
				var allowClose = true;

				// If the database download is still active, display a popup.
				if (DatabaseUpdater.IsBusy)
				{
					// Don't close the window while the dialog is displaying.
					allowClose = false;

					var isDialogOpen = true;
					var mbvm = new CustomMessageBoxVM(Resources.STR_DOWNLOAD_IN_PROGRESS,
													  "Download in progress") { DialogResult = 0 };
					mbvm.AddOption("Cancel download and exit now", 1);
					mbvm.DialogClosing += (aSender, aArgs) => { isDialogOpen = false; };

					// Start polling for either a cancel or for the download to finish.
					Task.Factory.StartNew(() =>
						 {
							 _dispatcher.BeginInvoke(new Action(() => { Child_RequestDialogOpen(this, mbvm); }));

							 var isDownloadBusy = true;
							 while (isDownloadBusy)
							 {
								 Thread.Sleep(100);

								 // Block until the download thread is dead one way or another.
								 isDownloadBusy = DatabaseUpdater.IsBusy;

								 // If the dialog just closed and the download thread is still active, kill the thread.
								 if (!isDialogOpen && (null != mbvm))
								 {
									 mbvm = null;
									 _appLog.Info("User cancelled database download at program exit.");
									 DatabaseUpdater.Cancel();
								 }
							 }
						 })
					 .ContinueWith(aTask =>
						 {
							 // Automatically close the window when the thread is done.
							 _dispatcher.BeginInvoke(new Action(() => { ForceWindowClose = true; }));
						 });
				}

				if (allowClose)
				{
					// No background thread is in progress; authorize window close.
					OnWindowClosing();
				}

				return allowClose;
			}
		}


		private void PollingDisableTokenSource_ZeroStateChanged(object aSender, CounterTokenEventArgs aArgs)
		{
			ApplyDevicePollSettings(aArgs.IsZero && Settings.Default.DevicePollingEnabled);
		}


		private void ApplyDevicePollSettings(bool aPollEnabled)
		{
			bool actuallyEnabled = false;

			if (aPollEnabled)
			{
				// Polling only supports ASCII for now. Only enable if devices exist.
				if ((PortFacade?.IsOpen ?? false) 
				  && PortFacade.Port.IsAsciiMode
				  && (PortFacade.AllConversations.Count(c => c.Device.IsSingleDevice) > 0))
				{
					actuallyEnabled = true;

					if (_autoDetectWorkflow is null)
					{
						_autoDetectWorkflow = new PeripheralAutodetectWorkflow(PortFacade, DeviceList.Devices.Cast<IDeviceView>())
						{
							PositionPollInterval = Settings.Default.DevicePollingInterval
						};

						_autoDetectWorkflow.Start();
					}
					else
					{
						_autoDetectWorkflow.PositionPollInterval = Settings.Default.DevicePollingInterval;
					}
				}
			}
			else
			{
				StopPolling();
			}

			foreach (var deviceView in DeviceList?.Devices.Cast<IDeviceView>() ?? new IDeviceView[] { })
			{
				deviceView.PollingEnabled = actuallyEnabled;
			}
		}


		private void StopPolling()
		{
			_autoDetectWorkflow?.Stop();
			_autoDetectWorkflow = null;
		}


		private void QuickChangeProtocol()
		{
			var oldState = PortFacade.IsInvalidateEnabled;
			PortFacade.IsInvalidateEnabled = false;

			// Message IDs not used because we don't care about the reply and this
			// is expected to disrupt communications.
			if (PortFacade.Port.IsAsciiMode)
			{
				PortFacade.Conversations.First().Send("tools setcomm 9600 1");
				_eventLog.LogEvent("Switch protocol button used", "To Binary");
			}
			else
			{
				PortFacade.Conversations.First().Send(Command.ConvertToASCII, 115200);
				_eventLog.LogEvent("Switch protocol button used", "To ASCII");
			}

			PortFacade.ClearDeferredInvalidation();
			PortFacade.IsInvalidateEnabled = oldState;
			_appLog.Info("Invalidating port because of user-requested protocol change.");
			PortFacade.Invalidate();
		}

		#region -- Serial message logging --

		private bool PollingMessageFilter(DeviceMessage aMessage)
			=> Settings.Default.ShowPollingMessages || !(_autoDetectWorkflow?.IsPollingMessage(aMessage) ?? false);


		private void Port_DataPacketSent(object aSender, DataPacketEventArgs aArgs)
		{
			TxIndicatorVM.On = true;

			if (!PortFacade.ContainsDevice(aArgs.DataPacket.DeviceNumber))
			{
				Terminal.AppendLine(aArgs.DataPacket.FormatRequest());
			}
		}


		private void Port_DataPacketReceived(object aSender, DataPacketEventArgs aArgs)
		{
			RxIndicatorVM.On = true;

			if (!PortFacade.ContainsDevice(aArgs.DataPacket.DeviceNumber))
			{
				Terminal.AppendLine(aArgs.DataPacket.FormatResponse());
			}
		}


		private void Port_ErrorReceived(object aSender, ZaberPortErrorReceivedEventArgs aArgs)
		{
			Terminal.AppendLine(string.Format("Port error: {0}", aArgs.ErrorType));
			if (null != aArgs.Message)
			{
				Terminal.AppendLine(aArgs.Message);
			}
		}


		private void AllDevices_MessageSent(object aSender, DeviceMessageEventArgs aArgs)
		{
			if (PollingMessageFilter(aArgs.DeviceMessage))
			{
				Terminal.AppendLine(aArgs.DeviceMessage.FormatRequest());
			}
		}


		private void AllDevices_MessageReceived(object aSender, DeviceMessageEventArgs aArgs)
		{
			if (PollingMessageFilter(aArgs.DeviceMessage))
			{
				Terminal.AppendLine(
					aArgs.DeviceMessage.FormatResponse()
						.Replace("responds",
							"                        responds"));
			}
		}

		#endregion

		#endregion

		#region -- Dialog Box Handling --

		// Invoked by child VMs when they want to display a dialog box.
		public void Child_RequestDialogOpen(object aSender, IDialogViewModel aVM)
		{
			// If the dialog box VM is itself able to open other dialogs, handle such messages.
			if (aVM is IDialogClient dlgClient)
			{
				dlgClient.RequestDialogOpen += Child_RequestDialogOpen;
				dlgClient.RequestDialogClose += Child_RequestDialogClose;
			}

			Dialogs.Add(aVM);
			if (null != aVM.Exception)
			{
				throw aVM.Exception;
			}
		}


		public void Child_RequestDialogClose(object aSender, IDialogViewModel aVM)
		{
			if (aVM is IDialogClient dlgClient)
			{
				dlgClient.RequestDialogOpen -= Child_RequestDialogOpen;
				dlgClient.RequestDialogClose -= Child_RequestDialogClose;
			}

			Dialogs.Remove(aVM);
		}

		#endregion

		#region -- Data members --

		private const string CONNECTION_MENU_ICON =
			"pack://application:,,,/ZaberWpfToolbox;component/Resources/Icons/64px-black-bars.png";

		private static readonly ILog _appLog = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Main window");

		private readonly Dispatcher _dispatcher;

		private RelayCommand _windowClosingCommand;
		private bool _forceWindowClose;

		private ZaberPortFacade _portFacade;
		private readonly ManualResetEvent _queryMutex = new ManualResetEvent(false);
		private readonly object _invalidationMutex = new object();
		private IBackgroundWorker _invalidationWorker;
		private PeripheralAutodetectWorkflow _autoDetectWorkflow;

		private readonly HashSet<DeviceVM> _deviceViewsToUpdate = new HashSet<DeviceVM>();
		private UnitOfMeasure _masterUnitOfMeasure = UnitOfMeasure.Data;
		private USBSerialPortWatcher _usbWatcher;
		private DispatcherTimer _deviceListUpdateTimer;

		// Don't change this private value directly. Use the public setter.
		private Protocol _protocol;
		private bool _inProtocolSetter;

		// Storage for view-bound properties.
		private TerminalPluginVM _terminal;
		private PortManagerVM _portManager;
		private bool _portTypeChanged = true;
		private string _openCloseButtonLabel = "_Open";
		private bool _openCloseButtonEnabled;
		private bool _isConnectionOpen;
		private bool _showEmptyDeviceListHint;
		private bool _openCloseButtonHasFocus;
		private ICommand _findDevicesCommand;
		private MenuItemVM _changeProtocolMenuItem;

		#endregion
	}
}

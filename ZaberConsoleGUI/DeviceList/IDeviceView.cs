﻿namespace ZaberConsole.DeviceList
{
	/// <summary>
	///     Abstraction of the view classes used to represent devices in the Device List.
	///     This interface exists to help control coupling outside the Device List.
	/// </summary>
	public interface IDeviceView
	{
		/// <summary>
		///     Address of the device represented.
		/// </summary>
		byte DeviceNumber { get; }

		/// <summary>
		///     Axis number of the device represented - 0 for a controller.
		/// </summary>
		int AxisNumber { get; }


		/// <summary>
		///     Notify the view when device polling is enabled or disabled.
		/// </summary>
		bool PollingEnabled { set; }


		/// <summary>
		///     Update the displayed position of the device in the user-selected units.
		/// </summary>
		/// <param name="aPositionInDeviceUnits">Current position in device units.</param>
		void UpdatePositionInSelectedUnits(decimal? aPositionInDeviceUnits);


		/// <summary>
		///     Notify the view that the device has a pending peripheral type change.
		/// </summary>
		/// <param name="aPendingId">Potential new peripheral ID.</param>
		void SetPendingPeripheralId(int aPendingId);


		/// <summary>
		///     Notify the view that the device has a peripheral connection issue (FZ flag).
		/// </summary>
		bool HasPeripheralConnectionIssue { get; set; }
	}
}

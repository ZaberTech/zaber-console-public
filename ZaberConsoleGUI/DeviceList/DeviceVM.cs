using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows.Input;
using log4net;
using Zaber;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Dialogs;

namespace ZaberConsole.DeviceList
{
	/// <summary>
	///     This class encapsulates all the display logic for a
	///     <see cref="ZaberDevice" /> object. Each row of the device list display grid is
	///     attached to one of these.
	///     Note this class monitors communication within a Conversation and must be
	///     Disposed, ideally immediately upon being unused.
	/// </summary>
	public class DeviceVM : ObservableObject, IDisposable, IDialogClient, IDeviceView
	{
		#region -- Events --

		/// <summary>
		///     Raised when properties have changed as a result of
		///     communication with devices. This is for notifying
		///     non-UI code, not part of UI updates.
		/// </summary>
		public event EventHandler RefreshRequired;

		#endregion
		#region-- Setup --

		/// <summary>
		///     Create a new instance and attach communication events.
		/// </summary>
		/// <param name="aPortFacade">The conversation manager.</param>
		/// <param name="aConversation">The conversation to monitor.</param>
		public DeviceVM(ZaberPortFacade aPortFacade, Conversation aConversation)
		{
			_dispatcher = DispatcherHelper.Dispatcher;
			PortFacade = aPortFacade;
			Conversation = aConversation;

			// Query the device type for legal units.
			IEnumerable<UnitOfMeasure> units = new List<UnitOfMeasure>();
			ZaberDevice deviceToQueryUnits;
			if (null != PortFacade && null != Conversation)
			{
				deviceToQueryUnits = Conversation.Device;
				if ((PortFacade.GetDevice(Conversation.Device.DeviceNumber).Axes.Count == 1)
					&& (deviceToQueryUnits.AxisNumber == 0))
				{
					// Device with single axis - get UoM from axis
					deviceToQueryUnits = Conversation.Device.Axes[0];
				}

				var posSettings = deviceToQueryUnits.DeviceType.Commands.Where(
					cmd => cmd.Command == Command.SetCurrentPosition || cmd.TextCommand == "pos");
				units = deviceToQueryUnits.GetUnitsOfMeasure(MeasurementType.Position, posSettings);
			}

			UnitsOfMeasure = new ObservableCollection<object>(units);
			SelectedUnit = UnitOfMeasure.Data;

			var device = Conversation.Device;
			device.MessageReceived += Device_MessageReceived;
			DeviceFlags = device.LastFlagsReceived ?? WarningFlags.None;

			var port = device.Port;

			// have to check for null in case the port got closed very quickly.
			if (port != null)
			{
				_portForDispose = port;
				port.ErrorReceived += Port_ErrorReceived;
			}

			var format = device.AxisNumber == 0 ? "{0:00}" : "{0:00} axis {1}";
			DeviceNumber = string.Format(CultureInfo.InvariantCulture,
										 format,
										 device.DeviceNumber,
										 device.AxisNumber);

			DeviceType = GetDeviceTypeStr();
			ControllerName = DeviceType;

			ShowQuickMovementButtons = device.IsAxis && device.DeviceType.IsMotionCapable;
			if (ShowQuickMovementButtons)
			{
				QuickControls = new QuickControlsVM(Conversation);
			}

			DeviceMenu = new DeviceMenuVM(PortFacade, Conversation);
			DeviceMenu.RequestDialogOpen += DeviceMenu_RequestDialogOpen;
			DeviceMenu.RequestDialogClose += DeviceMenu_RequestDialogClose;

			_peripheralId = device.DeviceType.PeripheralId;
			_pendingPeripheralId = _peripheralId;
			_deviceIsAutoDetectable = device.DeviceType.Capabilities.Contains(Capability.AutoDetect);
			_axisNumber = device.AxisNumber;

			IsActivated = true;
			ActivateButtonLabel = null;
			AxisInfoMessage = null;
			AxisInfoTooltip = null;
			if (0 == device.DeviceType.PeripheralId)
			{
				IsPeripheralIdSet = false;
				_peripheralName = null;
				CanEditPeripheralName = true;
			}
			else if (_deviceIsAutoDetectable)
			{
				IsPeripheralIdSet = true;
				CanEditPeripheralName = false;
			}
			else // Non-detectable peripheral ID set.
			{
				IsPeripheralIdSet = true;
				CanEditPeripheralName = true;
			}

			if (device.AxisNumber == 0 && device.DeviceType.FirmwareVersion != device.DeviceType.RealFirmwareVersion)
			{
				Notification = $"Assuming Firmware version {device.DeviceType.FirmwareVersion.ToString(NumberFormatInfo.InvariantInfo)} "
					+ $"because the actual version ({device.DeviceType.RealFirmwareVersion.ToString(NumberFormatInfo.InvariantInfo)}) "
					+ $"was not found in\r\nthe device database. Some device features may be shown inaccurately or omitted.\r\n"
					+ "Please switch to Zaber Launcher for up-to-date support of recent products.";
				DisplayNotification = true;
			}
		}


		/// <summary>
		///     Set up the completion list for when users type in a peripheral name to change the ID.
		/// </summary>
		/// <param name="aPeripherals">
		///     Peripheral ID and name pairs. All IDs must be unique
		///     and all names must be unique.
		/// </param>
		public void SetSupportedPeripherals(IEnumerable<Tuple<int, string>> aPeripherals)
		{
			var device = Conversation.Device;

			var newPeripherals = aPeripherals?.ToList();
			if ((null == newPeripherals) || !newPeripherals.Any())
			{
				_peripheralIdMap = null;
				_peripheralNameMap = null;
			}
			else
			{
				_peripheralIdMap = newPeripherals.ToDictionary(t => t.Item2.ToLowerInvariant(), t => t.Item1);
				_peripheralNameMap = newPeripherals.ToDictionary(t => t.Item1, t => t.Item2);

				// If the peripheral name is empty and the controller name is not (multi-axis), 
				// we want the controller name to exclude the peripheral name but include the 
				// firmware version.
				var controllerName = DeviceType.Split('+')[0].Trim();
				if (controllerName.Length > 0)
				{
					ControllerName = controllerName
								 + ", Firmware version "
								 + FormatFirmwareVersion(device.DeviceType.RealFirmwareVersion);
				}
				else
				{
					ControllerName = string.Empty;
				}
			}

			// User cannot edit the peripheral name if peripheral ID is read-only.
			CanEditPeripheralName = !IsPeripheralIdReadOnly();

			// User cannot edit the peripheral name if it is currently set to an auto-detectable ID.
			CanEditPeripheralName &= (0 == device.DeviceType.PeripheralId)
										|| !device.DeviceType.Capabilities.Contains(Capability.AutoDetect);

			ResetPeripheralName();
			OnPropertyChanged(nameof(SupportedPeripheralNames));
			OnPropertyChanged(nameof(ShowPeripheralName));
			OnPropertyChanged(nameof(CanEditPeripheralName));
		}


		internal void ResetPeripheralName() 
			=> PeripheralName = (_peripheralId != 0) ? GetOriginalPeripheralName() : null;


		private string GetOriginalPeripheralName()
		{
			string name = null;
			if (null != Conversation?.Device?.DeviceType)
			{
				var pid = GetRelevantPeripheralId();
				_peripheralNameMap?.TryGetValue(pid, out name);
			}

			return name ?? string.Empty;
		}


		private bool IsPeripheralIdReadOnly()
		{
			if (!_periperalIdIsReadOnly.HasValue)
			{
				var deviceType = Conversation?.Device?.DeviceType;
				if (deviceType != null)
				{
					var pIdSetting = deviceType.Commands
						.FirstOrDefault(c => c is SettingInfo si
										&& (("peripheralid" == si.TextCommand)
										|| ("peripheral.id" == si.TextCommand)
										|| (Command.SetPeripheralID == si.Command)))
									as SettingInfo;

					if (null != pIdSetting)
					{
						_periperalIdIsReadOnly = pIdSetting.IsReadOnlySetting || (Conversation.Device.AccessLevel < pIdSetting.AccessLevel);
					}
					else
					{
						// If there is no peripheral.id setting in the database, treat the peripheral ID as read-only.
						// * For devices not in the database, we don't allow editing the peripheral in the device list anyway.
						// * For integrated peripherals (no setting), we also don't allow editing the peripheral in the device list.
						// * For non-integrated peripherals, the setting is supposed to exist in the database so this case doesn't apply.
						_periperalIdIsReadOnly = true;
					}
				}
			}

			return _periperalIdIsReadOnly ?? false;
		}


		/// <summary>
		///     Gets the conversation around the device that this view represents.
		/// </summary>
		public Conversation Conversation { get; }


		/// <summary>
		///     Reference to the Zaber Port Facade instance, mainly for
		///     querying device units.
		/// </summary>
		protected ZaberPortFacade PortFacade { get; }


		/// <summary>
		///     Trigger the RefreshRequired event.
		/// </summary>
		protected void FireRefreshRequired() => RefreshRequired?.Invoke(this, EventArgs.Empty);


		/// <summary>
		///     Format a data value in another unit of measure.
		/// </summary>
		/// <param name="aData">The raw data value.</param>
		/// <returns>A formatted number in the current unit of measure.</returns>
		protected string FormatData(decimal aData)
		{
			// Enable UOM for single axis devices. 
			// The controller conversation contains no UOM information.
			if (Conversation.Axes.Count == 1)
			{
				return Conversation.Axes[0].Device.FormatData(aData, SelectedUnit);
			}

			return Conversation.Device.FormatData(aData, SelectedUnit);
		}

		#endregion
		#region -- View-Bound Properties --

		/// <summary>
		///     List of all units of measure applicable to this device.
		/// </summary>
		public ObservableCollection<object> UnitsOfMeasure
		{
			get => _applicableUnits;
			set => Set(ref _applicableUnits, value, nameof(UnitsOfMeasure));
		}


		/// <summary>
		///     The currently selected unit of measure.
		/// </summary>
		public UnitOfMeasure SelectedUnit
		{
			get => _selectedUnit;
			set
			{
				var original = _selectedUnit;
				Set(ref _selectedUnit, value, nameof(SelectedUnit));
				if (original != _selectedUnit)
				{
					FireRefreshRequired();
				}
			}
		}


		/// <summary>
		///     The device number (its address in the serial chain).
		/// </summary>
		public string DeviceNumber
		{
			get => _deviceNumber;
			set => Set(ref _deviceNumber, value, nameof(DeviceNumber));
		}


		/// <summary>
		///     A human-readable string describing the device.
		/// </summary>
		public string DeviceType
		{
			get => _deviceType;
			set => Set(ref _deviceType, value, nameof(DeviceType));
		}


		/// <summary>
		///     Whether or not this device is a peripheral that has been activated.
		///     This should only be false on FW7 controllers after unplugging a
		///     peripheral.
		/// </summary>
		public bool IsActivated
		{
			get => _isActivated;
			set => Set(ref _isActivated, value, nameof(IsActivated));
		}


		/// <summary>
		///     Whether or not the peripheral ID is nonzero, if this device is
		///     a peripheral.
		/// </summary>
		public bool IsPeripheralIdSet
		{
			get => _isPeripheralIdSet;
			set => Set(ref _isPeripheralIdSet, value, nameof(IsPeripheralIdSet));
		}


		/// <summary>
		///     Label for the smart peripheral activate button. The button is shown
		///     if this has content and the protocol is ASCII.
		/// </summary>
		public string ActivateButtonLabel
		{
			get => _activateButtonLabel;
			set => Set(ref _activateButtonLabel, value, nameof(ActivateButtonLabel));
		}


		/// <summary>
		///     Tooltip for the smart peripheral activate button.
		/// </summary>
		public string ActivateButtonToolTip
		{
			get => _activateButtonToolTip;
			set => Set(ref _activateButtonToolTip, value, nameof(ActivateButtonToolTip));
		}


		/// <summary>
		///     Informational message about the axis state when disconnected.
		///     The info icon is displayed if this is non-empty.
		/// </summary>
		public string AxisInfoMessage
		{
			get => _axisInfoMessage;
			set => Set(ref _axisInfoMessage, value, nameof(AxisInfoMessage));
		}


		/// <summary>
		///     Shorter form of <see cref="AxisInfoMessage"/>.
		/// </summary>
		public string AxisInfoTooltip
		{
			get => _axisInfoTooltip;
			set => Set(ref _axisInfoTooltip, value, nameof(AxisInfoTooltip));
		}


		/// <summary>
		///     Error message(s) related to this device. Displayed as a ToolTip.
		/// </summary>
		public string ErrorMessage
		{
			get => _errorMessage;
			set => Set(ref _errorMessage, value, nameof(ErrorMessage));
		}


		public string Notification
		{
			get => _notification;
			set => Set(ref _notification, value, nameof(Notification));
		}


		public bool DisplayNotification
		{
			get => _displayNotification;
			set => Set(ref _displayNotification, value, nameof(DisplayNotification));
		}


		/// <summary>
		///     The current position of the device, if known.
		/// </summary>
		public string Position
		{
			get => _position;
			set => Set(ref _position, value, nameof(Position));
		}


		/// <summary>
		///     True if this device should have the quick controls displayed.
		/// </summary>
		public bool ShowQuickMovementButtons
		{
			get => _showQuickMovementButtons;
			set => Set(ref _showQuickMovementButtons, value, nameof(ShowQuickMovementButtons));
		}


		public QuickControlsVM QuickControls
		{
			get => _quickControls;
			set => Set(ref _quickControls, value, nameof(QuickControls));
		}


		public DeviceMenuVM DeviceMenu
		{
			get => _deviceMenu;
			set => Set(ref _deviceMenu, value, nameof(DeviceMenu));
		}


		public ICommand ActivateButtonCommand
			=> OnDemandRelayCommand(ref _activateButtonCommand, _ => OnActivateClicked());


		public ICommand AxisInfoButtonCommand
			=> OnDemandRelayCommand(ref _axisInfoButtonCommand, _ => OnAxisInfoClicked());


		public string DeviceFlags
		{
			get => _deviceFlags;
			set
			{
				Set(ref _deviceFlags, value, nameof(DeviceFlags));
				UpdateDeviceFlagsHelp();
			}
		}


		public string DeviceFlagsHelp
		{
			get => _deviceFlagsHelp;
			set => Set(ref _deviceFlagsHelp, value, nameof(DeviceFlagsHelp));
		}


		/// <summary>
		///     Whether or not to show the editing box for the peripheral name.
		/// </summary>
		public bool ShowPeripheralName => null != _peripheralIdMap;


		/// <summary>
		///     Controls visibility of the axis info icon button.
		/// </summary>
		public bool ShowAxisInfoButton
		{
			get => _showAxisInfoButton;
			set => Set(ref _showAxisInfoButton, value, nameof(ShowAxisInfoButton));
		}


		/// <summary>
		///     Whether or not the user can edit the peripheral ID or name.
		/// </summary>
		public bool CanEditPeripheralName
		{
			get => _canEditPeripheralName;
			private set => Set(ref _canEditPeripheralName, value, nameof(CanEditPeripheralName));
		}


		/// <summary>
		///     Controller part of the device name.
		/// </summary>
		public string ControllerName
		{
			get => _controllerName;
			set => Set(ref _controllerName, value, nameof(ControllerName));
		}


		/// <summary>
		///     Editable peripheral name.
		/// </summary>
		public string PeripheralName
		{
			get => _peripheralName;
			set
			{
				var prev = _peripheralName;
				Set(ref _peripheralName, value, nameof(PeripheralName));
				if (prev != _peripheralName)
				{
					// Typing a new peripheral name can change the activation controls if visible.
					// Make this call asynchronously to prevent infinite recursion.
					_dispatcher.BeginInvoke(UpdatePeripheralConnectionStatus);

					// Note this is doing extra work by regenerating the autocomplete list 
					// with every keystroke, but it's simpler than trying to keep a saved
					// list accurate.
					OnPropertyChanged(nameof(SupportedPeripheralNames));
					OnPropertyChanged(nameof(PreviousPeripheralName));
				}
			} 
		}


		/// <summary>
		///     List of peripheral names supported, where it is legal for the user to select by name,
		/// </summary>
		public ObservableCollection<string> SupportedPeripheralNames
		{
			get
			{
				ObservableCollection<string> result = null;
				if (null != _peripheralIdMap)
				{
					// If the user is typing a number, switch the completion list to
					// allow completing by number.
					if (int.TryParse(PeripheralName, out var dummy))
					{
						result = new ObservableCollection<string>(
							_peripheralNameMap.OrderBy(pair => pair.Key).Select(pair => $"{pair.Key} ({pair.Value})"));
					}
					else
					{
						result = new ObservableCollection<string>(_peripheralNameMap.Values.OrderBy(v => v));
					}
				}

				return result ?? new ObservableCollection<string>();
			}
		}


		public string PreviousPeripheralName => GetOriginalPeripheralName();


		/// <summary>
		///     Change visibility of the peripheral name edit box.
		/// </summary>
		public ICommand CancelChangePeripheralCommand
			=> OnDemandRelayCommand(ref _showChangePeripheralCommand, _ => { ResetPeripheralName(); });


		/// <summary>
		///     Command invoked by the change peripheral context menu entries.
		/// </summary>
		public ICommand ChangePeripheralCommand
			=> OnDemandRelayCommand(ref _changePeripheralCommand, OnChangePeripheral);

		#endregion
		#region --- IDeviceView implementation ---

		/// <inheritdoc cref="IDeviceView.DeviceNumber" />
		byte IDeviceView.DeviceNumber => Conversation.Device.DeviceNumber;


		/// <inheritdoc cref="IDeviceView.AxisNumber" />
		public int AxisNumber => Conversation.Device.AxisNumber;


		/// <inheritdoc cref="IDeviceView.PollingEnabled" />
		public bool PollingEnabled
		{
			get => _pollingEnabled;
			set
			{
				_pollingEnabled = value;
				UpdatePeripheralConnectionStatus();
			}
		}


		/// <inheritdoc cref="IDeviceView.UpdatePositionInSelectedUnits" />
		public void UpdatePositionInSelectedUnits(decimal? aPosition)
			=> Position = aPosition != null ? FormatData(aPosition.Value) : string.Empty;


		/// <inheritdoc cref="IDeviceView.SetPendingPeripheralId" />
		public void SetPendingPeripheralId(int aNewId)
		{
			_pendingPeripheralId = aNewId;
			UpdatePeripheralConnectionStatus();
		}

		/// <inheritdoc cref="IDeviceView.HasPeripheralConnectionIssue" />
		public bool HasPeripheralConnectionIssue
		{
			get { return _hasPeripheralConnectionIssue; }
			set
			{
				if (_hasPeripheralConnectionIssue != value)
				{
					_hasPeripheralConnectionIssue = value;
					UpdatePeripheralConnectionStatus();
				}
			}
		}


		#endregion
		#region --- IDialogClient implementation ---

		/// <summary>
		///     Implementing VM should fire this event when it wants the main window
		///     to open a dialog using the VM it supplies.
		/// </summary>
		public event RequestDialogChange RequestDialogOpen;


		/// <summary>
		///     Implementing VM should fire this event if it wants to programmatically
		///     close the dialog it previously opened.
		/// </summary>
		#pragma warning disable CS0067 // Event is never used.
		public event RequestDialogChange RequestDialogClose;
		#pragma warning restore CS0067

		#endregion
		#region -- Device callbacks --

		private void Port_ErrorReceived(object aSender, ZaberPortErrorReceivedEventArgs aArgs)
		{
			_dispatcher.BeginInvoke(() => { ErrorMessage = $"Port error: {aArgs.ErrorType}"; });

			FireRefreshRequired();
		}


		private void Device_MessageReceived(object aSender, DeviceMessageEventArgs aArgs)
		{
			var device = aSender as ZaberDevice;
			var msg = aArgs.DeviceMessage;

			if ((device is null) || (msg is null))
			{
				return;
			}

			_dispatcher.BeginInvoke(() => DeviceFlags = device?.LastFlagsReceived ?? WarningFlags.None);

			if (aArgs.DeviceMessage.Command == Command.Error)
			{
				var errorNumber = (int)msg.NumericData;
				var errorMessage = Enum.GetName(typeof(ZaberError), errorNumber) ?? $"code = {errorNumber}";

				_dispatcher.BeginInvoke(() =>
				{
					ErrorMessage = $"Error response ({errorMessage}) from device {msg.DeviceNumber}";
				});

				if ((errorNumber == (int) ZaberError.Busy)
				&& _isManualMoveRunning
				&& device.IsSingleDevice)
				{
					if (!DisplayNotification)
					{
						_dispatcher.BeginInvoke(() =>
						{
							Notification = "The manual control knob is turned.\nThat command will not work until you center it.";
							DisplayNotification = true;
						});
					}
				}
			}
			else if (msg.Command == Command.ManualMoveTracking)
			{
				_isManualMoveRunning = true;
			}
			else
			{
				_dispatcher.BeginInvoke(() =>
				{
					ErrorMessage = string.Empty;
				});

				_isManualMoveRunning = false;
			}

			var shouldUpdate = 
				(null != msg.CommandInfo)
				&& msg.CommandInfo.IsCurrentPositionReturned
				&& device.IsSingleDevice
				&& ((device.AxisNumber != 0) || (device.Axes.Count <= 1));

			shouldUpdate |= _isManualMoveRunning | (Command.MoveTracking == msg.Command);

			if (shouldUpdate)
			{
				_positionData = msg.NumericData;
				_dispatcher.BeginInvoke(() =>
				{
					Position = FormatData(_positionData.Value);
				});
			}

			UpdatePeripheralConnectionStatus();
			FireRefreshRequired();
		}

		#endregion
		#region -- Private helpers --

		protected void OnAxisInfoClicked()
		{
			var mbvm = new CustomMessageBoxVM(AxisInfoMessage, "Information", null, "OK");
			mbvm.LoadBitmapIcon(@"pack://application:,,,/ZaberWpfToolbox;component/Resources/Icons/info-big.png");
			RequestDialogOpen?.Invoke(this, mbvm);
		}


		private string GetDeviceTypeStr()
		{
			// try to split string that is formatted as "foo + bar"
			var entries = Conversation.Device.DeviceType.ToString()
								   .Split(new[] { " + " },
										  2,
										  StringSplitOptions.RemoveEmptyEntries);

			var version = Conversation.Device.DeviceType.RealFirmwareVersion;
			string result = null;

			if (Conversation.Device.Port.IsAsciiMode
			&& (Conversation.Device.AxisNumber != 0)
			&& (entries.Length > 1))
			{
				// In ASCII mode, peripheral displayed under multi-axis 
				// device entry.
				result = " + " + entries[1].Trim();
				version = new FirmwareVersion();
			}
			else
			{
				result = Conversation.Device.DeviceType.ToString();
			}

			if ((version.ToInt() > 0) && !result.Contains("Firmware Version"))
			{
				result += ", Firmware Version " + FormatFirmwareVersion(version);
			}

			return result;
		}


		private string FormatFirmwareVersion(FirmwareVersion aVersion)
		{
			var result = $"{aVersion.Major}.{aVersion.Minor.ToString("00")}";
			if (aVersion.Build > 0)
			{
				result += $".{aVersion.Build.ToString("00")}";
			}

			return result;
		}


		private int GetRelevantPeripheralId()
		{
			var pid = Conversation.Device.DeviceType.PeripheralId;
			if ((Conversation.Device.AxisNumber == 0)
			&& (Conversation.Device.DeviceType.PeripheralMap != null)
			&& (Conversation.Device.Axes.Count == 1))
			{
				pid = Conversation.Device.Axes[0].DeviceType.PeripheralId;
			}

			return pid;
		}


		private void OnChangePeripheral(object aText)
		{
			var enteredText = aText as string;
			if (!string.IsNullOrEmpty(enteredText))
			{
				var found = false;
				if (int.TryParse(enteredText, out var id))
				{
					found = true;
				}
				else if (null != _peripheralIdMap)
				{
					found = _peripheralIdMap.TryGetValue(enteredText.ToLowerInvariant(), out id);
					if (!found)
					{
						// Autocomplete from ID number case.
						found = int.TryParse(enteredText.Split()[0], out id);
					}
				}

				OnPropertyChanged(nameof(ShowPeripheralName));

				if (found && (id != GetRelevantPeripheralId()))
				{
					CustomMessageBoxVM mbvm;

					var peripheralIdent = string.Empty;
					if (_peripheralNameMap?.TryGetValue(id, out peripheralIdent) ?? false)
					{
						mbvm = new CustomMessageBoxVM(
							string.Format(Properties.Resources.STR_CONFIRM_PID_CHANGE, peripheralIdent),
							Properties.Resources.STR_DLGTITLE_CONFIRM_PERIPHERAL_CHANGE);
					}
					else
					{
						peripheralIdent = id.ToString();
						mbvm = new CustomMessageBoxVM(
							string.Format(Properties.Resources.STR_CONFIRM_PID_NOT_SUPPORTED, peripheralIdent),
							Properties.Resources.STR_DLGTITLE_CONFIRM_PERIPHERAL_CHANGE);
					}

					mbvm.AddOption(ActivateButtonLabel ?? "Confirm", true);
					mbvm.AddOption("Cancel", false);
					mbvm.LoadBitmapIcon(@"pack://application:,,,/ZaberWpfToolbox;component/Resources/Icons/warning-big.png");
					RequestDialogOpen?.Invoke(this, mbvm);

					if (mbvm.DialogResult is bool ok && ok)
					{
						bool error = false;
						try
						{
							DeviceMessage reply = null;
							if (Conversation.Device.Port.IsAsciiMode)
							{
								// This is Request instead of Send because we have to handle
								// rejection by the device.
								reply = Conversation.Request($"set peripheralid {id}");
							}
							else
							{
								reply = Conversation.Request(Command.SetPeripheralID, id);
							}

							error = reply?.IsError ?? false;
						}
						catch (ErrorResponseException)
						{
							error = true;
						}
						catch (RequestTimeoutException)
						{
							// A timeout is assumed to mean the set peripheralid command worked
							// and triggered the port to reopen, in which case the reply might
							// be lost.
							_log.Debug($"Activate button set peripheral ID command timed out; this is expected.");
						}

						if (error)
						{
							_log.Info("The 'set peripheralid' command was rejected.");
							mbvm = new CustomMessageBoxVM(Properties.Resources.ERR_PID_REJECTED, "Warning", null, "OK");
							mbvm.LoadBitmapIcon(@"pack://application:,,,/ZaberWpfToolbox;component/Resources/Icons/warning-big.png");
							RequestDialogOpen?.Invoke(this, mbvm);
							_dispatcher.BeginInvoke(ResetPeripheralName);
						}

						return;
					}
				}

				ResetPeripheralName();
			}
		}


		private void UpdateDeviceFlagsHelp()
		{
			var result = WarningFlags.GetWarningDescription(DeviceFlags);
			if (null == result)
			{
				result = "Unrecognized flags";
			}

			DeviceFlagsHelp = result;
		}


		// Update peripheral autodetect controls if relevant.
		public void UpdatePeripheralConnectionStatus()
		{
			var isPeripheralIdSet = (0 != _peripheralId);
			var isActivated = true;
			var originalPeripheralName = GetOriginalPeripheralName();
			var peripheralNameToDisplay = (_peripheralId != 0) ? originalPeripheralName : null;
			string activateLabel = null;
			string activateTooltip = null;
			string axisInfoMessage = null;
			string axisInfoTooltip = null;
			var showAxisInfoButton = false;
			var canEditPeripheral = !isPeripheralIdSet || !_deviceIsAutoDetectable || !PollingEnabled;
			canEditPeripheral &= !IsPeripheralIdReadOnly();
			_activateButtonMessage = null;

			// If the currently displayed peripheral name is not what we would set, then
			// the user must be editing it.
			var isEditingPeripheralName = (PeripheralName != peripheralNameToDisplay);
			if (_hasPeripheralConnectionIssue)
			{
				isActivated = false;

				if (0 == _pendingPeripheralId) // Peripheral has been unplugged or dumb peripheral.
				{
					canEditPeripheral = true;
					showAxisInfoButton = true;
					axisInfoTooltip = Properties.Resources.STR_TIP_RECONNECT_PERIPHERAL;
					if (_deviceIsAutoDetectable)
					{
						axisInfoMessage = Properties.Resources.STR_INFO_RECONNECT_PERIPHERAL;
					}
					else
					{
						axisInfoMessage = Properties.Resources.STR_INFO_REACTIVATE_PERIPHERAL;
						activateLabel = Properties.Resources.STR_BUTTON_REACTIVATE;
						activateTooltip = Properties.Resources.STR_TOOLTIP_REACTIVATE;
					}
				}
				else // Different (and smart) peripheral has been plugged in.
				{
					string newPeripheralName = null;
					_peripheralNameMap?.TryGetValue(_pendingPeripheralId, out newPeripheralName);
					if (newPeripheralName is null)
					{
						newPeripheralName = _pendingPeripheralId.ToString(CultureInfo.InvariantCulture);
					}

					peripheralNameToDisplay = newPeripheralName;

					activateLabel = Properties.Resources.STR_BUTTON_ACTIVATE;
					_activateButtonMessage = string.Format(
						Properties.Resources.STR_TOOLTIP_ACTIVATE,
						_axisNumber,
						newPeripheralName,
						originalPeripheralName);
					activateTooltip = Properties.Resources.STR_TIP_ACTIVATION_NEEDED;

					canEditPeripheral = false;
				}
			}
			else if ((null != Conversation) && (0 != Conversation.Device.DeviceNumber) && (PortFacade?.IsOpen ?? false))
			{
				// This block is for when smart peripherals are present but polling is disabled,
				// to try to correctly draw attention to the device state at least initially.

				var device = Conversation.Device;
				var hasFZ = device.LastFlagsReceived?.Contains(WarningFlags.PeripheralInactive) ?? false;

				if (0 == device.AxisNumber)
				{
					// If this is a controller with FZ, display the appropriate info message.
					if (hasFZ)
					{
						axisInfoMessage = Properties.Resources.STR_CONTROLLER_AXIS_DISCONNECTED;
						axisInfoTooltip = Properties.Resources.STR_TIP_CONTROLLER_AXIS_DISCONNECTED;
						isActivated = false;
						canEditPeripheral = true;
						showAxisInfoButton = true;
					}
				}
				else
				{
					if (hasFZ)
					{
						axisInfoMessage = Properties.Resources.STR_AXIS_DISCONNECTED;
						axisInfoTooltip = Properties.Resources.STR_TIP_AXIS_DISCONNECTED;
						isActivated = false;
						canEditPeripheral = true;
					}
				}
			}

			if (isEditingPeripheralName && canEditPeripheral)
			{
				// If the user is typing in a peripheral ID, the activate button should
				// be visible and should apply the new ID.
				isActivated = false;
				activateLabel = Properties.Resources.STR_BUTTON_ACTIVATE;
				activateTooltip = Properties.Resources.STR_TOOLTIP_ACTIVATE_PID;
			}

			if (null == axisInfoMessage)
			{
				if (!isPeripheralIdSet)
				{
					axisInfoMessage = Properties.Resources.STR_INFO_AXIS_DEACTIVATED;
					axisInfoTooltip = Properties.Resources.STR_TIP_AXIS_DEACTIVATED;
				}
				else if (!isActivated)
				{
					axisInfoMessage = Properties.Resources.STR_AXIS_DISCONNECTED;
					axisInfoTooltip = Properties.Resources.STR_TIP_AXIS_DISCONNECTED;
				}
				else // Connected.
				{
					axisInfoMessage = Properties.Resources.STR_INFO_AXIS_CONNECTED;
					axisInfoTooltip = Properties.Resources.STR_TIP_AXIS_CONNECTED;
				}
			}

			_dispatcher.BeginInvoke(() =>
			{
				IsPeripheralIdSet = isPeripheralIdSet;
				IsActivated = isActivated;
				ActivateButtonLabel = activateLabel;
				ActivateButtonToolTip = activateTooltip;
				AxisInfoMessage = axisInfoMessage;
				AxisInfoTooltip = axisInfoTooltip;
				ShowAxisInfoButton = showAxisInfoButton;
				CanEditPeripheralName = canEditPeripheral;

				if (!isEditingPeripheralName)
				{
					_peripheralName = peripheralNameToDisplay;
					OnPropertyChanged(nameof(PeripheralName)); // Prevent recursion in tests.
				}
			});
		}


		protected void OnActivateClicked()
		{
			var computedPeripheralName = (_peripheralId != 0) ? GetOriginalPeripheralName() : null;
			var isEditingPeripheralName = (PeripheralName != computedPeripheralName);

			// If the user is editing the peripheral ID, the activate button should act like
			// pressing enter in the text box.
			if (isEditingPeripheralName && CanEditPeripheralName)
			{
				OnChangePeripheral(PeripheralName);
				return;
			}

			var text = _activateButtonMessage ?? ActivateButtonToolTip;
			if (Properties.Resources.STR_BUTTON_ACTIVATE == ActivateButtonLabel)
			{
				text = Properties.Resources.STR_NEW_CONNECTION + text;
			}

			var mbvm = new CustomMessageBoxVM(text, Properties.Resources.STR_DLGTITLE_CONFIRM_PERIPHERAL_CHANGE);
			mbvm.LoadBitmapIcon(@"pack://application:,,,/ZaberWpfToolbox;component/Resources/Icons/warning-big.png");
			mbvm.AddOption(ActivateButtonLabel, true);
			mbvm.AddOption("Cancel", false);
			RequestDialogOpen?.Invoke(this, mbvm);
			if (mbvm.DialogResult is bool activate && activate)
			{
				bool error = false;

				try
				{
					var reply = Conversation.Request("activate");
					error = reply?.IsError ?? false;
				}
				catch (ErrorResponseException)
				{
					error = true;
				}
				catch (RequestTimeoutException)
				{
					// A timeout is assumed to mean the activate command worked
					// and triggered the port to reopen, in which case the reply might
					// be lost.
					_log.Debug($"Activate button activate command timed out; this is expected.");
				}

				if (error)
				{
					_log.Info("Activate command rejected.");
					mbvm = new CustomMessageBoxVM(Properties.Resources.ERR_PID_REJECTED, "Warning", null, "OK");
					mbvm.LoadBitmapIcon(@"pack://application:,,,/ZaberWpfToolbox;component/Resources/Icons/warning-big.png");
					RequestDialogOpen?.Invoke(this, mbvm);
					_dispatcher.BeginInvoke(ResetPeripheralName);
				}
			}
		}


		private void DeviceMenu_RequestDialogOpen(object aSender, MvvmDialogs.ViewModels.IDialogViewModel aVM) 
			=> RequestDialogOpen?.Invoke(aSender, aVM);


		private void DeviceMenu_RequestDialogClose(object aSender, MvvmDialogs.ViewModels.IDialogViewModel aVM) 
			=> RequestDialogClose?.Invoke(aSender, aVM);

		#endregion
		#region -- IDisposable implementation --

		/// <summary>
		///     Performs application-defined tasks associated with freeing, releasing, or
		///     resetting unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);

			// not necessary here, but might be for derived types.
			GC.SuppressFinalize(this);
		}


		/// <summary>
		///     Performs application-defined tasks associated with freeing, releasing, or
		///     resetting unmanaged resources.
		/// </summary>
		/// <param name="isDisposing">
		///     True if the object is being disposed, and not
		///     garbage collected.
		/// </param>
		protected virtual void Dispose(bool isDisposing)
		{
			if (isDisposing)
			{
				var device = Conversation.Device;
				if (device != null)
				{
					device.MessageReceived -= Device_MessageReceived;
					if (_portForDispose != null)
					{
						_portForDispose.ErrorReceived -= Port_ErrorReceived;
					}
				}
			}
		}

		#endregion
		#region -- Data members --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private readonly IDispatcher _dispatcher;
		private readonly IZaberPort _portForDispose; // To unhook events after port is closed.
		private readonly int _axisNumber;
		private readonly int _peripheralId;
		private readonly bool _deviceIsAutoDetectable;

		private ObservableCollection<object> _applicableUnits;
		private string _deviceNumber;
		private string _deviceType;
		private bool _isActivated;
		private bool _isPeripheralIdSet;
		private string _activateButtonLabel;
		private string _activateButtonToolTip;
		private string _axisInfoMessage;
		private string _axisInfoTooltip;
		private string _errorMessage;
		private string _notification;
		private bool _displayNotification;
		private string _position;
		private UnitOfMeasure _selectedUnit;
		private bool _showQuickMovementButtons;
		private bool? _periperalIdIsReadOnly;

		private QuickControlsVM _quickControls;
		private DeviceMenuVM _deviceMenu;
		private RelayCommand _activateButtonCommand;
		private RelayCommand _axisInfoButtonCommand;

		private bool _isManualMoveRunning;
		private decimal? _positionData;
		private bool _canEditPeripheralName = true;
		private string _deviceFlags = WarningFlags.None;
		private string _deviceFlagsHelp = "No warnings";
		private RelayCommand _showChangePeripheralCommand;
		private RelayCommand _changePeripheralCommand;
		private IDictionary<string, int> _peripheralIdMap;
		private IDictionary<int, string> _peripheralNameMap;
		private string _controllerName = string.Empty;
		private string _peripheralName = string.Empty;
		private int _pendingPeripheralId;
		private bool _hasPeripheralConnectionIssue;
		private bool _pollingEnabled;
		private bool _showAxisInfoButton;
		private string _activateButtonMessage;

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using log4net;
using Zaber;
using ZaberConsole.Dialogs.Calibration;
using ZaberConsole.Dialogs.FirmwareUpdater;
using ZaberConsole.Properties;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Controls;
using ZaberWpfToolbox.Dialogs;

namespace ZaberConsole.DeviceList
{
	/// <summary>
	///		Encapsulates the device context menu on each row of the Device List.
	/// </summary>
	public class DeviceMenuVM : MenuButtonVM, IDialogClient
	{
		public DeviceMenuVM(ZaberPortFacade aFacade, Conversation aConversation)
		{
			_dispatcher = DispatcherHelper.Dispatcher;
			_portFacade = aFacade;
			_conversation = aConversation;
			IconUrl = MENU_ICON;
			RefreshItems();
		}


		public void RefreshItems()
		{
			var items = new List<MenuItemVM>();
			_calibrationMenuItem = null;
			UpdateAlertState(false);

			var device = _conversation?.Device;
			if ((_portFacade != null) && (device != null))
			{
				if (device.CanUpdateFirmware)
				{
					items.Add(new MenuItemVM
					{
						Title = Resources.LABEL_UPDATE_FIRMWARE,
						ToolTip = Resources.TIP_UPDATE_FIRMWARE,
						Command = new RelayCommand(_ => OpenFirmwareDialog())
					});
				}

				if (device.CanStoreCalibration)
				{
					_calibrationMenuItem = new MenuItemVM
					{
						Title = Resources.LABEL_OPEN_CALIBRATION,
						ToolTip = Resources.TIP_OPEN_CALIBRATION,
						Command = new RelayCommand(_ => OpenCalibrationDialog())
					};

					Task = Task.Factory.StartNew(() => CheckCalibrationStatus(_calibrationMenuItem));

					items.Add(_calibrationMenuItem);
				}
			}

			MenuItems = new ObservableCollection<MenuItemVM>(items);
		}

		#region -- IDialogClient implementation --

		/// <inheritdoc cref="IDialogClient.RequestDialogOpen"/>
		public event RequestDialogChange RequestDialogOpen;

		#pragma warning disable CS0067 // Event not used.
		/// <inheritdoc cref="IDialogClient.RequestDialogClose"/>
		public event RequestDialogChange RequestDialogClose;
		#pragma warning restore CS0067

		#endregion

		/// <summary>
		/// Temporary and for testing purposes only.
		/// </summary>
		public Task Task { get; private set; }


		private void OpenFirmwareDialog()
		{
			using (var _ = _portFacade.PollingDisableTokenSource.GetToken())
			{
				var wiz = new FirmwareUpdateDialogVM(_portFacade, _conversation);
				RequestDialogOpen?.Invoke(this, wiz);
				if (wiz.InvalidatePort)
				{
					_log.Info("Invalidating port because of firmware update.");
					_portFacade.Invalidate();
				}
			}
		}


		private void OpenCalibrationDialog()
		{
			RequestDialogOpen?.Invoke(this, new CalibrationDialogVM(_portFacade, _conversation));
			Task = Task.Factory.StartNew(() => CheckCalibrationStatus(_calibrationMenuItem));
		}


		private void CheckCalibrationStatus(MenuItemVM aMenuItem)
		{
			var alertSet = false;
			var device = _conversation.Device;
			if ((device.FirmwareVersion >= 700) || device.IsIntegrated)
			{
				if (!IsCalibrationTablePresent())
				{
					alertSet = true;
					_dispatcher.BeginInvoke(() =>
					{
						UpdateAlertState(true);
						aMenuItem.IconUrl = ALERT_ICON;
						aMenuItem.ToolTip = Resources.TIP_NEEDS_CALIBRATION;
					});
				}
			}
			else if (device.IsController) // FW6 controller; FW6 peripherals ignored.
			{
				if (device.Axes.Any(axis => axis.ShouldBeCalibrated))
				{
					var axesMissingData = new List<int>();
					var calibratedAxes = new HashSet<int>(DeviceHelper.ListCalibratedAxes(_conversation));

					foreach (var axis in device.Axes)
					{
						if (axis.ShouldBeCalibrated && !calibratedAxes.Contains(axis.AxisNumber))
						{
							axesMissingData.Add(axis.AxisNumber);
						}
					}

					if (axesMissingData.Count > 0)
					{
						string message = null;
						if (axesMissingData.Count == 1)
						{
							message = string.Format(Resources.TIP_DEVICE_MENU_CALIBRATION_FW6_CONTROLLER_SINGLE, axesMissingData.First());
						}
						else
						{
							message = string.Format(Resources.TIP_DEVICE_MENU_CALIBRATION_FW6_CONTROLLER_SINGLE,  axesMissingData);
						}

						alertSet = true;
						_dispatcher.BeginInvoke(() =>
						{
							UpdateAlertState(true);
							aMenuItem.IconUrl = ALERT_ICON;
							aMenuItem.ToolTip = message;
						});
					}
				}
			}

			// Clear the alert icon from the menu itself - this assumes no other menu items
			// are being updated in parallel.
			if (!alertSet)
			{
				_dispatcher.BeginInvoke(() =>
				{
					UpdateAlertState(MenuItems.Where(item => item != aMenuItem).Any(item => !string.IsNullOrEmpty(item.IconUrl)));
				});
			}
		}


		private bool IsCalibrationTablePresent()
		{
			try
			{
				var response = _conversation.Request("get calibration.type");
				if (response.NumericData >= 1m)
				{
					return true;
				}
			}
			catch (ErrorResponseException aError)
			{
				_log.Warn("Error response when checking for presence of calibration.", aError);
			}
			catch (TimeoutException aTimeout)
			{
				_log.Warn("Timeout when checking for presence of calibration.", aTimeout);
			}

			return false;
		}


		private void UpdateAlertState(bool aNewState)
		{
			ShowAlert = aNewState;
			ToolTip = aNewState 
				? Properties.Resources.TIP_DEVICE_MENU_ATTENTION 
				: Properties.Resources.TIP_DEVICE_MENU;
		}


		private const string MENU_ICON =
			"pack://application:,,,/ZaberWpfToolbox;component/Resources/Icons/64px-black-cog.png";

		private const string ALERT_ICON =
			"pack://application:,,,/ZaberWpfToolbox;component/Resources/Icons/64px-red-exclamation-circle.png";

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private IDispatcher _dispatcher;

		private readonly ZaberPortFacade _portFacade;
		private readonly Conversation _conversation;

		private MenuItemVM _calibrationMenuItem;
	}
}

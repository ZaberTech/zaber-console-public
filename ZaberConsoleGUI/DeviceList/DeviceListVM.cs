﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Input;
using Zaber.Telemetry;
using ZaberConsole.Properties;
using ZaberWpfToolbox;

namespace ZaberConsole.DeviceList
{
	/// <summary>
	///     ViewModel for the list of devices. Also tracks the currently
	///     selected device.
	/// </summary>
	public class DeviceListVM : ObservableObject
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor. Loads the column visibility settings.
		/// </summary>
		public DeviceListVM()
		{
			var settings = Settings.Default;
			if (null == settings.DeviceListHiddenColumns)
			{
				settings.DeviceListHiddenColumns = new StringCollection();
			}

			// Device number column is always visible.
			SetDefaultColumnVisibility();

			foreach (var name in settings.DeviceListHiddenColumns)
			{
				_columnVisible[name] = false;
			}
		}


		/// <summary>
		///     Resets the visibility state of all device list columns.
		/// </summary>
		public void ResetColumnVisibility()
		{
			// Device number column is always visible.
			SetDefaultColumnVisibility();
			SaveColumnSettings();
		}


		/// <summary>
		///     List of available devices to display. User can either modify
		///     the content or replace it with a new list to update the display.
		/// </summary>
		public ObservableCollection<DeviceVM> Devices
		{
			get => _devices;
			set => Set(ref _devices, value, nameof(Devices));
		}


		/// <summary>
		///     The currently selected device from the Devices list, or null if none selected.
		/// </summary>
		public DeviceVM SelectedDevice
		{
			get => _selectedDevice;
			set
			{
				// Couldn't find an easy way to do this in XAML.
				// This resets the peripheral name to its default when the selected device changes.
				if (!ReferenceEquals(value, _selectedDevice) && _selectedDevice is DeviceVM dvm)
				{
					dvm.ResetPeripheralName();
				}

				Set(ref _selectedDevice, value, nameof(SelectedDevice));
			}
		}


		/// <summary>
		///     Controls whether the device list is interactable or greyed out.
		/// </summary>
		public bool Enabled
		{
			get => _enabled;
			set => Set(ref _enabled, value, nameof(Enabled));
		}


		/// <summary>
		///     Indicates if the current protocol is ASCII or Binary.
		/// </summary>
		public bool IsAscii
		{
			get => _isAscii;
			set => Set(ref _isAscii, value, nameof(IsAscii));
		}


		/// <summary>
		///     Indicates whether a given column is shown.
		/// </summary>
		public IDictionary<string, bool> ColumnVisible => _columnVisible;


		/// <summary>
		///     Context menu command for column headers to show/hide columns.
		/// </summary>
		public ICommand ToggleColumnVisibiltyCommand
		{
			get
			{
				if (null == _toggleColumnVisibiltyCommand)
				{
					_toggleColumnVisibiltyCommand = new RelayCommand(aArg =>
					{
						var columnName = aArg as string;
						if (string.IsNullOrEmpty(columnName))
						{
							return;
						}

						if (!_columnVisible.TryGetValue(columnName, out var oldValue))
						{
							oldValue = true;
						}

						_columnVisible[columnName] = !oldValue;
						OnPropertyChanged(nameof(ColumnVisible));
						SaveColumnSettings();
						_eventLog.LogEvent("Column visibility changed", $"{columnName} = {!oldValue}");
					});
				}

				return _toggleColumnVisibiltyCommand;
			}
		}

		#endregion

		#region -- Non-Public Methods --

		private void SaveColumnSettings()
		{
			var settings = Settings.Default;
			settings.DeviceListHiddenColumns.Clear();
			foreach (var pair in _columnVisible)
			{
				if (!pair.Value)
				{
					settings.DeviceListHiddenColumns.Add(pair.Key);
				}
			}
		}


		private void SetDefaultColumnVisibility()
		{
			_columnVisible["DeviceType"] = true;
			_columnVisible["Position"] = true;
			_columnVisible["Flags"] = true;
			_columnVisible["Units"] = true;
			_columnVisible["QuickControls"] = true;
			OnPropertyChanged(nameof(ColumnVisible));
		}

		#endregion

		#region -- Data --

		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Device list");

		private ObservableCollection<DeviceVM> _devices = new ObservableCollection<DeviceVM>();
		private DeviceVM _selectedDevice;
		private bool _enabled = true;
		private bool _isAscii = true;
		private readonly Dictionary<string, bool> _columnVisible = new Dictionary<string, bool>();
		private ICommand _toggleColumnVisibiltyCommand;

		#endregion
	}
}

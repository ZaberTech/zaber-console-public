﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Zaber;
using Zaber.Telemetry;
using ZaberWpfToolbox;

namespace ZaberConsole.DeviceList
{
	/// <summary>
	///		Encapsulates the callbacks for the quick movement controls on the Device List.
	/// </summary>
	public class QuickControlsVM : ObservableObject
	{
		public QuickControlsVM(Conversation aConversation)
		{
			_dispatcher = DispatcherHelper.Dispatcher;
			_conversation = aConversation;

			// Set default jog distances and move velocities.
			Measurement positionDelta = null;
			Measurement zero = null;
			Measurement velocity = null;

			var motionType = _conversation.Device.DeviceType.MotionType;
			var pos = aConversation.Device.DeviceType.Commands
				.Where(cmd => cmd.Command == Command.ReturnCurrentPosition || (cmd.IsSetting && cmd.Name == "pos"));
			if (!pos.Any(cmd => cmd.ResponseUnit != null && cmd.ResponseUnitScale.HasValue))
			{
				motionType = MotionType.Other;
			}

			switch (motionType)
			{
				case MotionType.Linear:
					positionDelta = new Measurement(10, UnitOfMeasure.Millimeter);
					zero = new Measurement(0, UnitOfMeasure.Millimeter);
					velocity = new Measurement(5, UnitOfMeasure.MillimetersPerSecond);
					break;
				case MotionType.Rotary:
					positionDelta = new Measurement(5, UnitOfMeasure.Degree);
					zero = new Measurement(0, UnitOfMeasure.Degree);
					velocity = new Measurement(1, UnitOfMeasure.DegreesPerSecond);
					break;
				default:
					positionDelta = new Measurement(10000, UnitOfMeasure.Data);
					zero = new Measurement(0, UnitOfMeasure.Data);
					velocity = new Measurement(5000, UnitOfMeasure.Data);
					break;
			}

			JogLeftCommand = new RelayCommand(_ => LastTask = Task.Factory.StartNew(() => SendJogCommand(-positionDelta)));
			JogRightCommand = new RelayCommand(_ => LastTask = Task.Factory.StartNew(() => SendJogCommand(positionDelta)));

			// Attach commands to the quick controls buttons.
			if (_conversation.Device.Port.IsAsciiMode)
			{
				HomeCommand = new RelayCommand(_ => LastTask = Task.Factory.StartNew(() => QuickSendCommand("home")));
				StopCommand = new RelayCommand(_ => LastTask = Task.Factory.StartNew(() => QuickSendCommand("stop")));
				MoveLeftToEndCommand = new RelayCommand(_ => LastTask = Task.Factory.StartNew(() => QuickSendCommand("move min")));
				MoveLeftCommand = 
					new RelayCommand(_ => LastTask = Task.Factory.StartNew(() => QuickSendCommand("move vel", -velocity)));
				MoveRightCommand =
					new RelayCommand(_ => LastTask = Task.Factory.StartNew(() => QuickSendCommand("move vel", velocity)));
				MoveRightToEndCommand =
					new RelayCommand(_ => LastTask = Task.Factory.StartNew(() => QuickSendCommand("move max")));
			}
			else
			{
				HomeCommand = new RelayCommand(_ => LastTask = Task.Factory.StartNew(() => QuickSendCommand(Command.Home)));
				StopCommand = new RelayCommand(_ => LastTask = Task.Factory.StartNew(() => QuickSendCommand(Command.Stop)));
				MoveLeftToEndCommand =
					new RelayCommand(_ => LastTask = Task.Factory.StartNew(() => QuickSendCommand(Command.MoveAbsolute, zero)));
				MoveLeftCommand = new RelayCommand(
					_ => LastTask = Task.Factory.StartNew(() => QuickSendCommand(Command.MoveAtConstantSpeed, -velocity)));
				MoveRightCommand = new RelayCommand(
					_ => LastTask = Task.Factory.StartNew(() => QuickSendCommand(Command.MoveAtConstantSpeed, velocity)));
				MoveRightToEndCommand = new RelayCommand(_ =>
				{
					var maxPos = 0m;
					ShowQuickControlsPopup = false;

					// There is no move to maximum command in binary, so get the max pos and move to that.
					LastTask = Task.Factory.StartNew(() =>
					{
						try
						{
							lock (_queryMutex)
							{
								var message =
									_conversation.Request(Command.ReturnSetting, (int)Command.SetMaximumPosition);
								if ((null != message) && !message.IsError)
								{
									maxPos = message.NumericData;
								}
							}
						}
						catch (ErrorResponseException aException)
						{
							_dispatcher.BeginInvoke(() =>
							{
								QuickControlsPopupMessage = "Failed to get the device's maximum position:"
														+ Environment.NewLine
														+ aException.Response.FormatResponse();
								ShowQuickControlsPopup = true;
							});
						}

						if (maxPos > 0m)
						{
							QuickSendCommand(Command.MoveAbsolute, new Measurement(maxPos, UnitOfMeasure.Data));
						}
					});
				});
			}
		}


		private void QuickSendCommand(string aCommand, Measurement aData = null)
		{
			_dispatcher.BeginInvoke(() => { ShowQuickControlsPopup = false; });

			try
			{
				if (aData is null)
				{
					_conversation.Request(aCommand);
				}
				else
				{
					_conversation.RequestInUnits(aCommand, aData);
				}

				_eventLog.LogEvent("Quick controls used", $"{aCommand} {aData?.ToString() ?? string.Empty}");
			}
			catch (ErrorResponseException aException)
			{
				_dispatcher.BeginInvoke(() =>
				{
					QuickControlsPopupMessage = "The device rejected the command:"
											+ Environment.NewLine
											+ aException.Response.FormatResponse();
					ShowQuickControlsPopup = true;
				});
			}
			catch (ConversationException aException)
			{
				_dispatcher.BeginInvoke(() =>
				{
					QuickControlsPopupMessage = "The command produced an error:"
											+ Environment.NewLine
											+ aException.Message;
					ShowQuickControlsPopup = true;
				});
			}
		}


		private void QuickSendCommand(Command aCommand, Measurement aData = null)
		{
			_dispatcher.BeginInvoke(() => { ShowQuickControlsPopup = false; });

			try
			{
				if (aData is null)
				{
					_conversation.Request(aCommand);
				}
				else
				{
					_conversation.RequestInUnits(aCommand, aData);
				}

				_eventLog.LogEvent("Quick controls used", $"{aCommand} {aData?.ToString() ?? string.Empty}");
			}
			catch (ErrorResponseException aException)
			{
				_dispatcher.BeginInvoke(() =>
				{
					QuickControlsPopupMessage = "The device rejected the command:"
											+ Environment.NewLine
											+ aException.Response.FormatResponse();
					ShowQuickControlsPopup = true;
				});
			}
			catch (ConversationException aException)
			{
				_dispatcher.BeginInvoke(() =>
				{
					QuickControlsPopupMessage = "The command produced an error:"
											+ Environment.NewLine
											+ aException.Message;
					ShowQuickControlsPopup = true;
				});
			}
		}


		private void SendJogCommand(Measurement aDelta)
		{
			_dispatcher.BeginInvoke(() => { ShowQuickControlsPopup = false; });

			try
			{
				lock (_queryMutex)
				{
					// Attempt to calculate an indexed move.
					var newIndex = CalculateNextIndex(aDelta.Value >= 0);

					if (_conversation.Device.Port.IsAsciiMode)
					{
						if (newIndex.HasValue)
						{
							_conversation.Request("move index", newIndex.Value);
							_eventLog.LogEvent("Quick controls used", $"move index {newIndex.Value}");
						}
						else // If not indexed, moved by a position increment.
						{
							_conversation.RequestInUnits("move rel", aDelta);
							_eventLog.LogEvent("Quick controls used", $"move rel {aDelta}");
						}
					}
					else
					{
						if (newIndex.HasValue)
						{
							_conversation.Request(Command.MoveIndex, newIndex.Value);
							_eventLog.LogEvent("Quick controls used", $"{Command.MoveIndex} index {newIndex.Value}");
						}
						else // If not indexed, moved by a position increment.
						{
							_conversation.RequestInUnits(Command.MoveRelative, aDelta);
							_eventLog.LogEvent("Quick controls used", $"{Command.MoveRelative} index {aDelta}");
						}
					}
				}
			}
			catch (ErrorResponseException aException)
			{
				_dispatcher.BeginInvoke(() =>
				{
					QuickControlsPopupMessage = "The device rejected the command:"
											+ Environment.NewLine
											+ aException.Response.FormatResponse();
					ShowQuickControlsPopup = true;
				});
			}
			catch (ConversationException aException)
			{
				_dispatcher.BeginInvoke(() =>
				{
					QuickControlsPopupMessage = "The command produced an error:"
											+ Environment.NewLine
											+ aException.Message;
					ShowQuickControlsPopup = true;
				});
			}
			catch (Exception aException)
			{
				_dispatcher.BeginInvoke(() =>
				{
					QuickControlsPopupMessage = "There was an error executing the command:"
											+ Environment.NewLine
											+ aException.Message;
					ShowQuickControlsPopup = true;
				});
			}
		}


		// Attempts to calculate a new index for indexed moves.
		// Will fail if the device does not have the right settings to do the calculation.
		// Will also deliberately fail for linear devices because their default index size is tiny.
		private int? CalculateNextIndex(bool aForward)
		{
			try
			{
				if (_conversation.Device.Port.IsAsciiMode)
				{
					// Determine if the device has indexed positions.
					var message = _conversation.Request("get motion.index.dist");
					var indexDist = message.NumericData;

					message = _conversation.Request("get limit.cycle.dist");
					var cycleDist = message.NumericData;

					// If indexed and has a cycle, move by indices.
					if ((indexDist > 0m) && (cycleDist > indexDist))
					{
						var delta = aForward ? 1 : -1; // Always move by one index.
						message = _conversation.Request("get motion.index.num");
						var currentIndex = (int)message.NumericData - 1; // 0-base

						var numIndices = (int)Math.Floor(cycleDist / indexDist);
						var newIndex = (currentIndex + delta + numIndices) % numIndices;

						return newIndex + 1;
					}
				}
				else
				{
					// Determine if the device has indexed positions.
					var message = _conversation.Request(Command.ReturnSetting, (int)Command.SetIndexDistance);
					var indexDist = message.NumericData;

					message = _conversation.Request(Command.ReturnSetting, (int)Command.SetCycleDistance);
					var cycleDist = message.NumericData;

					// If indexed and has a cycle, move by indices.
					if ((indexDist > 0m) && (cycleDist > indexDist))
					{
						var delta = aForward ? 1 : -1; // Always move by one index.

						message = _conversation.Request(Command.ReturnCurrentPosition, 0);
						var currentIndex = (int)Math.Round(message.NumericData / indexDist);

						var numIndices = (int)Math.Round(cycleDist / indexDist);
						var newIndex = (currentIndex + delta + numIndices) % numIndices;

						return newIndex + 1;
					}
				}
			}
			catch (ErrorResponseException)
			{
			}

			return null;
		}


		/// <summary>
		///		For testing purposes only.
		/// </summary>
		public Task LastTask { get; private set; }

		#region -- View properties --

		public bool ShowQuickControlsPopup
		{
			get => _showQuickControlsPopup;
			set => Set(ref _showQuickControlsPopup, value, nameof(ShowQuickControlsPopup));
		}


		public string QuickControlsPopupMessage
		{
			get => _quickControlsPopupMessage;
			set => Set(ref _quickControlsPopupMessage, value, nameof(QuickControlsPopupMessage));
		}


		public ICommand MoveLeftToEndCommand
		{
			get => _moveLeftToEndCommand;
			set => Set(ref _moveLeftToEndCommand, value, nameof(MoveLeftToEndCommand));
		}


		public ICommand MoveLeftCommand
		{
			get => _moveLeftCommand;
			set => Set(ref _moveLeftCommand, value, nameof(MoveLeftCommand));
		}


		public ICommand JogLeftCommand
		{
			get => _jogLeftCommand;
			set => Set(ref _jogLeftCommand, value, nameof(JogLeftCommand));
		}


		public ICommand StopCommand
		{
			get => _stopCommand;
			set => Set(ref _stopCommand, value, nameof(StopCommand));
		}


		public ICommand JogRightCommand
		{
			get => _jogRightCommand;
			set => Set(ref _jogRightCommand, value, nameof(JogRightCommand));
		}


		public ICommand MoveRightCommand
		{
			get => _moveRightCommand;
			set => Set(ref _moveRightCommand, value, nameof(MoveRightCommand));
		}


		public ICommand MoveRightToEndCommand
		{
			get => _moveRightToEndCommand;
			set => Set(ref _moveRightToEndCommand, value, nameof(MoveRightToEndCommand));
		}


		public ICommand HomeCommand
		{
			get => _homeCommand;
			set => Set(ref _homeCommand, value, nameof(HomeCommand));
		}

		#endregion


		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Device list");

		private readonly IDispatcher _dispatcher;
		private readonly Conversation _conversation;
		private readonly object _queryMutex = new object();

		private bool _showQuickControlsPopup;
		private string _quickControlsPopupMessage = string.Empty;

		private ICommand _moveLeftToEndCommand = RelayCommand.CanDoNothing;
		private ICommand _moveLeftCommand = RelayCommand.CanDoNothing;
		private ICommand _jogLeftCommand = RelayCommand.CanDoNothing;
		private ICommand _stopCommand = RelayCommand.CanDoNothing;
		private ICommand _jogRightCommand = RelayCommand.CanDoNothing;
		private ICommand _moveRightCommand = RelayCommand.CanDoNothing;
		private ICommand _moveRightToEndCommand = RelayCommand.CanDoNothing;
		private ICommand _homeCommand = RelayCommand.CanDoNothing;
	}
}

﻿using Zaber;

namespace ZaberConsole
{
	internal class UnitOfMeasureView
	{
		#region -- Public Methods & Properties --

		public UnitOfMeasureView(UnitOfMeasure unitOfMeasure)
		{
			UnitOfMeasure = unitOfMeasure;
		}


		public UnitOfMeasure UnitOfMeasure { get; }

		public string Display => UnitOfMeasure.Abbreviation;

		#endregion
	}
}

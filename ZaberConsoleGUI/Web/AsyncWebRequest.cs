﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;

namespace ZaberConsole.Web
{
	/// <summary>
	///     Implements background web data fetching with optional timeouts.
	///     All downloaded data is accumulated in memory, so this might not be suitable
	///     for downloading really large files.
	/// </summary>
	public class AsyncWebRequest
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Create a request with an infinite timeout.
		/// </summary>
		/// <param name="aUrl">The URL to fetch.</param>
		public AsyncWebRequest(string aUrl)
			: this(aUrl, -1)
		{
			Error = false;
			ErrorMessage = string.Empty;
		}


		/// <summary>
		///     Create a request with a custom timeout.
		/// </summary>
		/// <param name="aUrl">The URL to fetch.</param>
		/// <param name="aTimeout">The timeout in milliseconds.</param>
		public AsyncWebRequest(string aUrl, int aTimeout)
		{
			_url = aUrl;
			_timeout = aTimeout;
		}


		/// <summary>
		///     Initiate the request.
		/// </summary>
		public void Begin()
		{
			try
			{
				_waitHandle = new ManualResetEvent(false);

				_request = (HttpWebRequest) WebRequest.Create(_url);
				_request.UserAgent = UserAgent;
				if (_timeout > 0)
				{
					_request.Timeout = _timeout;
				}

				if (!UseProxy)
				{
					_request.Proxy = null;
				}

				if (!string.IsNullOrEmpty(PostData))
				{
					var data = Encoding.ASCII.GetBytes(PostData);
					_request.Method = "POST";
					_request.ContentType = "application/json";
					_request.ContentLength = data.Length;

					using (var stream = _request.GetRequestStream())
					{
						stream.Write(data, 0, data.Length);
					}
				}

				var result = _request.BeginGetResponse(ResponseCallback, null);
				ThreadPool.RegisterWaitForSingleObject(result.AsyncWaitHandle,
													   TimeoutCallback,
													   _request,
													   _timeout,
													   true);
			}
			catch (Exception aException)
			{
				ErrorMessage = $"An error occurred while starting the request. Message: {aException.Message}";
				Error = true;
				IsFinished = true;
			}
		}


		/// <summary>
		///     Abort the request in progress.
		/// </summary>
		public void Abort()
		{
			if (_request != null)
			{
				_request.Abort();
				ErrorMessage = "The request was aborted by the client.";
				Error = true;
				IsFinished = true;
			}
		}


		/// <summary>
		///     Get the response data as a stream, optionally blocking until done.
		///     If there is an error or you call this without blocking before the
		///     request is finished, this will return null.
		/// </summary>
		/// <param name="aBlockUntilDone">Set to true to block until the request is finished.</param>
		/// <returns>
		///     A stream containing the response data, or null if there was an error or
		///     the request is not yet finished.
		/// </returns>
		public Stream GetResponse(bool aBlockUntilDone = false)
		{
			if (aBlockUntilDone)
			{
				_waitHandle?.WaitOne();
			}

			if (IsFinished && !Error)
			{
				if (null != _response)
				{
					_response.Close();
					_response = null;
				}

				if (null != _responseStream)
				{
					_responseStream.Close();
					_responseStream = null;
				}

				_outputBuffer.SetLength(_outputBuffer.Position);
				_outputBuffer.Position = 0L;
				return _outputBuffer;
			}

			return null;
		}


		/// <summary>
		///     Controls whether the request will use the user's default web proxy.
		///     Change this value before calling Begin() - changes made after the request has
		///     started will have no effect. Default value is true, which is the safest mode.
		/// </summary>
		public bool UseProxy { get; set; } = true;


		/// <summary>
		///     Sets an optional user-agent header string to include in sent requests.
		///     Ask Google about the formatting of this string. Default is null.
		/// </summary>
		public string UserAgent { get; set; }


		/// <summary>
		///     Returns true if the entire response has been read into memory
		///     or an error has occurred.
		/// </summary>
		public bool IsFinished { get; private set; }


		/// <summary>
		///     Returns the MIME content-type of the web response, once known.
		/// </summary>
		public string ContentType { get; private set; }


		/// <summary>
		///     Optional data to put in the POST request body. This is expected to be a JSON string.
		/// </summary>
		public string PostData { get; set; }


		/// <summary>
		///     Returns true if an error occurred and an error message is available.
		/// </summary>
		public bool Error { get; private set; }


		/// <summary>
		///     if <cref>Error</cref> is true, there will be a user-readable error
		///     message string available here.
		/// </summary>
		public string ErrorMessage { get; private set; }

		#endregion

		#region -- Non-Public Methods --

		private void ResponseCallback(IAsyncResult aAsyncResult)
		{
			try
			{
				_response = (HttpWebResponse) _request.EndGetResponse(aAsyncResult);
				ContentType = _response.ContentType;
				_responseStream = _response.GetResponseStream();
				_responseStream.BeginRead(_readBuffer, 0, BUFFER_SIZE, ReadCallback, null);
			}
			catch (Exception aException)
			{
				ErrorMessage = $"An error occurred while getting the server response. Message: {aException.Message}";
				Error = true;
				IsFinished = true;
				_waitHandle.Set();
			}
		}


		private void ReadCallback(IAsyncResult aAsyncResult)
		{
			try
			{
				var bytesRead = _responseStream.EndRead(aAsyncResult);
				if (bytesRead > 0)
				{
					_outputBuffer.Write(_readBuffer, 0, bytesRead);
					_responseStream.BeginRead(_readBuffer, 0, BUFFER_SIZE, ReadCallback, null);
				}
				else
				{
					IsFinished = true;
					_waitHandle.Set();
				}
			}
			catch (Exception aException)
			{
				ErrorMessage = $"An error occurred while reading the server response. Message: {aException.Message}";
				Error = true;
				IsFinished = true;
				_waitHandle.Set();
			}
		}


		private void TimeoutCallback(object aState, bool aTimedOut)
		{
			if (aTimedOut)
			{
				((HttpWebRequest) aState).Abort();
				ErrorMessage = "The request timed out.";
				Error = true;
				IsFinished = true;
			}
		}

		#endregion

		#region -- Data --

		private static readonly int BUFFER_SIZE = 4096;


		private readonly string _url;
		private readonly int _timeout = -1;
		private HttpWebRequest _request;
		private HttpWebResponse _response;
		private ManualResetEvent _waitHandle;
		private Stream _responseStream;
		private readonly byte[] _readBuffer = new byte[BUFFER_SIZE];
		private readonly MemoryStream _outputBuffer = new MemoryStream();

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ZaberConsole
{
	/// <summary>
	///     Useful extension methods for Type.
	/// </summary>
	public static class TypeExtensions
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Get the first attribute of a given type.
		/// </summary>
		/// <typeparam name="T">The type of attribute to look for.</typeparam>
		/// <param name="aType">The type to search for attributes</param>
		/// <param name="aInherit">Optional - true to search parent types of aType also. Defaults to false.</param>
		/// <returns>The first matching attribute, or null if none were found.</returns>
		public static T GetFirstAttributeOfType<T>(this Type aType, bool aInherit = false) where T : Attribute
			=> aType.GetAttributesOfType<T>(aInherit).FirstOrDefault();


		/// <summary>
		///     Find all attributes of a given type.
		/// </summary>
		/// <typeparam name="T">Type of the attribute to search for.</typeparam>
		/// <param name="aType">Type to search for attributes.</param>
		/// <param name="aInherit">Optional - true to search parent types of aType also. Defaults to false.</param>
		/// <returns>All of the matching attributes. May be an empty collection.</returns>
		public static IEnumerable<T> GetAttributesOfType<T>(this Type aType, bool aInherit = false) where T : Attribute
		{
			foreach (var attr in aType.GetCustomAttributes(typeof(T), aInherit))
			{
				if (typeof(T).IsAssignableFrom(attr.GetType()))
				{
					yield return attr as T;
				}
			}
		}

		#endregion
	}
}

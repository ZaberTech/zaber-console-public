﻿using System;
using System.IO;
using SevenZip;
using SevenZip.Compression.LZMA;

namespace ZaberConsole
{
	/// <summary>
	///     Static helper class for compressing and decompressing LZMA data.
	/// </summary>
	public static class CompressionHelper
	{
		#region -- Public data --

		/// <summary>
		///     Callback signature for compress/decompress progress reports.
		/// </summary>
		/// <param name="aBytesSoFar">Number of bytes encoded/decoded so far.</param>
		/// <param name="aTotalBytes">Total number of bytes to process.</param>
		public delegate void ProgressReportCallback(long aBytesSoFar, long aTotalBytes);

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Compress a stream of data.
		/// </summary>
		/// <param name="aDecompressedInput">The data to compress.</param>
		/// <param name="aCompressedOutput">Where to store the compressed result.</param>
		/// <param name="aProgressCallback">Optional Progress report callback.</param>
		public static void LzmaCompress(Stream aDecompressedInput, Stream aCompressedOutput,
										ProgressReportCallback aProgressCallback = null)
		{
			ProgressForwarder forwarder = null;
			if (null != aProgressCallback)
			{
				forwarder = new ProgressForwarder(aProgressCallback);
			}

			var encoder = new Encoder();
			encoder.WriteCoderProperties(aCompressedOutput);
			aCompressedOutput.Write(BitConverter.GetBytes(aDecompressedInput.Length), 0, 8);
			encoder.Code(aDecompressedInput, aCompressedOutput, aDecompressedInput.Length, -1, forwarder);
			aCompressedOutput.Flush();
		}


		/// <summary>
		///     Compress one file to another.
		/// </summary>
		/// <param name="aSourcePath">The file to compress. Must exist.</param>
		/// <param name="aDestPath">The file name to compress to. Will be overwritten if it exists.</param>
		/// <param name="aProgressCallback">Optional Progress report callback.</param>
		public static void LzmaCompress(string aSourcePath, string aDestPath,
										ProgressReportCallback aProgressCallback = null)
		{
			using (var source = File.OpenRead(aSourcePath))
			{
				if (File.Exists(aDestPath))
				{
					File.Delete(aDestPath);
				}

				using (var dest = File.OpenWrite(aDestPath))
				{
					LzmaCompress(source, dest, aProgressCallback);
				}
			}
		}


		/// <summary>
		///     Decompress a stream of data.
		/// </summary>
		/// <param name="aCompressedInput">The compressed input data. Must include the header added by <cref>LzmaCompress</cref>.</param>
		/// <param name="aDecompressedOutput">Where to store the decompressed output</param>
		/// <param name="aProgressCallback">Optional Progress report callback.</param>
		/// <remarks>The progress callback is currently never invoked by the LZMA library. A future version may change this.</remarks>
		public static void LzmaDecompress(Stream aCompressedInput, Stream aDecompressedOutput,
										  ProgressReportCallback aProgressCallback = null)
		{
			ProgressForwarder forwarder = null;
			if (null != aProgressCallback)
			{
				forwarder = new ProgressForwarder(aProgressCallback);
			}

			var properties = new byte[5];
			var lengthBytes = new byte[8];
			aCompressedInput.Read(properties, 0, 5);
			aCompressedInput.Read(lengthBytes, 0, 8);
			var fileLength = BitConverter.ToInt64(lengthBytes, 0);

			var decoder = new Decoder();
			decoder.SetDecoderProperties(properties);
			decoder.Code(aCompressedInput, aDecompressedOutput, aCompressedInput.Length, fileLength, forwarder);
			aDecompressedOutput.Flush();
		}


		/// <summary>
		///     Decompress one file to another.
		/// </summary>
		/// <param name="aSourcePath">The file to decompress. Must exist and be LZMA-compressed.</param>
		/// <param name="aDestPath">The file name to decompress to. Will be overwritten if it exists.</param>
		/// <param name="aProgressCallback">Optional Progress report callback.</param>
		/// <remarks>The progress callback is currently never invoked by the LZMA library. A future version may change this.</remarks>
		public static void LzmaDecompress(string aSourcePath, string aDestPath,
										  ProgressReportCallback aProgressCallback = null)
		{
			using (var source = File.OpenRead(aSourcePath))
			{
				if (File.Exists(aDestPath))
				{
					File.Delete(aDestPath);
				}

				using (var dest = File.OpenWrite(aDestPath))
				{
					LzmaDecompress(source, dest, aProgressCallback);
				}
			}
		}

		#endregion

		#region -- Data --

		private class ProgressForwarder : ICodeProgress
		{
			#region -- Public Methods & Properties --

			public ProgressForwarder(ProgressReportCallback aCallback)
			{
				_callback = aCallback;
			}


			public void SetProgress(long inSize, long outSize) => _callback(inSize, outSize);

			#endregion

			#region -- Data --

			private readonly ProgressReportCallback _callback;

			#endregion
		}

		#endregion
	}
}

﻿using System;
using System.IO;
using ZaberConsole.Properties;

namespace ZaberConsole
{
	internal partial class MainWindowVM
	{
		#region -- Public Methods & Properties --

		public SoftwareUpdaterVM SoftwareUpdater { get; private set; }

		#endregion

		#region -- Non-Public Methods --

		private void SetupSoftwareUpdater()
		{
			// Upgrade legacy settings for the database updater.
			var updaterSettings = Settings.Default.SoftwareUpdater;
			if (updaterSettings is null)
			{
				updaterSettings = new SoftwareUpdaterSettings
				{
					// Note this upgrade happens after the database updater settings upgrade.
					AutomaticallyCheckForUpdates = Settings.Default.AllowNetworkAccess
				};
				Settings.Default.SoftwareUpdater = updaterSettings;
			}

			var downloadPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
											@"Zaber Technologies",
											@"Zaber Console",
											@"Downloads");

			SoftwareUpdater = new SoftwareUpdaterVM(downloadPath,
													Configuration.ProgramVersion,
													updaterSettings);

			SoftwareUpdater.RequestDialogOpen += Child_RequestDialogOpen;
			SoftwareUpdater.RequestDialogClose += Child_RequestDialogClose;
			SoftwareUpdater.ShowStatusMessage += PluginManager_StatusMessageReceived;
		}


		private void ShutdownSoftwareUpdater()
		{
			SoftwareUpdater.Cancel();
			SoftwareUpdater.RequestDialogOpen -= Child_RequestDialogOpen;
			SoftwareUpdater.RequestDialogClose -= Child_RequestDialogClose;
			SoftwareUpdater.ShowStatusMessage -= PluginManager_StatusMessageReceived;
			SoftwareUpdater.Shutdown();
		}

		#endregion
	}
}

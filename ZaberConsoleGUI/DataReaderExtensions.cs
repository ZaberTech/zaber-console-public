﻿using System;
using System.Data.SQLite;

namespace ZaberConsole
{
	internal static class DataReaderExtensions
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Determine if a database cursor has a column with a given name. Does not alter the state of the cursor.
		/// </summary>
		/// <param name="aReader">The database cursor to check.</param>
		/// <param name="aColumnName">The name of the column to check.</param>
		/// <returns>True if the column exists.</returns>
		public static bool HasColumn(this SQLiteDataReader aReader, string aColumnName)
		{
			for (var i = 0; i < aReader.FieldCount; ++i)
			{
				if (aReader.GetName(i).Equals(aColumnName, StringComparison.InvariantCultureIgnoreCase))
				{
					return true;
				}
			}

			return false;
		}


		/// <summary>
		///     Determine if a given database column exists and has a non-null value.
		/// </summary>
		/// <param name="aReader">The database cursor that is reading a table. Its state is not altered.</param>
		/// <param name="aColumnName">The name of the column to check.</param>
		/// <returns>True if the column exists and has a non-null value at the current cursor row.</returns>
		public static bool ColumnNotNull(this SQLiteDataReader aReader, string aColumnName)
		{
			if (aReader.HasColumn(aColumnName))
			{
				return !aReader.IsDBNull(aReader.GetOrdinal(aColumnName));
			}

			return false;
		}

		#endregion
	}
}

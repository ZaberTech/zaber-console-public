﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Input;
using Xceed.Wpf.AvalonDock;
using Xceed.Wpf.AvalonDock.Layout.Serialization;

namespace ZaberConsole.Behaviors
{
	// Helper class to serialize and deserialize AvalonDock window layouts. Cooperates intimately
	// with the Plugin Manager.
	// Based on an example by Dirk Bahle:
	// http://www.codeproject.com/Articles/719143/AvalonDock-Tutorial-Part-Load-Save-Layout
	public static class WindowLayoutSerializer
	{
		#region -- Public data --

		public delegate void DeserializeDelegate(string aSerializedData,
												 EventHandler<LayoutSerializationCallbackEventArgs> aResolver);

		#endregion

		#region -- Public Methods & Properties --

		public static ICommand GetLoadLayoutCommand(DependencyObject aElement)
			=> (ICommand) aElement.GetValue(LoadLayoutCommandProperty);


		public static void SetLoadLayoutCommand(DependencyObject aElement, ICommand aValue)
			=> aElement.SetValue(LoadLayoutCommandProperty, aValue);


		public static ICommand GetSaveLayoutCommand(DependencyObject aElement)
			=> (ICommand) aElement.GetValue(SaveLayoutCommandProperty);


		public static void SetSaveLayoutCommand(DependencyObject aElement, ICommand aValue)
			=> aElement.SetValue(SaveLayoutCommandProperty, aValue);

		#endregion

		#region -- Non-Public Methods --

		// Update event handler hooks when the command binding changes.
		private static void OnLoadLayoutCommandChanged(DependencyObject aElement,
													   DependencyPropertyChangedEventArgs aArgs)
		{
			var element = aElement as FrameworkElement;
			if (null == element)
			{
				return;
			}

			element.Loaded -= OnElementLoaded;

			var command = aArgs.NewValue as ICommand;
			if (null != command)
			{
				element.Loaded += OnElementLoaded;
			}
		}


		// Callback invoked on docking manager load.
		private static void OnElementLoaded(object aSender, RoutedEventArgs aArgs)
		{
			var manager = aSender as DockingManager;
			if (null == manager)
			{
				return;
			}

			var command = GetLoadLayoutCommand(manager);
			if ((null != command) && command.CanExecute(manager))
			{
				// Invoke the bound command to load the serialized layout, and provide
				// a callback to process the serialized data.
				Application.Current.Dispatcher.BeginInvoke(new Action(() =>
				{
					command.Execute(new DeserializeDelegate((aSerializedData, aResolver) =>
					{
						// Process the serialized data by feeding it to Avalon's serializer... 
						var sr = new StringReader(aSerializedData);
						var layoutSerializer = new XmlLayoutSerializer(manager);

						// ... which requires a callback to resolve window IDs into VMs. This calls
						// a callback provided by the plugin manager to do the resolution.
						layoutSerializer.LayoutSerializationCallback += aResolver;

						layoutSerializer.Deserialize(sr);
					}));
				}));
			}
		}


		// Re-hook Windows event handlers when the save command binding changes.
		private static void OnSaveLayoutCommandChanged(DependencyObject aElement,
													   DependencyPropertyChangedEventArgs aArgs)
		{
			var element = aElement as FrameworkElement;
			if (null == element)
			{
				return;
			}

			element.Unloaded -= OnElementUnloaded;

			var command = aArgs.NewValue as ICommand;
			if (null != command)
			{
				element.Unloaded += OnElementUnloaded;
			}
		}


		// Save the window layout when the docking manager is unloaded.
		private static void OnElementUnloaded(object aSender, RoutedEventArgs aArgs)
		{
			var manager = aSender as DockingManager;
			if (null == manager)
			{
				return;
			}

			var command = GetSaveLayoutCommand(manager);
			if (null == command)
			{
				return;
			}


			// Serialze the window layout to a string.
			var serializedLayout = string.Empty;
			using (var writer = new StringWriter())
			{
				var ser = new XmlLayoutSerializer(manager);
				ser.Serialize(writer);
				serializedLayout = writer.ToString();
			}

			// Pass the string to the save command.
			if (command.CanExecute(serializedLayout))
			{
				command.Execute(serializedLayout);
			}
		}

		#endregion

		#region -- Data --

		// Command to invoke when the docking manager is loaded, to reload the previous window layout.
		private static readonly DependencyProperty LoadLayoutCommandProperty =
			DependencyProperty.RegisterAttached("LoadLayoutCommand",
												typeof(ICommand),
												typeof(WindowLayoutSerializer),
												new PropertyMetadata(null, OnLoadLayoutCommandChanged));


		// Command to invoke when the docking manager is unloaded, to save its window layout.
		private static readonly DependencyProperty SaveLayoutCommandProperty =
			DependencyProperty.RegisterAttached("SaveLayoutCommand",
												typeof(ICommand),
												typeof(WindowLayoutSerializer),
												new PropertyMetadata(null, OnSaveLayoutCommandChanged));

		#endregion
	}
}

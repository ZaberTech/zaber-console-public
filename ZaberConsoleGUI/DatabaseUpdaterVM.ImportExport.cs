﻿using System;
using System.IO;
using Zaber.Threading;
using ZaberConsole.Properties;
using ZaberWpfToolbox.Dialogs;

namespace ZaberConsole
{
	public partial class DatabaseUpdaterVM
	{
		#region -- Non-Public Methods --

		private void ImportDatabase()
		{
			lock (_syncRoot)
			{
				if (_worker is null)
				{
					_worker = BackgroundWorkerManager.CreateWorker();
				}
				else
				{
					return;
				}
			}

			_worker.DoWork += (aSender, aArgs) =>
			{
				aArgs.Result = null;
				_previousState = CurrentState;

				var sourceFile = PromptForImportExportFile(false);
				if (string.IsNullOrEmpty(sourceFile))
				{
					aArgs.Cancel = true;
					return;
				}

				string tempFile = null;
				_dispatcher.BeginInvoke(() =>
				{
					Progress = 0;
					CurrentState = State.Importing;
				});

				// The LZMA decompresser implementation currently doesn't report progress, so 
				// instead we'll watch the file size from a UI thread timer callback.
				string fileToWatch = null;
				EventHandler timerUpdateCallback = (__, _) =>
				{
					_dispatcher.BeginInvoke(() => { Progress = new FileInfo(fileToWatch).Length; });
				};

				_timer.Interval = TimeSpan.FromMilliseconds(100);
				_timer.Tick += timerUpdateCallback;

				try
				{
					var fInfo = new FileInfo(sourceFile);
					_dispatcher.BeginInvoke(() => { ProgressMaximum = fInfo.Length; });

					var destination = CreateNewDatabaseName();

					// If the selected file is compressed, first decompress it to a temp file.
					if (sourceFile.EndsWith(".lzma"))
					{
						_dispatcher.BeginInvoke(() =>
						{
							// Estimate compression ratio.
							ProgressMaximum = 10 * fInfo.Length;
							CurrentState = State.Decompressing;
						});

						tempFile = Path.GetTempFileName();
						fileToWatch = tempFile;
						_log.Info($"Decompressing imported database from {sourceFile}.");

						_timer.Start();
						CompressionHelper.LzmaDecompress(sourceFile, tempFile);
						_timer.Stop();

						ValidateDatabaseFile(tempFile);

						if (_worker.CancellationPending)
						{
							aArgs.Cancel = true;
							TempFileManager.RegisterForDelete(tempFile);
							TempFileManager.TryDeleteFiles();
							return;
						}

						File.Move(tempFile, destination);

						// Preserve timestamp of database content.
						File.SetLastWriteTimeUtc(destination, File.GetLastWriteTimeUtc(sourceFile));
						_log.Info($"Imported database deployed to {destination}.");
					}
					else
					{
						ValidateDatabaseFile(sourceFile);
						fileToWatch = destination;

						_timer.Start();
						File.Copy(sourceFile, destination, true);
						_timer.Stop();

						_log.Info($"Imported database from {sourceFile} to {destination}.");
					}

					aArgs.Result = destination;
					if (!_worker.CancellationPending)
					{
						_settings.LastImportSettings.Directory = Path.GetDirectoryName(sourceFile);
						_settings.LastImportSettings.Filename = Path.GetFileName(sourceFile);
					}

					_dispatcher.BeginInvoke(() => { Progress = ProgressMaximum; });
				}
				catch (Exception aException)
				{
					_dispatcher.BeginInvoke(() =>
					{
						RequestDialogOpen?.Invoke(this,
												  new CustomMessageBoxVM(
													  "Could not import database from "
												  + sourceFile
												  + "; error message: "
												  + aException.Message,
													  "Failed",
													  null,
													  "OK"));
					});

					_log.Error("Failed to import database from " + sourceFile, aException);
					throw aException;
				}
				finally
				{
					_timer.Stop();
					_timer.Tick -= timerUpdateCallback;
				}
			};

			_worker.RunWorkerCompleted += (aSender, aArgs) =>
			{
				lock (_syncRoot)
				{
					_worker = null;
				}

				if (aArgs.Cancelled)
				{
					_dispatcher.BeginInvoke(() =>
					{
						OnPropertyChanged(nameof(UsingImportedDatabase));
						CurrentState = _previousState;
					});

					ShowStatusMessage?.Invoke(this, Resources.STR_DB_IMPORT_CANCELLED, STATUS_DURATION);
				}
				else if (aArgs.Error != null)
				{
					_log.Error("Database import was aborted by an error.", aArgs.Error);
					_dispatcher.BeginInvoke(() =>
					{
						OnPropertyChanged(nameof(UsingImportedDatabase));
						CurrentState = State.Error;
						ErrorMessage = string.Format(Resources.ERR_DB_IMPORT_FAILED, aArgs.Error.Message);
					});
				}
				else
				{
					var newFile = aArgs.Result as string;
					LatestDatabasePath = newFile;
					_settings.ImportedDatabasePath = newFile;
					_dispatcher.BeginInvoke(() =>
					{
						OnPropertyChanged(nameof(UsingImportedDatabase));
						CurrentState = State.UpdateReady;
					});

					_eventLog.LogEvent("Database imported", $"File timestamp = {File.GetLastWriteTimeUtc(LatestDatabasePath)}");
				}
			};

			_worker.WorkerSupportsCancellation = true;

			_worker.Run();
		}


		private void ExportDatabase()
		{
			lock (_syncRoot)
			{
				if (_worker is null)
				{
					_worker = BackgroundWorkerManager.CreateWorker();
				}
				else
				{
					return;
				}
			}


			_worker.DoWork += (aSender, aArgs) =>
			{
				aArgs.Result = null;
				_previousState = CurrentState;

				var exportPath = PromptForImportExportFile(true);
				if (string.IsNullOrEmpty(exportPath))
				{
					aArgs.Cancel = true;
					return;
				}

				_log.Info($"Exporting current device database to {exportPath}");

				_dispatcher.BeginInvoke(() =>
				{
					ProgressMaximum = new FileInfo(LatestDatabasePath).Length;
					Progress = 0;
					CurrentState = State.Exporting;
				});

				// Rather than reimplement file copying with a progress callback, we'll just
				// watch the size of the destination file in a UI thread timer callback.
				EventHandler timerUpdateCallback = (__, _) =>
				{
					_dispatcher.BeginInvoke(() => { Progress = new FileInfo(exportPath).Length; });
				};

				_timer.Tick += timerUpdateCallback;

				try
				{
					if (exportPath.EndsWith(".lzma"))
					{
						_dispatcher.BeginInvoke(() => { CurrentState = State.Compressing; });

						CompressionHelper.LzmaCompress(LatestDatabasePath,
													   exportPath,
													   (aBytesSoFar, _) =>
													   {
														   if (_worker.CancellationPending)
														   {
															   throw new OperationCanceledException(
																   "Database export was cancelled during compression.");
														   }

														   _dispatcher.BeginInvoke(() => { Progress = aBytesSoFar; });
													   });

						// Copy the timestamp when decompressing a file because we care about when
						// the database content was created.
						File.SetLastWriteTimeUtc(exportPath, File.GetLastWriteTimeUtc(LatestDatabasePath));
					}
					else
					{
						_timer.Start();
						File.Copy(LatestDatabasePath, exportPath, true);
					}

					_dispatcher.BeginInvoke(() => { Progress = ProgressMaximum; });

					if (_worker.CancellationPending)
					{
						aArgs.Cancel = true;
					}
					else
					{
						aArgs.Result = exportPath;
					}
				}
				finally
				{
					_timer.Stop();
					_timer.Tick -= timerUpdateCallback;
				}
			};

			_worker.RunWorkerCompleted += (aSender, aArgs) =>
			{
				lock (_syncRoot)
				{
					_worker = null;
				}

				if (aArgs.Cancelled || aArgs.Error is OperationCanceledException)
				{
					_dispatcher.BeginInvoke(() => { CurrentState = _previousState; });

					ShowStatusMessage?.Invoke(this, Resources.STR_DB_EXPORT_CANCELLED, STATUS_DURATION);
				}
				else if (aArgs.Error != null)
				{
					_log.Error("Database export was aborted by an error:", aArgs.Error);
					_dispatcher.BeginInvoke(() =>
					{
						CurrentState = State.Error;
						ErrorMessage = string.Format(Resources.ERR_DB_EXPORT_FAILED, aArgs.Error.Message);
					});
				}
				else
				{
					var exportPath = aArgs.Result as string;
					_settings.LastExportSettings.Directory = Path.GetDirectoryName(exportPath);
					_settings.LastExportSettings.Filename = Path.GetFileName(exportPath);
					_log.Info("Current database exported to " + exportPath);

					_dispatcher.BeginInvoke(() => { CurrentState = _previousState; });

					_eventLog.LogEvent("Database exported");
				}
			};

			_worker.WorkerSupportsCancellation = true;

			_worker.Run();
		}


		private string PromptForImportExportFile(bool aIsExport)
		{
			var importExportSettings = aIsExport ? _settings.LastExportSettings : _settings.LastImportSettings;

			var dlg = new FileBrowserDialogParams
			{
				Action =
					aIsExport
						? FileBrowserDialogParams.FileActionType.Save
						: FileBrowserDialogParams.FileActionType.Open,
				AddExtension = false,
				CheckFileExists = !aIsExport,
				Filename = importExportSettings.Filename ?? Configuration.DatabaseFilename,
				Filter =
					"Sqlite Databases (*.sqlite)|*.sqlite|Compressed Sqlite Databases (*.sqlite*.lzma)|*.sqlite*.lzma|All files (*.*)|*.*",
				FilterIndex = importExportSettings.FilterIndex,
				InitialDirectory = importExportSettings.Directory,
				Title = (aIsExport ? "Export" : "Import") + " Device Database"
			};

			_dispatcher.Invoke(() => { RequestDialogOpen?.Invoke(this, dlg); });

			string path = null;
			if (dlg.DialogResult.HasValue && dlg.DialogResult.Value)
			{
				path = dlg.Filename;
				importExportSettings.FilterIndex = dlg.FilterIndex;
			}

			return path;
		}

		#endregion
	}
}

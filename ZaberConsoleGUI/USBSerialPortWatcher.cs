﻿using System;
using System.Management;
using System.Runtime.InteropServices;
using log4net;

namespace ZaberConsole
{
	/// <summary>
	///     This class generates events when serial ports are added or removed from the computer
	///     while the program is running.
	/// </summary>
	public class USBSerialPortWatcher : IDisposable
	{
		#region -- Public data --

		/// <summary>
		///     Event handler signature for port added events.
		/// </summary>
		/// <param name="aSender">Object generating the event.</param>
		public delegate void PortAddedHandler(object aSender);

		/// <summary>
		///     Event handler signature for port removed events.
		/// </summary>
		/// <param name="aSender">Object generating the event.</param>
		public delegate void PortRemovedHandler(object aSender);

		#endregion

		#region -- Events --

		/// <summary>
		///     Event fired when a new serial port is added.
		/// </summary>
		public event PortAddedHandler PortAdded;

		/// <summary>
		///     Event fired when a serial port is removed.
		/// </summary>
		public event PortRemovedHandler PortRemoved;

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Initialize the watcher. This does not actually start watching - call <see cref="Start" /> to do that.
		/// </summary>
		public USBSerialPortWatcher()
		{
			var addQuery =
				new WqlEventQuery(
					"SELECT * FROM __InstanceCreationEvent WITHIN 1 WHERE TargetInstance ISA 'Win32_PnPEntity'");
			_deviceAdded = new ManagementEventWatcher(addQuery);
			_deviceAdded.EventArrived += _deviceAdded_EventArrived;

			var removeQuery =
				new WqlEventQuery(
					"SELECT * FROM __InstanceDeletionEvent WITHIN 1 WHERE TargetInstance ISA 'Win32_PnPEntity'");
			_deviceRemoved = new ManagementEventWatcher(removeQuery);
			_deviceRemoved.EventArrived += _deviceRemoved_EventArrived;
		}


		/// <summary>
		///     Begin watching for port add/remove events.
		/// </summary>
		public void Start()
		{
			try
			{
				_deviceAdded.Start();
			}
			catch (Exception aException)
			{
				_deviceAdded.EventArrived -= _deviceAdded_EventArrived;
				FilterException(aException);
			}

			try
			{
				_deviceRemoved.Start();
			}
			catch (Exception aException)
			{
				_deviceAdded.Stop();
				_deviceAdded.EventArrived -= _deviceAdded_EventArrived;
				_deviceRemoved.EventArrived -= _deviceRemoved_EventArrived;
				FilterException(aException);
			}
		}


		/// <summary>
		///     Stop watching for port add/remove events.
		/// </summary>
		public void Stop()
		{
			try
			{
				_deviceAdded.Stop();
				_deviceAdded.EventArrived -= _deviceAdded_EventArrived;

				_deviceRemoved.Stop();
				_deviceRemoved.EventArrived -= _deviceRemoved_EventArrived;
			}
			catch (COMException aException)
			{
				// Rarely, we get some super old-school error from COM-land.
				// None of them are anywhere near well-documented, and we're
				// closing the form anyway. Ignore them.
				_log.Warn("Ignoring exception when stopping WMI queries.", aException);
			}
			catch (InvalidCastException aException)
			{
				// On some computers it appears as if the window closing event occurs
				// on a different thread from window loaded, which shouldn't be possible.
				// When this happens we get an InvalidCastException from COM land.
				// It should be safe to ignore it.
				_log.Warn("Ignoring exception when stopping WMI queries.", aException);
			}
			catch (NullReferenceException aException)
			{
				// This happens when the constructor fails to run for some
				// reason, usually because there's a problem with the Designer
				// code, leaving _deviceChanged uninitialized. It's unlikely 
				// that this will ever happen in released code, but we catch 
				// (and ignore) it here anyway to make *our* lives easier.
				_log.Warn("Ignoring exception when stopping WMI queries.", aException);
			}
			catch (ManagementException aException)
			{
				// Seen on one computer - quota error when doing WMI queries. It
				// happens when starting the queries, but might as well also 
				// handle it here just in case.
				_log.Warn("Ignoring exception when stopping WMI queries.", aException);
			}
		}


		public void Dispose()
		{
			_deviceAdded?.Dispose();
			_deviceRemoved?.Dispose();
		}

		#endregion

		#region -- Non-Public Methods --

		private void _deviceAdded_EventArrived(object sender, EventArrivedEventArgs e) => PortAdded?.Invoke(this);


		private void _deviceRemoved_EventArrived(object sender, EventArrivedEventArgs e) => PortRemoved?.Invoke(this);


		private void FilterException(Exception aException)
		{
			if (aException is COMException || aException is ManagementException || aException is OutOfMemoryException)
			{
				_log.Warn(
					"WMI event watchers not started. Addition or removal of serial ports from the computer will not be detected.",
					aException);
			}
			else
			{
				throw aException;
			}
		}

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(typeof(USBSerialPortWatcher));

		private readonly ManagementEventWatcher _deviceAdded;
		private readonly ManagementEventWatcher _deviceRemoved;

		#endregion
	}
}

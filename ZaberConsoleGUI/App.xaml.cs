﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using log4net;
using Microsoft.Win32;
using Zaber.Telemetry;
using Zaber.Telemetry.Consumers;
using ZaberConsole.Plugins;
using ZaberConsole.Plugins.CommandList;
using ZaberConsole.Properties;
using ZaberWpfToolbox.Dialogs.ErrorReporter;
using ZaberWpfToolbox.Settings;

namespace ZaberConsole
{
	/// <summary>
	///     The main entry point for the application.
	/// </summary>
	public partial class App : Application
	{
		#region -- Non-Public Methods --

		protected override void OnStartup(StartupEventArgs aArgs)
		{
			base.OnStartup(aArgs);

			var executingAssembly = Assembly.GetExecutingAssembly();
			_log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
			#if DEV
			_log.Info("========== Starting DEV.");
			#elif TEST
			_log.Info("========== Starting TEST.");
			#else
			_log.Info("========== Starting.");
			#endif
			_log.Info("Name=" + executingAssembly.FullName);
			_log.Info("Location=" + executingAssembly.Location);

			DispatcherUnhandledException += App_DispatcherUnhandledException;
			AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
			TaskScheduler.UnobservedTaskException += TaskScheduler_UnobservedTaskException;

			var settings = Settings.Default;
			if (settings.IsSettingUpgradeNeeded)
			{
				// try to copy settings values from previous version.
				settings.Upgrade();
				settings.IsSettingUpgradeNeeded = false;
			}

			// Ensure settings helper classes exist.
			if (settings.DoNotShowAgainSettings is null)
			{
				settings.DoNotShowAgainSettings = new DoNotShowAgainSettingsCollection();
			}

			if (Settings.Default.CommandListFavorites is null)
			{
				Settings.Default.CommandListFavorites = new FavoriteCommandSettings();
			}

			// Give the plugin manager the ability to merge resource dictionaries from plugins.
			PlugInManager.ApplicationResourceDictionary = Resources;

			// Treat WPF binding failures as popup errors in test builds.
			#if DEV && TEST
			BindingErrorTracer.SetTrace();
			#endif

			_log.Debug("Creating main window.");
			_mainVM = new MainWindowVM(); // VM must be created before window to ensure window size constraints work.
			_mainVM.MainWindowClosed += OnMainWindowClosed;
			var window = new MainWindow();
			window.DataContext = _mainVM;
			window.Show();
			_mainWindowOpen = true;
			_log.Debug("App start complete.");
		}


		// Invoked when the main windows has finished closing.
		private void OnMainWindowClosed(object sender, EventArgs e)
		{
			_mainWindowOpen = false;

			// Send the last telemetry if this is running inside Zaber.
			Router.Instance.Report();

			new Thread(() =>
			{
				// Wait up to two secnds to give the AvalonDock layout manager a chance to save its
				// window layout. Calling Shutdown() immediately will cause the Unloaded event to not
				// fire on controls in the main window. By waiting in a background thread like this
				// we give that a chance to happen.
				var count = 20;
				while (count > 0)
				{
					if (PlugInManager.WindowLayoutSaved)
					{
						break;
					}

					Thread.Sleep(100);
					--count;
				}

				if (!PlugInManager.WindowLayoutSaved)
				{
					_log.Warn("Window layout was not saved; Windows did not trigger necessary events.");
				}

				Dispatcher.BeginInvoke(new Action(() => { Shutdown(0); }));
			}) { Name = "Window layout save thread" }.Start();
		}

		#endregion


		#region -- Global exception handling --

		private void App_DispatcherUnhandledException(object aSender, DispatcherUnhandledExceptionEventArgs aArgs)
		{
			_log.Error("Unhandled dispatch exception", aArgs.Exception);
			ShowExceptionDialog(aArgs.Exception);
			aArgs.Handled = true;
		}


		private static void CurrentDomain_UnhandledException(object aSender, UnhandledExceptionEventArgs aArgs)
		{
			if (aArgs.ExceptionObject is Exception ex)
			{
				_log.Fatal("Unhandled exception.", ex);
				ShowExceptionDialog(ex);
			}
			else
			{
				_log.FatalFormat("Unhandled exception: {0}", aArgs.ExceptionObject);
				ShowExceptionDialog(new Exception(aArgs.ExceptionObject.ToString()));
			}
		}


		private static void TaskScheduler_UnobservedTaskException(object aSender,
																  UnobservedTaskExceptionEventArgs aArgs)
		{
			_log.Error("Unobserved Task Exception", aArgs.Exception);

			var exceptions = aArgs.Exception.Flatten().InnerExceptions;

			foreach (var exception in exceptions)
			{
				_log.FatalFormat("Unhandled Task exception: {0}", exception);
			}

			ShowExceptionDialog(exceptions);
		}


		private static void ShowExceptionDialog(params Exception[] aExceptions)
			=> ShowExceptionDialog(aExceptions.ToList());


		private static void ShowExceptionDialog(ICollection<Exception> aExceptions)
		{
			if (aExceptions is null || (0 == aExceptions.Count))
			{
				aExceptions = new List<Exception>
				{
					new ArgumentNullException(
						"Application-level exception handler was invoked without any exception info.")
				};
			}

			var aggregateBuilder = new StringBuilder();
			foreach (var exception in aExceptions)
			{
				aggregateBuilder.AppendLine(exception.Message);
				aggregateBuilder.AppendLine(exception.StackTrace);
			}

			var aggregate = aggregateBuilder.ToString();

			if (_mainWindowOpen)
			{
				try
				{
					var errorReporter = new ErrorReporter(aExceptions);
					var errorReportDialog = new ErrorReportDialogVM(errorReporter.Information, true);

					// Errors that occur within Zaber will have the user's identity by default so we can follow up.
					#if DEV
					errorReportDialog.Email = Environment.UserName;
					#endif

					errorReportDialog.SaveErrorReport += ErrorReporter.SaveErrorReport;
					_mainVM.Child_RequestDialogOpen(_mainVM, errorReportDialog);
				}
				catch (Exception aException)
				{
					var sb = new StringBuilder();
					sb.AppendLine("There was an exception when attempting to show the error dialog:");
					sb.AppendLine(aException.Message);
					sb.AppendLine(aException.StackTrace);

					sb.AppendLine("The errors that caused the dialog display were:");
					sb.AppendLine(aggregate);

					_log.Error(sb.ToString());
				}
			}
			else
			{
				var sb = new StringBuilder();
				sb.AppendLine("Error dialog not shown because main window already closed:");
				sb.AppendLine(aggregate);

				_log.Error(sb.ToString());
			}
		}

		#endregion

		#region -- Application-level data --

		public static Uri IconUri
		{
			get
			{
				if (_iconUri is null)
				{
					#if NIGHTLY
					_iconUri = new Uri("pack://application:,,,/ZaberWpfToolbox;component/Resources/Icons/ZaberConsole_nightly.ico");
					#elif DEV
					_iconUri = new Uri("pack://application:,,,/ZaberWpfToolbox;component/Resources/Icons/ZaberConsole_white.ico");
					#elif ALPHA
					_iconUri = new Uri("pack://application:,,,/ZaberWpfToolbox;component/Resources/Icons/ZaberConsole_red.ico");
					#elif TEST
					_iconUri = new Uri("pack://application:,,,/ZaberWpfToolbox;component/Resources/Icons/ZaberConsole_black.ico");
					#else
					_iconUri = new Uri("pack://application:,,,/ZaberWpfToolbox;component/Resources/Icons/ZaberConsole_cyan.ico");
					#endif
				}

				return _iconUri;
			}
		}


		/// <summary>
		///     Returns true if the detected MSIE version is expected to be compatible with
		///     protocol manual helpprofiling.
		/// </summary>
		public static bool IsIeVersionSupported
		{
			get
			{
				if (!_isIeVersionSupported.HasValue)
				{
					// Default to assuming it is not supported, in order to avoid exceptions.
					_isIeVersionSupported = false;

					try
					{
						var key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Internet Explorer");
						var version = key.GetValue("Version") as string;
						var parts = version.Split('.');
						var major = int.Parse(parts[0]);
						var minor = int.Parse(parts[1]);
						if (major >= 11)
						{
							_isIeVersionSupported = true;
						}
						else if ((9 == major) && (minor >= 11))
						{
							// For a while the version registry key deliberately lied to avoid breaking
							// software that depended on IE9. For greater versions, the major
							// version was set to 9 and the minor version was the real major version.
							_isIeVersionSupported = true;
						}
					}
					catch (Exception aException)
					{
						_log.Error("Failed to determine IE version.", aException);
					}
				}

				return _isIeVersionSupported.Value;
			}
		}

		private static ILog _log;
		private static MainWindowVM _mainVM;
		private static Uri _iconUri;
		private static bool _mainWindowOpen;
		private static bool? _isIeVersionSupported;

		#endregion
	}
}

﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;
using System.Diagnostics;
using log4net;

namespace ZaberConsole
{
	/// <summary>
	/// Extension methods for logging.
	/// </summary>
	public static class LogExtensions
	{
		/// <summary>
		/// Log a message at ERROR level in DEV (Zaber internal) builds only.
		/// Does nothing in public builds.
		/// </summary>
		/// <param name="aLogger">Logger instance to use.</param>
		/// <param name="aMessage">Message to log if in a DEV build.</param>
		[Conditional("DEV")]
		public static void DevOnlyError(this ILog aLogger, object aMessage)
			=> aLogger.Error(aMessage);


		/// <summary>
		/// Log a message at ERROR level in DEV (Zaber internal) builds only.
		/// Does nothing in public builds.
		/// </summary>
		/// <param name="aLogger">Logger instance to use.</param>
		/// <param name="aMessage">Message to log if in a DEV build.</param>
		/// <param name="aException">Exception to log with the message.</param>
		[Conditional("DEV")]
		public static void DevOnlyError(this ILog aLogger, object aMessage, Exception aException)
			=> aLogger.Error(aMessage, aException);


		/// <summary>
		/// Log a message at ERROR level in DEV (Zaber internal) builds only.
		/// Does nothing in public builds.
		/// </summary>
		/// <param name="aLogger">Logger instance to use.</param>
		/// <param name="aFormat">Format string for the message to log in DEV builds.</param>
		/// <param name="aArgs">Arguments for the format string.</param>
		[Conditional("DEV")]
		public static void DevOnlyErrorFormat(this ILog aLogger, string aFormat, params object[] aArgs)
			=> aLogger.ErrorFormat(aFormat, aArgs);


		/// <summary>
		/// Log a message at WARN level in DEV (Zaber internal) builds only.
		/// Does nothing in public builds.
		/// </summary>
		/// <param name="aLogger">Logger instance to use.</param>
		/// <param name="aMessage">Message to log if in a DEV build.</param>
		[Conditional("DEV")]
		public static void DevOnlyWarn(this ILog aLogger, object aMessage)
			=> aLogger.Warn(aMessage);


		/// <summary>
		/// Log a message at WARN level in DEV (Zaber internal) builds only.
		/// Does nothing in public builds.
		/// </summary>
		/// <param name="aLogger">Logger instance to use.</param>
		/// <param name="aMessage">Message to log if in a DEV build.</param>
		/// <param name="aException">Exception to log with the message.</param>
		[Conditional("DEV")]
		public static void DevOnlyWarn(this ILog aLogger, object aMessage, Exception aException)
			=> aLogger.Warn(aMessage, aException);


		/// <summary>
		/// Log a message at WARN level in DEV (Zaber internal) builds only.
		/// Does nothing in public builds.
		/// </summary>
		/// <param name="aLogger">Logger instance to use.</param>
		/// <param name="aFormat">Format string for the message to log in DEV builds.</param>
		/// <param name="aArgs">Arguments for the format string.</param>
		[Conditional("DEV")]
		public static void DevOnlyWarnFormat(this ILog aLogger, string aFormat, params object[] aArgs)
			=> aLogger.WarnFormat(aFormat, aArgs);


		/// <summary>
		/// Log a message at INFO level in DEV (Zaber internal) builds only.
		/// Does nothing in public builds.
		/// </summary>
		/// <param name="aLogger">Logger instance to use.</param>
		/// <param name="aMessage">Message to log if in a DEV build.</param>
		[Conditional("DEV")]
		public static void DevOnlyInfo(this ILog aLogger, object aMessage)
			=> aLogger.Info(aMessage);


		/// <summary>
		/// Log a message at INFO level in DEV (Zaber internal) builds only.
		/// Does nothing in public builds.
		/// </summary>
		/// <param name="aLogger">Logger instance to use.</param>
		/// <param name="aMessage">Message to log if in a DEV build.</param>
		/// <param name="aException">Exception to log with the message.</param>
		[Conditional("DEV")]
		public static void DevOnlyInfo(this ILog aLogger, object aMessage, Exception aException)
			=> aLogger.Info(aMessage, aException);


		/// <summary>
		/// Log a message at INFO level in DEV (Zaber internal) builds only.
		/// Does nothing in public builds.
		/// </summary>
		/// <param name="aLogger">Logger instance to use.</param>
		/// <param name="aFormat">Format string for the message to log in DEV builds.</param>
		/// <param name="aArgs">Arguments for the format string.</param>
		[Conditional("DEV")]
		public static void DevOnlyInfoFormat(this ILog aLogger, string aFormat, params object[] aArgs)
			=> aLogger.InfoFormat(aFormat, aArgs);


		/// <summary>
		/// Log a message at DEBUG level in DEV (Zaber internal) builds only.
		/// Does nothing in public builds.
		/// </summary>
		/// <param name="aLogger">Logger instance to use.</param>
		/// <param name="aMessage">Message to log if in a DEV build.</param>
		[Conditional("DEV")]
		public static void DevOnlyDebug(this ILog aLogger, object aMessage)
			=> aLogger.Debug(aMessage);


		/// <summary>
		/// Log a message at DEBUG level in DEV (Zaber internal) builds only.
		/// Does nothing in public builds.
		/// </summary>
		/// <param name="aLogger">Logger instance to use.</param>
		/// <param name="aMessage">Message to log if in a DEV build.</param>
		/// <param name="aException">Exception to log with the message.</param>
		[Conditional("DEV")]
		public static void DevOnlyDebug(this ILog aLogger, object aMessage, Exception aException)
			=> aLogger.Debug(aMessage, aException);


		/// <summary>
		/// Log a message at DEBUG level in DEV (Zaber internal) builds only.
		/// Does nothing in public builds.
		/// </summary>
		/// <param name="aLogger">Logger instance to use.</param>
		/// <param name="aFormat">Format string for the message to log in DEV builds.</param>
		/// <param name="aArgs">Arguments for the format string.</param>
		[Conditional("DEV")]
		public static void DevOnlyDebugFormat(this ILog aLogger, string aFormat, params object[] aArgs)
			=> aLogger.DebugFormat(aFormat, aArgs);


	}
}
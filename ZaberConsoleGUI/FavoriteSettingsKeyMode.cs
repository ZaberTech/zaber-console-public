﻿using System.ComponentModel;

namespace ZaberConsole
{
	/// <summary>
	///     Modes for how favorites are saved in user settings.
	/// </summary>
	public enum FavoriteSettingsKeyMode
	{
		/// <summary>
		///     Single list of favorites for all devices.
		/// </summary>
		[Description("together (same for all devices)")]
		Global,

		/// <summary>
		///     Save by device type or peripheral type.
		/// </summary>
		[Description("per device type")]
		ByType,

		/// <summary>
		///     Save by serial number if known, or fall back to device type if not.
		/// </summary>
		[Description("per device")]
		BySerialOrType
	}
}

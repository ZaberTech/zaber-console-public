﻿using System;
using ZaberWpfToolbox.Dialogs;

namespace ZaberConsole
{
	/// <summary>
	///     User settings class for persisting database updater settings
	///     and last updated information.
	/// </summary>
	[Serializable]
	public class DatabaseUpdaterSettings
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     The program is allowed to automatically check for database updates
		///     online at suitable moments. This does not imply automatically downloading them.
		/// </summary>
		public bool AutomaticallyCheckForUpdates { get; set; } = true;


		/// <summary>
		///		The program should automatically download the new device database
		///		when an update is detected.
		/// </summary>
		public bool AutomaticallyDownloadUpdates { get; set; }

		/// <summary>
		///     If set to a valid file path, the last-used database was imported by the user.
		/// </summary>
		public string ImportedDatabasePath { get; set; }


		/// <summary>
		///     Info about the last manually imported database.
		/// </summary>
		public FileBrowserUserSettings LastImportSettings { get; set; } = new FileBrowserUserSettings();


		/// <summary>
		///     Info about the last manually exported database.
		/// </summary>
		public FileBrowserUserSettings LastExportSettings { get; set; } = new FileBrowserUserSettings();

		#endregion
	}
}

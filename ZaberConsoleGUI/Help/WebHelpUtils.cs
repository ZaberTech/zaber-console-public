﻿using System;
using System.Reflection;
using HtmlAgilityPack;
using log4net;

namespace ZaberConsole.Help
{
	/// <summary>
	///     Utility functions for dealing with HTML files and URLs.
	/// </summary>
	public static class WebHelpUtils
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Get the element ID of the first article element that has a Dita data-* attribute containing
		///     the given data path. This is used to find the articles documenting individual commands or settings.
		/// </summary>
		/// <param name="aDocumentNode">The root node to search from.</param>
		/// <param name="aDataPath">The period-delimited path of the command or setting name to search for.</param>
		/// <param name="aIsCommand">True to search for command help; false for setting help.</param>
		/// <returns>The ID of the first matching article section, or NULL if not found.</returns>
		public static string GetArticleIdThatDocuments(HtmlNode aDocumentNode, string aDataPath, bool aIsCommand)
		{
			var itemType = aIsCommand ? "command" : "setting";

			// Search for articles about the command or setting.
			var searchResult = aDocumentNode.SelectSingleNode(
				$"//article[contains(concat(' ', normalize-space(@data-{itemType}), ' '), ' {aDataPath} ')]");

			if (!string.IsNullOrEmpty(searchResult?.Id))
			{
				return searchResult.Id;
			}

			// If that failed, try articles that claim to document the command or setting.
			searchResult = aDocumentNode.SelectSingleNode(
				$"//article[contains(concat(' ', normalize-space(@data-documents-{itemType}), ' '), ' {aDataPath} ')]");

			if (!string.IsNullOrEmpty(searchResult?.Id))
			{
				// set HelpUrl and break
				return searchResult.Id;
			}

			return null;
		}


		/// <summary>
		///     Load and parse an HTML file from a given URL, which can be a file: scheme URL.
		///     Exceptions will be thrown on failure.
		/// </summary>
		/// <param name="aUrl">URL to load.</param>
		/// <returns>The parsed HTML document on success.</returns>
		public static HtmlDocument ReadHtmlDocument(string aUrl)
		{
			HtmlDocument document = null;

			try
			{
				var url = new Uri(aUrl);
				if (url.Scheme == "file")
				{
					var newDoc = new HtmlDocument();
					newDoc.Load(url.LocalPath);
					document = newDoc;
				}
				else
				{
					var web = new HtmlWeb();
					document = web.Load(aUrl);
				}
			}
			catch (Exception aException)
			{
				_log.Error("Failed to load HTML document from " + aUrl, aException);
				throw;
			}

			return document;
		}

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		#endregion
	}
}

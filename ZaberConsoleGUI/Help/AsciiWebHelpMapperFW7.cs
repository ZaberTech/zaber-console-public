﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using HtmlAgilityPack;
using log4net;

namespace ZaberConsole.Help
{
	/// <summary>
	///     Command help helper for the ASCII protocol in Firmware 5 and 6 devices.
	/// </summary>
	public class AsciiWebHelpMapperFW7 : IWebHelpMapper
	{
		/// <summary>
		///     Initialize the mapper with session information.
		/// </summary>
		/// <param name="aOnline">
		///     If true, the mapper will try to use online help resources.
		///     Otherwise it will use resources bundled with the program.
		/// </param>
		/// <param name="aDitaVals">Device-specific profiling data to be used to format documents.</param>
		/// <param name="aCommandPaths">
		///     Map of space-delimited ASCII commands to period-
		///     delimited data-command names to search for in the HTML help.
		/// </param>
		public AsciiWebHelpMapperFW7(bool aOnline, (string, object[])? aDitaVals,
									 IDictionary<string, string> aCommandPaths)
		{
			if (aOnline)
			{
				DefaultHelpPath = ONLINE_ASCII_HELP_URL;
			}
			else
			{
				var baseUrl = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
				baseUrl = Path.Combine(baseUrl, OFFLINE_ASCII_HELP_PATH);
				DefaultHelpPath = new Uri(Path.GetFullPath(baseUrl)).AbsoluteUri;
			}

			_ditaVals = aDitaVals;
			_commandPaths = aCommandPaths;

			_documentLoadTask = Task.Factory.StartNew(() =>
			{
				_helpFile = WebHelpUtils.ReadHtmlDocument(DefaultHelpPath);
				_documentLoadTask = null;
			});
		}


		#region -- IWebHelpMapper implementation --

		/// <inheritdoc cref="IWebHelpMapper.GetCommandHelpPath" />
		public string GetCommandHelpPath(string aCommand)
		{
			aCommand = aCommand ?? string.Empty;
			if (!_commandCache.TryGetValue(aCommand, out var result))
			{
				var found = false;
				var documentNode = _helpFile?.DocumentNode;

				if ((null != documentNode) && !string.IsNullOrEmpty(aCommand))
				{
					var words = aCommand.Split(' ').ToList();
					while (words.Any())
					{
						//search for a heading in the protocol with a matching Id to the command
						var searchText = string.Join(" ", words);
						if (_commandPaths.TryGetValue(searchText, out var dataTag))
						{
							var id = WebHelpUtils.GetArticleIdThatDocuments(documentNode, dataTag, true);
							if (!string.IsNullOrEmpty(id))
							{
								result = DefaultHelpPath + "#" + id;
								_commandCache[aCommand] = result;
								found = true;
								break;
							}
						}

						// No results - if the command has multiple words, remove the last one and try again.
						words.RemoveAt(words.Count - 1);
					}
				}

				if (!found)
				{
					_log.DebugFormat("File '{0}' has no section about command '{1}'.", DefaultHelpPath, aCommand);
				}
			}

			return result;
		}


		/// <inheritdoc cref="IWebHelpMapper.GetSettingHelpPath" />
		public string GetSettingHelpPath(string aSetting)
		{
			aSetting = aSetting ?? string.Empty;
			if (!_settingCache.TryGetValue(aSetting, out var result))
			{
				var found = false;
				var documentNode = _helpFile?.DocumentNode;

				if ((null != documentNode) && !string.IsNullOrEmpty(aSetting))
				{
					// Search for an article that explicitly documents this setting.
					var searchText = aSetting.ToLower();
					var id = WebHelpUtils.GetArticleIdThatDocuments(documentNode, searchText, false);
					if (!string.IsNullOrEmpty(id))
					{
						result = DefaultHelpPath + "#" + id;
						_settingCache[aSetting] = result;
						found = true;
					}
				}

				if (!found)
				{
					_log.DebugFormat("File '{0}' has no section about setting '{1}'.", DefaultHelpPath, aSetting);
				}
			}

			return result;
		}


		/// <inheritdoc cref="IWebHelpMapper.DefaultHelpPath" />
		public string DefaultHelpPath { get; }


		/// <inheritdoc cref="IWebHelpMapper.GetProfilingData" />
		public (string, object[])? GetProfilingData() => _ditaVals;

		#endregion


		/// <summary>
		///     Block until the help file is finished loading.
		/// </summary>
		public void WaitForDocumentLoad() => _documentLoadTask?.Wait();


		#if DEV
		private const string ONLINE_ASCII_HELP_URL = "http://zshared.izaber.com/software/protocol-manual/ascii-7.html";
		#else
		private const string ONLINE_ASCII_HELP_URL = "https://www.zaber.com/software/protocol-manual/v1/ascii-7.html";
		#endif
		private const string OFFLINE_ASCII_HELP_PATH = "Help/Manuals/FW7/ascii-7.html";

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private readonly (string, object[])? _ditaVals;
		private readonly IDictionary<string, string> _commandPaths;
		private HtmlDocument _helpFile;

		private readonly ConcurrentDictionary<string, string>
			_commandCache = new ConcurrentDictionary<string, string>();

		private readonly ConcurrentDictionary<string, string>
			_settingCache = new ConcurrentDictionary<string, string>();

		private Task _documentLoadTask;
	}
}

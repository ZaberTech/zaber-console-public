﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using HtmlAgilityPack;
using log4net;

namespace ZaberConsole.Help
{
	/// <summary>
	///     Command help helper for the ASCII protocol in Firmware 5 and 6 devices.
	/// </summary>
	public class AsciiWebHelpMapperFW6 : IWebHelpMapper
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Initialize the mapper with session information, using default help file locations.
		/// </summary>
		/// <param name="aOnline">
		///     If true, the mapper will try to use online help resources.
		///     Otherwise it will use resources bundled with the program.
		/// </param>
		public AsciiWebHelpMapperFW6(bool aOnline)
		{
			if (aOnline)
			{
				DefaultHelpPath = ONLINE_ASCII_HELP_URL;
			}
			else
			{
				var baseUrl = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
				baseUrl = Path.Combine(baseUrl, OFFLINE_ASCII_HELP_PATH);
				DefaultHelpPath = new Uri(Path.GetFullPath(baseUrl)).AbsoluteUri;
			}

			_documentLoadTask = Task.Factory.StartNew(() =>
			{
				_helpFile = WebHelpUtils.ReadHtmlDocument(DefaultHelpPath);
				_documentLoadTask = null;
			});
		}


		/// <summary>
		///     Block until the help file is finished loading.
		/// </summary>
		public void WaitForDocumentLoad() => _documentLoadTask?.Wait();

		#endregion

		#region -- IWebHelpMapper implementation --

		/// <inheritdoc cref="IWebHelpMapper.GetCommandHelpPath" />
		public string GetCommandHelpPath(string aCommand)
		{
			aCommand = aCommand ?? string.Empty;
			if (!_linkCache.TryGetValue(aCommand, out var result))
			{
				var found = false;
				var documentNode = _helpFile?.DocumentNode;

				if ((null != documentNode) && !string.IsNullOrEmpty(aCommand))
				{
					var words = aCommand.Split(' ').ToList();
					while (words.Any())
					{
						//search for a heading in the protocol with a matching Id to the command
						var searchText = string.Join("_", words);
						var searchResult =
							documentNode.SelectSingleNode($"//span[@class='mw-headline'][@id='{searchText}']");

						if (null != searchResult)
						{
							result = DefaultHelpPath + "#" + searchText.Replace(" ", "_");
							_linkCache[aCommand] = result;
							found = true;
							break;
						}

						// remove last word and try again
						words.RemoveAt(words.Count - 1);
					}
				}

				if (!found)
				{
					_log.DebugFormat("File '{0}' has no section about command/setting '{1}'.",
									 DefaultHelpPath,
									 aCommand);
				}
			}

			return result;
		}


		/// <inheritdoc cref="IWebHelpMapper.GetSettingHelpPath" />
		public string GetSettingHelpPath(string aCommand) => GetCommandHelpPath(aCommand);


		/// <inheritdoc cref="IWebHelpMapper.DefaultHelpPath" />
		public string DefaultHelpPath { get; }


		/// <inheritdoc cref="IWebHelpMapper.GetProfilingData" />
		public (string, object[])? GetProfilingData() => null;

		#endregion

		#region -- Data --

		private const string ONLINE_ASCII_HELP_URL =
			"https://www.zaber.com/wiki/Manuals/ASCII_Protocol_Manual?action=render";

		private const string OFFLINE_ASCII_HELP_PATH = "Help/Manuals/FW6/ASCII/ASCII_Protocol_Manual.html";

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private HtmlDocument _helpFile;
		private readonly ConcurrentDictionary<string, string> _linkCache = new ConcurrentDictionary<string, string>();
		private Task _documentLoadTask;

		#endregion
	}
}

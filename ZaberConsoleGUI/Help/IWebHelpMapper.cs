﻿using Zaber;

namespace ZaberConsole.Help
{
	/// <summary>
	///     Interface for helpers that assist with loading and navigating HTML help files.
	/// </summary>
	public interface IWebHelpMapper : IDeviceHelp
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Get profiling data for this device.
		/// </summary>
		/// <returns>
		///     Device-type-specific documentation formatting data, or null
		///     of there is none.
		/// </returns>
		(string, object[])? GetProfilingData();

		#endregion
	}
}

﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using HtmlAgilityPack;
using log4net;

namespace ZaberConsole.Help
{
	/// <summary>
	///     Command help helper for the Binary protocol in Firmware 5 and 6 devices.
	/// </summary>
	public class BinaryWebHelpMapperFW6 : IWebHelpMapper
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Initialize the mapper with session information.
		/// </summary>
		/// <param name="aOnline">
		///     If true, the mapper will try to use online help resources.
		///     Otherwise it will use resources bundled with the program.
		/// </param>
		public BinaryWebHelpMapperFW6(bool aOnline)
		{
			if (aOnline)
			{
				DefaultHelpPath = ONLINE_BINARY_HELP_URL;
			}
			else
			{
				var baseUrl = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
				baseUrl = Path.Combine(baseUrl, OFFLINE_BINARY_HELP_PATH);
				DefaultHelpPath = new Uri(Path.GetFullPath(baseUrl)).AbsoluteUri;
			}

			_documentLoadTask = Task.Factory.StartNew(() =>
			{
				_helpFile = WebHelpUtils.ReadHtmlDocument(DefaultHelpPath);
				_documentLoadTask = null;
			});
		}


		/// <summary>
		///     Block until the help file is finished loading.
		/// </summary>
		public void WaitForDocumentLoad() => _documentLoadTask?.Wait();

		#endregion

		#region -- IWebHelpMapper implementation --

		/// <inheritdoc cref="IWebHelpMapper.GetCommandHelpPath" />
		public string GetCommandHelpPath(string aCommand)
		{
			aCommand = aCommand ?? string.Empty;
			if (!_linkCache.TryGetValue(aCommand, out var result))
			{
				var found = false;
				var documentNode = _helpFile?.DocumentNode;

				if ((null != documentNode) && !string.IsNullOrEmpty(aCommand))
				{
					var searchText = "Cmd " + aCommand;
					var searchResult = documentNode.SelectSingleNode(
						$"//span[@class='mw-headline'][substring(@id, string-length(@id) - string-length('{searchText}') + 1) = '{searchText}']");

					if (null == searchResult)
					{
						// The FW6 protocol manual has two different conventions for the ID attributes.
						searchText = "Cmd_" + aCommand;
						searchResult = documentNode.SelectSingleNode(
							$"//span[@class='mw-headline'][substring(@id, string-length(@id) - string-length('{searchText}') + 1) = '{searchText}']");
					}

					if (null != searchResult?.Id)
					{
						result = DefaultHelpPath + "#" + searchResult.Id;
						_linkCache[aCommand] = result;
						found = true;
					}
				}

				if (!found)
				{
					_log.DebugFormat("File '{0}' has no section about command/setting '{1}'.",
									 DefaultHelpPath,
									 aCommand);
				}
			}

			return result;
		}


		/// <inheritdoc cref="IWebHelpMapper.GetSettingHelpPath" />
		public string GetSettingHelpPath(string aCommand) => GetCommandHelpPath(aCommand);


		/// <inheritdoc cref="IWebHelpMapper.DefaultHelpPath" />
		public string DefaultHelpPath { get; }


		/// <inheritdoc cref="IWebHelpMapper.GetProfilingData" />
		public (string, object[])? GetProfilingData() => null;

		#endregion

		#region -- Data --

		private const string ONLINE_BINARY_HELP_URL =
			"https://www.zaber.com/wiki/Manuals/Binary_Protocol_Manual?action=render";

		private const string OFFLINE_BINARY_HELP_PATH = "Help/Manuals/FW6/Binary/Binary_Protocol_Manual.html";

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private HtmlDocument _helpFile;
		private readonly ConcurrentDictionary<string, string> _linkCache = new ConcurrentDictionary<string, string>();
		private Task _documentLoadTask;

		#endregion
	}
}

﻿using System;
using System.IO;
using System.Reflection;
using log4net;
using Zaber;

namespace ZaberConsole.Help
{
	/// <summary>
	///     Command help helper for cases when the browser version is not supported.
	/// </summary>
	public class NotSupportedHelpMapper : IWebHelpMapper
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Initialize the mapper with session information.
		/// </summary>
		/// <param name="aFirmwareVersion">Firmware version to display help link for.</param>
		/// <param name="aProtocol">Protocol currently in use.</param>
		public NotSupportedHelpMapper(FirmwareVersion aFirmwareVersion, Protocol aProtocol)
		{
			string fileName = null;
			if (aFirmwareVersion.Major < 7)
			{
				switch (aProtocol)
				{
					case Protocol.Binary:
						fileName = OFFLINE_BINARY6_HELP_PATH;
						break;
					default:
						fileName = OFFLINE_ASCII6_HELP_PATH;
						break;
				}
			}
			else
			{
				switch (aProtocol)
				{
					case Protocol.Binary:
						fileName = OFFLINE_BINARY7_HELP_PATH;
						break;
					default:
						fileName = OFFLINE_ASCII7_HELP_PATH;
						break;
				}
			}

			var baseUrl = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			baseUrl = Path.Combine(baseUrl, fileName);
			DefaultHelpPath = new Uri(Path.GetFullPath(baseUrl)).AbsoluteUri;
		}


		/// <summary>
		///     Block until the help file is finished loading.
		/// </summary>
		public void WaitForDocumentLoad()
		{
		}

		#endregion

		#region -- IWebHelpMapper implementation --

		/// <inheritdoc cref="IWebHelpMapper.GetCommandHelpPath" />
		public string GetCommandHelpPath(string aCommand) => DefaultHelpPath;


		/// <inheritdoc cref="IWebHelpMapper.GetSettingHelpPath" />
		public string GetSettingHelpPath(string aSetting) => DefaultHelpPath;


		/// <inheritdoc cref="IWebHelpMapper.DefaultHelpPath" />
		public string DefaultHelpPath { get; }


		/// <inheritdoc cref="IWebHelpMapper.GetProfilingData" />
		public (string, object[])? GetProfilingData() => null;

		#endregion

		#region -- Data --

		private const string OFFLINE_ASCII6_HELP_PATH = "Help/Manuals/Placeholder/link_to_ascii_6.html";
		private const string OFFLINE_ASCII7_HELP_PATH = "Help/Manuals/Placeholder/link_to_ascii_7.html";
		private const string OFFLINE_BINARY6_HELP_PATH = "Help/Manuals/Placeholder/link_to_binary_6.html";
		private const string OFFLINE_BINARY7_HELP_PATH = "Help/Manuals/Placeholder/link_to_binary_7.html";

		private static ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		#endregion
	}
}

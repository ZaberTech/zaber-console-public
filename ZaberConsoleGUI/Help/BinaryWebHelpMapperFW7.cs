﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using HtmlAgilityPack;
using log4net;

namespace ZaberConsole.Help
{
	/// <summary>
	///     Command help helper for the Binary protocol in Firmware 7 devices.
	/// </summary>
	public class BinaryWebHelpMapperFW7 : IWebHelpMapper
	{
		/// <summary>
		///     Initialize the mapper with session information.
		/// </summary>
		/// <param name="aOnline">
		///     If true, the mapper will try to use online help resources.
		///     <param name="aDitaVals">Device-specific profiling data to be used to format documents.</param>
		///     Otherwise it will use resources bundled with the program.
		/// </param>
		public BinaryWebHelpMapperFW7(bool aOnline, (string, object[])? aDitaVals)
		{
			if (aOnline)
			{
				DefaultHelpPath = ONLINE_BINARY_HELP_URL;
			}
			else
			{
				var baseUrl = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
				baseUrl = Path.Combine(baseUrl, OFFLINE_BINARY_HELP_PATH);
				DefaultHelpPath = new Uri(Path.GetFullPath(baseUrl)).AbsoluteUri;
			}

			_ditaVals = aDitaVals;

			_documentLoadTask = Task.Factory.StartNew(() =>
			{
				_helpFile = WebHelpUtils.ReadHtmlDocument(DefaultHelpPath);
				_documentLoadTask = null;
			});
		}


		#region -- IWebHelpMapper implementation --

		/// <inheritdoc cref="IWebHelpMapper.GetCommandHelpPath" />
		public string GetCommandHelpPath(string aCommand)
		{
			aCommand = aCommand ?? string.Empty;
			if (!_commandCache.TryGetValue(aCommand, out var result))
			{
				var found = false;
				var documentNode = _helpFile?.DocumentNode;

				if ((null != documentNode) && !string.IsNullOrEmpty(aCommand))
				{
					var id = WebHelpUtils.GetArticleIdThatDocuments(documentNode, aCommand, true);
					if (!string.IsNullOrEmpty(id))
					{
						result = DefaultHelpPath + "#" + id;
						_commandCache[aCommand] = result;
						found = true;
					}
				}

				if (!found)
				{
					_log.DebugFormat("File '{0}' has no section about command '{1}'.", DefaultHelpPath, aCommand);
				}
			}

			return result;
		}


		/// <inheritdoc cref="IDeviceTypeHelpInfo.GetSettingHelpPath" />
		public string GetSettingHelpPath(string aSetting)
		{
			aSetting = aSetting ?? string.Empty;
			if (!_settingCache.TryGetValue(aSetting, out var result))
			{
				var found = false;
				var documentNode = _helpFile?.DocumentNode;

				if ((null != documentNode) && !string.IsNullOrEmpty(aSetting))
				{
					var id = WebHelpUtils.GetArticleIdThatDocuments(documentNode, aSetting, false);
					if (!string.IsNullOrEmpty(id))
					{
						result = DefaultHelpPath + "#" + id;
						_settingCache[aSetting] = result;
						found = true;
					}
				}

				if (!found)
				{
					_log.DebugFormat("File '{0}' has no section about setting '{1}'.", DefaultHelpPath, aSetting);
				}
			}

			return result;
		}


		/// <inheritdoc cref="IWebHelpMapper.DefaultHelpPath" />
		public string DefaultHelpPath { get; }


		/// <inheritdoc cref="IWebHelpMapper.GetProfilingData" />
		public (string, object[])? GetProfilingData() => _ditaVals;

		#endregion


		/// <summary>
		///     Block until the help file is finished loading.
		/// </summary>
		public void WaitForDocumentLoad() => _documentLoadTask?.Wait();


		#if DEV
		private const string ONLINE_BINARY_HELP_URL =
 "http://zshared.izaber.com/software/protocol-manual/binary-7.html";
		#else
		private const string ONLINE_BINARY_HELP_URL = "https://www.zaber.com/software/protocol-manual/v1/binary-7.html";
		#endif
		private const string OFFLINE_BINARY_HELP_PATH = "Help/Manuals/FW7/binary-7.html";

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private readonly (string, object[])? _ditaVals;
		private HtmlDocument _helpFile;

		private readonly ConcurrentDictionary<string, string>
			_commandCache = new ConcurrentDictionary<string, string>();

		private readonly ConcurrentDictionary<string, string>
			_settingCache = new ConcurrentDictionary<string, string>();

		private Task _documentLoadTask;
	}
}

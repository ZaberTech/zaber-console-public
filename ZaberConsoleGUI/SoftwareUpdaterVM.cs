﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using FileDownloader;
using log4net;
using Zaber.FirmwareDownload;
using Zaber.Telemetry;
using Zaber.Threading;
using ZaberConsole.Plugins;
using ZaberConsole.Properties;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Dialogs;

namespace ZaberConsole
{
	/// <summary>
	///     Helper class for downloading program updates from Zaber.
	/// </summary>
	public class SoftwareUpdaterVM : ObservableObject, IDialogClient
	{
		#region -- Public data --

		/// <summary>
		///     Used to select icons and text for the program updater button.
		/// </summary>
		public enum State
		{
			UpToDate,
			UpdateAvailable,
			CheckingForUpdates,
			Downloading,
			UpdateReady,
			Uncertain,
			Error
		}

		#endregion

		#region -- Events --

		/// <summary>
		///     Event used to enable the updater to show messages on the status bar.
		/// </summary>
		public event PlugInManager.StatusMessageHandler ShowStatusMessage;

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Initialize the updater with configuration info.
		/// </summary>
		/// <param name="aDownloadDir">The directory to place downloaded installers in.</param>
		/// <param name="aCurrentVersion">The version currently installed; versions higher than this are considered newer.</param>
		/// <param name="aUserSettings">
		///     User settings from previous session. A reference to this object is
		///     saved internally and settings may be modified; the calling program is responsible for persisting
		///     the same instance to the next session.
		/// </param>
		public SoftwareUpdaterVM(string aDownloadDir, Version aCurrentVersion, SoftwareUpdaterSettings aUserSettings)
		{
			_dispatcher = DispatcherHelper.Dispatcher;
			_timer = DispatcherTimerHelper.CreateTimer();

			_downloadDir = aDownloadDir;
			_currentVersion = aCurrentVersion;

			// Most of the user settings are passed in rather than referred to statically 
			// to improve testability.
			_settings = aUserSettings;

			CurrentState = _settings.AutomaticallyCheckForUpdates ? State.UpToDate : State.Uncertain;

			try
			{
				var path = FindLatestExistingInstaller();
				if (!string.IsNullOrEmpty(path))
				{
					if (GetVersionFromInstallerPath(path) > _currentVersion)
					{
						_log.Info($"Found an unused installer for a newer version in {path}");
						_downloadedFilePath = path;
						CurrentState = State.UpdateReady;
					}
				}

				CleanupOldInstallers(_downloadedFilePath);
			}
			catch (Exception ex)
			{
				CurrentState = State.Error;
				ErrorMessage = Resources.STR_SW_INIT_FAILED;
				_enabled = false;
				_log.Error("Failed to initialize software updater.", ex);
			}

			_autoUpdateTimer = DispatcherTimerHelper.CreateTimer();
			_autoUpdateTimer.Interval = TimeSpan.FromDays(1);
			_autoUpdateTimer.Tick += AutoUpdateTimer_Tick;
			_autoUpdateTimer.Start();
		}


		/// <summary>
		///     Halt any active updater operations when the program is exiting.
		/// </summary>
		public void Shutdown()
		{
			_autoUpdateTimer.Tick -= AutoUpdateTimer_Tick;
			_autoUpdateTimer.Stop();
			Cancel();
		}


		/// <summary>
		///     Start a check for a new installer, if not already in progress.
		///     This method starts an asynchronous operation and returns immediately.
		/// </summary>
		public void CheckForUpdates()
		{
			if (!_enabled)
			{
				_log.Error("Software update check skipped because of initialization error.");
				return;
			}

			lock (_syncRoot)
			{
				if (_worker is null && _downloader is null)
				{
					_worker = BackgroundWorkerManager.CreateWorker();
				}
				else
				{
					return;
				}
			}

			_worker.DoWork += (aSender, aArgs) =>
			{
				aArgs.Result = false;
				_log.Info($"Checking for program installers for versions higher than {_currentVersion}.");

				_previousState = CurrentState;
				_dispatcher.BeginInvoke(() =>
				{
					CurrentState = State.CheckingForUpdates;
					ProgressMaximum = 30.0;
					Progress = 1.0;
				});

				// Make a fake progress bar with a timer since there is no progress
				// for a web header check and it will be either very fast or time out.
				EventHandler progressUpdateCallback = (__, _) =>
				{
					_dispatcher.BeginInvoke(() => { Progress += 1.0; });
				};

				_timer.Tick += progressUpdateCallback;
				_timer.Interval = TimeSpan.FromMilliseconds(100);

				try
				{
					_timer.Start();
					var startTime = DateTime.Now;
					var onlineVersion = GetLatestAvailableVersionNumber();
					if (onlineVersion is null)
					{
						throw new OperationCanceledException("Unable to retrieve program updates at this time.");
					}

					if (onlineVersion > _currentVersion)
					{
						aArgs.Result = onlineVersion;
					}

					if (_worker.CancellationPending)
					{
						aArgs.Cancel = true;
						return;
					}

					// Make sure the update check takes at least half a second 
					// so it looks like something happened. If the button changes
					// state too fast it looks like a glitch.
					while ((DateTime.Now - startTime).TotalMilliseconds < 500)
					{
						Thread.Sleep(100);
					}
				}
				finally
				{
					_timer.Stop();
					_timer.Tick -= progressUpdateCallback;
				}
			};

			_worker.RunWorkerCompleted += (aSender, aArgs) =>
			{
				lock (_syncRoot)
				{
					_worker = null;
				}

				if (aArgs.Cancelled)
				{
					_log.Warn("Program update check was cancelled by user.");
					_dispatcher.BeginInvoke(() => { CurrentState = _previousState; });
				}
				else if (aArgs.Error != null)
				{
					_log.Warn("Checking for a program update failed.", aArgs.Error);
					_dispatcher.BeginInvoke(() =>
					{
						CurrentState = State.Error;
						ErrorMessage = string.Format(Resources.STR_SW_UPDATE_CHECK_FAILED, aArgs.Error.Message);
					});
				}
				else
				{
					_availableVersion = aArgs.Result as Version;
					if (null != _availableVersion)
					{
						// Check to see if we already downloaded this version.
						var downloadNeeded = true;
						if (!string.IsNullOrEmpty(_downloadedFilePath) && File.Exists(_downloadedFilePath))
						{
							var haveInstallerVersion = GetVersionFromInstallerPath(_downloadedFilePath);
							if (null != haveInstallerVersion)
							{
								downloadNeeded = _availableVersion > haveInstallerVersion;
							}
						}

						if (downloadNeeded)
						{
							_log.Info("A new program version is available for download.");
							_dispatcher.BeginInvoke(() => { CurrentState = State.UpdateAvailable; });
						}
						else
						{
							_log.Info("A new program version is available and was previously downloaded.");
							_dispatcher.BeginInvoke(() => { CurrentState = State.UpdateReady; });
						}
					}
					else
					{
						_log.Info("The program is up to date.");
						_dispatcher.BeginInvoke(() => { CurrentState = State.UpToDate; });
					}
				}
			};

			_worker.WorkerSupportsCancellation = true;

			_worker.Run();
		}


		/// <summary>
		///     Cancel the database download/import/export/check if it is in progress.
		/// </summary>
		public void Cancel()
		{
			lock (_syncRoot)
			{
				_worker?.CancelAsync();
				_downloader?.CancelDownloadAsync();
				_downloader = null;
			}
		}


		/// <summary>
		///     Returns true if a database download/import/export/check attempt is currently in progress.
		/// </summary>
		public bool IsBusy => (null != _worker) || (null != _downloader);

		#endregion

		#region -- View data --

		/// <summary>
		///     Get or set the progress bar maximum.
		/// </summary>
		public double ProgressMaximum
		{
			get => _progressMaximum;
			set => Set(ref _progressMaximum, value, nameof(ProgressMaximum));
		}


		/// <summary>
		///     Get or set the progress bar value.
		/// </summary>
		public double Progress
		{
			get => _progress;
			set => Set(ref _progress, value, nameof(Progress));
		}


		/// <summary>
		///     Get or set the error message for the button tooltip.
		/// </summary>
		public string ErrorMessage
		{
			get => _errorMessage;
			set => Set(ref _errorMessage, value, nameof(ErrorMessage));
		}


		/// <summary>
		///     Checkbox in the context menu controls whether automatic updates checks should happen.
		/// </summary>
		public bool AutoCheckForUpdates
		{
			get => _settings.AutomaticallyCheckForUpdates;
			set
			{
				if (_settings.AutomaticallyCheckForUpdates != value)
				{
					_settings.AutomaticallyCheckForUpdates = value;
					OnPropertyChanged(nameof(AutoCheckForUpdates));
					if (value)
					{
						CheckForUpdates();
					}
				}
			}
		}


		/// <summary>
		///     Controls the icon displayed in the button.
		/// </summary>
		public State CurrentState
		{
			get => _currentState;
			set => Set(ref _currentState, value, nameof(CurrentState));
		}


		/// <summary>
		///     Command invoked by left-clicking the button.
		/// </summary>
		public ICommand ButtonCommand
			=> OnDemandRelayCommand(ref _buttonCommand,
									_ => OnButtonClicked(),
									_ => _enabled);

		#endregion

		#region -- IDialogClient --

		public event RequestDialogChange RequestDialogOpen;
		#pragma warning disable CS0067 // Event never used.
		public event RequestDialogChange RequestDialogClose;
		#pragma warning restore CS0067

		#endregion

		#region -- Protected and private methods --

		private void AutoUpdateTimer_Tick(object sender, EventArgs e)
		{
			switch (CurrentState)
			{
				case State.CheckingForUpdates: // Deliberate fall-through.
				case State.Downloading:
					_log.Info("Timed software update check skipped.");
					break;
				case State.UpToDate:           // Deliberate fall-through.
				case State.UpdateAvailable:    // Deliberate fall-through.
				case State.UpdateReady:        // Deliberate fall-through.
				case State.Uncertain:          // Deliberate fall-through.
				case State.Error:
					_log.Info("Timed software update check triggered.");
					if (_settings.AutomaticallyCheckForUpdates)
					{
						CheckForUpdates();
					}
					else
					{
						CurrentState = State.Uncertain;
					}
					break;
				default:
					throw new InvalidProgramException($"Unhandled case {CurrentState}!");
			}
		}

		/// <summary>
		///     Searches for previously downloaded installers and returns
		///     the path to the one that has the highest version number in
		///     its filename.
		/// </summary>
		/// <returns>A path to an installer, or null if none found.</returns>
		private string FindLatestExistingInstaller()
		{
			string bestMatch = null;
			var maxVersion = new Version();

			if (!Directory.Exists(_downloadDir))
			{
				Directory.CreateDirectory(_downloadDir);
			}

			foreach (var file in new DirectoryInfo(_downloadDir).GetFiles())
			{
				var version = GetVersionFromInstallerPath(file.FullName);
				if (null != version)
				{
					if (bestMatch is null || (version > maxVersion))
					{
						bestMatch = file.FullName;
						maxVersion = version;
					}
				}
			}

			return bestMatch;
		}


		/// <summary>
		///     Attempts to extract the version number from the path to an installer
		///     file.
		/// </summary>
		/// <param name="aPath">Path to an installer for this program.</param>
		/// <returns>
		///     The version number from the file name, or null if the
		///     file does not have the expected name for the program, or null
		///     if the file does not have a version number it its name.
		/// </returns>
		private Version GetVersionFromInstallerPath(string aPath)
		{
			var match = Regex.Match(aPath, INSTALLER_FILE_PATTERN);
			if (match.Success && (match.Groups.Count > 1))
			{
				return new Version(match.Groups[1].Value);
			}

			return null;
		}


		/// <summary>
		///     Remove any previously downloaded installers.
		/// </summary>
		/// <param name="aListOfFilesToKeep">Full paths to files that must not be deleted.</param>
		private void CleanupOldInstallers(params string[] aListOfFilesToKeep)
		{
			var skip = new HashSet<string>(aListOfFilesToKeep);

			foreach (var file in new DirectoryInfo(_downloadDir).GetFiles(Configuration.InstallerBaseName + "*.exe"))
			{
				if (!skip.Contains(file.FullName))
				{
					TempFileManager.RegisterForDelete(file.FullName);
				}
			}

			TempFileManager.TryDeleteFiles();
		}


		private void OnButtonClicked()
		{
			if (IsBusy)
			{
				Cancel();
			}
			else
			{
				switch (CurrentState)
				{
					case State.UpdateAvailable:
						StartDownload();
						break;
					case State.UpdateReady:
						if (PromptToLaunchInstaller())
						{
							LaunchInstaller();
						}

						break;
					default:
						CheckForUpdates();
						break;
				}
			}
		}


		private void StartDownload()
		{
			lock (_syncRoot)
			{
				if (_worker is null && _downloader is null)
				{
					_downloader =
						new FileDownloader.FileDownloader(new DownloadCache(_downloadDir, DOWNLOAD_EXTENSION));
				}
				else
				{
					return;
				}
			}

			try
			{
				var uri = GetDownloadUri(_availableVersion);
				var fileName = uri.Segments.Last();
				var file = Path.Combine(_downloadDir, fileName);

				_dispatcher.BeginInvoke(() => { CurrentState = State.Downloading; });

				if (File.Exists(file))
				{
					_log.Info($"Deleting previous download: {file}");
					File.Delete(file);
				}

				_log.Info($"Downloading {fileName}...");

				var lastProgressUpdate = DateTime.Now;
				_downloader.DownloadProgressChanged += (_, aArgs) =>
				{
					if ((DateTime.Now - lastProgressUpdate).TotalMilliseconds > 100)
					{
						lastProgressUpdate = DateTime.Now;
						_dispatcher.BeginInvoke(() =>
						{
							ProgressMaximum = aArgs.TotalBytesToReceive;
							Progress = aArgs.BytesReceived;
						});
					}
				};

				_downloader.DownloadFileCompleted += (_, aArgs) =>
				{
					lock (_syncRoot)
					{
						_downloader = null;
					}

					if (aArgs.State == CompletedState.Succeeded)
					{
						var renamedFile = aArgs.FileName.Replace(DOWNLOAD_EXTENSION, string.Empty);
						File.Move(aArgs.FileName, renamedFile);
						_downloadedFilePath = renamedFile;
						_log.Info($"New installer was successfully downloaded to {_downloadedFilePath}");

						_dispatcher.BeginInvoke(() => { CurrentState = State.UpdateReady; });

						_eventLog.LogEvent("Installer downloaded", _downloadedFilePath);
					}
					else if (aArgs.State == CompletedState.Canceled)
					{
						_log.Info("Installer download was cancelled.");
						ShowStatusMessage?.Invoke(this, Resources.STR_SW_DOWNLOAD_CANCELLED, STATUS_DURATION);

						_dispatcher.BeginInvoke(() =>
						{
							ErrorMessage = Resources.STR_SW_DOWNLOAD_CANCELLED;
							CurrentState = State.Error;
						});

						if (File.Exists(aArgs.FileName))
						{
							File.Delete(aArgs.FileName);
						}
					}
					else if (aArgs.State == CompletedState.Failed)
					{
						_log.Error("Installer download failed.", aArgs.Error);

						_dispatcher.BeginInvoke(() =>
						{
							CurrentState = State.Error;
							ErrorMessage = string.Format(Resources.STR_SW_DOWNLOAD_FAILED,
														 aArgs.Error?.Message ?? string.Empty);
						});

						if (File.Exists(aArgs.FileName))
						{
							File.Delete(aArgs.FileName);
						}
					}
				};

				_downloader.DownloadFileAsync(uri, $"{file}{DOWNLOAD_EXTENSION}");
			}
			catch (Exception e)
			{
				_log.Error("Failed to start download.", e);
				_downloader = null;
				_dispatcher.BeginInvoke(() =>
				{
					CurrentState = State.Error;
					ErrorMessage = string.Format(Resources.STR_SW_DOWNLOAD_FAILED, e.Message);
				});
			}
		}


		private bool PromptToLaunchInstaller()
		{
			var mb = new MessageBoxParams
			{
				Buttons = MessageBoxButton.YesNo,
				Caption = "Install new version",
				Icon = MessageBoxImage.Question,
				Message = Resources.STR_SW_UPDATE_READY
			};

			RequestDialogOpen?.Invoke(this, mb);

			return (MessageBoxResult.Yes == mb.Result) || (MessageBoxResult.OK == mb.Result);
		}


		private void LaunchInstaller()
		{
			if (string.IsNullOrEmpty(_downloadedFilePath) || !File.Exists(_downloadedFilePath))
			{
				_log.Error("Cannot launch installer because it doesn't exist.");
				return;
			}

			_log.Info("Launching new installer.");
			_eventLog.LogEvent("Installer launched", _downloadedFilePath);
			Process.Start(_downloadedFilePath);

			_log.Info("Exiting program as part of update.");
			_dispatcher.BeginInvoke(() => { Application.Current.Shutdown(); });
		}


		// This method is protected virtual to facilitate testing.
		protected virtual Version GetLatestAvailableVersionNumber()
		{
			var requestUrl = Configuration.GetFinderVersionQueryUrl(Configuration.SoftwareFinderId);
			var request = (HttpWebRequest) WebRequest.Create(requestUrl);
			if (Settings.Default.DisableWebProxy)
			{
				request.Proxy = null;
			}

			var assemblyName = Assembly.GetExecutingAssembly().GetName();
			request.UserAgent = string.Format("{0}/{1}", assemblyName.Name, assemblyName.Version);

			var result = (HttpWebResponse) request.GetResponse();

			var info = FirmwareWebQueryBuilder.DecodeResponse<Dictionary<string, object>>(result.GetResponseStream());
			result.Close();

			Version latest = null;
			if (info.TryGetValue("versions", out var versions))
			{
				foreach (var v in (versions as IEnumerable) ?? new object[0])
				{
					if (v is string verString && Regex.IsMatch(verString, @"\d+\.\d+\.\d+\.\d+"))
					{
						try
						{
							var ver = new Version(verString);
							if (latest is null || (ver > latest))
							{
								latest = ver;
							}
						}
						catch (OverflowException e)
						{
							_log.Warn($"Received a bogus version number: '{verString}'", e);
						}
					}
				}
			}
			else if (info.TryGetValue("error", out var error))
			{
				_log.Warn($"Could not retrieve version list. Response: '{error?.ToString() ?? "<null>"}'");
			}
			else
			{
				_log.Warn("Unknown error retrieving version list.");
			}

			return latest;
		}


		// This method is protected virtual to facilitate testing.
		protected virtual Uri GetDownloadUri(Version aVersionToDownload)
		{
			var updateUri = Configuration.GetDownloadUrlQueryUrl(Configuration.SoftwareFinderId,
																 aVersionToDownload.ToString());

			var request = (HttpWebRequest) WebRequest.Create(updateUri);
			if (Settings.Default.DisableWebProxy)
			{
				request.Proxy = null;
			}

			var result = (HttpWebResponse) request.GetResponse();

			var info = FirmwareWebQueryBuilder.DecodeResponse<Dictionary<string, object>>(result.GetResponseStream());
			result.Close();

			return new Uri(info["url"] as string);
		}

		#endregion

		#region -- Private member data --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Software updater");

		private static readonly string DOWNLOAD_EXTENSION = ".download";

		private static readonly string INSTALLER_FILE_PATTERN =
			$".*{Configuration.InstallerBaseName}-(\\d+\\.\\d+\\.\\d+\\.\\d+).exe$";

		private const int STATUS_DURATION = 2;

		private readonly SoftwareUpdaterSettings _settings;
		private readonly object _syncRoot = new object();

		private readonly IDispatcher _dispatcher;
		private readonly IDispatcherTimer _timer;
		private readonly IDispatcherTimer _autoUpdateTimer;
		private IBackgroundWorker _worker;
		private IFileDownloader _downloader;

		private readonly string _downloadDir;
		private readonly Version _currentVersion;
		private Version _availableVersion;
		private string _downloadedFilePath;

		private double _progressMaximum = 1.0;
		private double _progress;
		private string _errorMessage;
		private RelayCommand _buttonCommand;
		private State _currentState;
		private State _previousState;
		private readonly bool _enabled = true;

		#endregion
	}
}

﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using FileDownloader;

namespace ZaberConsole
{
	internal class DownloadCache : IDownloadCache
	{
		#region -- Public methods --

		public DownloadCache(string aCacheDirectory, string aDownloadExtension)
		{
			_cacheDirectory = aCacheDirectory;
			_downloadExtension = aDownloadExtension;
		}


		public void Add(Uri aUri, string aPath, WebHeaderCollection aHeaders)
		{
			var file = FilePathFromUri(aUri);
			if (!File.Exists(file))
			{
				File.Create(file).Dispose();
			}
		}


		public string Get(Uri aUri, WebHeaderCollection aHeaders)
		{
			var file = FilePathFromUri(aUri);
			if (File.Exists(file))
			{
				return file;
			}

			return null;
		}


		public void Invalidate(Uri aUri)
		{
			var file = FilePathFromUri(aUri);
			if (File.Exists(file))
			{
				File.Delete(file);
			}
		}

		#endregion

		#region -- Non-Public Methods --

		private string FilePathFromUri(Uri aUri)
		{
			var file = string.Empty;

			return Path.Combine(_cacheDirectory, $@"{aUri.Segments.Last()}{_downloadExtension}");
		}

		#endregion

		#region --- Private member data ---

		private readonly string _cacheDirectory;

		private readonly string _downloadExtension;

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using log4net;
using Zaber;
using Zaber.Ports;
using ZaberWpfToolbox.Logging;

namespace ZaberConsole
{
	/// <summary>
	///     A collection of low-level helper functions to magically detect and
	///     reconfigure Zaber devices in ways the Zaber library doesn't enable.
	/// </summary>
	internal static class DeviceDetector
	{
		#region -- Public data --

		// See Nathan's comment at http://trac/trac/ticket/64#comment:5.
		/// The magic ping is a global binary "echo" command, with a data
		/// payload of the ASCII string "/\r\n". This should make all 
		/// devices at the chosen baud rate reply regardless of protocol.
		public static readonly byte[] MAGIC_PING = { 0x00, 0x37, 0x2F, 0x0D, 0x0A, 0x00 };

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Tries to find the protocol and baud rate of the devices connected.
		/// </summary>
		/// <remarks>
		///     If devices are connected at different protocols and/or baudrates,
		///     then it is likely that this function will not find all of them, and
		///     will just report the protocol and baud rate of the first device. If
		///     that first device is a T-Series device, then this function will be
		///     able to tell that there are mismatched protocols/bauds, and will
		///     raise an exception.
		/// </remarks>
		/// <param name="portName">The name of the port to scan.</param>
		/// <exception cref="FormatException">
		///     The port received a reply it
		///     could not parse. It's likely that some data corruption occurred,
		///     a non-Zaber device is connected, or a Zaber device with move
		///     tracking enabled is moving and sending unsolicited messages.
		/// </exception>
		/// <exception cref="UnauthorizedAccessException">
		///     Access to the port is
		///     denied.
		/// </exception>
		/// <returns>
		///     A tuple containing the protocol and baudrate detected. If none
		///     was detected, Protocol.None and 0 are returned.
		/// </returns>
		public static Tuple<Protocol, int> DetectProtocolAndBaudRate(IRawPort aPort)
		{
			/* One might think that sending messages at the wrong baud rate
             * may be dangerous: one 0 byte at 9600 is 12 0 bytes at
             * 115200! But do not forget about framing bits. The A-Series pays
             * attention to framing errors, and does not act on or respond to
             * commands received at the wrong baud rate. Only the T-Series will
             * execute commands regardless of framing errors.
             * 
             * The T-Series only works at 9600, so it is impossible that they
             * might misinterpret a slower command, as there are no slower
             * baud rates.
             * 
             * Furthermore, from the Binary Protocol Manual:
             * "All instructions consist of a group of 6 bytes. They must be
             * transmitted with less than 10 ms between each byte. If the
             * device has received less than 6 bytes and then a period longer
             * than 10 ms passes, it ignores the bytes already received."
             * 
             * Because we wait at least 100ms to receive replies, T-Series
             * devices working at 9600 will not receive a full 6 bytes before
             * 10ms of silence elapses, and the device will discard the
             * partial command.
             * 
             * Therefore, it is always safe to send a single "magic ping"
             * command to all devices at all baud rates.
             */

			var asciiReplyRegex = new Regex(@"\@[0-9][0-9] [0-9] ");


			// This int is also used as a "is detected" flag: 
			// 0 = no devices detected, !0 = found a baud rate.
			var detectedBaudRate = 0;
			var detectedProtocol = Protocol.None;
			var devicesSeen = new List<byte>();

			aPort.Open();
			aPort.ReadTimeout = 200;

			var reply = new byte[6];
			foreach (var rate in aPort.GetApplicableBaudRates())
			{
				Log.Debug("Detecting devices at baud " + rate);
				aPort.BaudRate = rate;
				aPort.Write(MAGIC_PING);

				for (;;)
				{
					/* RECEIVE REPLY */
					try
					{
						// Read exactly 6 bytes.
						reply = aPort.Read(6);
					}
					catch (TimeoutException)
					{
						Log.Debug("Finished receiving responses at baud " + rate);
						aPort.ClearReceiveBuffer();
						break; // Read all input-- carry on to the next baud rate.
					}

					/* EXAMINE REPLY */
					var replyString = new string(reply.Select(b => (char) b).ToArray());
					if (asciiReplyRegex.IsMatch(replyString))
					{
						var number = byte.Parse(replyString.Substring(1, 2), CultureInfo.InvariantCulture);
						Log.Debug("Received ASCII reply from device number " + number + " at baud " + rate);
						try
						{
							// Clear out the rest of the reply.
							aPort.ReadLine();
						}
						catch (TimeoutException)
						{
							Log.ErrorFormat("Expected to read "
										+ "the rest of a message beginning with "
										+ "\"{0}\", but the read timed out.",
											replyString);
							aPort.ClearReceiveBuffer();
						}

						if (devicesSeen.Contains(number))
						{
							// Devices connected over USB (eg. the MCB-2) ignore
							// baud rate, and will respond at every rate. If we
							// have seen a device before, assume this to be the case.
							Log.InfoFormat("Saw device {0} before: there "
									   + "are possibly two devices with the same "
									   + "number, or a device connected over USB.",
										   number);
							continue;
						}

						devicesSeen.Add(number);

						if (detectedBaudRate == 0)
						{
							detectedProtocol = Protocol.ASCII;
							detectedBaudRate = rate;
						}
						else if ((detectedProtocol != Protocol.ASCII)
							 || (detectedBaudRate != rate))
						{
							throw new MismatchedDevicesException();
						}
					}

					// Check all 5 bytes of the echo to be sure.
					else if (((reply[1] == MAGIC_PING[1])
						  && (reply[2] == MAGIC_PING[2])
						  && (reply[3] == MAGIC_PING[3])
						  && (reply[4] == MAGIC_PING[4])
						  && (reply[5] == MAGIC_PING[5]))

							 // Also accept "command not valid" errors
							 // to support FW 2.93 devices.
						 || ((reply[1] == 255) && (reply[2] == 64)))
					{
						var number = reply[0];

						Log.Debug("Received Binary reply from device number " + number + " at baud " + rate);

						if (devicesSeen.Contains(number))
						{
							// Devices connected over USB (eg. the MCB-2) ignore
							// baud rate, and will respond at every rate. If we
							// have seen a device before, assume this to be the case.
							Log.InfoFormat("Saw device {0} before: there "
									   + "are possibly two devices with the same "
									   + "number, or a device connected over USB.",
										   number);
							continue;
						}

						devicesSeen.Add(number);

						if (detectedBaudRate == 0)
						{
							detectedProtocol = Protocol.Binary;
							detectedBaudRate = rate;
						}
						else if ((detectedProtocol != Protocol.Binary)
							 || (detectedBaudRate != rate))
						{
							throw new MismatchedDevicesException();
						}
					}
					else
					{
						var replyAsStr = "";
						try
						{
							replyAsStr = $" (\"{System.Text.Encoding.ASCII.GetString(reply)}\")";
						}
						catch (Exception)
						{
						}

						var errorMsg = $"Received a reply of an unknown type. First 6 bytes: {reply[0]} {reply[1]} {reply[2]} {reply[3]} {reply[4]} {reply[5]}{replyAsStr}. ";

						Log.Error(errorMsg);
						throw new FormatException(errorMsg);
					}
				}
			}

			return Tuple.Create(detectedProtocol, detectedBaudRate);
		}


		public static void ReconfigureDevices(IRawPort aPort, CancellationToken token, ILoggable logger)
		{
			_statusLog = logger;
			Log.Info("Reconfiguring devices...");

			aPort.ReadTimeout = 120;
			try
			{
				aPort.Open();
			}
			catch (UnauthorizedAccessException uae)
			{
				LogMessage("Could not open port. " + uae.Message);
				return;
			}

			int asciiDeviceCount = 0,
				binaryDeviceCount = 0;
			while (true)
			{
				int lastAsciiDeviceCount = asciiDeviceCount,
					lastBinaryDeviceCount = binaryDeviceCount;

				LogMessage("Counting Binary devices...");
				binaryDeviceCount = CountBinaryDevices(aPort);
				LogMessage("Counted {0} Binary devices.",
						   binaryDeviceCount);

				LogMessage("Counting ASCII devices...");
				Log.Debug("Converting devices to ASCII...");
				SwitchToAscii(aPort, 115200, binaryDeviceCount);

				// Check for cancellation.
				token.ThrowIfCancellationRequested();

				asciiDeviceCount = CountAsciiDevices(aPort);
				LogMessage("Counted {0} ASCII devices.",
						   asciiDeviceCount);

				// If we didn't get any new replies, then we have 
				// probably found all the devices on the chain.
				if ((asciiDeviceCount == lastAsciiDeviceCount)
				&& (binaryDeviceCount == lastBinaryDeviceCount))
				{
					LogMessage("Checking non-standard baud rates...");
					/* All devices capable of ASCII are capable of Binary
                        * at non-standard baudrates (ie. are not T-Series).
                        * We want to flip all the A- and X-Series devices
                        * through their possible baud rates, so we tell
                        * ReconfigureDevicesAtNonstandardBaudrates how many
                        * non-T-Series devices there are. */
					var newAsciiDeviceCount =
						CountDevicesAtNonstandardBaudrates(aPort, asciiDeviceCount, token);

					// If we found no new devices, we're done.
					if (newAsciiDeviceCount == asciiDeviceCount)
					{
						LogMessage("Found no new devices at non-standard baud rates.");
						asciiDeviceCount = newAsciiDeviceCount;
						break;
					}

					LogMessage("Found {0} new device(s) at non-standard baud rates.",
							   newAsciiDeviceCount - asciiDeviceCount);

					// If we found new devices, then we go again
					// with the new devices on the chain.
					asciiDeviceCount = newAsciiDeviceCount;
				}
				else if ((asciiDeviceCount < lastAsciiDeviceCount)
					 || (binaryDeviceCount < lastBinaryDeviceCount))
				{
					var errorMessage =
						string.Format("Received fewer replies than "
								  + "expected. Saw {0} ASCII devices and {1} Binary "
								  + "devices previously, only saw {2} ASCII devices and "
								  + "{3} Binary devices this pass. Make sure none of your "
								  + "devices' manual movement knobs are off-detent.",
									  lastAsciiDeviceCount,
									  lastBinaryDeviceCount,
									  asciiDeviceCount,
									  binaryDeviceCount);
					LogErrorMessage(errorMessage);

					// Double the sleep time to cater to especially slow-
					// changing devices. Not all devices are made equal.
					_sleepTime *= 2;
				}

				Log.Debug("Converting devices to Binary...");
				if (SwitchToBinary(aPort, 9600, asciiDeviceCount) > 0)
				{
					LogMessage("Some devices are in bootloader mode.");
					break;
				}

				// Check for cancellation.
				token.ThrowIfCancellationRequested();
			}

			/* Here we assume we have left the port in ASCII mode.
                * If that changes in the loop above, then this must change. */
			LogMessage("Renumbering ASCII devices...");

			aPort.Write("/renumber\r\n");
			Thread.Sleep(_sleepTime);
			var repliesReceived = 0;
			try
			{
				for (var i = 0; i < asciiDeviceCount; i++)
				{
					aPort.ReadLine();
					repliesReceived += 1;
				}
			}
			catch (TimeoutException)
			{
				LogErrorMessage("Tried to renumber {0} devices but only got a response from {1}.",
								asciiDeviceCount,
								repliesReceived);
			}

			// Renumber them in Binary too.
			SwitchToBinary(aPort, 9600, asciiDeviceCount);
			LogMessage("Renumbering Binary devices...");
			aPort.Write(Pack(0, Command.Renumber, 0));
			Thread.Sleep(_sleepTime);
			aPort.ClearReceiveBuffer();

			Log.Info("Done reconfiguring devices.");
		}


		public static Tuple<List<ZaberDevice>, List<ZaberDevice>> FindDevices(
			ZaberPortFacade portFacade, CancellationToken token)
		{
			Log.Info("Finding devices...");

			if (portFacade.IsOpen)
			{
				portFacade.Close();
			}

			LogMessage("Finding ASCII devices by name.");

			// Turn all devices to ASCII, regardless of what they were before.
			Log.Debug("Configuring port to Binary.");
			portFacade.Port.IsAsciiMode = false;
			portFacade.Port.BaudRate = 9600;
			portFacade.Port.Open();
			Log.Debug("Setting all devices to ASCII.");
			portFacade.Port.Send(0, Command.ConvertToASCII, 115200);
			Thread.Sleep(_sleepTime);
			portFacade.Port.Close();

			token.ThrowIfCancellationRequested();

			// Get ASCII devices.
			Log.Debug("Finding ASCII devices...");
			portFacade.Port.IsAsciiMode = true;
			portFacade.Port.BaudRate = 115200;
			try
			{
				portFacade.Open();
			}
			catch (RequestTimeoutException)
			{
				LogMessage("Reading ASCII devices timed out. "
					   + "All ASCII devices may not appear.");
			}

			var asciiDevices = (from conversation in portFacade.Conversations
								where conversation.Device.DeviceNumber != 0
								select conversation.Device).ToList();
			Log.DebugFormat("Found {0} ASCII devices.", asciiDevices.Count);

			token.ThrowIfCancellationRequested();

			var binaryDevices = new List<ZaberDevice>();

			LogMessage("Finding Binary devices by name.");

			// Turn the devices and portFacade to Binary.
			Log.Debug("Setting all device to Binary.");
			portFacade.GetConversation(0).Send("/tools setcomm 9600 1\r\n");
			Thread.Sleep(_sleepTime);
			portFacade.Close();

			token.ThrowIfCancellationRequested();

			// Get Binary devices.
			Log.Debug("Finding Binary devices...");
			portFacade.Port.IsAsciiMode = false;
			portFacade.Port.BaudRate = 9600;
			try
			{
				portFacade.Open();
			}
			catch (RequestTimeoutException)
			{
				LogMessage("Reading Binary devices timed out. "
					   + "All Binary devices may not appear.");
			}

			binaryDevices = (from conversation in portFacade.Conversations
							 where conversation.Device.DeviceNumber != 0
							 select conversation.Device).ToList();
			Log.DebugFormat("Found {0} Binary devices.", binaryDevices.Count);

			portFacade.Close();
			portFacade.DeviceTypeMap.Clear(); // Prevent recopying of cloned commands with nullified units.

			Log.Info("Done finding devices.");
			return Tuple.Create(asciiDevices, binaryDevices);
		}

		#endregion

		#region -- Non-Public Methods --

		private static void LogMessage(string message, params object[] args)
		{
			if (args.Length > 0)
			{
				message = string.Format(message, args);
			}

			if (null != _statusLog)
			{
				_statusLog.AppendLine(message);
			}

			Log.Info(message);
		}


		private static void LogErrorMessage(string message, params object[] args)
		{
			if (args.Length > 0)
			{
				message = string.Format(message, args);
			}

			if (null != _statusLog)
			{
				_statusLog.AppendLine("Error: " + message);
			}

			Log.Error(message);
		}


		private static int CountDevicesAtNonstandardBaudrates(IRawPort port,
															  int asciiDeviceCount, CancellationToken token)
		{
			var asciiCapableDevices = asciiDeviceCount;

			// The devices should be currently in ASCII/115200.
			// Check Binary/115200 first, then try the rest.
			Log.Debug("Converting devices to Binary/115200...");
			SwitchToBinary(port, 115200, asciiCapableDevices);

			token.ThrowIfCancellationRequested();

			asciiCapableDevices = CountBinaryDevices(port);
			Log.DebugFormat("Found {0} devices capable of Binary at 115200.",
							asciiCapableDevices);
			var newDevices = asciiCapableDevices - asciiDeviceCount;
			if (newDevices > 0)
			{
				LogMessage("Found {0} new device(s) at Binary/115200.", newDevices);
				asciiDeviceCount = asciiCapableDevices;
			}

			// Check the weird baud rates.
			foreach (var baud in port.GetApplicableBaudRates().Where(b => (b != 115200) && (b != 9600)))
			{
				token.ThrowIfCancellationRequested();

				Log.DebugFormat("Converting devices to ASCII/{0}...", baud);
				SwitchToAscii(port, baud, asciiCapableDevices);

				token.ThrowIfCancellationRequested();

				asciiCapableDevices = CountAsciiDevices(port);
				Log.DebugFormat("Found {0} devices capable of ASCII at {1}.", asciiCapableDevices, baud);

				newDevices = asciiCapableDevices - asciiDeviceCount;
				if (newDevices > 0)
				{
					LogMessage("Found {0} new device(s) at ASCII/{1}.", newDevices, baud);
					asciiDeviceCount = asciiCapableDevices;
				}

				token.ThrowIfCancellationRequested();

				Log.DebugFormat("Converting devices to Binary/{0}...", baud);
				SwitchToBinary(port, baud, asciiCapableDevices);

				token.ThrowIfCancellationRequested();

				asciiCapableDevices = CountBinaryDevices(port);
				Log.DebugFormat("Found {0} devices capable of Binary at {1}.", asciiCapableDevices, baud);

				newDevices = asciiCapableDevices - asciiDeviceCount;
				if (newDevices > 0)
				{
					LogMessage("Found {0} new device(s) at Binary/{1}.", newDevices, baud);
					asciiDeviceCount = asciiCapableDevices;
				}
			}

			token.ThrowIfCancellationRequested();

			// Check ASCII/9600.
			Log.Debug("Converting devices to ASCII/9600...");
			SwitchToAscii(port, 9600, asciiCapableDevices);
			asciiCapableDevices = CountAsciiDevices(port);
			Log.DebugFormat("Found {0} devices capable of ASCII at 9600.", asciiCapableDevices);
			newDevices = asciiCapableDevices - asciiDeviceCount;
			if (newDevices > 0)
			{
				LogMessage("Found {0} new device(s) at ASCII/9600.", newDevices);
			}

			// Turn the port back to ASCII/115200, as we found it.
			// We can't use SwitchToAscii here, 
			// because it assumes devices are in Binary.
			port.Write("/set comm.rs232.baud 115200\r\n");
			Thread.Sleep(_sleepTime);
			for (var i = 0; i < asciiCapableDevices; i++)
			{
				try
				{
					port.ReadLine();
				}
				catch (TimeoutException)
				{
					LogErrorMessage("Tried to convert {0} devices to "
								+ "115200 baud, but only received {1} responses.",
									asciiCapableDevices,
									i);
					break;
				}
			}

			port.BaudRate = 115200;

			return asciiCapableDevices;
		}


		private static void SwitchToAscii(IRawPort port, int baudRate, int expectedResponseCount)
		{
			port.Write(Pack(0, Command.ConvertToASCII, baudRate));
			Thread.Sleep(_sleepTime);

			var reply = new byte[6];
			for (var i = 0; i < expectedResponseCount; i++)
			{
				try
				{
					reply = port.Read(6);
				}
				catch (TimeoutException)
				{
					// This is a warning and not an error, since devices can
					// block replies returning from upstream devices if they
					// change their protocol/baud too fast.
					Log.WarnFormat("Tried to convert {0} devices to "
							   + "ASCII, but only received {1} responses.",
								   expectedResponseCount,
								   i);
					break;
				}

				Log.Debug("Converted device to ASCII. Received reply: " + reply);
			}

			port.BaudRate = baudRate;
			port.ClearReceiveBuffer();
		}


		private static int SwitchToBinary(IRawPort port, int baudRate, int expectedResponseCount)
		{
			port.Write(string.Format("/tools setcomm {0} 1\r\n", baudRate));
			Thread.Sleep(_sleepTime);

			var rejected = 0;
			for (var i = 0; i < expectedResponseCount; i++)
			{
				string line;
				try
				{
					line = port.ReadLine();
					if (line.Contains(WarningFlags.BootloaderMode)) // Bootloader won't switch to binary.
					{
						rejected++;
					}
				}
				catch (TimeoutException)
				{
					// This is a warning and not an error, since devices can
					// block replies returning from upstream devices if they
					// change their protocol/baud too fast.
					Log.WarnFormat("Tried to convert {0} devices to "
							   + "Binary, but only received {1} responses.",
								   expectedResponseCount,
								   i);
					break;
				}

				Log.Debug("Converted device to Binary. Received reply: "
					  + line);
			}

			port.BaudRate = baudRate;
			port.ClearReceiveBuffer();

			return rejected;
		}


		private static int CountBinaryDevices(IRawPort port)
		{
			var count = 0;
			var reply = new byte[6];

			port.Write(Pack(0, Command.ReturnDeviceID, 0));
			while (true)
			{
				try
				{
					// Read exactly 6 bytes.
					reply = port.Read(6);

					count++;

					// Detect manual knobs in off-detent position.
					if (Unpack(reply, out var devNum, out var command, out var position))
					{
						Log.InfoFormat("Received command: {0}", command.ToString());
						if (command == Command.ManualMoveTracking)
						{
							port.ClearReceiveBuffer();
							throw new DeviceFaultException(
								$"Manual device movement was detected. This can interfere with the detection process. Please ensure all devices with manual movement knobs are at the detent position. The offending device number is {devNum}.");
						}
					}
				}
				catch (TimeoutException)
				{
					port.ClearReceiveBuffer();
					break;
				}
			}

			return count;
		}


		private static int CountAsciiDevices(IRawPort port)
		{
			port.Write("/get deviceid\r\n");
			var count = 0;
			while (true)
			{
				try
				{
					port.ReadLine();
					count++;
				}
				catch (TimeoutException)
				{
					port.ClearReceiveBuffer();
					break;
				}
			}

			return count;
		}


		private static byte[] Pack(byte deviceNumber, Command commandNumber, int data)
		{
			var packet = new DataPacket
			{
				DeviceNumber = deviceNumber,
				Command = commandNumber,
				NumericData = data,
				Measurement = new Measurement(data, UnitOfMeasure.Data)
			};

			return PacketConverter.GetBytes(packet);
		}


		private static bool Unpack(byte[] aPacket, out byte aDeviceNumber, out Command aCommandNumber, out int aData)
		{
			aDeviceNumber = 0;
			aCommandNumber = Command.Error;
			aData = 0;

			if (aPacket.Length != 6)
			{
				return false;
			}

			aDeviceNumber = aPacket[0];
			aCommandNumber = (Command) aPacket[1];

			uint temp = 0;
			for (var i = 0; i < 4; ++i)
			{
				temp <<= 8;
				temp |= aPacket[i + 2];
			}

			aData = (int) temp;

			return true;
		}

		#endregion

		#region -- Data --

		private static readonly ILog Log =
			LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private static ILoggable _statusLog;
		private static int _sleepTime = 1000;

		#endregion
	}

	internal class MismatchedDevicesException : Exception
	{
	}
}

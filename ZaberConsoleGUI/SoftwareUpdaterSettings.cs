﻿using System;

namespace ZaberConsole
{
	/// <summary>
	///     User settings for <see cref="SoftwareUpdaterVM" />.
	/// </summary>
	[Serializable]
	public class SoftwareUpdaterSettings
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     The program is allowed to automatically check for new installers
		///     online at suitable moments. This does not imply automatically downloading them.
		/// </summary>
		public bool AutomaticallyCheckForUpdates { get; set; } = true;

		#endregion
	}
}

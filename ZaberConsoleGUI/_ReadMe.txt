Zaber Console
--------------------------

1. INTRODUCTION

Zaber Console is an application that lets you send commands to your Zaber 
devices, adjust their settings, and create simple scripts of commands. Visit
our wiki page for a guided tour and lots of sample scripts:
http://www.zaber.com/wiki/Software/Zaber_Console


2. RELEASE INFO

This release contains the binary files to run Zaber Console and Zaber Script 
Runner.

The Zaber Console and the Zaber library are released under the 
terms of the Apache Software License. Some third-party source code 
is included under different license terms (see license.txt).

   Copyright (c) 2017 Zaber Technologies Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.


3. INSTALLATION

For full installation instructions, visit the Zaber wiki:
http://www.zaber.com/wiki/Software/Zaber_Console

Zaber Console is available as an executable installer or in source
code form. Running the installer will download and install any missing 
Microsoft Windows components, and then install Zaber Console. 
It adds a Zaber Technologies folder to your Start menu.

If the automated install doesn't work for you, try the following:

Make sure you have Microsoft.NET framework 4.0 installed, or download it:
http://www.microsoft.com/en-us/download/details.aspx?id=17851

To determine which versions of the .NET Framework are installed and whether 
service packs have been applied, read this article:
http://support.microsoft.com/kb/318785



// Example script for transmitting a list of stream commands live to 
// a device. Requires a two-axis device that supports streams to be selected.
// Uses stream 1 and axes 1 and 2. Scripts similar to this can be
// generated from G-Code input using the G-Code Translator tab in
// Zaber Console.

#template(simple)

// Sequence of stream commands to send to the device.
// Edit this to suit your application.
Queue<string> commandList = new Queue<string>(new[]
{
	"stream 1 setup live 1 2",
    "stream 1 set maxspeed 630000",
    "stream 1 line abs 0 0",
    "stream 1 set maxspeed 87381",
    "stream 1 line abs 0 32000",
    "stream 1 line abs 64000 0",
    "stream 1 line abs 64000 32000",
    "stream 1 set maxspeed 630000",
    "stream 1 line abs 0 0",
});

var conv = Conversation;
TimeoutTimer timer500 = new TimeoutTimer { Timeout = 500 };

string strerror = null;

// Check that the device has its home reference, and home it if not.
if (conv.Request("").FlagText == "WR")
{
	Output.WriteLine("Device is not homed. Home now? [y/N]");
	if (Input.ReadLine().ToLowerInvariant() == "y")
	{
		conv.Request("home");
		conv.PollUntilIdle();
	}
	else
	{
		Output.WriteLine("Aborted.");
		return;
	}
}

// Clear the NI flag if set.
conv.Request("estop");
conv.Request("estop");

bool busy = true;

// Main loop - process commands from the list until done.
while (commandList.Count > 0 || busy)
{
	DeviceMessage msg = null;

	try
	{
		var topic = conv.StartTopic();
		if (commandList.Count < 1)
		{
			// Last command already sent; wait for idle.
			conv.Device.Send("");
		}
		else
		{
			// Send next command from queue.
			conv.Device.Send(commandList.Peek());
		}

		// Wait for a response from the device.
		if (!topic.Wait(timer500))
		{
			throw new RequestTimeoutException();
		}

		msg = topic.Response;
	}
	catch (Exception x) // Handle errors.
	{
		strerror = string.Format("Command {0} was rejected: {1}. Aborted.",
            commandList.Count > 0 ? commandList.Peek().ToString() : "polling", x.ToString());
		break;
	}

	// If the device's stream queue is full, we'll wait a while and try the same command again.
	if (msg.IsAgain)
	{
		System.Threading.Thread.Sleep(50);
		continue;
	}

	// Handle error responses.
	if (msg.IsError)
	{
		strerror = string.Format("Command {0} was rejected: {1}. Execution stopped.",
            commandList.Count > 0 ? commandList.Peek().ToString() : "polling", msg.TextData);
		break;
	}

	// Command was accepted;
	if (msg.FlagText == "--" || msg.FlagText == "ND")
	{
		// if it was not the last command, remove it from the queue.
		if (commandList.Count > 0)
		{
			commandList.Dequeue();
		}
		else if (msg.IsIdle) // if it was the last command, wait for device idle.
		{
			// loop can now exit on next cycle
			busy = false;
		}

		continue;
	}

	strerror = string.Format("Device replied with flag {0}. Execution stopped.", msg.FlagText);
	break;
}


// Clear the NI flag.
conv.Request("estop");
conv.Request("estop");

// Output error message if any.
if (strerror != null)
{
	Output.WriteLine(strerror);
}


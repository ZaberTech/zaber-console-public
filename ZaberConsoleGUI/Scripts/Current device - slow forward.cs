/* Start the current device moving forward */
/* The template declaration wraps the script in common code that's
 * the same for most scripts. See the Help tab for more info.
 */
#template(Simple)

if(PortFacade.Port.IsAsciiMode)
{
	Conversation.Request("move vel", 1000);
}
else
{
	Conversation.Request(Command.MoveAtConstantSpeed, 1000);
}

/* This file is a demonstration script that shows the basic features.
 * For a full explanation and more documentation, see the Zaber wiki:
 * https://www.zaber.com/wiki/Software/Zaber_Console
 */
/* The template declaration wraps the script in common code that's
 * the same for most scripts. See the Help tab for more info.
 */
#template(Simple)

// First check the current position and display it
int currentPosition = 
    Conversation.Request(Command.ReturnSetting, (int)Command.SetCurrentPosition).Data; 
Output.WriteLine("Current position is {0} microsteps.", currentPosition);

// Ask user for instructions
Output.WriteLine("How many moves would you like to make?");
String line = Input.ReadLine();
int moveCount = Convert.ToInt32(line);

Output.WriteLine("How many microsteps would you like in each move?");
line = Input.ReadLine();
int distance = Convert.ToInt32(line);
int totalDistance = moveCount * distance;

// Check that there is room for the requested moves
if (totalDistance > currentPosition)
{
    // complain to user
    Output.WriteLine("Not enough room for that.");

    // skip the rest of the script
    return;
}

// Loop through all the requested moves
for (int i = 0; i < moveCount; i++)
{
    int newPosition =
        Conversation.Request(Command.MoveRelative, -distance).Data;
    Output.WriteLine("New position is {0} microsteps.", newPosition);
}
// Return to starting position
int finalPosition =
    Conversation.Request(Command.MoveRelative, moveCount*distance).Data;
Output.WriteLine("New position is {0} microsteps.", finalPosition);
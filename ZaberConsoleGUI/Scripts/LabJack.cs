#template(simple)

// This example scripts demonstrates how to use the #import and #using 
// script directives to use .NET libraries that Zaber Console does not
// normally provide access to. This example shows how to read the
// inputs from a LabJack T4 over USB. You must already have the 
// LabJack .NET library installed in the location this script expects
// to find it (see below).

// The #import directive tells Zaber Console to add a reference to
// the named DLL file when compiling your script. Using quotation
// marks is required if the path to the DLL file includes spaces.
// The DLL must be a .NET assembly and if it imports any native
// DLLs it is responsible for selecting the right one for the
// computer you are running on.
#import("C:\Program Files (x86)\LabJack\Drivers\LabJack.LJM.dll")

// The #using directive tells Zaber Console to add a "using" or
// "import" statement (depending on what language you are using) for
// a namespace that isn't normally included in the script template.
// You only need this if you are using a #template statement in your
// script, and you do not need it if you are using Python as your
// script language.
#using(LabJack)

int handle = 0;
int devType = 0;
int conType = 0;
int serNum = 0;
int ipAddr = 0;
int port = 0;
int maxBytesPerMB = 0;
string ipAddrStr = "";
int errorAddress = 0;

try
{
	// This code was adapted from one of LabJack's .NET examples.
	
	// Open first found LabJack device.
	LJM.OpenS("ANY", "ANY", "ANY", ref handle);

	LJM.GetHandleInfo(handle, ref devType, ref conType, ref serNum, ref ipAddr, ref port, ref maxBytesPerMB);
	LJM.NumberToIP(ipAddr, ref ipAddrStr);
	Output.WriteLine("Opened a LabJack with Device type: " + devType + ", Connection type: " + conType + ",");
	Output.WriteLine("Serial number: " + serNum + ", IP address: " + ipAddrStr + ", Port: " + port + ",");
	Output.WriteLine("Max bytes per MB: " + maxBytesPerMB);
	Output.WriteLine();
		
	// Read the first two analog inputs every 500ms and print their values until the script ends.
	var values = new double[] { 0, 0 };
	var names = new string[] { "AIN0", "AIN1" };
	while (true)
	{
		LJM.eReadNames(handle, 1, names, values, ref errorAddress);
		Output.Write("Ain 0: " + values[0].ToString());
		Output.WriteLine("  Ain 1: " + values[1].ToString());
		Sleep(500);
	}
}
catch (LJM.LJMException e)
{
	Output.WriteLine(e.ToString());
}

LJM.CloseAll();  //Close all handles

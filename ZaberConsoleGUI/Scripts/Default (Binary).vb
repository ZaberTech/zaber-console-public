' This file is a demonstration script that shows the basic features.
' For a full explanation and more documentation, see the Zaber wiki:
' http://www.zaber.com/wiki/Software/Zaber_Console

' The template declaration wraps the script in common code that's
' the same for most scripts. See the Help tab for more info.
#template(Simple)

' First check the current position and display it
Dim currentPosition As Integer = _
    Conversation.Request(Command.ReturnSetting, Command.SetCurrentPosition).Data
Output.WriteLine("Current position is {0} microsteps.", currentPosition)

' Ask user for instructions
Output.WriteLine("How many moves would you like to make?")
Dim line As String = Input.ReadLine()
Dim moveCount As Integer = Convert.ToInt32(line)

Output.WriteLine("How many microsteps would you like in each move?")
line = Input.ReadLine()
Dim distance As Integer = Convert.ToInt32(line)
Dim totalDistance As Integer = moveCount * distance

' Check that there is room for the requested moves
If totalDistance > currentPosition Then
    ' complain to user
    Output.WriteLine("Not enough room for that.")

    ' skip the rest of the script
    Return
End If

' Loop through all the requested moves
For i As Integer = 1 To moveCount
    Dim newPosition As Integer = _
        Conversation.Request(Command.MoveRelative, -distance).Data
    Output.WriteLine("New position is {0}.", newPosition)
Next
' Return to starting position
Dim finalPosition As Integer = _
    Conversation.Request(Command.MoveRelative, moveCount*distance).Data
Output.WriteLine("New position is {0}.", finalPosition)

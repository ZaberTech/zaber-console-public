// Example of how to convert between real-world measurements and device units.

#template(simple)

// Check that a single movable axis is selected.
var device = Conversation.Device;
if ((device is DeviceCollection)
    || !device.IsSingleDevice
    || (device.DeviceType.MotionType == MotionType.None)
    || (device.Axes.Count > 1))
{
    Output.WriteLine("Please select a single axis.");
    return;
}


// Home the stage.
if (PortFacade.Port.IsAsciiMode)
{
    Conversation.Request("home");
    Conversation.PollUntilIdle();
}
else
{
    Conversation.Request(Command.Home, 0);
}

// Print out instructions.
Output.WriteLine("Enter relative distances to move, as numbers followed by an optional unit symbol.");
Output.WriteLine("The unit symbol must be separated from the number by a space.");
Output.WriteLine("If you omit the symbol, the default unit is native device units (microsteps in most cases).");
Output.WriteLine("Enter a blank line to end the script.");

// Print out all the units we can convert between for this device.
Output.WriteLine("The selected device supports these units of position:");
foreach (var uom in Conversation.Device.GetUnitsOfMeasure(MeasurementType.Position))
{
	if (UnitOfMeasure.Data != uom) // Don't show data because it's implicit when no symbol entered.
	{
		Output.WriteLine(uom.Abbreviation);
	}
}
Output.WriteLine();
	
// Main loop - wait for input, process it, move the device, repeat until an empty line is input.
while (true)
{
    // Get user input.
	var input = Input.ReadLine();
	if (string.IsNullOrEmpty(input))
	{
		break;
	}
		
    // Split the input into words and parse the number part.
	input = input.Trim();
	var inputWords = input.Split(' ');
    double distance = 0.0;
    try
    {
        distance = double.Parse(inputWords[0]);
    }
    catch (Exception aException)
    {
        Output.WriteLine(aException.Message);
        continue;
    }

    // Attempt to parse the unit symbol if present in the input.
    string symbol = (inputWords.Length > 1) ? inputWords[1] : null;

    UnitOfMeasure uom = null;
    if (!string.IsNullOrEmpty(symbol))
    {
        uom = device.UnitConverter.FindUnitByAbbreviation(symbol);
    }

    if (null != uom)
    {
        // If we found the unit, issue the move command.
        Output.WriteLine(string.Format("Moving by {0} {1}.", distance, uom.Abbreviation));
        try
        {
            decimal pos = 0;
            if (PortFacade.Port.IsAsciiMode)
            {
                Conversation.RequestInUnits("move rel", distance, uom);
                Conversation.PollUntilIdle();
                var data = Conversation.Request("get pos").Data;
                pos = device.ConvertData(data, uom).Value;
            }
            else
            {
                Conversation.RequestInUnits(Command.MoveRelative, distance, uom);
                var data = Conversation.Request(Command.ReturnCurrentPosition).Data;
                pos = device.ConvertData(data, uom).Value;
            }

            Output.WriteLine("Current device position is {0} {1}.", pos, uom.Abbreviation);
        }
        catch (Exception aException)
        {
            Output.WriteLine(aException.Message);
        }
    }
    else if (string.IsNullOrEmpty(symbol))
    {
        // No unit symbol was entered - issue the move using microstep units, then report the new absolute position.
        Output.WriteLine(string.Format("Moving by {0} microsteps.", distance));
        try
        {
            int pos = 0;
            if (PortFacade.Port.IsAsciiMode)
            {
                Conversation.Request("move rel", (int)distance);
                Conversation.PollUntilIdle();
                pos = Conversation.Request("get pos").Data;
            }
            else
            {
                Conversation.Request(Command.MoveRelative, (int)distance);
                pos = Conversation.Request(Command.ReturnCurrentPosition).Data;
            }

            Output.WriteLine(string.Format("Device position is now {0} microsteps.", pos));
        }
        catch (Exception aException)
        {
            Output.WriteLine(aException.Message);
        }
    }
    else
    {
        Output.WriteLine(string.Format("Could not identify unit symbol {0}.", symbol));
    }
}

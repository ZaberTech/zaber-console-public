/* This file is a demonstration script that shows how to work with 
 * Zaber products with multiple axes.
 * For a full documentation, see the Zaber wiki:
 * https://www.zaber.com/wiki/Software/Zaber_Console
 */
/* The template declaration wraps the script in common code that's
 * the same for most scripts. See the Help tab for more info.
 */
#template(Simple)

byte deviceAddress = Conversation.Device.DeviceNumber;
const int axisNumber1 = 1;
const int axisNumber2 = 2;

// check that this script is running on a device with 2 or more axes
if( Conversation.Device.AxisNumber != 0 )
{
	Output.WriteLine("Please select a device and not an axis conversation.");
	return;
}
if( Conversation.Axes.Count < 2 )
{
	Output.WriteLine("Please select a multiaxis device.");
	return;
}

// Axis conversations can be obtained from PortFascade
Conversation conversation1 = PortFacade.GetConversation(deviceAddress, axisNumber1);
Conversation conversation2 = PortFacade.GetConversation(deviceAddress, axisNumber2);

// Alternatively, the same conversation for each axis from the device conversation
//Conversation conversation1 = Conversation.Axes[axisNumber1-1]; // index for Axes starts from zero
//Conversation conversation2 = Conversation.Axes[axisNumber2-1];

// Home all axes and wait for completion
Output.WriteLine("Homing all axes");
Conversation.Request("home");	// command applies to all axes on this device
Conversation.PollUntilIdle();	// this blocks until all axes are idle

// Moving axes one at a time
Output.WriteLine("Moving axes one at a time");
conversation1.Request("move max");
conversation1.PollUntilIdle();
Output.WriteLine("Axis {0} arrives at position {1}", axisNumber1, conversation1.Request("get pos").Data);

conversation2.Request("move max");
conversation2.PollUntilIdle();
Output.WriteLine("Axis {0} arrives at position {1}", axisNumber2, conversation2.Request("get pos").Data);

// Moving both axes at the same time
Output.WriteLine("Moving both axes at the same time");
conversation1.Request("move min");
conversation2.Request("move min");
conversation1.PollUntilIdle();
conversation2.PollUntilIdle();
Output.WriteLine("Axis {0} is at position {1}", axisNumber1, conversation1.Request("get pos").Data);
Output.WriteLine("Axis {0} is at position {1}", axisNumber2, conversation2.Request("get pos").Data);

// Read minimum and maximum positions
int minPos1 = conversation1.Request("get limit.min").Data;
int minPos2 = conversation2.Request("get limit.min").Data;

// Cycle both axes until user hits Cancel button
Output.WriteLine("Cycling axes, hit Cancel button to stop.");
while(!IsCanceled)
{
	// check whether each axis is idle and move them
	DeviceMessage response;
	response = conversation1.Request("get pos");
	if( response.IsIdle )
	{
		int currentPos = response.Data;
		if(currentPos > minPos1)
		{
			conversation1.Request("move min");
		}
		else
		{
			conversation1.Request("move max");
		}
	}
	
	response = conversation2.Request("get pos");
	if( response.IsIdle )
	{
		int currentPos = response.Data;
		if(currentPos > minPos2)
		{
			conversation2.Request("move min");
		}
		else
		{
			conversation2.Request("move max");
		}
	}
}


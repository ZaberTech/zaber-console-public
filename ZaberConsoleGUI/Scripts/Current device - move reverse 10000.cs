/* Move the current device back 10000 microsteps */
/* The template declaration wraps the script in common code that's
 * the same for most scripts. See the Help tab for more info.
 */
#template(Simple)

if(PortFacade.Port.IsAsciiMode)
{
	Conversation.Request("move rel", -10000);
	Conversation.PollUntilIdle();
}
else
{
	Conversation.Request(Command.MoveRelative, -10000);
}

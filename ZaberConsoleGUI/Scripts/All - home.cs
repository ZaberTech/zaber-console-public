/* Send the Home command to all devices */
/* The template declaration wraps the script in common code that's
 * the same for most scripts. See the Help tab for more info.
 */
#template(Simple)

if(PortFacade.Port.IsAsciiMode)
{
	Conversation conversation = PortFacade.GetConversation(0,0);
	conversation.Request("home");
	conversation.PollUntilIdle();
}
else
{
	PortFacade.GetConversation(0).Request(Command.Home);
}

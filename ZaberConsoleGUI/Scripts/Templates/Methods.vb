' This template doesn't include a method declaration, so
' you can define more than one method in your script.
' Remember: you must declare a Run method like this:
' Public Overrides Sub Run()
' The same properties and methods are available from the 
' PlugInBase class:
' Input, Output, Log(message, exception), PortFacade, Conversation, 
' IsCanceled, Sleep(milliseconds), and CheckForCancellation().

Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.IO
Imports Zaber
Imports Zaber.PlugIns

Public Class Methods
    Inherits PlugInBase

    ' $INSERT-SCRIPT-HERE$
End Class

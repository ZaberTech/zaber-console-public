""" This file is a demonstration script that shows the basic features.

For a full explanation and more documentation, see the Zaber wiki:
http://www.zaber.com/wiki/Software/Zaber_Console
"""
import sys

# First check the current position and display it
current_position = conversation.Request("get pos").Data
print "Current position is %d microsteps." % current_position

# Ask user for instructions
move_count = input("How many moves would you like to make?")
distance = input("How many microsteps would you like in each move?")
total_distance = move_count * distance

# Check that there is room for the requested moves
if total_distance > current_position:
    # complain to user
    print "Not enough room for that."

    # skip the rest of the script
    sys.exit(0)

# Loop through all the requested moves
for _ in range(move_count):
    conversation.Request("move rel", -distance)
    conversation.PollUntilIdle()
    new_position = conversation.Request("get pos").Data
    print "New position is %d microsteps." % new_position

# Return to starting position
conversation.Request("move rel", move_count*distance)
conversation.PollUntilIdle()
final_position = conversation.Request("get pos").Data
print "New position is %d microsteps." % final_position

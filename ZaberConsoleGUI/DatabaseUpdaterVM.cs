﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using log4net;
using Zaber.Telemetry;
using Zaber.Threading;
using ZaberConsole.Plugins;
using ZaberConsole.Properties;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Dialogs;

namespace ZaberConsole
{
	/// <summary>
	///     Helper class for downloading device database updates from Zaber.
	/// </summary>
	public partial class DatabaseUpdaterVM : ObservableObject, IDialogClient
	{
		#region -- Public API --

		/// <summary>
		///     Used to select icons and text for the database updater button.
		/// </summary>
		public enum State
		{
			UpToDate,
			UpdateAvailable,
			CheckingForUpdates,
			Downloading,
			Importing,
			Exporting,
			Compressing,
			Decompressing,
			UpdateReady,
			Uncertain,
			Error
		}


		/// <summary>
		///     Event used to enable the updater to show messages on the status bar.
		/// </summary>
		public event PlugInManager.StatusMessageHandler ShowStatusMessage;


		/// <summary>
		///     Initialize the database updater with configuration info.
		/// </summary>
		/// <param name="aDownloadDir">The directory to place downloaded database files in.</param>
		/// <param name="aDbFilename">The consistent part of the filename to give to downloaded database files.</param>
		/// <param name="aDbUrl">URL to download database updates from.</param>
		/// <param name="aDefaultDbPath">Path to the default (bundled) database for fallback purposes.</param>
		/// <param name="aUserSettings">
		///     User settings from previous session. A reference to this object is
		///     saved internally and settings may be modified; the calling program is responsible for persisting
		///     the same instance to the next session.
		/// </param>
		public DatabaseUpdaterVM(string aDownloadDir, string aDbFilename, string aDbUrl, string aDefaultDbPath,
								 DatabaseUpdaterSettings aUserSettings)
		{
			_dispatcher = DispatcherHelper.Dispatcher;

			// The UI update timer has to be created in the UI thread or it will be
			// blocked by operations happening in the worker thread.
			_timer = DispatcherTimerHelper.CreateTimer();

			_downloadDir = aDownloadDir;
			_downloadFilenameBase = aDbFilename;
			_downloadUrl = aDbUrl;
			_defaultDbPath = aDefaultDbPath;

			// Most of the user settings are passed in rather than referred to statically to improve
			// testability.
			_settings = aUserSettings;

			CurrentState = _settings.AutomaticallyCheckForUpdates ? State.UpToDate : State.Uncertain;

			try
			{
				LatestDatabasePath = FindLatestDatabaseOnDisk();
				CleanupOldDatabaseFiles(LatestDatabasePath);
			}
			catch (Exception ex)
			{
				_log.Error("Database updater initialization failed.", ex);
				_enabled = false;
				CurrentState = State.Error;
				ErrorMessage = Resources.STR_DB_INIT_FAILED;
			}

			_autoUpdateTimer = DispatcherTimerHelper.CreateTimer();
			_autoUpdateTimer.Interval = TimeSpan.FromDays(1);
			_autoUpdateTimer.Tick += AutoUpdateTimer_Tick;
			_autoUpdateTimer.Start();
		}


		/// <summary>
		///     Halt any active updater operations when the program is exiting.
		/// </summary>
		public void Shutdown()
		{
			_autoUpdateTimer.Tick -= AutoUpdateTimer_Tick;
			_autoUpdateTimer.Stop();
			Cancel();
		}


		/// <summary>
		///     Returns the path to the latest available database. This will either be the most recently
		///     downloaded file, or the file bundled with the application if none have been downloaded.
		///     This will never refer to a partially downloaded file.
		/// </summary>
		public string LatestDatabasePath { get; private set; }


		/// <summary>
		///     Returns true if a database download/import/export/check attempt is currently in progress.
		/// </summary>
		public bool IsBusy => null != _worker;


		/// <summary>
		///     Cancel the database download/import/export/check if it is in progress.
		/// </summary>
		public void Cancel()
		{
			lock (_syncRoot)
			{
				_worker?.CancelAsync();
			}
		}


		/// <summary>
		///     Notify the database updater that the program is now using a given database file.
		///     This is for UI update purposes. This does not trigger a cleanup of old files.
		/// </summary>
		/// <param name="aPath">The full path of the database file that is being opened next.</param>
		public void NotifyDatabaseInUse(string aPath) => _dispatcher.BeginInvoke(() =>
		{
			if (State.UpdateReady == CurrentState)
			{
				CurrentState = State.UpToDate;
			}

			OnPropertyChanged(nameof(UsingImportedDatabase));
		});


		/// <summary>
		///     Remove any previously downloaded database files that are now out of date.
		/// </summary>
		/// <param name="aListOfFilesToKeep">List of files to specifically not delete.</param>
		public void CleanupOldDatabaseFiles(params string[] aListOfFilesToKeep)
		{
			var skip = new HashSet<string>(aListOfFilesToKeep)
			{
				// Never remove the bundled file or the currently recommended file.
				_defaultDbPath,
				LatestDatabasePath
			};

			foreach (var fileName in Directory.EnumerateFiles(_downloadDir, _downloadFilenameBase + "*"))
			{
				if (!skip.Contains(fileName))
				{
					TempFileManager.RegisterForDelete(fileName);
					if (fileName == _settings.ImportedDatabasePath)
					{
						_settings.ImportedDatabasePath = null;
						OnPropertyChanged(nameof(UsingImportedDatabase));
					}
				}
			}

			TempFileManager.TryDeleteFiles();
		}

		#endregion

		#region -- View data --

		/// <summary>
		///     Get or set the progress bar maximum.
		/// </summary>
		public double ProgressMaximum
		{
			get => _progressMaximum;
			set => Set(ref _progressMaximum, value, nameof(ProgressMaximum));
		}


		/// <summary>
		///     Get or set the progress bar value.
		/// </summary>
		public double Progress
		{
			get => _progress;
			set => Set(ref _progress, value, nameof(Progress));
		}


		/// <summary>
		///     Get or set the error message for the button tooltip.
		/// </summary>
		public string ErrorMessage
		{
			get => _errorMessage;
			set => Set(ref _errorMessage, value, nameof(ErrorMessage));
		}


		/// <summary>
		///     Used to programmatically show or hide the drop-down menu.
		/// </summary>
		public bool DropDownVisible
		{
			get => _dropDownVisible;
			set => Set(ref _dropDownVisible, value, nameof(DropDownVisible));
		}


		/// <summary>
		///     Checkbox in the context menu controls whether automatic update checks should happen.
		/// </summary>
		public bool AutoCheckForUpdates
		{
			get => _settings.AutomaticallyCheckForUpdates;
			set
			{
				if (_settings.AutomaticallyCheckForUpdates != value)
				{
					_settings.AutomaticallyCheckForUpdates = value;
					OnPropertyChanged(nameof(AutoCheckForUpdates));
					if (value)
					{
						CheckForUpdates(LatestDatabasePath);
					}
				}
			}
		}


		/// <summary>
		///     Checkbox in the context menu controls whether new databases should automatically be downloaded.
		/// </summary>
		public bool AutoDownload
		{
			get => _settings.AutomaticallyDownloadUpdates;
			set
			{
				if (_settings.AutomaticallyDownloadUpdates != value)
				{
					_settings.AutomaticallyDownloadUpdates = value;
					OnPropertyChanged(nameof(AutoDownload));
				}
			}
		}


		/// <summary>
		///     Controls the icon displayed in the button.
		/// </summary>
		public State CurrentState
		{
			get => _currentState;
			set => Set(ref _currentState, value, nameof(CurrentState));
		}


		/// <summary>
		///     Indicates whether a user-imported database is currently in use.
		/// </summary>
		public bool UsingImportedDatabase
			=> !string.IsNullOrEmpty(_settings.ImportedDatabasePath);


		/// <summary>
		///     Command invoked by left-clicking the button.
		/// </summary>
		public ICommand ButtonCommand
			=> OnDemandRelayCommand(ref _buttonCommand,
									_ => OnButtonClicked(),
									_ => _enabled);


		/// <summary>
		///     Command invoked by the force download context menu item.
		/// </summary>
		public ICommand ForceDownloadCommand
			=> OnDemandRelayCommand(ref _forceDownloadCommand,
									_ =>
									{
										_dispatcher.BeginInvoke(() => { DropDownVisible = false; });

										OnForceDownload();
									});


		/// <summary>
		///     Command invoked by the import context menu item.
		/// </summary>
		public ICommand ImportCommand
			=> OnDemandRelayCommand(ref _importCommand, _ => ImportDatabase());


		/// <summary>
		///     Command invoked by the export context menu item.
		/// </summary>
		public ICommand ExportCommand
			=> OnDemandRelayCommand(ref _exportCommand, _ => ExportDatabase());


		/// <summary>
		///     Command invoked by the copy url context menu item.
		/// </summary>
		public ICommand GetUrlCommand
			=> OnDemandRelayCommand(ref _getUrlCommand,
									_ =>
									{
										_dispatcher.BeginInvoke(() =>
										{
											DropDownVisible = false;
											Clipboard.SetDataObject(Configuration.DatabaseUrl);
										});

										ShowStatusMessage?.Invoke(this, Resources.STR_DB_URL_COPIED, STATUS_DURATION);
									});

		#endregion

		#region -- IDialogClient --

		public event RequestDialogChange RequestDialogOpen;
		#pragma warning disable CS0067 // Event never used.
		public event RequestDialogChange RequestDialogClose;
		#pragma warning restore CS0067

		#endregion

		#region -- Private code --

		private void AutoUpdateTimer_Tick(object sender, EventArgs e)
		{
			switch (CurrentState)
			{
				case State.CheckingForUpdates: // Deliberate fall-through.
				case State.Downloading:        // Deliberate fall-through.
				case State.Importing:          // Deliberate fall-through.
				case State.Exporting:          // Deliberate fall-through.
				case State.Compressing:        // Deliberate fall-through.
				case State.Decompressing:
					_log.Info("Timed database update check skipped.");
					break;
				case State.UpToDate:           // Deliberate fall-through.
				case State.UpdateAvailable:    // Deliberate fall-through.
				case State.UpdateReady:        // Deliberate fall-through.
				case State.Uncertain:          // Deliberate fall-through.
				case State.Error:
					_log.Info("Timed database update check triggered.");
					if (_settings.AutomaticallyCheckForUpdates)
					{
						CheckForUpdates(LatestDatabasePath);
					}
					else
					{
						CurrentState = State.Uncertain;
					}
					break;
				default:
					throw new InvalidProgramException($"Unhandled case {CurrentState}!");
			}
		}


		private string FindLatestDatabaseOnDisk()
		{
			string recommended = null;

			// If the last action was to import a custom database, continue using that.
			if (!string.IsNullOrEmpty(_settings.ImportedDatabasePath))
			{
				if (File.Exists(_settings.ImportedDatabasePath))
				{
					return _settings.ImportedDatabasePath;
				}

				// File no longer exists.
				_settings.ImportedDatabasePath = null;
				OnPropertyChanged(nameof(UsingImportedDatabase));
			}

			if (!Directory.Exists(_downloadDir))
			{
				Directory.CreateDirectory(_downloadDir);
			}

			var latest = DateTime.MinValue;

			foreach (var fileName in Directory.EnumerateFiles(_downloadDir, _downloadFilenameBase + "*"))
			{
				var timestamp = File.GetLastWriteTimeUtc(fileName);
				if (timestamp > latest)
				{
					latest = timestamp;
					recommended = fileName;
				}
			}

			if (recommended is null
			|| (File.GetLastWriteTimeUtc(recommended) < File.GetLastWriteTimeUtc(_defaultDbPath)))
			{
				recommended = _defaultDbPath;
				if (!File.Exists(recommended))
				{
					throw new InvalidDataException("Fatal: Could not find bundled device database.");
				}
			}

			return recommended;
		}


		private void OnButtonClicked()
		{
			if (!_enabled)
			{
				_log.Error("Database update button action cancelled due to initialization failure.");
				return;
			}

			if (IsBusy)
			{
				Cancel();
			}
			else if (_updateIsAvailable)
			{
				StartDownload();
			}
			else
			{
				CheckForUpdates(LatestDatabasePath);
			}
		}


		private void OnForceDownload()
		{
			if (IsBusy)
			{
				_log.Info("New download forced; cancelling download already in progress.");
				Cancel();
			}

			while (IsBusy)
			{
				Thread.Sleep(100);
			}

			StartDownload();
		}


		private string CreateNewDatabaseName()
		{
			uint maxCount = 0;
			var re = new Regex(@".*\.(\d+)$");

			foreach (var fileName in Directory.EnumerateFiles(_downloadDir, _downloadFilenameBase + "*"))
			{
				var m = re.Match(fileName);
				if (m.Success && (2 == m.Groups.Count))
				{
					if (uint.TryParse(m.Groups[1].Value, out var index))
					{
						maxCount = Math.Max(maxCount, index);
					}
				}
			}

			maxCount++; // Wraparound from uint.max to 0 could happen, but the .0 file should have been removed.
			return Path.Combine(_downloadDir, _downloadFilenameBase + "." + maxCount);
		}


		// Throws an exception if the file doesn't meet minimal Zaber Console requirements.
		private void ValidateDatabaseFile(string aFilePath)
		{
			try
			{
				// The SqliteDeviceDictionary constructor checks for the presence of unit definitions, 
				// param types and unit dimensions. More detailed checks can be added here if needed.
				using (var db = new SqliteDeviceDictionary(aFilePath))
				{
				}
			}
			catch (Exception e)
			{
				_log.Error($"File {aFilePath} is not a valid database.", e);
				throw new InvalidDataException($"The file {aFilePath} is not a valid device database.", e);
			}
		}

		#endregion

		#region -- Private data --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Database updater");
		private static readonly int STATUS_DURATION = 2;

		private readonly object _syncRoot = new object();
		private readonly IDispatcher _dispatcher;
		private readonly IDispatcherTimer _timer;
		private readonly IDispatcherTimer _autoUpdateTimer;
		private IBackgroundWorker _worker;

		private readonly DatabaseUpdaterSettings _settings;

		private readonly string _downloadDir;
		private readonly string _downloadFilenameBase;
		private readonly string _downloadUrl;
		private readonly string _defaultDbPath;

		private bool _updateIsAvailable;

		private double _progressMaximum = 1.0;
		private double _progress;
		private string _errorMessage;
		private bool _dropDownVisible;
		private readonly bool _enabled = true;
		private State _currentState;
		private State _previousState;
		private RelayCommand _buttonCommand;
		private RelayCommand _forceDownloadCommand;
		private RelayCommand _importCommand;
		private RelayCommand _exportCommand;
		private RelayCommand _getUrlCommand;

		#endregion
	}
}

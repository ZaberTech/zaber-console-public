﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using log4net;
using MvvmDialogs.ViewModels;
using Zaber;
using Zaber.Ports;
using Zaber.Telemetry;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Dialogs;
using ZaberWpfToolbox.Logging;

namespace ZaberConsole.Dialogs
{
	public class ReconfigureDialogVM : ObservableObject, ILoggable, IUserDialogViewModel, IDialogClient
	{
		#region -- Public Methods & Properties --

		#region -- Setup --

		public ReconfigureDialogVM(ZaberPortFacade aPort, IRawPort aRawPort)
		{
			Result = MessageBoxResult.None;

			_dispatcher = Dispatcher.CurrentDispatcher;
			_portFacade = aPort;
			_rawPort = aRawPort;
			_tokenSource = new CancellationTokenSource();

			_textVM.Text = _message1;
			_logVM.ButtonsVisible = false;

			CreateStatusTimer();
		}

		#endregion

		#region -- Public API --

		public MessageBoxResult Result { get; private set; }

		#endregion

		#endregion

		#region -- View-bound properties --

		public TextAndRadioButtonsVM TextAndButtonsVM
		{
			get => _textVM;
			set => Set(ref _textVM, value, nameof(TextAndButtonsVM));
		}


		public LogControlVM LogVM
		{
			get => _logVM;
			set => Set(ref _logVM, value, nameof(LogVM));
		}


		public string ProgressText
		{
			get => _progressText;
			set => Set(ref _progressText, value, nameof(ProgressText));
		}


		public bool WorkInProgress
		{
			get => _workInProgress;
			set => Set(ref _workInProgress, value, nameof(WorkInProgress));
		}


		public string LeftButtonText
		{
			get => _leftButtonText;
			set => Set(ref _leftButtonText, value, nameof(LeftButtonText));
		}


		public string RightButtonText
		{
			get => _rightButtonText;
			set => Set(ref _rightButtonText, value, nameof(RightButtonText));
		}


		public bool LeftButtonEnabled
		{
			get => _leftButtonEnabled;
			set => Set(ref _leftButtonEnabled, value, nameof(LeftButtonEnabled));
		}


		public bool RightButtonEnabled
		{
			get => _rightButtonEnabled;
			set => Set(ref _rightButtonEnabled, value, nameof(RightButtonEnabled));
		}


		public bool CloseSignal
		{
			get => _closeSignal;
			set => Set(ref _closeSignal, value, nameof(CloseSignal));
		}


		public ICommand LeftButtonCommand => new RelayCommand(_ => OnLeftButtonClick());


		public ICommand RightButtonCommand => new RelayCommand(_ => OnRightButtonClick());


		public ICommand WindowClosingCommand => new RelayCommand(_ => OnFormClosing());


		public ICommand EscapeCommand => new RelayCommand(_ =>
		{
			if (WorkInProgress)
			{
				Cancel();
			}
			else
			{
				RequestClose();
			}
		});

		#endregion

		#region -- IUserDialogViewModel implementation --

		public bool IsModal => true;


		public void RequestClose() => DialogClosing?.Invoke(this, null);


		public event EventHandler DialogClosing;


		public event EventHandler BringToFront;


		public void RequestBringToFront() => BringToFront?.Invoke(this, null);


		/// <summary>
		///     Any exception that was unhandled by the dialog itself.
		/// </summary>
		public Exception Exception { get; set; }

		#endregion

		#region -- IDialogClient implementation --

		public event RequestDialogChange RequestDialogOpen;
		#pragma warning disable CS0067 // Event is never used.
		public event RequestDialogChange RequestDialogClose;
		#pragma warning restore CS0067

		#endregion

		#region -- Helpers --

		private void OnLeftButtonClick()
		{
			switch (_screen)
			{
				case Screen.Intro:
					Reconfigure();
					break;
			}
		}


		private void OnRightButtonClick()
		{
			switch (_screen)
			{
				case Screen.Intro:
					RequestClose();
					break;
				case Screen.Scan:
					Cancel();
					break;
				case Screen.Results:
					RequestClose();
					break;
			}
		}


		private void Reconfigure()
		{
			if (ZaberPortFacadeState.Closed != _portFacade.CurrentState)
			{
				_portFacade.Close();
			}

			// Suppress the open/closed events while we repeatedly
			// open and close the portFacade.
			_portFacade.SuppressEvents = true;

			LeftButtonText = "Reconfiguring...";
			LeftButtonEnabled = false;
			ProgressText = _message2;
			WorkInProgress = true;
			_screen = Screen.Scan;

			StartStatusTimer();

			Task.Factory.StartNew(() =>
								  {
									  // First reconfigure, then find all the devices.
									  DeviceDetector.ReconfigureDevices(_rawPort, _tokenSource.Token, this);
									  _rawPort.Close();
									  if (_rawPort is IDisposable disposable)
									  {
										  disposable.Dispose();
									  }

									  _rawPort = null;

									  _tokenSource.Token.ThrowIfCancellationRequested();

									  return DeviceDetector.FindDevices(_portFacade, _tokenSource.Token);
								  },
								  _tokenSource.Token)

				 // We have to use ContinueWith instead of async/await because
				 // .NET 4.5 isn't supported on XP. *grumble grumble*
			 .ContinueWith(task =>
				 {
					 _tokenSource.Dispose();
					 _tokenSource = null;

					 var detectedProtocol = Protocol.None;
					 Action del = delegate
					 {
						 WorkInProgress = false;
						 RightButtonEnabled = true;
						 StopStatusTimer();
						 _screen = Screen.Results;
						 ProgressText = string.Empty;

						 switch (task.Status)
						 {
							 case TaskStatus.RanToCompletion:
								 detectedProtocol = ShowResultsAndGetProtocol(task.Result.Item1, task.Result.Item2);

								 // If we found no devices, use DialogResult.Ignore
								 // to signal so to the parent form. 
								 Result = detectedProtocol == Protocol.None ? MessageBoxResult.No : Result;
								 break;

							 case TaskStatus.Canceled:

								 // Set the DialogResult to close the window
								 // next time the button is clicked.
								 Result = MessageBoxResult.Cancel;
								 LeftButtonText = "Cancelled";
								 RightButtonText = "Close";
								 _textVM.RadioButtonsVisibility = Visibility.Collapsed;
								 _textVM.Text = new StringBuilder()
											.AppendLine("Reconfiguration cancelled successfully.")
											.AppendLine()
											.Append("You may now close this window.")
											.ToString();
								 break;

							 case TaskStatus.Faulted:
								 if (null != task.Exception)
								 {
									 foreach (var ex in task.Exception.Flatten().InnerExceptions)
									 {
										 Log.Error("Unhandled exception.", ex);

										 var message = string.Format("Unhandled exception: {0}\r\n\r\n{1}",
																	 ex.ToString(),
																	 ex.Message);

										 if (ex is DeviceFaultException)
										 {
											 // We deliberately throw this exception type to flag manual movement. No need
											 // to display the full exception trace.
											 message = ex.Message;
										 }

										 var mbvm = new MessageBoxParams
										 {
											 Message = message,
											 Caption = "Error reconfiguring",
											 Icon = MessageBoxImage.Exclamation,
											 Buttons = MessageBoxButton.OK
										 };

										 RequestDialogOpen?.Invoke(this, mbvm);
									 }

									 LeftButtonText = "Aborted";
									 RightButtonText = "Close";
									 _logVM.AppendLine("Reconfiguration was aborted by an error.");
								 }

								 break;
						 }
					 };

					 _dispatcher.Invoke(del);

					 if (detectedProtocol == Protocol.ASCII)
					 {
						 ConfigurePortAndDevicesToAscii();
						 _eventLog.LogEvent("Changed all devices to ASCII");
					 }
					 else if (detectedProtocol == Protocol.Binary)
					 {
						 ConfigurePortAndDevicesToBinary();
						 _eventLog.LogEvent("Changed all devices to Binary");
					 }
				 });
		}


		private void Cancel()
		{
			if ((null != _tokenSource) && !_tokenSource.IsCancellationRequested)
			{
				RightButtonText = "Cancelling...";
				RightButtonEnabled = false;
				AppendLine("Cancelling...");

				_tokenSource.Cancel();
			}
		}


		private Protocol ShowResultsAndGetProtocol(List<ZaberDevice> aAsciiDeviceList,
												   List<ZaberDevice> aBinaryDeviceList)
		{
			LeftButtonText = "Done";
			RightButtonText = "Finish";
			_textVM.RadioButtonsVisibility = Visibility.Collapsed;

			// Change the DialogResult to None so that the parent window won't
			// see the user pressing the "Close" button as a cancellation.
			Result = MessageBoxResult.OK;

			if ((0 == aAsciiDeviceList.Count) && (0 == aBinaryDeviceList.Count))
			{
				_textVM.Text = "No devices found.";
				return Protocol.None;
			}

			Protocol detected;
			var sb = new StringBuilder();
			sb.AppendLine("ASCII-capable devices found:");
			var bootloaders = false;
			foreach (var device in aAsciiDeviceList)
			{
				sb.Append(device.DeviceType);

				if (device.IsInBootloaderMode)
				{
					bootloaders = true;
					sb.Append(" (IN BOOTLOADER MODE)");
				}

				sb.AppendLine();
			}

			var notAllDevicesAreAsciiCapable = false;
			sb.AppendLine().AppendLine("Binary-capable devices found:");
			foreach (var device in aBinaryDeviceList)
			{
				sb.AppendLine(device.DeviceType.ToString());
				if (device.FirmwareVersion < 606)
				{
					notAllDevicesAreAsciiCapable = true;
				}
			}

			if (bootloaders)
			{
				detected = Protocol.ASCII;
				sb.AppendLine()
			   .Append("Some devices are in bootloader mode. This usually happens if ")
			   .AppendLine("a firmware update is interrupted or fails.")
			   .Append("The port has been configured to ASCII to allow you to fix them.")
			   .Append("First try issuing a system reset command. If that does not work, ")
			   .Append("try re-applying the firmware.")
			   .AppendLine();
			}
			else if (_textVM.AsciiMode)
			{
				if (notAllDevicesAreAsciiCapable)
				{
					// Found more Binary devices than ASCII: We have some devices
					// that won't do ASCII. Set the port to Binary regardless of
					// the user's preference.
					detected = Protocol.Binary;
					sb.AppendLine()
				   .Append("Not all devices are ASCII-capable: ")
				   .AppendLine("The port has been configured to Binary.");
				}
				else
				{
					detected = Protocol.ASCII;
					sb.AppendLine()
				   .Append("All devices are ASCII-capable: ")
				   .AppendLine("The port has been configured to ASCII.");
				}
			}
			else
			{
				detected = Protocol.Binary;
				sb.AppendLine().AppendLine("The port has been configured to Binary.");
			}

			_textVM.Text = sb.ToString();

			return detected;
		}


		private void ConfigurePortAndDevicesToAscii()
		{
			if (!_portFacade.Port.IsOpen)
			{
				_portFacade.Port.Open();
			}

			if (!_portFacade.Port.IsAsciiMode)
			{
				_portFacade.Port.Send(0, Command.ConvertToASCII, 115200);
				Thread.Sleep(1000);
				_portFacade.Port.IsAsciiMode = true;
				_portFacade.Port.BaudRate = 115200;
			}
		}


		private void ConfigurePortAndDevicesToBinary()
		{
			if (!_portFacade.Port.IsOpen)
			{
				_portFacade.Port.Open();
			}

			if (_portFacade.Port.IsAsciiMode)
			{
				// No message ID because we don't care about the reply and this
				// is expected to disrupt communications.
				_portFacade.Port.Send("/tools setcomm 9600 1\r\n");
				Thread.Sleep(1000);
				_portFacade.Port.IsAsciiMode = false;
				_portFacade.Port.BaudRate = 9600;
			}
		}


		private void OnFormClosing()
		{
			Cancel();

			// Make sure everything gets cleaned-up.
			_portFacade.SuppressEvents = false;
			StopStatusTimer();

			// Exceptions thrown in Tasks only propagate to the main thread
			// when the Task is finalized. Force finalization to find them.
			GC.Collect();
			GC.WaitForPendingFinalizers();
		}

		#endregion

		#region -- ILoggable implementation --

		/// <summary>
		///     Use Invoke to append a line to the statusTextBox. Callable from any thread.
		/// </summary>
		/// <param name="aMessage">The message to append to the box.</param>
		public void AppendLine(string aMessage)
			=> _dispatcher.Invoke(new Action(() => _logVM.Append(aMessage + Environment.NewLine)));

		#endregion

		#region -- DEV-only stuff --

		[Conditional("DEV")]
		private void CreateStatusTimer()
		{
			_statusTimer = new DispatcherTimer(DispatcherPriority.Background, _dispatcher);
			_statusTimer.Tick += Timer_Tick;
			_statusTimer.Interval = new TimeSpan(0, 0, 3);
			_statusTimer.IsEnabled = true;
		}


		[Conditional("DEV")]
		private void StartStatusTimer() => _statusTimer.Start();


		private void Timer_Tick(object aSender, EventArgs aArgs) => ProgressText = _message2
																			   + Environment.NewLine
																			   + _messages[
																					   _rand.Next(_messages.Length)];


		[Conditional("DEV")]
		private void StopStatusTimer() => _statusTimer.Stop();


		private static readonly string[] _messages =
		{
			"Reticulating splines...", "Detracting unilateral phase...", "Synchronizing cardinal gram-meters...",
			"Pre-famulating amulite...", "Preventing side-fumbling...", "Drawing reciprocation dingle-arm...",
			"Operating novertrunnions...", "Requiring forescent skor-motions...",
			"Bringing perfection to crudely conceived ideas...", "Supplying inverse reactive current...",
			"Surmounting malleable logarithmic casing...", "Fitting hydrocoptic marzel-vanes...",
			"Connecting differential girdle-springs..."
		};

		private readonly Random _rand = new Random();
		private DispatcherTimer _statusTimer;

		#endregion

		#region -- Data members --

		private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Find Devices wizard");

		private static readonly string _message1 =
			"There are two communication protocols that Zaber's devices may use: Zaber ASCII "
		+ "(available on A-Series and X-Series devices) and Zaber Binary (available on all devices), "
		+ "as well as several communication speed options, called baud rates. All devices connected "
		+ "to a port must be set to use the same protocol and baud rate. Ensure all manual movement knobs are at the detent position before starting this process.\n\n"
		+ "This tool lets you detect all connected devices and set them to use the same "
		+ "communication protocol and baud rate. Would you like Zaber Console to automatically configure your devices?";

		private static readonly string _message2 = "Detecting devices. This could take a minute or more.";


		private string _progressText = string.Empty;

		private enum Screen
		{
			Intro,
			Scan,
			Results
		}

		private readonly Dispatcher _dispatcher;
		private readonly ZaberPortFacade _portFacade;
		private IRawPort _rawPort;
		private CancellationTokenSource _tokenSource;

		private Screen _screen = Screen.Intro;
		private bool _workInProgress;
		private string _leftButtonText = "Configure";
		private string _rightButtonText = "Cancel";
		private bool _leftButtonEnabled = true;
		private bool _rightButtonEnabled = true;
		private bool _closeSignal;

		private TextAndRadioButtonsVM _textVM = new TextAndRadioButtonsVM();
		private LogControlVM _logVM = new LogControlVM();

		#endregion
	}
}

﻿using System.Windows;
using ZaberWpfToolbox;

namespace ZaberConsole.Dialogs
{
	public class TextAndRadioButtonsVM : ObservableObject
	{
		#region -- Public Methods & Properties --

		public string Text
		{
			get => _text;
			set => Set(ref _text, value, nameof(Text));
		}


		public Visibility RadioButtonsVisibility
		{
			get => _radioButtonsVisibility;
			set => Set(ref _radioButtonsVisibility, value, nameof(RadioButtonsVisibility));
		}


		public bool AsciiMode
		{
			get => !_binaryMode;

			// TODO: There must be a better way to do radio buttons.
			set
			{
				Set(ref _binaryMode, !value, nameof(AsciiMode));
				OnPropertyChanged(nameof(BinaryMode));
			}
		}


		public bool BinaryMode
		{
			get => _binaryMode;
			set
			{
				Set(ref _binaryMode, value, nameof(BinaryMode));
				OnPropertyChanged(nameof(AsciiMode));
			}
		}

		#endregion

		#region -- Data --

		private string _text = string.Empty;
		private Visibility _radioButtonsVisibility = Visibility.Visible;
		private bool _binaryMode;

		#endregion
	}
}

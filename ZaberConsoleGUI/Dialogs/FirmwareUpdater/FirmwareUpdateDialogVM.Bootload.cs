﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Threading;
using Zaber;
using Zaber.FirmwareDownload;
using ZaberWpfToolbox.Dialogs;

namespace ZaberConsole.Dialogs.FirmwareUpdater
{
	public partial class FirmwareUpdateDialogVM
	{
		#region -- Apply firmware to device --

		private void _worker_Bootload_DoWork(object aSender, DoWorkEventArgs aArgs)
		{
			string filePath = null;

			_dispatcher.Invoke(new Action(() =>
			{
				ApplyControlsEnabled = false;
				DownloadControlsEnabled = false;
				filePath = FirmwareFilePath;
			}));

			var conv = Conversation;
			if (null == conv)
			{
				_worker.ReportProgress(100, "No active conversation.");
				SetFailureState();
				return;
			}

			var device = conv.Device;
			if (null == device)
			{
				_worker.ReportProgress(100, "No device selected. You must select a single device to update.");
				SetFailureState();
				return;
			}

			if ((null == _data) || (_data.Length < 1))
			{
				_worker.ReportProgress(100,
									   "No firmware data loaded. You must download some or load a valid .fwu file.");
				SetFailureState();
				return;
			}

			if (_worker.CancellationPending)
			{
				_worker.ReportProgress(100, USER_CANCELLED);
				return;
			}

			_worker.ReportProgress(-1, $"Attempting update of device {device.DeviceNumber}");

			// Try to decode the firmware file.
			byte[] data = null;
			try
			{
				data = FirmwareFileUnpacker.Unpack(new MemoryStream(_data),
												   _platformId,
												   _serialNo,
												   (aCur, aMax) =>
												   {
													   _dispatcher.Invoke(new Action(() =>
													   {
														   _worker.ReportProgress(
															   (10 * aCur) / aMax);
													   }));
												   });
			}
			catch (Exception aException)
			{
				_worker.ReportProgress(100, $"Error decoding the firmware package file: {aException.Message}");
				return;
			}

			// Save and change comm.next.owner if it exists, to speed things up.
			int? nextOwner = null;
			if (device.DeviceType.FirmwareVersion > 723)
			{
				try
				{
					nextOwner = (int)conv.Request("get comm.next.owner").NumericData;
					conv.Request("set comm.next.owner 1");
				}
				catch
				{
					// If an error occurred while changing the value, don't try to
					// change it again later.
					nextOwner = null;
				}
			}

			// Have firmware in hand; try updating device.
			var upgradeStarted = false;

			var cancelled = false;
			var installed = false;
			string newVersion = null;
			uint newBuild = 0;

			_attemptedUpdate = true;
			_updateInterrupted = true;
			_updateRejected = false;
			_numDataMessagesSent = 0;
			var wereMessageIdsEnabled = conv.Device.AreAsciiMessageIdsEnabled;
			conv.Device.AreAsciiMessageIdsEnabled = false; // Bootloader mode doesn't include IDs in responses.
			var start = DateTime.Now;

			DispatcherOperation lastWait = null;
			var startTime = DateTime.Now;
			var lastDisplayUpdate = DateTime.Now;

			try
			{
				var response = conv.Request("system upgrade start");
				upgradeStarted = true;

				var count = (int) response.NumericData;
				var currentOffset = 0;
				while (count > 0)
				{
					if (_worker.CancellationPending)
					{
						_worker.ReportProgress(100, USER_CANCELLED);
						cancelled = true;
						break;
					}

					var now = DateTime.Now;

					// Limit progress bar update frequency to 4Hz.
					if ((now - lastDisplayUpdate).TotalMilliseconds > 250)
					{
						lastDisplayUpdate = now;
						lastWait = _dispatcher.BeginInvoke(new Action(() =>
						{
							// Calculate time remaining text.
							var proportion = currentOffset / (float) data.Length;
							var progressMessage = string.Empty;
							if (proportion > 0.0)
							{
								var elapsed = (now - startTime).TotalSeconds;
								var remaining = elapsed * ((1.0 - proportion) / proportion);
								var remainingTime = new TimeSpan(0, 0, (int) remaining);
								var minutes = (int) remainingTime.TotalMinutes;
								if (minutes > 0)
								{
									progressMessage += string.Format("{0}m ", minutes);
								}

								progressMessage += string.Format("{0}s remaining", remainingTime.Seconds);
							}

							// Update progress bar and time remaining label.
							_worker.ReportProgress((int) (10 + (90 * proportion)),
												   new Tuple<string, string>(null, progressMessage));
						}));
					}

					var encodedData = BaseNCodec.Encode(data, currentOffset, count, BaseNCodec.BASE_64_URLSAFE);
					currentOffset += count;
					++_numDataMessagesSent;
					response = conv.Request($"system upgrade data {encodedData}");
					count = (int) response.NumericData;
				}

				if (!cancelled)
				{
					conv.Request("system upgrade end");
					_worker.ReportProgress(100, $"Device {device.DeviceNumber} upgraded successfully.");
					_appLog.Info("Firmware update took " + (DateTime.Now - start).TotalMilliseconds + "ms.");

					if (_existingFwVersion.Major < 7) // Can immediately read the new version from the device.
					{
						newVersion = GetStringSetting(conv, "version");
						newBuild = GetIntSetting(conv, "version.build");
					}
					else if (null != _downloadedVersion) // Trust that the FW server gave us the right version number.
					{
						newVersion = _downloadedVersion.FWVersion;
						newBuild = _downloadedVersion.Build;

						var buildSuffix = "." + newBuild;
						if (newVersion.EndsWith(buildSuffix))
						{
							newVersion = newVersion.Substring(0, newVersion.Length - buildSuffix.Length);
						}
					}
					else // User loaded a FWU file from disk; can't tell the version.
					{
						newVersion = "Unknown (loaded from file)";
					}

					installed = true;
					_updateInterrupted = false;
				}
			}
			catch (ErrorResponseException aErrorException)
			{
				_updateRejected = true;
				if ((4 == _numDataMessagesSent) && (6 == _existingFwVersion.Major)) // Trac #1339
				{
					_worker.ReportProgress(100,
										   aErrorException.Message
									   + Environment.NewLine
									   + Environment.NewLine
									   + "Firmware Update Error: Encryption key mismatch. "
									   + "The firmware of the device has not been changed. "
									   + "Please contact Zaber technical support for assistance with changing the firmware version. ");
				}
				else
				{
					_worker.ReportProgress(100,
										   "The device rejected a system update command. This can happen if "
									   + "you try to update over a direct USB connection; use a serial cable or "
									   + "USB-to-serial adaptor instead. This error can also happen if the device or "
									   + "its existing firmware does not support firmware upgrades. "
									   + aErrorException.Message);
				}
			}
			catch (Exception aException)
			{
				_worker.ReportProgress(100,
									   $"An error occurred while upgrading device {device.DeviceNumber}; "
								   + "firmware update may not have been successful. Error message: "
								   + aException.Message);
			}
			finally
			{
				if (upgradeStarted)
				{
					if (cancelled)
					{
						try
						{
							conv.Request("system upgrade end");
						}
						catch (ErrorResponseException) { }

						if (nextOwner.HasValue)
						{
							try
							{
								conv.Request("set comm.next.owner", nextOwner.Value);
							}
							catch (Exception aException)
							{
								_worker.ReportProgress(100,
													   "An error occurred while restoring the original value of the " +
													   "comm.next.owner setting on the selected device. You should check " + 
													   "that the setting is correct. Error message: " + aException.Message);
							}
						}
					}

					OutputMessage("Restarting device");
					conv.Request("system reset");
					WaitForDeviceToRestart();
				}

				conv.Device.AreAsciiMessageIdsEnabled = wereMessageIdsEnabled;
			}

			// Wait for last UI thread invoke to be completed before ending the worker - errors otherwise.
			if (lastWait != null)
			{
				lastWait.Wait(new TimeSpan(0, 0, 1));
			}

			if (installed && !string.IsNullOrEmpty(newVersion))
			{
				ReportInstall(newVersion, newBuild);
			}
		}

		private void WaitForDeviceToRestart()
		{
			var device = Conversation.Device;
			var port = device.Port;
			if (device.SerialNumber == null)
			{
				return;
			}

			var deviceFound = new System.Threading.ManualResetEventSlim();
			EventHandler<DataPacketEventArgs> packetReceiver = (s, e) => {
				if (e.DataPacket.NumericData == device.SerialNumber)
				{
					deviceFound.Set();
				}
			};
			port.DataPacketReceived += packetReceiver;
			try
			{
				var sinceRestart = System.Diagnostics.Stopwatch.StartNew();
				do
				{
					port.Send("/get system.serial");
				} 
				while (!deviceFound.Wait(TimeSpan.FromSeconds(2)) && sinceRestart.Elapsed < TimeSpan.FromMinutes(2));

				if (!deviceFound.IsSet)
				{
					OutputMessage("Could not find the device after the restart");
				}
			} 
			catch (Exception aException)
			{
				OutputMessage($"Could not find the device after the restart because of error: {aException.Message}");
			} 
			finally
			{
				port.DataPacketReceived -= packetReceiver;
			}
		}


		private void _worker_Bootload_RunWorkerCompleted(object aSender, RunWorkerCompletedEventArgs aArgs)
		{
			_worker.DoWork -= _worker_Bootload_DoWork;
			_worker.RunWorkerCompleted -= _worker_Bootload_RunWorkerCompleted;
			Progress = 0;

			if (_attemptedUpdate)
			{
				var mbvm = new MessageBoxParams
				{
					Buttons = MessageBoxButton.OK,
					Icon = MessageBoxImage.Error
				};

				if (_updateRejected)
				{
					mbvm.Message = "The firmware update failed. "
							   + "Please check the messages or application log for more information. "
							   + "If the red light is blinking on your device, it may still be in update "
							   + "mode. You can try sending it a system reset command, or try updating "
							   + "the firmware again.";
					mbvm.Caption = "Firmware Update Failed";
				}
				else if (_updateInterrupted)
				{
					mbvm.Message = "The firmware update was interrupted before it completed. "
							   + "If the red light on your device is blinking then it is still in update mode, "
							   + "with a reduced command set. "
							   + "You can try issuing a system reset command to rectify this, but you may "
							   + "need to update the firmware again instead.";
					mbvm.Caption = "Device state changed";
				}
				else
				{
					// User must close the dialog and rediscover devices after an upgrade attempt.
					SetFailureState();
					InvalidatePort = true;

					mbvm.Message = "Update successful. Device numbers and protocol settings may have changed. "
							   + "The Firmware Updater window will now close and the device list will refresh.";
					mbvm.Caption = "Device state changed";
					mbvm.Icon = MessageBoxImage.Information;
				}

				RequestDialogOpen(this, mbvm);
			}

			if (null != aArgs.Error)
			{
				OutputMessage("An exception occurred while updating a device: " + aArgs.Error);
			}

			if (InvalidatePort)
			{
				RequestClose();
			}
		}


		private void ReportInstall(string aVersion, uint aBuild)
		{
			_eventLog.LogEvent("Installed firmware", $"{aVersion}.{aBuild}");

			var versionList = new List<FirmwareWebQueryBuilder.QueryFirmwareVersion>
			{
				new FirmwareWebQueryBuilder.QueryFirmwareVersion
				{
					PlatformID = _platformId,
					SerialNumber = _serialNo,
					FWVersion = aVersion,
					BuildNumber = aBuild,
					DeviceID = _deviceId,
				}
			};

			var query = FirmwareWebQueryBuilder.MakeInstallationReport(versionList);

			Stream response = null;
			string errorMsg = null;
			var prevCancellationPolicy = _allowDownloadCancellation;
			try
			{
				_allowDownloadCancellation = false;
				response = TryDownload(PORTAL_URL, query, 10000, out errorMsg, out var _);
				_appLog.Info("Firmware install result was successfully reported to server.");
			}
			finally
			{
				_allowDownloadCancellation = prevCancellationPolicy;
			}

			var error = false;

			if (_worker.CancellationPending)
			{
				_worker.ReportProgress(100, "Request cancelled.");
			}
			else if (response is null)
			{
				error = true;
				_worker.ReportProgress(100, errorMsg);
			}
			else
			{
				FirmwareWebQueryBuilder.Response json = null;
				try
				{
					json = FirmwareWebQueryBuilder.DecodeResponse<FirmwareWebQueryBuilder.Response>(response);

					if (json is null)
					{
						error = true;
						_worker.ReportProgress(100, "No JSON returned.");
					}
					else if (!json.Success)
					{
						error = true;
						_worker.ReportProgress(100,
											   $"There was an error communicating with the Zaber website: {json.Message}");
					}
				}
				catch (Exception aException)
				{
					error = true;
					_appLog.WarnFormat("Error decoding response: " + aException.Message);
				}
			}

			if (error)
			{
				_worker.ReportProgress(100,
									   "Unable to log information about your upgrade. "
								   + "This does not affect the success of the upgrade; "
								   + "it just means that Zaber may not have a record of the new version.");
			}
		}

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using log4net;
using MvvmDialogs.ViewModels;
using Zaber;
using Zaber.FirmwareDownload;
using Zaber.Telemetry;
using Zaber.Threading;
using ZaberConsole.Dialogs.Calibration;
using ZaberConsole.Properties;
using ZaberConsole.Web;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Dialogs;

namespace ZaberConsole.Dialogs.FirmwareUpdater
{
	public partial class FirmwareUpdateDialogVM : ObservableObject, IUserDialogViewModel, IDialogClient
	{
		#region -- Initialization --

		public FirmwareUpdateDialogVM(ZaberPortFacade aPort, Conversation aConversation)
		{
			_dispatcher = Dispatcher.CurrentDispatcher;

			PortFacade = aPort;
			Conversation = aConversation;

			_worker = BackgroundWorkerManager.CreateWorker();
			_worker.WorkerReportsProgress = true;
			_worker.WorkerSupportsCancellation = true;
			_worker.ProgressChanged += _worker_ProgressChanged;

			_eventLog.LogEvent("Update Firmware button clicked");
		}


		private ZaberPortFacade PortFacade { get; }


		private Conversation Conversation { get; }

		#endregion

		#region -- View bindings --

		public string DeviceInfo
		{
			get => _deviceInfo;
			set => Set(ref _deviceInfo, value, nameof(DeviceInfo));
		}


		public string Messages
		{
			get => _messages;
			set => Set(ref _messages, value, nameof(Messages));
		}


		/// <summary>
		///     Progression of the progress bar.
		/// </summary>
		public double Progress
		{
			get => _progress;
			set => Set(ref _progress, value, nameof(Progress));
		}


		/// <summary>
		///     Text displayed in the middle of the progress bar.
		/// </summary>
		public string ProgressText
		{
			get => _progressText;
			set => Set(ref _progressText, value, nameof(ProgressText));
		}


		public ObservableCollection<FirmwareVersionVM> AvailableVersions
		{
			get => _availableVersions;
			private set => Set(ref _availableVersions, value, nameof(AvailableVersions));
		}


		public FirmwareVersionVM SelectedVersion
		{
			get => _selectedVersion;
			set
			{
				Set(ref _selectedVersion, value, nameof(SelectedVersion));
				UpdateDownloadButtonEnables();
			}
		}


		public bool DownloadControlsEnabled
		{
			get => _downloadControlsEnabled;
			private set => Set(ref _downloadControlsEnabled, value, nameof(DownloadControlsEnabled));
		}


		public bool DownloadButtonHasFocus
		{
			get => _downloadButtonHasFocus;
			set => Set(ref _downloadButtonHasFocus, value, nameof(DownloadButtonHasFocus));
		}


		public bool VersionSelected
		{
			get => _versionSelected;
			private set => Set(ref _versionSelected, value, nameof(VersionSelected));
		}


		public bool ApplyControlsEnabled
		{
			get => _applyControlsEnabled;
			private set => Set(ref _applyControlsEnabled, value, nameof(ApplyControlsEnabled));
		}


		public bool CancelButtonEnabled
		{
			get => _cancelButtonEnabled;
			private set => Set(ref _cancelButtonEnabled, value, nameof(CancelButtonEnabled));
		}


		public bool CancelButtonHasFocus
		{
			get => _cancelButtonHasFocus;
			set => Set(ref _cancelButtonHasFocus, value, nameof(CancelButtonHasFocus));
		}


		public bool SaveButtonEnabled
		{
			get => _saveButtonEnabled;
			private set => Set(ref _saveButtonEnabled, value, nameof(SaveButtonEnabled));
		}


		public bool StartButtonHasFocus
		{
			get => _startButtonHasFocus;
			set => Set(ref _startButtonHasFocus, value, nameof(StartButtonHasFocus));
		}


		public bool AutoStartUpdate
		{
			get => _autoStartUpdate;
			set => Set(ref _autoStartUpdate, value, nameof(AutoStartUpdate));
		}


		public string FirmwareFilePath
		{
			get => _firmwareFilePath;
			set => Set(ref _firmwareFilePath, value, nameof(FirmwareFilePath));
		}


		public string CancelButtonLabel
		{
			get => _cancelButtonLabel;
			private set => Set(ref _cancelButtonLabel, value, nameof(CancelButtonLabel));
		}


		public string CancelButtonToolTip
		{
			get => _cancelButtonToolTip;
			private set => Set(ref _cancelButtonToolTip, value, nameof(CancelButtonToolTip));
		}


		public int SelectedTabIndex
		{
			get => _selectedTabIndex;
			set => Set(ref _selectedTabIndex, value, nameof(SelectedTabIndex));
		}


		public string KeyNotice
		{
			get => _keyNotice;
			private set => Set(ref _keyNotice, value, nameof(KeyNotice));
		}


		public ICommand WindowLoadedCommand => new RelayCommand(_ => OnWindowLoaded());


		public ICommand ViewReleaseNotesCommand => new RelayCommand(_ => OnViewReleasenotesPressed());


		public ICommand DownloadCommand => new RelayCommand(_ => OnDownloadPressed());


		public ICommand SaveFileCommand => new RelayCommand(_ => OnSaveFilePressed());


		public ICommand LoadFileCommand => new RelayCommand(_ => OnLoadFilePressed());


		public ICommand StartUpdateCommand => new RelayCommand(_ => OnStartUpdatePressed());


		public ICommand CancelButtonCommand => new RelayCommand(_ => RequestClose());


		public ICommand WindowClosingCommand => new RelayCommand(_ => RequestClose());


		public bool InvalidatePort { get; private set; }

		#endregion

		#region -- Functionality --

		private void OnWindowLoaded()
		{
			PortFacade.Opened += PortFacade_Opened;
			PortFacade.Closed += PortFacade_Closed;

			var folder = Settings.Default.FirmwareDownloadFolder;
			if (string.IsNullOrEmpty(folder) || !Directory.Exists(folder))
			{
				folder = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
				if (Directory.Exists(Path.Combine(folder, "Downloads")))
				{
					folder = Path.Combine(folder, "Downloads");
				}

				if (!Directory.Exists(folder))
				{
					OutputMessage("Could not find the firmware download folder. Please select a new one.");
					DownloadControlsEnabled = false;
					UpdateDownloadButtonEnables();
				}
				else
				{
					Settings.Default.FirmwareDownloadFolder = folder;
				}
			}

			// Kick off the initialization worker thread.
			_worker.DoWork += _worker_GatherData_DoWork;
			_worker.RunWorkerCompleted += _worker_GatherData_RunWorkerCompleted;
			_worker.Run();
		}


		private void PortFacade_Opened(object aSender, EventArgs aArgs)
		{
			ApplyControlsEnabled = File.Exists(FirmwareFilePath);
			CancelButtonEnabled = false;
		}


		private void PortFacade_Closed(object aSender, EventArgs aArgs)
		{
			CancelWorker();
			SetFailureState();
		}


		private void OnViewReleasenotesPressed()
		{
			var version = SelectedVersion?.Version;
			if (version != null)
			{
				var notes = version.Notes;
				string url = null;
				if (string.IsNullOrEmpty(notes))
				{
					notes = "No release notes are available for this version.";
				}
				else
				{
					// If the first word of the release notes is a URL, launch it instead of displaying the notes.
					var start = 0;
					while ((start < notes.Length) && char.IsWhiteSpace(notes[start]))
					{
						++start;
					}

					var len = 0;
					while (((start + len) < notes.Length) && !char.IsWhiteSpace(notes[start + len]))
					{
						++len;
					}

					var firstWord = notes.Substring(start, len);
					if (Uri.TryCreate(firstWord, UriKind.Absolute, out var temp))
					{
						if ((Uri.UriSchemeHttp == temp.Scheme) || (Uri.UriSchemeHttps == temp.Scheme))
						{
							url = firstWord;
						}
					}
				}

				if (string.IsNullOrEmpty(url))
				{
					var box = new CustomMessageBoxVM(notes, "Version Info", null, "OK");
					RequestDialogOpen?.Invoke(this, box);
				}
				else
				{
					Process.Start(url);
				}
			}
		}


		private void OnDownloadPressed()
		{
			_worker.DoWork += _worker_Download_DoWork;
			_worker.RunWorkerCompleted += _worker_Download_RunWorkerCompleted;
			_worker.Run();
		}


		private void OnSaveFilePressed()
		{
			var version = SelectedVersion?.Version;
			var fileName = $"{_deviceName}_{version.FWVersion}-{version.Build}.fwu";
			if (IsValidSerialNumber(_serialNo))
			{
				fileName = $"{_deviceName}_{version.FWVersion}-{version.Build}_sn_{_serialNo}.fwu";
			}

			// Prompt the user to select a filename, starting with a generated suggestion.
			var dlg = new FileBrowserDialogParams
			{
				DefaultExtension = ".fwu",
				Filename = fileName,
				Filter = "Firmware Update files (*.fwu)|*.fwu|All files (*.*)|*.*",
				InitialDirectory = Settings.Default.FirmwareDownloadFolder,
				Title = "Save Firmware Update File",
				CheckFileExists = false,
				Action = FileBrowserDialogParams.FileActionType.Save
			};

			RequestDialogOpen?.Invoke(this, dlg);

			if (dlg.DialogResult.HasValue && dlg.DialogResult.Value)
			{
				Settings.Default.FirmwareDownloadFolder = Path.GetDirectoryName(dlg.Filename);

				if (File.Exists(dlg.Filename))
				{
					File.Delete(dlg.Filename);
				}

				File.WriteAllBytes(dlg.Filename, _data);
				_dataIsSaved = true;

				OutputMessage($"Firmware data saved to {dlg.Filename}.");
				FirmwareFilePath = dlg.Filename;
				Settings.Default.LastFirmwareFilePath = FirmwareFilePath;

				_eventLog.LogEvent("Saved firmware to file");
			}
		}


		private void OnLoadFilePressed()
		{
			if (!_dataIsSaved)
			{
				var mbvm = new MessageBoxParams
				{
					Message = "Loading a file will discard the downloaded data in memory. Continue?",
					Caption = "Discard downloaded data?",
					Icon = MessageBoxImage.Exclamation
				};

				RequestDialogOpen?.Invoke(this, mbvm);

				if (MessageBoxResult.OK != mbvm.Result)
				{
					return;
				}
			}

			var dlg = new FileBrowserDialogParams
			{
				CheckFileExists = true,
				DefaultExtension = "fwu",
				Filter = "Firmware Update Files (*.fwu)|*.fwu|All files (*.*)|*.*"
			};

			if ((STR_USING_DOWNLOAD == FirmwareFilePath) || string.IsNullOrEmpty(FirmwareFilePath))
			{
				var path = Settings.Default.LastFirmwareFilePath;
				if (!string.IsNullOrEmpty(path) && File.Exists(path))
				{
					dlg.Filename = path;
				}
			}
			else
			{
				dlg.Filename = FirmwareFilePath;
			}

			RequestDialogOpen?.Invoke(this, dlg);

			if (dlg.DialogResult.HasValue && dlg.DialogResult.Value)
			{
				FirmwareFilePath = dlg.Filename;
				Settings.Default.LastFirmwareFilePath = FirmwareFilePath;
				_downloadedVersion = null;
				_data = File.ReadAllBytes(FirmwareFilePath);
				_dataIsSaved = true;
				SaveButtonEnabled = false;
				ValidateFirmware();
				_eventLog.LogEvent("Loaded firmware from file");
			}
		}


		private void OnStartUpdatePressed()
		{
			_worker.DoWork += _worker_Bootload_DoWork;
			_worker.RunWorkerCompleted += _worker_Bootload_RunWorkerCompleted;
			_worker.Run();
		}


		private void CancelWorker()
		{
			if (_worker.IsBusy)
			{
				_worker.CancelAsync();
			}

			Progress = 0.0;
			ProgressText = string.Empty;
		}


		private void _worker_ProgressChanged(object aSender, ProgressChangedEventArgs aArgs)
		{
			if (aArgs.ProgressPercentage >= 0)
			{
				Progress = aArgs.ProgressPercentage;
			}

			string newProgressText = null;
			if (aArgs.UserState is string message)
			{
				if (!string.IsNullOrEmpty(message))
				{
					OutputMessage(message);
				}
			}
			else if (aArgs.UserState is Tuple<string, string> pair)
			{
				if (!string.IsNullOrEmpty(pair.Item1))
				{
					OutputMessage(pair.Item1);
				}

				newProgressText = pair.Item2;
			}

			ProgressText = newProgressText;
		}


		private void OutputMessage(string aMessage) => _dispatcher.Invoke(new Action(() =>
		{
			_appLog.Info(aMessage);
			Messages += aMessage + Environment.NewLine;
		}));


		private void SetDownloadFailureState() => _dispatcher.Invoke(new Action(() =>
		{
			DownloadControlsEnabled = false;
		}));


		private void SetFailureState()
		{
			SetDownloadFailureState();
			_dispatcher.Invoke(new Action(() =>
			{
				ApplyControlsEnabled = false;
				VersionSelected = false;
				CancelButtonEnabled = true;
				CancelButtonLabel = "Close";
				CancelButtonHasFocus = true;
				CancelButtonToolTip = "Click to close this dialog once you are finished reading messages.";
				Progress = 0.0;
				ProgressText = string.Empty;
			}));

			_failed = true;
		}


		private static int CalculatePercentage(int aMajorTicks, int aMajorTickCount, int aMinorTicks,
											   int aMinorTickCount)
		{
			var percentage = 100 * aMajorTicks;
			percentage += (aMinorTicks * 100) / aMinorTickCount;
			percentage /= aMajorTickCount;
			return percentage;
		}


		private void UpdateDownloadButtonEnables() => VersionSelected =
			(null != SelectedVersion) && !string.IsNullOrEmpty(SelectedVersion.DisplayName);


		private bool ValidateFirmware()
		{
			var result = false;

			if (null != _data)
			{
				try
				{
					var data = FirmwareFileUnpacker.Unpack(new MemoryStream(_data), _platformId, _serialNo);
					ApplyControlsEnabled = (null != data) && (data.Length > 0);
					if (ApplyControlsEnabled)
					{
						OutputMessage("Firmware data validated. This firmware is applicable to the current device.");
						result = true;
					}
					else
					{
						OutputMessage("Currently loaded data is not applicable to the selected device.");
					}
				}
				catch (Exception)
				{
					ApplyControlsEnabled = false;
					OutputMessage("Currently loaded data is not a valid firmware update package.");
				}
			}
			else
			{
				OutputMessage("No firmware data loaded.");
				ApplyControlsEnabled = false;
				SaveButtonEnabled = false;
			}

			return result;
		}


		private Stream TryDownload(string aUrl, string aQuery, int aTimeout, out string aErrorMessage, out string aContentType)
		{
			aErrorMessage = string.Empty;
			aContentType = string.Empty;

			var start = DateTime.Now;
			var request = new AsyncWebRequest(aUrl, aTimeout)
			{
				UseProxy = !Settings.Default.DisableWebProxy,
				PostData = aQuery,
			};

			var assemblyName = Assembly.GetExecutingAssembly().GetName();
			request.UserAgent = string.Format("{0}/{1}", assemblyName.Name, assemblyName.Version);

			request.Begin();
			while (!request.IsFinished)
			{
				if (_worker.CancellationPending && _allowDownloadCancellation)
				{
					request.Abort();
					aErrorMessage = "Request cancelled.";
					return null;
				}

				if (_worker.IsBusy)
				{
					_dispatcher.Invoke(new Action(() =>
					{
						_worker.ReportProgress(
							(100 * (DateTime.Now - start).Milliseconds) / aTimeout);
					}));
				}

				Thread.Sleep(100);
			}

			if (request.Error)
			{
				aErrorMessage = request.ErrorMessage;
				return null;
			}

			aContentType = request.ContentType;

			return request.GetResponse();
		}


		private static bool IsValidSerialNumber(uint aNum) => (0 != aNum) && (0xFFFFFFFFU != aNum);

		#endregion

		#region -- IDialogClient implementation --

		#pragma warning disable CS0067 // Event is never used.
		public event RequestDialogChange RequestDialogClose;
		#pragma warning restore CS0067
		public event RequestDialogChange RequestDialogOpen;

		#endregion

		#region -- IUserDialogViewModel implementation --

		public bool IsModal => true;

		public event EventHandler DialogClosing;


		public void RequestClose()
		{
			CancelWorker();

			PortFacade.Opened -= PortFacade_Opened;
			PortFacade.Closed -= PortFacade_Closed;

			DialogClosing?.Invoke(this, null);
		}


		public event EventHandler BringToFront;


		public void RequestBringToFront() => BringToFront?.Invoke(this, null);


		/// <summary>
		///     Any exception that was unhandled by the dialog itself.
		/// </summary>
		public Exception Exception { get; set; }

		#endregion

		#region -- Data --

		public class FirmwareVersionVM
		{
			#region -- Public Methods & Properties --

			public FirmwareWebQueryBuilder.ResponseFWVersion Version { get; set; }

			public string DisplayName => Version.ToString();

			#endregion
		}

		private static readonly ILog _appLog = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Firmware updater");
		private static readonly string USER_CANCELLED = "Update operation cancelled by user.";
		private static readonly string STR_USING_DOWNLOAD = "(Using downloaded data)";

		#if DEV
		// Internal firmware downloads.
		private const string PORTAL_URL = "https://fwee-firmware.izaber.com:7087/";
		#else
		// Public firmware acccess point.
		private const string PORTAL_URL = "https://www.zaber.com/support/firmware-self-serve.php";
		#endif

		private readonly IBackgroundWorker _worker;
		private bool _allowDownloadCancellation = true;
		private readonly Dispatcher _dispatcher;

		private string _deviceInfo;
		private string _messages;
		private double _progress;
		private string _progressText = string.Empty;

		private ObservableCollection<FirmwareVersionVM> _availableVersions =
			new ObservableCollection<FirmwareVersionVM>();

		private FirmwareVersionVM _selectedVersion;
		private FirmwareWebQueryBuilder.ResponseFWVersion _downloadedVersion;
		private bool _autoStartUpdate = true;
		private string _firmwareFilePath;
		private bool _downloadControlsEnabled;
		private bool _downloadButtonHasFocus;
		private bool _versionSelected;
		private bool _applyControlsEnabled;
		private bool _cancelButtonEnabled = true;
		private bool _cancelButtonHasFocus;
		private bool _saveButtonEnabled;
		private bool _startButtonHasFocus;
		private string _cancelButtonLabel = "Cancel";
		private string _cancelButtonToolTip = "Abort download/update and close this dialog.";
		private int _selectedTabIndex;

		private string _keyNotice =
			"Firmware Update files are keyed to your device's serial number. If you want to update multiple devices to the same firmware version, you will need to download one file for each device.";

		private uint _platformId;
		private uint _serialNo;
		private bool _keyed;
		private byte _deviceNumber;
		private uint _deviceId;
		private FirmwareVersion _existingFwVersion;
		private readonly List<CalibrationTableVM> _calibrationTables = new List<CalibrationTableVM>();
		private string _deviceName = "";
		private bool _attemptedUpdate;
		private bool _updateInterrupted;
		private bool _updateRejected;
		private int _numDataMessagesSent;
		private bool _failed;
		private byte[] _data;
		private bool _dataIsSaved = true;
		private bool _isJoystick;

		#endregion
	}
}

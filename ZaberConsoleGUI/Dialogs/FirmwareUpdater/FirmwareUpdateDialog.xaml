<Window x:Class="ZaberConsole.Dialogs.FirmwareUpdater.FirmwareUpdateDialog"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:i="http://schemas.microsoft.com/xaml/behaviors"
        xmlns:prop="clr-namespace:ZaberConsole.Properties"
        xmlns:ztbb="clr-namespace:ZaberWpfToolbox.Behaviors;assembly=ZaberWpfToolbox"
        xmlns:zaberTheme="clr-namespace:ZaberWpfToolbox.Themes;assembly=ZaberWpfToolbox"
        mc:Ignorable="d"
        Title="Firmware Updater"
        Height="600"
        Width="600">

	<i:Interaction.Triggers>
		<i:EventTrigger EventName="Closing">
			<i:InvokeCommandAction Command="{Binding WindowClosingCommand}" />
		</i:EventTrigger>
		<i:EventTrigger EventName="Loaded">
			<i:InvokeCommandAction Command="{Binding WindowLoadedCommand}" />
		</i:EventTrigger>
	</i:Interaction.Triggers>

	<Grid>
		<Grid.RowDefinitions>
			<RowDefinition Height="Auto" />
			<RowDefinition Height="Auto" />
			<RowDefinition Height="Auto" />
			<RowDefinition Height="Auto" />
			<RowDefinition Height="Auto" />
			<RowDefinition Height="*" />
			<RowDefinition Height="Auto" />
			<RowDefinition Height="Auto" />
		</Grid.RowDefinitions>

		<Border BorderThickness="0"
		        Background="{DynamicResource {x:Static zaberTheme:ResourceKeys.ControlDataAreaBackgroundBrushKey}}">
			<StackPanel Orientation="Vertical"
			            Margin="3">
				<TextBlock TextWrapping="Wrap"
				           Margin="3"
				           Text="Use this window to download and apply firmware updates to your currently selected device." />

				<TextBlock TextWrapping="Wrap"
				           Margin="3"
				           Text="PLEASE NOTE that this operation may reset the device's settings to their default values. This operation may also change the device's current protocol and device number, possibly requiring use of the 'Find Devices' button in Zaber Console to rediscover it." />

				<TextBlock TextWrapping="Wrap"
				           Margin="3"
				           Text="{x:Static prop:Resources.STR_FWU_DATA_COLLECTION_NOTICE}" />
			</StackPanel>
		</Border>

		<Label Grid.Row="1"
		       Content="Device being updated:" />

		<TextBox Grid.Row="2"
		         IsReadOnly="True"
		         TextWrapping="Wrap"
		         Padding="3"
		         Margin="0,0,0,6"
		         ToolTip="This is the device you will be downloading and/or applying firmware updates for."
		         Text="{Binding DeviceInfo}" />

		<TabControl Grid.Row="3"
		            SelectedIndex="{Binding SelectedTabIndex}">
			<TabItem Header="Download New Firmware">
				<Grid>
					<Grid.RowDefinitions>
						<RowDefinition />
						<RowDefinition />
						<RowDefinition />
						<RowDefinition />
					</Grid.RowDefinitions>

					<Grid.ColumnDefinitions>
						<ColumnDefinition />
						<ColumnDefinition />
						<ColumnDefinition />
					</Grid.ColumnDefinitions>

					<Label Grid.ColumnSpan="3"
					       Content="Available firmware versions for the selected device:" />

					<ComboBox Grid.Row="1"
					          Grid.ColumnSpan="2"
					          IsEnabled="{Binding DownloadControlsEnabled}"
					          HorizontalAlignment="Stretch"
					          ToolTip="Select a firmware version to download."
					          ItemsSource="{Binding AvailableVersions}"
					          SelectedItem="{Binding SelectedVersion, Mode=TwoWay}">

						<ComboBox.ItemTemplate>
							<DataTemplate>
								<TextBlock Text="{Binding DisplayName}" />
							</DataTemplate>
						</ComboBox.ItemTemplate>

					</ComboBox>

					<Button Grid.Row="1"
					        Grid.Column="2"
					        HorizontalAlignment="Left"
					        Content="View Release Notes"
					        ToolTip="This will display the release notes for the currently selected firmware version in a new window."
					        IsEnabled="{Binding VersionSelected}"
					        Command="{Binding ViewReleaseNotesCommand}" />

					<Button Grid.Row="2"
					        Grid.Column="1"
					        Content="Download"
					        ToolTip="Click to download the currently selected firmware version."
					        IsEnabled="{Binding DownloadControlsEnabled}"
					        Command="{Binding DownloadCommand}"
					        Style="{DynamicResource BigButton}"
					        ztbb:SetFocusBehavior.HasFocus="{Binding DownloadButtonHasFocus, Mode=TwoWay}" />

					<CheckBox Grid.Row="2"
					          Grid.Column="2"
					          VerticalAlignment="Center"
					          Content="Auto-Start Update"
					          ToolTip="Checking this will cause device firmware update to start automatically after a successful firmware download."
					          IsEnabled="{Binding VersionSelected}"
					          IsChecked="{Binding AutoStartUpdate}" />

					<TextBlock TextWrapping="Wrap"
					           Grid.Row="3"
					           Grid.ColumnSpan="3"
					           Text="{Binding KeyNotice}" />

				</Grid>

			</TabItem>

			<TabItem Header="Apply Firmware to Device">
				<Grid>
					<Grid.RowDefinitions>
						<RowDefinition />
						<RowDefinition />
						<RowDefinition />
						<RowDefinition />
						<RowDefinition Height="*" />
					</Grid.RowDefinitions>

					<Grid.ColumnDefinitions>
						<ColumnDefinition />
						<ColumnDefinition />
						<ColumnDefinition />
						<ColumnDefinition />
						<ColumnDefinition />
					</Grid.ColumnDefinitions>

					<Label Grid.ColumnSpan="2"
					       Content="Firmware Update File to use:" />

					<TextBox Grid.Row="1"
					         Grid.ColumnSpan="3"
					         IsReadOnly="True"
					         ToolTip="Location of the firmware update to apply to your device."
					         IsEnabled="{Binding ApplyControlsEnabled}"
					         Text="{Binding FirmwareFilePath}" />

					<Button Grid.Row="1"
					        Grid.Column="3"
					        Content="Save File"
					        ToolTip="Save the current firmware data to a file so you can use it later without having to download it again. This is optional."
					        IsEnabled="{Binding SaveButtonEnabled}"
					        Command="{Binding SaveFileCommand}" />

					<Button Grid.Row="1"
					        Grid.Column="4"
					        Content="Load File"
					        ToolTip="Load firmware data from a previously saved file. Optional."
					        Command="{Binding LoadFileCommand}" />

					<Button Grid.Row="2"
					        Grid.Column="1"
					        Grid.ColumnSpan="3"
					        HorizontalAlignment="Center"
					        Content="Start Update"
					        ToolTip="Click to begin updating your device. PLEASE READ THE NOTE AT THE TOP OF THIS WINDOW FIRST."
					        IsEnabled="{Binding ApplyControlsEnabled}"
					        Command="{Binding StartUpdateCommand}"
					        Style="{DynamicResource BigButton}"
					        ztbb:SetFocusBehavior.HasFocus="{Binding StartButtonHasFocus, Mode=TwoWay}" />

				</Grid>
			</TabItem>
		</TabControl>

		<Label Grid.Row="4"
		       Content="Messages:" />

		<ScrollViewer Grid.Row="5">
			<i:Interaction.Behaviors>
				<ztbb:AutoScrollBehavior />
			</i:Interaction.Behaviors>
			<TextBlock TextWrapping="Wrap"
			           Padding="3"
			           Background="{DynamicResource {x:Static zaberTheme:ResourceKeys.ControlDataAreaBackgroundBrushKey}}"
			           Text="{Binding Messages}" />
		</ScrollViewer>

		<ProgressBar Grid.Row="6"
		             Minimum="0"
		             Maximum="100"
		             Value="{Binding Progress}" />

		<TextBlock Grid.Row="6"
		           HorizontalAlignment="Center"
		           VerticalAlignment="Center"
		           Panel.ZIndex="1"
		           Foreground="{DynamicResource {x:Static zaberTheme:ResourceKeys.ButtonHighlightedForegroundBrushKey}}"
		           Text="{Binding ProgressText}" />

		<Button Grid.Row="7"
		        HorizontalAlignment="Right"
		        ToolTip="{Binding CancelButtonToolTip}"
		        Content="{Binding CancelButtonLabel}"
		        IsEnabled="{Binding CancelButtonEnabled}"
		        Command="{Binding CancelButtonCommand}"
		        ztbb:SetFocusBehavior.HasFocus="{Binding CancelButtonHasFocus, Mode=TwoWay}" />

	</Grid>
</Window>
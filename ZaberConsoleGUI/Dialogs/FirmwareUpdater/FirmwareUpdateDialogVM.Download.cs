﻿using System;
using System.ComponentModel;
using Zaber;
using Zaber.FirmwareDownload;

namespace ZaberConsole.Dialogs.FirmwareUpdater
{
	public partial class FirmwareUpdateDialogVM
	{
		#region -- Firmware update file download --

		private void _worker_Download_DoWork(object aSender, DoWorkEventArgs aArgs)
		{
			FirmwareWebQueryBuilder.ResponseFWVersion version = null;

			// Can't access UI controls directly from a worker thread.
			_dispatcher.Invoke(new Action(() =>
			{
				DownloadControlsEnabled = false;
				version = SelectedVersion?.Version;
			}));

			if (null == version)
			{
				return;
			}

			_worker.ReportProgress(-1, "Downloading firmware update file...");
			var requestedVersion = new FirmwareWebQueryBuilder.QueryFirmwareVersion
			{
				PlatformID = _platformId,
				SerialNumber = _serialNo,
				IsKeyed = _keyed,
				FWVersion = version.FWVersion,
				BuildNumber = version.Build,
				DeviceID = _deviceId,
			};

			var query = FirmwareWebQueryBuilder.MakePackageRequest(new[] { requestedVersion });

			if (_worker.CancellationPending)
			{
				_worker.ReportProgress(100, USER_CANCELLED);
				SetFailureState();
				return;
			}

			var response = TryDownload(PORTAL_URL, query, 20000, out var errorMsg, out var contentType);

			if (null == response)
			{
				_worker.ReportProgress(100, errorMsg);
			}
			else if ("application/octet-stream" == contentType)
			{
				_downloadedVersion = null;
				_data = new byte[response.Length];
				response.ReadBlocking(_data, 0, (int) response.Length);
				_dataIsSaved = false;

				_dispatcher.Invoke(new Action(() =>
				{
					SaveButtonEnabled = true;
					FirmwareFilePath = STR_USING_DOWNLOAD;

					if (ValidateFirmware())
					{
						_downloadedVersion = version;
						SelectedTabIndex = 1;
						StartButtonHasFocus = true;
						_eventLog.LogEvent("Downloaded firmware", $"{version.FWVersion}.{version.Build}");
					}
					else if (AutoStartUpdate)
					{
						AutoStartUpdate = false;
						OutputMessage(
							"Downloaded firmware is not applicable to this device; disabling automatic transfer.");
					}

					_worker.ReportProgress(100);
				}));
			}
			else
			{
				FirmwareWebQueryBuilder.Response json = null;
				try
				{
					json = FirmwareWebQueryBuilder.DecodeResponse<FirmwareWebQueryBuilder.Response>(response);

					if (null == json)
					{
						_worker.ReportProgress(100,
											   "There was an error communicating with the Zaber firmware server. Downloads are not currently available.");
					}
					else if (!json.Success)
					{
						_worker.ReportProgress(100,
											   $"There was an error communicating with the Zaber firmware server: {json.Message}");
					}
					else
					{
						_worker.ReportProgress(100, json.Message);
					}
				}
				catch (Exception aException)
				{
					_worker.ReportProgress(100,
										   $"There was an error communicating with the Zaber firmware server: {aException.Message}");
				}
			}
		}


		private void _worker_Download_RunWorkerCompleted(object aSender, RunWorkerCompletedEventArgs aArgs)
		{
			_worker.DoWork -= _worker_Download_DoWork;
			_worker.RunWorkerCompleted -= _worker_Download_RunWorkerCompleted;
			Progress = 0.0;
			DownloadControlsEnabled = true;

			if (null != aArgs.Error)
			{
				OutputMessage("An exception occurred while downloading: " + aArgs.Error);
			}
			else if (AutoStartUpdate)
			{
				OnStartUpdatePressed();
			}
		}

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using Zaber;
using Zaber.FirmwareDownload;
using ZaberConsole.Dialogs.Calibration;
using ZaberConsole.Properties;
using ZaberWpfToolbox.Dialogs;

namespace ZaberConsole.Dialogs.FirmwareUpdater
{
	public partial class FirmwareUpdateDialogVM
	{
		#region ---- Background thread for gathering initial data ----

		private void _worker_GatherData_DoWork(object aSender, DoWorkEventArgs aArgs)
		{
			// Run an ordered list of steps. This would be a single huge function otherwise.
			var steps = new List<Func<bool>>
			{
				GatherData_CheckDevice,
				GatherData_GetPlatformId,
				GatherData_GetSerialNumber,
				GatherData_CheckKeyed,
				GatherData_SaveCalibrationTables,
				GatherData_ReportDeviceInfo,
				GatherData_CheckServerInfo,
				GatherData_GetFirmwareVersions
			};

			var numSteps = steps.Count;
			for (var i = 0; i < numSteps; ++i)
			{
				var step = steps[i];

				if (!step())
				{
					return;
				}

				if (_worker.CancellationPending)
				{
					_worker.ReportProgress(100, USER_CANCELLED);
					SetFailureState();
					return;
				}

				_dispatcher.Invoke(new Action(() => { _worker.ReportProgress((100 * i) / numSteps); }));
			}

			_dispatcher.Invoke(new Action(() => { DownloadButtonHasFocus = true; }));
		}


		private void _worker_GatherData_RunWorkerCompleted(object aSender, RunWorkerCompletedEventArgs aArgs)
		{
			_worker.DoWork -= _worker_GatherData_DoWork;
			_worker.RunWorkerCompleted -= _worker_GatherData_RunWorkerCompleted;
			Progress = 0.0;

			// If there are calibration tables that will be destroyed, 
			// inform the user they should back them up.
			if ((_calibrationTables.Count > 0) && ShouldBackupCalibrationTables(Conversation.Device))
			{
				var mbvm = new CustomMessageBoxVM(
					"This device contains calibration data. Updating the firmware will clear the "
				+ "calibration data from the device; You should back up the calibration data to "
				+ "disk and then use the calibration data tool (accessible from a button on the "
				+ "Options tab of Zaber Console) to restore it to the device once the firmware "
				+ "has been updated."
				+ Environment.NewLine
				+ Environment.NewLine
				+ "Do you want to save the calibration data now?",
					"Back Up Calibration Data") { DialogResult = 0 };

				mbvm.AddOption("Save data now", 1);
				mbvm.AddOption("I have already saved this data", 2);
				mbvm.AddOption("Cancel firmware update", 0);

				RequestDialogOpen?.Invoke(this, mbvm);

				switch ((int) mbvm.DialogResult)
				{
					case 2:
						OutputMessage("User authorized discarding calibration on device.");
						break;

					case 1:
					{
						var path = Settings.Default.LastCalibrationDataFolder;
						if (string.IsNullOrEmpty(path) || !Directory.Exists(path))
						{
							path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
						}

						try
						{
							var outputPath = CalibrationTableSerializer.PromptSaveToCsv(path,
																						_calibrationTables,
																						dlg =>
																						{
																							RequestDialogOpen?.Invoke(
																								this,
																								dlg);
																						});

							if (null == outputPath)
							{
								_failed = true;
								OutputMessage("Firmware update cancelled because user did not save calibration data.");
								RequestClose();
								return;
							}

							OutputMessage($"The calibration data from this device has been backed up in {outputPath}.");
						}
						catch (Exception aException)
						{
							OutputMessage($"There was an error saving the data to a CSV file: {aException.Message}");
							_failed = true;
						}
					}
						break;

					default:
						OutputMessage("User cancelled firmware update due to presence of calibration data.");
						_failed = true;
						RequestClose();
						return;
				}
			}


			// If bootloading a joystick, inform the user they should back up its config and recalibrate it.
			if (_isJoystick)
			{
				var mbvm = new CustomMessageBoxVM(
					"Firmware upgrade will reset all key commands and joystick settings to their "
				+ "default values, and will clear joystick axis calibration data. Use the joystick plugin to save this data before continuing, "
				+ "then again to reload the data after the firmware update is complete."
				+ Environment.NewLine
				+ Environment.NewLine
				+ "After updating firmware, use the joystick calibration wizard in the joystick "
				+ "plugin to recalibrate the device.",
					"Joystick Detected") { DialogResult = 1 };

				mbvm.AddOption("Proceed with firmware update", 1);
				mbvm.AddOption("Cancel firmware update", 0);

				RequestDialogOpen?.Invoke(this, mbvm);

				switch ((int) mbvm.DialogResult)
				{
					case 1:
						OutputMessage("User authorized discarding joystick settings.");
						break;

					default:
						OutputMessage("User cancelled joystick firmware update.");
						_failed = true;
						RequestClose();
						return;
				}
			}

			// A filename may have persisted in the settings, but we need the device info to validate it.
			// At this point we have the device info, so enable the start button if the previous file will work.
			if (!_failed)
			{
				ValidateFirmware();
			}

			if (aArgs.Error != null)
			{
				OutputMessage("An exception occurred while getting version info: " + aArgs.Error);
			}
		}


		#region ------ Data gathering helpers ------

		private bool GatherData_CheckDevice()
		{
			_worker.ReportProgress(-1, "Gathering device info...");

			var device = Conversation?.Device;
			if (null == device)
			{
				_worker.ReportProgress(100, "No device selected. You must select a single device to update.");
				SetFailureState();
				return false;
			}

			if (device is DeviceCollection)
			{
				_worker.ReportProgress(100,
									   "'All Devices' is selected. You must select an individual device to update.");
				SetFailureState();
				return false;
			}

			if (device.IsInBootloaderMode)
			{
				_worker.ReportProgress(-1,
									   "NOTE: This device is in bootloader mode, meaning the last firmware update "
								   + "attempt failed or was cancelled partway through. If the system reset "
								   + "command failed to recover it, you can try re-applying the firmware.");
			}
			else if (device.FirmwareVersion < new FirmwareVersion(6, 18))
			{
				_worker.ReportProgress(100,
									   $"The selected device has firmware version {device.FirmwareVersion} "
								   + "which does not support in-place updates. Please contact Zaber Support for other options.");
				SetFailureState();
				return false;
			}

			_deviceNumber = device.DeviceNumber;
			_deviceId = (uint)device.DeviceType.DeviceId;
			_existingFwVersion = device.FirmwareVersion;

			return true;
		}


		private bool GatherData_GetPlatformId()
		{
			var device = Conversation.Device;

			// Get the platform ID of the device.
			_worker.ReportProgress(-1, "Checking device platform ID...");
			_platformId = GetIntSetting(Conversation, "system.platform");

			// Hack - this device type doesn't have a bootloader.
			if ((new FirmwareVersion(6, 18) == device.FirmwareVersion) && (268500992 == _platformId))
			{
				_worker.ReportProgress(100,
									   "Sorry, but this device type does not support in-place firmware "
								   + "updates. Please contact Zaber Support for other options.");
				SetFailureState();
				return false;
			}

			if (0 == _platformId)
			{
				_worker.ReportProgress(100, "Unable to read the device's platform ID. Cannot continue.");
				SetFailureState();
				return false;
			}

			return true;
		}


		private bool GatherData_GetSerialNumber()
		{
			var device = Conversation.Device;

			// Get the serial number.
			_worker.ReportProgress(-1, "Checking device serial number...");
			_serialNo = GetIntSetting(Conversation, "system.serial");
			if (!IsValidSerialNumber(_serialNo))
			{
				#if DEV
				_serialNo = 0; // Treat both invalid values as zero for simplicity.
				_worker.ReportProgress(-1, "This device does not yet have a serial number.");
				_dispatcher.Invoke(new Action(() =>
				{
					KeyNotice = "NOTE: This device does not have a serial number; "
											+ "the firmware update file you download will be applicable "
											+ "to all devices of the same type.";
				}));
#else
				_worker.ReportProgress(100,
									   "Device has no serial number; new firmware can only be applied by Zaber. Please contact Customer Support.");
				_dispatcher.Invoke(new Action(() =>
				{
					KeyNotice =
						"ERROR: This device has no serial number and must be RMAed to update the firmware.";
				}));
				SetFailureState();
				return false;
				#endif
			}

			return true;
		}


		private bool GatherData_CheckKeyed()
		{
			var device = Conversation.Device;

			// Get the keyed flag
			#if DEV
			_worker.ReportProgress(-1, "Checking whether device is keyed...");
#endif

			var result = false;
			try
			{
				var response = Conversation.Request("get bootloader.keyed");
				_keyed = 0m != response.NumericData;
				result = true;
			}
			catch (Exception aException)
			{
				_worker.ReportProgress(-1,
									   $"Could not get setting bootloader.keyed from device {device.DeviceNumber}. Error: {aException.Message}");
				SetFailureState();
			}

			#if !DEV

			// Unkeyed devices must be returned to Zaber for firmware updates, even if they have a serial number.
			if (result && !_keyed)
			{
				result = false;
				_worker.ReportProgress(100,
									   "Sorry, new firmware can only be applied to this device by Zaber. Please contact Customer Support.");
				_dispatcher.Invoke(new Action(() =>
				{
					KeyNotice =
						"ERROR: This device must be RMAed to update the firmware.";
				}));
				SetFailureState();
			}
			#endif

			return result;
		}


		private bool GatherData_SaveCalibrationTables()
		{
			_worker.ReportProgress(-1, "Getting calibration tables...");
			_calibrationTables.Clear();

			DeviceHelper.ProgressReportCallback progressCallback = (aProgress, aMessage, aReportType) =>
			{
				_worker.ReportProgress((int) (10.0f * aProgress));
			};

			bool success = true;

			try
			{
				if (Conversation.Device.FirmwareVersion < 700)
				{
					// FW6 calibration tables always need to be backed up before a bootload, and are always stored on the controller.
					_calibrationTables.AddRange(DeviceHelper.ReadAllCalibrationTables(Conversation, progressCallback));
				}
				else
				{
					// If a FW7 controller is selected, try to read calibration tables from axes that are not auto-detectable.
					// Auto-detectable peripherals will preserve their calibration tables in peripheral memory during a bootload,
					// but FW7 stores the calibration tables for non-auto-detectable tables on the controller.
					// FW7 integrated devices do not need calibration to be backed up.
					foreach (var axisConversation in Conversation.Axes)
					{
						var deviceType = axisConversation.Device.DeviceType;
						if ((deviceType.PeripheralId != 0) && !deviceType.Capabilities.Contains(Capability.AutoDetect))
						{
							_calibrationTables.AddRange(DeviceHelper.ReadAllCalibrationTables(axisConversation, progressCallback));
						}
					}
				}

				_worker.ReportProgress(-1, $"Found {_calibrationTables.Count} calibration tables to be backed up.");
			}
			catch (Exception e)
			{
				_appLog.Error("Error reading calibration tables before firmware update.", e);
				success = false;
			}

			return success;
		}


		private bool GatherData_ReportDeviceInfo()
		{
			var conv = Conversation;

			var fwVersion = GetStringSetting(conv, "version");
			var fwBuild = GetIntSetting(conv, "version.build");

			var device = conv.Device;
			var deviceInfoStr = $"{device.Description}, firmware {fwVersion}";
			if (fwBuild > 0)
			{
				deviceInfoStr += $" build {fwBuild}";
			}

			deviceInfoStr += $", serial# {_serialNo}";

			#if DEV
			deviceInfoStr += _keyed ? " (keyed)" : " (unkeyed)";
			#endif

			_worker.ReportProgress(-1, "Selected device is " + deviceInfoStr);
			_dispatcher.Invoke(new Action(() => { DeviceInfo = deviceInfoStr; }));

			_isJoystick = device.DeviceType.IsJoystick;


			// Generate a filename-safe version of the device name for use in download filename generation.
			_deviceName = device.DeviceType.Name ?? "Unknown";
			_deviceName = _deviceName.Replace(' ', '_');
			foreach (var c in Path.GetInvalidFileNameChars())
			{
				_deviceName = _deviceName.Replace(c, '_');
			}

			return true;
		}


		private bool GatherData_CheckServerInfo()
		{
			_worker.ReportProgress(-1, "Contacting firmware server...");

			var query = FirmwareWebQueryBuilder.MakeServerInfoQuery();
			var response = TryDownload(PORTAL_URL, query, 20000, out var errorMsg, out var contentType);

			if (null == response)
			{
				_worker.ReportProgress(100, errorMsg);
				SetDownloadFailureState();
				return false;
			}

			var json = FirmwareWebQueryBuilder.DecodeResponse<FirmwareWebQueryBuilder.ServerInfoResponse>(response);

			if (null == json)
			{
				_worker.ReportProgress(100,
									   "There was an error communicating with the Zaber firmware server. Downloads are not currently available. The server may be down for maintenance or there may be a network problem; please try again later.");
				SetDownloadFailureState();
				return false;
			}

			if (!json.Success)
			{
				_worker.ReportProgress(100,
									   $"There was an error communicating with the Zaber firmware server: {json.Message}");
				SetDownloadFailureState();

				var mbvm = new MessageBoxParams
				{
					Message = json.Message,
					Caption = "Firmware service notice",
					Icon = MessageBoxImage.Information,
					Buttons = MessageBoxButton.OK
				};

				_dispatcher.BeginInvoke(new Action(() => { RequestDialogOpen?.Invoke(this, mbvm); }));

				return false;
			}

			if (!json.SupportedProtocolVersions.Contains(FirmwareWebQueryBuilder.CURRENT_PROTOCOL_VERSION))
			{
				_worker.ReportProgress(100, "This client software is not supported by the Zaber firmware server.");
				SetDownloadFailureState();
				return false;
			}

			_worker.ReportProgress(-1, "Server connection established.");

			return true;
		}


		private bool GatherData_GetFirmwareVersions()
		{
			_worker.ReportProgress(-1, "Retrieving available firmware versions...");

			// Query Zaber website for available versions.
			var deviceIdentifier = new FirmwareWebQueryBuilder.QueryDeviceIdentifier
			{
				PlatformID = _platformId,
				SerialNumber = _serialNo,
				DeviceNumber = _deviceNumber,
				IsKeyed = _keyed,
				DeviceID = _deviceId,
			};

			_appLog.Info("Querying available firmware versions.");
			var query = FirmwareWebQueryBuilder.MakeListVersionsQuery(new[] { deviceIdentifier });
			var response = TryDownload(PORTAL_URL, query, 20000, out var errorMsg, out var contentType);

			if (null == response)
			{
				_worker.ReportProgress(100, errorMsg);
				SetDownloadFailureState();
				return false;
			}

			var json = FirmwareWebQueryBuilder.DecodeResponse<FirmwareWebQueryBuilder.VersionListResponse>(response);

			if (null == json)
			{
				_worker.ReportProgress(100,
									   "There was an error communicating with the Zaber firmware server. Downloads are not currently available.");
				SetDownloadFailureState();
				return false;
			}

			if (!json.Success)
			{
				_worker.ReportProgress(100,
									   $"There was an error communicating with the Zaber firmware server: {json.Message}");
				SetDownloadFailureState();
				return false;
			}

			var versionsAvailable =
				new List<FirmwareWebQueryBuilder.ResponseFWVersion>(json.GetVersions(_deviceNumber));
			versionsAvailable.Sort((a, b) =>
			{
				var result = 0;
				var vA = ParseVersion(a.FWVersion, a.Build);
				var vB = ParseVersion(b.FWVersion, b.Build);
				if (vA.HasValue && vB.HasValue)
				{
					if (vA.Value > vB.Value)
					{
						result = 1;
					}
					else if (vA.Value < vB.Value)
					{
						result = -1;
					}
				}

				if (0 == result)
				{
					result = a.ToString().CompareTo(b.ToString());
				}

				return result;
			});

			if (versionsAvailable.Count < 1)
			{
				_worker.ReportProgress(100, "Sorry, there are no firmware downloads available for your device.");
				SetDownloadFailureState();
				_dispatcher.Invoke(new Action(() =>
				{
					SelectedVersion = null;
					AvailableVersions.Clear();
					KeyNotice += Environment.NewLine
							 + Environment.NewLine
							 + "Sorry, there are no firmware downloads available for your device.";
				}));

				return false;
			}

			_worker.ReportProgress(100,
								   $"Found {versionsAvailable.Count} firmware versions applicable to your device.");
			_dispatcher.Invoke(new Action(() =>
			{
				DownloadControlsEnabled = true;

				AvailableVersions =
					new ObservableCollection<FirmwareVersionVM>(
						versionsAvailable.Select(v => new FirmwareVersionVM { Version = v }));

				if (!AvailableVersions.Contains(SelectedVersion))
				{
					// Default to the highest non-engineering version.
					FirmwareVersionVM selection = null;

					foreach (var version in AvailableVersions.Reverse())
					{
						var fwv = ParseVersion(version.Version.FWVersion, version.Version.Build);
						if (fwv.HasValue && (fwv.Value.Minor < 90))
						{
							selection = version;
							break;
						}
					}

					SelectedVersion = selection;
				}
			}));

			return true;
		}


		private uint GetIntSetting(Conversation aConv, string aSettingName)
		{
			uint result = 0;

			try
			{
				var response = aConv.Request($"get {aSettingName}");
				result = (uint) response.NumericData;
			}
			catch (Exception aException)
			{
				_worker.ReportProgress(-1,
									   $"Could not get setting {aSettingName} from device {aConv.Device.DeviceNumber}. Error: {aException.Message}");
			}

			return result;
		}


		private string GetStringSetting(Conversation aConv, string aSettingName)
		{
			string result = null;

			try
			{
				var response = aConv.Request($"get {aSettingName}");
				result = response.TextData;
			}
			catch (Exception aException)
			{
				_worker.ReportProgress(-1,
									   $"Could not get setting {aSettingName} from device {aConv.Device.DeviceNumber}. Error: {aException.Message}");
			}

			return result;
		}


		private static FirmwareVersion? ParseVersion(string aVersion, uint aBuild = 0)
		{
			var major = 0;
			var minor = 0;

			try
			{
				var matches = Regex.Matches(aVersion, @"([0-9]+)\.([0-9]{2})");
				if ((1 <= matches.Count) && (3 == matches[0].Groups.Count))
				{
					major = int.Parse(matches[0].Groups[1].Value, CultureInfo.InvariantCulture);
					minor = int.Parse(matches[0].Groups[2].Value, CultureInfo.InvariantCulture);
					return new FirmwareVersion(major, minor, (int) aBuild);
				}
			}
			catch (ArgumentException)
			{
			}
			catch (FormatException)
			{
			}
			catch (OverflowException)
			{
			}

			return null;
		}


		private static bool ShouldBackupCalibrationTables(ZaberDevice aDevice)
		{
			var version = aDevice.FirmwareVersion;
			if (version.Major > 6)
			{
				// FW7 integrated devices save calibration data. 
				return aDevice.IsController;
			}

			return true;
		}

		#endregion

		#endregion
	}
}

﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Input;
using MvvmDialogs.ViewModels;
using ZaberConsole.Properties;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Dialogs;

namespace ZaberConsole.Dialogs
{
	/// <summary>
	///     ViewModel for the plugin options dialog.
	/// </summary>
	public class PluginOptionsDialogVM : ObservableObject, IUserDialogViewModel, IDialogClient
	{
		#region -- Setup --

		/// <summary>
		///     Default constructor. Initializes dialog result to false.
		/// </summary>
		public PluginOptionsDialogVM()
		{
			Result = false;
		}


		/// <summary>
		///     If true when the dialog closes, the user clicked the OK button
		///     and the new visibility settings should be applied.
		/// </summary>
		public bool Result { get; private set; }

		#endregion

		#region -- View-Bound properties --

		/// <summary>
		///     List of descriptions of available plugins.
		/// </summary>
		public ObservableCollection<PluginInstanceOptionsVM> Plugins
		{
			get => _plugins;
			set => Set(ref _plugins, value, nameof(Plugins));
		}


		/// <summary>
		///     Currently selected plugin in the list.
		/// </summary>
		public PluginInstanceOptionsVM SelectedPlugin
		{
			get => _selectedPlugin;
			set => Set(ref _selectedPlugin, value, nameof(SelectedPlugin));
		}


		/// <summary>
		///     Indicates that the plugin folder is specified.
		/// </summary>
		public bool IsPluginFolderSet
		{
			get
			{
				var dir = Settings.Default.PlugInsFolder;
				return !string.IsNullOrEmpty(dir) && Directory.Exists(dir);
			}
		}


		/// <summary>
		///     Command invoked by the "explore plugin folder" button.
		/// </summary>
		public ICommand ExploreFolderCommand => new RelayCommand(_ => OnExplorePluginFolder());


		/// <summary>
		///     Command invoked by the "set plugin folder" button.
		/// </summary>
		public ICommand SetFolderCommand => new RelayCommand(_ => OnSetPluginFolder());


		/// <summary>
		///     Command invoked by the cancel button.
		/// </summary>
		public ICommand CancelCommand => new RelayCommand(_ => RequestClose());


		/// <summary>
		///     Command invoked by the OK button.
		/// </summary>
		public ICommand OkCommand => new RelayCommand(_ => OnOkButton());

		#endregion

		#region -- Functionality --

		private void OnExplorePluginFolder()
		{
			var settings = Settings.Default;
			var plugInsFolder = settings.PlugInsFolder;

			if ((null == plugInsFolder) || (0 == plugInsFolder.Length))
			{
				OnSetPluginFolder();
				plugInsFolder = settings.PlugInsFolder;

				if ((null == plugInsFolder) || (0 == plugInsFolder.Length))
				{
					// must have cancelled
					return;
				}
			}

			if (Directory.Exists(plugInsFolder))
			{
				Process.Start(plugInsFolder);
			}
			else
			{
				var mbvm = new MessageBoxParams
				{
					Message = "Could not find directory '" + plugInsFolder + "'. Please set the plugin folder.",
					Buttons = MessageBoxButton.OK,
					Icon = MessageBoxImage.Error
				};

				RequestDialogOpen?.Invoke(this, mbvm);
			}
		}


		private void OnSetPluginFolder()
		{
			var settings = Settings.Default;
			var dlgVM = new FolderBrowserDialogParams();
			dlgVM.SelectedPath = settings.PlugInsFolder;
			dlgVM.Description = "Select a folder to hold your plug-ins.";
			dlgVM.ShowNewFolderButton = true;
			RequestDialogOpen?.Invoke(this, dlgVM);

			if (dlgVM.DialogResult.HasValue && dlgVM.DialogResult.Value)
			{
				settings.PlugInsFolder = dlgVM.SelectedPath;

				RequestClose();

				var mbvm = new MessageBoxParams
				{
					Message = "Plugins will be reloaded next time you start Zaber Console.",
					Caption = "Plugin folder changed",
					Buttons = MessageBoxButton.OK,
					Icon = MessageBoxImage.Information
				};

				RequestDialogOpen?.Invoke(this, mbvm);
				OnPropertyChanged("IsPluginFolderSet");
			}
		}


		private void OnOkButton()
		{
			Result = true;
			RequestClose();
		}

		#endregion

		#region -- IUserDialogViewModel implementation --

		public event EventHandler DialogClosing;


		public void RequestClose() => DialogClosing?.Invoke(this, null);


		public bool IsModal => true;


		public event EventHandler BringToFront;


		public void RequestBringToFront() => BringToFront?.Invoke(this, null);


		/// <summary>
		///     Any exception that was unhandled by the dialog itself.
		/// </summary>
		public Exception Exception { get; set; }

		#endregion

		#region -- IDialogClient --

		public event RequestDialogChange RequestDialogOpen;

		#pragma warning disable CS0067 // Event is never used.
		public event RequestDialogChange RequestDialogClose;
		#pragma warning restore CS0067

		#endregion

		#region -- Data --

		private ObservableCollection<PluginInstanceOptionsVM> _plugins =
			new ObservableCollection<PluginInstanceOptionsVM>();

		private PluginInstanceOptionsVM _selectedPlugin;

		#endregion
	}
}

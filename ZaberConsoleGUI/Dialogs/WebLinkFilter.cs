﻿using System;
using System.Diagnostics;
using ZaberConsole.Properties;
using ZaberWpfToolbox.Dialogs;

namespace ZaberConsole.Dialogs
{
	/// <summary>
	///     Helper for web browser helper navigation filter. If network access is disabled in the
	///     program settings, this will pop up a dialog when the user clicks a non-local link
	///     to ask them if they want to go online.
	/// </summary>
	public static class WebLinkFilter
	{
		#region -- Events --

		public static event RequestDialogChange RequestDialogOpen;

		#endregion

		#region -- Public Methods & Properties --

		public static Predicate<Uri> NavigationFilter => aUrl =>
		{
			if (!Settings.Default.AllowNetworkAccess
			&& !_enableThisSession
			&& (null != aUrl)
			&& !string.IsNullOrEmpty(aUrl.Scheme)
			&& !aUrl.Scheme.Contains("file")
			&& (null != RequestDialogOpen))
			{
				var message =
					"The link you have clicked leads to an online resource, "
				+ "but you have disabled network access in the Zaber Console options."
				+ Environment.NewLine
				+ Environment.NewLine
				+ "Do you want to enable network access and view this link?";
				var dlg = new CustomMessageBoxVM(message, "Allow Network Access?");
				dlg.DialogResult = 0;
				dlg.DoNotShowAgainSettings =
					Settings.Default.DoNotShowAgainSettings.FindOrCreate("WebLinkFilterPolicy");
				dlg.AddOption("Enable", 1);
				dlg.AddOption("Enable this session only", 2);
				dlg.AddOption("Open in external browser", 4);
				dlg.AddOption("Do not enable (cancels navigation)", 3);

				RequestDialogOpen.Invoke(null, dlg);

				var result = false;
				switch ((int) dlg.DialogResult)
				{
					case 4: // Open links in external browser.
						Process.Start(aUrl.AbsoluteUri);
						break;
					case 3: // User cancelled navigation.

						// Do not show again setting is saved in this case; online links will
						// not work as long as network access is disabled.
						break;
					case 2: // User opted to enable network access just for this session.
						_enableThisSession = true;
						result = true;

						// Cancel the do not show again setting in this case.
						dlg.DoNotShowAgainSettings.ShowDialog = true;
						break;
					case 1: // User opted to change setting.
						Settings.Default.AllowNetworkAccess = true;
						result = true;
						break;
				}

				return result;
			}

			return true;
		};

		#endregion

		#region -- Data --

		private static bool _enableThisSession;

		#endregion
	}
}

﻿using System;
using ZaberConsole.Plugins;
using ZaberWpfToolbox;

namespace ZaberConsole.Dialogs
{
	/// <summary>
	///     Row VM used in the Plugin Options dialog. Represents information about one known plugin type.
	/// </summary>
	public class PluginInstanceOptionsVM : ObservableObject
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Constructor. Initializes properties given a plugin type.
		/// </summary>
		/// <param name="aPluginType">The plugin type to represent.</param>
		/// <param name="aVisible">Whether or not the plugin is currently visible.</param>
		public PluginInstanceOptionsVM(PluginTypeInfo aPluginType, bool aVisible)
		{
			Name = aPluginType.DisplayName;
			AlwaysActive = aPluginType.AlwaysActive;
			HasUI = aPluginType.HasUI;
			PluginType = aPluginType;

			_visible = aVisible;

			if (HasUI)
			{
				VisibilityToolTip = AlwaysActive
					? "This plugin's user interface cannot be hidden."
					: "Show or hide this plugin's user interface.";
			}
			else
			{
				VisibilityToolTip = "This plugin has no user interface.";
			}

			if (string.IsNullOrEmpty(aPluginType.AssemblyInfo.AssemblyFilePath))
			{
				Details =
					$"Class {aPluginType.Type.Name} (built-in), Version = {aPluginType.AssemblyInfo.AssemblyVersion}";
			}
			else
			{
				Details =
					$"Class {aPluginType.Type.Name} from {aPluginType.AssemblyInfo.AssemblyFilePath}, Version = {aPluginType.AssemblyInfo.AssemblyVersion}";
			}

			if (!string.IsNullOrEmpty(aPluginType.Description))
			{
				Details = aPluginType.Description + Environment.NewLine + Environment.NewLine + Details;
			}
		}


		internal PluginTypeInfo PluginType { get; }

		public string Name { get; }

		public string Details { get; }


		public bool AlwaysActive { get; }

		public bool AlwaysVisible => AlwaysActive && HasUI;

		public string VisibilityToolTip { get; }

		public bool Visible
		{
			get => _visible;
			set
			{
				value |= AlwaysVisible;
				value &= HasUI;
				_visible = value;
				OnPropertyChanged(nameof(Visible));
			}
		}


		public bool HasUI { get; }

		#endregion

		#region -- Data --

		private bool _visible;

		#endregion
	}
}

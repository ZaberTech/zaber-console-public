﻿using System.Windows;

namespace ZaberConsole.Dialogs
{
	/// <summary>
	///     Interaction logic for PluginOptionsDialog.xaml
	/// </summary>
	public partial class PluginOptionsDialog : Window
	{
		#region -- Public Methods & Properties --

		public PluginOptionsDialog()
		{
			InitializeComponent();
		}

		#endregion
	}
}

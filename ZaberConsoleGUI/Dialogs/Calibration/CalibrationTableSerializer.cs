﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using DataAccess;
using MvvmDialogs.ViewModels;
using ZaberWpfToolbox.Dialogs;

namespace ZaberConsole.Dialogs.Calibration
{
	/// <summary>
	///     Helper class for loading and saving calibration tables to CSV files.
	/// </summary>
	public static class CalibrationTableSerializer
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Load all calibration tables in a given CSV file.
		/// </summary>
		/// <param name="aFilePath">
		///     Path to the file to load. This can be absolute or relative
		///     to the current working directory.
		/// </param>
		/// <returns>All the calibration tables found in the file.</returns>
		/// <remarks>This method does not trap file I/O exceptions - user code should handle them.</remarks>
		public static IEnumerable<CalibrationTableVM> LoadFromCsv(string aFilePath)
		{
			var result = new List<CalibrationTableVM>();

			if (File.Exists(aFilePath))
			{
				var data = DataTable.New.ReadLazy(aFilePath);
				foreach (var row in data.Rows)
				{
					var table = new CalibrationTableVM();
					table.DataVersion = int.Parse(row[DATA_VERSION_HEADER]);
					if (1 != table.DataVersion)
					{
						throw new FormatException($"Calibration data version {table.DataVersion} is not supported.");
					}

					table.TimeStamp = DateTime.Parse(row[TIME_STAMP_HEADER]);
					table.SerialNumber = uint.Parse(row[SERIAL_NUMBER_HEADER]);
					table.CalibrationType = int.Parse(row[CALIBRATION_TYPE_HEADER]);
					table.TableWidth = int.Parse(row[TABLE_WIDTH_HEADER]);
					table.Description = row[DESCRIPTION_HEADER];
					table.Data = new ObservableCollection<int>();
					table.Filename = Path.GetFileName(aFilePath);

					table.TableOffset = 0;
					if (row.ColumnNames.Contains(TABLE_OFFSET_HEADER))
					{
						table.TableOffset = int.Parse(row[TABLE_OFFSET_HEADER]);
					}

					table.IsSelected = false;
					for (var i = 0; i < 64; ++i)
					{
						table.Data.Add(int.Parse(row[DATA_HEADER_PREFIX + (i + 1)]));
					}

					result.Add(table);
				}
			}

			return result;
		}


		/// <summary>
		///     Save a list of calibration tables to a CSV file. If the file already exists, this will
		///     replace it.
		/// </summary>
		/// <param name="aFilePath">
		///     Path to the file to save. This can be absolute or relative
		///     to the current working directory.
		/// </param>
		/// <param name="aTables">Collection of calibration tables to save to the file.</param>
		/// <remarks>
		///     If the aTables list is empty, only the column headers will be written
		///     to the file.
		///     This method does not trap file I/O exceptions; the caller should handle them.
		/// </remarks>
		public static void SaveToCsv(string aFilePath, IEnumerable<CalibrationTableVM> aTables)
		{
			var data = new MutableDataTable();

			var count = aTables.Count();
			var dataVerCol = new Column(DATA_VERSION_HEADER, count);
			var timeStampCol = new Column(TIME_STAMP_HEADER, count);
			var serialCol = new Column(SERIAL_NUMBER_HEADER, count);
			var calTypeCol = new Column(CALIBRATION_TYPE_HEADER, count);
			var widthCol = new Column(TABLE_WIDTH_HEADER, count);
			var offsetCol = new Column(TABLE_OFFSET_HEADER, count);
			var descCol = new Column(DESCRIPTION_HEADER, count);

			var cols = new List<Column>();
			cols.Add(dataVerCol);
			cols.Add(timeStampCol);
			cols.Add(serialCol);
			cols.Add(calTypeCol);
			cols.Add(widthCol);
			cols.Add(offsetCol);
			cols.Add(descCol);

			dataVerCol.Values = aTables.Select(t => t.DataVersion.ToString()).ToArray();
			timeStampCol.Values = aTables.Select(t => t.TimeStamp.ToString()).ToArray();
			serialCol.Values = aTables.Select(t => t.SerialNumber.ToString()).ToArray();
			calTypeCol.Values = aTables.Select(t => t.CalibrationType.ToString()).ToArray();
			widthCol.Values = aTables.Select(t => t.TableWidth.ToString()).ToArray();
			offsetCol.Values = aTables.Select(t => t.TableOffset.ToString()).ToArray();
			descCol.Values = aTables.Select(t => t.Description ?? string.Empty).ToArray();
			for (var i = 0; i < 64; ++i)
			{
				var columnName = DATA_HEADER_PREFIX + (i + 1);
				var column = new Column(columnName, count);
				column.Values = aTables.Select(t => t.Data[i].ToString()).ToArray();
				cols.Add(column);
			}

			data.Columns = cols.ToArray();

			data.SaveCSV(aFilePath);
		}


		/// <summary>
		///     Prompt the user for a file to save calibration data to, and save the data if
		///     the user indicates to do so.
		/// </summary>
		/// <param name="aInitialDirectory">Directory to show initially when prompting for the location to save the file.</param>
		/// <param name="aTables">Collection of calibration tables to save.</param>
		/// <param name="aDialogPresenter">Callback to display dialog boxes.</param>
		/// <returns>The path to the file the data was saved in, or null if the data was not saved.</returns>
		/// <remarks>This method does not trap file I/O exceptions - user code should handle them.</remarks>
		public static string PromptSaveToCsv(string aInitialDirectory, IEnumerable<CalibrationTableVM> aTables,
											 Action<IDialogViewModel> aDialogPresenter)
		{
			string result = null;

			var dialog = new FileBrowserDialogParams();
			dialog.Filter = "Comma-Separated Value files (*.csv)|*.csv|All files (*.*)|*.*";
			dialog.FilterIndex = 1;
			dialog.InitialDirectory = aInitialDirectory;
			dialog.CheckFileExists = false;
			dialog.AddExtension = true;
			dialog.Action = FileBrowserDialogParams.FileActionType.Save;
			dialog.DefaultExtension = ".csv";
			dialog.Title = "Save Calibration Table(s) to CSV File";

			aDialogPresenter(dialog);

			if (dialog.DialogResult.HasValue && dialog.DialogResult.Value)
			{
				SaveToCsv(dialog.Filename, aTables);
				result = dialog.Filename;

				var mbvm2 = new MessageBoxParams
				{
					Message = $"Data successfully saved to {dialog.Filename}",
					Caption = "Success",
					Buttons = MessageBoxButton.OK,
					Icon = MessageBoxImage.Information
				};

				aDialogPresenter(mbvm2);
			}

			return result;
		}

		#endregion

		#region -- Data --

		private const string DATA_VERSION_HEADER = "Data Version";
		private const string TIME_STAMP_HEADER = "Time Stamp";
		private const string SERIAL_NUMBER_HEADER = "Serial Number";
		private const string CALIBRATION_TYPE_HEADER = "Calibration Type";
		private const string TABLE_WIDTH_HEADER = "Table Width";
		private const string TABLE_OFFSET_HEADER = "Table Offset";
		private const string DESCRIPTION_HEADER = "Description";
		private const string DATA_HEADER_PREFIX = "Data_";

		#endregion
	}
}

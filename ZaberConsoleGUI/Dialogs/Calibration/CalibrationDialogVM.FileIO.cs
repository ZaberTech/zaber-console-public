﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Zaber;
using ZaberConsole.Properties;
using ZaberWpfToolbox.Dialogs;

namespace ZaberConsole.Dialogs.Calibration
{
	public partial class CalibrationDialogVM
	{
		#region -- Non-Public Methods --

		internal void LoadTablesFromFiles(IEnumerable<string> aFileNames)
		{
			var newTables = new List<CalibrationTableVM>();
			var errors = new StringBuilder();

			if ((null == Conversation) || (!Conversation.Device.IsSingleDevice))
			{
				DisplayError(Resources.STR_CALIBRATION_ERROR_SINGLE_DEVICE);
				return;
			}

			foreach (var filePath in aFileNames)
			{
				try
				{
					newTables.AddRange(CalibrationTableSerializer.LoadFromCsv(filePath));
				}
				catch (Exception aException)
				{
					errors.AppendLine($"When reading {filePath}: {aException.Message}");
					_log.Error($"Exception reading {filePath}", aException);
				}
			}

			// If something was successfully loaded, update the last folder setting.
			if (newTables.Count > 0)
			{
				Settings.Default.LastCalibrationDataFolder = Path.GetDirectoryName(aFileNames.First());
			}

			// Display file load errors if any.
			var errorMsg = errors.ToString();
			if (!string.IsNullOrEmpty(errorMsg))
			{
				DisplayError(errorMsg);
			}

			// Filter loaded tables by device's supported type, if known.
			var multipleTablesAllowed =
				(null != Conversation)
				&& Conversation.Device.IsController
				&& (Conversation.Device.FirmwareVersion < 700);
			if (!multipleTablesAllowed && (1 == DeviceTables.Count))
			{
				newTables = newTables.Where(t => t.CalibrationType == DeviceTables[0].CalibrationType).ToList();
			}

			// Prompt user to select tables.
			if (newTables.Count < 1)
			{
				DisplayError(
					"None of the selected files contains any calibration tables compatible with the selected device.");
				newTables = new List<CalibrationTableVM>();
			}
			else
			{
				var selectionDialog = new CalibrationTableSelectionDialogVM(newTables, multipleTablesAllowed);
				RequestDialogOpen?.Invoke(this, selectionDialog);
				newTables = new List<CalibrationTableVM>();
				if (selectionDialog.Result)
				{
					newTables = new List<CalibrationTableVM>(selectionDialog.SelectedTables);
				}
			}

			if (newTables.Any())
			{
				SendTablesToDevice(newTables);
				_eventLog.LogEvent("Loaded calibration tables from file",
								   "Peripheral serial #s "
							   + string.Join(", ", newTables.Select(t => t.SerialNumber.ToString())));
			}
		}


		private void OnLoadFileClicked()
		{
			var dialog = new FileBrowserDialogParams();
			dialog.Filter = "Comma-Separated Value files (*.csv)|*.csv|All files (*.*)|*.*";
			dialog.FilterIndex = 1;
			dialog.InitialDirectory = Settings.Default.LastCalibrationDataFolder;
			dialog.MultiSelect = true;
			dialog.Title = "Load CSV Calibration Tables";

			RequestDialogOpen?.Invoke(this, dialog);

			if (!dialog.DialogResult.HasValue || !dialog.DialogResult.Value)
			{
				return;
			}

			// Load the selected files.
			LoadTablesFromFiles(dialog.Filenames);
		}


		private void OnSaveFileClicked(IEnumerable<CalibrationTableVM> aTables)
		{
			try
			{
				var outputPath =
					CalibrationTableSerializer.PromptSaveToCsv(Settings.Default.LastCalibrationDataFolder,
															   aTables,
															   dlg => { RequestDialogOpen?.Invoke(this, dlg); });

				Settings.Default.LastCalibrationDataFolder = Path.GetDirectoryName(outputPath);

				_eventLog.LogEvent("Saved calibration tables to file",
								   "Peripheral serial #s "
							   + string.Join(", ", aTables.Select(t => t.SerialNumber.ToString())));
			}
			catch (Exception aException)
			{
				var message = $"There was an error saving the data to a CSV file: {aException.Message}";
				_log.Error(message, aException);
				DisplayError(message);
			}
		}

		#endregion
	}
}

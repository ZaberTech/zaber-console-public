﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using GongSolutions.Wpf.DragDrop;
using log4net;
using MvvmDialogs.ViewModels;
using Zaber;
using Zaber.Telemetry;
using Zaber.Threading;
using ZaberConsole.Properties;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Dialogs;

namespace ZaberConsole.Dialogs.Calibration
{
	public partial class CalibrationDialogVM : ObservableObject, IUserDialogViewModel, IDialogClient
	{
		#region -- Setup --

		public CalibrationDialogVM(ZaberPortFacade aPort, Conversation aConversation)
		{
			_dispatcher = Dispatcher.CurrentDispatcher;

			PortFacade = aPort;
			Conversation = aConversation;

			_worker = BackgroundWorkerManager.CreateWorker();
			_worker.WorkerReportsProgress = true;
			_worker.WorkerSupportsCancellation = true;
			_worker.ProgressChanged += Worker_ProgressChanged;

			DialogClosing += OnDialogClosing;
		}


		private ZaberPortFacade PortFacade { get; }


		private Conversation Conversation {get; }

		#endregion

		#region -- View-bound properties --

		public ObservableCollection<CalibrationTableVM> DeviceTables
		{
			get => _deviceTables;
			set => Set(ref _deviceTables, value, nameof(DeviceTables));
		}


		public bool Busy
		{
			get => _busy;
			set => Set(ref _busy, value, nameof(Busy));
		}


		public double Progress
		{
			get => _progress;
			set => Set(ref _progress, value, nameof(Progress));
		}


		public string DeviceInfo
			=> ("Calibration currently loaded on: " + Conversation?.Device.Description) ?? string.Empty;


		public IDropTarget DeviceDropHandler => new DeviceTableDropHandler(this);


		public ICommand WindowLoadedCommand => new RelayCommand(_ => OnWindowLoaded());


		public ICommand WindowClosingCommand => new RelayCommand(_ => RequestClose());


		public ICommand LoadCsvCommand => new RelayCommand(_ => OnLoadFileClicked());


		public ICommand SaveCsvCommand => new RelayCommand(_ => OnSaveFileClicked(SelectedDeviceTables),
														   _ => SelectedDeviceTables.Any());


		public ICommand DeleteSelectedDeviceTableCommand
			=> new RelayCommand(_ => OnDeleteTablesClicked(),
								_ => SelectedDeviceTables.Any());

		#endregion

		#region -- IUserDialogViewModel implementation --

		public bool IsModal => true;

		public event EventHandler DialogClosing;

		public event EventHandler BringToFront;


		public void RequestClose()
		{
			CancelWorker();
			PortFacade.Closed -= PortFacade_Closed;
			DialogClosing?.Invoke(this, null);
		}


		public void RequestBringToFront() => BringToFront?.Invoke(this, null);

		#endregion

		#region -- IDialogClient implementation --

		public event RequestDialogChange RequestDialogOpen;
		#pragma warning disable CS0067 // Event is never used.
		public event RequestDialogChange RequestDialogClose;
		#pragma warning restore CS0067


		/// <summary>
		///     Any exception that was unhandled by the dialog itself.
		/// </summary>
		public Exception Exception { get; set; }

		#endregion

		#region -- Event Handlers --

		private void OnWindowLoaded()
		{
			PortFacade.Closed += PortFacade_Closed;

			// Read all calibration tables from the device as soon as the window opens.
			_worker.DoWork += ListDeviceTables_Worker;
			_worker.RunWorkerCompleted += ListDeviceTables_WorkCompleted;
			_worker.Run();

			// Special case: recommend FW 6.25 be upgraded to a newer version.
			_dispatcher.BeginInvoke(new Action(() =>
			{
				var deviceType = Conversation?.Device.DeviceType;
				if ((null != deviceType) && (deviceType.FirmwareVersion == 625))
				{
					var msgBox = new MessageBoxParams
					{
						Buttons = MessageBoxButton.OK,
						Caption = Resources.STR_FIRMWARE_UPDATE_RECOMMENDED,
						Icon = MessageBoxImage.Information,
						Message = Resources.CalibrationFWUpdateRecommendationText
					};

					RequestDialogOpen?.Invoke(this, msgBox);
				}
			}));
		}


		private void OnDeleteTablesClicked()
		{
			var dlg = new CustomMessageBoxVM(
				"WARNING: Calibration data will be lost unless you have saved it to a CSV file. Please be sure you have a backup before proceeding.",
				"Potential Data Loss",
				"Delete",
				"Cancel")
				{
					DoNotShowAgainSettings =
						Settings.Default.DoNotShowAgainSettings.FindOrCreate("DeleteCalibrationTables")
				};

			RequestDialogOpen?.Invoke(this, dlg);

			if (MessageBoxResult.Yes == (MessageBoxResult) dlg.DialogResult)
			{
				_worker.DoWork += DeleteTables_Worker;
				_worker.RunWorkerCompleted += DeleteTables_WorkCompleted;
				_worker.Run(SelectedDeviceTables);
			}
			else
			{
				// If the user didn't click yes, disable the do not show again behavior else
				// they will never be able to perform this action.
				dlg.DoNotShowAgainSettings.ShowDialog = true;
			}
		}


		private void PortFacade_Closed(object aSender, EventArgs aArgs)
		{
			SetBusy(false);
			DisplayError("The port has been closed; cannot continue.", true);
		}


		private void Worker_ProgressChanged(object aSender, ProgressChangedEventArgs aArgs)
		{
			if (aArgs.ProgressPercentage >= 0)
			{
				// BackgroundWorkers report progress on the thread that created them, so
				// a dispatcher should not be needed for UI updates here.
				Progress = aArgs.ProgressPercentage;
			}

			var error = aArgs.UserState as string;
			if (!string.IsNullOrEmpty(error))
			{
				Busy = false;
				DisplayError(error, true);
			}
		}


		private void OnDialogClosing(object aSender, EventArgs aArgs)
		{
			// Workaround for a FW 6.25 bug (see Trac ticket #2149).
			// The bug was that sending "calibration print" would cause some later commands to fail.
			var deviceType = Conversation?.Device.DeviceType;
			if ((null != deviceType) && (deviceType.FirmwareVersion == 625))
			{
				// No message ID because we don't care about the reply, and this is
				// expected to interrupt other operations anyway.
				Conversation.Send("system reset");
			}
		}


		private void DeviceHelperCallback(float aProgress, string aMessage, DeviceHelper.ReportType aReportType)
			=> _dispatcher.BeginInvoke(new Action(() =>
			{
				Progress = 100.0f * aProgress;
				if (DeviceHelper.ReportType.Error == aReportType)
				{
					Busy = false;
					DisplayError(aMessage, true);
				}
				else if (DeviceHelper.ReportType.Warning == aReportType)
				{
					DisplayError(aMessage, false);
				}
			}));


		private void DisplayError(string aMessage, bool aIsFatal = false)
		{
			var mbvm = new MessageBoxParams
			{
				Message =
					aMessage
				+ (aIsFatal
						? Environment.NewLine + Environment.NewLine + "The Calibration window will now close."
						: string.Empty),
				Caption = aIsFatal ? "Error" : "Warning",
				Buttons = MessageBoxButton.OK,
				Icon = aIsFatal ? MessageBoxImage.Error : MessageBoxImage.Exclamation
			};

			RequestDialogOpen?.Invoke(this, mbvm);

			if (aIsFatal)
			{
				_errorState = true;
				CancelWorker();
				RequestClose();
			}
		}

		#endregion

		#region -- Helpers --

		private IEnumerable<CalibrationTableVM> SelectedDeviceTables => DeviceTables.Where(t => t.IsSelected);


		internal void SendTablesToDevice(IEnumerable<CalibrationTableVM> aTables)
		{
			_worker.DoWork += WriteTables_Worker;
			_worker.RunWorkerCompleted += WriteTables_WorkCompleted;
			_worker.Run(aTables);
		}


		private void CancelWorker()
		{
			if (_worker.IsBusy)
			{
				_worker.CancelAsync();
			}

			Progress = 0.0;
		}


		private void SetBusy(bool aValue) => _dispatcher.Invoke(new Action(() => { Busy = aValue; }));

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Calibration manager");

		private readonly IBackgroundWorker _worker;
		private readonly Dispatcher _dispatcher;
		private bool _errorState;

		private ObservableCollection<CalibrationTableVM> _deviceTables = new ObservableCollection<CalibrationTableVM>();
		private bool _busy;
		private double _progress;

		#endregion
	}
}

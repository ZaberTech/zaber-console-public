﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Zaber;

namespace ZaberConsole.Dialogs.Calibration
{
	public partial class CalibrationDialogVM
	{
		#region -- Non-Public Methods --

		private void ListDeviceTables_Worker(object aSender, DoWorkEventArgs aArgs)
		{
			_worker.ReportProgress(0, null);
			SetBusy(true);

			try
			{
				aArgs.Result = DeviceHelper.ReadAllCalibrationTables(Conversation, DeviceHelperCallback);
			}
			catch (Exception aException)
			{
				_worker.ReportProgress(100, aException.Message);
				_log.Error("Querying device for calibration table list failed.", aException);
			}
		}


		private void ListDeviceTables_WorkCompleted(object aSender, RunWorkerCompletedEventArgs aArgs)
		{
			_worker.DoWork -= ListDeviceTables_Worker;
			_worker.RunWorkerCompleted -= ListDeviceTables_WorkCompleted;

			_dispatcher.BeginInvoke(new Action(() =>
			{
				Progress = 0.0;
				Busy = false;
				DeviceTables = new ObservableCollection<CalibrationTableVM>((aArgs.Result as IEnumerable<CalibrationTableVM>) ?? new CalibrationTableVM[0]);
				_log.Info($"Device listed {DeviceTables.Count} calibration tables.");
			}));
		}


		private void WriteTables_Worker(object aSender, DoWorkEventArgs aArgs)
		{
			_worker.ReportProgress(0, null);
			SetBusy(true);
			aArgs.Result = null;

			try
			{
				foreach (var table in (aArgs.Argument as IEnumerable<CalibrationTableVM>) ?? new CalibrationTableVM[0])
				{
					DeviceHelper.WriteCalibrationTable(Conversation, table, DeviceHelperCallback);
				}

				if (!_errorState)
				{
					aArgs.Result = DeviceHelper.ReadAllCalibrationTables(Conversation, DeviceHelperCallback);
				}
			}
			catch (Exception aException)
			{
				_worker.ReportProgress(100, aException.Message);
				_log.Error("Writing a calibration table failed.", aException);
			}
		}


		private void WriteTables_WorkCompleted(object aSender, RunWorkerCompletedEventArgs aArgs)
		{
			_worker.DoWork -= WriteTables_Worker;
			_worker.RunWorkerCompleted -= WriteTables_WorkCompleted;

			_dispatcher.BeginInvoke(new Action(() =>
			{
				Progress = 0.0;
				Busy = false;
				DeviceTables = new ObservableCollection<CalibrationTableVM>((aArgs.Result as IEnumerable<CalibrationTableVM>) ?? new CalibrationTableVM [0]);
			}));
		}


		private void DeleteTables_Worker(object aSender, DoWorkEventArgs aArgs)
		{
			_worker.ReportProgress(0, null);
			SetBusy(true);
			aArgs.Result = null;

			try
			{
				var list = (aArgs.Argument as IEnumerable<CalibrationTableVM>)?.ToList() ?? new List<CalibrationTableVM>();
				var count = list.Count;
				var index = 0;
				foreach (var table in list)
				{
					var request = "calibration delete";
					if ((0 == Conversation.Device.AxisNumber) && (Conversation.Device.FirmwareVersion.Major < 7))
					{
						request += " " + table.SerialNumber;
					}

					Conversation.Request(request);

					++index;
					_worker.ReportProgress((100 * index) / count);
					_eventLog.LogEvent("Deleted calibration table", $"Peripheral serial #{table.SerialNumber}");
				}

				if (!_errorState)
				{
					aArgs.Result = DeviceHelper.ReadAllCalibrationTables(Conversation, DeviceHelperCallback);
				}
			}
			catch (Exception aException)
			{
				_worker.ReportProgress(100, aException.Message);
				_log.Error("Deleting a calibration table failed.", aException);
			}
		}


		private void DeleteTables_WorkCompleted(object aSender, RunWorkerCompletedEventArgs aArgs)
		{
			_worker.DoWork -= DeleteTables_Worker;
			_worker.RunWorkerCompleted -= DeleteTables_WorkCompleted;

			_dispatcher.BeginInvoke(new Action(() =>
			{
				Progress = 0.0;
				Busy = false;
				DeviceTables = new ObservableCollection<CalibrationTableVM>((aArgs.Result as IEnumerable<CalibrationTableVM>) ?? new CalibrationTableVM[0]);
			}));
		}

		#endregion
	}
}

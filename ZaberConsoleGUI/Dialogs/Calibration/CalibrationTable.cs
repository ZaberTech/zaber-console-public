﻿using System;
using System.Collections.ObjectModel;
using ZaberWpfToolbox;

namespace ZaberConsole.Dialogs.Calibration
{
	/// <summary>
	/// Class corresponding to a device calibration table.
	/// </summary>
	public class CalibrationTable : ObservableObject
	{
		/// <summary>
		/// Data format version. Normally this should be 1. 0 is invalid, and
		/// values greater than 1 indicate a format change. Code should check this
		/// value to make sure it understands the format.
		/// </summary>
		public int DataVersion
		{
			get { return _dataVersion; }
			set
			{
				Set(ref _dataVersion, value, () => DataVersion);
			}
		}


		/// <summary>
		/// Date the data was generated.
		/// </summary>
		public DateTime TimeStamp
		{
			get { return _timeStamp; }
			set
			{
				Set(ref _timeStamp, value, () => TimeStamp);
			}
		}



		/// <summary>
		/// Peripheral serial number this data is intended for.
		/// </summary>
		public uint SerialNumber
		{
			get { return _serialNumber; }
			set
			{
				Set(ref _serialNumber, value, () => SerialNumber);
			}
		}


		/// <summary>
		/// Type of calibration data:
		/// 0 = not available
		/// 1 = direct encoder
		/// 2 = Lead screw
		/// </summary>
		public int CalibrationType
		{
			get { return _calibrationType; }
			set
			{
				Set(ref _calibrationType, value, () => CalibrationType);
			}
		}


		/// <summary>
		/// Indication of the quantity of calibration data.
		/// </summary>
		public int TableWidth
		{
			get { return _tableWidth; }
			set
			{
				Set(ref _tableWidth, value, () => TableWidth);
			}
		}


		/// <summary>
		/// Human-readable description of this data table - ie who made it, what for etc.
		/// </summary>
		public string Description
		{
			get { return _description; }
			set
			{
				Set(ref _description, value, () => Description);
			}
		}


		/// <summary>
		/// The actual data table.
		/// </summary>
		public ObservableCollection<int> Data
		{
			get { return _data; }
			set
			{
				Set(ref _data, value, () => Data);
			}
		}


		private int _dataVersion = 1;
		private DateTime _timeStamp = DateTime.Now;
		private uint _serialNumber = 0;
		private int _calibrationType = 0;
		private int _tableWidth = 0;
		private string _description = "Empty Table";
		private ObservableCollection<int> _data = new ObservableCollection<int>();
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;
using MvvmDialogs.ViewModels;
using ZaberWpfToolbox;

namespace ZaberConsole.Dialogs.Calibration
{
	public class CalibrationTableSelectionDialogVM : ObservableObject, IUserDialogViewModel
	{
		#region -- Setup and results --

		/// <summary>
		///     Initializes the dialog with a list of tables to select from.
		/// </summary>
		/// <param name="aTables">Data to populate the dialog's selection area with.</param>
		/// <param name="aAllowMultiSelect">True to enable multiple selection.</param>
		/// <exception cref="ArgumentException">There was not data provided to select from.</exception>
		public CalibrationTableSelectionDialogVM(IEnumerable<CalibrationTableVM> aTables,
												 bool aAllowMultiSelect = false)
		{
			_tables = new List<CalibrationTableVM>(aTables);
			_tablesView = new ListCollectionView(_tables);
			_tablesView.GroupDescriptions.Add(new PropertyGroupDescription("Filename"));

			if (Tables.Count == 0)
			{
				throw new ArgumentException("No tables provided to constructor.");
			}

			Result = false;
			AllowMultiSelect = aAllowMultiSelect;
		}


		/// <summary>
		///     Dialog result - true if the OK button was clicked, false otherwise.
		/// </summary>
		public bool Result { get; private set; }


		/// <summary>
		///     Tables that were selected when the dialog was closed.
		/// </summary>
		public IEnumerable<CalibrationTableVM> SelectedTables => _tables.Where(t => t.IsSelected);

		#endregion

		#region -- View-Bound Properties --

		public ListCollectionView Tables
		{
			get => _tablesView;
			set => Set(ref _tablesView, value, nameof(Tables));
		}


		public bool AllowMultiSelect
		{
			get => _allowMultiSelect;
			set => Set(ref _allowMultiSelect, value, nameof(AllowMultiSelect));
		}


		public ICommand OkButtonCommand
			=> new RelayCommand(_ =>
								{
									Result = true;
									RequestClose();
								},
								_ => SelectedTables.Any());


		public ICommand CancelButtonCommand => new RelayCommand(_ =>
		{
			Result = false;
			RequestClose();
		});

		#endregion

		#region -- IUserDialogViewModel implementation --

		public bool IsModal => true;

		public event EventHandler BringToFront;

		public event EventHandler DialogClosing;


		public void RequestBringToFront() => BringToFront?.Invoke(this, null);


		public void RequestClose() => DialogClosing?.Invoke(this, null);


		/// <summary>
		///     Any exception that was unhandled by the dialog itself.
		/// </summary>
		public Exception Exception { get; set; }

		#endregion

		#region -- Data --

		private readonly List<CalibrationTableVM> _tables;
		private ListCollectionView _tablesView;
		private bool _allowMultiSelect;

		#endregion
	}
}

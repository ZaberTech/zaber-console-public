﻿using System;
using System.Collections.ObjectModel;
using ZaberWpfToolbox;

namespace ZaberConsole.Dialogs.Calibration
{
	/// <summary>
	///     Class corresponding to a device calibration table.
	/// </summary>
	public class CalibrationTableVM : ObservableObject
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Data format version. Normally this should be 1. 0 is invalid, and
		///     values greater than 1 indicate a format change. Code should check this
		///     value to make sure it understands the format.
		/// </summary>
		public int DataVersion
		{
			get => _dataVersion;
			set => Set(ref _dataVersion, value, nameof(DataVersion));
		}


		/// <summary>
		///     Date the data was generated.
		/// </summary>
		public DateTime TimeStamp
		{
			get => _timeStamp;
			set => Set(ref _timeStamp, value, nameof(TimeStamp));
		}


		/// <summary>
		///     Peripheral serial number this data is intended for.
		/// </summary>
		public uint SerialNumber
		{
			get => _serialNumber;
			set => Set(ref _serialNumber, value, nameof(SerialNumber));
		}


		/// <summary>
		///     Type of calibration data:
		///     0 = not available
		///     1 = direct encoder
		///     2 = Lead screw
		/// </summary>
		public int CalibrationType
		{
			get => _calibrationType;
			set => Set(ref _calibrationType, value, nameof(CalibrationType));
		}


		/// <summary>
		///     Indication of the quantity of calibration data.
		/// </summary>
		public int TableWidth
		{
			get => _tableWidth;
			set => Set(ref _tableWidth, value, nameof(TableWidth));
		}


		/// <summary>
		///     Observable property for views to sync TableOffset.
		/// </summary>
		public int TableOffset
		{
			get => _tableOffset;
			set => Set(ref _tableOffset, value, nameof(TableOffset));
		}


		/// <summary>
		///     Human-readable description of this data table - ie who made it, what for etc.
		/// </summary>
		public string Description
		{
			get => _description;
			set => Set(ref _description, value, nameof(Description));
		}


		/// <summary>
		///     The actual data table.
		/// </summary>
		public ObservableCollection<int> Data
		{
			get => _data;
			set => Set(ref _data, value, nameof(Data));
		}


		/// <summary>
		///     The file name that this table was loaded from, if any. This is just for display
		///     and will not contain the full path of the file.
		/// </summary>
		public string Filename
		{
			get => _filename;
			set => Set(ref _filename, value, nameof(Filename));
		}


		/// <summary>
		///     Whether or not this table is selected for an action. Used by user interface views.
		/// </summary>
		public bool IsSelected
		{
			get => _isSelected;
			set => Set(ref _isSelected, value, nameof(IsSelected));
		}

		#endregion

		#region -- Data --

		private int _dataVersion = 1;
		private DateTime _timeStamp = DateTime.Now;
		private uint _serialNumber;
		private int _calibrationType;
		private int _tableWidth;
		private int _tableOffset;
		private string _description = "Empty Table";
		private ObservableCollection<int> _data = new ObservableCollection<int>();
		private string _filename;
		private bool _isSelected;

		#endregion
	}
}

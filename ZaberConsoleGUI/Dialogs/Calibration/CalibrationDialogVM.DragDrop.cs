﻿using System.Windows;
using GongSolutions.Wpf.DragDrop;

namespace ZaberConsole.Dialogs.Calibration
{
	public partial class CalibrationDialogVM
	{
		#region -- Public data --

		public class DeviceTableDropHandler : IDropTarget
		{
			#region -- Public Methods & Properties --

			public DeviceTableDropHandler(CalibrationDialogVM aVm)
			{
				_vm = aVm;
			}


			public void DragOver(IDropInfo aDropInfo)
			{
				aDropInfo.Effects = DragDropEffects.None;
				var obj = aDropInfo.Data as IDataObject;
				if ((null != obj) && obj.GetDataPresent(DataFormats.FileDrop))
				{
					var files = (string[]) obj.GetData(DataFormats.FileDrop);
					if ((null != files) && (files.Length > 0))
					{
						aDropInfo.Effects = DragDropEffects.Copy;
					}
				}
			}


			public void Drop(IDropInfo aDropInfo)
			{
				var obj = aDropInfo.Data as IDataObject;
				if ((null != obj)
				&& obj.GetDataPresent(DataFormats.FileDrop)
				&& (aDropInfo.TargetCollection == _vm.DeviceTables))
				{
					var files = (string[]) obj.GetData(DataFormats.FileDrop);
					_vm.LoadTablesFromFiles(files);
				}
			}

			#endregion

			#region -- Data --

			private readonly CalibrationDialogVM _vm;

			#endregion
		}

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using Zaber;
using ZaberWpfToolbox;

namespace ZaberConsole
{
	/// <summary>
	///     Used to poll one or all devices at regular intervals, and pass the responses
	///     to a user-defined callback.
	/// </summary>
	public class DevicePoller
	{
		/// <summary>
		///     Method signature for the poll response handler. 
		/// </summary>
		/// <param name="aSender">The DevicePoller invoking the event.</param>
		/// <param name="aConversation">The Conversation being polled.</param>
		/// <param name="aResponses">Replies to the poll message. There may be more than
		/// one if multiple devices are being polled. Note that collecting info responses
		/// is currently not supported.</param>
		public delegate void PollResponseHandler(object aSender, Conversation aConversation, IEnumerable<DeviceMessage> aResponses);

		/// <summary>
		///    Event handler for poll responses. Note this will typically be
		///	   invoked on a random thread.
		/// </summary>
		public event PollResponseHandler PollResponseReceived;


		/// <summary>
		///     Initialize the polling with a device to be polled. This does not
		///     initiate polling.
		/// </summary>
		/// <param name="aConversation">Device to poll. May be a ConversationCollection.</param>
		public DevicePoller(Conversation aConversation)
		{
			_conversation = aConversation;
			_timer = DispatcherTimerHelper.CreateTimer();
			_timer.Tick += _timer_Tick;
			TimeoutTimer = new TimeoutTimer { Timeout = 5000 };
		}

		/// <summary>
		///     Accessor to retrieve or change the conversation assigned to this poller.
		///     It is only safe to change the Conversation while the poller is disabled.
		/// </summary>
		public Conversation Conversation
		{
			get => _conversation;
			set
			{
				if (Enabled)
				{
					throw new InvalidOperationException("Changing the Conversation while a poller's timer is enabled is not safe.");
				}

				_conversation = value;
			}
		}


		/// <summary>
		///     ASCII command to send to the device every poll interval. This should
		///     not include the message header - just the command payload.
		/// </summary>
		public string Command { get; set; }


		/// <summary>
		///     Polling interval. Can be changed any time. No bounds checking is done,
		///     but the poller will not send another command until the response to the
		///     previous one has been dealt with.
		/// </summary>
		public TimeSpan Interval
		{
			get => _timer.Interval;
			set => _timer.Interval = value;
		}


		/// <summary>
		///     Start or stop the polling.
		/// </summary>
		public bool Enabled
		{
			get => _enabled;
			set
			{
				if (_enabled != value)
				{
					_timer.IsEnabled = value;
					if (value)
					{
						_reportedExceptions.Clear();
						_mutexLockFailuresSequential = 0;
					}
				}

				_enabled = value;
			}
		}


		/// <summary>
		///     If set, the poller will use the same message ID for all polling commands.
		///     If not set, it will use a new Conversation-assigned topic and message ID
		///     for each poll event.
		/// </summary>
		public byte? ReservedMessageId { get; set; }


		/// <summary>
		///     Timeout timer for poll messages. The poller does not count timeouts
		///     as errors but will log them in the application log. The default timeout
		///     is infinite, meaning communication failure will stall polling.
		/// </summary>
		public TimeoutTimer TimeoutTimer { get; set; }


		/// <summary>
		///     Maximum time to wait for other pollers to complete before giving up
		///     and skipping a poll event. Defaults to one second.
		/// </summary>
		public TimeSpan ContentionTimeout { get; set; } = TimeSpan.FromSeconds(1);


		/// <summary>
		///     Manually trigger a poll if one is not already in progress. If you want to
		///     wait for an in-progress poll to finish first, also use <see cref="WaitForPollComplete"/>.
		///	    Note this will restart the polling timer if it is enabled.
		/// </summary>
		public void PollNow()
		{
			lock (_syncRoot)
			{
				_timer.IsEnabled = false;

				if (_task is null)
				{
					_task = Task.Factory.StartNew(Poll);
				}
			}
		}


		/// <summary>
		///     Block until any poll currently in progress finishes.
		/// </summary>
		/// <param name="aTimeout">Maximum time to wait, or -1 milliseconds to wait forever.</param>
		/// <returns>True if the poll task completed; false if the timeout occurred.</returns>
		public bool WaitForPollComplete(TimeSpan aTimeout)
		{
			return _task?.Wait(aTimeout) ?? true;
		}


		private void _timer_Tick(object aSender, EventArgs aArgs)
		{
			PollNow();
		}


		// This exists for testing.
		private IDispatcherTimer GetTimer() => _timer;


		private void Poll()
		{
			// Helps but doesn't completely solve the race condition around port closing.
			if (!_conversation.Device.Port.IsOpen || !_conversation.Device.Port.IsAsciiMode)
			{
				return;
			}

			// Only allow one poller to send at a time.
			// For now this is a global mutex over all pollers because we are not
			// expecting to have many of them. If this becomes a performance problem
			// we can switch to per-device locking, but special handling of All Devices
			// will be needed.
			if (_mutex.WaitOne(ContentionTimeout))
			{
				_mutexLockFailuresSequential = 0;

				try
				{
					var mainTopic = ReservedMessageId.HasValue ? _conversation.StartTopic(ReservedMessageId.Value) : _conversation.StartTopic();
					_log.Debug($"Sending \"{Command}\" to device {_conversation.Device.DeviceNumber} {_conversation.Device.AxisNumber} with message ID {mainTopic.MessageId}.");
					_conversation.Device.Send(Command, mainTopic.MessageId);

					if (mainTopic.Wait(TimeoutTimer))
					{
						// We do not validate the topic because some of the responses
						// can be errors - for example if a device doesn't support
						// the command we are polling with.

						var allTopics =
							(mainTopic is ConversationTopicCollection topics)
								? topics.ToList()
								: new List<ConversationTopic> {mainTopic};

						var responses = new List<DeviceMessage>();

						var timeouts = 0;
						foreach (var topic in allTopics)
						{
							// Ignore replies to commands that pre-empted the poll command.
							if ((topic.ReplacementResponse is null) && (null != topic.Response))
							{
								responses.Add(topic.Response);
								_log.Debug($"Received reply: {topic.Response.FormatResponse()}");
							}
							else if (topic.ZaberPortError == ZaberPortError.ResponseTimeout)
							{
								timeouts++;
							}
							else
							{
								_log.Debug("Received a non-response.");
							}
						}

						if (timeouts > 0)
						{
							var msg = $"{timeouts} devices timed out while broadcast polling command '{Command}'.";
							ReportAndAggregateLogMessage(msg, _log.Warn);
						}

						if (responses.Count > 0)
						{
							_log.Debug("Invoking poll response callback.");
							PollResponseReceived?.Invoke(this, _conversation, responses);
						}
					}
					else
					{
						var msg = $"Timeout when polling device {Conversation.Device.DeviceNumber} with command '{Command}'.";
						ReportAndAggregateLogMessage(msg, _log.Warn);
					}
				}
				catch (Exception e)
				{
					ReportAndAggregateLogMessage(e.ToString(), _log.Error);

					// Eat exceptions to prevent possibly temporary conditions from
					// halting the polling tasks.
				}
				finally
				{
					_mutex.ReleaseMutex();
				}
			}
			else
			{
				var msg = $"Device poller for command '{Command}' failed to obtain mutex.";
				ReportAndAggregateLogMessage(msg, _log.Warn);

				_mutexLockFailuresSequential++;
				if (_mutexLockFailuresSequential >= 5)
				{
					_log.Warn($"Device poller for command '{Command}' failed to obtain mutex {_mutexLockFailuresSequential} times in a row.");
					_mutexLockFailuresSequential = 0;
				}
			}

			lock (_syncRoot)
			{
				_task = null;
				if (_enabled)
				{
					// Re-enable the timer after the poll is complete, to avoid
					// overlap if the device is very busy.
					_timer.IsEnabled = true;
				}
			}
		}


		private void ReportAndAggregateLogMessage(string aMessage, Action<string> aLogCallback)
		{
			if (_log.IsDebugEnabled)
			{
				_log.Debug(aMessage); // Log every occurrence at debug level.
				return;
			}

			if (!_reportedExceptions.ContainsKey(aMessage))
			{
				_reportedExceptions.Add(aMessage, 0);
				aLogCallback(aMessage + " (subsequent duplicates will accumulate)");
			}
			else
			{
				var count = _reportedExceptions[aMessage] + 1;
				if (count >= 20)
				{
					aLogCallback($"{aMessage} (has been repeated {count} times)");
					count = 0;
				}

				_reportedExceptions[aMessage] = count;
			}
		}


		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private static Mutex _mutex = new Mutex();

		private readonly IDispatcherTimer _timer;
		private readonly object _syncRoot = new object();
		private Conversation _conversation;
		private Dictionary<string, int> _reportedExceptions = new Dictionary<string, int>();
		private bool _enabled;
		private Task _task;
		private int _mutexLockFailuresSequential;
	}
}

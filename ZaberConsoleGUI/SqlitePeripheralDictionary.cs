﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using Zaber;

namespace ZaberConsole
{
	public class SqlitePeripheralDictionary : SqliteDictionary, IDictionary<int, DeviceType>
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Intended to quietly serve as the PeripheralMap for a ZaberDevice,
		///     this class lazily fetches data from the SQLite database.
		/// </summary>
		/// <param name="aFilename">The name of the SQLite database file.</param>
		/// <param name="aDeviceId">The device ID of the parent device.</param>
		/// <param name="aFirmwareVersion">
		///     The firmware version of the parent
		///     device.
		/// </param>
		public SqlitePeripheralDictionary(string aFilename, int aDeviceId, FirmwareVersion aFirmwareVersion, FirmwareVersion realFirmwareVersion)
			: base(aFilename)
		{
			_deviceId = aDeviceId;
			_firmwareVersion = aFirmwareVersion;
			_realFirmwareVersion = realFirmwareVersion;
			_cache = new Dictionary<int, DeviceType>();

			using (var cmd = new SQLiteCommand("SELECT Id, Name FROM Console_Devices WHERE "
										   + $"DeviceId = {_deviceId} AND "
										   + $"MajorVersion = {_firmwareVersion.Major} AND "
										   + $"MinorVersion = {_firmwareVersion.Minor} AND "
										   + $"Build = {_firmwareVersion.Build}",
											   _connection))
			{
				var success = false;
				using (var reader1 = cmd.ExecuteReader())
				{
					if (reader1.HasRows)
					{
						success = true;
						reader1.Read();

						_parentId = (int) (long) reader1["Id"];
						_parentName = (string) reader1["Name"];
					}
				}

				if (!success)
				{
					// If this specific firmware version (including the build number)
					// is not in the database, fall back to not using the build number.
					using (var cmd2 = new SQLiteCommand("SELECT Id, Name FROM Console_Devices WHERE "
													+ $"DeviceId = {_deviceId} AND "
													+ $"MajorVersion = {_firmwareVersion.Major} AND "
													+ $"MinorVersion = {_firmwareVersion.Minor} "
													+ "ORDER BY Build DESC LIMIT 1",
														_connection))
					{
						using (var reader2 = cmd2.ExecuteReader())
						{
							reader2.Read();
							_parentId = (int) (long) reader2["Id"];
							_parentName = (string) reader2["Name"];
						}
					}
				}
			}
		}


		public void Clear() => _cache.Clear();


		public void Add(KeyValuePair<int, DeviceType> aItem)
			=> throw new NotSupportedException("Collection is read-only.");


		public void Add(int aKey, DeviceType aValue)
			=> throw new NotSupportedException("Collection is read-only.");


		public bool Contains(KeyValuePair<int, DeviceType> aItem) => ContainsKey(aItem.Key);


		public bool ContainsKey(int aKey)
		{
			using (var peripheralCmd = new SQLiteCommand("SELECT Id FROM Console_Peripherals WHERE "
													 + $"ParentId = {_parentId} AND PeripheralId = {aKey}",
														 _connection))
			{
				return peripheralCmd.ExecuteScalar() != null;
			}
		}


		public void CopyTo(KeyValuePair<int, DeviceType>[] aDest, int aIndex)
			=> throw new NotImplementedException();


		public IEnumerator<KeyValuePair<int, DeviceType>> GetEnumerator()
		{
			using (var cmd = new SQLiteCommand("SELECT * FROM Console_Peripherals WHERE "
										   + $"ParentId = {_parentId}",
											   _connection))
			{
				using (var reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						var key = (int) (long) reader["PeripheralId"];

						yield return new KeyValuePair<int, DeviceType>(key, this[key]);
					}
				}
			}
		}


		public bool Remove(KeyValuePair<int, DeviceType> aItem)
			=> throw new NotSupportedException("Collection is read-only.");


		public bool Remove(int aKey)
			=> throw new NotSupportedException("Collection is read-only.");


		public bool TryGetValue(int aKey, out DeviceType aValue)
		{
			try
			{
				aValue = this[aKey];
				return true;
			}
			catch (KeyNotFoundException)
			{
				aValue = null;
				return false;
			}
		}


		public DeviceType this[int aKey]
		{
			get
			{
				if (_cache.ContainsKey(aKey))
				{
					// Return a copy so the original cannot be modified by user code.
					return new DeviceType(_cache[aKey]);
				}

				string productId = null;
				DeviceType peripheralType = null;

				using (var peripherals = SelectAndRead("Id, Name",
													   "Console_Peripherals",
													   "ParentId = {0} AND PeripheralId = {1}",
													   _parentId,
													   aKey))
				{
					if (!peripherals.Read())
					{
						throw new KeyNotFoundException();
					}

					peripheralType = new DeviceType
					{
						DeviceId = _deviceId,
						FirmwareVersion = _firmwareVersion,
						RealFirmwareVersion = _realFirmwareVersion,
						Name = _parentName + " + " + (string) peripherals["Name"],
						PeripheralId = aKey
					};

					productId = ((long) peripherals["Id"]).ToString();
				}

				var commands = GetCommandList(productId);

				// These commands are for a peripheral so set the IsAxisCommand flag.
				foreach (var cmd in commands)
				{
					cmd.IsAxisCommand = true;
				}

				peripheralType.Commands = commands;

				var converters = GetProductUnitConversions(productId, peripheralType.Commands);
				peripheralType.MotionType =
					DetermineMotionType(converters.Select(c => c.ReferenceUnit.Dimension), peripheralType.Commands);
				peripheralType.UnitConverter.Parent = UnitConverter;
				foreach (var converter in converters.Where(c => c.Scale.HasValue))
				{
					peripheralType.DeviceTypeUnitConversionInfo[converter.ReferenceUnit.Dimension] = converter;
				}

				peripheralType.TuningData = PopulateTuningData(productId);
				peripheralType.Capabilities = ReadCapabilities(productId);

				// Cache it for next time.
				_cache[aKey] = peripheralType;

				// Return a copy to prevent cached one from being modified by the caller.
				return new DeviceType(peripheralType);
			}

			set => throw new NotSupportedException("Collection is read-only.");
		}


		public int Count
		{
			get
			{
				using (var cmd = new SQLiteCommand("SELECT count(*) FROM Console_Peripherals WHERE "
											   + $"ParentId = {_parentId}",
												   _connection))
				{
					return (int) (long) cmd.ExecuteScalar();
				}
			}
		}


		public ICollection<int> Keys
		{
			get
			{
				using (var peripheralsReader = SelectAndRead("PeripheralId",
															 "Console_Peripherals",
															 "ParentId = {0}",
															 _parentId))
				{
					var keys = new List<int>();
					while (peripheralsReader.Read())
					{
						keys.Add((int) (long) peripheralsReader["PeripheralId"]);
					}

					return keys;
				}
			}
		}


		public ICollection<DeviceType> Values => Keys.Select(key => this[key]).ToList();

		#endregion

		#region -- Interfaces --

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

		#endregion

		#region -- Data --

		private readonly Dictionary<int, DeviceType> _cache;
		private readonly int _deviceId;
		private readonly FirmwareVersion _firmwareVersion;
		private readonly FirmwareVersion _realFirmwareVersion;
		private readonly int _parentId;
		private readonly string _parentName;

		#endregion
	}
}

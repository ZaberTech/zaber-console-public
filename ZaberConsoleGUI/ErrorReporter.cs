﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using log4net;
using Microsoft.Win32;
using Zaber;
using Zaber.Telemetry;
using ZaberConsole.Properties;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Dialogs;
using ZaberWpfToolbox.Dialogs.ErrorReporter;
using Application = System.Windows.Application;

namespace ZaberConsole
{
	internal class ErrorReporter : IDialogClient
	{
		#region -- Setup --

		public ErrorReporter()
		{
			ErrorList = new List<Exception>();
		}


		public ErrorReporter(ICollection<Exception> aExceptions)
		{
			ErrorList = aExceptions;
		}

		#endregion

		#region -- Public properties --

		public ErrorReportInformation Information
		{
			get
			{
				var eri = new ErrorReportInformation
				{
					// User-provided error description and email are added after returning from the dialog
					{ ErrorReportItems.Item.Date, Date },
					{ ErrorReportItems.Item.ProgramName, Configuration.ProgramName },
					{ ErrorReportItems.Item.ProgramVersion, ProgramVersion },
					{ ErrorReportItems.Item.Guid, MachineGuid },
					{ ErrorReportItems.Item.OperatingSystem, OperatingSystem },
					{ ErrorReportItems.Item.Screen, ScreenDimension },
					{ ErrorReportItems.Item.Devices, Devices },
					{ ErrorReportItems.Item.PortMode, PortMode },
					{ ErrorReportItems.Item.OpenTabs, OpenTabs },
					{ ErrorReportItems.Item.ActiveTab, ActiveTab },
					{ ErrorReportItems.Item.Memory, TotalPhysicalMemory },
					{ ErrorReportItems.Item.Language, Language },
					{ ErrorReportItems.Item.Installer, Installer },
					{ ErrorReportItems.Item.Exception, ExceptionDetails(ErrorList) },
					{ ErrorReportItems.Item.Assemblies, Assemblies },
					{ ErrorReportItems.Item.MessageLog, MessageLog },
					{ ErrorReportItems.Item.ApplicationLog, ApplicationLog },
					{ ErrorReportItems.Item.ApplicationSettings, ApplicationSettings },
					{ ErrorReportItems.Item.UserSettings, UserSettings }
				};

				eri.ErrorConditions = ErrorList?.Select(ex => ex.Message);

				return eri;
			}
		}

		public event RequestDialogChange RequestDialogOpen;
		#pragma warning disable CS0067 // Event is never used.
		public event RequestDialogChange RequestDialogClose;
		#pragma warning restore CS0067

		#endregion

		#region -- Public methods --

		public void ShowErrorReportDialog()
		{
			var triggeredByException = 0 < ErrorList.Count;
			var errorReportDialog = new ErrorReportDialogVM(Information, triggeredByException);

			// Errors that occur within Zaber will have the user's identity by default so we can follow up.
			#if DEV
			errorReportDialog.Email = Environment.UserName;
			#endif

			errorReportDialog.SaveErrorReport += SaveErrorReport;
			RequestDialogOpen.Invoke(this, errorReportDialog);
		}


		public static void SubmitErrorReportsAsync()
		{
			if (!_onlineMode)
			{
				return;
			}

			new Thread(() =>
			{
				var files = Directory.GetFiles(REPORT_QUEUE_DIRECTORY);
				foreach (var file in files)
				{
					if (!Path.GetExtension(file).Equals(".json") || TempFileManager.FileStagedForDeletion(file))
					{
						continue;
					}

					var success = PostJsonToServer(ERROR_REPORT_URI, File.ReadAllText(file));
					if (success)
					{
						TempFileManager.RegisterForDelete(file);
						TempFileManager.TryDeleteFiles();
					}
				}
			}) { Name = "Deferred error report filer thread" }.Start();
		}


		public static void SaveErrorReport(object aSender, SaveErrorReportEventArgs aEventArgs)
		{
			var reportFile = Path.Combine(REPORT_QUEUE_DIRECTORY, $"{RandomString(32)}.json");
			var report = aEventArgs.Information.Anonymize().ToJson();
			using (var streamWriter = File.CreateText(reportFile))
			{
				streamWriter.WriteLine(report);
			}

			SubmitErrorReportsAsync();
		}

		#endregion

		#region -- Private helper methods --

		private static bool PostJsonToServer(Uri aUri, string aJson)
		{
			var request = (HttpWebRequest) WebRequest.Create(aUri);
			request.ContentType = "application/json";
			request.Method = "POST";

			try
			{
				using (var streamWriter = new StreamWriter(request.GetRequestStream()))
				{
					streamWriter.Write(aJson);
				}

				request.GetResponse();
				_eventLog.LogEvent("Error report submitted");
				return true;
			}
			catch (WebException aException)
			{
				_log.Warn("Failed to transmit an error report.", aException);
				return false;
			}
		}


		private string FileContents(string path)
		{
			try
			{
				return Anonymize(File.ReadAllText(path));
			}
			catch (FileNotFoundException)
			{
				return $"{Anonymize(path)} does not exist";
			}
			catch
			{
				return _couldNotRetrieveInfo;
			}
		}


		private static string RandomString(int length)
		{
			var random = new Random();
			const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
			return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
		}


		private static string Anonymize(string str) => str.Replace(Environment.UserName, "username");


		private string ProcessInfo(string key)
		{
			var query = new SelectQuery(@"Select * from Win32_ComputerSystem");

			using (var searcher = new ManagementObjectSearcher(query))
			{
				var processes = new List<string>();
				foreach (ManagementObject process in searcher.Get())
				{
					process.Get();
					processes.Add(process[key].ToString());
				}

				return string.Join(", ", processes);
			}
		}


		private static string ExceptionDetails(IEnumerable<Exception> aExceptions)
		{
			var msg = new StringBuilder();
			foreach (var ex in aExceptions)
			{
				msg.AppendFormat("{0}: {1}\n{2}",
								 ex.GetType().Name,
								 ex.Message ?? string.Empty,
								 ex.StackTrace ?? string.Empty);
				msg.AppendLine();
				var inner = ex.InnerException;
				while (null != inner)
				{
					msg.AppendFormat("Inner {0}: {1}\n{2}",
									 inner.GetType().Name,
									 inner.Message ?? string.Empty,
									 inner.StackTrace ?? string.Empty);
					msg.AppendLine();
					inner = inner.InnerException;
				}
			}

			return Anonymize(msg.ToString());
		}

		#endregion

		#region -- Report items --

		private string Date
		{
			get
			{
				try
				{
					return DateTime.Now.ToLocalTime().ToString("yyyy-MM-dd HH:mm:sszzz");
				}
				catch
				{
					return _couldNotRetrieveInfo;
				}
			}
		}


		private readonly ICollection<Exception> ErrorList;


		private string MachineGuid
		{
			get
			{
				try
				{
					return Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Cryptography")
								.GetValue("MachineGuid")
								.ToString();
				}
				catch
				{
					return _couldNotRetrieveInfo;
				}
			}
		}


		private string OperatingSystem
		{
			get
			{
				try
				{
					return Environment.OSVersion.VersionString;
				}
				catch
				{
					return _couldNotRetrieveInfo;
				}
			}
		}


		private string ProgramVersion
		{
			get
			{
				try
				{
					return Assembly.GetEntryAssembly().GetName().Version.ToString();
				}
				catch
				{
					return _couldNotRetrieveInfo;
				}
			}
		}


		private string TotalPhysicalMemory
		{
			get
			{
				try
				{
					return ProcessInfo("TotalPhysicalMemory");
				}
				catch
				{
					return _couldNotRetrieveInfo;
				}
			}
		}


		private string Language
		{
			get
			{
				try
				{
					return CultureInfo.CurrentUICulture.Name;
				}
				catch
				{
					return _couldNotRetrieveInfo;
				}
			}
		}


		private string Installer => "Inno Setup (only)";


		private string Assemblies
		{
			get
			{
				try
				{
					var sb = new StringBuilder();
					var helper = new AssemblyInfoHelper();
					foreach (var info in helper.PopulateAssemblies())
					{
						sb.AppendFormat("{0} {1}, built {2}\n", info.AssemblyName, info.Version, info.BuildDate);
					}

					return sb.ToString();
				}
				catch
				{
					return _couldNotRetrieveInfo;
				}
			}
		}


		private string ScreenDimension
		{
			get
			{
				try
				{
					return string.Join(", ",
									   Screen.AllScreens.Select(
										   screen => $"{screen.Bounds.Width}x{screen.Bounds.Height}"));
				}
				catch
				{
					return _couldNotRetrieveInfo;
				}
			}
		}


		private string MessageLog
		{
			get
			{
				try
				{
					return _mainWindowVM.Terminal.Text;
				}
				catch
				{
					return _couldNotRetrieveInfo;
				}
			}
		}


		private string ApplicationLog
		{
			get
			{
				try
				{
					var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
											"Zaber Technologies",
											"Zaber Console");

					var logFile = Path.Combine(path, "ZaberConsole.log");

					Directory.CreateDirectory(path);
					return File.Exists(logFile) ? FileContents(logFile) : string.Empty;
				}
				catch
				{
					return _couldNotRetrieveInfo;
				}
			}
		}


		private string UserSettings
		{
			get
			{
				try
				{
					return FileContents(ConfigurationManager
									.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal)
									.FilePath);
				}
				catch
				{
					return _couldNotRetrieveInfo;
				}
			}
		}


		private string ApplicationSettings
		{
			get
			{
				try
				{
					return FileContents(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
				}
				catch
				{
					return _couldNotRetrieveInfo;
				}
			}
		}


		private string PortMode
		{
			get
			{
				try
				{
					return _mainWindowVM.DeviceList.IsAscii ? "Ascii" : "Binary";
				}
				catch
				{
					return _couldNotRetrieveInfo;
				}
			}
		}


		private string Devices
		{
			get
			{
				try
				{
					var deviceStrings = new List<string>();
					var devs = new List<ZaberDevice>();
					var selectedDevice = _mainWindowVM.PortFacade.SelectedConversation?.Device;

					foreach (var device in _mainWindowVM.PortFacade.Devices)
					{
						if ((0 == device.AxisNumber)
						&& (null != device.DeviceType.PeripheralMap)
						&& (device.Axes.Count == 1))
						{
							devs.Add(device.Axes[0]);
							if (device == selectedDevice)
							{
								selectedDevice = device.Axes[0];
							}
						}
						else
						{
							devs.Add(device);
							if (device.Axes.Count > 0)
							{
								foreach (var axis in device.Axes)
								{
									devs.Add(axis);
								}
							}
						}
					}

					foreach (var device in devs)
					{
						var str = $"{device.DeviceType.Name ?? "<null>"} ({device.FirmwareVersion.ToString()})";
						if (device == selectedDevice)
						{
							str += " (selected)";
						}

						deviceStrings.Add(str);
					}

					return string.Join(", ", deviceStrings);
				}
				catch
				{
					return _couldNotRetrieveInfo;
				}
			}
		}


		private string ActiveTab
		{
			get
			{
				try
				{
					return _mainWindowVM.PluginManager.ActiveTab.TabName;
				}
				catch
				{
					return _couldNotRetrieveInfo;
				}
			}
		}


		private string OpenTabs
		{
			get
			{
				try
				{
					return string.Join(", ", _mainWindowVM.PluginManager.Tabs.Select(tab => tab.TabName));
				}
				catch
				{
					return _couldNotRetrieveInfo;
				}
			}
		}

		#endregion

		#region -- Private member data --

		private static bool _onlineMode => Settings.Default.AllowNetworkAccess;


		private static string REPORT_QUEUE_DIRECTORY
		{
			get
			{
				var directory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
											 @"Zaber Technologies",
											 @"Zaber Console",
											 @"Error Reports");

				Directory.CreateDirectory(directory);

				return directory;
			}
		}


		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Error report dialog");

		private static readonly string _couldNotRetrieveInfo = "Could not retrieve information";
		private readonly MainWindowVM _mainWindowVM = Application.Current.MainWindow?.DataContext as MainWindowVM;


		#if (DEV || TEST)

		// Internal error report URI.
		private static readonly Uri ERROR_REPORT_URI = new Uri("https://fwee-firmware.izaber.com:6174/errors");
#else

		// Public error report URI.
		private static readonly Uri ERROR_REPORT_URI = new Uri("https://www.zaber.com/support/error-report.php");
		#endif

		#endregion
	}
}

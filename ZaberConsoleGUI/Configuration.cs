﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using log4net;

namespace ZaberConsole
{
	/// <summary>
	///     Container for build configuration specific global data about the program itself,
	///     such as important filenames and paths.
	/// </summary>
	public static class Configuration
	{
		/// <summary>
		///     Human-readable display name for the program.
		/// </summary>
		#if DEV
		#if NIGHTLY
		public static readonly string ProgramName = "Zaber Console (Nightly DEV)";
		#else
		public static readonly string ProgramName = "Zaber Console (DEV)";
		#endif
		#elif INSTALLTEST
		public static readonly string ProgramName = "Zaber Console (Install Test)";
		#elif ALPHA
		public static readonly string ProgramName = "Zaber Console (Alpha)";
		#else
		public static readonly string ProgramName = "Zaber Console";
		#endif

		/// <summary>
		///     Where the program should store data files it saves between sessions, such as
		///     the device database and window layout.
		/// </summary>
		public static readonly string AppDataPath = Path.Combine(
			Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
			"Zaber Technologies",
			"Zaber Console");

		/// <summary>
		///     Directory where the executable and its bundled files are installed.
		/// </summary>
		public static readonly string ProgramInstallDir = AppDomain.CurrentDomain.BaseDirectory;

		/// <summary>
		///     Base filename of the downloaded database.
		/// </summary>
		#if (DEV || TEST)
		public static readonly string DatabaseFilename = "devices_DEV.sqlite";
		#else
		public static readonly string DatabaseFilename = "devices.sqlite";
		#endif

		/// <summary>
		///     Path and filename of the database bundled with the program.
		/// </summary>
		public static readonly string BundledDatabasePath = Path.Combine(ProgramInstallDir, "devices.sqlite");

		/// <summary>
		///     Download URL for the latest database.
		/// </summary>
		#if (DEV || TEST)
		public static readonly string DatabaseUrl = 
			"https://zshared.izaber.com/device-database/devices-master.sqlite.lzma";
		#else
		public static readonly string DatabaseUrl =
			"https://www.zaber.com/software/device-database/devices-public.sqlite.lzma";
		#endif

		/// <summary>
		///     Base URL for the software finder API on Zaber's website
		/// </summary>
		public static readonly string SoftwareFinderUrl = "https://api.zaber.io/downloads/software-finder";

		/// <summary>
		///     Base URL for the software download server API on Zaber's website
		/// </summary>
		public static readonly string SoftwareDownloadUrl = "https://api.zaber.io/downloads/software-downloads";

		/// <summary>
		///     Software finder/downloader API ID for this program.
		/// </summary>
		#if DEV
		#if NIGHTLY
		public static readonly string SoftwareFinderId = "zaber_console_nightly_dev_installer";
		#else
		public static readonly string SoftwareFinderId = "zaber_console_dev_InnoSetup_installer";
		#endif
		#elif ALPHA
		public static readonly string SoftwareFinderId = "zaber_console_Alpha_InnoSetup_installer";
		#elif TEST
		public static readonly string SoftwareFinderId = "zaber_console_InstallTest_InnoSetup_installer";
		#else
		public static readonly string SoftwareFinderId = "zaber_console_InnoSetup_installer";
		#endif


		/// <summary>
		///     Generates a URL to use with the Software Finder on the Zaber website to
		///     get available version numbers of a software product.
		/// </summary>
		/// <param name="aProductId">
		///     Finder's product ID for the software.
		///     These are strings hardcoded in the software finder itself.
		/// </param>
		/// <returns>The URL to use to fetch available versions.</returns>
		public static string GetFinderVersionQueryUrl(string aProductId)
			=> $"{SoftwareDownloadUrl}?product={aProductId}";


		/// <summary>
		///     Generate a URL form which can be obtained the download URL for a specific
		///     version of a software product on the Zaber website. Doing a GET on this URL
		///     will return you some JSON containing the URL to use for downloading the
		///     actual software. This somewhat insulates software clients from changes in
		///     the way downloads are organized on the Zaber websute.
		/// </summary>
		/// <param name="aProductId">
		///     Finder's product ID for the software.
		///     These are strings hardcoded in the software finder itself.
		/// </param>
		/// <param name="aVersion">The specific version to download.</param>
		/// <returns>The URL to use to get the download URL.</returns>
		public static string GetDownloadUrlQueryUrl(string aProductId, string aVersion)
			=> $"{SoftwareDownloadUrl}?product={aProductId}&version={aVersion}&show_url=true";


		/// <summary>
		///     Base filename for this program's installer executable, without the version number or extension.
		/// </summary>
		#if DEV
		#if NIGHTLY
		public static readonly string InstallerBaseName = "InstallZaberConsoleNightlyDev";
		#else
		public static readonly string InstallerBaseName = "InstallZaberConsoleDev";
		#endif
		#elif ALPHA
		public static readonly string InstallerBaseName = "InstallZaberConsoleInternalAlpha";
		#elif TEST
		public static readonly string InstallerBaseName = "InstallZaberConsoleInstallTest";
		#else
		public static readonly string InstallerBaseName = "InstallZaberConsole";
		#endif


		/// <summary>
		///		Default user-agent header to use when making web requests. This is used to
		///		identify program version usage in the Zaber website logs.
		/// </summary>
		public static string UserAgent
		{
			get
			{
				var bitness = Environment.Is64BitOperatingSystem ? "Win64" : "Win32";
				return $"{ProgramName}/{ProgramVersion} ({Environment.OSVersion}; {bitness})";
			}
		}


		/// <summary>
		///     The version number of the program currently running.
		///     Returns null if there is an error getting the version number.
		/// </summary>
		public static Version ProgramVersion
		{
			get
			{
				if (_programVersion is null)
				{
					try
					{
						var assembly = Assembly.GetEntryAssembly();
						var fileVersionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);
						_programVersion = new Version(fileVersionInfo.ProductVersion);
					}
					catch (Exception aException)
					{
						_log.Error("Failed to read current program version.", aException);
					}
				}

				return _programVersion;
			}
		}


		/// <summary>
		/// Returns true if the given path points to something inside or below the program's
		/// install location. Does not check that the item actually exists; just that the
		/// path includes the directory where the executable is.
		/// </summary>
		/// <param name="aPath">Path to check for inclusion in the program install dir.</param>
		/// <returns>True if <paramref name="aPath"/> falls under the program install dir.</returns>
		public static bool IsInProgramInstallDir(string aPath)
		{
			try
			{
				return FileUtil.IsInOrBelowDirectory(ProgramInstallDir, aPath);
			}
			catch (Exception aException)
			{
				_log.Error($"Exception checking path relative to program: '{aPath}' vs '{ProgramInstallDir}'", aException);
			}

			return false;
		}


		/// <summary>
		/// Indicates that this build of the program is customer-facing rather than for internal use.
		/// </summary>
		#if DEV || TEST
		public static readonly bool IsPublicBuild = false;
		#else
		public static readonly bool IsPublicBuild = true;
		#endif

		static Configuration()
		{
			// Setting security protocol to TLS 1.2
			System.Net.ServicePointManager.SecurityProtocol = (System.Net.SecurityProtocolType)3072;
		}


		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private static Version _programVersion;
	}
}

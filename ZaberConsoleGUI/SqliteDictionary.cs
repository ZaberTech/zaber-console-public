﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Script.Serialization;

using log4net;

using Zaber;
using Zaber.Units;

using ZaberConsole.Help;
using ZaberConsole.Properties;

using HelpCacheKey = System.Tuple<int, int, Zaber.FirmwareVersion, bool>;
using Measurement = Zaber.Units.Measurement;

namespace ZaberConsole
{
	/// <summary>
	///     Holds behaviour common to both <see cref="SqlitePeripheralDictionary" /> and
	///     <see cref="SqliteDeviceDictionary" />.
	/// </summary>
	/// <remarks>
	///     This abstract class implements what parts of IDictionary it can without
	///     specifying the key type, as it differs between peripheral and device
	///     dictionaries. There isn't much, but it does try to DRY the code.
	/// </remarks>
	public abstract class SqliteDictionary : IDeviceHelpProvider, IDisposable
	{
		#region -- Public Methods & Properties --

		protected SqliteDictionary(string aFilename)
		{
			if (File.Exists(aFilename))
			{
				var connectionPath = aFilename;

				// Workaround for new connection parsing algorithm failing on network shares.
				// See http://system.data.sqlite.org/index.html/info/bbdda6eae2
				if (connectionPath.StartsWith(@"\\"))
				{
					connectionPath = connectionPath.Replace(@"\\", @"\\\\");
				}

				_connection = new SQLiteConnection($"Data Source={connectionPath};Version=3;Read Only=True");
			}
			else
			{
				throw new FileNotFoundException($"Could not find file {aFilename}");
			}

			_connection.Open();
			LoadUnitTable();
			LoadParamTable();
			FilePath = aFilename;
		}


		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}


		/// <summary>
		///     Retrieves a list of all user-visible binary commands, including
		///     settings as "set" and "return" commands. This is intended to provide
		///     help to a program user for mapping between command numbers and
		///     human-readable command names.
		/// </summary>
		/// <returns>A special list of binary commands and settings.</returns>
		public IList<CommandInfo> GetAdvancedBinaryCommands()
		{
			var commandList = new List<CommandInfo>();
			var commandsSeen = new HashSet<byte>();
			var found = false;

			// Get all binary commands uniquely by name in the database.
			// Note this query does not capture unit names, conversion functions and scales, but they are not needed in this list.
			try
			{
				var viewName = "Console_AllBinaryCommands2";
				viewName = ViewExists(viewName) ? viewName : "Console_AllBinaryCommands";
				using (var queryCommand = new SQLiteCommand($"SELECT * FROM {viewName} GROUP BY Name;", _connection))
				{
					using (var reader = queryCommand.ExecuteReader())
					{
						foreach (var command in PopulateBinaryCommands(reader))
						{
							if (!commandsSeen.Contains(command.Number))
							{
								commandsSeen.Add(command.Number);
								commandList.Add(command);
								found = true;
							}
						}
					}
				}
			}
			catch (SQLiteException)
			{
				// Expected in the fallback case. Remove try/catch as part of ticket #6468.
			}

			if (!found)
			{
				_log.Error("No binary commands were found in the database.");
			}

			// Get all of the binary settings the device has.
			// Note this query does not capture unit names, conversion functions and scales, but they are not needed in this list.
			found = false;

			try
			{
				var viewName = "Console_AllBinarySettings2";
				viewName = ViewExists(viewName) ? viewName : "Console_AllBinarySettings";
				using (var queryCommand = new SQLiteCommand($"SELECT * FROM {viewName} WHERE Name NOT NULL GROUP BY Name;", _connection))
				{
					using (var reader = queryCommand.ExecuteReader())
					{
						foreach (var command in PopulateBinarySettings(reader, true))
						{
							if (!commandsSeen.Contains(command.Number))
							{
								commandsSeen.Add(command.Number);

								if (command is ReadOnlySettingInfo)
								{
									command.Name = "Return " + command.Name;
								}
								else
								{
									command.Name = "Set " + command.Name;
								}

								commandList.Add(command);
								found = true;
							}
						}
					}
				}
			}
			catch (SQLiteException)
			{
				// Expected in the fallback case. Remove try/catch as part of ticket #6468.
			}

			if (!found)
			{
				_log.Error("No binary settings were found in the database.");
			}

			return commandList;
		}


		#region -- IDeviceHelpProvider implementation --

		/// <inheritdoc cref="IDeviceHelpProvider.GetHelpForDevice" />
		public IDeviceHelp GetHelpForDevice(ZaberDevice aDevice, bool aAsciiMode)
		{
			var devType = aDevice?.DeviceType;
			if (null == devType)
			{
				return null;
			}

			// Note for now all device help is only variable at the device type level.
			// If that ever changes to incorporate live data from the devices, we will
			// no longer be able to cache these things. At time of writing this is
			// thought unlikely because the website protocol manual doesn't have
			// access to live devices.
			var key = new HelpCacheKey(devType.DeviceId, devType.PeripheralId, devType.FirmwareVersion, aAsciiMode);
			if (!_deviceHelpCache.TryGetValue(key, out var mapper))
			{
				try
				{
					if (!App.IsIeVersionSupported)
					{
						mapper = new NotSupportedHelpMapper(aDevice.DeviceType.FirmwareVersion,
															aAsciiMode ? Protocol.ASCII : Protocol.Binary);
					}
					else if (aAsciiMode)
					{
						mapper = CreateAsciiHelpMapper(devType, Settings.Default.AllowNetworkAccess);
					}
					else // Binary
					{
						mapper = CreateBinaryHelpMapper(devType, Settings.Default.AllowNetworkAccess);
					}

					_deviceHelpCache[key] = mapper;
				}
				catch (Exception aException)
				{
					_log.Error(string.Format("There was an error creating a help provider for {0} device {1}.",
											 aAsciiMode ? "ASCII" : "Binary",
											 aDevice.Description),
							   aException);
				}
			}

			return mapper;
		}

		#endregion


		public ConversionTable UnitConverter { get; private set; }


		public bool IsReadOnly => true;


		/// <summary>
		///     The path to the file opened by this instance.
		/// </summary>
		public string FilePath { get; }

		#endregion

		#region -- Non-Public Methods --

		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				_connection.Close();
				_connection.Dispose();
			}
		}


		/// <summary>
		///     Quickly get a list of peripheral IDs and names supported by a given controller.
		///     This data is also available via <see cref="SqliteDeviceDictionary.Values" />
		///     but listing them all that way is very expensive.
		/// </summary>
		/// <param name="aDeviceId">Device ID of a controller or integrated device.</param>
		/// <param name="aFirmwareVersion">Firmware version of the device.</param>
		/// <returns>
		///     Zero or more peripheral IDs and names supported by the controller.
		///     The list includes the Safe Mode peripheral with ID 0.
		/// </returns>
		internal IEnumerable<Tuple<int, string>> GetSupportedPeripheralNames(
			int aDeviceId, FirmwareVersion aFirmwareVersion)
		{
			var result = new List<Tuple<int, string>>();

			try
			{
				using (var deviceReader = GetDeviceProductReader(aDeviceId, aFirmwareVersion))
				{
					if (deviceReader.Read())
					{
						var idColumn = deviceReader.GetOrdinal("Id");
						using (var peripheralsReader = SelectAndRead("PeripheralId, Name",
																	 "Console_Peripherals",
																	 "ParentId = {0}",
																	 deviceReader.GetInt32(idColumn)))
						{
							var periIdColumn = peripheralsReader.GetOrdinal("PeripheralId");
							var nameColumn = peripheralsReader.GetOrdinal("Name");
							while (peripheralsReader.Read())
							{
								result.Add(new Tuple<int, string>(peripheralsReader.GetInt32(periIdColumn),
																  peripheralsReader.GetString(nameColumn)));
							}
						}
					}
				}
			}
			catch (KeyNotFoundException)
			{
			}

			return result;
		}

		#endregion

		#region -- Data --

		// Data structure for serializing help profiling data.
		// Member names must match those expected by the profiling JavaScript function.
		internal struct HelpProfileIncludeTag
		{
			public HelpProfileIncludeTag(string aAttrib, string aValue)
			{
				att = aAttrib;
				val = aValue;
			}


			public string att;
			public string val;
		}

		// Data structure for serializing help profiling data.
		// Member names must match those expected by the profiling JavaScript function.
		internal struct HelpProfileData
		{
			public string deviceName;
			public string peripheralName;
			public string firmwareVersion;
			public HelpProfileIncludeTag[] includes;
			public HelpProfileIncludeTag[] excludes;
		}


		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		// Maps dimension names from the device database to Zaber.Units names.
		private static readonly IDictionary<string, Dimension> _dimensionNameMap = new Dictionary<string, Dimension>
		{
			{ "length", Dimension.Length },
			{ "acceleration", Dimension.Acceleration },
			{ "velocity", Dimension.Velocity },
			{ "angle", Dimension.Angle },
			{ "angular velocity", Dimension.AngularVelocity },
			{ "angular acceleration", Dimension.AngularAcceleration },
			{ "percent", Dimension.Ratio },
			{ "force", Dimension.Force },
			{ "ac electric current", Dimension.AlternatingCurrent },
			{ "dc electric current", Dimension.DirectCurrent }
		};

		private static readonly string[] _ditaPassthroughAttributes =
		{
			"min-fw", "max-fw", "command", "setting", "capability"
		};

		private readonly Dictionary<HelpCacheKey, IDeviceHelp> _deviceHelpCache =
			new Dictionary<HelpCacheKey, IDeviceHelp>();

		private const string NOHWMOD = " AND HardwareModified = 0 AND ParentHardwareModified = 0";

		protected readonly SQLiteConnection _connection;
		private readonly Dictionary<Command, string> _binaryParameterDescriptions = new Dictionary<Command, string>();
		private readonly Dictionary<Command, string> _binaryReturnDescriptions = new Dictionary<Command, string>();
		private Dictionary<string, string> _asciiFw7ProtocolManualPaths;

		// Table of predefined param types (does not include enums, which each have a different value list).
		private readonly IDictionary<int, ParameterType> _paramTypeMap = new Dictionary<int, ParameterType>();
		internal IDictionary<int, Dimension> _dimensionIdMap = new Dictionary<int, Dimension>();
		private readonly IDictionary<Dimension, Unit> _baseUnits = new Dictionary<Dimension, Unit>();
		private ComposedUnitParser _parser;
		private UnitConverter _converter;
		private bool _tuningWarningEmitted;

		#endregion

		#region -- Protected Helper Methods --

		// If the device's real FW version isn't in the database and has a higher minor version than what
		// is in the database, use the highest minor version found.
		protected FirmwareVersion? MapFirmwareVersion(int deviceId, FirmwareVersion firmwareVersion)
		{
			var major = firmwareVersion.Major;
			var minor = firmwareVersion.Minor;
			var build = firmwareVersion.Build;
			int? maxMinor;
			int? maxBuild;

			// Internal / custom FW case.
			if ((minor >= 95) && (build != 0))
			{
				// Try exact match first, then try for less precise match.
				using (var cmd = new SQLiteCommand("SELECT Id FROM Console_Devices "
											   + $"WHERE DeviceId = {deviceId} "
											   + $"AND MajorVersion = {major} "
											   + $"AND MinorVersion = {minor} "
											   + $"AND Build = {build} "
											   + "LIMIT 1",
												   _connection))
				{
					if (cmd.ExecuteScalar() != null)
					{
						return firmwareVersion;
					}
				}

				maxMinor = minor < 95 ? GetMaxMinorVersion(deviceId, major) : minor;
				if (!maxMinor.HasValue)
				{
					return null;
				}
				else if (minor < maxMinor)
				{
					// We still won't allow firmware versions that aren't in the database but are
					// older than the latest version that is in the database, because that would mean the
					// database had not updated with a the FW version when it still could have been.
					return null;
				}

				maxBuild = GetMaxBuildNumber(deviceId, major, minor);
				if (!maxBuild.HasValue)
				{
					return null;
				}

				return new FirmwareVersion(major, maxMinor.Value, maxBuild.Value);
			}

			// Public FW case.
			using (var cmd = new SQLiteCommand("SELECT Id FROM Console_Devices "
										   + $"WHERE DeviceId = {deviceId} "
										   + $"AND MajorVersion = {major} "
										   + $"AND MinorVersion = {minor} "
										   + "LIMIT 1",
											   _connection))
			{
				if (cmd.ExecuteScalar() != null)
				{
					return firmwareVersion;
				}
			}

			maxMinor = GetMaxMinorVersion(deviceId, major);
			if (!maxMinor.HasValue)
			{
				return null;
			}

			if (minor < maxMinor)
			{
				return null;
			}

			return new FirmwareVersion(major, maxMinor.Value);
		}


		protected int? GetMaxMinorVersion(int deviceId, int majorVersion)
		{
			using (var cmd = new SQLiteCommand("SELECT max(MinorVersion) AS MinorVersion FROM Console_Devices "
										   + $"WHERE DeviceId = {deviceId} "
										   + $"AND MajorVersion = {majorVersion} "
										   + "AND MinorVersion < 95",
											   _connection))
			{
				using (var reader = cmd.ExecuteReader())
				{
					if (reader.Read() && reader.HasColumn("MinorVersion"))
					{
						var value = reader["MinorVersion"];
						if (value != DBNull.Value)
						{
							return (int)(long)value;
						}
					}
				}
			}

			return null;
		}


		protected int? GetMaxBuildNumber(int deviceId, int majorVersion, int minorVersion)
		{
			using (var cmd = new SQLiteCommand("SELECT max(Build) AS Build FROM Console_Devices "
										   + $"WHERE DeviceId = {deviceId} "
										   + $"AND MajorVersion = {majorVersion} "
										   + $"AND MinorVersion = {minorVersion}",
											   _connection))
			{
				using (var reader = cmd.ExecuteReader())
				{
					if (reader.Read() && reader.HasColumn("Build"))
					{
						var value = reader["Build"];
						if (value != DBNull.Value)
						{
							return (int)(long)value;
						}
					}
				}
			}

			return null;
		}


		protected static MotionType DetermineMotionType(IEnumerable<Dimension> aConvertibleDimensions,
														IList<CommandInfo> aCommandList)
		{
			if (aConvertibleDimensions.Contains(Dimension.Length))
			{
				return MotionType.Linear;
			}

			if (aConvertibleDimensions.Contains(Dimension.Angle))
			{
				return MotionType.Rotary;
			}

			// We need to set the DeviceType's "MotionType" property to
			// make UOMs work properly. Try to grab it from the "current
			// position" command/setting.
			var positionCommand = aCommandList
				.FirstOrDefault(cmd => Command.SetCurrentPosition == (Command)cmd.Number || ("get pos" == cmd.TextCommand));

			// command number 20 is the Move Absolute command in binary.
			var moveCommand = aCommandList
				.FirstOrDefault(cmd => Command.MoveAbsolute == (Command)cmd.Number || ("move abs {x}" == cmd.TextCommand));

			if ((null == positionCommand?.RequestUnit?.MotionType) && (null == moveCommand))
			{
				return MotionType.None;
			}

			// If the device has a move command but not a get pos command,
			// use MotionType.Other to signify that it does move but we don't know how.
			return positionCommand?.RequestUnit?.MotionType ?? MotionType.Other;
		}


		protected IList<CommandInfo> GetCommandList(string aProductId)
		{
			var commandList = new List<CommandInfo>();

			// Get all of the binary commands the device supports.
			using (var binaryCommands = SelectAndRead("*",
													  "Console_BinaryCommands3",
													  "ProductId = {0}" + NOHWMOD,
													  aProductId))
			{
				commandList.AddRange(PopulateBinaryCommands(binaryCommands));
			}

			// Get all of the binary settings the device has.
			using (var binarySettings = SelectAndRead("*",
													  "Console_BinarySettings3",
													  "ProductId = {0} AND Name IS NOT NULL" + NOHWMOD,
													  aProductId))
			{
				commandList.AddRange(PopulateBinarySettings(binarySettings));
			}

			// Get all of the ASCII commands the device supports.
			using (var asciiCommands = SelectAndRead("*",
													 "Console_ASCIICommandTree4",
													 "ProductId = {0}" + NOHWMOD,
													 aProductId))
			{
				using (var enumData = SelectAndRead("TypeName, NodeName",
													"Console_EnumNodes",
													"1"))
				{
					commandList.AddRange(PopulateAsciiCommands(asciiCommands, enumData));
				}
			}

			// Get all of the ASCII settings the device supports.
			using (var asciiSettings = SelectAndRead("*",
													 "Console_ASCIISettings3",
													 "ProductId = {0} AND Name IS NOT NULL" + NOHWMOD,
													 aProductId))
			{
				using (var enumData = SelectAndRead("TypeName, NodeName",
													"Console_EnumNodes",
													"1"))
				{
					commandList.AddRange(PopulateAsciiSettings(asciiSettings, enumData));
				}
			}

			return commandList;
		}


		/// <summary>
		///     Reads guidance data for servo mode tuning algorithms from the database.
		///     This data is not expected to exist for all device types, so the return may
		///     be null.
		/// </summary>
		/// <param name="aProductId">Product ID (internal key) of the device or peripheral to query for.</param>
		/// <returns>Any tuning data found for the product, or null if not found.</returns>
		protected TuningData PopulateTuningData(string aProductId)
		{
			TuningData data = null;

			try
			{
				using (var tuningReader = SelectAndRead("*", "Console_ServoTuning", "ProductId = {0}", aProductId))
				{
					if ((null == tuningReader) || !tuningReader.HasRows)
					{
						return null;
					}

					if (!tuningReader.Read()) // There is only expected to be one row per product.
					{
						return null;
					}

					data = new TuningData();

					// Retrieve the force constant and assign units to it.
					if (!tuningReader.HasColumn("ForqueConstant"))
					{
						// This field is mandatory; we can't tune without it.
						_log.Info(
							"Device database has no forque constant column; servo tuning data will not be available.");
						return null;
					}

					var fcColumn = tuningReader.GetOrdinal("ForqueConstant");
					if (tuningReader.IsDBNull(fcColumn))
					{
						_log.InfoFormat(
							"Device database has no forque constant value for product ID {0}; servo tuning data will not be available for this device.",
							aProductId);
						return null;
					}

					data.ForqueConstant = tuningReader.GetDouble(fcColumn);


					// Retrieve the carriage inertia and assign units to it.
					if (tuningReader.HasColumn("CarriageInertia"))
					{
						var inertiaColumn = tuningReader.GetOrdinal("CarriageInertia");
						if (!tuningReader.IsDBNull(inertiaColumn))
						{
							var inertia = tuningReader.GetDouble(inertiaColumn);
							var dimName = tuningReader.GetString(tuningReader.GetOrdinal("CarriageInertiaDimension"));
							if (null != dimName)
							{
								// Units in the database are always assumed to be in the (database) base unit of the
								// dimension.
								var unit = _baseUnits.Where(pair => pair.Key.Name == dimName)
												  .Select(pair => pair.Value)
												  .FirstOrDefault();
								if (null != unit)
								{
									data.Inertia = new Measurement(inertia, unit);
								}
							}
						}
					}


					// Retrieve the JSON tuning guidance data.
					var guidanceColumn = tuningReader.GetOrdinal("TunerGuidanceData");
					if (!tuningReader.IsDBNull(guidanceColumn))
					{
						data.GuidanceData = tuningReader.GetString(guidanceColumn);
					}
				}
			}
			catch (SQLiteException aException)
			{
				if (!_tuningWarningEmitted)
				{
					_tuningWarningEmitted = true;
					_log.Warn("Device database has no servo tuning table.", aException);
				}
			}

			// Find out the active controller versions in this firmware.
			data.ControllerVersions = new Dictionary<string, int>();
			try
			{
				using (var versionReader =
					SelectAndRead("*", "Console_ServoControllers", "ProductId = {0}", aProductId))
				{
					var nameColumn = versionReader.GetOrdinal("ServoControllerName");
					var versionColumn = versionReader.GetOrdinal("ServoControllerVersion");

					while (versionReader.Read())
					{
						if (versionReader.IsDBNull(nameColumn) || versionReader.IsDBNull(versionColumn))
						{
							break;
						}

						var name = versionReader.GetString(nameColumn);
						var version = versionReader.GetInt32(versionColumn);
						data.ControllerVersions[name] = version;
					}
				}
			}
			catch (SQLiteException aException)
			{
				if (!_tuningWarningEmitted)
				{
					_tuningWarningEmitted = true;
					_log.Warn("Device database has no servo controller version info.", aException);
				}
			}

			// Don't store an empty data structure.
			if ((null == data.Inertia) && (null == data.GuidanceData))
			{
				return null;
			}

			return data;
		}


		/// <summary>
		///     Retrieve the capability names of a product from the database. Returns
		///     an empty collection if the database predates the necessary view.
		/// </summary>
		/// <param name="aProductId">Product ID to get capabilities for.</param>
		/// <returns>Zero or more capability names as strings.</returns>
		protected ICollection<string> ReadCapabilities(string aProductId)
		{
			var results = new List<string>();

			try
			{
				using (var capReader = SelectAndRead("Capability", "Console_Capabilities2", "ProductId = {0}" + NOHWMOD, aProductId))
				{
					var capColumn = capReader.GetOrdinal("Capability");
					while (capReader.Read())
					{
						var name = capReader.GetString(capColumn);
						results.Add(name);
					}
				}
			}
			catch (SQLiteException aException)
			{
				_log.Warn($"Device database has no capabilities for product { aProductId }.", aException);
			}

			return results;
		}


		/// <summary>
		///     Get a data reader that maps a device ID (only, not a peripheral ID) and a firmware version
		///     to a row in the products view. This can be used to get the product ID for other queries.
		/// </summary>
		/// <param name="aDeviceId">Controller or integrated device's device ID.</param>
		/// <param name="aFirmwareVersion">Device's firmware version.</param>
		/// <returns>A SQLiteDataReader from which zero or one rows of product information can be read.</returns>
		protected SQLiteDataReader GetDeviceProductReader(int aDeviceId, FirmwareVersion aFirmwareVersion)
		{
			SQLiteDataReader deviceReader = null;

			if (aFirmwareVersion.Minor >= 95)
			{
				var build = aFirmwareVersion.Build;

				// Minor versions 95-99 mean special things. We use the
				// build number to determine what kind of special thing.
				if (build == 0)
				{
					// If the build number is 0 for a device with minor
					// version >=95, then the device doesn't support the
					// "get version.build" command, so we can't know the
					// build number. Use the highest build number present
					// for that device in the database as a fallback.
					using (var cmd = new SQLiteCommand("SELECT MAX(Build) FROM Console_Devices WHERE "
												   + $"DeviceId = {aDeviceId} "
												   + $"AND MajorVersion = {aFirmwareVersion.Major} "
												   + $"AND MinorVersion = {aFirmwareVersion.Minor};",
													   _connection))
					{
						var result = cmd.ExecuteScalar();

						// SELECT MAX(Build) will return null if no entry exists
						// for the device ID and version number specified.
						if (result == DBNull.Value)
						{
							throw new KeyNotFoundException();
						}

						build = (int) (long) result;
					}
				}

				deviceReader = SelectAndRead("Id, Name",
											 "Console_Devices",
											 "DeviceId = {0} AND MajorVersion = {1} "
										 + "AND MinorVersion = {2} AND Build = {3}",
											 aDeviceId,
											 aFirmwareVersion.Major,
											 aFirmwareVersion.Minor,
											 build);
			}
			else
			{
				deviceReader = SelectAndRead("Id, Name",
											 "Console_Devices",
											 "DeviceId = {0} AND MajorVersion = {1} AND MinorVersion = {2}",
											 aDeviceId,
											 aFirmwareVersion.Major,
											 aFirmwareVersion.Minor);
			}

			return deviceReader;
		}


		/// <summary>
		///     Performs a "SELECT x FROM y WHERE z" query, and returns a reader
		///     from which query result data can be read.
		/// </summary>
		/// <param name="aSelection">Which columns to select.</param>
		/// <param name="aFrom">The table or view to select from.</param>
		/// <param name="aWhere">
		///     A condition which must be satisfied by a row to
		///     be included in the result. Can be a format string.
		/// </param>
		/// <param name="aArgs">
		///     Extra string arguments to fill into
		///     <paramref name="aWhere" />, if it is a format string.
		/// </param>
		/// <returns>A data reader with the result of the query.</returns>
		protected SQLiteDataReader SelectAndRead(string aSelection, string aFrom, string aWhere, params object[] aArgs)
		{
			if (aArgs.Length > 0)
			{
				aWhere = string.Format(aWhere, aArgs);
			}

			var queryString = $"SELECT {aSelection} FROM {aFrom} WHERE {aWhere};";
			using (var queryCommand = new SQLiteCommand(queryString, _connection))
			{
				var result = queryCommand.ExecuteReader();

				if (!result.HasRows)
				{
					_log.WarnFormat("Database query found nothing: '{0}'", queryString);
				}

				return result;
			}
		}


		/// <summary>
		///     Execute a query and invoke a callback for each resulting row.
		/// </summary>
		/// <param name="aQuery">SQL query string to run.</param>
		/// <param name="aRowCallback">Action to execute for each successfully read row.</param>
		protected void ForEachRow(string aQuery, Action<IDataRecord> aRowCallback)
		{
			using (var queryCommand = new SQLiteCommand(aQuery, _connection))
			{
				using (var queryReader = queryCommand.ExecuteReader())
				{
					while (queryReader.Read())
					{
						aRowCallback(queryReader);
					}
				}
			}
		}


		private void LoadUnitTable()
		{
			_parser = ComposedUnitParser.NewDefaultUnitParser();
			_converter = new UnitConverter(_parser, _parser.GetUnitRegistry());
			UnitConverter = new ConversionTable(_converter);

			// Read dimensions, creating new ones where we don't have an existing equivalent.
			var queryString = "SELECT * FROM Console_Dimensions;";
			using (var queryCommand = new SQLiteCommand(queryString, _connection))
			{
				using (var reader = queryCommand.ExecuteReader())
				{
					var idColumn = reader.GetOrdinal("Id");
					var nameColumn = reader.GetOrdinal("Name");
					while (reader.Read())
					{
						var id = reader.GetInt32(idColumn);
						var name = reader.GetString(nameColumn);

						if (!_dimensionNameMap.TryGetValue(name.ToLowerInvariant(), out var d))
						{
							d = Dimension.GetByName(name) ?? Dimension.GetByName(name.ToLowerInvariant());
							if (null == d)
							{
								d = new Dimension(name);
							}
						}

						_dimensionIdMap[id] = d;
					}
				}
			}

			// Read unit definitions and correlate by dimension.
			var unitsByDimension = new Dictionary<int, List<Tuple<string, string, double, double>>>();
			queryString = "SELECT * FROM Console_Units;";
			using (var queryCommand = new SQLiteCommand(queryString, _connection))
			{
				using (var reader = queryCommand.ExecuteReader())
				{
					var dimensionColumn = reader.GetOrdinal("DimensionId");
					var symbolColumn = reader.GetOrdinal("ShortName");
					var nameColumn = reader.GetOrdinal("LongName");
					var scaleColumn = reader.GetOrdinal("Scale");
					var offsetColumn = reader.GetOrdinal("Offset");

					while (reader.Read())
					{
						var dim = reader.GetInt32(dimensionColumn);
						var symbol = reader.GetString(symbolColumn);
						var name = reader.GetString(nameColumn);
						var scale = reader.GetDouble(scaleColumn);
						var offset = reader.GetDouble(offsetColumn);

						if (!unitsByDimension.ContainsKey(dim))
						{
							unitsByDimension[dim] = new List<Tuple<string, string, double, double>>();
						}

						unitsByDimension[dim]
						.Add(new Tuple<string, string, double, double>(symbol, name, 1.0 / scale, offset));
					}
				}
			}

			// Set up the units for each dimension.
			foreach (var pair in unitsByDimension)
			{
				var dimension = _dimensionIdMap[pair.Key];

				// Zaber.Units requires each dimension to have a base unit that has scale 1 and offset 0.

				Unit baseUnit = null;
				Tuple<string, string, double, double> baseUnitInfo = null;
				foreach (var unitInfo in pair.Value)
				{
					baseUnitInfo = unitInfo;
					if ((Math.Abs(unitInfo.Item3 - 1.0) < 0.001) && (Math.Abs(unitInfo.Item4) < 0.001))
					{
						// We always use the database definitions for units even if they already exist
						// in the global table. This lets us define an arbitrarily complete unit
						// system in the database and have it be internally consistent, while still
						// being able to access outside units.
						_log.DebugFormat("Found database base unit {0} for {1}.", unitInfo.Item1, dimension.Name);
						baseUnit = new BaseUnit(unitInfo.Item1, unitInfo.Item2)
						{
							Dimension = dimension,
							UseSIPrefixes = false // The database table defines all the prefixes we want.
						};

						UnitConverter.RegisterUnit(baseUnit);
						_baseUnits[dimension] = baseUnit;
						break;
					}
				}

				if (null == baseUnit)
				{
					throw new DataException("Could not identify a base unit for dimension " + dimension.Name);
				}

				// Now find or generate all the other units for this dimension.
				foreach (var unitInfo in pair.Value)
				{
					if (unitInfo != baseUnitInfo)
					{
						_log.DebugFormat("Found database unit {0} of {1}.", unitInfo.Item1, dimension.Name);
						var u = new ScaledShiftedUnit(unitInfo.Item1,
													  unitInfo.Item2,
													  baseUnit,
													  unitInfo.Item3,
													  unitInfo.Item4)
						{
							Dimension = dimension,
							UseSIPrefixes = false
						};

						UnitConverter.RegisterUnit(u);
					}
				}
			}

			// Since there is only one device database instance at a time, use its
			// unit conversion table as the default. It internally chains to the old default.
			UnitConverter.Parent = ConversionTable.Default;
			ConversionTable.Default = UnitConverter;
		}


		/// <summary>
		///     Get the product ID (primary key) for a given device, peripheral and firmware version.
		/// </summary>
		/// <param name="aFirmwareVersion">
		///     The firmware version of the device. The search will attempt
		///     to match the build number if it is set, and fall back to ignoring the build number
		///     if no match is found.
		/// </param>
		/// <param name="aDeviceId">The device ID reported by the device.</param>
		/// <param name="aPeripheralId">
		///     The peripheral ID reported by the device. Optional -
		///     pass in null if there is no peripheral. 0 means an axis in the unused or safe mode state.
		/// </param>
		/// <returns>The product ID if found, or NULL if not.</returns>
		protected int? GetProductId(FirmwareVersion aFirmwareVersion, int aDeviceId, int? aPeripheralId = null)
		{
			var deviceProdId = 0;
			var success = false;

			using (var devices = SelectAndRead("Id",
											   "Console_Devices",
											   "DeviceId = {0} AND MajorVersion = {1} AND MinorVersion = {2} AND Build = {3}",
											   aDeviceId,
											   aFirmwareVersion.Major,
											   aFirmwareVersion.Minor,
											   aFirmwareVersion.Build))
			{
				if (devices.Read())
				{
					deviceProdId = (int) (long) devices["Id"];
					success = true;
				}
			}

			if (!success)
			{
				// If this specific firmware version (including the build number)
				// is not in the database, fall back to not using the build number.
				using (var devices = SelectAndRead("Id",
												   "Console_Devices",
												   "DeviceId = {0} AND MajorVersion = {1} AND MinorVersion = {2} "
												   + "ORDER BY Build DESC LIMIT 1",
												   aDeviceId,
												   aFirmwareVersion.Major,
												   aFirmwareVersion.Minor))
				{
					if (devices.Read())
					{
						deviceProdId = (int) (long) devices["Id"];
						success = true;
					}
				}
			}

			if (!success)
			{
				return null;
			}

			if (aPeripheralId.HasValue)
			{
				using (var peripherals = SelectAndRead("Id",
													   "Console_Peripherals",
													   "ParentId = {0} AND PeripheralId = {1}",
													   deviceProdId,
													   aPeripheralId))
				{
					if (peripherals.Read())
					{
						return (int)(long)peripherals["Id"];
					}
				}
			}

			return deviceProdId;
		}


		/// <summary>
		///     Returns device type specific units for all relevant quantities.
		///     This does not include resolution-dependent units.
		/// </summary>
		/// <param name="aProductId"></param>
		/// <param name="aCommandList">Device's command list - used as a fallback for compatibility with old device databases.</param>
		/// <returns></returns>
		protected IEnumerable<IParameterConversionInfo> GetProductUnitConversions(
			string aProductId, IEnumerable<CommandInfo> aCommandList)
		{
			var result = new List<IParameterConversionInfo>();

			var view = "Console_ProductsDimensionsFunctions2";
			if (!ViewExists(view))
			{
				view = "Console_ProductsDimensionsFunctions";
			}

			var queryString = $"SELECT * FROM {view} WHERE ProductId = {aProductId};";
			try
			{
				using (var queryCommand = new SQLiteCommand(queryString, _connection))
				{
					using (var reader = queryCommand.ExecuteReader())
					{
						var dimensionColumn = reader.GetOrdinal("DimensionId");
						var functionColumn = reader.GetOrdinal("FunctionName");
						var scaleColumn = reader.GetOrdinal("Scale");

						while (reader.Read())
						{
							var dim = reader.GetInt32(dimensionColumn);
							var function = reader.GetString(functionColumn);
							decimal? scale = null;
							if (!reader.IsDBNull(scaleColumn))
							{
								scale = (decimal)reader.GetDouble(scaleColumn);
							}

							var dimension = _dimensionIdMap[dim];
							var baseUnit = GetBaseUnitForDimension(dimension);

							result.Add(new ParameterUnitConversion
							{
								ReferenceUnit = baseUnit,
								Scale = scale,
								Function = ParseUnitFunction(function),
								IsRequestRelativePosition = false
							});
						}
					}
				}
			}
			catch (SQLiteException aException)
			{
				_log.Error($"Found no unit conversion functions for product ID {aProductId}.", aException);
			}

			// If we're using an old device database, try to build the table from the command list instead.
			// This runs the risk of mistaking setting-specific units for device units, but
			// there are no known cases where they are different.
			if ((result.Count == 0) && (null != aCommandList))
			{
				var seenUnits = new HashSet<string>();
				foreach (var command in aCommandList)
				{
					var paramList = new List<IParameterConversionInfo>();
					if (command is ASCIICommandInfo asciiCommand)
					{
						foreach (var node in asciiCommand.Nodes)
						{
							paramList.Add(node.GetRequestConversion());
						}
					}
					else
					{
						paramList.Add(command.GetRequestConversion());
					}

					paramList.Add(command.GetResponseConversion());

					foreach (var paramInfo in paramList)
					{
						if ((null != paramInfo)
						&& paramInfo.Scale.HasValue
						&& (null != paramInfo.ReferenceUnit)
						&& !seenUnits.Contains(paramInfo.ReferenceUnit.Abbreviation))
						{
							seenUnits.Add(paramInfo.ReferenceUnit.Abbreviation);
							result.Add(paramInfo);
						}
					}
				}
			}

			return result;
		}


		private void LoadParamTable()
		{
			var queryString = "SELECT * FROM Console_ParamTypes;";
			using (var queryCommand = new SQLiteCommand(queryString, _connection))
			{
				using (var reader = queryCommand.ExecuteReader())
				{
					var idColumn = reader.GetOrdinal("Id");
					var nameColumn = reader.GetOrdinal("Name");
					var numericColumn = reader.GetOrdinal("IsNumeric");
					var signedColumn = reader.GetOrdinal("IsSigned");
					var boolColumn = reader.GetOrdinal("IsBoolean");
					var placesColumn = reader.GetOrdinal("DecimalPlaces");

					while (reader.Read())
					{
						var id = reader.GetInt32(idColumn);
						var name = reader.GetString(nameColumn);
						var numeric = reader.GetBoolean(numericColumn);
						var signed = reader.GetBoolean(signedColumn);
						var boolean = reader.GetBoolean(boolColumn);
						var decimals = reader.GetInt32(placesColumn);

						ParameterType pt = null;
						List<string> enumValues = null;
						if (name.ToLowerInvariant().Contains("enum"))
						{
							// Enums are a special case - they need to be instantiated where
							// used rather than stored in the global lookup table, because each
							// enum can have a different value set. This is a sentinel value that
							// is used in PopulateAsciiCommands() to distinguish an enum from a token.
							// Passing in a non-null list of values to the ParameterType constructor
							// causes IsEnumeration to return true.
							// Note that arity also requires duplicating the paramtype instances
							// so as to avoid changing the reference instance.
							enumValues = new List<string>();
						}

						pt = new ParameterType(enumValues)
						{
							Name = name,
							DecimalPlaces = decimals,
							IsBoolean = boolean,
							IsNumeric = numeric,
							IsSigned = signed
						};

						_paramTypeMap[id] = pt;
					}
				}
			}
		}


		/// <summary>
		///     Sets the parameter type of an ASCII command node object, and defines
		///     acceptable nodes if it's an enumerated parameter.
		/// </summary>
		/// <param name="aNode">The ASCII command node to populate.</param>
		/// <param name="aReader">The record to populate the node from.</param>
		/// <param name="aEnumReader">A list of all enum type records.</param>
		private void SetCommandNodeParameterType(ASCIICommandNode aNode, IDataRecord aReader,
												 List<IDataRecord> aEnumReader)
		{
			var arityColumn = aReader.GetOrdinal("ParamArity");
			int? arity = aReader.IsDBNull(arityColumn) ? null : (int?) aReader.GetInt32(arityColumn);

			aNode.ParamType = GetParameterType(aReader, GetParamEnumValues(aNode.NodeText, aEnumReader), arity);
		}


		private IEnumerable<string> GetParamEnumValues(string aMatch, List<IDataRecord> aEnumReader)
			=> (from entry in aEnumReader
				 where aMatch == (string) entry.GetValue(0)
				 select (string) entry.GetValue(1)).ToList();


		private ParameterType GetParameterType(IDataRecord aReader, IEnumerable<string> aEnumValues, int? aArity = 1)
		{
			var paramTypeCol = aReader.GetOrdinal("ParamTypeId");
			var paramTypeId = aReader.IsDBNull(paramTypeCol) ? -1 : aReader.GetInt32(paramTypeCol);
			var newType = new ParameterType();

			if (paramTypeId >= 0)
			{
				if (_paramTypeMap.TryGetValue(paramTypeId, out var pt))
				{
					if (pt.IsEnumeration) // Flag value - see comment in LoadParamTable().
					{
						var aEnumNodes = aEnumValues?.ToList();
						if (aEnumNodes?.Any() ?? false)
						{
							newType = new ParameterType(aEnumNodes);
						}
						else
						{
							_log.Error("Encountered an enum parameter type with no values; treating as token.");
						}
					}
					else
					{
						newType = new ParameterType(pt);
					}
				}
				else if (paramTypeId >= 0)
				{
					_log.ErrorFormat("Encountered a bogus parameter type {0}; treating as token.", paramTypeId);
				}

				newType.Arity = aArity;

				return newType;
			}

			// If there is no param type ID, do not set the node's param type.
			return null;
		}


		private Unit GetBaseUnitForDimension(Dimension aDimension)
		{
			foreach (var unit in _converter.FindUnits(aDimension))
			{
				if (unit.IsCoherent)
				{
					return unit;
				}
			}

			return null;
		}


		private UnitOfMeasure FindOrCreateBaseUnit(int aDimensionId)
		{
			if (!_dimensionIdMap.ContainsKey(aDimensionId))
			{
				throw new SQLiteException(string.Format("Unrecognized dimension ID {0}", aDimensionId));
			}

			var dimension = _dimensionIdMap[aDimensionId];
			var u = GetBaseUnitForDimension(dimension);
			if (null == u)
			{
				throw new SQLiteException(string.Format("Could not identify the default unit for dimension {0}",
														dimension));
			}

			return UnitConverter.FindUnitByAbbreviation(u.Abbreviation);
		}


		private static ScalingFunction ParseUnitFunction(string aFunctionName)
		{
			switch (aFunctionName)
			{
				case "linear-resolution":
					return ScalingFunction.LinearResolution;
				case "tangential-resolution":
					return ScalingFunction.TangentialResolution;
				case "linear":
					return ScalingFunction.Linear;
				case "reciprocal":
					return ScalingFunction.Reciprocal;
				default:
					throw new SQLiteException($"Unrecognized scaling function: {aFunctionName}");
			}
		}


		/// <summary>
		///     Sets the unit type, scaling function and scale of an ASCII
		///     command parameter node.
		/// </summary>
		/// <param name="aNodeInfo">The ASCII command node to populate.</param>
		/// <param name="aReader">The record to read from.</param>
		/// <param name="aDimensionColumn">Index of the column to read the dimension ID from.</param>
		/// <param name="aFunctionColumn">Index of the column to read the conversion function name from.</param>
		/// <param name="aScaleColumn">Index of the column to read the scale factor from.</param>
		private void SetCommandNodeUomProperties(ASCIICommandNode aNodeInfo, IDataRecord aReader,
			int aDimensionColumn, int aFunctionColumn, int aScaleColumn)
		{
			var dimensionId = aReader.GetInt32(aDimensionColumn);
			aNodeInfo.RequestUnit = FindOrCreateBaseUnit(dimensionId);
			aNodeInfo.RequestUnitFunction = ParseUnitFunction(aReader.GetString(aFunctionColumn));
			aNodeInfo.RequestUnitScale = aReader.GetDecimal(aScaleColumn);
		}


		/// <summary>
		///     Reads parameter UOM data from from a reader into a CommandInfo object.
		/// </summary>
		/// <param name="aCommandInfo">The object to be populated.</param>
		/// <param name="aReader">The reader to read from.</param>
		/// <param name="aDimensionColumn">Index of the column to read the dimension ID from.</param>
		/// <param name="aFunctionColumn">Index of the column to read the conversion function name from.</param>
		/// <param name="aScaleColumn">Index of the column to read the scale factor from.</param>
		private void SetParameterUomProperties(CommandInfo aCommandInfo, IDataRecord aReader,
			int aDimensionColumn, int aFunctionColumn, int aScaleColumn)
		{
			var dimensionId = aReader.GetInt32(aDimensionColumn);
			aCommandInfo.RequestUnit = FindOrCreateBaseUnit(dimensionId);
			aCommandInfo.RequestUnitFunction = ParseUnitFunction(aReader.GetString(aFunctionColumn));
			aCommandInfo.RequestUnitScale = aReader.GetDecimal(aScaleColumn);
		}


		/// <summary>
		///     Reads response UOM data from from a reader into a CommandInfo object.
		/// </summary>
		/// <param name="aCommandInfo">The object to be populated.</param>
		/// <param name="aReader">The reader to read from.</param>
		/// <param name="aDimensionColumn">Index of the column to read the dimension ID from.</param>
		/// <param name="aFunctionColumn">Index of the column to read the conversion function name from.</param>
		/// <param name="aScaleColumn">Index of the column to read the scale factor from.</param>
		private void SetResponseUomProperties(CommandInfo aCommandInfo, IDataRecord aReader,
			int aDimensionColumn, int aFunctionColumn, int aScaleColumn)
		{
			var dimensionId = aReader.GetInt32(aDimensionColumn);
			aCommandInfo.ResponseUnit = FindOrCreateBaseUnit(dimensionId);
			aCommandInfo.ResponseUnitFunction = ParseUnitFunction(aReader.GetString(aFunctionColumn));
			aCommandInfo.ResponseUnitScale = aReader.GetDecimal(aScaleColumn);
		}


		/// <summary>
		///     Reads UOM data from from a reader into a SettingInfo object.
		/// </summary>
		/// <param name="aSettingInfo">The object to populate.</param>
		/// <param name="aReader">The reader to read from.</param>
		/// <param name="aDimensionColumn">Index of the column to read the dimension ID from.</param>
		/// <param name="aFunctionColumn">Index of the column to read the conversion function name from.</param>
		/// <param name="aScaleColumn">Index of the column to read the scale factor from.</param>
		private void SetSettingUomProperties(SettingInfo aSettingInfo, IDataRecord aReader,
			int aDimensionColumn, int aFunctionColumn, int aScaleColumn)
		{
			var dimensionId = aReader.GetInt32(aDimensionColumn);
			aSettingInfo.RequestUnit = FindOrCreateBaseUnit(dimensionId);
			aSettingInfo.RequestUnitFunction = ParseUnitFunction(aReader.GetString(aFunctionColumn));
			aSettingInfo.RequestUnitScale = aReader.GetDecimal(aScaleColumn);
			aSettingInfo.ResponseUnit = aSettingInfo.RequestUnit;
			aSettingInfo.ResponseUnitFunction = aSettingInfo.RequestUnitFunction;
			aSettingInfo.ResponseUnitScale = aSettingInfo.RequestUnitScale;
		}

		#endregion

		#region -- Private helpers --

		private IEnumerable<CommandInfo> PopulateBinaryCommands(SQLiteDataReader aCommandReader)
		{
			var commandColumn = aCommandReader.GetOrdinal("Command");
			var commandNameColumn = GetColumnIndex(aCommandReader, "CommandName", "Name");
			var currentPositionColumn = aCommandReader.GetOrdinal("ReturnsCurrentPosition");
			var retryColumn = aCommandReader.GetOrdinal("Idempotent");
			var visibilityColumn = GetColumnIndex(aCommandReader, "Visibility", "OptionallyVisible");
			var requestRelativeColumn = aCommandReader.GetOrdinal("ParameterRelativePosition");
			var responseRelativeColumn = aCommandReader.GetOrdinal("ResponseRelativePosition");
			var parameterColumn = aCommandReader.GetOrdinal("Parameter");
			var paramDimensionColumn = aCommandReader.GetOrdinal("ParameterDimension");
			var paramFunctionColumn = aCommandReader.GetOrdinal("ParameterUnitFunction");
			var paramScaleColumn = aCommandReader.GetOrdinal("ParameterUnitScale");
			var responseDimensionColumn = aCommandReader.GetOrdinal("ResponseDimension");
			var responseFunctionColumn = aCommandReader.GetOrdinal("ResponseUnitFunction");
			var responseScaleColumn = aCommandReader.GetOrdinal("ResponseUnitScale");

			while (aCommandReader.Read())
			{
				var number = aCommandReader.GetByte(commandColumn);
				var commandInfo = new CommandInfo
				{
					Name = aCommandReader.GetString(commandNameColumn),
					Number = number,
					IsCurrentPositionReturned = aCommandReader.GetBoolean(currentPositionColumn),
					IsRetrySafe = aCommandReader.GetBoolean(retryColumn),
					IsBasic = !aCommandReader.GetBoolean(visibilityColumn),
					IsRequestRelativePosition = aCommandReader.GetBoolean(requestRelativeColumn),
					IsResponseRelativePosition = aCommandReader.GetBoolean(responseRelativeColumn),
					HasParameters = aCommandReader.GetBoolean(parameterColumn)
				};

				if (!aCommandReader.IsDBNull(paramFunctionColumn) && !aCommandReader.IsDBNull(paramScaleColumn))
				{
					SetParameterUomProperties(commandInfo, aCommandReader,
						paramDimensionColumn, paramFunctionColumn, paramScaleColumn);
				}

				if (!aCommandReader.IsDBNull(responseFunctionColumn) && !aCommandReader.IsDBNull(responseScaleColumn))
				{
					SetResponseUomProperties(commandInfo, aCommandReader, 
						responseDimensionColumn, responseFunctionColumn, responseScaleColumn);
				}

				PopulateBinaryCommandInfo(commandInfo);

				yield return commandInfo;
			}
		}


		private IEnumerable<CommandInfo> PopulateBinarySettings(SQLiteDataReader aSettingsReader, bool aSplitDualCommands = false)
		{
			var currentPositionColumn = aSettingsReader.GetOrdinal("CurrentPosition");
			var nameColumn = aSettingsReader.GetOrdinal("Name");
			var setColumn = aSettingsReader.GetOrdinal("SetCommand");
			var returnColumn = aSettingsReader.GetOrdinal("ReturnCommand");
			var basicColumn = GetColumnIndex(aSettingsReader, "Visibility", "OptionallyVisible");
			var relativePositionColumn = aSettingsReader.GetOrdinal("RelativePosition");
			var dimensionColumn = aSettingsReader.GetOrdinal("Dimension");
			var functionColumn = aSettingsReader.GetOrdinal("UnitFunction");
			var scaleColumn = aSettingsReader.GetOrdinal("UnitScale");

			while (aSettingsReader.Read())
			{
				// Settings can have both a get and a set command. We want to create two different
				// CommandInfo outputs in this case, so to simplify the code we'll work from a list of cases
				// that will typically have either one or two entries.
				var numbers = new List<Tuple<byte, bool>>();
				if (!aSettingsReader.IsDBNull(setColumn))
				{
					numbers.Add(new Tuple<byte, bool>(aSettingsReader.GetByte(setColumn), true)); // True means this is a setter.
				}

				if (!aSettingsReader.IsDBNull(returnColumn))
				{
					// Do not add a second command for the settings grid case.
					if (aSplitDualCommands || aSettingsReader.IsDBNull(setColumn))
					{
						numbers.Add(
							new Tuple<byte, bool>(aSettingsReader.GetByte(returnColumn), false)); // False means this is a getter.
					}
				}

				foreach (var number in numbers)
				{
					SettingInfo settingInfo;
					if (number.Item2)
					{
						settingInfo = new SettingInfo();
					}
					else
					{
						// If a setting has no "SetCommand" value, then it is a
						// read-only setting, which is handled differently.
						// Get its number from "ReturnCommand" instead.
						settingInfo = new ReadOnlySettingInfo();
					}

					settingInfo.IsCurrentPositionReturned = aSettingsReader.GetBoolean(currentPositionColumn);
					settingInfo.Name = aSettingsReader.GetString(nameColumn);
					settingInfo.Number = number.Item1;
					settingInfo.IsBasic = !aSettingsReader.GetBoolean(basicColumn);
					settingInfo.IsRequestRelativePosition = aSettingsReader.GetBoolean(relativePositionColumn);
					settingInfo.IsResponseRelativePosition = settingInfo.IsRequestRelativePosition;
					settingInfo.IsRetrySafe = true; // All settings are retry-safe.

					if (!aSettingsReader.IsDBNull(functionColumn) && !aSettingsReader.IsDBNull(scaleColumn))
					{
						SetSettingUomProperties(settingInfo, aSettingsReader,
							dimensionColumn, functionColumn, scaleColumn);
					}

					PopulateBinaryCommandInfo(settingInfo);

					yield return settingInfo;
				}
			}
		}


		private IEnumerable<CommandInfo> PopulateAsciiCommands(SQLiteDataReader aCommandReader,
															   SQLiteDataReader enumData)
		{
			// get columns for required fields (can now specify column return types)
			var idCol = aCommandReader.GetOrdinal("Id");
			var parentCol = aCommandReader.GetOrdinal("ParentId");
			var commandCol = aCommandReader.GetOrdinal("Command");
			var optionallyVisibleCol = GetColumnIndex(aCommandReader, "Visibility", "OptionallyVisible");
			var unitDimCol = aCommandReader.GetOrdinal("ParameterDimension");
			var callbackCol = aCommandReader.GetOrdinal("HasCallback");
			var relativePosCol = aCommandReader.GetOrdinal("RelativePosition");
			var accessCol = aCommandReader.GetOrdinal("AccessLevel");
			var dimensionColumn = aCommandReader.GetOrdinal("ParameterDimension");
			var functionColumn = aCommandReader.GetOrdinal("ParameterUnitFunction");
			var scaleColumn = aCommandReader.GetOrdinal("ParameterUnitScale");

			// convert the enum data to a list for querying multiple times
			var enumList = new List<IDataRecord>(enumData.Cast<IDataRecord>());

			// store all nodes from the query in the dictionary (for locating parents later)
			var nodes = new Dictionary<int, ASCIICommandNode>();

			while (aCommandReader.Read())
			{
				// create new ASCII command node
				ASCIICommandNode nodeNew;
				if (aCommandReader.IsDBNull(parentCol))
				{
					nodeNew = new ASCIICommandNode
					{
						NodeText = aCommandReader.GetString(commandCol),
						IsBasic = !aCommandReader.GetBoolean(optionallyVisibleCol),
						AccessLevel = aCommandReader.GetInt32(accessCol)
					};
				}
				else
				{
					var parent = nodes[aCommandReader.GetInt32(parentCol)];
					nodeNew = new ASCIICommandNode(parent)
					{
						NodeText = aCommandReader.GetString(commandCol),
						IsBasic = !aCommandReader.GetBoolean(optionallyVisibleCol),
						AccessLevel = aCommandReader.GetInt32(accessCol)
					};

					SetCommandNodeParameterType(nodeNew, aCommandReader, enumList);

					if (!aCommandReader.IsDBNull(unitDimCol) && !aCommandReader.IsDBNull(scaleColumn))
					{
						SetCommandNodeUomProperties(nodeNew, aCommandReader,
							dimensionColumn, functionColumn, scaleColumn);
					}
				}

				var id = aCommandReader.GetInt32(idCol);
				nodes.Add(id, nodeNew);

				// if the node has a callback, create a command and add it to the list
				if (aCommandReader.GetBoolean(callbackCol))
				{
					var newCmd = new ASCIICommandInfo(nodeNew);
					yield return newCmd;
				}
			}
		}


		private IEnumerable<CommandInfo> PopulateAsciiSettings(SQLiteDataReader aSettingsReader,
															   SQLiteDataReader enumData)
		{
			var nameColumn = aSettingsReader.GetOrdinal("Name");
			var writeLevelColumn = aSettingsReader.GetOrdinal("WriteLevel");
			var readLevelColumn = aSettingsReader.GetOrdinal("ReadLevel");
			var visibilityColumn = GetColumnIndex(aSettingsReader, "Visibility", "OptionallyVisible");
			var currentPositionColumn = aSettingsReader.GetOrdinal("CurrentPosition");
			var dimensionColumn = aSettingsReader.GetOrdinal("Dimension");
			var functionColumn = aSettingsReader.GetOrdinal("UnitFunction");
			var scaleColumn = aSettingsReader.GetOrdinal("UnitScale");

			var enumList = new List<IDataRecord>(enumData.Cast<IDataRecord>());

			while (aSettingsReader.Read())
			{
				var name = aSettingsReader.GetString(nameColumn);

				SettingInfo settingInfo;
				if (!aSettingsReader.IsDBNull(writeLevelColumn))
				{
					settingInfo = new SettingInfo { AccessLevel = aSettingsReader.GetInt32(writeLevelColumn) };
				}
				else // Read-only.
				{
					settingInfo = new ReadOnlySettingInfo();
				}

				settingInfo.Name = name;
				settingInfo.TextCommand = name;
				settingInfo.IsBasic = !aSettingsReader.GetBoolean(visibilityColumn);
				settingInfo.IsCurrentPositionReturned = aSettingsReader.GetBoolean(currentPositionColumn);
				settingInfo.IsRetrySafe = true; // All settings are retry-safe.

				if (!aSettingsReader.IsDBNull(readLevelColumn))
				{
					settingInfo.ReadAccessLevel = aSettingsReader.GetInt32(readLevelColumn);
				}

				if (!aSettingsReader.IsDBNull(functionColumn) && !aSettingsReader.IsDBNull(scaleColumn))
				{
					SetSettingUomProperties(settingInfo, aSettingsReader,
						dimensionColumn, functionColumn, scaleColumn);
				}

				settingInfo.ParamType =
					GetParameterType(aSettingsReader, GetParamEnumValues(settingInfo.TextCommand, enumList));

				yield return settingInfo;
			}
		}


		// This adds binary command info data that is scraped form the protocol manuals instead
		// of being in the device database. These values are used to format data echoed to the message log.
		private void PopulateBinaryCommandInfo(CommandInfo aCommand)
		{
			if (!_binaryParameterDescriptions.TryGetValue(aCommand.Command, out var value))
			{
				var typeInfo = typeof(Command);
				var valueInfo = typeInfo.GetMember(aCommand.Command.ToString());
				if (valueInfo.Length > 0)
				{
					var attributes = valueInfo[0].GetCustomAttributes(typeof(ParameterDescriptionAttribute), false);
					if (attributes.Length > 0)
					{
						aCommand.DataDescription = ((ParameterDescriptionAttribute) attributes[0]).Description;
					}

					attributes = valueInfo[0].GetCustomAttributes(typeof(ReturnDescriptionAttribute), false);
					if (attributes.Length > 0)
					{
						aCommand.ResponseDescription = ((ReturnDescriptionAttribute) attributes[0]).Description;
					}
				}

				// Cache the binary command data descriptions since they are the same for all devices
				// and reflection is expensive.
				_binaryParameterDescriptions[aCommand.Command] = aCommand.DataDescription;
				_binaryReturnDescriptions[aCommand.Command] = aCommand.ResponseDescription;
			}
			else
			{
				aCommand.DataDescription = value;
				value = null;
				_binaryReturnDescriptions.TryGetValue(aCommand.Command, out value);
				aCommand.ResponseDescription = value;
			}
		}


		private int GetColumnIndex(SQLiteDataReader aReader, params string[] aColumnNameWithFallbacks)
		{
			foreach (var name in aColumnNameWithFallbacks)
			{
				int column = aReader.GetOrdinal(name);
				if (column >= 0)
				{
					return column;
				}
			}

			_log.ErrorFormat("Could not find a database column named {0}", string.Join(" or ", aColumnNameWithFallbacks));
			throw new SQLiteException(Resources.ERR_COLUMN_NOT_FOUND);
		}


		private bool ViewExists(string aViewName)
		{
			var queryString = $"SELECT name FROM sqlite_master WHERE type='view' AND name='{aViewName}';";
			using (var queryCommand = new SQLiteCommand(queryString, _connection))
			{
				using (var queryReader = queryCommand.ExecuteReader())
				{
					return queryReader.HasRows;
				}
			}
		}


		/// <summary>
		///     Reads the contents of Console_Doc_ASCIICommandPaths into a lookup table
		///     that can be used to find the FW7 protocol manual command tag for a given
		///     ASCII command. This data is the same for all FW7 devices, so it is only
		///     loaded from the database on the first call.
		/// </summary>
		/// <returns>
		///     A dictionary mapping ASCII command tree strings such as "move abs x" to
		///     protocol manual data-command attribute names such as "move.abs.x".
		/// </returns>
		private IDictionary<string, string> ReadAsciiProtocolManualPaths()
		{
			if (null == _asciiFw7ProtocolManualPaths)
			{
				_asciiFw7ProtocolManualPaths = new Dictionary<string, string>();
				var map = new Dictionary<int, Tuple<int?, string, string>>();

				// Check if new view exists and fall back to the old one if needed.
				var viewName = "Console_Doc_ASCIICommandPaths";
				viewName = ViewExists(viewName) ? viewName : "Documentation_ASCIICommandPaths";

				var queryString = $"SELECT Id,Command,ParentId,Path FROM {viewName};";
				ForEachRow(queryString,
						   row =>
						   {
							   var id = row.GetInt32(row.GetOrdinal("Id"));
							   var word = row.GetString(row.GetOrdinal("Command"));
							   var path = row.GetString(row.GetOrdinal("Path"));

							   int? parent = null;
							   var parentColumn = row.GetOrdinal("ParentId");
							   if (!row.IsDBNull(parentColumn))
							   {
								   parent = row.GetInt32(parentColumn);
							   }

							   map[id] = new Tuple<int?, string, string>(parent, word, path);
						   });

				// Flatten the command word hierarchy to generate the final table.
				foreach (var pair in map)
				{
					var id = pair.Key;
					var pathInfo = pair.Value;

					var fullCommand = pathInfo.Item2;
					var parentId = pathInfo.Item1;
					while (parentId.HasValue)
					{
						var parentInfo = map[parentId.Value];
						parentId = parentInfo.Item1;
						fullCommand = parentInfo.Item2 + " " + fullCommand;
					}

					_asciiFw7ProtocolManualPaths[fullCommand] = pathInfo.Item3;
				}

				if (_asciiFw7ProtocolManualPaths.Count < 1)
				{
					_log.Error("Database contains no ASCII command paths.");
				}
			}

			return _asciiFw7ProtocolManualPaths;
		}


		private IDeviceHelp CreateAsciiHelpMapper(DeviceType aDeviceType, bool aOnline)
		{
			IDeviceHelp mapper;

			if (aDeviceType.FirmwareVersion.Major < 1) // Device collection.
			{
				var ditaVals = GetGenericDitaVals();
				var asciiHelpPaths = ReadAsciiProtocolManualPaths();
				mapper = new AsciiWebHelpMapperFW7(aOnline, ditaVals, asciiHelpPaths);
			}
			else if (aDeviceType.FirmwareVersion.Major >= 7)
			{
				var ditaVals = GetDitaVals(aDeviceType, Protocol.ASCII);
				var asciiHelpPaths = ReadAsciiProtocolManualPaths();
				mapper = new AsciiWebHelpMapperFW7(aOnline, ditaVals, asciiHelpPaths);
			}
			else
			{
				mapper = new AsciiWebHelpMapperFW6(aOnline);
			}

			return mapper;
		}


		private IDeviceHelp CreateBinaryHelpMapper(DeviceType aDeviceType, bool aOnline)
		{
			IDeviceHelp mapper;

			if (aDeviceType.FirmwareVersion.Major < 1) // Device collection.
			{
				mapper = new BinaryWebHelpMapperFW7(aOnline, GetGenericDitaVals());
			}
			else if (aDeviceType.FirmwareVersion.Major >= 7)
			{
				mapper = new BinaryWebHelpMapperFW7(aOnline, GetDitaVals(aDeviceType, Protocol.Binary));
			}
			else
			{
				mapper = new BinaryWebHelpMapperFW6(aOnline);
			}

			return mapper;
		}


		/// <summary>
		///     Generate Dita profiling data from the database Documentation views. This data is
		///     specific to a device type at present.
		/// </summary>
		/// <param name="aDeviceType">Device type to generate profiling data for.</param>
		/// <param name="aProtocol">Protocol currently in use.</param>
		/// <returns>
		///     A collection of key-value pairs that needs to be passed to the profiling
		///     JavaScript code in the help documents.
		/// </returns>
		/// <remarks>
		///     This code was adapted from the Python script that generates ditavals
		///     in XML format for the website.
		/// </remarks>
		private (string, object[])? GetDitaVals(DeviceType aDeviceType, Protocol aProtocol)
		{
			var productIds = new List<int>();

			// Collect profiling for the controller. If this is an axis, we still do this because Dita profiling 
			// requires a second pass for the controller if a peripheral is specified.
			int? productIdResult = GetProductId(aDeviceType.FirmwareVersion, aDeviceType.DeviceId);
			if (productIdResult.HasValue)
			{
				productIds.Add(productIdResult.Value);
			}

			// If this is an axis, collect profiling for the peripheral.
			if (aDeviceType.PeripheralMap?.Count == 0) // Only way to detect that a DeviceType is an axis.
			{
				productIdResult = GetProductId(aDeviceType.FirmwareVersion, aDeviceType.DeviceId, aDeviceType.PeripheralId);
				if (productIdResult.HasValue)
				{
					productIds.Add(productIdResult.Value);
				}
			}

			if (productIds.Count < 1)
			{
				return null;
			}

			var includes = new List<HelpProfileIncludeTag>();
			var excludes = new List<HelpProfileIncludeTag>();

			var protocolKey = Protocol.ASCII == aProtocol ? "ascii" : "binary";


			var tableNames = new List<string>();
			ForEachRow("SELECT Name FROM Documentation_ProfileTables3;",
					   tableRow =>
					   {
						   // Avoid overlapped queries.
						   tableNames.Add((string) tableRow["Name"]);
					   });

			foreach (var tableName in tableNames)
			{
				foreach (var productId in productIds)
				{
					var queryString =
						$"SELECT DISTINCT Att, Act, Val FROM {tableName} WHERE ProductId = {productId} AND Protocol = '{protocolKey}';";
					ForEachRow(queryString,
							   row =>
							   {
								   var actionColumn = row.GetOrdinal("Act");
								   if (!row.IsDBNull(actionColumn))
								   {
									   var att = row.GetString(row.GetOrdinal("Att"));
									   var action = row.GetString(actionColumn);
									   var val = row.GetValue(row.GetOrdinal("Val")).ToString();

									   switch (action)
									   {
										   case "include":
											   includes.Add(new HelpProfileIncludeTag(att, val));
											   break;
										   case "exclude":
											   excludes.Add(new HelpProfileIncludeTag(att, val));
											   break;
										   default:
											   _log.WarnFormat("Found unsupported action {0} in profiling table.",
															   action);
											   break;
									   }
								   }
							   });
				}
			}


			var deviceName = aDeviceType.Name ?? aDeviceType.ToString();
			var peripheralName = string.Empty;
			if (deviceName.Contains('+'))
			{
				var parts = deviceName.Split('+');
				deviceName = parts[0].Trim();
				peripheralName = parts[1].Trim();
			}

			var serializer = new JavaScriptSerializer();
			var json = serializer.Serialize(new HelpProfileData
			{
				deviceName = deviceName,
				peripheralName = peripheralName,
				firmwareVersion = aDeviceType.FirmwareVersion.ToString(NumberFormatInfo.InvariantInfo),
				includes = includes.ToArray(),
				excludes = excludes.ToArray()
			});

			// Uncomment this next line to dump the profiling data to a text file.
			//File.WriteAllText($"Ditavals for { deviceName }{ (string.IsNullOrEmpty(peripheralName) ? "" : " + " + peripheralName )}.json", json);

			// The returned tuple specifies the name of the javascript function to call
			// in the HTML help file, and the arguments to pass to it. Note that
			// only strings, numbers and arrays can be serialized. The outer array
			// created below specifies the ordered list of arguments to the function.
			return ("updateAndProfileFromZC", new object[] { json });
		}


		/// <summary>
		///     Create help profiling data that will result in a generic document being displayed.
		/// </summary>
		/// <returns>A default help profiling data set for the FW7 protocol manuals.</returns>
		private (string, object[])? GetGenericDitaVals()
		{
			var firmwareVersion = "7.00";

			var queryString =
				"SELECT MAX(MinorVersion) FROM Documentation_Products WHERE MinorVersion < 95 AND MajorVersion = 7;";
			using (var queryCommand = new SQLiteCommand(queryString, _connection))
			{
				using (var queryReader = queryCommand.ExecuteReader())
				{
					if (queryReader.Read())
					{
						var minor = (int) (long) queryReader[0];
						firmwareVersion = $"7.{minor:D2}";
					}
				}
			}

			var serializer = new JavaScriptSerializer();
			var json = serializer.Serialize(new HelpProfileData
			{
				deviceName = string.Empty,
				peripheralName = string.Empty,
				firmwareVersion = firmwareVersion,
				includes = null,
				excludes = null
			});

			// The returned tuple specifies the name of the javascript function to call
			// in the HTML help file, and the arguments to pass to it. Note that
			// only strings, numbers and arrays can be serialized. The outer array
			// created below specifies the ordered list of arguments to the function.
			return ("updateAndProfileForGenericProductFromZC", new object[] { json });
		}

		#endregion
	}
}

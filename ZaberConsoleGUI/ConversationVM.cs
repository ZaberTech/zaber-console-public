﻿using System.Globalization;
using Zaber;

namespace ZaberConsole
{
	public class ConversationVM
	{
		#region -- Public Methods & Properties --

		public ConversationVM(Conversation aConversation)
		{
			Conversation = aConversation;
		}


		public Conversation Conversation { get; }


		public string DeviceNumber
		{
			get
			{
				var format = Conversation.Device.AxisNumber == 0 ? "{0:00}" : "{0:00} {1}";
				return string.Format(CultureInfo.InvariantCulture,
									 format,
									 Conversation.Device.DeviceNumber,
									 Conversation.Device.AxisNumber);
			}
		}

		#endregion

		#region -- Data --

		#endregion
	}
}

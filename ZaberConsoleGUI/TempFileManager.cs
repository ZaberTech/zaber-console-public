﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using log4net;

namespace ZaberConsole
{
	/// <summary>
	///     Helper to clean up temp files at program exit. In .NET land, deleting temp files
	///     immediately after using them is hard to impossible, as they may be locked by external
	///     programs such as virus scanners or the debug host, or by hidden code objects that
	///     won't let go until the garbage collector has run several times. A more reliable way
	///     to clean up temp files - but still not perfect - is to keep a list of the ones that
	///     can't be deleted immediately and try to delete them when the program exits. That will
	///     often allow enough time to pass for file locks to be released.
	/// </summary>
	public static class TempFileManager
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Add a file to the list of files to be deleted later. This does not
		///     attempt immediate deletion - to do that, call this and then call
		///     <see cref="TryDeleteFiles" />.
		/// </summary>
		/// <param name="aPath">Path of the file to be deleted.</param>
		public static void RegisterForDelete(string aPath)
		{
			if (!Path.IsPathRooted(aPath))
			{
				aPath = Path.GetFullPath(aPath);
			}

			lock (_filesToDelete)
			{
				_filesToDelete.Add(aPath);
			}
		}


		/// <summary>
		///     Attempt to delete files that have been queued for deletion. Any that
		///     cannot be deleted will be retained for future attempts.
		/// </summary>
		/// <returns>True if all files were successfully deleted.</returns>
		public static bool TryDeleteFiles()
		{
			var todo = new List<string>();

			lock (_filesToDelete)
			{
				todo.AddRange(_filesToDelete);
				_filesToDelete.Clear();
			}

			var success = true;
			foreach (var path in todo)
			{
				if (File.Exists(path))
				{
					try
					{
						File.Delete(path);
					}
					catch (IOException aException)
					{
						_log.Warn("Failed to clean up temp file " + path, aException);
						_filesToDelete.Add(path);
						success = false;
					}
				}
			}

			return success;
		}


		public static bool FileStagedForDeletion(string aFile) => _filesToDelete.Contains(aFile);

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly List<string> _filesToDelete = new List<string>();

		#endregion
	}
}

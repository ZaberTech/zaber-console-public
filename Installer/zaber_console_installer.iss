#define ApplicationPublisher "Zaber Technologies"
#define ApplicationPublisherURL "https://www.zaber.com/"
#define ApplicationSupportURL "https://www.zaber.com/contact/"
#define SIGNPWD GetEnv('SIGNING_PASSWORD')

#ifdef stable
  #define ApplicationName "Zaber Console"
  #define ApplicationBaseExeName "ZaberConsole"
  #define InstallerBaseApplicationName "InstallZaberConsole"
  #define ApplicationUpdatesURL "https://www.zaber.com/support/software.php?installer=inno&version=stable"
  #define GUID "E29CE68A-9306-4FF4-9815-E6EF1312A8EF"
  #define SourceFilesDirectory "..\ZaberConsoleGUI\bin\Release\app.publish.prod"
#endif
#ifdef dev
  #define ApplicationName "Zaber Console (Dev)"
  #define ApplicationBaseExeName "ZaberConsoleDev"
  #define InstallerBaseApplicationName "InstallZaberConsoleDev"
  #define ApplicationUpdatesURL "https://www.zaber.com/support/software.php?installer=inno&version=dev"
  #define GUID "BE8DC147-D516-4E1D-A9B3-47F6E1A193D6"
  #define SourceFilesDirectory "..\ZaberConsoleGUI\bin\Release\app.publish.dev"
#endif
#ifdef nightlydev
  #define ApplicationName "Zaber Console (Nightly Dev)"
  #define ApplicationBaseExeName "ZaberConsoleNightlyDev"
  #define InstallerBaseApplicationName "InstallZaberConsoleNightlyDev"
  #define ApplicationUpdatesURL "https://www.zaber.com/support/software-downloads.php?product=zaber_console_nightly_dev_installer&version=latest"
  #define GUID "C05627EB-6A55-4EA4-B8AA-B7C5723E0B12"
  #define SourceFilesDirectory "..\ZaberConsoleGUI\bin\Release\app.publish.nightlydev"
#endif
#ifdef installtest
  #define ApplicationName "Zaber Console (INSTALL TEST)"
  #define ApplicationBaseExeName "ZaberConsoleInstallTest"
  #define InstallerBaseApplicationName "InstallZaberConsoleInstallTest"
  #define ApplicationUpdatesURL "https://www.zaber.com/support/software.php?installer=inno&version=test"
  #define GUID "0386B243-0792-4797-BDF9-3834AA7C2A41"
  #define SourceFilesDirectory "..\ZaberConsoleGUI\bin\Release\app.publish.test"
#endif
#ifdef internalalpha
  #define ApplicationName "Zaber Console (INTERNAL ALPHA)"
  #define ApplicationBaseExeName "ZaberConsoleInternalAlpha"
  #define InstallerBaseApplicationName "InstallZaberConsoleInternalAlpha"
  #define ApplicationUpdatesURL "https://www.zaber.com/support/software-downloads.php?product=zaber_console_Alpha_InnoSetup_installer&version=latest"
  #define GUID "4E4BD9AD-18CB-4152-81BF-FECCA62503EA"
  #define SourceFilesDirectory "..\ZaberConsoleGUI\bin\Release\app.publish.alpha"
#endif

#define ApplicationExeName ApplicationBaseExeName + ".exe"
#define ApplicationVersion GetFileVersion(SourceFilesDirectory + "\" + ApplicationExeName)
#define ApplicationFilesDirectory SourceFilesDirectory + "\Application Files\" + ApplicationBaseExeName + "_" + StringChange(ApplicationVersion, ".", "_") + "\*"


[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{{#GUID}}
AppName={#ApplicationName}
AppComments=Zaber Console is an application that lets you send commands to your Zaber devices, adjust their settings, and create simple scripts of commands.
AppContact={#ApplicationPublisher}
AppVersion={#ApplicationVersion}
AppVerName={#ApplicationName}
AppPublisher={#ApplicationPublisher}
AppPublisherURL={#ApplicationPublisherURL}
AppSupportURL={#ApplicationSupportURL}
AppUpdatesURL={#ApplicationUpdatesURL}
DefaultDirName={pf}\Zaber Technologies\{#ApplicationName}
DisableProgramGroupPage=yes
LicenseFile=..\ZaberConsoleGUI\license.txt
; InfoBeforeFile=..\ZaberConsoleGUI\_ReadMe.txt
OutputDir=..\dist\{#ApplicationBaseExeName}
OutputBaseFilename={#InstallerBaseApplicationName}-{#ApplicationVersion}
Compression=lzma
SolidCompression=yes
; Note: signtool defined in Tools > Configure Sign Tools...
; Configuration:
; Name of the Sign Tool: zcsign
; Command of the Sign Tool: $qC:\Program Files (x86)\Windows Kits\10\bin\10.0.22000.0\x86\signtool.exe$q sign /a /fd sha384 /csp "eToken Base Cryptographic Provider" $p /t http://timestamp.sectigo.com/authenticode /d $qZaber Console$q $f
SignTool=zcsign /f "{#SourcePath}..\ZaberCSDemo\packages\git\software-keys\keys\apps\sectigo2023.cer" /kc "[SafeNet Token JC 0{{{#SIGNPWD}}}]=Sectigo_20231102140616"
PrivilegesRequired=admin
; If the timestamp server goes down, an alternate is http://timestamp.verisign.com/scripts/timstamp.dll - this needs to be changed in the InnoSetup IDE Tools menu, and in the signing page of the ZaberConsoleGUI project properties.

DisableDirPage=no
CloseApplications=yes
RestartApplications=no
DisableFinishedPage=yes


[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"


[Messages]
; *** "License Agreement" page
WizardLicense=License Agreement
LicenseLabel=Please read the following important information before continuing.
LicenseLabel3=Please read the following License Aagreement. You must accept the terms of this agreement before continuing with the installation.
LicenseAccepted=I &accept the agreement
LicenseNotAccepted=I &do not accept the agreement

; *** "Select Destination Location" page
WizardSelectDir=Select destination location
SelectDirDesc=Where should [name] be installed?
SelectDirLabel3=Setup will install [name] in the following folder.
SelectDirBrowseLabel=To continue, click Next. If you would like to select a different folder, click Browse.
DiskSpaceMBLabel=At least [mb] MB of free disk space is required.
CannotInstallToNetworkDrive=Setup cannot install to a network drive.
CannotInstallToUNCPath=Setup cannot install to a UNC path.
InvalidPath=You must enter a full path including a drive letter. For example:%n%nC:\APP%n%nor a UNC path in the form:%n%n\\server\share
InvalidDrive=The drive or UNC share you selected does not exist or is not accessible. Please select another.
DiskSpaceWarningTitle=Not enough disk space
DiskSpaceWarning=Setup requires at least %1 KB of free space to install, but the selected drive only has %2 KB available.%n%nWould you like to continue anyway?
DirNameTooLong=The folder name or path is too long.
InvalidDirName=The folder name is not valid.
BadDirName32=Folder names cannot include any of the following characters:%n%n%1
DirExistsTitle=Folder exists
DirExists=The folder:%n%n%1%n%nalready exists. Would you like to install to that folder anyway?
DirDoesntExistTitle=Folder does not exist
DirDoesntExist=The folder:%n%n%1%n%ndoes not exist. Would you like to creat it?

; *** "Select Additional Tasks" page
WizardSelectTasks=Select tasks
SelectTasksDesc=Which tasks should be performed?
SelectTasksLabel2=Select the tasks you would like Setup to perform while installing [name], then click Next.

; *** "Ready to Install" page
WizardReady=Ready to install
ReadyLabel1=Setup is ready to install [name] on your computer.
ReadyLabel2a=Please close open instances of Zaber Console before installing.%n%nClick Install to continue with the installation, or click Back if you want to review or change any settings.
ReadyLabel2b=Click Install to continue with the installation.
ReadyMemoUserInfo=User information:
ReadyMemoDir=Destination location:
ReadyMemoType=Setup type:
ReadyMemoComponents=Selected components:
ReadyMemoGroup=Start Menu folder:
ReadyMemoTasks=Additional tasks:

; *** "Preparing to Install" page
WizardPreparing=Preparing to install
PreparingDesc=Setup is preparing to install [name] on your computer.
PreviousInstallNotCompleted=The installation/removal of a previous program was not completed. Please restart your computer to complete that installation.%n%nAfter restarting your computer, run Setup again to complete the installation of [name].
CannotContinue=Setup cannot continue. Please click Cancel to exit.
ApplicationsFound=The following applications are using files that need to be updated by Setup. It is recommended that you allow Setup to automatically close these applications.
ApplicationsFound2=The following applications are using files that need to be updated by Setup. It is recommended that you allow Setup to automatically close these applications. After the installation has completed, Setup will attempt to restart the applications.
CloseApplications=&Automatically close the applications
DontCloseApplications=&Do not close the applications

; *** "Installing" page
WizardInstalling=Installing
InstallingLabel=Please wait while Setup installs [name] on your computer.

; *** Misc. errors
ErrorCreatingDir=Setup was unable to create the directory "%1"
ErrorTooManyFilesInDir=Unable to create a file in the directory "%1" because it contains too many files

; *** Setup common messages
ExitSetupTitle=Exit Setup
ExitSetupMessage=Setup is not complete. If you exit now, the program will not be installed.%n%nYou may run Setup again at another time to complete the installation.%n%nExit Setup?

[CustomMessages]
LaunchWhenFinished=&Launch {#ApplicationName} when finished installing


[Tasks]
Name: "launchZaberConsoleOnComplete"; Description:  {cm:LaunchWhenFinished}
Name: "desktopicon"; Description: {cm:CreateDesktopIcon}; Flags: unchecked


[Files]
; ScriptRunner
Source: "..\ScriptRunner\ScriptRunner\bin\Release\*"; Excludes: "*.xml"; DestDir: {app};

; Zaber Console - this may overwrite some of the ScriptRunner files. They should be the same, but just in case this version is more authoritative.
; NOTE: Don't use "Flags: ignoreversion" on any shared system files
Source: "{#ApplicationFilesDirectory}"; DestDir: {app}; Flags: ignoreversion recursesubdirs


[Icons]
Name: "{commonprograms}\Zaber Technologies\{#ApplicationName}"; Filename: "{app}\{#ApplicationExeName}"
Name: "{commondesktop}\{#ApplicationName}"; Filename: "{app}\{#ApplicationExeName}"; Tasks: desktopicon


[Run]
Filename: "{app}\{#ApplicationExeName}"; Tasks: launchZaberConsoleOnComplete; Flags: nowait postinstall skipifsilent


[Code]
var
  ClickOnceUninstallStr: String;
  ClickOncePage: TWizardPage;

const
  KeyNameBase = 'Software\Microsoft\Windows\CurrentVersion\Uninstall';

procedure LaunchUninstaller(Sender: TObject);
var
  ResultCode: Integer;
  Error: Boolean;
begin
  Error := False;
  try
    if Exec('>', ClickOnceUninstallStr, '', SW_SHOW, ewWaitUntilTerminated, ResultCode) then
      WizardForm.NextButton.OnClick(ClickOncePage)
    else
      Error := True;
  except
    Error := True;
  end;

  if Error then
    MsgBox('The ClickOnce uninstaller may have failed, but you can still proceed with this installation by clicking the Next button.', mbInformation, mb_Ok);
end;

procedure InitializeWizard;
var
  AppNameStr: String;
  KeyNames: TArrayOfString;
  I: Integer;
  DisplayName: String;
  UninstallButton: TButton;
  Memo: TNewStaticText;  
begin
  { Check whether the ClickOnce version is installed }
  AppNameStr := ExpandConstant('{#ApplicationName}');
  if RegGetSubkeyNames(HKEY_CURRENT_USER, KeyNameBase, KeyNames) then
  begin
    for I := 0 to GetArrayLength(KeyNames)-1 do
    begin
      if RegQueryStringValue(HKEY_CURRENT_USER, KeyNameBase + '\' + KeyNames[I], 'DisplayName', DisplayName) then
      begin
        if CompareStr(AppNameStr, DisplayName) = 0 then
        begin
          if RegQueryStringValue(HKEY_CURRENT_USER, KeyNameBase + '\' + KeyNames[I], 'UninstallString', ClickOnceUninstallStr) then
          begin
            Break;
          end;
        end;
      end;
    end;
  end;

  { Create the uninstall page if found }
  if Length(ClickOnceUninstallStr) > 0 then
  begin
    ClickOncePage := CreateCustomPage(wpLicense, 
        'ClickOnce Cleanup', 
        'Remove ClickOnce edition of ' + AppNameStr + '?');

    Memo := TNewStaticText.Create(ClickOncePage);
    Memo.Width := ClickOncePage.SurfaceWidth;
    Memo.Height := 79;
    Memo.WordWrap := True;
    Memo.Caption := 'Setup has detected a previous version of ' 
        + AppNameStr 
        + ', which was installed with the ClickOnce installer. ' 
        + 'We recommend uninstalling the previous version to avoid having two installations.'
        + #13#10#13#10 
        + 'Would you like to uninstall the previous installation now?'
        + #13#10;
    Memo.Parent := ClickOncePage.Surface;

    UninstallButton := TNewButton.Create(ClickOncePage);
    UninstallButton.Top := Memo.Top + Memo.Height + ScaleY(8);
    UninstallButton.Width := ScaleX(220);
    UninstallButton.Height := ScaleY(23);
    UninstallButton.Caption := 'Launch ClickOnce Uninstaller';
    UninstallButton.OnClick := @LaunchUninstaller;
    UninstallButton.Parent := ClickOncePage.Surface;
  end;
end;

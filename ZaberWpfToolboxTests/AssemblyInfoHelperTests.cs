﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;
using ZaberWpfToolbox;

namespace ZaberWpfToolboxTests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class AssemblyInfoHelperTests
	{
		[SetUp]
		public void Setup() => _helper = new AssemblyInfoHelper();


		// Test that we got expected information after construction.
		[Test]
		public void TestEntryAssemblyInformation()
		{
			// There should be multiple attributes.
			var names = _helper.GetEntryAssemblyAttributeNames();
			Assert.Greater(names.Count(), 0);

			// Verify each has a unique name.
			var values = new HashSet<string>();
			foreach (var name in names)
			{
				var value = _helper.GetEntryAssemblyAttribute(name);
				Assert.IsNotNull(value);
				Assert.IsFalse(values.Contains(name));
				values.Add(name);
			}

			// Verify using a bogus name gets us a default value.
			var bogusValue = _helper.GetEntryAssemblyAttribute("BogusAttributeName");
			Assert.IsTrue(bogusValue.Contains("BogusAttributeName"));
		}


		// Test gathering application information.
		[Test]
		public void TestPopulateAppInfo()
		{
			var results = _helper.PopulateAppInfo();

			Assert.Greater(results.Count(), 0);

			var dict = results.ToDictionary(t => t.Item1, t => t.Item2);

			Assert.IsTrue(dict.ContainsKey("Executing Assembly"));
			Assert.AreEqual("ZaberWpfToolbox", dict["Executing Assembly"]);

			Assert.IsTrue(dict.ContainsKey("Calling Assembly"));
			Assert.AreEqual("ZaberWpfToolboxTests", dict["Calling Assembly"]);
		}


		// Test gathering assembly list.
		[Test]
		public void TestPopulateAssemblies()
		{
			var results = _helper.PopulateAssemblies();

			Assert.Greater(results.Count(), 0);

			foreach (var asm in results)
			{
				Assert.IsFalse(string.IsNullOrEmpty(asm.AssemblyName));
				Assert.IsFalse(string.IsNullOrEmpty(asm.Version));
				Assert.IsFalse(string.IsNullOrEmpty(asm.BuildDate));
				Assert.IsFalse(string.IsNullOrEmpty(asm.Location));
				Assert.IsTrue(File.Exists(asm.Location), "Assembly disk location is wrong.");
			}
		}


		// Test gathering info about assemblies.
		[Test]
		public void TestPopulateAssemblyDetails()
		{
			foreach (var asm in _helper.PopulateAssemblies())
			{
				var results = _helper.PopulateAssemblyDetails(asm.AssemblyName);
				Assert.Greater(results.Count(), 0);

				// Verify keys are unique.
				var keys = new HashSet<string>();
				foreach (var result in results)
				{
					Assert.IsFalse(keys.Contains(result.Item1));
					keys.Add(result.Item1);
				}
			}
		}


		// Test that no information is returned about a bogus assembly.
		[Test]
		public void TestPopulateAssemblyDetailsBogusName()
		{
			var results = _helper.PopulateAssemblyDetails("BogusAssemblyName");
			Assert.AreEqual(0, results.Count());
		}


		private AssemblyInfoHelper _helper;
	}
}

﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using ZaberWpfToolbox;

namespace ZaberWpfToolboxTests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class TimeSpanExtensionTests
	{
		[Test]
		[TestCaseSource("GetPrettyPrintCases")]
		public void TestPrettyPrint(string aExpected, TimeSpan aValue)
		{
			Assert.AreEqual(aExpected, aValue.PrettyPrint());
		}


		public static IEnumerable<object[]> GetPrettyPrintCases()
		{
			return new List<object[]>
			{
				// Zero, singular and multiple cases for each component.
				new object[] { string.Empty, TimeSpan.FromDays(0) },
				new object[] { "1 d", TimeSpan.FromDays(1) },
				new object[] { "2 d", TimeSpan.FromDays(2) },
				new object[] { string.Empty, TimeSpan.FromHours(0) },
				new object[] { "1 h", TimeSpan.FromHours(1) },
				new object[] { "2 h", TimeSpan.FromHours(2) },
				new object[] { string.Empty, TimeSpan.FromMinutes(0) },
				new object[] { "1 m", TimeSpan.FromMinutes(1) },
				new object[] { "2 m", TimeSpan.FromMinutes(2) },
				new object[] { string.Empty, TimeSpan.FromSeconds(0) },
				new object[] { "1 s", TimeSpan.FromSeconds(1) },
				new object[] { "2 s", TimeSpan.FromSeconds(2) },
				new object[] { string.Empty, TimeSpan.FromMilliseconds(0) },
				new object[] { "1 ms", TimeSpan.FromMilliseconds(1) },
				new object[] { "2 ms", TimeSpan.FromMilliseconds(2) },

				// Combination test cases.
				new object[] { "2 h, 15 m, 45 s", new TimeSpan(2, 15, 45) },
				new object[] { "2 h, 1 s", new TimeSpan(2, 0, 1) },
				new object[] { "1 d, 450 ms", new TimeSpan(1, 0, 0, 0, 450) },
			};
		}
	}
}

﻿using System;
using NUnit.Framework;
using ZaberWpfToolbox.Converters;

namespace ZaberWpfToolboxTests.Converters
{
	[TestFixture]
	[SetCulture("en-US")]
	public class ExpressionConverterTests
	{
		[SetUp]
		public void Setup() => _converter = new ExpressionConverter();


		[Test]
		public void TestBooleanExpression()
		{
			object[] args = { true, false };
			_converter.Expression = "a != b";
			var result = _converter.Convert(args, null, null, null);
			Assert.IsTrue(result is bool);
			Assert.IsTrue((bool) result);
		}


		[Test]
		public void TestConstantExpression()
		{
			_converter.Expression = "1 + 2 * 3 - 4 / (1 + 1)";
			var result = _converter.Convert(new object[] { }, null, null, null);
			Assert.IsTrue(result is int);
			Assert.AreEqual(5, (int) result);
		}


		[Test]
		public void TestConvertBack()
			=> Assert.Throws<NotImplementedException>(() => _converter.ConvertBack(null, null, null, null));


		[Test]
		public void TestDefaultConstructor() => Assert.IsTrue(string.IsNullOrEmpty(_converter.Expression));


		[Test]
		public void TestExpressionConstructor()
		{
			var conv = new ExpressionConverter("1 + 2");
			Assert.AreEqual("1 + 2", conv.Expression);
		}


		[Test]
		public void TestFloatExpression()
		{
			object[] args = { 1, 1.0 };
			_converter.Expression = "a + b";
			var result = _converter.Convert(args, null, null, null);
			Assert.IsTrue(result is double);
			Assert.AreEqual(2.0, (double) result);
		}


		[Test]
		public void TestStringExpression()
		{
			_converter.Expression = "(' foo' + 'bar ').Trim()";
			var result = _converter.Convert(null, null, null, null);
			Assert.IsTrue(result is string);
			Assert.AreEqual("foobar", (string) result);
		}


		[Test]
		public void TestTooManyVariables()
		{
			var args = new object[27];
			_converter.Expression = "1 + 2 * 3 - 4 / (1 + 1)";
			Assert.Throws<ArgumentException>(() => _converter.Convert(args, null, null, null));
		}


		[Test]
		public void TestVariables()
		{
			object[] args = { 1, 2, 3, 4 };
			_converter.Expression = "a + b * c - d / (a + a)";
			var result = _converter.Convert(args, null, null, null);
			Assert.IsTrue(result is int);
			Assert.AreEqual(5, (int) result);
		}


		private ExpressionConverter _converter;
	}
}

﻿using System;
using System.Windows;
using NUnit.Framework;
using ZaberWpfToolbox.Converters;

namespace ZaberWpfToolboxTests.Converters
{
	internal class SequentialConverterTests
	{
		#region -- Public Methods & Properties --

		[SetUp]
		public void Setup() => _converter = new SequentialValueConverter();


		[Test]
		public void StringNullToVisibilityTest()
		{
			string conversionString = null;
			_converter.Add(new StringEmptyConverter { NegateResult = true });
			_converter.Add(new ConfigurableBooleanToVisibilityConverter());

			var result = _converter.Convert(conversionString, null, null, null);
			var finalResult = (Visibility) result;

			Assert.AreEqual(Visibility.Collapsed, finalResult);
		}


		[Test]
		public void InvalidOperationTest()
		{
			var invalid = true;
			_converter.Add(new StringEmptyConverter());

			Assert.Throws<InvalidOperationException>(() => _converter.Convert(invalid, null, null, null));
		}


		[Test]
		public void TestConvertBack()
			=> Assert.Throws<NotImplementedException>(() => _converter.ConvertBack(true, null, null, null));

		#endregion

		#region -- Data --

		private SequentialValueConverter _converter;

		#endregion
	}
}

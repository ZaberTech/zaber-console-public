﻿using System.Windows;
using NUnit.Framework;
using ZaberWpfToolbox.Converters;

namespace ZaberWpfToolboxTests.Converters
{
	[TestFixture]
	[SetCulture("en-US")]
	public class EnumToBooleanConverterTests
	{
		[SetUp]
		public void Setup()
		{
			_converter = new EnumToBooleanConverter();
			_converter.NegateResult = false;
		}


		[Test]
		public void TestConvert()
		{
			// Check for equality and negated equality.
			Assert.IsTrue((bool) _converter.Convert(TestEnum.One, null, TestEnum.One, null));
			_converter.NegateResult = true;
			Assert.IsFalse((bool) _converter.Convert(TestEnum.One, null, TestEnum.One, null));
			_converter.NegateResult = false;

			// Check for inequality and negated inequality.
			Assert.IsFalse((bool) _converter.Convert(TestEnum.Two, null, TestEnum.One, null));
			_converter.NegateResult = true;
			Assert.IsTrue((bool) _converter.Convert(TestEnum.Two, null, TestEnum.One, null));
			_converter.NegateResult = false;
		}


		[Test]
		public void TestConvertBack()
		{
			// Test conversion of true to enum value, with and without negation.
			Assert.AreEqual(TestEnum.Two, (TestEnum) _converter.ConvertBack(true, null, TestEnum.Two, null));
			_converter.NegateResult = true;
			Assert.AreEqual(DependencyProperty.UnsetValue, _converter.ConvertBack(true, null, TestEnum.Two, null));
			_converter.NegateResult = false;

			// Test conversion of false to enum value, with and without negation.
			Assert.AreEqual(DependencyProperty.UnsetValue, _converter.ConvertBack(false, null, TestEnum.Two, null));
			_converter.NegateResult = true;
			Assert.AreEqual(TestEnum.Two, (TestEnum) _converter.ConvertBack(false, null, TestEnum.Two, null));
			_converter.NegateResult = false;
		}


		[Test]
		public void TestConvertNull()
		{
			var result = _converter.Convert(null, null, TestEnum.One, null);
			Assert.AreNotEqual(typeof(bool), result.GetType());
			Assert.AreEqual(DependencyProperty.UnsetValue, result);
		}


		[Test]
		public void TestConvertWrongType()
		{
			var result = _converter.Convert(1, null, TestEnum.One, null);
			Assert.AreNotEqual(typeof(bool), result.GetType());
			Assert.AreEqual(DependencyProperty.UnsetValue, result);
		}


		private enum TestEnum
		{
			Zero,
			One,
			Two
		}

		private EnumToBooleanConverter _converter;
	}
}

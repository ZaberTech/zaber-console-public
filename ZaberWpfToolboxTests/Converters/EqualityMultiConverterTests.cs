﻿using System;
using System.Globalization;
using NUnit.Framework;
using ZaberWpfToolbox.Converters;

namespace ZaberWpfToolboxTests.Converters
{
	[TestFixture]
	[SetCulture("en-US")]
	public class EqualityMultiConverterTests
	{
		[SetUp]
		public void Setup()
		{
			_converter = new EqualityMultiConverter();
			_converter.NegateResult = false;
		}


		[Test]
		public void TestCompareReferenceEqualReferenceTypes()
		{
			var c1 = new ReferenceEqualClass();
			var c2 = new ReferenceEqualClass();

			// Sanity check.
			Assert.AreEqual(c1, c1);
			Assert.AreNotEqual(c1, c2);

			// Different instances should compare false.
			Assert.IsFalse((bool)_converter.Convert(new[] { c1, c2 }, null, null, null));
			_converter.NegateResult = true;
			Assert.IsTrue((bool)_converter.Convert(new[] { c1, c2 }, null, null, null));
			_converter.NegateResult = false;

			// Same instance should compare true.
			Assert.IsTrue((bool)_converter.Convert(new[] { c1, c1 }, null, null, null));
			_converter.NegateResult = true;
			Assert.IsFalse((bool)_converter.Convert(new[] { c1, c1 }, null, null, null));
			_converter.NegateResult = false;

			Assert.IsTrue((bool)_converter.Convert(new[] { c1 }, null, null, null));
		}


		[Test]
		public void TestCompareValueEqualReferenceTypes()
		{
			// Test string comparisons.
			Assert.IsFalse((bool)_converter.Convert(new[] { "1", "2", "1" }, null, null, null));
			_converter.NegateResult = true;
			Assert.IsTrue((bool)_converter.Convert(new[] { "1", "2", "1" }, null, null, null));
			_converter.NegateResult = false;

			Assert.IsTrue((bool)_converter.Convert(new[] { "1", "1", "1" }, null, null, null));
			_converter.NegateResult = true;
			Assert.IsFalse((bool)_converter.Convert(new[] { "1", "1", "1" }, null, null, null));
			_converter.NegateResult = false;

			Assert.IsTrue((bool)_converter.Convert(new[] { "1" }, null, null, null));
		}


		[Test]
		public void TestCompareValueTypes()
		{
			// Test comparison of a couple of numeric types.
			Assert.IsFalse((bool)_converter.Convert(new object[] { 1U, 2U }, null, null, null));
			Assert.IsTrue((bool)_converter.Convert(new object[] { 1U, 1U }, null, null, null));
			_converter.NegateResult = true;
			Assert.IsFalse((bool)_converter.Convert(new object[] { 1U, 1U }, null, null, null));
			_converter.NegateResult = false;

			Assert.IsFalse((bool)_converter.Convert(new object[] { -1, -2 }, null, null, null));
			Assert.IsTrue((bool)_converter.Convert(new object[] { -1, -1 }, null, null, null));
			_converter.NegateResult = true;
			Assert.IsFalse((bool)_converter.Convert(new object[] { -1, -1 }, null, null, null));
			_converter.NegateResult = false;

			Assert.IsTrue((bool)_converter.Convert(new object[] { -1 }, null, null, null));
		}


		[Test]
		public void TestConvertBack()
			=> Assert.Throws<NotImplementedException>(
				() => _converter.ConvertBack(true, new [] {typeof(bool)}, true, CultureInfo.CurrentCulture));


		[Test]
		public void TestNullsEqual()
		{
			// Test that null is equal to null and output negation works.
			Assert.IsTrue((bool)_converter.Convert(null, null, null, null));
			Assert.IsTrue((bool)_converter.Convert(new object[] {null}, null, null, null));
			Assert.IsTrue((bool)_converter.Convert(new object[] { null, null }, null, null, null));

			_converter.NegateResult = false;

			// Test that null is not equal to not-null.
			Assert.IsFalse((bool)_converter.Convert(new object[] { null, 1 }, null, null, null));
			Assert.IsFalse((bool)_converter.Convert(new object[] { 0, null }, null, null, null));
		}


		// Class that is only equal to itself if its reference is equal.
		private class ReferenceEqualClass
		{
			#region -- Public Methods & Properties --

			public override bool Equals(object obj) => ReferenceEquals(this, obj);


			public override int GetHashCode() => base.GetHashCode();

			#endregion
		}


		private EqualityMultiConverter _converter;
	}
}

﻿using System;
using NUnit.Framework;
using ZaberWpfToolbox.Converters;

namespace ZaberWpfToolboxTests.Converters
{
	[TestFixture]
	[SetCulture("en-US")]
	public class EnumValueDescriptionConverterTests
	{
		[SetUp]
		public void SetUp() => _converter = new EnumValueDescriptionConverter();


		[Test]
		public void TestConvert()
		{
			var result = _converter.Convert(TestEnum1.TestValue1, null, null, null);
			Assert.AreEqual("Foo", result);
			result = _converter.Convert(TestEnum1.TestValue2, null, null, null);
			Assert.AreEqual("TestValue2", result);
			result = _converter.Convert(TestEnum2.TestValue3, null, null, null);
			Assert.AreEqual("TestValue3", result);
			result = _converter.Convert(TestEnum2.TestValue4, null, null, null);
			Assert.AreEqual("Bar", result);
		}


		[Test]
		public void TestConvertBack()
			=> Assert.Throws<NotImplementedException>(() => _converter.ConvertBack("Description", null, null, null));


		[Test]
		public void TestConvertWrongType()
		{
			var result = _converter.Convert(1, null, null, null);
			Assert.AreEqual(string.Empty, result);
		}


		private enum TestEnum1
		{
			[System.ComponentModel.Description("Foo")]
			TestValue1,

			TestValue2
		}


		private enum TestEnum2
		{
			TestValue3,

			[System.ComponentModel.Description("Bar")]
			TestValue4
		}

		private EnumValueDescriptionConverter _converter;
	}
}

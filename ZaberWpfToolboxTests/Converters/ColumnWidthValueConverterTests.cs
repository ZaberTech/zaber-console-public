﻿using System;
using System.Windows;
using NUnit.Framework;
using ZaberWpfToolbox.Converters;

namespace ZaberWpfToolboxTests.Converters
{
	[TestFixture]
	[SetCulture("en-US")]
	public class ColumnWidthValueConverterTests
	{
		[SetUp]
		public void Setup() => _converter = new ColumnWidthValueConverter();


		[Test]
		public void TestConstructor()
		{
			Assert.AreEqual(-1.0, _converter.Offset);
			Assert.AreEqual(1, _converter.NumColumns);
		}


		[Test]
		public void TestConvert()
		{
			var result = (GridLength) _converter.Convert(40.0, null, null, null);
			Assert.IsNotNull(result);
			Assert.AreEqual(GridUnitType.Pixel, result.GridUnitType);
			Assert.AreEqual(39.0, result.Value);

			_converter.NumColumns = 2;
			result = (GridLength) _converter.Convert(40.0, null, null, null);
			Assert.AreEqual(19.0, result.Value);

			_converter.Offset = -2.0;
			result = (GridLength) _converter.Convert(40.0, null, null, null);
			Assert.AreEqual(18.0, result.Value);
		}


		[Test]
		public void TestConvertBack()
			=> Assert.Throws<NotImplementedException>(() => _converter.ConvertBack(true, null, null, null));


		[Test]
		public void TestConvertNull()
			=> Assert.Throws<NullReferenceException>(() => _converter.Convert(null, null, null, null));


		[Test]
		public void TestConvertWrongType()
			=> Assert.Throws<InvalidCastException>(() => _converter.Convert("Foo", null, null, null));


		private ColumnWidthValueConverter _converter;
	}
}

﻿using System;
using NUnit.Framework;
using ZaberWpfToolbox.Converters;

namespace ZaberWpfToolboxTests.Converters
{
	[TestFixture]
	[SetCulture("en-US")]
	public class IsNullConverterTests
	{
		[SetUp]
		public void SetUp() => _converter = new IsNullConverter();


		[Test]
		public void TestConvertBack()
			=> Assert.Throws<NotSupportedException>(() => _converter.ConvertBack(null, null, null, null));


		[Test]
		public void TestNormalOperation()
		{
			// Null input should return true.
			Assert.IsTrue((bool) _converter.Convert(null, null, null, null));

			// Non-null input should return false.
			Assert.IsFalse((bool) _converter.Convert(true, null, null, null));

			// Test negated output.
			_converter.NegateResult = true;
			Assert.IsFalse((bool) _converter.Convert(null, null, null, null));
			Assert.IsTrue((bool) _converter.Convert(true, null, null, null));
		}


		private IsNullConverter _converter;
	}
}

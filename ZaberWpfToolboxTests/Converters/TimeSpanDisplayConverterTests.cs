﻿using System;
using NUnit.Framework;
using ZaberWpfToolbox.Converters;

namespace ZaberWpfToolboxTests.Converters
{
	[TestFixture]
	[SetCulture("en-US")]
	public class TimeSpanDisplayConverterTests
	{
		[SetUp]
		public void SetUp() => _converter = new TimeSpanDisplayConverter();


		[Test]
		public void TestConvert()
		{
			var result = _converter.Convert(new TimeSpan(0, 0, 1, 2, 3), null, null, null);
			Assert.AreEqual("1 m, 2 s, 3 ms", result);
			result = _converter.Convert(null, null, null, null);
			Assert.IsNull(result);
		}


		[Test]
		public void TestConvertBack()
			=> Assert.Throws<NotSupportedException>(() => _converter.ConvertBack("Foo", null, null, null));


		[Test]
		public void TestConvertWrongType()
			=> Assert.Throws<InvalidOperationException>(() => _converter.Convert("Foo", null, null, null));


		private TimeSpanDisplayConverter _converter;
	}
}

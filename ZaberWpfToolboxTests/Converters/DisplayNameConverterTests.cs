﻿using System;
using System.ComponentModel;
using NUnit.Framework;
using ZaberWpfToolbox.Converters;

namespace ZaberWpfToolboxTests.Converters
{
	[TestFixture]
	[SetCulture("en-US")]
	public class DisplayNameConverterTests
	{
		[SetUp]
		public void SetUp() => _converter = new DisplayNameConverter();


		[Test]
		public void TestConvert()
		{
			var result = _converter.Convert(typeof(TestClass1), null, null, null);
			Assert.AreEqual("Bar", result);
			result = _converter.Convert(new TestClass1(), null, null, null);
			Assert.AreEqual("Bar", result);
			result = _converter.Convert(typeof(TestClass2), null, null, null);
			Assert.AreEqual("TestClass2", result);
			result = _converter.Convert(new TestClass2(), null, null, null);
			Assert.AreEqual("TestClass2", result);
			result = _converter.Convert(null, null, null, null);
			Assert.IsNull(result);
		}


		[Test]
		public void TestConvertBack()
			=> Assert.Throws<NotImplementedException>(() => _converter.ConvertBack("Foo", null, null, null));


		[DisplayName("Bar")]
		private class TestClass1
		{
		}

		private class TestClass2
		{
		}


		private DisplayNameConverter _converter;
	}
}

﻿using System;
using System.Globalization;
using NUnit.Framework;
using ZaberWpfToolbox.Converters;

namespace ZaberWpfToolboxTests.Converters
{
	[TestFixture]
	[SetCulture("en-US")]
	public class BooleanAndConverterTests
	{
		[SetUp]
		public void SetUp()
		{
			_converter = new BooleanAndConverter();
			_converter.NegateResult = false;
		}


		[Test]
		public void TestConvertBack() => Assert.Throws<NotImplementedException>(
			() => _converter.ConvertBack(true, new[] { typeof(bool) }, true, CultureInfo.CurrentCulture));


		[Test]
		public void TestMultiInput()
		{
			object[] inputs = { false, false, false };

			// If any input is false, the result should be false.
			Assert.IsFalse((bool) _converter.Convert(inputs, null, null, null));
			inputs[1] = true;
			inputs[2] = true;
			Assert.IsFalse((bool) _converter.Convert(inputs, null, null, null));

			// With negated result, should return true.
			_converter.NegateResult = true;
			Assert.IsTrue((bool) _converter.Convert(inputs, null, null, null));

			// If all inputs are true, the result should be true.
			_converter.NegateResult = false;
			inputs[0] = true;
			Assert.IsTrue((bool) _converter.Convert(inputs, null, null, null));

			// With negated result, should return false.
			_converter.NegateResult = true;
			Assert.IsFalse((bool) _converter.Convert(inputs, null, null, null));

			// Non-boolean input should return false.
			_converter.NegateResult = false;
			inputs[1] = 1;
			Assert.IsFalse((bool) _converter.Convert(inputs, null, null, null));

			// With negated result, should return true.
			_converter.NegateResult = true;
			Assert.IsTrue((bool) _converter.Convert(inputs, null, null, null));
		}


		[Test]
		public void TestNonBooleanReturn()
		{
			object[] inputs = { false };
			_converter.TrueValue = "Foo";
			_converter.FalseValue = 1.23;

			Assert.AreEqual(1.23, _converter.Convert(inputs, null, null, null));

			inputs[0] = true;
			Assert.AreEqual("Foo", _converter.Convert(inputs, null, null, null));
		}


		[Test]
		public void TestSingleInput()
		{
			object[] inputs = { false };

			// Single false input should return false.
			Assert.IsFalse((bool) _converter.Convert(inputs, null, null, null));

			// With negated result, should return true.
			_converter.NegateResult = true;
			Assert.IsTrue((bool) _converter.Convert(inputs, null, null, null));

			// Single true input should return true.
			_converter.NegateResult = false;
			inputs[0] = true;
			Assert.IsTrue((bool) _converter.Convert(inputs, null, null, null));

			// With negated result, should return false.
			_converter.NegateResult = true;
			Assert.IsFalse((bool) _converter.Convert(inputs, null, null, null));

			// Non-boolean input should return false.
			_converter.NegateResult = false;
			inputs[0] = 1;
			Assert.IsFalse((bool) _converter.Convert(inputs, null, null, null));

			// With negated result, should return true.
			_converter.NegateResult = true;
			Assert.IsTrue((bool) _converter.Convert(inputs, null, null, null));
		}


		private BooleanAndConverter _converter;
	}
}

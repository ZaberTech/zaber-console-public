﻿using System;
using NUnit.Framework;
using ZaberWpfToolbox.Converters;

namespace ZaberWpfToolboxTests.Converters
{
	[TestFixture]
	[SetCulture("en-US")]
	public class StringEmptyConverterTests
	{
		[SetUp]
		public void Setup() => _converter = new StringEmptyConverter();


		[Test]
		public void TestConstructor() => Assert.IsFalse(_converter.NegateResult);


		[Test]
		public void TestConvertBack()
			=> Assert.Throws<NotSupportedException>(() => _converter.ConvertBack(true, null, null, null));


		[Test]
		public void TestConvertEmpty()
		{
			var result = _converter.Convert(string.Empty, null, null, null);
			Assert.IsTrue(result is bool);
			var b = (bool) result;
			Assert.IsTrue(b);

			// Test behavior with result negated.
			_converter.NegateResult = true;
			result = _converter.Convert(string.Empty, null, null, null);
			Assert.IsTrue(result is bool);
			b = (bool) result;
			Assert.IsFalse(b);
		}


		[Test]
		public void TestConvertNonEmpty()
		{
			var result = _converter.Convert(" ", null, null, null);
			Assert.IsTrue(result is bool);
			var b = (bool) result;
			Assert.IsFalse(b);

			// Test behavior with result negated.
			_converter.NegateResult = true;
			result = _converter.Convert(" ", null, null, null);
			Assert.IsTrue(result is bool);
			b = (bool) result;
			Assert.IsTrue(b);
		}


		[Test]
		public void TestConvertNonString()
			=> Assert.Throws<InvalidOperationException>(() => _converter.Convert(0, null, null, null));


		[Test]
		public void TestConvertNull()
		{
			var result = _converter.Convert(null, null, null, null);
			Assert.IsTrue(result is bool);
			var b = (bool) result;
			Assert.IsTrue(b);

			// Test behavior with result negated.
			_converter.NegateResult = true;
			result = _converter.Convert(null, null, null, null);
			Assert.IsTrue(result is bool);
			b = (bool) result;
			Assert.IsFalse(b);
		}


		private StringEmptyConverter _converter;
	}
}

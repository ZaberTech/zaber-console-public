﻿using System;
using System.Globalization;
using NUnit.Framework;
using ZaberWpfToolbox.Converters;

namespace ZaberWpfToolboxTests.Converters
{
	[TestFixture]
	[SetCulture("en-US")]
	public class EqualityConverterTests
	{
		[SetUp]
		public void Setup()
		{
			_converter = new EqualityConverter();
			_converter.NegateResult = false;
		}


		[Test]
		public void TestCompareReferenceEqualReferenceTypes()
		{
			var c1 = new ReferenceEqualClass();
			var c2 = new ReferenceEqualClass();

			// Sanity check.
			Assert.AreEqual(c1, c1);
			Assert.AreNotEqual(c1, c2);

			// Different instances should compare false.
			Assert.IsFalse((bool) _converter.Convert(c1, null, c2, null));
			_converter.NegateResult = true;
			Assert.IsTrue((bool) _converter.Convert(c1, null, c2, null));
			_converter.NegateResult = false;

			// Same instance should compare true.
			Assert.IsTrue((bool) _converter.Convert(c1, null, c1, null));
			_converter.NegateResult = true;
			Assert.IsFalse((bool) _converter.Convert(c1, null, c1, null));
			_converter.NegateResult = false;
		}


		[Test]
		public void TestCompareValueEqualReferenceTypes()
		{
			// Test string comparisons.
			Assert.IsFalse((bool) _converter.Convert("1", null, "2", null));
			_converter.NegateResult = true;
			Assert.IsTrue((bool) _converter.Convert("1", null, "2", null));
			_converter.NegateResult = false;

			Assert.IsTrue((bool) _converter.Convert("1", null, "1", null));
			_converter.NegateResult = true;
			Assert.IsFalse((bool) _converter.Convert("1", null, "1", null));
			_converter.NegateResult = false;
		}


		[Test]
		public void TestCompareValueTypes()
		{
			// Test comparison of a couple of numeric types.
			Assert.IsFalse((bool) _converter.Convert(1U, null, 2U, null));
			Assert.IsTrue((bool) _converter.Convert(1U, null, 1U, null));
			_converter.NegateResult = true;
			Assert.IsFalse((bool) _converter.Convert(1U, null, 1U, null));
			_converter.NegateResult = false;

			Assert.IsFalse((bool) _converter.Convert(-1, null, -2, null));
			Assert.IsTrue((bool) _converter.Convert(-1, null, -1, null));
			_converter.NegateResult = true;
			Assert.IsFalse((bool) _converter.Convert(-1, null, -1, null));
			_converter.NegateResult = false;
		}


		[Test]
		public void TestConvertBack()
			=> Assert.Throws<NotImplementedException>(
				() => _converter.ConvertBack(true, typeof(bool), true, CultureInfo.CurrentCulture));


		[Test]
		public void TestNullsEqual()
		{
			// Test that null is equal to null and output negation works.
			Assert.IsTrue((bool) _converter.Convert(null, null, null, null));
			_converter.NegateResult = true;
			Assert.IsFalse((bool) _converter.Convert(null, null, null, null));

			_converter.NegateResult = false;

			// Test that null is not equal to not-null.
			Assert.IsFalse((bool) _converter.Convert(null, null, 1, null));
			Assert.IsFalse((bool) _converter.Convert(0, null, null, null));
			_converter.NegateResult = true;
			Assert.IsTrue((bool) _converter.Convert(0, null, null, null));
			_converter.NegateResult = false;
		}


		// Class that is only equal to itself if its reference is equal.
		private class ReferenceEqualClass
		{
			#region -- Public Methods & Properties --

			public override bool Equals(object obj) => ReferenceEquals(this, obj);


			public override int GetHashCode() => base.GetHashCode();

			#endregion
		}


		private EqualityConverter _converter;
	}
}

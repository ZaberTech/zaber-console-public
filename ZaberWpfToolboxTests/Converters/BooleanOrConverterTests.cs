﻿using System;
using System.Globalization;
using NUnit.Framework;
using ZaberWpfToolbox.Converters;

namespace ZaberWpfToolboxTests.Converters
{
	[TestFixture]
	[SetCulture("en-US")]
	public class BooleanOrConverterTests
	{
		[SetUp]
		public void SetUp()
		{
			_converter = new BooleanOrConverter();
			_converter.NegateResult = false;
		}


		[Test]
		public void TestConvertBack() => Assert.Throws<NotImplementedException>(
			() => _converter.ConvertBack(true, new[] { typeof(bool) }, true, CultureInfo.CurrentCulture));


		[Test]
		public void TestMultiInput()
		{
			object[] inputs = { false, false, false };

			// If all inputs are false, the result should be false.
			Assert.IsFalse((bool) _converter.Convert(inputs, null, null, null));

			// With negated result, should return true.
			_converter.NegateResult = true;
			Assert.IsTrue((bool) _converter.Convert(inputs, null, null, null));

			for (var i = 1; i < 8; ++i)
			{
				inputs[0] = (i & 1) != 0;
				inputs[1] = (i & 2) != 0;
				inputs[2] = (i & 4) != 0;

				// If any input is true, the result should be true.
				_converter.NegateResult = false;
				Assert.IsTrue((bool) _converter.Convert(inputs, null, null, null));

				// Or false with negated output.
				_converter.NegateResult = true;
				Assert.IsFalse((bool) _converter.Convert(inputs, null, null, null));
			}

			// Non-boolean inputs should be ignored.
			_converter.NegateResult = false;
			inputs[0] = false;
			inputs[1] = 1;
			inputs[2] = false;
			Assert.IsFalse((bool) _converter.Convert(inputs, null, null, null));
			inputs[2] = true;
			Assert.IsTrue((bool) _converter.Convert(inputs, null, null, null));
			_converter.NegateResult = true;
			Assert.IsFalse((bool) _converter.Convert(inputs, null, null, null));
		}


		[Test]
		public void TestNonBooleanReturn()
		{
			object[] inputs = { false };
			_converter.TrueValue = "Foo";
			_converter.FalseValue = 1.23;

			Assert.AreEqual(1.23, _converter.Convert(inputs, null, null, null));

			inputs[0] = true;
			Assert.AreEqual("Foo", _converter.Convert(inputs, null, null, null));
		}


		[Test]
		public void TestSingleInput()
		{
			object[] inputs = { false };

			// Single false input should return false.
			Assert.IsFalse((bool) _converter.Convert(inputs, null, null, null));

			// With negated result, should return true.
			_converter.NegateResult = true;
			Assert.IsTrue((bool) _converter.Convert(inputs, null, null, null));

			// Single true input should return true.
			_converter.NegateResult = false;
			inputs[0] = true;
			Assert.IsTrue((bool) _converter.Convert(inputs, null, null, null));

			// With negated result, should return false.
			_converter.NegateResult = true;
			Assert.IsFalse((bool) _converter.Convert(inputs, null, null, null));

			// Non-boolean input should return false.
			_converter.NegateResult = false;
			inputs[0] = 1;
			Assert.IsFalse((bool) _converter.Convert(inputs, null, null, null));

			// With negated result, should return true.
			_converter.NegateResult = true;
			Assert.IsTrue((bool) _converter.Convert(inputs, null, null, null));
		}


		private BooleanOrConverter _converter;
	}
}

﻿using System;
using System.Windows;
using NUnit.Framework;
using ZaberWpfToolbox.Converters;

namespace ZaberWpfToolboxTests.Converters
{
	[TestFixture]
	[SetCulture("en-US")]
	public class ConfigurableBooleanToVisibilityConverterTests
	{
		[SetUp]
		public void Setup() => _converter = new ConfigurableBooleanToVisibilityConverter();


		// Check that the default settings haven't changed.
		[Test]
		public void TestConstructor()
		{
			Assert.IsFalse(_converter.ReverseBehavior);
			Assert.AreEqual(Visibility.Collapsed, _converter.HiddenState);
		}


		// Test backwards conversion with wrong input type.
		[Test]
		public void TestConverBackNonVisibilityInput()
			=> Assert.Throws<InvalidOperationException>(() => _converter.ConvertBack(0, null, null, null));


		// Test backwards conversion with default behavior.
		[Test]
		public void TestConvertBack()
		{
			var result = _converter.ConvertBack(Visibility.Collapsed, null, null, null);
			Assert.IsTrue(result is bool);
			var b = (bool) result;
			Assert.IsFalse(b);

			result = _converter.ConvertBack(Visibility.Hidden, null, null, null);
			Assert.IsTrue(result is bool);
			b = (bool) result;
			Assert.IsFalse(b);

			result = _converter.ConvertBack(Visibility.Visible, null, null, null);
			Assert.IsTrue(result is bool);
			b = (bool) result;
			Assert.IsTrue(b);
		}


		// Test backwards conversion with inverted behavior.
		[Test]
		public void TestConvertBackInverted()
		{
			_converter.ReverseBehavior = true;

			var result = _converter.ConvertBack(Visibility.Collapsed, null, null, null);
			Assert.IsTrue(result is bool);
			var b = (bool) result;
			Assert.IsTrue(b);

			result = _converter.ConvertBack(Visibility.Hidden, null, null, null);
			Assert.IsTrue(result is bool);
			b = (bool) result;
			Assert.IsTrue(b);

			result = _converter.ConvertBack(Visibility.Visible, null, null, null);
			Assert.IsTrue(result is bool);
			b = (bool) result;
			Assert.IsFalse(b);
		}


		// Verify that by default the converter behaves like a system BooleanToVisibilityConverter.
		[Test]
		public void TestDefaultBehavior()
		{
			var result = _converter.Convert(false, null, null, null);
			Assert.IsTrue(result is Visibility);
			var v = (Visibility) result;
			Assert.AreEqual(Visibility.Collapsed, v);

			result = _converter.Convert(true, null, null, null);
			Assert.IsTrue(result is Visibility);
			v = (Visibility) result;
			Assert.AreEqual(Visibility.Visible, v);
		}


		// Verify alternate hidden behavior.
		[Test]
		public void TestHiddenBehavior()
		{
			_converter.HiddenState = Visibility.Hidden;

			var result = _converter.Convert(false, null, null, null);
			Assert.IsTrue(result is Visibility);
			var v = (Visibility) result;
			Assert.AreEqual(Visibility.Hidden, v);

			result = _converter.Convert(true, null, null, null);
			Assert.IsTrue(result is Visibility);
			v = (Visibility) result;
			Assert.AreEqual(Visibility.Visible, v);
		}


		// Verify inverted default behavior
		[Test]
		public void TestinvertedDefaultBehavior()
		{
			_converter.ReverseBehavior = true;

			var result = _converter.Convert(false, null, null, null);
			Assert.IsTrue(result is Visibility);
			var v = (Visibility) result;
			Assert.AreEqual(Visibility.Visible, v);

			result = _converter.Convert(true, null, null, null);
			Assert.IsTrue(result is Visibility);
			v = (Visibility) result;
			Assert.AreEqual(Visibility.Collapsed, v);
		}


		// Verify alternate hidden behavior with inverted input.
		[Test]
		public void TestInvertedHiddenBehavior()
		{
			_converter.HiddenState = Visibility.Hidden;
			_converter.ReverseBehavior = true;

			var result = _converter.Convert(false, null, null, null);
			Assert.IsTrue(result is Visibility);
			var v = (Visibility) result;
			Assert.AreEqual(Visibility.Visible, v);

			result = _converter.Convert(true, null, null, null);
			Assert.IsTrue(result is Visibility);
			v = (Visibility) result;
			Assert.AreEqual(Visibility.Hidden, v);
		}


		// Test error case of non-bool input.
		[Test]
		public void TestNonBoolInput()
			=> Assert.Throws<InvalidOperationException>(() => _converter.Convert(0, null, null, null));


		private ConfigurableBooleanToVisibilityConverter _converter;
	}
}

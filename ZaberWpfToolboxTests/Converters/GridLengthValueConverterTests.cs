﻿using System;
using System.Windows;
using NUnit.Framework;
using ZaberWpfToolbox.Converters;

namespace ZaberWpfToolboxTests.Converters
{
	[TestFixture]
	[SetCulture("en-US")]
	public class GridLengthValueConverterTests
	{
		[SetUp]
		public void Setup() => _converter = new GridLengthValueConverter();


		[Test]
		public void TestClamp()
		{
			var result = _converter.Convert("-3*", null, "1.0", null);
			Assert.IsTrue(result is GridLength);
			var gl = (GridLength) result;
			Assert.AreEqual(1.0, gl.Value);
			Assert.IsFalse(gl.IsAbsolute);
			Assert.IsFalse(gl.IsAuto);
			Assert.IsTrue(gl.IsStar);
		}


		[Test]
		public void TestConvertAmbiguous()
		{
			var result = _converter.Convert("7", null, null, null);
			Assert.IsTrue(result is GridLength);
			var gl = (GridLength) result;
			Assert.AreEqual(7.0, gl.Value);
			Assert.IsTrue(gl.IsAbsolute);
			Assert.IsFalse(gl.IsAuto);
			Assert.IsFalse(gl.IsStar);
		}


		[Test]
		public void TestConvertAuto()
		{
			var result = _converter.Convert("Auto", null, null, null);
			Assert.IsTrue(result is GridLength);
			var gl = (GridLength) result;
			Assert.AreEqual(1.0, gl.Value);
			Assert.IsFalse(gl.IsAbsolute);
			Assert.IsTrue(gl.IsAuto);
			Assert.IsFalse(gl.IsStar);
		}


		[Test]
		public void TestConvertBackAuto()
		{
			var gl = new GridLength(1.0, GridUnitType.Auto);
			var result = _converter.ConvertBack(gl, null, null, null);
			Assert.IsTrue(result is string);
			var s = result as string;
			Assert.AreEqual("Auto", s);
		}


		[Test]
		public void TestConvertBackPx()
		{
			var gl = new GridLength(3.0, GridUnitType.Pixel);
			var result = _converter.ConvertBack(gl, null, null, null);
			Assert.IsTrue(result is string);
			var s = result as string;
			Assert.AreEqual("3", s);
		}


		[Test]
		public void TestConvertBackStar()
		{
			var gl = new GridLength(9.0, GridUnitType.Star);
			var result = _converter.ConvertBack(gl, null, null, null);
			Assert.IsTrue(result is string);
			var s = result as string;
			Assert.AreEqual("9*", s);
		}


		[Test]
		public void TestConvertBackWrongType()
			=> Assert.Throws<NotSupportedException>(() => _converter.ConvertBack("Foo", null, null, null));


		[Test]
		public void TestConvertBogusString()
			=> Assert.Throws<FormatException>(() => _converter.Convert("Guacamole", null, null, null));


		[Test]
		public void TestConvertNonString()
			=> Assert.Throws<NotSupportedException>(() => _converter.Convert(14, null, null, null));


		[Test]
		public void TestConvertPx()
		{
			var result = _converter.Convert("5px", null, null, null);
			Assert.IsTrue(result is GridLength);
			var gl = (GridLength) result;
			Assert.AreEqual(5.0, gl.Value);
			Assert.IsTrue(gl.IsAbsolute);
			Assert.IsFalse(gl.IsAuto);
			Assert.IsFalse(gl.IsStar);
		}


		[Test]
		public void TestConvertStar()
		{
			var result = _converter.Convert("3*", null, null, null);
			Assert.IsTrue(result is GridLength);
			var gl = (GridLength) result;
			Assert.AreEqual(3.0, gl.Value);
			Assert.IsFalse(gl.IsAbsolute);
			Assert.IsFalse(gl.IsAuto);
			Assert.IsTrue(gl.IsStar);
		}


		private GridLengthValueConverter _converter;
	}
}

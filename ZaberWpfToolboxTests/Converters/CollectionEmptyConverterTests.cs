﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using NUnit.Framework;
using ZaberWpfToolbox.Converters;

namespace ZaberWpfToolboxTests.Converters
{
	[TestFixture]
	[SetCulture("en-US")]
	public class CollectionEmptyConverterTests
	{
		[SetUp]
		public void Setup() => _converter = new CollectionEmptyConverter();


		[Test]
		public void TestConstructor() => Assert.IsFalse(_converter.NegateResult);


		[Test]
		public void TestConvertBack()
			=> Assert.Throws<NotSupportedException>(() => _converter.ConvertBack(true, null, null, null));


		[Test]
		public void TestConvertEmpty()
		{
			var empty = new List<string>();
			var result = _converter.Convert(empty, null, null, null);
			Assert.IsTrue(result is bool);
			var b = (bool) result;
			Assert.IsTrue(b);

			// Test behavior with result negated.
			_converter.NegateResult = true;
			result = _converter.Convert(empty, null, null, null);
			Assert.IsTrue(result is bool);
			b = (bool) result;
			Assert.IsFalse(b);
		}


		[Test]
		public void TestConvertNonCollection()
			=> Assert.Throws<InvalidOperationException>(() => _converter.Convert(0, null, null, null));


		[Test]
		public void TestConvertNonEmpty()
		{
			var list = new List<string>();
			list.Add("Foo");

			var result = _converter.Convert(list, null, null, null);
			Assert.IsTrue(result is bool);
			var b = (bool) result;
			Assert.IsFalse(b);

			// Test behavior with result negated.
			_converter.NegateResult = true;
			result = _converter.Convert(list, null, null, null);
			Assert.IsTrue(result is bool);
			b = (bool) result;
			Assert.IsTrue(b);
		}


		[Test]
		public void TestConvertNull()
		{
			var result = _converter.Convert(null, null, null, null);
			Assert.IsTrue(result is bool);
			var b = (bool) result;
			Assert.IsTrue(b);

			// Test behavior with result negated.
			_converter.NegateResult = true;
			result = _converter.Convert(null, null, null, null);
			Assert.IsTrue(result is bool);
			b = (bool) result;
			Assert.IsFalse(b);
		}


		[Test]
		public void TestDifferentTypes()
		{
			var list = new List<string>();
			list.Add("Foo");
			var result = _converter.Convert(list, null, null, null);
			Assert.IsTrue(result is bool);
			var b = (bool) result;
			Assert.IsFalse(b);

			var list2 = new StringCollection();
			list2.Add("Bar");
			result = _converter.Convert(list2, null, null, null);
			Assert.IsTrue(result is bool);
			b = (bool) result;
			Assert.IsFalse(b);

			var array = new object[] { 1, "Baz" };
			result = _converter.Convert(array, null, null, null);
			Assert.IsTrue(result is bool);
			b = (bool) result;
			Assert.IsFalse(b);

			var dict = new Dictionary<string, int>();
			dict.Add("Quux", 4);
			result = _converter.Convert(dict, null, null, null);
			Assert.IsTrue(result is bool);
			b = (bool) result;
			Assert.IsFalse(b);

			result = _converter.Convert("Strings are enumerable too!", null, null, null);
			Assert.IsTrue(result is bool);
			b = (bool) result;
			Assert.IsFalse(b);
		}


		private CollectionEmptyConverter _converter;
	}
}

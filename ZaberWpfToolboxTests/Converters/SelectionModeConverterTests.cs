﻿using System;
using System.Windows.Controls;
using NUnit.Framework;
using ZaberWpfToolbox.Converters;

namespace ZaberWpfToolboxTests.Converters
{
	[TestFixture]
	[SetCulture("en-US")]
	public class SelectionModeConverterTests
	{
		[SetUp]
		public void Setup() => _conv = new SelectionModeConverter();


		[Test]
		public void TestConvert()
		{
			var result = _conv.Convert(false, null, null, null);
			Assert.IsNotNull(result);
			Assert.IsTrue(result is SelectionMode);
			var mode = (SelectionMode) result;
			Assert.AreEqual(SelectionMode.Single, result);

			result = _conv.Convert(true, null, null, null);
			Assert.IsNotNull(result);
			Assert.IsTrue(result is SelectionMode);
			mode = (SelectionMode) result;
			Assert.AreEqual(SelectionMode.Extended, result);
		}


		[Test]
		public void TestConvertBack()
		{
			var result = _conv.ConvertBack(SelectionMode.Single, null, null, null);
			Assert.IsNotNull(result);
			Assert.IsTrue(result is bool);
			var b = (bool) result;
			Assert.IsFalse(b);

			result = _conv.ConvertBack(SelectionMode.Multiple, null, null, null);
			Assert.IsNotNull(result);
			Assert.IsTrue(result is bool);
			b = (bool) result;
			Assert.IsTrue(b);

			result = _conv.ConvertBack(SelectionMode.Extended, null, null, null);
			Assert.IsNotNull(result);
			Assert.IsTrue(result is bool);
			b = (bool) result;
			Assert.IsTrue(b);
		}


		[Test]
		public void TestConvertBackWrongType()
			=> Assert.Throws<ArgumentException>(() => _conv.ConvertBack(true, null, null, null));


		[Test]
		public void TestConvertWrongType()
			=> Assert.Throws<ArgumentException>(() => _conv.Convert(14, null, null, null));


		private SelectionModeConverter _conv;
	}
}

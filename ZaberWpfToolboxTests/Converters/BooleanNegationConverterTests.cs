﻿using System;
using NUnit.Framework;
using ZaberWpfToolbox.Converters;

namespace ZaberWpfToolboxTests.Converters
{
	[TestFixture]
	[SetCulture("en-US")]
	public class BooleanNegationConverterTests
	{
		[SetUp]
		public void SetUp() => _converter = new BooleanNegationConverter();


		[Test]
		public void ConvertBack_TestNormalOperation()
		{
			Assert.IsTrue((bool) _converter.Convert(false, typeof(bool), null, null));

			Assert.False((bool) _converter.Convert(true, typeof(bool), null, null));
		}


		[Test]
		public void ConvertBack_TestWrongTargetType()
			=> Assert.Throws<InvalidOperationException>(() => _converter.Convert(false, typeof(int), null, null));


		[Test]
		public void TestNormalOperation()
		{
			// False input should return true.
			Assert.IsTrue((bool) _converter.Convert(false, typeof(bool), null, null));

			// True input should return false.
			Assert.IsFalse((bool) _converter.Convert(true, typeof(bool), null, null));

			// Types assignable from bool should not cause exceptions.
			_converter.Convert(true, typeof(object), null, null);
			_converter.Convert(true, typeof(bool?), null, null);
		}


		[Test]
		public void TestWrongInputType()
			=> Assert.Throws<InvalidCastException>(() => _converter.Convert(1.0f, typeof(bool), null, null));


		[Test]
		public void TestWrongTargetType()
			=> Assert.Throws<InvalidOperationException>(() => _converter.Convert(false, typeof(int), null, null));


		[Test]
		public void WrongInputType()
			=> Assert.Throws<InvalidCastException>(() => _converter.Convert("zaber", typeof(bool), null, null));


		private BooleanNegationConverter _converter;
	}
}

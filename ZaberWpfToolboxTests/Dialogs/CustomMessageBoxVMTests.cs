﻿using System;
using System.Collections.Generic;
using System.Windows;
using NUnit.Framework;
using Zaber.Testing;
using ZaberWpfToolbox.Dialogs;
using ZaberWpfToolbox.Settings;

namespace ZaberWpfToolboxTests.Dialogs
{
	[TestFixture]
	[SetCulture("en-US")]
	public class CustomMessageBoxVMTests
	{
		[SetUp]
		public void Setup()
		{
			_vm = new CustomMessageBoxVM("message", "title");
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
		}


		[TearDown]
		public void Teardown() => _listener.Unlisten(_vm);


		[Test]
		public void ButtonPressSetsDoNotShowResult()
		{
			var settings = new DoNotShowAgainDialogSettings();
			settings.ShowDialog = false;
			settings.LastSelection = null;
			_vm.DoNotShowAgainSettings = settings;

			var cmd = _vm.ButtonClickCommand;
			Assert.IsTrue(cmd.CanExecute(42));
			cmd.Execute(42);
			Assert.IsFalse(settings.ShowDialog);
			Assert.AreEqual(42, settings.LastSelection);
			Assert.AreEqual(42, _vm.DialogResult);
		}


		[Test]
		public void ClosingWindowCancelsDoNotShow()
		{
			var settings = new DoNotShowAgainDialogSettings();
			settings.ShowDialog = false;
			settings.LastSelection = 42;
			_vm.DoNotShowAgainSettings = settings;
			_vm.DialogResult = 14;

			_vm.RequestClose();
			Assert.IsTrue(settings.ShowDialog);
			Assert.IsNull(settings.LastSelection);
			Assert.AreEqual(14, _vm.DialogResult);
		}


		[Test]
		public void TestButtonCommand()
		{
			var invocationCount = 0;

			EventHandler handler = (aSender, aArgs) => { ++invocationCount; };

			_vm.DialogClosing += handler;

			var cmd = _vm.ButtonClickCommand;
			Assert.IsTrue(cmd.CanExecute(null));
			cmd.Execute(null);
			_vm.DialogClosing -= handler;
			Assert.AreEqual(1, invocationCount);
			Assert.AreEqual(null, _vm.DialogResult);
		}


		[Test]
		public void TestCanBeNonModal()
		{
			Assert.IsTrue(_vm.IsModal);
			_vm.IsModal = false;
			Assert.IsFalse(_vm.IsModal);
		}


		[Test]
		public void TestConstructor()
		{
			Assert.AreEqual("title", _vm.Title);
			Assert.AreEqual("message", _vm.Message);
			var options = new List<CustomMessageBoxVM.Option>(_vm.Options);
			Assert.AreEqual(0, options.Count);
			Assert.IsNull(_vm.DoNotShowAgainSettings);
			Assert.IsNull(_vm.Exception);
		}


		[Test]
		public void TestDefaultOptionName()
		{
			_vm.AddOption("Foo", 1);
			_vm.AddOption("Bar", 2);
			_vm.DialogResult = 0;
			Assert.IsNull(_vm.FocusedButtonLabel);
			_vm.DialogResult = 1;
			Assert.AreEqual("Foo", _vm.FocusedButtonLabel);
			_vm.DialogResult = 2;
			Assert.AreEqual("Bar", _vm.FocusedButtonLabel);
			_vm.DialogResult = 3;
			Assert.IsNull(_vm.FocusedButtonLabel);
		}


		[Test]
		public void TestLegacyConstructor()
		{
			_vm = new CustomMessageBoxVM("message", "title", "left", "right");
			Assert.AreEqual("title", _vm.Title);
			Assert.AreEqual("message", _vm.Message);
			var options = new List<CustomMessageBoxVM.Option>(_vm.Options);
			Assert.AreEqual(2, options.Count);
			Assert.AreEqual("left", options[0].Label);
			Assert.AreEqual("right", options[1].Label);
			Assert.AreEqual(MessageBoxResult.None, _vm.DialogResult);
			Assert.AreEqual(MessageBoxResult.Yes, options[0].Identifier);
			Assert.AreEqual(MessageBoxResult.No, options[1].Identifier);
			Assert.IsTrue(_vm.IsModal);
			Assert.IsNull(_vm.DoNotShowAgainSettings);
			Assert.IsNull(_vm.Exception);
		}


		[Test]
		public void TestNonObservableProperties()
		{
			_listener.TestNonObservableProperty(_vm, () => _vm.DialogResult, MessageBoxResult.OK, 42);
			_listener.TestNonObservableProperty(_vm, () => _vm.Exception, null, new Exception());
		}


		[Test]
		public void TestObservableProperties()
		{
			_listener.TestObservableProperty(_vm, () => _vm.Title);
			_listener.TestObservableProperty(_vm, () => _vm.Message);
		}


		private CustomMessageBoxVM _vm;
		private ObservableObjectTester _listener;
	}
}

﻿using System;
using System.Linq;
using NUnit.Framework;
using Zaber.Testing;
using ZaberWpfToolbox.Dialogs;

namespace ZaberWpfToolboxTests.Dialogs
{
	[TestFixture]
	[SetCulture("en-US")]
	public class ProgressDialogVMTests
	{
		[SetUp]
		public void Setup()
		{
			_vm = new ProgressDialogVM();
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
		}


		[TearDown]
		public void Teardown() => _listener.Unlisten(_vm);


		[Test]
		public void TestAppend()
		{
			_vm.Append("Foo");
			Assert.AreEqual(1, _listener.GetNumNotifications());
			Assert.AreEqual("Foo", _vm.Message);
			_vm.Append("bar");
			Assert.AreEqual(2, _listener.GetNumNotifications());
			Assert.AreEqual("Foobar", _vm.Message);
			var props = _listener.GetPropertiesChanged().Distinct().ToList();
			Assert.AreEqual(1, props.Count);
			Assert.AreEqual(nameof(_vm.Message), props[0]);
		}


		[Test]
		public void TestAppendLine()
		{
			_vm.AppendLine("Foo");
			Assert.AreEqual(1, _listener.GetNumNotifications());
			Assert.AreEqual("Foo" + Environment.NewLine, _vm.Message);
			_vm.AppendLine("Bar");
			Assert.AreEqual(2, _listener.GetNumNotifications());
			Assert.AreEqual("Foo" + Environment.NewLine + "Bar" + Environment.NewLine, _vm.Message);
			var props = _listener.GetPropertiesChanged().Distinct().ToList();
			Assert.AreEqual(1, props.Count);
			Assert.AreEqual(nameof(_vm.Message), props[0]);
		}


		[Test]
		public void TestDefaultConstructor()
		{
			Assert.IsTrue(string.IsNullOrEmpty(_vm.Message));
			Assert.AreEqual(0.0, _vm.MinValue);
			Assert.AreEqual(100.0, _vm.MaxValue);
			Assert.AreEqual(0.0, _vm.Progress);
			Assert.IsFalse(_vm.HasDefiniteValue);
			Assert.IsTrue(_vm.IsModal);
			Assert.IsNull(_vm.Exception);
		}


		[Test]
		public void TestNonObservableProperties()
			=> _listener.TestNonObservableProperty(_vm, () => _vm.Exception, null, new Exception());


		[Test]
		public void TestObservableProperties()
		{
			_listener.TestObservableProperty(_vm, () => _vm.Message);
			_listener.TestObservableProperty(_vm, () => _vm.MinValue);
			_listener.TestObservableProperty(_vm, () => _vm.MaxValue);
			_listener.TestObservableProperty(_vm, () => _vm.Progress);
			_listener.TestObservableProperty(_vm, () => _vm.HasDefiniteValue);
		}


		[Test]
		public void TestRangeConstructor()
		{
			var vm = new ProgressDialogVM(1.0, 2.0);
			Assert.IsTrue(string.IsNullOrEmpty(vm.Message));
			Assert.AreEqual(1.0, vm.MinValue);
			Assert.AreEqual(2.0, vm.MaxValue);
			Assert.AreEqual(1.0, vm.Progress);
			Assert.IsTrue(vm.HasDefiniteValue);
			Assert.IsNull(_vm.Exception);
		}


		[Test]
		public void TestWindowLoadedCommand()
		{
			var tester = new WindowLoadedTestImplementation();
			Assert.AreEqual(0, tester.InvocationCount);

			var cmd = tester.WindowLoadedCommand;
			Assert.IsTrue(cmd.CanExecute(null));
			cmd.Execute(null);
			Assert.AreEqual(1, tester.InvocationCount);
		}


		private class WindowLoadedTestImplementation : ProgressDialogVM
		{
			#region -- Public data --

			public int InvocationCount;

			#endregion

			#region -- Non-Public Methods --

			protected override void OnWindowLoaded()
			{
				base.OnWindowLoaded();
				++InvocationCount;
			}

			#endregion
		}


		private ProgressDialogVM _vm;
		private ObservableObjectTester _listener;
	}
}

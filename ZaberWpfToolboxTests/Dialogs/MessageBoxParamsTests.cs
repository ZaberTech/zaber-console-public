﻿using System;
using System.Windows;
using NUnit.Framework;
using Zaber.Testing;
using ZaberWpfToolbox.Dialogs;

namespace ZaberWpfToolboxTests.Dialogs
{
	[TestFixture]
	[SetCulture("en-US")]
	public class MessageBoxParamsTests
	{
		[SetUp]
		public void Setup() => _vm = new MessageBoxParams();


		[Test]
		public void TestConstructor()
		{
			Assert.AreEqual("Confirm Action", _vm.Caption);
			Assert.IsTrue(string.IsNullOrEmpty(_vm.Message));
			Assert.AreEqual(MessageBoxButton.OKCancel, _vm.Buttons);
			Assert.AreEqual(MessageBoxImage.Question, _vm.Icon);
			Assert.AreEqual(MessageBoxResult.None, _vm.Result);
			Assert.IsNull(_vm.Exception);
		}


		[Test]
		public void TestNonObservableProperties()
		{
			var tester = new ObservableObjectTester();
			tester.TestNonObservableProperty(_vm, () => _vm.Message, "Foo", "Bar");
			tester.TestNonObservableProperty(_vm, () => _vm.Caption, "Foo", "Bar");
			tester.TestNonObservableProperty(_vm, () => _vm.Buttons, MessageBoxButton.YesNo, MessageBoxButton.OK);
			tester.TestNonObservableProperty(_vm, () => _vm.Icon, MessageBoxImage.Error, MessageBoxImage.Hand);
			tester.TestNonObservableProperty(_vm, () => _vm.Result, MessageBoxResult.No, MessageBoxResult.OK);
			tester.TestNonObservableProperty(_vm, () => _vm.Exception, null, new Exception());
		}


		private MessageBoxParams _vm;
	}
}

﻿using System;
using NUnit.Framework;
using Zaber.Testing;
using ZaberWpfToolbox.Dialogs;

namespace ZaberWpfToolboxTests.Dialogs
{
	[TestFixture]
	[SetCulture("en-US")]
	public class FolderBrowserDialogParamsTests
	{
		[SetUp]
		public void Setup() => _vm = new FolderBrowserDialogParams();


		[Test]
		public void TestConstructor()
		{
			Assert.IsTrue(string.IsNullOrEmpty(_vm.SelectedPath));
			Assert.AreEqual(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), _vm.DefaultPath);
			Assert.AreEqual("Select Folder", _vm.Description);
			Assert.IsTrue(_vm.ShowNewFolderButton);
			Assert.IsNull(_vm.DialogResult);
			Assert.IsNull(_vm.Exception);
		}


		[Test]
		public void TestNonObservableProperties()
		{
			var tester = new ObservableObjectTester();
			tester.TestNonObservableProperty(_vm, () => _vm.SelectedPath, "Foo", "Bar");
			tester.TestNonObservableProperty(_vm, () => _vm.DefaultPath, "Foo", "Bar");
			tester.TestNonObservableProperty(_vm, () => _vm.Description, "Foo", "Bar");
			tester.TestNonObservableProperty(_vm, () => _vm.ShowNewFolderButton, true, false);
			tester.TestNonObservableProperty(_vm, () => _vm.DialogResult, true, false);
			tester.TestNonObservableProperty(_vm, () => _vm.Exception, null, new Exception());
		}


		private FolderBrowserDialogParams _vm;
	}
}

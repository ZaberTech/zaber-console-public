﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using NUnit.Framework;
using Zaber.Testing;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Dialogs;

namespace ZaberWpfToolboxTests.Dialogs
{
	[TestFixture]
	[SetCulture("en-US")]
	public class AboutBoxVMTests
	{
		[SetUp]
		public void Setup()
		{
			_vm = new AboutBoxVM();
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
		}


		[TearDown]
		public void Teardown() => _listener.Unlisten(_vm);


		// Test double-clicking on an assembly.
		[Test]
		public void TestAssemblyDoubleClick()
		{
			// First wait for the details to populate.
			while (null == _vm.SelectedAssembly)
			{
				DispatcherUtil.SleepAndDoSyncEvents(100, 100);
			}

			Assert.IsNotNull(_vm.SelectedAssembly, "Info population thread did not complete within 5 seconds.");


			// Now select some other assembly.
			var newAssembly = _vm.AssemblyList.LastOrDefault();
			Assert.IsNotNull(newAssembly);
			Assert.AreNotEqual(newAssembly, _vm.SelectedAssembly);

			// Issue the double-click command.
			var cmd = _vm.AssemblyDoubleClickCommand;
			Assert.IsTrue(cmd.CanExecute(newAssembly));
			cmd.Execute(newAssembly);

			// Wait for BeginInvoke to finish.
			DispatcherUtil.DoEventsSync();

			// Check results.
			Assert.AreEqual(2, _vm.SelectedTabIndex);
			Assert.AreEqual(newAssembly, _vm.SelectedAssembly);
			Assert.IsTrue(_vm.ComboBoxHasFocus);
		}


		// Check that initial defaults are as expected.
		[Test]
		public void TestInitialValues()
		{
			Assert.AreEqual("ZaberWpfToolbox", _vm.Title);
			Assert.AreEqual("About ZaberWpfToolbox", _vm.WindowTitle);
			Assert.AreEqual("Zaber WPF application shared tools", _vm.Description);
			Assert.IsTrue(_vm.Version.Contains("Version"));
			Assert.IsTrue(_vm.BuildDate.Contains("Built on"));
			Assert.IsTrue(_vm.CopyrightMessage.Contains("Copyright"));
			Assert.IsNull(_vm.Icon);
			Assert.IsEmpty(_vm.ApplicationInfo);
			Assert.IsEmpty(_vm.AssemblyList);
			Assert.IsNull(_vm.SelectedAssembly);
			Assert.AreEqual(0, _vm.SelectedTabIndex);
			Assert.IsEmpty(_vm.AssemblyInfo);
			Assert.IsFalse(_vm.ComboBoxHasFocus);
			Assert.IsTrue(_vm.IsModal);
			Assert.IsNull(_vm.Exception);
		}


		// Test that certain properties are assignable.
		[Test]
		public void TestNonObservableProperties()
		{
			_listener.TestNonObservableProperty(_vm, () => _vm.WindowTitle, "Foo", "Bar");
			_listener.TestNonObservableProperty(_vm, () => _vm.Title, "Foo", "Bar");
			_listener.TestNonObservableProperty(_vm, () => _vm.Description, "Foo", "Bar");
			_listener.TestNonObservableProperty(_vm, () => _vm.Version, "Foo", "Bar");
			_listener.TestNonObservableProperty(_vm, () => _vm.BuildDate, "Foo", "Bar");
			_listener.TestNonObservableProperty(_vm, () => _vm.CopyrightMessage, "Foo", "Bar");
			_listener.TestNonObservableProperty(_vm, () => _vm.Exception, null, new Exception());
		}


		// Test that observable properties generate change events.
		[Test]
		public void TestObservableProperties()
		{
			// First wait for the details to populate.
			while (null == _vm.SelectedAssembly)
			{
				DispatcherUtil.SleepAndDoSyncEvents(100, 100);
			}

			Assert.IsNotNull(_vm.SelectedAssembly, "Info population thread did not complete within 5 seconds.");

			_listener.TestObservableProperty(_vm, () => _vm.SelectedTabIndex);
			_listener.TestObservableProperty(_vm, () => _vm.ComboBoxHasFocus);

			_listener.TestObservableProperty(_vm,
											 () => _vm.SelectedAssembly,
											 oldAsm =>
											 {
												 var index = _vm.AssemblyList.IndexOf(oldAsm);
												 return _vm.AssemblyList[(index + 1) % _vm.AssemblyList.Count];
											 });

			_listener.TestObservableProperty(_vm,
											 () => _vm.ApplicationInfo,
											 oldCollection => new ObservableCollection<Tuple<string, string>>());

			_listener.TestObservableProperty(_vm,
											 () => _vm.AssemblyList,
											 oldCollection
												 => new ObservableCollection<AssemblyInfoHelper.AssemblyProperties>());

			_listener.TestObservableProperty(_vm,
											 () => _vm.AssemblyInfo,
											 oldCollection => new ObservableCollection<Tuple<string, string>>());
		}


		// Test response to a window close request.
		[Test]
		public void TestWindowCloseCommand()
		{
			var invocationCount = 0;

			EventHandler handler = (aSender, aArgs) => { ++invocationCount; };

			_vm.DialogClosing += handler;

			var cmd = _vm.WindowClosingCommand;
			Assert.IsTrue(cmd.CanExecute(null));
			cmd.Execute(null);

			_vm.DialogClosing -= handler;

			Assert.AreEqual(1, invocationCount);
		}


		private AboutBoxVM _vm;
		private ObservableObjectTester _listener;
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Zaber.Testing;
using ZaberWpfToolbox.Dialogs;

namespace ZaberWpfToolboxTests.Dialogs
{
	[TestFixture]
	[SetCulture("en-US")]
	public class FileBrowserDialogParamsTests
	{
		[SetUp]
		public void Setup() => _vm = new FileBrowserDialogParams();


		[Test]
		public void TestDefaultConstructor()
		{
			Assert.AreEqual("Select File", _vm.Title);
			Assert.AreEqual("All files (*.*)|*.*", _vm.Filter);
			Assert.IsTrue(_vm.CheckFileExists);
			Assert.IsTrue(string.IsNullOrEmpty(_vm.InitialDirectory));
			Assert.AreEqual(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), _vm.DefaultDirectory);
			Assert.AreEqual(0, _vm.FilterIndex);
			Assert.IsFalse(_vm.AddExtension);
			Assert.IsTrue(string.IsNullOrEmpty(_vm.DefaultExtension));
			Assert.IsTrue(string.IsNullOrEmpty(_vm.Filename));
			Assert.IsNull(_vm.DialogResult);
			Assert.IsFalse(_vm.MultiSelect);
			Assert.IsNull(_vm.Filenames);
			Assert.IsTrue(_vm.PromptForOverwrite);
			Assert.AreEqual(FileBrowserDialogParams.FileActionType.Open, _vm.Action);
			Assert.IsNull(_vm.Exception);
		}


		[Test]
		public void TestNonObservableProperties()
		{
			var tester = new ObservableObjectTester();

			tester.TestNonObservableProperty(_vm, () => _vm.Title, "Foo", "Bar");
			tester.TestNonObservableProperty(_vm, () => _vm.InitialDirectory, "Foo", "Bar");
			tester.TestNonObservableProperty(_vm, () => _vm.DefaultDirectory, "Foo", "Bar");
			tester.TestNonObservableProperty(_vm, () => _vm.Filter, "Foo", "Bar");
			tester.TestNonObservableProperty(_vm, () => _vm.FilterIndex, 1, 2);
			tester.TestNonObservableProperty(_vm, () => _vm.AddExtension, true, false);
			tester.TestNonObservableProperty(_vm, () => _vm.DefaultExtension, "Foo", "Bar");
			tester.TestNonObservableProperty(_vm, () => _vm.CheckFileExists, true, false);
			tester.TestNonObservableProperty(_vm, () => _vm.Filename, "Foo", "Bar");
			tester.TestNonObservableProperty(_vm, () => _vm.Filenames, null, new List<string>());
			tester.TestNonObservableProperty(_vm, () => _vm.DialogResult, true, false);
			tester.TestNonObservableProperty(_vm, () => _vm.MultiSelect, true, false);
			tester.TestNonObservableProperty(_vm, () => _vm.PromptForOverwrite, true, false);
			tester.TestNonObservableProperty(_vm,
											 () => _vm.Action,
											 FileBrowserDialogParams.FileActionType.Open,
											 FileBrowserDialogParams.FileActionType.Save);
			tester.TestNonObservableProperty(_vm, () => _vm.Exception, null, new Exception());
		}


		[Test]
		public void TestSettingsConstructor()
		{
			var settings = new FileBrowserUserSettings
			{
				Directory = "Foo",
				Filename = "Bar",
				FilterIndex = 17
			};

			_vm = new FileBrowserDialogParams(settings);

			Assert.AreEqual("Select File", _vm.Title);
			Assert.AreEqual("All files (*.*)|*.*", _vm.Filter);
			Assert.IsTrue(_vm.CheckFileExists);
			Assert.AreEqual("Foo", _vm.InitialDirectory);
			Assert.AreEqual(17, _vm.FilterIndex);
			Assert.IsFalse(_vm.AddExtension);
			Assert.IsTrue(string.IsNullOrEmpty(_vm.DefaultExtension));
			Assert.AreEqual("Bar", _vm.Filename);
			Assert.IsNull(_vm.DialogResult);
			Assert.IsFalse(_vm.MultiSelect);
			Assert.IsNotNull(_vm.Filenames);
			Assert.AreEqual(1, _vm.Filenames.Count());
			Assert.AreEqual("Bar", _vm.Filenames.First());
			Assert.IsTrue(_vm.PromptForOverwrite);
			Assert.AreEqual(FileBrowserDialogParams.FileActionType.Open, _vm.Action);
			Assert.IsNull(_vm.Exception);
		}


		private FileBrowserDialogParams _vm;
	}
}

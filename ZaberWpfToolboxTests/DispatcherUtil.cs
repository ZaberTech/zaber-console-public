﻿using System;
using System.Security.Permissions;
using System.Threading;
using System.Windows.Threading;

namespace ZaberWpfToolboxTests
{
	/// <summary>
	///     Unit test helper to force the dispatcher to process Invoke and BeginInvoke calls
	///     triggered by VMs being tested.
	/// </summary>
	/// <remarks>
	///     Adapted from the thread:
	///     http://stackoverflow.com/questions/9336165/correct-method-for-using-the-wpf-dispatcher-in-unit-tests
	///     and thus falls under the CC-BY-SA 3.0 license: https://creativecommons.org/licenses/by-sa/3.0/
	/// </remarks>
	public static class DispatcherUtil
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Force execution of an async callback.
		/// </summary>
		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public static void DoEventsAsync()
		{
			var frame = new DispatcherFrame();
			Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
													 new DispatcherOperationCallback(ExitFrame),
													 frame);
			Dispatcher.PushFrame(frame);
		}


		/// <summary>
		///     Force execution of a synchronous callback. This should clear the invocation queue.
		/// </summary>
		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public static void DoEventsSync()
		{
			var frame = new DispatcherFrame();
			Dispatcher.CurrentDispatcher.Invoke(DispatcherPriority.Background,
												new DispatcherOperationCallback(ExitFrame),
												frame);
			Dispatcher.PushFrame(frame);
		}


		/// <summary>
		///     Do nothing but process dispatcher events for a specified period of time.
		/// </summary>
		/// <param name="aMilliseconds">The total time in milliseconds to process events before returning.</param>
		/// <param name="aGranularity">If nonzero, sleep for this many milliseconds between calls to event processing.</param>
		public static void SleepAndDoSyncEvents(int aMilliseconds, int aGranularity = 0)
		{
			var startTime = DateTime.UtcNow;
			do
			{
				DoEventsSync();

				if (aGranularity > 0)
				{
					Thread.Sleep(aGranularity);
				}
			} while ((DateTime.UtcNow - startTime).TotalMilliseconds < aMilliseconds);
		}

		#endregion

		#region -- Non-Public Methods --

		private static object ExitFrame(object aFrame)
		{
			((DispatcherFrame) aFrame).Continue = false;
			return null;
		}

		#endregion
	}
}

﻿using NUnit.Framework;
using Zaber.Testing;
using ZaberWpfToolbox;

namespace ZaberWpfToolboxTests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class ObservableObjectTests
	{
		[SetUp]
		public void Setup()
		{
			_testObject = new TestObject();
			_listener = new ObservableObjectTester();
			_listener.Listen(_testObject);
		}


		[TearDown]
		public void Teardown()
		{
			_listener.Unlisten(_testObject);
			_testObject = null;
		}


		// Test that we don't get notifications from objects we haven't subscribed to.
		[Test]
		public void TestChangeNotSubscribedTo()
		{
			_testObject.RefType = new TestObject();
			_listener.ClearNotifications();
			_testObject.RefType.RefType = new TestObject();
			Assert.AreEqual(0, _listener.GetNumNotifications());
		}


		// Test that we get proper notification of changes to a reference type.
		[Test]
		public void TestChangeRefType()
		{
			_testObject.RefType = new TestObject();
			Assert.IsTrue(_listener.OnePropertyWasChanged("RefType"));
		}


		// Test that we get proper notification of changes to a value type.
		[Test]
		public void TestChangeValueType()
		{
			_testObject.ValueType += 1;
			Assert.IsTrue(_listener.OnePropertyWasChanged("ValueType"));
		}


		// Test that getting a property name works for instance properties.
		[Test]
		public void TestGetNameInstance() => Assert.AreEqual("ValueType", nameof(_testObject.ValueType));


		// Test that getting a property name works for static properties.
		[Test]
		public void TestGetNameStatic() => Assert.AreEqual("DefaultValue", nameof(TestObject.DefaultValue));


		// Test that we don't get any notifications for non-changing writes to a reference type.
		[Test]
		public void TestNoChangeRefType()
		{
			_testObject.RefType = null;
			Assert.AreEqual(0, _listener.GetNumNotifications());
		}


		// Test that we don't get a change notification if we set a value type to the same value.
		[Test]
		public void TestNoChangeValueType()
		{
			_testObject.ValueType = 0;
			Assert.AreEqual(0, _listener.GetNumNotifications());
		}


		private class TestObject : ObservableObject
		{
			#region -- Public Methods & Properties --

			public int ValueType
			{
				get => _valueType;
				set => Set(ref _valueType, value, nameof(ValueType));
			}

			public TestObject RefType
			{
				get => _refType;
				set => Set(ref _refType, value, nameof(RefType));
			}

			public static int DefaultValue => 0;

			#endregion

			#region -- Data --

			private int _valueType = DefaultValue;
			private TestObject _refType;

			#endregion
		}


		private TestObject _testObject;
		private ObservableObjectTester _listener;
	}
}

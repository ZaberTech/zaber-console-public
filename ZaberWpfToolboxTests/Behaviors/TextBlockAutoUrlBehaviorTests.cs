﻿using System.Collections.Generic;
using NUnit.Framework;
using ZaberWpfToolbox.Behaviors;

namespace ZaberWpfToolboxTests.Behaviors
{
	[TestFixture]
	[SetCulture("en-US")]
	[SetUICulture("en-US")]
	public class TextBlockAutoUrlBehaviorTests
	{
		[Test]
		[TestCaseSource("GetBogusUrls")]
		public void TestNegativeMatches(string aUrl)
			=> Assert.AreEqual(0, TextBlockAutoUrlBehavior.RE_URL.Matches(aUrl).Count);


		[Test]
		[TestCaseSource("GetLegitUrls")]
		public void TestPositiveMatches(string aUrl)
			=> Assert.AreEqual(1, TextBlockAutoUrlBehavior.RE_URL.Matches(aUrl).Count);


		public static IEnumerable<object[]> GetLegitUrls()
		{
			string[] urls =
			{
				"[ABCD](ftp://abcd.de)", "[Zaber](http://zaber.com/index.html)", "[Zaber](https://www.zaber.com/)",
				"[Release Notes](https://iwiki.izaber.com/FWEE/Zaber_Console/Dev_Release_Notes)",
				"[Release Notes for v2.0.6.2247](https://www.zaber.com/wiki/Software/Zaber_Console/Release_notes#2.0.6.2247)",
				"[excellence](https://www.youtube.com/watch?v=RXJKdh1KZ0w)"
			};

			foreach (var url in urls)
			{
				yield return new object[] { url };
			}
		}


		public static IEnumerable<object[]> GetBogusUrls()
		{
			string[] urls =
			{
				"abcd.de", "c:\\foo\\bar.baz", "C://foo",

				// These next ones don't match because they're not in markdown format.
				"http://zaber.com/index.html", "https://www.zaber.com/"
			};

			foreach (var url in urls)
			{
				yield return new object[] { url };
			}
		}
	}
}

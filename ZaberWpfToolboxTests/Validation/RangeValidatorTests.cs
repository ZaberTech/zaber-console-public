﻿using System;
using System.Globalization;
using NUnit.Framework;
using ZaberWpfToolbox.Validation;

namespace ZaberWpfToolboxTests.Validation
{
	[TestFixture]
	[SetCulture("en-US")]
	public class RangeValidatorTests
	{
		[SetUp]
		public void Setup() => _validator = new RangeValidator();


		// Verifies that defaults are initialized as expected.
		[Test]
		public void TestConstructor()
		{
			Assert.AreEqual(decimal.MinValue, _validator.Min);
			Assert.AreEqual(decimal.MaxValue, _validator.Max);
		}


		// Test that without a range set, all doubles are accepted.
		[Test]
		public void TestDefaultRange()
		{
			var result = _validator.Validate("0", CultureInfo.InvariantCulture);
			Assert.IsTrue(result.IsValid);

			// Large floating-point values don't necessaraily work right with the < >
			// operators, so we use values slightly inside the actual range here.
			result = _validator.Validate(decimal.MinValue.ToString(_numFormat), _cult);
			Assert.IsTrue(result.IsValid);
			result = _validator.Validate(decimal.MaxValue.ToString(_numFormat), _cult);
			Assert.IsTrue(result.IsValid);
		}


		// Test that non-numeric text causes an error.
		[Test]
		public void TestNonNumeric()
		{
			var result = _validator.Validate("x", CultureInfo.InvariantCulture);
			Assert.IsFalse(result.IsValid);
			result = _validator.Validate("!", CultureInfo.InvariantCulture);
			Assert.IsFalse(result.IsValid);
		}


		// Test that range validation works.
		[Test]
		public void TestRange()
		{
			_validator.Min = -10m;
			_validator.Max = 10m;
			var result = _validator.Validate("-10.1", CultureInfo.InvariantCulture);
			Assert.IsFalse(result.IsValid);
			result = _validator.Validate("-9.9", CultureInfo.InvariantCulture);
			Assert.IsTrue(result.IsValid);
			result = _validator.Validate("0", CultureInfo.InvariantCulture);
			Assert.IsTrue(result.IsValid);
			result = _validator.Validate("9.9", CultureInfo.InvariantCulture);
			Assert.IsTrue(result.IsValid);
			result = _validator.Validate("10.1", CultureInfo.InvariantCulture);
			Assert.IsFalse(result.IsValid);
		}


		private RangeValidator _validator;
		private readonly CultureInfo _cult = CultureInfo.InvariantCulture;
		private readonly IFormatProvider _numFormat = CultureInfo.InvariantCulture.NumberFormat;
	}
}

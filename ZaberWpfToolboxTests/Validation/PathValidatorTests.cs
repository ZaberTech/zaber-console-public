﻿using System.Globalization;
using System.IO;
using NUnit.Framework;
using ZaberWpfToolbox.Validation;

namespace ZaberWpfToolboxTests.Validation
{
	[TestFixture]
	[SetCulture("en-US")]
	public class PathValidatorTests
	{
		[SetUp]
		public void Setup() => _validator = new PathValidator();


		// Verifies that defaults are initialized as expected.
		[Test]
		public void TestConstructor()
		{
			Assert.IsTrue(_validator.IsDir);
			Assert.IsTrue(_validator.IsFile);
		}


		// Test validation of directory names.
		[Test]
		public void TestDirs()
		{
			_validator.IsDir = true;
			_validator.IsFile = false;
			var path = string.Empty;
			do
			{
				path = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
			} while (Directory.Exists(path));

			// Result for directory that exists should be true.
			Directory.CreateDirectory(path);
			var result = _validator.Validate(path, CultureInfo.CurrentCulture);
			Assert.IsNotNull(result);
			Assert.IsTrue(result.IsValid);
			Assert.IsNull(result.ErrorContent);

			// Result for file check when path points to a directory should be false.
			_validator.IsDir = false;
			_validator.IsFile = true;
			result = _validator.Validate(path, CultureInfo.CurrentCulture);
			Assert.IsNotNull(result);
			Assert.IsFalse(result.IsValid);
			Assert.IsFalse(string.IsNullOrEmpty(result.ErrorContent as string));

			// Result for both checks when path does not exist should be false.
			Directory.Delete(path);
			result = _validator.Validate(path, CultureInfo.CurrentCulture);
			Assert.IsNotNull(result);
			Assert.IsFalse(result.IsValid);
			Assert.IsFalse(string.IsNullOrEmpty(result.ErrorContent as string));

			_validator.IsDir = true;
			_validator.IsFile = false;
			result = _validator.Validate(path, CultureInfo.CurrentCulture);
			Assert.IsNotNull(result);
			Assert.IsFalse(result.IsValid);
			Assert.IsFalse(string.IsNullOrEmpty(result.ErrorContent as string));
		}


		// Test validation of file names.
		[Test]
		public void TestFile()
		{
			_validator.IsDir = false;
			_validator.IsFile = true;
			var path = Path.GetTempFileName();

			// Result for file that exists should be true.
			var result = _validator.Validate(path, CultureInfo.CurrentCulture);
			Assert.IsNotNull(result);
			Assert.IsTrue(result.IsValid);
			Assert.IsNull(result.ErrorContent);

			// Result for dir check when path points to a false should be false.
			_validator.IsDir = true;
			_validator.IsFile = false;
			result = _validator.Validate(path, CultureInfo.CurrentCulture);
			Assert.IsNotNull(result);
			Assert.IsFalse(result.IsValid);
			Assert.IsFalse(string.IsNullOrEmpty(result.ErrorContent as string));

			// Result for both checks when path does not exist should be false.
			File.Delete(path);
			result = _validator.Validate(path, CultureInfo.CurrentCulture);
			Assert.IsNotNull(result);
			Assert.IsFalse(result.IsValid);
			Assert.IsFalse(string.IsNullOrEmpty(result.ErrorContent as string));

			_validator.IsDir = false;
			_validator.IsFile = true;
			result = _validator.Validate(path, CultureInfo.CurrentCulture);
			Assert.IsNotNull(result);
			Assert.IsFalse(result.IsValid);
			Assert.IsFalse(string.IsNullOrEmpty(result.ErrorContent as string));
		}


		private PathValidator _validator;
	}
}

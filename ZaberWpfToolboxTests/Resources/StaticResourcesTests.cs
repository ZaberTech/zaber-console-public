﻿using System.Windows;
using NUnit.Framework;
using ZaberWpfToolbox.Resources;

namespace ZaberWpfToolboxTests.Resources
{
	[TestFixture]
	[SetCulture("en-US")]
	public class StaticResourcesTests
	{
		[Test]
		public void TestLogicConvertersInitialized()
		{
			Assert.IsNotNull(StaticResources.NegateBool);

			Assert.IsNotNull(StaticResources.BooleanAnd);
			Assert.IsFalse(StaticResources.BooleanAnd.NegateResult);

			Assert.IsNotNull(StaticResources.BooleanNand);
			Assert.IsTrue(StaticResources.BooleanNand.NegateResult);

			Assert.IsNotNull(StaticResources.BooleanOr);
			Assert.IsFalse(StaticResources.BooleanOr.NegateResult);

			Assert.IsNotNull(StaticResources.BooleanNor);
			Assert.IsTrue(StaticResources.BooleanNor.NegateResult);

			Assert.IsNotNull(StaticResources.Equality);
			Assert.IsFalse(StaticResources.Equality.NegateResult);

			Assert.IsNotNull(StaticResources.Inequality);
			Assert.IsTrue(StaticResources.Inequality.NegateResult);
		}


		[Test]
		public void TestMiscConvertersInitialized()
		{
			Assert.IsNotNull(StaticResources.DetectEmptyString);
			Assert.IsFalse(StaticResources.DetectEmptyString.NegateResult);

			Assert.IsNotNull(StaticResources.DetectNonemptyString);
			Assert.IsTrue(StaticResources.DetectNonemptyString.NegateResult);

			Assert.IsNotNull(StaticResources.ConvertEnumToBoolean);
			Assert.IsFalse(StaticResources.ConvertEnumToBoolean.NegateResult);

			Assert.IsNotNull(StaticResources.ConvertEnumToNegatedBoolean);
			Assert.IsTrue(StaticResources.ConvertEnumToNegatedBoolean.NegateResult);

			Assert.IsNotNull(StaticResources.GridLengthSerializer);

			Assert.IsNotNull(StaticResources.EnableExtendedSelectionModeConverter);

			Assert.IsNotNull(StaticResources.EnumValueDescriptionConverter);

			Assert.IsNotNull(StaticResources.IsNull);
			Assert.IsNotNull(StaticResources.IsNotNull);

			Assert.IsNotNull(StaticResources.GetDisplayName);
		}


		[Test]
		public void TestVisibilityConvertersInitialized()
		{
			Assert.IsNotNull(StaticResources.ConvertBoolToVisibility);
			Assert.IsFalse(StaticResources.ConvertBoolToVisibility.ReverseBehavior);
			Assert.AreEqual(Visibility.Collapsed, StaticResources.ConvertBoolToVisibility.HiddenState);

			Assert.IsNotNull(StaticResources.ConvertBoolToHidden);
			Assert.IsFalse(StaticResources.ConvertBoolToHidden.ReverseBehavior);
			Assert.AreEqual(Visibility.Hidden, StaticResources.ConvertBoolToHidden.HiddenState);

			Assert.IsNotNull(StaticResources.ConvertNegatedBoolToCollapsed);
			Assert.IsTrue(StaticResources.ConvertNegatedBoolToCollapsed.ReverseBehavior);
			Assert.AreEqual(Visibility.Collapsed, StaticResources.ConvertNegatedBoolToCollapsed.HiddenState);

			Assert.IsNotNull(StaticResources.ConvertNegatedBoolToHidden);
			Assert.IsTrue(StaticResources.ConvertNegatedBoolToHidden.ReverseBehavior);
			Assert.AreEqual(Visibility.Hidden, StaticResources.ConvertNegatedBoolToHidden.HiddenState);
		}
	}
}

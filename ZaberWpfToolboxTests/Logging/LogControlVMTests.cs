﻿using System;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using NUnit.Framework;
using Zaber.Testing;
using ZaberWpfToolbox.Logging;

namespace ZaberWpfToolboxTests.Logging
{
	[TestFixture]
	[Apartment(ApartmentState.STA)]
	[SetCulture("en-US")]
	public class LogControlVMTests
	{
		[SetUp]
		public void Setup()
		{
			_vm = new LogControlVM();
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
		}


		[TearDown]
		public void Teardown() => _listener.Unlisten(_vm);


		[Test]
		public void TestAppend()
		{
			_vm.Append("Foo");
			_vm.Append("bar");
			WaitForTimer();
			Assert.AreEqual("Foobar", _vm.Text);
		}


		[Test]
		public void TestAppendDisabled()
		{
			_vm.IsDisabled = true;
			_vm.Append("Foo");
			_vm.Append("bar");
			WaitForTimer();
			Assert.IsTrue(string.IsNullOrEmpty(_vm.Text));
		}


		[Test]
		public void TestAppendLine()
		{
			_vm.AppendLine("Foo");
			_vm.AppendLine("Bar");
			WaitForTimer();
			Assert.AreEqual("Foo" + Environment.NewLine + "Bar" + Environment.NewLine, _vm.Text);
		}


		[Test]
		public void TestAppendLineDisabled()
		{
			_vm.IsDisabled = true;
			_vm.AppendLine("Foo");
			_vm.AppendLine("Bar");
			WaitForTimer();
			Assert.IsTrue(string.IsNullOrEmpty(_vm.Text));
		}


		[Test]
		public void TestClearLogCommand()
		{
			_vm.Append("Foo");
			WaitForTimer();
			Assert.AreEqual("Foo", _vm.Text);
			var cmd = _vm.ClearLogCommand;
			Assert.IsTrue(cmd.CanExecute(null));
			cmd.Execute(null);
			WaitForTimer();
			Assert.IsTrue(string.IsNullOrEmpty(_vm.Text));
		}


		[Test]
		public void TestConstructor()
		{
			Assert.AreEqual(10000, _vm.MaxLength);
			Assert.IsTrue(string.IsNullOrEmpty(_vm.Text));
			Assert.IsFalse(_vm.CopyButtonEnabled);
			Assert.IsTrue(_vm.ButtonsVisible);
			Assert.IsFalse(_vm.IsDisabled);
		}


		[Test]
		[STAThread]
		public void TestCopyLogCommand()
		{
			_vm.Append("Foo");
			WaitForTimer();
			Assert.AreEqual("Foo", _vm.Text);
			var cmd = _vm.CopyLogCommand;
			Assert.IsTrue(cmd.CanExecute(null));
			cmd.Execute(null);

			var obj = Clipboard.GetDataObject();
			var content = obj.GetData(typeof(string)) as string;
			Assert.IsNotNull(content);
			Assert.AreEqual("Foo", content);
		}


		[Test]
		public void TestNonObservableProperties()
		{
			_listener.TestNonObservableProperty(_vm, () => _vm.MaxLength, 1, 2);
			_listener.TestNonObservableProperty(_vm, () => _vm.IsDisabled, true, false);
		}


		[Test]
		public void TestObservableProperties()
		{
			_listener.TestObservableProperty(_vm, () => _vm.Text);
			_listener.TestObservableProperty(_vm, () => _vm.CopyButtonEnabled);
			_listener.TestObservableProperty(_vm, () => _vm.ButtonsVisible);
		}


		// Waits for the log's update thread timer to tick twice to ensure incoming text
		// or clear commands have been processed.
		private void WaitForTimer()
		{
			var timerCount = 0;
			var ticks = 0;

			EventHandler handler = (aSender, aArgs) => { ++timerCount; };

			var type = _vm.GetType();
			var member = type.GetField("_updateTimer", BindingFlags.Instance | BindingFlags.NonPublic);
			var timer = member.GetValue(_vm) as DispatcherTimer;
			timer.Tick += handler;

			while (timerCount < 2) // 2 to make sure test processing has happened before handler was invoked.
			{
				DispatcherUtil.DoEventsAsync();
				Thread.Sleep(50);
				ticks += 50;
				if (ticks > 2000)
				{
					Assert.Fail("Log timer is not ticking.");
				}
			}

			timer.Tick -= handler;
		}


		private LogControlVM _vm;
		private ObservableObjectTester _listener;
	}
}

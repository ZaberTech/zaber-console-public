﻿using NUnit.Framework;
using ZaberWpfToolbox.Settings;

namespace ZaberWpfToolboxTests.Settings
{
	[TestFixture]
	[SetCulture("en-US")]
	public class DoNotShowAgainSettingsCollectionTests
	{
		[SetUp]
		public void Setup() => _settings = new DoNotShowAgainSettingsCollection();


		[Test]
		public void TestClear()
		{
			Assert.AreEqual(0, _settings.DialogSettings.Count);
			_settings.FindOrCreate("Foo");
			_settings.FindOrCreate("Bar");
			Assert.AreEqual(2, _settings.DialogSettings.Count);
			_settings.Clear();
			Assert.AreEqual(0, _settings.DialogSettings.Count);
		}


		[Test]
		public void TestFindOrCreate()
		{
			Assert.AreEqual(0, _settings.DialogSettings.Count);
			var s1 = _settings.FindOrCreate("Foo");
			Assert.IsNotNull(s1);
			Assert.AreEqual(1, _settings.DialogSettings.Count);

			var s2 = _settings.FindOrCreate("Bar");
			Assert.IsNotNull(s2);
			Assert.IsFalse(ReferenceEquals(s1, s2));
			Assert.AreEqual(2, _settings.DialogSettings.Count);

			var s3 = _settings.FindOrCreate("Foo");
			Assert.IsNotNull(s3);
			Assert.IsTrue(ReferenceEquals(s1, s3));
			Assert.AreEqual(2, _settings.DialogSettings.Count);
		}


		private DoNotShowAgainSettingsCollection _settings;
	}
}

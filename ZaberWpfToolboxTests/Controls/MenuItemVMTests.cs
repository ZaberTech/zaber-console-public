﻿using NUnit.Framework;
using Zaber.Testing;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Controls;

namespace ZaberWpfToolboxTests.Controls
{
	[TestFixture]
	[SetCulture("en-US")]
	public class MenuItemVMTests
	{
		[SetUp]
		public void Setup()
		{
			_vm = new MenuItemVM();
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
		}


		[TearDown]
		public void Teardown()
		{
			_listener.Unlisten(_vm);
		}


		[Test]
		public void ObservableProperties()
		{
			_listener.TestObservableProperty(_vm, () => _vm.Title);
			_listener.TestObservableProperty(_vm, () => _vm.ToolTip);
			_listener.TestObservableProperty(_vm, () => _vm.IconUrl);
			_listener.TestObservableProperty(_vm, () => _vm.CommandParameter, _ => 1);
			_listener.TestObservableProperty(_vm, () => _vm.Command, _ => RelayCommand.DoNothing);
		}


		private MenuItemVM _vm;
		private ObservableObjectTester _listener;
	}
}

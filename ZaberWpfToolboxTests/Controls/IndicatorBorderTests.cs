﻿using System.Threading;
using System.Windows;
using System.Windows.Media;
using NUnit.Framework;
using ZaberWpfToolbox.Controls;

namespace ZaberWpfToolboxTests.Controls
{
	[TestFixture]
	[SetCulture("en-US")]
	[Apartment(ApartmentState.STA)]
	public class IndicatorBorderTests
	{
		[Test]
		public void Defaults()
		{
			var border = new IndicatorBorder();
			Assert.AreEqual(new Thickness(1), border.BorderThickness);
			Assert.AreEqual(Brushes.Transparent, border.BorderBrush);
			Assert.AreEqual(Brushes.Red, border.IndicationBrush);
			Assert.IsTrue(string.IsNullOrEmpty(border.Message));
			Assert.IsNull(border.ToolTip);
		}


		[Test]
		public void ChangeMessage()
		{
			var border = new IndicatorBorder();
			border.Message = "Foo!";
			Assert.AreEqual(Brushes.Red, border.BorderBrush);
			Assert.IsNotNull(border.ToolTip);

			border.Message = string.Empty;
			Assert.AreEqual(Brushes.Transparent, border.BorderBrush);
			Assert.IsNull(border.ToolTip);

			border.Message = null;
			Assert.AreEqual(Brushes.Transparent, border.BorderBrush);
			Assert.IsNull(border.ToolTip);
		}


		[Test]
		public void ChangeColor()
		{
			var border = new IndicatorBorder();
			border.IndicationBrush = Brushes.Blue;

			Assert.AreEqual(Brushes.Transparent, border.BorderBrush);
			border.Message = "Foo!";
			Assert.AreEqual(Brushes.Blue, border.BorderBrush);
			border.IndicationBrush = Brushes.Green;
			Assert.AreEqual(Brushes.Green, border.BorderBrush);
			Assert.AreEqual("Foo!", border.Message);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Zaber.Testing;
using ZaberWpfToolbox.Controls;

namespace ZaberWpfToolboxTests.Controls
{
	[TestFixture]
	[SetCulture("en-US")]
	public class MenuButtonVMTests
	{
		[SetUp]
		public void Setup()
		{
			_vm = new MenuButtonVM();
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
		}


		[TearDown]
		public void Teardown()
		{
			_listener.Unlisten(_vm);
		}


		[Test]
		public void ObservableProperties()
		{
			_listener.TestObservableProperty(_vm, () => _vm.ShowAlert);
			_listener.TestObservableProperty(_vm, () => _vm.ToolTip);
			_listener.TestObservableProperty(_vm, () => _vm.IconUrl);
			_listener.TestObservableProperty(_vm, () => _vm.MenuItems, aList => new ObservableCollection<MenuItemVM>() { new MenuItemVM() });
		}


		private MenuButtonVM _vm;
		private ObservableObjectTester _listener;
	}
}

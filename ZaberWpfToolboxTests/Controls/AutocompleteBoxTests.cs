﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using NUnit.Framework;
using ZaberWpfToolbox.Controls;

namespace ZaberWpfToolboxTests.Controls
{
	[TestFixture]
	[Apartment(ApartmentState.STA)]
	[SetCulture("en-US")]
	public class AutocompleteBoxTests
	{
		[SetUp]
		public void Setup()
		{
			_autocompleteBox = new AutocompleteBox();
			_autocompleteBox.EnteredText = string.Empty;
			_autocompleteBox.UnselectedText = string.Empty;
			_autocompleteBox.CommandList = new ObservableCollection<string>
			{
				"help",
				"help commands",
				"home",
				"move max",
				"move min",
				"move abs [x]",
				"virtual [vnum] setup angle [master] [slave] [theta]"
			};

			_textBox = typeof(AutocompleteBox)
				   .GetField("_textBox", BindingFlags.NonPublic | BindingFlags.GetField | BindingFlags.Instance)
				   .GetValue(_autocompleteBox) as TextBox;
			_listBox = typeof(AutocompleteBox)
				   .GetField("_listBox", BindingFlags.NonPublic | BindingFlags.GetField | BindingFlags.Instance)
				   .GetValue(_autocompleteBox) as ListBox;
		}


		[TearDown]
		public void Teardown()
		{
		}


		[Test]
		public void Test_textBoxHandlesDownArrowKey()
		{
			Assert.AreEqual(-1, _listBox.SelectedIndex);

			TypeKey(_textBox, Key.M);
			Assert.AreEqual(-1, _listBox.SelectedIndex);
			Assert.AreEqual("move", _textBox.Text);
			Assert.AreEqual("ove", _textBox.SelectedText);

			TypeKey(_textBox, Key.Down);
			Assert.AreEqual(0, _listBox.SelectedIndex);
			Assert.AreEqual("m", _textBox.Text);
			Assert.AreEqual(string.Empty, _textBox.SelectedText);
		}


		[Test]
		public void TestBasicFiltering()
		{
			TypeKey(_textBox, Key.H);
			Assert.That(_listBox.Items, Has.Member("help"));
			Assert.That(_listBox.Items, Has.Member("help commands"));
			Assert.That(_listBox.Items, Has.Member("home"));
			Assert.That(_listBox.Items, Has.No.Member("move max"));

			TypeKey(_textBox, Key.O);
			Assert.That(_listBox.Items, Has.No.Member("help"));
			Assert.That(_listBox.Items, Has.No.Member("help commands"));
			Assert.That(_listBox.Items, Has.Member("home"));
			Assert.That(_listBox.Items, Has.No.Member("move max"));

			TypeKey(_textBox, Key.Tab);
			Assert.That(_listBox.Items, Has.No.Member("help"));
			Assert.That(_listBox.Items, Has.Member("home"));
		}


		[Test]
		public void TestPreviousSelectionAlwaysPresentAtEnd()
		{
			_autocompleteBox.PreviousSelection = "move max";

			TypeKey(_textBox, Key.H);
			Assert.Contains("help", _listBox.Items);
			Assert.Contains("help commands", _listBox.Items);
			Assert.Contains("home", _listBox.Items);
			Assert.Contains("move max", _listBox.Items);

			TypeKey(_textBox, Key.G);
			Assert.Contains("move max", _listBox.Items);
			Assert.AreEqual(_listBox.Items.Count - 1, _listBox.Items.IndexOf("move max"));
		}


		[Test]
		public void TestBasicSelection()
		{
			TypeKey(_textBox, Key.H);
			Assert.AreEqual("help", _textBox.Text);
			Assert.AreEqual("elp", _textBox.SelectedText);

			TypeKey(_textBox, Key.O);
			Assert.AreEqual("home", _textBox.Text);
			Assert.AreEqual("me", _textBox.SelectedText);

			TypeKey(_textBox, Key.Tab);
			Assert.AreEqual("home", _textBox.Text);
			Assert.IsEmpty(_textBox.SelectedText);
		}


		[Test]
		public void TestDoesFiltersFragmentedWords()
		{
			TypeKey(_textBox, Key.H);
			TypeKey(_textBox, Key.O);
			Assert.That(_listBox.Items, Has.Member("home"));

			TypeKey(_textBox, Key.Space);
			TypeKey(_textBox, Key.M);
			TypeKey(_textBox, Key.E);
			Assert.That(_listBox.Items, Has.No.Member("home"));
		}


		[Test]
		public void TestDoesNotCompleteCommandIfTextMatchesValidCommand()
		{
			TypeKey(_textBox, Key.H);
			for (var i = 0; i < 3; ++i)
			{
				TypeKey(_textBox, Key.Tab);
				Assert.AreEqual("help", _textBox.Text);
				Assert.IsEmpty(_textBox.SelectedText);
			}

			TypeKey(_textBox, Key.Space);
			Assert.AreEqual("help commands", _textBox.Text);
			Assert.AreEqual("commands", _textBox.SelectedText);

			TypeKey(_textBox, Key.Tab);
			Assert.AreEqual("help commands", _textBox.Text);
			Assert.IsEmpty(_textBox.SelectedText);
		}


		[Test]
		public void TestDoesNotCompleteFragmentedWords()
		{
			TypeKey(_textBox, Key.H);
			TypeKey(_textBox, Key.O);
			Assert.AreEqual("home", _textBox.Text);
			Assert.AreEqual("me", _textBox.SelectedText);

			TypeKey(_textBox, Key.Space);
			TypeKey(_textBox, Key.M);
			TypeKey(_textBox, Key.E);
			Assert.AreEqual("ho me", _textBox.Text);
			Assert.IsEmpty(_textBox.SelectedText);
		}


		[Test]
		public void TestDoesNotCompletePartialArguments()
		{
			TypeText(_textBox, "he co");
			Assert.AreEqual("he co", _textBox.Text);
			Assert.IsEmpty(_textBox.SelectedText);

			TypeKey(_textBox, Key.Tab);
			Assert.AreEqual("he co", _textBox.Text);
			Assert.IsEmpty(_textBox.SelectedText);
		}


		[Test]
		public void TestDoesNotFilterCommandIfTextMatchesValidCommand()
		{
			TypeKey(_textBox, Key.H);
			for (var i = 0; i < 3; ++i)
			{
				TypeKey(_textBox, Key.Tab);
				Assert.That(_listBox.Items, Has.Member("help"));
				Assert.That(_listBox.Items, Has.Member("help commands"));
			}

			TypeKey(_textBox, Key.Space);
			TypeKey(_textBox, Key.Tab);
			Assert.That(_listBox.Items, Has.No.Member("help"));
			Assert.That(_listBox.Items, Has.Member("help commands"));
		}


		[Test]
		public void TestDoesNotFilterSuggestionWithRepeatedParams()
		{
			var suggestion = "stream [num] on [axes...] line abs [pos...]";
			_autocompleteBox.CommandList = new ObservableCollection<string> { suggestion };

			TypeText(_textBox, "stream 1 on a ");
			Assert.IsTrue(_listBox.Items.Contains(suggestion));

			TypeText(_textBox, "b line abs 1");
			Assert.IsTrue(_listBox.Items.Contains(suggestion));

			TypeText(_textBox, " 2 3");
			Assert.IsTrue(_listBox.Items.Contains(suggestion));
		}


		[Test]
		public void TestEmpty_textBox()
		{
			Assert.IsEmpty(_textBox.Text);
			Assert.IsEmpty(_textBox.SelectedText);
			Assert.IsEmpty(_listBox.Items);
		}


		[Test]
		public void TestEnteredTextMirrors_textBoxText()
		{
			Assert.IsEmpty(_textBox.Text);
			Assert.IsEmpty(_autocompleteBox.EnteredText);

			TypeKey(_textBox, Key.A);
			TypeKey(_textBox, Key.B);
			TypeKey(_textBox, Key.C);
			Assert.AreEqual("abc", _textBox.Text);
			Assert.AreEqual("abc", _autocompleteBox.EnteredText);

			TypeKey(_textBox, Key.Back);
			Assert.AreEqual("ab", _textBox.Text);
			Assert.AreEqual("ab", _autocompleteBox.EnteredText);
		}


		[Test]
		public void TestFiltersCompletePartialArguments()
		{
			TypeText(_textBox, "he co");
			Assert.That(_listBox.Items, Has.No.Member("home"));
			Assert.That(_listBox.Items, Has.No.Member("home commands"));
		}


		[Test]
		public void TestHistory()
		{
			TypeKeys(_textBox, Key.F, Key.O, Key.O);
			Assert.AreEqual("foo", _textBox.Text);
			TypeKey(_textBox, Key.Return);
			TypeKeys(_textBox, Key.Back, Key.Back, Key.Back);

			TypeKeys(_textBox, Key.B, Key.A, Key.R);
			Assert.AreEqual("bar", _textBox.Text);
			TypeKey(_textBox, Key.Return);
			TypeKeys(_textBox, Key.Back, Key.Back, Key.Back);

			TypeKey(_textBox, Key.Escape);
			TypeKey(_textBox, Key.Up);
			Assert.AreEqual("bar", _textBox.Text);
			TypeKey(_textBox, Key.Up);
			Assert.AreEqual("foo", _textBox.Text);
			TypeKey(_textBox, Key.Down);
			Assert.AreEqual("bar", _textBox.Text);
		}


		[Test]
		public void TestListBoxHandlesDoubleClickOnItem()
		{
			_textBox.Text = "virtual [vnum] setup angle 123";
			Assert.IsNotEmpty(_listBox.Items);

			_listBox.SelectedIndex = 0;
			Assert.AreEqual("virtual [vnum] setup angle [master] [slave] [theta]", _listBox.SelectedItem);

			Click(_listBox);
			Assert.AreEqual("virtual [vnum] setup angle 123 [slave] [theta]", _textBox.Text);
			Assert.AreEqual("[vnum]", _textBox.SelectedText);
		}


		[Test]
		public void TestListBoxHandlesEnter()
		{
			TypeKey(_textBox, Key.M);
			Assert.IsNotEmpty(_listBox.Items);

			TypeKey(_textBox, Key.Down);
			TypeKey(_listBox, Key.Down);
			TypeKey(_listBox, Key.Down);

			Assert.AreEqual("m", _textBox.Text);
			Assert.AreEqual(2, _listBox.SelectedIndex);
			Assert.AreEqual("move abs [x]", _listBox.SelectedItem);

			TypeKey(_listBox, Key.Enter);
			Assert.AreEqual("move abs [x]", _textBox.Text);
			Assert.AreEqual("[x]", _textBox.SelectedText);
		}


		[Test]
		public void TestListBoxHandlesEnterAndKeepsEnteredVariableArguments()
		{
			_textBox.Text = "virtual [vnum] setup angle 123";
			Assert.IsNotEmpty(_listBox.Items);

			TypeKey(_textBox, Key.Down);
			Assert.AreEqual(0, _listBox.SelectedIndex);
			Assert.AreEqual("virtual [vnum] setup angle [master] [slave] [theta]", _listBox.SelectedItem);

			TypeKey(_listBox, Key.Enter);
			Assert.AreEqual("virtual [vnum] setup angle 123 [slave] [theta]", _textBox.Text);
			Assert.AreEqual("[vnum]", _textBox.SelectedText);
		}


		[Test]
		public void TestListBoxHandlesRightArrow()
		{
			TypeKey(_textBox, Key.M);
			Assert.IsNotEmpty(_listBox.Items);

			TypeKey(_textBox, Key.Down);
			TypeKey(_listBox, Key.Down);
			TypeKey(_listBox, Key.Down);

			Assert.AreEqual("m", _textBox.Text);
			Assert.AreEqual(2, _listBox.SelectedIndex);
			Assert.AreEqual("move abs [x]", _listBox.SelectedItem);

			TypeKey(_listBox, Key.Right);
			Assert.AreEqual("move abs [x]", _textBox.Text);
			Assert.AreEqual("[x]", _textBox.SelectedText);
		}


		[Test]
		public void TestListBoxHandlesTab()
		{
			TypeKey(_textBox, Key.M);
			Assert.AreEqual("move", _textBox.Text);
			Assert.IsNotEmpty(_listBox.Items);

			var index = 2;
			_listBox.SelectedIndex = index;
			Assert.False(_textBox.IsFocused);
			Assert.AreEqual("move abs [x]", _listBox.Items[index]);

			TypeKey(_listBox, Key.Tab);
			Assert.True(_textBox.IsFocused);
			Assert.AreEqual("move abs", _textBox.Text);

			TypeKey(_listBox, Key.Tab);
			Assert.True(_textBox.IsFocused);
			Assert.AreEqual("move abs [x]", _textBox.Text);
			Assert.AreEqual("[x]", _textBox.SelectedText);
		}


		[Test]
		public void TestListBoxHandlesUpArrowKeyAtTop()
		{
			TypeKey(_textBox, Key.M);
			Assert.AreEqual("m".Length, _textBox.CaretIndex);
			Assert.AreEqual(-1, _listBox.SelectedIndex);
			Assert.IsNotEmpty(_listBox.Items);

			TypeKey(_textBox, Key.Down);
			Assert.AreEqual(0, _textBox.CaretIndex);

			TypeKey(_listBox, Key.Up);
			Assert.AreEqual("m".Length, _textBox.CaretIndex);
			Assert.AreEqual(-1, _listBox.SelectedIndex);

			// Test that going past the top of the list doesn't produce an error.
			TypeKey(_listBox, Key.Up);
			Assert.AreEqual("m".Length, _textBox.CaretIndex);
			Assert.AreEqual(-1, _listBox.SelectedIndex);
		}


		[Test]
		public void TestMultipleArgumentFilteringTabTabFromListBox()
		{
			TypeKey(_textBox, Key.M);
			Assert.That(_listBox.Items, Has.Member("move max"));
			Assert.That(_listBox.Items, Has.Member("move min"));
			Assert.That(_listBox.Items, Has.Member("move abs [x]"));
			Assert.That(_listBox.Items, Has.No.Member("home"));

			TypeKey(_textBox, Key.Down);
			TypeKey(_listBox, Key.Down);
			TypeKey(_listBox, Key.Down);
			Assert.AreEqual(2, _listBox.SelectedIndex);
			Assert.AreEqual("move abs [x]", _listBox.SelectedItem);

			TypeKey(_listBox, Key.Tab);
			Assert.That(_listBox.Items, Has.Member("move max"));
			Assert.That(_listBox.Items, Has.Member("move min"));
			Assert.That(_listBox.Items, Has.Member("move abs [x]"));

			TypeKey(_listBox, Key.Tab);
			Assert.That(_listBox.Items, Has.No.Member("move max"));
			Assert.That(_listBox.Items, Has.No.Member("move min"));
			Assert.That(_listBox.Items, Has.Member("move abs [x]"));
		}


		[Test]
		public void TestMultipleArgumentFilteringTabTabFromTextBox()
		{
			TypeKey(_textBox, Key.M);
			Assert.That(_listBox.Items, Has.Member("move max"));
			Assert.That(_listBox.Items, Has.Member("move min"));
			Assert.That(_listBox.Items, Has.Member("move abs [x]"));
			Assert.That(_listBox.Items, Has.No.Member("home"));

			TypeKey(_textBox, Key.Tab);
			Assert.That(_listBox.Items, Has.Member("move max"));
			Assert.That(_listBox.Items, Has.Member("move min"));
			Assert.That(_listBox.Items, Has.Member("move abs [x]"));

			TypeKey(_textBox, Key.Tab);
			Assert.That(_listBox.Items, Has.Member("move max"));
			Assert.That(_listBox.Items, Has.No.Member("move min"));
		}


		[Test]
		public void TestMultipleArgumentFilteringTabTypeFromTextBox()
		{
			TypeKey(_textBox, Key.M);
			Assert.That(_listBox.Items, Has.Member("move max"));
			Assert.That(_listBox.Items, Has.Member("move min"));
			Assert.That(_listBox.Items, Has.Member("move abs [x]"));
			Assert.That(_listBox.Items, Has.No.Member("home"));

			TypeKey(_textBox, Key.Tab);
			Assert.That(_listBox.Items, Has.Member("move max"));
			Assert.That(_listBox.Items, Has.Member("move min"));
			Assert.That(_listBox.Items, Has.Member("move abs [x]"));

			TypeKey(_textBox, Key.M);
			Assert.That(_listBox.Items, Has.Member("move max"));
			Assert.That(_listBox.Items, Has.Member("move min"));
			Assert.That(_listBox.Items, Has.No.Member("move abs [x]"));

			TypeKey(_textBox, Key.I);
			Assert.That(_listBox.Items, Has.No.Member("move max"));
			Assert.That(_listBox.Items, Has.Member("move min"));
		}


		[Test]
		public void TestMultipleArgumentSelectionTabTab()
		{
			TypeKey(_textBox, Key.M);
			Assert.AreEqual("move", _textBox.Text);
			Assert.AreEqual("ove", _textBox.SelectedText);

			TypeKey(_textBox, Key.Tab);
			Assert.AreEqual("move max", _textBox.Text);
			Assert.AreEqual("max", _textBox.SelectedText);

			TypeKey(_textBox, Key.Tab);
			Assert.AreEqual("move max", _textBox.Text);
			Assert.IsEmpty(_textBox.SelectedText);
		}


		[Test]
		public void TestMultipleArgumentSelectionTabType()
		{
			TypeKey(_textBox, Key.M);
			Assert.AreEqual("move", _textBox.Text);
			Assert.AreEqual("ove", _textBox.SelectedText);

			TypeKey(_textBox, Key.Tab);
			Assert.AreEqual("move max", _textBox.Text);
			Assert.AreEqual("max", _textBox.SelectedText);

			TypeKey(_textBox, Key.M);
			Assert.AreEqual("move max", _textBox.Text);
			Assert.AreEqual("ax", _textBox.SelectedText);

			TypeKey(_textBox, Key.I);
			Assert.AreEqual("move min", _textBox.Text);
			Assert.AreEqual("n", _textBox.SelectedText);
		}


		[Test]
		public void TestMultipleArgumentSelectionTypeType()
		{
			TypeKey(_textBox, Key.M);
			Assert.AreEqual("move", _textBox.Text);
			Assert.AreEqual("ove", _textBox.SelectedText);

			TypeText(_textBox, "ove");
			Assert.AreEqual("move max", _textBox.Text);
			Assert.AreEqual(" max", _textBox.SelectedText);

			TypeKey(_textBox, Key.Space);
			Assert.AreEqual("move max", _textBox.Text);
			Assert.AreEqual("max", _textBox.SelectedText);

			TypeKey(_textBox, Key.M);
			Assert.AreEqual("move max", _textBox.Text);
			Assert.AreEqual("ax", _textBox.SelectedText);
		}


		[Test]
		public void TestTabSelectsNextVariableArgument()
		{
			_textBox.Text = "virtual [vnum] setup angle [master] [slave] [theta]";
			_textBox.CaretIndex = _textBox.Text.Length;

			Assert.AreEqual("virtual [vnum] setup angle [master] [slave] [theta]", _textBox.Text);
			Assert.IsEmpty(_textBox.SelectedText);

			for (var i = 0; i < 3; ++i)
			{
				TypeKey(_textBox, Key.Tab);
				Assert.AreEqual("virtual [vnum] setup angle [master] [slave] [theta]", _textBox.Text);
				Assert.AreEqual("[vnum]", _textBox.SelectedText);
			}

			TypeKey(_textBox, Key.D0);
			Assert.AreEqual("virtual 0 setup angle [master] [slave] [theta]", _textBox.Text);
			Assert.IsEmpty(_textBox.SelectedText);

			TypeKey(_textBox, Key.Tab);
			Assert.AreEqual("virtual 0 setup angle [master] [slave] [theta]", _textBox.Text);
			Assert.AreEqual("[master]", _textBox.SelectedText);

			TypeKey(_textBox, Key.D1);
			TypeKey(_textBox, Key.D2);
			TypeKey(_textBox, Key.D3);
			Assert.AreEqual("virtual 0 setup angle 123 [slave] [theta]", _textBox.Text);
			Assert.IsEmpty(_textBox.SelectedText);

			TypeKey(_textBox, Key.Tab);
			Assert.AreEqual("virtual 0 setup angle 123 [slave] [theta]", _textBox.Text);
			Assert.AreEqual("[slave]", _textBox.SelectedText);
		}


		[Test]
		public void TestTextCannotBeCompleted()
		{
			TypeKey(_textBox, Key.X);
			Assert.AreEqual("x", _textBox.Text);
			Assert.IsEmpty(_textBox.SelectedText);
			Assert.IsEmpty(_listBox.Items);

			TypeKey(_textBox, Key.Y);
			Assert.AreEqual("xy", _textBox.Text);
			Assert.IsEmpty(_textBox.SelectedText);
			Assert.IsEmpty(_listBox.Items);

			TypeKey(_textBox, Key.Space);
			Assert.AreEqual("xy ", _textBox.Text);
			Assert.IsEmpty(_textBox.SelectedText);
			Assert.IsEmpty(_listBox.Items);

			TypeKey(_textBox, Key.Z);
			Assert.AreEqual("xy z", _textBox.Text);
			Assert.IsEmpty(_textBox.SelectedText);
			Assert.IsEmpty(_listBox.Items);
		}


		[Test]
		public void TestTextCannotBeCompletedAnymore()
		{
			TypeText(_textBox, "move ");
			Assert.AreEqual("move max", _textBox.Text);
			Assert.AreEqual("max", _textBox.SelectedText);
			Assert.IsNotEmpty(_listBox.Items);

			TypeKey(_textBox, Key.P);
			TypeKey(_textBox, Key.L);
			TypeKey(_textBox, Key.Z);
			Assert.AreEqual("move plz", _textBox.Text);
			Assert.IsEmpty(_textBox.SelectedText);
			Assert.IsEmpty(_listBox.Items);
		}


		[Test]
		public void TestTextDoesNotCompleteIfCaretIsNotAtEndOf_textBox()
		{
			TypeText(_textBox, "mve");
			Assert.AreEqual("mve", _textBox.Text);
			Assert.IsEmpty(_textBox.SelectedText);

			Assert.AreEqual("mve".Length, _textBox.CaretIndex);

			_textBox.CaretIndex = 1;
			TypeKey(_textBox, Key.O);
			Assert.AreEqual("move", _textBox.Text);
			Assert.IsEmpty(_textBox.SelectedText);
		}


		[Test]
		public void TestUnselectedTextIsOppositeof_textBoxSelectedText()
		{
			Assert.IsEmpty(_textBox.Text);
			Assert.IsEmpty(_autocompleteBox.UnselectedText);

			TypeKey(_textBox, Key.M);
			Assert.AreEqual("move", _textBox.Text);
			Assert.AreEqual("m", _autocompleteBox.UnselectedText);
			Assert.AreEqual("ove", _textBox.SelectedText);

			TypeKey(_textBox, Key.O);
			Assert.AreEqual("move", _textBox.Text);
			Assert.AreEqual("mo", _autocompleteBox.UnselectedText);
			Assert.AreEqual("ve", _textBox.SelectedText);

			TypeKey(_textBox, Key.Tab);
			Assert.AreEqual("move max", _textBox.Text);
			Assert.AreEqual("move ", _autocompleteBox.UnselectedText);
			Assert.AreEqual("max", _textBox.SelectedText);
		}


		[Test]
		public void TestUpArrowOnEmptyHistory()
		{
			TypeKeys(_textBox, Key.H);
			TypeKey(_textBox, Key.Up);
		}


		[Test]
		public void TestVariableArgumentCompletionTabbing()
		{
			TypeKey(_textBox, Key.V);
			Assert.AreEqual("virtual", _textBox.Text);
			Assert.AreEqual("irtual", _textBox.SelectedText);

			for (var i = 0; i < 3; ++i)
			{
				TypeKey(_textBox, Key.Tab);
				Assert.AreEqual("virtual [vnum]", _textBox.Text);
				Assert.AreEqual("[vnum]", _textBox.SelectedText);
			}

			TypeKey(_textBox, Key.D0);
			Assert.AreEqual("virtual 0 setup", _textBox.Text);
			Assert.AreEqual(" setup", _textBox.SelectedText);

			TypeKey(_textBox, Key.Tab);
			Assert.AreEqual("virtual 0 setup angle", _textBox.Text);
			Assert.AreEqual("angle", _textBox.SelectedText);

			TypeKey(_textBox, Key.Tab);
			Assert.AreEqual("virtual 0 setup angle [master]", _textBox.Text);
			Assert.AreEqual("[master]", _textBox.SelectedText);

			TypeKey(_textBox, Key.D1);
			Assert.AreEqual("virtual 0 setup angle 1 [slave]", _textBox.Text);
			Assert.AreEqual(" [slave]", _textBox.SelectedText);

			TypeKey(_textBox, Key.D2);
			TypeKey(_textBox, Key.D3);
			Assert.AreEqual("virtual 0 setup angle 123 [slave]", _textBox.Text);
			Assert.AreEqual(" [slave]", _textBox.SelectedText);

			TypeKey(_textBox, Key.Tab);
			Assert.AreEqual("virtual 0 setup angle 123 [slave]", _textBox.Text);
			Assert.AreEqual("[slave]", _textBox.SelectedText);

			TypeKey(_textBox, Key.D4);
			TypeKey(_textBox, Key.D5);
			TypeKey(_textBox, Key.D6);
			Assert.AreEqual("virtual 0 setup angle 123 456 [theta]", _textBox.Text);
			Assert.AreEqual(" [theta]", _textBox.SelectedText);

			TypeKey(_textBox, Key.Tab);
			Assert.AreEqual("virtual 0 setup angle 123 456 [theta]", _textBox.Text);
			Assert.AreEqual("[theta]", _textBox.SelectedText);

			TypeKey(_textBox, Key.D7);
			TypeKey(_textBox, Key.D8);
			TypeKey(_textBox, Key.D9);
			Assert.AreEqual("virtual 0 setup angle 123 456 789", _textBox.Text);
			Assert.IsEmpty(_textBox.SelectedText);

			TypeKey(_textBox, Key.Tab);
			Assert.AreEqual("virtual 0 setup angle 123 456 789", _textBox.Text);
			Assert.IsEmpty(_textBox.SelectedText);
		}


		[Test]
		public void TestVariableArgumentCompletionTyping()
		{
			TypeText(_textBox, "virtua");
			Assert.AreEqual("virtual", _textBox.Text);
			Assert.AreEqual("l", _textBox.SelectedText);

			TypeKey(_textBox, Key.L);
			Assert.AreEqual("virtual [vnum]", _textBox.Text);
			Assert.AreEqual(" [vnum]", _textBox.SelectedText);

			TypeKey(_textBox, Key.Space);
			Assert.AreEqual("virtual [vnum]", _textBox.Text);
			Assert.AreEqual("[vnum]", _textBox.SelectedText);

			TypeKey(_textBox, Key.D0);
			Assert.AreEqual("virtual 0 setup", _textBox.Text);
			Assert.AreEqual(" setup", _textBox.SelectedText);
		}


		[Test]
		public void TestVariableArgumentFiltering()
		{
			var keys = "virtual 1 setup angle 123 456 789";

			foreach (var key in StringToKeys(keys))
			{
				TypeKey(_textBox, key);
				Assert.That(_listBox.Items, Has.No.Member("home"));
				Assert.That(_listBox.Items, Has.Member("virtual [vnum] setup angle [master] [slave] [theta]"));
			}
		}


		internal class MockPresentationSource : PresentationSource
		{
			#region -- Public Methods & Properties --

			public override Visual RootVisual { get; set; }

			public override bool IsDisposed => false;

			#endregion

			#region -- Non-Public Methods --

			protected override CompositionTarget GetCompositionTargetCore() => null;

			#endregion
		}


		private void TypeKeys(Control aTarget, params Key[] aKeyList)
		{
			foreach (var key in aKeyList)
			{
				TypeKey(aTarget, key);
			}
		}


		private void TypeKeys(Control aTarget, IEnumerable<Key> aKeyList)
		{
			foreach (var key in aKeyList)
			{
				TypeKey(aTarget, key);
			}
		}


		private void TypeText(Control aTarget, string aText)
		{
			foreach (var key in StringToKeys(aText))
			{
				TypeKey(aTarget, key);
			}
		}


		private void TypeKey(Control aTarget, Key aKey)
		{
			var routedEvent = Keyboard.PreviewKeyDownEvent;
			var presentationSource = new MockPresentationSource();

			aTarget.RaiseEvent(new KeyEventArgs(Keyboard.PrimaryDevice,
												presentationSource,
												0,
												aKey) { RoutedEvent = routedEvent });

			routedEvent = Keyboard.KeyDownEvent;
			aTarget.RaiseEvent(new KeyEventArgs(Keyboard.PrimaryDevice,
												presentationSource,
												0,
												aKey) { RoutedEvent = routedEvent });


			var text = KeyToString(aKey);
			if (string.IsNullOrEmpty(text))
			{
				return;
			}

			routedEvent = TextCompositionManager.TextInputEvent;
			aTarget.RaiseEvent(new TextCompositionEventArgs(Keyboard.PrimaryDevice,
															new TextComposition(InputManager.Current, aTarget, text))
			{
				RoutedEvent = routedEvent
			});
		}


		private void Click(Control aTarget) => aTarget.RaiseEvent(new MouseButtonEventArgs(Mouse.PrimaryDevice,
																								 0,
																								 MouseButton.Left)
		{
			RoutedEvent = Control.MouseUpEvent
		});


		private string KeyToString(Key aKey)
		{
			if ((Key.A <= aKey) && (aKey <= Key.Z))
			{
				return aKey.ToString().ToLower();
			}

			if ((Key.D0 <= aKey) && (aKey <= Key.D9))
			{
				return aKey.ToString().Substring(1);
			}

			return null;
		}


		// Input casing ignored. Only handles letters, digits,
		// space, tab and enter.
		private IEnumerable<Key> StringToKeys(string aText)
		{
			foreach (var c in aText.ToUpperInvariant())
			{
				Key result;
				if (Enum.TryParse(c.ToString(), out result) && (result.ToString() == c.ToString()))
				{
					yield return result;
				}
				else if (' ' == c)
				{
					yield return Key.Space;
				}
				else if ('\t' == c)
				{
					yield return Key.Tab;
				}
				else if ('\n' == c)
				{
					yield return Key.Enter;
				}
				else if (Enum.TryParse("D" + c, out result))
				{
					yield return result;
				}
				else
				{
					Assert.Fail($"Can't handle character '{c}'.");
				}
			}
		}


		private KeyEventArgs KeyEventArgs(Key aKey)
			=> new KeyEventArgs(Keyboard.PrimaryDevice, new MockPresentationSource(), 0, aKey);


		private AutocompleteBox _autocompleteBox;
		private TextBox _textBox;
		private ListBox _listBox;
	}
}

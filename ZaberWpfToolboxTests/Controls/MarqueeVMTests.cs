﻿using System;
using System.Threading;
using NUnit.Framework;
using Zaber.Testing;
using ZaberTest.Testing;
using ZaberWpfToolbox.Controls;

namespace ZaberWpfToolboxTests.Controls
{
	[TestFixture]
	[SetCulture("en-US")]
	public class MarqueeVMTests
	{
		[SetUp]
		public void Setup()
		{
			DispatcherStack.Push(new ImmediateDispatcher());
			_updateTimer = new MockDispatcherTimer();
			DispatcherTimerStack.Push(_updateTimer);
			_expiryTimer = new MockDispatcherTimer();
			DispatcherTimerStack.Push(_expiryTimer);

			_vm = new MarqueeVM();
			_observer = new ObservableObjectTester();
			_observer.Listen(_vm);
		}


		[TearDown]
		public void Teardown()
		{
			DispatcherTimerStack.Reset();
			DispatcherStack.Pop();
		}


		[Test]
		public void TestAdd()
		{
			_observer.ClearNotifications();
			_vm.AddMessage(_owner1, "Foo", 1000);
			Assert.IsTrue(_observer.PropertyWasChanged(nameof(_vm.CurrentText)));
			Assert.IsTrue(_observer.PropertyWasChanged(nameof(_vm.AllText)));
			Assert.AreEqual("Foo", _vm.CurrentText);
			Assert.AreEqual("Foo", _vm.AllText);

			_observer.ClearNotifications();
			_vm.AddMessage(_owner1, "Bar", 1000);
			Assert.IsFalse(_observer.PropertyWasChanged(nameof(_vm.CurrentText)));
			Assert.IsTrue(_observer.PropertyWasChanged(nameof(_vm.AllText)));
			Assert.AreEqual("Foo", _vm.CurrentText);
			Assert.AreEqual("Foo" + Environment.NewLine + "Bar", _vm.AllText);
		}


		[Test]
		public void TestClear()
		{
			_vm.AddMessage(_owner1, "Foo", 1000);
			_vm.AddMessage(_owner2, "Bar", 1000);
			_vm.AddMessage(_owner1, "Baz", 1000);
			_vm.AddMessage(_owner2, "Quux", 1000);
			Assert.AreEqual("Foo", _vm.CurrentText);
			Assert.AreEqual(
				"Foo" + Environment.NewLine + "Bar" + Environment.NewLine + "Baz" + Environment.NewLine + "Quux",
				_vm.AllText);
			Assert.IsTrue(_updateTimer.IsEnabled);

			_observer.ClearNotifications();
			_vm.ClearMessages(_owner2);
			Assert.IsFalse(_observer.PropertyWasChanged(nameof(_vm.CurrentText)));
			Assert.IsTrue(_observer.PropertyWasChanged(nameof(_vm.AllText)));
			Assert.AreEqual("Foo", _vm.CurrentText);
			Assert.AreEqual("Foo" + Environment.NewLine + "Baz",
							_vm.AllText);

			_observer.ClearNotifications();
			_vm.ClearMessages(_owner1);
			Assert.IsTrue(_observer.PropertyWasChanged(nameof(_vm.CurrentText)));
			Assert.IsTrue(_observer.PropertyWasChanged(nameof(_vm.AllText)));
			Assert.IsTrue(string.IsNullOrEmpty(_vm.CurrentText));
			Assert.IsTrue(string.IsNullOrEmpty(_vm.AllText));
			Assert.IsFalse(_updateTimer.IsEnabled);
		}


		[Test]
		public void TestConstructor()
		{
			Assert.IsTrue(string.IsNullOrEmpty(_vm.CurrentText));
			Assert.IsTrue(string.IsNullOrEmpty(_vm.AllText));
		}


		[Test]
		public void TestExpiry()
		{
			var startTime = DateTime.Now;
			_vm.AddMessage(_owner1, "Foo", 2);
			_vm.AddMessage(_owner2, "Bar", 4);
			var expectedAllText = "Foo" + Environment.NewLine + "Bar";

			Assert.AreEqual("Foo", _vm.CurrentText);
			Assert.AreEqual(expectedAllText, _vm.AllText);
			Assert.IsTrue(_updateTimer.IsEnabled);
			Assert.IsTrue(_expiryTimer.IsEnabled);

			while ((DateTime.Now - startTime).TotalMilliseconds < 2000)
			{
				Thread.Sleep(100);
			}

			// The VM uses wall clock time to expire messages; it's possible for the test under 
			// CI conditions to miss intervals and have a race condition with the VM.
			// This if statement is a fudge to try to avoid the race condition.
			if ((DateTime.Now - startTime).TotalMilliseconds < 3500)
			{
				_observer.ClearNotifications();
				_expiryTimer.FireTickEvent();
				Assert.IsTrue(_observer.PropertyWasChanged(nameof(_vm.CurrentText)));
				Assert.IsTrue(_observer.PropertyWasChanged(nameof(_vm.AllText)));
				Assert.AreEqual("Bar", _vm.CurrentText);
				Assert.AreEqual("Bar", _vm.AllText);
			}

			while ((DateTime.Now - startTime).TotalMilliseconds < 4000)
			{
				Thread.Sleep(100);
			}

			_observer.ClearNotifications();
			_expiryTimer.FireTickEvent();
			Assert.IsTrue(_observer.PropertyWasChanged(nameof(_vm.CurrentText)));
			Assert.IsTrue(_observer.PropertyWasChanged(nameof(_vm.AllText)));
			Assert.IsTrue(string.IsNullOrEmpty(_vm.CurrentText));
			Assert.IsTrue(string.IsNullOrEmpty(_vm.AllText));
			Assert.IsFalse(_updateTimer.IsEnabled);
			Assert.IsFalse(_expiryTimer.IsEnabled);
		}


		[Test]
		public void TestObservableProperties()
		{
			_observer.TestObservableProperty(_vm, () => _vm.CurrentText);
			_observer.TestObservableProperty(_vm, () => _vm.AllText);
		}


		[Test]
		public void TestUpdate()
		{
			_vm.AddMessage(_owner1, "Foo", 1000);
			_vm.AddMessage(_owner2, "Bar", 1000);
			var expectedAllText = "Foo" + Environment.NewLine + "Bar";

			Assert.AreEqual("Foo", _vm.CurrentText);
			Assert.AreEqual(expectedAllText, _vm.AllText);

			_observer.ClearNotifications();
			_updateTimer.FireTickEvent();
			Assert.IsTrue(_observer.PropertyWasChanged(nameof(_vm.CurrentText)));
			Assert.IsFalse(_observer.PropertyWasChanged(nameof(_vm.AllText)));
			Assert.AreEqual("Bar", _vm.CurrentText);
			Assert.AreEqual(expectedAllText, _vm.AllText);

			_observer.ClearNotifications();
			_updateTimer.FireTickEvent();
			Assert.IsTrue(_observer.PropertyWasChanged(nameof(_vm.CurrentText)));
			Assert.IsFalse(_observer.PropertyWasChanged(nameof(_vm.AllText)));
			Assert.AreEqual("Foo", _vm.CurrentText);
			Assert.AreEqual(expectedAllText, _vm.AllText);
		}


		private MockDispatcherTimer _expiryTimer;
		private MockDispatcherTimer _updateTimer;
		private MarqueeVM _vm;
		private ObservableObjectTester _observer;
		private readonly object _owner1 = new object();
		private readonly object _owner2 = new object();
	}
}

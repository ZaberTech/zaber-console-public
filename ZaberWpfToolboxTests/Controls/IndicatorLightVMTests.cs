﻿using System.Linq;
using System.Windows.Media;
using NUnit.Framework;
using Zaber.Testing;
using ZaberTest.Testing;
using ZaberWpfToolbox.Controls;

namespace ZaberWpfToolboxTests.Controls
{
	[TestFixture]
	[SetCulture("en-US")]
	public class IndicatorLightVMTests
	{
		[SetUp]
		public void Setup()
		{
			DispatcherStack.Push(new ImmediateDispatcher());
			_timer = new MockDispatcherTimer();
			DispatcherTimerStack.Push(_timer);

			_vm = new IndicatorLightVM();
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
		}


		[TearDown]
		public void Teardown()
		{
			_listener.Unlisten(_vm);
			DispatcherStack.Pop();
			DispatcherTimerStack.Reset();
		}


		[Test]
		public void TestConstructor()
		{
			Assert.IsFalse(_vm.On);
			Assert.IsFalse(_vm.Lit);
			Assert.IsFalse(_vm.FlashWhenOn);
			Assert.AreEqual(250, _vm.LitPeriod);
			Assert.AreEqual(250, _vm.UnlitPeriod);
			Assert.AreEqual(Colors.Black, _vm.UnlitColor);
			Assert.AreEqual(Colors.Lime, _vm.LitColor);
			Assert.IsTrue(string.IsNullOrEmpty(_vm.Text));
			Assert.IsTrue(string.IsNullOrEmpty(_vm.ToolTip));
		}


		[Test]
		public void TestMultiFlashBehavior()
		{
			_vm.LitPeriod = _vm.UnlitPeriod = 50;
			_vm.FlashWhenOn = true;

			// Turn the light on and wait for several blinks.
			_vm.On = true;
			_listener.ClearNotifications();
			for (var i = 0; i < 4; i++)
			{
				_timer.FireTickEvent();
				DispatcherUtil.DoEventsSync();
			}

			// Verify the light has flashed multiple times and is still considered on.
			Assert.IsTrue(_vm.On);
			var notifications = _listener.GetPropertiesChanged().Where(p => "Lit" == p);
			var num = notifications.Count();
			Assert.AreEqual(num, 4); // Light should have flashed several times.

			// Make sure it doesn't keep blinking when turned off.
			_vm.On = false;
			DispatcherUtil.DoEventsSync();
			notifications = _listener.GetPropertiesChanged().Where(p => "Lit" == p);
			num = notifications.Count();
			Assert.IsFalse(_vm.Lit);
			Assert.IsFalse(_vm.On);
			Assert.IsFalse(_timer.IsEnabled);
			DispatcherUtil.SleepAndDoSyncEvents(1000, 50);

			Assert.IsFalse(_vm.Lit);
			Assert.IsFalse(_vm.On);
			notifications = _listener.GetPropertiesChanged().Where(p => "Lit" == p);
			Assert.AreEqual(num,
							notifications.Count()); // Should have been no more blicks after processing the off command.
		}


		[Test]
		public void TestNonObservableProperties()
		{
			_listener.TestNonObservableProperty(_vm, () => _vm.LitPeriod, 100, 200);
			_listener.TestNonObservableProperty(_vm, () => _vm.UnlitPeriod, 100, 200);
			_listener.TestNonObservableProperty(_vm, () => _vm.FlashWhenOn, true, false);
		}


		[Test]
		public void TestObservableProperties()
		{
			_listener.TestObservableProperty(_vm, () => _vm.Lit);
			_listener.TestObservableProperty(_vm, () => _vm.Text);
			_listener.TestObservableProperty(_vm, () => _vm.ToolTip);
			_listener.TestObservableProperty(_vm,
											 () => _vm.UnlitColor,
											 aColor =>
											 {
												 return Color.FromArgb((byte) (aColor.A ^ 0xFF),
																	   (byte) (aColor.R ^ 0xFF),
																	   (byte) (aColor.G ^ 0xFF),
																	   (byte) (aColor.B ^ 0xFF));
											 });
			_listener.TestObservableProperty(_vm,
											 () => _vm.LitColor,
											 aColor =>
											 {
												 return Color.FromArgb((byte) (aColor.A ^ 0xFF),
																	   (byte) (aColor.R ^ 0xFF),
																	   (byte) (aColor.G ^ 0xFF),
																	   (byte) (aColor.B ^ 0xFF));
											 });
		}


		// Tests that the light flashes once when triggered in default mode, then turns off.
		// Note this test could be sensitive to CPU load, resulting in rare false negatives.
		[Test]
		public void TestSingleFlashBehavior()
		{
			_vm.LitPeriod = _vm.UnlitPeriod = 100;

			// Turn the light on and wait for the change to process.
			_vm.On = true;
			DispatcherUtil.DoEventsSync();

			// Verify the light is on.
			Assert.IsTrue(_vm.On);
			Assert.IsTrue(_vm.Lit);

			// Wait for the timer to turn the light off.
			_timer.FireTickEvent();
			DispatcherUtil.DoEventsSync();
			Assert.IsFalse(_vm.Lit);
			Assert.IsFalse(_vm.On);

			// Make sure it stays off.
			Assert.IsFalse(_timer.IsEnabled);
			DispatcherUtil.SleepAndDoSyncEvents(100, 25);
			Assert.IsFalse(_vm.Lit);
			Assert.IsFalse(_vm.On);
		}


		private IndicatorLightVM _vm;
		private ObservableObjectTester _listener;
		private MockDispatcherTimer _timer;
	}
}

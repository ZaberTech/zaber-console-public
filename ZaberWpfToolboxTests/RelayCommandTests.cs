﻿using System;
using NUnit.Framework;
using ZaberWpfToolbox;

namespace ZaberWpfToolboxTests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class RelayCommandTests
	{
		// Test basic operation of a RelayCommand without a CanExecute predicate.
		[Test]
		public void TestBasicConstructor()
		{
			var counter = 0;

			var cmd = new RelayCommand(x => { counter += (int) x; });

			Assert.IsTrue(cmd.CanExecute(2));
			Assert.AreEqual(0, counter);
			cmd.Execute(2);
			Assert.AreEqual(2, counter);
			Assert.IsTrue(cmd.CanExecute(3));
			Assert.AreEqual(2, counter);
			cmd.Execute(3);
			Assert.AreEqual(5, counter);
			Assert.IsTrue(cmd.CanExecute(null));
		}


		// Test failure to supply an action to the single-argument constructor.
		[Test]
		public void TestBasicConstructorNoAction()
			=> Assert.Throws<ArgumentNullException>(() => new RelayCommand(null));


		// Test basic operation with a CanExecute predicate.
		[Test]
		public void TestTwoArgConstructor()
		{
			var counter = 0;

			var cmd = new RelayCommand(x => { counter += (int) x; }, x => x is int);

			Assert.IsTrue(cmd.CanExecute(2));
			Assert.IsFalse(cmd.CanExecute("2"));
			Assert.IsFalse(cmd.CanExecute(2U));
			Assert.IsFalse(cmd.CanExecute(2.0));
			Assert.AreEqual(0, counter);
			cmd.Execute(2);
			Assert.AreEqual(2, counter);
			Assert.IsTrue(cmd.CanExecute(3));
			Assert.IsFalse(cmd.CanExecute("3"));
			Assert.AreEqual(2, counter);
			cmd.Execute(3);
			Assert.AreEqual(5, counter);
			Assert.IsFalse(cmd.CanExecute(null));
		}


		// Test the two-argument constructor with a missing action.
		[Test]
		public void TestTwoArgConstructorNoAction()
			=> Assert.Throws<ArgumentNullException>(() => new RelayCommand(null, _ => true));


		// Test the two-argument constructor with a missing predicate.
		[Test]
		public void TestTwoArgConstructorNoPredicate()
			=> Assert.Throws<ArgumentNullException>(() => new RelayCommand(_ => { }, null));
	}
}

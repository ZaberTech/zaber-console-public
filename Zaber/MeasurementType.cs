﻿namespace Zaber
{
	/// <summary>
	///     Different kinds of measurement represented by the data in different
	///     commands. This is used to help convert units of measure.
	/// </summary>
	public enum MeasurementType
	{
		/// <summary>
		///     Other types of measurement that don't support unit of measure
		///     conversion.
		/// </summary>
		Other,

		/// <summary>
		///     Position a device can move to.
		/// </summary>
		Position,

		/// <summary>
		///     Velocity a device can move at.
		/// </summary>
		Velocity,

		/// <summary>
		///     Acceleration a device can apply.
		/// </summary>
		Acceleration,

		/// <summary>
		///     Electrical current (DC or unspecified).
		/// </summary>
		Current,

		/// <summary>
		///     Electrical current (AC).
		/// </summary>
		CurrentAC,

		/// <summary>
		///     Force.
		/// </summary>
		Force
	}
}

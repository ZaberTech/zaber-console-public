using System;
using System.Globalization;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Zaber
{
	/// <summary>
	///     Exception thrown by a <see cref="Conversation" /> when the response is
	///     an error.
	/// </summary>
	[Serializable]
	public class ErrorResponseException : ConversationException
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor. Initializes the exception with a generic message.
		/// </summary>
		public ErrorResponseException()
			: base("Error response received.")
		{
		}


		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aMessage">The message to display</param>
		public ErrorResponseException(string aMessage)
			: base(aMessage)
		{
		}


		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aMessage">The message to display</param>
		/// <param name="aException">The exception that caused this exception</param>
		public ErrorResponseException(string aMessage, Exception aException)
			: base(aMessage, aException)
		{
		}


		/// <summary>
		///     Initializes a new instance with serialized
		///     data.
		/// </summary>
		/// <param name="aSerializationInfo">
		///     The System.Runtime.Serialization.SerializationInfo that holds the serialized
		///     object data about the exception being thrown.
		/// </param>
		/// <param name="aContext">
		///     The System.Runtime.Serialization.StreamingContext that contains contextual
		///     information about the source or destination.
		/// </param>
		protected ErrorResponseException(SerializationInfo aSerializationInfo, StreamingContext aContext)
			: base(aSerializationInfo, aContext)
		{
			if (null == aSerializationInfo)
			{
				throw new ArgumentNullException(nameof(aSerializationInfo));
			}

			Response = (DeviceMessage) aSerializationInfo.GetValue("response", typeof(DeviceMessage));
		}


		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aResponse">The response that caused the exception</param>
		public ErrorResponseException(DeviceMessage aResponse)
			: base(BuildMessage(aResponse))
		{
			Response = aResponse;
		}


		/// <summary>
		///     Sets the System.Runtime.Serialization.SerializationInfo
		///     with information about the exception.
		/// </summary>
		/// <param name="aSerializationInfo">
		///     The System.Runtime.Serialization.SerializationInfo that holds the serialized
		///     object data about the exception being thrown.
		/// </param>
		/// <param name="aContext">
		///     The System.Runtime.Serialization.StreamingContext that contains contextual
		///     information about the source or destination.
		/// </param>
		/// <exception cref="ArgumentNullException">The info parameter is a null reference.</exception>
		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		public override void GetObjectData(SerializationInfo aSerializationInfo, StreamingContext aContext)
		{
			if (null == aSerializationInfo)
			{
				throw new ArgumentNullException(nameof(aSerializationInfo));
			}

			base.GetObjectData(aSerializationInfo, aContext);

			aSerializationInfo.AddValue("response", Response);
		}


		/// <summary>
		///     The response message from the device.
		/// </summary>
		public DeviceMessage Response { get; }

		#endregion

		#region -- Non-Public Methods --

		private static string BuildMessage(DeviceMessage aResponse)
		{
			if (null != aResponse.Text)
			{
				return string.Format(CultureInfo.CurrentCulture, "Error response: '{0}'.", aResponse.Text);
			}

			return string.Format(CultureInfo.CurrentCulture,
								 "Error response {0} received from device number {1}.",
								 (ZaberError) (int) aResponse.NumericData,
								 aResponse.DeviceNumber);
		}

		#endregion

		#region -- Data --

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Xml;
using Zaber.Units;

namespace Zaber
{
	/// <summary>
	///     Record ratios for converting between different units of measure.
	///     Note the functionality in this class has largely been replaced by the
	///     IUnitConverter implementation from Zaber.Units; this class remains for
	///     backwards compatibility and convenience.
	/// </summary>
	/// <remarks>
	///     As well as the usual conversion ratio, you can also include the step
	///     size of a Zaber device in the conversions.
	/// </remarks>
	public class ConversionTable
	{
		#region -- Public data --

		/// <summary>
		///     A conversion table that contains all the known linear position
		///     units of measure.
		///     This member is DEPRECATED and only included for backwards compatibility;
		///     use <see cref="ZaberDevice.UnitConverter" /> or
		///     <see cref="Convert(Measurement, UnitOfMeasure)" /> instead.
		/// </summary>
		[Obsolete]
		public static readonly ConversionTable LinearPosition;

		/// <summary>
		///     A conversion table that contains all the known linear position
		///     units of measure.
		///     This member is DEPRECATED and only included for backwards compatibility;
		///     use <see cref="ZaberDevice.UnitConverter" /> or
		///     <see cref="Convert(Measurement, UnitOfMeasure)" /> instead.
		/// </summary>
		[Obsolete]
		public static readonly ConversionTable RotaryPosition;

		/// <summary>
		///     A conversion table that contains all the known linear velocity
		///     units of measure.
		///     This member is DEPRECATED and only included for backwards compatibility;
		///     use <see cref="ZaberDevice.UnitConverter" /> or
		///     <see cref="Convert(Measurement, UnitOfMeasure)" /> instead.
		/// </summary>
		[Obsolete]
		public static readonly ConversionTable LinearVelocity;

		/// <summary>
		///     A conversion table that contains all the known rotary velocity
		///     units of measure.
		///     This member is DEPRECATED and only included for backwards compatibility;
		///     use <see cref="ZaberDevice.UnitConverter" /> or
		///     <see cref="Convert(Measurement, UnitOfMeasure)" /> instead.
		/// </summary>
		[Obsolete]
		public static readonly ConversionTable RotaryVelocity;

		/// <summary>
		///     A conversion table that contains all the known linear acceleration
		///     units of measure.
		///     This member is DEPRECATED and only included for backwards compatibility;
		///     use <see cref="ZaberDevice.UnitConverter" /> or
		///     <see cref="Convert(Measurement, UnitOfMeasure)" /> instead.
		/// </summary>
		[Obsolete]
		public static readonly ConversionTable LinearAcceleration;

		/// <summary>
		///     A conversion table that contains all the known rotary acceleration
		///     units of measure.
		///     This member is DEPRECATED and only included for backwards compatibility;
		///     use <see cref="ZaberDevice.UnitConverter" /> or
		///     <see cref="Convert(Measurement, UnitOfMeasure)" /> instead.
		/// </summary>
		[Obsolete]
		public static readonly ConversionTable RotaryAcceleration;

		/// <summary>
		///     A conversion table that converts between amps and milliamps.
		///     This member is DEPRECATED and only included for backwards compatibility;
		///     use <see cref="ZaberDevice.UnitConverter" /> or
		///     <see cref="Convert(Measurement, UnitOfMeasure)" /> instead.
		/// </summary>
		[Obsolete]
		public static readonly ConversionTable ElectricalCurrent;

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Global default unit converter. Does not include device-specific units.
		/// </summary>
		public static ConversionTable Default { get; set; }

		#endregion


		/// <summary>
		///     Static constructor. Initializes <see cref="Default" /> to contain the default global
		///     unit conversion table for Zaber Console.
		/// </summary>
		static ConversionTable()
		{
			// Load the standard unit lib.
			var table1 = Units.UnitConverter.Default;

			// Load legacy Zaber Console unit definitions.
			var reader = XmlReader.Create(Assembly.GetExecutingAssembly()
											   .GetManifestResourceStream("Zaber.ZaberConsoleUnits.xml"));
			var legaLib = new XmlUnitLibrary(reader);
			var parser2 = ComposedUnitParser.NewDefaultUnitParser();
			legaLib.LoadTo(parser2.GetUnitRegistry(), parser2, table1);

			// Use the standard unit library as the fallback.
			Default = new ConversionTable(new UnitConverter(parser2, parser2.GetUnitRegistry(), table1));

			// Initialize the legacy masked tables.
			#pragma warning disable CS0612 // Obsolete
			LinearPosition = new ConversionTable(UnitOfMeasure.InchSymbol,
												 UnitOfMeasure.MillimeterSymbol,
												 UnitOfMeasure.MicrometerSymbol,
												 UnitOfMeasure.MeterSymbol);
			RotaryPosition = new ConversionTable(UnitOfMeasure.RevolutionSymbol,
												 UnitOfMeasure.DegreeSymbol,
												 UnitOfMeasure.RadianSymbol);
			LinearVelocity = new ConversionTable(LinearPosition,
												 UnitOfMeasure.MicrometerSymbol,
												 UnitOfMeasure.MicrometersPerSecondSymbol,
												 UnitOfMeasure.MillimeterSymbol,
												 UnitOfMeasure.MillimetersPerSecondSymbol,
												 UnitOfMeasure.InchSymbol,
												 UnitOfMeasure.InchesPerSecondSymbol,
												 UnitOfMeasure.MeterSymbol,
												 UnitOfMeasure.MetersPerSecondSymbol);
			RotaryVelocity = new ConversionTable(UnitOfMeasure.RevolutionsPerMinuteSymbol,
												 UnitOfMeasure.DegreesPerSecondSymbol,
												 UnitOfMeasure.RadiansPerSecondSymbol);
			LinearAcceleration = new ConversionTable(LinearPosition,
													 UnitOfMeasure.MicrometerSymbol,
													 UnitOfMeasure.MicrometersPerSecondSquaredSymbol,
													 UnitOfMeasure.MillimeterSymbol,
													 UnitOfMeasure.MillimetersPerSecondSquaredSymbol,
													 UnitOfMeasure.InchSymbol,
													 UnitOfMeasure.InchesPerSecondSquaredSymbol,
													 UnitOfMeasure.MeterSymbol,
													 UnitOfMeasure.MetersPerSecondSquaredSymbol);
			RotaryAcceleration = new ConversionTable(UnitOfMeasure.RadiansPerSecondSquaredSymbol,
													 UnitOfMeasure.DegreesPerSecondSquaredSymbol);
			ElectricalCurrent = new ConversionTable(UnitOfMeasure.AmperesSymbol,
													UnitOfMeasure.MilliamperesSymbol);
			#pragma warning restore CS0612
		}


		/// <summary>
		///     Create a conversion table that wraps a pre-existing unit definiton table.
		/// </summary>
		/// <param name="aConverter"></param>
		public ConversionTable(IUnitConverter aConverter)
		{
			UnitConverter = aConverter;
		}


		/// <summary>
		///     Create a conversion table instance with local storage for unit definitions.
		/// </summary>
		public ConversionTable()
		{
			var parser = ComposedUnitParser.NewDefaultUnitParser();
			UnitConverter = new UnitConverter(parser, parser.GetUnitRegistry(), Default.UnitConverter);
			_parent = Default;
		}


		// Constructor for backwards-compatible tables that pretend to know about
		// limited subsets of units.
		private ConversionTable(params string[] aKnownUnitSymbols)
			: this(Default.UnitConverter)
		{
			_unitFilter = aKnownUnitSymbols.ToArray();
		}


		// Constructor for backwards-compatible tables that pretend to know about
		// limited subsets of units.
		private ConversionTable(ConversionTable aBase, params string[] aKnownUnitSymbols)
			: this(Default.UnitConverter)
		{
			var all = new HashSet<string>(aBase._unitFilter);
			foreach (var sym in aKnownUnitSymbols)
			{
				all.Add(sym);
			}

			_unitFilter = all.ToArray();
		}


		/// <summary>
		///     Unit converter used by this table.
		/// </summary>
		public IUnitConverter UnitConverter { get; internal set; }


		/// <summary>
		///     Fallback unit conversion table. This is used to look up units that
		///     aren't defined in the current table, and allows the creation of a hierarchy
		///     of tables where children can define new units or redefine existing units
		///     in a narrower context.
		/// </summary>
		/// <remarks>
		///     Conversion between units defined in different tables will not work unless
		///     they share the same coherent base unit. For this reason the base unit for
		///     each dimension must be decided at the application level.
		/// </remarks>
		public ConversionTable Parent
		{
			get => _parent;
			set
			{
				_parent = value;
				UnitConverter.Parent = value?.UnitConverter;
			}
		}


		/// <summary>
		///     Finds all known units of measure corresponding to a given dimension, expressed
		///     as a motion type and measurement type. This will also search parent tables recursively.
		/// </summary>
		/// <param name="aMotionType">The type of motion to filter by.</param>
		/// <param name="aMeasurementType">The type of measurement to filter by.</param>
		/// <returns>
		///     Zero or more units matching the arguments. Order is not guaranteed.
		///     Uniqueness is guaranteed, as is the return of the most context-specific
		///     unit definitions.
		/// </returns>
		public IEnumerable<UnitOfMeasure> FindUnits(MotionType aMotionType, MeasurementType aMeasurementType)
			=> FindUnitsByDimension(UnitOfMeasure.MotionToDimension(aMotionType, aMeasurementType));


		/// <summary>
		///     Convert a measurement to another unit of measure.
		/// </summary>
		/// <remarks>
		///     In cases where units have different definitions in narrower contexts (ie
		///     device-specific units) it is the caller's responsibility to ensure the
		///     correct version is passed in.
		/// </remarks>
		/// <param name="aFrom">The existing measurement.</param>
		/// <param name="aToUnit">The unit of measure to convert it to.</param>
		/// <returns>
		///     A new measurement with the requested unit of
		///     measure.
		/// </returns>
		/// <exception cref="ArgumentException">
		///     If either unit of measure
		///     isn't in the table.
		/// </exception>
		public Measurement Convert(Measurement aFrom, UnitOfMeasure aToUnit)
		{
			var sourceUnit = aFrom.Unit.Unit;

			if (null == sourceUnit)
			{
				throw new ArgumentException(
					string.Format(CultureInfo.CurrentCulture, "Unknown unit of measure: {0}.", aFrom.Unit),
					nameof(aFrom));
			}

			var destUnit = aToUnit.Unit;

			if (null == destUnit)
			{
				throw new ArgumentException(
					string.Format(CultureInfo.CurrentCulture, "Unknown unit of measure: {0}.", aToUnit),
					nameof(aToUnit));
			}

			return new Measurement(UnitConverter.Convert((double) aFrom.Value, sourceUnit, destUnit), aToUnit);
		}


		/// <summary>
		///     Find a unit of measure by its unit symbol. This will also search parent tables.
		/// </summary>
		/// <param name="aAbbrev">The unit symbol.</param>
		/// <returns>
		///     The matching unit definition, if any. If there are multiple
		///     matches, the most context-specific one is returned.
		/// </returns>
		public UnitOfMeasure FindUnitByAbbreviation(string aAbbrev)
		{
			Unit localUnit = null;
			if (UnitConverter.Registry.RegisteredUnits.TryGetValue(aAbbrev, out localUnit))
			{
				if (!_unitWrappers.ContainsKey(localUnit))
				{
					CreateUnitWrapper(localUnit);
				}

				return _unitWrappers[localUnit];
			}

			if (null != Parent)
			{
				return Parent.FindUnitByAbbreviation(aAbbrev);
			}

			return null;
		}


		/// <summary>
		///     Gets an unsorted list of the units of measure used in this table.
		///     This member is DEPRECATED and included for backward compatibility only;
		///     use <see cref="ConversionTable.Default" /> instead.
		/// </summary>
		[Obsolete]
		public IEnumerable<UnitOfMeasure> UnitsOfMeasure
		{
			get
			{
				IEnumerable<string> foo = _unitFilter;
				foo.Count();
				return _unitFilter.Select(s => UnitOfMeasure.FindByAbbreviation(s));
			}
		}


		/// <summary>
		///     Returns a System.String that represents the conversion table.
		///     This method is DEPRECATED and its behavior will be altered in future releases.
		/// </summary>
		/// <returns>
		///     A System.String that represents the unit of measure
		///     abbreviations in this conversion table.
		/// </returns>
		#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member.
		[Obsolete]
		public override string ToString()
		{
			return string.Join(", ", _unitFilter);
		}
		#pragma warning restore CS0809


		/// <summary>
		///     Register a new unit of measure with this table, as a unit from the
		///     Zaber.Units library. This is used by Zaber Console to device device-specific
		///     units like microsteps.
		/// </summary>
		/// <param name="aNewUnit"></param>
		public void RegisterUnit(Unit aNewUnit)
		{
			// Remove wrappers for previous unit with same symbol, if any.
			Unit oldUnit = null;
			if (UnitConverter.Registry.RegisteredUnits.TryGetValue(aNewUnit.Abbreviation, out oldUnit))
			{
				if (_unitWrappers.ContainsKey(oldUnit))
				{
					_unitWrappers.Remove(oldUnit);
				}
			}

			// Store new unit definition. Note this does not create the wrapper; that
			// is done lazily.
			UnitConverter.Registry.RegisteredUnits[aNewUnit.Abbreviation] = aNewUnit;
		}


		/// <summary>
		///     Find all units of measure that have a given dimension. This can be more
		///     precise than <see cref="FindUnits(MotionType, MeasurementType)" /> because it
		///     can find units that are specific to a database-defined dimension that has
		///     no predefined name in code.
		/// </summary>
		/// <param name="aDimension">The dimension to find units of.</param>
		/// <returns>Zero or more units of measure with the given dimension.</returns>
		public IEnumerable<UnitOfMeasure> FindUnitsByDimension(Dimension aDimension)
		{
			var found = new HashSet<string>();

			// Return locally defined units, creating wrappers as needed.
			foreach (var localUnit in UnitConverter.Registry.RegisteredUnits.Values)
			{
				var abbrev = localUnit.Abbreviation;
				if ((aDimension == localUnit.Dimension) && !found.Contains(abbrev))
				{
					found.Add(abbrev);
					if (!_unitWrappers.ContainsKey(localUnit))
					{
						CreateUnitWrapper(localUnit);
					}

					yield return _unitWrappers[localUnit];
				}
			}

			// Return units defined by parent tables, if not overridden locally.
			if (null != Parent)
			{
				foreach (var uom in Parent.FindUnitsByDimension(aDimension))
				{
					if (!found.Contains(uom.Abbreviation))
					{
						found.Add(uom.Abbreviation);
						yield return uom;
					}
				}
			}
		}


		private void CreateUnitWrapper(Unit aNewUnit)
		{
			// Attempt to hook up the derivative for this unit.
			string derivativeName = null;
			UnitOfMeasure derivative = null;
			var abbrev = aNewUnit.Abbreviation;
			if (abbrev.EndsWith("/s"))
			{
				derivativeName = abbrev.Replace("/s", "/s²");
			}
			else if (!abbrev.EndsWith("/s²") && (abbrev.Length > 0))
			{
				derivativeName = abbrev + "/s";
			}

			if (null != derivativeName)
			{
				derivative = FindUnitByAbbreviation(derivativeName);
				if (null != derivative)
				{
					_unitWrappers[derivative.Unit] = derivative;
				}
			}

			// Create the new unit wrapper.
			var newUnit = new UnitOfMeasure(aNewUnit, derivative);
			_unitWrappers[aNewUnit] = newUnit;
		}


		// List of units this table instance "knows" about. This is only for maintaining
		// a backward-compatible behavior for the static hardcoded conversion tables.
		private readonly string[] _unitFilter = new string[0];

		private readonly Dictionary<Unit, UnitOfMeasure> _unitWrappers = new Dictionary<Unit, UnitOfMeasure>();
		private ConversionTable _parent;
	}
}

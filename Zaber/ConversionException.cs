﻿using System;

namespace Zaber
{
	/// <summary>
	///     Exception throw when data conversion fails.
	/// </summary>
	public class ConversionException : Exception
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Creates UnitConversionException.
		/// </summary>
		public ConversionException(string message)
			: base(message)
		{
		}


		/// <summary>
		///     Creates UnitConversionException.
		/// </summary>
		public ConversionException(string message, Exception innerException)
			: base(message, innerException)
		{
		}

		#endregion
	}
}

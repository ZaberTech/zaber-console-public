namespace Zaber
{
	/// <summary>
	///     Descriptive information about a setting that Zaber devices support.
	/// </summary>
	/// <remarks>
	///     This class is specific to commands that read or adjust a
	///     device setting.
	/// </remarks>
	public class SettingInfo : CommandInfo
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Initializes a new instance of SettingInfo to default values.
		/// </summary>
		public SettingInfo()
		{
			ReadAccessLevel = DEFAULT_ACCESS_LEVEL;
		}


		/// <summary>
		///     Create a deep copy of the SettingInfo.
		/// </summary>
		/// <returns>A new instance of SettingInfo (as CommandInfo) with all the same property values as the instance invoked on.</returns>
		public override CommandInfo Clone()
		{
			var si = new SettingInfo();
			CopyProperties(this, si);
			return si;
		}


		/// <summary>
		///     Whether this object is equal to another.
		/// </summary>
		/// <param name="aOther">The object to compare this one to.</param>
		/// <returns>True if the objects are equal, false otherwise.</returns>
		public override bool Equals(object aOther)
		{
			var si = aOther as SettingInfo;
			if (null == si)
			{
				return false;
			}

			return (si.ReadAccessLevel == ReadAccessLevel) && base.Equals(aOther);
		}


		/// <summary>
		///     Returns a calculated hash code for this object.
		/// </summary>
		/// <returns>An integer hash code.</returns>
		public override int GetHashCode()
		{
			var code = base.GetHashCode();
			code = (31 * code) + ReadAccessLevel.GetHashCode();
			return code;
		}


		/// <summary>
		///     ToString override for debugger support.
		/// </summary>
		/// <returns>A textual identifier for the setting, for debugger display purposes.</returns>
		public override string ToString()
		{
			if (null != TextCommand)
			{
				return "ASCII setting: " + TextCommand;
			}

			return ("Binary setting: " + Name) ?? Command.ToString();
		}


		/// <summary>
		///     Is this command actually a setting that can also be retrieved?
		/// </summary>
		/// <remarks>
		///     A setting's value is set by sending the command with the new
		///     value as the data. A setting's value is read by sending the
		///     <see cref="Zaber.Command.ReturnSetting" /> command with the setting's
		///     command number as the data.
		/// </remarks>
		public override bool IsSetting => true;


		/// <summary>
		///     Gets or sets the read access level of this setting. This value,
		///     incombination with the current access level of the device,
		///     determines whether the setting is readable by the user.
		///     Defaults to zero. Normally one for common settings.
		/// </summary>
		/// <remarks>
		///     Typically, settingss with a value greater than one are either
		///     advanced settings or only kept for backward compatibility.
		/// </remarks>
		public int ReadAccessLevel { get; set; }


		/// <summary>
		///		Parameter type information for formatting values in the ASCII
		///		protocol (number of decimal places etc.)
		/// </summary>
		public ParameterType ParamType { get; set; }

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Helper for Clone() that copies properties from one instance to another.
		///     Subclasses should invoke this to handle base class properties.
		/// </summary>
		/// <param name="aFrom">The instance to copy values from.</param>
		/// <param name="aTo">The instance to copy values to.</param>
		protected static void CopyProperties(SettingInfo aFrom, SettingInfo aTo)
		{
			CommandInfo.CopyProperties(aFrom, aTo);
			aTo.ReadAccessLevel = aFrom.ReadAccessLevel;
		}

		#endregion
	}
}

﻿using System;
using System.IO;
using System.Linq;

namespace Zaber
{
	/// <summary>
	///     Helpers for working with streams and binary data. Used by some plugins.
	/// </summary>
	[CLSCompliant(false)]
	public static class StreamExtensions
	{
		#region -- Public data --

		/// <summary>
		///     Flag to repesent file endianness of multi-byte values.
		/// </summary>
		public enum Endianness
		{
			/// <summary>
			///     Value is stored little-endian (LSB first) in a stream.
			/// </summary>
			Little,

			/// <summary>
			///     Value is stored big-endian (MSB first) in a stream.
			/// </summary>
			Big
		}

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Read a 16-bit unsigned integer from a stream, endian-swapping if necessary to host order.
		/// </summary>
		/// <comments>Updates the current position of the stream read pointer by two bytes.</comments>
		/// <param name="aStream">The stream to read from.</param>
		/// <param name="aEndianness">The endianness of the value's representation in the stream.</param>
		/// <returns>The value read from 2 bytes at the current position in the stream.</returns>
		/// <exception cref="EndOfStreamException">The read attempt would pass the end of the input stream.</exception>
		public static ushort ReadUInt16(this Stream aStream, Endianness aEndianness = Endianness.Little)
		{
			var data = new byte[sizeof(ushort)];
			aStream.ReadBlocking(data, 0, sizeof(ushort));

			// Endian-swap if necessary.
			if (BitConverter.IsLittleEndian ^ (Endianness.Little == aEndianness))
			{
				data = data.Reverse().ToArray();
			}

			return BitConverter.ToUInt16(data, 0);
		}


		/// <summary>
		///     Read a 32-bit unsigned integer from a stream, endian-swapping if necessary to host order.
		/// </summary>
		/// <comments>Updates the current position of the stream read pointer by four bytes.</comments>
		/// <param name="aStream">The stream to read from.</param>
		/// <param name="aEndianness">The endianness of the value's representation in the stream.</param>
		/// <returns>The value read from 4 bytes at the current position in the stream.</returns>
		/// <exception cref="EndOfStreamException">The read attempt would pass the end of the input stream.</exception>
		public static uint ReadUInt32(this Stream aStream, Endianness aEndianness = Endianness.Little)
		{
			var data = new byte[sizeof(uint)];
			aStream.ReadBlocking(data, 0, sizeof(uint));

			// Endian-swap if necessary.
			if (BitConverter.IsLittleEndian ^ (Endianness.Little == aEndianness))
			{
				data = data.Reverse().ToArray();
			}

			return BitConverter.ToUInt32(data, 0);
		}


		/// <summary>
		///     Extension to Stream.Read() that blocks until all of the requested data is read from the stream.
		/// </summary>
		/// <param name="aStream">The stream to read from.</param>
		/// <param name="aOutput">Buffer to read to.</param>
		/// <param name="aPosition">Index within the buffer to start writing bytes.</param>
		/// <param name="aLength">Number of bytes to transfer from the stream to the buffer.</param>
		/// <exception cref="EndOfStreamException">The read attempt would pass the end of the input stream.</exception>
		public static void ReadBlocking(this Stream aStream, byte[] aOutput, int aPosition, int aLength)
		{
			if (aStream.Position > (aStream.Length - aLength))
			{
				throw new EndOfStreamException("Attempted to read past end of data.");
			}

			var bytesRead = 0;
			while (bytesRead < aLength)
			{
				bytesRead += aStream.Read(aOutput, aPosition + bytesRead, aLength - bytesRead);
			}
		}

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using log4net;
using Zaber.Units;

namespace Zaber
{
	/// <summary>
	///     Represents a device attached to the computer's port.
	/// </summary>
	/// <remarks>
	///     The ZaberDevice provides an event-driven interface to the device.
	///     Check out the <see cref="Conversation" /> class for request-driven
	///     interface.
	/// </remarks>
	public class ZaberDevice
	{
		#region -- Public data --

		/// <summary>
		///     Map of device flags to short descriptions of the conditions they signal.
		/// </summary>
		[Obsolete("Use " + nameof(WarningFlags) + "." + nameof(WarningFlags.GetWarningDescription) + " instead")]
		public static IDictionary<string, string> WarningFlagDescriptions = new Dictionary<string, string>
		{
			{ FLAGS_EMPTY, "No warnings" },
			{ "FD", "Driver disabled" },
			{ "FQ", "Encoder error" },
			{ "FS", "Stall detected" },
			{ "FT", "Excessive twist" },
			{ "FB", "Stream bounds error" },
			{ "FP", "Interpolated path deviation" },
			{ "FE", "Limit error" },
			{ "WL", "Unexpected limit trigger" },
			{ "WV", "Voltage out of range" },
			{ "WT", "System temperature high" },
			{ "WM", "Displaced when stationary" },
			{ "WR", "No reference position" },
			{ "NC", "Manual control" },
			{ "NI", "Command interrupted" },
			{ "ND", "Stream discontinuity" },
			{ "NU", "Setting update pending" },
			{ "NJ", "Joystick calibrating" },
			{ "WH", "Device not homed" },
			{ "WP", "Invalid calibration type" }
		};

		/// <summary>
		///     The default value for the <see cref="MicrostepResolution" />
		///     property.
		/// </summary>
		public const int DefaultMicrostepResolution = 64;

		/// <summary>
		///     Constant string representing empty device flags.
		/// </summary>
		[Obsolete("Use " + nameof(WarningFlags) + "." + nameof(WarningFlags.None) + " instead.")]
		public const string FLAGS_EMPTY = WarningFlags.None;

		#endregion

		#region -- Events --

		/// <summary>
		///     Raised when the device receives a data packet back from the port.
		/// </summary>
		/// <example>
		///     Register an event handler and print any received data to the
		///     output console.
		///     <code>
		///  private void Register(ZaberDevice device)
		///  {
		///      device.MessageReceived += 
		///          new EventHandler&lt;DeviceMessageEventArgs&gt;(device_MessageReceived);
		///  }
		/// 
		///  void device_MessageReceived(object sender, DeviceMessageEventArgs e)
		///  {
		///      System.Console.Out.WriteLine(
		///          "Device {0}: {1}({2})",
		///          e.DeviceMessage.DeviceNumber,
		///          e.DeviceMessage.Command,
		///          e.DeviceMessage.Data);
		///  }
		///  </code>
		/// </example>
		/// <remarks>
		///     Be careful when handling this event, because it is usually raised
		///     from a background thread. See
		///     <see cref="IZaberPort.DataPacketReceived" /> for details on how to
		///     handle events from background threads and how to just avoid them.
		/// </remarks>
		public event EventHandler<DeviceMessageEventArgs> MessageReceived;


		/// <summary>
		///     Raised when the device sends a packet to the port.
		/// </summary>
		/// <remarks>
		///     Be careful when handling this event, because it is occasionally raised
		///     from a background thread. See
		///     <see cref="IZaberPort.DataPacketReceived" /> for details on how to
		///     handle events from background threads and how to just avoid them.
		/// </remarks>
		public event EventHandler<DeviceMessageEventArgs> MessageSent;

		/// <summary>
		///     Raised when Port of this device changes.
		/// </summary>
		public event EventHandler<EventArgs> PortChanged;

		#endregion

		#region -- Public Methods & Properties --

		#region -- Initialization --

		/// <summary>
		///     Initialize a new instance.
		/// </summary>
		public ZaberDevice()
		{
			MicrostepResolution = DefaultMicrostepResolution;
			AccessLevel = DEFAULT_ACCESS_LEVEL;

			var parser = ComposedUnitParser.NewDefaultUnitParser();
			var converter = new UnitConverter(parser, parser.GetUnitRegistry());
			UnitConverter = new ConversionTable(converter);
		}

		#endregion

		#endregion

		#region -- Public Properties --

		/// <summary>
		///     Get a list of axes for this device.
		/// </summary>
		public ReadOnlyCollection<ZaberDevice> Axes => _axes.AsReadOnly();


		/// <summary>
		///     Gets or sets the axis number for devices that represent an axis on
		///     multi-axis controllers.
		/// </summary>
		/// <remarks>For the controller itself, this will be zero.</remarks>
		public int AxisNumber { get; set; }


		/// <summary>
		///     Enables or disables Binary message ids mode on this device.
		/// </summary>
		/// <remarks>
		///     <para>
		///         Message ids mode uses id numbers to match each response to
		///         the request that triggered it. Message ids are also known as
		///         logical channels.
		///     </para>
		/// </remarks>
		/// <seealso cref="ZaberPortFacade.AreMessageIdsEnabled" />
		/// <seealso cref="DataPacket.MessageId" />
		/// <remarks>
		///     This is an informational property only; changing it does not affect
		///     the hardware device.
		/// </remarks>
		public bool AreMessageIdsEnabled { get; set; }


		/// <summary>
		///     Enables or disables ASCII message IDs on this device.
		/// </summary>
		/// <seealso cref="DataPacket.MessageId" />
		public virtual bool AreAsciiMessageIdsEnabled
		{
			get
			{
				var fwVersion = DeviceType?.FirmwareVersion.ToInt() ?? 0;
				var messageIdsSupported = (fwVersion >= 614) // ASCII messages IDs were added in FW 6.14.
									  && (!IsInBootloaderMode
									  || (fwVersion >= 700)); // FW7 supports IDs during bootload; 6 doesn't.
				return _areAsciiMessageIdsEnabled && messageIdsSupported;
			}
			set => _areAsciiMessageIdsEnabled = value;
		}


		/// <summary>
		///     Set to true during device detection if the device returns the
		///     "NB" flag, which means it is in bootloader mode and has a reduced command set.
		/// </summary>
		public bool IsInBootloaderMode { get; set; }


		/// <summary>
		///     Gets a description of the device in a standard format.
		/// </summary>
		public string Description =>
			$"device {DeviceNumber}{(0 == AxisNumber ? "" : " axis " + AxisNumber)} ({DeviceType})";


		/// <summary>
		///     The device number is used as an address to send requests to.
		/// </summary>
		public byte DeviceNumber { get; set; }


		/// <summary>
		///     The device type determines the list of supported commands.
		/// </summary>
		public DeviceType DeviceType
		{
			get => _deviceType;
			set
			{
				_deviceType = value;
				UpdateResolutionUnits();
			}
		}


		/// <summary>
		///     The serial number of the device, if it has one. Useful for tracking
		///     device identity across sessions.
		/// </summary>
		[CLSCompliant(false)]
		public uint? SerialNumber { get; set; } = null;


		/// <summary>
		///     The current access level setting on the device, if applicable.
		///     Determines which commands and settings are visible to the user.
		///     If not applicable, defaults to int.MaxValue which makes all visible.
		/// </summary>
		public int AccessLevel { get; set; }


		/// <summary>
		///     The firmware version on the device.
		/// </summary>
		/// <remarks>
		///     This property is a convenient shortcut for
		///     DeviceType.FirmwareVersion. If you would like to set this property,
		///     set the FirmwareVersion property of the DeviceType.
		/// </remarks>
		public FirmwareVersion FirmwareVersion => DeviceType.FirmwareVersion;


		/// <summary>
		///     Gets a flag showing whether this device has any objects subscribed
		///     to its events. (Used for testing.)
		/// </summary>
		public bool HasSubscribers => (null != MessageReceived) || (null != MessageSent);


		/// <summary>
		///     True if this is a single device, and not a collection of devices.
		/// </summary>
		public virtual bool IsSingleDevice => true;


		/// <summary>
		///     True if this is an axis - that is, a peripheral or an integrated device.
		///     False means it's a standalone controller or device that is not motion capable.
		/// </summary>
		public bool IsAxis
			=> IsSingleDevice && ((MotionType.None != DeviceType.MotionType) || (1 == Axes.Count));


		/// <summary>
		///     True if this device is a controller that can host peripherals.
		///     False for integrated devices.
		/// </summary>
		/// <remarks>In ASCII, this and <see cref="IsPeripheral"/> should never
		/// be true at the same time. In Binary, they will be for non-integrated devices.</remarks>
		public bool IsController { get; set; }


		/// <summary>
		///     True if this device is a peripheral that attaches to a controller.
		///     False for integrated devices.
		/// </summary>
		/// <remarks>In ASCII, this and <see cref="IsController"/> should never
		/// be true at the same time. In Binary, they will be for non-integrated devices.</remarks>
		public bool IsPeripheral { get; set; }


		/// <summary>
		///     True if this is an integrated device - that is, having both controller
		///     and peripheral functionality in one. May be true for some devices that
		///     are not capable of motion - see <see cref="IsAxis"/>.
		/// </summary>
		public bool IsIntegrated { get; set; }


		/// <summary>
		///     Gets a flag indicating that this device is tracking the request
		///     that generated each response.
		/// </summary>
		/// <remarks>
		///     This is only true when the port is in ASCII mode.
		/// </remarks>
		public bool IsTrackingRequests => !(this is DeviceCollection) && (null != _port) && _port.IsAsciiMode;


		/// <summary>
		///     Gets or sets the current microstep resolution of this device.
		/// </summary>
		/// <remarks>
		///     This is updated whenever a
		///     <see cref="Command.SetMicrostepResolution" /> response is
		///     received.
		/// </remarks>
		/// <value>
		///     The number of microsteps in a full step. Default value is
		///     64.
		/// </value>
		public int MicrostepResolution
		{
			get => _microstepResolution;
			set
			{
				_microstepResolution = value;
				UpdateResolutionUnits();
			}
		}


		/// <summary>
		///     Last message flags received from the device. This is for display
		///     purposes only and should not be relied upon for detecting device state.
		/// </summary>
		public string LastFlagsReceived { get; set; }


		/// <summary>
		///     Indicates that the device's firmware can be updated at present.
		/// </summary>
		public bool CanUpdateFirmware
			=> (Port?.IsAsciiMode ?? false)
			   && Port.IsOpen
			   && (AxisNumber == 0)
			   && IsSingleDevice
			   && ((FirmwareVersion >= 618) || IsInBootloaderMode);


		/// <summary>
		///     Indicates that the device can store calibration tables. This is
		///     not the same as capable of being calibrated; for example this will be true
		///     for FW6 controllers, where the controller stores the data but
		///     it is the axes that are calibrated. Note this property also factors in the
		///     current port state, so also indicates if it is currently possible to
		///     alter the calibration tables.
		/// </summary>
		public bool CanStoreCalibration
		{
			get
			{
				if ((Port?.IsAsciiMode ?? false)
				 && Port.IsOpen
				 && IsSingleDevice)
				{
					// For FW6, controllers and integrated devices with encoders and
					// FW >= 6.22 are expected to support calibration.
					if ((FirmwareVersion >= 622) && (FirmwareVersion < 700))
					{
						return IsController || (IsIntegrated && ShouldBeCalibrated);
					}

					// For FW7, axes that have the EncoderDirect capability but are not
					// cyclic are expected to support calibration.
					if ((FirmwareVersion >= 700)
					 && DeviceType.Capabilities.Contains(Capability.EncoderDirect)
					 && !DeviceType.Capabilities.Contains(Capability.Cyclic))
					{
						return IsAxis;
					}
				}

				return false;
			}
		}


		/// <summary>
		///     Indicated whether this device is a type that should have a calibration
		///     table stored during normal operation.
		/// </summary>
		public bool ShouldBeCalibrated
		{
			get
			{
				if (FirmwareVersion < 700)
				{
					// TODO (Soleil 2020-05): Replace this when capabilities are available for FW6.
					return (DeviceType.Name ?? string.Empty).Contains("DE") && !DeviceType.IsRotaryDevice;
				}
				else
				{
					return DeviceType.Capabilities.Contains(Capability.EncoderDirect)
					       && !DeviceType.Capabilities.Contains(Capability.Cyclic);
				}
			}
		}

		/// <summary>
		///     The port to send requests to and receive responses from.
		/// </summary>
		public IZaberPort Port
		{
			get => _port;
			set
			{
				if (null != _port)
				{
					_port.DataPacketReceived -= OnDataPacketReceived;
					_port.DataPacketSent -= OnDataPacketSent;
				}

				_port = value;

				if (null != _port)
				{
					_port.DataPacketReceived += OnDataPacketReceived;
					_port.DataPacketSent += OnDataPacketSent;
				}

				PortChanged?.Invoke(this, EventArgs.Empty);
			}
		}


		/// <summary>
		///     Gets the most recently received position update for this device.
		/// </summary>
		public decimal? RecentPosition { get; private set; }


		/// <summary>
		///     Conversion table to convert between standard units and
		///     device-specific units. Takes current resolution into account
		///     where appropriate.
		/// </summary>
		/// <remarks>
		///     Do not cache the value of this property! It can be replaced if
		///     the device type, resolution or other settings change.
		/// </remarks>
		public ConversionTable UnitConverter { get; set; }


		/// <summary>
		///     Application-supplied help data for this device. This library does not reference
		///     this data; it is a place for applications to attach their own data for user help.
		/// </summary>
		public IDeviceHelp HelpSource { get; set; } = null;

		#endregion

		#region -- Event Handlers --

		/// <summary>
		///     Handle a data packet sent to the port and raise a corresponding event.
		/// </summary>
		/// <param name="aSender"></param>
		/// <param name="aArgs"></param>
		private void OnDataPacketSent(object aSender, DataPacketEventArgs aArgs)
		{
			if (aArgs.DataPacket.DeviceNumber != DeviceNumber)
			{
				return;
			}

			if ((0 != AxisNumber) && (aArgs.DataPacket.AxisNumber != AxisNumber))
			{
				return;
			}

			var message = CreateDeviceMessage(aArgs.DataPacket);
			if (IsTrackingRequests)
			{
				if (null != _currentRequest)
				{
					_log.InfoFormat("Request {0} pre-empted.", _currentRequest.FormatRequest());
				}

				_currentRequest = message;
			}

			if ((Command.Renumber == message.Command)
			|| ((null != message.Body) && Regex.IsMatch(message.Body, @"^(?!.*key\s).*(renumber|set\s+comm.address)")))
			{
				// The device will probably respond with a new device number.
				// Expect that response.
				_expectedNewNumber = (byte) message.NumericData;
			}


			if ((null != message.Body) && Regex.IsMatch(message.Body, @"^(?!.*key\s).*set\s+resolution"))
			{
				// Can't change resolution setting for multiple axes at once by sending multiple values; 
				// single value only. This filters out the case where the user types in two values.
				if ((1 == message.NumericValues.Count)

					// Only update resolution for device named in message, if the message is targeted at 
					// a single device. If it was sent to all devices, assume all updated if they accept the command.
				&& (message.DeviceNumber == DeviceNumber))
				{
					// The setup for expected resolution changes is different for single devices and
					// for the All Devices collection, hence the virtual method.
					ExpectNewResolution(message);
				}
			}

			if (_log.IsDebugEnabled)
			{
				_log.Debug(message.FormatRequest());
			}

			MessageSent?.Invoke(this, new DeviceMessageEventArgs(message));
		}


		/// <summary>
		///     Handle a data packet received from the port and raise a corresponding event.
		/// </summary>
		/// <param name="aSender"></param>
		/// <param name="aArgs"></param>
		private void OnDataPacketReceived(object aSender, DataPacketEventArgs aArgs)
		{
			// If a device's number is expected to change, expect that number
			// instead so that the reply is associated with the request, and
			// all of our threads actually complete.
			var expectedDeviceNumber = DeviceNumber;

			// A rejection means a renumber command can't have taken effect, so
			// if the response is a rejection then the expected device number is
			// the original, not the one sent with the renumber command.
			if (_expectedNewNumber.HasValue && !aArgs.DataPacket.IsError)
			{
				expectedDeviceNumber = _expectedNewNumber.Value;
			}

			if (aArgs.DataPacket.DeviceNumber != expectedDeviceNumber)
			{
				return;
			}

			// If we expected a new number and saw it, don't expect a new
			// number any more.
			_expectedNewNumber = null;

			// If this device was sent a resolution change request and it was accepted, we assume
			// the appropriate axes how have the new resolution value.
			if (null != _newMicrostepResolution)
			{
				if (!aArgs.DataPacket.IsError)
				{
					var axis = _newMicrostepResolution.Item1;
					var resolution = _newMicrostepResolution.Item2;
					if (axis == AxisNumber)
					{
						if (0 == axis)
						{
							// Resolution changed for controller and all axes.
							MicrostepResolution = resolution;
							foreach (var axisDevice in Axes)
							{
								axisDevice.MicrostepResolution = resolution;
							}
						}
						else
						{
							// Resolution changed on a single axis. Message axis numbers start from 1.
							MicrostepResolution = resolution;
						}
					}
				}

				_newMicrostepResolution = null;
			}

			if ((0 != AxisNumber) && (aArgs.DataPacket.AxisNumber != AxisNumber))
			{
				return;
			}

			var message = CreateDeviceMessage(aArgs.DataPacket);
			message.Request = _currentRequest;
			if (null != message.Request)
			{
				_currentRequest = null;
				message.CommandInfo = message.Request.CommandInfo;
			}

			var commandInfo = message.CommandInfo;
			if ((null != commandInfo) && commandInfo.IsCurrentPositionReturned)
			{
				RecentPosition = message.NumericData;
			}

			if (Command.SetMicrostepResolution == aArgs.DataPacket.Command)
			{
				MicrostepResolution = (int) aArgs.DataPacket.NumericData;
			}

			message.Measurement = CalculateMeasurement(commandInfo?.GetResponseConversion(), message.NumericData);

			if (_log.IsDebugEnabled)
			{
				_log.Debug(message.FormatResponse());
			}

			if (aArgs.DataPacket.AxisNumber == AxisNumber)
			{
				LastFlagsReceived = message.FlagText;
			}
			MessageReceived?.Invoke(this, new DeviceMessageEventArgs(message));
		}


		/// <summary>
		///     Protected method to allow derived classes to raise the
		///     MessageReceived event.
		/// </summary>
		/// <param name="aArgs">The details of the message.</param>
		protected void OnMessageReceived(DeviceMessageEventArgs aArgs) => MessageReceived?.Invoke(this, aArgs);


		/// <summary>
		///     Protected method to allow derived classes to raise the
		///     MessageSent event.
		/// </summary>
		/// <param name="aArgs">The details of the message.</param>
		protected void OnMessageSent(DeviceMessageEventArgs aArgs) => MessageSent?.Invoke(this, aArgs);

		#endregion

		#region -- Public Methods --

		/// <summary>
		///     Add a device that represents an axis for this controller.
		/// </summary>
		/// <param name="aAxis">The axis to add.</param>
		/// <remarks>
		///     All controllers have a collection of devices - one
		///     for each axis.
		/// </remarks>
		public void AddAxis(ZaberDevice aAxis) => _axes.Add(aAxis);


		/// <summary>
		///     Calculate what the data value should be to represent the requested
		///     measurement on the requested command.
		/// </summary>
		/// <param name="aCommand">The command that will use the data.</param>
		/// <param name="aMeasurement">
		///     The measurement to send to this device.
		/// </param>
		/// <returns>A data value to send as part of a command.</returns>
		public int CalculateData(Command aCommand, Measurement aMeasurement)
		{
			try
			{
				if (UnitOfMeasure.Data == aMeasurement.Unit)
				{
					return (int) Math.Round(aMeasurement.Value);
				}

				var commandInfo = DeviceType.GetCommandByNumber(aCommand);

				// Don't do unit conversions if this command has no parameters.
				if ((null != commandInfo) && !commandInfo.HasParameters)
				{
					return (int) Math.Round(aMeasurement.Value);
				}

				return (int) Math.Round(CalculateData(commandInfo?.GetRequestConversion(), aMeasurement));
			}
			catch (OverflowException e)
			{
				throw new ConversionException("Number is too large for the Binary Protocol", e);
			}
		}


		/// <summary>
		///     Calculate what the data value should be to represent the requested
		///     measurement on the requested command.
		/// </summary>
		/// <param name="aCommand">The command that will use the data.</param>
		/// <param name="aMeasurement">
		///     The measurement to send to this device.
		/// </param>
		/// <returns>A data value to send as part of a command.</returns>
		/// <remarks>
		///     This method is deprecated; use <see cref="CalculateExactData(string, Measurement, int)" /> instead.
		///     This method assumes that the requested command is structured as
		///     {COMMAND STRING} {PARAMETER} and that the parameter can only be
		///     an integer. For more complex command structures
		///     use <see cref="CalculateExactData(string, Measurement, int)" />.
		/// </remarks>
		[Obsolete("Use CalculateExactData() instead.")]
		public int CalculateData(string aCommand, Measurement aMeasurement)
			=> (int) Math.Round(CalculateExactData(aCommand, aMeasurement));


		/// <summary>
		///     Calculate what the data value should be to represent the requested
		///     measurement on the requested command.
		/// </summary>
		/// <param name="aCommand">The command that will use the data.</param>
		/// <param name="aMeasurement">
		///     The measurement to send to this device.
		/// </param>
		/// <returns>A data value to send as part of a command.</returns>
		/// <remarks>
		///     This method assumes that the requested command is structured as
		///     {COMMAND STRING} {PARAMETER} and that the parameter can only be
		///     an integer. For more complex command structures
		///     use <see cref="CalculateExactData(string, Measurement, int)" />.
		///     Also note that this function does not format the data as expected by
		///     the command; the caller is responsible for any necessary rounding.
		/// </remarks>
		public decimal CalculateExactData(string aCommand, Measurement aMeasurement)
		{
			if (UnitOfMeasure.Data == aMeasurement.Unit)
			{
				return aMeasurement.Value;
			}

			// commands for a device are indexed by their format.  Add space for
			// a parameter node at the end.
			var format = aCommand + " {0}";
			if (aCommand.StartsWith("get"))
			{
				format = aCommand;
			}

			try
			{
				return CalculateExactData(format, aMeasurement, 0);
			}
			catch (NullReferenceException)
			{
				//if a command can't be found, might be calculating a setting
				var cmdInfo = DeviceType.GetCommandByText(aCommand);
				return CalculateData(cmdInfo?.GetRequestConversion(), aMeasurement);
			}
		}


		/// <summary>
		///     Calculate what the data value should be to represent the nth
		///     parameter in the format of the requested ASCII command.
		///     This method rounds to the nearest integer. For commands that accept
		///     higher precision parameters, use <see cref="CalculateExactData(string, Measurement, int)" />.
		/// </summary>
		/// <param name="aFormat">
		///     The format of the command, in the same format
		///     provided by <see cref="ASCIICommandInfo.Name" />.
		/// </param>
		/// <param name="aMeasurement">The measurement to send to this device.</param>
		/// <param name="aNodeIndex">
		///     The index of the parameter node being calculated
		///     in the command's format.
		/// </param>
		/// <returns>A data value to send as part of a command.</returns>
		/// <remarks>This method is deprecated; use <see cref="CalculateExactData(string, Measurement, int)" /> instead.</remarks>
		[Obsolete("Use CalculateExactData() instead.")]
		public int CalculateData(string aFormat, Measurement aMeasurement, int aNodeIndex)
			=> (int) Math.Round(CalculateExactData(aFormat, aMeasurement, aNodeIndex));


		/// <summary>
		///     Calculate what the data value should be to represent the nth
		///     parameter in the format of the requested ASCII command.
		/// </summary>
		/// <param name="aFormat">
		///     The format of the command, in the same format
		///     provided by <see cref="ASCIICommandInfo.Name" />.
		/// </param>
		/// <param name="aMeasurement">The measurement to send to this device.</param>
		/// <param name="aNodeIndex">
		///     The index of the parameter node being calculated
		///     in the command's format.
		/// </param>
		/// <returns>
		///     A data value to send as part of a command. May require rounding and special
		///     formatting for some command parameters.
		/// </returns>
		public decimal CalculateExactData(string aFormat, Measurement aMeasurement, int aNodeIndex)
		{
			if (UnitOfMeasure.Data == aMeasurement.Unit)
			{
				return aMeasurement.Value;
			}

			// search for a valid command by its format, then get the
			// parameter node at nodeIndex
			var commandInfo = DeviceType.GetCommandByText(aFormat);
			if (null == commandInfo)
			{
				throw new NullReferenceException();
			}

			var nodeInfo = commandInfo?.GetRequestConversion();

			if (commandInfo is ASCIICommandInfo asciiInfo)
			{
				nodeInfo = asciiInfo.Nodes.Where(node => node.ParamType != null)
								 .ElementAt(aNodeIndex)
								 ?.GetRequestConversion();
			}

			return CalculateData(nodeInfo, aMeasurement);
		}


		/// <summary>
		///     Get a conversion between a given unit of measure and the device's native
		///     units for that quantity.
		/// </summary>
		/// <param name="aUnit">The unit to convert to device units.</param>
		/// <returns>
		///     A class that can transform quantities from the specified unit to
		///     the corresponding device-specific unit. Return will be null if
		///     there is no known conversion for that quantity type - for example
		///     asking for angle conversions on a linear device is not valid.
		/// </returns>
		/// <remarks>
		///     Be cautious about caching the return value, as it may
		///     depend on the current values of device settings such as resolution.
		///     Also, the result will always be a general-purpose conversion for
		///     the device; the potential exists for specific commands and settings
		///     to have different conversion factors. See
		///     <see cref="GetConversionToParameterUnits(Unit, IParameterConversionInfo)" />
		///     for more specific conversions.
		/// </remarks>
		public QuantityTransformation GetConversionToDeviceUnits(Unit aUnit)
			=> GetConversionToParameterUnits(aUnit, GetDeviceUnitConversionInfo(aUnit.Dimension));


		/// <summary>
		///     Get a conversion between a given unit of measure and device's native
		///     units for a particular command parameter, command result or setting.
		/// </summary>
		/// <param name="aUnit">The unit to convert to device units.</param>
		/// <param name="aParameter">
		///     Parameter unit conversion information.
		///     You can obtain this from <see cref="CommandInfo.GetRequestConversion()" />,
		///     <see cref="CommandInfo.GetResponseConversion()" /> or
		///     <see cref="ASCIICommandNode.GetRequestConversion()" />.
		/// </param>
		/// <returns>
		///     A class that can transform quantities from the specified unit to
		///     the corresponding parameter-specific unit. Return will be null if
		///     there is no known conversion for that quantity type - for example
		///     asking for angle conversions for a move command on a linear device
		///     is not valid.
		/// </returns>
		/// <remarks>
		///     Be cautious about caching the return value, as it may
		///     depend on the current values of device settings such as resolution.
		///     Also, the result may be specific to the parameter you specify and
		///     may not be generally applicable to other similar quantities on
		///     the same device. See <see cref="GetConversionToDeviceUnits(Unit)" />
		///     for general conversions. It is recommended to avoid using
		///     parameter-specific units in general-purpose code because different
		///     device types may not have the same commands and settings.
		/// </remarks>
		public QuantityTransformation GetConversionToParameterUnits(Unit aUnit, IParameterConversionInfo aParameter)
		{
			var transform = GetScalingFunctionTransform(aParameter, MicrostepResolution);
			if ((null != transform) && (transform.SourceUnit != aUnit))
			{
				transform = aUnit.GetTransformationTo(transform.SourceUnit).Chain(transform);
			}

			return transform;
		}


		/// <summary>
		///     Converts a data packet into a device message by populating the
		///     message id and command info fields.
		/// </summary>
		/// <param name="aDataPacket">The data packet to copy</param>
		/// <returns>A device message with the new fields populated</returns>
		public DeviceMessage CreateDeviceMessage(DataPacket aDataPacket)
		{
			if (null == aDataPacket)
			{
				throw new ArgumentNullException(nameof(aDataPacket));
			}

			var message = new DeviceMessage(aDataPacket);
			if ((MessageType.Binary == message.MessageType) && AreMessageIdsEnabled)
			{
				// Take most significant byte as Id.
				var numericData = (int) message.NumericData;
				message.MessageId = (byte) (numericData >> 24);

				// 255 or 0 means no Id.
				if (message.MessageId == 255)
				{
					message.MessageId = 0;
				}

				numericData = numericData & DATA_WITH_ID_MASK;

				// negative data has to be extended back into the most 
				// significant bit.
				if ((numericData & 0x800000) != 0)
				{
					numericData = numericData | -0x1000000;
				}

				message.NumericData = numericData;
			}

			if (null != DeviceType)
			{
				message.CommandInfo = null == aDataPacket.Body
					? DeviceType.GetCommandByNumber(aDataPacket.Command)
					: DeviceType.GetCommandByText(aDataPacket.Body);
			}

			return message;
		}


		/// <summary>
		///     Whether this object is equal to another.
		/// </summary>
		/// <param name="aOther">The object to compare this one to.</param>
		/// <returns>True if the objects are equal, false otherwise.</returns>
		public override bool Equals(object aOther)
		{
			if (ReferenceEquals(this, aOther))
			{
				return true;
			}

			var other = aOther as ZaberDevice;

			if (null == other)
			{
				return false;
			}

			// We purposefully skip a few properties in Equals and HashCode:
			// Description, HasSubscribers, IsTrackingRequests, Port, and
			// RecentPosition. All of these properties are either calculated
			// based off others (Description), or are set/re-set by the rest
			// of the library. We are only concerned with the properties we
			// set ourselves, which do not change regularly. This way, objects
			// that are considered to be equal will continue to be considered
			// equal until the user modifies them explicitly.
			if (!Axes.SequenceEqual(other.Axes))
			{
				return false;
			}

			if (AxisNumber != other.AxisNumber)
			{
				return false;
			}

			if (AreMessageIdsEnabled != other.AreMessageIdsEnabled)
			{
				return false;
			}

			if (AreAsciiMessageIdsEnabled != other.AreAsciiMessageIdsEnabled)
			{
				return false;
			}

			if (IsInBootloaderMode != other.IsInBootloaderMode)
			{
				return false;
			}

			if (DeviceNumber != other.DeviceNumber)
			{
				return false;
			}

			if (!Equals(DeviceType, other.DeviceType))
			{
				return false;
			}

			if (IsSingleDevice != other.IsSingleDevice)
			{
				return false;
			}

			if (MicrostepResolution != other.MicrostepResolution)
			{
				return false;
			}

			return true;
		}


		/// <summary>
		///     Display a response data value in another unit of measure. Requires certain
		///     commands to be present in the <see cref="DeviceType" />'s list of
		///     commands. See remarks for details.
		/// </summary>
		/// <param name="aRawData">The device data value to display.</param>
		/// <param name="aToUnit">The unit of measure to convert to.</param>
		/// <param name="aCommand">
		///     (optional) The command or setting to convert units for.
		///     Used as the source of the conversion function for "other" measurement types.
		/// </param>
		/// <returns>A string representation of the data value, rounded to at most 12 decimal places.</returns>
		/// <remarks>
		///     If a command info is provided, its response unit conversion info will be used to
		///     do the unit conversion. Otherwise a generic unit conversion using device properties
		///     rather than command properties will be attempte4d.
		/// </remarks>
		/// <exception cref="InvalidOperationException">
		///     The
		///     <see cref="DeviceType" />'s "Commands" list did not contain the
		///     required command to perform the conversion to the unit of measure
		///     specified.
		/// </exception>
		public string FormatData(decimal aRawData, UnitOfMeasure aToUnit, CommandInfo aCommand = null)
		{
			var measurementInDesiredUnit = ConvertData(aRawData, aToUnit, aCommand);
			return string.Format(CultureInfo.CurrentUICulture, "{0:G12}", measurementInDesiredUnit.Value);
		}


		/// <summary>
		///     Generate a string representation of an ASCII command with properly formatted
		///     data values substituted in for the parameters, given a format string.
		///     Result is suitable to send to the device.
		/// </summary>
		/// <param name="aFormat">
		///     The command to format. Must be an ASCII command
		///     with parameters represented by C# format string like placeholders, such
		///     as "{0}". The parameter placeholders must be numbered in ascending order starting
		///     with zero, and no formatting information is allowed.
		/// </param>
		/// <param name="aValues">
		///     Values for the command parameters, in the order
		///     they appear in the command. Values can be <see cref="ParameterData" />,
		///     <see cref="Measurement" /> or <see cref="Zaber.Units.Measurement" /> instances
		///     to use unit conversion, or any numeric type; in the latter case
		///     the units are assumed to be native device data units. For enumeration parameters,
		///     the value may either be an enumeration value with the right name, or a string,
		///     and will be checked against legal values. For token parameters, ToString() will
		///     be called on the object and the result is not checked for validity.
		/// </param>
		/// <returns>A formatted ASCII command suitable to send to a device</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">
		///     There are too few or
		///     too many values for the command.
		/// </exception>
		/// <exception cref="System.ArgumentException">
		///     It was not possible to
		///     do unit conversion for one of the values, or an enumeration value was illegal.
		/// </exception>
		/// <exception cref="InvalidDataException">
		///     Formatting information for the
		///     command could not be found. This likely means the format string does not
		///     match any commands the device actually has.
		/// </exception>
		public string FormatAsciiCommand(string aFormat, params object[] aValues)
		{
			var cmd = DeviceType.GetCommandByText(aFormat);
			if (null == cmd)
			{
				throw new InvalidDataException($"Could not find formatting information for command '{aFormat}'");
			}

			return FormatAsciiCommand(cmd, aValues);
		}


		/// <summary>
		///     Generate a string representation of an ASCII command with properly formatted
		///     data values substituted in for the parameters. Result is suitable to
		///     send to the device.
		/// </summary>
		/// <param name="aCommand">
		///     The command to format. Must be an ASCII command or
		///     setting.
		/// </param>
		/// <param name="aValues">
		///     Values for the command parameters, in the order
		///     they appear in the command. Values can be <see cref="ParameterData" />,
		///     <see cref="Measurement" /> or <see cref="Zaber.Units.Measurement" /> instances
		///     to use unit conversion, or any numeric type; in the latter case
		///     the units are assumed to be native device data units. For enumeration parameters,
		///     the value may either be an enumeration value with the right name, or a string,
		///     and will be checked against legal values. For token parameters, ToString() will
		///     be called on the object and the result is not checked for validity.
		///     Some commands have a single parameter placeholder that accepts a variable number
		///     of parameters; in these cases you can provide an array for that parameter, or
		///     else all remaining input values will be consumed.
		/// </param>
		/// <returns>A formatted ASCII command suitable to send to a device</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">
		///     There are too few or
		///     too many values for the command.
		/// </exception>
		/// <exception cref="System.ArgumentException">
		///     It was not possible to
		///     do unit conversion for one of the values, or an enumeration value was illegal.
		/// </exception>
		public string FormatAsciiCommand(CommandInfo aCommand, params object[] aValues)
		{
			var sb = new StringBuilder();

			if (aCommand is ASCIICommandInfo asciiCmd)
			{
				var paramIndex = 0;
				foreach (var node in asciiCmd.Nodes)
				{
					if (node.ParamType is null) // Keyword ; just add it verbatim.
					{
						sb.Append(node.NodeText);
						sb.Append(" ");
					}
					else // Parameter of some kind.
					{
						var nextParamValue = aValues[paramIndex];
						var objectsToFormat = new[] { nextParamValue } as Array;

						if (1 != node.ParamType.Arity) // Multiple values allowed.
						{
							if (nextParamValue.GetType().IsArray) // Multiple values provided as an array.
							{
								objectsToFormat = nextParamValue as Array ?? new object[0];
								if (node.ParamType.Arity.HasValue && (objectsToFormat.Length != node.ParamType.Arity))
								{
									// Param has specific arity and given data doesn't match.
									throw new ArgumentException(
										$"Array value does not match required length of {node.ParamType.Arity} for parameter {paramIndex - 1} of command '{aCommand.TextCommand}'");
								}

								paramIndex++;
							}
							else // Consume remaining inputs to fill multi-value parameter.
							{
								objectsToFormat = aValues.Skip(paramIndex).ToArray();
								paramIndex = aValues.Length;
							}
						}
						else // Single-value parameter.
						{
							paramIndex++;
						}

						// Format the parameter values(s) according to the parameter type.
						foreach (var val in objectsToFormat)
						{
							if (node.ParamType.IsNumeric || node.ParamType.IsBoolean
								) // Numeric parameter; convert and format number.
							{
								try
								{
									var textVal =
										FormatAsciiParameter(val, node.ParamType, node.GetRequestConversion());
									sb.Append(textVal);
								}
								catch (ArgumentException aArgException)
								{
									throw new ArgumentException(
										string.Format("Error formatting command '{0}'", aCommand.TextCommand),
										aArgException);
								}
							}
							else if (node.ParamType.IsEnumeration) // Enum parameter; check for legal value.
							{
								var enumStr = val as string ?? val.ToString();
								if (!node.ParamType.EnumValues.Contains(enumStr))
								{
									// Try to accept an alternate capitalization.
									var found = false;
									foreach (var valStr in node.ParamType.EnumValues)
									{
										if (string.Equals(enumStr, valStr, StringComparison.InvariantCultureIgnoreCase))
										{
											enumStr = valStr;
											found = true;
											break;
										}
									}

									if (!found)
									{
										throw new ArgumentException(
											string.Format("Value '{0}' is not legal for parameter {1} of command '{2}'",
														  enumStr,
														  paramIndex - 1,
														  aCommand.TextCommand));
									}
								}

								sb.Append(enumStr);
							}
							else if (node.ParamType.IsToken) // Token parameter; append as-is.
							{
								sb.Append(val);
							}
							else
							{
								throw new InvalidDataException(
									string.Format("Unrecognized parameter type for command '{0}'",
												  aCommand.TextCommand));
							}

							sb.Append(" ");
						}
					}
				}

				if (paramIndex != aValues.Length)
				{
					throw new ArgumentOutOfRangeException(
						"There were more parameter values than the command expected.");
				}
			}

			return sb.ToString().Trim();
		}


		/// <summary>
		///     Convert a response data value to another unit of measure. Requires certain
		///     commands to be present in the <see cref="DeviceType" />'s list of
		///     commands. See remarks for details.
		/// </summary>
		/// <param name="aRawData">The device data value to display.</param>
		/// <param name="aToUnit">The unit of measure to convert to.</param>
		/// <param name="aCommand">
		///     (optional) The command or setting to convert units for.
		///     Used as the source of the conversion function for "other" measurement types.
		/// </param>
		/// <returns>A string representation of the data value, rounded to at most 12 decimal places.</returns>
		/// <remarks>
		///     If a command info is provided, its response unit conversion info will be used to
		///     do the unit conversion. Otherwise a generic unit conversion using device properties
		///     rather than command properties will be attempte4d.
		/// </remarks>
		/// <exception cref="InvalidOperationException">
		///     The
		///     <see cref="DeviceType" />'s "Commands" list did not contain the
		///     required command to perform the conversion to the unit of measure
		///     specified.
		/// </exception>
		public Measurement ConvertData(decimal aRawData, UnitOfMeasure aToUnit, CommandInfo aCommand = null)
		{
			if (UnitOfMeasure.Data == aToUnit)
			{
				return new Measurement(aRawData, aToUnit);
			}

			// If this unit has a device-specific scale factor, always look it up
			// in the device's local table to ensure we're not using one from another 
			// device or a generic definition.
			if (aToUnit.IsDeviceDependent)
			{
				aToUnit = UnitConverter.FindUnitByAbbreviation(aToUnit.Abbreviation);
			}

			if (UnitOfMeasure.MicrostepsPerSecond == aToUnit)
			{
				var deviceVelocityUnits = new Measurement(aRawData,
														  UnitConverter.FindUnitByAbbreviation(
															  UnitOfMeasure.DeviceMicrostepVelocityUnitSymbol));
				var uStepVel = UnitConverter.Convert(deviceVelocityUnits,
													 UnitConverter.FindUnitByAbbreviation(
														 UnitOfMeasure.MicrostepsPerSecondSymbol));

				return new Measurement(uStepVel.Value, uStepVel.Unit);
			}

			if (UnitOfMeasure.MicrostepsPerSecondSquared == aToUnit)
			{
				var deviceAccelUnits = new Measurement(aRawData,
													   UnitConverter.FindUnitByAbbreviation(
														   UnitOfMeasure.DeviceMicrostepAccelerationUnitSymbol));
				var uStepAccel = UnitConverter.Convert(deviceAccelUnits,
													   UnitConverter.FindUnitByAbbreviation(
														   UnitOfMeasure.MicrostepsPerSecondSquaredSymbol));

				return new Measurement(uStepAccel.Value, uStepAccel.Unit);
			}

			QuantityTransformation conversion = null;
			var conversionInfo =
				aCommand?.GetRequestConversion() ?? GetDeviceUnitConversionInfo(aToUnit.Unit.Dimension);
			conversion = GetDeviceSpecificUnitConversion(conversionInfo);

			if (null != conversion)
			{
				if (UnitOfMeasure.Data.Unit.Abbreviation == conversion.TargetUnit.Abbreviation)
				{
					return new Measurement(aRawData, UnitOfMeasure.Data);
				}

				conversion = conversion.Chain(conversion.TargetUnit.GetTransformationTo(aToUnit.Unit));
			}

			if (null == conversion)
			{
				throw new InvalidOperationException("Data can only be "
												+ "formatted for devices with known position scaling "
												+ "factors. Please populate the DeviceType's Commands "
												+ "list before trying to use units of measure.");
			}

			var quantity = conversion.Transform(aRawData);
			return new Measurement(quantity, aToUnit);
		}


		/// <summary>
		///     Returns a calculated hash code for this object.
		/// </summary>
		/// <returns>An integer hash code.</returns>
		public override int GetHashCode()
		{
			var code = 17;

			if (null != Axes)
			{
				foreach (var axis in Axes)
				{
					code = (31 * code) + axis.GetHashCode();
				}
			}

			code = (31 * code) + AxisNumber;
			code = (31 * code) + (AreMessageIdsEnabled ? 1 : 0);
			code = (31 * code) + (AreAsciiMessageIdsEnabled ? 1 : 0);
			code = (31 * code) + (IsInBootloaderMode ? 1 : 0);
			if (null != DeviceType)
			{
				code = (31 * code) + DeviceType.GetHashCode();
			}

			code = (31 * code) + DeviceNumber;
			code = (31 * code) + (IsSingleDevice ? 1 : 0);
			code = (31 * code) + MicrostepResolution;

			return code;
		}


		/// <summary>
		///     Generates identifying information for this device, to help track it
		///     accross sessions.
		/// </summary>
		/// <returns>
		///     A new DeviceIdentity instance containing what identifying information
		///     is known about this device.
		/// </returns>
		[CLSCompliant(false)]
		public virtual DeviceIdentity GetIdentity()
		{
			var result = new DeviceIdentity
			{
				DeviceSerialNumber = SerialNumber,
				DeviceId = (uint) DeviceType.DeviceId,
				DeviceNumber = DeviceNumber,
				AxisNumber = AxisNumber
			};

			if (0 != DeviceType.PeripheralId)
			{
				result.PeripheralId = (uint) DeviceType.PeripheralId;
			}

			return result;
		}


		/// <summary>
		///     Get a list of the units of measure this device supports.
		/// </summary>
		/// <param name="aMeasurementType">
		///     The type of measurement: position,
		///     velocity, etc.
		/// </param>
		/// <param name="aContext">Optional list of commands or settings to harvest units from; defaults to all.</param>
		/// <returns>
		///     A list of units of measure, sorted with raw data as the
		///     first entry.
		/// </returns>
		public ICollection<UnitOfMeasure> GetUnitsOfMeasure(MeasurementType aMeasurementType, IEnumerable<CommandInfo> aContext = null)
		{
			var units = new List<UnitOfMeasure>();

			if ((aContext ?? DeviceType.Commands).Any(cmd =>
				(cmd.RequestUnit?.MeasurementType == aMeasurementType && cmd.RequestUnitScale.HasValue) ||
				(cmd.ResponseUnit?.MeasurementType == aMeasurementType && cmd.ResponseUnitScale.HasValue)))
			{
				foreach (var unit in UnitConverter.FindUnits(DeviceType.MotionType, aMeasurementType))
				{
					if (!UnitOfMeasure.HiddenUnitSymbols.Contains(unit.Abbreviation))
					{
						units.Add(unit);
					}
				}
			}

			units.Sort();
			units.Insert(0, UnitOfMeasure.Data);

			return units;
		}


		/// <summary>
		///     Get a list of the units of measure this device supports.
		/// </summary>
		/// <param name="aDimension">The dimension of the measure - length, velocity etc.</param>
		/// <returns>
		///     A list of units of measure, sorted with raw data as the
		///     first entry.
		/// </returns>
		public ICollection<UnitOfMeasure> GetUnitsOfMeasure(Dimension aDimension)
		{
			var units = new List<UnitOfMeasure>();

			foreach (var unit in UnitConverter.FindUnitsByDimension(aDimension))
			{
				if (!UnitOfMeasure.HiddenUnitSymbols.Contains(unit.Abbreviation))
				{
					units.Add(unit);
				}
			}

			units.Sort();
			units.Insert(0, UnitOfMeasure.Data);

			return units;
		}


		/// <summary>
		///     Get a list of all the units of measure this device supports,
		///     including all possible measurement types.
		/// </summary>
		/// <returns>A set of units of measure.</returns>
		/// <remarks>
		///     This method only returns units relevant to the old MeasurementType
		///     dimension enumeration, and not those relevant to the new database-driven
		///     unit system. Use GetUnitsOfMeasure(Dimension.AllDimensions) instead.
		/// </remarks>
		[Obsolete]
		public IEnumerable<UnitOfMeasure> GetAllUnitsOfMeasure()
		{
			var allUnits = new HashSet<UnitOfMeasure>();
			var measurementTypes = Enum.GetValues(typeof(MeasurementType));
			foreach (var unit in measurementTypes.Cast<MeasurementType>().SelectMany(mt => GetUnitsOfMeasure(mt)))
			{
				allUnits.Add(unit);
			}

			return allUnits;
		}


		/// <summary>
		///     Send a command to this device.
		/// </summary>
		/// <param name="aCommand">See <see cref="DataPacket.Command" />.</param>
		/// <param name="aData">See <see cref="DataPacket.NumericData" />.</param>
		/// <param name="aMessageId">See <see cref="DataPacket.MessageId" />.</param>
		/// <exception cref="InvalidOperationException">
		///     The specified port is
		///     not open.
		/// </exception>
		/// <exception cref="TimeoutException">
		///     The operation did not complete
		///     before the time-out period ended.
		/// </exception>
		/// <exception cref="IOException">
		///     The port is in an invalid state.
		/// </exception>
		public void Send(Command aCommand, int aData = 0, byte aMessageId = 0)
		{
			var commandInfo = DeviceType.GetCommandByNumber(aCommand);
			var measurement = CalculateMeasurement(commandInfo?.GetRequestConversion(), aData);
			var adjustedData = AdjustData(aData, aMessageId);
			_port.Send(DeviceNumber, aCommand, adjustedData, measurement);
		}


		/// <summary>
		///     Send a command to this device.
		/// </summary>
		/// <param name="aCommand">
		///     The command to send to the device without the
		///     slash, device address and axis number.
		/// </param>
		/// <param name="aMessageId">See <see cref="DataPacket.MessageId" />.</param>
		/// <exception cref="InvalidOperationException">
		///     The specified port is
		///     not open.
		/// </exception>
		/// <exception cref="TimeoutException">
		///     The operation did not complete
		///     before the time-out period ended.
		/// </exception>
		/// <exception cref="IOException">
		///     The port is in an invalid state.
		/// </exception>
		public void Send(string aCommand, byte aMessageId = 0)
		{
			var format = 0 != aMessageId ? "{0:00} {1} {2:00} {3}" : "{0:00} {1} {3}";
			_port.Send(string.Format(CultureInfo.InvariantCulture,
									 format,
									 DeviceNumber,
									 AxisNumber,
									 aMessageId,
									 aCommand));
		}


		/// <summary>
		///     Send a command to this device.
		/// </summary>
		/// <param name="aCommand">See <see cref="DataPacket.Command" />.</param>
		/// <param name="aMeasurement">
		///     The <see cref="DataPacket.NumericData" /> value
		///     will be calculated from this.
		/// </param>
		/// <param name="aMessageId">See <see cref="DataPacket.MessageId" />.</param>
		public void SendInUnits(Command aCommand, Measurement aMeasurement, byte aMessageId = 0)
		{
			var numericData = CalculateData(aCommand, aMeasurement);
			var adjustedData = AdjustData(numericData, aMessageId);
			_port.Send(DeviceNumber, aCommand, adjustedData, aMeasurement);
		}


		/// <summary>
		///     Send a command to this device.
		/// </summary>
		/// <param name="aCommand">
		///     The command to send to the device without the
		///     slash, device number or axis number.
		/// </param>
		/// <param name="aMeasurement">
		///     A data value to append to the command
		///     will be calculated from this. If it is null, then the command
		///     will be sent without any extra data.
		/// </param>
		/// <param name="aMessageId">The ASCII message ID to be sent.</param>
		/// <param name="aParamType">
		///     Optional parameter type information to control the numeric formatting. If omitted, integer
		///     values will be sent.
		/// </param>
		public void SendInUnits(string aCommand, Measurement aMeasurement, byte aMessageId = 0,
								ParameterType aParamType = null)
		{
			string format;
			if ((null == aMeasurement) && (0 == aMessageId))
			{
				format = "{0:00} {1} {3}";
			}
			else if ((null == aMeasurement) && (0 != aMessageId))
			{
				format = "{0:00} {1} {2:00} {3}";
			}
			else if ((null != aMeasurement) && (0 == aMessageId))
			{
				format = "{0:00} {1} {3} {4}";
			}
			else // measurement != null && messageId != 0
			{
				format = "{0:00} {1} {2:00} {3} {4}";
			}

			var measurementNum = null == aMeasurement ? 0 : CalculateExactData(aCommand, aMeasurement);
			string measurementStr;
			if (null == aParamType)
			{
				measurementStr = Math.Round(measurementNum).ToString();
			}
			else
			{
				measurementStr = aParamType.FormatNumeric(measurementNum);
			}

			_port.Send(string.Format(CultureInfo.InvariantCulture,
									 format,
									 DeviceNumber,
									 AxisNumber,
									 aMessageId,
									 aCommand,
									 measurementStr),
					   aMeasurement);
		}


		/// <summary>
		///     Sends a pre-formatted ASCII command to the device, converting
		///     all parameters into native units and inserting them using
		///     <see cref="string.Format(string, object[])" />.
		/// </summary>
		/// <param name="aFormat">The format to use, EXCLUDING device and axis ids.</param>
		/// <param name="aParamNodes">Pairs where the UnitOfMeasure component is null will be sent as raw strings.</param>
		/// <param name="aMessageId">The ASCII message ID to be sent to the device.</param>
		public void SendFormatInUnits(string aFormat, IEnumerable<ParameterData> aParamNodes, byte aMessageId = 0)
		{
			// get format for device and axis numbers
			var msgFormat = 0 != aMessageId ? "{0:00} {1} {2:00} {3}" : "{0:00} {1} {3}";

			// raw data to be entered into the command format
			var parameterDatas = aParamNodes.ToList();
			var maxIndex = parameterDatas.Any() ? parameterDatas.Max(n => n.ParameterIndex) : -1;

			// Make sure we reserve space for all expected parameters if that info is known.
			if (DeviceType.GetCommandByText(aFormat) is ASCIICommandInfo asciiInfo)
			{
				maxIndex = Math.Max(maxIndex, asciiInfo.Nodes.Count(node => node.IsParameter) - 1);
			}

			var parameters = new string[maxIndex + 1];

			void storeParam(int index, string value)
			{
				if (string.IsNullOrEmpty(parameters[index]))
				{
					parameters[index] = value;
				}
				else
				{
					parameters[index] += " " + value;
				}
			}

			// calculate data for each parameter node if its associated UOM
			// is not null
			foreach (var node in parameterDatas)
			{
				// Try to parse the data if a number, and if successful then unit convert it and format it for the parameter's precision.
				if ((null != node.SourceUnit) && node.TryParseData(out var val))
				{
					var value = CalculateExactData(aFormat, new Measurement(val, node.SourceUnit), node.ParameterIndex);
					storeParam(node.ParameterIndex,
							   node.ParamType?.FormatNumeric(value) ?? ((int) Math.Round(value)).ToString());
				}
				else
				{
					// If there is no unit associated or the data is not a number, use the raw string
					storeParam(node.ParameterIndex, node.Data);
				}
			}

			// Insert parameter data into command format string.
			// Note this relies on the .NET behavior of printing nothing for nulls
			// inside the parameter array; instead of raising an error here we
			// leave it up to the device to reject the command.
			var cmdFormatted =
				string.Format(CultureInfo.InvariantCulture, aFormat, parameters.Cast<object>().ToArray());

			// add device and axis numbers and send to device
			_port.Send(string.Format(CultureInfo.InvariantCulture,
									 msgFormat,
									 DeviceNumber,
									 AxisNumber,
									 aMessageId,
									 cmdFormatted));
		}


		/// <summary>
		///     Send a command to this device after a delay.
		/// </summary>
		/// <param name="aCommand">See <see cref="DataPacket.Command" />.</param>
		/// <param name="aData">See <see cref="DataPacket.NumericData" />.</param>
		/// <param name="aMessageId">See <see cref="DataPacket.MessageId" />.</param>
		/// <remarks>
		///     See <see cref="IZaberPort.SendDelayed" /> for the details
		///     of how the delay works. This command may never be sent, if the port
		///     gets closed.
		/// </remarks>
		/// <returns>
		///     The data packet that will be sent, or null if the port is
		///     already closed.
		/// </returns>
		public DataPacket SendDelayed(Command aCommand, int aData, byte aMessageId)
		{
			var port = _port;
			return port?.SendDelayed(DeviceNumber, aCommand, AdjustData(aData, aMessageId));
		}

		#endregion

		#region Protected methods

		/// <summary>
		///     Prime the device model for an impending microstep resolution change.
		/// </summary>
		/// <param name="aMessage">The message causing the change.</param>
		protected virtual void ExpectNewResolution(DeviceMessage aMessage)
			=> SetExpectedResolutionChange(this, aMessage.AxisNumber, (int) aMessage.NumericData);


		/// <summary>
		///     Record information about a new expected microstep resolution.
		/// </summary>
		/// <param name="aDevice">The device that is changing resolution.</param>
		/// <param name="aAxis">The affected axis.</param>
		/// <param name="aResolution">The new resolution.</param>
		protected static void SetExpectedResolutionChange(ZaberDevice aDevice, int aAxis, int aResolution)
			=> aDevice._newMicrostepResolution = new Tuple<int, int>(aAxis, aResolution);

		#endregion

		#region Private Methods

		private int AdjustData(int aData, byte aMessageId)
		{
			if (!AreMessageIdsEnabled)
			{
				return aData;
			}

			if ((aData < (-DATA_WITH_ID_MASK >> 1)) || ((DATA_WITH_ID_MASK >> 1) < aData))
			{
				throw new ConversionException("Data are too large for the Binary Protocol with IDs enabled.");
			}

			return (aMessageId << 24) | (aData & DATA_WITH_ID_MASK);
		}


		/// <summary>
		///     Calculate what the data value should be to represent the requested
		///     measurement on the requested command.
		/// </summary>
		/// <param name="aConversionInfo">
		///     The command metadata that will use the
		///     data.
		/// </param>
		/// <param name="aMeasurement">
		///     The measurement to send to this device.
		/// </param>
		/// <returns>A data value to send as part of a command. May need rounding depending on the parameter's precision.</returns>
		private decimal CalculateData(IParameterConversionInfo aConversionInfo, Measurement aMeasurement)
		{
			if (null == aConversionInfo?.Scale)
			{
				throw new InvalidOperationException("Unit of measure conversion is only allowed on known commands.");
			}

			if (UnitOfMeasure.MicrostepsPerSecond == aMeasurement.Unit)
			{
				// Re-lookup the definition of microsteps per second to make sure
				// we have the scale factor for this device.
				var uStepVel = new Measurement(aMeasurement.Value,
											   UnitConverter.FindUnitByAbbreviation(
												   UnitOfMeasure.MicrostepsPerSecondSymbol));
				var devUnits = UnitConverter.Convert(uStepVel,
													 UnitConverter.FindUnitByAbbreviation(
														 UnitOfMeasure.DeviceMicrostepVelocityUnitSymbol));

				return devUnits.Value;
			}

			if (UnitOfMeasure.MicrostepsPerSecondSquared == aMeasurement.Unit)
			{
				var uStepAccel = new Measurement(aMeasurement.Value,
												 UnitConverter.FindUnitByAbbreviation(
													 UnitOfMeasure.MicrostepsPerSecondSquaredSymbol));
				var devUnits = UnitConverter.Convert(uStepAccel,
													 UnitConverter.FindUnitByAbbreviation(
														 UnitOfMeasure.DeviceMicrostepAccelerationUnitSymbol));

				return devUnits.Value;
			}

			// Calculate measurement in base unit for command (ie m, m/s etc).
			var conversion = aMeasurement.Unit.Unit.GetTransformationTo(aConversionInfo.ReferenceUnit);
			var measurementInProperUnit = conversion.Transform(aMeasurement.Value);

			if (!aConversionInfo.IsRequestRelativePosition)
			{
				var transform = GetScalingFunctionTransform(aConversionInfo, MicrostepResolution);
				return transform.Transform(measurementInProperUnit);
			}

			// Relative distances are calculated as the difference
			// between the target position and the current position.
			var reverseTransform = GetScalingFunctionTransform(aConversionInfo, MicrostepResolution).Reverse();
			var recentValue = reverseTransform.Transform(RecentPosition.GetValueOrDefault());

			var absoluteMeasurement = measurementInProperUnit + recentValue;

			var forwardTransform = reverseTransform.Reverse();

			var absoluteData = forwardTransform.Transform(absoluteMeasurement);

			return absoluteData - RecentPosition.GetValueOrDefault();
		}


		/// <summary>
		///     Calculate a measurement for a given data value.
		/// </summary>
		/// <param name="aParameterInfo">
		///     The unit conversion data for the command that the data value is for.
		/// </param>
		/// <param name="aData">The data value to convert.</param>
		/// <returns>
		///     A measurement in the appropriate unit of measure, or as
		///     raw data if it couldn't be converted.
		/// </returns>
		private Measurement CalculateMeasurement(IParameterConversionInfo aParameterInfo, decimal aData)
		{
			if ((null == aParameterInfo) || !aParameterInfo.Scale.HasValue)
			{
				return new Measurement(aData, UnitOfMeasure.Data);
			}

			var reverseTransform = GetScalingFunctionTransform(aParameterInfo, MicrostepResolution).Reverse();
			var dataInUnits = reverseTransform.Transform(aData);
			return new Measurement(dataInUnits,
								   UnitConverter.FindUnitByAbbreviation(aParameterInfo.ReferenceUnit.Abbreviation));
		}


		/// <summary>
		///     Get a unit conversion function for a specific parameter or return value.
		/// </summary>
		/// <param name="aParamInfo">Unit conversion info for the quantity.</param>
		/// <returns>
		///     A unit quantity transformation that converts between the device units and the reference unit for the
		///     dimension.
		/// </returns>
		/// <remarks>Do not cache the return value as it is dependent on device state.</remarks>
		private QuantityTransformation GetDeviceSpecificUnitConversion(IParameterConversionInfo aParamInfo)
		{
			if ((null == aParamInfo) || !aParamInfo.Scale.HasValue)
			{
				return null;
			}

			return GetScalingFunctionTransform(aParamInfo, MicrostepResolution).Reverse();
		}


		/// <summary>
		///     Get device-specific unit conversion information. This data takes resolution
		///     into account, so it is not recommended to cache the return value.
		/// </summary>
		/// <param name="aDimension">Type of quantity to get the conversion for.</param>
		/// <returns>Unit conversion data for the quantity, or null if not known.</returns>
		private IParameterConversionInfo GetDeviceUnitConversionInfo(Dimension aDimension)
		{
			if (!_deviceSpecificConversions.TryGetValue(aDimension, out var result))
			{
				DeviceType.DeviceTypeUnitConversionInfo.TryGetValue(aDimension, out result);
			}

			return result;
		}


		/// <summary>
		///     Determine the base unit of position for this device - typically meters or degrees.
		/// </summary>
		/// <returns>Unit to use as the base for position unit conversions.</returns>
		private UnitOfMeasure GetCoherentPositionUnit()
		{
			var steps = UnitConverter.FindUnitByAbbreviation(UnitOfMeasure.StepSymbol);
			if (null == steps)
			{
				return null;
			}

			var transform = steps.Unit.TransformationToCoherent;
			return UnitOfMeasure.FindByAbbreviation(transform.TargetUnit.Abbreviation);
		}


		/// <summary>
		///     Determine the base unit of velocity for this device - typically meters or degrees per second.
		/// </summary>
		/// <returns>Unit to use as the base for velocity unit conversions.</returns>
		private UnitOfMeasure GetCoherentVelocityUnit()
		{
			var steps = UnitConverter.FindUnitByAbbreviation(UnitOfMeasure.DeviceStepVelocityUnitSymbol);
			if (null == steps)
			{
				return null;
			}

			var transform = steps.Unit.TransformationToCoherent;
			return UnitOfMeasure.FindByAbbreviation(transform.TargetUnit.Abbreviation);
		}


		/// <summary>
		///     Determine the base unit of acceleration for this device - typically meters or degrees per square second.
		/// </summary>
		/// <returns>Unit to use as the base for acceleration unit conversions.</returns>
		private UnitOfMeasure GetCoherentAccelerationUnit()
		{
			var steps = UnitConverter.FindUnitByAbbreviation(UnitOfMeasure.DeviceStepAccelerationUnitSymbol);
			if (null == steps)
			{
				return null;
			}

			var transform = steps.Unit.TransformationToCoherent;
			return UnitOfMeasure.FindByAbbreviation(transform.TargetUnit.Abbreviation);
		}


		/// <summary>
		///     Get a function that converts between a real-world unit and device-specific units.
		/// </summary>
		/// <param name="aInfo">Parameter or return value information to be converted to or from device units.</param>
		/// <param name="aResolution">The microstep resolution. Can be omitted if the scaling function does not use it.</param>
		/// <returns>
		///     A quantity converter that converts from the specified target unit to the device's native units for the
		///     quantity.
		/// </returns>
		/// <remarks>Do not cache the return value as it may be dependent on momentary device state.</remarks>
		private static QuantityTransformation GetScalingFunctionTransform(
			IParameterConversionInfo aInfo, int aResolution = 0)
		{
			if ((null == aInfo) || !aInfo.Scale.HasValue)
			{
				throw new ArgumentException("Cannot do unit conversion; scale factor is not known.");
			}

			if (((ScalingFunction.LinearResolution == aInfo.Function)
			 || (ScalingFunction.TangentialResolution == aInfo.Function))
			&& (0 == aResolution))
			{
				throw new ArgumentException("resolution cannot be 0 when it is requred by a scaling function.");
			}

			switch (aInfo.Function)
			{
				case ScalingFunction.Linear:

					// DeviceUnits = Scale * PhysicalUnits.
					return new LinearQuantityTransformation(aInfo.ReferenceUnit,
															UnitOfMeasure.Data.Unit,
															(double) aInfo.Scale,
															0.0);

				case ScalingFunction.Reciprocal:

					// DeviceUnits = Scale / PhysicalUnits.
					return new ReciprocalQuantityTransformation(aInfo.ReferenceUnit,
																UnitOfMeasure.Data.Unit,
																(double) aInfo.Scale) { HandleDivideByZero = true };

				case ScalingFunction.LinearResolution:

					// DeviceUnits = Scale * Resolution * PhysicalUnits.
					return new LinearQuantityTransformation(aInfo.ReferenceUnit,
															UnitOfMeasure.Data.Unit,
															(double) (aInfo.Scale * aResolution),
															0.0);

				case ScalingFunction.TangentialResolution:

					// DeviceUnits = Scale * Resolution 
					// * tan(PhysicalUnits * Pi / 180). 
					// This formula can only be applied to absolute positions.
					return new UserQuantityTransformation(aInfo.ReferenceUnit,
														  UnitOfMeasure.Data.Unit,
														  d => (double) (aInfo.Scale * aResolution)
														   * Math.Tan((d * Math.PI) / 180.0),
														  d => (180.0
															* Math.Atan(d / (double) (aInfo.Scale * aResolution)))
														   / Math.PI);

				default:
					throw new ArgumentException("Unexpected scaling function.");
			}
		}


		// Updates the unit of measure conversions for resolution- or other setting-specific
		// units. Note that this always creates a new local conversion table.
		private void UpdateResolutionUnits()
		{
			if ((null == DeviceType?.UnitConverter) || (null == DeviceType?.Commands))
			{
				// Can't define local units without having a base table and commands.
				return;
			}

			UnitConverter.Parent = DeviceType.UnitConverter;
			var basePositionUnit = GetCoherentPositionUnit();
			var baseVelocityUnit = GetCoherentVelocityUnit();
			var baseAccelerationUnit = GetCoherentAccelerationUnit();

			if (((null == basePositionUnit) | (null == baseVelocityUnit)) || (null == baseAccelerationUnit))
			{
				// Can't define microstep units without being able to identify coherent unit.
				return;
			}

			var newUnits = new List<Unit>();

			var positionScale = UnitConverter.UnitConverter.Convert(1.0,
																	basePositionUnit.Unit,
																	UnitConverter.UnitConverter.FindUnitBySymbol(
																		UnitOfMeasure.StepSymbol));
			var velocityScale = UnitConverter.UnitConverter.Convert(1.0,
																	baseVelocityUnit.Unit,
																	UnitConverter.UnitConverter.FindUnitBySymbol(
																		UnitOfMeasure
																		.DeviceStepVelocityUnitSymbol));
			var accelerationScale = UnitConverter.UnitConverter.Convert(1.0,
																		baseAccelerationUnit.Unit,
																		UnitConverter.UnitConverter.FindUnitBySymbol(
																			UnitOfMeasure
																			.DeviceStepAccelerationUnitSymbol));

			// Native time units are not seconds, so time-dependent microstep units need extra scaling.
			// These conversions avoid the use of hardcoded constants that are dependent on device hardware and firmware.

			// Generate a device unit for native acceleration scaled by resolution.
			var microAccel = CreateResolutionUnit(UnitOfMeasure.DeviceStepAccelerationUnitSymbol,
												  UnitOfMeasure.DeviceMicrostepAccelerationUnitSymbol,
												  "Device Acceleration Units");

			UnitConverter.RegisterUnit(microAccel);
			_deviceSpecificConversions[microAccel.Dimension] = new ParameterUnitConversion
			{
				ReferenceUnit = baseAccelerationUnit.Unit,
				IsRequestRelativePosition = false,
				Function = ScalingFunction.Linear,
				Scale = baseAccelerationUnit.Unit.GetTransformationTo(microAccel).Transform(1.0m)
			};

			// And a microsteps per square second acceleration unit.
			var microStepsPerSecondSquared = new ScaledShiftedUnit(UnitOfMeasure.MicrostepsPerSecondSquaredSymbol,
																   "Microsteps per Second Squared",
																   microAccel,
																   accelerationScale / positionScale);

			newUnits.Add(microStepsPerSecondSquared);

			// Generate a device unit for native velocity scaled by resolution.
			var microVel = CreateResolutionUnit(UnitOfMeasure.DeviceStepVelocityUnitSymbol,
												UnitOfMeasure.DeviceMicrostepVelocityUnitSymbol,
												"Device Velocity Units");

			UnitConverter.RegisterUnit(microVel);
			_deviceSpecificConversions[microVel.Dimension] = new ParameterUnitConversion
			{
				ReferenceUnit = baseVelocityUnit.Unit,
				IsRequestRelativePosition = false,
				Function = ScalingFunction.Linear,
				Scale = baseVelocityUnit.Unit.GetTransformationTo(microVel).Transform(1.0m)
			};

			// And a microsteps per second velocity unit.
			var microStepsPerSecond = new ScaledShiftedUnit(UnitOfMeasure.MicrostepsPerSecondSymbol,
															"Microsteps per Second",
															microVel,
															velocityScale / positionScale);

			newUnits.Add(microStepsPerSecond);

			// Generate a device unit for microsteps.
			var microSteps = CreateResolutionUnit(UnitOfMeasure.StepSymbol,
												  UnitOfMeasure.MicrostepSymbol,
												  "Microsteps");

			newUnits.Add(microSteps);
			_deviceSpecificConversions[microSteps.Dimension] = new ParameterUnitConversion
			{
				ReferenceUnit = basePositionUnit.Unit,
				IsRequestRelativePosition = false,
				Function = ScalingFunction.Linear,
				Scale = basePositionUnit.Unit.GetTransformationTo(microSteps).Transform(1.0m)
			};

			// Register the new units and add UOM wrappers.
			UnitOfMeasure derivative = null;
			foreach (var unit in newUnits)
			{
				unit.UseSIPrefixes = false;
				UnitConverter.RegisterUnit(unit);

				// Precache the new device-specific units and identify their derivatives.
				// Note the assignment of derivatives depends on iteration order here!
				var newUom = new UnitOfMeasure(unit, derivative) { IsDeviceDependent = true };

				_deviceUnits[unit.Abbreviation] = newUom;
				derivative = newUom;
			}
		}


		private Unit CreateResolutionUnit(string aBaseUnitSymbol, string aNewUnitSymbol, string aDescription)
		{
			var baseUnit = UnitConverter.FindUnitByAbbreviation(aBaseUnitSymbol);
			var newUnit = new ScaledShiftedUnit(aNewUnitSymbol,
												aDescription,
												baseUnit.Unit,
												1.0 / MicrostepResolution) { UseSIPrefixes = false };

			return newUnit;
		}


		/// <summary>
		///     Helper for FormatAsciiCommand().
		/// </summary>
		/// <param name="val">Single value to format.</param>
		/// <param name="aParamType">Formatting information for the command parametern.</param>
		/// <param name="aConversionInfo">Unit conversion information for the command parameter.</param>
		/// <returns>Parameter value unit converted and formatted appropriately, as a string.</returns>
		/// <exception cref="ArgumentException">The data type could not be interpreted.</exception>
		private string FormatAsciiParameter(object val, ParameterType aParamType,
											IParameterConversionInfo aConversionInfo)
		{
			var data = 0m;
			Unit sourceUnit = null;

			if (val is ParameterData pd)
			{
				data = decimal.Parse(pd.Data);
				sourceUnit = pd.SourceUnit.Unit;
			}
			else if (val is Measurement zm)
			{
				data = zm.Value;
				sourceUnit = zm.Unit.Unit;
			}
			else if (val is Units.Measurement um)
			{
				data = (decimal) um.Value;
				sourceUnit = um.Unit;
			}
			else if (val is System.IConvertible ic)
			{
				data = ic.ToDecimal(CultureInfo.InvariantCulture);
			}
			else if (val is string valStr)
			{
				data = decimal.Parse(valStr);
			}
			else
			{
				throw new ArgumentException("Numeric type cannot be converted.");
			}

			if ((null != sourceUnit) && (UnitOfMeasure.Data.Unit != sourceUnit))
			{
				var conversion = GetConversionToParameterUnits(sourceUnit, aConversionInfo)
							 ?? throw new ArgumentException(
									 "Could not find a unit conversion for one of the command parameters.");
				data = conversion.Transform(data);
			}

			var textVal = aParamType.FormatNumeric(data);
			return textVal;
		}

		#endregion

		#region -- Private data --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private const int DEFAULT_ACCESS_LEVEL = int.MaxValue;
		private const int DATA_WITH_ID_MASK = 0xFFFFFF;

		private IZaberPort _port;
		private readonly List<ZaberDevice> _axes = new List<ZaberDevice>();
		private DeviceMessage _currentRequest;
		private byte? _expectedNewNumber;
		private Tuple<int, int> _newMicrostepResolution;

		private DeviceType _deviceType;
		private int _microstepResolution;

		private readonly Dictionary<string, UnitOfMeasure> _deviceUnits = new Dictionary<string, UnitOfMeasure>();

		private readonly Dictionary<Dimension, IParameterConversionInfo> _deviceSpecificConversions =
			new Dictionary<Dimension, IParameterConversionInfo>();

		private bool _areAsciiMessageIdsEnabled;

		#endregion
	}
}

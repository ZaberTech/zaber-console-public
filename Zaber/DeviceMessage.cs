﻿using System;
using System.Globalization;
using System.Text;

namespace Zaber
{
	/// <summary>
	///     A device message is a <see cref="DataPacket" /> with some added information.
	/// </summary>
	/// <remarks>
	///     When a <see cref="ZaberDevice" /> receives a response from the port, it
	///     adds context details before raising its own event.
	/// </remarks>
	[Serializable]
	public class DeviceMessage : DataPacket
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor. Does nothing.
		/// </summary>
		public DeviceMessage()
		{
		}


		/// <summary>
		///     Parse a text message into a device message.
		/// </summary>
		/// <param name="aText">parse details from this text</param>
		public DeviceMessage(string aText)
			: base(aText)
		{
		}


		/// <summary>
		///     Copy constructor that copies a DataPacket.
		/// </summary>
		/// <param name="aSource">copy details from this packet</param>
		public DeviceMessage(DataPacket aSource)
			: base(aSource)
		{
			if (aSource is DeviceMessage other)
			{
				_commandInfo = other._commandInfo;
			}
		}


		/// <summary>
		///     Format the message as a string, assuming that it was sent as a
		///     request.
		/// </summary>
		/// <returns>The message as a string</returns>
		public override string FormatRequest()
		{
			var paramDescription = _commandInfo?.DataDescription;
			return Format("requests", paramDescription);
		}


		/// <summary>
		///     Format the message as a string, assuming that it was sent as a
		///     response.
		/// </summary>
		/// <returns>The message as a string</returns>
		public override string FormatResponse()
		{
			var paramDescription = _commandInfo?.ResponseDescription;
			return Format("responds", paramDescription);
		}


		/// <summary>
		///     More details about the command specified in <see cref="Command" />.
		/// </summary>
		/// <value>
		///     May be null if the command is not recognized.
		/// </value>
		public CommandInfo CommandInfo
		{
			get => _commandInfo;
			set => _commandInfo = value;
		}


		/// <summary>
		///     Gets or sets the request message that generated this response.
		/// </summary>
		/// <remarks>
		///     This is only set on response messages, and only when
		///     the request is known.
		/// </remarks>
		public DeviceMessage Request { get; set; }

		#endregion

		#region -- Non-Public Methods --

		private string Format(string aAction, string aParamDescription)
		{
			if (null != Text)
			{
				return Text;
			}

			var result = new StringBuilder();

			var deviceNumberText = DeviceNumber.ToString(CultureInfo.CurrentCulture).PadLeft(3);
			var messageIdText =
				MessageIdType.Numeric != MessageIdType
					? string.Empty
					: 0 == MessageId
						? "     "
						: string.Format(CultureInfo.CurrentCulture, "{0:000}: ", MessageId);

			if (null != _commandInfo)
			{
				var numericData = (int) NumericData;
				var param = null == aParamDescription
					? string.Empty
					: (Command.Error == Command) && Enum.IsDefined(typeof(ZaberError), numericData)
						? string.Format(CultureInfo.CurrentCulture, "({0})", (ZaberError) numericData)
						: string.Format(CultureInfo.CurrentCulture, "({0} = {1})", aParamDescription, NumericData);

				result.AppendFormat(CultureInfo.CurrentCulture,
									"{4}Device {0} {3} {1}{2}",
									deviceNumberText,
									_commandInfo.Name,
									param,
									aAction,
									messageIdText);
			}
			else
			{
				result.AppendFormat(CultureInfo.CurrentCulture,
									"{4}Device {0} {3} {1}({2})",
									deviceNumberText,
									Command,
									NumericData,
									aAction,
									messageIdText);
			}

			return result.ToString();
		}

		#endregion

		#region -- Data --

		private CommandInfo _commandInfo;

		#endregion
	}
}

﻿using System;
using System.ComponentModel;
using System.Linq;

namespace Zaber
{
	/// <summary>
	///     Extension methods for enumerations.
	/// </summary>
	public static class EnumExtensions
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Get the description of an enumeration value.
		/// </summary>
		/// <param name="aValue">A legal enum value.</param>
		/// <returns>
		///     The contents of the value's description attribute if it has one,
		///     or null.
		/// </returns>
		public static string GetDescription(this Enum aValue)
		{
			var enumType = aValue.GetType();
			if (Enum.IsDefined(enumType, aValue))
			{
				var fieldInfo = enumType.GetField(aValue.ToString());
				var attrs = fieldInfo?.GetCustomAttributes(false);

				if ((null != attrs) && (attrs.Length > 0))
				{
					var attrib = attrs.FirstOrDefault(a => a is DescriptionAttribute) as DescriptionAttribute;
					return attrib?.Description;
				}
			}

			return null;
		}

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace Zaber.FirmwareDownload
{
	/// <summary>
	///     This class can be used to extract one piece of firmware from a firmware data stream. It does not
	///     check file headers and expects them to have already been skipped, so can be used for firmware
	///     transmitted from any binary data source.
	/// </summary>
	[CLSCompliant(false)]
	public class FirmwareUnpacker
	{
		#region -- Public API --

		/// <summary>
		///     Type for progress report callback events.
		/// </summary>
		/// <param name="aCurrentProgress">Current progress value, starting at zero for no work done.</param>
		/// <param name="aMaximum">
		///     Maximum progress value that aCurrentProgress can reach. (Might not actually
		///     be that value when the work is completed though.)
		/// </param>
		public delegate void ProgressReportCallback(int aCurrentProgress, int aMaximum);

		/// <summary>
		///     Hook into this event to receive decode progress reports.
		/// </summary>
		public event ProgressReportCallback ProgressReported;


		/// <summary>
		///     Static helper method to extract one piece of firmware from a stream.
		/// </summary>
		/// <param name="aStream">The byte stream to extract from.</param>
		/// <param name="aPlatform">The target platform for the firmware (from the system.platform setting on the device).</param>
		/// <param name="aSerial">The serial number of the device (from the system.serial setting).</param>
		/// <param name="aProgressCallback">(Optional) callback for decode progress reporting.</param>
		/// <returns>A byte array containing the firmware image for the device.</returns>
		/// <exception cref="ObjectDisposedException">The stream has already been disposed.</exception>
		/// <exception cref="NotSupportedException">The stream type does not support reading.</exception>
		/// <exception cref="InvalidDataException">The data is corrupted.</exception>
		/// <exception cref="EndOfStreamException">The unpacker attempted to read past the end of the input stream.</exception>
		/// <exception cref="FirmwareUnpackException">
		///     The extract operation failed, for example
		///     because the stream does not contain the correct firmware for your device.
		/// </exception>
		public static byte[] Unpack(Stream aStream, uint aPlatform, uint aSerial,
									ProgressReportCallback aProgressCallback = null)
		{
			var unpacker = new FirmwareUnpacker(aStream, aPlatform, aSerial);

			if (null != aProgressCallback)
			{
				unpacker.ProgressReported += (aCurrent, aMax) => { aProgressCallback(aCurrent, aMax); };
			}

			return unpacker.Unpack();
		}


		/// <summary>
		///     Instantiate a firmware unpacker instance.
		/// </summary>
		/// <param name="aStream">
		///     The byte stream to extract from. This class does not assume ownership of the stream
		///     and the caller is responsible for closing and disposing it after the extractor is finished.
		/// </param>
		/// <param name="aPlatform">The target platform for the firmware (from the system.platform setting on the device).</param>
		/// <param name="aSerial">The serial number of the device (from the system.serial setting).</param>
		/// <exception cref="ArgumentNullException">The stream is null.</exception>
		public FirmwareUnpacker(Stream aStream, uint aPlatform, uint aSerial)
		{
			_input = aStream;
			if (null == _input)
			{
				throw new ArgumentNullException("Input stream is null.");
			}

			_platform = aPlatform;
			_serialNumber = aSerial;
			_output = new MemoryStream();

			_registers = new bool[65536];
			for (var i = 0; i < 65536; ++i)
			{
				_registers[i] = false;
			}

			// Build a dispatch table of opcode handler methods.
			foreach (var mi in GetType()
			.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
			{
				foreach (OpcodeHandlerAttribute attr in mi.GetCustomAttributes(typeof(OpcodeHandlerAttribute), true))
				{
					var op = attr.Opcode;
					_dispatchTable.Add(op, (OpcodeHandler) Delegate.CreateDelegate(typeof(OpcodeHandler), this, mi));
				}
			}
		}


		/// <summary>
		///     Extract one piece of firmware from the stream.
		/// </summary>
		/// <comments>
		///     Side effect: The input stream read pointer is affected and generally left at the end of the stream.
		///     You cannot call this twice on the same instance and expect valid results the second time.
		/// </comments>
		/// <returns>A byte array containing the firmware image for the device.</returns>
		/// <exception cref="ObjectDisposedException">The stream has already been disposed.</exception>
		/// <exception cref="NotSupportedException">The stream type does not support reading.</exception>
		/// <exception cref="InvalidDataException">The data is corrupted.</exception>
		/// <exception cref="EndOfStreamException">The unpacker attempted to read past the end of the input stream.</exception>
		/// <exception cref="FirmwareUnpackException">
		///     The extract operation failed, for example
		///     because the stream does not contain the correct firmware for your device.
		/// </exception>
		public byte[] Unpack()
		{
			Execute();
			var data = _output.GetBuffer();
			var result = data;

			// Make sure the output array contains only image data, and no unused bytes at the end.
			if (_output.Position != data.Length)
			{
				result = new byte[_output.Position];
				for (var i = 0; i < result.Length; ++i)
				{
					result[i] = data[i];
				}
			}

			return result;
		}

		#endregion

		#region -- Opcode handlers --

		[OpcodeHandler(Opcode.AND)]
		private void And()
		{
			var s1 = _input.ReadUInt16();
			var s2 = _input.ReadUInt16();
			var d = _input.ReadUInt16();
			if (0 == _skip)
			{
				_registers[d] = _registers[s1] & _registers[s2];
			}
		}


		[OpcodeHandler(Opcode.OR)]
		private void Or()
		{
			var s1 = _input.ReadUInt16();
			var s2 = _input.ReadUInt16();
			var d = _input.ReadUInt16();
			if (0 == _skip)
			{
				_registers[d] = _registers[s1] | _registers[s2];
			}
		}


		[OpcodeHandler(Opcode.XOR)]
		private void Xor()
		{
			var s1 = _input.ReadUInt16();
			var s2 = _input.ReadUInt16();
			var d = _input.ReadUInt16();
			if (0 == _skip)
			{
				_registers[d] = _registers[s1] ^ _registers[s2];
			}
		}


		[OpcodeHandler(Opcode.NOT)]
		private void Not()
		{
			var s = _input.ReadUInt16();
			var d = _input.ReadUInt16();
			if (0 == _skip)
			{
				_registers[d] = !_registers[s];
			}
		}


		[OpcodeHandler(Opcode.IF)]
		private void If()
		{
			var s = _input.ReadUInt16();

			if (_input.Position >= _input.Length)
			{
				throw new InvalidDataException("Attempted to read past end of data.");
			}

			var n = (uint) _input.ReadByte();

			// If we are currently skipping instructions, add this IF instruction's
			// skip count to the total because we have to skip all its body contents as
			// if they were part of the single IF instruction.
			// If we are not skipping instructions and the condition is false, start skipping.
			// Otherwise do not change the skip count, as skipped IF conditions should not
			// alter the place where we start evaluating code again.
			if ((_skip == 0) && !_registers[s])
			{
				_skip += n;
			}
		}


		[OpcodeHandler(Opcode.EMIT)]
		private void Emit()
		{
			var n = _input.ReadUInt16();

			if (_input.Position > (_input.Length - n))
			{
				throw new InvalidDataException("Attempted to read past end of data.");
			}

			var data = new byte[n];
			_input.ReadBlocking(data, 0, n);

			if (0 == _skip)
			{
				_output.Write(data, 0, n);
			}
		}


		[OpcodeHandler(Opcode.ERROR)]
		private void Error()
		{
			if (_input.Position >= _input.Length)
			{
				throw new InvalidDataException("Attempted to read past end of data.");
			}

			var len = _input.ReadByte();

			if (_input.Position > (_input.Length - len))
			{
				throw new InvalidDataException("Attempted to read past end of data.");
			}

			var buffer = new byte[len];
			_input.ReadBlocking(buffer, 0, len);

			if (0 == _skip)
			{
				throw new FirmwareUnpackException(Encoding.UTF8.GetString(buffer));
			}
		}


		[OpcodeHandler(Opcode.ISPLATFORM)]
		private void IsPlatform()
		{
			var p = _input.ReadUInt32();
			var d = _input.ReadUInt16();
			if (0 == _skip)
			{
				_registers[d] = p == _platform;
			}
		}


		[OpcodeHandler(Opcode.ISSERIAL)]
		private void IsSerial()
		{
			var s = _input.ReadUInt32();
			var d = _input.ReadUInt16();
			if (0 == _skip)
			{
				_registers[d] = s == _serialNumber;
			}
		}

		#endregion

		#region -- Non-Public Methods --

		private void Execute()
		{
			while (_input.Position < _input.Length)
			{
				var skipping = _skip > 0UL;

				var op = (Opcode) _input.ReadByte();
				OpcodeHandler handler = null;

				if (_dispatchTable.TryGetValue(op, out handler))
				{
					// We invoke the handlers for all instructions, even when skipping instructions,
					// because the instruction size varies. Handlers are responsible for skipping the
					// correct amount of data (including nested instructions) if we are in skip mode.
					handler();
				}
				else
				{
					throw new InvalidDataException($"Unrecognized opcode {(int) op}; data may be corrupt.");
				}

				if (skipping && (_skip > 0UL)) // Don't decrement after transitioning into the skipping state.
				{
					_skip--;
				}

				// Report progress.
				if (ProgressReported != null)
				{
					ProgressReported((int) _input.Position, (int) _input.Length);
				}
			}
		}

		#endregion

		#region -- Private types and data --

		// Supported opcodes
		private enum Opcode
		{
			AND = 0,
			OR = 1,
			XOR = 2,
			NOT = 3,
			IF = 4,
			EMIT = 5,
			ERROR = 6,
			ISPLATFORM = 7,
			ISSERIAL = 8
		}

		// Type of opcode handler methods.
		private delegate void OpcodeHandler();

		// Dispatch table for opcodes.
		private readonly Dictionary<Opcode, OpcodeHandler> _dispatchTable = new Dictionary<Opcode, OpcodeHandler>();

		// Attribute for identifying opcode handler methods.
		[AttributeUsage(AttributeTargets.Method)]
		private class OpcodeHandlerAttribute : Attribute
		{
			#region -- Public Methods & Properties --

			public OpcodeHandlerAttribute(Opcode aOp)
			{
				Opcode = aOp;
			}


			public Opcode Opcode { get; }

			#endregion
		}


		private readonly Stream _input;
		private readonly MemoryStream _output;
		private readonly uint _platform;
		private readonly uint _serialNumber;
		private readonly bool[] _registers;
		private ulong _skip;

		#endregion
	}
}

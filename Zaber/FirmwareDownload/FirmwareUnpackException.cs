﻿using System;
using System.Runtime.Serialization;

namespace Zaber.FirmwareDownload
{
	/// <summary>
	///     Conveys messages output by the firmware file decoder as a result of decoding.
	///     These are not the result of decoding errors, but rather messages that the decoder
	///     outputs in place of a firmware image, for example because the file does not
	///     contain the right firmware for your device.
	/// </summary>
	[Serializable]
	public class FirmwareUnpackException : Exception
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor. Does not initialize message.
		/// </summary>
		public FirmwareUnpackException()
		{
		}


		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aMessage">The message to display</param>
		public FirmwareUnpackException(string aMessage)
			: base(aMessage)
		{
		}


		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aMessage">The message to display</param>
		/// <param name="aException">The exception that caused this one.</param>
		public FirmwareUnpackException(string aMessage, Exception aException)
			: base(aMessage, aException)
		{
		}


		/// <summary>
		///     Initializes a new instance with serialized data.
		/// </summary>
		/// <param name="aSerializationInfo">
		///     The System.Runtime.Serialization.SerializationInfo that holds the serialized
		///     object data about the exception being thrown.
		/// </param>
		/// <param name="aContext">
		///     The System.Runtime.Serialization.StreamingContext that contains contextual
		///     information about the source or destination.
		/// </param>
		protected FirmwareUnpackException(SerializationInfo aSerializationInfo, StreamingContext aContext)
			: base(aSerializationInfo, aContext)
		{
		}

		#endregion
	}
}

﻿using System;
using System.Text;

namespace Zaber.FirmwareDownload
{
	/// <summary>
	///     Implements Base-64, Base-32 and Base-16 encoding and decoding of binary data, as
	///     described in RFC 4648: http://tools.ietf.org/html/rfc4648
	///     Selection of the encoding is determined by which alphabet is used; the alphabet
	///     size must be 65 for Base-64, 33 for Base-32, or 16 for Base-16 (which doesn't require
	///     padding, hence no extra padding character).
	/// </summary>
	/// <comments>
	///     Only encoding is currently implemented, as we haven't had a need for decoding yet.
	/// </comments>
	public static class BaseNCodec
	{
		#region -- Public Methods & Properties --

		#region -- Public API --

		/// <summary>
		///     Encode binary data into a string. This function will attempt to autodetect the number of bits
		///     per output character from the alphabet size, and can handle 1, 2, 3, 4, 5, 6 or 7 bits at a time.
		///     Non-power-of-two bit sizes require a padding character, and thus an odd-sized alphabet.
		///     Padding characters are added to achieve a standard quantum, which is the least common multiple
		///     of 8 and the number of encoding bits.
		///     This implementation does not output linefeeds.
		/// </summary>
		/// <param name="aData">Buffer containing the data to encode.</param>
		/// <param name="aStartIndex">Index of the first byte to encode from the buffer.</param>
		/// <param name="aLength">Number of bytes to encode from the buffer.</param>
		/// <param name="aAlphabet">Encoding alphabet to use.</param>
		/// <returns>A string representing the binary data encoded with the given alphabet.</returns>
		/// <exception cref="ArgumentOutOfRangeException">
		///     The start index and length arguments specify a range that
		///     is outside of the bounds of the input data buffer.
		/// </exception>
		/// <exception cref="ArgumentNullException">The input data buffer or alphabet string is null.</exception>
		/// <exception cref="InvalidOperationException">The alphabet size is inappropriate.</exception>
		public static string Encode(byte[] aData, int aStartIndex, int aLength, string aAlphabet)
		{
			if (null == aAlphabet)
			{
				throw new ArgumentNullException("Alphabet cannot be null.");
			}

			var outputSize = 0;
			var quantumSize = 0;
			var padding = '\0';

			// Guess base size in number of bits and padding character from alphabet size.
			for (var i = 1; i < 8; ++i)
			{
				if ((1 << i) == aAlphabet.Length)
				{
					outputSize = i;
					break;
				}

				if ((1 << i) == (aAlphabet.Length - 1))
				{
					outputSize = i;
					padding = aAlphabet[aAlphabet.Length - 1];
					break;
				}
			}

			// Validate guess; blame alphabet size if wrong.
			if (0 == outputSize)
			{
				throw new InvalidOperationException($"Unsupported alphabet size {aAlphabet.Length}.");
			}

			if ('\0' != padding)
			{
				if (0 == (outputSize & (outputSize - 1))) // Padding is not needed for power-of-two bit sizes.
				{
					throw new InvalidOperationException($"Unsupported alphabet size {aAlphabet.Length}.");
				}
			}

			// Input validated. Start encoding.
			quantumSize = LeastCommonMultiple(8, outputSize) / outputSize;
			var bitsLeft = 8 * aLength;
			var charsOutput = 0;

			var bitReader = new BitStreamReader(aData, aStartIndex, aLength);
			var output = new StringBuilder();

			// Main output loop.
			while (bitsLeft > 0)
			{
				var val = (int) bitReader.ReadBits(outputSize);
				output.Append(aAlphabet[val]);
				bitsLeft -= outputSize;
				charsOutput++;
			}

			// Add padding if needed.
			if ((charsOutput % quantumSize) > 0)
			{
				for (var i = charsOutput % quantumSize; i < quantumSize; ++i)
				{
					output.Append(padding);
				}
			}

			return output.ToString();
		}

		#endregion

		#endregion

		#region -- Non-Public Methods --

		#region -- Private methods --

		// TODO: Move this to a math helper if more such methods show up.
		// Also there is probably a faster way to compute this.
		private static int LeastCommonMultiple(params int[] aValues)
		{
			var num = aValues.Length;
			if (0 == num)
			{
				return 0;
			}

			if (1 == num)
			{
				return aValues[0];
			}

			var tempValues = new int[num];
			aValues.CopyTo(tempValues, 0);

			for (var i = 0; i < num; ++i)
			{
				if (tempValues[i] < 0)
				{
					tempValues[i] *= -1;
				}
				else if (0 == tempValues[i])
				{
					return 0;
				}
			}

			var minValue = tempValues[0];
			foreach (var v in tempValues)
			{
				if (v < minValue)
				{
					minValue = v;
				}
			}

			// Main loop: Add the original value of the smallest entry to its current value,
			// until all values are equal.
			// This could take a really long time if the LCM is a very big number. This algorithm
			// was chosen because our original need was the LCM of two numbers less than ten, and
			// the ability to handle more than two values was speculative generality.
			var same = false;
			while (!same)
			{
				var oldMin = minValue;
				same = true;
				minValue = -1;

				for (var i = 0; i < num; ++i)
				{
					if (tempValues[i] == oldMin)
					{
						tempValues[i] += aValues[i];
					}

					if ((minValue < 0) || (tempValues[i] < minValue))
					{
						minValue = tempValues[i];
					}

					if ((i > 0) && same)
					{
						same &= tempValues[i] == tempValues[i - 1];
					}
				}
			}

			return tempValues[0];
		}

		#endregion

		#endregion

		#region -- Predefined alphabets --

		/// <summary>
		///     The default Base-64 alphabet as described in the RFC.
		/// </summary>
		public const string BASE_64_STANDARD = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

		/// <summary>
		///     URL- and filename-safe Base-64 alphabet from the RFC.
		/// </summary>
		public const string BASE_64_URLSAFE = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_=";

		/// <summary>
		///     The default Base-32 alphabet from the RFC.
		/// </summary>
		public const string BASE_32_STANDARD = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567=";

		/// <summary>
		///     A Base-32 alphabet based on extending hexadecimal notation, as described in the RFC.
		/// </summary>
		public const string BASE_32_EXTENDED_HEX = "0123456789ABCDEFGHIJKLMNOPQRSTUV=";

		/// <summary>
		///     Normal hexadecimal encoding for Base-16.
		/// </summary>
		public const string BASE_16_STANDARD = "0123456789ABCDEF";

		#endregion
	}
}

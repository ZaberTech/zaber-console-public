﻿using System;
using System.IO;

namespace Zaber.FirmwareDownload
{
	/// <summary>
	///     Decodes a firmware file, including checking the header validity.
	///     It must be possible to determine the total length of the file; if that is not
	///     the case then you probably don't want the header checking, and should use the
	///     stream decoder instead.
	/// </summary>
	[CLSCompliant(false)]
	public static class FirmwareFileUnpacker
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Opens a file on disk and extracts one piece of firmware. The file must be readable to the current user
		///     and not opened for writing by another program.
		/// </summary>
		/// <param name="aFilename">Path to the file. Can be absolute or relative to the current working directory.</param>
		/// <param name="aPlatform">The platform ID of the device being upgraded. Retrieved from the system.platform setting.</param>
		/// <param name="aSerial">The serial number of the device being upgraded. Retrieved from the system.serial setting.</param>
		/// <param name="aProgressCallback">(Optional) callback for decode progress reporting.</param>
		/// <returns>The new (possibly encrypted) firmware image for the device.</returns>
		/// <exception cref="ArgumentException">The filename contains invalid characters.</exception>
		/// <exception cref="ArgumentNullException">The filename string is null.</exception>
		/// <exception cref="PathTooLongException">
		///     The filename is too long (more than 248 chararcters in the folder name
		///     or more than 260 in the file name).
		/// </exception>
		/// <exception cref="DirectoryNotFoundException">The directory part of the filename path does not exist.</exception>
		/// <exception cref="IOException">There was an error opening the file.</exception>
		/// <exception cref="UnauthorizedAccessException">
		///     The path does not refer to a file, or you do not have read permission for
		///     that file.
		/// </exception>
		/// <exception cref="FileNotFoundException">The file does not exist</exception>
		/// <exception cref="NotSupportedException">The path is in an invalid format.</exception>
		/// <exception cref="InvalidDataException">The file header is incorrect or the file is otherwise corrupted.</exception>
		/// <exception cref="EndOfStreamException">The unpacker attempted to read past the end of the input stream.</exception>
		/// <exception cref="FirmwareUnpackException">Decoding the data produced a message instead of a result.</exception>
		public static byte[] Unpack(string aFilename, uint aPlatform, uint aSerial,
									FirmwareUnpacker.ProgressReportCallback aProgressCallback = null)
		{
			using (var fs = File.Open(aFilename, FileMode.Open))
			{
				return Unpack(fs, aPlatform, aSerial, aProgressCallback);
			}
		}


		/// <summary>
		///     Extracts one piece of firmware from a data strea. This assumes the stream is a file and will
		///     check the validity of the header.
		/// </summary>
		/// <param name="aStream">Stream to rad from. Must already be positioned to the start of the firmware header.</param>
		/// <param name="aPlatform">The platform ID of the device being upgraded. Retrieved from the system.platform setting.</param>
		/// <param name="aSerial">The serial number of the device being upgraded. Retrieved from the system.serial setting.</param>
		/// <param name="aProgressCallback">(Optional) callback for decode progress reporting.</param>
		/// <returns>The new (possibly encrypted) firmware image for the device.</returns>
		/// <exception cref="ArgumentNullException">The stream is null.</exception>
		/// <exception cref="InvalidDataException">The file header is incorrect or the file is otherwise corrupted.</exception>
		/// <exception cref="EndOfStreamException">The unpacker attempted to read past the end of the input stream.</exception>
		/// <exception cref="FirmwareUnpackException">Decoding the data produced a message instead of a result.</exception>
		public static byte[] Unpack(Stream aStream, uint aPlatform, uint aSerial,
									FirmwareUnpacker.ProgressReportCallback aProgressCallback = null)
		{
			// Check header signature.
			for (var i = 0; i < 8; ++i)
			{
				var b = (byte) aStream.ReadByte();
				if (b != SIGNATURE[i])
				{
					throw new InvalidDataException("File lacks the ZABERFWU header signature.");
				}
			}

			// Check version number.
			var version = aStream.ReadByte();
			if (1 != version)
			{
				throw new InvalidDataException($"File version {version} is not supported.");
			}

			// Check file size.
			long len = aStream.ReadUInt32();
			if (len != aStream.Length)
			{
				throw new InvalidDataException($"File size is wrong; should be {len} bytes, is {aStream.Length}.");
			}

			return FirmwareUnpacker.Unpack(aStream, aPlatform, aSerial, aProgressCallback);
		}

		#endregion

		#region -- Data --

		private static readonly byte[] SIGNATURE = { 0x5A, 0x41, 0x42, 0x45, 0x52, 0x46, 0x57, 0x55 }; // "ZABERFWU"

		#endregion
	}
}

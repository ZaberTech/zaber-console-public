﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Script.Serialization;

namespace Zaber.FirmwareDownload
{
	/// <summary>
	///     Helper class for programs that wish to be able to download firmware updates for devices.
	///     Provides classes mirroring the varions Json query and response data structures, and
	///     serialization to and from Json strings.
	/// </summary>
	[CLSCompliant(false)]
	public static class FirmwareWebQueryBuilder
	{
		#region -- Public data --

		/// <summary>
		///     The firmware self-serve protocol version currently used by the client.
		/// </summary>
		public const int CURRENT_PROTOCOL_VERSION = 1;

		#endregion

		#region -- Data types for query serialization --

		/// <summary>
		///     Basic device identification information for a firmware download query.
		/// </summary>
		public class QueryDeviceIdentifier
		{
			#region -- Public Methods & Properties --

			/// <summary>
			///     The device's platform ID, returned by the "get system.platform" command.
			/// </summary>
			public uint PlatformID { get; set; }

			/// <summary>
			///     The device's serial number, returned by the "get system.serial" command.
			/// </summary>
			public uint SerialNumber { get; set; }

			/// <summary>
			///     The device's current device number in the chain. This is used to associate the
			///     response data with devices in list-versions query responses. It is not used
			///     in download requests.
			/// </summary>
			public byte DeviceNumber { get; set; }

			/// <summary>
			///     Indicated whether or not the device requires a keyed (encrypted) firmware
			///     image.
			/// </summary>
			public bool IsKeyed { get; set; }

			/// <summary>
			///     The device's device ID, returned by the "get deviceid" command.
			/// </summary>
			public uint DeviceID { get; set; }

			#endregion
		}


		/// <summary>
		///     Additional query data needed to request a download.
		/// </summary>
		public class QueryFirmwareVersion : QueryDeviceIdentifier
		{
			#region -- Public Methods & Properties --

			/// <summary>
			///     The requested firmware version, eg "6.18"
			/// </summary>
			public string FWVersion { get; set; }

			/// <summary>
			///     The requested build number.
			/// </summary>
			public uint BuildNumber { get; set; }

			#endregion
		}


		/// <summary>
		///     Base class for firmware web queries.
		/// </summary>
		public abstract class Query
		{
			#region -- Public Methods & Properties --

			/// <summary>
			///     Constructor for subclasses to use.
			/// </summary>
			/// <param name="aType">The type of query implemented by the subclass.</param>
			protected Query(QueryTypes aType)
			{
				QueryType = aType.ToString();
			}


			/// <summary>
			///     The type of query being made. This is populated automatically.
			/// </summary>
			public string QueryType { get; protected set; }

			/// <summary>
			///     Tells the server which version of the query protocol we're using.
			///     Currently only one version exists, but this will let future versions
			///     of the server identify old clients.
			/// </summary>
			public uint ProtocolVersion => CURRENT_PROTOCOL_VERSION;

			#endregion

			#region -- Data --

			/// <summary>
			///     Names of supported query types.
			/// </summary>
			protected enum QueryTypes
			{
				/// <summary>
				///     Request for information about the server.
				/// </summary>
				ServerInfo,

				/// <summary>
				///     Request for list of compatible firmware versions.
				/// </summary>
				ListVersions,

				/// <summary>
				///     Request for a firmware package download.
				/// </summary>
				Package,

				/// <summary>
				///     Report successful installation.
				/// </summary>
				ReportInstall
			}

			#endregion
		}


		/// <summary>
		///     Query data type for requesting server configuration info. Currently
		///     only used by tests but will be used by programs in future if the
		///     query formats change significantly.
		/// </summary>
		public class QueryServerInfo : Query
		{
			#region -- Public Methods & Properties --

			/// <summary>
			///     Default constructor - initializes a server query.
			/// </summary>
			public QueryServerInfo()
				: base(QueryTypes.ServerInfo)
			{
			}

			#endregion
		}


		/// <summary>
		///     Data type for a list-versions query.
		/// </summary>
		public class QueryListVersions : Query
		{
			#region -- Public Methods & Properties --

			/// <summary>
			///     Default constructor - initializes a list versions query.
			/// </summary>
			public QueryListVersions()
				: base(QueryTypes.ListVersions)
			{
				Devices = new List<QueryDeviceIdentifier>();
			}


			/// <summary>
			///     List of devices to find available versions for. Populate this with one or
			///     more <cref>QueryDeviceIdentifier</cref> instances.
			/// </summary>
			public IList<QueryDeviceIdentifier> Devices { get; set; }

			#endregion
		}


		/// <summary>
		///     Data type for a download request.
		/// </summary>
		public class QueryDownloadVersions : Query
		{
			#region -- Public Methods & Properties --

			/// <summary>
			///     Default constructor - initializes a download query.
			/// </summary>
			public QueryDownloadVersions()
				: base(QueryTypes.Package)
			{
				Versions = new List<QueryFirmwareVersion>();
			}


			/// <summary>
			///     List of firmware images to package into the downloaded file. Populate this
			///     with one or more <cref>QueryVersionRequest</cref> instances.
			/// </summary>
			public IList<QueryFirmwareVersion> Versions { get; set; }

			#endregion
		}


		/// <summary>
		///     Data type for an installation report.
		/// </summary>
		public class QueryReportInstall : Query
		{
			#region -- Public Methods & Properties --

			/// <summary>
			///     Default constructor - initializes an install report query.
			/// </summary>
			public QueryReportInstall()
				: base(QueryTypes.ReportInstall)
			{
				Devices = new List<QueryFirmwareVersion>();
			}


			/// <summary>
			///     List of firmware versions that have been installed on user's devices.
			/// </summary>
			public IList<QueryFirmwareVersion> Devices { get; set; }

			#endregion
		}


		/// <summary>
		///     Data type for an available firmware image.  These are returned in a
		///     <cref>Response</cref> after a list-versions query.
		/// </summary>
		public class ResponseFWVersion
		{
			#region -- Public Methods & Properties --

			/// <summary>
			///     Helper for displaying version info.
			/// </summary>
			/// <returns>A human-readable string naming the firmware version and build number.</returns>
			public override string ToString() => FWVersion;


			/// <summary>
			///     The firmware version, eg "6.18"
			/// </summary>
			public string FWVersion { get; set; }

			/// <summary>
			///     The release notes, if any were available.
			/// </summary>
			public string Notes { get; set; }

			/// <summary>
			///     The build number.
			/// </summary>
			public uint Build { get; set; }

			#endregion
		}


		/// <summary>
		///     Base type for query responses from the web server.
		/// </summary>
		public class Response
		{
			#region -- Public Methods & Properties --

			/// <summary>
			///     Indication of whether the last request succeeded or failed.
			///     All JSON responses should have this value set.
			/// </summary>
			public bool Success { get; set; }

			/// <summary>
			///     Human-readable message from the web server, if any.
			///     All responses with Success = false should have this set.
			/// </summary>
			public string Message { get; set; }

			#endregion
		}


		/// <summary>
		///     This is used for list-versions
		///     query responses and error responses, but not for actual firmware package downloads which
		///     come through as a binary stream.
		/// </summary>
		public class VersionListResponse : Response
		{
			#region -- Public Methods & Properties --

			/// <summary>
			///     Default constructor - initializes an empty list of versions.
			/// </summary>
			public VersionListResponse()
			{
				Versions = new Dictionary<string, List<ResponseFWVersion>>();
			}


			/// <summary>
			///     Helper to get the firmware versions for a particular device number.
			/// </summary>
			/// <param name="aDeviceNumber"></param>
			/// <returns></returns>
			public IEnumerable<ResponseFWVersion> GetVersions(byte aDeviceNumber)
			{
				List<ResponseFWVersion> result = null;
				Versions?.TryGetValue(aDeviceNumber.ToString(), out result);
				return result;
			}


			/// <summary>
			///     List of available firmware images matching the last list-versions query, if the
			///     last query was list-versions and there were no errors. Keys are the device numbers from
			///     the list-versions query. Unfortunately the key type has to be string instead of uint
			///     because of restrictions of the Json serializer.
			/// </summary>
			public IDictionary<string, List<ResponseFWVersion>> Versions { get; set; }

			#endregion
		}


		/// <summary>
		///     Response to the server settings query.
		/// </summary>
		public class ServerInfoResponse : Response
		{
			#region -- Public Methods & Properties --

			/// <summary>
			///     Client-server protocol version. Currently this must always be CURRENT_PROTOCOL_VERSION.
			/// </summary>
			[Obsolete]
			public int ProtocolVersion { get; set; }

			/// <summary>
			///     List of protocol versions supported by the server. If this is
			///     missing or empty, assume only version 1 is supported.
			/// </summary>
			public IList<int> SupportedProtocolVersions { get; set; }

			/// <summary>
			///     Is the server in debug mode?
			/// </summary>
			public bool Debug { get; set; }

			/// <summary>
			///     Is the server using a live database?
			/// </summary>
			public bool Live { get; set; }

			/// <summary>
			///     True if the server thinks you are not an internal Zaber user.
			/// </summary>
			public bool PublicRequest { get; set; }

			#endregion
		}

		#endregion

		#region -- Query and response helpers --

		/// <summary>
		///     Generate the JSON query string for a server info request.
		/// </summary>
		/// <returns>
		///     A JSON-encoded string containing the query data. The caller is responsible for encoding
		///     this string in a matter appropriate for communication with the firmware server.
		/// </returns>
		public static string MakeServerInfoQuery()
		{
			var query = new QueryServerInfo();
			var ser = new JavaScriptSerializer();
			return ser.Serialize(query);
		}


		/// <summary>
		///     Encode a list-versions request from data structure to URL-safe string.
		/// </summary>
		/// <param name="aDeviceList">A list of one or more fully populated <cref>QueryDeviceIdentifier</cref> instances.</param>
		/// <returns>
		///     A JSON-encoded string containing the query data. The caller is responsible for encoding
		///     this string in a matter appropriate for communication with the firmware server.
		/// </returns>
		public static string MakeListVersionsQuery(IEnumerable<QueryDeviceIdentifier> aDeviceList)
		{
			var query = new QueryListVersions();
			foreach (var device in aDeviceList)
			{
				query.Devices.Add(device);
			}

			var ser = new JavaScriptSerializer();
			return ser.Serialize(query);
		}


		/// <summary>
		///     Encode a firmware download request from data structure to URL-safe string.
		/// </summary>
		/// <param name="aDeviceList">
		///     A list of one or more populated <cref>QueryVersionRequest</cref> instances.
		///     The DeviceNumber field is ignored for this request.
		/// </param>
		/// <returns>
		///     A JSON-encoded string containing the query data. The caller is responsible for encoding
		///     this string in a matter appropriate for communication with the firmware server.
		/// </returns>
		public static string MakePackageRequest(IEnumerable<QueryFirmwareVersion> aDeviceList)
		{
			var query = new QueryDownloadVersions();
			foreach (var device in aDeviceList)
			{
				query.Versions.Add(device);
			}

			var ser = new JavaScriptSerializer();
			return ser.Serialize(query);
		}


		/// <summary>
		///     Generate a report about which firmware versions have been installed on which
		///     devices.
		/// </summary>
		/// <param name="aDeviceList">
		///     A list of one or more populated <cref>QueryFirmwareVersion</cref> instances.
		///     The DeviceNumber field is ignored for this request.
		/// </param>
		/// <returns>
		///     A JSON-encoded string containing the query data. The caller is responsible for encoding
		///     this string in a matter appropriate for communication with the firmware server.
		/// </returns>
		public static string MakeInstallationReport(IEnumerable<QueryFirmwareVersion> aDeviceList)
		{
			var query = new QueryReportInstall();
			foreach (var device in aDeviceList)
			{
				query.Devices.Add(device);
			}

			var ser = new JavaScriptSerializer();
			return ser.Serialize(query);
		}


		/// <summary>
		///     Convert a Json response string from the web server into a <cref>Response</cref> subclass, enabling
		///     easier programmatic retrieval of firmware version info or server-side error messages.
		///     Will throw exceptions if the string cannot be decoded to the specified type.
		/// </summary>
		/// <typeparam name="T">The type of response expected.</typeparam>
		/// <param name="aResponse">The body of a web response, as a stream.</param>
		/// <returns>A T instance populated with server response data.</returns>
		public static T DecodeResponse<T>(Stream aResponse)
		{
			var jsonText = "";
			using (var reader = new StreamReader(aResponse))
			{
				jsonText = reader.ReadToEnd();
			}

			return DecodeResponse<T>(jsonText);
		}


		/// <summary>
		///     Convert a Json response string from the web server into a <cref>Response</cref> subclass, enabling
		///     easier programmatic retrieval of firmware version info or server-side error messages.
		///     Will throw exceptions if the string cannot be decoded to the specified type.
		/// </summary>
		/// <typeparam name="T">The type of response expected.</typeparam>
		/// <param name="aResponse">The body of a web response, as a string.</param>
		/// <returns>A T instance populated with server response data.</returns>
		public static T DecodeResponse<T>(string aResponse)
		{
			var ser = new JavaScriptSerializer();
			var json = ser.Deserialize<T>(aResponse);

			return json;
		}

		#endregion
	}
}

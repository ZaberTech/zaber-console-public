﻿using System;
using System.IO;

namespace Zaber.FirmwareDownload
{
	/// <summary>
	///     Helper to read odd numbers of bits from a stream of bytes. Up to 64 bits can be
	///     read at a time. The most significant bit of the first byte of input becomes the first
	///     bit read, which becomes the most significant bit of whatever size is read. The least
	///     significant bit of the first input byte is the 8th bit read, and the MSB of the second
	///     byte is the 9th bit, and so on.
	///     If the end of the input is reached, the last remaining input bits will become the most
	///     significant bits of the last output, and the remainder of the output will be zeroed.
	/// </summary>
	[CLSCompliant(false)]
	public class BitStreamReader : IDisposable
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Initialize a new BitStreamReader to read from a given memory buffer.
		/// </summary>
		/// <param name="aBuffer">The boffer to read from.</param>
		/// <param name="aStartIndex">Index of the first byte to read in the buffer.</param>
		/// <param name="aLength">Number of bytes to read from the buffer.</param>
		/// <exception cref="ArgumentOutOfRangeException">
		///     The inputs describe a sequence of bytes
		///     outside the bounds of the buffer.
		/// </exception>
		/// <exception cref="ArgumentNullException">The input buffer is null.</exception>
		public BitStreamReader(byte[] aBuffer, int aStartIndex, int aLength)
		{
			if (null == aBuffer)
			{
				throw new ArgumentNullException("aBuffer is null.");
			}

			if ((aStartIndex < 0) || ((aStartIndex + aLength) > aBuffer.Length))
			{
				throw new ArgumentOutOfRangeException("aStartIndex and/or aLength are invalid for this buffer.");
			}

			_input = new MemoryStream(aBuffer, aStartIndex, aLength);
			_streamIsOwned = true;
		}


		/// <summary>
		///     Initialize a new BitStreamReader to read from an existing stream.
		/// </summary>
		/// <param name="aInput">The stream to read from.</param>
		/// <exception cref="ArgumentNullException">The stream is null.</exception>
		public BitStreamReader(Stream aInput)
		{
			if (null == aInput)
			{
				throw new ArgumentNullException("aInput is null.");
			}

			_input = aInput;
		}


		/// <summary>
		///     Dispose of resources held by this class instance, if any.
		/// </summary>
		public void Dispose()
		{
			if (_streamIsOwned && (null != _input))
			{
				_input.Dispose();
				_input = null;
			}
		}


		/// <summary>
		///     Read some bits from the input.
		/// </summary>
		/// <param name="aCount">Number of bits to read. Legal range is 1 to 64.</param>
		/// <returns>
		///     A 64-bit value containing the desired bits at the low end, with the first bit read
		///     as the most significant of those and the last bit read as the LSB. If the end of the input
		///     data is reached, further output is all zero bits.
		/// </returns>
		/// <exception cref="ArgumentOutOfRangeException">The count input has an illegal value.</exception>
		public ulong ReadBits(int aCount)
		{
			if ((aCount < 1) || (aCount > 64))
			{
				throw new ArgumentOutOfRangeException("Can only read between 1 and 64 bits at a time.");
			}

			var result = 0UL;

			var numOutputBitsLeft = aCount;

			while (numOutputBitsLeft > 0)
			{
				if (_currentBit <= 0)
				{
					_currentByte = 0;
					if (_input.Position < _input.Length)
					{
						_currentByte = (byte) _input.ReadByte();
					}

					_currentBit = 8;
				}

				var numBitsFromThisByte = Math.Min(_currentBit, numOutputBitsLeft);
				var bitsFromThisByte = (ulong) (_currentByte >> (_currentBit - numBitsFromThisByte));
				bitsFromThisByte &= (1UL << numBitsFromThisByte) - 1;
				result <<= numBitsFromThisByte;
				result |= bitsFromThisByte;
				numOutputBitsLeft -= numBitsFromThisByte;
				_currentBit -= numBitsFromThisByte;
			}

			return result;
		}

		#endregion

		#region -- Data --

		private Stream _input;
		private readonly bool _streamIsOwned;
		private byte _currentByte;
		private int _currentBit = -1;

		#endregion
	}
}

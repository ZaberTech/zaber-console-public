namespace Zaber
{
	/// <summary>
	///     Descriptive information about a read-only setting that Zaber devices
	///     support.
	/// </summary>
	/// <remarks>
	///     This class is specific to commands that return the value of a
	///     read-only device setting.
	/// </remarks>
	public class ReadOnlySettingInfo : SettingInfo
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Create a deep copy of the SettingInfo.
		/// </summary>
		/// <returns>A new instance of SettingInfo (as CommandInfo) with all the same property values as the instance invoked on.</returns>
		public override CommandInfo Clone()
		{
			var si = new ReadOnlySettingInfo();
			CopyProperties(this, si);
			return si;
		}


		/// <summary>
		///     Is this command actually a read-only setting?
		/// </summary>
		/// <remarks>
		///     Read-only settings are read differently from other settings. Just
		///     send the command and the value will be returned.
		/// </remarks>
		public override bool IsReadOnlySetting => true;

		#endregion
	}
}

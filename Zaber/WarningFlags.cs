﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace Zaber
{
	/// <summary>
	///     Container for ASCII warning flags definitions and supplementary data.
	/// </summary>
	public static class WarningFlags
	{
		/// <summary>
		///     No warnings are present.
		/// </summary>
		[Description("No warnings")]
		public const string None = "--";

		/// <summary>
		///     The device has experienced a critical system error.
		///     This should not occur during normal operation.
		/// </summary>
		[Description("Critical system error")]
		public const string CriticalSystemError = "FF";

		/// <summary>
		///     The controller has been given a peripheral ID
		///     that it does not understand.
		/// </summary>
		[Description("Peripheral not supported")]
		public const string PeripheralNotSupported = "FN";

		/// <summary>
		///     A peripheral is in the inactive state, due to
		///     being unplugged or replaced with a different
		///     peripheral that has not yet been configured.
		/// </summary>
		[Description("Peripheral inactive")]
		public const string PeripheralInactive = "FZ";

		/// <summary>
		///     The motor driver has been disabled because the
		///     hardware emergency stop was triggered.
		/// </summary>
		[Description("Driver disabled by hardware emergency stop")]
		public const string HardwareEmergencyStop = "FH";

		/// <summary>
		///     The device has disabled the driver in response to the driver voltage being
		///     outside the normal operating range. This can occur if a stall has occurred,
		///     if the motor is backdriven, if there is damage to the driver hardware, or if
		///     there is a problem with the power supply.
		/// </summary>
		[Description("Driver disabled due to overvoltage or undervoltage")]
		public const string OverOrUnderVoltage = "FV";

		/// <summary>
		///     The driver is disabled due to user command or device boot-up activities.
		/// </summary>
		[Description("Driver disabled")]
		public const string DriverDisabled = "FO";

		/// <summary>
		///     The driver is disabled due to the detection of high
		///     inrush current from the power supply. 
		/// </summary>
		[Description("Driver disabled due to current inrush")]
		public const string CurrentInrush = "FC";

		/// <summary>
		///     The driver is disabled due the motor temperature
		///     being too high.
		/// </summary>
		[Description("Driver disabled due to motor overheat")]
		public const string MotorTemperature = "FM";

		/// <summary>
		///     The device has disabled the driver due to an overheating or an over-current
		///     condition. In Firmware versions below 7.10, this was the only driver disabled
		///     warning flag and covered all causes.
		/// </summary>
		[Description("Driver disabled")]
		public const string DriverTemperatureOrCurrentError = "FD";

		/// <summary>
		///     The encoder-measured position may be unreliable. The encoder has encountered
		///     a read error due to poor sensor alignment, vibration, dirt or other
		///     environmental conditions.
		/// </summary>
		[Description("Encoder error")]
		public const string EncoderError = "FQ";

		/// <summary>
		///     The encoder has encountered a problem with the index mark and the home
		///     position may be unreliable. This could be due to poor sensor alignment,
		///     vibration, dirt, or other environmental conditions.
		/// </summary>
		[Description("Index error")]
		public const string IndexError = "FI";

		/// <summary>
		///     The encoder-measured position may be unreliable due to a problem interpolating
		///     the encoder signals. This could be due to poor sensor alignment, dirt, or
		///     other environmental conditions.
		/// </summary>
		[Description("Analog encoder sync error")]
		public const string AnalogEncoderSyncError = "FA";

		/// <summary>
		///     The axis detected a stall and has stopped; the requested movement was not
		///     completed.
		/// </summary>
		[Description("Stalled and stopped")]
		public const string StalledAndStopped = "FS";

		/// <summary>
		///     The device could not execute a previously sent stream movement because
		///     it failed a precondition (e.g. motion exceeds device bounds, calls nested
		///     too deeply).
		/// </summary>
		[Description("Stream bounds error")]
		public const string StreamBoundsError = "FB";

		/// <summary>
		///     The device has terminated streamed or sinusoidal motion because it slipped
		///     and thus deviated from the requested path.
		/// </summary>
		[Description("Interpolated path deviation")]
		public const string InterpolatedPathDeviation = "FP";

		/// <summary>
		///     Either the axis could not reach the target limit sensor or the sensor is faulty.
		/// </summary>
		[Description("Limit error")]
		public const string LimitError = "FE";

		/// <summary>
		///     The lockstep group has exceeded allowable twist and has stopped.
		/// </summary>
		[Description("Excessive twist")]
		public const string ExcessiveTwist = "FT";

		/// <summary>
		///     A movement operation did not complete due to a triggered limit sensor. This
		///     flag is set if a limit sensor interrupts a movement operation and the
		///     No Reference Position (WR) warning flag is not present. This may be an
		///     indication that the axis has slipped or one of limit.min and limit.max is
		///     incorrect.
		/// </summary>
		[Description("Unexpected limit trigger")]
		public const string UnexpectedLimitTrigger = "WL";

		/// <summary>
		///     The device is currently in bootloader mode, meaning an attempt to update
		///     the firmware was unsuccessful and must be repeated.
		/// </summary>
		[Description("Bootloader mode")]
		public const string BootloaderMode = "NB";

		/// <summary>
		///     The supply voltage is outside the recommended operating range of the device.
		///     Damage to the device could occur if not remedied.
		/// </summary>
		[Description("Voltage out of range")]
		public const string VoltageOutOfRange = "WV";

		/// <summary>
		///     The internal temperature of the controller or lamp has exceeded the recommended
		///     limit for the device.
		/// </summary>
		[Description("Temperature high")]
		public const string TemperatureHigh = "WT";

		/// <summary>
		///     The axis detected a stall, but has recovered or is recovering.
		/// </summary>
		[Description("Stalled with recovery")]
		public const string StalledWithRecovery = "WS";

		/// <summary>
		///     While not in motion, the axis has been forced out of its position.
		/// </summary>
		[Description("Displaced when stationary")]
		public const string DisplacedWhenStationary = "WM";

		/// <summary>
		///     An axis has been loaded with calibration data of a type it
		///     does not support.
		/// </summary>
		[Description("Invalid calibration type")]
		public const string InvalidCalibrationType = "WP";

		/// <summary>
		///     The axis does not have a reference position.
		/// </summary>
		[Description("No reference position")]
		public const string NoReferencePosition = "WR";

		/// <summary>
		///     The axis has a position reference, but has not been homed. As a result,
		///     calibration has been disabled. Home the axis to re-enable calibration.
		/// </summary>
		[Description("Device not homed")]
		public const string DeviceNotHomed = "WH";

		/// <summary>
		///     The axis is busy due to manual control via the knob.
		/// </summary>
		[Description("Manual control")]
		public const string ManualControl = "NC";

		/// <summary>
		///     The device prematurely ended a previous motion command or displacement
		///     recovery in order to execute a newly-received movement operation (either
		///     a command or manual control). This indicates that a movement command or
		///     displacement recovery did not complete. This flag will not occur if the
		///     movement that was prematurely ended was initiated using the manual knob.
		/// </summary>
		[Description("Movement interrupted")]
		public const string MovementInterrupted = "NI";

		/// <summary>
		///     The axis has slowed down while following a streamed motion path because
		///     it has run out of queued motions.
		/// </summary>
		[Description("Stream discontinuity")]
		public const string StreamDiscontinuity = "ND";

		/// <summary>
		///     One or more values in a command exceeded their specified levels of precision
		///     and were rounded.
		/// </summary>
		[Description("Value rounded")]
		public const string ValueRounded = "NR";

		/// <summary>
		///     A setting is pending to be updated or a reset is pending.
		/// </summary>
		[Description("Setting update pending")]
		public const string SettingUpdatePending = "NU";

		/// <summary>
		///     Joystick calibration is in progress. Moving the joystick will have no effect.
		/// </summary>
		[Description("Joystick calibrating")]
		public const string JoystickCalibrating = "NJ";


		/// <summary>
		///     Get the description of a single ASCII warning flag.
		/// </summary>
		/// <param name="aFlag">The flag to get a description of.</param>
		/// <returns>
		///     A short description of the flag's meaning, or null
		///     if not recogized.
		/// </returns>
		public static string GetWarningDescription(string aFlag)
		{
			aFlag = aFlag.Trim().ToUpperInvariant();
			if (_warningFlagDescriptions.TryGetValue(aFlag, out var desc))
			{
				return desc;
			}

			return null;
		}


		static WarningFlags()
		{
			_warningFlagDescriptions = new Dictionary<string, string>();
			foreach (var field in typeof(WarningFlags).GetFields(BindingFlags.Static | BindingFlags.Public))
			{
				if (field.IsLiteral && field.GetValue(null) is string val && !string.IsNullOrEmpty(val))
				{
					foreach (var attr in field.GetCustomAttributes(typeof(DescriptionAttribute), false))
					{
						if (attr is DescriptionAttribute desc)
						{
							if (_warningFlagDescriptions.ContainsKey(val))
							{
								throw new InvalidProgramException("Multiple definitions for warning flag " + val);
							}

							_warningFlagDescriptions[val] = desc.Description;
							break;
						}
					}
				}
			}
		}

		private static readonly IDictionary<string, string> _warningFlagDescriptions;
	}
}
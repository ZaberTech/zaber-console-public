﻿using System.ComponentModel;

namespace Zaber
{
	/// <summary>
	///     Used to provide textual descriptions of input data.
	/// </summary>
	public class ParameterDescriptionAttribute : DescriptionAttribute
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor. Does nothing.
		/// </summary>
		public ParameterDescriptionAttribute()
		{
		}


		/// <summary>
		///     Initializes the attribute with a description string.
		/// </summary>
		/// <param name="aDescription">The description for the associated parameter.</param>
		public ParameterDescriptionAttribute(string aDescription)
			: base(aDescription)
		{
		}

		#endregion
	}
}

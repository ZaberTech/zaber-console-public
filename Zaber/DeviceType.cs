﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Zaber.Units;

namespace Zaber
{
	/// <summary>
	///     Describes the type of a <see cref="ZaberDevice" />.
	/// </summary>
	/// <remarks>
	///     The device type determines the list of supported commands.
	///     A device's set of supported commands is determined by 3 things:
	///     device ID, peripheral ID, and firmware version.
	/// </remarks>
	public class DeviceType
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Initialize a new instance.
		/// </summary>
		public DeviceType()
		{
			Commands = new List<CommandInfo>();
			PeripheralMap = new Dictionary<int, DeviceType>();
			UnitConverter = new ConversionTable();
			DeviceTypeUnitConversionInfo = new Dictionary<Dimension, IParameterConversionInfo>();
		}


		/// <summary>
		///     Copy constructor.
		/// </summary>
		/// <param name="aSource">The DeviceType to copy details from.</param>
		/// <remarks>
		///     This copy constructor performs a deep copy of both the
		///     commands list and peripheral map from the source DeviceType. This
		///     means that changes made to one DeviceType's command list or
		///     peripherals map will not be applied to the other's.
		///     Exception: The peripheral map is not deep-copied, as this is
		///     potentially an expensive operation and it is supposed to be
		///     treated as read-only except at device detection time.
		/// </remarks>
		public DeviceType(DeviceType aSource)
		{
			var commandList = new List<CommandInfo>(aSource.Commands.Count);
			UnitConverter = aSource.UnitConverter;
			commandList.AddRange(aSource.Commands);
			Commands = commandList;
			DeviceId = aSource.DeviceId;
			FirmwareVersion = aSource.FirmwareVersion;
			RealFirmwareVersion = aSource.RealFirmwareVersion;
			MotionType = aSource.MotionType;
			Name = aSource.Name;
			PeripheralId = aSource.PeripheralId;
			PeripheralMap = aSource.PeripheralMap;
			Capabilities = aSource.Capabilities; // Note the property setter copies to a new collection.
			DeviceTypeUnitConversionInfo =
				new Dictionary<Dimension, IParameterConversionInfo>(aSource.DeviceTypeUnitConversionInfo);
			TuningData = (null != aSource.TuningData) ? new TuningData(aSource.TuningData) : null;
		}


		/// <summary>
		///     Determines whether this instance is equal to the specified object.
		/// </summary>
		/// <param name="aOther">The object to compare.</param>
		/// <returns>
		///     True if the object is a DeviceType whose properties are
		///     equal to those of this instance, false otherwise.
		/// </returns>
		public override bool Equals(object aOther)
		{
			if (ReferenceEquals(this, aOther))
			{
				return true;
			}

			var other = aOther as DeviceType;

			if (null == other)
			{
				return false;
			}

			if ((null == Name) || (null == other.Name))
			{
				if (Name != other.Name)
				{
					return false;
				}
			}

			if (Name != other.Name)
			{
				return false;
			}

			if ((null == Commands) || (null == other.Commands))
			{
				if (Commands != other.Commands)
				{
					return false;
				}
			}
			else if (!Commands.SequenceEqual(other.Commands))
			{
				return false;
			}

			if ((null == PeripheralMap) || (null == other.PeripheralMap))
			{
				if (PeripheralMap != other.PeripheralMap)
				{
					return false;
				}
			}
			else if (!PeripheralMap.SequenceEqual(other.PeripheralMap))
			{
				return false;
			}

			if ((TuningData?.GetHashCode() ?? 0) != (other.TuningData?.GetHashCode() ?? 0))
			{
				return false;
			}

			if (DeviceTypeUnitConversionInfo.Keys.Count != other.DeviceTypeUnitConversionInfo.Keys.Count)
			{
				return false;
			}

			if (!DeviceTypeUnitConversionInfo.Keys.All(k => other.DeviceTypeUnitConversionInfo.ContainsKey(k)
														&& Equals(DeviceTypeUnitConversionInfo[k],
																  other.DeviceTypeUnitConversionInfo[k])))
			{
				return false;
			}

			if (!Capabilities.OrderBy(name => name).SequenceEqual(other.Capabilities.OrderBy(name => name)))
			{
				return false;
			}

			return (DeviceId == other.DeviceId)
			   && (FirmwareVersion == other.FirmwareVersion)
			   && (RealFirmwareVersion == other.RealFirmwareVersion)
			   && (PeripheralId == other.PeripheralId)
			   && Equals(MotionType, other.MotionType);
		}


		/// <summary>
		///     Returns a calculated hash code for this object.
		/// </summary>
		/// <returns>An integer hash code.</returns>
		public override int GetHashCode()
		{
			var code = 17;

			if (null != Commands)
			{
				foreach (var cmd in Commands.OrderBy(cmd => cmd.TextCommand ?? cmd.Number.ToString()))
				{ 
					code = (31 * code) + cmd.GetHashCode();
				}
			}

			if (null != PeripheralMap)
			{
				foreach (var key in PeripheralMap.Keys.OrderBy(id => id))
				{
					code = (31 * code) + key.GetHashCode();
					code = (31 * code) + PeripheralMap[key].GetHashCode();
				}
			}

			code = (31 * code) + DeviceId;
			code = (31 * code) + FirmwareVersion.GetHashCode();
			code = (31 * code) + RealFirmwareVersion.GetHashCode();
			code = (31 * code) + PeripheralId;
			code = (31 * code) + MotionType.GetHashCode();

			if (null != Name)
			{
				code = (31 * code) + Name.GetHashCode();
			}

			if (null != TuningData)
			{
				code = (31 * code) + TuningData.GetHashCode();
			}

			if (null != DeviceTypeUnitConversionInfo)
			{
				foreach (var pair in DeviceTypeUnitConversionInfo.OrderBy(p => p.Key.Name))
				{
					code = (31 * code) + pair.Key.GetHashCode();
					code = (31 * code) + pair.Value.GetHashCode();
				}
			}

			foreach (var cap in (Capabilities ?? new string[0]).OrderBy(s => s))
			{
				code = (31 * code) + cap.GetHashCode();
			}

			return code;
		}


		/// <summary>
		///     Returns the <see cref="Name" /> or the <see cref="DeviceId" />.
		/// </summary>
		/// <returns>A descriptive string that can be displayed to a user.</returns>
		public override string ToString() => Name
										 ?? $"Zaber DeviceId {DeviceId}, Firmware Version {FirmwareVersion.Major}.{FirmwareVersion.Minor.ToString("00")}";


		/// <summary>
		///     Look up command details from the list of supported commands.
		/// </summary>
		/// <param name="aCommand">The command number to look up</param>
		/// <returns>
		///     The command details, or null if the command is not
		///     supported by this device type.
		/// </returns>
		public CommandInfo GetCommandByNumber(Command aCommand)
		{
			if ((null != _commandMap)
			&& _commandMap.TryGetValue(aCommand, out var commandInfo)
			&& ((0 == PeripheralId) || commandInfo.IsAxisCommand))
			{
				return commandInfo;
			}

			return null;
		}


		/// <summary>
		///     Look up command details from the list of supported commands.
		/// </summary>
		/// <param name="aText">The command name to look up.</param>
		/// <returns>
		///     The command details, or null if the command is not
		///     supported by this device type.
		/// </returns>
		public CommandInfo GetCommandByText(string aText)
		{
			var key = aText.Trim();
			if (key.StartsWith("get ", StringComparison.Ordinal) || key.StartsWith("set ", StringComparison.Ordinal))
			{
				// For settings, drop off the get/set word and anything after the setting name.
				var parts = key.Split(new [] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
				if (parts.Length > 1)
				{
					key = parts[1];
				}
			}

			if ((null != _textMap)
			&& _textMap.TryGetValue(key, out var commandInfo)
			&& ((0 == PeripheralId) || commandInfo.IsAxisCommand))
			{
				return commandInfo;
			}

			return null;
		}


		/// <summary>
		///     Look up peripheral type from the list of supported peripherals.
		/// </summary>
		/// <param name="aPeripheralId">The peripheral id to look up</param>
		/// <returns>A device type to use.</returns>
		public DeviceType GetPeripheralById(int aPeripheralId)
		{
			if ((null == PeripheralMap) || !PeripheralMap.ContainsKey(aPeripheralId))
			{
				return null;
			}

			return PeripheralMap[aPeripheralId];
		}


		/// <summary>
		///     Generates a device-specific unit conversion table for steps, native resolution-
		///     independent velocity and acceleration units, and
		///     device-specific units, with a fallback to the database-level table.
		/// </summary>
		public void UpdateHardwareUnits()
		{
			var newUnits = new HashSet<string>();

			var handler = new Action<UnitOfMeasure, decimal>((aBaseUnit, aScale) =>
			{
				string abbrev = null;
				string name = null;

				if ((Dimension.Length == aBaseUnit.Unit.Dimension) || (Dimension.Angle == aBaseUnit.Unit.Dimension))
				{
					abbrev = UnitOfMeasure.StepSymbol;
					name = "Steps";
				}
				else if ((Dimension.Velocity == aBaseUnit.Unit.Dimension)
					 || (Dimension.AngularVelocity == aBaseUnit.Unit.Dimension))
				{
					abbrev = UnitOfMeasure.DeviceStepVelocityUnitSymbol;
					name = "Unscaled Device Velocity Units";
				}
				else if ((Dimension.Acceleration == aBaseUnit.Unit.Dimension)
					 || (Dimension.AngularAcceleration == aBaseUnit.Unit.Dimension))
				{
					abbrev = UnitOfMeasure.DeviceStepAccelerationUnitSymbol;
					name = "Unscaled Device Acceleration Units";
				}

				if ((null != abbrev) && !newUnits.Contains(abbrev))
				{
					var newUnit = new ScaledShiftedUnit(abbrev, name, aBaseUnit.Unit, (double) (1m / aScale))
					{
						Dimension = aBaseUnit.Unit.Dimension,
						UseSIPrefixes = false
					};

					UnitConverter.RegisterUnit(newUnit);
					newUnits.Add(abbrev);
				}
			});

			foreach (var info in _commands)
			{
				if ((null != info.RequestUnit) && info.RequestUnitScale.HasValue)
				{
					handler(info.RequestUnit, info.RequestUnitScale.Value);
				}

				if ((null != info.ResponseUnit) && info.ResponseUnitScale.HasValue)
				{
					handler(info.ResponseUnit, info.ResponseUnitScale.Value);
				}

				if (info is ASCIICommandInfo asciiCommand)
				{
					foreach (var node in asciiCommand.Nodes)
					{
						if ((null != node.RequestUnit) && node.RequestUnitScale.HasValue)
						{
							handler(node.RequestUnit, node.RequestUnitScale.Value);
						}
					}
				}
			}
		}


		/// <summary>
		///     A custom name to display to the user.
		/// </summary>
		/// <seealso cref="ToString" />
		public string Name { get; set; }


		/// <summary>
		///     Gets or sets a list of the commands that this device type supports.
		/// </summary>
		public IList<CommandInfo> Commands
		{
			get => 0 == PeripheralId ? _commands : _commands.Where(c => c.IsAxisCommand).ToList();
			set
			{
				// Clear cached computed properties.
				_isJoystick = null;

				if (null == value)
				{
					_commands = null;
					return;
				}

				// use a read-only copy to avoid insertions or deletions
				_commands = new List<CommandInfo>(value).AsReadOnly();

				_commandMap = new Dictionary<Command, CommandInfo>();
				_textMap = new Dictionary<string, CommandInfo>();
				foreach (var command in _commands)
				{
					if (null == command.TextCommand)
					{
						if (_commandMap.ContainsKey(command.Command))
						{
							throw new ArgumentException(
								string.Format(CultureInfo.CurrentCulture,
											  "More than one entry found for command {0}.",
											  command.Command),
								nameof(value));
						}

						_commandMap.Add(command.Command, command);
					}
					else
					{
						_textMap.Add(command.TextCommand, command);
					}
				}

				UpdateHardwareUnits();
			}
		}


		/// <summary>
		///     Reflects the Firmware version used for database queries about this device, for
		///     the deprecation case where the last firmware version in the database is used
		///     instead of the real version.
		/// </summary>
		/// <value>The firmware version.</value>
		public FirmwareVersion FirmwareVersion { get; set; }


		/// <summary>
		///     Reflects the device's actual Firmware version when different from the effective version.
		/// </summary>
		/// <value>The firmware version.</value>
		public FirmwareVersion RealFirmwareVersion { get; set; }


		/// <summary>
		///     A dictionary of the peripherals that this device type supports,
		///     indexed by peripheral ID.
		/// </summary>
		public IDictionary<int, DeviceType> PeripheralMap { get; set; }


		/// <summary>
		///     A list of the peripherals that this device type supports.
		/// </summary>
		/// <remarks>
		///     This property returns the values of the <see cref="PeripheralMap" />
		///     dictionary.
		///     Setting this property will set <see cref="PeripheralMap" /> to match
		///     the value specified.
		/// </remarks>
		[Obsolete("Use PeripheralMap.")]
		public IList<DeviceType> Peripherals
		{
			get => PeripheralMap.Values.ToList();
			set
			{
				if (null == value)
				{
					PeripheralMap = null;
					return;
				}

				PeripheralMap = new Dictionary<int, DeviceType>();
				foreach (var peripheralType in value.Where(peripheralType =>

															   // Skip duplicates.
															   !PeripheralMap.ContainsKey(peripheralType.PeripheralId)))
				{
					PeripheralMap.Add(peripheralType.PeripheralId, peripheralType);
				}
			}
		}


		/// <summary>
		///     The identifier for this device type.
		/// </summary>
		public int DeviceId { get; set; }


		/// <summary>
		///     The identifier for a specific peripheral if this device type is
		///     customized for a peripheral.
		/// </summary>
		public int PeripheralId { get; set; }


		/// <summary>
		///     Gets or sets whether this device type uses linear, rotary, or some
		///     other type of motion.
		/// </summary>
		public MotionType MotionType { get; set; }


		/// <summary>
		///     Unit conversion table that can convert between standard units
		///     and units specific to this device.
		/// </summary>
		public ConversionTable UnitConverter { get; set; }


		// TODO (Soleil 2020-02): Remove the remark below when the DB is populated.
		/// <summary>
		///     Capabilities (features) of this device type. Each string has a 
		///     specific meaning - see <see cref="Capability"/> for some definitions.
		/// </summary>
		/// <remarks>This data is not yet populated for devices with firmware older
		/// than 7.0.</remarks>
		public ICollection<string> Capabilities
		{
			get => _capabilities;
			set
			{
				_capabilities = new HashSet<string>(value);
			}
		}


		/// <summary>
		///     Returns true if this device type is a joystick.
		///     Result can be a false negative if this is called before the command
		///     list is initialized.
		/// </summary>
		/// <remarks>
		///     This property is only reliable in the context of Zaber Console, where the Device Database
		///     is used to populate the device type's command list. The property will not return correct
		///     information in the ScriptRunner context where that data is not available.
		/// </remarks>
		public bool IsJoystick
		{
			get
			{
				if (!_isJoystick.HasValue)
				{
					var hasJoystickAxes = Commands?.Where(cmd => (int) Command.SetAxisDeviceNumber == cmd.Number).Any();
					var hasButtons = Commands?.Where(cmd => (int) Command.LoadEventInstruction == cmd.Number).Any();
					_isJoystick = (hasJoystickAxes.HasValue && hasJoystickAxes.Value)
							  || (hasButtons.HasValue && hasButtons.Value);
				}

				return _isJoystick.Value;
			}
		}


		/// <summary>
		///     Returns true if this device type is a rotary stage.
		///     The determination is made by checking the motion type on its movement commands.
		///     Result can be a false negative if this is called before the command
		///     list is initialized.
		/// </summary>
		/// <remarks>
		///     This property is only reliable in the context of Zaber Console, where the Device Database
		///     is used to populate the device type's command list. The property will not return correct
		///     information in the ScriptRunner context where that data is not available.
		/// </remarks>
		public bool IsRotaryDevice => DeviceTypeUnitConversionInfo.ContainsKey(Dimension.Angle);


		/// <summary>
		///     Returns true if this device type has a home command, which implies it
		///     is capable of some kind of movement.
		///     This will give a false negative if the device has not been identified.
		/// </summary>
		public bool IsMotionCapable
			=> Commands.Any(c => ("home" == c.TextCommand) || (Command.Home == c.Command));


		/// <summary>
		///     Guidance data for control loop tuning software, such as the Servo Tuner plugin.
		///     This data is not available for all device types and its content is subject to
		///     change without notice; it should not be used in scripts.
		/// </summary>
		public TuningData TuningData { get; set; }


		/// <summary>
		///     Lookup table to find device-specific unit conversion info for different quantities.
		/// </summary>
		public IDictionary<Dimension, IParameterConversionInfo> DeviceTypeUnitConversionInfo { get; }

		#endregion

		#region -- Data --

		private IList<CommandInfo> _commands;
		private IDictionary<Command, CommandInfo> _commandMap;
		private IDictionary<string, CommandInfo> _textMap;
		private HashSet<string> _capabilities = new HashSet<string>();
		private bool? _isJoystick;

		#endregion
	}
}

﻿using System;

namespace Zaber
{
	/// <summary>
	///     Helper class to assist with tracking individual devices across sessions.
	///     Provides a confidence level that a given device is the same physical unit as in a previous session.
	/// </summary>
	[Serializable]
	[CLSCompliant(false)]
	public class DeviceIdentity
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Mask off parts of this device identity.
		/// </summary>
		/// <param name="aMask">Bitfield controlling which parts of the identity to preserve.</param>
		/// <returns>A new DeviceIdentity instance with fields defaulted where corresponding mask bits are zero.</returns>
		public DeviceIdentity Mask(DeviceIdentityMask aMask)
		{
			var result = new DeviceIdentity();

			if ((aMask & DeviceIdentityMask.IsAllDevices) != DeviceIdentityMask.None)
			{
				result.IsAllDevices = IsAllDevices;
			}

			if ((aMask & DeviceIdentityMask.SerialNumber) != DeviceIdentityMask.None)
			{
				result.DeviceSerialNumber = DeviceSerialNumber;
			}

			if ((aMask & DeviceIdentityMask.DeviceId) != DeviceIdentityMask.None)
			{
				result.DeviceId = DeviceId;
			}

			if ((aMask & DeviceIdentityMask.PeripheralId) != DeviceIdentityMask.None)
			{
				result.PeripheralId = PeripheralId;
			}

			if ((aMask & DeviceIdentityMask.DeviceNumber) != DeviceIdentityMask.None)
			{
				result.DeviceNumber = DeviceNumber;
			}

			if ((aMask & DeviceIdentityMask.AxisNumber) != DeviceIdentityMask.None)
			{
				result.AxisNumber = AxisNumber;
			}

			return result;
		}


		/// <summary>
		///     Determine whether this device identity matches another, with an optional bit field
		///     controlling which properties to include in the comparison.
		/// </summary>
		/// <param name="aOther">Other identity to compare to.</param>
		/// <param name="aMask">Bitfield controlling which parts of the identity to match. Defaults to all.</param>
		/// <returns>True if the two identities match on all fields mentioned in the mask.</returns>
		public bool Matches(DeviceIdentity aOther, DeviceIdentityMask aMask = DeviceIdentityMask.All)
		{
			if ((aMask & DeviceIdentityMask.IsAllDevices) != DeviceIdentityMask.None)
			{
				if (IsAllDevices != aOther.IsAllDevices)
				{
					return false;
				}
			}

			if ((aMask & DeviceIdentityMask.SerialNumber) != DeviceIdentityMask.None)
			{
				if (DeviceSerialNumber != aOther.DeviceSerialNumber)
				{
					return false;
				}
			}

			if ((aMask & DeviceIdentityMask.DeviceId) != DeviceIdentityMask.None)
			{
				if (DeviceId != aOther.DeviceId)
				{
					return false;
				}
			}

			if ((aMask & DeviceIdentityMask.PeripheralId) != DeviceIdentityMask.None)
			{
				if (PeripheralId != aOther.PeripheralId)
				{
					return false;
				}
			}

			if ((aMask & DeviceIdentityMask.DeviceNumber) != DeviceIdentityMask.None)
			{
				if (DeviceNumber != aOther.DeviceNumber)
				{
					return false;
				}
			}

			if ((aMask & DeviceIdentityMask.AxisNumber) != DeviceIdentityMask.None)
			{
				if (AxisNumber != aOther.AxisNumber)
				{
					return false;
				}
			}

			return true;
		}


		/// <summary>
		///     Overridden hash code to test generate same hash for same data content.
		/// </summary>
		/// <returns>A hash of the data comprising this device's identity.</returns>
		public override int GetHashCode()
		{
			unchecked
			{
				var hash = 17;
				hash = (hash * 31) + DeviceSerialNumber.GetHashCode();
				hash = (hash * 31) + DeviceNumber.GetHashCode();
				hash = (hash * 31) + AxisNumber.GetHashCode();
				hash = (hash * 31) + DeviceId.GetHashCode();
				hash = (hash * 31) + PeripheralId.GetHashCode();
				hash = (hash * 31) + IsAllDevices.GetHashCode();
				return hash;
			}
		}


		/// <summary>
		///     Overridden equals test to compare the data comrising two device identities.
		/// </summary>
		/// <param name="aOther">Another device identity to compare.</param>
		/// <returns>True if the other identity has the same identifying data.</returns>
		public override bool Equals(object aOther)
		{
			var other = aOther as DeviceIdentity;
			if (null == other)
			{
				return false;
			}

			return GetHashCode() == other.GetHashCode();
		}


		/// <summary>
		///     Overridden string conversion to aid with debugging.
		/// </summary>
		/// <returns>A human-readable indication of the device identity.</returns>
		public override string ToString()
		{
			if (IsAllDevices)
			{
				return "All Devices";
			}

			if (DeviceSerialNumber.HasValue)
			{
				return "Serial #" + DeviceSerialNumber.Value;
			}

			if (0 == AxisNumber)
			{
				return $"device {DeviceNumber} ({DeviceId})";
			}

			var peripheral = string.Empty;
			if (PeripheralId.HasValue)
			{
				peripheral = $" ({PeripheralId.Value.ToString()})";
			}

			return $"device {DeviceNumber} ({DeviceId}) axis {AxisNumber}{peripheral}";
		}


		/// <summary>
		///     Device serial number, if it has one.
		/// </summary>
		public uint? DeviceSerialNumber { get; set; }


		/// <summary>
		///     Device number in the serial chain.
		/// </summary>
		public int DeviceNumber { get; set; }


		/// <summary>
		///     Axis number if a peripheral attached to a multi-axis controller.
		/// </summary>
		public int AxisNumber { get; set; }


		/// <summary>
		///     Device type ID.
		/// </summary>
		public uint DeviceId { get; set; }


		/// <summary>
		///     Peripheral type ID, if known.
		/// </summary>
		public uint? PeripheralId { get; set; }


		/// <summary>
		///     True if this device is the "All Devices" group. This group should match itself
		///     more strongly than anything else.
		/// </summary>
		public bool IsAllDevices { get; set; }

		#endregion
	}
}

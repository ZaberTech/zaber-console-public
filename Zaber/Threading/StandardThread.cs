﻿using System.Threading;

namespace Zaber.Threading
{
	/// <summary>
	///     Default thread implementation for Zaber Console - delegates
	///     to a regular system thread.
	/// </summary>
	public class StandardThread : IThread
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Initialize the thread with a non-parameterized entry point.
		/// </summary>
		/// <param name="aEntryPoint">Function to be run by the thread.</param>
		public StandardThread(ThreadStart aEntryPoint)
		{
			_thread = new Thread(aEntryPoint);
		}

		#endregion

		#region -- IThread --

		/// <inheritdoc cref="Thread.IsAlive" />
		public bool IsAlive => _thread.IsAlive;

		/// <inheritdoc cref="Thread.Name" />
		public string Name
		{
			get => _thread.Name;
			set => _thread.Name = value;
		}


		/// <inheritdoc cref="Thread.Start()" />
		public void Start() => _thread.Start();


		/// <inheritdoc cref="Thread.Abort()" />
		public void Abort() => _thread.Abort();


		/// <inheritdoc cref="Thread.Join()" />
		public void Join() => _thread.Join();

		#endregion

		#region -- Data --

		private readonly Thread _thread;

		#endregion
	}
}

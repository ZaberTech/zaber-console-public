﻿using System;
using System.Threading;

namespace Zaber.Threading
{
	/// <summary>
	///    A token representing an increment to a counter. This can be used
	///    for example when multiple systems want to enable or disable a shared
	///    subsystem without tripping over each other.
	///
	///    You must use the <see cref="CounterTokenSource"/> class to obtain an
	///    instance, and doing so increments a counter in the source. Disposing
	///    the token instance decrements the counter, and the owner of the source
	///    received events when the count either becomes zero or stops being zero.
	/// </summary>
	public class CounterToken : IDisposable
	{
		internal CounterToken(Action aEnterAction, Action aLeaveAction)
		{
			aEnterAction();
			_leaveAction = aLeaveAction;
		}


		/// <summary>
		///     Decrement the count of token instances from the original source.
		/// </summary>
		public void Dispose()
		{
			var action = Interlocked.Exchange(ref _leaveAction, null);
			action?.Invoke();
		}


		private Action _leaveAction;
	}
}

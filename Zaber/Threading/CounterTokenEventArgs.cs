﻿using System;

namespace Zaber.Threading
{
	/// <summary>
	///     Event arguments for the <see cref="CounterTokenSource.ZeroStateChanged"/> event.
	/// </summary>
	public class CounterTokenEventArgs : EventArgs
	{
		/// <summary>
		///     Indicates whether the new counter value is zero.
		/// </summary>
		public bool IsZero { get; set; }
	}
}

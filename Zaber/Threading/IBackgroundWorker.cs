﻿using System.ComponentModel;

namespace Zaber.Threading
{
	/// <summary>
	///     Interface for background worker implementations. This exists to enable better
	///     test control over multithreaded code.
	/// </summary>
	public interface IBackgroundWorker
	{
		#region -- Events --

		/// <summary>
		///     Event handler for doing the background work. This will be invoked on
		///     the thread that performs the work.
		/// </summary>
		event DoWorkEventHandler DoWork;

		/// <summary>
		///     Invoked on the foreground thread when progress reports are made, if
		///     progress reporting is supported.
		/// </summary>
		event ProgressChangedEventHandler ProgressChanged;

		/// <summary>
		///     Invoked on the foreground thread when the worker has completed.
		/// </summary>
		event RunWorkerCompletedEventHandler RunWorkerCompleted;

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Request cancellation of background work.
		/// </summary>
		void CancelAsync();


		/// <summary>
		///     Call this from your <see cref="DoWork" /> handler to report progress
		///     to the foreground thread.
		/// </summary>
		/// <param name="aPercentage">Percentage of work completed, from 0 to 100.</param>
		/// <param name="aStateInfo">Additional feedback on the work being done.</param>
		void ReportProgress(int aPercentage, object aStateInfo = null);


		/// <summary>
		///     Initiate the execution of the background work.
		/// </summary>
		/// <param name="aArg">Optional argument to pass to the <see cref="DoWork" /> handler.</param>
		void Run(object aArg = null);


		/// <summary>
		///     Determine if a work is being cancelled.
		/// </summary>
		bool CancellationPending { get; }

		/// <summary>
		///     Determine if background work is currently in progress.
		/// </summary>
		bool IsBusy { get; }

		/// <summary>
		///     Controls whether the worker makes progress reports.
		/// </summary>
		bool WorkerReportsProgress { get; set; }

		/// <summary>
		///     Controls whether or not the worker can be cancelled.
		/// </summary>
		bool WorkerSupportsCancellation { get; set; }

		#endregion
	}
}

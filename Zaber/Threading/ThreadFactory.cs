﻿using System;
using System.Threading;

namespace Zaber.Threading
{
	/// <summary>
	///     Helper to create threads of types appropriate to the context.
	///     produces normal system threads in a production environment, or
	///     testing-specific implmentations in test environments.
	/// </summary>
	public static class ThreadFactory
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Create a thread with a non-parameterized entry point.
		/// </summary>
		/// <param name="aEntryPoint">Function to be run by the thread.</param>
		public static IThread CreateThread(ThreadStart aEntryPoint)
			=> Activator.CreateInstance(_threadType, aEntryPoint) as IThread;


		/// <summary>
		///     Set the type of thread implementation returned by the
		///     <see cref="CreateThread(ThreadStart)" /> method.
		///     The specified type must implement <see cref="IThread" /> and
		///     must have a constructor taking a ThreadStart.
		/// </summary>
		public static Type ThreadType
		{
			get => _threadType;
			set
			{
				if (null == value)
				{
					throw new ArgumentNullException(nameof(value));
				}

				if (value.IsAbstract)
				{
					throw new ArgumentException(value.Name + " is an abstract type.");
				}

				if (!typeof(IThread).IsAssignableFrom(value))
				{
					throw new ArgumentException(value.Name + " does not implement IThread.");
				}

				_threadType = value;
			}
		}

		#endregion

		#region -- Data --

		private static Type _threadType = typeof(StandardThread);

		#endregion
	}
}

﻿using System;

namespace Zaber.Threading
{
	/// <summary>
	///     Helper class for instantiating background workers. Allows selection of the type
	///     of implementation to create. Defaults to <see cref="StandardWorker" />.
	/// </summary>
	public static class BackgroundWorkerManager
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Instantiate a new background worker object.
		/// </summary>
		/// <returns>
		///     A new instance of a background worker. The specific type
		///     returned is set by <see cref="WorkerType" />.
		/// </returns>
		public static IBackgroundWorker CreateWorker()
			=> Activator.CreateInstance(_workerType, true) as IBackgroundWorker;


		/// <summary>
		///     Get or set the type of worker implementation created by
		///     <see cref="CreateWorker" />. The type must be a subclass of
		///     <see cref="IBackgroundWorker" />.
		/// </summary>
		public static Type WorkerType
		{
			get => _workerType;
			set
			{
				if (null == value)
				{
					throw new ArgumentNullException(nameof(value));
				}

				if (value.IsAbstract)
				{
					throw new ArgumentException(value.Name + " is an abstract type.");
				}

				if (!typeof(IBackgroundWorker).IsAssignableFrom(value))
				{
					throw new ArgumentException(value.Name + " does not implement IBackgroundWorker.");
				}

				_workerType = value;
			}
		}

		#endregion

		#region -- Data --

		private static Type _workerType = typeof(StandardWorker);

		#endregion
	}
}

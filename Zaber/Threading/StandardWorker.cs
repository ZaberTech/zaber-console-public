﻿using System;
using System.ComponentModel;

namespace Zaber.Threading
{
	/// <summary>
	///     Default implementation of IBackgroundWorker; hands off everything to the
	///     .NET system BackgroundWorker class.
	/// </summary>
	public sealed class StandardWorker : IBackgroundWorker, IDisposable
	{
		#region -- Public Methods & Properties --

		#region -- Initialization --

		/// <summary>
		///     Constructor; for internal use only. To create a worker, use
		///     <see cref="BackgroundWorkerManager.CreateWorker()" />.
		/// </summary>
		internal StandardWorker()
		{
			_worker = new BackgroundWorker();
			_worker.DoWork += _worker_DoWork;
			_worker.ProgressChanged += _worker_ProgressChanged;
			_worker.RunWorkerCompleted += _worker_RunWorkerCompleted;
		}

		#endregion

		#region -- IDispoable implementation --

		/// <summary>
		///     Releases all system resources held by the worker.
		/// </summary>
		public void Dispose()
		{
			if (null != _worker)
			{
				_worker.Dispose();
			}
		}

		#endregion

		#endregion

		#region -- IBackgroundWorker implementation --

		/// <summary>
		///     Determine if a work is being cancelled.
		/// </summary>
		public bool CancellationPending => _worker.CancellationPending;


		/// <summary>
		///     Determine if background work is currently in progress.
		/// </summary>
		public bool IsBusy => _worker.IsBusy;


		/// <summary>
		///     Controls whether the worker makes progress reports.
		/// </summary>
		public bool WorkerReportsProgress
		{
			get => _worker.WorkerReportsProgress;
			set => _worker.WorkerReportsProgress = value;
		}


		/// <summary>
		///     Controls whether or not the worker can be cancelled.
		/// </summary>
		public bool WorkerSupportsCancellation
		{
			get => _worker.WorkerSupportsCancellation;
			set => _worker.WorkerSupportsCancellation = value;
		}


		/// <summary>
		///     Event handler for doing the background work. This will be invoked on
		///     the thread that performs the work.
		/// </summary>
		public event DoWorkEventHandler DoWork;


		/// <summary>
		///     Invoked on the foreground thread when progress reports are made, if
		///     progress reporting is supported.
		/// </summary>
		public event ProgressChangedEventHandler ProgressChanged;


		/// <summary>
		///     Invoked on the foreground thread when the worker has completed.
		/// </summary>
		public event RunWorkerCompletedEventHandler RunWorkerCompleted;


		/// <summary>
		///     Request cancellation of background work.
		/// </summary>
		public void CancelAsync() => _worker.CancelAsync();


		/// <summary>
		///     Call this from your <see cref="DoWork" /> handler to report progress
		///     to the foreground thread.
		/// </summary>
		/// <param name="aPercentage">Percentage of work completed, from 0 to 100.</param>
		/// <param name="aStateInfo">Additional feedback on the work being done.</param>
		public void ReportProgress(int aPercentage, object aStateInfo = null)
			=> _worker.ReportProgress(aPercentage, aStateInfo);


		/// <summary>
		///     Initiate the execution of the background work.
		/// </summary>
		/// <param name="aArg">Optional argument to pass to the <see cref="DoWork" /> handler.</param>
		public void Run(object aArg = null) => _worker.RunWorkerAsync(aArg);

		#endregion

		#region -- Private parts --

		private void _worker_DoWork(object aSender, DoWorkEventArgs aArgs) => DoWork?.Invoke(aSender, aArgs);


		private void _worker_ProgressChanged(object aSender, ProgressChangedEventArgs aArgs)
			=> ProgressChanged?.Invoke(aSender, aArgs);


		private void _worker_RunWorkerCompleted(object aSender, RunWorkerCompletedEventArgs aArgs)
			=> RunWorkerCompleted?.Invoke(aSender, aArgs);


		private readonly BackgroundWorker _worker;

		#endregion
	}
}

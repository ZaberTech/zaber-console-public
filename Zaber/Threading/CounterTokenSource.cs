﻿using System;

namespace Zaber.Threading
{
	/// <summary>
	///    Factory for <see cref="CounterToken"/> instances. This class stores a counter
	///    variable that is shared by all tokens it produces.
	/// </summary>
	public class CounterTokenSource
	{
		/// <summary>
		///     Event fired when the counter either becomes zero or changes from zero
		///     to one.
		/// </summary>
		public event EventHandler<CounterTokenEventArgs> ZeroStateChanged;


		/// <summary>
		///     Obtain a new token referencing this count. Causes
		///     the count to increment by 1, and disposing the returned
		///     token will decrement it by 1.
		/// </summary>
		/// <returns>A new instance of <see cref="CounterToken"/> that affects
		/// the count in this <see cref="CounterTokenSource"/> instance.</returns>
		public IDisposable GetToken() 
			=> new CounterToken(Increment, Decrement);


		private void Increment()
		{
			lock (_syncRoot)
			{
				_count++;
				if (_count == 1)
				{
					// Callback happens inside the lock to ensure they
					// happen in chronological order for one source.
					ZeroStateChanged?.Invoke(this, new CounterTokenEventArgs { IsZero = false });
				}
			}
		}


		private void Decrement()
		{
			lock (_syncRoot)
			{
				_count--;
				if (_count == 0)
				{
					// Callback happens inside the lock to ensure they
					// happen in chronological order for one source.
					ZeroStateChanged?.Invoke(this, new CounterTokenEventArgs { IsZero = true });
				}
				else if (_count < 0)
				{
					throw new InvalidProgramException("Over-free of CounterTokens!");
				}
			}
		}


		private readonly object _syncRoot = new object();
		private long _count;
	}
}

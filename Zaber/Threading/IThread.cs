﻿using System.Threading;

namespace Zaber.Threading
{
	/// <summary>
	///     Interface definition for threads as used by Zaber Console. Allows
	///     replacement with synchronous implementations for tests.
	/// </summary>
	public interface IThread
	{
		#region -- Public Methods & Properties --

		/// <inheritdoc cref="Thread.Start()" />
		void Start();


		/// <inheritdoc cref="Thread.Abort()" />
		void Abort();


		/// <inheritdoc cref="Thread.Join()" />
		void Join();


		/// <inheritdoc cref="Thread.IsAlive" />
		bool IsAlive { get; }

		/// <inheritdoc cref="Thread.Name" />
		string Name { get; set; }

		#endregion
	}
}

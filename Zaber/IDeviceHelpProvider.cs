﻿namespace Zaber
{
	/// <summary>
	///     Interface to be implemented by applications that can provide command and setting
	///     help information for devices. Such applications should attach an instance to
	///     <see cref="ZaberPortFacade.DeviceHelpProvider" /> before detecting devices.
	/// </summary>
	public interface IDeviceHelpProvider
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Get an instance of a device-specific help information source.
		/// </summary>
		/// <param name="aDevice">The specific device to provide help for.</param>
		/// <param name="aAsciiMode">True if the ASCII protocol is currently in use.</param>
		/// <returns>
		///     A new or existing help source, or NULL if there is nothing appropriate
		///     available.
		/// </returns>
		IDeviceHelp GetHelpForDevice(ZaberDevice aDevice, bool aAsciiMode);

		#endregion
	}
}

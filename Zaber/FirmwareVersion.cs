﻿using System.Globalization;

namespace Zaber
{
	/// <summary>
	///     A struct to represent the firmware version present on a device.
	/// </summary>
	/// <remarks>
	///     Previously, firmware version was stored as a 3-digit integer,
	///     with the first digit (the "100's digit") representing the major version,
	///     and the following two digits representing the minor version. This
	///     matched the way Zaber's firmware reported its firmware version. For this
	///     reason, operator overloads are provided for comparing this struct to
	///     integers. Note that when comparing to integers, build number is ignored.
	/// </remarks>
	public struct FirmwareVersion
	{
		/// <summary>
		///     The major version of the firmware.
		/// </summary>
		public readonly int Major;

		/// <summary>
		///     The minor version of the firmware.
		/// </summary>
		public readonly int Minor;

		/// <summary>
		///     The build number of the firmware, if applicable.
		/// </summary>
		public readonly int Build;


		/// <summary>
		///     Create a new FirmwareVersion from a 3-digit integer,
		///     as provided by firmware.
		/// </summary>
		/// <param name="aVersion">
		///     The version number, as a single integer
		///     like "617" for version 6.17.
		/// </param>
		/// <remarks>
		///     The build number will be set to 0, indicating that no
		///     build number is present.
		/// </remarks>
		public FirmwareVersion(int aVersion)
		{
			Major = aVersion / 100;
			Minor = aVersion % 100;
			Build = 0;
		}


		/// <summary>
		///     Create a new FirmwareVersion with no build number.
		/// </summary>
		/// <param name="aMajor">The major version.</param>
		/// <param name="aMinor">The minor version.</param>
		/// <remarks>
		///     The build number will be set to 0, indicating that no
		///     build number is present.
		/// </remarks>
		public FirmwareVersion(int aMajor, int aMinor)
		{
			Major = aMajor;
			Minor = aMinor;
			Build = 0;
		}


		/// <summary>
		///     Create a new FirmwareVersion, specifying a full build number.
		/// </summary>
		/// <param name="aMajor">The major version.</param>
		/// <param name="aMinor">The minor version.</param>
		/// <param name="aBuild">The build number.</param>
		public FirmwareVersion(int aMajor, int aMinor, int aBuild)
		{
			Major = aMajor;
			Minor = aMinor;
			Build = aBuild;
		}


		/// <summary>
		///     Whether this object is equal to another one.
		/// </summary>
		/// <param name="aOther">The object to compare this object to.</param>
		/// <returns>True if the objects are equal, false otherwise.</returns>
		/// <remarks>
		///     Unlike the == operator, this method cannot be used to
		///     compare FirmwareVersion objects with integers. A comparison with an
		///     integer will always return false.
		/// </remarks>
		public override bool Equals(object aOther) => aOther is FirmwareVersion && (this == (FirmwareVersion) aOther);


		/// <summary>
		///     Gets a hash code for this object.
		/// </summary>
		/// <returns>A unique hash code for the object.</returns>
		public override int GetHashCode() => Major ^ Minor ^ Build;


		/// <summary>
		///     Returns the firmware version, in the same format as given by a
		///     Zaber device.
		/// </summary>
		/// <returns>
		///     A 3-digit integer, where the first digit represents the
		///     major version number, and the following two represent the minor
		///     version number.
		/// </returns>
		public int ToInt() => (Major * 100) + Minor;


		/// <summary>
		///     Returns a string representation of the firmware version.
		/// </summary>
		/// <returns>
		///     A string representing the firmware in the same way it is
		///     represented by the device: as a 3-digit integer.
		/// </returns>
		public override string ToString() => $"{Major}{Minor:D2}";


		/// <summary>
		///     Returns a string representation of the firmware version.
		/// </summary>
		/// <param name="aFormat">Numeric formatting information.</param>
		/// <returns>
		///     A string representing the firmware using the decimal
		///     point separator specified by the given number format info.
		/// </returns>
		public string ToString(NumberFormatInfo aFormat)
		{
			var digits = aFormat.NumberDecimalDigits;
			var sep = aFormat.NumberDecimalSeparator;
			var output = string.Join(sep, Major.ToString(), Minor.ToString("D" + digits));
			if (Build != 0)
			{
				output += sep + Build;
			}

			return output;
		}


		/// <summary>
		///     Equality operator.
		/// </summary>
		/// <param name="aLeft">A FirmwareVersion to be compared.</param>
		/// <param name="aRight">The other FirmwareVersion to compare.</param>
		/// <returns>
		///     True if both operands have the same major, minor, and
		///     build numbers. False otherwise.
		/// </returns>
		public static bool operator ==(FirmwareVersion aLeft, FirmwareVersion aRight) => (aLeft.Major == aRight.Major)
																					 && (aLeft.Minor == aRight.Minor)
																					 && (aLeft.Build == aRight.Build);


		/// <summary>
		///     Inequality operator.
		/// </summary>
		/// <param name="aLeft">A FirmwareVersion to be compared.</param>
		/// <param name="aRight">The other FirmwareVersion to compare.</param>
		/// <returns>
		///     False if both operands have the same major, minor, and
		///     build numbers. True otherwise.
		/// </returns>
		public static bool operator !=(FirmwareVersion aLeft, FirmwareVersion aRight) => !(aLeft == aRight);


		/// <summary>
		///     Less than operator.
		/// </summary>
		/// <param name="aLeft">A FirmwareVersion to be compared.</param>
		/// <param name="aRight">The other FirmwareVersion to compare.</param>
		/// <returns>
		///     True if <paramref name="aLeft" /> is less than
		///     <paramref name="aRight" />. False otherwise.
		/// </returns>
		public static bool operator <(FirmwareVersion aLeft, FirmwareVersion aRight)
		{
			if (aLeft.Major == aRight.Major)
			{
				if (aLeft.Minor == aRight.Minor)
				{
					return aLeft.Build < aRight.Build;
				}

				return aLeft.Minor < aRight.Minor;
			}

			return aLeft.Major < aRight.Major;
		}


		/// <summary>
		///     Greater than operator.
		/// </summary>
		/// <param name="aLeft">A FirmwareVersion to be compared.</param>
		/// <param name="aRight">The other FirmwareVersion to compare.</param>
		/// <returns>
		///     True if <paramref name="aLeft" /> is greater than
		///     <paramref name="aRight" />. False otherwise.
		/// </returns>
		public static bool operator >(FirmwareVersion aLeft, FirmwareVersion aRight)
		{
			if (aLeft.Major == aRight.Major)
			{
				if (aLeft.Minor == aRight.Minor)
				{
					return aLeft.Build > aRight.Build;
				}

				return aLeft.Minor > aRight.Minor;
			}

			return aLeft.Major > aRight.Major;
		}


		/// <summary>
		///     Less than or equal to operator.
		/// </summary>
		/// <param name="aLeft">A FirmwareVersion to be compared.</param>
		/// <param name="aRight">The other FirmwareVersion to compare.</param>
		/// <returns>
		///     True if <paramref name="aLeft" /> is less than or equal to
		///     <paramref name="aRight" />. False otherwise.
		/// </returns>
		public static bool operator <=(FirmwareVersion aLeft, FirmwareVersion aRight)
		{
			if (aLeft.Major == aRight.Major)
			{
				if (aLeft.Minor == aRight.Minor)
				{
					return aLeft.Build <= aRight.Build;
				}

				return aLeft.Minor <= aRight.Minor;
			}

			return aLeft.Major <= aRight.Major;
		}


		/// <summary>
		///     Greater than or equal to operator.
		/// </summary>
		/// <param name="aLeft">A FirmwareVersion to be compared.</param>
		/// <param name="aRight">The other FirmwareVersion to compare.</param>
		/// <returns>
		///     True if <paramref name="aLeft" /> is greater than or equal to
		///     <paramref name="aRight" />. False otherwise.
		/// </returns>
		public static bool operator >=(FirmwareVersion aLeft, FirmwareVersion aRight)
		{
			if (aLeft.Major == aRight.Major)
			{
				if (aLeft.Minor == aRight.Minor)
				{
					return aLeft.Build >= aRight.Build;
				}

				return aLeft.Minor >= aRight.Minor;
			}

			return aLeft.Major >= aRight.Major;
		}


		/// <summary>
		///     Equality operator for comparing a FirmwareVersion to an integer.
		/// </summary>
		/// <param name="aVersion">A FirmwareVersion to be compared.</param>
		/// <param name="aIntValue">
		///     An integer to compare with <paramref name="aVersion" />
		/// </param>
		/// <returns>
		///     True if the major version of <paramref name="aVersion" /> is equal
		///     to <paramref name="aIntValue" /> divided by 100, and the minor version of
		///     <paramref name="aVersion" /> is equal to <paramref name="aIntValue" /> modulo 100.
		///     False otherwise.
		/// </returns>
		public static bool operator ==(FirmwareVersion aVersion, int aIntValue)
			=> (aVersion.Major == (aIntValue / 100)) && (aVersion.Minor == (aIntValue % 100));


		/// <summary>
		///     Inequality operator for comparing a FirmwareVersion to an integer.
		/// </summary>
		/// <param name="aVersion">A FirmwareVersion to be compared.</param>
		/// <param name="aIntValue">
		///     An integer to compare with <paramref name="aVersion" />
		/// </param>
		/// <returns>
		///     False if the major version of <paramref name="aVersion" /> is equal
		///     to <paramref name="aIntValue" /> divided by 100, or if the minor version of
		///     <paramref name="aVersion" /> is equal to <paramref name="aIntValue" /> modulo 100.
		///     True otherwise.
		/// </returns>
		public static bool operator !=(FirmwareVersion aVersion, int aIntValue) => !(aVersion == aIntValue);


		/// <summary>
		///     Less than operator for comparing a FirmwareVersion to an integer.
		/// </summary>
		/// <param name="aVersion">A FirmwareVersion to be compared.</param>
		/// <param name="aIntValue">
		///     An integer to compare with <paramref name="aVersion" />
		/// </param>
		/// <returns>
		///     True if the major version of <paramref name="aVersion" /> is less
		///     than <paramref name="aIntValue" /> divided by 100. If they are equal, then
		///     this operator evaluates to true if the minor version of
		///     <paramref name="aVersion" /> is less than <paramref name="aIntValue" /> modulo 100.
		///     False otherwise.
		/// </returns>
		public static bool operator <(FirmwareVersion aVersion, int aIntValue)
		{
			var iMajor = aIntValue / 100;
			var iMinor = aIntValue % 100;

			if (aVersion.Major == iMajor)
			{
				return aVersion.Minor < iMinor;
			}

			return aVersion.Major < iMajor;
		}


		/// <summary>
		///     Greater than operator for comparing a FirmwareVersion to an integer.
		/// </summary>
		/// <param name="aVersion">A FirmwareVersion to be compared.</param>
		/// <param name="aIntValue">
		///     An integer to compare with <paramref name="aVersion" />
		/// </param>
		/// <returns>
		///     True if the major version of <paramref name="aVersion" /> is
		///     greater than <paramref name="aIntValue" /> divided by 100. If they are
		///     equal, then this operator evaluates to true if the minor version of
		///     <paramref name="aVersion" /> is greater than <paramref name="aIntValue" /> modulo
		///     100. False otherwise.
		/// </returns>
		public static bool operator >(FirmwareVersion aVersion, int aIntValue)
		{
			var iMajor = aIntValue / 100;
			var iMinor = aIntValue % 100;

			if (aVersion.Major == iMajor)
			{
				return aVersion.Minor > iMinor;
			}

			return aVersion.Major > iMajor;
		}


		/// <summary>
		///     Less than or equal to operator for comparing a FirmwareVersion to an
		///     integer.
		/// </summary>
		/// <param name="aVersion">A FirmwareVersion to be compared.</param>
		/// <param name="aIntValue">
		///     An integer to compare with <paramref name="aVersion" />
		/// </param>
		/// <returns>
		///     True if the major version of <paramref name="aVersion" /> is less
		///     than <paramref name="aIntValue" /> divided by 100. If they are equal, then
		///     this operator evaluates to true if the minor version of
		///     <paramref name="aVersion" /> is less than or equal to <paramref name="aIntValue" />
		///     modulo 100. False otherwise.
		/// </returns>
		public static bool operator <=(FirmwareVersion aVersion, int aIntValue)
		{
			var iMajor = aIntValue / 100;
			var iMinor = aIntValue % 100;

			if (aVersion.Major == iMajor)
			{
				return aVersion.Minor <= iMinor;
			}

			return aVersion.Major < iMajor;
		}


		/// <summary>
		///     Greater than or equal to operator for comparing a FirmwareVersion to
		///     an integer.
		/// </summary>
		/// <param name="aVersion">A FirmwareVersion to be compared.</param>
		/// <param name="aIntValue">
		///     An integer to compare with <paramref name="aVersion" />
		/// </param>
		/// <returns>
		///     True if the major version of <paramref name="aVersion" /> is
		///     greater than <paramref name="aIntValue" /> divided by 100. If they are equal,
		///     then this operator evaluates to true if the minor version of
		///     <paramref name="aVersion" /> is greater than or equal to
		///     <paramref name="aIntValue" /> modulo 100. False otherwise.
		/// </returns>
		public static bool operator >=(FirmwareVersion aVersion, int aIntValue)
		{
			var iMajor = aIntValue / 100;
			var iMinor = aIntValue % 100;

			if (aVersion.Major == iMajor)
			{
				return aVersion.Minor >= iMinor;
			}

			return aVersion.Major > iMajor;
		}


		/// <summary>
		///     Equality operator for comparing a FirmwareVersion to an integer.
		/// </summary>
		/// <param name="aVersion">A FirmwareVersion to be compared.</param>
		/// <param name="aIntValue">
		///     An integer to compare with <paramref name="aVersion" />
		/// </param>
		/// <returns>
		///     True if the major version of <paramref name="aVersion" /> is equal
		///     to <paramref name="aIntValue" /> divided by 100, and the minor version of
		///     <paramref name="aVersion" /> is equal to <paramref name="aIntValue" /> modulo 100.
		///     False otherwise.
		/// </returns>
		public static bool operator ==(int aIntValue, FirmwareVersion aVersion) => aVersion == aIntValue;


		/// <summary>
		///     Inequality operator for comparing a FirmwareVersion to an integer.
		/// </summary>
		/// <param name="aVersion">A FirmwareVersion to be compared.</param>
		/// <param name="aIntValue">
		///     An integer to compare with <paramref name="aVersion" />
		/// </param>
		/// <returns>
		///     False if the major version of <paramref name="aVersion" /> is equal
		///     to <paramref name="aIntValue" /> divided by 100, or if the minor version of
		///     <paramref name="aVersion" /> is equal to <paramref name="aIntValue" /> modulo 100.
		///     True otherwise.
		/// </returns>
		public static bool operator !=(int aIntValue, FirmwareVersion aVersion) => aVersion != aIntValue;


		/// <summary>
		///     Less than operator for comparing a FirmwareVersion to an integer.
		/// </summary>
		/// <param name="aVersion">A FirmwareVersion to be compared.</param>
		/// <param name="aIntValue">
		///     An integer to compare with <paramref name="aVersion" />
		/// </param>
		/// <returns>
		///     True if the major version of <paramref name="aVersion" /> is less
		///     than <paramref name="aIntValue" /> divided by 100. If they are equal, then
		///     this operator evaluates to true if the minor version of
		///     <paramref name="aVersion" /> is less than <paramref name="aIntValue" /> modulo 100.
		///     False otherwise.
		/// </returns>
		public static bool operator <(int aIntValue, FirmwareVersion aVersion) => aVersion >= aIntValue;


		/// <summary>
		///     Greater than operator for comparing a FirmwareVersion to an integer.
		/// </summary>
		/// <param name="aVersion">A FirmwareVersion to be compared.</param>
		/// <param name="aIntValue">
		///     An integer to compare with <paramref name="aVersion" />
		/// </param>
		/// <returns>
		///     True if the major version of <paramref name="aVersion" /> is
		///     greater than <paramref name="aIntValue" /> divided by 100. If they are
		///     equal, then this operator evaluates to true if the minor version of
		///     <paramref name="aVersion" /> is greater than <paramref name="aIntValue" /> modulo
		///     100. False otherwise.
		/// </returns>
		public static bool operator >(int aIntValue, FirmwareVersion aVersion) => aVersion <= aIntValue;


		/// <summary>
		///     Less than or equal to operator for comparing a FirmwareVersion to an
		///     integer.
		/// </summary>
		/// <param name="aVersion">A FirmwareVersion to be compared.</param>
		/// <param name="aIntValue">
		///     An integer to compare with <paramref name="aVersion" />
		/// </param>
		/// <returns>
		///     True if the major version of <paramref name="aVersion" /> is less
		///     than <paramref name="aIntValue" /> divided by 100. If they are equal, then
		///     this operator evaluates to true if the minor version of
		///     <paramref name="aVersion" /> is less than or equal to <paramref name="aIntValue" />
		///     modulo 100. False otherwise.
		/// </returns>
		public static bool operator <=(int aIntValue, FirmwareVersion aVersion) => aVersion > aIntValue;


		/// <summary>
		///     Greater than or equal to operator for comparing a FirmwareVersion to
		///     an integer.
		/// </summary>
		/// <param name="aVersion">A FirmwareVersion to be compared.</param>
		/// <param name="aIntValue">
		///     An integer to compare with <paramref name="aVersion" />
		/// </param>
		/// <returns>
		///     True if the major version of <paramref name="aVersion" /> is
		///     greater than <paramref name="aIntValue" /> divided by 100. If they are equal,
		///     then this operator evaluates to true if the minor version of
		///     <paramref name="aVersion" /> is greater than or equal to
		///     <paramref name="aIntValue" /> modulo 100. False otherwise.
		/// </returns>
		public static bool operator >=(int aIntValue, FirmwareVersion aVersion) => aVersion < aIntValue;
	}
}

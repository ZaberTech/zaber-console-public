﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Zaber
{
	/// <summary>
	///     Guidance data used by device performance tuning algorithms.
	/// </summary>
	public class TuningData
	{
		/// <summary>
		///     Default constructor.
		/// </summary>
		public TuningData()
		{
		}

		/// <summary>
		///     Copy constructor.
		/// </summary>
		/// <param name="aOther">Instance to copy.</param>
		public TuningData(TuningData aOther)
		{
			Inertia = (null != aOther.Inertia) ? new Units.Measurement(aOther.Inertia) : null;
			ForqueConstant = aOther.ForqueConstant;
			GuidanceData = aOther.GuidanceData;

			if (null != aOther.ControllerVersions)
			{
				ControllerVersions = new Dictionary<string, int>(aOther.ControllerVersions);
			}
		}


		/// <summary>
		///     Inertia or rotational inertia of the moving part of the stage, without load.
		/// </summary>
		public Units.Measurement Inertia { get; set; }


		/// <summary>
		///     The conversion factor of driver current to force or torque output
		///     for the device. Typically in units of newtons per amp for linear
		///     devices or newton-meters per amp for rotary devices.
		/// </summary>
		public double ForqueConstant { get; set; }


		/// <summary>
		///     Tuning algorithm guidance data. This is typically JSON. The content varies by
		///     device type and tuning algorithm. It is considered opaque by most of Zaber
		///     Console (except tuning code) and should not be used in scripts because it
		///     is subject to change without notice.
		/// </summary>
		public string GuidanceData { get; set; }


		/// <summary>
		///     Table of control loop implementation versions for the device firmware. Keys
		///     are controller names such as "ffpid" and values are the versions of the implementation
		///     of the controller present in the device's firmware. This can be used to match
		///     tuning software with varying controller implementations in firmware.
		///     If a controller name is not present in this table, the version can be assumed to be 1.
		/// </summary>
		public IDictionary<string, int> ControllerVersions { get; set; }


		/// <inheritdoc cref="object.Equals(object)"/>
		public override bool Equals(object aObj)
		{
			if ((aObj is TuningData data)
			    && (Inertia == data.Inertia)
			    && (Math.Abs(ForqueConstant - data.ForqueConstant) < double.Epsilon)
			    && (GuidanceData == data.GuidanceData))
			{
				if ((null != ControllerVersions) && (null != data.ControllerVersions))
				{
					return ControllerVersions.OrderBy(pair => pair.Key)
						.SequenceEqual(data.ControllerVersions.OrderBy(pair => pair.Key));
				}
				else
				{
					return ControllerVersions == data.ControllerVersions;
				}
			}

			return false;
		}


		/// <inheritdoc cref="object.GetHashCode()"/>
		public override int GetHashCode()
		{
			var code = 17;

			if (null != Inertia)
			{
				code = (31 * code) + Inertia.GetHashCode();
			}

			code = (31 * code) + ForqueConstant.GetHashCode();

			if (null != GuidanceData)
			{
				code = (31 * code) + GuidanceData.GetHashCode();
			}

			if (null != ControllerVersions)
			{
				foreach (var pair in ControllerVersions.OrderBy(pair => pair.Key))
				{
					code = (31 * code) + pair.Key.GetHashCode();
					code = (31 * code) + pair.Value.GetHashCode();
				}
			}

			return code;
		}
	}
}

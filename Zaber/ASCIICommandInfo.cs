﻿using System.Collections.Generic;
using System.Linq;

namespace Zaber
{
	/// <summary>
	///     Information on an ASCII command defined by a series of tree nodes.
	///     <seealso cref="ASCIICommandNode" />
	/// </summary>
	public class ASCIICommandInfo : CommandInfo
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Constructs information about a single ASCII command from a tree
		///     "branch" of command nodes.
		/// </summary>
		/// <param name="aLeafNode">The outermost or "leaf" node of the command tree.</param>
		public ASCIICommandInfo(ASCIICommandNode aLeafNode)
		{
			Nodes = aLeafNode.GetBranch();
		}


		// Private constructor used by Clone().
		private ASCIICommandInfo()
		{
		}


		/// <summary>
		///     Create a copy of the CommandInfo. In the case of ASCII commands this is a shallow copy; since we
		///     never programmatically alter the underlying tree structure there is no need to clone that.
		/// </summary>
		/// <returns>A new instance of CommandInfo with all the same property values as the instance invoked on.</returns>
		public override CommandInfo Clone()
		{
			var ci = new ASCIICommandInfo();
			CopyProperties(this, ci);
			return ci;
		}


		/// <summary>
		///     The access level of the command, determined by its leaf node.
		/// </summary>
		public override int AccessLevel => Nodes.Last.Value.AccessLevel;


		/// <summary>
		///     Whether or not the command should be displayed without the "show all"
		///     checkbox being checked. Determined by the command's leaf node.
		/// </summary>
		public override bool IsBasic => Nodes.Last.Value.IsBasic;


		/// <summary>
		///     A text representation of the command where parameters can be inserted
		///     using <see cref="string.Format(string, object)" />.
		/// </summary>
		public override string Name
		{
			get
			{
				var i = 0;
				var format = "";

				foreach (var node in Nodes)
				{
					if (null == node.ParamType)
					{
						format += node.NodeText + " ";
					}
					else
					{
						format += "{" + i++ + "} ";
					}
				}

				return format.Substring(0, format.Length - 1);
			}
		}


		/// <summary>
		///     Complete string for the ASCII command. May include positional parameter references
		///     for commands that have parameters.
		/// </summary>
		public override string TextCommand => Name;


		/// <summary>
		///     All tree nodes associated with the command.
		/// </summary>
		public LinkedList<ASCIICommandNode> Nodes { get; private set; }

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Helper for Clone() that copies properties from one instance to another.
		///     Subclasses should invoke this to handle base class properties.
		/// </summary>
		/// <param name="aFrom">The instance to copy values from.</param>
		/// <param name="aTo">The instance to copy values to.</param>
		protected static void CopyProperties(ASCIICommandInfo aFrom, ASCIICommandInfo aTo)
		{
			CommandInfo.CopyProperties(aFrom, aTo);

			// Copy all the parameter nodes since those can be modified for multi-axis controllers.
			// Rest of the nodes are shared between commands. 
			aTo.Nodes = new LinkedList<ASCIICommandNode>(
				aFrom.Nodes.Select(node => node.IsParameter ? node.Clone() : node));
		}

		#endregion

		#region -- Data --

		#endregion
	}
}

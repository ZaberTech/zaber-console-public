﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Zaber
{
	/// <summary>
	///     Information on a parameter that can be passed as part of a valid
	///     ASCII command.
	/// </summary>
	public class ParameterType
	{
		#region -- Construction --

		/// <summary>
		///     Default constructor; constructor for enum types.
		/// </summary>
		/// <param name="aEnumNodes">Optional: List of enumeration values for enum type.</param>
		public ParameterType(IEnumerable<string> aEnumNodes = null)
		{
			_enumValues = aEnumNodes?.ToList();
		}


		/// <summary>
		///     Copy constructor. Use to make clones when changing the enum values
		///     or arity. This will make a copy of any enum values.
		/// </summary>
		/// <param name="aOther">Instance to copy.</param>
		public ParameterType(ParameterType aOther)
		{
			IsNumeric = aOther.IsNumeric;
			IsBoolean = aOther.IsBoolean;
			IsSigned = aOther.IsSigned;
			DecimalPlaces = aOther.DecimalPlaces;
			Arity = aOther.Arity;
			_name = aOther._name;
			_enumValues = aOther._enumValues?.ToList();
		}

		#endregion

		#region -- Public API --

		/// <summary>
		///     Optional name for the parameter type. Used for debugging and log messages.
		/// </summary>
		public string Name
		{
			get
			{
				if (!string.IsNullOrEmpty(_name))
				{
					return _name;
				}

				if (IsEnumeration)
				{
					return "Enum";
				}

				if (IsBoolean)
				{
					return "Bool";
				}

				if (IsNumeric)
				{
					return "Int";
				}

				if (IsToken)
				{
					return "Token";
				}

				return "Unknown";
			}
			set => _name = value;
		}

		/// <summary>
		///     Indicates this parameter type is numeric.
		/// </summary>
		/// <seealso cref="DecimalPlaces" />
		public bool IsNumeric { get; set; }


		/// <summary>
		///     Indicates this parameter type is boolean. Boolean counts as a numeric type with values 0 and 1.
		/// </summary>
		public bool IsBoolean { get; set; }


		/// <summary>
		///     For numeric values, indicates whether negative values are allowed.
		/// </summary>
		public bool IsSigned { get; set; }


		/// <summary>
		///     Returns true if this parameter type is an enumeration - that is, has a set of
		///     redefined values that are named.
		/// </summary>
		/// <seealso cref="EnumValues" />
		public bool IsEnumeration => null != _enumValues;


		/// <summary>
		///     Indicates this parameter type is a token (arbitrary string).
		/// </summary>
		public bool IsToken => !IsNumeric && !IsEnumeration && !IsBoolean;


		/// <summary>
		///     Enumeration values for this type.
		/// </summary>
		/// <see cref="IsEnumeration" />
		public IEnumerable<string> EnumValues => _enumValues;


		/// <summary>
		///     Number of decimal places for numeric values that allow sub-integer precision.
		/// </summary>
		/// <seealso cref="IsNumeric" />
		public int DecimalPlaces { get; set; }


		/// <summary>
		///     How many times this parameter can be repeated. If null, it can repeat
		///     an unspecified number of times and the success of the command is the
		///     only source of information about acceptability.
		/// </summary>
		public int? Arity { get; set; } = 1;


		/// <summary>
		///     Format a numeric value as a string according to the format expected for
		///     this parameter type. Only applicable to numeric types.
		/// </summary>
		/// <param name="aValue">Value to format.</param>
		/// <returns>String representation of the value.</returns>
		/// <exception cref="FormatException">This parameter is not numberic.</exception>
		public string FormatNumeric(decimal aValue)
		{
			if (IsBoolean)
			{
				return aValue >= 0.5m ? "1" : "0";
			}

			if (!IsNumeric)
			{
				throw new FormatException("Cannot format a non-numeric parameter type.");
			}

			var valueFormatStr = "{0:0}";
			if (DecimalPlaces > 0)
			{
				valueFormatStr = "{0:0." + "".PadRight(DecimalPlaces, '0') + "}";
			}

			return string.Format(CultureInfo.InvariantCulture, valueFormatStr, aValue);
		}


		/// <summary>
		///     ToString() override for debugging help.
		/// </summary>
		/// <returns>A string representation of this parameter type.</returns>
		public override string ToString() => "ParamType: " + Name;

		#endregion

		#region -- Data --

		private readonly IList<string> _enumValues;

		private string _name;

		#endregion
	}
}

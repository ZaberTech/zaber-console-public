﻿using System;
using System.Runtime.Serialization;

namespace Zaber
{
	/// <summary>
	///     A loopback connection was detected on a serial port.
	/// </summary>
	/// <remarks>
	///     A loopback connection
	///     is a connection from the serial port's send pin to the receive pin.
	///     This makes it appear as though the serial port is receiving a copy of
	///     every byte it sends. This can happen intentionally when a loopback dongle
	///     is connected to the serial port, or unintentionally when wires are
	///     connected incorrectly.
	/// </remarks>
	[Serializable]
	public class LoopbackException : Exception
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor. Initializes the exception with a generic message.
		/// </summary>
		public LoopbackException()
			: base("Loopback connection detected.")
		{
		}


		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aMessage">The message to display.</param>
		public LoopbackException(string aMessage)
			: base(aMessage)
		{
		}


		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aMessage">The message to display</param>
		/// <param name="aException">The exception that caused this exception</param>
		public LoopbackException(string aMessage, Exception aException)
			: base(aMessage, aException)
		{
		}


		/// <summary>
		///     Initializes a new instance with serialized data.
		/// </summary>
		/// <param name="aInfo">
		///     The System.Runtime.Serialization.SerializationInfo
		///     that holds the serialized object data about the exception being
		///     thrown.
		/// </param>
		/// <param name="aContext">
		///     The System.Runtime.Serialization.StreamingContext
		///     that contains contextual information about the source or
		///     destination.
		/// </param>
		protected LoopbackException(SerializationInfo aInfo, StreamingContext aContext)
			: base(aInfo, aContext)
		{
		}

		#endregion
	}
}

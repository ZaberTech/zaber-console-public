using System;

namespace Zaber
{
	/// <summary>
	///     Notification of a request or response from one of the devices in the chain.
	/// </summary>
	public class DataPacketEventArgs : EventArgs
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aPacket">See <see cref="DataPacket" /></param>
		public DataPacketEventArgs(DataPacket aPacket)
		{
			DataPacket = aPacket;
		}


		/// <summary>
		///     Gets the details of the device's request or response.
		/// </summary>
		public DataPacket DataPacket { get; }

		#endregion
	}
}

using System;

namespace Zaber
{
	/// <summary>
	///     Returns a response from one of the devices in the chain.
	/// </summary>
	public class ZaberPortErrorReceivedEventArgs : EventArgs
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aErrorType">The type of error that raised this event</param>
		public ZaberPortErrorReceivedEventArgs(ZaberPortError aErrorType)
		{
			ErrorType = aErrorType;
		}


		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aErrorType">The type of error that raised this event</param>
		/// <param name="aErrorMessage">A message associated with the error.</param>
		public ZaberPortErrorReceivedEventArgs(ZaberPortError aErrorType, string aErrorMessage)
		{
			ErrorType = aErrorType;
			Message = aErrorMessage;
		}


		/// <summary>
		///     The type of error that raised the event.
		/// </summary>
		public ZaberPortError ErrorType { get; set; }


		/// <summary>
		///     A string containing extra data related to the error.
		/// </summary>
		public string Message { get; set; }

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Zaber.Units;

namespace Zaber
{
	/// <summary>
	///     Describes a unit of measure used for converting measurements.
	/// </summary>
	public class UnitOfMeasure : IComparable<UnitOfMeasure>
	{
		#region -- Public Methods & Properties --

		#region -- IComparable<UnitOfMeasure> implementation --

		/// <summary>
		///     Compares this instance with another UnitOfMeasure object and
		///     indicates whether this instance precedes, follows, or appears in
		///     the same position in the sort order as other object.
		/// </summary>
		/// <param name="aOther">The object to compare to.</param>
		/// <returns>
		///     A 32-bit signed integer that indicates whether this
		///     instance precedes, follows, or appears in the same position in the
		///     sort order as the value parameter. Less than zero if this instance
		///     precedes other. Zero if this instance has the same position in the
		///     sort order as other. Greater than zero if this instance follows
		///     other or other is null.
		/// </returns>
		public int CompareTo(UnitOfMeasure aOther) => null == aOther
			? 1
			: string.Compare(Abbreviation, aOther.Abbreviation, StringComparison.Ordinal);

		#endregion

		#endregion

		#region -- Unit symbols --

		/// <summary>
		///     Unit symbol for native, untranslated device data.
		/// </summary>
		public static readonly string DataSymbol = "-- data --";

		/// <summary>
		///     Unit symbol for steps, a device-specific unit of length.
		///     This symbol is used internally in Zaber Console and not displayed.
		/// </summary>
		public static readonly string StepSymbol = "step";

		/// <summary>
		///     Unit symbol for microsteps, a device-specific unit of length.
		///     This symbol is used internally in Zaber Console and not displayed.
		/// </summary>
		public static readonly string MicrostepSymbol = "µstep";

		/// <summary>
		///     Unit symbol for steps per second, a device-specific unit of linear velocity.
		///     This symbol is used internally in Zaber Console and not displayed.
		/// </summary>
		public static readonly string StepsPerSecondSymbol = "step/s";

		/// <summary>
		///     Unit symbol for device-specific velocity units.
		///     This symbol is used internally in Zaber Console and not displayed.
		/// </summary>
		public static readonly string DeviceStepVelocityUnitSymbol = "step/dT";

		/// <summary>
		///     Unit symbol for device-specific velocity units.
		///     This symbol is used internally in Zaber Console and not displayed.
		/// </summary>
		public static readonly string DeviceMicrostepVelocityUnitSymbol = "µstep/dT";

		/// <summary>
		///     Unit symbol for steps per second squared, a device-specific unit of linear acceleration.
		///     This symbol is used internally in Zaber Console and not displayed.
		/// </summary>
		public static readonly string StepsPerSecondSquaredSymbol = "step/s²";

		/// <summary>
		///     Unit symbol for device-specific acceleration units.
		///     This symbol is used internally in Zaber Console and not displayed.
		/// </summary>
		public static readonly string DeviceStepAccelerationUnitSymbol = "step/dT²";

		/// <summary>
		///     Unit symbol for device-specific acceleration units.
		///     This symbol is used internally in Zaber Console and not displayed.
		/// </summary>
		public static readonly string DeviceMicrostepAccelerationUnitSymbol = "µstep/dT²";


		/// <summary>
		///     Unit symbol for meters, a unit of length.
		/// </summary>
		public static readonly string MeterSymbol = "m";

		/// <summary>
		///     Unit symbol for centiimeters, a unit of length.
		/// </summary>
		public static readonly string CentimeterSymbol = "cm";

		/// <summary>
		///     Unit symbol for millimeters, a unit of length.
		/// </summary>
		public static readonly string MillimeterSymbol = "mm";

		/// <summary>
		///     Unit symbol for micrometers, a unit of length.
		/// </summary>
		public static readonly string MicrometerSymbol = "µm";

		/// <summary>
		///     Unit symbol for nanometers, a unit of length.
		/// </summary>
		public static readonly string NanometerSymbol = "nm";

		/// <summary>
		///     Unit symbol for inches, a unit of length.
		/// </summary>
		public static readonly string InchSymbol = "in";


		/// <summary>
		///     Unit symbol for degrees, a unit of angle.
		/// </summary>
		public static readonly string DegreeSymbol = "°";

		/// <summary>
		///     Unit symbol for radians, a unit of angle.
		/// </summary>
		public static readonly string RadianSymbol = "rad";

		/// <summary>
		///     Unit symbol for revolutions, a unit of angle.
		/// </summary>
		public static readonly string RevolutionSymbol = "rev";


		/// <summary>
		///     Unit symbol for meters per second, a unit of linear velocity.
		/// </summary>
		public static readonly string MetersPerSecondSymbol = "m/s";

		/// <summary>
		///     Unit symbol for centimeters per second, a unit of linear velocity.
		/// </summary>
		public static readonly string CentimetersPerSecondSymbol = "cm/s";

		/// <summary>
		///     Unit symbol for millimeters per second, a unit of linear velocity.
		/// </summary>
		public static readonly string MillimetersPerSecondSymbol = "mm/s";

		/// <summary>
		///     Unit symbol for micrometers per second, a unit of linear velocity.
		/// </summary>
		public static readonly string MicrometersPerSecondSymbol = "µm/s";

		/// <summary>
		///     Unit symbol for nanometers per second, a unit of linear velocity.
		/// </summary>
		public static readonly string NanometersPerSecondSymbol = "nm/s";

		/// <summary>
		///     Unit symbol for microsteps per second, a device-specific unit of linear velocity.
		/// </summary>
		public static readonly string MicrostepsPerSecondSymbol = "µstep/s";

		/// <summary>
		///     Unit symbol for inches per second, a unit of linear velocity.
		/// </summary>
		public static readonly string InchesPerSecondSymbol = "in/s";

		/// <summary>
		///     Unit symbol for degrees per second, a unit of angular velocity.
		/// </summary>
		public static readonly string DegreesPerSecondSymbol = "°/s";

		/// <summary>
		///     Unit symbol for radians per second, a unit of angular velocity.
		/// </summary>
		public static readonly string RadiansPerSecondSymbol = "rad/s";

		/// <summary>
		///     Unit symbol for revolutions per minute, a unit of angular velocity.
		/// </summary>
		public static readonly string RevolutionsPerMinuteSymbol = "RPM";


		/// <summary>
		///     Unit symbol for meters per second squared, a unit of linear acceleration.
		/// </summary>
		public static readonly string MetersPerSecondSquaredSymbol = "m/s²";

		/// <summary>
		///     Unit symbol for centimeters per second squared, a unit of linear acceleration.
		/// </summary>
		public static readonly string CentimetersPerSecondSquaredSymbol = "cm/s²";

		/// <summary>
		///     Unit symbol for millimeters per second squared, a unit of linear acceleration.
		/// </summary>
		public static readonly string MillimetersPerSecondSquaredSymbol = "mm/s²";

		/// <summary>
		///     Unit symbol for micrometers per second squared, a unit of linear acceleration.
		/// </summary>
		public static readonly string MicrometersPerSecondSquaredSymbol = "µm/s²";

		/// <summary>
		///     Unit symbol for nanometers per second squared, a unit of linear acceleration.
		/// </summary>
		public static readonly string NanometersPerSecondSquaredSymbol = "nm/s²";

		/// <summary>
		///     Unit symbol for microsteps per second squared, a device-specific unit of linear acceleration.
		/// </summary>
		public static readonly string MicrostepsPerSecondSquaredSymbol = "µstep/s²";

		/// <summary>
		///     Unit symbol for inches per second squared, a unit of linear acceleration.
		/// </summary>
		public static readonly string InchesPerSecondSquaredSymbol = "in/s²";


		/// <summary>
		///     Unit symbol for degrees per second squared, a unit of angular acceleration.
		/// </summary>
		public static readonly string DegreesPerSecondSquaredSymbol = "°/s²";

		/// <summary>
		///     Unit symbol for radians per second squared, a unit of angular acceleration.
		/// </summary>
		public static readonly string RadiansPerSecondSquaredSymbol = "rad/s²";


		/// <summary>
		///     Unit symbol for newtons, a unit of force.
		/// </summary>
		public static readonly string NewtonsSymbol = "N";

		/// <summary>
		///     Unit symbol for millinewtons, a unit of force.
		/// </summary>
		public static readonly string MillinewtonsSymbol = "mN";

		/// <summary>
		///     Unit symbol for pound-force, a unit of force.
		/// </summary>
		public static readonly string PoundForceSymbol = "lbf";


		/// <summary>
		///     Unit symbol for amperes, a unit of DC electrical current.
		/// </summary>
		public static readonly string AmperesSymbol = "A";

		/// <summary>
		///     Unit symbol for milliamperes, a unit of DC electrical current.
		/// </summary>
		public static readonly string MilliamperesSymbol = "mA";

		/// <summary>
		///     Unit symbol for peak amperes, a unit of AC electrical current.
		/// </summary>
		public static readonly string AmperesPeakSymbol = "A(peak)";

		/// <summary>
		///     Unit symbol for RMS amperes, a unit of AC electrical current.
		/// </summary>
		public static readonly string AmperesRmsSymbol = "A(RMS)";


		/// <summary>
		///     Unit symbol for percentage, a unit of ratio.
		/// </summary>
		public static readonly string PercentSymbol = "%";


		/// <summary>
		///     Unit symbol for seconds, a unit of time.
		/// </summary>
		public static readonly string SecondsSymbol = "s";

		/// <summary>
		///     Unit symbol for milliseconds, a unit of time.
		/// </summary>
		public static readonly string MillisecondsSymbol = "ms";

		/// <summary>
		///     Unit symbol for microseconds, a unit of time.
		/// </summary>
		public static readonly string MicrosecondsSymbol = "µs";


		/// <summary>
		///     Symbols for internal intermediate units of measure that should
		///     normally be hidden from user interfaces.
		/// </summary>
		public static ISet<string> HiddenUnitSymbols = new HashSet<string>
		{
			StepSymbol,
			StepsPerSecondSymbol,
			StepsPerSecondSquaredSymbol,
			MicrostepSymbol,
			DeviceStepVelocityUnitSymbol,
			DeviceStepAccelerationUnitSymbol,
			DeviceMicrostepVelocityUnitSymbol,
			DeviceMicrostepAccelerationUnitSymbol
		};

		#endregion

		#region -- Predefined Units --

		/// <summary>
		///     Raw data value for any command.
		/// </summary>
		public static readonly UnitOfMeasure Data;

		// -------- Position -----------

		/// <summary>
		///     Measures position in metres.
		/// </summary>
		public static readonly UnitOfMeasure Meter;

		/// <summary>
		///     Measures position in millimeters
		/// </summary>
		public static readonly UnitOfMeasure Millimeter;

		/// <summary>
		///     Measures position in micrometers
		/// </summary>
		public static readonly UnitOfMeasure Micrometer;

		/// <summary>
		///     Measures position in inches
		/// </summary>
		public static readonly UnitOfMeasure Inch;

		/// <summary>
		///     Measures rotary position in degrees
		/// </summary>
		public static readonly UnitOfMeasure Degree;

		/// <summary>
		///     Measures rotary position in radians
		/// </summary>
		public static readonly UnitOfMeasure Radian;

		/// <summary>
		///     Measures rotary position in revolutions
		/// </summary>
		public static readonly UnitOfMeasure Revolution;


		// ---------- Velocity ----------

		/// <summary>
		///     Measures velocity in m/s
		/// </summary>
		public static readonly UnitOfMeasure MetersPerSecond;

		/// <summary>
		///     Measures velocity in mm/s
		/// </summary>
		public static readonly UnitOfMeasure MillimetersPerSecond;

		/// <summary>
		///     Measures velocity in µm/s
		/// </summary>
		public static readonly UnitOfMeasure MicrometersPerSecond;

		/// <summary>
		///     Measures velocity in microsteps per second. Note this is a placeholder unit
		///     for legacy support; the definition of microstep size is device dependent and should be
		///     looped up via the device's UnitConverter.
		/// </summary>
		public static readonly UnitOfMeasure MicrostepsPerSecond;

		/// <summary>
		///     Measures velocity in inches per second
		/// </summary>
		public static readonly UnitOfMeasure InchesPerSecond;

		/// <summary>
		///     Measures rotary velocity in degrees per second
		/// </summary>
		public static readonly UnitOfMeasure DegreesPerSecond;

		/// <summary>
		///     Measures rotary velocity in radians per second
		/// </summary>
		public static readonly UnitOfMeasure RadiansPerSecond;

		/// <summary>
		///     Measures rotary velocity in revolutions per minute
		/// </summary>
		public static readonly UnitOfMeasure RevolutionsPerMinute;


		// ---------- Acceleration ----------

		/// <summary>
		///     Measures acceleration in m/s²
		/// </summary>
		public static readonly UnitOfMeasure MetersPerSecondSquared;

		/// <summary>
		///     Measures acceleration in mm/s²
		/// </summary>
		public static readonly UnitOfMeasure MillimetersPerSecondSquared;

		/// <summary>
		///     Measures acceleration in µm/s²
		/// </summary>
		public static readonly UnitOfMeasure MicrometersPerSecondSquared;

		/// <summary>
		///     Measures acceleration in microsteps per second squared. Note this is a placeholder unit
		///     for legacy support; the definition of microstep size is device dependent and should be
		///     looped up via the device's UnitConverter.
		/// </summary>
		public static readonly UnitOfMeasure MicrostepsPerSecondSquared;

		/// <summary>
		///     Measures acceleration in inches per second squared
		/// </summary>
		public static readonly UnitOfMeasure InchesPerSecondSquared;

		/// <summary>
		///     Measures rotary acceleration in degrees per second squared
		/// </summary>
		public static readonly UnitOfMeasure DegreesPerSecondSquared;

		/// <summary>
		///     Measures rotary acceleration in radians per second squared
		/// </summary>
		public static readonly UnitOfMeasure RadiansPerSecondSquared;


		// ---------- Other Dimensions ----------

		/// <summary>
		///     Measures current in DC amperes.
		/// </summary>
		public static readonly UnitOfMeasure Amperes;

		/// <summary>
		///     Measures current in DC milliamperes.
		/// </summary>
		public static readonly UnitOfMeasure Milliamperes;


		/// <summary>
		///     Measures current as a percentage of total available current.
		/// </summary>
		public static readonly UnitOfMeasure Percent;

		#endregion

		#region -- Initialization --

		// Static constructor - initializes rearonly fields and abbreviation lookup table.
		static UnitOfMeasure()
		{
			// Create legacy unit definitions.
			Data = new UnitOfMeasure(new BaseUnit(DataSymbol, "Device Units"))
			{
				MotionType = MotionType.Other
			};

			MetersPerSecondSquared = new UnitOfMeasure(MetersPerSecondSquaredSymbol);
			MillimetersPerSecondSquared = new UnitOfMeasure(MillimetersPerSecondSquaredSymbol);
			MicrometersPerSecondSquared = new UnitOfMeasure(MicrometersPerSecondSquaredSymbol);
			MicrostepsPerSecondSquared =
				new UnitOfMeasure(new BaseUnit(MicrostepsPerSecondSquaredSymbol)
					{
						Dimension = Dimension.Acceleration,
						UseSIPrefixes = false
					})
					{
						IsDeviceDependent = true,
						MotionType = MotionType.Other
					};
			InchesPerSecondSquared = new UnitOfMeasure(InchesPerSecondSquaredSymbol);
			DegreesPerSecondSquared = new UnitOfMeasure(DegreesPerSecondSquaredSymbol);
			RadiansPerSecondSquared = new UnitOfMeasure(RadiansPerSecondSquaredSymbol);

			MetersPerSecond = new UnitOfMeasure(MetersPerSecondSymbol, MetersPerSecondSquared);
			MillimetersPerSecond = new UnitOfMeasure(MillimetersPerSecondSymbol, MillimetersPerSecondSquared);
			MicrometersPerSecond = new UnitOfMeasure(MicrometersPerSecondSymbol, MicrometersPerSecondSquared);
			MicrostepsPerSecond =
				new UnitOfMeasure(new BaseUnit(MicrostepsPerSecondSymbol)
								  {
									  Dimension = Dimension.Velocity,
									  UseSIPrefixes = false
								  },
								  MicrostepsPerSecondSquared)
				{
					IsDeviceDependent = true,
					MotionType = MotionType.Other
				};
			InchesPerSecond = new UnitOfMeasure(InchesPerSecondSymbol, InchesPerSecondSquared);
			DegreesPerSecond = new UnitOfMeasure(DegreesPerSecondSymbol, DegreesPerSecondSquared);
			RadiansPerSecond = new UnitOfMeasure(RadiansPerSecondSymbol, RadiansPerSecondSquared);
			RevolutionsPerMinute = new UnitOfMeasure(RevolutionsPerMinuteSymbol);

			Meter = new UnitOfMeasure(MeterSymbol, MetersPerSecond);
			Millimeter = new UnitOfMeasure(MillimeterSymbol, MillimetersPerSecond);
			Micrometer = new UnitOfMeasure(MicrometerSymbol, MicrometersPerSecond);
			Inch = new UnitOfMeasure(InchSymbol, InchesPerSecond);
			Degree = new UnitOfMeasure(DegreeSymbol, DegreesPerSecond);
			Radian = new UnitOfMeasure(RadianSymbol, RadiansPerSecond);
			Revolution = new UnitOfMeasure(RevolutionSymbol, RevolutionsPerMinute);

			Amperes = new UnitOfMeasure(AmperesSymbol);
			Milliamperes = new UnitOfMeasure(MilliamperesSymbol);

			// Legacy percent dimension was controlling current.
			Percent = new UnitOfMeasure(PercentSymbol) { MeasurementType = MeasurementType.Current };

			foreach (var property in typeof(UnitOfMeasure).GetFields(BindingFlags.Static | BindingFlags.Public))
			{
				if (typeof(UnitOfMeasure).IsAssignableFrom(property.FieldType))
				{
					var unit = property.GetValue(null) as UnitOfMeasure;
					_unitsByAbbreviation[unit.Abbreviation] = unit;
				}
			}
		}


		/// <summary>
		///     Initialize a new unit of measure by abbreviation, from the XML table of standard units.
		/// </summary>
		/// <param name="aAbbrev">Abbreviation for the unit. Must be devined in the default ConversionTable instance.</param>
		/// <param name="aDerivative">Optional derivative unit.</param>
		public UnitOfMeasure(string aAbbrev, UnitOfMeasure aDerivative = null)
			: this(ConversionTable.Default.UnitConverter.FindUnitBySymbol(aAbbrev), aDerivative)
		{
		}


		/// <summary>
		///     Initialize a new unit of measure object as a wrapper around a Zaber.Unit.
		/// </summary>
		/// <param name="aUnit">The Zaber.Unit to represent.</param>
		/// <param name="aDerivative">Optional derivative unit.</param>
		public UnitOfMeasure(Unit aUnit, UnitOfMeasure aDerivative = null)
		{
			if (null == aUnit)
			{
				throw new ArgumentNullException(nameof(aUnit));
			}

			Abbreviation = aUnit.Abbreviation;
			Unit = aUnit;
			Derivative = aDerivative;

			DimensionToMotion(aUnit.Dimension, out var motionType, out var measurementType);
			MotionType = motionType;
			MeasurementType = measurementType;
		}

		#endregion

		#region -- Public properties --

		/// <summary>
		///     Get an abbreviation describing the unit of measure.
		///     Note this representation is intended for internal lookups and may not
		///     be pretty for display; in particular whitespace is not allowed and is
		///     by convention replaced with underscores.
		/// </summary>
		public string Abbreviation { get; }


		/// <summary>
		///     The related unit of measure in the next measurement type.
		/// </summary>
		/// <remarks>
		///     The derivative of position is velocity, and the
		///     derivative of velocity is acceleration.
		/// </remarks>
		public UnitOfMeasure Derivative { get; }


		/// <summary>
		///     The type of motion that the unit describes.
		/// </summary>
		/// <remarks>
		///     For example, "metres", "metres per second", and "metres
		///     per second squared" all describe linear motions. "Degrees" is a
		///     rotary motion. "Amperes" are not a measure of motion at all, and
		///     so are marked as "MotionType.Other".
		/// </remarks>
		public MotionType MotionType { get; private set; }


		/// <summary>
		///     The type of measurement represented by the unit.
		/// </summary>
		public MeasurementType MeasurementType { get; private set; }


		/// <summary>
		///     Unterlying unit definition from the Zaber.Units assembly.
		///     Used for measurement conversions.
		/// </summary>
		public Unit Unit { get; }


		/// <summary>
		///     Indicates that the definition of this unit is device-dependent, which means
		///     its <see cref="Unit" /> should nto be used, and instead the correct unit should
		///     be looked up in the device's unit converter using the same abbreviation.
		/// </summary>
		public bool IsDeviceDependent { get; set; }

		#endregion

		#region -- Public methods --

		/// <summary>
		///     Convert legacy Zaber Console motion type and measurement type to a Zaber.Units Dimension.
		/// </summary>
		/// <param name="aMotionType">The motion type.</param>
		/// <param name="aMeasurementType">The measurement type.</param>
		/// <returns>A dimension if there is a meaningful conversion.</returns>
		public static Dimension MotionToDimension(MotionType aMotionType, MeasurementType aMeasurementType)
		{
			_dimensionsByTypes.TryGetValue(new Tuple<MotionType, MeasurementType>(aMotionType, aMeasurementType),
										   out var dim);

			if (null == dim)
			{
				switch (aMeasurementType)
				{
					case MeasurementType.Force:
						dim = Dimension.Force;
						break;
					case MeasurementType.Current:
						dim = Dimension.DirectCurrent;
						break;
					case MeasurementType.CurrentAC:
						dim = Dimension.AlternatingCurrent;
						break;
				}
			}

			return dim;
		}


		/// <summary>
		///     Map a Zaber.Units Dimension to the legacy Zaber Console MotionType and MeasurementType enums.
		/// </summary>
		/// <param name="aDimension">The Dimension to convert.</param>
		/// <param name="aMotionType">Output: The corresponding motion type, or None if no match.</param>
		/// <param name="aMeasurementType">Output: The corrsponding measurement type, or Other if no match.</param>
		public static void DimensionToMotion(Dimension aDimension, out MotionType aMotionType,
											 out MeasurementType aMeasurementType)
		{
			if (!_motionTypesByDimension.TryGetValue(aDimension, out aMotionType))
			{
				aMotionType = MotionType.None;
			}

			if (!_measurementTypesByDimension.TryGetValue(aDimension, out aMeasurementType))
			{
				aMeasurementType = MeasurementType.Other;
			}
		}


		/// <summary>
		///     Create a string representation of the unit of measure.
		/// </summary>
		/// <returns>The abbreviation for this unit of measure, formatted for display.</returns>
		public override string ToString() => Abbreviation.Replace('_', ' ');


		/// <summary>
		///     Determines whether this instance and another object have the same
		///     symbol. Does not check numerical definition.
		/// </summary>
		/// <param name="aOther">The object to compare to.</param>
		/// <returns>True if the other object has the same abbreviation.</returns>
		public override bool Equals(object aOther)
		{
			var other = aOther as UnitOfMeasure;
			return (null != other) && string.Equals(Abbreviation, other.Abbreviation);
		}


		/// <summary>
		///     Determines whether two units of measure have the same
		///     symbol. Does not check numerical definition.
		/// </summary>
		/// <param name="aFirst">One unit of measure to compare.</param>
		/// <param name="aSecond">The other unit of measure.</param>
		/// <returns>True if the two objects have the same abbreviation.</returns>
		public static bool operator ==(UnitOfMeasure aFirst, UnitOfMeasure aSecond) => Equals(aFirst, aSecond);


		/// <summary>
		///     Determines whether two units of measure have different
		///     symbols. Does not check numerical definition.
		/// </summary>
		/// <param name="aFirst">One unit of measure to compare.</param>
		/// <param name="aSecond">The other unit of measure.</param>
		/// <returns>True if the two objects have different abbreviations.</returns>
		public static bool operator !=(UnitOfMeasure aFirst, UnitOfMeasure aSecond) => !Equals(aFirst, aSecond);


		/// <summary>
		///     Determines the order of two units of measure.
		/// </summary>
		/// <param name="aFirst">One unit of measure to compare.</param>
		/// <param name="aSecond">The other unit of measure.</param>
		/// <returns>True if the first objects precedes the other.</returns>
		public static bool operator <(UnitOfMeasure aFirst, UnitOfMeasure aSecond)
			=> ((null == aFirst) && (null != aSecond)) || ((null != aFirst) && (aFirst.CompareTo(aSecond) < 0));


		/// <summary>
		///     Determines the order of two units of measure.
		/// </summary>
		/// <param name="aFirst">One unit of measure to compare.</param>
		/// <param name="aSecond">The other unit of measure.</param>
		/// <returns>True if the first objects follows the other.</returns>
		public static bool operator >(UnitOfMeasure aFirst, UnitOfMeasure aSecond)
			=> (null != aFirst) && (aFirst.CompareTo(aSecond) > 0);


		/// <summary>
		///     Returns the hash code for this object.
		/// </summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		public override int GetHashCode() => Unit.GetHashCode();


		/// <summary>
		///     Get a reference to a <cref>UnitOfMeasure</cref> given its abbreviation.
		/// </summary>
		/// <param name="aUnitName">The abbreviated name of the unit, as from the <cref>Abbreviation</cref> property.</param>
		/// <returns>The corresponding static <cref>UnitOfMeasure</cref> instance, or null if not found.</returns>
		public static UnitOfMeasure FindByAbbreviation(string aUnitName)
		{
			if (_unitsByAbbreviation.ContainsKey(aUnitName))
			{
				return _unitsByAbbreviation[aUnitName];
			}

			return null;
		}

		#endregion

		#region -- Private data --

		private static readonly Dictionary<string, UnitOfMeasure> _unitsByAbbreviation =
			new Dictionary<string, UnitOfMeasure>();

		private static readonly IDictionary<Dimension, MeasurementType> _measurementTypesByDimension =
			new Dictionary<Dimension, MeasurementType>
			{
				{ Dimension.Length, MeasurementType.Position },
				{ Dimension.Angle, MeasurementType.Position },
				{ Dimension.Velocity, MeasurementType.Velocity },
				{ Dimension.AngularVelocity, MeasurementType.Velocity },
				{ Dimension.Acceleration, MeasurementType.Acceleration },
				{ Dimension.AngularAcceleration, MeasurementType.Acceleration },
				{ Dimension.AlternatingCurrent, MeasurementType.CurrentAC },
				{ Dimension.DirectCurrent, MeasurementType.Current },
				{ Dimension.Force, MeasurementType.Force }
			};

		private static readonly IDictionary<Dimension, MotionType> _motionTypesByDimension =
			new Dictionary<Dimension, MotionType>
			{
				{ Dimension.Length, MotionType.Linear },
				{ Dimension.Angle, MotionType.Rotary },
				{ Dimension.Velocity, MotionType.Linear },
				{ Dimension.AngularVelocity, MotionType.Rotary },
				{ Dimension.Acceleration, MotionType.Linear },
				{ Dimension.AngularAcceleration, MotionType.Rotary }
			};

		private static readonly IDictionary<Tuple<MotionType, MeasurementType>, Dimension> _dimensionsByTypes =
			new Dictionary<Tuple<MotionType, MeasurementType>, Dimension>
			{
				{
					new Tuple<MotionType, MeasurementType>(MotionType.Linear, MeasurementType.Position),
					Dimension.Length
				},
				{
					new Tuple<MotionType, MeasurementType>(MotionType.Linear, MeasurementType.Velocity),
					Dimension.Velocity
				},
				{
					new Tuple<MotionType, MeasurementType>(MotionType.Linear, MeasurementType.Acceleration),
					Dimension.Acceleration
				},
				{
					new Tuple<MotionType, MeasurementType>(MotionType.Rotary, MeasurementType.Position), Dimension.Angle
				},
				{
					new Tuple<MotionType, MeasurementType>(MotionType.Rotary, MeasurementType.Velocity),
					Dimension.AngularVelocity
				},
				{
					new Tuple<MotionType, MeasurementType>(MotionType.Rotary, MeasurementType.Acceleration),
					Dimension.AngularAcceleration
				}
			};

		#endregion
	}
}

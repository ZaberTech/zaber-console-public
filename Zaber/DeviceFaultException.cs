using System;
using System.Globalization;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Zaber
{
	/// <summary>
	///     Exception thrown by a <see cref="Conversation" /> when the response has
	///     a fault flag.
	/// </summary>
	[Serializable]
	public class DeviceFaultException : ConversationException
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor. Initializes the exception with a generic message.
		/// </summary>
		public DeviceFaultException()
			: base("Device fault reported.")
		{
		}


		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aMessage">The message to display</param>
		public DeviceFaultException(string aMessage)
			: base(aMessage)
		{
		}


		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aMessage">The message to display</param>
		/// <param name="aException">The exception that caused this exception</param>
		public DeviceFaultException(string aMessage, Exception aException)
			: base(aMessage, aException)
		{
		}


		/// <summary>
		///     Initializes a new instance with serialized
		///     data.
		/// </summary>
		/// <param name="aSerializationInfo">
		///     The System.Runtime.Serialization.SerializationInfo that holds the serialized
		///     object data about the exception being thrown.
		/// </param>
		/// <param name="aContext">
		///     The System.Runtime.Serialization.StreamingContext that contains contextual
		///     information about the source or destination.
		/// </param>
		protected DeviceFaultException(SerializationInfo aSerializationInfo, StreamingContext aContext)
			: base(aSerializationInfo, aContext)
		{
			if (null == aSerializationInfo)
			{
				throw new ArgumentNullException("info");
			}

			Response = (DeviceMessage) aSerializationInfo.GetValue("response", typeof(DeviceMessage));
		}


		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aResponse">The response that caused the exception</param>
		public DeviceFaultException(DeviceMessage aResponse)
			: base(BuildMessage(aResponse))
		{
			Response = aResponse;
		}


		/// <summary>
		///     Sets the System.Runtime.Serialization.SerializationInfo
		///     with information about the exception.
		/// </summary>
		/// <param name="aSerializationInfo">
		///     The System.Runtime.Serialization.SerializationInfo that holds the serialized
		///     object data about the exception being thrown.
		/// </param>
		/// <param name="aContext">
		///     The System.Runtime.Serialization.StreamingContext that contains contextual
		///     information about the source or destination.
		/// </param>
		/// <exception cref="ArgumentNullException">The info parameter is a null reference.</exception>
		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		public override void GetObjectData(SerializationInfo aSerializationInfo, StreamingContext aContext)
		{
			if (null == aSerializationInfo)
			{
				throw new ArgumentNullException("info");
			}

			base.GetObjectData(aSerializationInfo, aContext);

			aSerializationInfo.AddValue("response", Response);
		}


		/// <summary>
		///     The response message from the device.
		/// </summary>
		public DeviceMessage Response { get; }

		#endregion

		#region -- Non-Public Methods --

		private static string BuildMessage(DeviceMessage aResponse)
			=> string.Format(CultureInfo.CurrentCulture,
							 "{3}: Fault {0} reported by device {1}{2}. See protocol manual or send "
						 + "command '/{1} help warnflags' to see full description.",
							 aResponse.FlagText,
							 aResponse.DeviceNumber,
							 0 == aResponse.AxisNumber
								 ? string.Empty
								 : " axis "
							   + aResponse.AxisNumber,
							 typeof(DeviceFaultException).Name);

		#endregion
	}
}

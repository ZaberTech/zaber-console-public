﻿namespace Zaber
{
	/// <summary>
	///     Describes scaling functions which are to be applied to convert between
	///     real-life units of measure and Zaber device data units.
	/// </summary>
	public enum ScalingFunction
	{
		/// <summary>
		///     Device units = scale * physical units.
		/// </summary>
		Linear,

		/// <summary>
		///     Device units = scale / physical units.
		/// </summary>
		Reciprocal,

		/// <summary>
		///     Device units = scale * resolution * physical units.
		/// </summary>
		LinearResolution,

		/// <summary>
		///     Device units = scale * resolution * tan(physical units * Pi / 180)
		/// </summary>
		/// <remarks>
		///     Warning: This formula can only be applied to absolute
		///     positions. It is not applicable to relative positions, velocities,
		///     or accelerations.
		/// </remarks>
		TangentialResolution
	}
}

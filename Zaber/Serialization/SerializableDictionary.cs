﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Zaber.Serialization
{
	/// <summary>
	///     Generic dictionary class that supports XML serialization.
	///     Adapted from https://www.codeproject.com/Questions/454134/Serialize-Dictionary-in-csharp
	///     Falls under the Code Project Open License: http://www.codeproject.com/info/cpol10.aspx
	/// </summary>
	/// <typeparam name="TKey">Key type. Must itself be serializable.</typeparam>
	/// <typeparam name="TValue">Value type. Must itself be serializable.</typeparam>
	[XmlRoot("Dictionary")]
	public class SerializableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, IXmlSerializable
	{
		#region -- IXmlSerializable Members --

		/// <summary>
		///     Method not implemented as specified in IXmlSerializable documentaiton.
		/// </summary>
		/// <returns>null</returns>
		public XmlSchema GetSchema() => null;


		/// <summary>
		///     Deserialize the dictionary from an XML reader.
		/// </summary>
		/// <param name="aReader">XML parser already at the start of the dictionary content.</param>
		public void ReadXml(XmlReader aReader)
		{
			var keySerializer = new XmlSerializer(typeof(TKey));
			var valueSerializer = new XmlSerializer(typeof(TValue));
			var wasEmpty = aReader.IsEmptyElement;
			aReader.Read();
			if (wasEmpty)
			{
				return;
			}

			while (XmlNodeType.EndElement != aReader.NodeType)
			{
				aReader.ReadStartElement("Item");

				aReader.ReadStartElement("Key");
				var key = (TKey) keySerializer.Deserialize(aReader);
				aReader.ReadEndElement();

				aReader.ReadStartElement("Value");
				var value = (TValue) valueSerializer.Deserialize(aReader);
				aReader.ReadEndElement();

				Add(key, value);

				aReader.ReadEndElement();
				aReader.MoveToContent();
			}

			aReader.ReadEndElement();
		}


		/// <summary>
		///     Save the dictionary content to an XML writer.
		/// </summary>
		/// <param name="aWriter">The write to use to store the data.</param>
		public void WriteXml(XmlWriter aWriter)
		{
			var keySerializer = new XmlSerializer(typeof(TKey));
			var valueSerializer = new XmlSerializer(typeof(TValue));

			foreach (var key in Keys)
			{
				aWriter.WriteStartElement("Item");

				aWriter.WriteStartElement("Key");
				keySerializer.Serialize(aWriter, key);
				aWriter.WriteEndElement();

				aWriter.WriteStartElement("Value");
				var value = this[key];
				valueSerializer.Serialize(aWriter, value);
				aWriter.WriteEndElement();

				aWriter.WriteEndElement();
			}
		}

		#endregion
	}
}

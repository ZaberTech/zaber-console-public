﻿using System;

namespace Zaber.Serialization
{
	/// <summary>
	///     Helper class for serializing generic 2-tuples. Has implicit cast operators
	///     to regular generic tuples, and a parameterless constructor to support serialization.
	/// </summary>
	/// <typeparam name="T1">Type of the first element. Must be a serializable type.</typeparam>
	/// <typeparam name="T2">Type of the second element. Must be a serializable type.</typeparam>
	public class SerializableTuple<T1, T2>
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Cast operator for converting from a generic tuple.
		/// </summary>
		/// <param name="aTuple">Data to convert.</param>
		public static implicit operator SerializableTuple<T1, T2>(Tuple<T1, T2> aTuple) => new SerializableTuple<T1, T2>
		{
			Item1 = aTuple.Item1,
			Item2 = aTuple.Item2
		};


		/// <summary>
		///     Cast operator for converting to a generic tuple.
		/// </summary>
		/// <param name="aTuple">Data to convert.</param>
		public static implicit operator Tuple<T1, T2>(SerializableTuple<T1, T2> aTuple)
			=> new Tuple<T1, T2>(aTuple.Item1, aTuple.Item2);


		/// <summary>
		///     First item.
		/// </summary>
		public T1 Item1 { get; set; }


		/// <summary>
		///     Second item.
		/// </summary>
		public T2 Item2 { get; set; }

		#endregion
	}
}

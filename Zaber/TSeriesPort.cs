﻿using System.IO.Ports;
using Zaber.Ports;

namespace Zaber
{
	/// <summary>
	///     DEPRECATED - This class exists to support legacy code. New code that specifically needs to use a
	///     serial port should use <see cref="Ports.RS232Port" /> instead.
	/// </summary>
	public class TSeriesPort : RS232Port
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Creates a new instance.
		/// </summary>
		/// <param name="aSerialPort">Serial port to communicate with.</param>
		/// <param name="aPacketConverter">
		///     Used to convert between the byte stream
		///     and the <see cref="DataPacket" /> structure.
		/// </param>
		public TSeriesPort(SerialPort aSerialPort, PacketConverter aPacketConverter)
			: base(aSerialPort, aPacketConverter)
		{
		}

		#endregion
	}
}

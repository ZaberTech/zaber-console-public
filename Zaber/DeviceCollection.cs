using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Zaber
{
	/// <summary>
	///     A collection of <see cref="ZaberDevice" /> objects. It's used to represent
	///     an Alias device number.
	/// </summary>
	/// <remarks>
	///     This class is both a ZaberDevice and a collection of them. Any requests
	///     you send to it get received by all its member devices. (The hardware handles
	///     that.) Any request or response events from its members will also be raised
	///     by the collection. (The software handles that.)
	/// </remarks>
	public class DeviceCollection : ZaberDevice, IList<ZaberDevice>
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Get identifying information about this device.
		/// </summary>
		/// <returns>Device identity for the "all devices" collection.</returns>
		[CLSCompliant(false)]
		public override DeviceIdentity GetIdentity()
		{
			var result = base.GetIdentity();
			result.IsAllDevices = true;
			return result;
		}


		#region -- IEnumerable<ZaberDevice> Implementation --

		/// <summary>
		///     Returns an enumerator that iterates through the collection.
		/// </summary>
		/// <returns>an enumerator that iterates through the collection.</returns>
		public IEnumerator<ZaberDevice> GetEnumerator() => _members.GetEnumerator();

		#endregion

		/// <summary>
		///     True if this is a single device, and not a collection of devices.
		/// </summary>
		public override bool IsSingleDevice => false;


		/// <inheritdoc cref="ZaberDevice.AreAsciiMessageIdsEnabled" />
		public override bool AreAsciiMessageIdsEnabled
		{
			get
			{
				// For the collection, return true if any contained device
				// has IDs enabled - see ticket RM4327. If we don't do this
				// and a device with IDs enabled has message ID 0 already in
				// use, we will get a false ID exhaustion error.
				foreach (var device in _members)
				{
					if (device.AreAsciiMessageIdsEnabled)
					{
						return true;
					}
				}

				return false;
			}

			set { }
		}

		#endregion

		#region -- Interfaces --

		#region -- IEnumerable Implementation --

		/// <summary>
		///     Returns an enumerator that iterates through the collection.
		/// </summary>
		/// <returns>an enumerator that iterates through the collection.</returns>
		IEnumerator IEnumerable.GetEnumerator() => _members.GetEnumerator();

		#endregion

		#region -- ICollection<ZaberDevice> Implementation --

		/// <summary>
		///     Adds a device to the collection.
		/// </summary>
		/// <param name="aItem">The device to add to the collection.</param>
		public void Add(ZaberDevice aItem)
		{
			if (0 == _members.Count)
			{
				_isDeviceTypeNameCalculated = null == DeviceType.Name;
				DeviceType = new DeviceType(DeviceType)
				{
					Commands = aItem.DeviceType.Commands // Copy commands.
				};
			}
			else
			{
				var textCommands =
					new HashSet<string>(DeviceType.Commands.Select(cmd => cmd.TextCommand).Where(text => null != text));
				List<CommandInfo> newCommands = null;

				foreach (var command in aItem.DeviceType.Commands)
				{
					var isNew = null != command.TextCommand
						? !textCommands.Contains(command.TextCommand)
						: null == DeviceType.GetCommandByNumber(command.Command);
					if (isNew)
					{
						if (null == newCommands)
						{
							newCommands = new List<CommandInfo>(DeviceType.Commands);
						}

						newCommands.Add(command);
					}
				}

				if (null != newCommands)
				{
					DeviceType.Commands = newCommands;
				}
			}

			_members.Add(aItem);

			aItem.MessageSent += Item_MessageSent;
			aItem.MessageReceived += Item_MessageReceived;

			if (_isDeviceTypeNameCalculated)
			{
				var deviceNumbers = _members.Select(device => device.DeviceNumber.ToString(CultureInfo.CurrentCulture))
										 .ToList();
				deviceNumbers.Sort();
				DeviceType.Name = string.Format(CultureInfo.CurrentCulture,
												"Alias ({0})",
												string.Join(", ", deviceNumbers.ToArray()));
			}
		}


		/// <summary>
		///     Removes all devices from the collection.
		/// </summary>
		public void Clear()
		{
			foreach (var member in _members)
			{
				UnregisterFrom(member);
			}

			_members.Clear();
		}


		/// <summary>
		///     Determines whether the collection contains a specific device.
		/// </summary>
		/// <param name="aItem">The device to locate in the collection</param>
		/// <returns>true if the device is found in the collection, otherwise false.</returns>
		public bool Contains(ZaberDevice aItem) => _members.Contains(aItem);


		/// <summary>
		///     Copies the devices in the collection to a System.Array,
		///     starting at a particular System.Array index.
		/// </summary>
		/// <param name="aArray">
		///     The one-dimensional System.Array that is the
		///     destination of the devices copied from the collection. The
		///     System.Array must have zero-based indexing.
		/// </param>
		/// <param name="aArrayIndex">
		///     The zero-based index in array at which
		///     copying begins.
		/// </param>
		/// <exception cref="ArgumentOutOfRangeException">
		///     arrayIndex is less
		///     than 0.
		/// </exception>
		/// <exception cref="ArgumentNullException">array is null.</exception>
		/// <exception cref="ArgumentException">
		///     arrayIndex is equal to or
		///     greater than the length of array.-or-The number of elements in the
		///     source collection is greater than the available space from
		///     arrayIndex to the end of the destination array.
		/// </exception>
		public void CopyTo(ZaberDevice[] aArray, int aArrayIndex) => _members.CopyTo(aArray, aArrayIndex);


		/// <summary>
		///     Gets the number of devices contained in the collection.
		/// </summary>
		public int Count => _members.Count;


		/// <summary>
		///     Gets a value indicating whether the collection is read-only.
		/// </summary>
		public bool IsReadOnly => false;


		/// <summary>
		///     Removes the first occurrence of a specific device from the collection.
		/// </summary>
		/// <param name="aItem">The device to remove from the collection.</param>
		/// <returns>
		///     true if the device was successfully removed from the collection,
		///     otherwise false. This method also returns false if the device is not found in
		///     the collection.
		/// </returns>
		public bool Remove(ZaberDevice aItem)
		{
			UnregisterFrom(aItem);
			return _members.Remove(aItem);
		}

		#endregion

		#region -- IList<ZaberDevice> Implementation --

		/// <summary>
		///     Searches for the specified device and returns the zero-based index of the
		///     first occurrence within the entire list.
		/// </summary>
		/// <param name="aItem">
		///     The device to locate in the list. The value
		///     can be null.
		/// </param>
		/// <returns>
		///     The zero-based index of the first occurrence of item within the entire
		///     list, if found; otherwise, �1.
		/// </returns>
		public int IndexOf(ZaberDevice aItem) => _members.IndexOf(aItem);


		/// <summary>
		///     Inserts an element into the list at the specified index.
		/// </summary>
		/// <param name="aIndex">The zero-based index at which item should be inserted.</param>
		/// <param name="aItem">The object to insert. The value can be null.</param>
		/// <exception cref="ArgumentOutOfRangeException">
		///     index is less than 0.
		///     -or- index is greater than <see cref="Count" />.
		/// </exception>
		public void Insert(int aIndex, ZaberDevice aItem) => _members.Insert(aIndex, aItem);


		/// <summary>
		///     Removes the element at the specified index of the list.
		/// </summary>
		/// <param name="aIndex">The zero-based index of the element to remove.</param>
		/// <exception cref="ArgumentOutOfRangeException">
		///     index is less than 0.
		///     -or- index is equal to or greater than <see cref="Count" />.
		/// </exception>
		public void RemoveAt(int aIndex) => _members.RemoveAt(aIndex);


		/// <summary>
		///     Gets or sets the element at the specified index.
		/// </summary>
		/// <param name="aIndex">The zero-based index of the element to get or set.</param>
		/// <returns>The element at the specified index.</returns>
		/// <exception cref="ArgumentOutOfRangeException">
		///     index is less than 0.
		///     -or- index is equal to or greater than <see cref="Count" />.
		/// </exception>
		public ZaberDevice this[int aIndex]
		{
			get => _members[aIndex];
			set => _members[aIndex] = value;
		}


		/// <summary>
		///     Prime the device collection to expect a new microstep resolution on one or more devices.
		/// </summary>
		/// <param name="aMessage">The message triggering the change.</param>
		protected override void ExpectNewResolution(DeviceMessage aMessage)
		{
			// The message is targeted at all devices, and this is the "all devices" container.
			// Set up all devices to expect a response to resolution change.
			foreach (var device in this)
			{
				SetExpectedResolutionChange(device, aMessage.AxisNumber, (int)aMessage.NumericData);
			}
		}

		#endregion

		#endregion

		#region -- Non-Public Methods --

		private void Item_MessageReceived(object aSender, DeviceMessageEventArgs aArgs)
		{
			if (IsOriginalSender(aSender, aArgs))
			{
				OnMessageReceived(aArgs);
			}
		}


		private static bool IsOriginalSender(object aSender, DeviceMessageEventArgs aArgs)
		{
			var device = (ZaberDevice) aSender;
			return device.DeviceNumber == aArgs.DeviceMessage.DeviceNumber;
		}


		private void Item_MessageSent(object aSender, DeviceMessageEventArgs aArgs)
		{
			if (IsOriginalSender(aSender, aArgs))
			{
				OnMessageSent(aArgs);
			}
		}


		private void UnregisterFrom(ZaberDevice aItem)
		{
			aItem.MessageReceived -= Item_MessageReceived;
			aItem.MessageSent -= Item_MessageSent;
		}

		#endregion

		#region -- Data --

		private readonly List<ZaberDevice> _members = new List<ZaberDevice>();
		private bool _isDeviceTypeNameCalculated;

		#endregion
	}
}

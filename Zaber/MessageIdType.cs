﻿namespace Zaber
{
	/// <summary>
	///     Indicates the type of the message ID in a message.
	/// </summary>
	public enum MessageIdType
	{
		/// <summary>
		///     Message ID is not used.
		/// </summary>
		None,

		/// <summary>
		///     A numeric message ID is present in the message.
		/// </summary>
		Numeric,

		/// <summary>
		///     A "--" in the message ID field of the ASCII command
		///     indicates that the device should not respond with
		///     a reply message.
		/// </summary>
		/// <remarks>
		///     This type is not supported in Binary messages.
		/// </remarks>
		SuppressReply
	}
}

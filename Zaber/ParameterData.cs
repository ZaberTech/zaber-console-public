﻿using System.Collections.Generic;
using System.Globalization;

namespace Zaber
{
	/// <summary>
	///     Represents raw data, unit conversion and formatting info for one command parameter
	///     being passed to
	///     <see
	///         cref="ZaberDevice.SendFormatInUnits(string, System.Collections.Generic.IEnumerable{Zaber.ParameterData}, byte)" />
	///     .
	/// </summary>
	public class ParameterData
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Helper constructor for initializing all fields.
		/// </summary>
		/// <param name="aParamindex">Index of the command parameter this data is intended for.</param>
		/// <param name="aData">Data to be converted.</param>
		/// <param name="aType">Information about the parameter the data is applied to.</param>
		/// <param name="aUnit">The unit of measure of the data, if applicable.</param>
		public ParameterData(int aParamindex, string aData, ParameterType aType, UnitOfMeasure aUnit)
		{
			ParameterIndex = aParamindex;
			Data = aData;
			ParamType = aType;
			SourceUnit = aUnit;
		}


		/// <summary>
		///     Attempt to interpret the contents of <see cref="Data" /> as a number using either the
		///     current culture or the invariant culture.
		/// </summary>
		/// <param name="aValue">Converted numeric value if successful.</param>
		/// <returns>True if parsing was successful.</returns>
		public bool TryParseData(out decimal aValue)
			=> decimal.TryParse(Data, NumberStyles.Float, CultureInfo.CurrentCulture, out aValue)
		   || decimal.TryParse(Data, NumberStyles.Float, CultureInfo.InvariantCulture, out aValue);


		/// <summary>
		///     Unformatted, user-entered data. This may be numeric, an enum value name, or free-form string.
		/// </summary>
		public string Data { get; set; }


		/// <summary>
		///     The type of the parameter the data is being applied to. For numeric parameters, this
		///     is the source of the precision information.
		/// </summary>
		public ParameterType ParamType { get; set; }


		/// <summary>
		///     The user-selected unit of measure for the quantity expressed in <see cref="Data" />, if
		///     the parameter type is numeric.
		/// </summary>
		public UnitOfMeasure SourceUnit { get; set; }


		/// <summary>
		///     Index of the command parameter this data us meant to satisfy.
		/// </summary>
		public int ParameterIndex { get; set; }

		#endregion
	}
}

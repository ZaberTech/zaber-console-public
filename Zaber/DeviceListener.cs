﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Zaber
{
	/// <summary>
	///     Registering for response events from a <see cref="ZaberDevice" /> can be
	///     awkward, especially in scripts. This class registers for the response
	///     events and stores them until you request them with the
	///     NextResponse() method. Listening is started automatically when you
	///     create a listener, but you can stop and start listening with the
	///     Stop() and Start() methods.
	/// </summary>
	public class DeviceListener : ResponseListener<DeviceMessage>
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Create a new instance.
		/// </summary>
		/// <param name="aDevice">The device to listen to for responses.</param>
		public DeviceListener(ZaberDevice aDevice)
		{
			_device = aDevice;
			Start();
		}


		/// <summary>
		///     Create a new instance.
		/// </summary>
		/// <param name="aConversation">
		///     The conversation to listen to for
		///     responses.
		/// </param>
		public DeviceListener(Conversation aConversation)
			: this(aConversation.Device)
		{
		}


		/// <summary>
		///     This method is for ASCII only. Collect all replies to a command that produces multiple responses from a single
		///     device.
		///     This function blocks until the end of responses is detected or the timeout expires.
		///     NOTE this version does not allow use of message IDs.
		/// </summary>
		/// <param name="aDevice">Device to listen to.</param>
		/// <param name="aRequest">Code that sends the command to the device.</param>
		/// <param name="aResponseHandler">
		///     Code that processes each response from the device, including the initial response to the
		///     command.
		/// </param>
		/// <param name="aTimeout">Optional timeout; response collection will end if this timeout elapses between responses.</param>
		/// <remarks>
		///     This method will not work correctly if addressed to multiple devices or axes.
		///     For firmware versions earlier than 6.14, this method always relies on the timeout to decide
		///     when the list is finished; you must use a non-infinite timeout with such firmware.
		/// </remarks>
		[Obsolete("Use the version that takes a Conversation instead of a ZaberDevice.")]
		public static void ReceiveListResponse(ZaberDevice aDevice, Action aRequest,
											   Action<DeviceMessage> aResponseHandler, TimeoutTimer aTimeout = null)
		{
			// With firmware 6.14 and up, we can detect the end of an info list faster by
			// sending an extra empty command to the device. The firmware guarantees a response
			// to the first command, then all of the info messages, then a response to the second command.
			var useEarlyTerminationTrick = aDevice.DeviceType.FirmwareVersion >= 614;
			var breakOnNextReply = false;

			if (!useEarlyTerminationTrick && ((aTimeout is null) || (Timeout.Infinite == aTimeout.Timeout)))
			{
				throw new ArgumentException("A finite timeout is required with firmware older than v6.14.");
			}

			using (var listener = new DeviceListener(aDevice))
			{
				var loop = true;

				// Make the request.
				try
				{
					aRequest();
					if (useEarlyTerminationTrick)
					{
						aDevice.Send(string.Empty); // The reply to this will be discarded below.
					}
				}
				catch (ErrorResponseException)
				{
					// Don't wait if the command was rejected.
					loop = false;
				}
				catch (ZaberPortErrorException aException)
				{
					// Ignore malformed packets and attempt to carry on.
					if (ZaberPortError.InvalidPacket != aException.ErrorType)
					{
						throw;
					}
				}

				// Receive responses.
				while (loop)
				{
					try
					{
						var response = (aTimeout is null) ? listener.NextResponse() : listener.NextResponse(aTimeout);
						if (useEarlyTerminationTrick && (MessageType.Response == response.MessageType))
						{
							if (breakOnNextReply)
							{
								// Discard list terminating response if used, and end the list.
								break;
							}

							breakOnNextReply = true;
						}

						aResponseHandler(response);
					}
					catch (RequestTimeoutException)
					{
						// Time out also ends the list. 
						break;
					}
				}
			}
		}


		/// <summary>
		///     This method is for ASCII only. Collect all replies to a command that produces multiple responses from a single
		///     device.
		///     This function blocks until the end of responses is detected or the timeout expires.
		/// </summary>
		/// <param name="aConversation">Conversation with the device to listen to.</param>
		/// <param name="aRequest">Code that sends the command to the device.</param>
		/// <param name="aResponseHandler">
		///     Code that processes each response from the device, including the initial response to the
		///     command.
		/// </param>
		/// <param name="aTimeout">Optional timeout; response collection will end if this timeout elapses between responses.</param>
		/// <remarks>
		///     This method will not work correctly if addressed to multiple devices or axes.
		///     For firmware versions earlier than 6.14, this method always relies on the timeout to decide
		///     when the list is finished; you must use a non-infinite timeout with such firmware.
		///     The caller-provided callbacks must use the message ID of the provided ConversationTopic
		///     in the command they send that will generate the list response.
		/// </remarks>
		public static void ReceiveListResponse(Conversation aConversation, Action<ConversationTopic> aRequest,
											   Action<DeviceMessage> aResponseHandler, TimeoutTimer aTimeout = null)
		{
			// With firmware 6.14 and up, we can detect the end of an info list faster by
			// sending an extra empty command to the device. The firmware guarantees a response
			// to the first command, then all of the info messages, then a response to the second command.
			var device = aConversation.Device;
			var useEarlyTerminationTrick = device.DeviceType.FirmwareVersion >= 614;
			var breakOnNextReply = false;

			if (!useEarlyTerminationTrick && ((aTimeout is null) || (Timeout.Infinite == aTimeout.Timeout)))
			{
				throw new ArgumentException("A finite timeout is required with firmware older than v6.14.");
			}

			var topic = aConversation.StartTopic();
			ConversationTopic footerTopic = null;

			var matchFilter = new Predicate<DeviceMessage>(aMessage => (aMessage.MessageId == topic.MessageId) || (aMessage.MessageId == footerTopic?.MessageId));

			using (var listener = new DeviceListener(device) { Filter = matchFilter })
			{
				var loop = true;

				// Make the request.
				try
				{
					aRequest(topic);
					if (useEarlyTerminationTrick)
					{
						footerTopic = aConversation.StartTopic();
						footerTopic.EnableTimeouts = false; // The timeout on this one should be relative to the last info message.
						device.Send(string.Empty, footerTopic.MessageId); // The reply to this will be discarded below.
					}
				}
				catch (ErrorResponseException)
				{
					// Don't wait if the command was rejected.
					loop = false;
				}
				catch (ZaberPortErrorException aException)
				{
					// Ignore malformed packets and attempt to carry on.
					if (ZaberPortError.InvalidPacket != aException.ErrorType)
					{
						// Ensure this topic gets cleaned up on error.
						if (footerTopic != null)
						{
							footerTopic.EnableTimeouts = true;
							footerTopic.StartTimeout(aConversation.Timeout);
						}

						throw;
					}
				}
				catch
				{
					// Ensure this topic gets cleaned up on error.
					if (footerTopic != null)
					{
						footerTopic.EnableTimeouts = true;
						footerTopic.StartTimeout(aConversation.Timeout);
					}

					throw;
				}

				// Receive responses.
				while (loop)
				{
					try
					{
						var response = (aTimeout is null) ? listener.NextResponse() : listener.NextResponse(aTimeout);
						if (useEarlyTerminationTrick && (MessageType.Response == response.MessageType))
						{
							if (breakOnNextReply)
							{
								// Discard list terminating response if used, and end the list.
								break;
							}

							breakOnNextReply = true;
						}

						aResponseHandler(response);
					}
					catch (RequestTimeoutException)
					{
						// Time out also ends the list.
						// Note this can cause items to be dropped from the end of the list
						// if the port is congested and the timeout is too short.
						break;
					}
				}
			}

			topic.Validate();
			if (null != footerTopic)
			{
				footerTopic.EnableTimeouts = true;
				footerTopic.StartTimeout(aTimeout?.Timeout ?? aConversation.Timeout);
				if (null != aTimeout)
				{
					footerTopic.Wait(aTimeout);
				}
				else
				{
					footerTopic.Wait();
				}

				footerTopic.Validate();
			}
		}


		/// <summary>
		///     Send an ASCII command and collect all of the info response content into a string collection.
		///     Does not include the response to the initial command. This function blocks until all info
		///     messages have been received and/or the timeout expires.
		///     NOTE this version does not allow use of message IDs; prefer
		///     <see cref="ReceiveInfoListContent(Conversation, string, TimeoutTimer)"/> instead.
		/// </summary>
		/// <param name="aDevice">Device to send the command to.</param>
		/// <param name="aCommand">Command to send.</param>
		/// <param name="aTimeout">Optional timeout to terminate waiting for responses.</param>
		/// <returns>
		///     A collection of zero or more strings containing the bodies of all info
		///     messages resulting from the command. Reply message contents are not included.
		/// </returns>
		[Obsolete("Use the version that takes a Conversation instead of a ZaberDevice.")]
		public static IEnumerable<string> ReceiveInfoListContent(ZaberDevice aDevice, string aCommand,
																 TimeoutTimer aTimeout = null)
		{
			var responses = new List<string>();

			var request = new Action(() => { aDevice.Send(aCommand); });

			var handler = new Action<DeviceMessage>(aMessage =>
			{
				if (MessageType.Comment == aMessage.MessageType)
				{
					var body = aMessage.Body.Trim();
					if (body.Length > 0)
					{
						responses.Add(body);
					}
				}
			});

			ReceiveListResponse(aDevice, request, handler, aTimeout);

			return responses;
		}


		/// <summary>
		///     Send an ASCII command and collect all of the info response content into a string collection.
		///     Does not include the response to the initial command. This function blocks until all info
		///     messages have been received and/or the timeout expires.
		/// </summary>
		/// <param name="aConversation">Conversation with the device to send the command to.</param>
		/// <param name="aCommand">Command to send.</param>
		/// <param name="aTimeout">Optional timeout to terminate waiting for responses.</param>
		/// <returns>
		///     A collection of zero or more strings containing the bodies of all info
		///     messages resulting from the command. Reply message contents are not included.
		/// </returns>
		public static IEnumerable<string> ReceiveInfoListContent(Conversation aConversation, string aCommand,
			TimeoutTimer aTimeout = null)
		{
			var responses = new List<string>();

			var request = new Action<ConversationTopic>(aTopic =>
			{
				aConversation.Device.Send(aCommand, aTopic.MessageId);
			});

			var handler = new Action<DeviceMessage>(aMessage =>
			{
				if (MessageType.Comment == aMessage.MessageType)
				{
					var body = aMessage.Body.Trim();
					if (body.Length > 0)
					{
						responses.Add(body);
					}
				}
			});

			ReceiveListResponse(aConversation, request, handler, aTimeout);

			return responses;
		}

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Register with the device.
		/// </summary>
		protected override void RegisterEvent() => _device.MessageReceived += Device_MessageReceived;


		/// <summary>
		///     Unregister from the device.
		/// </summary>
		protected override void UnregisterEvent() => _device.MessageReceived -= Device_MessageReceived;


		/// <summary>
		///     Handle a response event from the device.
		/// </summary>
		/// <param name="aSender">The device sending the response.</param>
		/// <param name="aArgs">The details of the response.</param>
		private void Device_MessageReceived(object aSender, DeviceMessageEventArgs aArgs)
			=> OnItemReceived(aArgs.DeviceMessage);

		#endregion

		#region -- Data --

		private readonly ZaberDevice _device;

		#endregion
	}
}

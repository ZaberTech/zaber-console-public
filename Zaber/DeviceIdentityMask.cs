﻿using System;

namespace Zaber
{
	/// <summary>
	///     Mask bits for stripping information from <see cref="DeviceIdentity" /> objects
	///     in preparation for doing exact matches.
	/// </summary>
	[Flags]
	public enum DeviceIdentityMask
	{
		/// <summary>
		///     Ignore all identity information.
		/// </summary>
		None = 0,

		/// <summary>
		///     Consider serial number matches.
		/// </summary>
		SerialNumber = 1,

		/// <summary>
		///     Consider device type matches.
		/// </summary>
		DeviceId = 2,

		/// <summary>
		///     Consider peripheral type matches.
		/// </summary>
		PeripheralId = 4,

		/// <summary>
		///     Consider device number matches.
		/// </summary>
		DeviceNumber = 8,

		/// <summary>
		///     Consider axis number matches.
		/// </summary>
		AxisNumber = 16,

		/// <summary>
		///     Consider "all devices" status matches.
		/// </summary>
		IsAllDevices = 32,

		/// <summary>
		///     Use all identity information.
		/// </summary>
		All = SerialNumber | DeviceId | PeripheralId | DeviceNumber | AxisNumber | IsAllDevices
	}
}

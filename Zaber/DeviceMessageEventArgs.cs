using System;

namespace Zaber
{
	/// <summary>
	///     Notification of a request or response from a <see cref="ZaberDevice" />.
	/// </summary>
	public class DeviceMessageEventArgs : EventArgs
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aSeviceMessage">See <see cref="DeviceMessage" /></param>
		public DeviceMessageEventArgs(DeviceMessage aSeviceMessage)
		{
			DeviceMessage = aSeviceMessage;
		}


		/// <summary>
		///     The details of the device's request or response.
		/// </summary>
		public DeviceMessage DeviceMessage { get; set; }

		#endregion

		#region -- Data --

		#endregion
	}
}

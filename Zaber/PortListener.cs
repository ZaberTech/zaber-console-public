﻿namespace Zaber
{
	/// <summary>
	///     Registering for response events from a port can be
	///     awkward, especially in scripts. This class registers for the response
	///     events and stores them until you request them with the
	///     NextResponse() method. Listening is started automatically when you
	///     create a listener, but you can stop and start listening with the
	///     Stop() and Start() methods.
	/// </summary>
	public class PortListener : ResponseListener<DataPacket>
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Create a new instance.
		/// </summary>
		/// <param name="aPort">The port to listen to for responses.</param>
		public PortListener(IZaberPort aPort)
		{
			_port = aPort;
			Start();
		}

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Register with the port.
		/// </summary>
		protected override void RegisterEvent() => _port.DataPacketReceived += port_DataPacketReceived;


		/// <summary>
		///     Unregister from the port.
		/// </summary>
		protected override void UnregisterEvent() => _port.DataPacketReceived -= port_DataPacketReceived;


		/// <summary>
		///     Handle a response event from the port.
		/// </summary>
		/// <param name="aSender">The port sending the response.</param>
		/// <param name="aArgs">The details of the response.</param>
		private void port_DataPacketReceived(object aSender, DataPacketEventArgs aArgs)
			=> OnItemReceived(aArgs.DataPacket);

		#endregion

		#region -- Data --

		private readonly IZaberPort _port;

		#endregion
	}
}

﻿using System;
using System.Globalization;

namespace Zaber
{
	/// <summary>
	///     Holds the combination of a measured value and its unit of measure.
	/// </summary>
	[Serializable]
	public class Measurement
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Initialize a new instance.
		/// </summary>
		/// <param name="aMeasurement">The measurement to make a copy of.</param>
		public Measurement(Measurement aMeasurement)
		{
			Value = aMeasurement.Value;
			Unit = aMeasurement.Unit;
		}


		/// <summary>
		///     Initialize a new instance.
		/// </summary>
		/// <param name="aValue">The measured value.</param>
		/// <param name="aUnit">What units the measured value is in.</param>
		public Measurement(decimal aValue, UnitOfMeasure aUnit)
		{
			Value = aValue;
			Unit = aUnit;
		}


		/// <summary>
		///     Initialize a new instance.
		/// </summary>
		/// <param name="aValue">The measured value.</param>
		/// <param name="aUnit">What units the measured value is in.</param>
		public Measurement(double aValue, UnitOfMeasure aUnit)
		{
			Value = (decimal) aValue;
			Unit = aUnit;
		}


		/// <summary>
		///     Initialize a new instance.
		/// </summary>
		/// <param name="aValue">The measured value.</param>
		/// <param name="aUnit">What units the measured value is in.</param>
		public Measurement(int aValue, UnitOfMeasure aUnit)
		{
			Value = aValue;
			Unit = aUnit;
		}


		/// <summary>
		///     Copy constructor to convert a measurement to another related unit.
		/// </summary>
		/// <param name="aMeasurement">
		///     The measurement whose value the new
		///     measurement should share.
		/// </param>
		/// <param name="aNewUnit">The new unit to convert the value to.</param>
		public Measurement(Measurement aMeasurement, UnitOfMeasure aNewUnit)
		{
			var newMeasurement = aMeasurement + new Measurement(0, aNewUnit);

			Value = newMeasurement.Value;
			Unit = newMeasurement.Unit;
		}


		/// <summary>
		///     Compare with another object.
		/// </summary>
		/// <param name="aOther">the object to compare with</param>
		/// <returns>
		///     True if it is another Measurement with the same value and
		///     units, otherwise false.
		/// </returns>
		public override bool Equals(object aOther)
		{
			var other = aOther as Measurement;
			if (null == other)
			{
				return false;
			}

			return Equals(Unit, other.Unit) && Equals(Value, other.Value);
		}


		/// <summary>
		///     Serves as a hash function for this type.
		/// </summary>
		/// <returns>A hash code for the current Measurement.</returns>
		public override int GetHashCode() => Unit.GetHashCode() ^ Value.GetHashCode();


		/// <summary>
		///     Convert the Measurement to a string.
		/// </summary>
		/// <returns>A string representation of the Measurement.</returns>
		public override string ToString() => Value + (null == Unit ? string.Empty : Unit.Abbreviation);


		/// <summary>
		///     Adds two specified measurements. Note that if one of the measurements is
		///     in a device-specific unit, the conversion may not be applicable to another
		///     device.
		/// </summary>
		/// <param name="aFirst">A measurement.</param>
		/// <param name="aSecond">A measurement.</param>
		/// <returns>
		///     The sum of the two measurements, using the second
		///     parameter's units of measure.
		/// </returns>
		/// <exception cref="ArgumentException">
		///     The units of the parameters
		///     are incompatible.
		/// </exception>
		public static Measurement operator +(Measurement aFirst, Measurement aSecond)
		{
			var value = aFirst.Value;
			if (aFirst.Unit != aSecond.Unit)
			{
				var converted = ConversionTable.Default.Convert(aFirst, aSecond.Unit);
				if (null == converted)
				{
					throw new ArgumentException(string.Format(CultureInfo.CurrentUICulture,
															  "Units of measure {0} and {1} cannot be added together.",
															  aFirst.Unit,
															  aSecond.Unit));
				}

				value = converted.Value;
			}

			return new Measurement(value + aSecond.Value, aSecond.Unit);
		}


		/// <summary>
		///     Adds two specified measurements.
		/// </summary>
		/// <param name="aFirst">A measurement.</param>
		/// <param name="aSecond">A measurement.</param>
		/// <returns>
		///     The sum of the two measurements, using the second
		///     parameter's units of measure.
		/// </returns>
		/// <exception cref="ArgumentException">
		///     The units of the parameters
		///     are incompatible.
		/// </exception>
		public static Measurement Add(Measurement aFirst, Measurement aSecond) => aFirst + aSecond;


		/// <summary>
		///     Subtracts two specified measurements.
		/// </summary>
		/// <param name="aFirst">A measurement.</param>
		/// <param name="aSecond">A measurement.</param>
		/// <returns>
		///     The second measurement subtracted from the first, using
		///     the second parameter's units of measure.
		/// </returns>
		/// <exception cref="ArgumentException">
		///     The units of the parameters
		///     are incompatible.
		/// </exception>
		public static Measurement operator -(Measurement aFirst, Measurement aSecond) => aFirst + -aSecond;


		/// <summary>
		///     Subtracts two specified measurements.
		/// </summary>
		/// <param name="aFirst">A measurement.</param>
		/// <param name="aSecond">A measurement.</param>
		/// <returns>
		///     The second measurement subtracted from the first, using
		///     the second parameter's units of measure.
		/// </returns>
		/// <exception cref="ArgumentException">
		///     The units of the parameters
		///     are incompatible.
		/// </exception>
		public static Measurement Subtract(Measurement aFirst, Measurement aSecond) => aFirst - aSecond;


		/// <summary>
		///     Negates the value of a specified measurement.
		/// </summary>
		/// <param name="aMeasurement">A measurement.</param>
		/// <returns>The negative value of the measurement.</returns>
		public static Measurement operator -(Measurement aMeasurement)
			=> new Measurement(-aMeasurement.Value, aMeasurement.Unit);


		/// <summary>
		///     Negates the value of a specified measurement.
		/// </summary>
		/// <param name="aMeasurement">A measurement.</param>
		/// <returns>The negative value of the measurement.</returns>
		public static Measurement Negate(Measurement aMeasurement) => -aMeasurement;


		/// <summary>
		///     Multiplies a measurement by a ratio.
		/// </summary>
		/// <param name="aMeasurement">A measurement.</param>
		/// <param name="aRatio">A ratio.</param>
		/// <returns>The measurement multiplied by the ratio.</returns>
		public static Measurement operator *(decimal aRatio, Measurement aMeasurement)
			=> new Measurement(aRatio * aMeasurement.Value, aMeasurement.Unit);


		/// <summary>
		///     Multiplies a measurement by a ratio.
		/// </summary>
		/// <param name="aMeasurement">A measurement.</param>
		/// <param name="aRatio">A ratio.</param>
		/// <returns>The measurement multiplied by the ratio.</returns>
		public static Measurement operator *(double aRatio, Measurement aMeasurement)
			=> (decimal) aRatio * aMeasurement;


		/// <summary>
		///     Multiplies a measurement by a ratio.
		/// </summary>
		/// <param name="aMeasurement">A measurement.</param>
		/// <param name="aRatio">A ratio.</param>
		/// <returns>The measurement multiplied by the ratio.</returns>
		public static Measurement operator *(int aRatio, Measurement aMeasurement) => (decimal) aRatio * aMeasurement;


		/// <summary>
		///     Multiplies a measurement by a ratio.
		/// </summary>
		/// <param name="aMeasurement">A measurement.</param>
		/// <param name="aRatio">A ratio.</param>
		/// <returns>The measurement multiplied by the ratio.</returns>
		public static Measurement operator *(Measurement aMeasurement, decimal aRatio) => aRatio * aMeasurement;


		/// <summary>
		///     Multiplies a measurement by a ratio.
		/// </summary>
		/// <param name="aMeasurement">A measurement.</param>
		/// <param name="aRatio">A ratio.</param>
		/// <returns>The measurement multiplied by the ratio.</returns>
		public static Measurement operator *(Measurement aMeasurement, double aRatio) => aRatio * aMeasurement;


		/// <summary>
		///     Multiplies a measurement by a ratio.
		/// </summary>
		/// <param name="aMeasurement">A measurement.</param>
		/// <param name="aRatio">A ratio.</param>
		/// <returns>The measurement multiplied by the ratio.</returns>
		public static Measurement operator *(Measurement aMeasurement, int aRatio) => aRatio * aMeasurement;


		/// <summary>
		///     Multiplies a measurement by a ratio.
		/// </summary>
		/// <param name="aMeasurement">A measurement.</param>
		/// <param name="aRatio">A ratio.</param>
		/// <returns>The measurement multiplied by the ratio.</returns>
		public static Measurement Multiply(Measurement aMeasurement, double aRatio) => aMeasurement * aRatio;


		/// <summary>
		///     Divides a measurement by a ratio.
		/// </summary>
		/// <param name="aMeasurement">A measurement.</param>
		/// <param name="aRatio">A ratio.</param>
		/// <returns>The measurement divided by the ratio.</returns>
		public static Measurement operator /(Measurement aMeasurement, decimal aRatio)
			=> new Measurement(aMeasurement.Value / aRatio, aMeasurement.Unit);


		/// <summary>
		///     Divides a measurement by a ratio.
		/// </summary>
		/// <param name="aMeasurement">A measurement.</param>
		/// <param name="aRatio">A ratio.</param>
		/// <returns>The measurement divided by the ratio.</returns>
		public static Measurement operator /(Measurement aMeasurement, double aRatio)
			=> aMeasurement / (decimal) aRatio;


		/// <summary>
		///     Divides a measurement by a ratio.
		/// </summary>
		/// <param name="aMeasurement">A measurement.</param>
		/// <param name="aRatio">A ratio.</param>
		/// <returns>The measurement divided by the ratio.</returns>
		public static Measurement operator /(Measurement aMeasurement, int aRatio) => aMeasurement / (decimal) aRatio;


		/// <summary>
		///     Divides a measurement by a ratio.
		/// </summary>
		/// <param name="aMeasurement">A measurement.</param>
		/// <param name="aRatio">A ratio.</param>
		/// <returns>The measurement divided by the ratio.</returns>
		public static Measurement Divide(Measurement aMeasurement, double aRatio) => aMeasurement / aRatio;


		/// <summary>
		///     Compare two measurements.
		/// </summary>
		/// <param name="aFirst">A measurement.</param>
		/// <param name="aSecond">A measurement</param>
		/// <returns>
		///     True if the two measurements have the same values
		///     and units, otherwise false.
		/// </returns>
		public static bool operator ==(Measurement aFirst, Measurement aSecond) => Equals(aFirst, aSecond);


		/// <summary>
		///     Compare two measurements.
		/// </summary>
		/// <param name="aFirst">A measurement.</param>
		/// <param name="aSecond">A measurement</param>
		/// <returns>
		///     False if the two measurements have the same values
		///     and units, otherwise true.
		/// </returns>
		public static bool operator !=(Measurement aFirst, Measurement aSecond) => !Equals(aFirst, aSecond);


		/// <summary>
		///     Gets the measured value.
		/// </summary>
		public decimal Value { get; }


		/// <summary>
		///     Describes what units the measured value is in.
		/// </summary>
		public UnitOfMeasure Unit { get; }

		#endregion
	}
}

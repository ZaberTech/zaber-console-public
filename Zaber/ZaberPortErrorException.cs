using System;
using System.Globalization;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Zaber
{
	/// <summary>
	///     Exception thrown when a <see cref="Conversation" />'s request is
	///     interrupted by a <see cref="ZaberPortError" />.
	/// </summary>
	[Serializable]
	public class ZaberPortErrorException : ConversationException
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor. Initializes the exception for no error condition.
		/// </summary>
		public ZaberPortErrorException()
			: base(BuildMessage(ZaberPortError.None))
		{
		}


		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aMessage">The message to display</param>
		public ZaberPortErrorException(string aMessage)
			: base(aMessage)
		{
		}


		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aMessage">The message to display</param>
		/// <param name="aException">The exception that caused this one</param>
		public ZaberPortErrorException(string aMessage, Exception aException)
			: base(aMessage, aException)
		{
		}


		/// <summary>
		///     Initializes a new instance with serialized
		///     data.
		/// </summary>
		/// <param name="aSerializationInfo">
		///     The System.Runtime.Serialization.SerializationInfo that holds the serialized
		///     object data about the exception being thrown.
		/// </param>
		/// <param name="aContext">
		///     The System.Runtime.Serialization.StreamingContext that contains contextual
		///     information about the source or destination.
		/// </param>
		protected ZaberPortErrorException(SerializationInfo aSerializationInfo, StreamingContext aContext)
			: base(aSerializationInfo, aContext)
		{
			if (null == aSerializationInfo)
			{
				throw new ArgumentNullException(nameof(aSerializationInfo));
			}

			ErrorType = (ZaberPortError) aSerializationInfo.GetValue("errorType", typeof(ZaberPortError));
		}


		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aErrorType">The type of error that caused this exception</param>
		public ZaberPortErrorException(ZaberPortError aErrorType)
			: base(BuildMessage(aErrorType))
		{
			ErrorType = aErrorType;
		}


		/// <summary>
		///     Sets the System.Runtime.Serialization.SerializationInfo
		///     with information about the exception.
		/// </summary>
		/// <param name="aSerializationInfo">
		///     The System.Runtime.Serialization.SerializationInfo that holds the serialized
		///     object data about the exception being thrown.
		/// </param>
		/// <param name="aContext">
		///     The System.Runtime.Serialization.StreamingContext that contains contextual
		///     information about the source or destination.
		/// </param>
		/// <exception cref="ArgumentNullException">The info parameter is a null reference.</exception>
		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		public override void GetObjectData(SerializationInfo aSerializationInfo, StreamingContext aContext)
		{
			base.GetObjectData(aSerializationInfo, aContext);
			if (null == aSerializationInfo)
			{
				throw new ArgumentNullException(nameof(aSerializationInfo));
			}

			aSerializationInfo.AddValue("errorType", ErrorType);
		}


		/// <summary>
		///     The type of error that caused the exception.
		/// </summary>
		public ZaberPortError ErrorType { get; }

		#endregion

		#region -- Non-Public Methods --

		private static string BuildMessage(ZaberPortError aErrorType)
			=> string.Format(CultureInfo.CurrentCulture, "Port error: {0}", aErrorType);

		#endregion

		#region -- Data --

		#endregion
	}
}

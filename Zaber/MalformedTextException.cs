﻿using System;

namespace Zaber
{
	/// <summary>
	///     Thrown when an ASCII message could not be parsed.
	/// </summary>
	public class MalformedTextException : Exception
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Creates a new instance of the MalformedTextException class.
		/// </summary>
		/// <param name="aMessage">
		///     A message describing why this exception was
		///     created.
		/// </param>
		public MalformedTextException(string aMessage)
			: base(aMessage)
		{
		}

		#endregion
	}
}

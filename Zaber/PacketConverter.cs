﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using log4net;

namespace Zaber
{
	/// <summary>
	///     This class is used inside the library to convert between the serial port
	///     byte stream and the <see cref="DataPacket" /> data structure. It also
	///     times out when a byte gets
	///     dropped and a partial packet is left in the serial port for too long.
	/// </summary>
	public class PacketConverter
	{
		#region -- Public data --

		/// <summary>
		///     Constant providing the number of bytes in a Binary protocol message.
		/// </summary>
		public const int BINARY_PACKET_SIZE = 6;

		#endregion

		#region -- Events --

		/// <summary>
		///     Raised when a port error is detected, including partial packet
		///     timeouts.
		/// </summary>
		public event EventHandler<ZaberPortErrorReceivedEventArgs> PortError;


		/// <summary>
		///     Raised when a complete packet is received.
		///     The packet is included in the event arguments.
		/// </summary>
		/// <remarks>
		///     Be careful when handling this event, because it is usually raised
		///     from a background thread. See
		///     <see cref="IZaberPort.DataPacketReceived" /> for details.
		/// </remarks>
		public event EventHandler<DataPacketEventArgs> DataPacketReceived;

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Create a new instance.
		/// </summary>
		public PacketConverter()
		: this(_log.IsDebugEnabled ? 10 : 0) // history of 10 when debugging
		{
		}


		/// <summary>
		///     Create a new instance and record packet history.
		/// </summary>
		/// <param name="aHistoryCount">The number of bytes to record.</param>
		/// <remarks>
		///     Use <see cref="FormatHistory" /> to retrieve the
		///     packet history.
		/// </remarks>
		public PacketConverter(int aHistoryCount)
		{
			if (aHistoryCount > 0)
			{
				_ringBuffer = new TraceInfo[aHistoryCount];
				_stopwatch = new Stopwatch();
				_stopwatch.Start();
			}
		}


		/// <summary>
		///     Clears the PacketConverter's buffers.
		/// </summary>
		/// <remarks>
		///     This should be called when an incomplete message has been read.
		///     For example, it should be called after closing a port, or when
		///     no data has been read for a long time.
		/// </remarks>
		public void ClearBuffer()
		{
			_buffer.Clear();
			_continuations.Clear();
		}


		/// <summary>
		///     Whether this object is equal to another.
		/// </summary>
		/// <param name="aOther">The object to compare this one to.</param>
		/// <returns>True if the objects are equal, false otherwise.</returns>
		public override bool Equals(object aOther)
		{
			if (ReferenceEquals(this, aOther))
			{
				return true;
			}

			if (!(aOther is PacketConverter other))
			{
				return false;
			}

			if ((_ringBuffer.Length != other._ringBuffer.Length) || (IsAsciiMode != other.IsAsciiMode))
			{
				return false;
			}

			return true;
		}


		/// <summary>
		///     Format the contents of any partial packet as a string of
		///     hexadecimal digits.
		/// </summary>
		/// <remarks>This is mostly useful for testing purposes.</remarks>
		/// <returns>A string of hexadecimal digits, possibly empty.</returns>
		public string FormatPartialPacket()
		{
			var msg = new StringBuilder();
			msg.Append("Partial packet: 0x");
			foreach (var b in _buffer)
			{
				msg.Append(b.ToString("X2", CultureInfo.CurrentCulture));
			}

			return msg.ToString();
		}


		/// <summary>
		///     Dump any partially complete continuation messages, for debugging
		///     purposes.
		/// </summary>
		/// <returns>A multi-line string of message segments, possibly empty.</returns>
		public string FormatPartialCompletions()
		{
			var sb = new StringBuilder();
			if (_continuations.Any())
			{
				sb.AppendLine("Incomplete message continuations:");
				foreach (var pair in _continuations)
				{
					foreach (var packet in pair.Value)
					{
						sb.AppendLine(packet.Text);
					}
				}
			}

			return sb.ToString();
		}


		/// <summary>
		///     Format the history of the last few bytes received along with
		///     the timing.
		/// </summary>
		/// <returns>The formatted history.</returns>
		/// <exception cref="InvalidOperationException">
		///     when no history count
		///     was specified for this object at construction.
		/// </exception>
		public string FormatHistory()
		{
			if (null == _ringBuffer)
			{
				throw new InvalidOperationException("History is not enabled.");
			}

			var elapsedTime = ReadStopwatch();
			var previousTime = _ringBuffer[_ringIndex].Milliseconds;

			var ringDump = new StringBuilder();
			ringDump.AppendFormat("{0}ms since last byte. The last {1} bytes and times(with delta) were:",
								  elapsedTime,
								  _ringBuffer.Length);
			ringDump.AppendLine();

			for (var i = 0; i < _ringBuffer.Length; i++)
			{
				var traceInfo = _ringBuffer[(_ringIndex + i) % _ringBuffer.Length];

				var asciiChar = string.Empty;
				if (IsAsciiMode)
				{
					var ch = (char) traceInfo.Data;
					if (_escapeCodes.TryGetValue(ch, out var chAsStr))
					{
						asciiChar = $" '{chAsStr}'";
					}
					else if (char.IsControl(ch))
					{
						asciiChar = string.Format(" '\\{0}'", traceInfo.Data);
					}
					else
					{
						asciiChar = $" '{ch}'";
					}
				}

				ringDump.AppendFormat("{1:X2}{3} {0}({2})",
									  traceInfo.Milliseconds,
									  traceInfo.Data,
									  traceInfo.Milliseconds - previousTime,
									  asciiChar);

				ringDump.AppendLine();
				if (IsAsciiMode)
				{
					if ((char) traceInfo.Data == '\n')
					{
						ringDump.AppendLine("---");
					}
				}
				else
				{
					if (((i + _ringIndex + 1) % BINARY_PACKET_SIZE) == 0)
					{
						ringDump.AppendLine("---");
					}
				}

				previousTime = traceInfo.Milliseconds;
			}

			var history = ringDump.ToString();
			return history;
		}


		/// <summary>
		///     Returns a calculated hash code for this object.
		/// </summary>
		/// <returns>An integer hash code.</returns>
		public override int GetHashCode()
		{
			var code = 17;

			code = (31 * code) + (IsAsciiMode ? 1 : 0);
			code = (31 * code) + _ringBuffer.Length;

			return code;
		}


		/// <summary>
		///     Add another byte to the incoming byte stream. If this byte
		///     completes a packet, then the <see cref="DataPacketReceived" /> event
		///     will be raised.
		/// </summary>
		/// <param name="aValue">The byte to receive.</param>
		public void ReceiveByte(byte aValue)
		{
			if (null != _ringBuffer)
			{
				RecordRingBuffer(aValue);
			}

			lock (_buffer)
			{
				DataPacket data = null;
				if (IsAsciiMode)
				{
					if ('\r' == aValue)
					{
						// ignore it
					}
					else if ('\n' == aValue)
					{
						string str = null;
						try
						{
							str = Encoding.UTF8.GetString(_buffer.ToArray());
							data = new DataPacket(str);
							if (data.IsChecksumInvalid)
							{
								data = null;
								PortError?.Invoke(this,
												  new ZaberPortErrorReceivedEventArgs(ZaberPortError.InvalidChecksum));
							}
						}
						catch (MalformedTextException)
						{
							var errorData = str ?? string.Empty;
							if (string.IsNullOrEmpty(errorData) && (_buffer.Count > 0))
							{
								var sb = new StringBuilder();
								sb.Append("[ ");
								foreach (var b in _buffer)
								{
									sb.AppendFormat("{0:X2} '{1}', ", b, (char) b);
								}

								sb.Remove(sb.Length - 2, 2); // Remove last ','
								sb.Append(" ]");
								errorData = sb.ToString();
							}

							var errorMessage = $"Received malformed reply: {errorData}";
							_log.Error(errorMessage);
							PortError?.Invoke(this,
											  new ZaberPortErrorReceivedEventArgs(
												  ZaberPortError.InvalidPacket,
												  errorMessage));
						}
						finally
						{
							_buffer.Clear();
						}
					}
					else
					{
						// In the ASCII protocol, the '@', '#' and "!" characters can only
						// appear a the start of a message. If we encounter one without
						// seeing a CR before it, the intervening data must be garbage from
						// the serial port buffer or line noise.
						if ((_buffer.Count > 0) && ("@#!".IndexOf((char) aValue) >= 0))
						{
							_buffer.Clear();
						}

						_buffer.Add(aValue);
					}
				}
				else
				{
					_buffer.Add(aValue);
					if (_buffer.Count == BINARY_PACKET_SIZE)
					{
						data = BuildDataPacket();
						_buffer.Clear();
					}
				}

				if (null != data)
				{
					CombineAndDispatch(data);
				}
			}
		}


		/// <summary>
		///     Convert a <see cref="DataPacket" /> to a byte stream.
		/// </summary>
		/// <param name="aDataPacket">The data packet to convert</param>
		/// <returns>An array of 6 bytes.</returns>
		public static byte[] GetBytes(DataPacket aDataPacket)
		{
			if (null == aDataPacket)
			{
				throw new ArgumentNullException(nameof(aDataPacket));
			}

			var bytes = new byte[BINARY_PACKET_SIZE];
			bytes[0] = aDataPacket.DeviceNumber;
			bytes[1] = (byte) aDataPacket.Command;
			var data = (int) aDataPacket.NumericData;
			bytes[2] = (byte) (data & 0x000000FF);
			bytes[3] = (byte) ((data & 0x0000FF00) >> 8);
			bytes[4] = (byte) ((data & 0x00FF0000) >> 16);
			bytes[5] = (byte) ((data & 0xFF000000) >> 24);
			return bytes;
		}


		/// <summary>
		///     Flag that gets or sets whether the converter is in ASCII mode.
		///     If it's not in ASCII mode, then all received data will be
		///     parsed as binary, 6-byte packets.
		/// </summary>
		public bool IsAsciiMode { get; set; }

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Read the current time from the stop watch.
		/// </summary>
		/// <returns>The elapsed time in milliseconds.</returns>
		/// <remarks>This method is just to facilitate testing.</remarks>
		protected virtual long ReadStopwatch() => _stopwatch.ElapsedMilliseconds;


		/// <summary>
		///     Initialize the data structure and read the values from the buffer.
		/// </summary>
		private DataPacket BuildDataPacket()
		{
			var dataPacket = new DataPacket
			{
				DeviceNumber = _buffer[0],
				Command = (Command) _buffer[1]
			};

			var data = 0;
			for (var i = 5; i >= 2; i--)
			{
				data <<= 8;
				data += _buffer[i];
			}

			dataPacket.NumericData = data;
			dataPacket.Measurement = new Measurement(data, UnitOfMeasure.Data);

			return dataPacket;
		}


		private void CombineAndDispatch(DataPacket aPacket)
		{
			var data = aPacket;

			if (data.IsContinuation)
			{
				_log.DebugFormat("Received a continuation: '{0}'", data.Text);
				// Using the device number as the dictionary key is sufficient because
				// firmware does not overlap handling of axis-scope commands.
				if (!_continuations.TryGetValue(data.DeviceNumber, out var pieces) || (pieces.Count < 1))
				{
					PortError?.Invoke(this,
						new ZaberPortErrorReceivedEventArgs(
							ZaberPortError.InvalidContinuation,
							"Received a continuation without a header."));
					data = null;
				}
				else
				{
					pieces.Add(data);
					if (data.IsComplete)
					{
						_continuations.Remove(data.DeviceNumber);
						try
						{
							data = DataPacket.JoinContinuations(pieces);
							_log.DebugFormat("Continuation completes previous messages as '{0}'", data.Text);
						}
						catch (Exception aException)
						{
							data = null;
							_log.Error("Failed to merge multipart message.", aException);
							PortError?.Invoke(this,
								new ZaberPortErrorReceivedEventArgs(
									ZaberPortError.InvalidContinuation,
									"Failed to merge multipart message: " + aException.Message));
						}
					}
					else
					{
						_log.Debug("Continuation not complete; stored for later.");
						data = null;
					}
				}
			}
			else if (!data.IsComplete)
			{
				if (_continuations.ContainsKey(data.DeviceNumber))
				{
					PortError?.Invoke(this,
						new ZaberPortErrorReceivedEventArgs(
							ZaberPortError.InvalidContinuation,
							"Received a to-be-continued message before the previous one was completed."));
				}

				_continuations[data.DeviceNumber] = new List<DataPacket>{ data };
				_log.DebugFormat("Received and stored an incomplete message: '{0}'", data.Text);
				data = null;
			}

			if (null != data)
			{
				DataPacketReceived?.Invoke(this, new DataPacketEventArgs(data));
			}
		}


		private void RecordRingBuffer(byte aValue)
		{
			_ringBuffer[_ringIndex].Milliseconds = ReadStopwatch();
			_ringBuffer[_ringIndex].Data = aValue;

			_ringIndex = (_ringIndex + 1) % _ringBuffer.Length;
		}

		#endregion

		#region -- Data --

		private struct TraceInfo
		{
			public byte Data;
			public long Milliseconds;
		}


		private const int PACKET_SIZE = 6;
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private static readonly IDictionary<char, string> _escapeCodes = new Dictionary<char, string>
		{
			{ '\a', @"\a" },
			{ '\b', @"\b" },
			{ '\f', @"\f" },
			{ '\n', @"\n" },
			{ '\r', @"\r" },
			{ '\t', @"\t" },
			{ '\v', @"\v" }
		};

		private readonly List<byte> _buffer = new List<byte>();
		private readonly TraceInfo[] _ringBuffer;
		private readonly Stopwatch _stopwatch;
		private int _ringIndex;
		private Dictionary<int, List<DataPacket>> _continuations = new Dictionary<int, List<DataPacket>>();

		#endregion
	}
}

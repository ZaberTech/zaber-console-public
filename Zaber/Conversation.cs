﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Reflection;
using System.Threading;
using IronPython.Modules;
using log4net;

namespace Zaber
{
	/// <summary>
	///     Converts from the asynchronous, event-driven model of the
	///     <see cref="ZaberDevice" /> to a synchronous, request/response model.
	/// </summary>
	/// <remarks>
	///     When you call the <see cref="Request(Command, int)" /> method, the conversation will
	///     call <see cref="ZaberDevice.Send(Command, int, byte)" /> on its device, and then sleep until
	///     a response is returned. See the <see cref="Request(Command, int)" /> method notes for
	///     details on support for message ids and pitfalls to avoid.
	/// </remarks>
	public class Conversation : IDisposable
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aDevice">See <see cref="Device" /></param>
		public Conversation(ZaberDevice aDevice)
		{
			Device = aDevice;
			Device.MessageReceived += Device_MessageReceived;
			Device.PortChanged += Device_PortChanged;

			TimeoutTimer = new TimeoutTimer { Timeout = System.Threading.Timeout.Infinite };

			AlertTimeoutTimer = new TimeoutTimer { Timeout = System.Threading.Timeout.Infinite };

			PollTimeoutTimer = new TimeoutTimer { Timeout = 100 };
		}


		/// <summary>
		///     Send a command to a Zaber device that doesn't require a data value
		///     and wait for the response.
		/// </summary>
		/// <param name="aCommand">See <see cref="DataPacket.Command" />.</param>
		/// <returns>The response message</returns>
		/// <exception cref="RequestReplacedException">
		///     Another request has replaced this request.
		/// </exception>
		/// <exception cref="ErrorResponseException">
		///     The response was <see cref="Command.Error" />.
		/// </exception>
		/// <exception cref="ZaberPortErrorException">
		///     A port error occurred while waiting for a response.
		/// </exception>
		/// <exception cref="RequestTimeoutException">
		///     The request did not
		///     complete before the timeout expired.
		/// </exception>
		/// <remarks>
		///     See more details in the other override of <see cref="Request(Command, int)" />.
		/// </remarks>
		public virtual DeviceMessage Request(Command aCommand) => Request(aCommand, 0);


		/// <summary>
		///     Send a command to a Zaber device and wait for the response.
		/// </summary>
		/// <param name="aCommand">See <see cref="DataPacket.Command" />.</param>
		/// <param name="aData">See <see cref="DataPacket.NumericData" />.</param>
		/// <returns>The response message</returns>
		/// <exception cref="RequestReplacedException">
		///     Another request has replaced this request.
		/// </exception>
		/// <exception cref="ErrorResponseException">
		///     The response was <see cref="Command.Error" />.
		/// </exception>
		/// <exception cref="ZaberPortErrorException">
		///     A port error occurred while waiting for a response.
		/// </exception>
		/// <exception cref="RequestTimeoutException">
		///     The request did not
		///     complete before the timeout expired.
		/// </exception>
		/// <remarks>
		///     <para>
		///         This method sends the request on to the device and then
		///         puts the thread to sleep until a response is received or the
		///         <see cref="TimeoutTimer" /> times out. However,
		///         there are some situations where it can get confused about
		///         which response goes with which request.
		///     </para>
		///     <para>
		///         If message ids are enabled on the device, then coordinating
		///         requests and responses is reliable. There were some bugs in the
		///         firmware related to message ids, so make sure your firmware is at
		///         least version 5.07.
		///     </para>
		///     <para>
		///         If message ids are disabled (the default), then the following
		///         scenarios can cause errors:
		///         <list type="bullet">
		///             <item>
		///                 You request a pre-emptable command from the conversation, and
		///                 while it is running, a joystick sends a command to the same device.
		///                 The conversation didn't see the request, so it assumes that the
		///                 response is for the message it sent and returns it normally.
		///             </item>
		///             <item>
		///                 You request a command from the conversation, and just as it
		///                 starts, another thread requests another command. Depending on the
		///                 timing, the conversation may not record the requests in the same
		///                 order they get sent to the device, and the conversation may return
		///                 your command's response to the other thread and the other command's
		///                 response to you.
		///             </item>
		///             <item>
		///                 You request a command from the conversation, and just as
		///                 it finishes, another thread requests another command. Depending
		///                 on the timing, the conversation may return your command's response
		///                 to the other thread and the other command's response to you.
		///             </item>
		///         </list>
		///     </para>
		/// </remarks>
		public virtual DeviceMessage Request(Command aCommand, int aData)
			=> RequestInUnits(aCommand, new Measurement(aData, UnitOfMeasure.Data));


		/// <summary>
		///     Send a text command to a Zaber device and wait for the response.
		/// </summary>
		/// <param name="aText">
		///     The command to send to the device without the
		///     slash or device number.
		/// </param>
		/// <returns>The response message</returns>
		/// <exception cref="ErrorResponseException">
		///     The response was an error.
		/// </exception>
		/// <exception cref="ZaberPortErrorException">
		///     A port error occurred while waiting for a response.
		/// </exception>
		/// <exception cref="RequestTimeoutException">
		///     No response was received
		///     before the timeout expired.
		/// </exception>
		/// <remarks>
		///     <para>
		///         This method sends the request on to the device and then
		///         puts the thread to sleep until a response is received or the
		///         <see cref="TimeoutTimer" /> times out.
		///     </para>
		/// </remarks>
		public DeviceMessage Request(string aText) => RequestInUnits(aText, null);


		/// <summary>
		///     Send a text command to a Zaber device and wait for the response.
		/// </summary>
		/// <param name="aText">
		///     The command to send to the device without the
		///     slash or device number.
		/// </param>
		/// <param name="aMeasurement">
		///     A data value to append to the command
		///     will be calculated from this. If it is null, then the command
		///     will be sent without any extra data.
		/// </param>
		/// <returns>The response message</returns>
		/// <exception cref="ErrorResponseException">
		///     The response was an error.
		/// </exception>
		/// <exception cref="ZaberPortErrorException">
		///     A port error occurred while waiting for a response.
		/// </exception>
		/// <exception cref="RequestTimeoutException">
		///     No response was received
		///     before the timeout expired.
		/// </exception>
		/// <remarks>
		///     This method sends the request on to the device and then
		///     puts the thread to sleep until a response is received or the
		///     <see cref="TimeoutTimer" /> times out.
		/// </remarks>
		public DeviceMessage RequestInUnits(string aText, Measurement aMeasurement)
		{
			ConversationTopic topic;

			do
			{
				if (Device.AreAsciiMessageIdsEnabled)
				{
					topic = StartTopic();
					Device.SendInUnits(aText, aMeasurement, topic.MessageId);
				}
				else
				{
					topic = StartTopic(0);
					Device.SendInUnits(aText, aMeasurement);
				}

				if (!topic.Wait(TimeoutTimer))
				{
					throw new RequestTimeoutException();
				}
			} while (topic.IsFlowControlRejected);

			topic.Validate();
			return topic.Response;
		}


		/// <summary>
		///     Send a command to a Zaber device and wait for the response.
		/// </summary>
		/// <param name="aText">
		///     The command to send to the device without the
		///     slash or device number.
		/// </param>
		/// <param name="aValue">
		///     Calculate the <see cref="DataPacket.TextData" />
		///     value from this.
		/// </param>
		/// <param name="aUnit">
		///     Calculate the <see cref="DataPacket.TextData" />
		///     value from this.
		/// </param>
		/// <returns>The response message</returns>
		/// <remarks>
		///     For more details and a list of possible exceptions, see
		///     <see cref="Request(string, decimal)" />.
		/// </remarks>
		public DeviceMessage RequestInUnits(string aText, decimal aValue, UnitOfMeasure aUnit)
			=> RequestInUnits(aText, new Measurement(aValue, aUnit));


		/// <summary>
		///     Send a command to a Zaber device and wait for the response.
		/// </summary>
		/// <param name="aText">
		///     The command to send to the device without the
		///     slash or device number.
		/// </param>
		/// <param name="aValue">
		///     Calculate the <see cref="DataPacket.TextData" />
		///     value from this.
		/// </param>
		/// <param name="aUnit">
		///     Calculate the <see cref="DataPacket.TextData" />
		///     value from this.
		/// </param>
		/// <returns>The response message</returns>
		/// <remarks>
		///     For more details and a list of possible exceptions, see
		///     <see cref="Request(string, decimal)" />.
		/// </remarks>
		public DeviceMessage RequestInUnits(string aText, double aValue, UnitOfMeasure aUnit)
			=> RequestInUnits(aText, new Measurement(aValue, aUnit));


		/// <summary>
		///     Send a text command to a Zaber device and wait for the response.
		/// </summary>
		/// <param name="aText">
		///     The command to send to the device without the
		///     slash or device number.
		/// </param>
		/// <param name="aData">
		///     A data value to be appended to the command.
		/// </param>
		/// <returns>The response message</returns>
		/// <exception cref="ErrorResponseException">
		///     The response was an error.
		/// </exception>
		/// <exception cref="ZaberPortErrorException">
		///     A port error occurred while waiting for a response.
		/// </exception>
		/// <exception cref="RequestTimeoutException">
		///     No response was received
		///     before the timeout expired.
		/// </exception>
		/// <remarks>
		///     <para>
		///         This method sends the request on to the device and then
		///         puts the thread to sleep until a response is received or the
		///         <see cref="TimeoutTimer" /> times out.
		///     </para>
		/// </remarks>
		public DeviceMessage Request(string aText, decimal aData)
			=> RequestInUnits(aText, new Measurement(aData, UnitOfMeasure.Data));


		/// <summary>
		///     Send a command to a Zaber device and wait for the response.
		/// </summary>
		/// <param name="aCommand">See <see cref="DataPacket.Command" />.</param>
		/// <param name="aMeasurement">
		///     Calculate the <see cref="DataPacket.NumericData" />
		///     value from this.
		/// </param>
		/// <returns>The response message</returns>
		/// <exception cref="RequestReplacedException">
		///     Another request has replaced this request.
		/// </exception>
		/// <exception cref="ErrorResponseException">
		///     The response was <see cref="Command.Error" />.
		/// </exception>
		/// <exception cref="ZaberPortErrorException">
		///     A port error occurred while waiting for a response.
		/// </exception>
		/// <exception cref="RequestTimeoutException">
		///     The request did not
		///     complete before the timeout expired.
		/// </exception>
		/// <remarks>
		///     <para>
		///         This method sends the request on to the device and then
		///         puts the thread to sleep until a response is received or the
		///         <see cref="TimeoutTimer" /> times out. However,
		///         there are some situations where it can get confused about
		///         which response goes with which request.
		///     </para>
		///     <para>
		///         If message ids are enabled on the device, then coordinating
		///         requests and responses is reliable. There were some bugs in the
		///         firmware related
		///         to message ids, so make sure your firmware is at least
		///         version 5.07.
		///     </para>
		///     <para>
		///         If message ids are disabled (the default), then the following
		///         scenarios can cause errors:
		///         <list type="bullet">
		///             <item>
		///                 You request a pre-emptable command from the conversation, and
		///                 while it is running, a joystick sends a command to the same device.
		///                 The conversation didn't see the request, so it assumes that the
		///                 response is for the message it sent and returns it normally.
		///             </item>
		///             <item>
		///                 You request a command from the conversation, and just as it
		///                 starts, another thread requests another command. Depending on the
		///                 timing, the conversation may not record the requests in the same
		///                 order they get sent to the device, and the conversation may return
		///                 your command's response to the other thread and the other command's
		///                 response to you.
		///             </item>
		///             <item>
		///                 You request a command from the conversation, and just as
		///                 it finishes, another thread requests another command. Depending
		///                 on the timing, the conversation may return your command's response
		///                 to the other thread and the other command's response to you.
		///             </item>
		///         </list>
		///     </para>
		/// </remarks>
		public virtual DeviceMessage RequestInUnits(Command aCommand, Measurement aMeasurement)
		{
			var topic = StartTopic();
			var data = Device.CalculateData(aCommand, aMeasurement);
			PrepareForRetry(aCommand, data, topic);

			Device.Send(aCommand, data, topic.MessageId);

			if (!topic.Wait(TimeoutTimer))
			{
				throw new RequestTimeoutException();
			}

			topic.Validate();

			var response = topic.Response;
			var commandInfo = response.CommandInfo;
			var motionType = Device.DeviceType.MotionType;
			if ((motionType != MotionType.Other)
			&& (aMeasurement.Unit != response.Measurement.Unit)
			&& (UnitOfMeasure.Data != aMeasurement.Unit)
			&& (null != commandInfo?.RequestUnit)
			&& (null != commandInfo.ResponseUnit)
			&& (commandInfo.RequestUnit.MeasurementType == commandInfo.ResponseUnit.MeasurementType))
			{
				var convertedMeasurement = Device.UnitConverter.Convert(response.Measurement, aMeasurement.Unit);

				response = new DeviceMessage(response) { Measurement = convertedMeasurement };
			}

			return response;
		}


		/// <summary>
		///     Send a command to a Zaber device and wait for the response.
		/// </summary>
		/// <param name="aCommand">See <see cref="DataPacket.Command" />.</param>
		/// <param name="aValue">
		///     Calculate the <see cref="DataPacket.NumericData" />
		///     value from this.
		/// </param>
		/// <param name="aUnit">
		///     Calculate the <see cref="DataPacket.NumericData" />
		///     value from this.
		/// </param>
		/// <returns>The response message</returns>
		/// <remarks>
		///     For more details and a list of possible exceptions, see
		///     <see cref="Request(Command, int)" />.
		/// </remarks>
		public virtual DeviceMessage RequestInUnits(Command aCommand, decimal aValue, UnitOfMeasure aUnit)
			=> RequestInUnits(aCommand, new Measurement(aValue, aUnit));


		/// <summary>
		///     Send a command to a Zaber device and wait for the response.
		/// </summary>
		/// <param name="aCommand">See <see cref="DataPacket.Command" />.</param>
		/// <param name="aValue">
		///     Calculate the <see cref="DataPacket.NumericData" />
		///     value from this.
		/// </param>
		/// <param name="aUnit">
		///     Calculate the <see cref="DataPacket.NumericData" />
		///     value from this.
		/// </param>
		/// <returns>The response message</returns>
		/// <remarks>
		///     For more details and a list of possible exceptions, see
		///     <see cref="Request(Command, int)" />.
		/// </remarks>
		public virtual DeviceMessage RequestInUnits(Command aCommand, int aValue, UnitOfMeasure aUnit)
			=> RequestInUnits(aCommand, new Measurement(aValue, aUnit));


		/// <summary>
		///     Send a command to a Zaber device and wait for the response.
		/// </summary>
		/// <param name="aCommand">See <see cref="DataPacket.Command" />.</param>
		/// <param name="aValue">
		///     Calculate the <see cref="DataPacket.NumericData" />
		///     value from this.
		/// </param>
		/// <param name="aUnit">
		///     Calculate the <see cref="DataPacket.NumericData" />
		///     value from this.
		/// </param>
		/// <returns>The response message</returns>
		/// <remarks>
		///     For more details and a list of possible exceptions, see
		///     <see cref="Request(Command, int)" />.
		/// </remarks>
		public virtual DeviceMessage RequestInUnits(Command aCommand, double aValue, UnitOfMeasure aUnit)
			=> RequestInUnits(aCommand, new Measurement(aValue, aUnit));


		/// <summary>
		///     Wait until the device sends an alert message.
		/// </summary>
		/// <returns>The details of the alert message</returns>
		public DeviceMessage WaitForAlert()
		{
			const byte messageId = 0;
			var topic = StartTopic(messageId);
			topic.IsAlert = true;

			if (0 == Device.AxisNumber)
			{
				throw new InvalidOperationException("WaitForAlert on AxisNumber 0 Invalid");
			}

			if (!topic.Wait(AlertTimeoutTimer))
			{
				throw new TimeoutException("Timed out while waiting for alert message.");
			}

			topic.Validate();

			return topic.Response;
		}


		/// <summary>
		///     Repeatedly check the device's status until it is idle, using
		///     text-mode requests.
		/// </summary>
		public virtual void PollUntilIdle() => PollUntilIdle(PollTimeoutTimer);


		/// <summary>
		///     Repeatedly check the device's status until it is idle, using
		///     text-mode requests and the given timer.
		/// </summary>
		/// <param name="aTimer">
		///     How long to wait between polling requests.
		/// </param>
		/// <param name="aExceptionOnWarnings">
		///     Throw an exception if the NI flag
		///     is returned by the device. Optional and defaults to true. You may want to
		///     pass in false if you are sending overlapping movements to a multi-axis device.
		/// </param>
		/// <exception cref="DeviceFaultException">
		///     Device reply contains
		///     a warning flag beginning with an "F".
		///     Device has a fault condition.
		/// </exception>
		/// <exception cref="RequestReplacedException">
		///     Previous movement
		///     command has been interrupted and did not complete.
		///     The command was replaced by a new command, or interruped
		///     by manual movement control.
		/// </exception>
		public virtual void PollUntilIdle(TimeoutTimer aTimer, bool aExceptionOnWarnings = true)
		{
			_idleAlertEvent.Reset();
			bool isIdle;
			do
			{
				var resp = Request("");
				isIdle = resp.IsIdle;

				if (!isIdle)
				{
					isIdle = aTimer.WaitOne(_idleAlertEvent);
				}
				else
				{
					//check if device has raised any fault
					if (resp.HasFault)
					{
						throw new DeviceFaultException(resp);
					}

					//check the flags to see if the command was replaced
					if (aExceptionOnWarnings && (WarningFlags.None != resp.FlagText))
					{
						//there are active flags, get all flags and see if NI is set
						if (Request("warnings").Body.Contains(WarningFlags.MovementInterrupted))
						{
							throw new RequestReplacedException(string.Format(CultureInfo.CurrentCulture,
																			 "{2}: Device {0}{1} Previous command was interrupted",
																			 Device.DeviceNumber,
																			 0 == Device.AxisNumber
																				 ? ""
																				 : " axis " + Device.AxisNumber,
																			 typeof(RequestReplacedException).Name));
						}
					}
				}
			} while (!isIdle);
		}


		/// <summary>
		///     Record that a request is about to be made.
		/// </summary>
		/// <returns>The topic that will monitor the request.</returns>
		/// <remarks>
		///     <para>
		///         You can then make the request using the device directly and call
		///         <see cref="ConversationTopic.Wait()" /> to wait for the response. This is
		///         useful if you want to make several unrelated requests of several devices
		///         and then wait until they are all completed. If message ids are enabled,
		///         be sure to use the message id from the topic when sending the request.
		///     </para>
		///     <para>
		///         If message ids are disabled, then this technique is not thread-safe,
		///         because there's no guarantee that two requests from two threads will get
		///         sent to the device in the same order that their topics were started.
		///         If you're sending requests to one device from more than one thread,
		///         either enable message ids, or implement your own locking at a higher level.
		///     </para>
		/// </remarks>
		/// <exception cref="InvalidOperationException">
		///     All message IDs are in
		///     use in other pending conversations.
		/// </exception>
		public virtual ConversationTopic StartTopic()
		{
			const int maxMessageIds = 254; // 0 and 255 are not allowed.

			for (var i = 0; i < maxMessageIds; i++)
			{
				try
				{
					return StartTopic(CalculateMessageId());
				}
				catch (ActiveMessageIdException)
				{
					// Try the next message ID.
				}
			}

			throw new InvalidOperationException("No available message ID found.");
		}


		/// <summary>
		///     Record that a request is about to be made.
		/// </summary>
		/// <returns>The topic that will monitor the request.</returns>
		/// <param name="aMessageId">The message id that the request will use.</param>
		/// <remarks>
		///     <para>
		///         You can then make the request using the device directly and call
		///         <see cref="ConversationTopic.Wait()" /> to wait for the response. This is
		///         useful if you want to make several unrelated requests of several devices
		///         and then wait until they are all completed. If message ids are disabled,
		///         messageId will be ignored.
		///     </para>
		///     <para>
		///         If message ids are disabled, then this technique is not thread-safe,
		///         because there's no guarantee that two requests from two threads will get
		///         sent to the device in the same order that their topics were started.
		///         If you're sending requests to one device from more than one thread,
		///         either enable message ids, or implement your own locking at a higher level.
		///     </para>
		/// </remarks>
		/// <exception cref="ActiveMessageIdException">
		///     A topic already exists
		///     with the specified message ID.
		/// </exception>
		public virtual ConversationTopic StartTopic(byte aMessageId)
		{
			var topic = new ConversationTopic();
			_log.DebugFormat(CultureInfo.CurrentCulture, "Adding to topic map");
			lock (_topicMap)
			{
				if (!_isRegisteredForPortEvents)
				{
					_portEventsPort = Device.Port;
					_portEventsPort.DataPacketSent += Port_DataPacketSent;
					_portEventsPort.ErrorReceived += Port_ErrorReceived;
					_isRegisteredForPortEvents = true;
				}

				if (Device.Port.IsAsciiMode ? Device.AreAsciiMessageIdsEnabled : Device.AreMessageIdsEnabled)
				{
					if (_topicMap.ContainsKey(aMessageId))
					{
						throw new ActiveMessageIdException();
					}

					topic.MessageId = aMessageId;
					_topicMap.Add(topic.MessageId, topic);
				}

				_topicList.AddLast(topic);

				topic.Completed += Topic_Completed;
				topic.Timeout += Topic_Timeout;
			}

			return topic;
		}


		/// <summary>
		///     Reserve a specific message ID. Reservation means the reserved ID will
		///     never automatically be assigned by <see cref="StartTopic()"/>, but can
		///     be manually used in calls to <see cref="StartTopic(byte)"/>.
		///     The caller is responsible for making sure the specified ID is not
		///     already in use elsewhere, and is in the legal range for the protocol.
		/// </summary>
		/// <param name="aMessageId">The ID to reserve. Must be greater than zero
		/// and less than either 100 (ASCII protocol) or 255 (Binary protocol).</param>
		/// <returns>True if the ID is not already reserved.</returns>
		public static bool ReserveMessageId(byte aMessageId)
		{
			if (aMessageId < 1)
			{
				throw new ArgumentException($"{nameof(aMessageId)} must be greater than zero.");
			}

			lock (_reservedMessageIDs)
			{
				if (_reservedMessageIDs[aMessageId])
				{
					return false;
				}

				_reservedMessageIDs[aMessageId] = true;
				return true;
			}
		}


		/// <summary>
		///     Release a previously reserved message ID, making it available for automatic
		///     allocation again.
		/// </summary>
		/// <param name="aMessageId">The message ID to release.</param>
		public static void UnreserveMessageId(byte aMessageId)
		{
			if (aMessageId < 1)
			{
				throw new ArgumentException($"{nameof(aMessageId)} must be greater than zero.");
			}

			lock (_reservedMessageIDs)
			{
				_reservedMessageIDs[aMessageId] = false;
			}
		}


		/// <summary>
		///     Clear all message ID reservations. Caller is responsible for knowing
		///     when it is safe to do this (ie when no systems are using reserved IDs).
		/// </summary>
		public static void ResetMessageIdReservations()
		{
			lock (_reservedMessageIDs)
			{
				_reservedMessageIDs.SetAll(false);
			}
		}


		/// <summary>
		///     Send a command to this device that doesn't require a data value
		///     and don't wait for the response.
		/// </summary>
		/// <param name="aCommand">See <see cref="DataPacket.Command" />.</param>
		public void Send(Command aCommand) => Device.Send(aCommand);


		/// <summary>
		///     Send a command to this device without using a message id and don't
		///     wait for the response.
		/// </summary>
		/// <param name="aCommand">See <see cref="DataPacket.Command" />.</param>
		/// <param name="aData">See <see cref="DataPacket.NumericData" />.</param>
		public void Send(Command aCommand, int aData) => Device.Send(aCommand, aData);


		/// <summary>
		///     Send a command to this device and don't wait for a response.
		/// </summary>
		/// <param name="aCommand">See <see cref="DataPacket.Command" />.</param>
		/// <param name="aData">See <see cref="DataPacket.NumericData" />.</param>
		/// <param name="aMessageId">See <see cref="DataPacket.MessageId" />.</param>
		public void Send(Command aCommand, int aData, byte aMessageId) => Device.Send(aCommand, aData, aMessageId);


		/// <summary>
		///     Send a command to this device and don't wait for a response.
		/// </summary>
		/// <param name="aCommand">
		///     The command to send to the device without the
		///     slash or device number.
		/// </param>
		public void Send(string aCommand) => Device.Send(aCommand);


		/// <summary>
		///     Send a command to this device and don't wait for a response.
		/// </summary>
		/// <param name="aCommand">
		///     The command to send to the device without the
		///     slash, device address and axis number.
		/// </param>
		/// <param name="aMessageId">See <see cref="DataPacket.MessageId" />.</param>
		public void Send(string aCommand, byte aMessageId) => Device.Send(aCommand, aMessageId);


		/// <summary>
		///     Add a conversation that represents an axis for this controller.
		/// </summary>
		/// <param name="aAxis">The axis to add.</param>
		/// <remarks>
		///     All controllers have a collection of devices - one
		///     for each axis.
		/// </remarks>
		public void AddAxis(Conversation aAxis) => _axes.Add(aAxis);


		/// <summary>
		///     The device that this conversation will send requests through.
		/// </summary>
		public ZaberDevice Device { get; }


		/// <summary>
		///     Gets or sets the number of times to retry a request after a port error.
		///     Defaults to 0.
		/// </summary>
		/// <remarks>
		///     By default, when the port raises its
		///     <see cref="IZaberPort.ErrorReceived" /> event, any waiting
		///     conversation topics throw a <see cref="ZaberPortErrorException" />.
		///     However, you can set this property to make the conversation retry
		///     the request a few times before throwing the exception. Only commands
		///     that are safe will be retried. For example, Move Relative cannot
		///     be retried, it will always throw an exception if a port error
		///     occurs.
		/// </remarks>
		public virtual int RetryCount { get; set; }


		/// <summary>
		///     This is really only used for testing purposes to make sure that no
		///     requests are left over in any of the internal data structures.
		/// </summary>
		public virtual bool IsWaiting
		{
			get
			{
				lock (_topicMap)
				{
					return (_topicMap.Count > 0) || (_topicList.Count > 0);
				}
			}
		}


		/// <summary>
		///     Gets or sets the length of time in milliseconds to wait for a response to
		///     any request. Use System.Threading.Timeout.Infinite (-1) to wait forever.
		///     Defaults to infinite.
		/// </summary>
		public int Timeout
		{
			get => TimeoutTimer.Timeout;
			set => TimeoutTimer.Timeout = value;
		}


		/// <summary>
		///     Used to implement the <see cref="Timeout" />. This is only really useful for
		///     testing, most clients can just use the <see cref="Timeout" /> property.
		/// </summary>
		public TimeoutTimer TimeoutTimer { get; set; }


		/// <summary>
		///     Gets or sets the length of time in milliseconds to wait for an alert message.
		///     Use System.Threading.Timeout.Infinite (-1) to wait forever.
		///     Defaults to infinite.
		/// </summary>
		public int AlertTimeout
		{
			get => AlertTimeoutTimer.Timeout;
			set => AlertTimeoutTimer.Timeout = value;
		}


		/// <summary>
		///     Used to implement the <see cref="AlertTimeout" />. This is only
		///     really useful for testing, most clients can just use the
		///     <see cref="AlertTimeout" /> property.
		/// </summary>
		public TimeoutTimer AlertTimeoutTimer { get; set; }


		/// <summary>
		///     Gets or sets the length of time in milliseconds to wait between
		///     polling requests.
		///     Defaults to 100.
		/// </summary>
		/// <exception cref="ArgumentOutOfRangeException">
		///     When the timeout
		///     value is negative or zero.
		/// </exception>
		public int PollTimeout
		{
			get => PollTimeoutTimer.Timeout;
			set
			{
				if (value <= 0)
				{
					throw new ArgumentOutOfRangeException(nameof(value), "PollTimeout must be positive.");
				}

				PollTimeoutTimer.Timeout = value;
			}
		}


		/// <summary>
		///     Used to implement the <see cref="PollTimeout" />. This is only
		///     really useful for testing, most clients can just use the
		///     <see cref="PollTimeout" /> property.
		/// </summary>
		public TimeoutTimer PollTimeoutTimer { get; set; }


		/// <summary>
		///     Get a list of axes for this device.
		/// </summary>
		public ReadOnlyCollection<Conversation> Axes => _axes.AsReadOnly();

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Record the request that started a topic so it can be retried if
		///     necessary.
		/// </summary>
		/// <param name="aCommand">See <see cref="DataPacket.Command" />.</param>
		/// <param name="aData">See <see cref="DataPacket.NumericData" />.</param>
		/// <param name="aTopic">
		///     The topic to record this request on. It should
		///     have been returned by StartTopic.
		/// </param>
		protected void PrepareForRetry(Command aCommand, int aData, ConversationTopic aTopic)
		{
			aTopic.RequestCommand = aCommand;
			aTopic.RequestData = aData;
			var commandInfo = Device.DeviceType.GetCommandByNumber(aCommand);
			aTopic.RetryCount = (null != commandInfo) && commandInfo.IsRetrySafe ? RetryCount : 0;
		}


		/// <summary>
		///     Calculates the next message id.
		/// </summary>
		/// <returns>The message id.</returns>
		protected byte CalculateMessageId()
		{
			byte messageId = 0;

			lock (_topicMap)
			{
				var port = Device.Port;
				if (port == null)
				{
					throw new InvalidOperationException("Port used by this conversation was closed.");
				}

				int attempts = 100;

				lock (_reservedMessageIDs)
				{
					if (port.IsAsciiMode && Device.AreAsciiMessageIdsEnabled)
					{
						do
						{
							messageId = (byte) (_lastId = (_lastId % 99) + 1);
							attempts--;
						} while (_reservedMessageIDs[messageId] && (attempts > 0));
					}
					else if (!port.IsAsciiMode && Device.AreMessageIdsEnabled)
					{
						attempts = 255;
						do
						{
							messageId = (byte) (_lastId = (_lastId % 254) + 1);
							attempts--;
						} while (_reservedMessageIDs[messageId] && (attempts > 0));
					}
				}

				if (attempts < 0)
				{
					throw new InvalidOperationException("All message IDs are in use or reserved.");
				}
			}

			return messageId;
		}


		/// <summary>
		///     Find the topic that is waiting for the response we just received.
		///     If there's no message id, just return the most recently started
		///     topic.
		/// </summary>
		/// <param name="aMessageId">
		///     Message id used to identify the topic,
		///     or zero.
		/// </param>
		/// <param name="aMessageType">
		///     Message type we just received. Some
		///     kinds of text responses get ignored.
		/// </param>
		/// <param name="aMessageIdType">Message ID type of message.</param>
		/// <returns>The topic that is waiting for the response.</returns>
		private ConversationTopic FindTopic(byte aMessageId, MessageType aMessageType, MessageIdType aMessageIdType)
		{
			if ((MessageType.Binary != aMessageType)
			&& (MessageType.Response != aMessageType)
			&& (MessageType.Alert != aMessageType))
			{
				return null;
			}

			lock (_topicMap)
			{
				ConversationTopic topic;
				if (_topicMap.ContainsKey(aMessageId))
				{
					topic = _topicMap[aMessageId];
				}
				else if ((_topicList.Count > _topicMap.Count)
					 && (((MessageType.Binary == aMessageType) && (0 == aMessageId))
					 || ((MessageType.Response == aMessageType) && (MessageIdType.None == aMessageIdType))
					 || (MessageType.Alert == aMessageType)))
				{
					// Offer last topic
					// if Binary message ID is 0 (or equivalently, it has no message ID),
					// or it is an ASCII reply with no message ID,
					// or it is an ASCII alert topic
					topic = _topicList.Count > 0 ? _topicList.Last.Value : null;
				}
				else
				{
					topic = null;
				}

				// Only associate alert message to a topic if the topic is waiting for one
				if ((null != topic) && ((MessageType.Alert == aMessageType) != topic.IsAlert))
				{
					topic = null;
				}

				return topic;
			}
		}


		/// <summary>
		///     Checks if the current response replaces pre-empted commands.
		/// </summary>
		/// <param name="aTopic"></param>
		/// <param name="aResponse"></param>
		private void CheckReplacement(ConversationTopic aTopic, DeviceMessage aResponse)
		{
			if (MessageType.Binary != aResponse.MessageType)
			{
				// Only binary mode commands are preempted.
				return;
			}

			if (((Command.Home < aResponse.Command) && (aResponse.Command < Command.UnexpectedPosition))
			|| ((Command.UnexpectedPosition < aResponse.Command) && (aResponse.Command < Command.MoveToStoredPosition))
			|| (Command.Stop < aResponse.Command))
			{
				// Commands beyond Stop don't replace the pre-empted command.
				// They pre-empt it and then return to it.
				// Same with commands between home and unexpected position
				// Same with commands between unexpected position and Move to stored position
				return;
			}

			lock (_topicMap)
			{
				while ((_topicList.Count > 0) && (_topicList.First.Value != aTopic))
				{
					var waitingTopic = _topicList.First.Value;
					_topicList.RemoveFirst();
					_topicMap.Remove(waitingTopic.MessageId);

					// also releases the topic
					waitingTopic.ReplacementResponse = aResponse;
				}
			}
		}


		/// <summary>
		///     Unhook the conversation from port events when the port is closed.
		/// </summary>
		private void DisconnectPortEvents()
		{
			if (_isRegisteredForPortEvents)
			{
				_isRegisteredForPortEvents = false;
				_portEventsPort.DataPacketSent -= Port_DataPacketSent;
				_portEventsPort.ErrorReceived -= Port_ErrorReceived;
				_portEventsPort = null;
			}

			foreach (var conversation in _axes)
			{
				conversation.DisconnectPortEvents();
			}
		}


		private void Topic_Completed(object aSender, EventArgs aArgs)
		{
			if (null == aSender)
			{
				throw new ArgumentNullException(nameof(aSender));
			}

			var topic = aSender as ConversationTopic;
			if (null == topic)
			{
				throw new ArgumentException("Sender must be a ConversationTopic.", nameof(aSender));
			}

			lock (_topicMap)
			{
				RemoveTopic(topic);
			}
		}


		private void Topic_Timeout(object aSender, EventArgs aArgs)
		{
			lock (_topicMap)
			{
				TopicError(aSender as ConversationTopic, ZaberPortError.ResponseTimeout);
			}
		}


		private void Device_MessageReceived(object aSender, DeviceMessageEventArgs aArgs)
		{
			if ((MessageType.Alert == aArgs.DeviceMessage.MessageType) && aArgs.DeviceMessage.IsIdle)
			{
				_idleAlertEvent.Set();
			}

			var messageId = aArgs.DeviceMessage.MessageId;
			_log.DebugFormat(CultureInfo.CurrentCulture,
							 "Grabbing topic map for response to message id {0}",
							 messageId);

			lock (_topicMap)
			{
				var topic = FindTopic(messageId, aArgs.DeviceMessage.MessageType, aArgs.DeviceMessage.MessageIdType);
				if ((null != topic) && (null != topic.RetryPacket))
				{
					Device.Port.CancelDelayedPacket(topic.RetryPacket);
				}

				var response = aArgs.DeviceMessage;
				CheckReplacement(topic, response);

				if (null == topic)
				{
					return;
				}

				_log.DebugFormat(CultureInfo.CurrentCulture, "Releasing response to message id {0}", topic.MessageId);
				topic.Response = aArgs.DeviceMessage;
			}
		}


		private void Device_PortChanged(object sender, EventArgs e)
		{
			lock (_topicMap)
			{
				DisconnectPortEvents();
			}
		}


		private void Port_DataPacketSent(object aSender, DataPacketEventArgs aArgs)
		{
			var packet = aArgs.DataPacket;

			// timeouts can be issued for ASCII only
			var isAsciiAndIdsEnabled = Device.Port.IsAsciiMode && Device.AreAsciiMessageIdsEnabled;
			var isForThisDevice = (packet.DeviceNumber == 0) || (packet.DeviceNumber == Device.DeviceNumber);

			if (!isAsciiAndIdsEnabled || !isForThisDevice)
			{
				return;
			}

			var messageId = packet.MessageId;
			lock (_topicMap)
			{
				var topic = FindTopic(messageId, MessageType.Response, MessageIdType.Numeric);
				if (topic == null)
				{
					return;
				}

				topic.StartTimeout(Timeout <= 0 ? ASCII_TOPIC_CLEANUP_TIMEOUT : Timeout);
			}
		}


		private void RemoveTopic(ConversationTopic aTopic)
		{
			lock (_topicMap)
			{
				_topicMap.Remove(aTopic.MessageId);
				_topicList.Remove(aTopic);
			}
		}


		private void Port_ErrorReceived(object aSender, ZaberPortErrorReceivedEventArgs aArgs)
		{
			lock (_topicMap)
			{
				// Copy list because releasing the topic will remove it from topicList.
				var topics = new List<ConversationTopic>();
				topics.AddRange(_topicList);
				foreach (var topic in topics)
				{
					TopicError(topic, aArgs.ErrorType);
				}
			}
		}


		private void TopicError(ConversationTopic aTopic, ZaberPortError aError)
		{
			if (aTopic.IsComplete)
			{
				return;
			}

			if (aTopic.RetryCount > 0)
			{
				aTopic.RetryCount--;
				var port = Device.Port;
				if (null != aTopic.RetryPacket)
				{
					port?.CancelDelayedPacket(aTopic.RetryPacket);
				}

				aTopic.RetryPacket = Device.SendDelayed(aTopic.RequestCommand, aTopic.RequestData, aTopic.MessageId);
			}
			else
			{
				// also releases the topic and removes it
				aTopic.ZaberPortError = aError;
			}
		}

		#endregion

		#region IDisposable Members

		/// <summary>
		///     Performs application-defined tasks associated with freeing, releasing, or
		///     resetting unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);

			// not necessary here, but might be for derived types.
			GC.SuppressFinalize(this);
		}


		/// <summary>
		///     Performs application-defined tasks associated with freeing, releasing, or
		///     resetting unmanaged resources.
		/// </summary>
		/// <param name="aIsDisposing">
		///     True if the object is being disposed, and not
		///     garbage collected.
		/// </param>
		protected virtual void Dispose(bool aIsDisposing)
		{
			if (aIsDisposing)
			{
				Device.MessageReceived -= Device_MessageReceived;
				Device.PortChanged -= Device_PortChanged;

				_idleAlertEvent.Close();
				DisconnectPortEvents();
			}
		}

		#endregion

		#region -- Data members --

		private const int ASCII_TOPIC_CLEANUP_TIMEOUT = 1000;
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly BitArray _reservedMessageIDs = new BitArray(256, false);

		private readonly Dictionary<byte, ConversationTopic> _topicMap = new Dictionary<byte, ConversationTopic>();
		private readonly LinkedList<ConversationTopic> _topicList = new LinkedList<ConversationTopic>();
		private readonly ManualResetEvent _idleAlertEvent = new ManualResetEvent(false);
		private readonly List<Conversation> _axes = new List<Conversation>();

		private int _lastId;
		private bool _isRegisteredForPortEvents;
		private IZaberPort _portEventsPort;

		#endregion
	}
}

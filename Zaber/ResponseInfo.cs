namespace Zaber
{
	/// <summary>
	///     Descriptive information about a command that Zaber devices support.
	/// </summary>
	/// <remarks>This class is specific to response-only commands.</remarks>
	public class ResponseInfo : CommandInfo
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Create a deep copy of the ResponseInfo.
		/// </summary>
		/// <returns>A new instance of ResponseInfo (as CommandInfo) with all the same property values as the instance invoked on.</returns>
		public override CommandInfo Clone()
		{
			var ri = new ResponseInfo();
			CopyProperties(this, ri);
			return ri;
		}


		/// <summary>
		///     Response-only commands are never sent to the devices. They come back as
		///     responses. The error response is a good example.
		/// </summary>
		public override bool IsResponseOnly => true;

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Zaber
{
	/// <summary>
	///     A collection of all the data that gets sent to and received from the
	///     T-Series devices.
	/// </summary>
	/// <remarks>
	///     This class is mostly used inside the Zaber library. It converts
	///     the commands to byte streams and back again. The place that client
	///     programs will see it is in the <see cref="IZaberPort.DataPacketReceived" />
	///     event handler.
	/// </remarks>
	/// <example>
	///     Receive a response
	///     <code>
	/// void port_DataPacketReceived(object sender, DataPacketReceivedEventArgs e)
	/// {
	///     System.Console.Out.WriteLine(
	///         "Device {0}: {1}({2})",
	///         e.Data.DeviceNumber,
	///         e.Data.Command,
	///         e.Data.Data);
	/// }
	/// </code>
	/// </example>
	[Serializable]
	public class DataPacket
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Initialize the data structure with all values set to defaults.
		/// </summary>
		public DataPacket()
		{
		}


		/// <summary>
		///     Parse a text message into a data packet.
		/// </summary>
		/// <param name="aText">parse details from this text</param>
		/// <remarks>
		///     Expected format for reply message is
		///     `@dd a [mm ]ff ssss ww xxxx[:CC]` where dd is the device number,
		///     a is the axis number, mm is the message ID, ff is a flag (OK or RJ
		///     for reject), ssss is the status (IDLE or BUSY), ww is the top
		///     warning flag, xxxx is a list of one or more data values, and CC is
		///     the checksum.
		/// </remarks>
		/// <exception cref="MalformedTextException">
		///     The text could not
		///     be parsed and so is malformed.
		/// </exception>
		public DataPacket(string aText)
		{
			IsComplete = true;
			Text = aText;

			// Extract message header
			if (aText.StartsWith("!", StringComparison.Ordinal))
			{
				MessageType = MessageType.Alert;
			}
			else if (aText.StartsWith("#", StringComparison.Ordinal))
			{
				MessageType = MessageType.Comment;
			}
			else if (aText.StartsWith("/", StringComparison.Ordinal))
			{
				MessageType = MessageType.Request;
			}
			else if (aText.StartsWith("@", StringComparison.Ordinal))
			{
				MessageType = MessageType.Response;
			}
			else
			{
				throw new MalformedTextException("Message has invalid type (first character): " + aText);
			}

			// CHECKSUM 
			string content;
			if ((aText.Length < 4) || (':' != aText[aText.Length - 3]))
			{
				// No checksum.
				try
				{
					content = aText.Substring(1);
				}
				catch (ArgumentOutOfRangeException)
				{
					throw new MalformedTextException("Reply too short to be valid: " + aText);
				}
			}
			else
			{
				// Evaluate checksum.
				content = aText.Substring(1, aText.Length - 4);
				var checksumText = aText.Substring(aText.Length - 2);
				#pragma warning disable IDE0018 // Inline variable declaration
				byte checksum;
				#pragma warning restore IDE0018 // Inline variable declaration
				IsChecksumInvalid = !byte.TryParse(checksumText,
												   NumberStyles.HexNumber,
												   CultureInfo.InvariantCulture,
												   out checksum);
				if (!IsChecksumInvalid)
				{
					foreach (var c in Encoding.ASCII.GetBytes(content))
					{
						checksum = (byte) ((checksum + c) & 0xFF);
					}

					IsChecksumInvalid = 0 != checksum;
				}
			}

			// To be continued.
			if (content.EndsWith(@"\"))
			{
				IsComplete = false;
				content = content.Substring(0, content.Length - 1);
			}

			char[] delimiters = { ' ', '\r', '\n' };

			Body = content;

			// ADDRESS, AXIS, AND MESSAGE ID
			// Extract device address
			var tokens = Body.Split(delimiters, 2, StringSplitOptions.RemoveEmptyEntries);
			if ((tokens.Length > 0)
			&& byte.TryParse(tokens[0], NumberStyles.Integer, CultureInfo.InvariantCulture, out _deviceNumber))
			{
				// Device address exists. Check for more contents.
				if (tokens.Length > 1)
				{
					// More contents in message
					Body = tokens[1];

					// Extract axis number
					tokens = Body.Split(delimiters, 2, StringSplitOptions.RemoveEmptyEntries);
					if ((tokens.Length > 0) && int.TryParse(tokens[0], out var axisNumberCandidate))
					{
						// Axis number exists. Set it, then check for more contents.
						AxisNumber = axisNumberCandidate;
						if (tokens.Length > 1)
						{
							// More contents in message.
							Body = tokens[1];

							// Extract message ID
							tokens = Body.Split(delimiters, 2, StringSplitOptions.RemoveEmptyEntries);
							if ((tokens.Length > 0) && byte.TryParse(tokens[0], out var messageIdCandidate))
							{
								// Message ID exists
								MessageIdType = MessageIdType.Numeric;
								_messageId = messageIdCandidate;

								// Check for more contents.
								Body = tokens.Length > 1 ? tokens[1] : "";
							} // end if (message ID)
							else if ((tokens.Length > 0) && tokens[0].Equals(WarningFlags.None))
							{
								// Suppressing message ID
								MessageIdType = MessageIdType.SuppressReply;

								// Check for more contents.
								Body = tokens.Length > 1 ? tokens[1] : "";
							}
							else
							{
								MessageIdType = MessageIdType.None;
							}
						}
						else
						{
							// End of message
							Body = "";
						}
					} // end if (axis)
					else if ((MessageType.Response == MessageType)
						 || (MessageType.Alert == MessageType)
						 || (MessageType.Comment == MessageType))
					{
						throw new MalformedTextException("Received a message without an axis number: " + aText);
					}
				}
				else
				{
					// End of message
					Body = "";
				}
			} // end if (address)
			else if ((MessageType.Response == MessageType)
				 || (MessageType.Alert == MessageType)
				 || (MessageType.Comment == MessageType))
			{
				throw new MalformedTextException("Received a message without a device address: " + aText);
			}

			// Extract reply contents
			if (MessageType.Response == MessageType)
			{
				tokens = Body.Split(delimiters, 4, StringSplitOptions.RemoveEmptyEntries);

				if (!IsComplete)
				{
					// With message continuations is is possible for the reply payload to be empty; 
					// to preserve previously existing logic we start the payload with an empty string
					// instead of a missing message part.
					Array.Resize(ref tokens, tokens.Length + 1);
					tokens[tokens.Length - 1] = string.Empty;
				}

				if (tokens.Length < 4)
				{
					throw new MalformedTextException("Received malformed reply: " + aText);
				}

				var flag = tokens[0];
				IsError = flag.Equals("RJ");
				var status = tokens[1];
				IsIdle = status.Equals("IDLE");
				FlagText = tokens[2];
				HasFault = FlagText.StartsWith("F", StringComparison.OrdinalIgnoreCase);
				TextData = tokens[3];
				IsAgain = IsError && tokens[3].Equals("AGAIN");
				TextDataValues = TextData.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)
									  .ToList()
									  .AsReadOnly();
				var dataValuesList = new List<decimal?>();

				foreach (var s in TextDataValues)
				{
					if (s == "NA")
					{
						dataValuesList.Add(null);
					}
					else if (decimal.TryParse(s, NumberStyles.Any, CultureInfo.InvariantCulture, out var dataValue))
					{
						dataValuesList.Add(dataValue);
					}
					else
					{
						// As soon as you find something that doesn't
						// look like a data value, stop looking.
						break;
					}
				}

				NumericValues = dataValuesList.AsReadOnly();
				#pragma warning disable CS0618 // Type or member is obsolete
				NumericDataValues = dataValuesList.Select(value => value ?? 0).ToList().AsReadOnly();
				DataValues = dataValuesList
						 .Select(d => (int) Math.Max(int.MinValue, Math.Min(int.MaxValue, Math.Round(d ?? 0))))
						 .ToList()
						 .AsReadOnly();
				#pragma warning restore CS0618 // Type or member is obsolete

				NumericData = dataValuesList.Count > 0 ? dataValuesList[0] ?? 0 : 0m;
			}
			else if (MessageType.Alert == MessageType)
			{
				tokens = Body.Split(delimiters, 2, StringSplitOptions.RemoveEmptyEntries);
				if ((2 == tokens.Length) && (tokens[0].Equals("IDLE") || tokens[0].Equals("BUSY")))
				{
					IsIdle = tokens[0].Equals("IDLE");
					FlagText = tokens[1];
					HasFault = FlagText.StartsWith("F", StringComparison.OrdinalIgnoreCase);
				}

				// else do not throw an exception: some alerts don't have 
				// status or flag fields. That's OK.
			}
			else if (MessageType.Request == MessageType)
			{
				// Try to find a data value, if one exists.
				var dataValuesList = new List<decimal?>();

				// Look backwards: data values come at the end.
				foreach (var token in Body.Split(' ').Reverse())
				{
					if (token == "NA")
					{
						dataValuesList.Add(null);
					}
					else if (decimal.TryParse(token, NumberStyles.Any, CultureInfo.InvariantCulture, out var dataValue))
					{
						dataValuesList.Add(dataValue);
					}
					else
					{
						// As soon as you find something that doesn't
						// look like a data value, stop looking.
						break;
					}
				}

				if (dataValuesList.Count > 0)
				{
					// Reverse it again so that everything is in order.
					dataValuesList.Reverse();
					NumericValues = dataValuesList.AsReadOnly();
					NumericData = dataValuesList[0] ?? 0;
					#pragma warning disable CS0618 // Type or member is obsolete
					NumericDataValues = dataValuesList.Select(value => value ?? 0).ToList().AsReadOnly();
					DataValues = dataValuesList
							 .Select(d => (int) Math.Max(int.MinValue, Math.Min(int.MaxValue, Math.Round(d ?? 0))))
							 .ToList()
							 .AsReadOnly();
					#pragma warning restore CS0618 // Type or member is obsolete
				}
			}
			else if (MessageType.Comment == MessageType)
			{
				if (Body.StartsWith("cont "))
				{
					IsContinuation = true;
					Body = Body.Substring("cont".Length).Trim(); // Intentilally keeping the space after "cont".
				}
			}
		}


		/// <summary>
		///     Copy constructor.
		/// </summary>
		/// <param name="aSource">copy details from this packet</param>
		public DataPacket(DataPacket aSource)
		{
			if (null == aSource)
			{
				throw new ArgumentNullException(nameof(aSource));
			}

			_deviceNumber = aSource._deviceNumber;
			AxisNumber = aSource.AxisNumber;
			_command = aSource._command;
			_numericData = aSource._numericData;
			_dataAsInt = aSource._dataAsInt;
			Measurement = aSource.Measurement;
			Body = aSource.Body;
			IsError = aSource.IsError;
			IsAgain = aSource.IsAgain;
			MessageType = aSource.MessageType;
			Text = aSource.Text;
			FlagText = aSource.FlagText;
			HasFault = aSource.HasFault;
			TextData = aSource.TextData;
			TextDataValues = aSource.TextDataValues; // OK to not duplicate since it should be a read-only collection.
			NumericValues = aSource.NumericValues;
			#pragma warning disable CS0618 // Type or member is obsolete
			NumericDataValues = aSource.NumericDataValues;
			DataValues = aSource.DataValues; // OK to not duplicate since it should be a read-only collection.
			#pragma warning restore CS0618 // Type or member is obsolete
			IsIdle = aSource.IsIdle;
			IsChecksumInvalid = aSource.IsChecksumInvalid;
			_messageId = aSource._messageId;
			MessageIdType = aSource.MessageIdType;
			IsComplete = aSource.IsComplete;
			IsContinuation = aSource.IsContinuation;
		}



		/// <summary>
		///     JoinContinuations a series of message continuations into a single packet.
		/// </summary>
		/// <param name="aPacketList">The packets to merge. There must be at least
		/// two, the first one must be a response with <see cref="IsComplete"/> = false,
		/// and all subsequent ones must be info messages with <see cref="IsContinuation"/> = true.
		/// Ordering in important. All must have the same device number, axis number and
		/// message ID. The last message must be complete.</param>
		/// <returns>A single packet that combines the payloads of all the input packets,
		/// as if received as a single long message</returns>
		/// <exception cref="ArgumentNullException">The input list is null.</exception>
		/// <exception cref="ArgumentException">There are fewer than two entries in the input list.</exception>
		/// <exception cref="InvalidOperationException">The input list doesn't satisfy the
		/// constraints outlined in the summary.</exception>
		public static DataPacket JoinContinuations(IEnumerable<DataPacket> aPacketList)
		{
			var sb = new StringBuilder();

			if (aPacketList is null)
			{
				throw new ArgumentNullException(nameof(aPacketList));
			}

			var head = aPacketList.First();
			if ((MessageType.Response != head.MessageType) || head.IsComplete)
			{
				throw new InvalidOperationException("First packet must be an incomplete response.");
			}

			// use the verbatim text of the header to keep the address and message ID,
			// but strip off everything from the '\' at the end.
			var headerText = head.Text;
			headerText = headerText.Substring(0, headerText.IndexOf('\\'));
			sb.Append(headerText.Trim());

			DataPacket last = null;
			foreach (var part in aPacketList.Skip(1))
			{
				if ((part.DeviceNumber != head.DeviceNumber)
				    || (part.AxisNumber != head.AxisNumber)
				    || (part.MessageId != head.MessageId)
				    || (MessageType.Comment != part.MessageType)
				    || !part.IsContinuation)
				{
					throw new InvalidOperationException("Packet does not continue the initial packet.");
				}

				if (!string.IsNullOrEmpty(part.Body))
				{
					sb.Append(" ");
					sb.Append(part.Body);
				}

				last = part;
			}

			if (last is null)
			{
				throw new InvalidOperationException("No continuations were provided.");
			}
			else if (!last.IsComplete)
			{
				throw new InvalidOperationException("The packet sequence is incomplete.");
			}

			return new DataPacket(sb.ToString());
		}


		/// <summary>
		///     Format the data packet as a string, assuming that it was sent as a
		///     request.
		/// </summary>
		/// <returns>The message as a string</returns>
		public virtual string FormatRequest() => Format("requests");


		/// <summary>
		///     Format the message as a string, assuming that it was sent as a
		///     response.
		/// </summary>
		/// <returns>The message as a string</returns>
		public virtual string FormatResponse() => Format("responds");


		/// <summary>
		///     Convenience function for iterating over all the numeric
		///     values in a message payload, tracking the axis number.
		/// </summary>
		/// <param name="aCallback">Callback to invoke for each value.
		/// The callback received the 0-based index of the data and the
		/// data itself, which may be null if the device responded with "NA".</param>
		public void ForEachNumericValue(Action<int, decimal?> aCallback)
		{
			for (int i = 0; i < NumericValues.Count; i++)
			{
				aCallback(i, NumericValues[i]);
			}
		}


		/// <summary>
		///     Throws an exception if the checksum was invalid.
		/// </summary>
		/// <exception cref="ZaberPortErrorException">
		///     When the checksum was
		///     invalid.
		/// </exception>
		public void ValidateChecksum()
		{
			if (IsChecksumInvalid)
			{
				throw new ZaberPortErrorException(ZaberPortError.InvalidChecksum);
			}
		}


		/// <summary>
		///     Data value to go with the command or response.
		/// </summary>
		/// <value>
		///     Any integer value, positive or negative. Details depend on the
		///     different command values. See the user manual.
		/// </value>
		/// <remarks>
		///     This property is deprecated in favor of <see cref="NumericData" />
		///     because a 32-bit integer cannot represent the full range an precision of
		///     firmware data. Values out of range will be clamped to the integer range, and
		///     non-integer values will be rounded.
		/// </remarks>
		[Obsolete("Use the NumericData property instead.")]
		public int Data
		{
			get => _dataAsInt;
			set
			{
				_dataAsInt = value;
				_numericData = value;
			}
		}


		/// <summary>
		///     Data value to go with the command or response.
		/// </summary>
		/// <value>
		///     Any integer value, positive or negative. Details depend on the
		///     different command values. See the user manual.
		/// </value>
		/// <remarks>
		///     For ASCII responses with multiple data values, this
		///     will hold the first data value. <see cref="NumericDataValues" /> will hold
		///     all the data values. If the data value is text, then this will
		///     be zero.
		/// </remarks>
		public decimal NumericData
		{
			get => _numericData;
			set
			{
				_numericData = value;
				_dataAsInt = (int) Math.Max(int.MinValue, Math.Min(int.MaxValue, Math.Round(value)));
			}
		}


		/// <summary>
		///     An array of data values to go with the command or response.
		/// </summary>
		/// <value>
		///     An array of integer values, positive or negative. Deatils
		///     depend on the different commands. See the user manual.
		/// </value>
		/// <remarks>
		///     This property is deprecated in favor of <see cref="NumericValues" />
		///     because a 32-bit integer cannot represent the full range an precision of
		///     firmware data. Values out of range will be clamped to the integer range, and
		///     non-integer values will be rounded.
		/// </remarks>
		[Obsolete("Use the NumericValues property instead.")]
		public IList<int> DataValues { get; }


		/// <summary>
		///     An array of data values to go with the command or response.
		/// </summary>
		/// <value>
		///     An array of integer values, positive or negative. Deatils
		///     depend on the different commands. See the user manual.
		/// </value>
		/// <remarks>
		///     This property is deprecated in favor of <see cref="NumericValues" />
		///     because firmware does not provide values for settings that do not apply for particular axis.
		///     If any of the data values cannot be parsed as a number, the parsing will stop at that value.
		///     <see cref="TextDataValues" /> will hold the text version of each data value.
		/// </remarks>
		[Obsolete("Use the NumericValues property instead.")]
		public IList<decimal> NumericDataValues { get; }


		/// <summary>
		///     An array of data values to go with the command or response.
		/// </summary>
		/// <value>
		///     An array of nullable integer values, positive or negative. 
		///     Deatils depend on the different commands. See the user manual.
		/// </value>
		/// <remarks>
		///     If any of the data values is "NA", then the corresponding entry will be null.
		///     If any of the data values cannot be parsed as a number, the parsing will stop at that value.
		///     <see cref="TextDataValues" /> will hold the text version of each data value.
		/// </remarks>
		public IList<decimal?> NumericValues { get; }


		/// <summary>
		///     Gets or sets a measurement that represents the data value in
		///     another unit of measure.
		/// </summary>
		public Measurement Measurement { get; set; }


		/// <summary>
		///     The specific command to execute. See the user manual for a list of
		///     supported commands and the data values they support. Some common
		///     commands are listed in the <see cref="Zaber.Command" /> enumeration.
		/// </summary>
		/// <value>
		///     You can assign any byte value, but you have to cast it to a Command
		///     type first.
		/// </value>
		/// <example>
		///     Use one of the values from the Command enumeration.
		///     <code>
		/// DataPacket data = new DataPacket();
		/// data.DeviceNumber = 3;
		/// data.Command = Command.ConstantSpeed;
		/// data.Data = 100;
		/// </code>
		/// </example>
		/// <example>
		///     Cast a byte value to a command value.
		///     <code>
		/// byte commandFromUser = 43; // Maybe these get read from a form
		/// int dataFromUser = 100;
		/// 
		/// DataPacket data = new DataPacket();
		/// data.DeviceNumber = 3;
		/// data.Command = (Command)commandFromUser;
		/// data.Data = dataFromUser;
		/// </code>
		/// </example>
		public Command Command
		{
			get => _command;
			set
			{
				_command = value;
				IsError = _command == Command.Error;
				MessageType = MessageType.Binary;
			}
		}


		/// <summary>
		///     The device number in the chain that this command should be sent to
		///     or was received from.
		/// </summary>
		/// <remarks>
		///     You can send a command directly to a device, or send a command to
		///     an alias device number. 0 is always an alias for all devices, and
		///     each device can set another alias number. Several devices can use
		///     the same alias, and all of them will respond to a command using
		///     the alias device number.
		/// </remarks>
		public byte DeviceNumber
		{
			get => _deviceNumber;
			set => _deviceNumber = value;
		}


		/// <summary>
		///     Gets or sets the axis number on the device that this command
		///     should be sent to or was received from.
		/// </summary>
		/// <remarks>
		///     You can send a command directly to an axis, or send a command
		///     to all axes on the device. Axis number 0 represents all axes
		///     on the device.
		/// </remarks>
		public int AxisNumber { get; set; }


		/// <summary>
		///     Gets or sets the type of message ID in this message.
		/// </summary>
		public MessageIdType MessageIdType { get; set; }


		/// <summary>
		///     Gets or sets the message ID corresponding to this message.
		/// </summary>
		/// <remarks>
		///     Message Ids allow you to coordinate responses with the requests
		///     that triggered them.
		///     In T-series Binary protocol, messages Ids are also known as logical channels.
		/// </remarks>
		public byte MessageId
		{
			get => _messageId;
			set
			{
				_messageId = value;
				MessageIdType = MessageIdType.Numeric;
			}
		}


		/// <summary>
		///     Gets a flag showing if this packet is an error message.
		/// </summary>
		public bool IsError { get; private set; }


		/// <summary>
		///     Gets a flag showing if this packet is an error message with the AGAIN response.
		/// </summary>
		public bool IsAgain { get; }


		/// <summary>
		///     Gets a flag showing if this packet has a warning flag that is a
		///     fault.
		/// </summary>
		public bool HasFault { get; }


		/// <summary>
		///     Gets the data field from a text-mode response, otherwise null.
		/// </summary>
		public string TextData { get; }


		/// <summary>
		///     Gets the flag field from a text-mode response, otherwise null.
		/// </summary>
		public string FlagText { get; }


		/// <summary>
		///     Gets the individual data values from a text-mode response,
		///     otherwise null.
		/// </summary>
		/// <value>The data values are split by spaces and stored here.</value>
		public IList<string> TextDataValues { get; }


		/// <summary>
		///     Gets a flag showing if the device is idle.
		/// </summary>
		public bool IsIdle { get; }


		/// <summary>
		///     Gets the type of message.
		/// </summary>
		public MessageType MessageType { get; private set; }


		/// <summary>
		///     Gets the original text of the message, or null for binary messages.
		/// </summary>
		public string Text { get; }


		/// <summary>
		///     Gets part of the original text of the message, without the start
		///     marker, device number, message ID, or check sum. Only available for text
		///     responses, null for everything else.
		/// </summary>
		public string Body { get; }


		/// <summary>
		///     Gets a flag showing whether the message had a checksum that didn't
		///     match its content.
		/// </summary>
		public bool IsChecksumInvalid { get; }


		/// <summary>
		///     Indicates that this packet contains a complete message. This will only
		///     be false if the message payload ends with '\'.
		/// </summary>
		public bool IsComplete { get; } = true;


		/// <summary>
		///     Indicates that this packet continues a previous incomplete message.
		///     This will only be true if the packet is an info message and the
		///     payload starts with "cont".
		/// </summary>
		public bool IsContinuation { get; }

		#endregion

		#region -- Non-Public Methods --

		private string Format(string aAction)
		{
			if (null != Text)
			{
				return Text;
			}

			var deviceNumberText = DeviceNumber.ToString(CultureInfo.CurrentCulture).PadLeft(3);

			return string.Format(CultureInfo.CurrentCulture,
								 "Device {0} {3} {1}({2})",
								 deviceNumberText,
								 _command,
								 _numericData,
								 aAction);
		}

		#endregion

		#region -- Data --

		private byte _deviceNumber;
		private byte _messageId;
		private Command _command;
		private decimal _numericData;
		private int _dataAsInt;

		#endregion
	}
}

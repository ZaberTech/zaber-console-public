﻿using System;
using System.Collections.Generic;

namespace Zaber.Ports

{
	/// <summary>
	///     "Raw" communication port interface needed for protocol and baud rate detection.
	/// </summary>
	public interface IRawPort
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Open the port.
		/// </summary>
		void Open();


		/// <summary>
		///     Close the port.
		/// </summary>
		void Close();


		/// <summary>
		///     Write raw data to the port.
		/// </summary>
		/// <param name="aData">Buffer containing data to write.</param>
		void Write(byte[] aData);


		/// <summary>
		///     Write an ASCII string.
		/// </summary>
		/// <param name="aData"></param>
		void Write(string aData);


		/// <summary>
		///     Read raw data from the port without parsing.
		/// </summary>
		/// <param name="aCount">Number of bytes to read.</param>
		/// <returns>Bytes read, if the requested count was read before a timeout.</returns>
		/// <exception cref="TimeoutException">A timeout occurred before reading the requested number of bytes.</exception>
		byte[] Read(int aCount);


		/// <summary>
		///     Read ASCII text until a newline is encountered.
		/// </summary>
		/// <returns></returns>
		string ReadLine();


		/// <summary>
		///     Discard any data in the receive buffer.
		/// </summary>
		void ClearReceiveBuffer();


		/// <summary>
		///     Discard any data in the send buffer.
		/// </summary>
		void ClearTransmitBuffer();


		/// <summary>
		///     Get baud rates to try.
		/// </summary>
		/// <returns></returns>
		IEnumerable<int> GetApplicableBaudRates();


		/// <summary>
		///     Timeout for reading data.
		/// </summary>
		int ReadTimeout { get; set; }

		/// <summary>
		///     Get or set the current baud rate, if applicable.
		/// </summary>
		int BaudRate { get; set; }

		#endregion
	}
}

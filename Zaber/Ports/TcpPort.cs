﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using log4net;

namespace Zaber.Ports
{
	/// <summary>
	///     Encapsulates serial port settings, and translates between the byte
	///     streams and the DataPacket structure.
	/// </summary>
	public class TcpPort : PortBase, IZaberPort
	{
		#region -- Public Methods & Properties --

		#region -- Initialization --

		/// <summary>
		///     Creates a new instance.
		/// </summary>
		/// <param name="aHostname">Name or IP address of the device to connect to.</param>
		/// <param name="aPort">TCP port number of the device to connect to.</param>
		/// <param name="aPacketConverter">
		///     Used to convert between the byte stream
		///     and the <see cref="DataPacket" /> structure.
		/// </param>
		public TcpPort(string aHostname, int aPort, PacketConverter aPacketConverter = null)
			: base(aPacketConverter)
		{
			_hostName = aHostname;
			_port = aPort;
		}

		#endregion

		#endregion

		#region -- Non-Public Methods --

		#region -- IDisposable Implementation --

		/// <inheritdoc cref="PortBase.Dispose(bool)" />
		protected override void Dispose(bool aIsDisposing)
		{
			if (aIsDisposing)
			{
				_socket?.Dispose();
			}

			base.Dispose(aIsDisposing);
		}

		#endregion

		#endregion

		#region -- Public Properties --

		/// <summary>
		///     Is the port open?
		/// </summary>
		public virtual bool IsOpen => _socket?.IsBound ?? false;


		/// <summary>
		///     Gets or sets the baud rate of the underlying SerialPort.
		/// </summary>
		public int BaudRate
		{
			get => -1;
			set { }
		}


		/// <summary>
		///     Gets the port name that was last sent to <see cref="Open" />.
		/// </summary>
		/// <value>
		///     The port name that was last sent to <see cref="Open" />, or
		///     null if <see cref="Open" /> has not been called.
		/// </value>
		public string PortName => $"{_hostName}:{_port}";

		#endregion

		#region -- Public Methods --

		/// <summary>
		///     Close and release the serial port. This must be called to avoid
		///     locking the port when you are finished with it. It must also be called
		///     on application exit to terminate the receiver thread.
		/// </summary>
		/// <exception cref="InvalidOperationException">
		///     The port is not open.
		/// </exception>
		/// <exception cref="IOException">
		///     The port does not exist, or a
		///     sharing violation has occurred.
		/// </exception>
		public void Close()
		{
			HaltDelayedPackets();

			// Terminate the reader thread.
			StopServiceThread();
			ClearPartiallyReceivedData();
			DispatchEvents();

			_log.Debug("Closing.");
			CloseSocket();
			_log.Debug("Closed.");
		}


		/// <summary>
		///     Whether this object is equal to another.
		/// </summary>
		/// <param name="aOther">The object to compare this one to.</param>
		/// <returns>True if the objects are equal, false otherwise.</returns>
		public override bool Equals(object aOther)
		{
			if (ReferenceEquals(this, aOther))
			{
				return true;
			}

			if (!base.Equals(aOther))
			{
				return false;
			}

			var other = aOther as TcpPort;

			if (null == other)
			{
				return false;
			}

			if ((IsOpen != other.IsOpen)
			|| !Equals(PacketConverter, other.PacketConverter)
			|| (PortName != other.PortName)
			|| !Equals(_socket, other._socket))
			{
				return false;
			}

			return true;
		}


		/// <summary>
		///     Returns a calculated hash code for this object.
		/// </summary>
		/// <returns>An integer hash code.</returns>
		public override int GetHashCode()
		{
			var code = base.GetHashCode();

			code = (31 * code) + (IsOpen ? 1 : 0);
			if (null != PortName)
			{
				code = (31 * code) + PortName.GetHashCode();
			}

			if (null != _socket)
			{
				code = (31 * code) + _socket.GetHashCode();
			}

			return code;
		}


		/// <summary>
		///     Implements a deprecated interface method. Do not use.
		/// </summary>
		/// <exception cref="NotImplementedException">Always thrown.</exception>
		public string[] GetPortNames()
			=> throw new NotImplementedException("This method is specific to serial ports and is deprecated.");


		/// <summary>
		///     Open the port to begin sending and receiving data. Be sure to call
		///     <see cref="IDisposable.Dispose()" /> when you are finished with
		///     the port.
		/// </summary>
		/// <param name="aHostAndPort">
		///     DEPRECATED. Pass in null to use connection settings configured
		///     when the port was created (recommended). If non-null, the format is expected
		///     to be "hostname:port" where hostname can also be an IP address.
		/// </param>
		public void Open(string aHostAndPort = null)
		{
			_log.Debug("Opening.");

			if (!string.IsNullOrEmpty(aHostAndPort))
			{
				var parts = aHostAndPort.Split(':');
				int port;
				if ((parts.Length != 2) || !int.TryParse(parts[1], out port))
				{
					throw new ArgumentException(
						$"Connection string '{aHostAndPort}' not in expected host:port format.");
				}

				_hostName = parts[0];
				_port = port;
			}

			try
			{
				OpenSocket(_hostName, _port);
			}
			catch (Exception e)
			{
				_log.Error($"Error opening TCP/IP connection to {aHostAndPort}.", e);
				throw;
			}

			// Start up the receiver thread.
			StartServiceThread();

			_log.Debug($"Opened {PortName}.");
		}


		/// <summary>
		///     Timeout for waiting for data to be received.
		/// </summary>
		public int ReadTimeout
		{
			get => _socket.ReceiveTimeout;
			set => _socket.ReceiveTimeout = value;
		}

		#endregion

		#region -- Event Handlers --

		private static void Socket_DataReceived(IAsyncResult aResult)
		{
			var port = aResult.AsyncState as TcpPort;
			if (!port.ServiceThreadIsExiting)
			{
				var socket = port._socket;
				var numRead = 0;

				try
				{
					numRead = socket.EndReceive(aResult);
				}
				catch (Exception aException)
				{
					port.HandleReceiveException(aException);
				}

				if (numRead > 0)
				{
					var smallBuf = new byte[numRead];
					Array.Copy(port._receiveBuffer, smallBuf, numRead);
					port.QueueReceived(smallBuf);
					port.KickServiceThread();
				}

				try
				{
					socket.BeginReceive(port._receiveBuffer,
										0,
										port._receiveBuffer.Length,
										SocketFlags.None,
										Socket_DataReceived,
										port);
				}
				catch (Exception aException)
				{
					port.HandleReceiveException(aException);
				}
			}
		}


		/// <inheritdoc cref="PortBase.ReceiveDispatch()" />
		protected override void ReceiveDispatch()
		{
			// Start the async receive callback loop, then enter the normal receive dispatcher.
			try
			{
				_socket.BeginReceive(_receiveBuffer,
									 0,
									 _receiveBuffer.Length,
									 SocketFlags.None,
									 Socket_DataReceived,
									 this);
			}
			catch (Exception aException)
			{
				HandleReceiveException(aException);
			}

			base.ReceiveDispatch();
		}


		/// <inheritdoc cref="PortBase.OnErrorReceived(ZaberPortErrorReceivedEventArgs)" />
		protected override void OnErrorReceived(ZaberPortErrorReceivedEventArgs aError)
		{
			_log.ErrorFormat(CultureInfo.CurrentCulture, "Port error: {0}", aError.ErrorType);
			if (null != aError.Message)
			{
				_log.Error(aError.Message);
			}

			if (CanInvokeErrorReceived && IsOpen)
			{
				InvokeErrorReceived(this, aError);
			}
			else if (!IsOpen)
			{
				CloseSocket();
			}
		}

		#endregion

		#region -- Private Methods --

		/// <summary>
		///     Write a stream of bytes to the port.
		/// </summary>
		/// <param name="aData">The bytes to write</param>
		/// <remarks>
		///     This is protected so that it can be stubbed out during
		///     testing.
		/// </remarks>
		/// <exception cref="InvalidOperationException">The port is not open.</exception>
		/// <exception cref="TimeoutException">
		///     The operation did not complete
		///     before the time-out period ended.
		/// </exception>
		/// <exception cref="SocketException">A networking error other than a timeout occurred.</exception>
		protected override void WriteData(byte[] aData)
		{
			lock (SendLock)
			{
				if (!IsOpen)
				{
					throw new InvalidOperationException("The port is not open.");
				}

				try
				{
					_socket.Send(aData);
				}
				catch (SocketException se)
				{
					_log.Warn("Got an error sending socket data.", se);
					switch (se.SocketErrorCode)
					{
						case SocketError.TimedOut:
							throw new TimeoutException("Socket send timed out.", se);
						default:
							throw;
					}
				}
			}
		}


		private void HandleReceiveException(Exception aException)
		{
			if (aException is ObjectDisposedException ode)
			{
				_log.Error("Socket was closed unexpectedly.", ode);
			}
			else if (aException is SocketException se)
			{
				_log.Error("There was a socket error during operation.", se);
			}
			else
			{
				throw aException;
			}

			ThreadPool.QueueUserWorkItem(state =>
			{
				try
				{
					Close();
				}
				catch (Exception e)
				{
					_log.Error("Additional error while resetting port state.", e);
				}

				InvokeClosedUnexpectedly(this);
			});
		}

		#endregion

		#region -- Socket helpers --

		private void OpenSocket(string aHostname, int aPort)
		{
			if (null != _socket)
			{
				throw new InvalidOperationException("Socket opened twice.");
			}

			var addresses = Dns.GetHostAddresses(aHostname);

			// On some computers, looking up a hostname may return a non-functional IPv6 address; only use IPv4.
			var address = addresses.Where(addr => addr.AddressFamily == AddressFamily.InterNetwork).FirstOrDefault();
			if (null == address)
			{
				throw new ArgumentException($"Can't locate an IP address for {aHostname}.");
			}

			var endPoint = new IPEndPoint(address, aPort);
			var sock = new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
			sock.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);

			// Disable Nagle's algorithm at the PC end.
			// Note this should also be done at the device end if possible, or performance may suffer.
			// See http://www.stuartcheshire.org/papers/NagleDelayedAck/
			sock.NoDelay = true;

			sock.Connect(endPoint);

			_socket = sock;
		}


		private void CloseSocket()
		{
			if (null != _socket)
			{
				if (_socket.Connected)
				{
					try
					{
						_socket.Shutdown(SocketShutdown.Both);
						_socket.Close();
					}
					catch (SocketException se)
					{
						_log.Error("Exception while closing socket.", se);
					}
					catch (ObjectDisposedException ode)
					{
						_log.Error("Exception while closing socket.", ode);
					}
				}

				_socket = null;
			}
		}

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(typeof(TcpPort));

		private string _hostName;
		private int _port;
		private Socket _socket;
		private readonly byte[] _receiveBuffer = new byte[1024];

		#endregion
	}
}

﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.IO.Ports;
using System.Threading.Tasks;
using log4net;

namespace Zaber.Ports
{
	/// <summary>
	///     Encapsulates serial port settings, and translates between the byte
	///     streams and the DataPacket structure.
	/// </summary>
	public class RS232Port : PortBase, IZaberPort
	{
		#region -- Public Methods & Properties --

		#region -- Initialization --

		/// <summary>
		///     Creates a new instance.
		/// </summary>
		/// <param name="aSerialPort">Serial port to communicate with.</param>
		/// <param name="aPacketConverter">
		///     Used to convert between the byte stream
		///     and the <see cref="DataPacket" /> structure.
		/// </param>
		public RS232Port(SerialPort aSerialPort, PacketConverter aPacketConverter)
			: base(aPacketConverter)
		{
			_serialPort = aSerialPort;
			PortName = _serialPort.PortName;
			if (SerialPort.InfiniteTimeout == aSerialPort.ReadTimeout)
			{
				_serialPort.ReadTimeout = DEFAULT_READ_TIMEOUT;
			}
		}

		#endregion

		#endregion

		#region -- Non-Public Methods --

		#region -- IDisposable Implementation --

		/// <inheritdoc cref="PortBase.Dispose(bool)" />
		protected override void Dispose(bool aIsDisposing)
		{
			if (aIsDisposing)
			{
				if (null != _serialPort.Container) // Force the container to dispose too.
				{
					_serialPort.Container.Dispose();
				}

				if ((null != _baseStream) && _baseStream.CanRead)
				{
					// Force the base stream to close first before disposing, 
					// even if _serialPort thinks it is closed.
					_baseStream.Close();
					GC.ReRegisterForFinalize(_baseStream);
				}

				_serialPort.Dispose();
			}

			base.Dispose(aIsDisposing);
		}

		#endregion

		#endregion

		#region -- Public Properties --

		/// <summary>
		///     Is the port open?
		/// </summary>
		public virtual bool IsOpen => _serialPort.IsOpen;


		/// <summary>
		///     Gets or sets the baud rate of the underlying SerialPort.
		/// </summary>
		public int BaudRate
		{
			get => _serialPort.BaudRate;
			set => _serialPort.BaudRate = value;
		}


		/// <summary>
		///     Gets the port name that was last sent to <see cref="Open" />.
		/// </summary>
		/// <value>
		///     The port name that was last sent to <see cref="Open" />, or
		///     null if <see cref="Open" /> has not been called.
		/// </value>
		public string PortName { get; set; }


		/// <inheritdoc cref="IZaberPort.ReadTimeout" />
		public int ReadTimeout
		{
			get => _serialPort.ReadTimeout;
			set => _serialPort.ReadTimeout = value;
		}

		#endregion

		#region -- Public Methods --

		/// <summary>
		///     Close and release the serial port. This must be called to avoid
		///     locking the port when you are finished with it. It must also be called
		///     on application exit to terminate the receiver thread.
		/// </summary>
		/// <exception cref="InvalidOperationException">
		///     The port is not open.
		/// </exception>
		/// <exception cref="IOException">
		///     The port does not exist, or a
		///     sharing violation has occurred.
		/// </exception>
		public void Close()
		{
			lock (_lock)
			{
				if (!_isOpened)
				{
					throw new InvalidOperationException("Port is already closed.");
				}
			}

			HaltDelayedPackets();

			// Terminate the reader thread.
			StopServiceThread();
			ClearPartiallyReceivedData();
			DispatchEvents();

			_log.Debug("Closing.");

			// Explicitly release the base stream so we don't get an 
			// UnauthorizedAccessException if we re-open the port too quickly.
			if (null != _baseStream)
			{
				GC.ReRegisterForFinalize(_baseStream);
			}

			lock (_lock)
			{
				DetachPortEvents(_serialPort);
				_serialPort.Close();
				_isOpened = false;
			}

			_log.Debug("Closed.");
		}


		/// <summary>
		///     Whether this object is equal to another.
		/// </summary>
		/// <param name="aOther">The object to compare this one to.</param>
		/// <returns>True if the objects are equal, false otherwise.</returns>
		public override bool Equals(object aOther)
		{
			if (ReferenceEquals(this, aOther))
			{
				return true;
			}

			if (!base.Equals(aOther))
			{
				return false;
			}

			if (!(aOther is RS232Port other))
			{
				return false;
			}

			if ((BaudRate != other.BaudRate)
			|| (IsOpen != other.IsOpen)
			|| (PortName != other.PortName)
			|| !Equals(_serialPort, other._serialPort))
			{
				return false;
			}

			return true;
		}


		/// <summary>
		///     Returns a calculated hash code for this object.
		/// </summary>
		/// <returns>An integer hash code.</returns>
		public override int GetHashCode()
		{
			var code = base.GetHashCode();

			code = (31 * code) + BaudRate;
			code = (31 * code) + (IsOpen ? 1 : 0);

			if (null != PortName)
			{
				code = (31 * code) + PortName.GetHashCode();
			}

			if (null != _serialPort)
			{
				code = (31 * code) + _serialPort.GetHashCode();
			}

			return code;
		}


		/// <summary>
		///     Get a list of all available serial ports.
		/// </summary>
		/// <returns>
		///     An array of port names, one of which should be passed to
		///     <see cref="Open" />. The names are sorted.
		/// </returns>
		public static string[] GetSerialPortNames()
		{
			var portNames = SerialPort.GetPortNames();
			Array.Sort(portNames);
			return portNames;
		}


		/// <summary>
		///     Get a list of all available serial ports.
		/// </summary>
		/// <returns>
		///     An array of port names, one of which should be passed to
		///     <see cref="Open" />. The names are sorted.
		/// </returns>
		public string[] GetPortNames() => GetSerialPortNames();


		/// <summary>
		///     Open the port to begin sending and receiving data. Be sure to call
		///     <see cref="IDisposable.Dispose()" /> when you are finished with
		///     the port.
		/// </summary>
		/// <param name="aPortName">
		///     DEPRECATED. Pass in null to use the serial port this
		///     object was configured to use at instantiation time (recommended). If a non-null
		///     value is used, it must be a valid name for a serial port attached to the system.
		/// </param>
		[SuppressMessage("Microsoft.Usage",
			"CA1816:CallGCSuppressFinalizeCorrectly",
			Justification = "Needed to make SerialPort class work properly.")]
		public void Open(string aPortName = null)
		{
			_log.Debug("Opening.");

			aPortName = aPortName ?? PortName;
			_serialPort.PortName = aPortName;

			lock (_lock)
			{
				_serialPort.Open();
				PortName = aPortName;
				_isOpened = true;
				_serialPort.DtrEnable = true;
				_serialPort.DiscardInBuffer();
				_serialPort.DiscardOutBuffer();
				AttachPortEvents(_serialPort);
				_closedUnexpectedlyRaised = false;
			}


			// Hang onto the base stream ourselves so we can close it manually
			// if the serial port gets suddenly unplugged. This solves a
			// problem in the .NET framework where an unplugged serial port
			// will leave its stream open.
			_baseStream = _serialPort.BaseStream;
			GC.SuppressFinalize(_baseStream);

			// Start up the receiver thread.
			StartServiceThread();

			_log.Debug("Opened.");
		}

		#endregion

		#region -- Event Handlers --

		private void SerialPort_DataReceived(object aSender, SerialDataReceivedEventArgs aArgs)
		{
			var port = (SerialPort) aSender;

			// Credit to Ben Voigt for suggesting using BaseStream.BeginRead
			// for faster, more reliable reads by reducing P/Invoke calls: 
			// http://www.sparxeng.com/blog/software/must-use-net-system-io-ports-serialport
			// Note that this approach must be used *instead* of using the
			// SerialPort class to read, never in combination with. SerialPort
			// keeps its own internal read buffer, and things could get lost
			// there if we allow SerialPort to read for itself.
			try
			{
				// SerialPort.ReadBufferSize is the size of the underlying
				// Windows/UART serial port buffer, *not* the extra one that the
				// SerialPort class uses internally. Therefore, it is the size
				// of the buffer available to the underlying SerialStream.
				var buffer = new byte[port.ReadBufferSize];

				var result = port.BaseStream.BeginRead(buffer, 0, buffer.Length, null, null);

				try
				{
					// SerialPort locks the underlying SerialStream before 
					// raising the DataReceived event. This guarantees that only
					// one thread will be reading at a time, but only so long as
					// the handler is still running. We use EndRead() here 
					// instead of a callback from BeginRead() to hold the lock 
					// until we're done reading.
					var bytesRead = port.BaseStream.EndRead(result);

					Array.Resize(ref buffer, bytesRead);
					QueueReceived(buffer);
					KickServiceThread();
				}
				catch (IOException ioe)
				{
					OnErrorReceived(new ZaberPortErrorReceivedEventArgs(ZaberPortError.ReceiveException));
					_log.Error("Encountered an error while reading.", ioe);
				}
			}
			catch (InvalidOperationException)
			{
				// The port has been closed while we were reading.
				// Stop trying to read.
				lock (PacketConverter)
				{
					_log.WarnFormat("Port closed while reading. Flushing PacketConverter buffer. Buffer contents: {0} {1}",
									PacketConverter.FormatPartialPacket(),
									PacketConverter.FormatPartialCompletions());
					PacketConverter.ClearBuffer();
				}
			}
		}


		/// <summary>
		///     Receive an error from the port.
		/// </summary>
		/// <param name="aSender">The port</param>
		/// <param name="aArgs">Details of the error.</param>
		private void SerialPort_ErrorReceived(object aSender, SerialErrorReceivedEventArgs aArgs)
			=> OnErrorReceived(new ZaberPortErrorReceivedEventArgs((ZaberPortError) aArgs.EventType));


		/// <inheritdoc cref="PortBase.OnErrorReceived(ZaberPortErrorReceivedEventArgs)" />
		protected override void OnErrorReceived(ZaberPortErrorReceivedEventArgs aError)
		{
			_log.ErrorFormat(CultureInfo.CurrentCulture, "Port error: {0}", aError.ErrorType);
			if (null != aError.Message)
			{
				_log.Error(aError.Message);
			}

			if (CanInvokeErrorReceived && IsOpen)
			{
				InvokeErrorReceived(this, aError);
			}
			else if (!IsOpen)
			{
				DetachPortEvents(_serialPort);
				_serialPort.Close();
			}
		}

		#endregion

		#region -- Private Methods --

		/// <summary>
		///     Write a stream of bytes to the port.
		/// </summary>
		/// <param name="aData">The bytes to write</param>
		/// <remarks>
		///     This is protected so that it can be stubbed out during
		///     testing.
		/// </remarks>
		/// <exception cref="InvalidOperationException">
		///     The specified port is
		///     not open.
		/// </exception>
		/// <exception cref="TimeoutException">
		///     The operation did not complete
		///     before the time-out period ended.
		/// </exception>
		protected override void WriteData(byte[] aData)
		{
			if (!CheckIsConsistentlyOpen())
			{
				return;
			}

			lock (SendLock)
			{
				_serialPort.Write(aData, 0, aData.Length);
			}
		}


		/// <summary>
		///     Checks whether the internal information about port being opened matches the port's state.
		///     The state may not match in case when port becomes physically disconnected.
		///     Raises ClosedUnexpectedly event when such situation is detected.
		/// </summary>
		/// <returns>True when port is opened correctly.</returns>
		private bool CheckIsConsistentlyOpen()
		{
			var raise = false;

			lock (_lock)
			{
				var isInconsistent = _isOpened && !_serialPort.IsOpen;

				if (!isInconsistent)
				{
					return true;
				}

				if (!_closedUnexpectedlyRaised)
				{
					_closedUnexpectedlyRaised = true;
					raise = true;
				}
			}

			if (raise)
			{
				Task.Factory.StartNew(() => { InvokeClosedUnexpectedly(this); });
			}

			return false;
		}

		#endregion

		#region -- Event helpers --

		private void AttachPortEvents(SerialPort aPort)
		{
			aPort.DataReceived += SerialPort_DataReceived;
			aPort.ErrorReceived += SerialPort_ErrorReceived;
		}


		private void DetachPortEvents(SerialPort aPort)
		{
			aPort.DataReceived -= SerialPort_DataReceived;
			aPort.ErrorReceived -= SerialPort_ErrorReceived;
		}

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(typeof(RS232Port));
		private const int DEFAULT_READ_TIMEOUT = 100;

		private readonly object _lock = new object();
		private readonly SerialPort _serialPort;
		private Stream _baseStream;

		private bool _isOpened;
		private bool _closedUnexpectedlyRaised;

		#endregion
	}
}

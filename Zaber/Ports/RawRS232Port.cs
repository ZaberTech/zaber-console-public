﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Reflection;
using log4net;

namespace Zaber.Ports
{
	/// <summary>
	///     Simplified synchronous serial port implementation used for detection of protocol
	///     and baud rate. Does not do any packet parsing.
	/// </summary>
	public class RawRS232Port : IRawPort
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Create a raw port wrapper for a the named serial port.
		/// </summary>
		/// <param name="aPort">The serial port resource to use</param>
		public RawRS232Port(SerialPort aPort)
		{
			_port = aPort;
		}


		/// <inheritdoc cref="IRawPort.Open" />
		public void Open()
		{
			_port.Open();
			_port.DtrEnable = true;
			_log.DebugFormat("Opened {0}", _port.PortName);
		}


		/// <inheritdoc cref="IRawPort.Close" />
		public void Close()
		{
			if (_port.IsOpen)
			{
				_port.Close();
				_log.DebugFormat("Closed {0}", _port.PortName);
			}
		}


		/// <inheritdoc cref="IRawPort.Write(byte[])" />
		public void Write(byte[] aData)
		{
			_log.DebugFormat("Sending {0}", BitConverter.ToString(aData));
			_port.Write(aData, 0, aData.Length);
		}


		/// <inheritdoc cref="IRawPort.Write(string)" />
		public void Write(string aData)
		{
			_log.DebugFormat("Sending '{0}'", aData);
			_port.Write(aData);
		}


		/// <inheritdoc cref="IRawPort.Read" />
		public byte[] Read(int aCount)
		{
			var result = new byte[aCount];
			for (var i = aCount; i > 0; i -= _port.Read(result, aCount - i, i))
			{
			}

			_log.DebugFormat("Received {0}", BitConverter.ToString(result));
			return result;
		}


		/// <inheritdoc cref="IRawPort.ReadLine" />
		public string ReadLine()
		{
			var result = _port.ReadLine();
			_log.DebugFormat("Received {0}", result);
			return result;
		}


		/// <inheritdoc cref="IRawPort.ClearReceiveBuffer" />
		public void ClearReceiveBuffer()
		{
			_port.DiscardInBuffer();
			_log.Debug("Cleared receive buffer.");
		}


		/// <inheritdoc cref="IRawPort.ClearTransmitBuffer" />
		public void ClearTransmitBuffer()
		{
			_port.DiscardOutBuffer();
			_log.Debug("Cleared transmit buffer.");
		}


		/// <inheritdoc cref="IRawPort.GetApplicableBaudRates" />
		public IEnumerable<int> GetApplicableBaudRates() => SupportedSerialBaudRates;


		/// <inheritdoc cref="IRawPort.ReadTimeout" />
		public int ReadTimeout
		{
			get => _port.ReadTimeout;
			set => _port.ReadTimeout = value;
		}


		/// <summary>
		///     List of serial baud rates supported by Zaber devices.
		/// </summary>
		public static IEnumerable<int> SupportedSerialBaudRates => _supportedBaudRates;


		/// <inheritdoc cref="IRawPort.BaudRate" />
		public int BaudRate
		{
			get => _port.BaudRate;
			set => _port.BaudRate = value;
		}

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private static readonly int[] _supportedBaudRates = { 115200, 57600, 38400, 19200, 9600 };

		private readonly SerialPort _port;

		#endregion
	}
}

﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using log4net;

namespace Zaber.Ports
{
	using QueuedEvent = Tuple<EventHandler<DataPacketEventArgs>, DataPacketEventArgs>;

	/// <summary>
	///     Common base class for port implementations - implements functionality
	///     expected to be common to most port types.
	/// </summary>
	public abstract class PortBase : IDisposable
	{
		#region -- Initialization --

		/// <summary>
		///     Subclasses must use this constructor.
		/// </summary>
		/// <param name="aPacketConverter">Optional custom packetizer implemenation.</param>
		protected PortBase(PacketConverter aPacketConverter = null)
		{
			PacketConverter = aPacketConverter ?? new PacketConverter();
			PacketConverter.DataPacketReceived += PacketConverter_DataPacketReceived;
			PacketConverter.PortError += PacketConverter_PortError;

			_delayedSendTimer = new Timer(OnDelayedSendTimer);
			DelayMilliseconds = 500;
			#pragma warning disable CS0618 // Obsolete
			SubscriberTimer = new TimeoutTimer { Timeout = 500 };
			#pragma warning restore CS0618
		}

		#endregion

		#region -- Public properties --

		/// <summary>
		///     Flag that gets or sets whether checksums are sent with each text
		///     message. If true, the checksum will be calculated with the
		///     Longitudinal Redundancy Check (LRC) algorithm.
		/// </summary>
		public bool AreChecksumsSent { get; set; }


		/// <summary>
		///     Get or set the delay period to use with <see cref="SendDelayed" />.
		///     500 ms by default.
		/// </summary>
		public int DelayMilliseconds { get; set; }


		/// <summary>
		///     Flag that gets or sets whether the port is in ASCII mode.
		///     If it's not in ASCII mode, then all received data will be
		///     parsed as binary, 6-byte packets.
		/// </summary>
		public bool IsAsciiMode
		{
			get => PacketConverter.IsAsciiMode;
			set => PacketConverter.IsAsciiMode = value;
		}


		/// <summary>
		///     Gets the packet converter used to convert between the byte stream
		///     and the <see cref="DataPacket" /> structure.
		/// </summary>
		public PacketConverter PacketConverter { get; }


		/// <summary>
		///     Synchronization lock that controls when data packets are sent to
		///     the port.
		/// </summary>
		/// <remarks>
		///     If you lock this, then no data packets will be sent until
		///     you release it.
		/// </remarks>
		public object SendLock { get; } = new object();


		/// <summary>
		///     Gets or sets the timer that waits for an event subscriber to finish
		///     processing an event.
		/// </summary>
		/// <remarks>
		///     This object used to be used to make sure no handler
		///     subscribed to this class' events ran too long, as each was executed
		///     synchronously. It is now deprecated: All events handlers are now
		///     called asynchronously, so it doesn't matter if they run long
		///     sometimes. It is the responsibility of the subscribed method to not
		///     run long/be "badly behaved", not of the class which raises the
		///     event.
		/// </remarks>
		[Obsolete("No longer used.")]
		public TimeoutTimer SubscriberTimer { get; }

		#endregion

		#region -- Public methods --

		/// <summary>
		///     Convenience method for commands that ignore the data value.
		///     It sends 0 as the data value.
		/// </summary>
		/// <param name="aDeviceNumber">See <see cref="DataPacket.DeviceNumber" />.</param>
		/// <param name="aCommand">See <see cref="Zaber.Command" />.</param>
		public void Send(byte aDeviceNumber, Command aCommand) => Send(aDeviceNumber, aCommand, 0);


		/// <summary>
		///     Send a command to a device on the chain.
		/// </summary>
		/// <param name="aDeviceNumber">See <see cref="DataPacket.DeviceNumber" />.</param>
		/// <param name="aCommand">See <see cref="DataPacket.Command" />.</param>
		/// <param name="aData">See <see cref="DataPacket.NumericData" />.</param>
		/// <exception cref="InvalidOperationException">
		///     The specified port is
		///     not open.
		/// </exception>
		/// <exception cref="IOException">
		///     The port is in an invalid state.
		/// </exception>
		public void Send(byte aDeviceNumber, Command aCommand, int aData) => Send(aDeviceNumber, aCommand, aData, null);


		/// <summary>
		///     Send a command to a device on the chain.
		/// </summary>
		/// <param name="aDeviceNumber">See <see cref="DataPacket.DeviceNumber" />.</param>
		/// <param name="aCommand">See <see cref="DataPacket.Command" />.</param>
		/// <param name="aData">See <see cref="DataPacket.NumericData" />.</param>
		/// <param name="aMeasurement">
		///     An optional measurement that the data
		///     value was calculated from. May be null.
		/// </param>
		/// <exception cref="InvalidOperationException">
		///     The specified port is
		///     not open.
		/// </exception>
		/// <exception cref="IOException">
		///     The port is in an invalid state.
		/// </exception>
		public void Send(byte aDeviceNumber, Command aCommand, int aData, Measurement aMeasurement)
		{
			if (IsAsciiMode)
			{
				throw new InvalidOperationException("Binary commands cannot be sent when the port is in ASCII mode.");
			}

			var dataPacket = new DataPacket
			{
				DeviceNumber = aDeviceNumber,
				Command = aCommand,
				NumericData = aData,
				Measurement = aMeasurement ?? new Measurement(aData, UnitOfMeasure.Data)
			};

			_log.Debug("Sending.");
			WriteData(PacketConverter.GetBytes(dataPacket));

			OnDataPacketSent(dataPacket);
		}


		/// <summary>
		///     Send a text-mode command to a device on the chain.
		/// </summary>
		/// <param name="aCommand">The full command to send to the device.</param>
		/// <param name="aMeasurement">
		///     An optional measurement that the data
		///     value was calculated from. May be null.
		/// </param>
		/// <exception cref="InvalidOperationException">
		///     The specified port is
		///     not open.
		/// </exception>
		/// <exception cref="IOException">
		///     The port is in an invalid state.
		/// </exception>
		public void Send(string aCommand, Measurement aMeasurement)
		{
			if (!IsAsciiMode)
			{
				throw new InvalidOperationException("ASCII commands cannot be sent when the port is in binary mode.");
			}

			var textToSend =
				aCommand.StartsWith("/", StringComparison.Ordinal)
					? aCommand
					: "/" + aCommand;

			if (AreChecksumsSent)
			{
				byte checksum = 0x00;
				var core = textToSend.Substring(1);
				foreach (var c in Encoding.ASCII.GetBytes(core))
				{
					checksum = (byte) ((checksum + c) & 0xFF);
				}

				checksum = (byte) (((checksum ^ 0xFF) + 1) & 0xFF);
				textToSend += string.Format(CultureInfo.InvariantCulture, ":{0:X2}", checksum);
			}

			var dataPacket = new DataPacket(textToSend) { Measurement = aMeasurement };

			_log.Debug("Sending.");

			WriteData(Encoding.ASCII.GetBytes(textToSend + "\r\n"));

			// There is a remote chance of a race condition between these two lines;
			// If the ending thread happens to get pre-empted between WriteData and 
			// OnDataPacketSent, the device response could get received before the send
			// notifications get queued. If this ever happens, we might have to make
			// sending and receiving mutually exclusive. That should be safe given
			// the existence of the receiver thread; incoming data will not be lost,
			// just delayed until the sender finishes.
			OnDataPacketSent(dataPacket);
		}


		/// <summary>
		///     Send a text-mode command to a device on the chain.
		/// </summary>
		/// <param name="aCommand">The full command to send to the device.</param>
		/// <exception cref="InvalidOperationException">
		///     The specified port is
		///     not open.
		/// </exception>
		/// <exception cref="IOException">
		///     The port is in an invalid state.
		/// </exception>
		public void Send(string aCommand) => Send(aCommand, null);


		/// <summary>
		///     Send a command to a device on the chain after a delay.
		/// </summary>
		/// <param name="aDeviceNumber">See <see cref="DataPacket.DeviceNumber" />.</param>
		/// <param name="aCommand">See <see cref="DataPacket.Command" />.</param>
		/// <param name="aData">See <see cref="DataPacket.NumericData" />.</param>
		/// <returns>
		///     The data packet that will be sent. The data packet can be
		///     passed to <see cref="CancelDelayedPacket" /> to stop it from being
		///     sent.
		/// </returns>
		/// <remarks>
		///     Wait for <see cref="DelayMilliseconds" /> before sending
		///     the command. If you call this method several times, the commands
		///     get queued up and sent one at a time with a delay before each one.
		/// </remarks>
		public DataPacket SendDelayed(byte aDeviceNumber, Command aCommand, int aData)
		{
			var dataPacket = new DataPacket
			{
				DeviceNumber = aDeviceNumber,
				Command = aCommand,
				NumericData = aData
			};

			lock (_delayedPackets)
			{
				_delayedPackets.AddLast(dataPacket);
				EnableDelayedSendTimer(true);
			}

			return dataPacket;
		}


		/// <summary>
		///     Cancel a delayed packet that was requested by
		///     <see cref="SendDelayed" />.
		/// </summary>
		/// <param name="aPacket">
		///     The packet that was returned by
		///     <see cref="SendDelayed" />.
		/// </param>
		/// <returns>
		///     True if the packet was canceled before being sent, otherwise
		///     false.
		/// </returns>
		public bool CancelDelayedPacket(DataPacket aPacket)
		{
			lock (_delayedPackets)
			{
				var isFound = _delayedPackets.Remove(aPacket);
				if (0 == _delayedPackets.Count)
				{
					EnableDelayedSendTimer(false);
				}

				return isFound;
			}
		}


		/// <summary>
		///     Report that an invalid packet was received by raising the
		///     <see cref="ErrorReceived" /> event.
		/// </summary>
		/// <remarks>
		///     This can be called by any higher-level code that decides
		///     a packet was invalid. An example would be if the device number
		///     doesn't match any known devices.
		/// </remarks>
		public void ReportInvalidPacket()
			=> OnErrorReceived(new ZaberPortErrorReceivedEventArgs(ZaberPortError.InvalidPacket));


		/// <summary>
		///     Whether this object is equal to another.
		/// </summary>
		/// <param name="aOther">The object to compare this one to.</param>
		/// <returns>True if the objects are equal, false otherwise.</returns>
		public override bool Equals(object aOther)
		{
			if (ReferenceEquals(this, aOther))
			{
				return true;
			}

			if (!(aOther is PortBase other))
			{
				return false;
			}

			if ((AreChecksumsSent != other.AreChecksumsSent)
			|| (DelayMilliseconds != other.DelayMilliseconds)
			|| (IsAsciiMode != other.IsAsciiMode)
			|| !Equals(PacketConverter, other.PacketConverter)
			|| (SendLock != other.SendLock))
			{
				return false;
			}

			return true;
		}


		/// <summary>
		///     Returns a calculated hash code for this object.
		/// </summary>
		/// <returns>An integer hash code.</returns>
		public override int GetHashCode()
		{
			var code = 17;

			code = (31 * code) + (AreChecksumsSent ? 1 : 0);
			code = (31 * code) + DelayMilliseconds;
			code = (31 * code) + (IsAsciiMode ? 1 : 0);
			if (null != PacketConverter)
			{
				code = (31 * code) + PacketConverter.GetHashCode();
			}

			if (null != SendLock)
			{
				code = (31 * code) + SendLock.GetHashCode();
			}

			return code;
		}

		#endregion

		#region -- Public events --

		/// <summary>
		///     Raised when the port receives data back from the devices. When a complete
		///     response has been received, it's converted to a <see cref="DataPacket" /> object and
		///     sent through this event.
		/// </summary>
		/// <example>
		///     Register an event handler and print any received data to the
		///     output console.
		///     <code>
		///  private void MyForm_Load(object sender, EventArgs e)
		///  {
		///      port.DataPacketReceived += 
		///          new EventHandler&lt;DataPacketEventArgs&gt;(port_DataPacketReceived);
		///  }
		/// 
		///  void port_DataPacketReceived(object sender, DataPacketEventArgs e)
		///  {
		///      System.Console.Out.WriteLine(
		///          "Device {0}: {1}({2})",
		///          e.Data.DeviceNumber,
		///          e.Data.Command,
		///          e.Data.Data);
		///  }
		///  </code>
		/// </example>
		/// <remarks>
		///     <para>
		///         Be careful when handling this event, because it is usually raised
		///         from a background thread. The simplest way to deal with that is not
		///         to register for this event at all. Use a <see cref="Conversation" />
		///         to coordinate requests and responses instead. It handles all the
		///         threading issues and just returns the response as the return
		///         value from <see cref="Conversation.Request(Command, int)" />.
		///     </para>
		///     <para>
		///         If you do want to handle this event safely in your user
		///         interface, read the first two sections of this article:
		///         http://weblogs.asp.net/justin_rogers/articles/126345.aspx
		///         It shows how to use <c>Control.InvokeRequired</c> and
		///         <c>Control.BeginInvoke</c> to move execution back onto the UI
		///         thread from a background thread.
		///     </para>
		/// </remarks>
		public event EventHandler<DataPacketEventArgs> DataPacketReceived;


		/// <summary>
		///     Raised when the port sends data to the devices.
		/// </summary>
		/// <remarks>
		///     Be careful when handling this event, because it is occasionally
		///     raised from a background thread. See
		///     <see cref="DataPacketReceived" /> for details. Some scenarios where
		///     it can be raised from a background thread are: executing scripts
		///     and automatically adjusting message id mode.
		/// </remarks>
		public event EventHandler<DataPacketEventArgs> DataPacketSent;


		/// <summary>
		///     Raised when the underlying port raises its own <c>ErrorReceived</c>
		///     event, or when a partial data packet is received because some bytes
		///     have been dropped. The type of error is described by
		///     <see cref="ZaberPortErrorReceivedEventArgs.ErrorType" />.
		/// </summary>
		/// <remarks>
		///     Be careful when handling this event, because it is usually raised
		///     from a background thread. See <see cref="DataPacketReceived" /> for
		///     details.
		/// </remarks>
		public event EventHandler<ZaberPortErrorReceivedEventArgs> ErrorReceived;


		/// <summary>
		///     Raised when internal information about port being opened does not match port.IsOpen.
		/// </summary>
		public event EventHandler ClosedUnexpectedly;

		#endregion

		#region -- Internal event handling --

		/// <summary>
		///     Invoked when there is an error receiving a packet.
		/// </summary>
		/// <param name="aError">Code and user/log message for the error type.</param>
		protected abstract void OnErrorReceived(ZaberPortErrorReceivedEventArgs aError);


		private void PacketConverter_PortError(object aSender, ZaberPortErrorReceivedEventArgs aArgs)
			=> OnErrorReceived(aArgs);


		private void PacketConverter_DataPacketReceived(object aSender, DataPacketEventArgs aArgs)
		{
			if (_log.IsDebugEnabled)
			{
				_log.Debug(aArgs.DataPacket.FormatResponse());
			}

			if (null != DataPacketReceived)
			{
				var receivers = DataPacketReceived.GetInvocationList();
				foreach (EventHandler<DataPacketEventArgs> receiver in receivers)
				{
					_pendingEvents.Enqueue(new QueuedEvent(receiver, aArgs));
				}

				if (null != _serviceThread)
				{
					_serviceThreadGate.Set();
				}
				else
				{
					// In test contexts the service thread may not be started.
					DispatchEvents();
				}
			}
		}


		/// <summary>
		///     Start up the receiver service thread.
		/// </summary>
		protected void StartServiceThread()
		{
			_serviceThreadShouldExit = false;
			if (null == _serviceThread)
			{
				_serviceThread = new Thread(ReceiveDispatch) { Name = "Port service thread" };

				_serviceThread.Start();
			}
		}


		/// <summary>
		///     Terminates the receive service thread and waits for it to end.
		/// </summary>
		protected void StopServiceThread()
		{
			_serviceThreadShouldExit = true;
			_serviceThreadGate.Set();

			var thread = _serviceThread;
			if (thread != null)
			{
				thread.Join();
				_serviceThread = null;
			}
		}


		/// <summary>
		///     Wake the service thread when data may have been received.
		/// </summary>
		protected void KickServiceThread() => _serviceThreadGate.Set();


		/// <summary>
		///     Check whether the service thread is ending.
		/// </summary>
		protected bool ServiceThreadIsExiting => _serviceThreadShouldExit;


		/// <summary>
		///     Subclasses should call this when closing the port to discard
		///     delayed packets and stop the send timer.
		/// </summary>
		protected void HaltDelayedPackets()
		{
			lock (_delayedPackets)
			{
				_delayedPackets.Clear();
				EnableDelayedSendTimer(false);
			}
		}


		/// <summary>
		///     Purge any partially received packets. This should be called when
		///     closing the port, after calling <see cref="StopServiceThread" />.
		/// </summary>
		protected void ClearPartiallyReceivedData()
		{
			// Clear any remaining data out of the receive buffer.
			while (!_receivedBytes.IsEmpty)
			{
				_receivedBytes.TryDequeue(out var dummy);
			}

			lock (PacketConverter)
			{
				PacketConverter.ClearBuffer();
			}
		}


		/// <summary>
		///     Default service thread for receiving port data and packetizing it.
		///     Subclasses can override this if different behavior is needed.
		/// </summary>
		protected virtual void ReceiveDispatch()
		{
			while (!_serviceThreadShouldExit)
			{
				// Order of operations is important here. The expectation is that
				// this thread should spend most of its time in the WaitOne call.
				// When that unblocks, it will then do all pending work befeore
				// checking whether the thread should exit. This enables us to 
				// shut down the thread cleanly when the port is closed, provided
				// Close() stops adding work to the queue before asserting the semaphore.
				_serviceThreadGate.WaitOne();
				_serviceThreadGate.Reset();

				// Receive serial port data first; this may generate more notification events.
				lock (PacketConverter)
				{
					byte[] buffer = null;
					while (_receivedBytes.TryDequeue(out buffer))
					{
						for (var i = 0; i < buffer.Length; i++)
						{
							PacketConverter.ReceiveByte(buffer[i]);
						}
					}
				}

				// Dispatch send/receive notifications. These are queued up and sent by a thread in
				// order to help ensure they are processed in chronological order. Just
				// dispatching them immediately on send or receive can easily cause the
				// device responses to be seen before the commands, because of interactions
				// between threads, and the WPF UI event dispatchers.
				DispatchEvents();
			}
		}


		/// <summary>
		///     Place received data into a thread-safe queue for the service thread to deal with.
		/// </summary>
		/// <param name="aData">Bytes received from the port.</param>
		protected void QueueReceived(byte[] aData) => _receivedBytes.Enqueue(aData);


		/// <summary>
		///     Should only be called by the event dispatcher thread or test fixtures.
		/// </summary>
		protected void DispatchEvents()
		{
			QueuedEvent e = null;
			while (_pendingEvents.TryDequeue(out e))
			{
				try
				{
					e.Item1.Invoke(this, e.Item2);
				}
				catch (Exception aException)
				{
					_log.Error("Port tx/rx event handler threw an exception.", aException);
				}
			}
		}


		private void OnDataPacketSent(DataPacket aDataPacket)
		{
			if (_log.IsDebugEnabled)
			{
				_log.Debug(aDataPacket.FormatRequest());
			}

			if (null != DataPacketSent)
			{
				var args = new DataPacketEventArgs(aDataPacket);
				var receivers = DataPacketSent.GetInvocationList();
				foreach (EventHandler<DataPacketEventArgs> receiver in receivers)
				{
					_pendingEvents.Enqueue(new QueuedEvent(receiver, args));
				}

				if (null != _serviceThread)
				{
					_serviceThreadGate.Set();
				}
				else
				{
					// In test contexts the service thread may not be started.
					DispatchEvents();
				}
			}
		}


		/// <summary>
		///     Handle the timer event for sending delayed requests.
		/// </summary>
		/// <param name="aIgnored">State object for the timer. Not used.</param>
		protected virtual void OnDelayedSendTimer(object aIgnored)
		{
			DataPacket dataPacket;
			lock (_delayedPackets)
			{
				if (0 == _delayedPackets.Count)
				{
					// port might have been closed during delay
					return;
				}

				dataPacket = _delayedPackets.First.Value;
				_delayedPackets.RemoveFirst();
				if (0 == _delayedPackets.Count)
				{
					EnableDelayedSendTimer(false);
				}
			}

			Send(dataPacket.DeviceNumber, dataPacket.Command, (int) Math.Round(dataPacket.NumericData));
		}


		/// <summary>
		///     Used by subclasses to check if the ErrorReceived event is hooked up.
		/// </summary>
		protected bool CanInvokeErrorReceived => null != ErrorReceived;


		/// <summary>
		///     Used by subclasses to invoke the ErrorReceived event.
		/// </summary>
		/// <param name="aSender">Object originating the event.</param>
		/// <param name="aArgs">Error code and message.</param>
		protected void InvokeErrorReceived(object aSender, ZaberPortErrorReceivedEventArgs aArgs)
			=> ErrorReceived(aSender, aArgs);


		/// <summary>
		///     Used by subclasses to invoke the ClosedUnexpectedly event.
		/// </summary>
		/// <param name="aSender">Object originating the event.</param>
		protected void InvokeClosedUnexpectedly(object aSender) => ClosedUnexpectedly?.Invoke(aSender, EventArgs.Empty);

		#endregion

		#region -- Protected/private methods --

		/// <summary>
		///     Function to actually transmit bytes over the port - must be implemented by subclasses.
		/// </summary>
		/// <param name="aData"></param>
		protected abstract void WriteData(byte[] aData);


		/// <summary>
		///     Turn the delayed send timer on or off.
		/// </summary>
		/// <remarks>
		///     The timer is used for the <see cref="SendDelayed" />
		///     method.
		/// </remarks>
		/// <param name="aIsEnabled">
		///     True to enable the timer, otherwise
		///     false.
		/// </param>
		protected virtual void EnableDelayedSendTimer(bool aIsEnabled)
		{
			var milliseconds = aIsEnabled ? DelayMilliseconds : Timeout.Infinite;
			_delayedSendTimer.Change(milliseconds, milliseconds);
		}

		#endregion

		#region -- IDisposable Implementation --

		/// <summary>
		///     Performs application-defined tasks associated with freeing, releasing, or
		///     resetting unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);

			// not necessary here, but might be for derived types.
			GC.SuppressFinalize(this);
		}


		/// <summary>
		///     Performs application-defined tasks associated with freeing, releasing, or
		///     resetting unmanaged resources.
		/// </summary>
		/// <param name="aIsDisposing">
		///     True if the object is being disposed, and not
		///     garbage collected.
		/// </param>
		protected virtual void Dispose(bool aIsDisposing)
		{
			if (aIsDisposing)
			{
				_delayedSendTimer.Dispose();
			}
		}

		#endregion

		private static readonly ILog _log = LogManager.GetLogger(typeof(PortBase));

		private readonly LinkedList<DataPacket> _delayedPackets = new LinkedList<DataPacket>();
		private readonly Timer _delayedSendTimer;

		private readonly ConcurrentQueue<byte[]> _receivedBytes = new ConcurrentQueue<byte[]>();

		private Thread _serviceThread;
		private readonly ManualResetEvent _serviceThreadGate = new ManualResetEvent(false);
		private volatile bool _serviceThreadShouldExit;

		private readonly ConcurrentQueue<QueuedEvent> _pendingEvents = new ConcurrentQueue<QueuedEvent>();
	}
}

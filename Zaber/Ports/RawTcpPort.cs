﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using log4net;

namespace Zaber.Ports
{
	/// <summary>
	///     TCP/IP implementation of the raw, synchronous port interface used for device
	///     and protocol detection.
	/// </summary>
	public class RawTcpPort : IRawPort, IDisposable
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Create a raw port wrapper for a given endpoint.
		/// </summary>
		public RawTcpPort(string aHostName, int aPort)
		{
			_hostName = aHostName;
			_port = aPort;
		}


		/// <inheritdoc cref="IRawPort.Open" />
		public void Open()
		{
			var addresses = Dns.GetHostAddresses(_hostName);

			// On some computers, looking up a hostname may return a non-functional IPv6 address; only use IPv4.
			var address = addresses.Where(addr => addr.AddressFamily == AddressFamily.InterNetwork).FirstOrDefault();
			if (null == address)
			{
				throw new ArgumentException($"Can't locate an IP address for {_hostName}.");
			}

			var endPoint = new IPEndPoint(address, _port);
			_socket = new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
			if (_readTimeout > 0)
			{
				_socket.ReceiveTimeout = _readTimeout;
			}

			_socket.NoDelay = true; // See comment on same call in TcpPort.
			_socket.Connect(endPoint);

			_stream = new NetworkStream(_socket);
			_reader = new StreamReader(_stream, Encoding);
			_writer = new StreamWriter(_stream, Encoding) { AutoFlush = true };

			_log.DebugFormat("Opened {0}:{1}", _hostName, _port);
		}


		/// <inheritdoc cref="IRawPort.Close" />
		public void Close()
		{
			if (null != _socket)
			{
				if (_socket.Connected)
				{
					_socket.Shutdown(SocketShutdown.Both);
					_socket.Close();
				}

				_reader = null;
				_writer = null;
				_stream = null;
				_socket = null;
				_log.DebugFormat("Closed {0}:{1}", _hostName, _port);
			}
		}


		/// <inheritdoc cref="IRawPort.Write(byte[])" />
		public void Write(byte[] aData)
		{
			_log.DebugFormat("Sending {0}", BitConverter.ToString(aData));
			var chars = Encoding.GetChars(aData);
			_writer.Write(chars, 0, chars.Length);
		}


		/// <inheritdoc cref="IRawPort.Write(string)" />
		public void Write(string aData)
		{
			_log.DebugFormat("Sending {0}", aData);
			_writer.Write(aData);
		}


		/// <inheritdoc cref="IRawPort.Read" />
		public byte[] Read(int aCount)
		{
			var chars = new char[aCount];
			var numRead = 0;

			try
			{
				while (numRead < aCount)
				{
					numRead += _reader.Read(chars, numRead, aCount - numRead);
				}
			}
			catch (IOException ioe)
			{
				if (ioe.InnerException is SocketException se)
				{
					if (se.SocketErrorCode == SocketError.TimedOut)
					{
						throw new TimeoutException();
					}
				}

				throw;
			}

			var result = Encoding.GetBytes(chars);
			_log.DebugFormat("Received {0}", BitConverter.ToString(result));
			return result;
		}


		/// <inheritdoc cref="IRawPort.ReadLine" />
		public string ReadLine() => _reader.ReadLine();


		/// <inheritdoc cref="IRawPort.ClearReceiveBuffer" />
		public void ClearReceiveBuffer()
		{
			_reader.DiscardBufferedData();
			_log.Debug("Cleared receive buffer.");
		}


		/// <inheritdoc cref="IRawPort.ClearTransmitBuffer" />
		public void ClearTransmitBuffer() => _log.Debug("Cleared transmit buffer.");


		/// <inheritdoc cref="IRawPort.GetApplicableBaudRates" />
		public IEnumerable<int> GetApplicableBaudRates() => new[] { -1 };


		/// <inheritdoc cref="IDisposable.Dispose" />
		public void Dispose()
		{
			_reader?.Dispose();
			_writer?.Dispose();
			_stream?.Dispose();
			_socket?.Dispose();
		}


		/// <inheritdoc cref="IRawPort.ReadTimeout" />
		public int ReadTimeout
		{
			get => _socket.ReceiveTimeout;
			set
			{
				if (null != _socket)
				{
					_socket.ReceiveTimeout = value;
				}

				_readTimeout = value;
			}
		}


		/// <inheritdoc cref="IRawPort.BaudRate" />
		public int BaudRate
		{
			get => -1;
			set { }
		}


		// For testing purposes.
		private static Encoding Encoding { get; } = Encoding.GetEncoding("ISO-8859-1"); // Does not modify binary bytes.

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(typeof(RawTcpPort));

		private readonly string _hostName;
		private readonly int _port;
		private int _readTimeout;
		private Socket _socket;
		private NetworkStream _stream;
		private StreamReader _reader;
		private StreamWriter _writer;

		#endregion
	}
}

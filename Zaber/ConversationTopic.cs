using System;
using System.Threading;

namespace Zaber
{
	/// <summary>
	///     Coordinates a request with a response
	///     in the communications with a single device.
	/// </summary>
	[Serializable]
	public class ConversationTopic : IDisposable
	{
		#region -- Events --

		/// <summary>
		///     Raised when the topic is completed.
		/// </summary>
		/// <remarks>
		///     The topic can be completed by setting any of these properties:
		///     <see cref="Response" />, <see cref="ZaberPortError" />,
		///     <see cref="ReplacementResponse" />.
		/// </remarks>
		public event EventHandler Completed;


		/// <summary>
		///     Raised when the topic's internal timeout triggers.
		/// </summary>
		public event EventHandler Timeout;

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Constructs ConversationTopic.
		/// </summary>
		public ConversationTopic()
		{
			_timeout = new Timer(OnTimeout);
		}


		/// <summary>
		///     Stop waiting for a response.
		/// </summary>
		/// <remarks>Sets IsCanceled to true and marks the topic complete.</remarks>
		public virtual void Cancel()
		{
			IsCanceled = true;
			Complete();
		}


		/// <summary>
		///     Blocks the current thread until the topic is completed.
		/// </summary>
		/// <returns>This method always returns true.</returns>
		/// <remarks>
		///     The topic can be completed by setting any of these properties:
		///     <see cref="Response" />, <see cref="ZaberPortError" />,
		///     <see cref="ReplacementResponse" />.
		/// </remarks>
		public virtual bool Wait() => _waitHandle.WaitOne();


		/// <summary>
		///     Blocks the current thread until the topic is completed or the
		///     timer times out.
		/// </summary>
		/// <param name="aTimeoutTimer">The timer to wait with.</param>
		/// <returns>
		///     True if the topic completed before the timer expired,
		///     otherwise false.
		/// </returns>
		/// <remarks>
		///     The topic can be completed by setting any of these properties:
		///     <see cref="Response" />, <see cref="ZaberPortError" />,
		///     <see cref="ReplacementResponse" />.
		/// </remarks>
		public virtual bool Wait(TimeoutTimer aTimeoutTimer) => aTimeoutTimer.WaitOne(_waitHandle);


		/// <summary>
		///     Validates that the request has been completed successfully.
		/// </summary>
		/// <remarks>
		///     If you want to check whether the response is valid without throwing
		///     an exception, use <see cref="IsValid" /> instead.
		/// </remarks>
		/// <exception cref="InvalidOperationException">
		///     The request has not
		///     been completed yet.
		/// </exception>
		/// <exception cref="RequestReplacedException">
		///     The request was replaced
		///     by another request and will not complete.
		/// </exception>
		/// <exception cref="ZaberPortErrorException">
		///     A port error occurred, so
		///     the status of the request is unknown.
		/// </exception>
		/// <exception cref="ErrorResponseException">
		///     The device responded with
		///     an error.
		/// </exception>
		/// <exception cref="RequestCollectionException">
		///     Multiple requests were
		///     made together, and some of them failed.
		/// </exception>
		public virtual void Validate()
		{
			if (!IsComplete)
			{
				throw new InvalidOperationException("Validate cannot be called until the request is complete.");
			}

			if (null != ReplacementResponse)
			{
				throw new RequestReplacedException(ReplacementResponse);
			}

			if (ZaberPortError.ResponseTimeout == ZaberPortError)
			{
				throw new RequestTimeoutException();
			}

			if (ZaberPortError.None != ZaberPortError)
			{
				throw new ZaberPortErrorException(ZaberPortError);
			}

			if (IsCanceled)
			{
				throw new RequestCanceledException();
			}

			if (Response.IsError)
			{
				throw new ErrorResponseException(Response);
			}
		}


		/// <summary>
		///		Setting this to false causes subsequent calls to <see cref="StartTimeout"/>
		///		to do nothing, and prevents any pre-existing timeout from calling the event
		///		handler. It does not affect <see cref="StopTimeout"/>.
		/// </summary>
		public bool EnableTimeouts { get; set; } = true;


		/// <summary>
		///     Starts timeout countdown which eventually invokes the Timeout event.
		/// </summary>
		/// <param name="aTimeout">timeout in milliseconds</param>
		public void StartTimeout(int aTimeout)
		{
			if (EnableTimeouts)
			{
				_timeout.Change(aTimeout, System.Threading.Timeout.Infinite);
			}
		}


		/// <summary>
		///     Stops ongoing timeout countdown.
		/// </summary>
		public void StopTimeout()
			=> _timeout.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);


		/// <summary>
		///     Gets or sets the port error that was detected while waiting
		///     for a response.
		/// </summary>
		/// <remarks>Setting this completes the topic.</remarks>
		public virtual ZaberPortError ZaberPortError
		{
			get => _zaberPortError;
			set
			{
				_zaberPortError = value;
				Complete();
			}
		}


		/// <summary>
		///     Gets or sets the details of a response when this topic's request has
		///     been replaced by another.
		/// </summary>
		/// <remarks>Setting this completes the topic.</remarks>
		public virtual DeviceMessage ReplacementResponse
		{
			get => _replacementResponse;
			set
			{
				_replacementResponse = value;
				Complete();
			}
		}


		/// <summary>
		///     Gets or sets the command that was sent to the device.
		/// </summary>
		public virtual Command RequestCommand { get; set; }


		/// <summary>
		///     Gets or sets the data value that was sent to the device.
		/// </summary>
		public virtual int RequestData { get; set; }


		/// <summary>
		///     Gets or sets the number of retries remaining.
		/// </summary>
		public virtual int RetryCount { get; set; }


		/// <summary>
		///     Gets or sets the details of the response received from the device.
		/// </summary>
		/// <remarks>Setting this completes the topic.</remarks>
		public virtual DeviceMessage Response
		{
			get => _response;
			set
			{
				_response = value;
				Complete();
			}
		}


		/// <summary>
		///     Gets or sets a data packet that was used to retry the request after
		///     a port error.
		/// </summary>
		/// <remarks>
		///     This is recorded so that the retry can be cancelled if the
		///     original response is received before the retry request gets sent.
		/// </remarks>
		public DataPacket RetryPacket { get; set; }


		/// <summary>
		///     Gets or sets a flag showing if this topic is waiting for an alert
		///     message instead of a regular response.
		/// </summary>
		public bool IsAlert { get; set; }


		/// <summary>
		///     Get a flag showing whether this topic was canceled by a call to
		///     <see cref="Cancel" />.
		/// </summary>
		public virtual bool IsCanceled { get; private set; }


		/// <summary>
		///     Get a WaitHandle that will be released when the topic is completed.
		/// </summary>
		/// <remarks>
		///     This is useful when you want to use more sophisticated thread coordination.
		///     You can use this WaitHandle in the WaitHandle class's WaitAll() or WaitAny()
		///     methods. If you just want to do a simple wait, use the <see cref="Wait()" />
		///     or <see cref="Wait(TimeoutTimer)" /> methods.
		/// </remarks>
		public WaitHandle WaitHandle => _waitHandle;

		/// <summary>
		///     Gets or sets the message identifier used to coordinate requests
		///     and responses.
		/// </summary>
		public byte MessageId { get; set; }


		/// <summary>
		///     Gets a flag showing whether the request has been completed.
		/// </summary>
		public bool IsComplete { get; private set; }

		/// <summary>
		///     Gets a flag showing whether the request has been completed successfully.
		/// </summary>
		/// <seealso cref="Validate" />
		public virtual bool IsValid =>
			IsComplete
		&& !IsCanceled
		&& (null == ReplacementResponse)
		&& (ZaberPortError.None == ZaberPortError)
		&& (null != Response)
		&& !Response.IsError;


		/// <summary>
		///     Gets a flag showing whether the request was rejected for flow control reasons and should be retried.
		/// </summary>
		public virtual bool IsFlowControlRejected =>
			IsComplete
		&& !IsCanceled
		&& (null == ReplacementResponse)
		&& (ZaberPortError.None == ZaberPortError)
		&& (null != Response)
		&& Response.IsAgain;

		#endregion

		#region -- Interfaces --

		#region -- IDisposable Implementation --

		/// <summary>
		///     Performs application-defined tasks associated with freeing, releasing, or
		///     resetting unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);

			// not necessary here, but might be for derived types.
			GC.SuppressFinalize(this);
		}


		/// <summary>
		///     Performs application-defined tasks associated with freeing, releasing, or
		///     resetting unmanaged resources.
		/// </summary>
		/// <param name="aIsDisposing">
		///     True if the object is being disposed, and not
		///     garbage collected.
		/// </param>
		protected virtual void Dispose(bool aIsDisposing)
		{
			if (aIsDisposing)
			{
				Complete();

				_waitHandle.Close();
				_timeout.Dispose();
			}
		}

		#endregion

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Mark the topic as complete, raise the event, and set the wait handle.
		/// </summary>
		protected virtual void Complete()
		{
			lock (_lock)
			{
				if (IsComplete)
				{
					return;
				}

				IsComplete = true;
			}

			StopTimeout();
			Completed?.Invoke(this, EventArgs.Empty);
			_waitHandle.Set();
		}


		/// <summary>
		///     Timer callback which invokes the Timeout event.
		/// </summary>
		protected virtual void OnTimeout(object state)
		{
			if (EnableTimeouts)
			{
				Timeout?.Invoke(this, EventArgs.Empty);
			}
		}

		#endregion

		#region -- Data --

		[NonSerialized]
		private readonly ManualResetEvent _waitHandle = new ManualResetEvent(false);

		[NonSerialized]
		private readonly Timer _timeout;

		[NonSerialized]
		private readonly object _lock = new object();

		private DeviceMessage _response;
		private ZaberPortError _zaberPortError;
		private DeviceMessage _replacementResponse;

		#endregion
	}
}

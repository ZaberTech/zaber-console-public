﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using log4net;

namespace Zaber
{
	/// <summary>
	///     Treats a collection of conversations as a single conversation. Useful
	///     for making requests from aliases (including the "all devices" alias on
	///     device number 0). When you make a request, it waits for a response from
	///     every conversation in the collection before it returns.
	/// </summary>
	public class ConversationCollection : Conversation, IList<Conversation>
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Create a new instance.
		/// </summary>
		/// <param name="aDevices">
		///     The device that this conversation will send
		///     single requests through.
		/// </param>
		public ConversationCollection(DeviceCollection aDevices)
			: base(aDevices)
		{
		}


		/// <summary>
		///     Send a request to a Zaber device collection and wait for the responses
		///     from all its member devices.
		/// </summary>
		/// <param name="aCommand">See <see cref="DataPacket.Command" />.</param>
		/// <param name="aData">See <see cref="DataPacket.NumericData" />.</param>
		/// <returns>
		///     The response message from the first conversation in the
		///     list.
		/// </returns>
		/// <exception cref="RequestCollectionException">
		///     At least one conversation in the list raised an exception. The detailed
		///     responses and exceptions are available from the exception as a list of
		///     <see cref="ConversationTopic" /> objects.
		/// </exception>
		/// <exception cref="RequestTimeoutException">
		///     Some of the requests did not
		///     complete before the timeout expired.
		/// </exception>
		/// <remarks>
		///     This method sends the request on to this conversation's device and then
		///     puts the thread to sleep until a response is received from every
		///     conversation in the list. However,
		///     there are some situations where it can get confused about
		///     which response goes with which request. See <see cref="Conversation.Request(Command, int)" />
		///     for details.
		/// </remarks>
		/// <seealso cref="RequestCollection" />
		public override DeviceMessage Request(Command aCommand, int aData)
		{
			var msg = RequestCollection(aCommand, aData).FirstOrDefault();

			if (null == msg)
			{
				throw new InvalidOperationException(
					"Response properties are undefined for an empty collection. This may occur if no devices have been discovered.");
			}

			return msg;
		}


		/// <summary>
		///     Record that a request is about to be made.
		/// </summary>
		/// <param name="aMessageId">The message id that the request will use.</param>
		/// <returns>The topic that will monitor the request.</returns>
		/// <remarks>
		///     <para>
		///         You can then make the request using the device directly and call
		///         <see cref="ConversationTopic.Wait()" /> to wait for the response. This is
		///         useful if you want to make several unrelated requests of several devices
		///         and then wait until they are all completed. If message ids are enabled,
		///         be sure to use the message id from the topic when sending the request.
		///     </para>
		///     <para>
		///         If message ids are disabled, then this technique is not thread-safe,
		///         because there's no guarantee that two requests from two threads will get
		///         sent to the device in the same order that their topics were started.
		///         If you're sending requests to one device from more than one thread,
		///         either enable message ids, or implement your own locking at a higher level.
		///     </para>
		/// </remarks>
		public override ConversationTopic StartTopic(byte aMessageId) => StartTopicCollection(aMessageId);


		/// <summary>
		///     Record that a request is about to be made to several devices.
		/// </summary>
		/// <returns>The topic that will monitor the request.</returns>
		/// <remarks>
		///     <para>
		///         You can then make the request using the devices directly and call
		///         <see cref="ConversationTopic.Wait()" /> to wait for the response. This is
		///         useful if you want to make several unrelated requests of several devices
		///         and then wait until they are all completed. If message ids are enabled,
		///         be sure to use the message id from the topic when sending the request.
		///     </para>
		///     <para>
		///         If message ids are disabled, then this technique is not thread-safe,
		///         because there's no guarantee that two requests from two threads will get
		///         sent to the device in the same order that their topics were started.
		///         If you're sending requests to one device from more than one thread,
		///         either enable message ids, or implement your own locking at a higher level.
		///     </para>
		/// </remarks>
		public ConversationTopicCollection StartTopicCollection()
		{
			const int maxMessageIds = 254; // 0 and 255 are not allowed.

			for (var i = 0; i < maxMessageIds; i++)
			{
				try
				{
					return StartTopicCollection(CalculateMessageId());
				}
				catch (ActiveMessageIdException)
				{
					// continue trying for all possible message ids
				}
			}

			throw new InvalidOperationException("All message ids are active.");
		}


		/// <summary>
		///     Record that a request is about to be made to several devices.
		/// </summary>
		/// <param name="aMessageId">The message id that the request will use.</param>
		/// <returns>The topic that will monitor the request.</returns>
		/// <remarks>
		///     <para>
		///         You can then make the request using the devices directly and call
		///         <see cref="ConversationTopic.Wait()" /> to wait for the response. This is
		///         useful if you want to make several unrelated requests of several devices
		///         and then wait until they are all completed. If message ids are enabled,
		///         be sure to use the message id from the topic when sending the request.
		///     </para>
		///     <para>
		///         If message ids are disabled, then this technique is not thread-safe,
		///         because there's no guarantee that two requests from two threads will get
		///         sent to the device in the same order that their topics were started.
		///         If you're sending requests to one device from more than one thread,
		///         either enable message ids, or implement your own locking at a higher level.
		///     </para>
		/// </remarks>
		public ConversationTopicCollection StartTopicCollection(byte aMessageId)
		{
			var topics = new ConversationTopicCollection { MessageId = aMessageId };

			try
			{
				foreach (var item in FindSingleConversations())
				{
					topics.Add(item.StartTopic(aMessageId));
				}
			}
			catch (ActiveMessageIdException)
			{
				foreach (var topic in topics)
				{
					topic.Cancel();
				}

				throw;
			}

			lock (_locker)
			{
				_waitingTopics.Add(topics);
				topics.Completed += Topic_Completed;
			}

			return topics;
		}


		/// <summary>
		///     Send a request to several conversations and coordinate all the responses.
		/// </summary>
		/// <param name="aCommand">See <see cref="DataPacket.Command" />.</param>
		/// <param name="aData">
		///     An array of data values. One will be sent to each
		///     conversation in the list. There are two special cases: one entry and no entries.
		///     An array with one entry will be used to send a single request to this
		///     conversation's device and then the thread will block until a response
		///     is received from every conversation in the list. An array with no entries
		///     is the same as one entry with value 0. See <see cref="DataPacket.NumericData" />.
		/// </param>
		/// <returns>
		///     A list of response messages in the same order that the
		///     conversations appear in the list.
		/// </returns>
		/// <exception cref="RequestCollectionException">
		///     At least one request in the list raised an exception. The detailed
		///     responses and exceptions are available from the exception as a list of
		///     <see cref="ConversationTopic" /> objects.
		/// </exception>
		/// <exception cref="RequestTimeoutException">
		///     Some of the requests did not
		///     complete before the timeout expired.
		/// </exception>
		/// <remarks>
		///     This method sends the request on to the device and then
		///     puts the thread to sleep until a response is received from every
		///     device in the collection. However,
		///     there are some situations where it can get confused about
		///     which response goes with which request. See
		///     <see cref="Conversation.Request(Command, int)" />
		///     for details.
		/// </remarks>
		/// <seealso cref="Request" />
		public IList<DeviceMessage> RequestCollection(Command aCommand, params int[] aData)
		{
			ConversationTopicCollection topics;
			var currentMembers = new List<Conversation>();
			lock (_locker)
			{
				topics = StartTopicCollection();

				// take a snapshot of the current members because new ones might
				// get added while you're sending requests.
				currentMembers.AddRange(_members);
			}

			if (aData.Length < 2)
			{
				var selectedData = 0 == aData.Length ? 0 : aData[0];
				_log.Debug("Sending to alias device.");

				foreach (var topic in topics)
				{
					PrepareForRetry(aCommand, selectedData, topic);
				}

				Device.Send(aCommand, selectedData, topics.MessageId);
			}
			else
			{
				_log.Debug("Sending to individual devices.");
				for (var i = 0; i < currentMembers.Count; i++)
				{
					PrepareForRetry(aCommand, aData[i], topics[i]);
					var device = currentMembers[i].Device;
					device.Send(aCommand, aData[i], topics.MessageId);
				}
			}

			_log.Debug("Waiting for members.");
			if (!topics.Wait(TimeoutTimer))
			{
				throw new RequestTimeoutException();
			}

			topics.Validate();
			return topics.Select(topic => topic.Response).ToList();
		}


		/// <summary>
		///     Repeatedly check the device's status until it is idle, using
		///     text-mode requests.
		/// </summary>
		public override void PollUntilIdle() => PollUntilIdle(PollTimeoutTimer);


		/// <summary>
		///     Repeatedly check the device's status until it is idle, using
		///     text-mode requests and the given timer.
		/// </summary>
		/// <param name="aTimer">
		///     How long to wait between polling requests.</param>
		/// <param name="aExceptionOnWarnings">Optional: Throw an exception if there are warning flags. Defaults to true.</param>
		public override void PollUntilIdle(TimeoutTimer aTimer, bool aExceptionOnWarnings = true)
		{
			foreach (var item in FindSingleConversations())
			{
				item.PollUntilIdle(aTimer, aExceptionOnWarnings);
			}
		}


		/// <summary>
		///     Sorts the items in the list by their device numbers ascending.
		/// </summary>
		public void Sort() => _members.Sort(new ConversationComparer());


		#region -- IEnumerable<Conversation> Implementation --

		/// <summary>
		///     Returns an enumerator that iterates through the collection.
		/// </summary>
		/// <returns>an enumerator that iterates through the collection.</returns>
		public IEnumerator<Conversation> GetEnumerator() => _members.GetEnumerator();

		#endregion


		/// <summary>
		///     Gets or sets the number of times to retry a request after a port error.
		///     Defaults to 0.
		/// </summary>
		/// <remarks>
		///     By default, when the port raises its
		///     <see cref="IZaberPort.ErrorReceived" /> event, any waiting
		///     conversation topics throw a <see cref="ZaberPortErrorException" />.
		///     However, you can set this property to make the conversation retry
		///     the request a few times before throwing the exception. Only commands
		///     that are safe will be retried. For example, Move Relative cannot
		///     be retried, it will always throw an exception if a port error
		///     occurs. Setting this on a conversation collection will set it
		///     on all members of the collection.
		/// </remarks>
		public override int RetryCount
		{
			get => base.RetryCount;
			set
			{
				base.RetryCount = value;
				foreach (var member in _members)
				{
					member.RetryCount = value;
				}
			}
		}


		/// <summary>
		///     This is really only used for testing purposes to make sure that no
		///     requests are left over in any of the internal data structures.
		/// </summary>
		public override bool IsWaiting
		{
			get
			{
				lock (_locker)
				{
					return _waitingTopics.Count != 0;
				}
			}
		}

		#endregion

		#region -- Interfaces --

		#region -- IEnumerable Implementation --

		/// <summary>
		///     Returns an enumerator that iterates through the collection.
		/// </summary>
		/// <returns>an enumerator that iterates through the collection.</returns>
		IEnumerator IEnumerable.GetEnumerator() => _members.GetEnumerator();

		#endregion

		#region -- IList<Conversation> Implementation --

		/// <summary>
		///     Searches for the specified conversation and returns the zero-based index of the
		///     first occurrence within the entire list.
		/// </summary>
		/// <param name="aItem">
		///     The conversation to locate in the list. The value
		///     can be null.
		/// </param>
		/// <returns>
		///     The zero-based index of the first occurrence of item within the entire
		///     list, if found; otherwise, –1.
		/// </returns>
		public int IndexOf(Conversation aItem) => _members.IndexOf(aItem);


		/// <summary>
		///     Inserts an element into the list at the specified index.
		/// </summary>
		/// <param name="aIndex">The zero-based index at which item should be inserted.</param>
		/// <param name="aItem">The object to insert. The value can be null.</param>
		/// <exception cref="ArgumentOutOfRangeException">
		///     index is less than 0.
		///     -or- index is greater than <see cref="Count" />.
		/// </exception>
		public void Insert(int aIndex, Conversation aItem) => _members.Insert(aIndex, aItem);


		/// <summary>
		///     Removes the element at the specified index of the list.
		/// </summary>
		/// <param name="aIndex">The zero-based index of the element to remove.</param>
		/// <exception cref="ArgumentOutOfRangeException">
		///     index is less than 0.
		///     -or- index is equal to or greater than <see cref="Count" />.
		/// </exception>
		public void RemoveAt(int aIndex) => _members.RemoveAt(aIndex);


		/// <summary>
		///     Gets or sets the element at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index of the element to get or set.</param>
		/// <returns>The element at the specified index.</returns>
		/// <exception cref="ArgumentOutOfRangeException">
		///     index is less than 0.
		///     -or- index is equal to or greater than <see cref="Count" />.
		/// </exception>
		public Conversation this[int index]
		{
			get => _members[index];
			set => _members[index] = value;
		}

		#endregion

		#endregion

		#region -- Non-Public Methods --

		// Finds all sub-conversations that belong to the collection, but does not include
		// axis conversations.
		private List<Conversation> FindSingleConversations()
		{
			lock (_locker)
			{
				var singleConversations = new List<Conversation>();
				var visitedCollections = new HashSet<ICollection<Conversation>>();
				AddSingleConversations(_members, singleConversations, visitedCollections, false);

				return singleConversations;
			}
		}


		// Finds all sub-conversations that belong to the collection, including axes of
		// standalone controllers.
		private List<Conversation> FindAllConversations()
		{
			lock (_locker)
			{
				var singleConversations = new List<Conversation>();
				var visitedCollections = new HashSet<ICollection<Conversation>>();
				AddSingleConversations(_members, singleConversations, visitedCollections, true);

				return singleConversations;
			}
		}


		private void AddSingleConversations(ICollection<Conversation> aSource, List<Conversation> aSingleConversations,
											HashSet<ICollection<Conversation>> aVisitedCollections,
											bool aIncludeAxes)
		{
			lock (_locker)
			{
				if (aVisitedCollections.Contains(aSource))
				{
					return;
				}

				aVisitedCollections.Add(aSource);
				foreach (var item in aSource)
				{
					if (item.Device.IsSingleDevice)
					{
						if (!aSingleConversations.Contains(item))
						{
							aSingleConversations.Add(item);
						}

						if (aIncludeAxes)
						{
							foreach (var axisConversation in item.Axes)
							{
								if (!aSingleConversations.Contains(axisConversation))
								{
									aSingleConversations.Add(axisConversation);
								}
							}
						}
					}
					else
					{
						var collection = (ConversationCollection) item;
						AddSingleConversations(collection, aSingleConversations, aVisitedCollections, aIncludeAxes);
					}
				}
			}
		}


		private void Topic_Completed(object aSender, EventArgs aArgs)
		{
			var topicCollection = (ConversationTopicCollection)aSender;
			lock (_locker)
			{
				_waitingTopics.Remove(topicCollection);
			}
		}

		#endregion

		#region -- Data --

		private class ConversationComparer : IComparer<Conversation>
		{
			#region -- Public Methods & Properties --

			public int Compare(Conversation aLeft, Conversation aRight)
				=> aLeft.Device.DeviceNumber.CompareTo(aRight.Device.DeviceNumber);

			#endregion
		}

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private readonly object _locker = new object();
		private readonly List<Conversation> _members = new List<Conversation>();
		private readonly List<ConversationTopicCollection> _waitingTopics = new List<ConversationTopicCollection>();

		#endregion

		#region -- ICollection<Conversation> Implementation --

		/// <summary>
		///     Adds a conversation to the collection.
		/// </summary>
		/// <param name="aItem">The conversation to add to the collection</param>
		public void Add(Conversation aItem) => _members.Add(aItem);


		/// <summary>
		///     Removes all conversations from the collection.
		/// </summary>
		public void Clear() => _members.Clear();


		/// <summary>
		///     Determines whether the collection contains a specific conversation.
		/// </summary>
		/// <param name="aItem">The conversation to locate in the collection</param>
		/// <returns>true if the conversation is found in the collection, otherwise false.</returns>
		public bool Contains(Conversation aItem) => _members.Contains(aItem);


		/// <summary>
		///     Copies the conversations in the collection to a System.Array,
		///     starting at a particular System.Array index.
		/// </summary>
		/// <param name="aArray">
		///     The one-dimensional System.Array that is the
		///     destination of the conversations copied from the collection. The
		///     System.Array must have zero-based indexing.
		/// </param>
		/// <param name="aArrayIndex">
		///     The zero-based index in array at which
		///     copying begins.
		/// </param>
		/// <exception cref="ArgumentOutOfRangeException">
		///     arrayIndex is less
		///     than 0.
		/// </exception>
		/// <exception cref="ArgumentNullException">array is null.</exception>
		/// <exception cref="ArgumentException">
		///     arrayIndex is equal to or
		///     greater than the length of array.-or-The number of elements in the
		///     source collection is greater than the available space from
		///     arrayIndex to the end of the destination array.
		/// </exception>
		public void CopyTo(Conversation[] aArray, int aArrayIndex) => _members.CopyTo(aArray, aArrayIndex);


		/// <summary>
		///     Gets the number of conversations contained in the collection.
		/// </summary>
		public int Count => _members.Count;


		/// <summary>
		///     Gets a value indicating whether the collection is read-only.
		/// </summary>
		public bool IsReadOnly => false;


		/// <summary>
		///     Removes the first occurrence of a specific conversation from the collection.
		/// </summary>
		/// <param name="aItem">The conversation to remove from the collection.</param>
		/// <returns>
		///     true if the conversation was successfully removed from the collection,
		///     otherwise false. This method also returns false if the conversation is not found in
		///     the collection.
		/// </returns>
		public bool Remove(Conversation aItem) => _members.Remove(aItem);

		#endregion
	}
}

﻿using Zaber.Units;

namespace Zaber
{
	/// <summary>
	///     Represents an object which contains data that can be converted from
	///     real-world units to native Zaber device units.
	/// </summary>
	public interface IParameterConversionInfo
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     The unit of measure in which the value is measured.
		/// </summary>
		Unit ReferenceUnit { get; }


		/// <summary>
		///     The translation function for translating physical units for the
		///     value.
		/// </summary>
		ScalingFunction Function { get; }


		/// <summary>
		///     The scaling factor from physical units to native device units.
		/// </summary>
		decimal? Scale { get; }


		/// <summary>
		///     Whether the value is a relative position.
		/// </summary>
		bool IsRequestRelativePosition { get; }

		#endregion
	}
}

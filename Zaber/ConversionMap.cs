﻿using System;
using System.Linq;

namespace Zaber
{
	/// <summary>
	///     A collection of conversion tables to use when converting units of
	///     measure. DEPRECATED; use ConversionTable instead.
	/// </summary>
	[Obsolete]
	public static class ConversionMap
	{
		#region -- Public Methods & Properties --

		static ConversionMap()
		{
			var motionTypeCount = Enum.GetValues(typeof(MotionType)).Length;
			var measurementTypeCount = Enum.GetValues(typeof(MeasurementType)).Length;
			Tables = new ConversionTable[motionTypeCount, measurementTypeCount];
			Add(MotionType.Linear, MeasurementType.Position, ConversionTable.LinearPosition);
			Add(MotionType.Rotary, MeasurementType.Position, ConversionTable.RotaryPosition);
			Add(MotionType.Linear, MeasurementType.Velocity, ConversionTable.LinearVelocity);
			Add(MotionType.Rotary, MeasurementType.Velocity, ConversionTable.RotaryVelocity);
			Add(MotionType.Linear, MeasurementType.Acceleration, ConversionTable.LinearAcceleration);
			Add(MotionType.Rotary, MeasurementType.Acceleration, ConversionTable.RotaryAcceleration);
			Add(MotionType.Linear, MeasurementType.Current, ConversionTable.ElectricalCurrent);
			Add(MotionType.Rotary, MeasurementType.Current, ConversionTable.ElectricalCurrent);
		}


		/// <summary>
		///     Add an entry to the map showing which conversion table to use for a
		///     combination of motion and measurement types. DEPRECATED; do not use.
		/// </summary>
		/// <param name="aMotionType">The type of motion represented.</param>
		/// <param name="aMeasurementType">
		///     The type of measurement represented.
		/// </param>
		/// <param name="aTable">
		///     The conversion table to use for the given
		///     types of motion and measurement.
		/// </param>
		[Obsolete]
		public static void Add(MotionType aMotionType, MeasurementType aMeasurementType, ConversionTable aTable)
			=> Tables[(int) aMotionType, (int) aMeasurementType] = aTable;


		/// <summary>
		///     Find the conversion table to use for a combination of motion and
		///     measurement types. DEPRECATED; do not use.
		/// </summary>
		/// <param name="aMotionType">The type of motion to find.</param>
		/// <param name="aMeasurementType">
		///     The type of measurement to fine.
		/// </param>
		/// <returns>The conversion table </returns>
		[Obsolete]
		public static ConversionTable Find(MotionType aMotionType, MeasurementType aMeasurementType)
			=> Tables[(int) aMotionType, (int) aMeasurementType];


		/// <summary>
		///     Find the conversion table that contains a specific unit of measure.
		///     DEPRECATED; do not use.
		/// </summary>
		/// <param name="aUnit">The unit of measure to find.</param>
		/// <returns>The conversion table </returns>
		[Obsolete]
		public static ConversionTable Find(UnitOfMeasure aUnit)
		{
			foreach (var table in Tables)
			{
				if ((null != table) && table.UnitsOfMeasure.Contains(aUnit))
				{
					return table;
				}
			}

			return null;
		}

		#endregion

		#region -- Data --

		private static readonly ConversionTable[,] Tables;

		#endregion
	}
}

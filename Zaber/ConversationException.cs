using System;
using System.Runtime.Serialization;

namespace Zaber
{
	/// <summary>
	///     This is a base class for all the exceptions that the
	///     <see cref="Conversation" /> class throws. It lets you simplify your
	///     catch blocks if you want to treat all exceptions the same.
	/// </summary>
	[Serializable]
	public class ConversationException : Exception
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor - does not set a message.
		/// </summary>
		public ConversationException()
		{
		}


		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aMessage">The message to display</param>
		public ConversationException(string aMessage)
			: base(aMessage)
		{
		}


		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aMessage">The message to display</param>
		/// <param name="aException">The exception that caused this one.</param>
		public ConversationException(string aMessage, Exception aException)
			: base(aMessage, aException)
		{
		}


		/// <summary>
		///     Initializes a new instance with serialized
		///     data.
		/// </summary>
		/// <param name="aInfo">
		///     The System.Runtime.Serialization.SerializationInfo that holds the serialized
		///     object data about the exception being thrown.
		/// </param>
		/// <param name="aContext">
		///     The System.Runtime.Serialization.StreamingContext that contains contextual
		///     information about the source or destination.
		/// </param>
		protected ConversationException(SerializationInfo aInfo, StreamingContext aContext)
			: base(aInfo, aContext)
		{
		}

		#endregion
	}
}

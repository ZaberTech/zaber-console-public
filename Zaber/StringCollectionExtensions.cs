﻿using System.Collections.Specialized;

namespace Zaber
{
	/// <summary>
	///     Helpful extension methods for the StringCollection class.
	/// </summary>
	public static class StringCollectionExtensions
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Add a string to the collection only if it is not already there.
		/// </summary>
		/// <param name="aCollection">The StringCollection to add to.</param>
		/// <param name="aValue">The string to add to the collection.</param>
		/// <returns>True if the value was added; false if it was already present.</returns>
		public static bool AddIfUnique(this StringCollection aCollection, string aValue)
		{
			if (!aCollection.Contains(aValue))
			{
				aCollection.Add(aValue);
				return true;
			}

			return false;
		}


		/// <summary>
		///     Duplicate a StringCollection.
		/// </summary>
		/// <param name="aCollection">The collection to duplicate.</param>
		/// <returns>A copy of the input collection, containing the same strings in the same order.</returns>
		public static StringCollection Clone(this StringCollection aCollection)
		{
			var result = new StringCollection();

			foreach (var s in aCollection)
			{
				result.Add(s);
			}

			return result;
		}

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using log4net;
using Zaber.Ports;
using Zaber.Threading;

namespace Zaber
{
	/// <summary>
	///     This is the main class that most applications will interact with.
	/// </summary>
	/// <remarks>
	///     The standard pattern is:
	///     <list type="number">
	///         <item>Call <see cref="GetPortNames" /> and display them to the user.</item>
	///         <item>When the user picks one, pass it to <see cref="Open" />.</item>
	///         <item>
	///             Either call <see cref="GetDevice(byte,int)" /> to get a specific
	///             device, call <see cref="Devices" /> to get the all of them, call
	///             <see cref="GetConversation(byte,int)" /> to get a specific conversation,
	///             or call <see cref="Conversations" /> to get all of them.
	///         </item>
	///         <item>
	///             Either make requests from conversations or send commands to
	///             devices and register to receive the response events.
	///         </item>
	///     </list>
	///     See the ZaberDeviceDemo and ZaberConversationDemo projects for some
	///     example code.
	/// </remarks>
	/// <remarks>
	///     This class is sealed. It has not been designed for inheritance, and any
	///     class which extends this class should do so by composition ("has a")
	///     rather than inheritance ("is a").
	/// </remarks>
	public sealed class ZaberPortFacade : IDisposable
	{
		#region -- Events --

		/// <summary>
		///     Raised when the current conversation has changed.
		/// </summary>
		/// <remarks>
		///     Get the <see cref="Conversation" /> property
		///     to see what the new one is.
		/// </remarks>
		public event EventHandler SelectedConversationChanged;


		/// <summary>
		///     Raised when the port facade has finished opening.
		/// </summary>
		public event EventHandler Opened;


		/// <summary>
		///     Raised when the port is about to close. This does not fire
		///     if the port is unexpectedly closed by the system.
		/// </summary>
		public event EventHandler Closing;


		/// <summary>
		///     Raised when the port facade is closed.
		/// </summary>
		public event EventHandler Closed;

		/// <summary>
		///     Raised when the port is closed because of some external cause.
		/// </summary>
		public event EventHandler ClosedUnexpectedly;


		/// <summary>
		///     Raised when the port facade is invalidated and should be reopened.
		/// </summary>
		/// <remarks>
		///     The port facade is invalidated when the list of connected
		///     devices may have become inaccurate. This happens after a response
		///     to the Renumber command, the Convert to ASCII/Binary command,
		///     the RestoreSettings command, or the SetAlias command.
		///     If client applications want to be sure the device list is
		///     accurate, they should call <see cref="Close" /> and then
		///     <see cref="Open" /> to reopen the port and requery the list of
		///     connected devices. The event is only raised the first time the
		///     facade is invalidated, so if two renumber commands are sent without
		///     reopening the port facade in between, only the first one will raise
		///     this event.
		/// </remarks>
		public event EventHandler Invalidated;

		#endregion

		#region -- Public Methods & Properties --

		#region -- Initialization --

		/// <summary>
		///     Default constructor. Initializes members to default values.
		/// </summary>
		public ZaberPortFacade()
		{
			var allDevices = new DeviceCollection
			{
				DeviceNumber = 0,
				UnitConverter = new ConversionTable()
			};

			_devices[0] = allDevices;

			_selectedConversation = _conversations[0] = new ConversationCollection(allDevices);

			DeviceTypeMap = new Dictionary<Tuple<int, FirmwareVersion>, DeviceType>();
		}

		#endregion

		#region -- IDisposable Implementation --

		/// <summary>
		///     Performs application-defined tasks associated with freeing, releasing, or
		///     resetting unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			_port.Dispose();
			foreach (var conversation in _conversations.Values)
			{
				conversation.Dispose();
			}
		}

		#endregion

		#endregion

		#region -- Public Methods --

		/// <summary>
		///     Add a DeviceType to <see cref="DeviceTypeMap" />.
		/// </summary>
		/// <param name="aDeviceType">The DeviceType to add.</param>
		public void AddDeviceType(DeviceType aDeviceType)
		{
			var key = new Tuple<int, FirmwareVersion>(aDeviceType.DeviceId, aDeviceType.FirmwareVersion);
			DeviceTypeMap[key] = aDeviceType;
		}


		/// <summary>
		///     Check to see if there is a conversation with the given device number.
		/// </summary>
		/// <param name="aDeviceNumber">
		///     The number of the device whose
		///     conversation you want.
		/// </param>
		/// <returns>true if the conversation exists.</returns>
		public bool ContainsConversation(byte aDeviceNumber)
		{
			lock (_locker)
			{
				return _conversations.ContainsKey(aDeviceNumber);
			}
		}


		/// <summary>
		///     Check to see if there is a device with the given device number.
		/// </summary>
		/// <param name="aDeviceNumber">The number of the device you want.</param>
		/// <returns>true if the device exists.</returns>
		public bool ContainsDevice(byte aDeviceNumber) => ContainsConversation(aDeviceNumber);


		/// <summary>
		///     Look up a conversation by device number and axis number.
		/// </summary>
		/// <param name="aDeviceNumber">
		///     The number of the device whose
		///     conversation you want. Device number 0 or other alias numbers will
		///     return a ConversationCollection that represents all the devices
		///     using that alias number.
		/// </param>
		/// <param name="aAxisNumber">
		///     The number of the axis you want. Numbers
		///     1 through 9 will return individual axes, and 0 will return the main
		///     conversation.
		/// </param>
		/// <returns>The requested conversation.</returns>
		/// <exception cref="KeyNotFoundException">
		///     If no conversation has the
		///     requested device number and axis number.
		/// </exception>
		/// <seealso cref="ContainsDevice" />
		public Conversation GetConversation(byte aDeviceNumber, int aAxisNumber = 0)
		{
			lock (_locker)
			{
				if (!_conversations.TryGetValue(aDeviceNumber, out var conversation))
				{
					throw new KeyNotFoundException(string.Format(CultureInfo.CurrentCulture,
																 "Device number {0} not found.",
																 aDeviceNumber));
				}

				if (0 == aAxisNumber)
				{
					return conversation;
				}

				if ((aAxisNumber > conversation.Axes.Count) || (aAxisNumber < 0))
				{
					throw new KeyNotFoundException(string.Format(CultureInfo.CurrentCulture,
																 "Axis number {0} not found on device number {1}.",
																 aAxisNumber,
																 aDeviceNumber));
				}

				return conversation.Axes[aAxisNumber - 1];
			}
		}


		/// <summary>
		///     Look up a conversation collection by device number.
		/// </summary>
		/// <param name="aDeviceNumber">
		///     The number of the device whose
		///     conversation you want.
		/// </param>
		/// <returns>The conversation collection with the requested device number.</returns>
		/// <exception cref="KeyNotFoundException">
		///     If no conversation has the
		///     requested device number.
		/// </exception>
		/// <exception cref="InvalidCastException">
		///     If the conversation is a single
		///     conversation instead of a collection.
		/// </exception>
		/// <remarks>
		///     To avoid the exceptions, you can safely get a conversation collection
		///     as follows:
		///     <example>
		///         if (portFacade.ContainsDevice(deviceNumber))
		///         {
		///         ConversationCollection conversations =
		///         portFacade.GetConversation(deviceNumber)
		///         as ConversationCollection;
		///         if (conversations != null)
		///         {
		///         // Use conversations for something
		///         }
		///         }
		///     </example>
		/// </remarks>
		public ConversationCollection GetConversationCollection(byte aDeviceNumber)
			=> (ConversationCollection) GetConversation(aDeviceNumber);


		/// <summary>
		///     Look up a device by device number and axis number.
		/// </summary>
		/// <param name="aDeviceNumber">
		///     The number of the device you want.
		///     Device number 0 or other alias numbers will return a
		///     DeviceCollection that represents all the devices using that alias
		///     number.
		/// </param>
		/// <param name="aAxisNumber">
		///     The number of the axis you want. Numbers
		///     1 through 9 will return individual axes, and 0 will return the main
		///     device.
		/// </param>
		/// <returns>The requested device.</returns>
		/// <exception cref="KeyNotFoundException">
		///     If no device has the
		///     requested device number and axis number.
		/// </exception>
		/// <seealso cref="ContainsDevice" />
		public ZaberDevice GetDevice(byte aDeviceNumber, int aAxisNumber = 0)
			=> GetConversation(aDeviceNumber, aAxisNumber).Device;


		/// <summary>
		///     Look up a device collection by device number.
		/// </summary>
		/// <param name="aDeviceNumber">The number of the device you want.</param>
		/// <returns>The device with the requested device number.</returns>
		/// <exception cref="KeyNotFoundException">
		///     If no device has the
		///     requested device number.
		/// </exception>
		/// <exception cref="InvalidCastException">
		///     If the device is a single
		///     device instead of a collection.
		/// </exception>
		/// <remarks>
		///     To avoid the exceptions, you can safely get a device collection
		///     as follows:
		///     <example>
		///         if (portFacade.ContainsDevice(deviceNumber))
		///         {
		///         DeviceCollection devices =
		///         portFacade.GetDevice(deviceNumber)
		///         as DeviceCollection;
		///         if (devices != null)
		///         {
		///         // Use devices for something
		///         }
		///         }
		///     </example>
		/// </remarks>
		public DeviceCollection GetDeviceCollection(byte aDeviceNumber)
			=> (DeviceCollection) GetDevice(aDeviceNumber);


		/// <summary>
		///     Gets a DeviceType from the <see cref="DeviceTypeMap" />.
		/// </summary>
		/// <param name="aDeviceId">
		///     The device ID of the desired DeviceType.
		/// </param>
		/// <param name="aFirmwareVersion">
		///     The firmware version of the desired
		///     DeviceType, in the form given by the device (a 3-digit integer).
		/// </param>
		/// <exception cref="KeyNotFoundException">
		///     There does not exist an entry
		///     for the given device ID and firmware version.
		/// </exception>
		/// <returns>
		///     A DeviceType with the specified device ID and firmware
		///     version.
		/// </returns>
		public DeviceType GetDeviceType(int aDeviceId, int aFirmwareVersion)
			=> GetDeviceType(aDeviceId, new FirmwareVersion(aFirmwareVersion));


		/// <summary>
		///     Gets a DeviceType from the <see cref="DeviceTypeMap" />.
		/// </summary>
		/// <param name="aDeviceId">
		///     The device ID of the desired DeviceType.
		/// </param>
		/// <param name="aFirmwareVersion">
		///     The firmware version of the desired
		///     DeviceType.
		/// </param>
		/// <exception cref="KeyNotFoundException">
		///     There does not exist an entry
		///     for the given device ID and firmware version.
		/// </exception>
		/// <returns>
		///     A DeviceType with the specified device ID and firmware
		///     version.
		/// </returns>
		public DeviceType GetDeviceType(int aDeviceId, FirmwareVersion aFirmwareVersion)
		{
			var key = new Tuple<int, FirmwareVersion>(aDeviceId, aFirmwareVersion);
			return DeviceTypeMap[key];
		}


		/// <summary>
		///     Open the port and check what devices are present.
		/// </summary>
		/// <param name="aPortName">
		///     One of the port names returned by
		///     <see cref="GetPortNames" />.
		/// </param>
		/// <remarks>Be sure to call <see cref="Close()" /> when you're finished.</remarks>
		/// <exception cref="LoopbackException">
		///     A loopback connection was
		///     detected on the serial port. To recover from this exception,
		///     either physically remove the loopback connection or call
		///     <see cref="OpenWithoutQuery" />.
		/// </exception>
		/// <exception cref="UnauthorizedAccessException">
		///     Access is denied to
		///     the port.
		/// </exception>
		/// <exception cref="IOException">
		///     The port is in an invalid state.
		/// </exception>
		/// <exception cref="ZaberPortErrorException">
		///     An error occurred while
		///     querying devices.
		/// </exception>
		/// <exception cref="LoopbackException">
		///     A loopback connection was
		///     detected.
		/// </exception>
		public void Open(string aPortName = null)
		{
			IZaberPort port;
			lock (_locker)
			{
				port = _port;
			}

			port.Open(aPortName);

			TimeoutTimer queryTimeoutTimer;
			lock (_locker)
			{
				_currentState = ZaberPortFacadeState.QueryingDevices;
				_deviceIds = new Dictionary<byte, int>();
				_devicesInBootloaderMode = new HashSet<byte>();
				_areMessageIdsEnabled = false;
				_isInvalidated = false;
				queryTimeoutTimer = _queryTimeoutTimer;
				_errorDuringQuery = ZaberPortError.None;
				_isVoltageBadDuringQuery = false;
				IsCollisionDetected = false;
			}

			try
			{
				if (port.IsAsciiMode)
				{
					QueryAsciiDevices(port, queryTimeoutTimer);
				}
				else
				{
					QueryBinaryDevices(port, queryTimeoutTimer);
				}

				if (null != DeviceHelpProvider)
				{
					foreach (var device in _devices.Values)
					{
						device.HelpSource = DeviceHelpProvider.GetHelpForDevice(device, port.IsAsciiMode);

						foreach (var peripheral in device.Axes)
						{
							peripheral.HelpSource = DeviceHelpProvider.GetHelpForDevice(peripheral, port.IsAsciiMode);
						}
					}
				}

				if ((null != Opened) && !SuppressEvents)
				{
					Opened(this, EventArgs.Empty);
				}
			}
			catch (Exception)
			{
				port.Close();
				lock (_locker)
				{
					_currentState = ZaberPortFacadeState.Closed;
				}

				throw;
			}
		}


		/// <summary>
		///     Open the port without checking what devices are present and without
		///     building the <see cref="ZaberDevice" /> and
		///     <see cref="Conversation" /> objects to represent those devices.
		/// </summary>
		/// <param name="aPortName">
		///     One of the port names returned by
		///     <see cref="GetPortNames" />.
		/// </param>
		/// <remarks>
		///     There are always device and conversation objects for
		///     device number 0 (all devices), but the conversation is empty and
		///     will not support calls to the Request method. Be sure to call
		///     <see cref="Close()" /> when you're finished.
		/// </remarks>
		/// <exception cref="UnauthorizedAccessException">
		///     Access is denied to
		///     the port.
		/// </exception>
		/// <exception cref="IOException">
		///     The port is in an invalid state.
		/// </exception>
		public void OpenWithoutQuery(string aPortName = null)
		{
			IZaberPort port;
			lock (_locker)
			{
				port = _port;
				IsCollisionDetected = false;
			}

			try
			{
				port.Open(aPortName);
			}
			catch (Exception)
			{
				if ((null != port) && port.IsOpen)
				{
					port.Close();
				}

				lock (_locker)
				{
					_currentState = ZaberPortFacadeState.Closed;
				}

				throw;
			}

			lock (_locker)
			{
				_devices[0].Port = port;
				_selectedConversation = _conversations[0];
				_currentState = ZaberPortFacadeState.Open;
			}

			if ((null != Opened) && !SuppressEvents)
			{
				Opened(this, EventArgs.Empty);
			}
		}

		#endregion

		#region -- Private Methods --

		/// <summary>
		///     Check the 6th bit of a response to the "Get Device Mode" command.
		///     If the device's message ID mode does not match the setting of this
		///     ZaberPortFacade, send a command to set the device to match.
		/// </summary>
		/// <param name="aDataPacket">
		///     A DataPacket containing the response to a
		///     "Get Device Mode" command.
		/// </param>
		private void CheckMessageIdsMode(DataPacket aDataPacket)
		{
			DeviceModes newMode;
			IZaberPort port;

			lock (_locker)
			{
				if (!_areMessageIdsMonitored)
				{
					return;
				}

				var oldMode = (DeviceModes) (int) aDataPacket.NumericData;
				var areMessageIdsEnabledOnDevice = DeviceModes.None != (oldMode & DeviceModes.EnableMessageIdsMode);
				if (_areMessageIdsEnabled == areMessageIdsEnabledOnDevice)
				{
					return;
				}

				newMode = oldMode ^ DeviceModes.EnableMessageIdsMode;
				port = _port;
			}

			port.Send(aDataPacket.DeviceNumber, Command.SetDeviceMode, (int) newMode);
		}


		/// <summary>
		///     Ask each device whether it has Message IDs enabled. If not all
		///     devices agree, then try to set all devices to the mode of the first
		///     device to have responded.
		/// </summary>
		/// <param name="aConversations">Conversations with the devices to check.</param>
		/// <returns>Whether message IDs are enabled.</returns>
		private bool DetermineMessageIdMode(SortedDictionary<byte, Conversation> aConversations)
		{
			var isFirstDevice = true;
			var areMessageIdsEnabled = false;

			/* FW5 does not support the "Get Message ID Mode" command, and the
			 * occasional FW6 device has some trouble with the "Get Device Mode"
			 * command. So, we send the "Get Device Mode" command to FW5 
			 * devices, and the "Get Message ID Mode" command to FW6 devices. */
			var fw5Conversations = new List<Conversation>();
			var fw6Conversations = new List<Conversation>();
			foreach (var c in aConversations.Values)
			{
				if (c.Device.FirmwareVersion.Major < 6)
				{
					fw5Conversations.Add(c);
				}
				else
				{
					fw6Conversations.Add(c);
				}
			}

			var fw5Responses =
				EnumerateBinaryRequests(fw5Conversations, Command.ReturnSetting, (int) Command.SetDeviceMode);
			foreach (var response in fw5Responses.Where(r => null != r))
			{
				var conversation = aConversations[response.DeviceNumber];

				var deviceMode = (DeviceModes) (int) response.NumericData;
				var areMessageIdsEnabledOnDevice = DeviceModes.None != (deviceMode & DeviceModes.EnableMessageIdsMode);
				if (isFirstDevice)
				{
					areMessageIdsEnabled = areMessageIdsEnabledOnDevice;
					isFirstDevice = false;
				}
				else if (areMessageIdsEnabledOnDevice != areMessageIdsEnabled)
				{
					var newDeviceMode = deviceMode ^ DeviceModes.EnableMessageIdsMode;

					try
					{
						conversation.Request(Command.SetDeviceMode, (int) newDeviceMode);
					}
					catch (ErrorResponseException)
					{
						// If we can't set the message ID mode on all devices,
						// behave as if message IDs are not enabled.
						_log.WarnFormat("Could not set message ID mode on device {0}. Response received: {1}.",
										response.DeviceNumber,
										response.FormatResponse());

						return false;
					}
				}
			}

			var fw6Responses = EnumerateBinaryRequests(fw6Conversations, 
													   Command.ReturnSetting, 
													   (int)Command.SetMessageIDMode);

			foreach (var response in fw6Responses.Where(r => null != r))
			{
				var conversation = aConversations[response.DeviceNumber];

				var areMessageIdsEnabledOnDevice = 1m == response.NumericData;
				if (isFirstDevice)
				{
					areMessageIdsEnabled = areMessageIdsEnabledOnDevice;
					isFirstDevice = false;
				}
				else if (areMessageIdsEnabled != areMessageIdsEnabledOnDevice)
				{
					try
					{
						conversation.Request(Command.SetMessageIDMode, areMessageIdsEnabled ? 1 : 0);
					}
					catch (ErrorResponseException)
					{
						// If we can't set the message ID mode on all devices,
						// behave as if message IDs are not enabled.
						_log.WarnFormat("Could not set message ID mode on device {0}. Response received: {1}.",
										response.DeviceNumber,
										response.FormatResponse());
						return false;
					}
				}
			}

			if (IsCollisionDetected)
			{
				// Wait for extra replies from devices with duplicate device numbers.
				QueryTimeoutTimer.Sleep();
			}

			foreach (var conversation in aConversations.Values)
			{
				conversation.Device.AreMessageIdsEnabled = areMessageIdsEnabled;
			}

			return areMessageIdsEnabled;
		}


		/// <summary>
		///     Send the same request to all devices, and wait for a response.
		///     Yield the response for each request, so that calling code can
		///     follow up before the next request is sent.
		/// </summary>
		/// <param name="aConversations">
		///     The conversations to send the request
		///     to.
		/// </param>
		/// <param name="aCommand">The ASCII command to be sent.</param>
		/// <returns>
		///     The responses in the same order as the conversations.
		///     Any device collection conversation (alias or All device) will have
		///     null responses.
		/// </returns>
		/// <remarks>
		///     Message IDs are deliberately not used here because this is part of
		///     device detection and some older devices don't support message IDs. This
		///     function should only be used when the port is opening, and not after
		///     device detection is finished.
		/// </remarks>
		private IEnumerable<DeviceMessage> EnumerateAsciiRequests(IEnumerable<Conversation> aConversations,
																  string aCommand)
		{
			foreach (var conversation in aConversations)
			{
				if (!conversation.Device.IsSingleDevice)
				{
					// skip device collections (all or alias)
					yield return null;
					continue;
				}

				var topic = conversation.StartTopic();
				conversation.Device.Send(aCommand);

				if (!topic.Wait(_queryTimeoutTimer))
				{
					throw new RequestTimeoutException();
				}

				if (topic.IsValid)
				{
					yield return topic.Response;
				}
				else if (IsUnsupportedSetting(topic.Response))
				{
					yield return null;
				}

				if (_isVoltageBadDuringQuery)
				{
					// Allow low voltage error to go by.
					_queryTimeoutTimer.Sleep();
				}
			}

			if (IsCollisionDetected)
			{
				// Wait for extra replies from devices with duplicate device numbers.
				QueryTimeoutTimer.Sleep();
			}
		}


		/// <summary>
		///     Send the same request to all devices, and wait for a response.
		///     Yield the response for each request, so that calling code can
		///     follow up before the next request is sent.
		/// </summary>
		/// <param name="aConversations">
		///     The conversations to send the request
		///     to.
		/// </param>
		/// <param name="aCommand">
		///     The command to send if the port is in Binary
		///     mode.
		/// </param>
		/// <param name="aData">
		///     The data to send if the port is in Binary
		///     mode.
		/// </param>
		/// <returns>
		///     The responses in the same order as the conversations.
		///     Any device collection conversation (alias, or All device) will have null responses.
		///     If the command is not supported, response is null.
		/// </returns>
		private IEnumerable<DeviceMessage> EnumerateBinaryRequests(IEnumerable<Conversation> aConversations,
																   Command aCommand, int aData)
		{
			foreach (var conversation in aConversations)
			{
				if (!conversation.Device.IsSingleDevice)
				{
					// skip device collections (all or alias)
					yield return null;
					continue;
				}

				var topic = conversation.StartTopic();
				if ((Command.ReturnSetting == aCommand)
				&& ((int) Command.SetMicrostepResolution == aData)
				&& (conversation.Device.FirmwareVersion.Major < 5))
				{
					/* FW 2.93 has fixed resolution of 64. It also does not
					 * reply to ReturnSetting(SetMicrostepResolution), 
					 * resulting in a TimeoutException when attempting to open
					 * the port. */
					conversation.Device.MicrostepResolution = 64;
					yield return null;
					continue;
				}

				conversation.Device.Send(aCommand, aData, topic.MessageId);

				if (!topic.Wait(_queryTimeoutTimer))
				{
					throw new RequestTimeoutException();
				}

				if (topic.IsValid)
				{
					yield return topic.Response;
				}
				else if (IsUnsupportedSetting(topic.Response))
				{
					yield return null;
				}

				if (_isVoltageBadDuringQuery)
				{
					// allow low voltage error to go by.
					_queryTimeoutTimer.Sleep();
				}
			}

			if (IsCollisionDetected)
			{
				// Wait for extra replies from devices with duplicate device numbers.
				QueryTimeoutTimer.Sleep();
			}
		}


		private void GetAliases(IDictionary<byte, ZaberDevice> aDevices,
								SortedDictionary<byte, Conversation> aConversations)
		{
			var singleDeviceConversations = new List<Conversation>();
			singleDeviceConversations.AddRange(aConversations.Values);

			var minVersion = new FirmwareVersion(5, 0);

			foreach (var conversation in singleDeviceConversations)
			{
				if (conversation.Device.FirmwareVersion < minVersion)
				{
					continue;
				}

				var oldTimer = conversation.TimeoutTimer;
				conversation.TimeoutTimer = _queryTimeoutTimer;
				var aliasNumberAsInt =
					(int) conversation.Request(Command.ReturnSetting, (int) Command.SetAliasNumber).NumericData;
				conversation.TimeoutTimer = oldTimer;

				if (_isVoltageBadDuringQuery)
				{
					// allow low voltage error to go by.
					_queryTimeoutTimer.Sleep();
				}

				if (!IsValidByteValue(aliasNumberAsInt) || (0 == aliasNumberAsInt))
				{
					// ignore alias if it's an invalid value or the default.
					continue;
				}

				var aliasNumber = (byte) aliasNumberAsInt;
				DeviceCollection aliasDevice;
				ConversationCollection aliasConversation;
				if (aDevices.ContainsKey(aliasNumber))
				{
					var deviceInAliasPosition = aDevices[aliasNumber];
					if (deviceInAliasPosition.IsSingleDevice)
					{
						// Ignore alias if it collides with a real device.
						continue;
					}

					// else: We already have this alias all set up.
					aliasDevice = (DeviceCollection) deviceInAliasPosition;
					aliasConversation = (ConversationCollection) aConversations[aliasNumber];
				}
				else
				{
					aliasDevice = new DeviceCollection
					{
						DeviceNumber = aliasNumber,
						DeviceType = new DeviceType(DefaultDeviceType),
						Port = conversation.Device.Port,
						AreMessageIdsEnabled = conversation.Device.AreMessageIdsEnabled
					};

					aDevices[aliasNumber] = aliasDevice;

					aliasConversation = new ConversationCollection(aliasDevice);
					aConversations[aliasNumber] = aliasConversation;
				}

				aliasDevice.Add(conversation.Device);
				aliasConversation.Add(conversation);
			}

			if (IsCollisionDetected)
			{
				// Wait for extra replies from devices with duplicate device numbers.
				QueryTimeoutTimer.Sleep();
			}
		}


		/// <summary>
		///     Query each device for the list of text commands that it supports.
		/// </summary>
		/// <param name="aConversations">The list of conversations to query.</param>
		/// <param name="aQueryTimeoutTimer">How long to wait for responses.</param>
		private static void GetTextCommands(SortedDictionary<byte, Conversation> aConversations,
											TimeoutTimer aQueryTimeoutTimer)
		{
			foreach (var conversation in aConversations.Values.Where(

				// Skip device collections (All, alias) and individual axes.
				conversation => conversation.Device.IsSingleDevice
							&& (0 == conversation.Device.AxisNumber)
							&& !conversation.Device.IsInBootloaderMode))
			{
				using (var listener = new DeviceListener(conversation.Device))
				{
					try
					{
						conversation.Request("help commands list");
					}
					catch (ZaberPortErrorException ex)
					{
						// Ignore invalid packets and bravely carry on.
						// Line noise/data corruption shouldn't necessarily
						// stop us, as long as the command went through OK.
						if (ZaberPortError.InvalidPacket != ex.ErrorType)
						{
							throw;
						}
					}

					// Get known ASCII commands from the device's DeviceType.
					var device = conversation.Device;
					var knownCommands = new Dictionary<string, CommandInfo>();
					foreach (var cmd in device.DeviceType.Commands.Where(cmd => null != cmd.TextCommand))
					{
						knownCommands[cmd.TextCommand] = cmd;
					}

					// Give the device a new DeviceType based on the old one,
					// then fill its commands list with known commands plus the
					// ones parsed from the real-life device's response.
					device.DeviceType = new DeviceType(device.DeviceType);
					var asciiCommands = new Dictionary<string, CommandInfo>();
					while (true)
					{
						DeviceMessage infoResponse;
						try
						{
							infoResponse = listener.NextResponse(aQueryTimeoutTimer);
						}
						catch (RequestTimeoutException ex)
						{
							//If there are no more responses, bail out.
							_log.Warn("Unexpected end of text commands list.", ex);
							break;
						}

						if (MessageType.Comment != infoResponse.MessageType)
						{
							continue;
						}

						var commandParts = infoResponse
									   .Body.Trim()
									   .Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
						if (0 == commandParts.Length)
						{
							break; // Empty line signifies end of list.
						}

						if (1 == commandParts.Length)
						{
							// Though conversation.Request checks for malformed
							// messages, it only checks according to general
							// rules. We still need to check that replies are
							// valid for our purpose.
							_log.WarnFormat("Device reported malformed command string. Could not parse message: {0}",
											infoResponse);
							continue; // Skip this unparseable reply.
						}

						// The first (0th) part contains metadata about the
						// command. The first word of the command proper is
						// the second (1st) part of "commandParts".
						var startPart = 1;

						// Lower-case words are command keywords. Count them.
						var partCount = startPart;
						while (((startPart + partCount) < commandParts.Length)
						   && !commandParts[startPart + partCount].ToCharArray().Any(char.IsUpper))
						{
							partCount++;
						}

						// If there are remaining words, then they must be
						// parameters (upper-case).
						var hasParams = (startPart + partCount) < commandParts.Length;

						CommandInfo commandInfo;
						if ("set" == commandParts[startPart])
						{
							startPart++;
							partCount--;
							commandInfo = new SettingInfo();
						}
						else if ("get" == commandParts[startPart])
						{
							startPart++;
							partCount--;

							// A "get" command without a matching "set" must
							// be read-only.
							// 
							// Though "get" commands appear *first* in the list
							// when queried from a device, the "set" entry will
							// overwrite this "read-only" entry once it comes
							// around. Thereby, any "get" commands which were
							// not followed by "set" commands will remain read-
							// only settings.
							commandInfo = new ReadOnlySettingInfo();
						}
						else
						{
							commandInfo = new CommandInfo();
						}

						// "commandName" is the command without parameters.
						var commandName = string.Join(" ", commandParts, startPart, partCount);
						commandInfo.HasParameters = hasParams;
						commandInfo.TextCommand = commandName;
						commandInfo.Name = commandName;

						// The first 3 characters of the first word determine
						// whether a command is basic, and whether it is an
						// axis- or device-scope command.
						commandInfo.IsBasic =
							(0 == commandParts.Length)
						|| (commandParts[0].Length < 2)
						|| ('a' == commandParts[0][1]); //a = always, o = optional
						commandInfo.IsAxisCommand =
							(0 == commandParts.Length)
						|| (commandParts[0].Length < 3)
						|| ('a' == commandParts[0][2]); // a = axis, d = device

						// Lop the last word off of the command repeatedly,
						// trying to find a command string which matches an
						// entry in the list of known commands.
						while (partCount >= 1)
						{
							var parentName = string.Join(" ", commandParts, startPart, partCount);

							if (knownCommands.TryGetValue(parentName, out var knownCommand))
							{
								// Found it! Copy what properties we can from it.
								commandInfo.RequestUnit = knownCommand.RequestUnit;
								commandInfo.RequestUnitScale = knownCommand.RequestUnitScale;
								commandInfo.RequestUnitFunction = knownCommand.RequestUnitFunction;
								commandInfo.DataDescription = knownCommand.DataDescription;
								commandInfo.ResponseUnit = knownCommand.ResponseUnit;
								commandInfo.ResponseUnitScale = knownCommand.ResponseUnitScale;
								commandInfo.ResponseUnitFunction = knownCommand.ResponseUnitFunction;
								commandInfo.ResponseDescription = knownCommand.ResponseDescription;
								commandInfo.IsCurrentPositionReturned = knownCommand.IsCurrentPositionReturned;
								break;
							}

							partCount--;
						}

						asciiCommands[commandName] = commandInfo;
					}

					// Combine all the commands we just made with the device's
					// list of Binary commands, then set that as its new list.
					var newCommands = new List<CommandInfo>();

					// Preserve Binary commands.
					newCommands.AddRange(device.DeviceType.Commands.Where(c => null == c.TextCommand));
					newCommands.AddRange(asciiCommands.Values);
					device.DeviceType.Commands = newCommands;
				}
			}
		}


		private static bool IsUnsupportedSetting(DeviceMessage aResponse) 
			=> (null != aResponse)
			 && (((Command.Error == aResponse.Command)
			  && ((int) ZaberError.SettingInvalid
			  == (int) aResponse.NumericData))
			 || ((null != aResponse.TextData)
			 && aResponse.TextData.Equals("BADCOMMAND")));


		private static bool IsValidByteValue(int aIntValue)
			=> (byte.MinValue <= aIntValue) && (aIntValue <= byte.MaxValue);


		/// <summary>
		///     Query connected devices using the ASCII protocol.
		/// </summary>
		/// <param name="aPort">The serial port to use.</param>
		/// <param name="aQueryTimeoutTimer">Used to wait for responses.</param>
		/// <exception cref="ZaberPortErrorException">
		///     An error occurred while
		///     querying devices.
		/// </exception>
		/// <exception cref="LoopbackException">
		///     A loopback connection was
		///     detected.
		/// </exception>
		private void QueryAsciiDevices(IZaberPort aPort, TimeoutTimer aQueryTimeoutTimer)
		{
			// Get the ID of each device first. The _deviceIds dictionary
			// collects responses from the receiving thread. 
			aPort.Send("/get deviceid");

			_log.Debug("Waiting for device id responses.");
			aQueryTimeoutTimer.Sleep();
			_log.Debug("Finished waiting for device id responses.");

			// Check for errors while querying device IDs.
			lock (_locker)
			{
				if (ZaberPortError.None != _errorDuringQuery)
				{
					throw new ZaberPortErrorException(_errorDuringQuery);
				}

				if (_deviceIds.ContainsKey(0))
				{
					throw new LoopbackException(string.Format(CultureInfo.CurrentCulture,
															  "Loopback connection detected on {0}.",
															  aPort.PortName));
				}
			}

			var bootloaderModeDetected = false;
			var devices = new SortedDictionary<byte, ZaberDevice>();
			var conversations = new SortedDictionary<byte, Conversation>();

			// We can't reliably parse replies if there are collisions.
			if (!IsCollisionDetected)
			{
				// If all is well, populate device and conversation dictionaries.
				_log.Debug("Creating devices and conversations.");
				lock (_locker)
				{
					foreach (var entry in _deviceIds)
					{
						var device = new ZaberDevice
						{
							DeviceNumber = entry.Key,
							Port = aPort,
							IsInBootloaderMode = _devicesInBootloaderMode.Contains(entry.Key),
							DeviceType = new DeviceType
							{
								DeviceId = entry.Value,
								Commands = new List<CommandInfo>()
							}
						};

						bootloaderModeDetected |= device.IsInBootloaderMode;

						var conversation = new Conversation(device);

						devices[entry.Key] = device;
						conversations[entry.Key] = conversation;
					}

					// We use _deviceIds' nullity to signal to the receiving thread
					// whether to treat incoming data as a device ID. Set it to
					// null before sending out any more messages.
					_deviceIds = null;
					_devicesInBootloaderMode = null;
				}

				_log.Debug("Requesting firmware versions.");
				var versionResponses = EnumerateAsciiRequests(conversations.Values, "get version");
				foreach (var response in versionResponses.Where(r => null != r))
				{
					var device = devices[response.DeviceNumber];
					var versionPieces = response.TextData.Split(new[] { '.' }, 2);
					var versionText = versionPieces.Length > 1 ? versionPieces[0] + versionPieces[1] : versionPieces[0];
					var firmwareVersion =
						new FirmwareVersion(Convert.ToInt32(versionText, CultureInfo.InvariantCulture));

					// Get access level from devices that support it.
					if ((firmwareVersion >= new FirmwareVersion(6, 6)) && !device.IsInBootloaderMode)
					{
						var conversation = conversations[response.DeviceNumber];

						try
						{
							var accessLevel = conversation.Request("get system.access");
							device.AccessLevel = (int) accessLevel.NumericData;
						}
						catch (ErrorResponseException)
						{
						}
					}

					// Get serial number from devices that support it.
					if ((firmwareVersion >= new FirmwareVersion(6, 15)) && !device.IsInBootloaderMode)
					{
						var conversation = conversations[response.DeviceNumber];

						try
						{
							var serialNo = conversation.Request("get system.serial");
							var data = uint.Parse(serialNo.TextData, CultureInfo.InvariantCulture);
							if ((0 != data) && (uint.MaxValue != data))
							{
								device.SerialNumber = data;
							}
						}
						catch (ErrorResponseException)
						{
						}
					}

					if (firmwareVersion.Minor >= 95)
					{
						// Minor versions 95 and above are reserved for special 
						// builds. Minor version 98 is for ENG parts,
						// and 99 is for special internal dev builds.
						// Ask for a build number to determine precisely
						// what version of firmware is present.
						var conversation = conversations[response.DeviceNumber];
						try
						{
							var buildNumber = (int) conversation.Request("get version.build").NumericData;

							// If we did not get an ErrorResponseException from
							// our Request, then we have a build number!
							device.DeviceType.FirmwareVersion =
								new FirmwareVersion(firmwareVersion.Major, firmwareVersion.Minor, buildNumber);
							continue;
						}
						catch (ErrorResponseException)
						{
							// Many devices predate the "version.build" setting.
							// If we get an error response, then the command is 
							// unsupported. Leave the build number at 0 as fallback.
						}
					}

					device.DeviceType.FirmwareVersion = firmwareVersion;
				}

				// Now that we have both the device ID and firmware version, we
				// have a fully unique ID with which to determine the device's
				// supported commands. Get them from the DeviceTypeMap, if it has
				// been filled.
				var unfoundDevices = new SortedDictionary<byte, Conversation>();
				foreach (var device in devices.Values)
				{
					var deviceType = device.DeviceType;
					var key = new Tuple<int, FirmwareVersion>(deviceType.DeviceId, deviceType.FirmwareVersion);
					if (DeviceTypeMap.ContainsKey(key))
					{
						device.DeviceType = DeviceTypeMap[key];
					}
					else
					{
						_log.WarnFormat("Could not find an entry in the "
									+ "device type map for device {0} with firmware {1}.",
										deviceType.DeviceId,
										deviceType.FirmwareVersion);

						unfoundDevices.Add(device.DeviceNumber, conversations[device.DeviceNumber]);
					}
				}

				// For each device we can't find in the dictionary, 
				// ask it directly which commands it supports.
				GetTextCommands(unfoundDevices, aQueryTimeoutTimer);

				// We need to set up the ZaberDevice's axes before we set the
				// peripheral IDs for each axis.
				if (bootloaderModeDetected)
				{
					_log.Debug("Skipping axis detection because a bootloader was detected");
				}
				else
				{
					_log.Debug("Requesting axis count.");
					var axesResponses = EnumerateAsciiRequests(conversations.Values, "get system.axiscount");
					foreach (var response in axesResponses.Where(r => null != r))
					{
						var conversation = conversations[response.DeviceNumber];
						var device = conversation.Device;

						for (var i = 0; i < (int) response.NumericData; i++)
						{
							var axisDevice = new ZaberDevice
							{
								DeviceNumber = device.DeviceNumber,
								AxisNumber = i + 1,
								AccessLevel = device.AccessLevel,
								DeviceType = new DeviceType(device.DeviceType),
								Port = device.Port
							};

							device.AddAxis(axisDevice);

							var axisConversation = new Conversation(axisDevice);
							conversation.AddAxis(axisConversation);
						}
					}

					// Now that we have the axes set up, we can set their 
					// peripheral IDs and microstep resolutions.
					_log.Debug("Requesting peripheral IDs.");
					var peripheralResponses = EnumerateAsciiRequests(conversations.Values, "get peripheralid");
					foreach (var response in peripheralResponses.Where(r => null != r))
					{
						var device = devices[response.DeviceNumber];

						int count;
						if (response.NumericValues.Count != device.Axes.Count)
						{
							_log.WarnFormat("Number of data values in response "
										+ "does not match number of axes. Number of data "
										+ "values: {0}. Number of axes: {1}. Reply "
										+ "received: {2}.",
											response.NumericValues.Count,
											device.Axes.Count,
											response.Text);

							count = Math.Min(response.NumericValues.Count, device.Axes.Count);
						}
						else
						{
							count = response.NumericValues.Count;
						}

						for (var i = 0; i < count; i++)
						{
							// Check the parent DeviceType's "Peripherals" list for
							// this peripheral.
							var periId = (int) response.NumericValues[i];
							if (device.DeviceType.PeripheralMap.ContainsKey(periId))
							{
								// Found it!
								device.Axes[i].DeviceType = device.DeviceType.PeripheralMap[periId];

								// Add the peripheral's commands to the controller.

								var commandsList = new List<CommandInfo>(device.DeviceType.Commands);

								var seenCommands = new HashSet<Tuple<Command, string>>(
									commandsList.Select(
										cmd => new Tuple<Command, string>(cmd.Command, cmd.TextCommand)));

								foreach (var command in device.Axes[i].DeviceType.Commands)
								{
									var cmdKey = new Tuple<Command, string>(command.Command, command.TextCommand);
									if (!seenCommands.Contains(cmdKey))
									{
										seenCommands.Add(cmdKey);

										var parentCmd = command.Clone(); // To avoid clearing the axis's command units.

										// Strip the UOM metadata from the command
										// before giving it to the parent, if the
										// parent is a multi-axis controller.
										if (count > 1)
										{
											parentCmd.RequestUnit = null;
											parentCmd.RequestUnitScale = null;
											parentCmd.ResponseUnit = null;
											parentCmd.ResponseUnitScale = null;

											if (parentCmd is ASCIICommandInfo)
											{
												// Strip the UOM metadata from the parameter nodes as well.
												foreach (var node in (parentCmd as ASCIICommandInfo).Nodes.Where(node => node.IsParameter))
												{
													node.RequestUnit = null;
													node.RequestUnitScale = null;
												}
											}
										}

										commandsList.Add(parentCmd);
									}
								}

								device.DeviceType.Commands = commandsList.AsReadOnly();
							}
							else
							{
								// Use safe mode peripheral if possible; make a copy since we'll be modifying it below.
								var axis = device.Axes[i].DeviceType;
								if (device.DeviceType.PeripheralMap.ContainsKey(0))
								{
									axis = new DeviceType(device.DeviceType.PeripheralMap[0]);
								}
								else
								{
									axis.Name = "+ Peripheral ID " + periId;
								}

								device.Axes[i].DeviceType = axis;

								// Since we didn't find the axis' type in the map
								// nor in the parent device's list, we must convert
								// the axis' type into an axis type manually.
								axis.PeripheralId = periId;

								// Only keep axis commands in the axis' list.
								axis.Commands = axis.Commands.Where(c => c.IsAxisCommand).ToList();

								// A peripheral can't have peripherals.
								axis.PeripheralMap = null;

								_log.WarnFormat("Could not find an entry in the "
											+ "peripherals map for peripheral ID {0} "
											+ "connected to {1}.",
												axis.PeripheralId,
												device.DeviceType);
							}

							device.IsController = true;
							device.Axes[i].IsPeripheral = true;
						}
					}

					_log.Debug("Requesting microstep resolution.");
					var resolutionResponses = EnumerateAsciiRequests(conversations.Values, "get resolution");
					foreach (var response in resolutionResponses.Where(r => null != r))
					{
						var device = devices[response.DeviceNumber];

						int count;
						if (response.NumericValues.Count != device.Axes.Count)
						{
							_log.WarnFormat("Number of data values in response "
										+ "does not match number of axes. Number of data "
										+ "values: {0}. Number of axes: {1}. Reply "
										+ "received: {2}.",
											response.NumericValues.Count,
											device.Axes.Count,
											response.Text);

							count = Math.Min(response.NumericValues.Count, device.Axes.Count);
						}
						else
						{
							count = response.NumericValues.Count;
						}

						if (1 == count)
						{
							device.MicrostepResolution = (int) response.NumericValues[0];
							device.Axes[0].MicrostepResolution = (int) response.NumericValues[0];
						}
						else
						{
							device.MicrostepResolution = 0;
							for (var i = 0; i < count; i++)
							{
								device.Axes[i].MicrostepResolution = (int) (response.NumericValues[i] ?? 0);
							}
						}
					}

					foreach (var device in devices.Values)
					{
						device.IsIntegrated = !device.IsController
											&& !device.IsPeripheral
											&& (device.DeviceType.MotionType != MotionType.None);
					}
				}
			}

			lock (_locker)
			{
				_currentState = ZaberPortFacadeState.Open;
				_areMessageIdsMonitored = true;

				// Set up the "All" device and conversation.
				var allDevices = (DeviceCollection) _devices[0];
				foreach (var device in devices.Values)
				{
					allDevices.Add(device);
				}

				devices[0] = allDevices;
				var allConversations = (ConversationCollection) _conversations[0];

				foreach (var conversation in conversations.Values)
				{
					allConversations.Add(conversation);
				}

				_selectedConversation = conversations[0] = allConversations;

				// Save our temporary device and conversation lists.
				_devices = devices;
				_conversations = conversations;
			}
		}


		/// <summary>
		///     Query connected devices using the binary protocol.
		/// </summary>
		/// <param name="aPort">Serial port to use.</param>
		/// <param name="aQueryTimeoutTimer">Used to wait for responses.</param>
		/// <exception cref="ZaberPortErrorException">
		///     An error occurred while
		///     querying devices.
		/// </exception>
		/// <exception cref="LoopbackException">
		///     A loopback connection was
		///     detected.
		/// </exception>
		private void QueryBinaryDevices(IZaberPort aPort, TimeoutTimer aQueryTimeoutTimer)
		{
			/* Get the ID of each device first. The _deviceIds dictionary
			 * collects responses from the receiving thread. */
			aPort.Send(0, Command.ReturnDeviceID);

			_log.Debug("Waiting for device id responses.");
			aQueryTimeoutTimer.Sleep();
			_log.Debug("Finished waiting for device id responses.");

			// Check for errors while querying device IDs.
			lock (_locker)
			{
				if (ZaberPortError.None != _errorDuringQuery)
				{
					throw new ZaberPortErrorException(_errorDuringQuery);
				}

				if (_deviceIds.ContainsKey(0))
				{
					throw new LoopbackException(string.Format(CultureInfo.CurrentCulture,
															  "Loopback connection detected on {0}.",
															  aPort.PortName));
				}
			}

			// If all is well, populate device and conversation dictionaries.
			_log.Debug("Creating devices and conversations.");
			var devices = new SortedDictionary<byte, ZaberDevice>();
			var conversations = new SortedDictionary<byte, Conversation>();
			lock (_locker)
			{
				foreach (var entry in _deviceIds)
				{
					var device = new ZaberDevice
					{
						DeviceNumber = entry.Key,
						Port = aPort,
						DeviceType = new DeviceType
						{
							DeviceId = entry.Value,
							Commands = new List<CommandInfo>()
						}
					};

					var conversation = new Conversation(device);

					devices[entry.Key] = device;
					conversations[entry.Key] = conversation;
				}
			}

			_log.Debug("Requesting firmware versions.");
			foreach (var response in EnumerateBinaryRequests(conversations.Values, Command.ReturnFirmwareVersion, 0)
			.Where(r => null != r))
			{
				var device = devices[response.DeviceNumber];
				var fwVersion = new FirmwareVersion((int) response.NumericData);

				// Get serial number from devices that support it.
				if (fwVersion >= new FirmwareVersion(6, 07))
				{
					var conversation = conversations[response.DeviceNumber];

					try
					{
						var serialNo = conversation.Request(Command.ReturnSerialNumber);
						var data = (uint) (int) serialNo.NumericData;
						if ((0 != data) && (uint.MaxValue != data))
						{
							device.SerialNumber = data;
						}
					}
					catch (ErrorResponseException)
					{
					}
				}

				if (fwVersion.Minor >= 95)
				{
					// Minor versions 95 and above are reserved for special 
					// builds. Minor version 98 is for ENG parts,
					// and 99 is for special internal dev builds.
					// Ask for a build number to determine precisely
					// what version of firmware is present.
					var conversation = conversations[response.DeviceNumber];
					try
					{
						var buildNumber = conversation.Request(Command.ReturnFirmwareBuild);

						// If we did not get an ErrorResponseException from
						// our Request, then we have a build number!
						fwVersion = new FirmwareVersion(fwVersion.Major,
														fwVersion.Minor,
														(int) buildNumber.NumericData);
					}
					catch (ErrorResponseException)
					{
						// Many devices predate the "version.build" setting.
						// If we get an error response, then the command is 
						// unsupported. Leave the build number at 0 as fallback.
					}
				}

				device.DeviceType.FirmwareVersion = fwVersion;
			}

			// Now that we have both the device ID and firmware version, we 
			// have a fully unique ID with which to determine the device's 
			// supported commands. Get them from the DeviceTypeMap, if it has
			// been filled. Devices which are actually peripherals will later
			// be replaced by the correct DeviceType later on, when we query
			// peripheral IDs.
			foreach (var device in devices.Values)
			{
				var deviceType = device.DeviceType;
				var key = new Tuple<int, FirmwareVersion>(deviceType.DeviceId, deviceType.FirmwareVersion);
				if (DeviceTypeMap.ContainsKey(key))
				{
					device.DeviceType = DeviceTypeMap[key];
				}
				else
				{
					_log.WarnFormat("Could not find an entry in the "
								+ "device type map for device {0} with firmware {1}.",
									deviceType.DeviceId,
									deviceType.FirmwareVersion);
				}
			}

			// It is important that we ask for alias numbers *after* setting 
			// each device's DeviceType, as the DeviceCollection class assumes
			// that the DeviceType of each of its members has already been set
			// when they are added. 
			_log.Debug("Requesting alias numbers.");
			GetAliases(devices, conversations);

			_log.Debug("Requesting microstep resolution.");
			foreach (var _ in EnumerateBinaryRequests(conversations.Values,
													  Command.ReturnSetting,
													  (int) Command.SetMicrostepResolution))
			{
				// Do nothing: the receive thread automatically sets the
				// microstep resolution as responses come in.
			}

			_log.Debug("Requesting peripheral IDs.");

			// Get peripheral IDs, if the device's firmware is new enough.
			var controllerConversations = conversations.Values.Where(c => c.Device.FirmwareVersion >= 603);
			var peripheralResponses = EnumerateBinaryRequests(controllerConversations, 
															  Command.ReturnSetting, 
															  (int)Command.SetPeripheralID);

			foreach (var response in peripheralResponses.Where(
				r => (null != r) && (Command.SetPeripheralID == r.Command)))
			{
				var device = devices[response.DeviceNumber];
				var peripheralId = (int) response.NumericData;

				// If the PeripheralTypeMap has been populated, grab the
				// DeviceType for the peripheral from there.
				var deviceType = device.DeviceType;
				if (device.DeviceType.PeripheralMap.ContainsKey(peripheralId))
				{
					// This works differently in Binary than in ASCII,
					// since Binary has no notion of axes. The whole
					// device becomes the peripheral.

					// Add the peripheral's commands to the controller.
					var commandsList = new List<CommandInfo>(device.DeviceType.Commands.Select(cmd =>
					{
						// Since we're merging controller and peripheral in binary mode, mark all commands
						// as axis so they don't get filtered out by DeviceType.Commands.get() during display.
						var cmdCopy = cmd.Clone();
						cmdCopy.IsAxisCommand = true;
						return cmdCopy;
					}));

					var seenCommands = new HashSet<Tuple<Command, string>>(
						commandsList.Select(cmd => new Tuple<Command, string>(cmd.Command, cmd.TextCommand)));

					foreach (var command in device.DeviceType.PeripheralMap[peripheralId].Commands)
					{
						var cmdKey = new Tuple<Command, string>(command.Command, command.TextCommand);
						if (!seenCommands.Contains(cmdKey))
						{
							seenCommands.Add(cmdKey);
							commandsList.Add(command);
						}
					}

					device.DeviceType = device.DeviceType.PeripheralMap[peripheralId];
					device.DeviceType.Commands = commandsList.AsReadOnly();
				}
				else
				{
					_log.WarnFormat("Could not find an entry in the "
								+ "peripherals map for peripheral {0} "
								+ "connected to {1} ({2}).",
									peripheralId,
									string.Format("device #{0}, type {1}, FW {2}",
												  device.DeviceNumber,
												  deviceType.DeviceId,
												  deviceType.FirmwareVersion),
									deviceType);

					device.DeviceType.PeripheralId = peripheralId;
				}

				device.IsController = true;
				device.IsPeripheral = true;
			}

			foreach (var device in devices.Values)
			{
				device.IsIntegrated = !device.IsController 
									&& !device.IsPeripheral 
									&& (device.DeviceType.MotionType != MotionType.None);
			}

			_log.Debug("Requesting device modes.");
			var areMessageIdsEnabled = DetermineMessageIdMode(conversations);

			lock (_locker)
			{
				_currentState = ZaberPortFacadeState.Open;
				_deviceIds = null;
				_devicesInBootloaderMode = null;
				_areMessageIdsEnabled = areMessageIdsEnabled;
				_areMessageIdsMonitored = true;
				var allDevices = (DeviceCollection) _devices[0];
				allDevices.AreMessageIdsEnabled = _areMessageIdsEnabled;
				foreach (var device in devices.Values)
				{
					allDevices.Add(device);
				}

				devices[0] = allDevices;
				var allConversations = (ConversationCollection) _conversations[0];
				foreach (var conversation in conversations.Values)
				{
					allConversations.Add(conversation);
				}

				_selectedConversation = conversations[0] = allConversations;
				_devices = devices;
				_conversations = conversations;
			}
		}


		private bool ShouldInvalidate(DataPacketEventArgs aArgs)
		{
			lock (_locker)
			{
				// Invalidation in ASCII is done through the 
				// "_shouldInvalidateNextResponse" flag and some regex 
				// pattern-matching to make sure each device responds.
				if (_shouldInvalidateNextResponse)
				{
					foreach (var pattern in _invalidateNextResponsePatterns)
					{
						if (Regex.IsMatch(aArgs.DataPacket.Text, pattern))
						{
							_log.Info($"Received expected post-invalidation response '{aArgs.DataPacket.Text}'.");
							_invalidateNextResponsePatterns.Remove(pattern);
							break;
						}
					}

					// If the device rejected a command that could invalidate the port,
					// we should clear expectations for remaining responses and skip
					// invalidation - the device should not have changed its state.
					if (aArgs.DataPacket.IsError)
					{
						_invalidateNextResponsePatterns.Clear();
						_shouldInvalidateNextResponse = false;
						return false;
					}

					if (0 == _invalidateNextResponsePatterns.Count)
					{
						_log.Info("Invalidating port now because all expected responses have been received.");
						_shouldWaitBeforeReopening = true;
						_shouldInvalidateNextResponse = false;
						return true;
					}
				}

				// No device exists with the number in the data packet we just received.
				// This happens when a "renumber" command has been sent out, 
				// and the device is replying with its new number instead of its old.
				if (!_devices.ContainsKey(aArgs.DataPacket.DeviceNumber))
				{
					// Joysticks with "joy.debug" set to 1 will echo back all
					// commands they are sending forward. This can trigger a
					// false positive here. Don't invalidate if this is the case.
					if (MessageType.Request == aArgs.DataPacket.MessageType)
					{
						return false;
					}

					// The device numbers have changed: invalidation patterns are incorrect. Trash them.
					_log.Info("Invalidating port now because a new device number was detected.");
					_invalidateNextResponsePatterns.Clear();
					_shouldWaitBeforeReopening = true;
					return true;
				}

				// From here on down all has to do with Binary replies.
				if (_shouldInvalidateNextResponseBinary)
				{
					_shouldInvalidateNextResponseBinary = false;

					if (aArgs.DataPacket.Command == Command.Error)
					{
						return false;
					}

					_log.Info(
						$"Invalidating port now because a reply to command {aArgs.DataPacket.Command} was received.");
					_shouldWaitBeforeReopening = true;
					return true;
				}

				// The rest of these checks are for alias commands.
				if (Command.SetAliasNumber != aArgs.DataPacket.Command)
				{
					// not an alias command!
					return false;
				}

				if (!_devices.ContainsKey(aArgs.DataPacket.DeviceNumber))
				{
					_log.Info("Invalidating port now because a new binary device number was detected.");

					// new device, must be new alias, too
					return true;
				}

				// have to do this in case there's a message id in there.
				var aliasNumberAsInt = (int) _devices[0].CreateDeviceMessage(aArgs.DataPacket).NumericData;
				if (!IsValidByteValue(aliasNumberAsInt))
				{
					// invalid alias number, ignore it.
					return false;
				}

				var aliasNumber = (byte) aliasNumberAsInt;
				if (!_devices.ContainsKey(aliasNumber))
				{
					// don't have that alias in the collection, must be new
					_log.Info("Invalidating port now because a new binary alias was detected (case 1).");
					return true;
				}

				if (!(_devices[aliasNumber] is DeviceCollection alias))
				{
					// There's a single device in that slot already. Ignore the alias.
					return false;
				}

				var device = _devices[aArgs.DataPacket.DeviceNumber];
				if (0 != aliasNumber)
				{
					// It's only a new alias if the alias collection 
					// doesn't already have it.
					if (!alias.Contains(device))
					{
						_log.Info("Invalidating port now because a new binary alias was detected (case 2).");
						return true;
					}

					return false;
				}

				// new alias is 0 (all devices) so we have to
				// check if the old alias is still out there
				foreach (var otherDevice in _devices.Values)
				{
					if (otherDevice.Equals(alias) || otherDevice.Equals(device))
					{
						continue;
					}

					if (otherDevice is DeviceCollection oldAlias && oldAlias.Contains(device))
					{
						// There is another alias that contains this device.
						// We need to reset.
						_log.Info("Invalidating port now because an alias has changed.");
						return true;
					}
				}

				// didn't find another alias that contains this device.
				// We don't need to reset.
				return false;
			}
		}

		#endregion

		#region -- Public Properties --

		/// <summary>
		///     Gets a list of all conversations for all the devices,
		///     their alias numbers, and individual axes.
		/// </summary>
		/// <remarks>
		///     This collection includes conversations of all attached devices,
		///     their alias conversations (if the alias number is non-zero and does not collide with
		///     an existent conversation), their axis conversations, and the "All Device" conversation
		///     (<see cref="ZaberDevice.DeviceNumber" /> 0).
		///     This collection include individual axes
		///     (non-zero <see cref="ZaberDevice.AxisNumber" />). Also see <see cref="Conversations" />.
		/// </remarks>
		public IEnumerable<Conversation> AllConversations
		{
			get
			{
				foreach (var conversation in _conversations.Values)
				{
					yield return conversation;
					foreach (var axis in conversation.Axes)
					{
						yield return axis;
					}
				}
			}
		}


		/// <summary>
		///     Get or set a flag that will raise a port error for an invalid
		///     packet if a response packet has an unknown device number.
		/// </summary>
		public bool AreDeviceNumbersValidated { get; set; }


		/// <summary>
		///     Enables or disables message ids mode on all devices attached
		///     to the port.
		/// </summary>
		/// <remarks>
		///     <para>
		///         Message ids mode uses id numbers to match each response to
		///         the request that triggered it. Message ids are also known as
		///         logical channels.
		///     </para>
		///     <para>
		///         The Zaber port facade forces you to keep the message ids
		///         mode the same on all devices attached to the port, either all on
		///         or all off. This avoids major confusion after a renumber request.
		///         Setting this property will trigger a "Return Setting" command
		///         for the device mode. The Zaber port facade will automatically send
		///         "Set Device Mode" commands to switch any devices that don't match
		///         the requested setting. It will continue to do this whenever it
		///         sees a "Set Device Mode" response with the wrong setting of
		///         message ids mode.
		///     </para>
		///     <para>
		///         A device may reject an attempt to change its message ID mode
		///         if it is busy at the time, so it is possible to have a mixture of
		///         enabled and disabled on the same chain; reading this property does not
		///         reveal if this is the case. The way to be sure is to check the
		///         AreMessageIdsEnabled property on each device.
		///     </para>
		/// </remarks>
		public bool AreMessageIdsEnabled
		{
			get
			{
				lock (_locker)
				{
					return _areMessageIdsEnabled;
				}
			}
			set
			{
				// ASCII always supports message IDs without a device mode change, so just enable them in software.
				if (Port.IsAsciiMode)
				{
					lock (_locker)
					{
						foreach (var conversation in AllConversations)
						{
							conversation.Device.AreAsciiMessageIdsEnabled = value;
						}

						_areMessageIdsEnabled = value;
					}

					return;
				}

				// Binary - first turn off message ID interpretation on software reception.
				var conversations = new List<Conversation>();
				lock (_locker)
				{
					_areMessageIdsMonitored = false;
					conversations.AddRange(_conversations.Values);
					foreach (var conversation in conversations)
					{
						conversation.Device.AreMessageIdsEnabled = false;
					}
				}

				// Now try to change the mode on each device.
				foreach (var conversation in conversations.Where(c => c.Device.IsSingleDevice))
				{
					var topic1 = conversation.StartTopic();

					if (conversation.Device.FirmwareVersion < 600)
					{
						conversation.Device.Send(Command.ReturnSetting, (int) Command.SetDeviceMode, topic1.MessageId);
						if (!topic1.Wait(QueryTimeoutTimer))
						{
							throw new RequestTimeoutException();
						}

						topic1.Validate();
						var oldMode = (DeviceModes) (int) topic1.Response.NumericData;
						var newMode = oldMode | DeviceModes.EnableMessageIdsMode;

						if (!value)
						{
							newMode ^= DeviceModes.EnableMessageIdsMode;
						}

						if (newMode != oldMode)
						{
							var topic2 = conversation.StartTopic();
							conversation.Device.Send(Command.SetDeviceMode, (int)newMode, topic2.MessageId);
							if (!topic2.Wait(QueryTimeoutTimer))
							{
								throw new RequestTimeoutException();
							}

							topic2.Validate();
						}
					}
					else
					{
						conversation.Device.Send(Command.ReturnSetting,
												 (int)Command.SetMessageIDMode,
												 topic1.MessageId);
						if (!topic1.Wait(QueryTimeoutTimer))
						{
							throw new RequestTimeoutException();
						}

						topic1.Validate();

						var oldMode = 1m == topic1.Response.NumericData;
						if (oldMode != value)
						{
							var topic2 = conversation.StartTopic();
							conversation.Device.Send(Command.SetMessageIDMode, value ? 1 : 0, topic2.MessageId);
							if (!topic2.Wait(QueryTimeoutTimer))
							{
								throw new RequestTimeoutException();
							}

							topic2.Validate();
						}
					}
				}

				lock (_locker)
				{
					foreach (var conversation in conversations)
					{
						conversation.Device.AreMessageIdsEnabled = value;
					}

					_areMessageIdsEnabled = value;
					_areMessageIdsMonitored = true;
				}
			}
		}


		/// <summary>
		///     The baud rate of the underlying port.
		/// </summary>
		public int BaudRate
		{
			get => Port.BaudRate;
			set => Port.BaudRate = value;
		}


		/// <summary>
		///     Gets a list of all conversations for all devices and their alias numbers.
		/// </summary>
		/// <remarks>
		///     This collection includes conversations of all attached devices,
		///     their alias conversations (if the alias number is non-zero
		///     and does not collide with an existent conversation),
		///     and the "All Device" conversation (<see cref="ZaberDevice.DeviceNumber" /> 0).
		///     This collection does not include individual axes
		///     (non-zero <see cref="ZaberDevice.AxisNumber" />).
		///     Also see <see cref="AllConversations" />.
		/// </remarks>
		public ICollection<Conversation> Conversations
		{
			get
			{
				lock (_locker)
				{
					var copy = new List<Conversation>();
					copy.AddRange(_conversations.Values);
					return copy;
				}
			}
		}


		/// <summary>
		///     Gets the current state of the facade.
		/// </summary>
		public ZaberPortFacadeState CurrentState
		{
			get
			{
				lock (_locker)
				{
					return _currentState;
				}
			}
		}

		/// <summary>
		///     A device type that will be used for any device ids not found in
		///     <see cref="DeviceTypeMap" />. Effectively, this is just used to
		///     specify a generic set of commands and settings.
		/// </summary>
		public DeviceType DefaultDeviceType
		{
			get
			{
				lock (_locker)
				{
					return _defaultDeviceType;
				}
			}
			set
			{
				lock (_locker)
				{
					_defaultDeviceType = value;

					_devices[0].DeviceType = new DeviceType(_defaultDeviceType) { Name = "All Devices" };
				}
			}
		}


		/// <summary>
		///     A collection of all the devices attached to the port.
		/// </summary>
		public ICollection<ZaberDevice> Devices
		{
			get
			{
				lock (_locker)
				{
					var copy = new List<ZaberDevice>();
					copy.AddRange(_devices.Values);
					return copy;
				}
			}
		}


		/// <summary>
		///     A dictionary of the known device types, keyed by a tuple with
		///     the value (deviceID, firmware version).
		/// </summary>
		public IDictionary<Tuple<int, FirmwareVersion>, DeviceType> DeviceTypeMap
		{
			get
			{
				lock (_locker)
				{
					return _deviceTypeMap;
				}
			}
			set
			{
				lock (_locker)
				{
					_deviceTypeMap = value;
				}
			}
		}


		/// <summary>
		///     Application-provided source of device help information. Used to populate
		///     device help sources during detection.
		/// </summary>
		public IDeviceHelpProvider DeviceHelpProvider { get; set; }


		/// <summary>
		///     A list of the known device types.
		/// </summary>
		/// <remarks>
		///     Setting this property will set DeviceTypeMap to a new Dictionary
		///     instance, whose contents match those of the list specified.
		/// </remarks>
		/// <remarks>
		///     This property has been deprecated. It has been left here for
		///     backwards-compatibility, and may be removed in future versions.
		/// </remarks>
		[Obsolete("Use GetDeviceType or DeviceTypeMap.")]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public IList<DeviceType> DeviceTypes
		{
			get => DeviceTypeMap.Select(kvPair => kvPair.Value).ToList();
			set
			{
				lock (_locker)
				{
					_deviceTypeMap = new Dictionary<Tuple<int, FirmwareVersion>, DeviceType>();
					foreach (var entry in value)
					{
						var key = new Tuple<int, FirmwareVersion>(entry.DeviceId, entry.FirmwareVersion);
						_deviceTypeMap.Add(key, entry);
					}
				}
			}
		}


		/// <summary>
		///     Gets a flag that shows whether two devices responded with the
		///     same device number.
		/// </summary>
		/// <remarks>
		///     If this is true, the client should send <see cref="Command.Renumber" />
		///     to device number 0 to force all devices to choose unique device
		///     numbers.
		/// </remarks>
		public bool IsCollisionDetected { get; private set; }


		/// <summary>
		///     Gets or sets a flag that enables or disables the
		///     <see cref="Invalidated" /> event. Enabled by default.
		/// </summary>
		public bool IsInvalidateEnabled
		{
			get => _isInvalidateEnabled;
			set
			{
				if (_isInvalidateEnabled != value)
				{
					_isInvalidateEnabled = value;
					if (value && (_invalidationDeferred || _invalidateNextResponsePatterns.Any()))
					{
						_log.Info("Invalidating port upon invalidation enable because some expectations were queued.");
						_invalidateNextResponsePatterns.Clear();
						_shouldInvalidateNextResponse = false;
						_invalidationDeferred = false;
						Invalidated?.Invoke(this, null);
					}
				}
			}
		}


		/// <summary>
		///     Source of tokens that can be used to temporarily disable systems that poll devices.
		///     Call <see cref="CounterTokenSource.GetToken()"/> on this to disable polling, and then
		///     dispose the token to re-enable it. Overlapping tokens is allowed.
		///     Systems that poll devices should subscribe to <see cref="CounterTokenSource.ZeroStateChanged"/>
		///     and poll only when the state is zero.
		/// </summary>
		public CounterTokenSource PollingDisableTokenSource { get; } = new CounterTokenSource();


		/// <summary>
		///     Clear any pending port close/reopen invalidations that were queued while invalidation
		///     was disabled. Call this just before setting <see cref="IsInvalidateEnabled" /> back to true
		///     if you want to prevent a close and reopen of the port and are certain it's safe to skip.
		///     This exists because reading joystick settings can trigger invalidation.
		/// </summary>
		public void ClearDeferredInvalidation() => _invalidationDeferred = false;


		/// <summary>
		///     Raises the port invalidated event, if invalidation is enabled.
		/// </summary>
		public void Invalidate()
		{
			if (IsInvalidateEnabled)
			{
				_log.Info("Firing port invalidation event.");
				Invalidated?.Invoke(this, EventArgs.Empty);
			}
			else
			{
				_log.Info("Port invalidation is currently disabled.");
			}
		}


		/// <summary>
		///     Gets or sets the port that this facade is attached to.
		/// </summary>
		public IZaberPort Port
		{
			get
			{
				lock (_locker)
				{
					return _port;
				}
			}
			set
			{
				lock (_locker)
				{
					if (null != _port)
					{
						_port.DataPacketReceived -= Port_DataPacketReceived;
						_port.DataPacketSent -= Port_DataPacketSent;
						_port.ErrorReceived -= Port_ErrorReceived;
						_port.ClosedUnexpectedly -= Port_ClosedUnexpectedly;
					}

					_port = value;

					if (null != _port)
					{
						_port.DataPacketReceived += Port_DataPacketReceived;
						_port.DataPacketSent += Port_DataPacketSent;
						_port.ErrorReceived += Port_ErrorReceived;
						_port.ClosedUnexpectedly += Port_ClosedUnexpectedly;
					}

					_devices[0].Port = _port;
				}
			}
		}


		/// <summary>
		///     Gets or sets the time to wait in milliseconds for responses from
		///     all devices.
		/// </summary>
		public int QueryTimeout
		{
			get
			{
				lock (_locker)
				{
					return _queryTimeoutTimer.Timeout;
				}
			}
			set
			{
				lock (_locker)
				{
					_queryTimeoutTimer.Timeout = value;
				}
			}
		}


		/// <summary>
		///     Gets or sets a timeout timer used to time how long we wait for
		///     responses from all devices.
		/// </summary>
		public TimeoutTimer QueryTimeoutTimer
		{
			get
			{
				lock (_locker)
				{
					return _queryTimeoutTimer;
				}
			}
			set
			{
				lock (_locker)
				{
					_queryTimeoutTimer = value;
				}
			}
		}


		/// <summary>
		///     Get or set the currently selected conversation.
		/// </summary>
		/// <remarks>
		///     The meaning of this property is entirely up to the client program.
		///     However, whenever this property changes, it will raise the
		///     <see cref="SelectedConversationChanged" /> event. Every time the
		///     port facade is opened or closed, this is set to the conversation
		///     for device number 0, or all devices.
		/// </remarks>
		public Conversation SelectedConversation
		{
			get
			{
				lock (_locker)
				{
					return _selectedConversation;
				}
			}
			set
			{
				bool isChanged;

				lock (_locker)
				{
					isChanged = value != _selectedConversation;
					if (isChanged)
					{
						_selectedConversation = value;
					}
				}

				if (isChanged && (null != SelectedConversationChanged) && !SuppressEvents)
				{
					SelectedConversationChanged(this, EventArgs.Empty);
				}
			}
		}


		/// <summary>
		///     If set to true, no events will be fired by this ZaberPortFacade
		///     instance.
		/// </summary>
		public bool SuppressEvents { get; set; }

		#endregion

		#region -- Event Handlers --

		private void Port_DataPacketSent(object aSender, DataPacketEventArgs aArgs)
		{
			lock (_locker)
			{
				var text = aArgs.DataPacket.Text;
				if (_port.IsAsciiMode
				&& (ZaberPortFacadeState.QueryingDevices != _currentState)
				&& (null != text)
				&& !_isInvalidated)
				{
					// These patterns can result in the device's number
					// being changed. Expect responses from any device number.
					if (Regex.IsMatch(text, @"^(?!.*key\s).*(renumber|system\s+factoryreset|set\s+comm.address)"))
					{
						_shouldInvalidateNextResponse = true;
						_log.Info($"Queueing a port invalidation because command '{text}' was sent.");

						if (0 == aArgs.DataPacket.DeviceNumber)
						{
							foreach (var _ in Devices.Where(d => 0 != d.DeviceNumber))
							{
								_invalidateNextResponsePatterns.Add(
									"@[0-9][0-9] 0 ([0-9][0-9] )?OK (IDLE|BUSY) (--|[A-Z][A-Z]) .+");
							}
						}
						else
						{
							_invalidateNextResponsePatterns.Add(
								"@[0-9][0-9] 0 ([0-9][0-9] )?OK (IDLE|BUSY) (--|[A-Z][A-Z]) .+");
						}
					}

					// These patterns won't change a device's number.
					// Expect responses from the same device numbers.
					else if (Regex.IsMatch(text, @"^(?!.*key\s).*(system\s+restore|set\s+comm.protocol|set\s+comm.rs*|set\s+comm.usb*|set\s+deviceid|set\s+device.id|set\s+peripheralid|set\s+peripheral.id|set\s+system.access|tools\s+setcomm|activate)"))
					{
						_shouldInvalidateNextResponse = true;
						_log.Info($"Queueing a port invalidation because command '{text}' was sent.");

						if (0 == aArgs.DataPacket.DeviceNumber)
						{
							foreach (var device in Devices.Where(device => 0 != device.DeviceNumber))
							{
								_invalidateNextResponsePatterns.Add(
									$"@{device.DeviceNumber:00} {device.AxisNumber} ([0-9][0-9] )?OK (IDLE|BUSY) (--|[A-Z][A-Z]) .+");
							}
						}
						else
						{
							_invalidateNextResponsePatterns.Add(
								$"@{aArgs.DataPacket.DeviceNumber:00} {aArgs.DataPacket.AxisNumber} ([0-9][0-9] )?OK (IDLE|BUSY) (--|[A-Z][A-Z]) .+");
						}
					}

					// else do nothing: the command isn't invalidating.
				}

				if (!_port.IsAsciiMode
				&& (ZaberPortFacadeState.QueryingDevices != _currentState)
				&& !_isInvalidated)
				{
					var isChangingBaudRate = (aArgs.DataPacket.Command == Command.SetBaudRate)
										 && (aArgs.DataPacket.Measurement?.Value != BaudRate);

					var isOtherInvalidatingCommand = (Command.Renumber == aArgs.DataPacket.Command)
												 || (Command.SetPeripheralID == aArgs.DataPacket.Command)
												 || (Command.RestoreSettings == aArgs.DataPacket.Command)
												 || (Command.ConvertToASCII == aArgs.DataPacket.Command);

					if (isChangingBaudRate || isOtherInvalidatingCommand)
					{
						_shouldInvalidateNextResponseBinary = true;
					}
				}
			}
		}


		private void Port_DataPacketReceived(object aSender, DataPacketEventArgs aArgs)
		{
			_log.Debug("Receiving.");
			var shouldInvalidate = false;
			lock (_locker)
			{
				_log.Debug("Receiving...");
				switch (_currentState)
				{
					case ZaberPortFacadeState.Closed:
						_log.Debug("Closed.");
						return;

					case ZaberPortFacadeState.QueryingDevices:
						var isDeviceId = (Command.ReturnDeviceID == aArgs.DataPacket.Command)
									 || (MessageType.Response == aArgs.DataPacket.MessageType);
						if ((null != _deviceIds) && isDeviceId)
						{
							var deviceNumber = aArgs.DataPacket.DeviceNumber;
							if (!_deviceIds.ContainsKey(deviceNumber))
							{
								// FW7 increased device ID size to 32 bits and reports uint32.max if the value has not been set.
								// Since int32 device IDs are very entrenched in Zaber Console at present, convert to signed
								// and truncate to 32 bits - this will report a device ID of -1 in the unconfigured case.
								if (aArgs.DataPacket.NumericData < 0m)
								{
									_deviceIds[aArgs.DataPacket.DeviceNumber] = (int) aArgs.DataPacket.NumericData;
								}
								else
								{
									_deviceIds[aArgs.DataPacket.DeviceNumber] =
										(int) (uint) Math.Min(aArgs.DataPacket.NumericData, uint.MaxValue);
								}

								if ((aArgs.DataPacket.FlagText ?? string.Empty).Contains(WarningFlags.BootloaderMode))
								{
									_devicesInBootloaderMode.Add(aArgs.DataPacket.DeviceNumber);
								}
							}
							else
							{
								IsCollisionDetected = true;
							}
						}
						else if ((Command.Error == aArgs.DataPacket.Command)
							 && (((int) ZaberError.VoltageLow == (int) aArgs.DataPacket.NumericData)
							 || ((int) ZaberError.VoltageHigh == (int) aArgs.DataPacket.NumericData)))
						{
							_isVoltageBadDuringQuery = true;
						}

						break;

					case ZaberPortFacadeState.Open:
						if (Command.SetDeviceMode == aArgs.DataPacket.Command)
						{
							CheckMessageIdsMode(aArgs.DataPacket);
							return;
						}
						else if (ShouldInvalidate(aArgs) && !_isInvalidated)
						{
							if (IsInvalidateEnabled)
							{
								_isInvalidated = true;
								shouldInvalidate = true;
							}
							else
							{
								_invalidationDeferred = true;
							}
						}

						if (AreDeviceNumbersValidated
						&& !ContainsDevice(aArgs.DataPacket.DeviceNumber)
						&& !_isInvalidated)
						{
							Port.ReportInvalidPacket();
						}

						break;
				}
			}

			_log.Debug("Received.");

			if (IsInvalidateEnabled
			&& shouldInvalidate
			&& (null != Invalidated)
			&& !SuppressEvents)
			{
				_log.Info("Firing port invalidated event.");
				Invalidated(this, EventArgs.Empty);
			}
		}


		private void Port_ErrorReceived(object aSender, ZaberPortErrorReceivedEventArgs aArgs)
		{
			lock (_locker)
			{
				if (ZaberPortFacadeState.QueryingDevices == _currentState)
				{
					_errorDuringQuery = aArgs.ErrorType;
				}
			}
		}


		private void Port_ClosedUnexpectedly(object sender, EventArgs e)
			=> ClosedUnexpectedly?.Invoke(this, EventArgs.Empty);

		#endregion

		#region -- IZaberPort delegation methods --

		/// <summary>
		///     DEPRECATED. Get a list of all available serial ports.
		/// </summary>
		/// <returns>
		///     An array of port names, one of which should be passed to
		///     <see cref="Open" />.
		/// </returns>
		public string[] GetPortNames() => RS232Port.GetSerialPortNames();


		/// <summary>
		///     Close and release the serial port. This must be called to avoid
		///     locking the port when you are finished with it.
		/// </summary>
		/// <exception cref="IOException">
		///     The port is in an invalid state.
		/// </exception>
		public void Close()
		{
			if (!SuppressEvents)
			{
				Closing?.Invoke(this, EventArgs.Empty);
			}

			TimeoutTimer queryTimeoutTimer;
			bool shouldWait;

			lock (_locker)
			{
				_currentState = ZaberPortFacadeState.Closed;
				shouldWait = _shouldWaitBeforeReopening;
				_shouldWaitBeforeReopening = false;
				_shouldInvalidateNextResponse = false;
				_shouldInvalidateNextResponseBinary = false;
				_invalidateNextResponsePatterns.Clear();
				queryTimeoutTimer = _queryTimeoutTimer;
			}

			if (shouldWait)
			{
				_log.Debug("Waiting for devices to finish executing commands.");
				queryTimeoutTimer.Sleep();
			}

			IZaberPort port;
			_log.Debug("Closing.");
			lock (_locker)
			{
				_log.Debug("Closing...");
				var allDevices = GetDeviceCollection(0);
				var allConversations = GetConversationCollection(0);
				foreach (var conversation in allConversations)
				{
					conversation.Device.Port = null;
					if (0 != conversation.Device.DeviceNumber)
					{
						foreach (var axis in conversation.Device.Axes)
						{
							axis.Port = null;
						}

						conversation.Dispose();
					}
				}

				allDevices.Clear();
				_devices.Clear();
				_devices[0] = allDevices;

				DeviceTypeMap.Clear(); // Do not cache commands and settings across port sessions!

				_conversations.Clear();
				allConversations.Clear();
				_selectedConversation = _conversations[0] = allConversations;

				allDevices.AreMessageIdsEnabled = false;
				_areMessageIdsEnabled = false;
				_areMessageIdsMonitored = false;
				port = _port;
			}

			_log.Debug("Closing....");

			try
			{
				port.Close();
				_log.Debug("Closed.");
			}
			finally
			{
				if ((null != Closed) && !SuppressEvents)
				{
					Closed(this, EventArgs.Empty);
				}

				_log.Debug("Closed.");
			}
		}


		/// <summary>
		///     Is the port open?
		/// </summary>
		/// <remarks>
		///     This doesn't always agree with <see cref="CurrentState" />.
		///     For example, after a USB-to-serial adapter has been disconnected.
		/// </remarks>
		public bool IsOpen
		{
			get
			{
				IZaberPort port;
				lock (_locker)
				{
					port = _port;
				}

				return port.IsOpen;
			}
		}

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private readonly object _locker = new object();
		private readonly IList<string> _invalidateNextResponsePatterns = new List<string>();
		private SortedDictionary<byte, ZaberDevice> _devices = new SortedDictionary<byte, ZaberDevice>();
		private SortedDictionary<byte, Conversation> _conversations = new SortedDictionary<byte, Conversation>();
		private IZaberPort _port;
		private DeviceType _defaultDeviceType;
		private IDictionary<Tuple<int, FirmwareVersion>, DeviceType> _deviceTypeMap;

		private bool _areMessageIdsMonitored;

		// Invalidate if device mode is wrong.
		private bool _areMessageIdsEnabled;
		private TimeoutTimer _queryTimeoutTimer = new TimeoutTimer();
		private ZaberPortError _errorDuringQuery;
		private bool _isVoltageBadDuringQuery;
		private bool _shouldInvalidateNextResponse;
		private bool _shouldInvalidateNextResponseBinary;
		private bool _isInvalidated;
		private bool _shouldWaitBeforeReopening;
		private bool _isInvalidateEnabled = true;
		private bool _invalidationDeferred;
		private ZaberPortFacadeState _currentState = ZaberPortFacadeState.Closed;

		private Dictionary<byte, int> _deviceIds;

		private HashSet<byte> _devicesInBootloaderMode;

		// only used (non-null) during query state.

		private Conversation _selectedConversation;

		#endregion
	}
}

﻿using System;
using System.Threading;

namespace Zaber
{
	/// <summary>
	///     This class is simply a wrapper around standard timing utilities.
	/// </summary>
	/// <remarks>
	///     This class allows you to write unit tests that replace the TimeoutTimer
	///     with a MockTimeoutTimer and then the unit test can make the timer time
	///     out on demand. You should copy the TimeoutTimer to your application
	///     and the MockTimeoutTimer to your test application.
	/// </remarks>
	public class TimeoutTimer
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Sleep for the configured length of time.
		/// </summary>
		/// <remarks>
		///     This is equivalent to
		///     Thread.Sleep(Timeout), but it allows you to write unit tests that make
		///     the timer sleep until the unit test tells it to wake up.
		/// </remarks>
		public virtual void Sleep() => Thread.Sleep(Timeout);


		/// <summary>
		///     Blocks the current thread until the WaitHandle receives a signal or the
		///     timer times out.
		/// </summary>
		/// <param name="aWaitHandle">The wait handle to block on.</param>
		/// <returns>true if the wait handle receives a signal; otherwise false.</returns>
		/// <remarks>
		///     This is equivalent to WaitHandle.WaitOne(Timeout), but it allows you
		///     to write unit tests that make the timer wait until the unit test
		///     tells it to time out.
		/// </remarks>
		public virtual bool WaitOne(WaitHandle aWaitHandle)
		{
			if (null == aWaitHandle)
			{
				throw new ArgumentNullException("waitHandle");
			}

			return aWaitHandle.WaitOne(Timeout, false);
		}


		/// <summary>
		///     Whether this object is equal to another.
		/// </summary>
		/// <param name="aOther">The object to compare this one to.</param>
		/// <returns>True if the objects are equal, false otherwise.</returns>
		public override bool Equals(object aOther)
		{
			if (ReferenceEquals(this, aOther))
			{
				return true;
			}

			var other = aOther as TimeoutTimer;

			if (null == other)
			{
				return false;
			}

			return Timeout == other.Timeout;
		}


		/// <summary>
		///     Returns a calculated hash code for this object.
		/// </summary>
		/// <returns>An integer hash code.</returns>
		public override int GetHashCode() => Timeout;


		/// <summary>
		///     The length of time in milliseconds before a timeout occurs, or
		///     or Timeout.Infinite (-1) to wait indefinitely.
		/// </summary>
		public int Timeout { get; set; }

		#endregion
	}
}

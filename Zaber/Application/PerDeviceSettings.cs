﻿using System;

namespace Zaber.Application
{
	/// <summary>
	///     Used to serialize the user's settings for one device or device type.
	///     Client code should subclass this to add whatever data is to be saved.
	///     Subclasses should be marked with the [Serializable] attribute in order to
	///     be saved correctly.
	/// </summary>
	[CLSCompliant(false)]
	public abstract class PerDeviceSettings
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     What identifying information is known about the device.
		/// </summary>
		public DeviceIdentity DeviceIdentity { get; set; }

		#endregion
	}
}

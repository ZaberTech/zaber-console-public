﻿using System;
using System.Collections.Generic;

namespace Zaber.Application
{
	/// <summary>
	///     Used to save and restore user app settings keyed by device serial number or type.
	///     Client code should subclass this for the particular settings type in use. Subclasses should
	///     be marked with the [Serializable] attribute in order to be saved correctly.
	/// </summary>
	/// <typeparam name="T">
	///     Type of the per-device settings data class. Must be a subclass of
	///     <cref>PerDeviceSettings</cref> and be a reference type with a default constructor.
	/// </typeparam>
	[CLSCompliant(false)]
	public abstract class PerDeviceSettingsCollection<T> where T : PerDeviceSettings, new()
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Finds the favorite settings instance for a given device, if it exists.
		/// </summary>
		/// <param name="aDevice">Identity information about the device to get settings for.</param>
		/// <param name="aMask">Bit field controlling which identity fields to use in the comparison. Optional</param>
		/// <returns>An instance of <cref>T</cref> for the specified device, or null if not found.</returns>
		public virtual T FindSettings(DeviceIdentity aDevice, DeviceIdentityMask aMask = DeviceIdentityMask.All)
		{
			T bestMatch = null;

			foreach (var settings in PerDeviceSettings)
			{
				if (aDevice.Matches(settings.DeviceIdentity, aMask))
				{
					bestMatch = settings;
					break;
				}
			}

			return bestMatch;
		}


		/// <summary>
		///     Finds the favorite settings instance for a given device, or creates
		///     it if there is no match that meets the specified similarity threshold.
		/// </summary>
		/// <param name="aDevice">Identity information about the device to get settings for.</param>
		/// <param name="aMask">Bit field controlling which identity fields to use in the comparison. Optional.</param>
		/// <returns>An instance of <cref>T</cref> for the specified device.</returns>
		public virtual T FindOrCreateSettings(DeviceIdentity aDevice, DeviceIdentityMask aMask = DeviceIdentityMask.All)
		{
			var bestMatch = FindSettings(aDevice, aMask);
			if (null == bestMatch)
			{
				bestMatch = new T();
				bestMatch.DeviceIdentity = aDevice;
				PerDeviceSettings.Add(bestMatch);
			}

			return bestMatch;
		}


		/// <summary>
		///     Clears all existing settings.
		/// </summary>
		public void Clear() => PerDeviceSettings.Clear();


		/// <summary>
		///     Favorites for previously seen devices and device types.
		/// </summary>
		public List<T> PerDeviceSettings { get; set; } = new List<T>();

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zaber.Application
{
	/// <summary>
	///     Extra utility methods for enumerables.
	/// </summary>
	public static class IEnumerableExtensions
	{
		/// <summary>
		///    Formats a list of objects to a string using English grammar
		///    rules. Returns the empty string if there are no inputs. For one input,
		///    returns "X" where X is the result of calling ToString() on the
		///    input item. For two, returns "X and Y". For more than two,
		///    returns the form "X, Y and Z".
		/// </summary>
		/// <typeparam name="T">Object type contained in the collection.</typeparam>
		/// <param name="aItems">Collection of items to format.</param>
		/// <param name="aConjunction">Word to use to join the last two items of a collection. Defaults to "and"</param>
		/// <returns>A string representation of the collection.</returns>
		public static string JoinWithPunctuation<T>(this IEnumerable<T> aItems, string aConjunction = "and")
		{
			var sb = new StringBuilder();

			var list = new List<T>(aItems);

			if (list.Count == 1)
			{
				sb.Append(list[0]);
			}
			else if (list.Count > 1)
			{
				var last = list.Last();
				list.RemoveAt(list.Count - 1);

				if (list.Count > 0)
				{
					sb.Append(string.Join(", ", list));
					sb.Append($" {aConjunction} ");
				}

				sb.Append(last);
			}

			return sb.ToString();
		}
	}
}

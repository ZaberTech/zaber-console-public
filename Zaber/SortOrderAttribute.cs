﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Zaber
{
	/// <summary>
	///     Attribute to assist sorting of types by relative numeric values.
	/// </summary>
	public class SortOrderAttribute : Attribute
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Constructor for numeric sort ordering.
		/// </summary>
		/// <param name="aNumericOrder">Value to use for sorting</param>
		public SortOrderAttribute(double aNumericOrder)
		{
			NumericOrder = aNumericOrder;
		}


		/// <summary>
		///     Sort helper function. Sorts objects by their types' SortOrder attribute.
		///     No sorting is done using means other than attribute values - that is, there is no secondary lexical sort.
		/// </summary>
		/// <param name="aValues">
		///     List of objects to sort. Any that are missing the SortOrder attribute are treated as having a
		///     numeric sort order of 0.
		/// </param>
		/// <returns>Sorted list.</returns>
		public static IEnumerable<T> SortInstances<T>(IEnumerable<T> aValues) => aValues.OrderBy(o =>
		{
			var val = 0.0;

			if (null != o)
			{
				foreach (var attr in o.GetType().GetCustomAttributes(typeof(SortOrderAttribute), true))
				{
					var so = attr as SortOrderAttribute;
					if (null != so)
					{
						val = so.NumericOrder;
						break;
					}
				}
			}

			return val;
		});


		/// <summary>
		///     Sort helper function. Sorts types by their SortOrder attribute.
		///     No sorting is done using means other than attribute values - that is, there is no secondary lexical sort.
		/// </summary>
		/// <param name="aTypes">
		///     List of types to sort. Any that are missing the SortOrder attribute are treated as having a
		///     numeric sort order of 0.
		/// </param>
		/// <returns>Sorted list.</returns>
		public static IEnumerable<Type> SortTypes(IEnumerable<Type> aTypes) => aTypes.OrderBy(t =>
		{
			var val = 0.0;

			if (null != t)
			{
				foreach (var attr in t.GetCustomAttributes(typeof(SortOrderAttribute), true))
				{
					var so = attr as SortOrderAttribute;
					if (null != so)
					{
						val = so.NumericOrder;
						break;
					}
				}
			}

			return val;
		});


		/// <summary>
		///     Numeric value for sorting.
		/// </summary>
		public double NumericOrder { get; set; }

		#endregion
	}
}

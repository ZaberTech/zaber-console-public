﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using log4net;

namespace Zaber
{
	/// <summary>
	///     Registering for response events can be awkward, especially in scripts.
	///     This class provides the base features for other classes to register
	///     for events and store them until you request them with the
	///     <see cref="NextResponse()" /> method. Start and stop listening
	///     with the <see cref="Start" /> and <see cref="Stop" /> methods.
	/// </summary>
	/// <typeparam name="T">the type of response objects</typeparam>
	public abstract class ResponseListener<T> : IDisposable where T : class
	{
		#region -- Public Methods & Properties --


		/// <summary>
		/// Optional matching filter for received items. If non-null, this
		/// filter must return true for a received item to halt the timeout.
		/// </summary>
		public Predicate<T> Filter { get; set; }


		/// <summary>
		///     Stop listening for responses.
		/// </summary>
		public void Stop()
		{
			lock (_locker)
			{
				if (null == _semaphore)
				{
					throw new InvalidOperationException("Listener is already stopped.");
				}

				UnregisterEvent();

				_semaphore.Close();
				_semaphore = null;
			}
		}


		/// <summary>
		///     Start listening for responses.
		/// </summary>
		public void Start()
		{
			_log.Debug("starting listener");
			lock (_locker)
			{
				if (null != _semaphore)
				{
					throw new InvalidOperationException("Listener is already started.");
				}

				_semaphore = new Semaphore(0, int.MaxValue);
				RegisterEvent();
			}
		}


		/// <summary>
		///     Returns the next response or waits for one to be received.
		/// </summary>
		/// <returns>The response message</returns>
		/// <remarks>
		///     See <see cref="NextResponse(bool)" /> if you don't want to wait for
		///     a response to be received.
		/// </remarks>
		public T NextResponse() => NextResponse(true);


		/// <summary>
		///     Returns the next response or waits for one to be received.
		/// </summary>
		/// <param name="aIsBlocking">
		///     True if the method should wait for a
		///     response if there are none already received.
		/// </param>
		/// <returns>
		///     The response message or null if no responses have
		///     been received.
		/// </returns>
		public T NextResponse(bool aIsBlocking)
		{
			var semaphore = _semaphore;
			CheckIsListening(semaphore);
			var timeout = aIsBlocking ? Timeout.Infinite : 0;
			if (!semaphore.WaitOne(timeout, false))
			{
				return null;
			}

			lock (_locker)
			{
				return _responses.Dequeue();
			}
		}


		/// <summary>
		///     Returns the next response or waits for one to be received.
		/// </summary>
		/// <param name="aTimer">
		///     The timer that tells how long to wait
		///     for a response to be received.
		/// </param>
		/// <returns>The response message.</returns>
		/// <exception cref="RequestTimeoutException">
		///     When the timer expires
		///     before any response is received.
		/// </exception>
		public T NextResponse(TimeoutTimer aTimer)
		{
			var semaphore = _semaphore;
			CheckIsListening(semaphore);
			if (!aTimer.WaitOne(semaphore))
			{
				throw new RequestTimeoutException();
			}

			lock (_locker)
			{
				return _responses.Dequeue();
			}
		}


		/// <summary>
		///     Gets a flag showing whether the listener is currently listening for
		///     responses.
		/// </summary>
		public bool IsListening
		{
			get
			{
				lock (_locker)
				{
					return null != _semaphore;
				}
			}
		}

		#endregion

		#region -- IDisposable Implementation --

		/// <summary>
		///     Performs application-defined tasks associated with freeing, releasing, or
		///     resetting unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);

			// not necessary here, but might be for derived types.
			GC.SuppressFinalize(this);
		}


		/// <summary>
		///     Performs application-defined tasks associated with freeing, releasing, or
		///     resetting unmanaged resources.
		/// </summary>
		/// <param name="aIsDisposing">
		///     True if the object is being disposed, and not
		///     garbage collected.
		/// </param>
		protected virtual void Dispose(bool aIsDisposing)
		{
			if (aIsDisposing)
			{
				lock (_locker)
				{
					UnregisterEvent();
					_semaphore.Close();
					_semaphore = null;
				}
			}
		}

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Derived classes need to override this method and register with
		///     whatever source of events they care about.
		/// </summary>
		/// <seealso cref="UnregisterEvent" />
		/// <seealso cref="OnItemReceived" />
		protected abstract void RegisterEvent();


		/// <summary>
		///     Derived classes need to override this method and unregister from
		///     whatever source of events they care about.
		/// </summary>
		/// <seealso cref="RegisterEvent" />
		/// <seealso cref="OnItemReceived" />
		protected abstract void UnregisterEvent();


		/// <summary>
		///     Handle a response event.
		/// </summary>
		/// <param name="aResponse">The details of the response.</param>
		/// <seealso cref="RegisterEvent" />
		/// <seealso cref="UnregisterEvent" />
		protected void OnItemReceived(T aResponse)
		{
			lock (_locker)
			{
				if (null != _semaphore)
				{
					if ((Filter is null) || Filter(aResponse))
					{
						_responses.Enqueue(aResponse);
						_semaphore.Release();
					}
				}
			}
		}


		/// <summary>
		///     Check that the listener is still listening, based on the current
		///     semaphore.
		/// </summary>
		/// <param name="aSemaphore">The value to check for null.</param>
		/// <exception cref="InvalidOperationException">NextResponse() was called when the listener was not listening.</exception>
		private static void CheckIsListening(Semaphore aSemaphore)
		{
			if (null == aSemaphore)
			{
				throw new InvalidOperationException("NextResponse() called when listener is not listening.");
			}
		}

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private readonly Queue<T> _responses = new Queue<T>();
		private Semaphore _semaphore;
		private readonly object _locker = new object();

		#endregion
	}
}

﻿using System.Collections.Generic;

namespace Zaber
{
	/// <summary>
	///     Represents a node in the ASCII command tree for a device.  Tracing the parents
	///     of a command node should yield the information required to construct a command.
	///     <seealso cref="ASCIICommandInfo" />
	/// </summary>
	#pragma warning disable CS0618 // Type or member is obsolete
	public class ASCIICommandNode : IConvertible
		#pragma warning restore CS0618 // Type or member is obsolete
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Constructs an ASCII command node with a parent element.
		/// </summary>
		/// <param name="aParentNode">The previous node in the command tree.</param>
		public ASCIICommandNode(ASCIICommandNode aParentNode = null)
		{
			_parent = aParentNode;
		}


		/// <summary>
		///     Get unit conversion info about this command's request parameter.
		/// </summary>
		/// <returns>An instance of a class that implements <see cref="IParameterConversionInfo" /></returns>
		public virtual IParameterConversionInfo GetRequestConversion() => new ParameterUnitConversion
		{
			ReferenceUnit = RequestUnit?.Unit,
			Function = RequestUnitFunction,
			Scale = RequestUnitScale,
			IsRequestRelativePosition = IsRequestRelativePosition
		};


		/// <summary>
		///     Retrieves a linked list of ASCII command nodes leading up to the current node.
		/// </summary>
		/// <returns>
		///     The tree elements leading to the command node, starting with the root.
		/// </returns>
		public LinkedList<ASCIICommandNode> GetBranch()
		{
			var branchList = new LinkedList<ASCIICommandNode>();
			var parentNode = this;

			while (null != parentNode)
			{
				branchList.AddFirst(parentNode);
				parentNode = parentNode._parent;
			}

			return branchList;
		}


		/// <summary>
		///     Creates shallow copy of ASCIICommandNode.
		/// </summary>
		public virtual ASCIICommandNode Clone()
		{
			var copy = new ASCIICommandNode();
			CopyProperties(this, copy);
			return copy;
		}


		/// <summary>
		///     Helper for debugging.
		/// </summary>
		/// <returns>A string representation of this node's content.</returns>
		public override string ToString() => ("ASCII cmd node: " + NodeText) ?? "?";


		/// <summary>
		///     The individual access level of the node.
		/// </summary>
		public int AccessLevel { get; set; }


		/// <summary>
		///     Whether or not the command can be seen in the ASCII command list by default.
		/// </summary>
		public bool IsBasic { get; set; }


		/// <summary>
		///     Is the command argument a parameter (inifinite number of options, e.g. numerical/text input)
		/// </summary>
		public bool IsParameter => null != ParamType;


		/// <summary>
		///     Is the command argument an enumerable parameter  (finite number of options)
		/// </summary>
		public bool IsEnumerableParameter => (null != ParamType) && ParamType.IsEnumeration;


		/// <summary>
		///     A text represenatation of the command node.
		/// </summary>
		public string NodeText { get; set; }


		/// <summary>
		///     The parameter type of the node, or null if it is not a parameter.
		/// </summary>
		public ParameterType ParamType { get; set; }


		/// <summary>
		///     The unit associated with the node, if it is a parameter.
		/// </summary>
		public UnitOfMeasure RequestUnit { get; set; }


		/// <summary>
		///     The scaling function used to convert the node's value (if it is a parameter)
		///     to units for a Zaber device.
		/// </summary>
		public ScalingFunction RequestUnitFunction { get; set; }


		/// <summary>
		///     The scaling factor from physical units to the parameter value.
		/// </summary>
		public decimal? RequestUnitScale { get; set; }


		/// <summary>
		///     Whether or not the node, if a parameter, is a relative position.
		/// </summary>
		public bool IsRequestRelativePosition { get; set; }

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Called by Clone() to copy properties from one instance to another.
		///     Subclasses' Clone() implementations can call this to handle the base class copying.
		/// </summary>
		/// <param name="aFrom">The instance to copy values from.</param>
		/// <param name="aTo">The instance to copy values to.</param>
		protected static void CopyProperties(ASCIICommandNode aFrom, ASCIICommandNode aTo)
		{
			aTo.AccessLevel = aFrom.AccessLevel;
			aTo.IsBasic = aFrom.IsBasic;
			aTo.IsRequestRelativePosition = aFrom.IsRequestRelativePosition;
			aTo.NodeText = aFrom.NodeText;
			aTo.ParamType = aFrom.ParamType;
			aTo.RequestUnit = aFrom.RequestUnit;
			aTo.RequestUnitFunction = aFrom.RequestUnitFunction;
			aTo.RequestUnitScale = aFrom.RequestUnitScale;
			aTo._parent = aFrom._parent;
		}

		#endregion

		#region -- Data --

		private ASCIICommandNode _parent;

		#endregion
	}
}

using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Zaber
{
	/// <summary>
	///     This exception is thrown by a <see cref="Conversation" /> when
	///     a request gets replaced before responding.
	/// </summary>
	[Serializable]
	public class RequestReplacedException : ConversationException
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor. Initializes the exception with a generic message.
		/// </summary>
		public RequestReplacedException()
			: base(DEFAULT_MESSAGE)
		{
		}


		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aMessage">The message to display</param>
		public RequestReplacedException(string aMessage)
			: base(aMessage)
		{
		}


		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aMessage">The message to display</param>
		/// <param name="aException">The exception that caused this one</param>
		public RequestReplacedException(string aMessage, Exception aException)
			: base(aMessage, aException)
		{
		}


		/// <summary>
		///     Initializes a new instance with serialized
		///     data.
		/// </summary>
		/// <param name="aSerializationInfo">
		///     The System.Runtime.Serialization.SerializationInfo that holds the serialized
		///     object data about the exception being thrown.
		/// </param>
		/// <param name="aContext">
		///     The System.Runtime.Serialization.StreamingContext that contains contextual
		///     information about the source or destination.
		/// </param>
		protected RequestReplacedException(SerializationInfo aSerializationInfo, StreamingContext aContext)
			: base(aSerializationInfo, aContext)
		{
			if (null == aSerializationInfo)
			{
				throw new ArgumentNullException(nameof(aSerializationInfo));
			}

			ReplacementResponse =
				(DeviceMessage) aSerializationInfo.GetValue("replacementResponse", typeof(DeviceMessage));
		}


		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aReplacementResponse">The response that replaced the expected one.</param>
		public RequestReplacedException(DeviceMessage aReplacementResponse)
			: base(DEFAULT_MESSAGE)
		{
			ReplacementResponse = aReplacementResponse;
		}


		/// <summary>
		///     Sets the System.Runtime.Serialization.SerializationInfo
		///     with information about the exception.
		/// </summary>
		/// <param name="aSerializationInfo">
		///     The System.Runtime.Serialization.SerializationInfo that holds the serialized
		///     object data about the exception being thrown.
		/// </param>
		/// <param name="aContext">
		///     The System.Runtime.Serialization.StreamingContext that contains contextual
		///     information about the source or destination.
		/// </param>
		/// <exception cref="ArgumentNullException">The info parameter is a null reference.</exception>
		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		public override void GetObjectData(SerializationInfo aSerializationInfo, StreamingContext aContext)
		{
			if (null == aSerializationInfo)
			{
				throw new ArgumentNullException(nameof(aSerializationInfo));
			}

			base.GetObjectData(aSerializationInfo, aContext);

			aSerializationInfo.AddValue("replacementResponse", ReplacementResponse);
		}


		/// <summary>
		///     The response to the command that replaced the current command.
		/// </summary>
		public DeviceMessage ReplacementResponse { get; }

		#endregion

		#region -- Data --

		private const string DEFAULT_MESSAGE = "Current request was replaced by a new request.";

		#endregion
	}
}

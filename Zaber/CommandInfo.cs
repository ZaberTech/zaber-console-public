using System;
using System.Text.RegularExpressions;

namespace Zaber
{
	/// <summary>
	///     Descriptive information about a command that Zaber devices support.
	/// </summary>
	[Serializable]
	#pragma warning disable CS0618 // Type or member is obsolete
	public class CommandInfo : IConvertible
		#pragma warning restore CS0618 // Type or member is obsolete
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Initialize a new instance.
		/// </summary>
		public CommandInfo()
		{
			HasParameters = true;
			AccessLevel = DEFAULT_ACCESS_LEVEL;
			_helpText = "";
		}


		/// <summary>
		///     Get unit conversion info about this command's request parameter.
		/// </summary>
		/// <returns>An instance of a class that implements <see cref="IParameterConversionInfo" /></returns>
		public virtual IParameterConversionInfo GetRequestConversion() => new ParameterUnitConversion
		{
			ReferenceUnit = RequestUnit?.Unit,
			Function = RequestUnitFunction,
			Scale = RequestUnitScale,
			IsRequestRelativePosition = IsRequestRelativePosition
		};


		/// <summary>
		///     Get unit conversion info about this command's response data.
		/// </summary>
		/// <returns>An instance of a class that implements <see cref="IParameterConversionInfo" /></returns>
		public IParameterConversionInfo GetResponseConversion() => new ParameterUnitConversion
		{
			ReferenceUnit = ResponseUnit?.Unit,
			Function = ResponseUnitFunction,
			Scale = ResponseUnitScale,
			IsRequestRelativePosition = IsResponseRelativePosition
		};


		/// <summary>
		///     Whether this object is equal to another.
		/// </summary>
		/// <param name="aOther">The object to compare this one to.</param>
		/// <returns>True if the objects are equal, false otherwise.</returns>
		public override bool Equals(object aOther)
		{
			if (ReferenceEquals(this, aOther))
			{
				return true;
			}

			var other = aOther as CommandInfo;

			if (null == other)
			{
				return false;
			}

			return Equals(Command, other.Command)
			   && Equals(DataDescription, other.DataDescription)
			   && (HasParameters == other.HasParameters)
			   && (IsAxisCommand == other.IsAxisCommand)
			   && (IsBasic == other.IsBasic)
			   && (AccessLevel == other.AccessLevel)
			   && (IsReadOnlySetting == other.IsReadOnlySetting)
			   && (IsResponseOnly == other.IsResponseOnly)
			   && (IsRetrySafe == other.IsRetrySafe)
			   && (IsSetting == other.IsSetting)
			   && (IsCurrentPositionReturned == other.IsCurrentPositionReturned)
			   && Equals(Name, other.Name)
			   && (Number == other.Number)
			   && Equals(ResponseDescription, other.ResponseDescription)
			   && Equals(TextCommand, other.TextCommand)
			   && Equals(RequestUnit, other.RequestUnit)
			   && (RequestUnitFunction == other.RequestUnitFunction)
			   && (RequestUnitScale == other.RequestUnitScale)
			   && Equals(ResponseUnit, other.ResponseUnit)
			   && (ResponseUnitFunction == other.ResponseUnitFunction)
			   && (ResponseUnitScale == other.ResponseUnitScale);
		}


		/// <summary>
		///     Returns a calculated hash code for this object.
		/// </summary>
		/// <returns>An integer hash code.</returns>
		public override int GetHashCode()
		{
			var code = 17;

			code = (31 * code) + Command.GetHashCode();
			if (null != DataDescription)
			{
				code = (31 * code) + DataDescription.GetHashCode();
			}

			code = (31 * code) + (HasParameters ? 1 : 0);
			code = (31 * code) + (IsAxisCommand ? 1 : 0);
			code = (31 * code) + (IsBasic ? 1 : 0);
			code = (31 * code) + AccessLevel.GetHashCode();
			code = (31 * code) + (IsReadOnlySetting ? 1 : 0);
			code = (31 * code) + (IsResponseOnly ? 1 : 0);
			code = (31 * code) + (IsRetrySafe ? 1 : 0);
			code = (31 * code) + (IsSetting ? 1 : 0);
			code = (31 * code) + (IsCurrentPositionReturned ? 1 : 0);

			if (null != Name)
			{
				code = (31 * code) + Name.GetHashCode();
			}

			if (null != ResponseDescription)
			{
				code = (31 * code) + ResponseDescription.GetHashCode();
			}

			if (null != TextCommand)
			{
				code = (31 * code) + TextCommand.GetHashCode();
			}

			if (null != RequestUnit)
			{
				code = (31 * code) + RequestUnit.GetHashCode();
			}

			code = (31 * code) + RequestUnitFunction.GetHashCode();
			code = (31 * code) + RequestUnitScale.GetHashCode();

			if (null != ResponseUnit)
			{
				code = (31 * code) + ResponseUnit.GetHashCode();
			}

			code = (31 * code) + ResponseUnitFunction.GetHashCode();
			code = (31 * code) + ResponseUnitScale.GetHashCode();

			return code;
		}


		/// <summary>
		///     Create a deep copy of the CommandInfo.
		/// </summary>
		/// <returns>A new instance of CommandInfo with all the same property values as the instance invoked on.</returns>
		public virtual CommandInfo Clone()
		{
			var ci = new CommandInfo();
			CopyProperties(this, ci);
			return ci;
		}


		/// <summary>
		///     ToString override for debugger support.
		/// </summary>
		/// <returns>A textual identifier for the command, for debugger display purposes.</returns>
		public override string ToString()
		{
			if (null != TextCommand)
			{
				return "ASCII command: " + TextCommand;
			}

			return ("Binary command: " + Name) ?? Command.ToString();
		}


		/// <summary>
		///     The enumeration entry from <see cref="Zaber.Command" />
		///     that represents this command.
		/// </summary>
		public Command Command
		{
			get => (Command) Number;
			set => Number = (byte) value;
		}


		/// <summary>
		///     Describes the data value of this command.
		/// </summary>
		/// <value>
		///     Null if the data field is ignored.
		/// </value>
		public string DataDescription { get; set; }


		/// <summary>
		///     Does the command take parameters. (Only relevant for ASCII commands.)
		/// </summary>
		public bool HasParameters { get; set; }


		/// <summary>
		///     Gets or sets a flag showing whether the command can be sent to one
		///     axis of a multi-axis device. (Only relevant for text commands.)
		/// </summary>
		public bool IsAxisCommand { get; set; }


		/// <summary>
		///     Gets or sets a flag marking this command as a simple one.
		///     Defaults to false.
		/// </summary>
		public virtual bool IsBasic { get; set; }


		/// <summary>
		///     Gets or sets a flag showing whether the response to this command
		///     contains the device's current position.
		/// </summary>
		public bool IsCurrentPositionReturned { get; set; }


		/// <summary>
		///     Gets or sets the access level of this command or setting. This value,
		///     in combination with the current access level of the device,
		///     determines whether the command will be displayed to the user.
		///     For settings, this property indicates the access level needed for
		///     writing the value.
		///     Defaults to zero. Normally one for common commands.
		/// </summary>
		/// <remarks>
		///     Typically, commands with a value greater than one are either
		///     advanced commands or only kept for backward compatibility.
		/// </remarks>
		public virtual int AccessLevel { get; set; }


		/// <summary>
		///     DEPRECATED - compare <see cref="Zaber.CommandInfo.AccessLevel" /> with
		///     the device's current access level to determine whether a command or setting
		///     should be visible.
		/// </summary>
		[Obsolete("No longer used.")]
		public bool IsHidden => AccessLevel > 1;


		/// <summary>
		///     Is this command actually a read-only setting?
		/// </summary>
		/// <remarks>
		///     Read-only settings are read differently from other settings. Just
		///     send the command and the value will be returned.
		/// </remarks>
		public virtual bool IsReadOnlySetting => false;


		/// <summary>
		///     Response-only commands are never sent to the devices. They come back as
		///     responses. The error response is a good example.
		/// </summary>
		public virtual bool IsResponseOnly => false;


		/// <summary>
		///     Gets or sets a flag showing whether it is safe to send the command
		///     again when a response is lost.
		/// </summary>
		public bool IsRetrySafe { get; set; }


		/// <summary>
		///     Is this command actually a setting that can also be retrieved?
		/// </summary>
		/// <remarks>
		///     A setting's value is set by sending the command with the new
		///     value as the data. A setting's value is read by sending the
		///     <see cref="Zaber.Command.ReturnSetting" /> command with the setting's
		///     command number as the data.
		/// </remarks>
		public virtual bool IsSetting => false;


		/// <summary>
		///     Whether the parameter is a relative position.
		/// </summary>
		public bool IsRequestRelativePosition { get; set; }


		/// <summary>
		///     Whether the response to this command is a relative position.
		/// </summary>
		public bool IsResponseRelativePosition { get; set; }


		/// <summary>
		///     A brief name for the command
		/// </summary>
		public virtual string Name { get; set; }


		/// <summary>
		///     The code number that represents this command.
		/// </summary>
		public byte Number { get; set; }


		/// <summary>
		///     The unit of measure in which the parameter is measured.
		/// </summary>
		public UnitOfMeasure RequestUnit { get; set; }


		/// <summary>
		///     The translation function for translating physical units for the
		///     parameter.
		/// </summary>
		public ScalingFunction RequestUnitFunction { get; set; }


		/// <summary>
		///     The scaling factor from physical units to the parameter value.
		/// </summary>
		public decimal? RequestUnitScale { get; set; }


		/// <summary>
		///     Describes the meaning of the data value in a response packet.
		/// </summary>
		public string ResponseDescription { get; set; }


		/// <summary>
		///     The unit of measure in which the return value is measured.
		/// </summary>
		public UnitOfMeasure ResponseUnit { get; set; }


		/// <summary>
		///     The translation function for translating physical units for the
		///     return value.
		/// </summary>
		public ScalingFunction ResponseUnitFunction { get; set; }


		/// <summary>
		///     The scaling factor from base physical units to the return value.
		/// </summary>
		public decimal? ResponseUnitScale { get; set; }


		/// <summary>
		///     A text version of the command to use when the port is in ASCII
		///     mode.
		/// </summary>
		/// <remarks>
		///     Some commands will have a text version, some will have a
		///     binary version, some will have both.
		/// </remarks>
		public virtual string TextCommand { get; set; }

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Called by Clone() to copy properties from one instance to another.
		///     Subclasses' Clone() implementations can call this to handle the base class copying.
		/// </summary>
		/// <param name="aFrom">The instance to copy values from.</param>
		/// <param name="aTo">The instance to copy values to.</param>
		protected static void CopyProperties(CommandInfo aFrom, CommandInfo aTo)
		{
			aTo.Command = aFrom.Command;
			aTo.DataDescription = aFrom.DataDescription;
			aTo.HasParameters = aFrom.HasParameters;
			aTo._helpText = aFrom._helpText;
			aTo.IsAxisCommand = aFrom.IsAxisCommand;
			aTo.IsBasic = aFrom.IsBasic;
			aTo.AccessLevel = aFrom.AccessLevel;
			aTo.IsRetrySafe = aFrom.IsRetrySafe;
			aTo.IsRequestRelativePosition = aFrom.IsRequestRelativePosition;
			aTo.IsResponseRelativePosition = aFrom.IsResponseRelativePosition;
			aTo.IsCurrentPositionReturned = aFrom.IsCurrentPositionReturned;
			aTo.Name = aFrom.Name;
			aTo.Number = aFrom.Number;
			aTo.ResponseDescription = aFrom.ResponseDescription;
			aTo.TextCommand = aFrom.TextCommand;
			aTo.RequestUnit = aFrom.RequestUnit;
			aTo.RequestUnitFunction = aFrom.RequestUnitFunction;
			aTo.RequestUnitScale = aFrom.RequestUnitScale;
			aTo.ResponseUnit = aFrom.ResponseUnit;
			aTo.ResponseUnitFunction = aFrom.ResponseUnitFunction;
			aTo.ResponseUnitScale = aFrom.ResponseUnitScale;
		}


		private static string NormalizeSubstringWhitespace(string aInput, int aStartIndex, int aLength)
		{
			var rawParagraph = aInput.Substring(aStartIndex, aLength);
			return Regex.Replace(rawParagraph, @"\s+", " ").Trim();
		}

		#endregion

		#region -- Data --

		/// <summary>
		///     Default access level when not known from database.
		/// </summary>
		protected const int DEFAULT_ACCESS_LEVEL = 0;

		private string _helpText;

		#endregion
	}
}

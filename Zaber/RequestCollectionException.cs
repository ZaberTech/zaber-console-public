using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;

namespace Zaber
{
	/// <summary>
	///     Exception thrown by a <see cref="ConversationCollection" /> when some
	///     requests in the list fail.
	/// </summary>
	[Serializable]
	public class RequestCollectionException : ConversationException
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor. Initializes the exception with a generic message.
		/// </summary>
		public RequestCollectionException()
			: base("Some requests failed.")
		{
			_topics = new List<ConversationTopic>();
		}


		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aMessage">The message to display</param>
		public RequestCollectionException(string aMessage)
			: base(aMessage)
		{
			_topics = new List<ConversationTopic>();
		}


		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aMessage">The message to display</param>
		/// <param name="aException">The exception that caused this exception</param>
		public RequestCollectionException(string aMessage, Exception aException)
			: base(aMessage, aException)
		{
			_topics = new List<ConversationTopic>();
		}


		/// <summary>
		///     Initializes a new instance with serialized
		///     data.
		/// </summary>
		/// <param name="aSerializationInfo">
		///     The System.Runtime.Serialization.SerializationInfo that holds the serialized
		///     object data about the exception being thrown.
		/// </param>
		/// <param name="aContext">
		///     The System.Runtime.Serialization.StreamingContext that contains contextual
		///     information about the source or destination.
		/// </param>
		protected RequestCollectionException(SerializationInfo aSerializationInfo, StreamingContext aContext)
			: base(aSerializationInfo, aContext)
		{
			if (null == aSerializationInfo)
			{
				throw new ArgumentNullException(nameof(aSerializationInfo));
			}

			var topicCount = aSerializationInfo.GetInt32("topicCount");
			_topics = new List<ConversationTopic>();
			for (var i = 0; i < topicCount; i++)
			{
				var name = string.Format(CultureInfo.InvariantCulture, "topic{0}", i++);
				_topics.Add((ConversationTopic) aSerializationInfo.GetValue(name, typeof(ConversationTopic)));
			}
		}


		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aTopics">
		///     All the topics used in the request, including
		///     the one or ones that caused the exception
		/// </param>
		public RequestCollectionException(ICollection<ConversationTopic> aTopics)
			: base(BuildMessage(aTopics))
		{
			_topics = new List<ConversationTopic>();
			_topics.AddRange(aTopics);
		}


		/// <summary>
		///     Builds a text message that describes why some requests failed.
		/// </summary>
		/// <param name="aTopics">The topics to validate.</param>
		/// <returns>
		///     A text message if some topics were invalid,
		///     otherwise an empty string.
		/// </returns>
		public static string BuildMessage(ICollection<ConversationTopic> aTopics)
		{
			var portErrors = new HashSet<ZaberPortError>();

			var sb = new StringBuilder();
			var errorCount = 0;
			foreach (var topic in aTopics)
			{
				if (null != topic)
				{
					if (topic.IsValid)
					{
						continue;
					}

					if (ZaberPortError.None != topic.ZaberPortError)
					{
						portErrors.Add(topic.ZaberPortError);
						continue;
					}
				}

				if (0 == errorCount)
				{
					sb.Append("Some requests failed: ");
				}
				else
				{
					sb.Append("; ");
				}

				errorCount++;

				if (null == topic)
				{
					sb.Append("null topic");
				}
				else if (topic.ReplacementResponse != null)
				{
					sb.AppendFormat("request replaced on device {0}", topic.ReplacementResponse.DeviceNumber);
				}
				else if (null == topic.Response)
				{
					sb.Append("null response");
				}
				else if (topic.Response.IsError)
				{
					string errorMessage;
					if (MessageType.Binary != topic.Response.MessageType)
					{
						errorMessage = topic.Response.TextData;
					}
					else
					{
						var errorNumber = (int) topic.Response.NumericData;
						var errorName = Enum.GetName(typeof(ZaberError), errorNumber);
						errorMessage =
							null == errorName
								? string.Format(CultureInfo.CurrentCulture, "error {0}", errorNumber)
								: string.Format(CultureInfo.CurrentCulture, "{0} error", errorName);
					}

					sb.AppendFormat("{1} on device {0}", topic.Response.DeviceNumber, errorMessage);
				}
			}

			if (errorCount > 0)
			{
				sb.Append(".");
			}

			foreach (var portError in portErrors)
			{
				if (sb.Length > 0)
				{
					sb.Append(" ");
				}

				sb.AppendFormat("Some requests failed because of a port error: {0}.", portError);
			}

			return sb.ToString();
		}


		/// <summary>
		///     Sets the System.Runtime.Serialization.SerializationInfo
		///     with information about the exception.
		/// </summary>
		/// <param name="aSerializationInfo">
		///     The System.Runtime.Serialization.SerializationInfo that holds the serialized
		///     object data about the exception being thrown.
		/// </param>
		/// <param name="aContext">
		///     The System.Runtime.Serialization.StreamingContext that contains contextual
		///     information about the source or destination.
		/// </param>
		/// <exception cref="ArgumentNullException">The info parameter is a null reference.</exception>
		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		public override void GetObjectData(SerializationInfo aSerializationInfo, StreamingContext aContext)
		{
			if (null == aSerializationInfo)
			{
				throw new ArgumentNullException(nameof(aSerializationInfo));
			}

			base.GetObjectData(aSerializationInfo, aContext);

			aSerializationInfo.AddValue("topicCount", _topics.Count);
			var i = 0;
			foreach (var topic in _topics)
			{
				var name = string.Format(CultureInfo.InvariantCulture, "topic{0}", i++);
				aSerializationInfo.AddValue(name, topic);
			}
		}


		/// <summary>
		///     All the topics used in the request, including
		///     the one or ones that caused the exception.
		/// </summary>
		public IList<ConversationTopic> Topics => _topics;

		#endregion

		#region -- Data --

		private readonly List<ConversationTopic> _topics;

		#endregion
	}
}

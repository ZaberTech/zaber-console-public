﻿using System;

namespace Zaber
{
	/// <summary>
	///     Represents an object which contains data that can be converted from
	///     real-world units to native Zaber device units.
	/// </summary>
	[Obsolete("Use CommandInfo.GetRequestConversion() instead.")]
	public interface IConvertible
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     The unit of measure in which the value is measured.
		/// </summary>
		UnitOfMeasure RequestUnit { get; set; }


		/// <summary>
		///     The translation function for translating physical units for the
		///     value.
		/// </summary>
		ScalingFunction RequestUnitFunction { get; set; }


		/// <summary>
		///     The scaling factor from physical units to native device units.
		/// </summary>
		decimal? RequestUnitScale { get; set; }


		/// <summary>
		///     Whether the value is a relative position.
		/// </summary>
		bool IsRequestRelativePosition { get; set; }

		#endregion
	}
}

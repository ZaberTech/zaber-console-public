using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Zaber
{
	/// <summary>
	///     This exception is thrown by a <see cref="Conversation" /> when
	///     a request is canceled by another thread.
	/// </summary>
	[Serializable]
	public class RequestCanceledException : ConversationException
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor. Initializes the exception with a generic message.
		/// </summary>
		public RequestCanceledException()
			: base(DEFAULT_MESSAGE)
		{
		}


		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aMessage">The message to display</param>
		public RequestCanceledException(string aMessage)
			: base(aMessage)
		{
		}


		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		/// <param name="aMessage">The message to display</param>
		/// <param name="aException">The exception that caused this one</param>
		public RequestCanceledException(string aMessage, Exception aException)
			: base(aMessage, aException)
		{
		}


		/// <summary>
		///     Initializes a new instance with serialized
		///     data.
		/// </summary>
		/// <param name="aSerializationInfo">
		///     The System.Runtime.Serialization.SerializationInfo that holds the serialized
		///     object data about the exception being thrown.
		/// </param>
		/// <param name="aContext">
		///     The System.Runtime.Serialization.StreamingContext that contains contextual
		///     information about the source or destination.
		/// </param>
		protected RequestCanceledException(SerializationInfo aSerializationInfo, StreamingContext aContext)
			: base(aSerializationInfo, aContext)
		{
		}


		/// <summary>
		///     Sets the System.Runtime.Serialization.SerializationInfo
		///     with information about the exception.
		/// </summary>
		/// <param name="aSerializationInfo">
		///     The System.Runtime.Serialization.SerializationInfo that holds the serialized
		///     object data about the exception being thrown.
		/// </param>
		/// <param name="aContext">
		///     The System.Runtime.Serialization.StreamingContext that contains contextual
		///     information about the source or destination.
		/// </param>
		/// <exception cref="ArgumentNullException">The info parameter is a null reference.</exception>
		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		public override void GetObjectData(SerializationInfo aSerializationInfo, StreamingContext aContext)
		{
			if (null == aSerializationInfo)
			{
				throw new ArgumentNullException(nameof(aSerializationInfo));
			}

			base.GetObjectData(aSerializationInfo, aContext);
		}

		#endregion

		#region -- Data --

		private const string DEFAULT_MESSAGE = "Request was canceled.";

		#endregion
	}
}

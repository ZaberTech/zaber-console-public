﻿using Zaber.Units;

namespace Zaber
{
	/// <summary>
	///     Contains data about conversions between real-world units and native Zaber device units.
	/// </summary>
	public class ParameterUnitConversion : IParameterConversionInfo
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     String conversion override for debugging.
		/// </summary>
		/// <returns>A string representation of the object.</returns>
		public override string ToString()
		{
			var func = Function.ToString();
			var relative = string.Empty;
			if (IsRequestRelativePosition)
			{
				relative = "relative ";
			}

			var scale = (Scale ?? 0m).ToString();
			var unit = ReferenceUnit?.Abbreviation ?? "?";

			return string.Format("{0}{1} {2} * {3}", relative, func, scale, unit);
		}


		/// <summary>
		///     The unit of measure in which the value is measured.
		///     This also provides the dimension of the quantity.
		/// </summary>
		public Unit ReferenceUnit { get; set; }


		/// <summary>
		///     The translation function for translating physical units for the
		///     value.
		/// </summary>
		public ScalingFunction Function { get; set; }


		/// <summary>
		///     The scaling factor from physical units to native device units.
		/// </summary>
		public decimal? Scale { get; set; }


		/// <summary>
		///     Whether the value is a relative position.
		/// </summary>
		public bool IsRequestRelativePosition { get; set; }


		/// <summary>
		///    Custom hash code for testing content equality - needed
		///    for hashing DeviceTypes consistently.
		/// </summary>
		/// <returns>A hash code determined by the contents of the object</returns>
		public override int GetHashCode()
		{
			var code = (31 * 17) + Function.GetHashCode();
			code = (31 * code) + IsRequestRelativePosition.GetHashCode();

			if (ReferenceUnit != null)
			{
				code = (31 * code) + ReferenceUnit.GetHashCode();
			}

			if (Scale.HasValue)
			{
				code = (31 * code) + Scale.Value.GetHashCode();
			}

			return code;
		}


		/// <summary>
		///     Overridden because GetHashCode is overridden.
		///     Implements content equality.
		/// </summary>
		/// <param name="aOther">Object to check for equality to this.</param>
		/// <returns>True if the contents are the same.</returns>
		public override bool Equals(object aOther)
		{
			if (!(aOther is ParameterUnitConversion p))
			{
				return false;
			}

			return (Scale == p.Scale) // NOTE testing float equality! Normally bad, but corresponds to comparing hashes.
				&& (Function == p.Function)
				&& (IsRequestRelativePosition == p.IsRequestRelativePosition)
				&& (ReferenceUnit == p.ReferenceUnit);
		}

		#endregion
	}
}

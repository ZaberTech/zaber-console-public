﻿namespace Zaber
{
	/// <summary>
	///     Interface for objects that provide user-facing help information for device
	///     scoped things, such as commands and settings.
	///     This library does not provide any help data; this interface exists to be implemented
	///     by applications that use the library. An interface definition is provided in order
	///     to provide a partially type-safe way to attach the data to the <see cref="ZaberDevice" />
	///     class.
	/// </summary>
	public interface IDeviceHelp
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Returns a link to the most specific help section available for a given
		///     command, given the name of same.
		/// </summary>
		/// <param name="aCommand">
		///     Name of the command to get help for.
		///     In the Binary protocol this can be the command number as a string.
		/// </param>
		/// <returns>An application-defined identifier for the location where help is to be found.</returns>
		/// <remarks>
		///     This will return NULL if there is no specific help path; caller may use
		///     <see cref="DefaultHelpPath" /> as a substitute in this case.
		/// </remarks>
		string GetCommandHelpPath(string aCommand);


		/// <summary>
		///     Returns a link to the most specific help section available for a given
		///     setting, given the name of same.
		/// </summary>
		/// <param name="aSetting">
		///     Name of the setting to get help for.
		///     In the Binary protocol this can be the return or set command number as a string.
		/// </param>
		/// <returns>An application-defined identifier for the location where help is to be found.</returns>
		/// <remarks>
		///     This will return NULL if there is no specific help path; caller may use
		///     <see cref="DefaultHelpPath" /> as a substitute in this case.
		/// </remarks>
		string GetSettingHelpPath(string aSetting);


		/// <summary>
		///     Path for generic help for this device, to be displayed when nothing specific
		///     to the current context is available.
		/// </summary>
		string DefaultHelpPath { get; }

		#endregion
	}
}

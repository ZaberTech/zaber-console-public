﻿using System.ComponentModel;

namespace Zaber
{
	/// <summary>
	///     Used to provide textual descriptions of return values.
	/// </summary>
	public class ReturnDescriptionAttribute : DescriptionAttribute
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor. Does nothing.
		/// </summary>
		public ReturnDescriptionAttribute()
		{
		}


		/// <summary>
		///     Initializes the attribute with a descrioption string.
		/// </summary>
		/// <param name="aDescription">A description of the associated return value.</param>
		public ReturnDescriptionAttribute(string aDescription)
			: base(aDescription)
		{
		}

		#endregion
	}
}

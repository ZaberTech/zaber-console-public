﻿using System;
using System.IO;

namespace Zaber.PlugIns
{
	/// <summary>
	///     Holds the details about a script template
	/// </summary>
	public class ScriptTemplate
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Create a new instance.
		/// </summary>
		/// <param name="aFileInfo">
		///     The file to read the template text from. Null is
		///     equivalent to an empty file.
		/// </param>
		public ScriptTemplate(FileInfo aFileInfo)
		{
			File = aFileInfo;
			if (null == aFileInfo)
			{
				_text = "";
			}
		}


		/// <summary>
		///     The file that the template text came from.
		/// </summary>
		public FileInfo File { get; }


		/// <summary>
		///     Get the text from the template file.
		/// </summary>
		/// <exception cref="System.Security.SecurityException">
		///     The caller does not have the
		///     required permission to read the file.
		/// </exception>
		/// <exception cref="FileNotFoundException">The file is not found.</exception>
		/// <exception cref="UnauthorizedAccessException">The path is a directory.</exception>
		/// <exception cref="DirectoryNotFoundException">
		///     The specified path is invalid,
		///     such as being on an unmapped drive.
		/// </exception>
		public string Text
		{
			get
			{
				if (null == _text)
				{
					using (var reader = File.OpenText())
					{
						_text = reader.ReadToEnd();
					}
				}

				return _text;
			}
		}

		#endregion

		#region -- Data --

		private string _text;

		#endregion
	}
}

﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Zaber.PlugIns
{
	/// <summary>
	///     Provides scripting services for statically-typed languages.
	/// </summary>
	public class CompiledScriptLanguage : ScriptLanguage
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Create a new instance.
		/// </summary>
		/// <param name="aCompilerInfo">
		///     The compiler that this language
		///     represents.
		/// </param>
		public CompiledScriptLanguage(CompilerInfo aCompilerInfo)
		{
			if (null == aCompilerInfo)
			{
				throw new ArgumentNullException(nameof(aCompilerInfo));
			}

			CompilerInfo = aCompilerInfo;
			PickName(aCompilerInfo.GetLanguages());
		}


		/// <summary>
		///     Convert a script to a plug in that can be executed.
		/// </summary>
		/// <param name="aSourceCode">The source code for the script.</param>
		/// <param name="extraAssemblies">List of user assemblies to reference.</param>
		/// <returns>The plug in that can be executed.</returns>
		/// <exception cref="ArgumentException">
		///     The compiler generated errors.
		///     The error details can be found in the
		///     <see cref="ScriptLanguage.CompilerErrors" />
		///     property. Compiler warnings do not cause the exception, but they
		///     can also be found in the
		///     <see cref="ScriptLanguage.CompilerErrors" /> property.
		/// </exception>
		public override IPlugIn CreatePlugIn(string aSourceCode, IEnumerable<Assembly> extraAssemblies = null)
		{
			CompilerErrors.Clear();
			var compiler = new ScriptCompiler(CompilerInfo);
			try
			{
				return compiler.Compile(aSourceCode, extraAssemblies);
			}
			finally
			{
				if (null != compiler.Errors)
				{
					CompilerErrors.AddRange(compiler.Errors);
				}
			}
		}


		/// <summary>
		///     Determines whether the specified System.Object is equal to the current System.Object.
		/// </summary>
		/// <param name="aOther">The System.Object to compare with the current System.Object.</param>
		/// <returns>
		///     true if the specified System.Object is equal to the current System.Object;
		///     otherwise, false.
		/// </returns>
		/// <remarks>
		///     Just compares whether the two CompiledScriptLanguages use the
		///     same compiler info.
		/// </remarks>
		public override bool Equals(object aOther)
		{
			var other = aOther as CompiledScriptLanguage;

			if (null == other)
			{
				return false;
			}

			return CompilerInfo.Equals(other.CompilerInfo);
		}


		/// <summary>
		///     Serves as a hash function for the CompiledScriptLanguage.
		/// </summary>
		/// <returns>A hash code for the CompiledScriptLanguage.</returns>
		/// <remarks>Just returns the compiler info's hash code.</remarks>
		public override int GetHashCode() => CompilerInfo.GetHashCode();


		/// <summary>
		///     The details of the compiler this language represents.
		/// </summary>
		public CompilerInfo CompilerInfo { get; }


		/// <summary>
		///     Get a list of the file extensions this language supports.
		/// </summary>
		/// <returns>
		///     A collection of strings. The strings do not start with a
		///     period.
		/// </returns>
		public override ICollection<string> UniqueExtensions
		{
			get
			{
				var uniqueExtensions = new List<string>();

				var allExtensions = CompilerInfo.GetExtensions();
				foreach (var trimmed in allExtensions
									.Select(extension => extension.TrimStart('.'))
									.Where(trimmed => !uniqueExtensions.Contains(trimmed)))
				{
					uniqueExtensions.Add(trimmed);
				}

				return uniqueExtensions;
			}
		}

		#endregion
	}
}

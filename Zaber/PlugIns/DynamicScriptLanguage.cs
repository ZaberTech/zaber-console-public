﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Scripting.Hosting;

namespace Zaber.PlugIns
{
	/// <summary>
	///     Provides scripting services for dynamically-typed languages.
	/// </summary>
	public class DynamicScriptLanguage : ScriptLanguage
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Create an instance.
		/// </summary>
		/// <param name="aScriptEngine">a script engine for the language</param>
		public DynamicScriptLanguage(ScriptEngine aScriptEngine)
		{
			_scriptEngine = aScriptEngine;
			PickName(aScriptEngine.Setup.Names);
		}


		/// <summary>
		///     Convert a script to a plug in that can be executed.
		/// </summary>
		/// <param name="aSourceCode">The source code for the script.</param>
		/// <param name="extraAssemblies">Unused.</param>
		/// <returns>The plug in that can be executed.</returns>
		public override IPlugIn CreatePlugIn(string aSourceCode, IEnumerable<Assembly> extraAssemblies = null)
		{
			CompilerErrors.Clear();

			return new DynamicPlugIn(aSourceCode, _scriptEngine);
		}


		/// <summary>
		///     Determines whether the specified System.Object is equal to the current System.Object.
		/// </summary>
		/// <param name="aOther">The System.Object to compare with the current System.Object.</param>
		/// <returns>
		///     true if the specified System.Object is equal to the current System.Object;
		///     otherwise, false.
		/// </returns>
		/// <remarks>
		///     Just compares whether the two DynamicScriptLanguages use the
		///     same script engine.
		/// </remarks>
		public override bool Equals(object aOther)
		{
			var other = aOther as DynamicScriptLanguage;

			if (null == other)
			{
				return false;
			}

			return _scriptEngine.Equals(other._scriptEngine);
		}


		/// <summary>
		///     Serves as a hash function for the DynamicScriptLanguage.
		/// </summary>
		/// <returns>A hash code for the DynamicScriptLanguage.</returns>
		/// <remarks>Just returns the script engine's hash code.</remarks>
		public override int GetHashCode() => _scriptEngine.GetHashCode();


		/// <summary>
		///     Get a list of the file extensions this language supports.
		/// </summary>
		/// <returns>
		///     A collection of strings. The strings do not start with a
		///     period.
		/// </returns>
		public override ICollection<string> UniqueExtensions
			=> new List<string>(_scriptEngine.Setup.FileExtensions.Select(ext => ext.Substring(1)));

		#endregion

		#region -- Data --

		private readonly ScriptEngine _scriptEngine;

		#endregion
	}
}

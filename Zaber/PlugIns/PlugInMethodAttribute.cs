﻿using System;

namespace Zaber.PlugIns
{
	/// <summary>
	///     Mark a plugin's method to be automatically invoked by the host
	///     program when certain events occur.
	/// </summary>
	[AttributeUsage(AttributeTargets.Method)]
	public sealed class PlugInMethodAttribute : Attribute
	{
		#region -- Public data --

		/// <summary>
		///     Event types defined by Zaber applications.
		/// </summary>
		public enum EventType
		{
			/// <summary>
			///     The plugin has been activated. This happens before it is shown.
			/// </summary>
			PluginActivated,

			/// <summary>
			///     The plugin has been deactivated. This happens after it is hidden.
			/// </summary>
			PluginDeactivated,

			/// <summary>
			///     The UI associated with the plugin has become visible.
			/// </summary>
			PluginShown,

			/// <summary>
			///     The UI associated with the plugin has been hidden.
			/// </summary>
			PluginHidden

			// Add new values to the end of this enum to avoid breaking binary compatibility.
		}

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Constructor - initializes event type.
		/// </summary>
		/// <param name="aEvent">The type of event that should invoke the associated method.</param>
		public PlugInMethodAttribute(EventType aEvent)
		{
			Event = aEvent;
		}


		/// <summary>
		///     Gets or sets the event type that should cause the associated method to be invoked.
		/// </summary>
		public EventType Event { get; }

		#endregion
	}
}

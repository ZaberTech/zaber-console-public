﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security;

namespace Zaber.PlugIns
{
	/// <summary>
	///     Searches for files within a named folder, and looks up the directory
	///     tree to find that folder if necessary.
	/// </summary>
	public class FileFinder : IFileFinder
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Creates a new FileFinder using the specified directory.
		/// </summary>
		/// <param name="aDirectory">
		///     The full path of the folder to look in
		///     for files.
		/// </param>
		public FileFinder(string aDirectory)
		{
			Directory = new DirectoryInfo(aDirectory);
		}


		/// <summary>
		///     Creates a new FileFinder using the specified directory.
		/// </summary>
		/// <param name="aDirectory">The folder to look in for files.</param>
		public FileFinder(DirectoryInfo aDirectory)
		{
			Directory = aDirectory;
		}


		/// <summary>
		///     Create a new instance of the FileFinder class, climbing the
		///     directory tree if necessary.
		/// </summary>
		/// <param name="aStartDir">The directory to start searching from.</param>
		/// <param name="aFolderName">The folder to look for.</param>
		public FileFinder(DirectoryInfo aStartDir, string aFolderName)
			: this(aStartDir, aFolderName, true)
		{
			// No additional behaviour.
		}


		/// <summary>
		///     Create a new instance of the FileFinder class, climbing the
		///     directory tree if necessary when specified.
		/// </summary>
		/// <param name="aStartDir">
		///     The directory to start searching from,
		///     or to simply look in if <paramref name="aClimb" /> is false.
		/// </param>
		/// <param name="aFolderName">The folder to look for.</param>
		/// <param name="aClimb">
		///     If true, "climb up" the directory structure
		///     looking for <paramref name="aFolderName" />. If false, only check in
		///     <paramref name="aStartDir" />.
		/// </param>
		public FileFinder(DirectoryInfo aStartDir, string aFolderName, bool aClimb)
		{
			var currentDirectory = aStartDir;
			do
			{
				var matches = currentDirectory.GetDirectories(aFolderName);
				if (matches.Length > 0)
				{
					Directory = matches[0];
					return;
				}


				if (aClimb)
				{
					try
					{
						currentDirectory = currentDirectory.Parent;
					}
					catch (SecurityException)
					{
						// If we aren't allowed to go any higher, give up.
						break;
					}
				}
				else
				{
					break;
				}
			} while (null != currentDirectory);

			Directory = null;
		}


		/// <summary>
		///     Find the default file in the folder.
		/// </summary>
		/// <param name="aLanguage">
		///     Specifies the file extensions you want to
		///     look for
		/// </param>
		/// <returns>The requested file or null if none could be found.</returns>
		/// <remarks>
		///     We look for files that are named Default.* where .* is one
		///     of the extensions supported by the language.
		/// </remarks>
		public FileInfo FindDefaultFile(ScriptLanguage aLanguage)
		{
			if (null == Directory)
			{
				return null;
			}

			var allMatches = new List<FileInfo>();

			foreach (var extension in aLanguage.UniqueExtensions)
			{
				var matches = Directory.GetFiles("Default." + extension);
				allMatches.AddRange(matches);
			}

			switch (allMatches.Count)
			{
				case 0:
					return null;
				case 1:
					return allMatches[0];
			}

			allMatches.Sort(delegate(FileInfo a, FileInfo b)
			{
				return string.Compare(a.FullName,
									  b.FullName,
									  StringComparison.CurrentCultureIgnoreCase);
			});

			return allMatches[0];
		}


		/// <summary>
		///     Find the named file in the folder.
		/// </summary>
		/// <param name="aFilename">The name of the file to find. It may contain wild cards.</param>
		/// <returns>The requested file, or null if it could not be found</returns>
		/// <remarks>
		///     We search the folder for the named file. If
		///     more than one file matches the request, the first one is returned.
		/// </remarks>
		public FileInfo FindFile(string aFilename)
		{
			if ((null == Directory) || !System.IO.Directory.Exists(Directory.FullName))
			{
				return null;
			}

			var matches = Directory.GetFiles(aFilename);
			return matches.Length > 0 ? matches[0] : null;
		}


		/// <summary>
		///     Gets the directory that this finder is using.
		/// </summary>
		public DirectoryInfo Directory { get; }

		#endregion

		#region -- Data --

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;

namespace Zaber.PlugIns
{
	/// <summary>
	///     An interface that plug ins can use to pass data between them.
	/// </summary>
	/// <remarks>
	///     If one plug in wants to send data to another one, it can declare a
	///     property of type IPlugInManager. Then it calls manager.SetProperty()
	///     to pass data to any plug ins with properties of that type. If you want
	///     to have more than one property of the same type, use names for the
	///     properties.
	/// </remarks>
	public interface IPlugInManager
	{
		#region -- Events --

		/// <summary>
		///     Event invoked by the Plugin Manager when the user elects to restore all
		///     user settings to default values. Plugins should reset all their
		///     custom user settings to default values when this is invoked.
		/// </summary>
		event EventHandler SettingsResetEvent;

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Set a property of the given type on any plug ins that have one.
		/// </summary>
		/// <param name="aValue">The value to pass to each plug in.</param>
		/// <param name="aType">
		///     The type of property to set. Any properties of
		///     this type or its subtypes will be set.
		/// </param>
		void SetProperty(object aValue, Type aType);


		/// <summary>
		///     Set a property of the given type and name on any plug ins that
		///     have one.
		/// </summary>
		/// <param name="aValue">The value to pass to each plug in.</param>
		/// <param name="aType">
		///     The type of property to set. Any properties of
		///     this type or its subtypes will be set.
		/// </param>
		/// <param name="aName">
		///     The name of the property. This will match the
		///     name declared in the <see cref="PlugInPropertyAttribute" />. The
		///     default name is null.
		/// </param>
		void SetProperty(object aValue, Type aType, string aName);


		/// <summary>
		///     Invoke plugin methods associated with a given program event type.
		///     This will invoke all public instance methods on all plugins that
		///     are tagged with the PlugInMethodAttribute, provided the attribute's
		///     Event value matches aEventType.
		/// </summary>
		/// <param name="aEventType">The type of event that is occurring.</param>
		void InvokeMethod(PlugInMethodAttribute.EventType aEventType);


		/// <summary>
		///     Display the tab for a given plugin type, if it is not currently
		///     the active tab.
		/// </summary>
		/// <param name="aPluginType">Type of the plugin class whose tab should be shown.</param>
		/// <param name="aFocus">Whether to focus the tab after adding it. Defaults to true.</param>
		void ShowPluginTab(Type aPluginType, bool aFocus = true);


		/// <summary>
		///     Display the tab for a given plugin type, if it is not currently
		///     the active tab.
		/// </summary>
		/// <param name="aPluginName">Tab name of the plugin class whose tab should be shown.</param>
		/// <param name="aFocus">Whether to focus the tab after adding it. Defaults to true.</param>
		void ShowPluginTab(string aPluginName, bool aFocus = true);


		/// <summary>
		///     Find out what plugin types are currently active in the user interface. This is the
		///     list of plugin types that are currently activated and have user interfaces
		///     in the docking manager. It does not indicate whether or not they are visible;
		///     see <see cref="GetVisiblePluginTypes" />.
		/// </summary>
		/// <returns>Collection of active plugins' types.</returns>
		IEnumerable<Type> GetActivePluginTypes();


		/// <summary>
		///     Find out what plugin types are currently displayed to the user. This is the
		///     list of plugin types that are currently activated and have user interfaces
		///     that are not currently hidden. There will be one for the active tab in the
		///     main window, and possible more for active tabs in floating windows. This is
		///     a subset of active tabs; see <see cref="GetActivePluginTypes" />.
		/// </summary>
		/// <returns>Collection of visible plugins' types.</returns>
		IEnumerable<Type> GetVisiblePluginTypes();


		/// <summary>
		///     Scan all known plugins for instantiable classes that are assignable to
		///     the given type. Normally used to find plugged-in implementations of an
		///     interface. This also scans the main program assemblies.
		/// </summary>
		/// <param name="aInterfaceType">Type to find implementations of.</param>
		/// <returns>
		///     Zero or more types that are instantiable and are assignable
		///     to the specified type. Each distinct type is guaranteed to appear only
		///     once. Ordering is not specified.
		/// </returns>
		IEnumerable<Type> FindConcreteImplementationsOf(Type aInterfaceType);


		/// <summary>
		///     Display a passive status message in the program window.
		///     Each plugin can only have one message active - call again with
		///     a null or empty string to clear the message earlier than scheduled.
		/// </summary>
		/// <param name="aOwner">Key for the message - can be used to clear it ahead of schedule.</param>
		/// <param name="aMessage">Message to display, or null to clear.</param>
		/// <param name="aDuration">
		///     Length of time to display the message for, in seconds.
		///     Values less than 1 will be set to 1, unless the message is null.
		/// </param>
		void ShowStatusText(object aOwner, string aMessage, int aDuration = 0);

		#endregion
	}
}

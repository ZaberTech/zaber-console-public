﻿using System;

namespace Zaber.PlugIns
{
	/// <summary>
	///     Mark a class as a plug in to be used in a Zaber library client program.
	/// </summary>
	/// <remarks>
	///     If the plugin class derives from ZaberWpfToolbox.ObservableObject, ZaberConsole.Plugins.PluginTabVM
	///     or System.Windows.Forms.Control (deprecated), Zaber Console will display the plugin as a tab if
	///     the user selects it.
	/// </remarks>
	[AttributeUsage(AttributeTargets.Class)]
	public sealed class PlugInAttribute : Attribute
	{
		#region -- Public data --

		/// <summary>
		///     Used by WPF plugins to identify their resource dictionary types.
		/// </summary>
		public const string PLUGIN_RESOURCE_DICTIONARY = "PluginResourceDictionary";

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor. Ses default values for all properties.
		/// </summary>
		public PlugInAttribute()
		{
			AlwaysActive = false;
			Dockable = true;
		}


		/// <summary>
		///     Naming constructor. Sets the plugin name and defaults other properties.
		/// </summary>
		/// <param name="aName"></param>
		public PlugInAttribute(string aName)
			: this()
		{
			Name = aName;
		}


		/// <summary>
		///     Declare a name to be used for display in the client program. If not set, the
		///     class name will be used.
		/// </summary>
		public string Name { get; set; }


		/// <summary>
		///     Provide a short description of what the plugin is for or what it does. Optional.
		/// </summary>
		public string Description { get; set; }


		/// <summary>
		///     If true, at least one instance of the associated plugin must always be instantiated,
		///     and visible if it has a user interface. Defaults to false.
		/// </summary>
		public bool AlwaysActive { get; set; }


		/// <summary>
		///     If true, the user interface tab for this plugin can be undocked and made into a floating
		///     window. Defaults to true.
		/// </summary>
		public bool Dockable { get; set; }

		#endregion
	}
}

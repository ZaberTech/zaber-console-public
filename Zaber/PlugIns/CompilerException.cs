﻿using System;

namespace Zaber.PlugIns
{
	/// <summary>
	///     Thrown when a script or plug-in could not be compiled.
	/// </summary>
	/// <remarks>
	///     This exception wraps an inner exception. Use the
	///     InnerException property to access it.
	/// </remarks>
	public class CompilerException : Exception
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Creates a new instance of the CompilerException class.
		/// </summary>
		/// <param name="aMessage">A message associated with the error.</param>
		/// <param name="aInnerException">
		///     The exception which caused the error.
		/// </param>
		public CompilerException(string aMessage, Exception aInnerException)
			: base(aMessage, aInnerException)
		{
		}


		/// <summary>
		///     Optional list of line numbers that have compile errors on them.
		/// </summary>
		public int[] ErrorLineNumbers { get; set; }

		#endregion
	}
}

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using IronPython.Hosting;

namespace Zaber.PlugIns
{
	/// <summary>
	///     Represents a language compiler that can be used for scripting.
	///     This is an abstract class and cannot be instantiated directly.
	/// </summary>
	public abstract class ScriptLanguage
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Constructor for subclasses to use. Initializes members to default values.
		/// </summary>
		protected ScriptLanguage()
		{
			CompilerErrors = new CompilerErrorCollection();
		}


		/// <summary>
		///     Find all languages installed on this computer.
		/// </summary>
		/// <returns>A collection of <see cref="ScriptLanguage" /> objects.</returns>
		public static ICollection<ScriptLanguage> FindAll()
		{
			var languages = new Dictionary<string, ScriptLanguage>();

			var compilers = CodeDomProvider.GetAllCompilerInfo();
			foreach (var compiler in compilers)
			{
				if (compiler.IsCodeDomProviderTypeValid)
				{
					var languageNames = string.Join(";", compiler.GetLanguages());
					languages[languageNames] = new CompiledScriptLanguage(compiler);
				}
			}

			var languageList = new List<ScriptLanguage>(languages.Values);
			foreach (var language in _dynamicExtensions.Values.Where(language => !languageList.Contains(language)))
			{
				languageList.Add(language);
			}

			languageList.Sort((a, b) => string.Compare(a.Name, b.Name, StringComparison.CurrentCultureIgnoreCase));

			return languageList;
		}


		/// <summary>
		///     Convert a script to a plug in that can be executed.
		/// </summary>
		/// <param name="sourceCode">The source code for the script.</param>
		/// <param name="extraAssemblies">list of user assemblies referenced by the script.</param>
		/// <returns>The plug in that can be executed.</returns>
		/// <exception cref="ArgumentException">
		///     The compiler generated errors.
		///     The error details can be found in the <see cref="CompilerErrors" />
		///     property. Compiler warnings do not cause the exception, but they
		///     can also be found in the <see cref="CompilerErrors" /> property.
		/// </exception>
		public abstract IPlugIn CreatePlugIn(string sourceCode, IEnumerable<Assembly> extraAssemblies = null);


		/// <summary>
		///     Find the scripting language that compiles a given file type.
		/// </summary>
		/// <param name="file">The file whose extension will be used.</param>
		/// <returns>The scripting language that compiles the file type.</returns>
		public static ScriptLanguage FindByExtension(FileInfo file)
		{
			if (null == file)
			{
				throw new ArgumentNullException(nameof(file));
			}

			try
			{
				return FindByExtension(file.Extension);
			}
			catch (ArgumentException)
			{
				return FindByExtension(file.Extension.ToLower());
			}
		}


		/// <summary>
		///     Find the scripting language that compiles a given file type.
		/// </summary>
		/// <param name="aExtension">The extension part of the file name, with leading period.</param>
		/// <returns>The scripting language that compiles the file type.</returns>
		public static ScriptLanguage FindByExtension(string aExtension)
		{
			if (_dynamicExtensions.TryGetValue(aExtension, out var language))
			{
				return language;
			}

			if (CodeDomProvider.IsDefinedExtension(aExtension))
			{
				var languageName = CodeDomProvider.GetLanguageFromExtension(aExtension);
				var compiler = CodeDomProvider.GetCompilerInfo(languageName);
				return new CompiledScriptLanguage(compiler);
			}

			throw new ArgumentException(string.Format(CultureInfo.CurrentCulture,
													  "No compiler found for file type {0}",
													  aExtension));
		}


		/// <summary>
		///     The name of this language
		/// </summary>
		public string Name { get; private set; }


		/// <summary>
		///     A file filter for use with file dialogs.
		/// </summary>
		public string FileFilter
		{
			get
			{
				var extensionList = new StringBuilder();
				var extensions = UniqueExtensions;

				foreach (var extension in extensions)
				{
					if (extensionList.Length > 0)
					{
						extensionList.Append(";");
					}

					extensionList.AppendFormat("*.{0}", extension);
				}

				return string.Format(CultureInfo.CurrentCulture,
									 "{0} files ({1})|{2}",
									 Name,
									 extensionList,
									 extensionList);
			}
		}


		/// <summary>
		///     Get a list of the file extensions this language supports.
		/// </summary>
		/// <returns>
		///     A collection of strings. The strings do not start with a
		///     period.
		/// </returns>
		public abstract ICollection<string> UniqueExtensions { get; }


		/// <summary>
		///     Gets a collection of compiler errors from the last call to
		///     <see cref="CreatePlugIn" />.
		/// </summary>
		public CompilerErrorCollection CompilerErrors { get; }

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Select the best language name to display.
		/// </summary>
		/// <param name="aLanguageNames">Possible names for this language.</param>
		protected void PickName(IEnumerable<string> aLanguageNames)
		{
			foreach (var languageName in aLanguageNames)
			{
				if ((null == Name) || (languageName.Length > Name.Length))
				{
					Name = languageName;
				}
			}
		}


		/// <summary>
		///     Load the dynamic language extensions map.
		/// </summary>
		/// <returns>
		///     A mapping from file extensions to the languages that use
		///     them.
		/// </returns>
		private static Dictionary<string, DynamicScriptLanguage> LoadDynamicExtensions()
		{
			var extensions = new Dictionary<string, DynamicScriptLanguage>();
			try
			{
				var python = LoadPython();
				foreach (var extension in python.UniqueExtensions)
				{
					extensions["." + extension] = python;
				}
			}
			catch (FileNotFoundException)
			{
				// Dlls for IronPython are missing, just don't include it.
			}

			return extensions;
		}


		/// <summary>
		///     Try to load the Python language if the necessary dlls are present.
		/// </summary>
		private static DynamicScriptLanguage LoadPython() => new DynamicScriptLanguage(Python.CreateEngine());

		#endregion

		#region -- Data --

		private static readonly Dictionary<string, DynamicScriptLanguage> _dynamicExtensions = LoadDynamicExtensions();

		#endregion
	}
}

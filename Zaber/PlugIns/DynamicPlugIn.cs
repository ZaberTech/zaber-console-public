﻿using System;
using System.Globalization;
using System.IO;
using System.Text;
using IronPython.Runtime.Exceptions;
using IronPython.Runtime.Operations;
using Microsoft.Scripting;
using Microsoft.Scripting.Hosting;

namespace Zaber.PlugIns
{
	/// <summary>
	///     An adapter from the IPlugIn interface to a dynamic language script.
	/// </summary>
	public class DynamicPlugIn : IPlugIn
	{
		#region -- Public Methods & Properties --

		#region -- Setup --

		/// <summary>
		///     Create a new instance.
		/// </summary>
		/// <param name="aSourceCode">the source code to run</param>
		/// <param name="aScriptEngine">
		///     the script engine to run the script
		///     with
		/// </param>
		public DynamicPlugIn(string aSourceCode, ScriptEngine aScriptEngine)
		{
			_sourceCode = aSourceCode;
			_scriptEngine = aScriptEngine;
		}

		#endregion

		#endregion

		#region -- IPlugIn implementation --

		/// <summary>
		///     Run the plug in's operation.
		/// </summary>
		/// <remarks>
		///     The method returns when the operation has completed. Exceptions in the
		///     operation will be thrown from this method.
		/// </remarks>
		public void Run()
		{
			var initSource = _scriptEngine.CreateScriptSourceFromString(@"
import clr
import System.Threading
clr.AddReference('Zaber')
from Zaber import (Command, DeviceListener, DeviceModes, Measurement,
                   MeasurementType, UnitOfMeasure, ZaberError, ZaberPortError,
                   ZaberPortFacadeState)

def sleep(milliseconds):
    System.Threading.Thread.Sleep(milliseconds)

is_canceled = False
def cancel():
    global is_canceled
    is_canceled = True
    if port_facade.Port.IsAsciiMode:
        port_facade.Port.Send('stop')
    else:
        port_facade.Port.Send(0, Command.Stop, 0)
");
			var source = _scriptEngine.CreateScriptSourceFromString(_sourceCode);
			_scriptScope = _scriptEngine.CreateScope();
			_scriptScope.SetVariable("conversation", Conversation);
			_scriptScope.SetVariable("port_facade", PortFacade);

			initSource.Execute(_scriptScope);
			try
			{
				source.Execute(_scriptScope);
			}
			catch (SystemExitException)
			{
				// Nothing to do, this is a valid way to exit the script.
			}
			catch (SyntaxErrorException syntaxException)
			{
				var ce = new CompilerException(syntaxException.Message, null)
				{
					ErrorLineNumbers = new[] { syntaxException.Line - 1 }
				};

				throw new ArgumentException(string.Format(CultureInfo.CurrentCulture,
														  "{0} (line {1}, column {2})",
														  syntaxException.Message,
														  syntaxException.Line,
														  syntaxException.Column),
											ce);
			}
			catch (Exception aException)
			{
				var frames = PythonOps.GetDynamicStackFrames(aException);
				if ((null != frames) && (frames.Length > 0))
				{
					var ce = new CompilerException(aException.Message, null)
					{
						ErrorLineNumbers = new[] { frames[0].GetFileLineNumber() - 1 }
					};

					throw new ArgumentException(string.Format(CultureInfo.CurrentCulture,
															  "{0} (line {1})",
															  aException.Message,
															  frames[0].GetFileLineNumber()),
												ce);
				}

				throw;
			}
		}


		/// <summary>
		///     Ask the operation to stop running.
		/// </summary>
		/// <remarks>
		///     <para>
		///         This is just a request, plug ins do not have to respect it. A common
		///         implementation is to set an IsCancelled flag and leave it up to
		///         the running code to check the flag at regular intervals.
		///     </para>
		///     <para>
		///         If a plug in does abort its operation it can either return
		///         normally, or throw an exception.
		///         <see cref="System.OperationCanceledException" /> is a good exception
		///         to throw in this case.
		///     </para>
		/// </remarks>
		public void Cancel()
		{
			var cancelSource = _scriptEngine.CreateScriptSourceFromString(@"cancel()");
			cancelSource.Execute(_scriptScope);
		}


		/// <summary>
		///     Gets or sets the port facade that gives access to all the devices
		///     and conversations.
		/// </summary>
		/// <remarks>
		///     The port facade may or may not be open when it is passed to this
		///     property. Scripts can check by using the
		///     <see cref="ZaberPortFacade.IsOpen" /> property.
		///     The plug in does not own this facade and will not dispose it
		///     during Dispose().
		/// </remarks>
		public ZaberPortFacade PortFacade { get; set; }


		/// <summary>
		///     Gets or sets the conversation that was selected by the user.
		/// </summary>
		/// <value>
		///     May be null if none was selected, or if the port hasn't been opened
		///     yet.
		/// </value>
		/// <remarks>
		///     The plug in does not own this conversation and will not dispose it
		///     during Dispose().
		/// </remarks>
		public Conversation Conversation { get; set; }


		/// <summary>
		///     Gets or sets a text reader that will provide input to the script.
		/// </summary>
		/// <remarks>
		///     The plug in owns this reader and will dispose it during
		///     Dispose().
		/// </remarks>
		public TextReader Input
		{
			get => _input;
			set
			{
				_input = value;

				// This ignores any binary output and just displays text.
				var ignoredBinaryInput = new MemoryStream();
				_scriptEngine.Runtime.IO.SetInput(ignoredBinaryInput, _input, Encoding.UTF8);
			}
		}


		/// <summary>
		///     Gets or sets a text writer that will receive output from the script.
		/// </summary>
		/// <remarks>
		///     The plug in owns this writer and will dispose it during
		///     Dispose().
		/// </remarks>
		public TextWriter Output
		{
			get => _output;
			set
			{
				_output = value;

				// This ignores any binary output and just displays text.
				var ignoredBinaryOutput = new MemoryStream();
				_scriptEngine.Runtime.IO.SetOutput(ignoredBinaryOutput, _output);
				_scriptEngine.Runtime.IO.SetErrorOutput(ignoredBinaryOutput, _output);
			}
		}

		#endregion

		#region -- IDisposable implementation --

		/// <summary>
		///     Performs application-defined tasks associated with freeing, releasing, or
		///     resetting unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);

			// not necessary here, but might be for derived types.
			GC.SuppressFinalize(this);
		}


		/// <summary>
		///     Performs application-defined tasks associated with freeing, releasing, or
		///     resetting unmanaged resources.
		/// </summary>
		/// <param name="aIsDisposing">
		///     True if the object is being disposed, and not
		///     garbage collected.
		/// </param>
		protected virtual void Dispose(bool aIsDisposing)
		{
			// Nothing to dispose.
		}

		#endregion

		#region -- Data --

		private readonly string _sourceCode;
		private readonly ScriptEngine _scriptEngine;
		private ScriptScope _scriptScope;
		private TextReader _input;
		private TextWriter _output;

		#endregion
	}
}

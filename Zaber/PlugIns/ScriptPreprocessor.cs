﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Scripting.Runtime;

namespace Zaber.PlugIns
{
	/// <summary>
	///     Does preprocessing on script text, including substituting templates and
	///     #using directives.
	/// </summary>
	public class ScriptPreprocessor
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Sets the language to be used for script preprocessing.
		/// </summary>
		/// <param name="aLanguage">
		///     The detected or user-selected language of the scripts
		///     to be processed.
		/// </param>
		public ScriptPreprocessor([NotNull] ScriptLanguage aLanguage)
		{
			_language = aLanguage ?? throw new ArgumentNullException(nameof(aLanguage));
		}


		/// <summary>
		///     Identifies the name of the template to be used with a script. The caller is responsible
		///     for locating and loading the template with this name before calling
		///     <see cref="Preprocess" />.
		/// </summary>
		/// <param name="aScript">The original text of the script.</param>
		/// <returns>The name of the template to use, or null if none is specified.</returns>
		public string GetTemplateName(string aScript)
		{
			if (null == aScript)
			{
				return null;
			}

			var match = FindTemplateDirective(aScript);
			return match.Success ? match.Groups[1].Value : null;
		}


		/// <summary>
		///     Convert the raw text of a string into the text to be compiled. The caller is
		///     responsible for passing in the text of the template identified by
		///     <see cref="GetTemplateName" />. This will remove all preprocessor directives from
		///     the text, perform template substitution, and replace all #using directives with
		///     import statements appropriate to the language. The caller must check
		///     <see cref="ExtraAssemblies" /> afterward to see if any additional user-specified
		///     assemblies must be loaded before compiling the script.
		/// </summary>
		/// <param name="aScript">The raw text of the script.</param>
		/// <param name="aTemplate">The text of the template, if any.</param>
		/// <returns>Fully processed script text, ready to compile.</returns>
		/// <remarks>The output text always uses CR-LF line endings.</remarks>
		public string Preprocess(string aScript, string aTemplate)
		{
			LineNumberMap.Clear();

			// Text formatting is sensitive to line endings. Force line endings
			// to always be CR-LFs.
			aScript = FixLineEndings(aScript);
			aTemplate = FixLineEndings(aTemplate);

			// Perform template substitution first, since templates
			// could potentially contain other directives.
			ReadScriptAndSubstituteTemplate(aScript, aTemplate);

			// Scan for and remove any #import statements.
			FindAndRemoveImportDirectives();

			// Scan for and remove and #using statements.
			FindAndRemoveUsingDirectives();

			// Insert custom usings.
			if (_usings.Count > 0)
			{
				InsertUsings();
			}

			// Build the line number map and flatten the code into a single string again.
			var newCode = new StringBuilder();
			var lineNo = 0;
			foreach (var line in _linesOfCode)
			{
				newCode.Append(line.Item1 + "\r\n");

				// Note about the string constant above: AppendLine and Environment.NewLine produce
				// inconsistent results between Windows versions. On some builds of Windows 10,
				// Environment.NewLine is "\r".
				LineNumberMap[lineNo] = line.Item2;
				lineNo++;
			}

			return newCode.ToString();
		}


		/// <summary>
		///     Paths or names of assemblies referenced in #using preprocessor directives.
		///     This data is only valid after <see cref="Preprocess" /> has been called.
		/// </summary>
		public IEnumerable<string> ExtraAssemblies { get; private set; } = new string[0];


		/// <summary>
		///     Maps line numbers in compiled script code to line numbers in user-entered code.
		///     This data is only valid after <see cref="Preprocess" /> has been called.
		///     User code line numbers are positive and 0-based. Template line numbers
		///     are negative and start at -1.
		/// </summary>
		public IDictionary<int, int> LineNumberMap { get; } = new Dictionary<int, int>();

		#endregion

		#region -- Non-Public Methods --

		private void ReadScriptAndSubstituteTemplate(string aScript, string aTemplate)
		{
			var sourceCode = new StringBuilder();
			var isMarkerFound = false;
			string line;

			// Split the user code into parts before and after the #template directive.
			var templateMatch = FindTemplateDirective(aScript);
			var before = aScript ?? string.Empty;
			var after = string.Empty;
			var templateDirectiveLineCount = 0;
			if (templateMatch.Success)
			{
				before = aScript.Substring(0, templateMatch.Index);
				after = aScript.Substring((templateMatch.Index + templateMatch.Length)
									  - 1); // -1 is because $ matches one \r or \n.
				var directive = aScript.Substring(templateMatch.Index, templateMatch.Length - 1);
				templateDirectiveLineCount = Regex.Matches(directive, "[\r\n]{1,2}").Count;
			}

			// Read the user code into a list of strings, keeping track of the original line numbers.
			_linesOfCode = new List<Tuple<string, int>>();
			var lineNo = 0;
			var scriptReader = new StringReader(before);
			while (null != (line = scriptReader.ReadLine()))
			{
				_linesOfCode.Add(new Tuple<string, int>(line.TrimEnd(), lineNo));
				lineNo++;
			}

			// Bump line numbers to account for any lines withing the #template directive, since
			// it is allowed to span multiple lines.
			lineNo += templateDirectiveLineCount;

			// Read the remainder of the code after the directive.
			scriptReader = new StringReader(after);
			while (null != (line = scriptReader.ReadLine()))
			{
				_linesOfCode.Add(new Tuple<string, int>(line.TrimEnd(), lineNo));
				lineNo++;
			}

			// Read the template and look for the place to substitute the user code.
			if (!string.IsNullOrEmpty(aTemplate))
			{
				var originalLines = _linesOfCode;
				_linesOfCode = new List<Tuple<string, int>>();
				lineNo = 1;

				var templateReader = new StringReader(aTemplate);
				while (null != (line = templateReader.ReadLine()))
				{
					if (line.Contains(TEMPLATE_CONTENT_MARKER))
					{
						isMarkerFound = true;

						// Note the entire template line with the marker gets replaced,
						// so there's no extracting substrings from that line here.
						_linesOfCode.AddRange(originalLines);
					}
					else // Output template line verbatim.
					{
						_linesOfCode.Add(
							new Tuple<string, int>(line.TrimEnd(),
												   -lineNo)); // Negative line numbers mean template lines.
					}

					lineNo++;
				}

				if (!isMarkerFound)
				{
					throw new ArgumentException("Template does not contain " + TEMPLATE_CONTENT_MARKER);
				}
			}
		}


		/// <summary>
		///     Removes #import directives from the script and stores their content in
		///     <see cref="ExtraAssemblies" />.
		/// </summary>
		private void FindAndRemoveImportDirectives()
		{
			var extraAssemblies = new List<string>();
			var namesSeen = new HashSet<string>();
			var i = 0;
			while (i < _linesOfCode.Count)
			{
				var match = IMPORT_REGEX.Match(_linesOfCode[i].Item1);
				if (match.Success)
				{
					var asmPath = match.Groups[1].Captures[0].ToString();

					// Add assembly names in order specified, but only if unique.
					if (!string.IsNullOrEmpty(asmPath) && !namesSeen.Contains(asmPath))
					{
						extraAssemblies.Add(asmPath);
						namesSeen.Add(asmPath);
					}

					_linesOfCode.RemoveAt(i);
				}
				else
				{
					i++;
				}
			}

			ExtraAssemblies = extraAssemblies;
		}


		/// <summary>
		///     Removes #using directives from the script and records their content.
		/// </summary>
		private void FindAndRemoveUsingDirectives()
		{
			_usings = new List<Tuple<string, int>>();
			var i = 0;

			while (i < _linesOfCode.Count)
			{
				var match = USING_REGEX.Match(_linesOfCode[i].Item1);
				if (match.Success)
				{
					var nameSpace = match.Groups[1].Captures[0].ToString();

					// Just store all namespaces seen - duplicates will be removed later.
					if (!string.IsNullOrEmpty(nameSpace))
					{
						_usings.Add(new Tuple<string, int>(nameSpace, _linesOfCode[i].Item2));
					}

					_linesOfCode.RemoveAt(i);
				}
				else
				{
					i++;
				}
			}
		}


		/// <summary>
		///     Inserts using directives in language-appropriate formatting for each #using directive.
		///     The inserted directives are placed after the last one found in the code, if any,
		///     or else at the start of the file.
		/// </summary>
		private void InsertUsings()
		{
			var namespacesSeen = new HashSet<string>();
			var insertIndex = 0;

			var re = _usingRegexes[_language.Name];
			var format = _usingFormats[_language.Name];

			// Scan the code for any existing import directives,
			// note the namespaces used so we don't duplicate them,
			// and track the line number where one was last seen to 
			// be the insert point for new ones.
			var lineIndex = 0;
			foreach (var line in _linesOfCode)
			{
				var match = re.Match(line.Item1);
				if (match.Success)
				{
					var nameSpace = match.Groups[1].Captures[0].ToString();
					if (!string.IsNullOrEmpty(nameSpace))
					{
						namespacesSeen.Add(nameSpace);
						insertIndex = lineIndex + 1;
					}
				}

				lineIndex++;
			}


			// Now insert new using directives at the insertion point, avoiding duplicates.
			foreach (var use in _usings)
			{
				var nameSpace = use.Item1;
				if (!namespacesSeen.Contains(nameSpace))
				{
					namespacesSeen.Add(nameSpace);
					var newDirective = string.Format(format, nameSpace);
					var newLine = new Tuple<string, int>(newDirective, use.Item2);
					_linesOfCode.Insert(insertIndex, newLine);
					insertIndex++;
				}
			}
		}


		/// <summary>
		///     Finds the #template directive in a script.
		/// </summary>
		/// <param name="aScript">The raw text of the script.</param>
		/// <returns>A regex match either locating the directive or not, if there isn't one.</returns>
		private Match FindTemplateDirective(string aScript) => TEMPLATE_REGEX.Match(aScript);


		/// <summary>
		///     Generates an appropriate import or using statement for the language.
		/// </summary>
		/// <param name="aModuleName">User-specified name of the namespace to use.</param>
		/// <returns>A "using" string appropriate to the current script language.</returns>
		private string EmitImportStatement(string aModuleName)
		{
			if (_usingFormats.TryGetValue(_language?.Name.ToLower() ?? "unknown", out var formatStr))
			{
				return string.Format(formatStr, aModuleName);
			}

			throw new InvalidDataException("#using directives are not supported in the current script language.");
		}


		/// <summary>
		///     Returns a language-appropriate regular expression for detecting import statements.
		/// </summary>
		/// <returns>A regular expression.</returns>
		private Regex GetImportDetectionRegex()
		{
			if (_usingRegexes.TryGetValue(_language?.Name.ToLower() ?? "unknown", out var re))
			{
				return re;
			}

			throw new InvalidDataException("#using directives are not supported in the current script language.");
		}


		private static string FixLineEndings(string aStr)
		{
			var str = aStr;

			if (!string.IsNullOrEmpty(str))
			{
				str = str
					.Replace("\r\n", "\n")  // Can now distinguish non-Windows endings.
					.Replace("\r", "\n")    // Handle multi-line Unix endings.
					.Replace("\n", "\r\n"); // Return to Windows style.
			}

			return str;
		}

		#endregion

		#region -- Data --

		// Template files contain this text where user-authored code should be inserted.
		private const string TEMPLATE_CONTENT_MARKER = "$INSERT-SCRIPT-HERE$";

		private const string TEMPLATE_PATTERN =
			@"^[ \t]*" // Any leading space at start of line.
		+ @"#template\(" // Directive marker and opening parenthesis.
		+ @"([^)\r\n]+)" // Capture name of template until closing parenthesis.
		+ @"\).*$"; // Any carriage return and/or line feed at the end of the line.

		// Regular expression for finding #template directives.
		private static readonly Regex TEMPLATE_REGEX =
			new Regex(TEMPLATE_PATTERN, RegexOptions.Multiline | RegexOptions.Compiled);

		private const string IMPORT_PATTERN =
			@"^[ \t]*" // Any leading space at start of line.
		+ @"#import\(""?" // Directive marker, opening parenthesis and quote.
		+ @"([^\r\n""]+)" // Capture name of assembly until closing parenthesis.
		+ @"""?\).*$"; // Closing quote and parenthesis and any junk on the rest of the line.

		// Regular expression for finding #import directives.
		private static readonly Regex IMPORT_REGEX = new Regex(IMPORT_PATTERN, RegexOptions.Compiled);


		private const string USING_PATTERN =
			@"^[ \t]*" // Any leading space at start of line.
		+ @"#using\(""?" // Directive marker, opening parenthesis and quote.
		+ @"([^\r\n""]+)" // Capture name of namespace until closing parenthesis.
		+ @"""?\).*$"; // Closing quote and parenthesis and any junk on the rest of the line.

		// Regular expression for finding #import directives.
		private static readonly Regex USING_REGEX = new Regex(USING_PATTERN, RegexOptions.Compiled);


		// Format strings for outputting using statements for each language.
		private static readonly IDictionary<string, string> _usingFormats = new Dictionary<string, string>
		{
			{ "csharp", "using {0};" },
			{ "javascript", "import {0}" },
			{ "visualbasic", "Imports {0}" },
			{ "c++", "#using \"{0}\"" }
		};


		// Regular expressions for locating existing using statements for each language.
		private static readonly IDictionary<string, Regex> _usingRegexes = new Dictionary<string, Regex>
		{
			{
				"csharp",
				new Regex(@"^[ \t]*using[ \t]+(\S)+[ \t]*;[ \t]*\r?\n?", RegexOptions.Multiline | RegexOptions.Compiled)
			},
			{
				"javascript",
				new Regex(@"^[ \t]*import[ \t]+(\S)+[ \t]*\r?\n?", RegexOptions.Multiline | RegexOptions.Compiled)
			},
			{
				"visualbasic",
				new Regex(@"^[ \t]*Imports[ \t]+(\S)+[ \t]*\r?\n?", RegexOptions.Multiline | RegexOptions.Compiled)
			},
			{
				"c++",
				new Regex(@"^[ \t]*#using[ \t]+\""(\S)+\""[ \t]*\r?\n?", RegexOptions.Multiline | RegexOptions.Compiled)
			}
		};

		private readonly ScriptLanguage _language;
		private List<Tuple<string, int>> _linesOfCode;
		private List<Tuple<string, int>> _usings;

		#endregion
	}
}

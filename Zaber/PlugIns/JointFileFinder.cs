﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Zaber.PlugIns
{
	/// <summary>
	///     Uses multiple IFileFinder to pretend to be IFileFinder itself.
	/// </summary>
	public class JointFileFinder : IFileFinder
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Creates a new JointFileFinder.
		/// </summary>
		/// <param name="aFinders">Finders to be joint.</param>
		public JointFileFinder(IEnumerable<IFileFinder> aFinders)
		{
			_finders = aFinders.ToArray();
		}


		/// <summary>
		///     Find the default file in the folder.
		/// </summary>
		/// <param name="aLanguage">
		///     Specifies the file extensions you want to
		///     look for
		/// </param>
		/// <returns>The requested file or null if none could be found.</returns>
		/// <remarks>
		///     We look for files that are named Default.* where .* is one
		///     of the extensions supported by the language.
		/// </remarks>
		public FileInfo FindDefaultFile(ScriptLanguage aLanguage)
		{
			foreach (var finder in _finders)
			{
				var file = finder.FindDefaultFile(aLanguage);
				if (file != null)
				{
					return file;
				}
			}

			return null;
		}


		/// <summary>
		///     Find the named file in the folder.
		/// </summary>
		/// <param name="aFilename">The name of the file to find. It may contain wild cards.</param>
		/// <returns>The requested file, or null if it could not be found</returns>
		/// <remarks>
		///     We search the folder for the named file. If
		///     more than one file matches the request, the first one is returned.
		/// </remarks>
		public FileInfo FindFile(string aFilename)
		{
			foreach (var finder in _finders)
			{
				var file = finder.FindFile(aFilename);
				if (file != null)
				{
					return file;
				}
			}

			return null;
		}

		#endregion

		#region -- Data --

		private readonly IFileFinder[] _finders;

		#endregion
	}
}

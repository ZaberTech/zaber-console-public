﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using Microsoft.Scripting.Runtime;

namespace Zaber.PlugIns
{
	/// <summary>
	///     Represents a script file with features for using templates and compiling.
	/// </summary>
	public class Script
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Create a new instance.
		/// </summary>
		/// <param name="aFileInfo">The file to load the script from.</param>
		/// <param name="aTemplateFinder">Used to find template files.</param>
		public Script([NotNull] FileInfo aFileInfo, IFileFinder aTemplateFinder)
		{
			File = aFileInfo ?? throw new ArgumentNullException(nameof(aFileInfo));

			using (var reader = aFileInfo.OpenText())
			{
				_originalText = reader.ReadToEnd();
			}

			Language = ScriptLanguage.FindByExtension(aFileInfo);
			_templateFinder = aTemplateFinder;
			Preprocess(false);
		}


		/// <summary>
		///     Create a new instance without a file.
		/// </summary>
		/// <param name="aLanguage">The language that this script will be written in.</param>
		/// <param name="aTemplateFinder">Used to find template files.</param>
		public Script(ScriptLanguage aLanguage, IFileFinder aTemplateFinder)
		{
			_originalText = "";
			Language = aLanguage;
			_templateFinder = aTemplateFinder;
			Preprocess(false);
		}


		/// <summary>
		///     Write the current script text back to the file it was loaded from.
		/// </summary>
		public void Save()
		{
			using (var writer = new StreamWriter(File.FullName))
			{
				writer.Write(_originalText);
			}

			IsDirty = false;
		}


		/// <summary>
		///     Write the current script text to a new file.
		/// </summary>
		/// <param name="aNewFileInfo">The file to write the script to</param>
		public void SaveAs(FileInfo aNewFileInfo)
		{
			File = aNewFileInfo;
			Language = ScriptLanguage.FindByExtension(aNewFileInfo);
			Save();
		}


		/// <summary>
		///     Compile a plug in assembly and find the plug in class within it.
		/// </summary>
		/// <returns>An instance of the plug in class</returns>
		/// <remarks>
		///     This takes the script's source code and compiles it in memory to
		///     an assembly. It then searches the assembly for a class that
		///     implements the <see cref="IPlugIn" /> interface. It creates an
		///     instance and returns it.
		/// </remarks>
		/// <exception cref="CompilerException">
		///     The compiler generated errors.
		///     The error details can be found in the <see cref="CompilerErrors" />
		///     property. Compiler warnings do not cause the exception, but they
		///     can also be found in the <see cref="CompilerErrors" /> property.
		/// </exception>
		/// <exception cref="FileNotFoundException">
		///     The #template declaration
		///     in the script did not match any template file names.
		/// </exception>
		public IPlugIn Build()
		{
			CompilerErrors = new CompilerErrorCollection();

			// Check that we don't have some unknown template.
			Preprocess(true);

			var usedAssemblies = new List<Assembly>();

			// Load any assemblies needed by this script.
			foreach (var path in _preprocessor.ExtraAssemblies)
			{
				// Skip redundant loads of assemblies that have already been loaded, by
				// comparing paths. This does not prevent loading assemblies that have
				// identical names or that have already been loaded by Zaber Console itself,
				// because these are legal (though risky) operations in .NET. 
				if (!_loadedAssemblies.ContainsKey(path))
				{
					try
					{
						var asm = Assembly.LoadFile(path);
						_loadedAssemblies[path] = asm;
					}
					catch (Exception aException)
					{
						CompilerErrors.Add(new CompilerError(path,
															 0,
															 0,
															 "0",
															 $"Failed to load external assembly {path}: {aException.Message}"));
					}
				}

				usedAssemblies.Add(_loadedAssemblies[path]);
			}

			try
			{
				return Language.CreatePlugIn(_preprocessedText, usedAssemblies);
			}
			catch (ArgumentException ae)
			{
				var errors = new int[Language.CompilerErrors.Count];
				for (var i = 0; i < errors.Length; ++i)
				{
					var lineNo = Language.CompilerErrors[i].Line;
					if (_preprocessor.LineNumberMap.TryGetValue(lineNo - 1, out var userLineNo))
					{
						if (userLineNo >= 0)
						{
							lineNo = userLineNo + 1;
						}
					}

					errors[i] = lineNo - 1;
				}

				throw new CompilerException("Could not compile script.", ae) { ErrorLineNumbers = errors };
			}
			finally
			{
				CompilerErrors = Language.CompilerErrors;
				foreach (CompilerError error in CompilerErrors)
				{
					if (_preprocessor.LineNumberMap.TryGetValue(error.Line - 1, out var lineNo))
					{
						if (lineNo >= 0)
						{
							error.Line = lineNo + 1;
						}
					}
				}
			}
		}


		/// <summary>
		///     The file this script was loaded from. May be null.
		/// </summary>
		public FileInfo File { get; private set; }


		/// <summary>
		///     The template referenced by the script file. If there is no template
		///     declaration in the script file, then this will still return a valid
		///     template, but the template will have empty text and a null file.
		/// </summary>
		public ScriptTemplate Template { get; private set; }


		/// <summary>
		///     The text from the script file. This is the raw text as the user sees it,
		///     not the preprocessor output that is actually compiled.
		/// </summary>
		public string Text
		{
			get => _originalText;
			set
			{
				if (_originalText.Equals(value))
				{
					return;
				}

				_originalText = value;
				IsDirty = true;

				Preprocess(false);
			}
		}


		/// <summary>
		///     True if the text has been changed since the script was loaded or saved.
		/// </summary>
		public bool IsDirty { get; private set; }


		/// <summary>
		///     The .NET language that the script will be compiled with.
		/// </summary>
		/// <remarks>The choice is based on the script file's extension.</remarks>
		public ScriptLanguage Language { get; set; }


		/// <summary>
		///     Gets the compiler errors that were generated during <see cref="Build" />
		/// </summary>
		public CompilerErrorCollection CompilerErrors { get; private set; }

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Create a new script preprocessor, load template if necessary and perform preprocessing.
		/// </summary>
		/// <param name="aShouldThrow">
		///     Controls whether or not exceptions should be thrown during
		///     preprocessing; use false when the text is being edited and true when it's time
		///     to run the script.
		/// </param>
		private void Preprocess(bool aShouldThrow)
		{
			_preprocessor = new ScriptPreprocessor(Language);
			var templateName = _preprocessor.GetTemplateName(_originalText);

			if (!IsTemplateLoaded(templateName))
			{
				if (null == templateName)
				{
					Template = new ScriptTemplate(null);
				}
				else
				{
					var templateFile = FindTemplateFile(templateName);
					if ((null == templateFile) && aShouldThrow)
					{
						var msg = string.Format(CultureInfo.CurrentCulture,
												"Template file not found: '{0}'.",
												templateName);
						throw new FileNotFoundException(msg, templateName);
					}

					Template = new ScriptTemplate(templateFile);
				}
			}

			try
			{
				_preprocessedText = _preprocessor.Preprocess(_originalText, Template?.Text);
			}
			catch (Exception aException)
			{
				if (aShouldThrow)
				{
					throw aException;
				}
			}
		}


		private FileInfo FindTemplateFile(string aTemplateName)
		{
			var templateFile = _templateFinder.FindFile(aTemplateName);
			if (null != templateFile)
			{
				return templateFile;
			}

			var templateNameInfo = new FileInfo(aTemplateName);
			if (0 == templateNameInfo.Extension.Length)
			{
				foreach (var extension in Language.UniqueExtensions)
				{
					templateFile = _templateFinder.FindFile(aTemplateName + "." + extension);
					if (null != templateFile)
					{
						return templateFile;
					}
				}
			}

			return null;
		}


		/// <summary>
		///     Determines whether the currently-loaded template's name matches
		///     the specified string.
		/// </summary>
		/// <param name="aTemplateName">The name of the template.</param>
		/// <returns>True if the template is loaded, false otherwise.</returns>
		private bool IsTemplateLoaded(string aTemplateName)
		{
			if (null == Template)
			{
				return false;
			}

			if (null == Template.File)
			{
				return aTemplateName == null;
			}

			if (null == aTemplateName)
			{
				return false;
			}

			if (string.Equals(Template.File.Name, aTemplateName, StringComparison.CurrentCultureIgnoreCase))
			{
				return true;
			}

			var trimmedExtension = Template.File.Extension.Length > 0 ? Template.File.Extension.Substring(1) : "";
			if (!Language.UniqueExtensions.Contains(trimmedExtension))
			{
				return false; // loaded template is for a different language.
			}

			var templateNameInfo = new FileInfo(aTemplateName);
			return (0 == templateNameInfo.Extension.Length)
			   && string.Equals(aTemplateName + Template.File.Extension,
								Template.File.Name,
								StringComparison.CurrentCultureIgnoreCase);
		}

		#endregion

		#region -- Data --

		private static readonly IDictionary<string, Assembly> _loadedAssemblies = new Dictionary<string, Assembly>();
		private readonly IFileFinder _templateFinder;

		private string _originalText;
		private string _preprocessedText;
		private ScriptPreprocessor _preprocessor;

		#endregion
	}
}

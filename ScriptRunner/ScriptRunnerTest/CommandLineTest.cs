using NUnit.Framework;
using ScriptRunner;

namespace ScriptRunnerTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class CommandLineTest
	{
		[Test]
		public void Alias()
		{
			// SETUP
			string[] args = { "/p", "COM6" };
			var nameToRead = "port";
			var nameToWrite = "p";

			// EXEC
			var commandLine = new CommandLine();
			commandLine.AddAlias(nameToRead, nameToWrite);

			commandLine.Parse(args);

			var port = commandLine[nameToRead];

			// VERIFY
			Assert.AreEqual("COM6",
							port,
							"Port should match");
		}


		[Test]
		public void DefaultParameter()
		{
			// SETUP
			string[] args = { "sample.cs" };

			// EXEC
			var commandLine = new CommandLine { DefaultParameter = "script" };

			commandLine.Parse(args);

			var script = commandLine["script"];

			// VERIFY
			Assert.AreEqual("sample.cs",
							script,
							"Script should match");
		}


		[Test]
		public void FlagAlias()
		{
			// SETUP
			string[] args = { "/?" };

			// EXEC
			var commandLine = new CommandLine();
			commandLine.AddFlag("help");
			commandLine.AddAlias("help", "?");

			commandLine.Parse(args);

			var isHelpRequested = commandLine.GetFlag("help");

			// VERIFY
			Assert.IsTrue(isHelpRequested,
						  "Help flag should match");
		}


		[Test]
		public void FlagOff()
		{
			// SETUP
			string[] args = { };

			// EXEC
			var commandLine = new CommandLine();
			commandLine.AddFlag("log");

			commandLine.Parse(args);

			var isLogging = commandLine.GetFlag("log");

			// VERIFY
			Assert.IsFalse(isLogging,
						   "Logging should match");
		}


		[Test]
		public void FlagOn()
		{
			// SETUP
			string[] args = { "/log" };

			// EXEC
			var commandLine = new CommandLine();
			commandLine.AddFlag("log");

			commandLine.Parse(args);

			var isLogging = commandLine.GetFlag("log");

			// VERIFY
			Assert.IsTrue(isLogging,
						  "Logging should match");
		}


		[Test]
		public void IgnoreCaseOfAlias()
		{
			// SETUP
			string[] args = { "/T", "foo.cs", "/p", "COM3" };

			// EXEC
			var commandLine = new CommandLine();
			commandLine.AddAlias("temPlate", "t");
			commandLine.AddAlias("pOrt", "P");

			commandLine.Parse(args);

			var template = commandLine["templAte"];
			var port = commandLine["porT"];

			// VERIFY
			Assert.AreEqual("foo.cs",
							template,
							"Template should match");
			Assert.AreEqual("COM3",
							port,
							"Port should match");
		}


		[Test]
		public void IgnoreCaseOfFlag()
		{
			// SETUP
			string[] args = { "/lOg" };

			// EXEC
			var commandLine = new CommandLine();
			commandLine.AddFlag("loG");


			commandLine.Parse(args);

			var isLogging = commandLine.GetFlag("Log");

			// VERIFY
			Assert.IsTrue(isLogging,
						  "Logging should match");
		}


		[Test]
		public void IgnoreCaseOfParameter()
		{
			// SETUP
			string[] args = { "/pORt", "COM3" };

			// EXEC
			var commandLine = new CommandLine();

			commandLine.Parse(args);

			var port = commandLine["PorT"];

			// VERIFY
			Assert.AreEqual("COM3",
							port,
							"Port should match");
		}


		[Test]
		public void NoDefaultParameter()
		{
			// SETUP
			string[] args = { "sample.cs" };

			// EXEC
			var commandLine = new CommandLine();

			commandLine.Parse(args);

			var isValid = commandLine.IsValid;

			// VERIFY
			Assert.IsFalse(isValid,
						   "Command line should not be valid");
		}


		[Test]
		public void NotSet()
		{
			// SETUP
			string[] args = { "/script", "sample.cs" };

			// EXEC
			var commandLine = new CommandLine();

			commandLine.Parse(args);

			var template = commandLine["template"];
			var isValid = commandLine.IsValid;

			// VERIFY
			Assert.AreEqual(null,
							template,
							"Template should match");
		}


		[Test]
		public void Slash()
		{
			// SETUP
			string[] args = { "/port", "COM6" };

			// EXEC
			var commandLine = new CommandLine();

			commandLine.Parse(args);

			var port = commandLine["port"];
			var isValid = commandLine.IsValid;

			// VERIFY
			Assert.AreEqual("COM6",
							port,
							"Port should match");
			Assert.IsTrue(isValid,
						  "Command line should be valid");
		}


		[Test]
		public void SlashWithNoValue()
		{
			// SETUP
			string[] args = { "/port" };

			// EXEC
			var commandLine = new CommandLine();

			commandLine.Parse(args);

			var isValid = commandLine.IsValid;

			// VERIFY
			Assert.IsFalse(isValid,
						   "Command line should be invalid");
		}
	}
}

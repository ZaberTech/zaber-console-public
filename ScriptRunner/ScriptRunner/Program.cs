﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Reflection;
using System.Threading;
using log4net;
using Zaber;
using Zaber.PlugIns;
using Zaber.Ports;

namespace ScriptRunner
{
	public class Program
	{
		private static int Main(string[] args)
		{
			ZaberPortFacade portFacade = null;

			try
			{
				log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
				log.Info("Starting.");

				var commandLine = new CommandLine { DefaultParameter = "script" };

				commandLine.AddFlag("log");
				commandLine.AddFlag("showports");
				commandLine.AddFlag("help");
				commandLine.AddAlias("script", "s");
				commandLine.AddAlias("port", "p");
				commandLine.AddAlias("device", "d");
				commandLine.AddAlias("axis", "a");
				commandLine.AddAlias("folder", "f");
				commandLine.AddAlias("help", "h");
				commandLine.AddAlias("help", "?");
				commandLine.AddAlias("mode", "m");
				commandLine.AddAlias("baud", "b");
				commandLine.Parse(args);
				if (!commandLine.IsValid)
				{
					var message = "Invalid command-line arguments";
					ShowUsage(message);
					throw new ArgumentException(message);
				}

				portFacade = new ZaberPortFacade
				{
					DefaultDeviceType = new DeviceType
					{
						Commands = new List<CommandInfo>(),
						UnitConverter = ConversionTable.Default
					},
					Port = new RS232Port(new SerialPort(),
										 new PacketConverter()),
					QueryTimeout = 1000
				};


				if (commandLine.GetFlag("showports"))
				{
					ShowPorts(portFacade);
				}
				else if (commandLine.GetFlag("help"))
				{
					ShowUsage("");
				}
				else
				{
					RunScript(commandLine, portFacade);
				}

				log.Info("Exiting normally.");
				return 0;
			}
			catch (Exception e)
			{
				log.Error("Exiting with an exception", e);
				Console.Error.WriteLine("Exiting with an exception: {0}", e.Message);
				return -1;
			}
			finally
			{
				if ((null != portFacade) && portFacade.IsOpen)
				{
					portFacade.Close();
				}
			}
		}


		private static void RunScript(CommandLine commandLine, ZaberPortFacade portFacade)
		{
			var scriptFileName = commandLine["script"];
			if (scriptFileName is null)
			{
				ShowUsage("You must specify a script file.");
				throw new ArgumentNullException("script");
			}

			var folderName = commandLine["folder"];
			var portName = commandLine["port"];
			var modeText = commandLine["mode"];
			var portIsAscii = (modeText != null)
				&& modeText.StartsWith("a", StringComparison.InvariantCultureIgnoreCase);
			var deviceNumber = (byte?) commandLine.GetIntegerOption("device", byte.MaxValue);
			var axisNumber = commandLine.GetIntegerOption("axis", int.MaxValue);
			var isLoggingOn = commandLine.GetFlag("log");
			var baudRate = commandLine.GetIntegerOption("baud", int.MaxValue);
			if (folderName is null)
			{
				folderName = Directory.GetCurrentDirectory();
			}

			var scriptsDirectory =
				folderName != null
					? new DirectoryInfo(folderName)
					: new DirectoryInfo(Directory.GetCurrentDirectory());
			var templateFinder = new FileFinder(scriptsDirectory, "Templates");

			var scriptFile = new FileInfo(Path.Combine(folderName, scriptFileName));
			if (!scriptFile.Exists)
			{
				throw new FileNotFoundException("Script file not found.",
												scriptFileName);
			}

			var script = new Script(scriptFile, templateFinder);
			IPlugIn plugIn;
			plugIn = script.Build();

			if (portName != null)
			{
				portFacade.Port.IsAsciiMode = portIsAscii;

				if (portFacade.Port is RS232Port port)
				{
					if (!baudRate.HasValue)
					{
						baudRate = portIsAscii ? 115200 : 9600;
					}

					port.BaudRate = baudRate.Value;
				}

				portFacade.Open(portName);
				Thread.Sleep(1000);
			}

			if (isLoggingOn)
			{
				var allDevices = portFacade.GetDevice(0);
				allDevices.MessageReceived +=
					OnAllDevicesMessageReceived;
				allDevices.MessageSent +=
					OnAllDevicesMessageSent;
				portFacade.Port.ErrorReceived +=
					Port_ErrorReceived;
			}

			plugIn.Input = Console.In;
			plugIn.Output = Console.Out;
			plugIn.PortFacade = portFacade;
			plugIn.Conversation = portFacade.GetConversation(deviceNumber.GetValueOrDefault(),
															 axisNumber.GetValueOrDefault());

			log.Info("Running script.");
			plugIn.Run();
		}


		private static void OnAllDevicesMessageSent(object sender, DeviceMessageEventArgs e)
			=> Console.Out.WriteLine(e.DeviceMessage.FormatRequest());


		private static void OnAllDevicesMessageReceived(object sender, DeviceMessageEventArgs e)
			=> Console.Out.WriteLine(e.DeviceMessage.FormatResponse());


		private static void Port_ErrorReceived(object sender, ZaberPortErrorReceivedEventArgs e)
			=> Console.Out.WriteLine("Port error: {0}",
									 e.ErrorType);


		private static void ShowPorts(ZaberPortFacade portFacade)
		{
			Console.Out.WriteLine("Available ports are:");
			foreach (var portName in portFacade.GetPortNames())
			{
				Console.Out.WriteLine(portName);
			}
		}


		private static void ShowUsage(string message)
		{
			var writer = Console.Error;
			writer.WriteLine();
			writer.WriteLine(message);
			writer.WriteLine(@"
ScriptRunner is a tool for running Zaber Console scripts from the command 
line, withut a GUI.

***NOTE*** Zaber Console scripting is now deprecated; for new development
please use the Zaber Motion Library instead. Instructions can be found
online at https://www.zaber.com/software/docs/motion-library/

Usage:
  Run a script file: 
  ScriptRunner [options] scriptname
  -OR-
  List available port names:
  ScriptRunner /showports
  -OR-
  Show this message:
  ScriptRunner /help
Where:
  scriptname is the name of a script file. The file name extension tells it 
    what language to run the script in (e.g., .cs files run in C#).
  options are any combination of the following:
    /port portname - open a serial port before starting the script. The
      port name is COM1, COM2, etc.
    /p portname - same as above
    /mode mode - set which mode to open the serial port in: 'b' for binary or
      'a' for ASCII. The default is binary.
    /m mode - same as above
    /baud rate - open the serial port with the requested baud rate. This
      defaults to 9600 in binary mode and 115200 in ASCII mode.
    /b rate - same as above
    /device number - pass the conversation for a device to the script.
    /d number - same as above
    /axis number - pass the conversation for an axis on a multi-axis device to
      the script.
    /a number - same as above
    /folder foldername - search for scripts and templates in the given folder.
    /f foldername - same as above
    /log - log all device requests and responses to the console.
");
		}


		private static ILog log;
	}
}

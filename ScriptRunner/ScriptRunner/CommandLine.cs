using System;
using System.Collections.Generic;

namespace ScriptRunner
{
	public class CommandLine
	{
		public void Parse(string[] args)
		{
			for (var i = 0; i < args.Length; i++)
			{
				if (args[i][0] == '/')
				{
					var parameterName = args[i].Substring(1).ToLowerInvariant();
					if (aliasMap.ContainsKey(parameterName))
					{
						parameterName = aliasMap[parameterName];
					}

					if (flagMap.ContainsKey(parameterName))
					{
						flagMap[parameterName] = true;
					}
					else if ((i + 1) < args.Length)
					{
						map[parameterName] = args[++i];
					}
					else
					{
						IsValid = false;
					}
				}
				else if (DefaultParameter != null)
				{
					map[DefaultParameter] = args[i];
				}
				else
				{
					IsValid = false;
				}
			}
		}


		public void AddAlias(string nameToRead, string nameToWrite) => aliasMap[nameToWrite.ToLowerInvariant()] =
			nameToRead.ToLowerInvariant();


		public bool GetFlag(string flagName) => flagMap[flagName.ToLowerInvariant()];


		public void AddFlag(string flagName) => flagMap[flagName.ToLowerInvariant()] = false;


		public int? GetIntegerOption(string optionName, int maxValue)
		{
			var optionText = this[optionName];
			if (optionText is null)
			{
				return null;
			}

			var isOptionValid = int.TryParse(optionText, out var optionValue);
			if (!isOptionValid)
			{
				throw new ArgumentException($"Option '{optionName}' must be a number, but was {optionText}.");
			}

			if (optionValue > maxValue)
			{
				throw new ArgumentOutOfRangeException($"Option '{optionName}' cannot be larger than {maxValue}.");
			}

			return optionValue;
		}


		public bool IsValid { get; private set; } = true;


		public string DefaultParameter { get; set; }

		public string this[string parameterName]
		{
			get
			{
				map.TryGetValue(parameterName.ToLowerInvariant(),
								out var value);
				return value;
			}
		}

		private readonly Dictionary<string, string> map =
			new Dictionary<string, string>(); // parameter name -> value

		private readonly Dictionary<string, string> aliasMap =
			new Dictionary<string, string>(); // name to write -> name to read

		private readonly Dictionary<string, bool> flagMap =
			new Dictionary<string, bool>(); // flag name -> value
	}
}

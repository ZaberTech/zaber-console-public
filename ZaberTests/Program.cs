﻿using System;
using System.Reflection;
using log4net;

namespace ZaberTest
{
	internal class Program
	{
		#region -- Public Methods & Properties --

		public static void Main()
		{
			var log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
			log.Info("Starting test.");

			try
			{
				var test = new DataPacketTest();

				test.FromBusyString();

				log.Info("Test passed.");
			}
			catch (Exception ex)
			{
				log.Error("Test failed.", ex);
			}
		}

		#endregion
	}
}

﻿using System.Globalization;
using System.Threading;
using NUnit.Framework;
using Zaber;
using ZaberTest.Testing;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class ParameterDataTests
	{
		[Test]
		public void TestConstructor()
		{
			var t = AsciiCommandBuilder.StandardParamTypes["Int32"];
			var p = new ParameterData(1, "a", t, UnitOfMeasure.Data);
			Assert.AreEqual(1, p.ParameterIndex);
			Assert.AreEqual("a", p.Data);
			Assert.AreEqual(t, p.ParamType);
			Assert.AreEqual(UnitOfMeasure.Data, p.SourceUnit);
		}


		[Test]
		public void TestConstructorAcceptsNull()
		{
			var p = new ParameterData(0, null, null, null);
			Assert.AreEqual(0, p.ParameterIndex);
			Assert.IsNull(p.Data);
			Assert.IsNull(p.ParamType);
			Assert.IsNull(p.SourceUnit);
		}


		[Test]
		public void TestParseData()
		{
			TestParseDataForCulture("en-US");
			TestParseDataForCulture("de-DE");
			TestParseDataForCulture("de-AT");
			TestParseDataForCulture("fr-FR");
		}


		private void TestParseDataForCulture(string aCultureName)
		{
			var testValue = 123.4567m;
			var testValueInvariant = testValue.ToString();
			var origCult = Thread.CurrentThread.CurrentCulture;
			Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(aCultureName);
			var str = testValue.ToString();
			var p = new ParameterData(1, str, null, null);
			Assert.IsTrue(p.TryParseData(out var result1));
			Assert.AreEqual(testValue, result1);
			p.Data = testValueInvariant;
			Assert.IsTrue(p.TryParseData(out var result2));
			Assert.AreEqual(testValue, result2);
			Thread.CurrentThread.CurrentCulture = origCult;
		}
	}
}

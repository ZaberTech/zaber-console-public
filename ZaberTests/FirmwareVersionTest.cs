﻿using System.Globalization;
using NUnit.Framework;
using Zaber;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class FirmwareVersionTest
	{
		[Test]
		public void GreaterThanComparisonWithFirmwareVersion()
		{
			Assert.That(Six > FivePointOneSeven);
			Assert.That(SixPointOhThree > Six);
			Assert.That(FivePointOneSeven > Five);
			Assert.That(SixPointOhThree > FivePointOneSeven);
			Assert.That(Six > Five);
			Assert.That(!(Five > Six));
			Assert.That(!(FivePointOneSeven > SixPointOhThree));
		}


		[Test]
		public void LessThanComparisonWithFirmwareVersion()
		{
			Assert.That(Six < SixPointOhThree);
			Assert.That(Five < FivePointOneSeven);
			Assert.That(Five < Six);
			Assert.That(FivePointOneSeven < SixPointOhThree);
			Assert.That(FivePointOneSeven < Six);
			Assert.That(!(Six < Five));
			Assert.That(!(SixPointOhThree < FivePointOneSeven));
		}


		private static readonly FirmwareVersion Six = new FirmwareVersion(600);

		private static readonly FirmwareVersion SixPointOhThree =
			new FirmwareVersion(603);

		private static readonly FirmwareVersion Five = new FirmwareVersion(500);

		private static readonly FirmwareVersion FivePointOneSeven =
			new FirmwareVersion(517);


		[Test]
		public void LessThanOrEqualComparisonWithFirmwareVersion()
		{
			Assert.That(Six <= SixPointOhThree);
			Assert.That(Five <= FivePointOneSeven);
			Assert.That(Five <= Six);
			Assert.That(FivePointOneSeven <= SixPointOhThree);
			Assert.That(FivePointOneSeven <= Six);
			#pragma warning disable CS1718 // Comparison made to same variable.
			Assert.That(Six <= Six);
			Assert.That(FivePointOneSeven <= FivePointOneSeven);
			#pragma warning restore CS1718
		}


		[Test]
		public void GreaterThanOrEqualComparisonWithFirmwareVersion()
		{
			Assert.That(Six >= FivePointOneSeven);
			#pragma warning disable CS1718 // Comparison made to same variable.
			Assert.That(Six >= Six);
			#pragma warning restore CS1718
			Assert.That(FivePointOneSeven >= Five);
			Assert.That(SixPointOhThree >= FivePointOneSeven);
			Assert.That(!(Five >= Six));
			Assert.That(!(FivePointOneSeven >= SixPointOhThree));
		}


		[Test]
		public void EqualityComparisonWithFirmwareVersion()
		{
			#pragma warning disable CS1718 // Comparison made to same variable.
			Assert.That(Five == Five);
			#pragma warning restore CS1718
			Assert.That(Five == new FirmwareVersion(500));
			Assert.That(Five == new FirmwareVersion(5, 0));
			Assert.That(SixPointOhThree == new FirmwareVersion(603));
			Assert.That(!(Six == SixPointOhThree));
		}


		[Test]
		public void InequalityComparisonWithFirmwareVersion()
		{
			Assert.That(Five != FivePointOneSeven);
			Assert.That(Five != Six);
			#pragma warning disable CS1718 // Comparison made to same variable.
			Assert.That(!(Five != Five));
			#pragma warning restore CS1718
			Assert.That(!(Five != new FirmwareVersion(500)));
			Assert.That(FivePointOneSeven != new FirmwareVersion(516));
		}


		[Test]
		public void LessThanComparisonWithInt()
		{
			Assert.That(Six < 601);
			Assert.That(506 < Six);
			Assert.That(503 < FivePointOneSeven);
			Assert.That(FivePointOneSeven < 518);
			Assert.That(Five < 603);
			Assert.That(599 < Six);
		}


		[Test]
		public void GreaterThanComparisonWithInt()
		{
			Assert.That(FivePointOneSeven > 500);
			Assert.That(FivePointOneSeven > 516);
			Assert.That(501 > Five);
			Assert.That(Five > 469);
			Assert.That(700 > SixPointOhThree);
			Assert.That(FivePointOneSeven > 504);
		}


		[Test]
		public void LessThanOrEqualToComparisonWithInt()
		{
			Assert.That(Five <= 500);
			Assert.That(Five <= 501);
			Assert.That(499 <= Five);
			Assert.That(400 <= Five);
			Assert.That(FivePointOneSeven <= 600);
			Assert.That(SixPointOhThree <= 700);
			Assert.That(!(Five <= 400));
			Assert.That(!(700 <= SixPointOhThree));
		}


		[Test]
		public void GreaterThanOrEqualToComparisonWithInt()
		{
			Assert.That(Six >= 599);
			Assert.That(Six >= 600);
			Assert.That(601 >= Six);
			Assert.That(699 >= SixPointOhThree);
			Assert.That(SixPointOhThree >= 603);
			Assert.That(SixPointOhThree >= 602);
			Assert.That(!(SixPointOhThree >= 604));
			Assert.That(!(400 >= Six));
			Assert.That(!(506 >= FivePointOneSeven));
		}


		[Test]
		public void EqualityComparisonWithInt()
		{
			Assert.That(Six == 600);
			Assert.That(Five == 500);
			Assert.That(!(Six == 601));
			Assert.That(517 == FivePointOneSeven);
			Assert.That(FivePointOneSeven == 517);
			Assert.That(603 == SixPointOhThree);
		}


		[Test]
		public void InequalityComparisonWithInt()
		{
			Assert.That(Six != 601);
			Assert.That(601 != Six);
			Assert.That(!(SixPointOhThree != 603));
			Assert.That(!(517 != FivePointOneSeven));
		}


		[Test]
		public void TestToString()
		{
			Assert.AreEqual("600", Six.ToString());
			Assert.AreEqual("6.00", Six.ToString(NumberFormatInfo.InvariantInfo));

			var buildVer = new FirmwareVersion(7, 99, 123);
			Assert.AreEqual("799", buildVer.ToString());
			Assert.AreEqual("7.99.123", buildVer.ToString(NumberFormatInfo.InvariantInfo));
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;

namespace ZaberTest
{
	public abstract class ListTest<L, I>
		where L : IList<I>
	{
		#region -- Public Methods & Properties --

		[Test]
		public void Clear()
		{
			// SETUP
			var list = CreateList();

			// EXEC
			list.Clear();

			// VERIFY
			Assert.That(list.Count,
						Is.EqualTo(0));
		}


		[Test]
		public void CopyTo()
		{
			// SETUP
			var list = CreateList();
			var array = new I[1];

			// EXEC
			list.CopyTo(array, 0);

			// VERIFY
			Assert.That(array[0],
						Is.SameAs(list[0]));
		}


		[Test]
		public void NongenericEnumerator()
		{
			// SETUP
			var list = CreateList();

			IEnumerable enumerable = list;
			var firstItem = default(I);

			// EXEC
			foreach (I item in enumerable)
			{
				firstItem = item;
				break;
			}

			// VERIFY
			Assert.That(firstItem,
						Is.SameAs(list[0]));
		}


		[Test]
		public void IndexOf()
		{
			// SETUP
			var list = CreateList();

			// EXEC
			var i = list.IndexOf(list[0]);

			// VERIFY
			Assert.That(i,
						Is.EqualTo(0));
		}


		[Test]
		public void Remove()
		{
			// SETUP
			var list = CreateList();

			// EXEC
			list.Remove(list[0]);

			// VERIFY
			Assert.That(list.Count,
						Is.EqualTo(0));
		}


		[Test]
		public void Insert()
		{
			// SETUP
			var list = CreateList();

			// EXEC
			list.Insert(0, default(I));

			// VERIFY
			Assert.That(list[0],
						Is.Null);
		}


		[Test]
		public void Indexer()
		{
			// SETUP
			var list = CreateList();

			// EXEC
			list[0] = default(I);

			// VERIFY
			Assert.That(list[0],
						Is.Null);
		}


		[Test]
		public void RemoveAt()
		{
			// SETUP
			var list = CreateList();

			// EXEC
			list.RemoveAt(0);

			// VERIFY
			Assert.That(list.Count,
						Is.EqualTo(0));
		}


		[Test]
		public void ReadOnly()
		{
			// SETUP
			var list = CreateList();

			// EXEC
			var isReadOnly = list.IsReadOnly;

			// VERIFY
			Assert.That(isReadOnly,
						Is.False);
		}

		#endregion

		#region -- Non-Public Methods --

		protected abstract L CreateList();

		#endregion
	}
}

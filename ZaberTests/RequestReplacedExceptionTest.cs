using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using NUnit.Framework;
using Zaber;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class RequestReplacedExceptionTest
	{
		[Test]
		public void Serialize()
		{
			// SETUP
			var response = new DeviceMessage();
			response.NumericData = 77;
			var ex1 = new RequestReplacedException(response);
			RequestReplacedException ex2 = null;

			// EXEC
			using (Stream stream = new MemoryStream())
			{
				// serialize to an in-memory stream
				IFormatter formatter = new BinaryFormatter();
				formatter.Serialize(stream, ex1);

				// deserialize again
				stream.Seek(0, SeekOrigin.Begin);
				ex2 = (RequestReplacedException) formatter.Deserialize(stream);
			}

			// VERIFY
			Assert.IsNotNull(ex2.ReplacementResponse,
							 "Response should not be null after serialization.");
			Assert.AreEqual(77,
							ex2.ReplacementResponse.NumericData,
							"Response data should be carried through serialization");
		}
	}
}

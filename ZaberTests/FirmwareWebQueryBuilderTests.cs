﻿using System.Collections.Generic;
using NUnit.Framework;
using Zaber.FirmwareDownload;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class FirmwareWebQueryBuilderTests
	{
		[Test]
		public void TestMakeInstallationReport()
		{
			var versions = new List<FirmwareWebQueryBuilder.QueryFirmwareVersion>();
			versions.Add(new FirmwareWebQueryBuilder.QueryFirmwareVersion
			{
				BuildNumber = 1,
				FWVersion = "2",
				PlatformID = 3,
				SerialNumber = 4
			});
			versions.Add(new FirmwareWebQueryBuilder.QueryFirmwareVersion
			{
				BuildNumber = 5,
				FWVersion = "6",
				PlatformID = 7,
				SerialNumber = 8
			});

			var data = FirmwareWebQueryBuilder.DecodeResponse<FirmwareWebQueryBuilder.QueryReportInstall>(
				FirmwareWebQueryBuilder.MakeInstallationReport(versions));

			Assert.IsNotNull(data);
			Assert.AreEqual("ReportInstall", data.QueryType);
			Assert.AreEqual(2, data.Devices.Count);
			Assert.AreEqual(1, data.Devices[0].BuildNumber);
			Assert.AreEqual("2", data.Devices[0].FWVersion);
			Assert.AreEqual(3, data.Devices[0].PlatformID);
			Assert.AreEqual(4, data.Devices[0].SerialNumber);
			Assert.AreEqual(5, data.Devices[1].BuildNumber);
			Assert.AreEqual("6", data.Devices[1].FWVersion);
			Assert.AreEqual(7, data.Devices[1].PlatformID);
			Assert.AreEqual(8, data.Devices[1].SerialNumber);
		}


		[Test]
		public void TestMakeListVersionsQuery()
		{
			var devices = new List<FirmwareWebQueryBuilder.QueryDeviceIdentifier>();
			devices.Add(new FirmwareWebQueryBuilder.QueryDeviceIdentifier
			{
				DeviceNumber = 1,
				PlatformID = 2,
				SerialNumber = 3
			});
			devices.Add(new FirmwareWebQueryBuilder.QueryDeviceIdentifier
			{
				DeviceNumber = 4,
				PlatformID = 5,
				SerialNumber = 6
			});

			var data = FirmwareWebQueryBuilder.DecodeResponse<FirmwareWebQueryBuilder.QueryListVersions>(
				FirmwareWebQueryBuilder.MakeListVersionsQuery(devices));

			Assert.IsNotNull(data);
			Assert.AreEqual("ListVersions", data.QueryType);
			Assert.AreEqual(2, data.Devices.Count);
			Assert.AreEqual(1, data.Devices[0].DeviceNumber);
			Assert.AreEqual(2, data.Devices[0].PlatformID);
			Assert.AreEqual(3, data.Devices[0].SerialNumber);
			Assert.AreEqual(4, data.Devices[1].DeviceNumber);
			Assert.AreEqual(5, data.Devices[1].PlatformID);
			Assert.AreEqual(6, data.Devices[1].SerialNumber);
		}


		[Test]
		public void TestMakePackageRequest()
		{
			var versions = new List<FirmwareWebQueryBuilder.QueryFirmwareVersion>();
			versions.Add(new FirmwareWebQueryBuilder.QueryFirmwareVersion
			{
				BuildNumber = 1,
				FWVersion = "2",
				PlatformID = 3,
				SerialNumber = 4
			});
			versions.Add(new FirmwareWebQueryBuilder.QueryFirmwareVersion
			{
				BuildNumber = 5,
				FWVersion = "6",
				PlatformID = 7,
				SerialNumber = 8
			});

			var data = FirmwareWebQueryBuilder.DecodeResponse<FirmwareWebQueryBuilder.QueryDownloadVersions>(
				FirmwareWebQueryBuilder.MakePackageRequest(versions));

			Assert.IsNotNull(data);
			Assert.AreEqual("Package", data.QueryType);
			Assert.AreEqual(2, data.Versions.Count);
			Assert.AreEqual(1, data.Versions[0].BuildNumber);
			Assert.AreEqual("2", data.Versions[0].FWVersion);
			Assert.AreEqual(3, data.Versions[0].PlatformID);
			Assert.AreEqual(4, data.Versions[0].SerialNumber);
			Assert.AreEqual(5, data.Versions[1].BuildNumber);
			Assert.AreEqual("6", data.Versions[1].FWVersion);
			Assert.AreEqual(7, data.Versions[1].PlatformID);
			Assert.AreEqual(8, data.Versions[1].SerialNumber);
		}


		[Test]
		public void TestMakeServerInfoQuery()
		{
			var data = FirmwareWebQueryBuilder.DecodeResponse<FirmwareWebQueryBuilder.QueryServerInfo>(
				FirmwareWebQueryBuilder.MakeServerInfoQuery());

			Assert.IsNotNull(data);
			Assert.AreEqual("ServerInfo", data.QueryType);
		}
	}
}

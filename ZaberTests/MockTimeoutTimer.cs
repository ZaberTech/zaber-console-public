﻿using System.Threading;
using Zaber;

namespace ZaberTest
{
	public class MockTimeoutTimer : TimeoutTimer
	{
		#region -- Public Methods & Properties --

		public void TimeoutNow() => timerHandle.Set();


		public override void Sleep() => timerHandle.WaitOne();


		public override bool WaitOne(WaitHandle waitHandle)
		{
			var satisfiedIndex =
				WaitHandle.WaitAny(new[] { timerHandle, waitHandle });
			return satisfiedIndex == 1;
		}

		#endregion

		#region -- Data --

		private readonly EventWaitHandle timerHandle = new AutoResetEvent(false);

		#endregion
	}
}

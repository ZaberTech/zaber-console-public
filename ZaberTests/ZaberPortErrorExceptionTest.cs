using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using NUnit.Framework;
using Zaber;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class ZaberPortErrorExceptionTest
	{
		[Test]
		public void Serialize()
		{
			// SETUP
			var ex1 = new ZaberPortErrorException(ZaberPortError.PacketTimeout);
			ZaberPortErrorException ex2 = null;

			// EXEC
			using (Stream stream = new MemoryStream())
			{
				// serialize to an in-memory stream
				IFormatter formatter = new BinaryFormatter();
				formatter.Serialize(stream, ex1);

				// deserialize again
				stream.Seek(0, SeekOrigin.Begin);
				ex2 = (ZaberPortErrorException) formatter.Deserialize(stream);
			}

			// VERIFY
			Assert.IsNotNull(ex2.ErrorType,
							 "ErrorType should not be null after serialization.");
			Assert.AreEqual(ZaberPortError.PacketTimeout,
							ex2.ErrorType,
							"ErrorType should be carried through serialization");
		}
	}
}

﻿using System;
using NUnit.Framework;
using TickingTest;
using Zaber;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class PortListenerTest
	{
		[Test]
		public void Blocking() => TestFramework.RunOnce(new BlockingThreads());


		[Test]
		public void NextAfterStop()
		{
			// SETUP
			var port = new MockPort();

			// EXEC
			var listener = new PortListener(port);
			listener.Stop();

			var ex = Assert.Throws<InvalidOperationException>(() => listener.NextResponse());

			// VERIFY
			Assert.That(ex.Message,
						Is.EqualTo("NextResponse() called when listener is not listening."),
						"error message");
		}


		[Test]
		public void NextResponse()
		{
			// SETUP
			byte deviceNumber = 23;
			var port = new MockPort();
			var command = Command.Home;
			var data = 0;

			// EXEC
			var listener = new PortListener(port);
			port.FireResponse(deviceNumber, command, data);

			var response = listener.NextResponse();

			Assert.AreEqual(command,
							response.Command,
							"command");
			Assert.AreEqual(data,
							response.NumericData,
							"data");
		}


		[Test]
		public void NextTimerAfterStop()
		{
			// SETUP
			var port = new MockPort();
			var timer = new MockTimeoutTimer();

			// EXEC
			var listener = new PortListener(port);
			listener.Stop();

			var ex = Assert.Throws<InvalidOperationException>(() => listener.NextResponse(timer));

			// VERIFY
			Assert.That(ex.Message,
						Is.EqualTo("NextResponse() called when listener is not listening."),
						"error message");
		}


		[Test]
		public void NonBlocking()
		{
			// SETUP
			byte deviceNumber = 23;
			var port = new MockPort();
			var command = Command.Home;
			var data = 0;
			var isBlocking = false;

			// EXEC
			var listener = new PortListener(port);
			port.FireResponse(deviceNumber, command, data);

			var response = listener.NextResponse(isBlocking);

			Assert.AreEqual(command,
							response.Command,
							"command");
			Assert.AreEqual(data,
							response.NumericData,
							"data");
		}


		[Test]
		public void NonBlockingNoResponse()
		{
			// SETUP
			var port = new MockPort();
			var isBlocking = false;

			// EXEC
			var listener = new PortListener(port);

			var response = listener.NextResponse(isBlocking);

			Assert.IsNull(response,
						  "response");
		}


		[Test]
		public void StartWhenStarted()
		{
			// SETUP
			var port = new MockPort();

			// EXEC
			var listener = new PortListener(port);
			string msg = null;
			try
			{
				listener.Start();
				Assert.Fail("should have thrown.");
			}
			catch (InvalidOperationException ex)
			{
				msg = ex.Message;
			}

			// VERIFY
			Assert.AreEqual("Listener is already started.",
							msg,
							"msg");
		}


		[Test]
		public void StopWhenStopped()
		{
			// SETUP
			var port = new MockPort();

			// EXEC
			var listener = new PortListener(port);
			listener.Stop();

			string msg = null;
			try
			{
				listener.Stop();
				Assert.Fail("should have thrown.");
			}
			catch (InvalidOperationException ex)
			{
				msg = ex.Message;
			}

			// VERIFY
			Assert.AreEqual("Listener is already stopped.",
							msg,
							"msg");
		}


		[Test]
		public void TimerSuccess() => TestFramework.RunOnce(new TimerSuccessThreads());


		[Test]
		public void TimerTimeout() => TestFramework.RunOnce(new TimerTimeoutThreads());


		private class BlockingThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Main()
			{
				// SETUP
				_port = new MockPort();

				// EXEC
				var listener = new PortListener(_port);

				var response = listener.NextResponse();

				AssertTick(1);
				Assert.AreEqual(COMMAND,
								response.Command,
								"command");
				Assert.AreEqual(DATA,
								response.NumericData,
								"data");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				_port.FireResponse(DEVICE_NUMBER, COMMAND, DATA);
			}

			#endregion

			#region -- Data --

			private const byte DEVICE_NUMBER = 23;
			private const Command COMMAND = Command.Home;
			private const int DATA = 0;
			private MockPort _port;

			#endregion
		}

		private class TimerSuccessThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Main()
			{
				// SETUP
				_port = new MockPort();

				// EXEC
				DataPacket response;
				using (var listener = new PortListener(_port))
				{
					response = listener.NextResponse(_mockTimer);
				}

				// VERIFY
				AssertTick(1);
				Assert.That(response.NumericData,
							Is.EqualTo(3456),
							"response data");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);
				_port.FireResponse("@01 0 OK IDLE -- 3456");
			}

			#endregion

			#region -- Data --

			private MockPort _port;
			private readonly MockTimeoutTimer _mockTimer = new MockTimeoutTimer();

			#endregion
		}

		private class TimerTimeoutThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Main()
			{
				// SETUP
				_port = new MockPort();

				// EXEC
				Assert.Throws(typeof(RequestTimeoutException),
							  delegate
							  {
								  using (var listener = new PortListener(_port))
								  {
									  listener.NextResponse(_mockTimer);
								  }
							  },
							  "next response");

				// VERIFY
				AssertTick(1);
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);
				_mockTimer.TimeoutNow();

				WaitForTick(2);
				_port.FireResponse("late response");
			}

			#endregion

			#region -- Data --

			private MockPort _port;
			private readonly MockTimeoutTimer _mockTimer = new MockTimeoutTimer();

			#endregion
		}
	}
}

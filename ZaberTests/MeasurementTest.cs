﻿using System;
using NUnit.Framework;
using Zaber;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class MeasurementTest
	{
		[Test]
		public void Arithmetic()
		{
			// SETUP
			var measurement1 =
				new Measurement(0.2, UnitOfMeasure.Millimeter);
			var measurement2 =
				new Measurement(0.3, UnitOfMeasure.Millimeter);
			var expectedSum =
				new Measurement(0.5, UnitOfMeasure.Millimeter);
			var expectedDiff =
				new Measurement(0.1, UnitOfMeasure.Millimeter);
			var ratio = 2.0;
			var expectedProduct =
				new Measurement(0.4, UnitOfMeasure.Millimeter);
			var expectedTriple =
				new Measurement(0.6, UnitOfMeasure.Millimeter);
			var expectedNegative =
				new Measurement(-0.2, UnitOfMeasure.Millimeter);
			var expectedQuotient =
				new Measurement(0.1, UnitOfMeasure.Millimeter);


			// EXEC
			var sum = measurement1 + measurement2;
			var diff = measurement2 - measurement1;
			var product1 = ratio * measurement1;
			var product2 = measurement1 * ratio;
			var triple = 3 * measurement1;
			var negative = -measurement1;
			var quotient = measurement1 / ratio;

			// VERIFY
			Assert.That(sum,
						Is.EqualTo(expectedSum),
						"Sum");
			Assert.That(diff,
						Is.EqualTo(expectedDiff),
						"Difference");
			Assert.That(product1,
						Is.EqualTo(expectedProduct),
						"Product1");
			Assert.That(product2,
						Is.EqualTo(expectedProduct),
						"Product2");
			Assert.That(triple,
						Is.EqualTo(expectedTriple),
						"Triple");
			Assert.That(negative,
						Is.EqualTo(expectedNegative),
						"Negative");
			Assert.That(quotient,
						Is.EqualTo(expectedQuotient),
						"Quotient");
		}


		[Test]
		public void Construct()
		{
			// EXEC
			var decimalMeasurement =
				new Measurement(1.0m, UnitOfMeasure.Micrometer);
			var doubleMeasurement =
				new Measurement(1.0, UnitOfMeasure.Micrometer);
			var intMeasurement =
				new Measurement(1, UnitOfMeasure.Micrometer);
			var measurement1 = new Measurement(10, UnitOfMeasure.Millimeter);
			var measurement2 = new Measurement(measurement1, UnitOfMeasure.Micrometer);

			// VERIFY
			Assert.AreEqual(measurement2.Value, measurement1.Value * 1000);
			Assert.AreEqual(decimalMeasurement,
							doubleMeasurement,
							"measurement constructed from a double.");
			Assert.AreEqual(decimalMeasurement,
							intMeasurement,
							"measurement constructed from an int.");
		}


		[Test]
		public void IncompatibleUnitSum()
		{
			// SETUP
			var measurement1 =
				new Measurement(0.2, UnitOfMeasure.Degree);
			var measurement2 =
				new Measurement(300, UnitOfMeasure.Micrometer);

			// EXEC
			var ex = Assert.Throws<InvalidOperationException>(delegate
			{
				var sum = measurement1 + measurement2;
			});

			// VERIFY
			Assert.That(ex.Message,
						Is.EqualTo("Cannot convert from ° to µm."),
						"error message");
		}


		[Test]
		public void MixedUnitSum()
		{
			// SETUP
			var measurement1 =
				new Measurement(0.2, UnitOfMeasure.Millimeter);
			var measurement2 =
				new Measurement(300, UnitOfMeasure.Micrometer);

			// result takes units of second argument
			var expectedSum =
				new Measurement(500, UnitOfMeasure.Micrometer);

			// EXEC
			var sum = measurement1 + measurement2;

			// VERIFY
			Assert.That(sum,
						Is.EqualTo(expectedSum),
						"Sum");
		}


		[Test]
		public void String()
		{
			// SETUP
			var measurement =
				new Measurement(0.2, UnitOfMeasure.Millimeter);

			// EXEC
			var s = measurement.ToString();

			// VERIFY
			Assert.That(s,
						Is.EqualTo("0.2mm"),
						"String representation.");
		}
	}
}

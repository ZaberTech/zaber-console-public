﻿using NUnit.Framework;
using Zaber;

#pragma warning disable CS0612 // 'name' is obsolete
namespace ZaberTest
{
	[TestFixture]
	[Category("active")]
	[SetCulture("en-US")]
	internal class ConversionMapTest
	{
		[Test]
		public void FindByType()
		{
			// EXEC
			ConversionMap.Add(MotionType.Linear,
							  MeasurementType.Position,
							  ConversionTable.LinearPosition);
			var table = ConversionMap.Find(MotionType.Linear, MeasurementType.Position);

			// VERIFY
			Assert.That(table,
						Is.SameAs(ConversionTable.LinearPosition),
						"table");
		}


		[Test]
		public void FindByUnit()
		{
			// EXEC
			ConversionMap.Add(MotionType.Linear,
							  MeasurementType.Position,
							  ConversionTable.LinearPosition);
			ConversionMap.Add(MotionType.Linear,
							  MeasurementType.Velocity,
							  ConversionTable.LinearVelocity);
			var table = ConversionMap.Find(UnitOfMeasure.Millimeter);

			// VERIFY
			Assert.That(table,
						Is.SameAs(ConversionTable.LinearPosition),
						"table");
		}


		[Test]
		public void MultipleMeasurementTypes()
		{
			// EXEC
			ConversionMap.Add(MotionType.Linear,
							  MeasurementType.Position,
							  ConversionTable.LinearPosition);
			ConversionMap.Add(MotionType.Linear,
							  MeasurementType.Velocity,
							  ConversionTable.LinearVelocity);
			var positionTable = ConversionMap.Find(MotionType.Linear, MeasurementType.Position);
			var velocityTable = ConversionMap.Find(MotionType.Linear, MeasurementType.Velocity);

			// VERIFY
			Assert.That(positionTable,
						Is.SameAs(ConversionTable.LinearPosition),
						"linear table");
			Assert.That(velocityTable,
						Is.SameAs(ConversionTable.LinearVelocity),
						"velocity table");
		}


		[Test]
		public void MultipleMotionTypes()
		{
			// EXEC
			ConversionMap.Add(MotionType.Linear,
							  MeasurementType.Position,
							  ConversionTable.LinearPosition);
			ConversionMap.Add(MotionType.Rotary,
							  MeasurementType.Position,
							  ConversionTable.RotaryPosition);
			var linearTable = ConversionMap.Find(MotionType.Linear, MeasurementType.Position);
			var rotaryTable = ConversionMap.Find(MotionType.Rotary, MeasurementType.Position);

			// VERIFY
			Assert.That(linearTable,
						Is.SameAs(ConversionTable.LinearPosition),
						"linear table");
			Assert.That(rotaryTable,
						Is.SameAs(ConversionTable.RotaryPosition),
						"rotary table");
		}
	}
}
#pragma warning restore CS0612

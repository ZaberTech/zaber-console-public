﻿using System;
using System.Linq;
using NUnit.Framework;
using Zaber;
using ZaberTest.Testing;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class ParameterTypeTests
	{
		[Test]
		public void TestFormatEnum()
		{
			var p = new ParameterType(new[] { "Foo", "Bar", "Baz" });
			Assert.Throws<FormatException>(() => p.FormatNumeric(1.0m));
		}


		[Test]
		public void TestFormatToken()
		{
			var p = AsciiCommandBuilder.StandardParamTypes["Token"];
			Assert.Throws<FormatException>(() => p.FormatNumeric(1.0m));
		}


		[Test]
		public void TestParseAndFormatNumerics()
		{
			var v = 55.55m;
			TestFormatNumeric("Tenths", v, "55.6");
			TestFormatNumeric("UTenths", v, "55.6");
			TestFormatNumeric("Uint8", v, "56");
			TestFormatNumeric("Int8", v, "56");
			TestFormatNumeric("Uint16", v, "56");
			TestFormatNumeric("Int16", v, "56");
			TestFormatNumeric("Uint32", v, "56");
			TestFormatNumeric("Int32", v, "56");
			TestFormatNumeric("Uint64", v, "56");
			TestFormatNumeric("Int64", v, "56");
			TestFormatNumeric("Uint", v, "56");
			TestFormatNumeric("Int", v, "56");
			TestFormatBoolean("Bool", v, "1");
			TestFormatBoolean("Bool", 0.4m, "0");
			TestFormatNumeric("IntDP1", v, "55.6");
			TestFormatNumeric("UintDP1", v, "55.6");
			TestFormatNumeric("Uint16DP1", v, "55.6");
			TestFormatNumeric("Uint16DP2", v, "55.55");
			TestFormatNumeric("Uint16DP3", v, "55.550");
			TestFormatNumeric("Uint32DP1", v, "55.6");
			TestFormatNumeric("Uint32DP3", v, "55.550");
			TestFormatNumeric("Uint64DP9", v, "55.550000000");
			TestFormatNumeric("Int16DP1", v, "55.6");
			TestFormatNumeric("Int32DP3", v, "55.550");
		}


		[Test]
		public void TestParseEnum()
		{
			var p = new ParameterType(new[] { "Foo", "Bar", "Baz" });
			Assert.IsFalse(p.IsToken);
			Assert.IsFalse(p.IsNumeric);
			Assert.IsTrue(p.IsEnumeration);
			Assert.IsNotNull(p.EnumValues);
			Assert.AreEqual(3, p.EnumValues.Count());
			var vals = p.EnumValues.ToArray();
			Assert.AreEqual("Foo", vals[0]);
			Assert.AreEqual("Bar", vals[1]);
			Assert.AreEqual("Baz", vals[2]);
		}


		[Test]
		[SetCulture("fr-FR")]
		public void FormatNumericIgnoresCulture()
		{
			var p = AsciiCommandBuilder.StandardParamTypes["Uint32DP3"];
			var s = p.FormatNumeric(12.345m);
			Assert.AreEqual("12.345", s);
		}


		private void TestFormatNumeric(string aTypeName, decimal aTestValue, string aExpectedString)
		{
			var p = AsciiCommandBuilder.StandardParamTypes[aTypeName];
			Assert.IsFalse(p.IsToken);
			Assert.IsFalse(p.IsBoolean);
			Assert.IsTrue(p.IsNumeric);
			Assert.IsFalse(p.IsEnumeration);
			Assert.IsNull(p.EnumValues);
			var s = p.FormatNumeric(aTestValue);
			Assert.AreEqual(aExpectedString, s);
		}


		private void TestFormatBoolean(string aTypeName, decimal aTestValue, string aExpectedString)
		{
			var p = AsciiCommandBuilder.StandardParamTypes[aTypeName];
			Assert.IsFalse(p.IsToken);
			Assert.IsTrue(p.IsBoolean);
			Assert.IsFalse(p.IsNumeric);
			Assert.IsFalse(p.IsEnumeration);
			Assert.IsNull(p.EnumValues);
			var s = p.FormatNumeric(aTestValue);
			Assert.AreEqual(aExpectedString, s);
		}
	}
}

﻿using System;
using NUnit.Framework;
using Zaber;
using Desc = System.ComponentModel.DescriptionAttribute;

namespace ZaberTest
{
	public enum TestEnum
	{
		One = 1,

		[Obsolete]
		[Desc("Also")]
		Two = 2,

		[Desc("Crowded")]
		Three = 3
	}


	[TestFixture]
	[SetCulture("en-US")]
	public class EnumExtensionTests
	{
		[Test]
		public void TestFindAttribute() => Assert.AreEqual("Crowded", TestEnum.Three.GetDescription());


		[Test]
		public void TestFindRightAttribute()
		{
			var e = (TestEnum) 2;
			Assert.AreEqual("Also", e.GetDescription());
		}


		[Test]
		public void TestNullIfInvalidValue()
		{
			var e = (TestEnum) 44;
			Assert.IsNull(e.GetDescription());
		}


		[Test]
		public void TestNullIfNoDescription() => Assert.IsNull(TestEnum.One.GetDescription());
	}
}

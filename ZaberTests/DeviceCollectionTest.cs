using System.Linq;
using NUnit.Framework;
using Rhino.Mocks;
using Rhino.Mocks.Interfaces;
using Zaber;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class DeviceCollectionTest : ListTest<DeviceCollection, ZaberDevice>
	{
		[SetUp]
		public void SetUp()
		{
			nextDeviceNumber = 17;
			mocks = new MockRepository();
			port = mocks.StrictMock<IZaberPort>();

			port.IsAsciiMode = false;
			LastCall.PropertyBehavior();
			port.IsAsciiMode = false;

			port.DataPacketReceived += null;
			packetReceivedRaiser =
				LastCall.IgnoreArguments().Repeat.Any().GetEventRaiser();
			port.DataPacketSent += null;
			packetSentRaiser =
				LastCall.IgnoreArguments().Repeat.Any().GetEventRaiser();

			eventSource = null;
			eventArgs = null;
			eventType = null;
		}


		[Test]
		public void Contains()
		{
			// SETUP
			var deviceCollection = CreateDeviceCollection();
			var member = AddMember(deviceCollection);
			var nonmember = new ZaberDevice();

			// EXPECT
			mocks.ReplayAll();

			// EXEC
			deviceCollection.Port = port;
			member.Port = port;

			var containsMember = deviceCollection.Contains(member);
			var containsNonmember = deviceCollection.Contains(nonmember);
			deviceCollection.Remove(member);
			var containsAfterRemove = deviceCollection.Contains(member);

			// VERIFY
			Assert.IsTrue(containsMember,
						  "Should contain member");
			Assert.IsFalse(containsNonmember,
						   "Should not contain nonmember");
			Assert.IsFalse(containsAfterRemove,
						   "Should not contain member after remove");

			mocks.VerifyAll();
		}


		/// <summary>
		///     A device collection's list of commands should be the union of
		///     all its members' lists of commands.
		/// </summary>
		[Test]
		public void MemberCommands()
		{
			// SETUP
			var expectedCommands = new[] { Command.Home, Command.ReturnDeviceID, Command.LoadEventInstruction };

			var deviceCollection = new DeviceCollection
			{
				DeviceType = CreateDeviceTypeWithCommands(Command.Home,
														  Command.ReturnDeviceID,
														  Command.LoadEventInstruction,
														  Command.ReadRegister)
			};

			var member1 = new ZaberDevice
			{
				DeviceType = CreateDeviceTypeWithCommands(Command.Home,
														  Command.ReturnDeviceID)
			};

			var member2 = new ZaberDevice
			{
				DeviceType = CreateDeviceTypeWithCommands(Command.LoadEventInstruction,
														  Command.ReturnDeviceID)
			};

			// EXPECT
			mocks.ReplayAll();

			// EXEC
			deviceCollection.Add(member1);
			deviceCollection.Add(member2);

			var allCommands = deviceCollection.DeviceType.Commands.Select(cmd => cmd.Command).ToArray();

			// VERIFY
			Assert.That(allCommands,
						Is.EquivalentTo(expectedCommands),
						"commands");

			mocks.VerifyAll();
		}


		[Test]
		public void RequestByAlias()
		{
			// SETUP
			var deviceCollection = CreateDeviceCollectionAndListen();
			var member = AddMember(deviceCollection);

			var aliasDeviceNumber = deviceCollection.DeviceNumber;

			var requestArgs =
				new DataPacketEventArgs(new DataPacket());
			requestArgs.DataPacket.Command = Command.Home;
			requestArgs.DataPacket.DeviceNumber = aliasDeviceNumber;
			requestArgs.DataPacket.NumericData = 42;

			// EXPECT
			port.Send(aliasDeviceNumber,
					  Command.Home,
					  0,
					  new Measurement(0, UnitOfMeasure.Data));

			mocks.ReplayAll();

			// EXEC
			deviceCollection.Port = port;
			member.Port = port;
			var isSingleDevice = deviceCollection.IsSingleDevice;

			deviceCollection.Send(Command.Home, 0);
			packetSentRaiser.Raise(port,
								   requestArgs);

			// VERIFY
			Assert.IsFalse(isSingleDevice,
						   "Collection should not be a single device.");
			Assert.AreEqual("DataPacketSent",
							eventType,
							"Event type should match");
			Assert.AreSame(deviceCollection,
						   eventSource,
						   "Device should raise packet event");
			Assert.AreEqual("Where the heart is",
							eventArgs.DeviceMessage.CommandInfo.Name,
							"Command info should be set.");

			mocks.VerifyAll();
		}


		[Test]
		public void RequestByMember()
		{
			// SETUP
			var deviceCollection = CreateDeviceCollectionAndListen();
			var member = AddMember(deviceCollection);
			var memberDeviceNumber = member.DeviceNumber;

			var requestArgs =
				new DataPacketEventArgs(new DataPacket());
			requestArgs.DataPacket.Command = Command.Home;
			requestArgs.DataPacket.DeviceNumber = memberDeviceNumber;
			requestArgs.DataPacket.NumericData = 42;

			// EXPECT
			port.Send(memberDeviceNumber,
					  Command.Home,
					  0,
					  new Measurement(0, UnitOfMeasure.Data));

			mocks.ReplayAll();

			// EXEC
			deviceCollection.Port = port;
			member.Port = port;

			member.Send(Command.Home, 0);
			packetSentRaiser.Raise(port,
								   requestArgs);

			// VERIFY
			Assert.AreEqual("DataPacketSent",
							eventType,
							"Event type should match");
			Assert.AreSame(deviceCollection,
						   eventSource,
						   "Device should raise packet event");
			Assert.AreEqual("Where the heart is",
							eventArgs.DeviceMessage.CommandInfo.Name,
							"Command info should be set.");

			mocks.VerifyAll();
		}


		[Test]
		public void RequestByNestedMember()
		{
			// SETUP
			var deviceCollection = CreateDeviceCollection();
			var member = AddMember(deviceCollection);
			var parentCollection = CreateDeviceCollectionAndListen();
			parentCollection.Add(deviceCollection);
			parentCollection.Add(member);
			var memberDeviceNumber = member.DeviceNumber;

			var requestArgs =
				new DataPacketEventArgs(new DataPacket());
			requestArgs.DataPacket.Command = Command.Home;
			requestArgs.DataPacket.DeviceNumber = memberDeviceNumber;
			requestArgs.DataPacket.NumericData = 42;

			// EXPECT
			port.Send(memberDeviceNumber,
					  Command.Home,
					  0,
					  new Measurement(0, UnitOfMeasure.Data));

			mocks.ReplayAll();

			// EXEC
			deviceCollection.Port = port;
			member.Port = port;

			member.Send(Command.Home, 0);
			packetSentRaiser.Raise(port,
								   requestArgs);

			// VERIFY
			Assert.AreEqual("DataPacketSent",
							eventType,
							"Event type should match");
			Assert.AreSame(parentCollection,
						   eventSource,
						   "Parent collection should raise packet event");
			Assert.AreEqual("Where the heart is",
							eventArgs.DeviceMessage.CommandInfo.Name,
							"Command info should be set.");

			mocks.VerifyAll();
		}


		[Test]
		public void ResponseFromAlias()
		{
			// SETUP
			var deviceCollection = CreateDeviceCollectionAndListen();
			var member = AddMember(deviceCollection);
			var responseArgs =
				new DataPacketEventArgs(new DataPacket());
			responseArgs.DataPacket.Command = Command.Home;
			responseArgs.DataPacket.DeviceNumber = deviceCollection.DeviceNumber;
			responseArgs.DataPacket.NumericData = 42;

			// EXPECT
			mocks.ReplayAll();

			// EXEC
			deviceCollection.Port = port;
			member.Port = port;

			packetReceivedRaiser.Raise(port,
									   responseArgs);

			// VERIFY
			Assert.AreEqual("DataPacketReceived",
							eventType,
							"Event type should match");
			Assert.AreSame(deviceCollection,
						   eventSource,
						   "Device should raise packet event");
			Assert.AreEqual("Where the heart is",
							eventArgs.DeviceMessage.CommandInfo.Name,
							"Command info should be set.");
			mocks.VerifyAll();
		}


		[Test]
		public void ResponseFromMember()
		{
			// SETUP
			var deviceCollection = CreateDeviceCollectionAndListen();
			var member = AddMember(deviceCollection);
			var responseArgs =
				new DataPacketEventArgs(new DataPacket());
			responseArgs.DataPacket.Command = Command.Home;
			responseArgs.DataPacket.DeviceNumber = member.DeviceNumber;
			responseArgs.DataPacket.NumericData = 42;

			// EXPECT
			mocks.ReplayAll();

			// EXEC
			deviceCollection.Port = port;
			member.Port = port;

			packetReceivedRaiser.Raise(port,
									   responseArgs);

			// VERIFY
			Assert.AreEqual("DataPacketReceived",
							eventType,
							"Event type should match");
			Assert.AreSame(deviceCollection,
						   eventSource,
						   "Device should raise packet event");
			Assert.AreEqual("Where the heart is",
							eventArgs.DeviceMessage.CommandInfo.Name,
							"Command info should be set.");
			mocks.VerifyAll();
		}


		[Test]
		public void ResponseFromNestedMember()
		{
			// SETUP
			var deviceCollection = CreateDeviceCollection();
			var member = AddMember(deviceCollection);
			var parentCollection = CreateDeviceCollectionAndListen();
			parentCollection.Add(deviceCollection);
			parentCollection.Add(member);

			var responseArgs =
				new DataPacketEventArgs(new DataPacket());
			responseArgs.DataPacket.Command = Command.Home;
			responseArgs.DataPacket.DeviceNumber = member.DeviceNumber;
			responseArgs.DataPacket.NumericData = 42;

			// EXPECT
			mocks.ReplayAll();

			// EXEC
			parentCollection.Port = port;
			deviceCollection.Port = port;
			member.Port = port;

			packetReceivedRaiser.Raise(port,
									   responseArgs);

			// VERIFY
			Assert.AreEqual("DataPacketReceived",
							eventType,
							"Event type should match");
			Assert.AreSame(parentCollection,
						   eventSource,
						   "Device should raise packet event");
			Assert.AreEqual("Where the heart is",
							eventArgs.DeviceMessage.CommandInfo.Name,
							"Command info should be set.");
			mocks.VerifyAll();
		}


		[Test]
		public void ResponseFromRemovedMember()
		{
			// SETUP
			var deviceCollection = CreateDeviceCollectionAndListen();
			var member = AddMember(deviceCollection);
			var responseArgs =
				new DataPacketEventArgs(new DataPacket());
			responseArgs.DataPacket.Command = Command.Home;
			responseArgs.DataPacket.DeviceNumber = member.DeviceNumber;
			responseArgs.DataPacket.NumericData = 42;

			// EXPECT
			mocks.ReplayAll();

			// EXEC
			deviceCollection.Port = port;
			member.Port = port;

			deviceCollection.Remove(member);
			packetReceivedRaiser.Raise(port,
									   responseArgs);

			// VERIFY
			Assert.IsNull(eventType,
						  "No event should be raised after member is removed");
			mocks.VerifyAll();
		}


		[Test]
		public void UnrelatedRequestEvent()
		{
			// SETUP
			var deviceCollection = CreateDeviceCollectionAndListen();
			var member = AddMember(deviceCollection);
			var requestArgs =
				new DataPacketEventArgs(new DataPacket());
			requestArgs.DataPacket.Command = Command.Home;
			requestArgs.DataPacket.DeviceNumber = 23; // unrelated device number
			requestArgs.DataPacket.NumericData = 42;

			// EXPECT
			mocks.ReplayAll();

			// EXEC
			deviceCollection.Port = port;
			member.Port = port;

			packetSentRaiser.Raise(port,
								   requestArgs);

			// VERIFY
			Assert.IsNull(eventType,
						  "No event should be raised");

			mocks.VerifyAll();
		}


		[Test]
		public void UnrelatedResponse()
		{
			// SETUP
			var deviceCollection = CreateDeviceCollectionAndListen();
			var member = AddMember(deviceCollection);
			var responseArgs =
				new DataPacketEventArgs(new DataPacket());
			responseArgs.DataPacket.Command = Command.ReturnDeviceID;
			responseArgs.DataPacket.DeviceNumber = 22; // Response from different device
			responseArgs.DataPacket.NumericData = 42;

			// EXPECT
			mocks.ReplayAll();

			// EXEC
			deviceCollection.Port = port;
			member.Port = port;

			packetReceivedRaiser.Raise(port,
									   responseArgs);

			// VERIFY
			Assert.IsNull(eventSource,
						  "Device should not raise packet event for different device number");

			mocks.VerifyAll();
		}


		private void OnDeviceMessageReceived(object sender, DeviceMessageEventArgs e)
		{
			Assert.IsNull(eventType,
						  "This should be the first event.");

			eventType = "DataPacketReceived";
			eventSource = sender;
			eventArgs = e;
		}


		private void OnDeviceMessageSent(object sender, DeviceMessageEventArgs e)
		{
			Assert.IsNull(eventType,
						  "This should be the first event.");

			eventType = "DataPacketSent";
			eventSource = sender;
			eventArgs = e;
		}


		private DeviceCollection CreateDeviceCollectionAndListen()
		{
			var deviceCollection = CreateDeviceCollection();

			deviceCollection.MessageSent +=
				OnDeviceMessageSent;
			deviceCollection.MessageReceived +=
				OnDeviceMessageReceived;
			return deviceCollection;
		}


		private DeviceCollection CreateDeviceCollection()
		{
			var deviceCollection = new DeviceCollection
			{
				DeviceNumber = nextDeviceNumber++,
				DeviceType = DeviceTypeMother.CreateDeviceTypeWithCommands()
			};

			return deviceCollection;
		}


		private ZaberDevice AddMember(DeviceCollection deviceCollection)
		{
			var member = new ZaberDevice
			{
				DeviceNumber = nextDeviceNumber++,
				DeviceType = DeviceTypeMother.CreateDeviceTypeWithCommands()
			};

			deviceCollection.Add(member);
			return member;
		}


		protected override DeviceCollection CreateList()
		{
			var list = CreateDeviceCollection();
			AddMember(list);

			return list;
		}


		private DeviceType CreateDeviceTypeWithCommands(params Command[] commands)
		{
			var deviceType = new DeviceType
			{
				Commands = commands.Select(cmd => new CommandInfo { Command = cmd }).ToList()
			};

			return deviceType;
		}


		private object eventSource;
		private DeviceMessageEventArgs eventArgs;
		private string eventType;
		private MockRepository mocks;
		private IZaberPort port;
		private IEventRaiser packetSentRaiser;
		private IEventRaiser packetReceivedRaiser;
		private byte nextDeviceNumber;
	}
}

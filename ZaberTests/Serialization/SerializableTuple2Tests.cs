﻿using System;
using NUnit.Framework;
using Zaber.Serialization;
using ZaberTest.Testing;

namespace ZaberTest.Serialization
{
	[TestFixture]
	[SetCulture("en-US")]
	public class SerializableTuple2Tests
	{
		[Test]
		public void TestCast()
		{
			var t1 = new Tuple<string, string>("Foo", "Bar");
			var t2 = (SerializableTuple<string, string>) t1;
			Assert.AreEqual(t1.Item1, t2.Item1);
			Assert.AreEqual(t1.Item2, t2.Item2);
			var t3 = (Tuple<string, string>) t2;
			Assert.AreEqual(t1.Item1, t3.Item1);
			Assert.AreEqual(t1.Item2, t3.Item2);
		}


		[Test]
		public void TestNonSerializableType()
		{
			// Generic Tuple<> is not serializable, so this test covers both that 
			// a SerializableTuple is not serializable if one of its type arguments
			// isn't, and that generic Tuples' won't automatically be converted.
			var d = new SerializableTuple<int, Tuple<string, string>>();
			d.Item1 = 5;
			d.Item2 = new Tuple<string, string>("Foo", "Bar");

			Assert.Throws<InvalidOperationException>(() => ObjectToXmlString.Serialize(d));
		}


		[Test]
		public void TestStrings()
		{
			var d = new SerializableTuple<string, string>();
			d.Item1 = "Foo";
			d.Item2 = "Bar";

			var s = ObjectToXmlString.Serialize(d);
			Assert.IsFalse(string.IsNullOrEmpty(s));
			var d2 = ObjectToXmlString.Deserialize<SerializableTuple<string, string>>(s);
			Assert.IsNotNull(d2);
			Assert.AreEqual(d.Item1, d2.Item1);
			Assert.AreEqual(d.Item2, d2.Item2);
		}


		[Test]
		public void TestValueTypes()
		{
			var d = new SerializableTuple<int, double>();
			d.Item1 = 5;
			d.Item2 = 7.2;

			var s = ObjectToXmlString.Serialize(d);
			Assert.IsFalse(string.IsNullOrEmpty(s));
			var d2 = ObjectToXmlString.Deserialize<SerializableTuple<int, double>>(s);
			Assert.IsNotNull(d2);
			Assert.AreEqual(d.Item1, d2.Item1);
			Assert.AreEqual(d.Item2, d2.Item2);
		}
	}
}

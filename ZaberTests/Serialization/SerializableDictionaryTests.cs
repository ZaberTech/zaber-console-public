﻿using System;
using NUnit.Framework;
using Zaber.Serialization;
using ZaberTest.Testing;

namespace Zaber.Tests.Serialization
{
	[TestFixture]
	[SetCulture("en-US")]
	public class SerializableDictionaryTests
	{
		[Test]
		public void TestNonSerializableType()
		{
			var d = new SerializableDictionary<int, Tuple<string, string>>();
			d[1] = new Tuple<string, string>("Foo", "bar");

			Assert.Throws<InvalidOperationException>(() => ObjectToXmlString.Serialize(d));
		}


		[Test]
		public void TestStrings()
		{
			var d = new SerializableDictionary<string, string>();
			d["foo"] = "bar";
			d["Foo"] = "Bar";

			var s = ObjectToXmlString.Serialize(d);
			Assert.IsFalse(string.IsNullOrEmpty(s));
			var d2 = ObjectToXmlString.Deserialize<SerializableDictionary<string, string>>(s);
			Assert.IsNotNull(d2);
			Assert.AreEqual(d.Count, d2.Count);

			Assert.AreEqual("bar", d2["foo"]);
			Assert.AreEqual("Bar", d2["Foo"]);
		}


		[Test]
		public void TestValueTypes()
		{
			var d = new SerializableDictionary<int, double>();
			for (var i = -5; i <= 5; ++i)
			{
				d[i] = 0.25 * i;
			}

			var s = ObjectToXmlString.Serialize(d);
			Assert.IsFalse(string.IsNullOrEmpty(s));
			var d2 = ObjectToXmlString.Deserialize<SerializableDictionary<int, double>>(s);
			Assert.IsNotNull(d2);
			Assert.AreEqual(d.Count, d2.Count);

			for (var i = -5; i <= 5; ++i)
			{
				Assert.AreEqual(0.25 * i, d2[i]);
			}
		}
	}
}

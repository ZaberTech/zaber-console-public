﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using log4net;
using NUnit.Framework;
using Zaber;

namespace ZaberTest
{
	public class MockPort : IZaberPort
	{
		#region -- Events --

		public event EventHandler<DataPacketEventArgs> DataPacketReceived;

		public event EventHandler<DataPacketEventArgs> DataPacketSent;

		public event EventHandler<ZaberPortErrorReceivedEventArgs> ErrorReceived;

		public event EventHandler ClosedUnexpectedly;

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Expect that the Send method will be called with a binary command.
		/// </summary>
		/// <param name="aDeviceNumber">The device number to expect.</param>
		/// <param name="aCommand">The command to expect.</param>
		/// <param name="aData">The data value to expect.</param>
		public void Expect(byte aDeviceNumber, Command aCommand, int aData)
		{
			var expectation = new Expectation
			{
				ExpectedRequest = CreateDataPacket(aDeviceNumber, aCommand, aData, null)
			};

			_expectations.AddLast(expectation);
			Log.DebugFormat("expect {0}", expectation.ExpectedRequest.FormatRequest());
		}


		/// <summary>
		///     Expect that the Send method will be called with a text command.
		/// </summary>
		/// <param name="aMessage">The command to expect.</param>
		public void Expect(string aMessage)
		{
			var textToSend =
				aMessage.StartsWith("/", StringComparison.Ordinal)
					? aMessage
					: "/" + aMessage;

			var expectation = new Expectation { ExpectedRequest = new DataPacket(textToSend) };

			_expectations.AddLast(expectation);
			Log.DebugFormat("expect {0}", expectation.ExpectedRequest.FormatRequest());
		}


		public void AddResponse(byte aDeviceNumber, Command aCommand, int aData)
			=> _expectations.Last.Value.ResponsePackets.Add(CreateDataPacket(aDeviceNumber,
																			 aCommand,
																			 aData,
																			 null));


		public void AddResponse(string aMessage)
			=> _expectations.Last.Value.ResponsePackets.Add(new DataPacket(aMessage));


		public void FireResponse(byte aDeviceNumber, Command aCommand, int aData)
		{
			if (null == DataPacketReceived)
			{
				return;
			}

			var dataPacket = CreateDataPacket(aDeviceNumber,
											  aCommand,
											  aData,
											  null);

			DataPacketReceived(this, new DataPacketEventArgs(dataPacket));
		}


		public void FireResponse(string aMessage)
		{
			if (null == DataPacketReceived)
			{
				return;
			}

			var dataPacket = new DataPacket(aMessage);

			DataPacketReceived(this, new DataPacketEventArgs(dataPacket));
		}


		public void FirePortError(ZaberPortError aError)
			=> ErrorReceived?.Invoke(this, new ZaberPortErrorReceivedEventArgs(aError));


		public void FirePortClosedUnexpectedly() => ClosedUnexpectedly?.Invoke(this, EventArgs.Empty);


		public void AddException(Exception aException) => _expectations.Last.Value.Exception = aException;


		public void AddPortError(ZaberPortError aError) => _expectations.Last.Value.PortErrors.Add(aError);


		public void Verify()
		{
			if (_expectations.Count > 0)
			{
				var sb = new StringBuilder();
				sb.AppendLine("Unmatched mock port expectations:");
				foreach (var expectation in _expectations)
				{
					if (null == expectation.ExpectedRequest)
					{
						sb.AppendLine("Receive()");
					}
					else
					{
						sb.AppendLine(expectation.ExpectedRequest.FormatRequest());
					}
				}

				Assert.Fail(sb.ToString());
			}

			Assert.That(ActualInvalidPacketCount,
						Is.EqualTo(ExpectedInvalidPacketCount),
						"invalid packet count");
		}


		/// <summary>
		///    Returns the number of expectations still outstanding.
		/// </summary>
		public int UnmatchedExpectationCount => _expectations.Count;


		/// <summary>
		///     Add expectations and responses from a port after reading a text file that is representative
		///     of what appears on the log on Zaber Console after the initial query of a joystick.
		/// </summary>
		public void ReadExpectationsFromFile(string aFilePath)
		{
			try
			{
				ReadExpectationsFromString(File.ReadAllText(aFilePath));
			}
			catch (Exception aException)
			{
				Assert.Fail(
					"File not found. Please verify the path is correct and the file is marked as content to be copied in the Visual Studio project. Error: "
				+ aException.Message);
			}
		}


		/// <summary>
		///     Add expectations and responses from a string copies for the Zaber Console message log.
		/// </summary>
		public void ReadExpectationsFromString(string aContent)
		{
			using (var reader = new StringReader(aContent))
			{
				string line;

				while ((line = reader.ReadLine()) != null)
				{
					var cleanLine = line.TrimStart();
					if (cleanLine.Length > 0)
					{
						switch (cleanLine[0])
						{
							case '/':
								Expect(cleanLine);
								break;
							case '@':
								AddResponse(cleanLine);
								break;
							case '#':
								AddResponse(cleanLine);
								break;
						}
					}
				}
			}
		}


		public string[] GetPortNames() => ExpectedPortNames;


		public void Open(string aPortName)
		{
			IsOpen = true;
			PortName = aPortName;
		}


		public void Send(byte aDeviceNumber, Command aCommand) => Send(aDeviceNumber, aCommand, 0);


		public void Send(byte aDeviceNumber, Command aCommand, int aData) => Send(aDeviceNumber, aCommand, aData, null);


		public void Send(byte aDeviceNumber, Command aCommand, int aData, Measurement aMeasurement)
		{
			var actualRequest = CreateDataPacket(aDeviceNumber, aCommand, aData, aMeasurement);
			Send(actualRequest);
		}


		/// <summary>
		///     Send a text-mode command to a device on the chain.
		/// </summary>
		/// <param name="aMessage">The full command to send to the device.</param>
		public void Send(string aMessage) => Send(aMessage, null);


		/// <summary>
		///     Send an ASCII command to a device on the chain.
		/// </summary>
		/// <param name="aMessage">See <see cref="DataPacket.Text" />.</param>
		/// <param name="aMeasurement">
		///     An optional measurement that the
		///     command's data value was calculated from. May be null.
		/// </param>
		public void Send(string aMessage, Measurement aMeasurement)
		{
			var textToSend =
				aMessage.StartsWith("/", StringComparison.Ordinal)
					? aMessage
					: "/" + aMessage;

			var dataPacket = new DataPacket(textToSend) { Measurement = aMeasurement };

			Send(dataPacket);
		}


		public void SendDelayed(byte aDeviceNumber, Command aCommand, int aData) => throw new NotSupportedException();


		public bool CancelDelayedPacket(DataPacket aPacket) => throw new NotSupportedException();


		public void ReportInvalidPacket()
		{
			ActualInvalidPacketCount++;
			Assert.That(ActualInvalidPacketCount,
						Is.LessThanOrEqualTo(ExpectedInvalidPacketCount),
						"invalid packet count");
		}


		public void Close() => IsOpen = false;


		public void Dispose() => Close();


		public int ExpectedInvalidPacketCount { get; set; }

		public int ActualInvalidPacketCount { get; private set; }

		public object SendLock { get; } = new object();

		public string PortName { get; private set; }

		public string[] ExpectedPortNames { get; set; }

		public int ReadTimeout { get; set; } = int.MaxValue;


		public bool IsOpen { get; private set; }


		/// <summary>
		///     Flag that gets or sets whether the port is in ASCII mode.
		///     If it's not in ASCII mode, then all received data will be
		///     parsed as binary, 6-byte packets.
		/// </summary>
		public bool IsAsciiMode { get; set; }


		/// <summary>
		///     Flag that gets or sets whether checksums are sent with each text
		///     message. If true, the checksum will be calculated with the
		///     Longitudinal Redundancy Check (LRC) algorithm.
		/// </summary>
		public bool AreChecksumsSent { get; set; }


		public int DelayMilliseconds { get; set; }


		public int BaudRate { get; set; }

		#endregion

		#region -- Interfaces --

		DataPacket IZaberPort.SendDelayed(byte aDeviceNumber, Command aCommand, int aData)
			=> throw new NotSupportedException();

		#endregion

		#region -- Non-Public Methods --

		private static DataPacket CreateDataPacket(byte aDeviceNumber,
												   Command aCommand,
												   int aData,
												   Measurement aMeasurement)
		{
			var request = new DataPacket
			{
				DeviceNumber = aDeviceNumber,
				Command = aCommand,
				NumericData = aData,
				Measurement = aMeasurement
			};

			return request;
		}


		private void Send(DataPacket aActualRequest)
		{
			if (0 == _expectations.Count)
			{
				Assert.Fail("Unexpected data packet: {0}", aActualRequest.FormatRequest());
			}

			var expectation = _expectations.First.Value;
			_expectations.RemoveFirst();

			Log.DebugFormat("sending {0}", aActualRequest.FormatRequest());

			var expectedCall =
				expectation.ExpectedRequest == null
					? "Receive()"
					: expectation.ExpectedRequest.FormatRequest();
			var actualCall = aActualRequest.FormatRequest();

			Assert.AreEqual(expectedCall, actualCall, "port call");

			if (null != expectation.Exception)
			{
				throw expectation.Exception;
			}

			DataPacketSent?.Invoke(this, new DataPacketEventArgs(aActualRequest));

			if (null != DataPacketReceived)
			{
				foreach (var response in expectation.ResponsePackets)
				{
					DataPacketReceived(this, new DataPacketEventArgs(response));
				}
			}

			if (null != ErrorReceived)
			{
				foreach (var error in expectation.PortErrors)
				{
					ErrorReceived(this, new ZaberPortErrorReceivedEventArgs(error));
				}
			}
		}

		#endregion

		#region -- Data --

		private class Expectation
		{
			#region -- Public Methods & Properties --

			public Expectation()
			{
				ResponsePackets = new List<DataPacket>();
				PortErrors = new List<ZaberPortError>();
			}


			public DataPacket ExpectedRequest { get; set; }

			public List<DataPacket> ResponsePackets { get; }

			public List<ZaberPortError> PortErrors { get; }

			public Exception Exception { get; set; }

			#endregion
		}

		private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private readonly LinkedList<Expectation> _expectations = new LinkedList<Expectation>();

		#endregion
	}
}

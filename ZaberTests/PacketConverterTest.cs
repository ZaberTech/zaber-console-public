using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using NUnit.Framework;
using Zaber;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	[SetUICulture("en-US")]
	public class PacketConverterTest
	{
		[SetUp]
		public void SetUp()
		{
			_receivedPacket = null;
			_portError = ZaberPortError.None;

			var historyCount = 2;
			_converter = new MockConverter(historyCount * PacketConverter.BINARY_PACKET_SIZE);
			_converter.DataPacketReceived += (s, e) => { _receivedPacket = e.DataPacket; };
			_converter.PortError += (s, e) => { _portError = e.ErrorType; };
		}


		[Test]
		public void BinaryResponseInAsciiMode()
		{
			// EXEC
			_converter.IsAsciiMode = true;

			// The leading 47 = '/', faking a valid ASCII command.
			ReceiveBytes(47, 42, 100, 0, 0, 0);

			var isFirstPacketReceived = _receivedPacket != null;

			ReceiveBytes(3, 55, 10, 0, 0, 0);
			var isSecondPacketReceived = _receivedPacket != null;

			Assert.That(isFirstPacketReceived,
						Is.False,
						"first packet received");

			// Data value of 10 looks like a line feed and triggers a packet.
			Assert.That(isSecondPacketReceived,
						Is.True,
						"second packet received");
			Assert.That(_receivedPacket.Text,
						Is.EqualTo("\x2f\x2a\x64\x00\x00\x00\x03\x37"),
						"packet text");
			Assert.That(_receivedPacket.MessageType,
						Is.EqualTo(MessageType.Request),
						"message type");
		}


		[Test]
		public void CreateBigBytes()
		{
			ReceiveBytes(0, 0, 4, 3, 2, 1);

			var data = _receivedPacket;

			Assert.AreEqual(0x01020304, // Hex makes it easy to see each byte value
							data.NumericData,
							"Data should match");
		}


		[Test]
		public void CreateNegativeBytes()
		{
			ReceiveBytes(0, 0, 246, 255, 255, 255);

			var data = _receivedPacket;

			Assert.AreEqual(-10,
							data.NumericData,
							"Data should match");
		}


		[Test]
		public void CreateSimpleBytes()
		{
			ReceiveBytes(3, 42, 100, 0, 0, 0);

			var data = _receivedPacket;

			Assert.AreEqual(3,
							data.DeviceNumber,
							"device number should match");
			Assert.AreEqual((Command) 42,
							data.Command,
							"Command should match");
			Assert.AreEqual(100,
							data.NumericData,
							"Data should match");
		}


		[Test]
		public void FormatPartialPacket()
		{
			var expectedPacket =
				"Partial packet: 0x0108FF";

			// EXEC
			ReceiveBytes(1, 8, 255);
			var packet = _converter.FormatPartialPacket();

			// VERIFY
			Assert.That(packet,
						Is.EqualTo(expectedPacket));
		}


		[Test]
		public void GetBigBytes()
		{
			var data = new DataPacket();
			data.NumericData = 0x01020304; // Hex makes it easy to see each byte value

			var bytes = PacketConverter.GetBytes(data);

			Assert.AreEqual(4,
							bytes[2],
							"Data's least significant byte should match");
			Assert.AreEqual(3,
							bytes[3],
							"Data's second byte should match");
			Assert.AreEqual(2,
							bytes[4],
							"Data's third byte should match");
			Assert.AreEqual(1,
							bytes[5],
							"Data's most significant byte should match");
		}


		[Test]
		public void GetNegativeBytes()
		{
			var data = new DataPacket();
			data.NumericData = -10;

			var bytes = PacketConverter.GetBytes(data);

			Assert.AreEqual(246,
							bytes[2],
							"Data's least significant byte should match");
			Assert.AreEqual(255,
							bytes[3],
							"Data's second byte should match");
			Assert.AreEqual(255,
							bytes[4],
							"Data's third byte should match");
			Assert.AreEqual(255,
							bytes[5],
							"Data's most significant byte should match");
		}


		[Test]
		public void GetNullBytes()
		{
			// SETUP
			DataPacket data = null;
			string msg = null;

			// EXEC
			try
			{
				PacketConverter.GetBytes(data);
				Assert.Fail("Should have thrown.");
			}
			catch (ArgumentNullException e)
			{
				msg = e.Message;
			}

			// VERIFY
			Assert.AreEqual("Value cannot be null.\r\nParameter name: aDataPacket",
							msg);
		}


		[Test]
		public void GetSimpleBytes()
		{
			var data = new DataPacket();
			data.DeviceNumber = 3;
			data.Command = Command.MoveAtConstantSpeed;
			data.NumericData = 100;

			var bytes = PacketConverter.GetBytes(data);

			Assert.AreEqual(3,
							bytes[0],
							"device number should match");
			Assert.AreEqual((byte) Command.MoveAtConstantSpeed,
							bytes[1],
							"Command should match");
			Assert.AreEqual(100,
							bytes[2],
							"Data should match");
		}


		[Test]
		public void History()
		{
			var expectedHistory =
				@"30ms since last byte. The last 12 bytes and times(with delta) were:
00 0(0)
00 0(0)
00 0(0)
---
00 0(0)
00 0(0)
00 0(0)
00 0(0)
00 0(0)
00 0(0)
---
01 10(10)
0C 15(5)
14 30(15)
".Replace("\r", "")
 .Replace("\n", Environment.NewLine);

			// EXEC
			_converter.ElapsedMilliseconds = 10;
			_converter.ReceiveByte(1);

			_converter.ElapsedMilliseconds = 15;
			_converter.ReceiveByte(12);

			_converter.ElapsedMilliseconds = 30;
			_converter.ReceiveByte(20);

			var history = _converter.FormatHistory();

			// VERIFY
			Assert.That(history,
						Is.EqualTo(expectedHistory));
		}


		[Test]
		public void HistoryAscii()
		{
			var expectedHistory =
				@"30ms since last byte. The last 12 bytes and times(with delta) were:
00 '\0' 0(0)
00 '\0' 0(0)
00 '\0' 0(0)
00 '\0' 0(0)
00 '\0' 0(0)
00 '\0' 0(0)
00 '\0' 0(0)
00 '\0' 0(0)
00 '\0' 0(0)
0A '\n' 10(10)
---
41 'A' 15(5)
42 'B' 30(15)
".Replace("\r", "")
 .Replace("\n", Environment.NewLine);

			_converter.IsAsciiMode = true;
			_converter.ElapsedMilliseconds = 10;
			_converter.ReceiveByte(10);

			_converter.ElapsedMilliseconds = 15;
			_converter.ReceiveByte(65);

			_converter.ElapsedMilliseconds = 30;
			_converter.ReceiveByte(66);

			var history = _converter.FormatHistory();

			// VERIFY
			Assert.That(history,
						Is.EqualTo(expectedHistory));
		}


		[Test]
		public void HistoryAsciiWrap()
		{
			var expectedHistory =
				@"0ms since last byte. The last 12 bytes and times(with delta) were:
0A '\n' 0(0)
---
41 'A' 0(0)
42 'B' 0(0)
43 'C' 0(0)
44 'D' 0(0)
0A '\n' 0(0)
---
61 'a' 0(0)
62 'b' 0(0)
63 'c' 0(0)
64 'd' 0(0)
65 'e' 0(0)
66 'f' 0(0)
".Replace("\r", "")
 .Replace("\n", Environment.NewLine);

			// EXEC
			_converter.IsAsciiMode = true;
			ReceiveBytes(65, 66, 67, 10, 65, 66, 67, 68, 10, 97, 98, 99, 100, 101, 102);
			var history = _converter.FormatHistory();

			// VERIFY
			Assert.That(history,
						Is.EqualTo(expectedHistory));
		}


		[Test]
		public void HistoryWrap()
		{
			var expectedHistory =
				@"0ms since last byte. The last 12 bytes and times(with delta) were:
02 0(0)
03 0(0)
04 0(0)
05 0(0)
06 0(0)
---
07 0(0)
08 0(0)
09 0(0)
0A 0(0)
0B 0(0)
0C 0(0)
---
0D 0(0)
".Replace("\r", "")
 .Replace("\n", Environment.NewLine);

			// EXEC
			ReceiveBytes(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13);
			var history = _converter.FormatHistory();

			// VERIFY
			Assert.That(history,
						Is.EqualTo(expectedHistory));
		}


		[Test]
		public void MalformedMessagesTriggerError()
		{
			var malformed = "$02 ! SDSS OK -- OMGWTFBBQ\r\n";

			_converter.IsAsciiMode = true;
			ReceiveText(malformed);
			Assert.That(_portError,
						Is.EqualTo(ZaberPortError.InvalidPacket),
						"Malformed message did not cause exception.");
		}


		[Test]
		public void NoHistory()
		{
			// SETUP
			var historyCount = 0;
			var converter = new PacketConverter(historyCount);

			// EXEC
			try
			{
				converter.FormatHistory();

				// VERIFY
				Assert.Fail("should have thrown");
			}
			catch (InvalidOperationException ex)
			{
				Assert.That(ex.Message,
							Is.EqualTo("History is not enabled."),
							"message");
			}
		}


		/// <summary>
		///     We don't expect to receive requests, so the / doesn't restart the
		///     packet.
		/// </summary>
		[Test]
		public void NoRestartText()
		{
			// EXEC
			var expectedText =
				"#01 0 Sample help message that mentions a '/ command'.";
			_converter.IsAsciiMode = true;
			ReceiveText(expectedText + "\r\n");

			// VERIFY
			Assert.That(_receivedPacket.Text,
						Is.EqualTo(expectedText),
						"text");
			Assert.That(_receivedPacket.MessageType,
						Is.EqualTo(MessageType.Comment),
						"message type");
		}


		/// <summary>
		///     If we get a partial packet and then a new one starts,
		///     we should throw away the partial one.
		/// </summary>
		[Test]
		[Sequential]
		public void RestartText([Values("!01 0 status idle",
									"#01 0 sample help message.",
									"@01 0 OK R IDLE -- 0")]
								string expectedText,
								[Values(MessageType.Alert,
									MessageType.Comment,
									MessageType.Response)]
								MessageType expectedType)
		{
			// EXEC
			_converter.IsAsciiMode = true;
			ReceiveText("@partial" + expectedText + "\r\n");

			// VERIFY
			Assert.That(_receivedPacket.Text,
						Is.EqualTo(expectedText),
						"text");
			Assert.That(_receivedPacket.MessageType,
						Is.EqualTo(expectedType),
						"message type");
		}


		[Test]
		public void TextResponseInAsciiMode()
		{
			var expectedText = "@01 0 OK R IDLE -- 0";

			// EXEC
			_converter.IsAsciiMode = true;
			ReceiveText(expectedText + "\r\n");

			// VERIFY
			Assert.That(_receivedPacket.MessageType,
						Is.EqualTo(MessageType.Response),
						"message type");
			Assert.That(_receivedPacket.Text,
						Is.EqualTo(expectedText),
						"text");
		}


		[Test]
		public void TextResponseInBinaryMode()
		{
			var packets = new List<DataPacket>();
			_converter.DataPacketReceived +=
				delegate(object sender, DataPacketEventArgs eventArgs) { packets.Add(eventArgs.DataPacket); };
			var expectedText = "@01 0 OK R IDLE -- 0";

			// EXEC
			ReceiveText(expectedText + "\r\n");
			Thread.Sleep(200);

			// VERIFY
			// The response text was 22 bytes long, so we should see three
			// packets, plus some still in the buffer.
			Assert.That(packets.Count,
						Is.EqualTo(3),
						"packet count");
			Assert.That(packets[0].MessageType,
						Is.EqualTo(MessageType.Binary),
						"message type");
			Assert.That(packets[0].DeviceNumber,
						Is.EqualTo((byte) '@'),
						"device number");

			// Clean up after ourselves.
			_converter.ClearBuffer();
		}


		[Test]
		public void ReceiveContinuation()
		{
			var part1 = "@01 0 OK IDLE -- 0 1 2 3 4 5\\";
			var part2 = "#01 0 cont 6 7 8 9 10\\";
			var part3 = "#01 0 cont 11 12 13 14 15";

			_converter.IsAsciiMode = true;
			ReceiveText(part1 + "\r\n");
			Assert.IsNull(_receivedPacket);
			ReceiveText(part2 + "\r\n");
			Assert.IsNull(_receivedPacket);
			ReceiveText(part3 + "\r\n");
			Assert.IsNotNull(_receivedPacket);
			
			Assert.AreEqual(MessageType.Response, _receivedPacket.MessageType);
			Assert.AreEqual("@01 0 OK IDLE -- 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15", _receivedPacket.Text);
		}


		[Test]
		public void ReceiveContinuationFirstPacketEmpty()
		{
			var part1 = "@01 0 01 OK IDLE -- \\";
			var part2 = "#01 0 01 cont abcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdab";

			_converter.IsAsciiMode = true;
			ReceiveText(part1 + "\r\n");
			Assert.IsNull(_receivedPacket);
			ReceiveText(part2 + "\r\n");
			Assert.IsNotNull(_receivedPacket);

			Assert.AreEqual(MessageType.Response, _receivedPacket.MessageType);
			Assert.AreEqual("@01 0 01 OK IDLE -- abcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdab", _receivedPacket.Text);
		}


		[Test]
		public void ReceiveContinuationMultipleEmptyPackets()
		{
			// This case is distinct from ReceiveContinuationFirstPacketEmpty() because
			// the first packet is handled differently when joining packets.
			var part1 = "@01 0 OK IDLE -- \\";
			var part2 = "#01 0 cont \\";
			var part3 = "#01 0 cont \\";
			var part4 = "#01 0 cont 1 2 3 4 5";

			_converter.IsAsciiMode = true;
			ReceiveText(part1 + "\r\n");
			Assert.IsNull(_receivedPacket);
			ReceiveText(part2 + "\r\n");
			Assert.IsNull(_receivedPacket);
			ReceiveText(part3 + "\r\n");
			Assert.IsNull(_receivedPacket);
			ReceiveText(part4 + "\r\n");
			Assert.IsNotNull(_receivedPacket);

			Assert.AreEqual(MessageType.Response, _receivedPacket.MessageType);
			Assert.AreEqual("@01 0 OK IDLE -- 1 2 3 4 5", _receivedPacket.Text);
		}


		[Test]
		public void DoesNotJoinInfoToContinuation()
		{
			var part1 = "@01 0 OK IDLE -- 0 1 2 3 4 5\\";
			var part2 = "#01 0 cont 6 7 8 9 10";
			var part3 = "#01 0 11 12 13 14 15";

			_converter.IsAsciiMode = true;
			ReceiveText(part1 + "\r\n");
			Assert.IsNull(_receivedPacket);
			ReceiveText(part2 + "\r\n");
			Assert.AreEqual("@01 0 OK IDLE -- 0 1 2 3 4 5 6 7 8 9 10", _receivedPacket.Text);
			ReceiveText(part3 + "\r\n");
			Assert.AreEqual("#01 0 11 12 13 14 15", _receivedPacket.Text);
		}


		[Test]
		public void ReceiveInterleavedContinuations()
		{
			var part1 = "@01 0 OK IDLE -- 0 1 2 3 4 5\\";
			var part2 = "@02 0 OK IDLE -- 0 -1 -2 -3 -4 -5\\";
			var part3 = "#01 0 cont 6 7 8 9 10\\";
			var part4 = "#02 0 cont -6 -7 -8 -9";
			var part5 = "#01 0 cont 11 12 13 14 15";

			_converter.IsAsciiMode = true;
			ReceiveText(part1 + "\r\n");
			Assert.IsNull(_receivedPacket);
			ReceiveText(part2 + "\r\n");
			Assert.IsNull(_receivedPacket);
			ReceiveText(part3 + "\r\n");
			Assert.IsNull(_receivedPacket);

			ReceiveText(part4 + "\r\n");
			Assert.IsNotNull(_receivedPacket);
			Assert.AreEqual(MessageType.Response, _receivedPacket.MessageType);
			Assert.AreEqual("@02 0 OK IDLE -- 0 -1 -2 -3 -4 -5 -6 -7 -8 -9", _receivedPacket.Text);

			ReceiveText(part5 + "\r\n");
			Assert.IsNotNull(_receivedPacket);
			Assert.AreEqual(MessageType.Response, _receivedPacket.MessageType);
			Assert.AreEqual("@01 0 OK IDLE -- 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15", _receivedPacket.Text);
		}


		[Test]
		public void ContinuationErrorTwoHeadersFromSameDevice()
		{
			var part1 = "@01 0 OK IDLE -- 1 2 3 4 5\\";
			var part2 = "@01 0 OK IDLE -- 6 7 8 9 10\\";

			_converter.IsAsciiMode = true;
			ReceiveText(part1 + "\r\n");
			Assert.IsNull(_receivedPacket);
			Assert.AreEqual(ZaberPortError.None, _portError);
			ReceiveText(part2 + "\r\n");
			Assert.IsNull(_receivedPacket);
			Assert.AreEqual(ZaberPortError.InvalidContinuation, _portError);
		}


		[Test]
		public void ContinuationErrorNoHeader()
		{
			var part1 = "#01 0 cont 6 7 8 9 10\\";

			_converter.IsAsciiMode = true;
			ReceiveText(part1 + "\r\n");
			Assert.IsNull(_receivedPacket);
			Assert.AreEqual(ZaberPortError.InvalidContinuation, _portError);
		}


		[Test]
		public void FormatContinuation()
		{
			var data = "@01 0 OK IDLE -- 0 1 2 3 4 5\\\r\n#01 0 cont 6 7 8 9 10\\\r\n";

			_converter.IsAsciiMode = true;
			ReceiveText(data);

			Assert.IsTrue(_converter.FormatPartialCompletions().Contains(data));
		}


		[Test]
		public void ClearBufferMidContinuation()
		{
			var part1 = "@01 0 OK IDLE -- 0 1 2 3 4 5\\";
			var part2 = "#01 0 cont 6 7 8 9 10\\";
			var part3 = "#01 0 cont 11 12 13 14 15";

			_converter.IsAsciiMode = true;
			ReceiveText(part1 + "\r\n");
			Assert.IsNull(_receivedPacket);
			ReceiveText(part2 + "\r\n");
			Assert.IsNull(_receivedPacket);
			Assert.AreEqual(ZaberPortError.None, _portError);

			_converter.ClearBuffer();
			ReceiveText(part3 + "\r\n");
			Assert.IsNull(_receivedPacket);
			Assert.AreEqual(ZaberPortError.InvalidContinuation, _portError);
		}


		private void ReceiveBytes(params byte[] bytes)
		{
			foreach (var b in bytes)
			{
				_converter.ReceiveByte(b);
			}
		}


		/// <summary>
		///     Convert text to bytes and receive them. "\r\n" will not be added.
		/// </summary>
		/// <param name="text">The text to receive</param>
		private void ReceiveText(string text) => ReceiveBytes(Encoding.ASCII.GetBytes(text));


		private MockConverter _converter;
		private DataPacket _receivedPacket;
		private ZaberPortError _portError;
	}

	internal class MockConverter : PacketConverter
	{
		#region -- Public Methods & Properties --
		public MockConverter(int historyCount)
		: base(historyCount)
		{
		}

		public long ElapsedMilliseconds { get; set; }

		#endregion

		#region -- Non-Public Methods --
		protected override long ReadStopwatch() => ElapsedMilliseconds;

		#endregion
	}
}

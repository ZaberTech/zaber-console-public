using NUnit.Framework;
using TickingTest;
using Zaber;

namespace ZaberTest
{
	/// <summary>
	///     Remaining tests:
	///     - Ignore alerts and info when waiting for response
	///     - Ignore others when waiting for alert
	///     - Implement polling
	///     - check firmware version before sending text commands?
	/// </summary>
	public class ConversationWithoutChannelsTest : ConversationTest
	{
		[Test]
		public void EnableMessageIdsWhileWaiting()
			=> TestFramework.RunOnce(new EnableMessageIdsWhileWaitingThreads(this));


		/// <summary>
		///     This is testing a scenario where the main thread makes a request
		///     and then a background thread makes a request. The response to the
		///     first request is received just after the second response goes out.
		///     Here's the timeline:
		///     0: main thread requests MoveRelative
		///     1: background thread requests MoveAbsolute
		///     2: response thread responds to MoveRelative
		///     3: response thread responds to MoveAbsolute
		///     Unfortunately, without message ids enabled, we have to assume
		///     that the first response is for the last request and therefore
		///     replaces the first request. The first request will throw an
		///     exception and the second request will get the first response.
		///     The second response will be ignored.
		/// </summary>
		[Test]
		public override void ReceiveJustAfterSend() => TestFramework.RunOnce(new ReceiveJustAfterSendThreads(this));


		/// <summary>
		///     A response is received to an unrelated command and that command
		///     doesn't replace the request we were waiting for.  Unfortunately,
		///     without message ids enabled, we can't reliably detect that
		///     the response is unrelated to our request, so we just assume that
		///     the response is for the last request and return it normally.
		///     The actual response just gets ignored when it finally arrives.
		/// </summary>
		[Test]
		public override void UnexpectedResponseDoesntReplace()
			=> TestFramework.RunOnce(new UnexpectedResponseDoesntReplaceThreads(this));


		/// <summary>
		///     A response is received to an unrelated command and that command
		///     has replaced the request we were waiting for. Unfortunately,
		///     without message ids enabled, we can't reliably detect that
		///     the response is unrelated to our request, so we just assume that
		///     the response is for the last request and return it normally.
		/// </summary>
		[Test]
		public override void UnexpectedResponseReplaces()
			=> TestFramework.RunOnce(new UnexpectedResponseReplacesThreads(this));


		public override int AdjustData(int data, byte messageId) => data;


		private class UnexpectedResponseDoesntReplaceThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public UnexpectedResponseDoesntReplaceThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// EXPECT
				Port.Send(Device.DeviceNumber,
						  Command.MoveRelative,
						  MainTest.AdjustData(1000, 1),
						  new Measurement(1, UnitOfMeasure.Millimeter));

				FinishRecording();

				// EXEC
				var response =
					Conversation.Request(Command.MoveRelative, 1000);

				// VERIFY
				AssertTick(1); // even though it's wrong
				Mocks.VerifyAll();

				Assert.AreEqual(Command.ReturnDeviceID,
								response.Command,
								"Response data should match (even though it's wrong)");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				var responsePacket = new DataPacket();
				responsePacket.Command = Command.ReturnDeviceID;
				responsePacket.NumericData = 131;
				responsePacket.DeviceNumber = Device.DeviceNumber;

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket));

				WaitForTick(2);

				responsePacket.Command = Command.MoveRelative;
				responsePacket.NumericData = MainTest.AdjustData(12345, 1);

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket));
			}

			#endregion
		}

		private class UnexpectedResponseReplacesThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public UnexpectedResponseReplacesThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// EXPECT
				Port.Send(Device.DeviceNumber,
						  Command.MoveRelative,
						  MainTest.AdjustData(1000, 1),
						  new Measurement(1, UnitOfMeasure.Millimeter));

				FinishRecording();

				// EXEC
				var response =
					Conversation.Request(Command.MoveRelative, 1000);

				// VERIFY
				AssertTick(1);
				Mocks.VerifyAll();

				Assert.AreEqual(Command.Stop,
								response.Command,
								"Response command should match");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				var stopCommandResponsePacket = new DataPacket();
				stopCommandResponsePacket.Command = Command.Stop;
				stopCommandResponsePacket.NumericData = 0;
				stopCommandResponsePacket.DeviceNumber = Device.DeviceNumber;

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(stopCommandResponsePacket));
			}

			#endregion
		}

		private class ReceiveJustAfterSendThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public ReceiveJustAfterSendThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void FirstRequest()
			{
				// EXPECT
				Port.Send(Device.DeviceNumber,
						  Command.MoveRelative,
						  MainTest.AdjustData(1000, 1),
						  new Measurement(1, UnitOfMeasure.Millimeter));
				Port.Send(Device.DeviceNumber,
						  Command.MoveAbsolute,
						  MainTest.AdjustData(54321, 2),
						  new Measurement(54.321, UnitOfMeasure.Millimeter));

				FinishRecording();

				// EXEC
				DeviceMessage response = null;
				try
				{
					Conversation.Request(Command.MoveRelative, 1000);
					Assert.Fail("Should have thrown.");
				}
				catch (RequestReplacedException ex)
				{
					response = ex.ReplacementResponse;
				}

				// VERIFY
				AssertTick(3);
				Mocks.VerifyAll();

				Assert.AreEqual(Command.MoveRelative,
								response.Command,
								"Response command from exception should match");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void SecondRequest()
			{
				WaitForTick(2);
				var secondResponse =
					Conversation.Request(Command.MoveAbsolute, 54321);

				// VERIFY
				AssertTick(3);
				Assert.AreEqual(Command.MoveRelative,
								secondResponse.Command,
								"Second response command should match");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Responses()
			{
				WaitForTick(3);

				var responsePacket1 = new DataPacket();
				responsePacket1.Command = Command.MoveRelative;
				responsePacket1.NumericData = MainTest.AdjustData(12345, 1); // position and msgId
				responsePacket1.DeviceNumber = Device.DeviceNumber;

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket1));

				WaitForTick(4);

				var responsePacket2 = new DataPacket();
				responsePacket2.Command = Command.MoveAbsolute;
				responsePacket2.NumericData = MainTest.AdjustData(54321, 2); // position and msgId
				responsePacket2.DeviceNumber = Device.DeviceNumber;

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket2));
			}

			#endregion
		}

		private class EnableMessageIdsWhileWaitingThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public EnableMessageIdsWhileWaitingThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// EXPECT
				Port.Send(Device.DeviceNumber,
						  Command.SetHoldCurrent,
						  1000,
						  new Measurement(1000, UnitOfMeasure.Data)); // no message id

				FinishRecording();

				// EXEC
				var response =
					Conversation.Request(Command.SetHoldCurrent, 1000); // no message id because they're disabled.
				AssertTick(1);

				// VERIFY
				Mocks.VerifyAll();

				Assert.AreEqual(12345,
								response.NumericData,
								"Response data should match");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				Device.AreMessageIdsEnabled = true; // turn on after request is made.

				var responsePacket = new DataPacket();
				responsePacket.Command = Command.SetHoldCurrent;
				responsePacket.NumericData = 12345; // position (no message id because it was sent with none.)
				responsePacket.DeviceNumber = Device.DeviceNumber;

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket));
			}

			#endregion
		}


		protected override ZaberDevice CreateDevice(byte deviceNumber)
		{
			var device = base.CreateDevice(deviceNumber);
			device.AreMessageIdsEnabled = false;
			return device;
		}
	}
}

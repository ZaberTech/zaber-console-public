using System.Collections.Generic;
using NUnit.Framework;
using Rhino.Mocks;
using Rhino.Mocks.Interfaces;
using TickingTest;
using Zaber;

namespace ZaberTest
{
	[TestFixture]
	public class ConversationCollectionTest : ListTest<ConversationCollection, Conversation>
	{
		[TearDown]
		public void Teardown() => Conversation.ResetMessageIdReservations();


		[Test]
		public void ErrorResponse() => TestFramework.RunOnce(new ErrorResponseThreads());


		/// <summary>
		///     Shows a scenario where the message id of the single conversation
		///     collides with the message id of the conversation collection, so
		///     it has to move on to the next available id.
		/// </summary>
		[Test]
		public void MessageIdCollisionInCollection() => TestFramework.RunOnce(new MessageIdCollisionThreads());


		[Test]
		public void PollUntilIdle() => TestFramework.RunOnce(new PollUntilIdleThreads());


		[Test]
		public void Request() => TestFramework.RunOnce(new RequestThreads());


		[Test]
		public void RequestCollectionNoData() => TestFramework.RunOnce(new RequestCollectionNoDataThreads());


		[Test]
		public void RequestCollectionSeparate() => TestFramework.RunOnce(new RequestCollectionSeparateThreads());


		[Test]
		public void RequestCollectionThroughAlias()
			=> TestFramework.RunOnce(new RequestCollectionThroughAliasThreads());


		[Test]
		public void ResponsesOutOfOrder() => TestFramework.RunOnce(new ResponsesOutOfOrderThreads());


		[Test]
		public void RetryCountSetOnMembers()
		{
			// SETUP
			var mocks = new MockRepository();
			var port = CreatePort(mocks);
			var device1 = CreateDevice(1);
			var aliasDevice = CreateDeviceCollection(100);
			aliasDevice.Add(device1);
			const int expectedRetryCount = 12;

			// EXPECT
			mocks.ReplayAll();

			// EXEC
			device1.Port = port;
			aliasDevice.Port = port;
			var conversation1 = new Conversation(device1);
			var conversations =
				new ConversationCollection(aliasDevice) { conversation1 };

			conversations.RetryCount = expectedRetryCount;

			// VERIFY
			mocks.VerifyAll();
			Assert.That(conversation1.RetryCount,
						Is.EqualTo(expectedRetryCount),
						"member conversation's retry count");
		}


		/// <summary>
		///     Can the conversation collection retry a request after a port error?
		///     Timeline:
		///     0: send request to alias device number
		///     1: port error, so send request again but from first and second device numbers
		///     2: send response from first device
		///     3: send response from second device
		/// </summary>
		[Test]
		public void RetryPortError() => TestFramework.RunOnce(new RetryPortErrorThreads());


		/// <summary>
		///     Can the conversation collection retry a request after a port error?
		///     Timeline:
		///     0: send requests to first and second device numbers
		///     1: port error, so send request again to alias device number
		///     2: send response from first device
		///     3: send response from second device
		/// </summary>
		[Test]
		public void RetryWithIndividualDataValues()
			=> TestFramework.RunOnce(new RetryWithIndividualDataValuesThreads());


		[Test]
		public void RM4327_CleanupAfterMessageIDCollisionInAscii()
		{
			var mocks = new MockRepository();
			var port = CreatePort(mocks);
			port.IsAsciiMode = true;

			var allDevices = new DeviceCollection
			{
				DeviceType = new DeviceType(),
				Port = port
			};

			var dev1 = CreateDevice(1);
			dev1.Port = port;
			dev1.AreAsciiMessageIdsEnabled = true;
			dev1.DeviceType.FirmwareVersion = new FirmwareVersion(6, 28);
			var dev2 = CreateDevice(2);
			dev2.Port = port;
			dev2.AreAsciiMessageIdsEnabled = true;
			dev2.DeviceType.FirmwareVersion = new FirmwareVersion(6, 28);

			allDevices.Add(dev1);
			allDevices.Add(dev2);

			var collection = new ConversationCollection(allDevices)
			{
				new Conversation(dev1),
				new Conversation(dev2)
			};

			collection[1].StartTopic(5);
			Assert.IsFalse(collection[0].IsWaiting);

			// Before the bug fix, the next call would leak a new topic in the first conversation.
			Assert.Throws<ActiveMessageIdException>(() => collection.StartTopicCollection(5));
			Assert.IsFalse(collection[0].IsWaiting);
		}


		[Test]
		public void RM4327_CleanupAfterMessageIDCollisionInBinary()
		{
			var mocks = new MockRepository();
			var port = CreatePort(mocks);
			port.IsAsciiMode = false;

			var allDevices = new DeviceCollection
			{
				DeviceType = new DeviceType(),
				Port = port,
				AreMessageIdsEnabled = true
			};

			var dev1 = CreateDevice(1);
			dev1.Port = port;
			dev1.AreMessageIdsEnabled = true;
			dev1.DeviceType.FirmwareVersion = new FirmwareVersion(6, 28);
			var dev2 = CreateDevice(2);
			dev2.Port = port;
			dev2.AreMessageIdsEnabled = true;
			dev2.DeviceType.FirmwareVersion = new FirmwareVersion(6, 28);

			allDevices.Add(dev1);
			allDevices.Add(dev2);

			var collection = new ConversationCollection(allDevices)
			{
				new Conversation(dev1),
				new Conversation(dev2)
			};

			collection[1].StartTopic(5);
			Assert.IsFalse(collection[0].IsWaiting);

			// Before the bug fix, the next call would leak a new topic in the first conversation.
			Assert.Throws<ActiveMessageIdException>(() => collection.StartTopicCollection(5));
			Assert.IsFalse(collection[0].IsWaiting);
		}


		[Test]
		public void RM4327_TopicLeakDoesNotCauseIdExhaustionInAscii()
		{
			var mocks = new MockRepository();
			var port = CreatePort(mocks);
			port.IsAsciiMode = true;

			var allDevices = new DeviceCollection
			{
				DeviceType = new DeviceType(),
				Port = port
			};

			var dev1 = CreateDevice(1);
			dev1.Port = port;
			dev1.AreAsciiMessageIdsEnabled = true;
			dev1.DeviceType.FirmwareVersion = new FirmwareVersion(6, 28);
			var dev2 = CreateDevice(2);
			dev2.Port = port;
			dev2.AreAsciiMessageIdsEnabled = true;
			dev2.DeviceType.FirmwareVersion = new FirmwareVersion(6, 28);

			allDevices.Add(dev1);
			allDevices.Add(dev2);

			var collection = new ConversationCollection(allDevices)
			{
				new Conversation(dev1),
				new Conversation(dev2)
			};

			collection[0].StartTopic(0);

			// Before the fix, starting a collection topic when individual devices
			// had message IDs enabled but the collection did not would cause
			// an out of IDs error if ID 0 was already in use.
			collection.StartTopic();
		}


		[Test]
		public void RM4327_TopicLeakDoesNotCauseIdExhaustionInBinary()
		{
			var mocks = new MockRepository();
			var port = CreatePort(mocks);
			port.IsAsciiMode = false;

			var allDevices = new DeviceCollection
			{
				DeviceType = new DeviceType(),
				Port = port,
				AreMessageIdsEnabled = true
			};

			var dev1 = CreateDevice(1);
			dev1.Port = port;
			dev1.AreMessageIdsEnabled = true;
			dev1.DeviceType.FirmwareVersion = new FirmwareVersion(6, 28);
			var dev2 = CreateDevice(2);
			dev2.Port = port;
			dev2.AreMessageIdsEnabled = true;
			dev2.DeviceType.FirmwareVersion = new FirmwareVersion(6, 28);

			allDevices.Add(dev1);
			allDevices.Add(dev2);

			var collection = new ConversationCollection(allDevices)
			{
				new Conversation(dev1),
				new Conversation(dev2)
			};

			collection[0].StartTopic(0);

			// Before the fix, starting a collection topic when individual devices
			// had message IDs enabled but the collection did not would cause
			// an out of IDs error if ID 0 was already in use.
			collection.StartTopic();
		}


		[Test]
		public void Sort()
		{
			// SETUP
			var list = CreateList(5);
			var conversation3 = list[2]; // device number 3 is at index 2
			list.RemoveAt(2);
			list.Insert(0, conversation3);

			// EXEC
			list.Sort();

			// VERIFY
			Assert.That(list[2].Device.DeviceNumber,
						Is.EqualTo(conversation3.Device.DeviceNumber),
						"device number");
		}


		[Test]
		public void StartTopic() => TestFramework.RunOnce(new StartTopicThreads());


		/// <summary>
		///     allConversations contains itself.
		/// </summary>
		[Test]
		public void StartTopicWithCollectionCycle()
		{
			// SETUP
			var device = CreateDevice(1);
			var aliasDevice = CreateDeviceCollection(100);
			var allDevices = CreateDeviceCollection(0);

			var conversation = new Conversation(device);
			var aliasConversation = new ConversationCollection(aliasDevice);
			var allConversations = new ConversationCollection(allDevices);

			// EXPECT
			var mocks = new MockRepository();
			var port = CreatePort(mocks);
			mocks.ReplayAll();

			device.Port = port;
			aliasDevice.Port = port;
			allDevices.Port = port;

			// EXEC
			aliasConversation.Add(conversation);
			allConversations.Add(conversation);
			allConversations.Add(aliasConversation);
			allConversations.Add(allConversations);

			var topicCollection = allConversations.StartTopicCollection();

			// VERIFY
			Assert.AreEqual(1,
							topicCollection.Count,
							"number of topics");
		}


		[Test]
		public void SkipReservedMessageIds()
		{
			var device1 = CreateDevice(1);
			var device2 = CreateDevice(2);
			var allDevices = CreateDeviceCollection(0);
			allDevices.Add(device1);
			allDevices.Add(device2);

			var conversation1 = new Conversation(device1);
			var conversation2 = new Conversation(device2);
			var allConversations = new ConversationCollection(allDevices);
			allConversations.Add(conversation1);
			allConversations.Add(conversation2);

			var mocks = new MockRepository();
			var port = CreatePort(mocks);
			mocks.ReplayAll();

			device1.Port = port;
			device2.Port = port;
			allDevices.Port = port;

			Conversation.ReserveMessageId(1);
			Conversation.ReserveMessageId(3);
			var topic1a = conversation1.StartTopic();
			var topic2 = conversation2.StartTopic();
			var topic1b = conversation1.StartTopic();
			Assert.AreEqual(2, topic1a.MessageId);
			Assert.AreEqual(2, topic2.MessageId);
			Assert.AreEqual(4, topic1b.MessageId);

			topic1a.Cancel();
			topic1b.Cancel();
			topic2.Cancel();
		}


		/// <summary>
		///     Here, the conversation is only found under aliasConversation, not directly in
		///     allConversations. We need to traverse the nested collections to look for
		///     conversations we don't already know about.
		/// </summary>
		[Test]
		public void StartTopicWithDeeplyNestedCollection()
		{
			// SETUP
			var device = CreateDevice(1);
			var aliasDevice = CreateDeviceCollection(100);
			var allDevices = CreateDeviceCollection(0);

			var conversation = new Conversation(device);
			var aliasConversation = new ConversationCollection(aliasDevice);
			var allConversations = new ConversationCollection(allDevices);

			// EXPECT
			var mocks = new MockRepository();
			var port = CreatePort(mocks);
			mocks.ReplayAll();

			device.Port = port;
			aliasDevice.Port = port;
			allDevices.Port = port;

			// EXEC
			// conversation is not a direct member of allConversations.
			aliasConversation.Add(conversation);
			allConversations.Add(aliasConversation);

			var topicCollection = allConversations.StartTopicCollection();

			// VERIFY
			Assert.AreEqual(1,
							topicCollection.Count,
							"number of topics");
		}


		/// <summary>
		///     Here, the conversation is a member of allConversations and aliasConversation,
		///     so aliasConversation should be ignored.
		/// </summary>
		[Test]
		public void StartTopicWithNestedCollection()
		{
			// SETUP
			var device = CreateDevice(1);
			var aliasDevice = CreateDeviceCollection(100);
			var allDevices = CreateDeviceCollection(0);

			var conversation = new Conversation(device);
			var aliasConversation = new ConversationCollection(aliasDevice);
			var allConversations = new ConversationCollection(allDevices);

			// EXPECT
			var mocks = new MockRepository();
			var port = CreatePort(mocks);
			mocks.ReplayAll();

			device.Port = port;
			aliasDevice.Port = port;
			allDevices.Port = port;

			// EXEC
			aliasConversation.Add(conversation);
			allConversations.Add(conversation);
			allConversations.Add(aliasConversation);

			var topicCollection = allConversations.StartTopicCollection();

			// VERIFY
			Assert.AreEqual(1,
							topicCollection.Count,
							"number of topics");
		}


		[Test]
		public void Timeout() => TestFramework.RunOnce(new TimeoutThreads());


		/// <summary>
		///     When UnexpectedResponseIsInvalid is set, member responses
		///     should not trigger a port error.
		/// </summary>
		[Test]
		public void UnexpectedResponseIsInvalid() => TestFramework.RunOnce(new UnexpectedResponseIsInvalidThreads());


		private class RequestThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Request()
			{
				// SETUP
				var mocks = new MockRepository();
				_port = CreatePort(mocks);
				var device1 = CreateDevice(FirstDeviceNumber);
				var device2 = CreateDevice(SecondDeviceNumber);
				var aliasDevice = CreateDeviceCollection(AliasDeviceNumber);

				// EXPECT
				_packetReceivedRaiser = CreateDataPacketReceivedRaiser(_port);
				_port.Send(AliasDeviceNumber,
						   Command.MoveRelative,
						   AdjustData(1000, 1),
						   new Measurement(1, UnitOfMeasure.Millimeter));

				mocks.ReplayAll();

				// EXEC
				aliasDevice.Port = _port;
				device1.Port = _port;
				device2.Port = _port;
				var conversation1 = new Conversation(device1);
				var conversation2 = new Conversation(device2);
				var conversations =
					new ConversationCollection(aliasDevice)
					{
						conversation1,
						conversation2
					};

				var response =
					conversations.Request(Command.MoveRelative, 1000);
				AssertTick(2);

				// VERIFY
				mocks.VerifyAll();

				Assert.AreEqual(FirstDeviceResponse,
								response.NumericData,
								"Response data should match first device's response");
				Assert.IsFalse(conversations.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				var responsePacket1 = new DataPacket
				{
					Command = Command.MoveRelative,
					NumericData = AdjustData(FirstDeviceResponse,
											 1), // position and msgId
					DeviceNumber = FirstDeviceNumber
				};

				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responsePacket1));

				WaitForTick(2);

				var responsePacket2 = new DataPacket
				{
					Command = Command.MoveRelative,
					NumericData = AdjustData(SecondDeviceResponse,
											 1), // position and msgId
					DeviceNumber = SecondDeviceNumber
				};

				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responsePacket2));
			}

			#endregion

			#region -- Data --

			private const byte AliasDeviceNumber = 99;
			private const byte FirstDeviceNumber = 17;
			private const byte SecondDeviceNumber = 42;
			private const int FirstDeviceResponse = 12345;
			private const int SecondDeviceResponse = 9999;
			private IZaberPort _port;
			private IEventRaiser _packetReceivedRaiser;

			#endregion
		}

		private class MessageIdCollisionThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void SingleRequest()
			{
				// SETUP
				var mocks = new MockRepository();
				_port = CreatePort(mocks);
				var device1 = CreateDevice(FirstDeviceNumber);
				var aliasDevice = CreateDeviceCollection(AliasDeviceNumber);

				// EXPECT
				_packetReceivedRaiser = CreateDataPacketReceivedRaiser(_port);
				_port.Send(FirstDeviceNumber,
						   Command.MoveRelative,
						   AdjustData(1000, 1),
						   new Measurement(1, UnitOfMeasure.Millimeter));
				_port.Send(AliasDeviceNumber,
						   Command.ReturnDeviceID,
						   AdjustData(0, 2),
						   new Measurement(0, UnitOfMeasure.Data));

				mocks.ReplayAll();

				// EXEC
				aliasDevice.Port = _port;
				device1.Port = _port;
				var conversation1 = new Conversation(device1);
				_conversations = new ConversationCollection(aliasDevice) { conversation1 };

				var response =
					conversation1.Request(Command.MoveRelative, 1000);
				AssertTick(3);

				// VERIFY
				mocks.VerifyAll();

				Assert.AreEqual(MoveResponse,
								response.NumericData,
								"Response data should match device's first response");
				Assert.IsFalse(_conversations.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void CollectionRequest()
			{
				WaitForTick(1);

				// EXEC
				var response = _conversations.Request(Command.ReturnDeviceID);
				AssertTick(2);

				// VERIFY
				Assert.AreEqual(DeviceIdResponse,
								response.NumericData,
								"Response data should match device's second response");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(2);

				var responsePacket2 = new DataPacket
				{
					Command = Command.ReturnDeviceID,
					NumericData = AdjustData(DeviceIdResponse,
											 2), // data and msgId
					DeviceNumber = FirstDeviceNumber
				};

				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responsePacket2));

				WaitForTick(3);

				var responsePacket1 = new DataPacket
				{
					Command = Command.MoveRelative,
					NumericData = AdjustData(MoveResponse,
											 1), // position and msgId
					DeviceNumber = FirstDeviceNumber
				};

				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responsePacket1));
			}

			#endregion

			#region -- Data --

			private const byte AliasDeviceNumber = 99;
			private const byte FirstDeviceNumber = 17;
			private const int MoveResponse = 12345;
			private const int DeviceIdResponse = 3003;
			private IZaberPort _port;
			private IEventRaiser _packetReceivedRaiser;
			private ConversationCollection _conversations;

			#endregion
		}

		private class TimeoutThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Request()
			{
				// SETUP
				var mocks = new MockRepository();
				_port = CreatePort(mocks);
				var device1 = CreateDevice(FirstDeviceNumber);
				var device2 = CreateDevice(SecondDeviceNumber);
				var aliasDevice = CreateDeviceCollection(AliasDeviceNumber);
				_mockTimer = new MockTimeoutTimer();

				// EXPECT
				_packetReceivedRaiser = CreateDataPacketReceivedRaiser(_port);
				_port.Send(AliasDeviceNumber,
						   Command.MoveRelative,
						   AdjustData(1000, 1),
						   new Measurement(1, UnitOfMeasure.Millimeter));

				mocks.ReplayAll();

				// EXEC
				aliasDevice.Port = _port;
				device1.Port = _port;
				device2.Port = _port;
				var conversation1 = new Conversation(device1);
				var conversation2 = new Conversation(device2);
				var conversations =
					new ConversationCollection(aliasDevice)
					{
						conversation1,
						conversation2
					};
				conversations.TimeoutTimer = _mockTimer;

				try
				{
					conversations.Request(Command.MoveRelative, 1000);

					Assert.Fail("Should have thrown.");
				}
				catch (RequestTimeoutException)
				{
				}

				AssertTick(2);

				// VERIFY
				mocks.VerifyAll();

				Assert.IsTrue(conversations.IsWaiting,
							  "Conversation should still be waiting after timeout");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				var responsePacket1 = new DataPacket
				{
					Command = Command.MoveRelative,
					NumericData = AdjustData(FirstDeviceResponse,
											 1), // position and msgId
					DeviceNumber = FirstDeviceNumber
				};

				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responsePacket1));

				WaitForTick(2);

				_mockTimer.TimeoutNow();
			}

			#endregion

			#region -- Data --

			private const byte AliasDeviceNumber = 99;
			private const byte FirstDeviceNumber = 17;
			private const byte SecondDeviceNumber = 42;
			private const int FirstDeviceResponse = 12345;
			private const int SecondDeviceResponse = 9999;
			private IZaberPort _port;
			private IEventRaiser _packetReceivedRaiser;
			private MockTimeoutTimer _mockTimer;

			#endregion
		}

		private class StartTopicThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Request()
			{
				// SETUP
				var mocks = new MockRepository();
				_port = CreatePort(mocks);
				var device1 = CreateDevice(FirstDeviceNumber);
				var device2 = CreateDevice(SecondDeviceNumber);
				var aliasDevice = CreateDeviceCollection(AliasDeviceNumber);

				// EXPECT
				_packetReceivedRaiser = CreateDataPacketReceivedRaiser(_port);
				_port.Send(AliasDeviceNumber,
						   Command.MoveRelative,
						   AdjustData(1000, 1),
						   new Measurement(1, UnitOfMeasure.Millimeter));

				mocks.ReplayAll();

				// EXEC
				aliasDevice.Port = _port;
				device1.Port = _port;
				device2.Port = _port;
				var conversation1 = new Conversation(device1);
				var conversation2 = new Conversation(device2);
				var conversations =
					new ConversationCollection(aliasDevice)
					{
						conversation1,
						conversation2
					};

				var topic = conversations.StartTopic();
				aliasDevice.Send(Command.MoveRelative, 1000, topic.MessageId);
				topic.Wait();
				topic.Validate();
				var response = topic.Response;
				AssertTick(2);

				// VERIFY
				mocks.VerifyAll();

				Assert.AreEqual(FirstDeviceResponse,
								response.NumericData,
								"Response data should match first device's response");
				Assert.IsFalse(conversations.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				var responsePacket1 = new DataPacket
				{
					Command = Command.MoveRelative,
					NumericData = AdjustData(FirstDeviceResponse,
											 1), // position and msgId
					DeviceNumber = FirstDeviceNumber
				};

				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responsePacket1));

				WaitForTick(2);

				var responsePacket2 = new DataPacket
				{
					Command = Command.MoveRelative,
					NumericData = AdjustData(SecondDeviceResponse,
											 1), // position and msgId
					DeviceNumber = SecondDeviceNumber
				};

				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responsePacket2));
			}

			#endregion

			#region -- Data --

			private const byte AliasDeviceNumber = 99;
			private const byte FirstDeviceNumber = 17;
			private const byte SecondDeviceNumber = 42;
			private const int FirstDeviceResponse = 12345;
			private const int SecondDeviceResponse = 9999;
			private IZaberPort _port;
			private IEventRaiser _packetReceivedRaiser;

			#endregion
		}

		private class ResponsesOutOfOrderThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Request()
			{
				// SETUP
				var mocks = new MockRepository();
				_port = CreatePort(mocks);
				var device1 = CreateDevice(FirstDeviceNumber);
				var device2 = CreateDevice(SecondDeviceNumber);
				var aliasDevice = CreateDeviceCollection(AliasDeviceNumber);

				// EXPECT
				_packetReceivedRaiser = CreateDataPacketReceivedRaiser(_port);
				_port.Send(AliasDeviceNumber,
						   Command.MoveRelative,
						   AdjustData(1000, 1),
						   new Measurement(1, UnitOfMeasure.Millimeter));

				mocks.ReplayAll();

				// EXEC
				aliasDevice.Port = _port;
				device1.Port = _port;
				device2.Port = _port;
				var conversation1 = new Conversation(device1);
				var conversation2 = new Conversation(device2);
				var conversations =
					new ConversationCollection(aliasDevice)
					{
						conversation1,
						conversation2
					};

				var response =
					conversations.Request(Command.MoveRelative, 1000);
				AssertTick(2);

				// VERIFY
				mocks.VerifyAll();

				Assert.AreEqual(FirstDeviceResponse,
								response.NumericData,
								"Response data should match first device's response");
				Assert.IsFalse(conversations.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				var responsePacket2 = new DataPacket
				{
					Command = Command.MoveRelative,
					NumericData = AdjustData(SecondDeviceResponse,
											 1), // position and msgId
					DeviceNumber = SecondDeviceNumber
				};

				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responsePacket2));

				WaitForTick(2);

				var responsePacket1 = new DataPacket
				{
					Command = Command.MoveRelative,
					NumericData = AdjustData(FirstDeviceResponse,
											 1), // position and msgId
					DeviceNumber = FirstDeviceNumber
				};

				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responsePacket1));
			}

			#endregion

			#region -- Data --

			private const byte AliasDeviceNumber = 99;
			private const byte FirstDeviceNumber = 17;
			private const byte SecondDeviceNumber = 42;
			private const int FirstDeviceResponse = 12345;
			private const int SecondDeviceResponse = 9999;
			private IZaberPort _port;
			private IEventRaiser _packetReceivedRaiser;

			#endregion
		}

		private class RequestCollectionThroughAliasThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Request()
			{
				// SETUP
				var mocks = new MockRepository();
				_port = CreatePort(mocks);
				var device1 = CreateDevice(FirstDeviceNumber);
				var device2 = CreateDevice(SecondDeviceNumber);
				var aliasDevice = CreateDeviceCollection(AliasDeviceNumber);

				// EXPECT
				_packetReceivedRaiser = CreateDataPacketReceivedRaiser(_port);
				_port.Send(AliasDeviceNumber,
						   Command.MoveRelative,
						   AdjustData(1000, 1),
						   new Measurement(1, UnitOfMeasure.Millimeter));

				mocks.ReplayAll();

				// EXEC
				aliasDevice.Port = _port;
				device1.Port = _port;
				device2.Port = _port;
				var conversation1 = new Conversation(device1);
				var conversation2 = new Conversation(device2);
				var conversations =
					new ConversationCollection(aliasDevice)
					{
						conversation1,
						conversation2
					};

				var responses =
					conversations.RequestCollection(Command.MoveRelative, 1000);
				AssertTick(2);

				// VERIFY
				mocks.VerifyAll();

				Assert.AreEqual(FirstDeviceResponse,
								responses[0].NumericData,
								"Response data should match first device's response");
				Assert.AreEqual(SecondDeviceResponse,
								responses[1].NumericData,
								"Response data should match second device's response");
				Assert.IsFalse(conversations.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				var responsePacket1 = new DataPacket
				{
					Command = Command.MoveRelative,
					NumericData = AdjustData(FirstDeviceResponse,
											 1), // position and msgId
					DeviceNumber = FirstDeviceNumber
				};

				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responsePacket1));

				WaitForTick(2);

				var responsePacket2 = new DataPacket
				{
					Command = Command.MoveRelative,
					NumericData = AdjustData(SecondDeviceResponse,
											 1), // position and msgId
					DeviceNumber = SecondDeviceNumber
				};

				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responsePacket2));
			}

			#endregion

			#region -- Data --

			private const byte AliasDeviceNumber = 99;
			private const byte FirstDeviceNumber = 17;
			private const byte SecondDeviceNumber = 42;
			private const int FirstDeviceResponse = 12345;
			private const int SecondDeviceResponse = 9999;
			private IZaberPort _port;
			private IEventRaiser _packetReceivedRaiser;

			#endregion
		}

		private class ErrorResponseThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[Initialize]
			public void Init()
			{
				// SETUP
				mocks = new MockRepository();
				_port = CreatePort(mocks);
				device1 = CreateDevice(FirstDeviceNumber);
				device2 = CreateDevice(SecondDeviceNumber);
				aliasDevice = CreateDeviceCollection(AliasDeviceNumber);

				_packetReceivedRaiser = CreateDataPacketReceivedRaiser(_port);
			}


			[TestThread]
			public void Request()
			{
				// EXPECT
				_port.Send(AliasDeviceNumber,
						   Command.MoveRelative,
						   AdjustData(1000, 1),
						   new Measurement(1, UnitOfMeasure.Millimeter));

				mocks.ReplayAll();

				// EXEC
				aliasDevice.Port = _port;
				device1.Port = _port;
				device2.Port = _port;
				var conversation1 = new Conversation(device1);
				var conversation2 = new Conversation(device2);
				var conversations =
					new ConversationCollection(aliasDevice)
					{
						conversation1,
						conversation2
					};

				string msg = null;
				IList<ConversationTopic> topics = null;
				try
				{
					conversations.RequestCollection(Command.MoveRelative, 1000);
				}
				catch (RequestCollectionException ex)
				{
					msg = ex.Message;
					topics = ex.Topics;
				}

				AssertTick(2);

				// VERIFY
				mocks.VerifyAll();

				Assert.AreEqual("Some requests failed: Busy error on device 17.",
								msg,
								"error message");
				Assert.AreEqual(FirstDeviceResponse,
								topics[0].Response.NumericData,
								"Response data should match first device's response");
				Assert.AreEqual(SecondDeviceResponse,
								topics[1].Response.NumericData,
								"Response data should match second device's response");
				Assert.IsFalse(conversations.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				var responsePacket1 = new DataPacket
				{
					Command = Command.Error,
					NumericData = AdjustData(FirstDeviceResponse,
											 1), // position and msgId
					DeviceNumber = FirstDeviceNumber
				};

				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responsePacket1));

				WaitForTick(2);

				var responsePacket2 = new DataPacket
				{
					Command = Command.MoveRelative,
					NumericData = AdjustData(SecondDeviceResponse,
											 1), // position and msgId
					DeviceNumber = SecondDeviceNumber
				};

				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responsePacket2));
			}

			#endregion

			#region -- Data --

			private const byte AliasDeviceNumber = 99;
			private const byte FirstDeviceNumber = 17;
			private const byte SecondDeviceNumber = 42;
			private const int FirstDeviceResponse = (int) ZaberError.Busy;
			private const int SecondDeviceResponse = 9999;
			private IZaberPort _port;
			private IEventRaiser _packetReceivedRaiser;
			private MockRepository mocks;
			private ZaberDevice device1;
			private ZaberDevice device2;
			private DeviceCollection aliasDevice;

			#endregion
		}

		private class RequestCollectionNoDataThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Request()
			{
				// SETUP
				var mocks = new MockRepository();
				_port = CreatePort(mocks);
				var device1 = CreateDevice(FirstDeviceNumber);
				var device2 = CreateDevice(SecondDeviceNumber);
				var aliasDevice = CreateDeviceCollection(AliasDeviceNumber);

				// EXPECT
				_packetReceivedRaiser = CreateDataPacketReceivedRaiser(_port);
				_port.Send(AliasDeviceNumber,
						   Command.Stop,
						   AdjustData(0, 1),
						   new Measurement(0, UnitOfMeasure.Data));

				mocks.ReplayAll();

				// EXEC
				aliasDevice.Port = _port;
				device1.Port = _port;
				device2.Port = _port;
				var conversation1 = new Conversation(device1);
				var conversation2 = new Conversation(device2);
				var conversations =
					new ConversationCollection(aliasDevice)
					{
						conversation1,
						conversation2
					};

				var responses =
					conversations.RequestCollection(Command.Stop);
				AssertTick(2);

				// VERIFY
				mocks.VerifyAll();

				Assert.AreEqual(FirstDeviceResponse,
								responses[0].NumericData,
								"Response data should match first device's response");
				Assert.AreEqual(SecondDeviceResponse,
								responses[1].NumericData,
								"Response data should match second device's response");
				Assert.IsFalse(conversations.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				var responsePacket1 = new DataPacket
				{
					Command = Command.Stop,
					NumericData = AdjustData(FirstDeviceResponse,
											 1), // position and msgId
					DeviceNumber = FirstDeviceNumber
				};

				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responsePacket1));

				WaitForTick(2);

				var responsePacket2 = new DataPacket
				{
					Command = Command.Stop,
					NumericData = AdjustData(SecondDeviceResponse,
											 1), // position and msgId
					DeviceNumber = SecondDeviceNumber
				};

				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responsePacket2));
			}

			#endregion

			#region -- Data --

			private const byte AliasDeviceNumber = 99;
			private const byte FirstDeviceNumber = 17;
			private const byte SecondDeviceNumber = 42;
			private const int FirstDeviceResponse = 12345;
			private const int SecondDeviceResponse = 9999;
			private IZaberPort _port;
			private IEventRaiser _packetReceivedRaiser;

			#endregion
		}

		private class RequestCollectionSeparateThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Request()
			{
				// SETUP
				var mocks = new MockRepository();
				_port = CreatePort(mocks);
				var device1 = CreateDevice(FirstDeviceNumber);
				var device2 = CreateDevice(SecondDeviceNumber);
				var aliasDevice = CreateDeviceCollection(AliasDeviceNumber);
				const int firstMovement = 1000;
				const int secondMovement = 2000;

				// EXPECT
				_packetReceivedRaiser = CreateDataPacketReceivedRaiser(_port);
				_port.Send(FirstDeviceNumber,
						   Command.MoveRelative,
						   AdjustData(firstMovement, 1),
						   new Measurement(firstMovement / 1000, UnitOfMeasure.Millimeter));
				_port.Send(SecondDeviceNumber,
						   Command.MoveRelative,
						   AdjustData(secondMovement, 1),
						   new Measurement(secondMovement / 1000, UnitOfMeasure.Millimeter));

				mocks.ReplayAll();

				// EXEC
				aliasDevice.Port = _port;
				device1.Port = _port;
				device2.Port = _port;
				var conversation1 = new Conversation(device1);
				var conversation2 = new Conversation(device2);
				var conversations =
					new ConversationCollection(aliasDevice)
					{
						conversation1,
						conversation2
					};

				var responses =
					conversations.RequestCollection(Command.MoveRelative, firstMovement, secondMovement);
				AssertTick(2);

				// VERIFY
				mocks.VerifyAll();

				Assert.AreEqual(FirstDeviceResponse,
								responses[0].NumericData,
								"Response data should match first device's response");
				Assert.AreEqual(SecondDeviceResponse,
								responses[1].NumericData,
								"Response data should match second device's response");
				Assert.IsFalse(conversations.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				var responsePacket1 = new DataPacket
				{
					Command = Command.MoveRelative,
					NumericData = AdjustData(FirstDeviceResponse,
											 1), // position and msgId
					DeviceNumber = FirstDeviceNumber
				};

				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responsePacket1));

				WaitForTick(2);

				var responsePacket2 = new DataPacket
				{
					Command = Command.MoveRelative,
					NumericData = AdjustData(SecondDeviceResponse,
											 1), // position and msgId
					DeviceNumber = SecondDeviceNumber
				};

				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responsePacket2));
			}

			#endregion

			#region -- Data --

			private const byte AliasDeviceNumber = 99;
			private const byte FirstDeviceNumber = 17;
			private const byte SecondDeviceNumber = 42;
			private const int FirstDeviceResponse = 12345;
			private const int SecondDeviceResponse = 9999;
			private IZaberPort _port;
			private IEventRaiser _packetReceivedRaiser;

			#endregion
		}

		private class RetryPortErrorThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Request()
			{
				// SETUP
				var mocks = new MockRepository();
				_port = CreatePort(mocks);
				var device1 = CreateDevice(FirstDeviceNumber);
				var device2 = CreateDevice(SecondDeviceNumber);
				var aliasDevice = CreateDeviceCollection(AliasDeviceNumber);
				var firstDeviceRetryPacket = new DataPacket();
				var secondDeviceRetryPacket = new DataPacket();

				// EXPECT
				_packetReceivedRaiser = CreateDataPacketReceivedRaiser(_port);
				_errorReceivedRaiser = CreateErrorReceivedRaiser(_port);
				_port.Send(AliasDeviceNumber,
						   Command.MoveAbsolute,
						   AdjustData(1000, 1),
						   new Measurement(1, UnitOfMeasure.Millimeter));
				Expect.Call(_port.SendDelayed(FirstDeviceNumber,
											  Command.MoveAbsolute,
											  AdjustData(1000, 1)))
				   .Return(firstDeviceRetryPacket);
				Expect.Call(_port.SendDelayed(SecondDeviceNumber,
											  Command.MoveAbsolute,
											  AdjustData(1000, 1)))
				   .Return(secondDeviceRetryPacket);
				Expect.Call(_port.CancelDelayedPacket(firstDeviceRetryPacket)).Return(false);
				Expect.Call(_port.CancelDelayedPacket(secondDeviceRetryPacket)).Return(false);

				mocks.ReplayAll();

				// EXEC
				aliasDevice.Port = _port;
				device1.Port = _port;
				device2.Port = _port;
				var conversation1 = new Conversation(device1);
				var conversation2 = new Conversation(device2);
				var conversations =
					new ConversationCollection(aliasDevice)
					{
						conversation1,
						conversation2
					};
				conversations.RetryCount = 3;

				var response =
					conversations.Request(Command.MoveAbsolute, 1000);
				AssertTick(3);

				// VERIFY
				mocks.VerifyAll();

				Assert.AreEqual(FirstDeviceResponse,
								response.NumericData,
								"Response data should match first device's response");
				Assert.IsFalse(conversations.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				_errorReceivedRaiser.Raise(_port,
										   new ZaberPortErrorReceivedEventArgs(ZaberPortError.Frame));

				WaitForTick(2);

				var responsePacket1 = new DataPacket
				{
					Command = Command.MoveRelative,
					NumericData = AdjustData(FirstDeviceResponse,
											 1), // position and msgId
					DeviceNumber = FirstDeviceNumber
				};

				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responsePacket1));

				WaitForTick(3);

				var responsePacket2 = new DataPacket
				{
					Command = Command.MoveRelative,
					NumericData = AdjustData(SecondDeviceResponse,
											 1), // position and msgId
					DeviceNumber = SecondDeviceNumber
				};

				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responsePacket2));
			}

			#endregion

			#region -- Data --

			private const byte AliasDeviceNumber = 99;
			private const byte FirstDeviceNumber = 17;
			private const byte SecondDeviceNumber = 42;
			private const int FirstDeviceResponse = 12345;
			private const int SecondDeviceResponse = 9999;
			private IZaberPort _port;
			private IEventRaiser _packetReceivedRaiser;
			private IEventRaiser _errorReceivedRaiser;

			#endregion
		}

		private class RetryWithIndividualDataValuesThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Request()
			{
				// SETUP
				var mocks = new MockRepository();
				_port = CreatePort(mocks);
				var device1 = CreateDevice(FirstDeviceNumber);
				var device2 = CreateDevice(SecondDeviceNumber);
				var aliasDevice = CreateDeviceCollection(AliasDeviceNumber);
				var firstDeviceRetryPacket = new DataPacket();
				var secondDeviceRetryPacket = new DataPacket();

				// EXPECT
				_packetReceivedRaiser = CreateDataPacketReceivedRaiser(_port);
				_errorReceivedRaiser = CreateErrorReceivedRaiser(_port);
				_port.Send(FirstDeviceNumber,
						   Command.MoveAbsolute,
						   AdjustData(1000, 1),
						   new Measurement(1, UnitOfMeasure.Millimeter));
				_port.Send(SecondDeviceNumber,
						   Command.MoveAbsolute,
						   AdjustData(2000, 1),
						   new Measurement(2, UnitOfMeasure.Millimeter));
				Expect.Call(_port.SendDelayed(FirstDeviceNumber,
											  Command.MoveAbsolute,
											  AdjustData(1000, 1)))
				   .Return(firstDeviceRetryPacket);
				Expect.Call(_port.SendDelayed(SecondDeviceNumber,
											  Command.MoveAbsolute,
											  AdjustData(2000, 1)))
				   .Return(secondDeviceRetryPacket);
				Expect.Call(_port.CancelDelayedPacket(firstDeviceRetryPacket)).Return(false);
				Expect.Call(_port.CancelDelayedPacket(secondDeviceRetryPacket)).Return(false);

				mocks.ReplayAll();

				// EXEC
				aliasDevice.Port = _port;
				device1.Port = _port;
				device2.Port = _port;
				var conversation1 = new Conversation(device1);
				var conversation2 = new Conversation(device2);
				var conversations =
					new ConversationCollection(aliasDevice)
					{
						conversation1,
						conversation2
					};
				conversations.RetryCount = 3;

				conversations.RequestCollection(Command.MoveAbsolute, 1000, 2000);
				AssertTick(3);

				// VERIFY
				mocks.VerifyAll();

				Assert.IsFalse(conversations.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				_errorReceivedRaiser.Raise(_port,
										   new ZaberPortErrorReceivedEventArgs(ZaberPortError.Frame));

				WaitForTick(2);

				var responsePacket1 = new DataPacket
				{
					Command = Command.MoveRelative,
					NumericData = AdjustData(FirstDeviceResponse,
											 1), // position and msgId
					DeviceNumber = FirstDeviceNumber
				};

				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responsePacket1));

				WaitForTick(3);

				var responsePacket2 = new DataPacket
				{
					Command = Command.MoveRelative,
					NumericData = AdjustData(SecondDeviceResponse,
											 1), // position and msgId
					DeviceNumber = SecondDeviceNumber
				};

				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responsePacket2));
			}

			#endregion

			#region -- Data --

			private const byte AliasDeviceNumber = 99;
			private const byte FirstDeviceNumber = 17;
			private const byte SecondDeviceNumber = 42;
			private const int FirstDeviceResponse = 12345;
			private const int SecondDeviceResponse = 9999;
			private IZaberPort _port;
			private IEventRaiser _packetReceivedRaiser;
			private IEventRaiser _errorReceivedRaiser;

			#endregion
		}

		private class PollUntilIdleThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Request()
			{
				// SETUP
				_mockTimer = new MockTimeoutTimer();
				var mocks = new MockRepository();
				_port = CreatePort(mocks);
				var device1 = CreateDevice(FirstDeviceNumber);
				var device2 = CreateDevice(SecondDeviceNumber);
				var aliasDevice = CreateDeviceCollection(AliasDeviceNumber);

				// EXPECT
				_packetReceivedRaiser = CreateDataPacketReceivedRaiser(_port);
				const string expectedCommand1 = "17 0 ";
				const string expectedCommand2 = "42 0 ";
				_port.Send(expectedCommand1, null);
				_port.Send(expectedCommand1, null);
				_port.Send(expectedCommand1, null);
				_port.Send(expectedCommand2, null);

				mocks.ReplayAll();

				// EXEC
				aliasDevice.Port = _port;
				device1.Port = _port;
				device2.Port = _port;
				var conversation1 = new Conversation(device1);
				var conversation2 = new Conversation(device2);
				var conversations =
					new ConversationCollection(aliasDevice)
					{
						conversation1,
						conversation2
					};
				conversations.PollTimeoutTimer = _mockTimer;

				conversations.PollUntilIdle();
				AssertTick(6);

				// VERIFY
				mocks.VerifyAll();
			}


			[TestThread]
			public void Response()
			{
				var responseBusy17 = new DataPacket("@17 0 OK BUSY -- 0");
				var responseIdle17 = new DataPacket("@17 0 OK IDLE -- 0");
				var responseIdle42 = new DataPacket("@42 0 OK IDLE -- 0");

				WaitForTick(1);
				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responseBusy17));

				WaitForTick(2);
				_mockTimer.TimeoutNow();

				WaitForTick(3);
				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responseBusy17));

				WaitForTick(4);
				_mockTimer.TimeoutNow();

				WaitForTick(5);
				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responseIdle17));

				WaitForTick(6);
				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responseIdle42));
			}

			#endregion

			#region -- Data --

			private const byte AliasDeviceNumber = 99;
			private const byte FirstDeviceNumber = 17;
			private const byte SecondDeviceNumber = 42;
			private MockTimeoutTimer _mockTimer;
			private IZaberPort _port;
			private IEventRaiser _packetReceivedRaiser;

			#endregion
		}

		private class UnexpectedResponseIsInvalidThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Request()
			{
				// SETUP
				var mocks = new MockRepository();
				_port = CreatePort(mocks);
				var device1 = CreateDevice(FirstDeviceNumber);
				var device2 = CreateDevice(SecondDeviceNumber);
				var aliasDevice = CreateDeviceCollection(AliasDeviceNumber);
				aliasDevice.Add(device1);
				aliasDevice.Add(device2);

				// EXPECT
				_packetReceivedRaiser = CreateDataPacketReceivedRaiser(_port);
				_port.Send(AliasDeviceNumber,
						   Command.MoveRelative,
						   AdjustData(1000, 1),
						   new Measurement(1, UnitOfMeasure.Millimeter));

				mocks.ReplayAll();

				// EXEC
				aliasDevice.Port = _port;
				device1.Port = _port;
				device2.Port = _port;
				var conversation1 = new Conversation(device1);
				var conversation2 = new Conversation(device2);
				var conversations =
					new ConversationCollection(aliasDevice)
					{
						conversation1,
						conversation2
					};

				var response =
					conversations.Request(Command.MoveRelative, 1000);
				AssertTick(2);

				// VERIFY
				mocks.VerifyAll();

				Assert.AreEqual(FirstDeviceResponse,
								response.NumericData,
								"Response data should match first device's response");
				Assert.IsFalse(conversations.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				var responsePacket1 = new DataPacket
				{
					Command = Command.MoveRelative,
					NumericData = AdjustData(FirstDeviceResponse,
											 1), // position and msgId
					DeviceNumber = FirstDeviceNumber
				};

				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responsePacket1));

				WaitForTick(2);

				var responsePacket2 = new DataPacket
				{
					Command = Command.MoveRelative,
					NumericData = AdjustData(SecondDeviceResponse,
											 1), // position and msgId
					DeviceNumber = SecondDeviceNumber
				};

				_packetReceivedRaiser.Raise(_port,
											new DataPacketEventArgs(responsePacket2));
			}

			#endregion

			#region -- Data --

			private const byte AliasDeviceNumber = 99;
			private const byte FirstDeviceNumber = 17;
			private const byte SecondDeviceNumber = 42;
			private const int FirstDeviceResponse = 12345;
			private const int SecondDeviceResponse = 9999;
			private IZaberPort _port;
			private IEventRaiser _packetReceivedRaiser;

			#endregion
		}


		private static int AdjustData(int data, byte messageId) => (data & 0xFFFFFF) | (messageId << 24);


		private static ZaberDevice CreateDevice(byte deviceNumber)
		{
			var device = new ZaberDevice
			{
				DeviceNumber = deviceNumber,
				DeviceType = DeviceTypeMother.CreateDeviceTypeWithCommands(),
				AreMessageIdsEnabled = true
			};

			return device;
		}


		private static DeviceCollection CreateDeviceCollection(byte deviceNumber)
		{
			var device = new DeviceCollection
			{
				DeviceNumber = deviceNumber,
				DeviceType = DeviceTypeMother.CreateDeviceTypeWithCommands(),
				AreMessageIdsEnabled = true
			};

			return device;
		}


		private static IZaberPort CreatePort(MockRepository mocks)
		{
			var port = mocks.StrictMock<IZaberPort>();

			port.IsAsciiMode = false;
			LastCall.PropertyBehavior();
			port.IsAsciiMode = false;

			port.DataPacketReceived += null;
			LastCall.IgnoreArguments().Repeat.Any();
			port.DataPacketSent += null;
			LastCall.IgnoreArguments().Repeat.Any();
			port.ErrorReceived += null;
			LastCall.IgnoreArguments().Repeat.Any();
			return port;
		}


		private static IEventRaiser CreateDataPacketReceivedRaiser(IZaberPort port)
		{
			port.DataPacketReceived += null;
			return LastCall.Repeat.Any().GetEventRaiser();
		}


		private static IEventRaiser CreateDataPacketSentRaiser(IZaberPort port)
		{
			port.DataPacketSent += null;
			return LastCall.Repeat.Any().GetEventRaiser();
		}


		private static IEventRaiser CreateErrorReceivedRaiser(IZaberPort port)
		{
			port.ErrorReceived += null;
			return LastCall.Repeat.Any().GetEventRaiser();
		}


		protected override ConversationCollection CreateList() => CreateList(1);


		private static ConversationCollection CreateList(byte count)
		{
			var aliasDevice = CreateDeviceCollection(0);
			var conversations = new ConversationCollection(aliasDevice);
			for (byte i = 0; i < count; i++)
			{
				var device = CreateDevice((byte) (i + 1));
				var conversation = new Conversation(device);
				conversations.Add(conversation);
			}

			return conversations;
		}
	}
}

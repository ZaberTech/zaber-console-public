﻿using System.IO;
using System.Reflection;

namespace ZaberTest
{
	/// <summary>
	///     Helper for locating and managing data files used by tests.
	/// </summary>
	public static class TestDataFileHelper
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Resolves a file path relative to the assembly requesting it.
		/// </summary>
		/// <param name="aRelativePath">
		///     Path to the data file and its filename, relative to where the caller's assembly is expected
		///     to be deployed.
		/// </param>
		/// <returns>An absolute path to the data file.</returns>
		public static string GetDeployedPath(string aRelativePath)
		{
			var caller = Assembly.GetCallingAssembly();
			var baseDir = Path.GetDirectoryName(Path.GetFullPath(caller.Location));
			return Path.Combine(baseDir, aRelativePath);
		}

		#endregion
	}
}

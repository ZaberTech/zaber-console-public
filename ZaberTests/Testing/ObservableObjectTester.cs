﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using NUnit.Framework;

namespace Zaber.Testing
{
	/// <summary>
	///     Base class for ViewModel tests - provides help with testing property changed notifications.
	/// </summary>
	public class ObservableObjectTester
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Attach listener to an object's PropertyChanged callback.
		/// </summary>
		/// <param name="aObject">The object instance to listen to.</param>
		public void Listen(INotifyPropertyChanged aObject) => aObject.PropertyChanged += OnPropertyChanged;


		/// <summary>
		///     Remove listener from an object.
		/// </summary>
		/// <param name="aObject">The object instance to listen to.</param>
		public void Unlisten(INotifyPropertyChanged aObject) => aObject.PropertyChanged -= OnPropertyChanged;


		/// <summary>
		///     Get the number of notifications received since the last clear.
		/// </summary>
		/// <returns>Number of notifications received from all objects since the last clear.</returns>
		public int GetNumNotifications() => _notifications.Count;


		/// <summary>
		///     Clear the list of notifications received.
		/// </summary>
		public void ClearNotifications() => _notifications = new List<Tuple<object, string>>();


		/// <summary>
		///     Clear all notifications received from a particular VM.
		/// </summary>
		/// <param name="aSender">The object for which to remove all notifications from the list.</param>
		public void ClearNotificationsFrom(object aSender) => _notifications =
			new List<Tuple<object, string>>(_notifications.Where(t => t.Item1 != aSender));


		/// <summary>
		///     Clear all notifications received for a particular property name.
		/// </summary>
		/// <param name="aPropertyname">The property name to filter from the notification list.</param>
		public void ClearNotifications(string aPropertyname) => _notifications =
			new List<Tuple<object, string>>(_notifications.Where(t => t.Item2 != aPropertyname));


		/// <summary>
		///     Get notifications received as an ordered sequence of <sender, propertyname> pairs.
		/// </summary>
		/// <returns>All notifications since the last clear, as pairs of sender objects and property names.</returns>
		public IEnumerable<Tuple<object, string>> GetNotifications() => _notifications;


		/// <summary>
		///     Get an ordered sequence of objects that have sent propertychanged notifications.
		/// </summary>
		/// <returns>
		///     All objects that have send property changed notifications since the last clear.
		///     The list is not filtered for redundancy so the same object may appear multiple times in a row.
		/// </returns>
		public IEnumerable<object> GetNotifiers() => _notifications.Select(t => t.Item1);


		/// <summary>
		///     Get an ordered sequence of changed property names.
		/// </summary>
		/// <returns>The names of properties that have changed since the last clear.</returns>
		public IEnumerable<string> GetPropertiesChanged() => _notifications.Select(t => t.Item2);


		/// <summary>
		///     Convenience function to check that a given property has changed somewhere.
		/// </summary>
		/// <param name="aPropertyName">Name of the property to check for.</param>
		/// <returns>True if the named property has changed on any registered object since the last clear.</returns>
		public bool PropertyWasChanged(string aPropertyName) => GetPropertiesChanged().Contains(aPropertyName);


		/// <summary>
		///     Convenience function to check that a given property has changed on a specific object.
		/// </summary>
		/// <param name="aVM">The object instance to check.</param>
		/// <param name="aPropertyName">The name of the property to check for.</param>
		/// <returns>True if the named property has changed on the specified object since the last clear.</returns>
		public bool PropertyWasChanged(object aVM, string aPropertyName)
			=> _notifications.Contains(new Tuple<object, string>(aVM, aPropertyName));


		/// <summary>
		///     Convenience function to check that a given property has changed somewhere and it was the only property changed.
		/// </summary>
		/// <param name="aPropertyName">Name of the property to check for.</param>
		/// <returns>
		///     True if the named property has changed on any registered object since the last clear,
		///     and it is the only property that has changed on any object.
		/// </returns>
		public bool OnePropertyWasChanged(string aPropertyName)
			=> (1 == GetNumNotifications()) && GetPropertiesChanged().Contains(aPropertyName);


		/// <summary>
		///     Convenience function to check that a given property has changed on a specific object and it was the
		///     only property changed on any object.
		/// </summary>
		/// <param name="aVM">The object instance to check.</param>
		/// <param name="aPropertyName">The name of the property to check for.</param>
		/// <returns>
		///     True if the named property has changed on the specified object since the last clear, and
		///     there have been no other property changes on any object.
		/// </returns>
		public bool OnePropertyWasChanged(object aVM, string aPropertyName)
			=> (1 == GetNumNotifications()) && _notifications.Contains(new Tuple<object, string>(aVM, aPropertyName));


		/// <summary>
		///     Perform basic tests on an observable string property.
		///     Side effect: If the property setter has the side effect of changing other properties, it is up
		///     to the caller to verify those are present and in the right order. Otherwise the caller should
		///     check that no other properties were changed. This method will filter out notifications for the
		///     property it is testing.
		/// </summary>
		/// <param name="aTestObject">The VM instance on which to test property changes.</param>
		/// <param name="aProperty">A lambda referring to the property.</param>
		public void TestObservableProperty(object aTestObject, Expression<Func<string>> aProperty)
			=> TestObservableProperty(aTestObject, aProperty, str => null == str ? "Foo" : str + " foo");


		/// <summary>
		///     Perform basic tests on an observable boolean property.
		///     Side effect: If the property setter has the side effect of changing other properties, it is up
		///     to the caller to verify those are present and in the right order. Otherwise the caller should
		///     check that no other properties were changed. This method will filter out notifications for the
		///     property it is testing.
		/// </summary>
		/// <param name="aTestObject">The VM instance on which to test property changes.</param>
		/// <param name="aProperty">A lambda referring to the property.</param>
		public void TestObservableProperty(object aTestObject, Expression<Func<bool>> aProperty)
			=> TestObservableProperty(aTestObject, aProperty, b => !b);


		/// <summary>
		///     Perform basic tests on an observable integer property.
		///     Side effect: If the property setter has the side effect of changing other properties, it is up
		///     to the caller to verify those are present and in the right order. Otherwise the caller should
		///     check that no other properties were changed. This method will filter out notifications for the
		///     property it is testing.
		/// </summary>
		/// <param name="aTestObject">The VM instance on which to test property changes.</param>
		/// <param name="aProperty">A lambda referring to the property.</param>
		public void TestObservableProperty(object aTestObject, Expression<Func<int>> aProperty)
			=> TestObservableProperty(aTestObject, aProperty, i => i + 1);


		/// <summary>
		///     Perform basic tests on an observable unsigned integer property.
		///     Side effect: If the property setter has the side effect of changing other properties, it is up
		///     to the caller to verify those are present and in the right order. Otherwise the caller should
		///     check that no other properties were changed. This method will filter out notifications for the
		///     property it is testing.
		/// </summary>
		/// <param name="aTestObject">The VM instance on which to test property changes.</param>
		/// <param name="aProperty">A lambda referring to the property.</param>
		public void TestObservableProperty(object aTestObject, Expression<Func<uint>> aProperty)
			=> TestObservableProperty(aTestObject, aProperty, i => i + 1);


		/// <summary>
		///     Perform basic tests on an observable double property.
		///     Side effect: If the property setter has the side effect of changing other properties, it is up
		///     to the caller to verify those are present and in the right order. Otherwise the caller should
		///     check that no other properties were changed. This method will filter out notifications for the
		///     property it is testing.
		/// </summary>
		/// <param name="aTestObject">The VM instance on which to test property changes.</param>
		/// <param name="aProperty">A lambda referring to the property.</param>
		public void TestObservableProperty(object aTestObject, Expression<Func<double>> aProperty)
			=> TestObservableProperty(aTestObject, aProperty, d => d + 1.0);


		/// <summary>
		///     Perform basic tests on an observable decimal property.
		///     Side effect: If the property setter has the side effect of changing other properties, it is up
		///     to the caller to verify those are present and in the right order. Otherwise the caller should
		///     check that no other properties were changed. This method will filter out notifications for the
		///     property it is testing.
		/// </summary>
		/// <param name="aTestObject">The VM instance on which to test property changes.</param>
		/// <param name="aProperty">A lambda referring to the property.</param>
		public void TestObservableProperty(object aTestObject, Expression<Func<decimal>> aProperty)
			=> TestObservableProperty(aTestObject, aProperty, d => d + 1.0m);


		/// <summary>
		///     Perform basic tests on an observable double property.
		///     Side effect: If the property setter has the side effect of changing other properties, it is up
		///     to the caller to verify those are present and in the right order. Otherwise the caller should
		///     check that no other properties were changed. This method will filter out notifications for the
		///     property it is testing.
		/// </summary>
		/// <param name="aTestObject">The VM instance on which to test property changes.</param>
		/// <param name="aProperty">A lambda referring to the property.</param>
		public void TestObservableProperty(object aTestObject, Expression<Func<float>> aProperty)
			=> TestObservableProperty(aTestObject, aProperty, f => f + 1.0f);


		/// <summary>
		///     Perform basic tests on a property without knowing its type.
		///     Side effect: If the property setter has the side effect of changing other properties, it is up
		///     to the caller to verify those are present and in the right order. Otherwise the caller should
		///     check that no other properties were changed. This method will filter out notifications for the
		///     property it is testing.
		/// </summary>
		/// <param name="aTestObject">The VM instance on which to test property changes.</param>
		/// <param name="aProperty">A lambda referring to the property.</param>
		/// <param name="aMakeDifferent">
		///     A function that creates a value differing from its argument but of the same type, and
		///     never returns null.
		/// </param>
		public void TestObservableProperty<T>(object aTestObject, Expression<Func<T>> aProperty,
											  Func<T, T> aMakeDifferent)
		{
			ClearNotifications();

			var propertyName = ((MemberExpression) aProperty.Body).Member.Name;
			var pi = aTestObject.GetType()
							 .GetProperty(propertyName,
										  BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
			Assert.IsNotNull(pi, $"Could not find property {propertyName}.");

			// Save starting value.
			var initialValue = (T) pi.GetValue(aTestObject, null);

			// Assign starting value back to property and verify no notifications were received.
			pi.SetValue(aTestObject, initialValue, null);
			Assert.IsFalse(PropertyWasChanged(propertyName),
						   $"Assigning same value back to property {propertyName} should not have fired OnPropertyChanged event.");

			// Construct a different value and assign it.
			var newValue = aMakeDifferent(initialValue);
			Assert.AreNotEqual(initialValue, newValue, nameof(aMakeDifferent) + " did not produce a different value.");
			pi.SetValue(aTestObject, newValue, null);
			Assert.IsTrue(PropertyWasChanged(propertyName),
						  $"Assigning a new value to property {propertyName} should have fired OnPropertyChanged event.");
			ClearNotifications(propertyName);

			// Assign null.
			if (!typeof(T).IsValueType)
			{
				pi.SetValue(aTestObject, null, null);
				Assert.IsTrue(PropertyWasChanged(propertyName),
							  $"Assigning null to property {propertyName} should have fired OnPropertyChanged event.");
				ClearNotifications(propertyName);
			}

			// Assign the initial value again and expect another event.
			if (null != initialValue)
			{
				pi.SetValue(aTestObject, initialValue, null);
				Assert.IsTrue(PropertyWasChanged(propertyName),
							  $"Assigning the original value to changed property {propertyName} should have fired OnPropertyChanged event.");
				ClearNotifications(propertyName);
			}
		}


		/// <summary>
		///     Test that assignments to a non-observable property with public getter and setter work.
		///     Original value is restored on success.
		/// </summary>
		/// <typeparam name="T">Inferred type of the property.</typeparam>
		/// <param name="aTestObject">Object instance on which to change the property.</param>
		/// <param name="aProperty">Reference to the property.</param>
		/// <param name="aTestValue1">A test value to assign to the property.</param>
		/// <param name="aTestValue2">A different test value to assign to the property.</param>
		public void TestNonObservableProperty<T>(object aTestObject, Expression<Func<T>> aProperty, T aTestValue1,
												 T aTestValue2)
		{
			var propertyName = ((MemberExpression) aProperty.Body).Member.Name;

			var objType = aTestObject.GetType();
			var pi = objType.GetProperty(propertyName);
			var originalValue = (T) pi.GetValue(aTestObject, null);
			pi.SetValue(aTestObject, aTestValue1, null);
			Assert.AreEqual(aTestValue1, (T) pi.GetValue(aTestObject, null));
			pi.SetValue(aTestObject, aTestValue2, null);
			Assert.AreEqual(aTestValue2, (T) pi.GetValue(aTestObject, null));
			pi.SetValue(aTestObject, originalValue, null);
			Assert.AreEqual(originalValue, (T) pi.GetValue(aTestObject, null));
		}

		#endregion

		#region -- Non-Public Methods --

		// Property changed handler.
		private void OnPropertyChanged(object aSender, PropertyChangedEventArgs aArgs)
			=> _notifications.Add(new Tuple<object, string>(aSender, aArgs.PropertyName));

		#endregion

		#region -- Data --

		private List<Tuple<object, string>> _notifications = new List<Tuple<object, string>>();

		#endregion
	}
}

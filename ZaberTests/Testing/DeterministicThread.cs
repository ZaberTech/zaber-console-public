﻿using System.Threading;
using Zaber.Threading;

namespace ZaberTest.Testing
{
	/// <summary>
	///     Thread implementation for testing purposes - this does not execute
	///     the thread code when the code unter test calls start, but instead
	///     waits until the test code kicks it off.
	/// </summary>
	public class DeterministicThread : IThread
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Initialize the thread with a non-parameterized entry point.
		/// </summary>
		/// <param name="aEntryPoint">Function to be run by the thread.</param>
		public DeterministicThread(ThreadStart aEntryPoint)
		{
			_entryPoint = aEntryPoint;
		}


		/// <summary>
		///     Executes the thread entry point code immediately in the current thread.
		/// </summary>
		public void Start()
		{
			IsAlive = true;
			if (OkToStart)
			{
				_entryPoint?.Invoke();
				IsAlive = false;
			}
		}


		/// <summary>
		///     Has no effect in this implementation.
		/// </summary>
		public void Abort()
		{
		}


		/// <summary>
		///     Does nothing in this implementation.
		/// </summary>
		public void Join()
		{
		}


		/// <summary>
		///     Control when the "background" code actually executes. Calling
		///     <see cref="Start" /> will not start the job unless this is true.
		///     If <see cref="Start" /> has already been called when you set this
		///     to true, the work will be done at that time.
		/// </summary>
		public bool OkToStart
		{
			get => _okToStart;
			set
			{
				_okToStart = value;
				if (value && IsAlive)
				{
					_entryPoint?.Invoke();
					IsAlive = false;
				}
			}
		}

		/// <summary>
		///     Check whether the thread is running. For this implementation,
		///     this returns true while the thread is executing, meaning a true
		///     value can be seen only if queried by another thread.
		/// </summary>
		public bool IsAlive { get; private set; }

		/// <inheritdoc cref="Thread.Name" />
		public string Name { get; set; }

		#endregion

		#region -- Data --

		private readonly ThreadStart _entryPoint;
		private bool _okToStart;

		#endregion
	}
}

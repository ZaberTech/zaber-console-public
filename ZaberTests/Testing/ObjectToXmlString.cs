﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace ZaberTest.Testing
{
	/// <summary>
	///     XML serialization helper for unit tests.
	/// </summary>
	public static class ObjectToXmlString
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Serialize an object to an XML string.
		/// </summary>
		/// <param name="aObject">Object to serialize.</param>
		/// <returns>XML representation of the object in a string.</returns>
		public static string Serialize(object aObject)
		{
			var ser = new XmlSerializer(aObject.GetType());
			using (var writer = new StringWriter())
			{
				ser.Serialize(writer, aObject);
				return writer.ToString();
			}
		}


		/// <summary>
		///     Deserialize an object from an XML string.
		/// </summary>
		/// <typeparam name="T">Type of object to deserialize.</typeparam>
		/// <param name="aXml">XML representation of the object.</param>
		/// <returns>Deserialized instance of the object.</returns>
		public static T Deserialize<T>(string aXml) => (T) Deserialize(aXml, typeof(T));


		/// <summary>
		///     Deserialize an object from an XML string.
		/// </summary>
		/// <param name="aXml">XML representation of the object.</param>
		/// <param name="aType">Expected type of the object.</param>
		/// <returns>Deserialized instance of the object.</returns>
		public static object Deserialize(string aXml, Type aTargetType)
		{
			var ser = new XmlSerializer(aTargetType);
			using (var reader = new StringReader(aXml))
			{
				return ser.Deserialize(reader);
			}
		}

		#endregion
	}
}

﻿using System;
using System.Reflection;

namespace ZaberTest.Testing
{
	/// <summary>
	///     Helper for invoking non-public methods etc.
	/// </summary>
	public static class InvocationHelper
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Call a private static method of a class.
		/// </summary>
		/// <typeparam name="T">The class type to make the call on.</typeparam>
		/// <param name="aMethodName">Name of the method.</param>
		/// <param name="aArgs">Arguments to the method.</param>
		/// <returns>The return value of the method.</returns>
		public static object InvokePrivateStaticMethod<T>(string aMethodName, params object[] aArgs)
			=> InvokePrivateStaticMethod(typeof(T), aMethodName, aArgs);


		/// <summary>
		///     Call a private static method of a class.
		/// </summary>
		/// <param name="aType">The class type to make the call on.</param>
		/// <param name="aMethodName">Name of the method.</param>
		/// <param name="aArgs">Arguments to the method.</param>
		/// <returns>The return value of the method.</returns>
		public static object InvokePrivateStaticMethod(Type aType, string aMethodName, params object[] aArgs)
		{
			var method = aType.GetMethod(aMethodName, BindingFlags.NonPublic | BindingFlags.Static);
			if (null == method)
			{
				throw new ArgumentException($"Couldn't find private static method {aMethodName} on type {aType.Name}.");
			}

			return method.Invoke(null, aArgs);
		}


		/// <summary>
		///     Call a private instance method of a class.
		/// </summary>
		/// <param name="aInstance">Object instance to invoke the method on.</param>
		/// <param name="aMethodName">Name of the method.</param>
		/// <param name="aArgs">Arguments to the method.</param>
		/// <returns>The return value of the method.</returns>
		public static object InvokePrivateMethod(object aInstance, string aMethodName, params object[] aArgs)
		{
			var type = aInstance.GetType();
			var method = type.GetMethod(aMethodName, BindingFlags.NonPublic | BindingFlags.Instance);
			if (null == method)
			{
				throw new ArgumentException(
					$"Couldn't find private method {aMethodName} on instance of type {type.Name}.");
			}

			return method.Invoke(aInstance, aArgs);
		}


		/// <summary>
		///     Call a private instance property setter.
		/// </summary>
		/// <param name="aInstance">Object instance to set the value on.</param>
		/// <param name="aPropertyName">Name of the property to set.</param>
		/// <param name="aNewValue">New value for the property.</param>
		public static void SetPrivateProperty(object aInstance, string aPropertyName, object aNewValue)
		{
			var type = aInstance.GetType();
			var prop = type.GetProperty(aPropertyName,
										BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
			if (null == prop)
			{
				throw new ArgumentException(
					$"Couldn't find private settable property {aPropertyName} on instance of type {type.Name}.");
			}

			prop.SetValue(aInstance, aNewValue, null);
		}


		/// <summary>
		///     Change the value of a private instance field.
		/// </summary>
		/// <param name="aInstance">Object instance to set the value on.</param>
		/// <param name="aFieldName">Name of the field to set.</param>
		/// <param name="aNewValue">New value for the property.</param>
		public static void SetPrivateField(object aInstance, string aFieldName, object aNewValue)
		{
			var type = aInstance.GetType();
			var field = type.GetField(aFieldName,
									 BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
			if (null == field)
			{
				throw new ArgumentException(
					$"Couldn't find private field {aFieldName} on instance of type {type.Name}.");
			}

			field.SetValue(aInstance, aNewValue);
		}


		/// <summary>
		///     Read the value of a private static class property.
		/// </summary>
		/// <param name="aClassType">The type to read the property from.</param>
		/// <param name="aPropertyName">The name of the property to read.</param>
		/// <returns>The value of the property.</returns>
		public static object GetPrivateStaticProperty(Type aClassType, string aPropertyName)
		{
			var prop = aClassType.GetProperty(aPropertyName,
											  BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.GetProperty);
			if (null == prop)
			{
				throw new ArgumentException(
					$"Couldn't find private static gettable property {aPropertyName} on type {aClassType.Name}.");
			}

			return prop.GetValue(null, null);
		}

		#endregion
	}
}

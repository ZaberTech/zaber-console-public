﻿using System;
using System.IO;

namespace ZaberTest.Testing
{
	/// <summary>
	///     Utility functions to help unit tests with file system operations.
	/// </summary>
	public class FileSystemHelper
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Directory.Delete doesn't work if there are files in subdirectories. This function
		///     recursively deletes everything in a directory.
		/// </summary>
		/// <param name="aPath"></param>
		public static void DeleteDirectoryRecursive(string aPath)
		{
			if (!Directory.Exists(aPath))
			{
				return;
			}

			foreach (var file in Directory.GetFiles(aPath))
			{
				File.Delete(file);
			}

			foreach (var dir in Directory.GetDirectories(aPath))
			{
				DeleteDirectoryRecursive(dir);
			}

			Directory.Delete(aPath);
		}


		/// <summary>
		///     Copy one file to a new path and set the last modified time of the copy. Destination will
		///     be overwritten.
		/// </summary>
		/// <param name="aSourcePath">Path to the source file.</param>
		/// <param name="aDestPath">Path to the destination - must include the file name.</param>
		/// <param name="aTimestamp">Timestamp to set on the copy.</param>
		public static void CopyFileAndSetWriteTime(string aSourcePath, string aDestPath, DateTime aTimestamp)
		{
			File.Copy(aSourcePath, aDestPath, true);
			File.SetLastWriteTime(aDestPath, aTimestamp);
		}

		#endregion
	}
}

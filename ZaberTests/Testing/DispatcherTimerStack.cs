﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using ZaberWpfToolbox;

namespace ZaberTest.Testing
{
	/// <summary>
	///     Helper class for tests to manage customer displatcher timers.
	///     Note the behavior is different from <see cref="DispatcherStack" />; pushed
	///     timer instances are expected to be consumed during tests, leaving the
	///     stack of pushed instances empty. Tests that use this class should call
	///     <see cref="Reset(bool)" /> in their teardown methods to verify that all instances
	///     were consumed and leave the stack clear for the next test.
	/// </summary>
	public class DispatcherTimerStack
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Add a customer timer instance to the stack.
		/// </summary>
		/// <param name="aTimer">Timer instance to add.</param>
		public static void Push(IDispatcherTimer aTimer)
			=> InvocationHelper.InvokePrivateStaticMethod(typeof(DispatcherTimerHelper), "PushTimer", aTimer);


		/// <summary>
		///     Clear the stack of custom dispatcher timers and optionally assert if any were left over.
		/// </summary>
		/// <param name="aAssertEmpty">Assert if some timers were not used. Defaults to true.</param>
		public static void Reset(bool aAssertEmpty = true)
		{
			var result =
				InvocationHelper.InvokePrivateStaticMethod(typeof(DispatcherTimerHelper), "Reset") as
					IEnumerable<IDispatcherTimer>;
			Assert.IsFalse(aAssertEmpty && (null != result) && result.Any(),
						   "Fewer dispatcher timers were used than expected.");
		}

		#endregion
	}
}

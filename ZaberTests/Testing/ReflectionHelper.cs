﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace ZaberTest.Testing
{
	/// <summary>
	///     Helper for reflecting type information.
	/// </summary>
	public static class ReflectionHelper
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Find all concrete implementations of an interface in one or more assemblies.
		/// </summary>
		/// <param name="aInterfaceType">Type to search for assignability to.</param>
		/// <param name="aAssembies">List of assemblies to search for types.</param>
		/// <returns>Zero or more types that are assignable to the specified interface type.</returns>
		public static IEnumerable<Type> FindImplementationsOf(Type aInterfaceType, params Assembly[] aAssembies)
		{
			foreach (var asm in aAssembies)
			{
				foreach (var type in asm.GetExportedTypes())
				{
					if (!type.IsAbstract && aInterfaceType.IsAssignableFrom(type))
					{
						yield return type;
					}
				}
			}
		}

		#endregion
	}
}

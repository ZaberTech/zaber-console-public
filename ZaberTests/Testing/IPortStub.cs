﻿using System.Collections.Generic;
using Zaber;

namespace ZaberTest.Testing
{
	public interface IPortStub : IZaberPort
	{
		#region -- Public Methods & Properties --

		void ReceiveBytes();

		void TriggerDelayedSend();

		/// <summary>
		///     Gets or sets a flag that records whether to pretend the serial port
		///     is open.
		/// </summary>
		bool IsStubPortOpen { get; set; }

		List<byte> BytesToReceive { get; set; }

		List<byte> SentBytes { get; set; }

		bool IsDelayTimerEnabled { get; set; }

		#endregion
	}
}

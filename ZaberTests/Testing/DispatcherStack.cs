﻿using System;
using System.Collections.Generic;
using ZaberWpfToolbox;

namespace Zaber.Testing
{
	/// <summary>
	///     Helper for managing dispatchers during testing. Provides facilities
	///     for overriding and restoring the current dispatcher implementation, and
	///     detects when a test did not restore the original dispatcher state.
	/// </summary>
	public static class DispatcherStack
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Override the current dispatcher with a new implementation.
		///     Call this in the setup of tests that need to use a custom dispatcher implementation.
		/// </summary>
		/// <param name="aNewDispatcher">The new dispatcher to use.</param>
		/// <exception cref="InvalidProgramException">A missing pop in another test was detected.</exception>
		public static void Push(IDispatcher aNewDispatcher)
		{
			while (0 != _dispatchers.Count)
			{
				var tos = _dispatchers.Pop();
				DispatcherHelper.Dispatcher = tos.Item1;
				throw new InvalidProgramException("A test failed to pop the dispatcher pushed at: " + tos.Item2);
			}

			_dispatchers.Push(new Tuple<IDispatcher, string>(DispatcherHelper.Dispatcher, Environment.StackTrace));
			DispatcherHelper.Dispatcher = aNewDispatcher;
		}


		/// <summary>
		///     Restore the previous dispatcher context. Call this from the teardown of tests
		///     that use custom dispatchers. Calls to pop must correspond 1:1 with calls to push.
		/// </summary>
		/// <exception cref="InvalidProgramException">An extra pop in another test was detected.</exception>
		public static void Pop()
		{
			while (_dispatchers.Count < 1)
			{
				throw new InvalidProgramException("Detected extra pop of dispatcher type.");
			}

			var tos = _dispatchers.Pop();
			DispatcherHelper.Dispatcher = tos.Item1;
		}

		#endregion

		#region -- Data --

		private static readonly Stack<Tuple<IDispatcher, string>>
			_dispatchers = new Stack<Tuple<IDispatcher, string>>();

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using Zaber.Threading;

namespace Zaber.Testing
{
	/// <summary>
	///     Helper for managing background worker types during testing. Provides facilities
	///     for overriding and restoring the current worker type, and
	///     detects when a test did not restore the original state.
	/// </summary>
	public static class BackgroundWorkerStack
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Override the current background worker type with a new type.
		///     Call this in the setup of tests that need to use a custom worker implementation.
		/// </summary>
		/// <param name="aNewType">The new background worker to use.</param>
		/// <exception cref="InvalidProgramException">A missing pop in another test was detected.</exception>
		public static void Push(Type aNewType)
		{
			while (0 != _workerTypes.Count)
			{
				var tos = _workerTypes.Pop();
				BackgroundWorkerManager.WorkerType = tos.Item1;
				throw new InvalidProgramException("A test failed to pop the background worker type pushed by: "
											  + tos.Item2);
			}

			_workerTypes.Push(new Tuple<Type, string>(BackgroundWorkerManager.WorkerType, Environment.StackTrace));
			BackgroundWorkerManager.WorkerType = aNewType;
		}


		/// <summary>
		///     Restore the previous background worker type. Call this from the teardown of tests
		///     that use custom worker types. Calls to pop must correspond 1:1 with calls to push.
		/// </summary>
		/// <exception cref="InvalidProgramException">An extra pop in another test was detected.</exception>
		public static void Pop()
		{
			while (_workerTypes.Count < 1)
			{
				throw new InvalidProgramException("Detected extra pop of background worker type.");
			}

			var tos = _workerTypes.Pop();
			BackgroundWorkerManager.WorkerType = tos.Item1;
		}

		#endregion

		#region -- Data --

		private static readonly Stack<Tuple<Type, string>> _workerTypes = new Stack<Tuple<Type, string>>();

		#endregion
	}
}

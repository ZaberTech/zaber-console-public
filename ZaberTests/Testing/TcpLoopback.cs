﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace ZaberTest.Testing
{
	/// <summary>
	///     Listens for connections on a TCP port and echoes back and data it receives.
	/// </summary>
	public class TcpLoopback
	{
		#region -- Public Methods & Properties --

		public TcpLoopback(int aPortNumber)
		{
			_portNumber = aPortNumber;
		}


		// Adapted from https://gist.github.com/jrusbatch/4211535
		public static int GetAvailablePortNumber()
		{
			var startingPort = 10000;
			var properties = IPGlobalProperties.GetIPGlobalProperties();

			var tcpConnectionPorts = new HashSet<int>(properties.GetActiveTcpConnections()
															 .Where(n => n.LocalEndPoint.Port >= startingPort)
															 .Select(n => n.LocalEndPoint.Port));

			var tcpListenerPorts = new HashSet<int>(properties.GetActiveTcpListeners()
														   .Where(n => n.Port >= startingPort)
														   .Select(n => n.Port));

			var udpListenerPorts = new HashSet<int>(properties.GetActiveUdpListeners()
														   .Where(n => n.Port >= startingPort)
														   .Select(n => n.Port));

			var port = Enumerable.Range(startingPort, ushort.MaxValue)
							  .Where(i => !tcpConnectionPorts.Contains(i))
							  .Where(i => !tcpListenerPorts.Contains(i))
							  .Where(i => !udpListenerPorts.Contains(i))
							  .First();

			return port;
		}


		public void Start()
		{
			if (null != _task)
			{
				throw new InvalidOperationException("Server started twice.");
			}

			Exception = null;
			_task = Task.Factory.StartNew(() => Run(_portNumber));
			_started.Wait();
		}


		public void Stop()
		{
			if (null != _task)
			{
				_shouldExit = true;
				_signal.Set();
				_task.Wait();
				_task = null;
			}
		}


		public Exception Exception { get; private set; }

		#endregion

		#region -- Non-Public Methods --

		private void Run(int aPortNumber)
		{
			var tasks = new List<Task>();
			var listener = new TcpListener(IPAddress.Any, aPortNumber);
			listener.Start();

			_started.Set();
			while (!_shouldExit)
			{
				if (listener.Pending())
				{
					var client = listener.AcceptTcpClient();
					tasks.Add(Task.Factory.StartNew(() => HandleClient(client)));
				}
				else
				{
					Thread.Sleep(10);
				}
			}

			foreach (var client in tasks)
			{
				client.Wait();
			}

			listener.Stop();
		}


		private void HandleClient(TcpClient client)
		{
			var queue = new ConcurrentQueue<byte[]>();

			try
			{
				var stream = client.GetStream();

				var write = Task.Factory.StartNew(() =>
				{
					using (var writer = new BinaryWriter(stream))
					{
						while (!_shouldExit && client.Connected)
						{
							byte[] data = null;
							while (queue.TryDequeue(out data))
							{
								try
								{
									writer.Write(data);
									writer.Flush();
								}
								catch (Exception aException)
								{
									Exception = aException;
									_shouldExit = true;
								}
							}

							_signal.Wait();
						}
					}
				});

				var read = Task.Factory.StartNew(() =>
				{
					using (var reader = new BinaryReader(stream))
					{
						while (!_shouldExit && client.Connected)
						{
							var data = new byte[1024];
							var numRead = 0;

							try
							{
								numRead = reader.Read(data, 0, data.Length);
							}
							catch (Exception aException)
							{
								Exception = aException;
								_shouldExit = true;
							}

							if (numRead > 0)
							{
								if (numRead < data.Length)
								{
									var buf = new byte[numRead];
									Array.Copy(data, buf, numRead);
									data = buf;
								}

								queue.Enqueue(data);
								_signal.Set();
							}
						}
					}
				});

				using (client)
				{
					read.Wait();
					write.Wait();
				}
			}
			catch (Exception aException)
			{
				Exception = aException;
			}
		}

		#endregion

		#region -- Data --

		private readonly int _portNumber;
		private readonly ManualResetEventSlim _started = new ManualResetEventSlim(false);
		private readonly ManualResetEventSlim _signal = new ManualResetEventSlim(false);
		private Task _task;
		private volatile bool _shouldExit;

		#endregion
	}
}

﻿using System.Threading;
using Zaber.Threading;

namespace ZaberTest.Testing
{
	/// <summary>
	///     Synchronous "thread" implementation for tests. Calling
	///     <see cref="Start" /> will execute the entry point function
	///     immediately in the current thread.
	/// </summary>
	public class SynchronousThread : IThread
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Initialize the thread with a non-parameterized entry point.
		/// </summary>
		/// <param name="aEntryPoint">Function to be run by the thread.</param>
		public SynchronousThread(ThreadStart aEntryPoint)
		{
			_entryPoint = aEntryPoint;
		}


		/// <summary>
		///     Executes the thread entry point code immediately in the current thread.
		/// </summary>
		public void Start()
		{
			IsAlive = true;
			_entryPoint?.Invoke();
			IsAlive = false;
		}


		/// <summary>
		///     Has no effect in this implementation.
		/// </summary>
		public void Abort()
		{
		}


		/// <summary>
		///     Does nothing in this implementation.
		/// </summary>
		public void Join()
		{
		}


		/// <summary>
		///     Check whether the thread is running. For this implementation,
		///     this returns true while the thread is executing, meaning a true
		///     value can be seen only if queried by another thread.
		/// </summary>
		public bool IsAlive { get; private set; }

		/// <inheritdoc cref="Thread.Name" />
		public string Name { get; set; }

		#endregion

		#region -- Data --

		private readonly ThreadStart _entryPoint;

		#endregion
	}
}

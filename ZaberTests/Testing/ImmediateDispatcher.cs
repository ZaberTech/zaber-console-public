﻿using System;
using ZaberWpfToolbox;

namespace Zaber.Testing
{
	/// <summary>
	///     IDispatcher implementation for tests that check that view data ends up correctly
	///     after an async operaiton like port open/close. This forces everything to occur
	///     synchronously.
	/// </summary>
	public class ImmediateDispatcher : IDispatcher
	{
		#region -- Public Methods & Properties --

		public void BeginInvoke(Action aAction) => aAction.Invoke();

		public void Invoke(Action aAction) => aAction.Invoke();

		#endregion
	}
}

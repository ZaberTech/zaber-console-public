﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace ZaberTest.Testing
{
	/// <summary>
	///     Extra assert helpers that are not in NUnit.
	/// </summary>
	public static class AssertUtils
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Compares two sequences. They are considered the same if they have the
		///     same length and each item's default equality operation is true for the
		///     corresponding item in the other collection.
		///     Null inputs are error conditions. Empty inputs are not.
		/// </summary>
		/// <typeparam name="T">
		///     Base type of the collections' contents. Use object if
		///     the collections contain different types that are expected to compare equal.
		/// </typeparam>
		/// <param name="aExpected">The expected sequence.</param>
		/// <param name="aActual">The result to compare to expected.</param>
		public static void SequenceEqual<T>(IEnumerable<T> aExpected, IEnumerable<T> aActual)
		{
			Assert.IsNotNull(aExpected, nameof(aExpected) + " must not be null.");
			Assert.IsNotNull(aActual, nameof(aExpected) + " must not be null.");
			var expected = aExpected.ToArray();
			var actual = aActual.ToArray();
			var length = Math.Max(expected.Length, actual.Length);
			for (var i = 0; i < length; i++)
			{
				Assert.Greater(expected.Length,
							   i,
							   $"Collection contains more elements than expected; first extra is at index {i}: '{actual[i]?.ToString() ?? "<null>"}'");

				Assert.Greater(actual.Length,
							   i,
							   $"Collection contains fewer elements than expected; first missing is at index {i}: '{expected[i]?.ToString() ?? "<null>"}'");

				Assert.AreEqual(expected[i],
								actual[i],
								$"Collection contents differ at index {i}: expected '{expected[i]?.ToString() ?? "<null>"}' but was '{actual[i]?.ToString() ?? "<null>"}");
			}
		}

		#endregion
	}
}

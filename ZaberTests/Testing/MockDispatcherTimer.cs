﻿using System;
using NUnit.Framework;
using ZaberWpfToolbox;

namespace ZaberTest.Testing
{
	/// <summary>
	///     Mock implementation of a DispatcherTimer for testing purposes. Does not use any
	///     actual timers; test code controls when it ticks.
	/// </summary>
	public class MockDispatcherTimer : IDispatcherTimer
	{
		#region -- Events --

		/// <inheritdoc cref="IDispatcherTimer.Tick" />
		public event EventHandler Tick;

		#endregion

		#region -- Public Methods & Properties --

		public void FireTickEvent()
		{
			Assert.IsTrue(IsEnabled, "Test code error: A timer won't generate tick events when not started.");
			Tick?.Invoke(this, EventArgs.Empty);
		}


		/// <inheritdoc cref="IDispatcherTimer.Start" />
		public void Start() => IsEnabled = true;


		/// <inheritdoc cref="IDispatcherTimer.Stop" />
		public void Stop() => IsEnabled = false;


		/// <inheritdoc cref="IDispatcherTimer.IsEnabled" />
		public bool IsEnabled { get; set; }

		/// <inheritdoc cref="IDispatcherTimer.Interval" />
		public TimeSpan Interval { get; set; }

		#endregion
	}
}

﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Web.Script.Serialization;
using NUnit.Framework;
using Zaber;
using Zaber.Units;

namespace ZaberTest.Testing
{
	/// <summary>
	///     Helper class for generating complex ASCII commands that can be used
	///     with <see cref="ZaberDevice.FormatAsciiCommand(string, object[])" />.
	/// </summary>
	public static class AsciiCommandBuilder
	{
		#region -- Public data --

		public static IDictionary<string, ParameterType> StandardParamTypes = new Dictionary<string, ParameterType>
		{
			{
				"Tenths", new ParameterType
				{
					Name = "Tenths",
					IsNumeric = true,
					IsSigned = true,
					DecimalPlaces = 1
				}
			},
			{
				"UTenths", new ParameterType
				{
					Name = "UTenths",
					IsNumeric = true,
					DecimalPlaces = 1
				}
			},
			{ "Token", new ParameterType { Name = "Token" } },
			{
				"Uint", new ParameterType
				{
					Name = "Uint",
					IsNumeric = true
				}
			},
			{
				"Uint8", new ParameterType
				{
					Name = "Uint8",
					IsNumeric = true
				}
			},
			{
				"Uint16", new ParameterType
				{
					Name = "Uint16",
					IsNumeric = true
				}
			},
			{
				"Uint32", new ParameterType
				{
					Name = "Uint32",
					IsNumeric = true
				}
			},
			{
				"Uint64", new ParameterType
				{
					Name = "Uint64",
					IsNumeric = true
				}
			},
			{
				"Int", new ParameterType
				{
					Name = "Int",
					IsNumeric = true,
					IsSigned = true
				}
			},
			{
				"Int8", new ParameterType
				{
					Name = "Int8",
					IsNumeric = true,
					IsSigned = true
				}
			},
			{
				"Int16", new ParameterType
				{
					Name = "Int16",
					IsNumeric = true,
					IsSigned = true
				}
			},
			{
				"Int32", new ParameterType
				{
					Name = "Int32",
					IsNumeric = true,
					IsSigned = true
				}
			},
			{
				"Int64", new ParameterType
				{
					Name = "Int64",
					IsNumeric = true,
					IsSigned = true
				}
			},
			{
				"Bool", new ParameterType
				{
					Name = "Bool",
					IsBoolean = true
				}
			},
			{
				"Uint16DP1", new ParameterType
				{
					Name = "Uint16DP1",
					IsNumeric = true,
					DecimalPlaces = 1
				}
			},
			{
				"Uint16DP2", new ParameterType
				{
					Name = "Uint16DP2",
					IsNumeric = true,
					DecimalPlaces = 2
				}
			},
			{
				"Uint16DP3", new ParameterType
				{
					Name = "Uint16DP3",
					IsNumeric = true,
					DecimalPlaces = 3
				}
			},
			{
				"Uint32DP1", new ParameterType
				{
					Name = "Uint32DP1",
					IsNumeric = true,
					DecimalPlaces = 1
				}
			},
			{
				"Uint32DP3", new ParameterType
				{
					Name = "Uint32DP3",
					IsNumeric = true,
					DecimalPlaces = 3
				}
			},
			{
				"Uint64DP9", new ParameterType
				{
					Name = "Uint64DP9",
					IsNumeric = true,
					DecimalPlaces = 9
				}
			},
			{
				"Int16DP1", new ParameterType
				{
					Name = "Int16DP1",
					IsNumeric = true,
					IsSigned = true,
					DecimalPlaces = 1
				}
			},
			{
				"Int32DP3", new ParameterType
				{
					Name = "Int32DP3",
					IsNumeric = true,
					IsSigned = true,
					DecimalPlaces = 3
				}
			},
			{
				"IntDP1", new ParameterType
				{
					Name = "IntDP1",
					IsNumeric = true,
					IsSigned = true,
					DecimalPlaces = 1
				}
			},
			{
				"UintDP1", new ParameterType
				{
					Name = "Uint16DP1",
					IsNumeric = true,
					DecimalPlaces = 1
				}
			}
		};

		/// <summary>
		///     Resource name for the basic move command definitions.
		/// </summary>
		public const string BASIC_MOVE_COMMAND_FILE = "ZaberTest.Resources.BasicMoveCommands.json";

		/// <summary>
		///     Resource name for the basic force command definitions.
		/// </summary>
		public const string FORCE_COMMAND_FILE = "ZaberTest.Resources.ForceCommands.json";

		/// <summary>
		///     Resource name for the canned joystick axis command definitions.
		/// </summary>
		public const string JOYSTICK_COMMAND_FILE = "ZaberTest.Resources.JoystickCommands.json";

		/// <summary>
		///     Resource name for the canned joystick key command definitions.
		/// </summary>
		public const string KEY_COMMAND_FILE = "ZaberTest.Resources.KeyCommands.json";

		/// <summary>
		///     Resource name for the canned servo command definitions.
		/// </summary>
		public const string SERVO_COMMAND_FILE = "ZaberTest.Resources.ServoCommands.json";

		/// <summary>
		///     Resource name for the canned servo command definitions.
		/// </summary>
		public const string CALIBRATION_COMMAND_FILE = "ZaberTest.Resources.CalibrationCommands.json";

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Read command definitions from a JSON file or embedded resource.
		/// </summary>
		/// <param name="aFilePath">
		///     Path to the file to read, or name of a resource
		///     file in the Zaber.Test assembly.
		/// </param>
		/// <returns>Commands parsed from the file.</returns>
		public static IEnumerable<ASCIICommandInfo> ReadCommandDefinitionsFromJsonFile(string aFilePath)
		{
			string fileText = null;
			if (File.Exists(aFilePath))
			{
				fileText = File.ReadAllText(aFilePath);
			}
			else
			{
				var assembly = Assembly.GetExecutingAssembly();

				using (var stream = assembly.GetManifestResourceStream(aFilePath))
				{
					using (var reader = new StreamReader(stream))
					{
						fileText = reader.ReadToEnd();
					}
				}
			}

			return ReadCommandDefinitionsFromJsonString(fileText);
		}


		/// <summary>
		///     Read command definitions from a JSON string.
		/// </summary>
		/// <param name="aText">String containing JSON command definition.</param>
		/// <returns>Commands parsed from the string.</returns>
		public static IEnumerable<ASCIICommandInfo> ReadCommandDefinitionsFromJsonString(string aText)
		{
			var ser = new JavaScriptSerializer();
			var json = ser.Deserialize<JsonAsciiCommandFile>(aText);
			foreach (var jsonCommand in json.Commands)
			{
				ASCIICommandNode node = null;

				foreach (var jsonNode in jsonCommand.Nodes)
				{
					node = new ASCIICommandNode(node) { NodeText = jsonNode.Text };

					switch (jsonNode.ParameterType)
					{
						case "keyword":
							break;
						case "Enum":
							node.ParamType = new ParameterType(jsonNode.EnumValues);
							break;
						default:
							if (!string.IsNullOrEmpty(jsonNode.ParameterType)
							&& StandardParamTypes.ContainsKey(jsonNode.ParameterType))
							{
								node.ParamType = new ParameterType(StandardParamTypes[jsonNode.ParameterType]);
							}

							break;
					}

					if (!string.IsNullOrEmpty(jsonNode.RequestUnitSymbol))
					{
						node.RequestUnit = UnitOfMeasure.FindByAbbreviation(jsonNode.RequestUnitSymbol);
						if (null == node.RequestUnit)
						{
							var unit = UnitConverter.Default.FindUnitBySymbol(jsonNode.RequestUnitSymbol);
							if (null != unit)
							{
								node.RequestUnit = new UnitOfMeasure(unit);
							}
							else
							{
								Assert.Fail($"Could not find unit definition for '{jsonNode.RequestUnitSymbol}'");
							}
						}
					}

					node.RequestUnitScale = jsonNode.Scale;

					if (null != node.ParamType)
					{
						node.ParamType.Arity = jsonNode.Arity;
					}

					if (jsonNode.Function.HasValue)
					{
						node.RequestUnitFunction = jsonNode.Function.Value;
					}
				}

				var command = new ASCIICommandInfo(node);

				if (!string.IsNullOrEmpty(jsonCommand.ResponseUnitSymbol))
				{
					command.ResponseUnit = UnitOfMeasure.FindByAbbreviation(jsonCommand.ResponseUnitSymbol);
				}

				command.ResponseUnitScale = jsonCommand.ResponseScale;

				if (jsonCommand.ResponseFunction.HasValue)
				{
					command.RequestUnitFunction = jsonCommand.ResponseFunction.Value;
				}

				yield return command;
			}
		}

		#endregion
	}
}

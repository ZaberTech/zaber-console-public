﻿using System;
using System.Collections.Generic;
using Zaber;

namespace ZaberTest.Testing
{
	[Serializable]
	public class JsonAsciiCommandFile
	{
		#region -- Public Methods & Properties --

		public IList<JsonAsciiCommand> Commands { get; set; }

		#endregion
	}


	[Serializable]
	public class JsonAsciiCommand
	{
		#region -- Public Methods & Properties --

		public IList<JsonAsciiNode> Nodes { get; set; }

		public decimal? ResponseScale { get; set; }

		public ScalingFunction? ResponseFunction { get; set; }

		public string ResponseUnitSymbol { get; set; }

		#endregion
	}


	[Serializable]
	public class JsonAsciiNode
	{
		#region -- Public Methods & Properties --

		public string Text { get; set; }

		public string ParameterType { get; set; }

		public IList<string> EnumValues { get; set; }

		public decimal? Scale { get; set; }

		public ScalingFunction? Function { get; set; }

		public string RequestUnitSymbol { get; set; }

		public int? Arity { get; set; } = 1;

		#endregion
	}
}

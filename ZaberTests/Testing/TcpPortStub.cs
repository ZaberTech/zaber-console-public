﻿using System;
using System.Collections.Generic;
using Zaber.Ports;

namespace ZaberTest.Testing
{
	internal class TcpPortStub : TcpPort, IPortStub
	{
		#region -- Public Methods & Properties --

		public TcpPortStub()
			: base("localhost", 65535)
		{
			BytesToReceive = new List<byte>();
			SentBytes = new List<byte>();
		}


		public void ReceiveBytes()
		{
			if (BytesToReceive == null)
			{
				throw new InvalidOperationException();
			}

			foreach (var c in BytesToReceive)
			{
				PacketConverter.ReceiveByte(c);
			}

			BytesToReceive.Clear();
			DispatchEvents();
		}


		public void TriggerDelayedSend() => OnDelayedSendTimer(null);


		/// <summary>
		///     Gets or sets a flag that records whether to pretend the serial port
		///     is open.
		/// </summary>
		public bool IsStubPortOpen { get; set; }

		public override bool IsOpen => IsStubPortOpen;

		public List<byte> BytesToReceive { get; set; }

		public List<byte> SentBytes { get; set; }

		public bool IsDelayTimerEnabled { get; set; }

		#endregion

		#region -- Non-Public Methods --

		protected override void WriteData(byte[] bytes) => SentBytes.AddRange(bytes);


		protected override void EnableDelayedSendTimer(bool isEnabled) => IsDelayTimerEnabled = isEnabled;

		#endregion
	}
}

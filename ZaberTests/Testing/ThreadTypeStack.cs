﻿using System;
using System.Collections.Generic;
using Zaber.Threading;

namespace Zaber.Testing
{
	/// <summary>
	///     Helper for managing thread types during testing. Provides facilities
	///     for overriding and restoring the current thread type, and
	///     detects when a test did not restore the original state.
	/// </summary>
	public static class ThreadTypeStack
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Override the current thread type with a new type.
		///     Call this in the setup of tests that need to use a custom thread implementation.
		/// </summary>
		/// <param name="aNewType">The new thread to use.</param>
		/// <exception cref="InvalidProgramException">A missing pop in another test was detected.</exception>
		public static void Push(Type aNewType)
		{
			while (0 != _threadTypes.Count)
			{
				var tos = _threadTypes.Pop();
				BackgroundWorkerManager.WorkerType = tos.Item1;
				throw new InvalidProgramException("A test failed to pop the thread type pushed by: " + tos.Item2);
			}

			_threadTypes.Push(new Tuple<Type, string>(ThreadFactory.ThreadType, Environment.StackTrace));
			ThreadFactory.ThreadType = aNewType;
		}


		/// <summary>
		///     Restore the previous thread type. Call this from the teardown of tests
		///     that use custom thread types. Calls to pop must correspond 1:1 with calls to push.
		/// </summary>
		/// <exception cref="InvalidProgramException">An extra pop in another test was detected.</exception>
		public static void Pop()
		{
			while (_threadTypes.Count < 1)
			{
				throw new InvalidProgramException("Detected extra pop of thread type.");
			}

			var tos = _threadTypes.Pop();
			ThreadFactory.ThreadType = tos.Item1;
		}

		#endregion

		#region -- Data --

		private static readonly Stack<Tuple<Type, string>> _threadTypes = new Stack<Tuple<Type, string>>();

		#endregion
	}
}

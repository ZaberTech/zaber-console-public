﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using TickingTest;
using Zaber;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class DeviceListenerTest
	{
		[Test]
		public void Blocking() => TestFramework.RunOnce(new BlockingThreads());


		[Test]
		public void FromConversation()
		{
			// SETUP
			byte deviceNumber = 23;
			var port = new MockPort();
			var device = CreateDevice(deviceNumber, port);
			var conversation = new Conversation(device);
			var command = Command.Home;
			var data = 0;

			// EXEC
			var listener = new DeviceListener(conversation);
			port.FireResponse(deviceNumber, command, data);

			var response = listener.NextResponse();

			Assert.AreEqual(command,
							response.Command,
							"command");
			Assert.AreEqual(data,
							response.NumericData,
							"data");
		}


		[Test]
		public void NextResponse()
		{
			// SETUP
			byte deviceNumber = 23;
			var port = new MockPort();
			var device = CreateDevice(deviceNumber, port);
			var command = Command.Home;
			var data = 0;

			// EXEC
			var listener = new DeviceListener(device);
			port.FireResponse(deviceNumber, command, data);

			var response = listener.NextResponse();

			Assert.AreEqual(command,
							response.Command,
							"command");
			Assert.AreEqual(data,
							response.NumericData,
							"data");
		}


		[Test]
		public void NextResponseWithFilter()
		{
			byte deviceNumber = 23;
			var port = new MockPort();
			var device = CreateDevice(deviceNumber, port);

			var listener = new DeviceListener(device)
			{
				Filter = msg => 1m == msg.NumericData
			};

			port.FireResponse(deviceNumber, Command.Home, 1);
			port.FireResponse(deviceNumber, Command.EchoData, 0);

			var response = listener.NextResponse();

			Assert.AreEqual(Command.Home, response.Command);
			Assert.AreEqual(1m, response.NumericData);
		}


		[Test]
		public void NonBlocking()
		{
			// SETUP
			byte deviceNumber = 23;
			var port = new MockPort();
			var device = CreateDevice(deviceNumber, port);
			var command = Command.Home;
			var data = 0;
			var isBlocking = false;

			// EXEC
			var listener = new DeviceListener(device);
			port.FireResponse(deviceNumber, command, data);

			var response = listener.NextResponse(isBlocking);

			Assert.AreEqual(command,
							response.Command,
							"command");
			Assert.AreEqual(data,
							response.NumericData,
							"data");
		}


		[Test]
		public void NonBlockingNoResponse()
		{
			// SETUP
			byte deviceNumber = 23;
			var port = new MockPort();
			var device = CreateDevice(deviceNumber, port);
			var isBlocking = false;

			// EXEC
			var listener = new DeviceListener(device);

			var response = listener.NextResponse(isBlocking);

			Assert.IsNull(response, "response");
		}


		[Test]
		public void StartWhenStarted()
		{
			// SETUP
			byte deviceNumber = 23;
			var port = new MockPort();
			var device = CreateDevice(deviceNumber, port);

			// EXEC
			var listener = new DeviceListener(device);
			string msg = null;
			try
			{
				listener.Start();
				Assert.Fail("should have thrown.");
			}
			catch (InvalidOperationException ex)
			{
				msg = ex.Message;
			}

			// VERIFY
			Assert.AreEqual("Listener is already started.",
							msg,
							"msg");
		}


		[Test]
		public void Stop()
		{
			// SETUP
			byte deviceNumber = 23;
			var port = new MockPort();
			var device = CreateDevice(deviceNumber, port);

			// EXEC
			var listener = new DeviceListener(device);
			var hasSubscribersAfterCreate = device.HasSubscribers;
			listener.Stop();
			var hasSubscribersAfterStop = device.HasSubscribers;
			listener.Start();
			var hasSubscribersAfterStart = device.HasSubscribers;

			Assert.IsTrue(hasSubscribersAfterCreate,
						  "hasSubscribers");
			Assert.IsFalse(hasSubscribersAfterStop,
						   "hasSubscribers");
			Assert.IsTrue(hasSubscribersAfterStart,
						  "hasSubscribers");
		}


		[Test]
		public void StopWhenStopped()
		{
			// SETUP
			byte deviceNumber = 23;
			var port = new MockPort();
			var device = CreateDevice(deviceNumber, port);

			// EXEC
			var listener = new DeviceListener(device);
			listener.Stop();

			string msg = null;
			try
			{
				listener.Stop();
				Assert.Fail("should have thrown.");
			}
			catch (InvalidOperationException ex)
			{
				msg = ex.Message;
			}

			// VERIFY
			Assert.AreEqual("Listener is already stopped.",
							msg,
							"msg");
		}


		[Test]
		public void TestInfoList()
		{
			var timer = new MockTimeoutTimer();
			var port = new MockPort();
			var device = CreateDevice(1, port);
			device.DeviceType.FirmwareVersion = new FirmwareVersion(6, 14);
			var conv = new Conversation(device);

			port.ReadExpectationsFromFile(Path.Combine(TestContext.CurrentContext.TestDirectory,
													   "list_response_new_fw.txt"));
			var responses = DeviceListener.ReceiveInfoListContent(conv, "servo print params ffpid", timer);

			var settingNames = new HashSet<string>(new[]
			{
				"finv1", "finv2", "fcla", "fcla.hs", "fclg", "fclg.hs", "ki",
				"ki.hs", "kp", "kp.hs", "kd", "kd.hs", "fc1", "fc1.hs",
				"gain"
			});

			foreach (var msg in responses)
			{
				var parts = msg.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
				Assert.AreEqual("ffpid", parts[0]);
				Assert.IsTrue(settingNames.Contains(parts[1]));
			}
		}


		[Test]
		public void TestInfoList_NullTimeout_RM7767()
		{
			var port = new MockPort();
			port.IsAsciiMode = true;
			var device = CreateDevice(1, port);
			device.DeviceType.FirmwareVersion = new FirmwareVersion(6, 14);
			device.AreAsciiMessageIdsEnabled = true;
			var conv = new Conversation(device);
			conv.Timeout = 1000;

			port.ReadExpectationsFromString(
				@"/01 0 01 help commands list
				@01 0 01 OK IDLE -- 0
				/01 0 02 
				#01 0 01 foo
				#01 0 01 bar
				#01 0 01 baz
				@01 0 02 OK IDLE -- 0");

			// Before the fix a null reference error would have happened here.
			var responses = DeviceListener.ReceiveInfoListContent(conv, "help commands list").ToArray();
			Assert.IsTrue((new string[] { "foo", "bar", "baz" }).SequenceEqual(responses));
		}


		[Test]
		public void TestInfoList_OldFirmware()
		{
			var timer = new TimeoutTimer();
			timer.Timeout = 250;
			var port = new MockPort();
			var device = CreateDevice(1, port);
			device.DeviceType.FirmwareVersion = new FirmwareVersion(6, 13);
			var conv = new Conversation(device);

			port.ReadExpectationsFromFile(Path.Combine(TestContext.CurrentContext.TestDirectory,
													   "list_response_old_fw.txt"));
			var responses = DeviceListener.ReceiveInfoListContent(conv, "servo print params ffpid", timer);

			var settingNames = new HashSet<string>(new[]
			{
				"finv1", "finv2", "fcla", "fcla.hs", "fclg", "fclg.hs", "ki",
				"ki.hs", "kp", "kp.hs", "kd", "kd.hs", "tf", "tf.hs", "gain"
			});

			foreach (var msg in responses)
			{
				var parts = msg.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
				Assert.AreEqual("ffpid", parts[0]);
				Assert.IsTrue(settingNames.Contains(parts[1]));
			}
		}


		[Test]
		public void TestInfoList_OldFirmware_NoTimeout()
		{
			var port = new MockPort();
			var device = CreateDevice(1, port);
			device.DeviceType.FirmwareVersion = new FirmwareVersion(6, 13);
			var conv = new Conversation(device);

			Assert.Throws<ArgumentException>(() => DeviceListener.ReceiveInfoListContent(conv, "servo print params ffpid"));
		}


		[Test]
		public void TestInfoList_WaitForCorrectFooterReply_RM6791()
		{
			var timer = new MockTimeoutTimer();
			var port = new MockPort();
			port.IsAsciiMode = true;
			var device = CreateDevice(1, port);
			device.DeviceType.FirmwareVersion = new FirmwareVersion(6, 14);
			device.AreAsciiMessageIdsEnabled = true;
			var conv = new Conversation(device);

			port.ReadExpectationsFromString(
			  @"/01 0 01 help commands list
				@01 0 01 OK IDLE -- 0
				/01 0 02 
				#01 0 01 foo
				@01 1 03 OK IDLE -- 0
				#01 0 01 bar
				@01 1 04 OK IDLE -- 0
				@02 1 05 OK IDLE -- 0
				@03 1 06 OK IDLE -- 0
				#01 0 01 baz
				@01 0 02 OK IDLE -- 0
				");
			var responses = DeviceListener.ReceiveInfoListContent(conv, "help commands list", timer).ToArray();

			// Before the fix only "foo" would have been in the result 
			// because one of the other replies would have been mistaken for it.
			Assert.IsTrue((new string[] { "foo", "bar", "baz"}).SequenceEqual(responses));
		}


		[Test]
		public void TestInfoList_WaitForFooterReplyBeforeValidating_RM6791()
		{
			var timer = new MockTimeoutTimer()
			{
				Timeout = 1000
			};

			var port = new MockPort();
			port.IsAsciiMode = true;
			var device = CreateDevice(1, port);
			device.DeviceType.FirmwareVersion = new FirmwareVersion(6, 14);
			device.AreAsciiMessageIdsEnabled = true;
			var conv = new Conversation(device);
			conv.Timeout = 5000;

			port.ReadExpectationsFromString(
				@"/01 0 01 help commands list
				@01 0 01 OK IDLE -- 0
				/01 0 02 
				#01 0 01 foo
				#01 0 01 bar
				#01 0 01 baz");

			Task.Factory.StartNew(() =>
			{
				var startTime = DateTime.Now;
				while (port.UnmatchedExpectationCount > 0)
				{
					System.Threading.Thread.Sleep(10);
					if ((DateTime.Now - startTime).TotalSeconds > 30)
					{
						Assert.Fail("Test stalled.");
					}
				}

				timer.TimeoutNow(); // Terminates list reception.
				port.FireResponse("@01 0 02 OK IDLE -- 0");
			});

			// Before the fix a validation error would have happened here
			// because topic.Validate() would have been called on the footer
			// message topic before its response was received (the FireResponse call above).
			var responses = DeviceListener.ReceiveInfoListContent(conv, "help commands list", timer).ToArray();
			Assert.IsTrue((new string[] { "foo", "bar", "baz" }).SequenceEqual(responses));
		}


		[Test]
		public void TestInfoList_TerminatorTimeoutStartsAtEndOfList_RM7768()
		{
			var timer = new MockTimeoutTimer()
			{
				Timeout = 2000
			};

			var port = new MockPort();
			port.IsAsciiMode = true;
			var device = CreateDevice(1, port);
			device.DeviceType.FirmwareVersion = new FirmwareVersion(6, 14);
			device.AreAsciiMessageIdsEnabled = true;
			var conv = new Conversation(device);
			conv.Timeout = 100;

			port.ReadExpectationsFromString(
				@"/01 0 01 help commands list
				@01 0 01 OK IDLE -- 0
				/01 0 02 
				#01 0 01 foo
				#01 0 01 bar");

			Task.Factory.StartNew(() =>
			{
				var startTime = DateTime.Now;
				while (port.UnmatchedExpectationCount > 0)
				{
					System.Threading.Thread.Sleep(10);
					if ((DateTime.Now - startTime).TotalSeconds > 30)
					{
						Assert.Fail("Test stalled.");
					}
				}

				System.Threading.Thread.Sleep(500);  // Delay end of list enough that default timeout would have fired before fix.
				port.FireResponse("#01 0 01 baz");
				port.FireResponse("@01 0 02 OK IDLE -- 0");
			});

			// Before the fix a validation error would have happened here
			// because sending the command would have caused the footer topic timeout to
			// be reset to the conversation's default timeout (100ms in this test) and that
			// timer would have been started sometime during reception of the info messages
			// instead of after the last one.
			var responses = DeviceListener.ReceiveInfoListContent(conv, "help commands list", timer).ToArray();
			Assert.IsTrue((new string[] { "foo", "bar", "baz" }).SequenceEqual(responses));
		}


		private class BlockingThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Main()
			{
				// SETUP
				_port = new MockPort();
				var device = CreateDevice(DEVICE_NUMBER, _port);

				// EXEC
				var listener = new DeviceListener(device);

				var response = listener.NextResponse();

				AssertTick(1);
				Assert.AreEqual(COMMAND,
								response.Command,
								"command");
				Assert.AreEqual(DATA,
								response.NumericData,
								"data");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				_port.FireResponse(DEVICE_NUMBER, COMMAND, DATA);
			}

			#endregion

			#region -- Data --

			private const byte DEVICE_NUMBER = 23;
			private const Command COMMAND = Command.Home;
			private const int DATA = 0;
			private MockPort _port;

			#endregion
		}


		private static ZaberDevice CreateDevice(byte deviceNumber, IZaberPort port)
		{
			var device = new ZaberDevice();
			device.Port = port;
			device.DeviceNumber = deviceNumber;
			device.DeviceType =
				DeviceTypeMother.CreateDeviceTypeWithCommands();
			return device;
		}
	}
}

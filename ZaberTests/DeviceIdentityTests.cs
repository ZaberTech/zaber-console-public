﻿using System.Collections.Generic;
using NUnit.Framework;
using Zaber;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class DeviceIdentityTests
	{
		[Test]
		public void TestConstructor()
		{
			var id = new DeviceIdentity();
			Assert.AreEqual(0, id.AxisNumber);
			Assert.AreEqual(0, id.DeviceId);
			Assert.AreEqual(0, id.DeviceNumber);
			Assert.AreEqual(null, id.DeviceSerialNumber);
			Assert.IsFalse(id.IsAllDevices);
			Assert.AreEqual(null, id.PeripheralId);
		}


		// Test custom equality comparison.
		[Test]
		public void TestEquals()
		{
			var id1 = new DeviceIdentity();
			var id2 = new DeviceIdentity();

			Assert.IsFalse(id1.Equals(null));
			Assert.IsTrue(id1.Equals(id2));
			Assert.IsTrue(id2.Equals(id1));

			id2.IsAllDevices = !id2.IsAllDevices;
			Assert.IsFalse(id1.Equals(id2));
			Assert.IsFalse(id2.Equals(id1));
			id2.IsAllDevices = !id2.IsAllDevices;
			Assert.IsTrue(id1.Equals(id2));
			Assert.IsTrue(id2.Equals(id1));

			id2.AxisNumber++;
			Assert.IsFalse(id1.Equals(id2));
			Assert.IsFalse(id2.Equals(id1));
			id2.AxisNumber--;
			Assert.IsTrue(id1.Equals(id2));
			Assert.IsTrue(id2.Equals(id1));

			id2.DeviceId++;
			Assert.IsFalse(id1.Equals(id2));
			Assert.IsFalse(id2.Equals(id1));
			id2.DeviceId--;
			Assert.IsTrue(id1.Equals(id2));
			Assert.IsTrue(id2.Equals(id1));

			id2.DeviceNumber++;
			Assert.IsFalse(id1.Equals(id2));
			Assert.IsFalse(id2.Equals(id1));
			id2.DeviceNumber--;
			Assert.IsTrue(id1.Equals(id2));
			Assert.IsTrue(id2.Equals(id1));

			id2.DeviceSerialNumber = 1;
			Assert.IsFalse(id1.Equals(id2));
			Assert.IsFalse(id2.Equals(id1));
			id1.DeviceSerialNumber = 1;
			Assert.IsTrue(id1.Equals(id2));
			Assert.IsTrue(id2.Equals(id1));

			id2.PeripheralId = 1;
			Assert.IsFalse(id1.Equals(id2));
			Assert.IsFalse(id2.Equals(id1));
			id1.PeripheralId = 1;
			Assert.IsTrue(id1.Equals(id2));
			Assert.IsTrue(id2.Equals(id1));
		}


		// Verify that changing any property changes the hash code.
		[Test]
		public void TestGetHashCode()
		{
			var id = new DeviceIdentity();
			var hash = id.GetHashCode();
			var hashes = new HashSet<int>();
			hashes.Add(hash);

			id.IsAllDevices = true;
			hash = id.GetHashCode();
			Assert.IsFalse(hashes.Contains(hash));
			hashes.Add(hash);

			id.AxisNumber++;
			hash = id.GetHashCode();
			Assert.IsFalse(hashes.Contains(hash));
			hashes.Add(hash);

			id.DeviceId++;
			hash = id.GetHashCode();
			Assert.IsFalse(hashes.Contains(hash));
			hashes.Add(hash);

			id.DeviceNumber++;
			hash = id.GetHashCode();
			Assert.IsFalse(hashes.Contains(hash));
			hashes.Add(hash);

			id.DeviceSerialNumber = 1;
			hash = id.GetHashCode();
			Assert.IsFalse(hashes.Contains(hash));
			hashes.Add(hash);

			id.PeripheralId = 1;
			hash = id.GetHashCode();
			Assert.IsFalse(hashes.Contains(hash));
			hashes.Add(hash);
		}


		// Test masking.
		[Test]
		public void TestMask()
		{
			var id = new DeviceIdentity();
			id.AxisNumber = 1;
			id.DeviceId = 2;
			id.DeviceNumber = 3;
			id.DeviceSerialNumber = 4;
			id.IsAllDevices = true;
			id.PeripheralId = 5;

			var clone = id.Mask(DeviceIdentityMask.All);
			Assert.AreEqual(id, clone);

			clone = id.Mask(DeviceIdentityMask.None);
			Assert.AreEqual(clone, new DeviceIdentity());

			clone = id.Mask(DeviceIdentityMask.AxisNumber);
			Assert.AreEqual(clone, new DeviceIdentity { AxisNumber = 1 });

			clone = id.Mask(DeviceIdentityMask.DeviceId);
			Assert.AreEqual(clone, new DeviceIdentity { DeviceId = 2 });

			clone = id.Mask(DeviceIdentityMask.DeviceNumber);
			Assert.AreEqual(clone, new DeviceIdentity { DeviceNumber = 3 });

			clone = id.Mask(DeviceIdentityMask.IsAllDevices);
			Assert.AreEqual(clone, new DeviceIdentity { IsAllDevices = true });

			clone = id.Mask(DeviceIdentityMask.PeripheralId);
			Assert.AreEqual(clone, new DeviceIdentity { PeripheralId = 5 });

			clone = id.Mask(DeviceIdentityMask.SerialNumber);
			Assert.AreEqual(clone, new DeviceIdentity { DeviceSerialNumber = 4 });
		}


		// Test comparison with masks.
		[Test]
		public void TestMatch()
		{
			var id = new DeviceIdentity();
			id.AxisNumber = 1;
			id.DeviceId = 2;
			id.DeviceNumber = 3;
			id.DeviceSerialNumber = 4;
			id.IsAllDevices = true;
			id.PeripheralId = 5;

			var clone = id.Mask(DeviceIdentityMask.All);
			Assert.AreEqual(id, clone);
			Assert.IsTrue(id.Matches(clone));

			clone = id.Mask(DeviceIdentityMask.None);
			Assert.IsTrue(clone.Matches(new DeviceIdentity()));

			Assert.IsTrue(id.Matches(new DeviceIdentity { AxisNumber = 1 }, DeviceIdentityMask.AxisNumber));
			Assert.IsTrue(id.Matches(new DeviceIdentity { DeviceId = 2 }, DeviceIdentityMask.DeviceId));
			Assert.IsTrue(id.Matches(new DeviceIdentity { DeviceNumber = 3 }, DeviceIdentityMask.DeviceNumber));
			Assert.IsTrue(id.Matches(new DeviceIdentity { IsAllDevices = true }, DeviceIdentityMask.IsAllDevices));
			Assert.IsTrue(id.Matches(new DeviceIdentity { PeripheralId = 5 }, DeviceIdentityMask.PeripheralId));
			Assert.IsTrue(id.Matches(new DeviceIdentity { DeviceSerialNumber = 4 }, DeviceIdentityMask.SerialNumber));
		}


		// Test string conversion contains salient information.
		[Test]
		public void TestToString()
		{
			var id = new DeviceIdentity();
			id.DeviceNumber = 2;
			id.DeviceId = 123;
			var str = id.ToString();
			Assert.IsTrue(str.Contains("2"));
			Assert.IsTrue(str.Contains("123"));

			id.AxisNumber = 4;
			id.PeripheralId = 56;
			str = id.ToString();
			Assert.IsTrue(str.Contains("2"));
			Assert.IsTrue(str.Contains("123"));
			Assert.IsTrue(str.Contains("4"));
			Assert.IsTrue(str.Contains("56"));

			id.DeviceSerialNumber = 987;
			str = id.ToString();
			Assert.IsTrue(str.Contains("987"));
			Assert.IsFalse(str.Contains("2"));
			Assert.IsFalse(str.Contains("123"));
			Assert.IsFalse(str.Contains("4"));
			Assert.IsFalse(str.Contains("56"));

			id.IsAllDevices = true;
			str = id.ToString();
			Assert.IsFalse(str.Contains("987"));
			Assert.IsFalse(str.Contains("2"));
			Assert.IsFalse(str.Contains("123"));
			Assert.IsFalse(str.Contains("4"));
			Assert.IsFalse(str.Contains("56"));
			Assert.IsTrue(str.Contains("All"));
		}
	}
}

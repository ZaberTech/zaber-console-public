﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Zaber.FirmwareDownload;

namespace Zaber.FirmwareUnpacker.Tests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class BaseNCodecEncoderTests
	{
		[SetUp]
		public void Setup()
		{
			_expectedEncodeDefaultBase64 = new Dictionary<byte[], string>();
			_expectedEncodeDefaultBase64.Add(DATA1, "");
			_expectedEncodeDefaultBase64.Add(DATA2, "Zg==");
			_expectedEncodeDefaultBase64.Add(DATA3, "Zm8=");
			_expectedEncodeDefaultBase64.Add(DATA4, "Zm9v");
			_expectedEncodeDefaultBase64.Add(DATA5, "Zm9vYg==");
			_expectedEncodeDefaultBase64.Add(DATA6, "Zm9vYmE=");
			_expectedEncodeDefaultBase64.Add(DATA7, "Zm9vYmFy");
			_expectedEncodeDefaultBase64.Add(DATA8, "/+A=");

			_expectedEncodeUrlSafeBase64 = new Dictionary<byte[], string>();
			_expectedEncodeUrlSafeBase64.Add(DATA1, "");
			_expectedEncodeUrlSafeBase64.Add(DATA2, "Zg==");
			_expectedEncodeUrlSafeBase64.Add(DATA3, "Zm8=");
			_expectedEncodeUrlSafeBase64.Add(DATA4, "Zm9v");
			_expectedEncodeUrlSafeBase64.Add(DATA5, "Zm9vYg==");
			_expectedEncodeUrlSafeBase64.Add(DATA6, "Zm9vYmE=");
			_expectedEncodeUrlSafeBase64.Add(DATA7, "Zm9vYmFy");
			_expectedEncodeUrlSafeBase64.Add(DATA8, "_-A=");

			_expectedEncodeDefaultBase32 = new Dictionary<byte[], string>();
			_expectedEncodeDefaultBase32.Add(DATA1, "");
			_expectedEncodeDefaultBase32.Add(DATA2, "MY======");
			_expectedEncodeDefaultBase32.Add(DATA3, "MZXQ====");
			_expectedEncodeDefaultBase32.Add(DATA4, "MZXW6===");
			_expectedEncodeDefaultBase32.Add(DATA5, "MZXW6YQ=");
			_expectedEncodeDefaultBase32.Add(DATA6, "MZXW6YTB");
			_expectedEncodeDefaultBase32.Add(DATA7, "MZXW6YTBOI======");

			_expectedEncodeExtendedHexBase32 = new Dictionary<byte[], string>();
			_expectedEncodeExtendedHexBase32.Add(DATA1, "");
			_expectedEncodeExtendedHexBase32.Add(DATA2, "CO======");
			_expectedEncodeExtendedHexBase32.Add(DATA3, "CPNG====");
			_expectedEncodeExtendedHexBase32.Add(DATA4, "CPNMU===");
			_expectedEncodeExtendedHexBase32.Add(DATA5, "CPNMUOG=");
			_expectedEncodeExtendedHexBase32.Add(DATA6, "CPNMUOJ1");
			_expectedEncodeExtendedHexBase32.Add(DATA7, "CPNMUOJ1E8======");

			_expectedEncodeDefaultBase16 = new Dictionary<byte[], string>();
			_expectedEncodeDefaultBase16.Add(DATA1, "");
			_expectedEncodeDefaultBase16.Add(DATA2, "66");
			_expectedEncodeDefaultBase16.Add(DATA3, "666F");
			_expectedEncodeDefaultBase16.Add(DATA4, "666F6F");
			_expectedEncodeDefaultBase16.Add(DATA5, "666F6F62");
			_expectedEncodeDefaultBase16.Add(DATA6, "666F6F6261");
			_expectedEncodeDefaultBase16.Add(DATA7, "666F6F626172");
		}


		[Test]
		public void TestBadAlphabet()
			=> Assert.Throws<InvalidOperationException>(() => BaseNCodec.Encode(DATA8, 0, 2, "ABC"));


		[Test]
		public void TestBadRange1()
			=> Assert.Throws<ArgumentOutOfRangeException>(
				() => BaseNCodec.Encode(DATA8, -1, 2, BaseNCodec.BASE_16_STANDARD));


		[Test]
		public void TestBadRange2()
			=> Assert.Throws<ArgumentOutOfRangeException>(
				() => BaseNCodec.Encode(DATA8, 1, 2, BaseNCodec.BASE_16_STANDARD));


		[Test]
		public void TestBaseEncoder()
		{
			TestDictionaryEncodes(_expectedEncodeDefaultBase64, BaseNCodec.BASE_64_STANDARD);
			TestDictionaryEncodes(_expectedEncodeUrlSafeBase64, BaseNCodec.BASE_64_URLSAFE);
			TestDictionaryEncodes(_expectedEncodeDefaultBase32, BaseNCodec.BASE_32_STANDARD);
			TestDictionaryEncodes(_expectedEncodeExtendedHexBase32, BaseNCodec.BASE_32_EXTENDED_HEX);
			TestDictionaryEncodes(_expectedEncodeDefaultBase16, BaseNCodec.BASE_16_STANDARD);
		}


		[Test]
		public void TestNullAlphabet()
			=> Assert.Throws<ArgumentNullException>(() => BaseNCodec.Encode(DATA8, 0, 2, null));


		[Test]
		public void TestNullData()
			=> Assert.Throws<ArgumentNullException>(() => BaseNCodec.Encode(null, 0, 2, BaseNCodec.BASE_16_STANDARD));


		private void TestDictionaryEncodes(Dictionary<byte[], string> aTable, string aAlphabet)
		{
			foreach (var key in aTable.Keys)
			{
				var result = BaseNCodec.Encode(key, 0, key.Length, aAlphabet);
				var expected = aTable[key];
				Assert.AreEqual(expected, result, $"Encoder output is wrong for data: {key}");
			}
		}


		private static readonly byte[] DATA1 = new byte[0];
		private static readonly byte[] DATA2 = { 0x66 }; // "f"
		private static readonly byte[] DATA3 = { 0x66, 0x6F }; // "fo"
		private static readonly byte[] DATA4 = { 0x66, 0x6F, 0x6F }; // "foo"
		private static readonly byte[] DATA5 = { 0x66, 0x6F, 0x6F, 0x62 }; // "foob"
		private static readonly byte[] DATA6 = { 0x66, 0x6F, 0x6F, 0x62, 0x61 }; // "fooba"
		private static readonly byte[] DATA7 = { 0x66, 0x6F, 0x6F, 0x62, 0x61, 0x72 }; // "foobar"
		private static readonly byte[] DATA8 = { 0xFF, 0xE0 }; // Test for differences between Base64 alphabets.

		private Dictionary<byte[], string> _expectedEncodeDefaultBase64;
		private Dictionary<byte[], string> _expectedEncodeUrlSafeBase64;
		private Dictionary<byte[], string> _expectedEncodeDefaultBase32;
		private Dictionary<byte[], string> _expectedEncodeExtendedHexBase32;
		private Dictionary<byte[], string> _expectedEncodeDefaultBase16;
	}
}

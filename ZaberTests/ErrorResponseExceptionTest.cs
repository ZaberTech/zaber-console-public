﻿using NUnit.Framework;
using Zaber;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class ErrorResponseExceptionTest
	{
		[Test]
		public void Message()
		{
			// SETUP
			var deviceMessage = new DeviceMessage();
			deviceMessage.Command = Command.Error;
			deviceMessage.NumericData = (int) ZaberError.SettingInvalid;
			deviceMessage.DeviceNumber = 3;
			var ex = new ErrorResponseException(deviceMessage);

			// EXEC
			var msg = ex.Message;

			// VERIFY
			Assert.AreEqual("Error response SettingInvalid received from device number 3.",
							msg,
							"message");
		}


		[Test]
		public void UnknownErrorCode()
		{
			// SETUP
			var deviceMessage = new DeviceMessage();
			deviceMessage.Command = Command.Error;
			deviceMessage.NumericData = 7777;
			deviceMessage.DeviceNumber = 3;
			var ex = new ErrorResponseException(deviceMessage);

			// EXEC
			var msg = ex.Message;

			// VERIFY
			Assert.AreEqual("Error response 7777 received from device number 3.",
							msg,
							"message");
		}
	}
}

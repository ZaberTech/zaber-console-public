﻿using System;
using NUnit.Framework;
using Rhino.Mocks;
using Rhino.Mocks.Interfaces;
using TickingTest;
using Zaber;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class ConversationTest
	{
		[Test]
		public void AlertTimeout() => TestFramework.RunOnce(new AlertTimeoutThreads(this));


		[Test]
		public void CancelTopic() => TestFramework.RunOnce(new CancelTopicThreads(this));


		/// <summary>
		///     This is testing a scenario where the main thread makes a request
		///     and then a background thread makes a request that would replace the
		///     first one, but it gets an error response and leaves the first
		///     request running. Here's the timeline:
		///     0: main thread requests Home
		///     1: background thread requests MoveRelative
		///     2: response thread responds to MoveRelative with Error
		///     3: response thread responds to Home
		/// </summary>
		[Test]
		public void ErrorDoesntReplace() => TestFramework.RunOnce(new ErrorDoesntReplaceThreads(this));


		[Test]
		public void ErrorResponse() => TestFramework.RunOnce(new ErrorResponseThreads(this));


		[Test]
		public void GetAxes()
		{
			// SETUP
			var mocks = new MockRepository();
			var device = CreateDevice(17);
			var conversation = new Conversation(device);
			var axis1 = AddAxis(conversation, 1);
			var axis2 = AddAxis(conversation, 2);

			// EXEC
			var axes = conversation.Axes;

			// VERIFY
			Assert.That(axes[0],
						Is.SameAs(axis1),
						"axis 1");
			Assert.That(axes[1],
						Is.SameAs(axis2),
						"axis 2");
		}


		/// <summary>
		///     Check that it will just ignore any responses that come back when
		///     there are no active requests.
		/// </summary>
		[Test]
		public void IgnoreResponseWhenNotWaiting()
		{
			// SETUP
			const byte deviceNumber = 23;

			var responsePacket = new DataPacket
			{
				Command = Command.ReturnDeviceID,
				NumericData = AdjustData(301, 2), // DeviceId and msgId
				DeviceNumber = deviceNumber
			};

			// EXPECT
			var mocks = new MockRepository();
			var port = CreatePort(mocks);
			var device = CreateDevice(deviceNumber);

			var packetReceivedRaiser = CreateDataPacketReceivedRaiser(port);

			mocks.ReplayAll();

			// EXEC
			device.Port = port;
			var conversation = new Conversation(device);

			packetReceivedRaiser.Raise(port,
									   new DataPacketEventArgs(responsePacket));

			// VERIFY
			mocks.VerifyAll();
			Assert.IsFalse(conversation.IsWaiting,
						   "Conversation should not be waiting for anything");
		}


		/// <summary>
		///     Tests that once you loop around, you skip over any message ids that
		///     are still active.
		/// </summary>
		[Test]
		public void MessageIdCollision()
		{
			// SETUP
			const byte expectedDeviceNumber = 23;

			// EXPECT
			var mocks = new MockRepository();
			var port = CreatePort(mocks);
			var device = CreateDevice(expectedDeviceNumber);

			if (!device.AreMessageIdsEnabled)
			{
				// This test is only relevant when message ids are enabled.
				return;
			}

			mocks.ReplayAll();

			// EXEC
			device.Port = port;
			var conversation = new Conversation(device);

			var topic1 = conversation.StartTopic();

			//topic1.Cancel(); We don't cancel it, so it stays active and gets skipped.

			for (var i = 2; i <= 253; i++)
			{
				var topic = conversation.StartTopic();
				topic.Cancel();
			}

			var topic254 = conversation.StartTopic();
			topic254.Cancel();

			var topicLooped = conversation.StartTopic();
			topicLooped.Cancel();

			// now cancel topic1
			topic1.Cancel();

			// VERIFY
			mocks.VerifyAll();
			Assert.AreEqual(1,
							topic1.MessageId,
							"topic1 message id");
			Assert.AreEqual(254,
							topic254.MessageId,
							"topic254 message id");
			Assert.AreEqual(2,
							topicLooped.MessageId,
							"topicLooped message id");
			Assert.IsFalse(conversation.IsWaiting,
						   "Conversation should not be waiting for anything");
		}


		[Test]
		[Description("Verify that ASCII message IDs are automatically disabled when not supported.")]
		public void MessageIdsDisabledAutomatically()
		{
			// Sanity check that the test helper function enables message IDs.
			var device1 = CreateDeviceAscii(1, new FirmwareVersion(6, 28));
			Assert.IsTrue(device1.AreAsciiMessageIdsEnabled,
						  "Test configuration problem - message IDs should be enabled by default.");

			// Message ID mode should read as disabled for firmware versions that don't support it.
			var device2 = CreateDeviceAscii(1, new FirmwareVersion(6, 13));
			Assert.IsFalse(device2.AreAsciiMessageIdsEnabled);

			// Message ID mode should read as disabled for FW6 when bootloader mode is detected.
			var device3 = CreateDeviceAscii(1, new FirmwareVersion(6, 28));
			device3.IsInBootloaderMode = true;
			Assert.IsFalse(device3.AreAsciiMessageIdsEnabled);

			// Message ID mode should read as enabled for FW7, even if we see an NB flag.
			var device4 = CreateDeviceAscii(1, new FirmwareVersion(7, 0));
			Assert.IsTrue(device4.AreAsciiMessageIdsEnabled);
			device4.IsInBootloaderMode = true;
			Assert.IsTrue(device4.AreAsciiMessageIdsEnabled);
		}


		[Test]
		public void PollUntilIdle() => TestFramework.RunOnce(new PollUntilIdleThreads(this));


		[Test]
		public void PollUntilIdleAlert() => TestFramework.RunOnce(new PollUntilIdleAlertThreads(this));


		[Test]
		public void PollUntilIdleReplaced() => TestFramework.RunOnce(new PollUntilIdleReplacedThreads(this));


		[Test]
		public void PortErrorRaised() => TestFramework.RunOnce(new PortErrorRaisedThreads(this));


		/// <summary>
		///     This is testing a scenario where the main thread makes a request
		///     and then a background thread makes a request. The response to the
		///     first request is received just after the second response goes out.
		///     Here's the timeline:
		///     0: main thread requests MoveRelative
		///     1: background thread requests MoveAbsolute
		///     2: response thread responds to MoveRelative
		///     3: response thread responds to MoveAbsolute
		/// </summary>
		[Test]
		public virtual void ReceiveJustAfterSend() => TestFramework.RunOnce(new ReceiveJustAfterSendThreads(this));


		[Test]
		public void Request() => TestFramework.RunOnce(new RequestThreads(this));


		[Test]
		public void RequestUnitOfMeasure() => TestFramework.RunOnce(new RequestUnitOfMeasureThreads(this));


		/// <summary>
		///     Test that the conversation can retry after port errors occur.
		///     Here's the timeline:
		///     0: main thread requests MoveAbsolute with RetryCount of 1
		///     1: Port error occurs, conversation calls Port.DelayedSend(MoveAbsolute)
		///     2: Second port error occurs, conversation throws exception
		/// </summary>
		[Test]
		public virtual void RetryCountExceeded() => TestFramework.RunOnce(new RetryCountExceededThreads(this));


		/// <summary>
		///     Test that the conversation can retry after port errors occur.
		///     Here's the timeline:
		///     0: main thread requests MoveAbsolute
		///     1: Port error occurs, conversation calls Port.DelayedSend(MoveAbsolute)
		///     2: Response received
		/// </summary>
		[Test]
		public virtual void RetryPortError() => TestFramework.RunOnce(new RetryPortErrorThreads(this));


		/// <summary>
		///     Test that the conversation can retry after two port errors occur.
		///     Here's the timeline:
		///     0: main thread requests MoveAbsolute
		///     1: Port error occurs, conversation calls Port.DelayedSend(MoveAbsolute)
		///     2: Another port error occurs, first retry is canceled, a new one is sent.
		///     3: Response received
		/// </summary>
		[Test]
		public virtual void RetryTwice() => TestFramework.RunOnce(new RetryTwiceThreads(this));


		/// <summary>
		///     Test that the conversation won't retry if the command is unsafe.
		///     Here's the timeline:
		///     0: main thread requests MoveAbsolute
		///     1: Port error occurs, conversation throws
		/// </summary>
		[Test]
		public virtual void RetryUnsafe() => TestFramework.RunOnce(new RetryUnsafeThreads(this));


		/// <summary>
		///     This is to test that the message id rolls over from 254 back to 1.
		///     (0 and 255 are special ids that aren't used.)
		/// </summary>
		[Test]
		public void Rollover()
		{
			// SETUP
			const byte expectedDeviceNumber = 23;

			// EXPECT
			var mocks = new MockRepository();
			var port = CreatePort(mocks);
			var device = CreateDevice(expectedDeviceNumber);

			if (!device.AreMessageIdsEnabled)
			{
				// This test is only relevant when message ids are enabled.
				return;
			}

			mocks.ReplayAll();

			// EXEC
			device.Port = port;
			var conversation = new Conversation(device);

			var topic1 = conversation.StartTopic();
			topic1.Cancel();

			for (var i = 2; i <= 253; i++)
			{
				var topic = conversation.StartTopic();
				topic.Cancel();
			}

			var topic254 = conversation.StartTopic();
			topic254.Cancel();

			var topicLooped = conversation.StartTopic();
			topicLooped.Cancel();

			// VERIFY
			mocks.VerifyAll();
			Assert.AreEqual(1,
							topic1.MessageId,
							"topic1 message id");
			Assert.AreEqual(254,
							topic254.MessageId,
							"topic254 message id");
			Assert.AreEqual(1,
							topicLooped.MessageId,
							"topicLooped message id");
			Assert.IsFalse(conversation.IsWaiting,
						   "Conversation should not be waiting for anything");
		}


		/// <summary>
		///     This is testing a scenario where the main thread makes a request
		///     and then a background thread makes a request and receives a response
		///     while the first request is still executing. Here's the timeline:
		///     0: main thread requests MoveRelative
		///     1: background thread requests ReturnDeviceId
		///     2: response thread responds to ReturnDeviceId
		///     3: response thread responds to MoveRelative
		/// </summary>
		[Test]
		public void SecondRequest() => TestFramework.RunOnce(new SecondRequestThreads(this));


		/// <summary>
		///     This is testing a scenario where the main thread makes a request
		///     and then a background thread makes a request that replaces the
		///     first one. Here's the timeline:
		///     0: main thread requests MoveRelative
		///     1: background thread requests Stop
		///     2: response thread responds to Stop
		/// </summary>
		[Test]
		public void SecondRequestReplaces() => TestFramework.RunOnce(new SecondRequestReplacesThreads(this));


		[Test]
		public void StartTopic() => TestFramework.RunOnce(new StartTopicThreads(this));


		[Test]
		public void StartTopicErrorResponse() => TestFramework.RunOnce(new StartTopicErrorResponseThreads(this));


		[Test]
		public void StartTopicTimeout() => TestFramework.RunOnce(new StartTopicTimeoutThreads(this));


		[Test]
		public void TextModeDataRequest() => TestFramework.RunOnce(new TextModeDataRequestThreads(this));


		[Test]
		public void TextModeError() => TestFramework.RunOnce(new TextModeErrorThreads(this));


		[Test]
		public void TextModeFault() => TestFramework.RunOnce(new TextModeFaultThreads(this));


		[Test]
		public void TextModeIgnoreAlert() => TestFramework.RunOnce(new TextModeIgnoreAlertThreads(this));


		[Test]
		public void TextModeIgnoreInfo() => TestFramework.RunOnce(new TextModeIgnoreInfoThreads(this));


		[Test]
		public void TextModeIgnoreResponse() => TestFramework.RunOnce(new TextModeIgnoreResponseThreads(this));


		[Test]
		public void TextModeRequest() => TestFramework.RunOnce(new TextModeRequestThreads(this));


		[Test]
		public void Timeout() => TestFramework.RunOnce(new TimeoutThreads(this));


		[Test]
		public void TimeoutProperty()
		{
			// SETUP
			var mocks = new MockRepository();
			var device = CreateDevice(17);
			var conversation = new Conversation(device);
			const int expectedTimeout = 5000;

			// EXEC
			conversation.Timeout = expectedTimeout;
			var actualTimeout = conversation.Timeout;

			// VERIFY
			Assert.That(actualTimeout,
						Is.EqualTo(expectedTimeout));
		}


		[Test]
		public void TopicWillTimeoutEventually()
		{
			// SETUP
			const byte expectedDeviceNumber = 23;
			const byte messageId = 27;

			var mocks = new MockRepository();

			var port = CreatePort(mocks);
			port.IsAsciiMode = true;
			var packetSent = CreateDataPacketSentRaiser(port);

			var device = CreateDeviceAscii(expectedDeviceNumber);
			Assert.IsTrue(device.AreAsciiMessageIdsEnabled);

			// EXPECT
			port.Send(device.DeviceNumber,
					  Command.MoveRelative,
					  1000,
					  new Measurement(1, UnitOfMeasure.Millimeter));

			mocks.ReplayAll();

			// EXEC
			device.Port = port;

			var conversation = new Conversation(device);
			conversation.Timeout = 1;

			var topic1 = conversation.StartTopic(messageId);

			device.Send(Command.MoveRelative, 1000, topic1.MessageId);
			packetSent.Raise(port,
							 new DataPacketEventArgs(new DataPacket
							 {
								 MessageId = messageId,
								 DeviceNumber = expectedDeviceNumber
							 }));

			topic1.Wait();

			Assert.IsTrue(topic1.IsComplete,
						  "is complete");
			Assert.AreEqual(ZaberPortError.ResponseTimeout,
							topic1.ZaberPortError,
							"ZaberPortError is ResponseTimeout");

			// VERIFY
			mocks.VerifyAll();
		}


		/// <summary>
		///     A response is received to an unrelated command and that command
		///     doesn't replace the request we were waiting for. The original
		///     request should continue to wait
		/// </summary>
		[Test]
		public virtual void UnexpectedResponseDoesntReplace()
			=> TestFramework.RunOnce(new UnexpectedResponseDoesntReplaceThreads(this));


		/// <summary>
		///     A response is received to an unrelated command and that command
		///     has replaced the request we were waiting for. A
		///     RequestReplacedException should be thrown.
		/// </summary>
		[Test]
		public virtual void UnexpectedResponseReplaces()
			=> TestFramework.RunOnce(new UnexpectedResponseReplacesThreads(this));


		[Test]
		public void WaitForAlert() => TestFramework.RunOnce(new WaitForAlertThreads(this));


		public virtual int AdjustData(int data, byte messageId) => (data & 0xFFFFFF) | (messageId << 24);


		private class RequestThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public RequestThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// EXPECT
				Port.Send(Device.DeviceNumber,
						  Command.MoveRelative,
						  MainTest.AdjustData(1000, 1),
						  new Measurement(1, UnitOfMeasure.Millimeter));

				FinishRecording();

				// EXEC
				var response =
					Conversation.Request(Command.MoveRelative, 1000);
				AssertTick(1);

				// VERIFY
				Mocks.VerifyAll();

				Assert.AreEqual(12345,
								response.NumericData,
								"Response data should match");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				var responsePacket = new DataPacket
				{
					Command = Command.MoveRelative,
					NumericData = MainTest.AdjustData(12345, 1), // position and msgId
					DeviceNumber = Device.DeviceNumber
				};

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket));
			}

			#endregion
		}

		private class RequestUnitOfMeasureThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public RequestUnitOfMeasureThreads(ConversationTest mainTest)
				:
				base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// SETUP
				Device.DeviceType.MotionType = MotionType.Linear;
				var requestedMeasurement =
					new Measurement(1000, UnitOfMeasure.Micrometer);
				var expectedSentMeasurement =
					new Measurement(1, UnitOfMeasure.Millimeter);
				var expectedReceivedMeasurement =
					new Measurement(200, UnitOfMeasure.Micrometer);

				// EXPECT
				Port.Send(Device.DeviceNumber,
						  Command.MoveRelative,
						  MainTest.AdjustData(1000, 1),
						  expectedSentMeasurement);

				FinishRecording();

				// EXEC
				var response = Conversation.RequestInUnits(Command.MoveRelative,
														   requestedMeasurement);
				AssertTick(1);

				// VERIFY
				Mocks.VerifyAll();

				Assert.AreEqual(expectedReceivedMeasurement,
								response.Measurement,
								"Response measurement should match");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				var responsePacket = new DataPacket
				{
					Command = Command.MoveRelative,
					NumericData = MainTest.AdjustData(200, 1), // position and msgId
					DeviceNumber = Device.DeviceNumber
				};

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket));
			}

			#endregion
		}

		private class StartTopicThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public StartTopicThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// EXPECT
				Port.Send(Device.DeviceNumber,
						  Command.MoveRelative,
						  MainTest.AdjustData(1000, MessageId),
						  new Measurement(1, UnitOfMeasure.Millimeter));

				FinishRecording();

				// EXEC
				var topic =
					Conversation.StartTopic(MessageId);

				Device.Send(Command.MoveRelative, 1000, MessageId);

				// After that, we wait for the response.
				topic.Wait();
				AssertTick(1);

				// VERIFY
				Mocks.VerifyAll();

				Assert.AreEqual(12345,
								topic.Response.NumericData,
								"Response data should match");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				var responsePacket = new DataPacket
				{
					Command = Command.MoveRelative,
					NumericData = MainTest.AdjustData(12345, MessageId), // position and msgId
					DeviceNumber = Device.DeviceNumber
				};

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket));
			}

			#endregion

			#region -- Data --

			private const byte MessageId = 27;

			#endregion
		}

		private class CancelTopicThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public CancelTopicThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// EXPECT
				Port.Send(Device.DeviceNumber,
						  Command.MoveRelative,
						  MainTest.AdjustData(1000, MessageId),
						  new Measurement(1, UnitOfMeasure.Millimeter));

				FinishRecording();

				// EXEC
				_topic = Conversation.StartTopic(MessageId);

				Device.Send(Command.MoveRelative, 1000, MessageId);

				// After that, we wait for the response.
				_topic.Wait();

				try
				{
					_topic.Validate();
					Assert.Fail("Should have thrown.");
				}
				catch (RequestCanceledException)
				{
				}

				AssertTick(1);

				// VERIFY
				Mocks.VerifyAll();

				Assert.AreEqual(null,
								_topic.Response,
								"Response should match");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Cancel()
			{
				WaitForTick(1);

				_topic.Cancel();
			}

			#endregion

			#region -- Data --

			private const byte MessageId = 27;
			private ConversationTopic _topic;

			#endregion
		}

		private class StartTopicErrorResponseThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public StartTopicErrorResponseThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// EXPECT
				Port.Send(Device.DeviceNumber,
						  Command.MoveRelative,
						  MainTest.AdjustData(1000, MessageId),
						  new Measurement(1, UnitOfMeasure.Millimeter));

				FinishRecording();

				// EXEC
				var topic =
					Conversation.StartTopic(MessageId);

				Device.Send(Command.MoveRelative, 1000, MessageId);

				// After that, we wait for the response.
				topic.Wait();
				AssertTick(1);

				// VERIFY
				Mocks.VerifyAll();

				Assert.AreEqual(ErrorCode,
								topic.Response.NumericData,
								"Response data should match");
				Assert.AreEqual(false,
								topic.IsValid,
								"is valid");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				var responsePacket = new DataPacket
				{
					Command = Command.Error,
					NumericData = MainTest.AdjustData(ErrorCode, MessageId),
					DeviceNumber = Device.DeviceNumber
				};

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket));
			}

			#endregion

			#region -- Data --

			private const byte ErrorCode = 255;
			private const byte MessageId = 27;

			#endregion
		}

		private class StartTopicTimeoutThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public StartTopicTimeoutThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// SETUP
				const byte messageId = 32;
				_mockTimer2 = new MockTimeoutTimer();

				// EXPECT
				Port.Send(Device.DeviceNumber,
						  Command.MoveRelative,
						  MainTest.AdjustData(1000, messageId),
						  new Measurement(1, UnitOfMeasure.Millimeter));

				FinishRecording();

				// EXEC
				var topic =
					Conversation.StartTopic(messageId);

				Device.Send(Command.MoveRelative, 1000, messageId);

				// After that, we wait for the response.
				var isComplete = topic.Wait(_mockTimer2);
				AssertTick(1);

				// VERIFY
				Mocks.VerifyAll();

				Assert.AreEqual(false,
								isComplete,
								"is complete");
				Assert.IsTrue(Conversation.IsWaiting,
							  "Conversation should still be waiting after timeout");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				_mockTimer2.TimeoutNow();
			}

			#endregion

			#region -- Data --

			private MockTimeoutTimer _mockTimer2;

			#endregion
		}

		private class TimeoutThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public TimeoutThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// EXPECT
				Port.Send(Device.DeviceNumber,
						  Command.MoveRelative,
						  MainTest.AdjustData(1000, 1),
						  new Measurement(1, UnitOfMeasure.Millimeter));

				FinishRecording();

				// EXEC
				string msg = null;
				try
				{
					Conversation.Request(Command.MoveRelative, 1000);

					Assert.Fail("Should have thrown.");
				}
				catch (RequestTimeoutException ex)
				{
					msg = ex.Message;
				}

				AssertTick(1);

				// VERIFY
				Mocks.VerifyAll();

				Assert.AreEqual("Request timed out.",
								msg,
								"error message");
				Assert.IsTrue(Conversation.IsWaiting,
							  "Conversation should still be waiting after a timeout");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				MockTimer.TimeoutNow();
			}

			#endregion
		}

		private class SecondRequestThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public SecondRequestThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void FirstRequest()
			{
				// EXPECT
				Port.Send(Device.DeviceNumber,
						  Command.MoveRelative,
						  MainTest.AdjustData(1000, 1),
						  new Measurement(1, UnitOfMeasure.Millimeter));
				Port.Send(Device.DeviceNumber,
						  Command.ReturnDeviceID,
						  MainTest.AdjustData(0, 2),
						  new Measurement(0, UnitOfMeasure.Data));

				FinishRecording();

				// EXEC
				var response =
					Conversation.Request(Command.MoveRelative, 1000);

				// VERIFY
				AssertTick(3);
				Mocks.VerifyAll();

				Assert.AreEqual(12345,
								response.NumericData,
								"Response data should match");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void SecondRequest()
			{
				WaitForTick(1);
				var secondResponse =
					Conversation.Request(Command.ReturnDeviceID);

				// VERIFY
				AssertTick(2);
				Assert.AreEqual(301,
								secondResponse.NumericData,
								"Second response data should match");
			}


			[TestThread]
			public void Responses()
			{
				WaitForTick(2);

				var responsePacket1 = new DataPacket
				{
					Command = Command.ReturnDeviceID,
					NumericData = MainTest.AdjustData(301, 2), // DeviceId and msgId
					DeviceNumber = Device.DeviceNumber
				};

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket1));

				WaitForTick(3);

				var responsePacket2 = new DataPacket
				{
					Command = Command.MoveRelative,
					NumericData = MainTest.AdjustData(12345, 1), // position and msgId
					DeviceNumber = Device.DeviceNumber
				};

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket2));
			}

			#endregion
		}

		private class UnexpectedResponseReplacesThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public UnexpectedResponseReplacesThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// EXPECT
				Port.Send(Device.DeviceNumber,
						  Command.MoveRelative,
						  MainTest.AdjustData(1000, 1),
						  new Measurement(1, UnitOfMeasure.Millimeter));

				FinishRecording();

				// EXEC
				string msg = null;
				DeviceMessage response = null;
				try
				{
					Conversation.Request(Command.MoveRelative, 1000);

					Assert.Fail("Should have thrown.");
				}
				catch (RequestReplacedException ex)
				{
					msg = ex.Message;
					response = ex.ReplacementResponse;
				}

				// VERIFY
				AssertTick(1);
				Mocks.VerifyAll();

				Assert.AreEqual("Current request was replaced by a new request.",
								msg,
								"Exception message should match");
				Assert.AreEqual(Command.Stop,
								response.Command,
								"Response command should match");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				var stopCommandResponsePacket = new DataPacket
				{
					Command = Command.Stop,
					NumericData = 0,
					DeviceNumber = Device.DeviceNumber
				};

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(stopCommandResponsePacket));
			}

			#endregion
		}

		private class UnexpectedResponseDoesntReplaceThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public UnexpectedResponseDoesntReplaceThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// EXPECT
				Port.Send(Device.DeviceNumber,
						  Command.MoveRelative,
						  MainTest.AdjustData(1000, 1),
						  new Measurement(1, UnitOfMeasure.Millimeter));

				FinishRecording();

				// EXEC
				var response =
					Conversation.Request(Command.MoveRelative, 1000);

				// VERIFY
				AssertTick(2);
				Mocks.VerifyAll();

				Assert.AreEqual(Command.MoveRelative,
								response.Command,
								"Response data should match");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				var responsePacket = new DataPacket
				{
					Command = Command.ReturnDeviceID,
					NumericData = 131,
					DeviceNumber = Device.DeviceNumber
				};

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket));

				WaitForTick(2);

				responsePacket.Command = Command.MoveRelative;
				responsePacket.NumericData = MainTest.AdjustData(12345, 1);

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket));
			}

			#endregion
		}

		private class SecondRequestReplacesThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public SecondRequestReplacesThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void FirstRequest()
			{
				// EXPECT
				Port.Send(Device.DeviceNumber,
						  Command.MoveRelative,
						  MainTest.AdjustData(1000, 1),
						  new Measurement(1, UnitOfMeasure.Millimeter));
				Port.Send(Device.DeviceNumber,
						  Command.Stop,
						  MainTest.AdjustData(0, 2),
						  new Measurement(0, UnitOfMeasure.Data));

				FinishRecording();

				// EXEC
				string msg = null;
				DeviceMessage response = null;
				try
				{
					Conversation.Request(Command.MoveRelative, 1000);

					Assert.Fail("Should have thrown.");
				}
				catch (RequestReplacedException ex)
				{
					msg = ex.Message;
					response = ex.ReplacementResponse;
				}

				AssertTick(2);
				WaitForTick(3); // lets second request record its response

				// VERIFY
				Mocks.VerifyAll();

				Assert.AreEqual("Current request was replaced by a new request.",
								msg,
								"Exception message should match");
				Assert.AreSame(response,
							   _secondResponse,
							   "Both requests should get the same response");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void SecondRequest()
			{
				WaitForTick(1);

				_secondResponse = Conversation.Request(Command.Stop);

				// VERIFY
				AssertTick(2);
				Assert.AreEqual(12345,
								_secondResponse.NumericData,
								"Second response data should match");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(2);

				var responsePacket = new DataPacket
				{
					Command = Command.Stop,
					NumericData = MainTest.AdjustData(12345, 2), // Position and msgId
					DeviceNumber = Device.DeviceNumber
				};
				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket));
			}

			#endregion

			#region -- Data --

			private DeviceMessage _secondResponse;

			#endregion
		}

		private class ErrorResponseThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public ErrorResponseThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// EXPECT
				Port.Send(Device.DeviceNumber,
						  Command.MoveRelative,
						  MainTest.AdjustData(1000, 1),
						  new Measurement(1, UnitOfMeasure.Millimeter));

				FinishRecording();

				// EXEC
				string msg = null;
				DeviceMessage response = null;
				try
				{
					Conversation.Request(Command.MoveRelative, 1000);

					Assert.Fail("Should have thrown.");
				}
				catch (ErrorResponseException ex)
				{
					msg = ex.Message;
					response = ex.Response;
				}

				AssertTick(1);

				// VERIFY
				Mocks.VerifyAll();

				Assert.AreEqual((int) ZaberError.Busy,
								response.NumericData,
								"Response data should match");
				Assert.AreEqual("Error response Busy received from device number 17.",
								msg,
								"Message should match");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				var responsePacket = new DataPacket
				{
					Command = Command.Error,
					NumericData = MainTest.AdjustData((int) ZaberError.Busy, 1), // position and msgId
					DeviceNumber = Device.DeviceNumber
				};
				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket));
			}

			#endregion
		}

		private class ErrorDoesntReplaceThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public ErrorDoesntReplaceThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void FirstRequest()
			{
				// EXPECT
				Port.Send(Device.DeviceNumber,
						  Command.Home,
						  MainTest.AdjustData(0, 1),
						  new Measurement(0, UnitOfMeasure.Data));
				Port.Send(Device.DeviceNumber,
						  Command.MoveRelative,
						  MainTest.AdjustData(1000, 2),
						  new Measurement(1, UnitOfMeasure.Millimeter));

				FinishRecording();

				// EXEC
				var response =
					Conversation.Request(Command.Home);

				// VERIFY
				AssertTick(3);
				Mocks.VerifyAll();

				Assert.AreEqual(0,
								response.NumericData,
								"Response data should match");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void SecondRequest()
			{
				WaitForTick(1);

				DeviceMessage secondResponse = null;
				try
				{
					Conversation.Request(Command.MoveRelative, 1000);

					Assert.Fail("Should have thrown from second request.");
				}
				catch (ErrorResponseException ex)
				{
					secondResponse = ex.Response;
				}

				// VERIFY
				AssertTick(2);
				Assert.AreEqual(255,
								secondResponse.NumericData,
								"Second response data should match");
			}


			[TestThread]
			public void Responses()
			{
				WaitForTick(2);

				var responsePacket1 = new DataPacket
				{
					Command = Command.Error,
					NumericData = MainTest.AdjustData(255, 2), // Error code and msgId
					DeviceNumber = Device.DeviceNumber
				};

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket1));

				WaitForTick(3);

				var responsePacket2 = new DataPacket
				{
					Command = Command.Home,
					NumericData = MainTest.AdjustData(0, 1), // position and msgId
					DeviceNumber = Device.DeviceNumber
				};

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket2));
			}

			#endregion
		}

		private delegate void SendDelegate(byte deviceNumber, Command command, int data);

		private class PortErrorRaisedThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public PortErrorRaisedThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// EXPECT
				Port.Send(Device.DeviceNumber,
						  Command.MoveRelative,
						  MainTest.AdjustData(1000, 1),
						  new Measurement(1, UnitOfMeasure.Millimeter));

				FinishRecording();

				// EXEC
				var errorType = ZaberPortError.None;
				try
				{
					Conversation.Request(Command.MoveRelative, 1000);

					Assert.Fail("Should have thrown");
				}
				catch (ZaberPortErrorException ex)
				{
					errorType = ex.ErrorType;
				}

				AssertTick(1);

				// VERIFY
				Mocks.VerifyAll();

				Assert.AreEqual(ZaberPortError.PacketTimeout,
								errorType,
								"Error type should match");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				ErrorReceivedRaiser.Raise(Port,
										  new ZaberPortErrorReceivedEventArgs(ZaberPortError.PacketTimeout));
			}

			#endregion
		}

		private class ReceiveJustAfterSendThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public ReceiveJustAfterSendThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void FirstRequest()
			{
				// EXPECT
				Port.Send(Device.DeviceNumber,
						  Command.MoveRelative,
						  MainTest.AdjustData(1000, 1),
						  new Measurement(1, UnitOfMeasure.Millimeter));
				Port.Send(Device.DeviceNumber,
						  Command.MoveAbsolute,
						  MainTest.AdjustData(54321, 2),
						  new Measurement(54.321, UnitOfMeasure.Millimeter));

				FinishRecording();

				// EXEC
				var response =
					Conversation.Request(Command.MoveRelative, 1000);

				// VERIFY
				AssertTick(2);
				Mocks.VerifyAll();

				Assert.AreEqual(Command.MoveRelative,
								response.Command,
								"Response command should match");
				Assert.IsTrue(Conversation.IsWaiting,
							  "Conversation should still be waiting for second response");
			}


			[TestThread]
			public void SecondRequest()
			{
				WaitForTick(1);
				var secondResponse =
					Conversation.Request(Command.MoveAbsolute, 54321);

				// VERIFY
				AssertTick(3);
				Assert.AreEqual(Command.MoveAbsolute,
								secondResponse.Command,
								"Second response command should match");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Responses()
			{
				WaitForTick(2);

				var responsePacket1 = new DataPacket
				{
					Command = Command.MoveRelative,
					NumericData = MainTest.AdjustData(12345, 1), // position and msgId
					DeviceNumber = Device.DeviceNumber
				};

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket1));

				WaitForTick(3);

				var responsePacket2 = new DataPacket
				{
					Command = Command.MoveAbsolute,
					NumericData = MainTest.AdjustData(54321, 2), // position and msgId
					DeviceNumber = Device.DeviceNumber
				};

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket2));
			}

			#endregion
		}

		private class RetryPortErrorThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public RetryPortErrorThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// SETUP
				var retryPacket = new DataPacket();

				// EXPECT
				Port.Send(Device.DeviceNumber,
						  Command.MoveAbsolute,
						  MainTest.AdjustData(1000, 1),
						  new Measurement(1, UnitOfMeasure.Millimeter));

				// expect a retry with same msg id
				Expect.Call(Port.SendDelayed(Device.DeviceNumber,
											 Command.MoveAbsolute,
											 MainTest.AdjustData(1000, 1)))
				   .Return(retryPacket);
				Expect.Call(Port.CancelDelayedPacket(retryPacket)).Return(false);

				FinishRecording();

				// EXEC
				Conversation.RetryCount = 3;
				var response =
					Conversation.Request(Command.MoveAbsolute, 1000);

				// VERIFY
				AssertTick(2);
				Mocks.VerifyAll();

				Assert.AreEqual(Command.MoveAbsolute,
								response.Command,
								"Response command should match");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				ErrorReceivedRaiser.Raise(Port,
										  new ZaberPortErrorReceivedEventArgs(ZaberPortError.Frame));

				WaitForTick(2);

				var responsePacket1 = new DataPacket
				{
					Command = Command.MoveAbsolute,
					NumericData = MainTest.AdjustData(1000, 1), // position and msgId
					DeviceNumber = Device.DeviceNumber
				};

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket1));
			}

			#endregion
		}

		private class RetryTwiceThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public RetryTwiceThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// SETUP
				var retryPacket1 = new DataPacket();
				var retryPacket2 = new DataPacket();

				// EXPECT
				Port.Send(Device.DeviceNumber,
						  Command.MoveAbsolute,
						  MainTest.AdjustData(1000, 1),
						  new Measurement(1, UnitOfMeasure.Millimeter));

				// expect a retry with same msg id
				Expect.Call(Port.SendDelayed(Device.DeviceNumber,
											 Command.MoveAbsolute,
											 MainTest.AdjustData(1000, 1)))
				   .Return(retryPacket1);
				Expect.Call(Port.CancelDelayedPacket(retryPacket1)).Return(true);
				Expect.Call(Port.SendDelayed(Device.DeviceNumber,
											 Command.MoveAbsolute,
											 MainTest.AdjustData(1000, 1)))
				   .Return(retryPacket2);
				Expect.Call(Port.CancelDelayedPacket(retryPacket2)).Return(false);

				FinishRecording();

				// EXEC
				Conversation.RetryCount = 3;
				var response =
					Conversation.Request(Command.MoveAbsolute, 1000);

				// VERIFY
				AssertTick(3);
				Mocks.VerifyAll();

				Assert.AreEqual(Command.MoveAbsolute,
								response.Command,
								"Response command should match");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				ErrorReceivedRaiser.Raise(Port,
										  new ZaberPortErrorReceivedEventArgs(ZaberPortError.Frame));

				WaitForTick(2);

				ErrorReceivedRaiser.Raise(Port,
										  new ZaberPortErrorReceivedEventArgs(ZaberPortError.Frame));

				WaitForTick(3);

				var responsePacket1 = new DataPacket
				{
					Command = Command.MoveAbsolute,
					NumericData = MainTest.AdjustData(1000, 1), // position and msgId
					DeviceNumber = Device.DeviceNumber
				};

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket1));
			}

			#endregion
		}

		private class RetryUnsafeThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public RetryUnsafeThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// EXPECT
				Port.Send(Device.DeviceNumber,
						  Command.MoveRelative,
						  MainTest.AdjustData(1000, 1),
						  new Measurement(1, UnitOfMeasure.Millimeter));

				FinishRecording();

				// EXEC
				Conversation.RetryCount = 3;
				string msg = null;
				try
				{
					Conversation.Request(Command.MoveRelative, 1000);
					Assert.Fail("should have thrown.");
				}
				catch (ZaberPortErrorException ex)
				{
					msg = ex.Message;
				}

				// VERIFY
				AssertTick(1);
				Mocks.VerifyAll();

				Assert.That(msg,
							Is.EqualTo("Port error: Frame"),
							"message");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				ErrorReceivedRaiser.Raise(Port,
										  new ZaberPortErrorReceivedEventArgs(ZaberPortError.Frame));
			}

			#endregion
		}

		private class RetryCountExceededThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public RetryCountExceededThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// EXPECT
				Port.Send(Device.DeviceNumber,
						  Command.MoveAbsolute,
						  MainTest.AdjustData(1000, 1),
						  new Measurement(1, UnitOfMeasure.Millimeter));

				// expect a retry with same msg id
				Port.SendDelayed(Device.DeviceNumber,
								 Command.MoveAbsolute,
								 MainTest.AdjustData(1000, 1));

				FinishRecording();

				// EXEC
				Conversation.RetryCount = 1;
				string msg = null;
				try
				{
					Conversation.Request(Command.MoveAbsolute, 1000);
				}
				catch (ZaberPortErrorException ex)
				{
					msg = ex.Message;
				}

				// VERIFY
				AssertTick(2);
				Mocks.VerifyAll();

				Assert.That(msg,
							Is.EqualTo("Port error: Frame"),
							"message");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				ErrorReceivedRaiser.Raise(Port,
										  new ZaberPortErrorReceivedEventArgs(ZaberPortError.Frame));

				WaitForTick(2);

				ErrorReceivedRaiser.Raise(Port,
										  new ZaberPortErrorReceivedEventArgs(ZaberPortError.Frame));
			}

			#endregion
		}

		private class TextModeRequestThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public TextModeRequestThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// SETUP
				Device.DeviceNumber = 17;

				// EXPECT
				Port.Send("17 0 move rel 1000", null);

				FinishRecording();

				// EXEC
				var response =
					Conversation.Request("move rel 1000");

				// VERIFY
				AssertTick(1);
				Mocks.VerifyAll();

				Assert.That(response.Text,
							Is.EqualTo(ExpectedResponse),
							"response text");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				var responsePacket = new DataPacket(ExpectedResponse);

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket));
			}

			#endregion

			#region -- Data --

			private const string ExpectedResponse = "@17 0 OK IDLE -- 0";

			#endregion
		}

		private class TextModeIgnoreAlertThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public TextModeIgnoreAlertThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// SETUP
				Device.DeviceNumber = 17;

				// EXPECT
				Port.Send("17 0 move rel 1000", null);

				FinishRecording();

				// EXEC
				var response =
					Conversation.Request("move rel 1000");

				// VERIFY
				AssertTick(2);
				Mocks.VerifyAll();

				Assert.That(response.Text,
							Is.EqualTo(ExpectedResponse),
							"response text");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				var alertPacket = new DataPacket("!17 1 IDLE --");

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(alertPacket));

				WaitForTick(2);

				var responsePacket = new DataPacket(ExpectedResponse);

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket));
			}

			#endregion

			#region -- Data --

			private const string ExpectedResponse = "@17 0 OK IDLE -- 0";

			#endregion
		}

		private class TextModeIgnoreInfoThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public TextModeIgnoreInfoThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// SETUP
				Device.DeviceNumber = 17;

				// EXPECT
				Port.Send("17 0 move rel 1000", null);

				FinishRecording();

				// EXEC
				var response =
					Conversation.Request("move rel 1000");

				// VERIFY
				AssertTick(2);
				Mocks.VerifyAll();

				Assert.That(response.Text,
							Is.EqualTo(ExpectedResponse),
							"response text");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				var infoPacket = new DataPacket("#17 0 some info message");

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(infoPacket));

				WaitForTick(2);

				var responsePacket = new DataPacket(ExpectedResponse);

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket));
			}

			#endregion

			#region -- Data --

			private const string ExpectedResponse = "@17 0 OK IDLE -- 0";

			#endregion
		}

		private class TextModeIgnoreResponseThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public TextModeIgnoreResponseThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// SETUP
				Device.DeviceNumber = 17;
				Device.AxisNumber = 1;

				// EXPECT
				FinishRecording();

				// EXEC
				var response = Conversation.WaitForAlert();

				// VERIFY
				AssertTick(2);
				Mocks.VerifyAll();

				Assert.That(response.Text,
							Is.EqualTo(ExpectedAlert),
							"response text");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				var responsePacket = new DataPacket("@17 1 OK IDLE -- 0");

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket));

				WaitForTick(2);

				var alertPacket = new DataPacket(ExpectedAlert);

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(alertPacket));
			}

			#endregion

			#region -- Data --

			private const string ExpectedAlert = "!17 1 IDLE --";

			#endregion
		}

		private class TextModeDataRequestThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public TextModeDataRequestThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// SETUP
				Device.DeviceNumber = 17;

				// EXPECT
				Port.Send("17 0 move rel 1000",
						  new Measurement(1000, UnitOfMeasure.Data));

				FinishRecording();

				// EXEC
				var response =
					Conversation.Request("move rel", 1000m);

				// VERIFY
				AssertTick(1);
				Mocks.VerifyAll();

				Assert.That(response.Text,
							Is.EqualTo(ExpectedResponse),
							"response text");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				var responsePacket = new DataPacket(ExpectedResponse);

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket));
			}

			#endregion

			#region -- Data --

			private const string ExpectedResponse = "@17 0 OK IDLE -- 0";

			#endregion
		}

		private class TextModeFaultThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public TextModeFaultThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// SETUP
				Device.DeviceNumber = 17;

				// EXPECT
				Port.Send("17 0 ", null);

				FinishRecording();

				// EXEC
				var ex = Assert.Throws<DeviceFaultException>(() => Conversation.PollUntilIdle());

				// VERIFY
				AssertTick(1);
				Mocks.VerifyAll();

				Assert.That(ex.Message.Contains("Fault FX reported by device"),
							"error message");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				var responsePacket = new DataPacket("@17 0 OK IDLE FX 0");

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket));
			}

			#endregion
		}

		private class TextModeErrorThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public TextModeErrorThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// SETUP
				Device.DeviceNumber = 17;

				// EXPECT
				Port.Send("17 0 move rel 1000", null);

				FinishRecording();

				// EXEC
				var ex = Assert.Throws<ErrorResponseException>(() => Conversation.Request("move rel 1000"));

				// VERIFY
				AssertTick(1);
				Mocks.VerifyAll();

				Assert.That(ex.Message,
							Is.EqualTo("Error response: '@17 0 RJ IDLE -- 0'."),
							"error message");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				var responsePacket = new DataPacket("@17 0 RJ IDLE -- 0");

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket));
			}

			#endregion
		}

		private class WaitForAlertThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public WaitForAlertThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// SETUP
				Device.DeviceNumber = 17;
				Device.AxisNumber = 1;

				// EXPECT
				FinishRecording();

				// EXEC
				var response =
					Conversation.WaitForAlert();

				// VERIFY
				AssertTick(1);
				Mocks.VerifyAll();

				Assert.That(response.Text,
							Is.EqualTo(ExpectedResponse),
							"response text");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				var responsePacket = new DataPacket(ExpectedResponse);

				PacketReceivedRaiser.Raise(Port,
										   new DataPacketEventArgs(responsePacket));
			}

			#endregion

			#region -- Data --

			private const string ExpectedResponse = "!17 1 IDLE --";

			#endregion
		}

		private class AlertTimeoutThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public AlertTimeoutThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				Device.AxisNumber = 1;

				// EXPECT
				FinishRecording();

				// EXEC
				Conversation.AlertTimeoutTimer = _alertTimer;

				var ex = Assert.Throws<TimeoutException>(() => Conversation.WaitForAlert());

				// VERIFY
				AssertTick(2);
				Mocks.VerifyAll();

				Assert.That(ex.Message,
							Is.EqualTo("Timed out while waiting for alert message."),
							"error message");
				Assert.IsTrue(Conversation.IsWaiting,
							  "Conversation should still be waiting after a timeout");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);
				MockTimer.TimeoutNow();

				WaitForTick(2);
				_alertTimer.TimeoutNow();
			}

			#endregion

			#region -- Data --

			private readonly MockTimeoutTimer _alertTimer = new MockTimeoutTimer();

			#endregion
		}

		private class PollUntilIdleThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public PollUntilIdleThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// SETUP
				Device.DeviceNumber = 17;
				Conversation.PollTimeoutTimer = _pollTimer;

				// EXPECT
				const string expectedCommand = "17 0 ";
				Port.Send(expectedCommand, null);
				Port.Send(expectedCommand, null);
				Port.Send(expectedCommand, null);

				FinishRecording();

				// EXEC
				Conversation.PollUntilIdle();

				// VERIFY
				AssertTick(5);
				Mocks.VerifyAll();

				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				var busyArgs = new DataPacketEventArgs(new DataPacket("@17 0 OK BUSY -- 0"));
				var idleArgs = new DataPacketEventArgs(new DataPacket("@17 0 OK IDLE -- 0"));

				WaitForTick(1);
				PacketReceivedRaiser.Raise(Port,
										   busyArgs);
				WaitForTick(2);
				_pollTimer.TimeoutNow();

				WaitForTick(3);
				PacketReceivedRaiser.Raise(Port,
										   busyArgs);
				WaitForTick(4);
				_pollTimer.TimeoutNow();

				WaitForTick(5);
				PacketReceivedRaiser.Raise(Port,
										   idleArgs);
			}

			#endregion

			#region -- Data --

			private readonly MockTimeoutTimer _pollTimer = new MockTimeoutTimer();

			#endregion
		}

		private class PollUntilIdleAlertThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public PollUntilIdleAlertThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// SETUP
				Device.DeviceNumber = 17;
				Conversation.PollTimeoutTimer = _pollTimer;

				// EXPECT
				const string expectedCommand = "17 0 ";
				Port.Send(expectedCommand, null);
				Port.Send(expectedCommand, null);

				FinishRecording();

				// EXEC
				Conversation.PollUntilIdle();

				// VERIFY
				AssertTick(4);
				Mocks.VerifyAll();

				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				var busyArgs = new DataPacketEventArgs(new DataPacket("@17 0 OK BUSY -- 0"));
				var alertArgs = new DataPacketEventArgs(new DataPacket("!17 0 IDLE --"));

				WaitForTick(1);
				PacketReceivedRaiser.Raise(Port,
										   busyArgs);
				WaitForTick(2);
				_pollTimer.TimeoutNow();

				WaitForTick(3);
				PacketReceivedRaiser.Raise(Port,
										   busyArgs);

				WaitForTick(4);
				PacketReceivedRaiser.Raise(Port,
										   alertArgs);
			}

			#endregion

			#region -- Data --

			private readonly MockTimeoutTimer _pollTimer = new MockTimeoutTimer();

			#endregion
		}

		private class PollUntilIdleReplacedThreads : ConversationTestThreads
		{
			#region -- Public Methods & Properties --

			public PollUntilIdleReplacedThreads(ConversationTest mainTest)
				: base(mainTest)
			{
			}


			[TestThread]
			public void Request()
			{
				// SETUP
				Device.DeviceNumber = 17;
				Conversation.PollTimeoutTimer = _pollTimer;

				// EXPECT
				const string expectedCommand = "17 0 ";
				Port.Send(expectedCommand, null);
				Port.Send(expectedCommand, null);
				Port.Send(expectedCommand, null);
				Port.Send("17 0 warnings", null);

				FinishRecording();

				// EXEC
				var ex = Assert.Throws<RequestReplacedException>(() => Conversation.PollUntilIdle());

				// VERIFY
				AssertTick(6);
				Mocks.VerifyAll();

				Assert.That(ex.Message.Contains("Previous command was interrupted"),
							"Command interrupted");
				Assert.IsFalse(Conversation.IsWaiting,
							   "Conversation should not be waiting for anything");
			}


			[TestThread]
			public void Response()
			{
				var busyArgs = new DataPacketEventArgs(new DataPacket("@17 0 OK BUSY -- 0"));
				var idleArgs = new DataPacketEventArgs(new DataPacket("@17 0 OK IDLE WR 0"));
				var warnArgs = new DataPacketEventArgs(new DataPacket("@17 0 OK IDLE WR 02 WR NI"));

				WaitForTick(1);
				PacketReceivedRaiser.Raise(Port,
										   busyArgs);
				WaitForTick(2);
				_pollTimer.TimeoutNow();

				WaitForTick(3);
				PacketReceivedRaiser.Raise(Port,
										   busyArgs);
				WaitForTick(4);
				_pollTimer.TimeoutNow();

				WaitForTick(5);
				PacketReceivedRaiser.Raise(Port,
										   idleArgs);
				WaitForTick(6);
				PacketReceivedRaiser.Raise(Port,
										   warnArgs);
			}

			#endregion

			#region -- Data --

			private readonly MockTimeoutTimer _pollTimer = new MockTimeoutTimer();

			#endregion
		}


		protected class ConversationTestThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			protected ConversationTestThreads(ConversationTest mainTest)
			{
				MainTest = mainTest;
			}


			[Initialize]
			public virtual void Initialize()
			{
				// SETUP
				Mocks = new MockRepository();
				Port = CreatePort(Mocks);
				Device = MainTest.CreateDevice(17);
				Conversation = new Conversation(Device);
				MockTimer = new MockTimeoutTimer();
				Conversation.TimeoutTimer = MockTimer;

				// EXPECT
				PacketReceivedRaiser = CreateDataPacketReceivedRaiser(Port);
				ErrorReceivedRaiser = CreateErrorReceivedRaiser(Port);
			}


			protected ConversationTest MainTest { get; }

			protected IZaberPort Port { get; set; }

			protected IEventRaiser PacketReceivedRaiser { get; set; }

			protected IEventRaiser ErrorReceivedRaiser { get; set; }

			protected ZaberDevice Device { get; set; }

			protected Conversation Conversation { get; set; }

			protected MockTimeoutTimer MockTimer { get; set; }

			protected MockRepository Mocks { get; set; }

			#endregion

			#region -- Non-Public Methods --

			protected void FinishRecording()
			{
				Mocks.ReplayAll();
				Device.Port = Port;
			}

			#endregion
		}


		protected virtual ZaberDevice CreateDevice(byte deviceNumber)
		{
			var device = new ZaberDevice
			{
				DeviceNumber = deviceNumber,
				DeviceType = DeviceTypeMother.CreateDeviceTypeWithCommands(),
				AreMessageIdsEnabled = true
			};

			return device;
		}


		protected virtual ZaberDevice CreateDeviceAscii(byte deviceNumber)
			=> CreateDeviceAscii(deviceNumber, new FirmwareVersion(6, 14));


		protected virtual ZaberDevice CreateDeviceAscii(byte deviceNumber, FirmwareVersion fwVersion)
		{
			var deviceType = DeviceTypeMother.CreateDeviceTypeWithCommands();
			deviceType.FirmwareVersion = fwVersion;
			var device = new ZaberDevice
			{
				DeviceNumber = deviceNumber,
				DeviceType = deviceType,
				AreAsciiMessageIdsEnabled = true
			};

			return device;
		}


		private static Conversation AddAxis(Conversation conversation, int axisNumber)
		{
			var device = conversation.Device;
			var axisDevice = new ZaberDevice
			{
				DeviceType = device.DeviceType,
				DeviceNumber = device.DeviceNumber,
				AxisNumber = axisNumber
			};
			device.AddAxis(axisDevice);

			var axisConversation = new Conversation(axisDevice);
			conversation.AddAxis(axisConversation);
			return axisConversation;
		}


		private static IZaberPort CreatePort(MockRepository mocks)
		{
			var port = mocks.StrictMock<IZaberPort>();

			port.IsAsciiMode = false;
			LastCall.PropertyBehavior();
			port.IsAsciiMode = false;

			port.DataPacketReceived += null;
			LastCall.IgnoreArguments().Repeat.Any();
			port.DataPacketSent += null;
			LastCall.IgnoreArguments().Repeat.Any();
			port.ErrorReceived += null;
			LastCall.IgnoreArguments().Repeat.Any();
			return port;
		}


		private static IEventRaiser CreateDataPacketReceivedRaiser(IZaberPort port)
		{
			port.DataPacketReceived += null;
			return LastCall.Repeat.Any().GetEventRaiser();
		}


		protected static IEventRaiser CreateDataPacketSentRaiser(IZaberPort port)
		{
			port.DataPacketSent += null;
			return LastCall.Repeat.Any().GetEventRaiser();
		}


		private static IEventRaiser CreateErrorReceivedRaiser(IZaberPort port)
		{
			port.ErrorReceived += null;
			return LastCall.Repeat.Any().GetEventRaiser();
		}
	}
}

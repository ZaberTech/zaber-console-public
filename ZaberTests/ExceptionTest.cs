﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using NUnit.Framework;
using Zaber;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	[SetUICulture("en-US")]
	public class ExceptionTest
	{
		[Test]
		[TestCaseSource("GetTestParameters")]
		public void Construct(Type exceptionType, string defaultMessage)
		{
			// SETUP
			var customMessage = "lorem ipsum";

			// EXEC
			var ex = (Exception) Activator.CreateInstance(exceptionType);
			var ex2 = (Exception) Activator.CreateInstance(exceptionType,
														   customMessage);
			var ex3 = (Exception) Activator.CreateInstance(exceptionType,
														   customMessage,
														   ex);

			// VERIFY
			Assert.That(ex.Message,
						Is.EqualTo(defaultMessage),
						"default message");
			Assert.That(ex2.Message,
						Is.EqualTo(customMessage),
						"custom message");
			Assert.That(ex3.Message,
						Is.EqualTo(customMessage),
						"custom message with inner exception");
			Assert.That(ex3.InnerException,
						Is.SameAs(ex),
						"inner exception");
		}


		[Test]
		[TestCaseSource("GetTestParameters")]
		public void Serialize(Type exceptionType, string defaultMessage)
		{
			// SETUP
			var customMessage = "dolores sit amet";
			var ex1 = (Exception) Activator.CreateInstance(exceptionType,
														   customMessage);
			Exception ex2;

			// EXEC
			using (var fs = new MemoryStream())
			{
				// Construct a BinaryFormatter and use it 
				// to serialize the data to the stream.
				var formatter = new BinaryFormatter();


				// Serialize the array elements.
				formatter.Serialize(fs, ex1);

				// Deserialize the array elements.
				fs.Position = 0;

				ex2 = (Exception) formatter.Deserialize(fs);
			}

			// VERIFY
			Assert.That(ex2.Message,
						Is.EqualTo(customMessage),
						"custom message");
		}


		[Test]
		[TestCaseSource("GetTestParameters")]
		public void SerializeToNull(Type exceptionType, string defaultMessage)
		{
			var ex = (Exception) Activator.CreateInstance(exceptionType);
			var context = new StreamingContext();

			try
			{
				ex.GetObjectData(null, context);

				Assert.Fail("should have thrown.");
			}
			catch (ArgumentNullException thrown)
			{
				var match = false;
				match |= thrown.Message == "Value cannot be null.\r\nParameter name: aSerializationInfo";
				match |= thrown.Message == "Value cannot be null.\r\nParameter name: info";
				Assert.IsTrue(match, "message");
			}
		}


		public static IEnumerable<object[]> GetTestParameters()
		{
			yield return new object[] { typeof(ActiveMessageIdException), "Message id is already active." };
			yield return new object[]
			{
				typeof(ConversationException), "Exception of type 'Zaber.ConversationException' was thrown."
			};
			yield return new object[] { typeof(ErrorResponseException), "Error response received." };
			yield return new object[] { typeof(LoopbackException), "Loopback connection detected." };
			yield return new object[] { typeof(RequestCanceledException), "Request was canceled." };
			yield return new object[] { typeof(RequestCollectionException), "Some requests failed." };
			yield return new object[]
			{
				typeof(RequestReplacedException), "Current request was replaced by a new request."
			};
			yield return new object[] { typeof(RequestTimeoutException), "Request timed out." };
			yield return new object[] { typeof(ZaberPortErrorException), "Port error: None" };
		}
	}
}

﻿using System.IO.Ports;
using NUnit.Framework;
using ZaberTest.Testing;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class PortTestsRS232 : PortTestsBase
	{
		protected override IPortStub CreatePort() => new SerialPortStub(new SerialPort());
	}
}

﻿using System.Collections.Generic;
using System.IO;
using NUnit.Framework;
using Zaber.FirmwareDownload;
using ZaberTest;

namespace Zaber.FirmwareUnpacker.Tests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class FirmwareFileUnpackerTest
	{
		[Test]
		public void TestAnd()
		{
			var b = GetSingleByteOutput("test_data\\And.bin", 0, 0);
			Assert.AreEqual(0, b, "Output is incorrect for the false AND false case.");

			b = GetSingleByteOutput("test_data\\And.bin", 0, 1);
			Assert.AreEqual(0, b, "Output is incorrect for the false AND true case.");

			b = GetSingleByteOutput("test_data\\And.bin", 1, 0);
			Assert.AreEqual(0, b, "Output is incorrect for the true AND false case.");

			b = GetSingleByteOutput("test_data\\And.bin", 1, 1);
			Assert.AreEqual(1, b, "Output is incorrect for the true AND true case.");
		}


		[Test]
		public void TestBadEmitSize()
			=> Assert.Throws<InvalidDataException>(() => GetSingleByteOutput("test_data\\Corrupted5.bin", 0, 0));


		[Test]
		public void TestBadOpcode()
			=> Assert.Throws<InvalidDataException>(() => GetSingleByteOutput("test_data\\Corrupted4.bin", 0, 0));


		[Test]
		public void TestBadSignature()
			=> Assert.Throws<InvalidDataException>(() => GetSingleByteOutput("test_data\\Corrupted1.bin", 0, 0));


		[Test]
		public void TestBadSize()
			=> Assert.Throws<InvalidDataException>(() => GetSingleByteOutput("test_data\\Corrupted3.bin", 0, 0));


		[Test]
		public void TestBadVersion()
			=> Assert.Throws<InvalidDataException>(() => GetSingleByteOutput("test_data\\Corrupted2.bin", 0, 0));


		[Test]
		public void TestCallbacks()
		{
			var progressValues = new List<int>();
			var data = FirmwareFileUnpacker.Unpack(TestDataFileHelper.GetDeployedPath("test_data\\ExampleFWFile.bin"),
												   268566528,
												   12345,
												   (cur, max) =>
												   {
													   progressValues.Add(cur);
													   Assert.GreaterOrEqual(max, cur);
												   });


			var len = progressValues.Count;
			Assert.Greater(len, 0, "No progress reports were sent.");
			for (var i = 1; i < len; ++i)
			{
				Assert.GreaterOrEqual(progressValues[i],
									  progressValues[i - 1],
									  "Progress values out of order; shoudl always increase.");
			}
		}


		[Test]
		public void TestExampleDataCorrectKeys()
		{
			var data = FirmwareFileUnpacker.Unpack(TestDataFileHelper.GetDeployedPath("test_data\\ExampleFWFile.bin"),
												   268566528,
												   12345);

			Assert.AreEqual(EXAMPLE_OUTPUT_DATA.Length,
							data.Length,
							"Firmware image data is the wrong length.");

			for (var i = 0; i < data.Length; ++i)
			{
				Assert.AreEqual(data[i], EXAMPLE_OUTPUT_DATA[i], $"Firmware image data is incorrect at position {i}.");
			}
		}


		[Test]
		public void TestExampleDataWrongPlatform()
		{
			try
			{
				var data = FirmwareFileUnpacker.Unpack(
					TestDataFileHelper.GetDeployedPath("test_data\\ExampleFWFile.bin"),
					0,
					12345);
			}
			catch (FirmwareUnpackException e)
			{
				// The error message comes from the test data file so should be consistent unless someone edits the file.
				Assert.AreEqual("This firmware image is for platform 268566528 only.",
								e.Message,
								"Extraction failure message is wrong.");
			}
		}


		[Test]
		public void TestExampleDataWrongSerial()
		{
			try
			{
				var data = FirmwareFileUnpacker.Unpack(
					TestDataFileHelper.GetDeployedPath("test_data\\ExampleFWFile.bin"),
					268566528,
					0);
			}
			catch (FirmwareUnpackException e)
			{
				// The error message comes from the test data file so should be consistent unless someone edits the file.
				Assert.AreEqual("This firmware image is for device serial number 12345 only.",
								e.Message,
								"Extraction failure message is wrong.");
			}
		}


		[Test]
		public void TestNestedIfCombinations()
		{
			var b = GetSingleByteOutput("test_data\\NestedIfs.bin", 1, 1);
			Assert.AreEqual(3, b, "Output is incorrect for the true-true nested if case.");

			b = GetSingleByteOutput("test_data\\NestedIfs.bin", 1, 0);
			Assert.AreEqual(2, b, "Output is incorrect for the true-false nested if case.");

			b = GetSingleByteOutput("test_data\\NestedIfs.bin", 0, 1);
			Assert.AreEqual(1, b, "Output is incorrect for the false-true nested if case.");

			b = GetSingleByteOutput("test_data\\NestedIfs.bin", 0, 0);
			Assert.AreEqual(0, b, "Output is incorrect for the false-false nested if case.");
		}


		[Test]
		public void TestNestedIfStatements_RM3901()
		{
			// These two files contain firmware for the same devices but in different order.
			// This exposed a bug related to skipping instructions in nested IF statements.
			TestNestedIfStatements_RM3901_common("test_data\\xmcb1-6.98.1364-test-2.fwu");
			TestNestedIfStatements_RM3901_common("test_data\\xmcb1-6.98.1364-test-3.fwu");
		}


		[Test]
		public void TestOr()
		{
			var b = GetSingleByteOutput("test_data\\Or.bin", 0, 0);
			Assert.AreEqual(0, b, "Output is incorrect for the false OR false case.");

			b = GetSingleByteOutput("test_data\\Or.bin", 0, 1);
			Assert.AreEqual(1, b, "Output is incorrect for the false OR true case.");

			b = GetSingleByteOutput("test_data\\Or.bin", 1, 0);
			Assert.AreEqual(1, b, "Output is incorrect for the true OR false case.");

			b = GetSingleByteOutput("test_data\\Or.bin", 1, 1);
			Assert.AreEqual(1, b, "Output is incorrect for the true OR true case.");
		}


		[Test]
		public void TestXor()
		{
			var b = GetSingleByteOutput("test_data\\Xor.bin", 0, 0);
			Assert.AreEqual(0, b, "Output is incorrect for the false XOR false case.");

			b = GetSingleByteOutput("test_data\\Xor.bin", 0, 1);
			Assert.AreEqual(1, b, "Output is incorrect for the false XOR true case.");

			b = GetSingleByteOutput("test_data\\Xor.bin", 1, 0);
			Assert.AreEqual(1, b, "Output is incorrect for the true XOR false case.");

			b = GetSingleByteOutput("test_data\\Xor.bin", 1, 1);
			Assert.AreEqual(0, b, "Output is incorrect for the true XOR true case.");
		}


		private void TestNestedIfStatements_RM3901_common(string aFilePath)
		{
			// These combinations of platform and serial number should produce data.
			var data = FirmwareFileUnpacker.Unpack(TestDataFileHelper.GetDeployedPath(aFilePath), 268568208, 35542);
			Assert.Greater(data.Length, 0);
			data = FirmwareFileUnpacker.Unpack(TestDataFileHelper.GetDeployedPath(aFilePath), 268567051, 41200);
			Assert.Greater(data.Length, 0);

			// These combinations of platform and serial number should NOT produce data.
			Assert.Throws<FirmwareUnpackException>(
				() => FirmwareFileUnpacker.Unpack(TestDataFileHelper.GetDeployedPath(aFilePath), 268568208, 41200));
			Assert.Throws<FirmwareUnpackException>(
				() => FirmwareFileUnpacker.Unpack(TestDataFileHelper.GetDeployedPath(aFilePath), 268567051, 35542));
		}


		private static byte GetSingleByteOutput(string aFile, uint aPlatform, uint aSerial)
		{
			var data = FirmwareFileUnpacker.Unpack(TestDataFileHelper.GetDeployedPath(aFile), aPlatform, aSerial);
			Assert.AreEqual(1, data.Length, $"Image length is {data.Length}, should be 1.");
			return data[0];
		}


		private static readonly byte[] EXAMPLE_OUTPUT_DATA =
		{
			0x36, 0xD6, 0x22, 0x30, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x02, 0x10, 0x00,
			0x00, 0x00, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06
		};
	}
}

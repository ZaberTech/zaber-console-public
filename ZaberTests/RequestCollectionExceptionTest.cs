﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using NUnit.Framework;
using Zaber;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class RequestCollectionExceptionTest
	{
		[Test]
		public void AllValid()
		{
			// SETUP
			var topics = new List<ConversationTopic>();
			topics.Add(new ConversationTopic());
			topics.Add(new ConversationTopic());
			topics[0].Response = new DeviceMessage();
			topics[1].Response = new DeviceMessage();

			// EXEC
			var ex = new RequestCollectionException(topics);
			var msg = ex.Message;

			// VERIFY
			Assert.AreEqual("",
							msg,
							"error message");
		}


		[Test]
		public void DifferentPortErrors()
		{
			// SETUP
			var topics = new List<ConversationTopic>();
			topics.Add(new ConversationTopic());
			topics.Add(new ConversationTopic());
			topics[0].ZaberPortError = ZaberPortError.PacketTimeout;
			topics[1].ZaberPortError = ZaberPortError.Frame;

			// EXEC
			var ex = new RequestCollectionException(topics);
			var msg = ex.Message;

			// VERIFY
			Assert.AreEqual(
				"Some requests failed because of a port error: PacketTimeout. Some requests failed because of a port error: Frame.",
				msg,
				"error message");
		}


		[Test]
		public void DuplicatePortErrors()
		{
			// SETUP
			var topics = new List<ConversationTopic>();
			topics.Add(new ConversationTopic());
			topics.Add(new ConversationTopic());
			topics[0].ZaberPortError = ZaberPortError.PacketTimeout;
			topics[1].ZaberPortError = ZaberPortError.PacketTimeout;

			// EXEC
			var ex = new RequestCollectionException(topics);
			var msg = ex.Message;

			// VERIFY
			Assert.AreEqual("Some requests failed because of a port error: PacketTimeout.",
							msg,
							"error message");
		}


		[Test]
		public void EmptyList()
		{
			// EXEC
			var ex1 = new RequestCollectionException();
			var topicCount1 = ex1.Topics.Count;
			var ex2 = new RequestCollectionException("lorem ipsum");
			var topicCount2 = ex2.Topics.Count;
			var ex3 = new RequestCollectionException("lorem ipsum", ex1);
			var topicCount3 = ex3.Topics.Count;

			// VERIFY
			Assert.That(topicCount1,
						Is.EqualTo(0),
						"topic count");
			Assert.That(topicCount2,
						Is.EqualTo(0),
						"topic count with custom message");
			Assert.That(topicCount3,
						Is.EqualTo(0),
						"topic count with inner exception");
		}


		[Test]
		public void ErrorResponse()
		{
			// SETUP
			var topics = new List<ConversationTopic>();
			topics.Add(new ConversationTopic());
			topics.Add(new ConversationTopic());
			topics[0].Response = new DeviceMessage();
			topics[1].Response = new DeviceMessage();
			topics[1].Response.DeviceNumber = 12;
			topics[1].Response.Command = Command.Error;
			topics[1].Response.NumericData = (int) ZaberError.CommandInvalid;

			// EXEC
			var ex = new RequestCollectionException(topics);
			var msg = ex.Message;

			// VERIFY
			Assert.AreEqual("Some requests failed: CommandInvalid error on device 12.",
							msg,
							"error message");
		}


		[Test]
		public void Mixture()
		{
			// SETUP
			var topics = new List<ConversationTopic>();
			topics.Add(new ConversationTopic());
			topics.Add(new ConversationTopic());
			topics.Add(new ConversationTopic());
			topics.Add(new ConversationTopic());
			topics[0].ReplacementResponse = new DeviceMessage();
			topics[0].ReplacementResponse.DeviceNumber = 10;
			topics[1].ZaberPortError = ZaberPortError.Frame;
			topics[2].Response = new DeviceMessage(); // success
			topics[3].Response = new DeviceMessage();
			topics[3].Response.DeviceNumber = 15;
			topics[3].Response.Command = Command.Error;
			topics[3].Response.NumericData = (int) ZaberError.OffsetInvalid;

			// EXEC
			var ex = new RequestCollectionException(topics);
			var msg = ex.Message;

			// VERIFY
			Assert.AreEqual(
				"Some requests failed: request replaced on device 10; OffsetInvalid error on device 15. Some requests failed because of a port error: Frame.",
				msg,
				"error message");
		}


		[Test]
		public void NullResponse()
		{
			// SETUP
			var topics = new List<ConversationTopic>();
			topics.Add(new ConversationTopic());
			topics.Add(new ConversationTopic());
			topics[1].ReplacementResponse = new DeviceMessage();
			topics[1].ReplacementResponse.DeviceNumber = 10;

			// EXEC
			var ex = new RequestCollectionException(topics);
			var msg = ex.Message;

			// VERIFY
			Assert.AreEqual("Some requests failed: null response; request replaced on device 10.",
							msg,
							"error message");
		}


		[Test]
		public void NullTopic()
		{
			// SETUP
			var topics = new List<ConversationTopic>();
			topics.Add(null);
			topics.Add(new ConversationTopic());
			topics[1].ReplacementResponse = new DeviceMessage();
			topics[1].ReplacementResponse.DeviceNumber = 10;

			// EXEC
			var ex = new RequestCollectionException(topics);
			var msg = ex.Message;

			// VERIFY
			Assert.AreEqual("Some requests failed: null topic; request replaced on device 10.",
							msg,
							"error message");
		}


		[Test]
		public void PortError()
		{
			// SETUP
			var topics = new List<ConversationTopic>();
			topics.Add(new ConversationTopic());
			topics.Add(new ConversationTopic());
			topics[0].Response = new DeviceMessage();
			topics[1].ZaberPortError = ZaberPortError.Frame;

			// EXEC
			var ex = new RequestCollectionException(topics);
			var msg = ex.Message;

			// VERIFY
			Assert.AreEqual("Some requests failed because of a port error: Frame.",
							msg,
							"error message");
		}


		[Test]
		public void ReplacedRequest()
		{
			// SETUP
			var topics = new List<ConversationTopic>();
			topics.Add(new ConversationTopic());
			topics.Add(new ConversationTopic());
			topics[0].Response = new DeviceMessage();
			topics[1].ReplacementResponse = new DeviceMessage();
			topics[1].ReplacementResponse.DeviceNumber = 12;

			// EXEC
			var ex = new RequestCollectionException(topics);
			var msg = ex.Message;

			// VERIFY
			Assert.AreEqual("Some requests failed: request replaced on device 12.",
							msg,
							"error message");
		}


		[Test]
		public void Serialize()
		{
			// SETUP
			var topics = new List<ConversationTopic>();
			topics.Add(new ConversationTopic());
			topics[0].ZaberPortError = ZaberPortError.Frame;
			var ex1 = new RequestCollectionException(topics);
			RequestCollectionException ex2;

			// EXEC
			using (var fs = new MemoryStream())
			{
				// Construct a BinaryFormatter and use it 
				// to serialize the data to the stream.
				var formatter = new BinaryFormatter();


				// Serialize the array elements.
				formatter.Serialize(fs, ex1);

				// Deserialize the array elements.
				fs.Position = 0;

				ex2 = (RequestCollectionException) formatter.Deserialize(fs);
			}

			// VERIFY
			Assert.That(ex2.Message,
						Is.EqualTo("Some requests failed because of a port error: Frame."),
						"error message");
			Assert.That(ex2.Topics.Count,
						Is.EqualTo(1),
						"topic count");
		}


		[Test]
		public void TextModeErrorResponse()
		{
			// SETUP
			var topics = new List<ConversationTopic>();
			topics.Add(new ConversationTopic());
			topics.Add(new ConversationTopic());
			topics[0].Response = new DeviceMessage("@01 0 RJ BUSY -- BADDATA");
			topics[1].Response = new DeviceMessage("@03 0 RJ IDLE -- BADCOMMAND");

			// EXEC
			var ex = new RequestCollectionException(topics);
			var msg = ex.Message;

			// VERIFY
			Assert.AreEqual("Some requests failed: BADDATA on device 1; BADCOMMAND on device 3.",
							msg,
							"error message");
		}


		[Test]
		public void UnknownErrorResponse()
		{
			// SETUP
			var topics = new List<ConversationTopic>();
			topics.Add(new ConversationTopic());
			topics.Add(new ConversationTopic());
			topics[0].Response = new DeviceMessage();
			topics[1].Response = new DeviceMessage();
			topics[1].Response.DeviceNumber = 12;
			topics[1].Response.Command = Command.Error;
			topics[1].Response.NumericData = 66;

			// EXEC
			var ex = new RequestCollectionException(topics);
			var msg = ex.Message;

			// VERIFY
			Assert.AreEqual("Some requests failed: error 66 on device 12.",
							msg,
							"error message");
		}
	}
}

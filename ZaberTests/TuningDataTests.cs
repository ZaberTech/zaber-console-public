﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Zaber;
using Zaber.Units;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class TuningDataTests
	{
		[Test]
		public void DefaultInitialization()
		{
			var data = new TuningData();
			Assert.IsNull(data.Inertia);
			Assert.AreEqual(0.0, data.ForqueConstant);
			Assert.IsNull(data.GuidanceData);
			Assert.IsNull(data.ControllerVersions);
		}


		[Test]
		public void CopyConstructor()
		{
			var original = new TuningData
			{
				Inertia = new Zaber.Units.Measurement(100.0, UnitConverter.Default.FindUnitBySymbol("g")),
				ForqueConstant = 1000.0,
				GuidanceData = "Foo",
				ControllerVersions = new Dictionary<string, int> { { "x", 1 }, {"y", 2} }
			};

			var data = new TuningData(original);
			Assert.AreEqual(original.Inertia, data.Inertia);
			Assert.IsFalse(object.ReferenceEquals(original.Inertia, data.Inertia));
			Assert.AreEqual(original.ForqueConstant, data.ForqueConstant);
			Assert.AreEqual(original.GuidanceData, data.GuidanceData);
			Assert.AreEqual(original.ControllerVersions, data.ControllerVersions);
			Assert.IsFalse(object.ReferenceEquals(original.ControllerVersions, data.ControllerVersions));
		}


		[Test]
		public void CopyConstructorWithNullProperties()
		{
			var original = new TuningData();

			var data = new TuningData(original);
			Assert.IsNull(data.Inertia);
			Assert.AreEqual(0.0, data.ForqueConstant);
			Assert.IsNull(data.GuidanceData);
			Assert.IsNull(data.ControllerVersions);
		}


		[Test]
		public void Equality()
		{
			var original = new TuningData
			{
				Inertia = new Zaber.Units.Measurement(100.0, UnitConverter.Default.FindUnitBySymbol("g")),
				ForqueConstant = 1000.0,
				GuidanceData = "Foo",
				ControllerVersions = new Dictionary<string, int> { { "x", 1 }, { "y", 2 } }
			};

			var copy = new TuningData(original);
			Assert.IsTrue(original.Equals(copy));

			copy.Inertia = new Zaber.Units.Measurement(101.0, UnitConverter.Default.FindUnitBySymbol("g"));
			Assert.IsFalse(original.Equals(copy));

			copy = new TuningData(original);
			copy.ForqueConstant = 100.0;
			Assert.IsFalse(original.Equals(copy));

			copy = new TuningData(original);
			copy.GuidanceData = "Bar";
			Assert.IsFalse(original.Equals(copy));

			copy = new TuningData(original);
			copy.ControllerVersions = new Dictionary<string, int> {{"x", 2}, {"y", 1}};
			Assert.IsFalse(original.Equals(copy));
		}


		[Test]
		public void HashCode()
		{
			var original = new TuningData
			{
				Inertia = new Zaber.Units.Measurement(100.0, UnitConverter.Default.FindUnitBySymbol("g")),
				ForqueConstant = 1000.0,
				GuidanceData = "Foo",
				ControllerVersions = new Dictionary<string, int> { { "x", 1 }, { "y", 2 } }
			};

			var copy = new TuningData(original);
			Assert.AreEqual(original.GetHashCode(), copy.GetHashCode());

			copy.Inertia = new Zaber.Units.Measurement(101.0, UnitConverter.Default.FindUnitBySymbol("g"));
			Assert.AreNotEqual(original.GetHashCode(), copy.GetHashCode());

			copy = new TuningData(original);
			copy.ForqueConstant = 100.0;
			Assert.AreNotEqual(original.GetHashCode(), copy.GetHashCode());

			copy = new TuningData(original);
			copy.GuidanceData = "Bar";
			Assert.AreNotEqual(original.GetHashCode(), copy.GetHashCode());

			copy = new TuningData(original);
			copy.ControllerVersions = new Dictionary<string, int> { { "x", 2 }, { "y", 1 } };
			Assert.AreNotEqual(original.GetHashCode(), copy.GetHashCode());
		}
	}
}

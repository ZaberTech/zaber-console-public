﻿using System.IO;
using System.Linq;
using NUnit.Framework;
using Zaber.PlugIns;

namespace ZaberTest.PlugIns
{
	[TestFixture]
	[SetCulture("en-US")]
	public class ScriptPreprocessorTests
	{
		[Test]
		[Description("Verifies correct substitution in both a complex script and complex template.")]
		public void TestComplexUseCase()
		{
			var script = LoadTextFile("script-with-complex-usage.txt");
			var preprocessor = new ScriptPreprocessor(ScriptLanguage.FindByExtension(".js"));
			var templateName = preprocessor.GetTemplateName(script) + ".txt";
			var template = LoadTextFile(templateName);
			var output = preprocessor.Preprocess(script, template);
			var expected = LoadTextFile("script-with-complex-usage-result.txt");
			Assert.AreEqual(expected, output);
			var assemblies = preprocessor.ExtraAssemblies.ToArray();
			Assert.AreEqual(3, assemblies.Length);
			Assert.AreEqual("Template Import 1", assemblies[0]);
			Assert.AreEqual("User Import 1", assemblies[1]);
			Assert.AreEqual("UserImport2", assemblies[2]);

			// The sources of the output lines of text are complicated in this case!
			// This table omits whitespace lines since it doesn't matter where they came from.
			var expectedLineMap = new[]
			{
				// output     // input (-ve and 1-based for template)
				0, -1, 1, -3, 2, -5, 5, 1, 6, 2, 7, 6, 9, 8, 10, -2, 11, 4, 12, 10, 13, -11, 16, 12, 18, 14, 19, 15, 21,
				-9, 22, -10, 23, -12
			};

			for (var i = 0; i < expectedLineMap.Length; i += 2)
			{
				Assert.AreEqual(expectedLineMap[i + 1], preprocessor.LineNumberMap[expectedLineMap[i]]);
			}
		}


		[Test]
		[Description("Verifies use of import directive alone.")]
		public void TestImportDirective()
		{
			var script = LoadTextFile("script-with-one-import.txt");
			var preprocessor = new ScriptPreprocessor(ScriptLanguage.FindByExtension(".cs"));
			var output = preprocessor.Preprocess(script, null);
			var expected = LoadTextFile("script-with-one-import-result.txt");
			Assert.AreEqual(expected, output);
			Assert.AreEqual(1, preprocessor.ExtraAssemblies.Count());
			Assert.AreEqual("/foo/bar.dll", preprocessor.ExtraAssemblies.First());

			// This preprocess just deletes line 1, leaving two.
			Assert.AreEqual(2, preprocessor.LineNumberMap.Count);
			Assert.AreEqual(0, preprocessor.LineNumberMap[0]);
			Assert.AreEqual(2, preprocessor.LineNumberMap[1]);
		}


		[Test]
		[Description("Verifies the script preprocessor makes no changes if there are no directives.")]
		public void TestNoDirectives()
		{
			var script = LoadTextFile("script-with-no-directives.cs");
			var preprocessor = new ScriptPreprocessor(ScriptLanguage.FindByExtension(".cs"));
			var output = preprocessor.Preprocess(script, null);
			Assert.AreEqual(script, output);
			Assert.IsEmpty(preprocessor.ExtraAssemblies);

			// Without any directives, input and output text lines should correspond 1:1.
			foreach (var pair in preprocessor.LineNumberMap)
			{
				Assert.AreEqual(pair.Value, pair.Key);
			}
		}


		[Test]
		[Description("Verifies use of template substitution alone.")]
		public void TestTemplateDirective()
		{
			var script = LoadTextFile("script-with-one-template.txt");
			var preprocessor = new ScriptPreprocessor(ScriptLanguage.FindByExtension(".cs"));
			var templateName = preprocessor.GetTemplateName(script) + ".txt";
			var template = LoadTextFile(templateName);
			var output = preprocessor.Preprocess(script, template);
			var expected = LoadTextFile("script-with-one-template-result.txt");
			Assert.AreEqual(expected, output);
			Assert.IsEmpty(preprocessor.ExtraAssemblies);

			// Output lines 4-7 should map to input lines 1-3. The rest should be from the template.
			foreach (var pair in preprocessor.LineNumberMap)
			{
				if (pair.Value >= 0)
				{
					Assert.AreEqual(pair.Key - 3, pair.Value);
				}
			}
		}


		[Test]
		[Description("Verifies correction of Unix line endings.")]
		public void TestLineEndingSubstitutionUnix()
		{
			var script = "#template(foo)\r\rCode here\r";
			var template = "{\r// $INSERT-SCRIPT-HERE$\r}\r";
			var preprocessor = new ScriptPreprocessor(ScriptLanguage.FindByExtension(".cs"));
			var output = preprocessor.Preprocess(script, template);
			var expected = "{\r\n\r\n\r\nCode here\r\n}\r\n";
			Assert.AreEqual(expected, output);
		}


		[Test]
		[Description("Verifies correction of mixed line endings.")]
		public void TestLineEndingSubstitutionMixed()
		{
			var script = "#template(foo)\rCode here\n";
			var template = "{\r\n\r// $INSERT-SCRIPT-HERE$\n}\r";
			var preprocessor = new ScriptPreprocessor(ScriptLanguage.FindByExtension(".cs"));
			var output = preprocessor.Preprocess(script, template);
			var expected = "{\r\n\r\n\r\nCode here\r\n}\r\n";
			Assert.AreEqual(expected, output);
		}


		[Test]
		[Description("Verifies preservation of Windows line endings.")]
		public void TestLineEndingSubstitutionWindows()
		{
			var script = "#template(foo)\r\n\r\nCode here\r\n";
			var template = "{\r\n// $INSERT-SCRIPT-HERE$\r\n}\r\n";
			var preprocessor = new ScriptPreprocessor(ScriptLanguage.FindByExtension(".cs"));
			var output = preprocessor.Preprocess(script, template);
			var expected = "{\r\n\r\n\r\nCode here\r\n}\r\n";
			Assert.AreEqual(expected, output);
		}


		[Test]
		[Description("Verifies use of using directive alone.")]
		public void TestUsingDirective()
		{
			var script = LoadTextFile("script-with-one-using.txt");
			foreach (var ext in new[] { ".cs", ".vb", ".js", ".py" })
			{
				TestUsingDirectivePerLanguage(script, ".cs");
			}
		}


		[Test]
		[Description("Verifies using directive inserts after existing usings.")]
		public void TestUsingDirectiveInsertsAfterOtherUsings()
		{
			var script = LoadTextFile("script-with-mixed-usings.txt");
			var preprocessor = new ScriptPreprocessor(ScriptLanguage.FindByExtension(".cs"));
			var output = preprocessor.Preprocess(script, null);
			var expected = LoadTextFile("script-with-mixed-usings-result.txt");
			Assert.AreEqual(expected, output);
			Assert.IsEmpty(preprocessor.ExtraAssemblies);

			// This test should delete line 6 and insert a new line 3, referencing 6.
			var expectedLineMap = new[] { 0, 0, 1, 1, 2, 2, 3, 6, 4, 3, 5, 4, 6, 5, 7, 7, 8, 8 };

			Assert.AreEqual(expectedLineMap.Length / 2, preprocessor.LineNumberMap.Count);
			for (var i = 0; i < expectedLineMap.Length; i += 2)
			{
				Assert.AreEqual(expectedLineMap[i + 1], preprocessor.LineNumberMap[expectedLineMap[i]]);
			}
		}


		private void TestUsingDirectivePerLanguage(string aScript, string aExtension)
		{
			var preprocessor = new ScriptPreprocessor(ScriptLanguage.FindByExtension(aExtension));
			var output = preprocessor.Preprocess(aScript, null);
			var expected = LoadTextFile("script-with-one-using-result" + aExtension);
			Assert.AreEqual(expected, output);
			Assert.IsEmpty(preprocessor.ExtraAssemblies);

			// This preprocess removes line 1 then inserts a new line 0.
			Assert.AreEqual(3, preprocessor.LineNumberMap.Count);
			Assert.AreEqual(1, preprocessor.LineNumberMap[0]);
			Assert.AreEqual(0, preprocessor.LineNumberMap[1]);
			Assert.AreEqual(2, preprocessor.LineNumberMap[2]);
		}


		private static string LoadTextFile(string aRelativePath)
		{
			var path = TestDataFileHelper.GetDeployedPath(Path.Combine("test_data", "scripts", aRelativePath));
			var text = File.ReadAllText(path);

			// Make sure the text from the reference files contains the same line endings we
			// output to merged scripts. Git checkouts could use Unix-style line endings when
			// run under CygWin, and some builds of Windows 10 default to Unix-style.
			if (!text.Contains("\r"))
			{
				text = text.Replace("\n", "\r\n");
			}

			return text;
		}
	}
}

using System;
using System.CodeDom.Compiler;
using System.IO;
using NUnit.Framework;
using Zaber.PlugIns;

namespace ZaberTest.PlugIns
{
	[TestFixture]
	[SetCulture("en-US")]
	[SetUICulture("en-US")]
	public class ScriptCompilerTest
	{
		[Test]
		public void Compile()
		{
			// SETUP
			var compiler = CreateCompiler();

			var sourceCode =
				@"
class PlugIn : Zaber.PlugIns.PlugInBase
{
    public override void Run()
    {
        Output.Write(""Hello, World!"");
    }
}
";

			// EXEC
			var plugIn = compiler.Compile(sourceCode);
			plugIn.Output = new StringWriter();
			plugIn.Run();
			var output = plugIn.Output.ToString();
			var errorCount = compiler.Errors.Count;

			// VERIFY
			Assert.AreEqual("Hello, World!",
							output,
							"Output should match");
			Assert.AreEqual(0,
							errorCount,
							"Number of errors should match");
		}


		[Test]
		public void CompileError()
		{
			// SETUP
			var compiler = CreateCompiler();

			var sourceCode =
				@"
class PlugIn : Zaber.PlugIns.PlugInBase
{
    public override void Run()
    {
        OutPPPut.Write(""Hello, World!"");
    }
}
";

			// EXEC
			string msg = null;
			try
			{
				compiler.Compile(sourceCode);

				Assert.Fail("Should have thrown");
			}
			catch (ArgumentException ex)
			{
				msg = ex.Message;
			}

			var errors = compiler.Errors;

			// VERIFY
			Assert.AreEqual("Source code generated compiler errors.\r\nParameter name: aSourceCode",
							msg,
							"Exception message should match");
			Assert.AreEqual(1,
							errors.Count,
							"Number of errors should match");
			Assert.AreEqual("The name 'OutPPPut' does not exist in the current context",
							errors[0].ErrorText,
							"Compiler error message should match");
		}


		[Test]
		public void CompilerWarning()
		{
			// SETUP
			var compiler = CreateCompiler();

			var sourceCode =
				@"
class PlugIn : Zaber.PlugIns.PlugInBase
{
    public override void Run()
    {
        int unread = 42;
        Output.Write(""Hello, World!"");
    }
}
";

			// EXEC
			var plugIn = compiler.Compile(sourceCode);
			plugIn.Output = new StringWriter();
			plugIn.Run();
			var output = plugIn.Output.ToString();
			var errors = compiler.Errors;

			// VERIFY
			Assert.AreEqual("Hello, World!",
							output,
							"Output should match");
			Assert.AreEqual(1,
							errors.Count,
							"Number of errors (actually warnings) should match.");
			Assert.AreEqual("The variable 'unread' is assigned but its value is never used",
							errors[0].ErrorText,
							"Warning message should match");
		}


		[Test]
		public void NoPlugInClass()
		{
			// SETUP
			var compiler = CreateCompiler();

			var sourceCode =
				@"
class PlugIn
{
    public void Run()
    {
        System.Console.Out.Write(""Hello, World!"");
    }
}
";

			// EXEC
			string msg = null;
			try
			{
				compiler.Compile(sourceCode);

				Assert.Fail("Should have thrown");
			}
			catch (ArgumentException ex)
			{
				msg = ex.Message;
			}

			var errorCount = compiler.Errors.Count;

			// VERIFY
			Assert.AreEqual("No class found that implements IPlugIn.\r\nParameter name: aSourceCode",
							msg,
							"Exception message should match");
			Assert.AreEqual(0,
							errorCount,
							"Number of errors should match");
		}


		private static ScriptCompiler CreateCompiler()
		{
			var compilerInfo = CodeDomProvider.GetCompilerInfo("C#");
			var zaberLibrary = new FileInfo("Zaber.dll");
			var compiler = new ScriptCompiler(compilerInfo);
			return compiler;
		}
	}
}

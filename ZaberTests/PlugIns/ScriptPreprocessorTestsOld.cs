using System;
using System.Linq;
using NUnit.Framework;
using Zaber.PlugIns;

namespace ZaberTest.PlugIns
{
	/// <summary>
	///     These were originally tests for the CodeBuilder class, which has been replaced
	///     by the new ScriptPreprocessor class, but the tests have been converted to ensure
	///     backwards compatibility.
	/// </summary>
	[TestFixture]
	[SetCulture("en-US")]
	public class ScriptPreprocessorTestsOld
	{
		[SetUp]
		public void Setup() => _language = ScriptLanguage.FindAll().First();


		[Test]
		public void Build()
		{
			// SETUP
			var template =
				"This is template line 1"
			+ Environment.NewLine
			+ "/* This could be any kind of comment $INSERT-SCRIPT-HERE$ surrounding the marker */"
			+ Environment.NewLine
			+ "This is template line 3";
			var script =
				"	Script line 1"
			+ Environment.NewLine
			+ "	Script line 2";
			var expectedCode =
				"This is template line 1"
			+ Environment.NewLine
			+ "	Script line 1"
			+ Environment.NewLine
			+ "	Script line 2"
			+ Environment.NewLine
			+ "This is template line 3"
			+ Environment.NewLine; // New preprocessor adds a newline at end of file.

			// EXEC
			var processor = new ScriptPreprocessor(_language);
			var code = processor.Preprocess(script, template);

			// VERIFY
			Assert.AreEqual(expectedCode,
							code,
							"Code should match");
		}


		[Test]
		public void BuildWithBlankLineBeforeExplicitTemplate()
		{
			// SETUP
			var script =
				"	Script line 1"
			+ Environment.NewLine
			+ "#template(SomeTemplateName)"
			+ Environment.NewLine
			+ "	Script line 2";
			var expectedTemplateName = "SomeTemplateName";
			var template =
				"This is template line 1"
			+ Environment.NewLine
			+ "/* This could be any kind of comment $INSERT-SCRIPT-HERE$ surrounding the marker */"
			+ Environment.NewLine
			+ "This is template line 3";
			var expectedCode =
				"This is template line 1"
			+ Environment.NewLine
			+ "	Script line 1"
			+ Environment.NewLine
			+ Environment.NewLine
			+ "	Script line 2"
			+ Environment.NewLine
			+ "This is template line 3"
			+ Environment.NewLine;


			// EXEC
			var processor = new ScriptPreprocessor(_language);
			var code = processor.Preprocess(script, template);
			var templateName = processor.GetTemplateName(script);

			// VERIFY
			Assert.AreEqual(expectedTemplateName,
							templateName,
							"Template name should match");
			Assert.AreEqual(expectedCode,
							code,
							"Code should match");
		}


		[Test]
		public void BuildWithExplicitTemplate()
		{
			// SETUP
			var script =
				"	Script line 1"
			+ Environment.NewLine
			+ "#template(SomeTemplateName)"
			+ Environment.NewLine
			+ "	Script line 2";
			var expectedTemplateName = "SomeTemplateName";
			BuildWithExplicitTemplate(script, expectedTemplateName, _language);
		}


		[Test]
		public void BuildWithExplicitTemplateExtraParenthesis()
		{
			// SETUP
			var script =
				"	Script line 1"
			+ Environment.NewLine
			+ "#template(SomeTemplateName) junk (food)"
			+ Environment.NewLine
			+ "	Script line 2";
			var expectedTemplateName = "SomeTemplateName";
			BuildWithExplicitTemplate(script, expectedTemplateName, _language);
		}


		[Test]
		public void BuildWithExplicitTemplateLeadingSpace()
		{
			// SETUP
			var script =
				"	Script line 1"
			+ Environment.NewLine
			+ "    #template(SomeTemplateName)"
			+ Environment.NewLine
			+ "	Script line 2";
			var expectedTemplateName = "SomeTemplateName";
			BuildWithExplicitTemplate(script, expectedTemplateName, _language);
		}


		[Test]
		public void BuildWithExplicitTemplateTrailingComment()
		{
			// SETUP
			var script =
				"	Script line 1"
			+ Environment.NewLine
			+ "#template(SomeTemplateName) some junk to ignore"
			+ Environment.NewLine
			+ "	Script line 2";
			var expectedTemplateName = "SomeTemplateName";
			BuildWithExplicitTemplate(script, expectedTemplateName, _language);
		}


		[Test]
		public void EmptyTemplateDeclaration()
		{
			// SETUP
			var template = @"";
			var script =
				"	Script line 1"
			+ Environment.NewLine
			+ "#template()"
			+ Environment.NewLine
			+ "Script line 2"
			+ Environment.NewLine;
			var expectedCode = script;

			// EXEC
			var processor = new ScriptPreprocessor(_language);
			var code = processor.Preprocess(script, template);
			var templateName = processor.GetTemplateName(script);

			// VERIFY
			Assert.AreEqual(expectedCode,
							code,
							"Code should match");
			Assert.AreEqual(null,
							templateName,
							"Template name");
		}


		[Test]
		public void MissingMarker()
		{
			// SETUP
			var template =
				"This is template line 1"
			+ Environment.NewLine
			+ "/* This has a misspelled marker $INSERT-SCRRRRRRIPT-HERE$ */"
			+ Environment.NewLine
			+ "This is template line 3";
			var script = "foo";

			// EXEC
			string msg = null;
			try
			{
				var processor = new ScriptPreprocessor(_language);
				processor.Preprocess(script, template);

				Assert.Fail("Should have thrown");
			}
			catch (ArgumentException ex)
			{
				msg = ex.Message;
			}

			// VERIFY
			Assert.AreEqual("Template does not contain $INSERT-SCRIPT-HERE$",
							msg,
							"Message should match");
		}


		[Test]
		public void NoTemplate()
		{
			// SETUP
			var template = @"";
			var script =
				"	Script line 1"
			+ Environment.NewLine
			+ "	Script line 2"
			+ Environment.NewLine;
			var expectedCode = script;

			// EXEC
			var processor = new ScriptPreprocessor(_language);
			var code = processor.Preprocess(script, template);
			var templateName = processor.GetTemplateName(script);

			// VERIFY
			Assert.AreEqual(expectedCode,
							code,
							"Code should match");
			Assert.AreEqual(null,
							templateName,
							"Template name");
		}


		[Test]
		public void PartialTemplateDeclaration()
		{
			// SETUP
			var template = @"";
			var script =
				"	Script line 1"
			+ Environment.NewLine
			+ "#template("
			+ Environment.NewLine
			+ "	Script line 2 (with parentheses)"
			+ Environment.NewLine;
			var expectedCode = script;

			// EXEC
			var processor = new ScriptPreprocessor(_language);
			var code = processor.Preprocess(script, template);
			var templateName = processor.GetTemplateName(script);

			// VERIFY
			Assert.AreEqual(expectedCode,
							code,
							"Code should match");
			Assert.AreEqual(null,
							templateName,
							"Template name");
		}


		private static void BuildWithExplicitTemplate(string script, string expectedTemplateName,
													  ScriptLanguage aLanguage)
		{
			var template =
				"This is template line 1"
			+ Environment.NewLine
			+ "/* This could be any kind of comment $INSERT-SCRIPT-HERE$ surrounding the marker */"
			+ Environment.NewLine
			+ "This is template line 3";
			var expectedCode =
				"This is template line 1"
			+ Environment.NewLine
			+ "	Script line 1"
			+ Environment.NewLine
			+ Environment.NewLine
			+ "	Script line 2"
			+ Environment.NewLine
			+ "This is template line 3"
			+ Environment.NewLine;


			// EXEC
			var processor = new ScriptPreprocessor(aLanguage);
			var code = processor.Preprocess(script, template);
			var templateName = processor.GetTemplateName(script);

			// VERIFY
			Assert.AreEqual(expectedTemplateName,
							templateName,
							"Template name should match");
			Assert.AreEqual(expectedCode,
							code,
							"Code should match");
		}


		private ScriptLanguage _language;
	}
}

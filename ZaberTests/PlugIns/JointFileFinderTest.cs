﻿using System.IO;
using NUnit.Framework;
using Rhino.Mocks;
using Zaber.PlugIns;

namespace ZaberTest.PlugIns
{
	[TestFixture]
	[SetCulture("en-US")]
	public class JointFileFinderTest
	{
		[Test]
		public void FindDefaultFile_ReturnsFileIfAnyFinderReturnsIt()
		{
			// SETUP
			var lanauge = MockRepository.GenerateStrictMock<ScriptLanguage>();
			var fileInfo = new FileInfo("a.txt");

			var finder1 = MockRepository.GenerateStrictMock<IFileFinder>();
			finder1.Stub(f => f.FindDefaultFile(lanauge)).Return(null);

			var finder2 = MockRepository.GenerateStrictMock<IFileFinder>();
			finder2.Stub(f => f.FindDefaultFile(lanauge)).Return(fileInfo);

			// EXEC
			var finder = new JointFileFinder(new[] { finder1, finder2 });

			var info = finder.FindDefaultFile(lanauge);

			// VERIFY
			Assert.AreSame(fileInfo, info);
		}


		[Test]
		public void FindDefaultFile_ReturnsNullIfAllFindersReturnNull()
		{
			// SETUP
			var lanauge = MockRepository.GenerateStrictMock<ScriptLanguage>();

			var finder1 = MockRepository.GenerateStrictMock<IFileFinder>();
			finder1.Stub(f => f.FindDefaultFile(lanauge)).Return(null);

			var finder2 = MockRepository.GenerateStrictMock<IFileFinder>();
			finder2.Stub(f => f.FindDefaultFile(lanauge)).Return(null);

			// EXEC
			var finder = new JointFileFinder(new[] { finder1, finder2 });

			var info = finder.FindDefaultFile(lanauge);

			// VERIFY
			Assert.IsNull(info);
		}


		[Test]
		public void FindDefaultFile_ReturnsResultFromFormerFinder()
		{
			// SETUP
			var lanauge = MockRepository.GenerateStrictMock<ScriptLanguage>();
			var fileInfo1 = new FileInfo("a.txt");
			var fileInfo2 = new FileInfo("b.txt");

			var finder1 = MockRepository.GenerateStrictMock<IFileFinder>();
			finder1.Stub(f => f.FindDefaultFile(lanauge)).Return(fileInfo1);

			var finder2 = MockRepository.GenerateStrictMock<IFileFinder>();
			finder2.Stub(f => f.FindDefaultFile(lanauge)).Return(fileInfo2);

			// EXEC
			var finderOrder1 = new JointFileFinder(new[] { finder1, finder2 });
			var finderOrder2 = new JointFileFinder(new[] { finder2, finder1 });

			var info1 = finderOrder1.FindDefaultFile(lanauge);
			var info2 = finderOrder2.FindDefaultFile(lanauge);

			// VERIFY
			Assert.AreSame(fileInfo1, info1);
			Assert.AreSame(fileInfo2, info2);
		}


		[Test]
		public void FindFile_ReturnsFileIfAnyFinderReturnsIt()
		{
			// SETUP
			const string TEST_FILE = "x.txt";
			var fileInfo = new FileInfo(TEST_FILE);

			var finder1 = MockRepository.GenerateStrictMock<IFileFinder>();
			finder1.Stub(f => f.FindFile(TEST_FILE)).Return(null);

			var finder2 = MockRepository.GenerateStrictMock<IFileFinder>();
			finder2.Stub(f => f.FindFile(TEST_FILE)).Return(fileInfo);

			// EXEC
			var finder = new JointFileFinder(new[] { finder1, finder2 });

			var info = finder.FindFile(TEST_FILE);

			// VERIFY
			Assert.AreSame(fileInfo, info);
		}


		[Test]
		public void FindFile_ReturnsNullIfAllFindersReturnNull()
		{
			// SETUP
			const string TEST_FILE = "x.txt";

			var finder1 = MockRepository.GenerateStrictMock<IFileFinder>();
			finder1.Stub(f => f.FindFile(TEST_FILE)).Return(null);

			var finder2 = MockRepository.GenerateStrictMock<IFileFinder>();
			finder2.Stub(f => f.FindFile(TEST_FILE)).Return(null);

			// EXEC
			var finder = new JointFileFinder(new[] { finder1, finder2 });

			var info = finder.FindFile(TEST_FILE);

			// VERIFY
			Assert.IsNull(info);
		}


		[Test]
		public void FindFile_ReturnsResultFromFormerFinder()
		{
			// SETUP
			const string TEST_FILE = "x.txt";
			var fileInfo1 = new FileInfo(Path.Combine("a", TEST_FILE));
			var fileInfo2 = new FileInfo(Path.Combine("b", TEST_FILE));

			var finder1 = MockRepository.GenerateStrictMock<IFileFinder>();
			finder1.Stub(f => f.FindFile(TEST_FILE)).Return(fileInfo1);

			var finder2 = MockRepository.GenerateStrictMock<IFileFinder>();
			finder2.Stub(f => f.FindFile(TEST_FILE)).Return(fileInfo2);

			// EXEC
			var finderOrder1 = new JointFileFinder(new[] { finder1, finder2 });
			var finderOrder2 = new JointFileFinder(new[] { finder2, finder1 });

			var info1 = finderOrder1.FindFile(TEST_FILE);
			var info2 = finderOrder2.FindFile(TEST_FILE);

			// VERIFY
			Assert.AreSame(fileInfo1, info1);
			Assert.AreSame(fileInfo2, info2);
		}
	}
}

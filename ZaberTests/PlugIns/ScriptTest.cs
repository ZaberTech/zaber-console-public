﻿using System;
using System.Diagnostics;
using System.IO;
using NUnit.Framework;
using Rhino.Mocks;
using Zaber.PlugIns;

namespace ZaberTest.PlugIns
{
	[TestFixture]
	[SetCulture("en-US")]
	[SetUICulture("en-US")]
	public class ScriptTest
	{
		[Test]
		public void Build()
		{
			// SETUP
			var content =
				@"
using System;
using System.IO;
using Zaber.PlugIns;

namespace ZaberTest.PlugIns
{
    class Default : PlugInBase
    {
        public override void Run()
        {
            Output.Write(""test output"");
        }
    }
}
";
			var extension = ".cs";
			var scriptFile = CreateTempFile(content, extension);

			try
			{
				// EXPECT
				var mocks = new MockRepository();
				var fileFinder = mocks.StrictMock<IFileFinder>();

				mocks.ReplayAll();

				// EXEC
				var script = new Script(scriptFile, fileFinder);
				var plugIn = script.Build();
				plugIn.Output = new StringWriter();
				plugIn.Run();
				var output = plugIn.Output.ToString();

				// VERIFY
				mocks.VerifyAll();

				Assert.AreEqual("test output",
								output,
								"output");
			}
			finally
			{
				DeleteTempFile(scriptFile);
			}
		}


		[Test]
		public void BuildWithError()
		{
			// SETUP
			var content =
				@"
using System;
using System.IO;
using Zaber.PlugIns;

namespace ZaberTest.PlugIns
{
    class Default : PlugInBase
    {
        public override void Run()
        {
            Output.WriteX(""test output""); // No such method as WriteX
        }
    }
}
";
			var extension = ".cs";
			var scriptFile = CreateTempFile(content, extension);

			try
			{
				// EXPECT
				var mocks = new MockRepository();
				var fileFinder = mocks.StrictMock<IFileFinder>();

				mocks.ReplayAll();

				// EXEC
				var script = new Script(scriptFile, fileFinder);
				string msg = null;
				try
				{
					script.Build();
					Assert.Fail("Should have thrown.");
				}
				catch (CompilerException ex)
				{
					msg = ex.Message;
				}

				var errors = script.CompilerErrors;

				// VERIFY
				mocks.VerifyAll();

				Assert.AreEqual(1,
								errors.Count,
								"number of errors/warnings");
				Assert.IsFalse(errors[0].IsWarning,
							   "IsWarning");
				Assert.IsTrue(errors[0]
						  .ErrorText.Contains("'System.IO.TextWriter' does not contain a definition for 'WriteX'"),
							  "compiler error message");
				Assert.AreEqual(12,
								errors[0].Line,
								"compiler error line number");
			}
			finally
			{
				DeleteTempFile(scriptFile);
			}
		}


		[Test]
		public void BuildWithTemplate()
		{
			// SETUP
			var templateContent =
				@"
using System;
using System.IO;
using Zaber.PlugIns;

namespace ZaberTest.PlugIns
{
    class Default : PlugInBase
    {
        public override void Run()
        {
            // $INSERT-SCRIPT-HERE$
        }
    }
}
";
			var extension = ".cs";
			var templateFile = CreateTempFile(templateContent, extension);
			var scriptContent = string.Format(@"
#template({0})
Output.Write(""test output"");
",
											  templateFile.Name);
			var scriptFile = CreateTempFile(scriptContent, extension);

			try
			{
				// EXPECT
				var mocks = new MockRepository();
				var fileFinder = mocks.StrictMock<IFileFinder>();

				Expect.Call(fileFinder.FindFile(templateFile.Name)).Return(templateFile);

				mocks.ReplayAll();

				// EXEC
				var script = new Script(scriptFile, fileFinder);
				var plugIn = script.Build();
				plugIn.Output = new StringWriter();
				plugIn.Run();
				var output = plugIn.Output.ToString();

				// VERIFY
				mocks.VerifyAll();

				Assert.AreEqual("test output",
								output,
								"output");
			}
			finally
			{
				DeleteTempFile(templateFile);
				DeleteTempFile(scriptFile);
			}
		}


		[Test]
		public void BuildWithTemplateAndError()
		{
			// SETUP
			var templateContent =
				@"
using System;
using System.IO;
using Zaber.PlugIns;

namespace ZaberTest.PlugIns
{
    class Default : PlugInBase
    {
        public override void Run()
        {
            // $INSERT-SCRIPT-HERE$
        }
    }
}
";
			var extension = ".cs";
			var templateFile = CreateTempFile(templateContent, extension);
			var scriptContent = string.Format(@"
#template({0})
Output.WriteX(""test output""); // No such method as WriteX
",
											  templateFile.Name);
			var scriptFile = CreateTempFile(scriptContent, extension);

			try
			{
				// EXPECT
				var mocks = new MockRepository();
				var fileFinder = mocks.StrictMock<IFileFinder>();

				Expect.Call(fileFinder.FindFile(templateFile.Name)).Return(templateFile);

				mocks.ReplayAll();

				// EXEC
				var script = new Script(scriptFile, fileFinder);
				try
				{
					script.Build();
					Assert.Fail("Should have thrown.");
				}
				catch (CompilerException)
				{
					// expected.
				}

				var errors = script.CompilerErrors;

				// VERIFY
				mocks.VerifyAll();

				Assert.AreEqual(1,
								errors.Count,
								"number of errors/warnings");
				Assert.AreEqual(3,
								errors[0].Line,
								"compiler error line number");
			}
			finally
			{
				DeleteTempFile(scriptFile);
			}
		}


		[Test]
		public void BuildWithUnknownTemplate()
		{
			// SETUP
			var content = "#template(Foo.txt)";
			var extension = ".cs";
			var scriptFile = CreateTempFile(content, extension);

			try
			{
				// EXPECT
				var mocks = new MockRepository();
				var fileFinder = mocks.StrictMock<IFileFinder>();

				// tries to load template once during constructor
				Expect.Call(fileFinder.FindFile("Foo.txt")).Return(null);

				// then tries again during Build()
				Expect.Call(fileFinder.FindFile("Foo.txt")).Return(null);

				mocks.ReplayAll();

				// EXEC
				var script = new Script(scriptFile, fileFinder);
				string msg = null;
				try
				{
					script.Build();
					Assert.Fail("Should have thrown.");
				}
				catch (FileNotFoundException ex)
				{
					msg = ex.Message;
				}

				var errors = script.CompilerErrors;

				// VERIFY
				mocks.VerifyAll();

				Assert.AreEqual("Template file not found: 'Foo.txt'.",
								msg,
								"Exception message");
				Assert.AreEqual(0,
								errors.Count,
								"number of errors/warnings");
			}
			finally
			{
				DeleteTempFile(scriptFile);
			}
		}


		[Test]
		public void BuildWithWarning()
		{
			// SETUP
			var content =
				@"
using System;
using System.IO;
using Zaber.PlugIns;

namespace ZaberTest.PlugIns
{
    class Default : PlugInBase
    {
        public override void Run()
        {
            String foo = ""Foo"";
            Output.Write(""test output"");
        }
    }
}
";
			var extension = ".cs";
			var scriptFile = CreateTempFile(content, extension);

			try
			{
				// EXPECT
				var mocks = new MockRepository();
				var fileFinder = mocks.StrictMock<IFileFinder>();

				mocks.ReplayAll();

				// EXEC
				var script = new Script(scriptFile, fileFinder);
				var plugIn = script.Build();
				var errors = script.CompilerErrors;
				plugIn.Output = new StringWriter();
				plugIn.Run();
				var output = plugIn.Output.ToString();

				// VERIFY
				mocks.VerifyAll();

				Assert.AreEqual("test output",
								output,
								"output");
				Assert.AreEqual(1,
								errors.Count,
								"number of errors/warnings");
				Assert.IsTrue(errors[0].IsWarning,
							  "IsWarning");
				Assert.AreEqual("The variable 'foo' is assigned but its value is never used",
								errors[0].ErrorText,
								"message");
			}
			finally
			{
				DeleteTempFile(scriptFile);
			}
		}


		[Test]
		public void LanguageCSharp()
		{
			// SETUP
			var content = "Script text";
			var extension = ".cs";
			var scriptFile = CreateTempFile(content, extension);

			try
			{
				// EXPECT
				var mocks = new MockRepository();
				var fileFinder = mocks.StrictMock<IFileFinder>();

				mocks.ReplayAll();

				// EXEC
				var script = new Script(scriptFile, fileFinder);
				var language = script.Language;

				// VERIFY
				mocks.VerifyAll();
				Assert.AreEqual("csharp",
								language.Name,
								"language");
			}
			finally
			{
				DeleteTempFile(scriptFile);
			}
		}


		[Test]
		public void LanguageVB()
		{
			// SETUP
			var content = "Script text";
			var extension = ".vb";
			var scriptFile = CreateTempFile(content, extension);

			try
			{
				// EXPECT
				var mocks = new MockRepository();
				var fileFinder = mocks.StrictMock<IFileFinder>();

				mocks.ReplayAll();

				// EXEC
				var script = new Script(scriptFile, fileFinder);
				var language = script.Language;

				// VERIFY
				mocks.VerifyAll();
				Assert.AreEqual("visualbasic",
								language.Name,
								"language");
			}
			finally
			{
				DeleteTempFile(scriptFile);
			}
		}


		[Test]
		public void LoadNoTemplate()
		{
			// SETUP
			var content = "Script without template";
			var extension = ".cs";
			var scriptFile = CreateTempFile(content, extension);

			try
			{
				// EXPECT
				var mocks = new MockRepository();
				var fileFinder = mocks.StrictMock<IFileFinder>();

				mocks.ReplayAll();

				// EXEC
				var script = new Script(scriptFile, fileFinder);
				var loadedTemplate = script.Template;

				// VERIFY
				mocks.VerifyAll();

				Assert.IsNull(loadedTemplate.File,
							  "template file");
			}
			finally
			{
				DeleteTempFile(scriptFile);
			}
		}


		[Test]
		public void LoadTemplate()
		{
			// SETUP
			var content = "#template(Foo.txt)";
			var extension = ".cs";
			var scriptFile = CreateTempFile(content, extension);

			try
			{
				// EXPECT
				var templateFile = new FileInfo("bogusFolder/Foo.txt");
				var mocks = new MockRepository();
				var fileFinder = mocks.StrictMock<IFileFinder>();

				Expect.Call(fileFinder.FindFile("Foo.txt")).Return(templateFile);

				mocks.ReplayAll();

				// EXEC
				var script = new Script(scriptFile, fileFinder);
				var loadedTemplate = script.Template;

				// VERIFY
				mocks.VerifyAll();

				Assert.AreEqual(templateFile,
								loadedTemplate.File,
								"template file");
			}
			finally
			{
				DeleteTempFile(scriptFile);
			}
		}


		[Test]
		public void LoadTemplateWithExtension()
		{
			// SETUP
			var content = "#template(Foo)";
			var extension = ".cs";
			var scriptFile = CreateTempFile(content, extension);

			try
			{
				// EXPECT
				var templateFile = new FileInfo("bogusFolder/Foo.txt");
				var mocks = new MockRepository();
				var fileFinder = mocks.StrictMock<IFileFinder>();

				Expect.Call(fileFinder.FindFile("Foo")).Return(null);
				Expect.Call(fileFinder.FindFile("Foo.cs")).Return(templateFile);

				mocks.ReplayAll();

				// EXEC
				var script = new Script(scriptFile, fileFinder);
				var loadedTemplate = script.Template;

				// VERIFY
				mocks.VerifyAll();

				Assert.AreEqual(templateFile,
								loadedTemplate.File,
								"template file");
			}
			finally
			{
				DeleteTempFile(scriptFile);
			}
		}


		/// <summary>
		///     We don't complain at load time about unknown templates. Wait
		///     until Build() gets called.
		/// </summary>
		[Test]
		public void LoadUnknownTemplate()
		{
			// SETUP
			var content = "#template(Foo.txt)";
			var extension = ".cs";
			var scriptFile = CreateTempFile(content, extension);

			try
			{
				// EXPECT
				var mocks = new MockRepository();
				var fileFinder = mocks.StrictMock<IFileFinder>();

				Expect.Call(fileFinder.FindFile("Foo.txt")).Return(null);

				mocks.ReplayAll();

				// EXEC
				var script = new Script(scriptFile, fileFinder);
				var loadedTemplate = script.Template;
				var templateFile = loadedTemplate.File;
				var templateText = script.Template.Text;

				// VERIFY
				mocks.VerifyAll();

				Assert.AreEqual("",
								templateText,
								"template text");
				Assert.IsNull(templateFile,
							  "template file");
			}
			finally
			{
				DeleteTempFile(scriptFile);
			}
		}


		[Test]
		public void NewScriptWithNoFile()
		{
			// SETUP
			var language =
				ScriptLanguage.FindByExtension(new FileInfo("foo.cs"));

			// EXPECT
			var mocks = new MockRepository();
			var fileFinder = mocks.StrictMock<IFileFinder>();

			mocks.ReplayAll();

			// EXEC
			var script = new Script(language, fileFinder);
			var text = script.Text;
			var scriptFile = script.File;

			// VERIFY
			mocks.VerifyAll();

			Assert.AreEqual("",
							text,
							"script text");
			Assert.IsNull(scriptFile,
						  "script file");
		}


		[Test]
		public void NullFile()
		{
			// SETUP
			// EXPECT
			var mocks = new MockRepository();
			var fileFinder = mocks.StrictMock<IFileFinder>();

			mocks.ReplayAll();

			// EXEC
			string msg = null;
			try
			{
				new Script((FileInfo) null, fileFinder);
				Assert.Fail("Should have thrown");
			}
			catch (ArgumentNullException ex)
			{
				msg = ex.Message;
			}

			// VERIFY
			mocks.VerifyAll();

			Assert.AreEqual("Value cannot be null.\r\nParameter name: aFileInfo",
							msg,
							"message");
		}


		[Test]
		public void ReadText()
		{
			// SETUP
			var content = "Script text";
			var extension = ".cs";
			var scriptFile = CreateTempFile(content, extension);

			try
			{
				// EXPECT
				var mocks = new MockRepository();
				var fileFinder = mocks.StrictMock<IFileFinder>();

				mocks.ReplayAll();

				// EXEC
				var script = new Script(scriptFile, fileFinder);
				var text = script.Text;
				var isDirty = script.IsDirty;

				// VERIFY
				mocks.VerifyAll();
				Assert.AreEqual(content,
								text,
								"text of script");
				Assert.AreEqual(false,
								isDirty,
								"isDirty");
			}
			finally
			{
				DeleteTempFile(scriptFile);
			}
		}


		[Test]
		public void RemoveTemplateExtension()
		{
			// SETUP
			var content = "Script text\r\n#template(Foo.cs)";
			var newContent = "new text\r\n#template(Foo)";
			var extension = ".cs";
			var scriptFile = CreateTempFile(content, extension);

			try
			{
				// EXPECT
				var template1File = new FileInfo("bogusFolder/Foo.cs");
				var mocks = new MockRepository();
				var fileFinder = mocks.StrictMock<IFileFinder>();

				Expect.Call(fileFinder.FindFile("Foo.cs")).Return(template1File);

				mocks.ReplayAll();

				// EXEC
				var script = new Script(scriptFile, fileFinder);
				script.Text = newContent;
				var newTemplate = script.Template;

				// VERIFY
				mocks.VerifyAll();
				Assert.AreEqual(template1File,
								newTemplate.File,
								"new template file");
			}
			finally
			{
				DeleteTempFile(scriptFile);
			}
		}


		[Test]
		public void Save()
		{
			// SETUP
			var content = "Old content";
			var newContent = "New content";
			var extension = ".cs";
			var scriptFile = CreateTempFile(content, extension);

			try
			{
				// EXPECT
				var mocks = new MockRepository();
				var fileFinder = mocks.StrictMock<IFileFinder>();

				mocks.ReplayAll();

				// EXEC
				var script = new Script(scriptFile, fileFinder) { Text = newContent };
				script.Save();
				var isDirty = script.IsDirty;

				// VERIFY
				mocks.VerifyAll();

				VerifyTempFile(scriptFile, newContent);

				Assert.AreEqual(false,
								isDirty,
								"isDirty");
			}
			finally
			{
				DeleteTempFile(scriptFile);
			}
		}


		[Test]
		public void SaveAs()
		{
			// SETUP
			var content = "Old content";
			var newContent = "New content";
			var extension = ".cs";
			var scriptFile = CreateTempFile(content, extension);
			var newFile = CreateTempFile("", extension);

			try
			{
				// EXPECT
				var mocks = new MockRepository();
				var fileFinder = mocks.StrictMock<IFileFinder>();

				mocks.ReplayAll();

				// EXEC
				var script = new Script(scriptFile, fileFinder);
				script.Text = newContent;
				script.SaveAs(newFile);
				var isDirty = script.IsDirty;

				// VERIFY
				mocks.VerifyAll();

				VerifyTempFile(scriptFile, content);
				VerifyTempFile(newFile, newContent);

				Assert.AreEqual(false,
								isDirty,
								"isDirty");
			}
			finally
			{
				DeleteTempFile(scriptFile);
			}
		}


		[Test]
		public void SwitchFromTemplateWithStrangeExtensionToNoExtension()
		{
			// SETUP
			var content = "Script text\r\n#template(Foo.txt)";
			var newContent = "new text\r\n#template(Foo)";
			var extension = ".cs";
			var scriptFile = CreateTempFile(content, extension);

			try
			{
				// EXPECT
				var template1File = new FileInfo("bogusFolder/Foo.txt");
				var template2File = new FileInfo("bogusFolder/Foo.cs");
				var mocks = new MockRepository();
				var fileFinder = mocks.StrictMock<IFileFinder>();

				Expect.Call(fileFinder.FindFile("Foo.txt")).Return(template1File);
				Expect.Call(fileFinder.FindFile("Foo")).Return(null);
				Expect.Call(fileFinder.FindFile("Foo.cs")).Return(template2File);

				mocks.ReplayAll();

				// EXEC
				var script = new Script(scriptFile, fileFinder);
				script.Text = newContent;
				var newTemplate = script.Template;

				// VERIFY
				mocks.VerifyAll();
				Assert.AreEqual(template2File,
								newTemplate.File,
								"new template file");
			}
			finally
			{
				DeleteTempFile(scriptFile);
			}
		}


		[Test]
		public void WriteSameText()
		{
			// SETUP
			var content = "Script text";
			var extension = ".cs";
			var scriptFile = CreateTempFile(content, extension);

			try
			{
				// EXPECT
				var mocks = new MockRepository();
				var fileFinder = mocks.StrictMock<IFileFinder>();

				mocks.ReplayAll();

				// EXEC
				var script = new Script(scriptFile, fileFinder);
				script.Text = content;
				var isDirty = script.IsDirty;

				// VERIFY
				mocks.VerifyAll();
				Assert.AreEqual(false,
								isDirty,
								"isDirty");
			}
			finally
			{
				DeleteTempFile(scriptFile);
			}
		}


		[Test]
		public void WriteText()
		{
			// SETUP
			var content = "Script text";
			var expectedContent = "new text";
			var extension = ".cs";
			var scriptFile = CreateTempFile(content, extension);

			try
			{
				// EXPECT
				var mocks = new MockRepository();
				var fileFinder = mocks.StrictMock<IFileFinder>();

				mocks.ReplayAll();

				// EXEC
				var script = new Script(scriptFile, fileFinder);
				script.Text = expectedContent;
				var text = script.Text;
				var isDirty = script.IsDirty;

				// VERIFY
				mocks.VerifyAll();
				Assert.AreEqual(expectedContent,
								text,
								"text of script");
				Assert.AreEqual(true,
								isDirty,
								"isDirty");
			}
			finally
			{
				DeleteTempFile(scriptFile);
			}
		}


		[Test]
		public void WriteTextWithNewTemplate()
		{
			// SETUP
			var content = "Script text\r\n#template(Foo.txt)";
			var newContent = "new text\r\n#template(Bar.txt)";
			var extension = ".cs";
			var scriptFile = CreateTempFile(content, extension);

			try
			{
				// EXPECT
				var template1File = new FileInfo("bogusFolder/Foo.txt");
				var template2File = new FileInfo("bogusFolder/Bar.txt");
				var mocks = new MockRepository();
				var fileFinder = mocks.StrictMock<IFileFinder>();

				Expect.Call(fileFinder.FindFile("Foo.txt")).Return(template1File);
				Expect.Call(fileFinder.FindFile("Bar.txt")).Return(template2File);

				mocks.ReplayAll();

				// EXEC
				var script = new Script(scriptFile, fileFinder);
				script.Text = newContent;
				var newTemplate = script.Template;

				// VERIFY
				mocks.VerifyAll();
				Assert.AreEqual(template2File,
								newTemplate.File,
								"new template file");
			}
			finally
			{
				DeleteTempFile(scriptFile);
			}
		}


		[Test]
		public void WriteTextWithSameTemplate()
		{
			// SETUP
			var content = "Script text\r\n#template(Foo.txt)";
			var newContent = "new text\r\n#template(Foo.txt)";
			var extension = ".cs";
			var scriptFile = CreateTempFile(content, extension);

			try
			{
				// EXPECT
				var template1File = new FileInfo("bogusFolder/Foo.txt");
				var mocks = new MockRepository();
				var fileFinder = mocks.StrictMock<IFileFinder>();

				Expect.Call(fileFinder.FindFile("Foo.txt")).Return(template1File);

				mocks.ReplayAll();

				// EXEC
				var script = new Script(scriptFile, fileFinder);
				script.Text = newContent;
				var newTemplate = script.Template;

				// VERIFY
				mocks.VerifyAll();
				Assert.AreEqual(template1File,
								newTemplate.File,
								"new template file");
			}
			finally
			{
				DeleteTempFile(scriptFile);
			}
		}


		private void VerifyTempFile(FileInfo tempFile, string expectedContent)
		{
			using (var reader = tempFile.OpenText())
			{
				var actualContent = reader.ReadToEnd();
				Assert.AreEqual(expectedContent,
								actualContent,
								"Contents of file " + tempFile.Name);
			}
		}


		private void DeleteTempFile(FileInfo tempFile)
		{
			try
			{
				tempFile.Delete();
				var tempFilename = tempFile.FullName;
				var baseFilename = tempFilename.Substring(0,
														  tempFilename.Length - tempFile.Extension.Length);
				File.Delete(baseFilename);
			}
			catch (Exception e)
			{
				// Swallow exception when deleting temp files.
				Debug.WriteLine(e);
			}
		}


		private static FileInfo CreateTempFile(string content, string extension)
		{
			var basefileName = Path.GetTempFileName();
			var tempFile = new FileInfo(basefileName + extension);
			using (var writer = new StreamWriter(tempFile.FullName))
			{
				writer.Write(content);
			}

			return tempFile;
		}
	}
}

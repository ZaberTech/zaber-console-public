using System;
using System.CodeDom.Compiler;
using System.IO;
using System.Text;
using IronPython.Hosting;
using NUnit.Framework;
using Zaber.PlugIns;

namespace ZaberTest.PlugIns
{
	[TestFixture]
	[SetCulture("en-US")]
	public class ScriptLanguageTest
	{
		[Test]
		public void CompiledFileFilter()
		{
			// SETUP
			ScriptLanguage language = new CompiledScriptLanguage(CodeDomProvider.GetCompilerInfo("c#"));

			// EXEC
			var filter = language.FileFilter;

			// VERIFY
			Assert.AreEqual("csharp files (*.cs)|*.cs",
							filter,
							"Filter should match");
		}


		[Test]
		[Sequential]
		public void CreateCompiledPlugIn([Values("Foo.cs", "Foo.py")] string fileName,
										 [Values(@"
class Simple : Zaber.PlugIns.PlugInBase
{
    public override void Run()
    {
        Output.WriteLine(""The rain in Spain"");
    }
}
",
											 @"
print 'The rain in Spain'
")]
										 string sourceCode)
		{
			// EXEC
			var language =
				ScriptLanguage.FindByExtension(new FileInfo(fileName));
			IPlugIn plugIn = null;
			try
			{
				plugIn = language.CreatePlugIn(sourceCode);
			}
			catch (ArgumentException ex)
			{
				var errors = language.CompilerErrors;
				var summary = new StringWriter();
				summary.WriteLine(ex.Message);
				summary.WriteLine();
				foreach (var error in errors)
				{
					summary.WriteLine(error);
				}

				Assert.Fail(summary.ToString());
			}

			plugIn.Output = new StringWriter();
			plugIn.Run();
			var outputText = plugIn.Output.ToString();

			// VERIFY
			Assert.That(outputText,
						Is.EqualTo("The rain in Spain\r\n"),
						"output text");
		}


		[Test]
		public void DynamicFileFilter()
		{
			// SETUP
			ScriptLanguage language = new DynamicScriptLanguage(Python.CreateEngine());

			// EXEC
			var filter = language.FileFilter;

			// VERIFY
			Assert.AreEqual("IronPython files (*.py)|*.py",
							filter,
							"Filter should match");
		}


		[Test]
		public void Equals()
		{
			// SETUP
			var file = new FileInfo("Foo.cs");

			// EXEC
			var language1 = ScriptLanguage.FindByExtension(file);
			var language2 = ScriptLanguage.FindByExtension(file);

			var areEqual = language1.Equals(language2);

			// VERIFY
			Assert.IsTrue(areEqual,
						  "Equals() should return true");
		}


		[Test]
		public void FindAll()
		{
			// EXEC
			var languages = ScriptLanguage.FindAll();

			// VERIFY
			Assert.AreNotEqual(0,
							   languages.Count,
							   "Should find at least one language");
			var names = new StringBuilder();
			var isCSharpFound = false;
			var isPythonFound = false;
			foreach (var language in languages)
			{
				if (names.Length > 0)
				{
					names.Append(", ");
				}

				names.Append(language.Name);

				if (language.Name.Equals("csharp", StringComparison.CurrentCultureIgnoreCase))
				{
					isCSharpFound = true;
				}

				if (language.Name.Equals("IronPython", StringComparison.CurrentCultureIgnoreCase))
				{
					isPythonFound = true;
				}
			}

			Assert.IsTrue(isCSharpFound,
						  "Should have found CSharp, but only found " + names);
			Assert.IsTrue(isPythonFound,
						  "Should have found Python, but only found " + names);
		}


		[Test]
		public void FindCompiledLanguageByExtension()
		{
			FileInfo file = new FileInfo("Foo.cs");
			ScriptLanguage language = ScriptLanguage.FindByExtension(file);
			Assert.AreEqual("csharp", language.Name, "Language should match");

			file = new FileInfo("Foo.CS");
			language = ScriptLanguage.FindByExtension(file);
			Assert.AreEqual("csharp", language.Name, "Language should match");
		}


		[Test]
		public void FindDynamicLanguageByExtension()
		{
			FileInfo file = new FileInfo("Foo.py");
			ScriptLanguage language = ScriptLanguage.FindByExtension(file);
			Assert.AreEqual("IronPython", language.Name, "Language should match");

			file = new FileInfo("Foo.PY");
			language = ScriptLanguage.FindByExtension(file);
			Assert.AreEqual("IronPython", language.Name, "Language should match");
		}


		[Test]
		public void Hashcode()
		{
			// SETUP
			var file = new FileInfo("Foo.cs");

			// EXEC
			var language1 = ScriptLanguage.FindByExtension(file);
			var language2 = ScriptLanguage.FindByExtension(file);

			var hashCode1 = language1.GetHashCode();
			var hashCode2 = language2.GetHashCode();

			// VERIFY
			Assert.AreEqual(hashCode1,
							hashCode2,
							"hash code");
		}


		[Test]
		public void UnknownExtension()
		{
			// SETUP
			var file = new FileInfo("Foo.x23");

			// EXEC
			string msg = null;
			try
			{
				ScriptLanguage.FindByExtension(file);

				Assert.Fail("Should have thrown");
			}
			catch (ArgumentException ex)
			{
				msg = ex.Message;
			}

			// VERIFY
			Assert.AreEqual("No compiler found for file type .x23",
							msg,
							"Message should match");
		}
	}
}

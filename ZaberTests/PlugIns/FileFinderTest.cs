using System.CodeDom.Compiler;
using System.IO;
using NUnit.Framework;
using Zaber.PlugIns;

namespace ZaberTest.PlugIns
{
	[TestFixture]
	[SetCulture("en-US")]
	public class FileFinderTest
	{
		[SetUp]
		public void SetUp() => workingFolder = null;


		[TearDown]
		public void TearDown()
		{
			if (workingFolder != null)
			{
				workingFolder.Delete(true);
			}
		}


		[Test]
		public void DefaultTemplate()
		{
			// SETUP
			workingFolder = CreateTempFolder();
			var templates = workingFolder.CreateSubdirectory("Templates");
			TouchFile(templates, "ATemplate.cs");
			var file = TouchFile(templates, "DefAUlt.cs");
			ScriptLanguage language =
				new CompiledScriptLanguage(CodeDomProvider.GetCompilerInfo("c#"));

			// EXEC
			var finder = new FileFinder(workingFolder, "Templates");
			var result = finder.FindDefaultFile(language);

			// VERIFY
			Assert.AreEqual(file.FullName,
							result.FullName,
							"Template should match");
		}


		[Test]
		public void FileByName()
		{
			// SETUP
			workingFolder = CreateTempFolder();
			var scripts = workingFolder.CreateSubdirectory("Scripts");
			var file = TouchFile(scripts, "AScript.cs");
			TouchFile(scripts, "Default.cs");

			// EXEC
			var finder = new FileFinder(workingFolder, "Scripts");
			var result = finder.FindFile("AScript.cs");

			// VERIFY
			Assert.AreEqual(file.FullName,
							result.FullName,
							"File should match");
		}


		[Test]
		public void FindStartingFolder()
		{
			// SETUP
			workingFolder = CreateTempFolder();
			var targetName = "TargetX27";
			var target = workingFolder.CreateSubdirectory(targetName);

			// EXEC
			var finder = new FileFinder(target, targetName);
			var result = finder.Directory;

			// VERIFY
			Assert.AreEqual(target.FullName,
							result.FullName,
							"Target folder should match");
		}


		[Test]
		public void LocalFolder()
		{
			// SETUP
			workingFolder = CreateTempFolder();
			var targetName = "TargetX27";
			var target = workingFolder.CreateSubdirectory(targetName);

			// EXEC
			var finder = new FileFinder(workingFolder, targetName);
			var result = finder.Directory;

			// VERIFY
			Assert.AreEqual(target.FullName,
							result.FullName,
							"Target folder should match");
		}


		[Test]
		public void NoDefaultTemplate()
		{
			// SETUP
			workingFolder = CreateTempFolder();
			var templates = workingFolder.CreateSubdirectory("Templates");
			TouchFile(templates, "ATemplate.cs");
			TouchFile(templates, "SomeOtherTemplate.cs");
			ScriptLanguage language =
				new CompiledScriptLanguage(CodeDomProvider.GetCompilerInfo("c#"));

			// EXEC
			var finder = new FileFinder(workingFolder, "Templates");
			var result = finder.FindDefaultFile(language);

			// VERIFY
			Assert.AreEqual(null,
							result,
							"Should not find any template");
		}


		[Test]
		public void NoTemplateFile()
		{
			// SETUP
			workingFolder = CreateTempFolder();
			var templates = workingFolder.CreateSubdirectory("Templates");
			ScriptLanguage language =
				new CompiledScriptLanguage(CodeDomProvider.GetCompilerInfo("c#"));

			// EXEC
			var finder = new FileFinder(workingFolder, "Templates");
			var result = finder.FindDefaultFile(language);

			// VERIFY
			Assert.IsNull(result,
						  "Template should be null");
		}


		[Test]
		public void NoTemplateFolder()
		{
			// SETUP
			workingFolder = CreateTempFolder();
			var templates = workingFolder.CreateSubdirectory("Junk");
			ScriptLanguage language =
				new CompiledScriptLanguage(CodeDomProvider.GetCompilerInfo("c#"));

			// EXEC
			var finder = new FileFinder(workingFolder, "TargetX27");
			var folder = finder.Directory; // Shouldn't exist
			var templateFile = finder.FindDefaultFile(language);

			// VERIFY
			Assert.IsNull(folder == null ? null : folder.FullName,
						  "Template folder should be null");
			Assert.IsNull(templateFile,
						  "Template should be null");
		}


		[Test]
		public void OneTemplate()
		{
			// SETUP
			workingFolder = CreateTempFolder();
			var templates = workingFolder.CreateSubdirectory("Templates");
			TouchFile(templates, "Sample.cs");
			ScriptLanguage language =
				new CompiledScriptLanguage(CodeDomProvider.GetCompilerInfo("c#"));

			// EXEC
			var finder = new FileFinder(workingFolder, "Templates");
			var result = finder.FindDefaultFile(language);

			// VERIFY
			Assert.AreEqual(null,
							result,
							"No template should match");
		}


		[Test]
		public void ThreeLevels()
		{
			// SETUP
			workingFolder = CreateTempFolder();
			var templates = workingFolder.CreateSubdirectory("Templates");
			var bin = workingFolder.CreateSubdirectory("bin");
			var debug = bin.CreateSubdirectory("debug");

			// EXEC
			var finder = new FileFinder(debug, "Templates");
			var result = finder.Directory;

			// VERIFY
			Assert.AreEqual(templates.FullName,
							result.FullName,
							"Template folder should match");
		}


		private FileInfo TouchFile(DirectoryInfo dir, string fileName)
		{
			var file = new FileInfo(Path.Combine(dir.FullName, fileName));
			using (var stream = file.AppendText())
			{
			}

			return file;
		}


		private DirectoryInfo CreateTempFolder()
		{
			var tempPath = Path.GetTempPath();
			var fileName = Path.GetRandomFileName();
			var dir = new DirectoryInfo(Path.Combine(tempPath, fileName));
			Assert.IsFalse(dir.Exists,
						   "Directory should not exist: " + dir.FullName);
			dir.Create();
			return dir;
		}


		private DirectoryInfo workingFolder;
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using NUnit.Framework;
using Rhino.Mocks;
using Rhino.Mocks.Interfaces;
using Zaber;
using Zaber.Units;
using ZaberTest.Testing;
using Measurement = Zaber.Measurement;

namespace ZaberTest
{
	/// <summary>
	///     Remaining tests:
	///     Missing derivatives on step size uom
	/// </summary>
	[TestFixture]
	[Category("active")]
	[SetCulture("en-US")]
	[SetUICulture("en-US")]
	public class ZaberDeviceTest
	{
		[SetUp]
		public void SetUp()
		{
			_mocks = new MockRepository();
			_port = _mocks.StrictMock<IZaberPort>();

			_port.IsAsciiMode = false;
			LastCall.PropertyBehavior();
			_port.IsAsciiMode = false;

			_port.DataPacketReceived += null;
			_packetReceivedRaiser =
				LastCall.IgnoreArguments().Repeat.Any().GetEventRaiser();
			_port.DataPacketSent += null;
			_packetSentRaiser =
				LastCall.IgnoreArguments().Repeat.Any().GetEventRaiser();

			_device = new ZaberDevice();

			_eventSource = null;
			_eventArgs = null;
			_eventType = null;
			_device.MessageSent += OnDeviceMessageSent;
			_device.MessageReceived += OnDeviceMessageReceived;

			_device.DeviceNumber = 17;
			_device.DeviceType = DeviceTypeMother.CreateDeviceTypeWithCommands();
		}


		[Test]
		public void CalculateAccelerationData()
		{
			// SETUP
			var deviceType = new DeviceType
			{
				MotionType = MotionType.Linear,
				Commands = new List<CommandInfo>
				{
					new SettingInfo
					{
						Command = Command.SetAcceleration,
						RequestUnit = UnitOfMeasure.MetersPerSecondSquared,
						RequestUnitScale = 0.000001m,
						RequestUnitFunction = ScalingFunction.LinearResolution
					}
				}
			};
			var device = new ZaberDevice { DeviceType = deviceType };
			const int resolution = ZaberDevice.DefaultMicrostepResolution;
			var measurement =
				new Measurement(2m, UnitOfMeasure.MillimetersPerSecondSquared);
			var expectedData = (int) Math.Round(((2 / 0.1) * resolution) / 11250);

			// EXEC
			var actualData = device.CalculateData(Command.SetAcceleration,
												  measurement);

			// VERIFY
			Assert.That(actualData,
						Is.EqualTo(expectedData),
						"data");
		}


		[Test]
		public void CalculateDataThrowsExceptionOnTooLargeNumber()
		{
			// SETUP
			var deviceType = new DeviceType
			{
				MotionType = MotionType.Linear,
				Commands = new List<CommandInfo>
				{
					new CommandInfo
					{
						Command = Command.MoveAbsolute,
						RequestUnit = UnitOfMeasure.Meter,
						RequestUnitScale = 10000m,
						RequestUnitFunction = ScalingFunction.LinearResolution
					}
				}
			};
			var device = new ZaberDevice { DeviceType = deviceType };
			var measurement = new Measurement((decimal) int.MaxValue + 1, UnitOfMeasure.Millimeter);

			// EXEC & VERIFY
			Assert.Throws<ConversionException>(() =>
			{
				var actualData = device.CalculateData(Command.MoveAbsolute,
													  measurement);
			});
		}


		[Test]
		public void CalculateDataThrowsExceptionOnTooLargeNumberWithId()
		{
			// SETUP
			_device.AreMessageIdsEnabled = true;
			_device.Port = _port;

			// EXEC & VERIFY
			Assert.Throws<ConversionException>(() => { _device.Send(Command.EchoData, 1 << 24); });
		}


		[Test]
		public void CalculateDataWithoutUnits()
		{
			// SETUP
			var device = CreateDeviceWithCommands();
			var measurement = new Measurement(200m, UnitOfMeasure.Data);
			const int expectedData = 200;

			// EXEC
			var actualData = device.CalculateData(Command.MoveAbsolute,
												  measurement);

			// VERIFY
			Assert.That(actualData,
						Is.EqualTo(expectedData),
						"data");
		}


		[Test]
		public void CalculateOtherMeasurementTypeWithUnitOfMeasure()
		{
			// SETUP
			var deviceType = new DeviceType { MotionType = MotionType.Linear };
			var device = new ZaberDevice { DeviceType = deviceType };
			var measurement = new Measurement(50m, UnitOfMeasure.Millimeter);

			// EXEC
			var ex = Assert.Throws(typeof(InvalidOperationException),
								   delegate
								   {
									   device.CalculateData(Command.MoveAbsolute,
															measurement);
								   });

			// VERIFY
			Assert.That(ex.Message,
						Is.EqualTo("Unit of measure conversion is only allowed on known commands."),
						"error meassage");
		}


		[Test]
		public void CalculatePositionData()
		{
			// SETUP
			var deviceType = new DeviceType
			{
				MotionType = MotionType.Linear,
				Commands = new List<CommandInfo>
				{
					new CommandInfo
					{
						Command = Command.MoveAbsolute,
						RequestUnit = UnitOfMeasure.Meter,
						RequestUnitScale = 10000m,
						RequestUnitFunction = ScalingFunction.LinearResolution
					}
				}
			};
			var device = new ZaberDevice { DeviceType = deviceType };
			var measurement = new Measurement(2m, UnitOfMeasure.Millimeter);
			const int resolution = ZaberDevice.DefaultMicrostepResolution;
			const int expectedData = (int) ((2 / 0.1) * resolution);

			// EXEC
			var actualData = device.CalculateData(Command.MoveAbsolute,
												  measurement);

			// VERIFY
			Assert.That(actualData,
						Is.EqualTo(expectedData),
						"data");
		}


		[Test]
		public void CalculateVelocityData()
		{
			// SETUP
			var deviceType = new DeviceType
			{
				MotionType = MotionType.Linear,
				Commands = new[]
				{
					new CommandInfo
					{
						Command = Command.MoveAtConstantSpeed,
						RequestUnit = UnitOfMeasure.MetersPerSecond,
						RequestUnitScale = 1066.66666666666666667m,
						RequestUnitFunction = ScalingFunction.LinearResolution
					}
				}
			};
			var device = new ZaberDevice { DeviceType = deviceType };
			const int resolution = ZaberDevice.DefaultMicrostepResolution;
			var measurement = new Measurement(2m, UnitOfMeasure.MillimetersPerSecond);
			var expectedData = (int) Math.Round(((2 / 0.1) * resolution) / 9.375);

			// EXEC
			var actualData = device.CalculateData(Command.MoveAtConstantSpeed,
												  measurement);

			// VERIFY
			Assert.That(actualData,
						Is.EqualTo(expectedData),
						"data");
		}


		[Test]
		public void CalculateVelocityDataInMicrosteps()
		{
			// SETUP
			var deviceType = new DeviceType
			{
				MotionType = MotionType.Linear,
				Commands = new[]
				{
					new CommandInfo
					{
						Command = Command.MoveAtConstantSpeed,
						RequestUnit = UnitOfMeasure.MetersPerSecond,
						RequestUnitScale = 1000m / 9.375m,
						RequestUnitFunction = ScalingFunction.LinearResolution
					},
					new CommandInfo
					{
						Command = Command.MoveAbsolute,
						RequestUnit = UnitOfMeasure.Meter,
						RequestUnitScale = 1000m,
						RequestUnitFunction = ScalingFunction.LinearResolution
					},
					new CommandInfo
					{
						Command = Command.SetAcceleration,
						RequestUnit = UnitOfMeasure.MetersPerSecondSquared,
						RequestUnitScale = 1000m,
						RequestUnitFunction = ScalingFunction.LinearResolution
					}
				}
			};

			var device = new ZaberDevice { DeviceType = deviceType };
			var measurement = new Measurement(200m, UnitOfMeasure.MicrostepsPerSecond);
			var expectedData = (int) Math.Round(200 / 9.375);

			// EXEC
			var actualData = device.CalculateData(Command.MoveAtConstantSpeed,
												  measurement);

			// VERIFY
			Assert.That(actualData,
						Is.EqualTo(expectedData),
						"data");
		}


		[Test]
		public void FormatAccelerationData()
		{
			// SETUP
			var device = CreateDeviceWithCommands();
			const int data = 200;
			const string expectedDisplay = "200";

			// EXEC
			var actualDisplay = device.FormatData(data,
												  UnitOfMeasure.MicrometersPerSecondSquared);

			// VERIFY
			Assert.That(actualDisplay,
						Is.EqualTo(expectedDisplay),
						"display");
		}


		[Test]
		public void FormatDataForOtherMeasurementType()
		{
			// SETUP
			var device = CreateDeviceWithCommands();
			const int data = 10;
			const string expectedDisplay = "2";

			var legacyMeasure = new UnitOfMeasure(UnitOfMeasure.PercentSymbol);
			InvocationHelper.SetPrivateProperty(legacyMeasure,
												nameof(legacyMeasure.MeasurementType),
												MeasurementType.Other);

			var cmd = new CommandInfo
			{
				TextCommand = "run current",
				RequestUnit = legacyMeasure,
				RequestUnitScale = 20.0m,
				RequestUnitFunction = ScalingFunction.Reciprocal,
				ResponseUnit = legacyMeasure,
				ResponseUnitScale = 20.0m,
				ResponseUnitFunction = ScalingFunction.Reciprocal
			};


			// EXEC
			var actualDisplay = device.FormatData(data,
												  legacyMeasure,
												  cmd);

			// VERIFY
			Assert.That(actualDisplay,
						Is.EqualTo(expectedDisplay),
						"display");
		}


		[Test]
		public void FormatDataForOtherMeasurementTypeWithoutCommandInfo()
		{
			// SETUP
			var device = CreateDeviceWithCommands();
			const int data = 20;

			// EXEC
			Assert.Throws<InvalidOperationException>(() => device.FormatData(data, UnitOfMeasure.Percent));
		}


		[Test]
		public void FormatDataWithoutUnits()
		{
			// SETUP
			var device = CreateDeviceWithCommands();
			const int data = 2000;
			const string expectedDisplay = "2000";

			// EXEC
			var actualDisplay = device.FormatData(data,
												  UnitOfMeasure.Data);

			// VERIFY
			Assert.That(actualDisplay,
						Is.EqualTo(expectedDisplay),
						"display");
		}


		[Test]
		public void FormatOtherMeasurementTypeWithUnitOfMeasure()
		{
			// SETUP
			var deviceType = new DeviceType { MotionType = MotionType.Linear };
			var device = new ZaberDevice { DeviceType = deviceType };
			const int data = 2000;

			// EXEC
			var ex = Assert.Throws(typeof(InvalidOperationException),
								   delegate
								   {
									   device.FormatData(data,
														 UnitOfMeasure.Millimeter);
								   });

			// VERIFY
			Assert.That(ex.Message,
						Does.StartWith("Data can only be formatted for devices with"),
						"error meassage");
		}


		[Test]
		public void FormatPositionData()
		{
			// SETUP
			var device = CreateDeviceWithCommands();
			const int resolution = ZaberDevice.DefaultMicrostepResolution;
			const int data = (int) ((2 / 0.1) * resolution);
			const string expectedDisplay = "1.28";

			// EXEC
			var actualDisplay = device.FormatData(data,
												  UnitOfMeasure.Millimeter);

			// VERIFY
			Assert.That(actualDisplay,
						Is.EqualTo(expectedDisplay),
						"display");
		}


		[Test]
		public void FormatVelocityData()
		{
			// SETUP
			var device = CreateDeviceWithCommands();
			const int data = 200;
			const string expectedDisplay = "200";

			// EXEC
			var actualDisplay = device.FormatData(data,
												  UnitOfMeasure.MicrometersPerSecond);

			// VERIFY
			Assert.That(actualDisplay,
						Is.EqualTo(expectedDisplay),
						"display");
		}


		[Test]
		public void FormatVelocityDataFirmware6()
		{
			// SETUP
			var deviceType = new DeviceType
			{
				MotionType = MotionType.Linear,
				FirmwareVersion = new FirmwareVersion(602),
				Commands = new[]
				{
					new CommandInfo
					{
						Command = Command.MoveAtConstantSpeed,
						RequestUnit = UnitOfMeasure.MetersPerSecond,
						RequestUnitScale = 1000m / 1.6384m,
						RequestUnitFunction = ScalingFunction.LinearResolution
					},
					new CommandInfo
					{
						Command = Command.MoveAbsolute,
						RequestUnit = UnitOfMeasure.Meter,
						RequestUnitScale = 1000m,
						RequestUnitFunction = ScalingFunction.LinearResolution
					},
					new CommandInfo
					{
						Command = Command.SetAcceleration,
						RequestUnit = UnitOfMeasure.MetersPerSecondSquared,
						RequestUnitScale = 1000m,
						RequestUnitFunction = ScalingFunction.LinearResolution
					}
				}
			};

			var device = new ZaberDevice { DeviceType = deviceType };
			const int data = 200;
			const decimal coeffecient = 1.6384m;
			var expectedDisplay = (data * coeffecient).ToString("G12");

			// EXEC
			var actualDisplay = device.FormatData(data,
												  UnitOfMeasure.MicrostepsPerSecond);

			// VERIFY
			Assert.That(actualDisplay,
						Is.EqualTo(expectedDisplay),
						"display");
		}


		[Test]
		public void FormatVelocityDataInMicrosteps()
		{
			// SETUP
			var deviceType = DeviceTypeMother.CreateDeviceTypeWithCommands();

			var device = new ZaberDevice { DeviceType = deviceType };


			const int data = 1000;
			var expectedDisplay = $"{data}";

			// EXEC
			var actualDisplay = device.FormatData(data,
												  UnitOfMeasure.MicrostepsPerSecond);

			// VERIFY
			Assert.That(actualDisplay,
						Is.EqualTo(expectedDisplay),
						"display");
		}


		[Test]
		public void GetAxes()
		{
			// SETUP
			_device.DeviceNumber = 9;
			var axis1 = AddAxis(1);
			var axis2 = AddAxis(2);

			// EXEC
			var axes = _device.Axes;

			// VERIFY
			Assert.That(axes[0],
						Is.SameAs(axis1),
						"axis 1");
			Assert.That(axes[1],
						Is.SameAs(axis2),
						"axis 2");
		}


		[Test]
		public void GetUnitsOfMeasure()
		{
			var deviceType = new DeviceType { MotionType = MotionType.Linear };
			var device = new ZaberDevice { DeviceType = deviceType };

			// Without a valid unit conversion in a command, we only get native units.
			var expectedUnits1 = new[]
			{
				UnitOfMeasure.DataSymbol
			};

			var units = device.GetUnitsOfMeasure(MeasurementType.Velocity).Select(u => u.Abbreviation).ToArray();
			Assert.That(units, Is.EqualTo(expectedUnits1), "units");

			deviceType.Commands = new List<CommandInfo> { new CommandInfo {
					ResponseUnit = UnitOfMeasure.MetersPerSecond,
					ResponseUnitScale = 0.01m,
				}};

			// Without the database, we only get the units defined in the XML file.
			var expectedUnits2 = new[]
			{
				UnitOfMeasure.DataSymbol, UnitOfMeasure.InchesPerSecondSymbol, UnitOfMeasure.MetersPerSecondSymbol,
				UnitOfMeasure.MillimetersPerSecondSymbol, UnitOfMeasure.MicrometersPerSecondSymbol
			};

			units = device.GetUnitsOfMeasure(MeasurementType.Velocity).Select(u => u.Abbreviation).ToArray();
			Assert.That(units, Is.EqualTo(expectedUnits2), "units");
		}


		[Test]
		public void RequestMeasurementCalculated()
		{
			// SETUP
			_device.DeviceType.MotionType = MotionType.Linear;
			var expectedMeasurement = new Measurement(0.1,
													  UnitOfMeasure.Millimeter);
			const int data = 100;
			var deviceNumber = _device.DeviceNumber;

			// EXPECT
			_port.Send(deviceNumber,
					   Command.MoveAbsolute,
					   data,
					   expectedMeasurement);

			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;

			_device.Send(Command.MoveAbsolute, data);

			// VERIFY
			_mocks.VerifyAll();
		}


		[Test]
		public void RequestMeasurementData()
		{
			// SETUP
			var deviceNumber = _device.DeviceNumber;
			var measurement = new Measurement(0, UnitOfMeasure.Data);

			var requestArgs =
				new DataPacketEventArgs(new DataPacket())
				{
					DataPacket =
					{
						Command = Command.Home,
						DeviceNumber = deviceNumber,
						NumericData = 42,
						Measurement = measurement
					}
				};

			// EXPECT
			_port.Send(deviceNumber,
					   Command.Home,
					   0,
					   measurement);

			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;
			var isSingleDevice = _device.IsSingleDevice;

			_device.Send(Command.Home);
			_packetSentRaiser.Raise(_port,
									requestArgs);

			// VERIFY
			Assert.IsTrue(isSingleDevice,
						  "Should be a single device.");
			Assert.AreEqual("DataPacketSent",
							_eventType,
							"Event type should match");
			Assert.AreSame(_device,
						   _eventSource,
						   "Device should raise packet event");
			Assert.AreEqual("Where the heart is",
							_eventArgs.DeviceMessage.CommandInfo.Name,
							"Command info should be set.");

			_mocks.VerifyAll();
		}


		[Test]
		public void RequestMeasurementExplicit()
		{
			// SETUP
			_device.DeviceType.MotionType = MotionType.Linear;
			var requestMeasurement = new Measurement(1,
													 UnitOfMeasure.Millimeter);
			const int expectedData = 1000;
			var deviceNumber = _device.DeviceNumber;

			// EXPECT
			_port.Send(deviceNumber,
					   Command.MoveAbsolute,
					   expectedData,
					   requestMeasurement);

			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;

			_device.SendInUnits(Command.MoveAbsolute, requestMeasurement);

			// VERIFY
			_mocks.VerifyAll();
		}


		[Test]
		public void RequestMeasurementExplicitAscii()
		{
			// SETUP
			_device.DeviceNumber = 17;
			_device.DeviceType.MotionType = MotionType.Linear;
			var requestMeasurement = new Measurement(1,
													 UnitOfMeasure.Millimeter);
			const string expectedCommand = "17 0 set pos 1000";

			// EXPECT
			_port.Send(expectedCommand,
					   requestMeasurement);

			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;

			_device.SendInUnits("set pos", requestMeasurement);

			// VERIFY
			_mocks.VerifyAll();
		}


		[Test]
		public void RequestTracking()
		{
			// SETUP
			const string expectedRequestText = "/17 get pos";
			var requestArgs =
				new DataPacketEventArgs(new DataPacket(expectedRequestText));
			var responseArgs =
				new DataPacketEventArgs(new DataPacket("@17 0 OK IDLE -- 42"));
			_port.IsAsciiMode = true;

			// EXPECT
			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;
			var isTrackingDefault = _device.IsTrackingRequests;
			_packetSentRaiser.Raise(_port,
									requestArgs);

			_eventType = null; // stop test from whining about second event

			_packetReceivedRaiser.Raise(_port,
										responseArgs);
			var isTrackingAfter = _device.IsTrackingRequests;
			var request = _eventArgs.DeviceMessage.Request;
			var requestCommandInfo = request.CommandInfo;
			var responseCommandInfo = _eventArgs.DeviceMessage.CommandInfo;

			// VERIFY
			Assert.That(isTrackingDefault,
						Is.True,
						"default value of IsTrackingRequests");
			Assert.That(isTrackingAfter,
						Is.True,
						"value of IsTrackingRequests after receive");
			Assert.That(request.Text,
						Is.EqualTo(expectedRequestText),
						"request text");
			_mocks.VerifyAll();
		}


		[Test]
		public void RequestTrackingBinary()
		{
			// SETUP
			_port.IsAsciiMode = false;

			// EXPECT
			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;
			var isTrackingDefault = _device.IsTrackingRequests;

			// VERIFY
			Assert.That(isTrackingDefault,
						Is.False,
						"default value of IsTrackingRequests");
			_mocks.VerifyAll();
		}


		[Test]
		public void RequestWithId()
		{
			// SETUP
			_device.AreMessageIdsEnabled = true;
			var deviceNumber = _device.DeviceNumber;

			// EXPECT
			_port.Send(deviceNumber,
					   Command.MoveAtConstantSpeed,
					   42 + (13 * 0x1000000),
					   new Measurement(42, UnitOfMeasure.Data));

			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;

			_device.Send(Command.MoveAtConstantSpeed, 42, 13);

			// VERIFY
			_mocks.VerifyAll();
		}


		[Test]
		public void RequestWithIdAndNegativeData()
		{
			// SETUP
			_device.AreMessageIdsEnabled = true;
			var deviceNumber = _device.DeviceNumber;

			// EXPECT
			_port.Send(deviceNumber,
					   Command.MoveAtConstantSpeed,
					   (-42 & 0xFFFFFF) + (13 * 0x1000000),
					   new Measurement(-42, UnitOfMeasure.Data));

			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;

			_device.Send(Command.MoveAtConstantSpeed, -42, 13);

			// VERIFY
			_mocks.VerifyAll();
		}


		[Test]
		public void RequestWithIdDisabled()
		{
			// SETUP
			_device.AreMessageIdsEnabled = false;
			var deviceNumber = _device.DeviceNumber;

			// EXPECT
			_port.Send(deviceNumber,
					   Command.MoveAtConstantSpeed,
					   42,
					   new Measurement(42, UnitOfMeasure.Data));

			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;

			_device.Send(Command.MoveAtConstantSpeed, 42, 13);

			// VERIFY
			_mocks.VerifyAll();
		}


		[Test]
		public void ResponseMeasurementCalculated()
		{
			// SETUP
			_device.DeviceType.MotionType = MotionType.Linear;
			const int data = 42;
			var expectedMeasurement =
				new Measurement(0.042, UnitOfMeasure.Millimeter);
			var receivedMeasurement =
				new Measurement(data, UnitOfMeasure.Data);

			var responseArgs =
				new DataPacketEventArgs(new DataPacket())
				{
					DataPacket =
					{
						Command = Command.MoveAbsolute,
						DeviceNumber = _device.DeviceNumber,
						NumericData = data,
						Measurement = receivedMeasurement
					}
				};


			// EXPECT
			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;

			_packetReceivedRaiser.Raise(_port,
										responseArgs);

			// VERIFY
			Assert.That(_eventArgs.DeviceMessage.Measurement,
						Is.EqualTo(expectedMeasurement),
						"measurement");
			_mocks.VerifyAll();
		}


		[Test]
		public void ResponseMeasurementCalculatedAscii()
		{
			// SETUP
			_device.DeviceType.MotionType = MotionType.Linear;
			_device.DeviceNumber = 13;

			var requestArgs =
				new DataPacketEventArgs(new DataPacket("/13 get pos"));
			var responseArgs =
				new DataPacketEventArgs(new DataPacket("@13 0 OK IDLE -- 42"));
			var expectedMeasurement =
				new Measurement(0.042, UnitOfMeasure.Millimeter);
			_port.IsAsciiMode = true;

			// EXPECT
			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;
			_packetSentRaiser.Raise(_port,
									requestArgs);

			_eventType = null; // stop test whining about second event

			_packetReceivedRaiser.Raise(_port,
										responseArgs);

			// VERIFY
			Assert.That(_eventArgs.DeviceMessage.Measurement,
						Is.EqualTo(expectedMeasurement),
						"measurement");
			_mocks.VerifyAll();
		}


		[Test]
		public void ResponseMeasurementCalculatedWithDistanceToAxis()
		{
			// SETUP
			const int data = 42;
			var receivedMeasurement =
				new Measurement(data, UnitOfMeasure.Data);

			var responseArgs =
				new DataPacketEventArgs(new DataPacket())
				{
					DataPacket =
					{
						Command = Command.MoveAbsolute,
						DeviceNumber = _device.DeviceNumber,
						NumericData = data,
						Measurement = receivedMeasurement
					}
				};

			// EXPECT
			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;

			_packetReceivedRaiser.Raise(_port,
										responseArgs);

			// VERIFY
			Assert.That(_eventArgs.DeviceMessage.Measurement.Unit,
						Is.EqualTo(UnitOfMeasure.Millimeter),
						"measurement");
			_mocks.VerifyAll();
		}


		[Test]
		public void ResponseMeasurementData()
		{
			// SETUP
			var expectedMeasurement = new Measurement(42, UnitOfMeasure.Data);
			var responseArgs =
				new DataPacketEventArgs(new DataPacket())
				{
					DataPacket =
					{
						Command = Command.EchoData,
						DeviceNumber = _device.DeviceNumber,
						NumericData = 42,
						Measurement = expectedMeasurement
					}
				};


			// EXPECT
			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;

			_packetReceivedRaiser.Raise(_port,
										responseArgs);

			// VERIFY
			Assert.AreEqual("DataPacketReceived",
							_eventType,
							"Event type should match");
			Assert.AreSame(_device,
						   _eventSource,
						   "Device should raise packet event");
			Assert.AreEqual("Echo Data",
							_eventArgs.DeviceMessage.CommandInfo.Name,
							"Command info should be set.");
			Assert.That(_eventArgs.DeviceMessage.Measurement,
						Is.EqualTo(expectedMeasurement),
						"measurement");
			_mocks.VerifyAll();
		}


		[Test]
		public void ResponseMessageIdWithNegativeData()
		{
			// SETUP
			_device.AreMessageIdsEnabled = true;

			var responseArgs =
				new DataPacketEventArgs(new DataPacket())
				{
					DataPacket =
					{
						Command = Command.Home,
						DeviceNumber = _device.DeviceNumber,
						NumericData = -10
					}
				};

			// EXPECT
			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;

			_packetReceivedRaiser.Raise(_port,
										responseArgs);

			// VERIFY
			Assert.AreEqual(-10,
							_eventArgs.DeviceMessage.NumericData,
							"Data should match");
			Assert.AreEqual(0,
							_eventArgs.DeviceMessage.MessageId,
							"Message Id should match");
			_mocks.VerifyAll();
		}


		[Test]
		public void ResponseWithAxisNumber()
		{
			// SETUP
			_device.DeviceNumber = 1;
			var axis1 = AddAxis(1);
			var axis2 = AddAxis(2);
			var listener0 = new DeviceListener(_device);
			var listener1 = new DeviceListener(axis1);
			var listener2 = new DeviceListener(axis2);
			var responsePacket = new DataPacket("@01 0 OK IDLE -- 0") { AxisNumber = 2 };
			var responseArgs = new DataPacketEventArgs(responsePacket);

			// EXPECT
			_mocks.ReplayAll();

			// EXEC
			_device.Port = axis1.Port = axis2.Port = _port;

			_packetReceivedRaiser.Raise(_port,
										responseArgs);
			const bool isBlocking = false;
			var response0 = listener0.NextResponse(isBlocking);
			var response1 = listener1.NextResponse(isBlocking);
			var response2 = listener2.NextResponse(isBlocking);

			// VERIFY
			Assert.That(response0,
						Is.Not.Null,
						"device response");
			Assert.That(response1,
						Is.Null,
						"axis 1 response");
			Assert.That(response2,
						Is.Not.Null,
						"axis 2 response");
			_mocks.VerifyAll();
		}


		[Test]
		public void ResponseWithId()
		{
			// SETUP
			_device.AreMessageIdsEnabled = true;

			var responseArgs =
				new DataPacketEventArgs(new DataPacket())
				{
					DataPacket =
					{
						Command = Command.Home,
						DeviceNumber = _device.DeviceNumber,
						NumericData = 42 + (22 * 0x1000000)
					}
				};

			// EXPECT
			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;

			_packetReceivedRaiser.Raise(_port,
										responseArgs);

			// VERIFY
			Assert.AreEqual(42,
							_eventArgs.DeviceMessage.NumericData,
							"Data should match");
			Assert.AreEqual(22,
							_eventArgs.DeviceMessage.MessageId,
							"Message Id should match");
			_mocks.VerifyAll();
		}


		[Test]
		public void ResponseWithIdDisabled()
		{
			// SETUP
			_device.AreMessageIdsEnabled = false;

			var responseArgs =
				new DataPacketEventArgs(new DataPacket())
				{
					DataPacket =
					{
						Command = Command.Home,
						DeviceNumber = _device.DeviceNumber,
						NumericData = 42 + (22 * 0x1000000)
					}
				};

			// EXPECT
			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;

			_packetReceivedRaiser.Raise(_port,
										responseArgs);

			// VERIFY
			Assert.AreEqual(42 + (22 * 0x1000000),
							_eventArgs.DeviceMessage.NumericData,
							"Data should match");
			Assert.AreEqual(0,
							_eventArgs.DeviceMessage.MessageId,
							"Message Id should match");
			_mocks.VerifyAll();
		}


		/// <summary>
		///     If the axis devices raise events for responses from the main
		///     device, then it messes up their conversations. I think it ends
		///     up raising the events twice, or something.
		///     The problem goes away if you just don't raise events on the
		///     axis devices for responses from the main device.
		/// </summary>
		[Test]
		public void ResponseWithoutAxisNumber()
		{
			// SETUP
			_device.DeviceNumber = 1;
			var axis1 = AddAxis(1);
			var axis2 = AddAxis(2);
			var listener0 = new DeviceListener(_device);
			var listener1 = new DeviceListener(axis1);
			var listener2 = new DeviceListener(axis2);
			var responsePacket = new DataPacket("@01 0 OK IDLE -- 0");
			var responseArgs = new DataPacketEventArgs(responsePacket);

			// EXPECT
			_mocks.ReplayAll();

			// EXEC
			_device.Port = axis1.Port = axis2.Port = _port;

			_packetReceivedRaiser.Raise(_port,
										responseArgs);
			const bool isBlocking = false;
			var response0 = listener0.NextResponse(isBlocking);
			var response1 = listener1.NextResponse(isBlocking);
			var response2 = listener2.NextResponse(isBlocking);

			// VERIFY
			Assert.That(response0,
						Is.Not.Null,
						"device response");
			Assert.That(response1,
						Is.Null,
						"axis 1 response");
			Assert.That(response2,
						Is.Null,
						"axis 2 response");
			_mocks.VerifyAll();
		}


		[Test]
		public void SendAxis()
		{
			// SETUP
			_device.DeviceNumber = 9;
			var axis1 = AddAxis(1);
			var axis2 = AddAxis(2);
			const string text = "move abs 0";
			const string expectedText = "09 2 " + text;

			var requestArgs =
				new DataPacketEventArgs(new DataPacket("/" + expectedText));

			// EXPECT
			_port.Send(expectedText);

			_mocks.ReplayAll();

			// EXEC
			axis1.Port = axis2.Port = _device.Port = _port;

			axis2.Send(text);

			_packetSentRaiser.Raise(_port,
									requestArgs);

			// VERIFY
			_mocks.VerifyAll();
		}


		[Test]
		public void SendDelayed()
		{
			// SETUP
			var deviceNumber = _device.DeviceNumber;

			// EXPECT
			_port.SendDelayed(deviceNumber, Command.Home, 0);

			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;

			_device.SendDelayed(Command.Home, 0, 0);

			// VERIFY
			_mocks.VerifyAll();
		}


		[Test]
		public void SendDelayedWithId()
		{
			// SETUP
			_device.AreMessageIdsEnabled = true;
			var deviceNumber = _device.DeviceNumber;

			// EXPECT
			_port.SendDelayed(deviceNumber, Command.MoveAtConstantSpeed, 42 + (13 * 0x1000000));

			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;

			_device.SendDelayed(Command.MoveAtConstantSpeed, 42, 13);

			// VERIFY
			_mocks.VerifyAll();
		}


		[Test]
		public void SendText()
		{
			// SETUP
			_device.DeviceNumber = 9;
			const string text = "get pos";
			const string expectedText = "09 0 " + text;

			var requestArgs =
				new DataPacketEventArgs(new DataPacket("/" + expectedText));

			// EXPECT
			_port.Send(expectedText);

			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;

			_device.Send(text);

			_packetSentRaiser.Raise(_port,
									requestArgs);

			// VERIFY
			Assert.AreEqual("DataPacketSent",
							_eventType,
							"Event type should match");
			Assert.AreSame(_device,
						   _eventSource,
						   "Device should raise packet event");

			_mocks.VerifyAll();
		}


		[Test]
		public void UnrelatedRequestEvent()
		{
			// SETUP
			var requestArgs =
				new DataPacketEventArgs(new DataPacket())
				{
					DataPacket =
					{
						Command = Command.Home,

						// unrelated device number
						DeviceNumber = 23,
						NumericData = 42
					}
				};

			// EXPECT
			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;

			_packetSentRaiser.Raise(_port,
									requestArgs);

			// VERIFY
			Assert.IsNull(_eventType,
						  "No event should be raised");

			_mocks.VerifyAll();
		}


		[Test]
		public void UnrelatedResponse()
		{
			// SETUP
			var responseArgs =
				new DataPacketEventArgs(new DataPacket())
				{
					DataPacket =
					{
						Command = Command.ReturnDeviceID,

						// Response from different device
						DeviceNumber = 22,
						NumericData = 42
					}
				};

			// EXPECT
			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;

			_packetReceivedRaiser.Raise(_port,
										responseArgs);

			// VERIFY
			Assert.IsNull(_eventSource,
						  "Device should not raise packet event for different device number");

			_mocks.VerifyAll();
		}


		[Test]
		public void CanUpdateFirmware()
		{
			var deviceType = new DeviceType
			{
				FirmwareVersion = new FirmwareVersion(6, 18),
				Name = "Test device"
			};

			var port = new MockPort
			{
				IsAsciiMode = true
			};

			var device = new ZaberDevice
			{
				DeviceType = deviceType,
				Port = port
			};

			port.Open("COM1");

			Assert.IsTrue(device.CanUpdateFirmware);

			device.DeviceType.FirmwareVersion = new FirmwareVersion(6, 17);
			Assert.IsFalse(device.CanUpdateFirmware);
			device.DeviceType.FirmwareVersion = new FirmwareVersion(6, 18);

			port.IsAsciiMode = false;
			Assert.IsFalse(device.CanUpdateFirmware);
			port.IsAsciiMode = true;

			device.AxisNumber = 1;
			Assert.IsFalse(device.CanUpdateFirmware);
			device.AxisNumber = 0;

			var device2 = new DeviceCollection();
			device2.DeviceType = new DeviceType { Name = "All Devices" };
			device2.Add(device);
			Assert.IsFalse(device2.CanUpdateFirmware);

			Assert.IsTrue(device.CanUpdateFirmware, "Some test condition was not properly reset.");

			port.Close();
			Assert.IsFalse(device.CanUpdateFirmware);
		}


		[Test]
		public void CanUpdateFirmware_BootloaderMode()
		{
			var deviceType = new DeviceType
			{
				FirmwareVersion = new FirmwareVersion(0, 0),
				Name = "Bootloader"
			};

			var port = new MockPort
			{
				IsAsciiMode = true
			};

			var device = new ZaberDevice
			{
				DeviceType = deviceType,
				Port = port, 
				IsInBootloaderMode = true
			};

			port.Open("COM1");

			Assert.IsTrue(device.CanUpdateFirmware);
		}


		[Test]
		public void CanStoreCalibration_FW6()
		{
			var deviceType = new DeviceType
			{
				FirmwareVersion = new FirmwareVersion(6, 22),
				MotionType = MotionType.None,
				Name = "Test controller"
			};

			var port = new MockPort
			{
				IsAsciiMode = true
			};

			var device = new ZaberDevice
			{
				DeviceType = deviceType,
				AxisNumber = 0, 
				IsController = true,
				Port = port
			};

			port.Open("COM1");

			Assert.IsTrue(device.CanStoreCalibration);

			device.DeviceType.FirmwareVersion = new FirmwareVersion(6, 21);
			Assert.IsFalse(device.CanStoreCalibration);
			device.DeviceType.FirmwareVersion = new FirmwareVersion(6, 22);

			port.IsAsciiMode = false;
			Assert.IsFalse(device.CanStoreCalibration);
			port.IsAsciiMode = true;

			device.AxisNumber = 1;
			device.IsController = false;
			device.IsPeripheral = true;
			Assert.IsFalse(device.CanStoreCalibration);
			device.AxisNumber = 0;
			device.IsController = true;
			device.IsPeripheral = false;

			var device2 = new DeviceCollection();
			device2.DeviceType = new DeviceType { Name = "All Devices" };
			device2.Add(device);
			Assert.IsFalse(device2.CanStoreCalibration);

			Assert.IsTrue(device.CanStoreCalibration, "Some test condition was not properly reset.");

			port.Close();
			Assert.IsFalse(device.CanStoreCalibration);
		}


		[Test]
		public void CanStoreCalibration_FW7()
		{
			var deviceType = new DeviceType
			{
				FirmwareVersion = new FirmwareVersion(7, 10),
				MotionType = MotionType.Linear,
				Name = "Test Controller"
			};

			deviceType.Capabilities.Add(Capability.EncoderDirect);

			var port = new MockPort
			{
				IsAsciiMode = true
			};

			var device = new ZaberDevice
			{
				DeviceType = deviceType,
				AxisNumber = 1,
				Port = port
			};

			port.Open("COM1");

			Assert.IsTrue(device.CanStoreCalibration);

			device.DeviceType.Capabilities.Remove(Capability.EncoderDirect);
			Assert.IsFalse(device.CanStoreCalibration);
			device.DeviceType.Capabilities.Add(Capability.EncoderDirect);

			device.DeviceType.Capabilities.Add(Capability.Cyclic);
			Assert.IsFalse(device.CanStoreCalibration);
			device.DeviceType.Capabilities.Remove(Capability.Cyclic);

			device.DeviceType.MotionType = MotionType.None;
			Assert.IsFalse(device.CanStoreCalibration);
			device.DeviceType.MotionType = MotionType.Linear;

			var device2 = new DeviceCollection();
			device2.DeviceType = new DeviceType { Name = "All Devices" };
			device2.Add(device);
			Assert.IsFalse(device2.CanStoreCalibration);

			Assert.IsTrue(device.CanStoreCalibration, "Some test condition was not properly reset.");

			port.Close();
			Assert.IsFalse(device.CanStoreCalibration);
		}


		[Test]
		public void ShouldBeCalibrated_FW6()
		{
			var deviceType = new DeviceType
			{
				FirmwareVersion = new FirmwareVersion(6, 22),
				Name = "X-LRQ-DE50"
			};

			var device = new ZaberDevice
			{
				DeviceType = deviceType
			};

			Assert.IsTrue(device.ShouldBeCalibrated);

			device.DeviceType.Name = "X-LRQ";
			Assert.IsFalse(device.ShouldBeCalibrated);
			device.DeviceType.Name = "X-LRQ-DE50";

			device.DeviceType.DeviceTypeUnitConversionInfo[Dimension.Angle] = new ParameterUnitConversion();
			Assert.IsFalse(device.ShouldBeCalibrated);
			device.DeviceType.DeviceTypeUnitConversionInfo.Remove(Dimension.Angle);

			Assert.IsTrue(device.ShouldBeCalibrated);
		}


		[Test]
		public void ShouldBeCalibrated_FW7()
		{
			var deviceType = new DeviceType
			{
				FirmwareVersion = new FirmwareVersion(7, 10),
				Name = "Test device"
			};

			var device = new ZaberDevice
			{
				DeviceType = deviceType
			};

			device.DeviceType.Capabilities.Add(Capability.EncoderDirect);

			Assert.IsTrue(device.ShouldBeCalibrated);

			device.DeviceType.Capabilities.Remove(Capability.EncoderDirect);
			Assert.IsFalse(device.ShouldBeCalibrated);
			device.DeviceType.Capabilities.Add(Capability.EncoderDirect);

			device.DeviceType.Capabilities.Add(Capability.Cyclic);
			Assert.IsFalse(device.ShouldBeCalibrated);
			device.DeviceType.Capabilities.Remove(Capability.Cyclic);

			Assert.IsTrue(device.ShouldBeCalibrated);
		}


		[Test]
		public void GetAllUnitsOfMeasure()
		{
			// SETUP
			var deviceType = new DeviceType
			{
				MotionType = MotionType.Linear,
				Commands = new[]
				{
					new CommandInfo
					{
						Command = Command.MoveAtConstantSpeed,
						RequestUnit = UnitOfMeasure.MetersPerSecond,
						RequestUnitScale = 1000m / 1.6384m,
						RequestUnitFunction = ScalingFunction.LinearResolution
					},
					new CommandInfo
					{
						Command = Command.MoveAbsolute,
						RequestUnit = UnitOfMeasure.Meter,
						RequestUnitScale = 1000m,
						RequestUnitFunction = ScalingFunction.LinearResolution
					},
					new CommandInfo
					{
						Command = Command.SetAcceleration,
						RequestUnit = UnitOfMeasure.MetersPerSecondSquared,
						RequestUnitScale = 1000m,
						RequestUnitFunction = ScalingFunction.LinearResolution
					}
				}
			};

			var device = new ZaberDevice { DeviceType = deviceType };

			// EXEC
			#pragma warning disable CS0612 // 'name' is obsolete
			var units = device.GetAllUnitsOfMeasure().ToList();
			#pragma warning restore CS0612

			// VERIFY
			Assert.That(units.Contains(UnitOfMeasure.Meter),
						Is.True,
						"units contains mm");
			Assert.That(units.Contains(UnitOfMeasure.MetersPerSecond),
						Is.True,
						"units contains mm/s");
		}


		[Test]
		public void TestLinearScalingFunctionReversal()
		{
			var paramInfo = new ParameterUnitConversion
			{
				Function = ScalingFunction.Linear,
				ReferenceUnit = UnitOfMeasure.Data.Unit,
				Scale = 3m
			};

			var physicalUnits = 2m;
			var deviceUnits = 6m;
			var resolution = 64;

			var transform =
				(QuantityTransformation) InvocationHelper.InvokePrivateStaticMethod<ZaberDevice>(
					"GetScalingFunctionTransform",
					paramInfo,
					resolution);
			var result = transform.Transform(physicalUnits);
			Assert.AreEqual((double) deviceUnits, (double) result, 0.0001);
			transform = transform.Reverse();
			result = transform.Transform(deviceUnits);
			Assert.AreEqual((double) physicalUnits, (double) result, 0.0001);
		}


		[Test]
		public void TestReciprocalScalingFunctionReversal()
		{
			var paramInfo = new ParameterUnitConversion
			{
				Function = ScalingFunction.Reciprocal,
				ReferenceUnit = UnitOfMeasure.Data.Unit,
				Scale = 6m
			};

			var physicalUnits = 2m;
			var deviceUnits = 3m;
			var resolution = 64;

			var transform =
				(QuantityTransformation) InvocationHelper.InvokePrivateStaticMethod<ZaberDevice>(
					"GetScalingFunctionTransform",
					paramInfo,
					resolution);
			var result = transform.Transform(physicalUnits);
			Assert.AreEqual((double) deviceUnits, (double) result, 0.0001);
			transform = transform.Reverse();
			result = transform.Transform(deviceUnits);
			Assert.AreEqual((double) physicalUnits, (double) result, 0.0001);
		}


		[Test]
		public void TestLinearResolutionScalingFunctionReversal()
		{
			var paramInfo = new ParameterUnitConversion
			{
				Function = ScalingFunction.LinearResolution,
				ReferenceUnit = UnitOfMeasure.Data.Unit,
				Scale = 3m
			};

			var physicalUnits = 2m;
			var resolution = 64;
			var deviceUnits = resolution * 6m;

			var transform =
				(QuantityTransformation) InvocationHelper.InvokePrivateStaticMethod<ZaberDevice>(
					"GetScalingFunctionTransform",
					paramInfo,
					resolution);
			var result = transform.Transform(physicalUnits);
			Assert.AreEqual((double) deviceUnits, (double) result, 0.0001);
			transform = transform.Reverse();
			result = transform.Transform(deviceUnits);
			Assert.AreEqual((double) physicalUnits, (double) result, 0.0001);
		}


		[Test]
		public void TestTangentialResolutionScalingFunctionReversal()
		{
			var paramInfo = new ParameterUnitConversion
			{
				Function = ScalingFunction.TangentialResolution,
				ReferenceUnit = UnitOfMeasure.Data.Unit,
				Scale = 3m
			};

			var physicalUnits = 31.4729m; // Chosen to minimize error due to casting to int in reverse call.
			var resolution = 64;
			var deviceUnits = paramInfo.Scale.Value
						  * resolution
						  * (decimal) Math.Tan(((double) physicalUnits * Math.PI) / 180.0);

			var transform =
				(QuantityTransformation) InvocationHelper.InvokePrivateStaticMethod<ZaberDevice>(
					"GetScalingFunctionTransform",
					paramInfo,
					resolution);
			var result = transform.Transform(physicalUnits);
			Assert.AreEqual((double) deviceUnits, (double) result, 0.0001);
			transform = transform.Reverse();
			result = transform.Transform(deviceUnits);
			Assert.AreEqual((double) physicalUnits, (double) result, 0.2);
		}


		[Test]
		public void TestGetConversionToDeviceUnits()
		{
			var deviceType = new DeviceType
			{
				MotionType = MotionType.Linear,
				DeviceTypeUnitConversionInfo =
				{
					{
						Dimension.Length, new ParameterUnitConversion
						{
							Function = ScalingFunction.LinearResolution,
							ReferenceUnit = UnitOfMeasure.Meter.Unit,
							Scale = 10000m
						}
					}
				}
			};
			var device = new ZaberDevice { DeviceType = deviceType };

			var unit = device.UnitConverter.FindUnitByAbbreviation("mm");
			var transform = device.GetConversionToDeviceUnits(unit.Unit);

			var result = transform.Transform(2m);
			var resolution = device.MicrostepResolution;
			var expectedData = (int) ((2 / 0.1) * resolution);

			Assert.AreEqual(expectedData, result);
		}


		[Test]
		public void TestGetConversionToCommandUnits()
		{
			var command = new CommandInfo
			{
				Command = Command.MoveAbsolute,
				RequestUnit = UnitOfMeasure.Meter,
				RequestUnitScale = 10000m,
				RequestUnitFunction = ScalingFunction.LinearResolution
			};

			var deviceType = new DeviceType
			{
				MotionType = MotionType.Linear,
				Commands = new List<CommandInfo> { command }
			};

			var device = new ZaberDevice { DeviceType = deviceType };

			var unit = device.UnitConverter.FindUnitByAbbreviation("mm");
			var transform = device.GetConversionToParameterUnits(unit.Unit, command.GetRequestConversion());

			var result = transform.Transform(2m);
			var resolution = device.MicrostepResolution;
			var expectedData = (int) ((2 / 0.1) * resolution);

			Assert.AreEqual(expectedData, result);
		}


		[Test]
		public void TestFormatAsciiCommandMultiValueBasic()
		{
			var device = CreateDeviceWithMultiParameterCommand("move {0} vel {1} accel {2}",
															   new Tuple<UnitOfMeasure, decimal, ParameterType>(
																   UnitOfMeasure.Meter,
																   15m,
																   AsciiCommandBuilder.StandardParamTypes["Int32"]),
															   new Tuple<UnitOfMeasure, decimal, ParameterType>(
																   UnitOfMeasure.MetersPerSecond,
																   25m,
																   AsciiCommandBuilder.StandardParamTypes["Int32"]),
															   new Tuple<UnitOfMeasure, decimal, ParameterType>(
																   UnitOfMeasure.MetersPerSecondSquared,
																   100m,
																   AsciiCommandBuilder.StandardParamTypes["Tenths"]));

			// Test
			var cmd = device.DeviceType.GetCommandByText("move {0} vel {1} accel {2}");
			Assert.IsNotNull(cmd);
			var result = device.FormatAsciiCommand(cmd, 1, 2.1, 3.1m);

			// No scaling since we used scalar values and it is assumed we already did unit conversion.
			Assert.AreEqual("move 1 vel 2 accel 3.1", result);
		}


		[Test]
		public void TestFormatAsciiCommandMultiValueArrayCase1()
		{
			var device = CreateDeviceWithMultiParameterCommand("foo {0} bar {%1}",
															   new Tuple<UnitOfMeasure, decimal, ParameterType>(
																   UnitOfMeasure.Meter,
																   10m,
																   AsciiCommandBuilder.StandardParamTypes["Int32"]),
															   new Tuple<UnitOfMeasure, decimal, ParameterType>(
																   UnitOfMeasure.Meter,
																   20m,
																   AsciiCommandBuilder.StandardParamTypes["Int32"]));

			// Test
			var q = new Measurement(1m, UnitOfMeasure.Meter);
			var result = device.FormatAsciiCommand("foo {0} bar {1}", q, q, 1, q, q);

			// No scaling since we used scalar values and it is assumed we already did unit conversion.
			Assert.AreEqual("foo 640 bar 1280 1 1280 1280", result);
		}


		[Test]
		public void TestFormatAsciiCommandMultiValueArrayCase2()
		{
			var device = CreateDeviceWithMultiParameterCommand("foo {0} bar {%1}",
															   new Tuple<UnitOfMeasure, decimal, ParameterType>(
																   UnitOfMeasure.Meter,
																   10m,
																   AsciiCommandBuilder.StandardParamTypes["Int32"]),
															   new Tuple<UnitOfMeasure, decimal, ParameterType>(
																   UnitOfMeasure.Meter,
																   20m,
																   AsciiCommandBuilder.StandardParamTypes["Int32"]));

			// Test
			var q = new Measurement(1m, UnitOfMeasure.Meter);
			var result = device.FormatAsciiCommand("foo {0} bar {1}", q, new object[] { q, 1, q, q });

			// No scaling since we used scalar values and it is assumed we already did unit conversion.
			Assert.AreEqual("foo 640 bar 1280 1 1280 1280", result);
		}


		[Test]
		public void TestFormatAsciiCommandMultiValueWithUnits()
		{
			var device = CreateDeviceWithMultiParameterCommand("move {0} vel {1} accel {2}",
															   new Tuple<UnitOfMeasure, decimal, ParameterType>(
																   UnitOfMeasure.Meter,
																   15m,
																   AsciiCommandBuilder.StandardParamTypes["Int32"]),
															   new Tuple<UnitOfMeasure, decimal, ParameterType>(
																   UnitOfMeasure.MetersPerSecond,
																   25m,
																   AsciiCommandBuilder.StandardParamTypes["Int32"]),
															   new Tuple<UnitOfMeasure, decimal, ParameterType>(
																   UnitOfMeasure.MetersPerSecondSquared,
																   100m,
																   AsciiCommandBuilder.StandardParamTypes["Tenths"]));

			// Test
			var cmd = device.DeviceType.GetCommandByText("move {0} vel {1} accel {2}");
			Assert.IsNotNull(cmd);
			var result = device.FormatAsciiCommand(cmd,
												   new Measurement(4m, UnitOfMeasure.Meter),
												   new Zaber.Units.Measurement(2.1, UnitOfMeasure.MetersPerSecond.Unit),
												   new ParameterData(0,
																	 "3.001",
																	 AsciiCommandBuilder.StandardParamTypes["Tenths"],
																	 UnitOfMeasure.MetersPerSecondSquared));

			// Don't forget default resolution is 64.
			// Also note device-level scales are ignored since the command has its own.
			Assert.AreEqual("move 3840 vel 3360 accel 19206.4", result);
		}


		[Test]
		public void TestFormatAsciiCommandMultiValueWithTokens()
		{
			var device = CreateDeviceWithMultiParameterCommand("with {0} do {1} {2} while {3}",
															   new Tuple<UnitOfMeasure, decimal, ParameterType>(
																   UnitOfMeasure.Data,
																   1m,
																   AsciiCommandBuilder.StandardParamTypes["Token"]),
															   new Tuple<UnitOfMeasure, decimal, ParameterType>(
																   UnitOfMeasure.Data,
																   1m,
																   new ParameterType(new[] { "Foo", "Bar", "Baz" })),
															   new Tuple<UnitOfMeasure, decimal, ParameterType>(
																   UnitOfMeasure.Data,
																   1m,
																   new ParameterType(new[] { "Foo", "Bar", "Baz" })),
															   new Tuple<UnitOfMeasure, decimal, ParameterType>(
																   UnitOfMeasure.Data,
																   1m,
																   AsciiCommandBuilder.StandardParamTypes["Bool"]));

			// Test
			var cmd = device.DeviceType.GetCommandByText("with {0} do {1} {2} while {3}");
			Assert.IsNotNull(cmd);
			var result = device.FormatAsciiCommand(cmd, "glee", Meta.Foo, "bar", true);
			Assert.AreEqual("with glee do Foo Bar while 1", result);
		}


		[Test]
		public void TestFormatAsciiCommandEnumErrorDetection()
		{
			var device = CreateDeviceWithMultiParameterCommand("poke {0}",
															   new Tuple<UnitOfMeasure, decimal, ParameterType>(
																   UnitOfMeasure.Data,
																   1m,
																   new ParameterType(new[] { "Foo", "Bar", "Baz" })));

			// Test
			var cmd = device.DeviceType.GetCommandByText("poke {0}");
			Assert.IsNotNull(cmd);
			Assert.Throws<ArgumentException>(delegate { device.FormatAsciiCommand(cmd, "Quux"); });
		}


		[Test]
		public void TestSendFormatInUnits_NoParams()
		{
			var device = CreateDeviceWithMultiParameterCommand("home");
			var port = new MockPort();
			device.Port = port;
			device.DeviceNumber = 1;
			port.Expect("/01 0 home");

			// Test
			device.SendFormatInUnits("home", new ParameterData[0]);
			port.Verify();
		}


		[Test]
		public void TestSendFormatInUnits_OneParam()
		{
			var device = CreateDeviceWithMultiParameterCommand("move abs {0}",
															   new Tuple<UnitOfMeasure, decimal, ParameterType>(
																   UnitOfMeasure.Meter,
																   1m,
																   AsciiCommandBuilder.StandardParamTypes["Int64"]));
			var port = new MockPort();
			device.Port = port;
			device.DeviceNumber = 1;

			var param1 = new ParameterData(
				0, 
				"1", 
				new ParameterType { DecimalPlaces = 1, IsNumeric = true },
				UnitOfMeasure.Meter);

			port.Expect("/01 0 move abs 64.0");

			device.SendFormatInUnits("move abs {0}", new ParameterData[] { param1 });
			port.Verify();
		}


		[Test]
		public void RM6500_TestSendFormatInUnits_MissingParam()
		{
			var device = CreateDeviceWithMultiParameterCommand("move abs {0}",
															   new Tuple<UnitOfMeasure, decimal, ParameterType>(
																   UnitOfMeasure.Meter,
																   1m,
																   AsciiCommandBuilder.StandardParamTypes["Int64"]));
			var port = new MockPort();
			device.Port = port;
			device.DeviceNumber = 1;

			// The space at the is an unimportant side effect; in the real world
			// the device would reject this with BADDATA.
			port.Expect("/01 0 move abs ");

			// Before the fix this would have thrown a FormatException.
			device.SendFormatInUnits("move abs {0}", new ParameterData[0]);
			port.Verify();
		}


		[Test]
		public void TestSendFormatInUnits_OneParamWithArity()
		{
			var device = CreateDeviceWithMultiParameterCommand("move abs {0}",
															   new Tuple<UnitOfMeasure, decimal, ParameterType>(
																   UnitOfMeasure.Meter,
																   1m,
																   AsciiCommandBuilder.StandardParamTypes["Int64"]));
			var port = new MockPort();
			device.Port = port;
			device.DeviceNumber = 1;

			var paramType = new ParameterType
			{
				DecimalPlaces = 1,
				IsNumeric = true,
				Arity = null
			};

			var param1 = new ParameterData(
				0,
			   "1",
				paramType,
			   UnitOfMeasure.Meter);


			var param2 = new ParameterData(
				0,
			   "2",
				paramType,
				UnitOfMeasure.Meter);

			port.Expect("/01 0 move abs 64.0 128.0");

			// Test
			device.SendFormatInUnits("move abs {0}", new ParameterData[] { param1, param2 });
			port.Verify();
		}


		[Test]
		public void TestSendFormatInUnits_MultiParam()
		{
			var device = CreateDeviceWithMultiParameterCommand(
				"with {0} do {1}",
			   new Tuple<UnitOfMeasure, decimal, ParameterType>(
				   UnitOfMeasure.Data,
				   1m,
				   AsciiCommandBuilder.StandardParamTypes["Token"]),
			   new Tuple<UnitOfMeasure, decimal, ParameterType>(
				   UnitOfMeasure.Data,
				   1m,
				   new ParameterType(new[] { "Foo", "Bar", "Baz" })));

			var port = new MockPort();
			device.Port = port;
			device.DeviceNumber = 1;

			var param1 = new ParameterData(
				0,
			    "class",
				null,
			    UnitOfMeasure.Data);

			var param2 = new ParameterData(
				1,
				"Foo",
				null,
				UnitOfMeasure.Data);

			port.Expect("/01 0 with class do Foo");

			// Test
			device.SendFormatInUnits("with {0} do {1}", new ParameterData[] { param1, param2 });
			port.Verify();
		}


		[Test]
		public void HashEquality()
		{
			var device1 = CreateDeviceWithCommands();
			var device2 = CreateDeviceWithCommands();
			AddAxis(device1, 1);
			AddAxis(device1, 2);
			AddAxis(device2, 1);
			AddAxis(device2, 2);

			Assert.AreEqual(device1.GetHashCode(), device2.GetHashCode());
		}


		private static ZaberDevice CreateDeviceWithCommands()
		{
			var device = new ZaberDevice { DeviceType = DeviceTypeMother.CreateDeviceTypeWithCommands() };

			return device;
		}


		private static ZaberDevice CreateDeviceWithMultiParameterCommand(
			string aFormat, params Tuple<UnitOfMeasure, decimal, ParameterType>[] aParamTypes)
		{
			var seenDimensions = new HashSet<Dimension>();

			ASCIICommandNode node = null;
			foreach (var part in aFormat.Split(' '))
			{
				var match = Regex.Match(part, @"\{%{0,1}(\d+)\}");
				if (match.Groups.Count > 1)
				{
					var index = int.Parse(match.Groups[1].Value);
					var paramInfo = aParamTypes[index];
					var paramType = paramInfo.Item3;
					if (null != paramType)
					{
						paramType = new ParameterType(paramType);
						paramType.Arity = part.Contains("%") ? (int?) null : 1;
					}

					node = new ASCIICommandNode(node)
					{
						NodeText = part,
						RequestUnit = paramInfo.Item1,
						RequestUnitFunction = ScalingFunction.LinearResolution,
						RequestUnitScale = paramInfo.Item2,
						ParamType = paramType
					};

					seenDimensions.Add(paramInfo.Item1.Unit.Dimension);
				}
				else
				{
					node = new ASCIICommandNode(node) { NodeText = part };
				}
			}

			var command = new ASCIICommandInfo(node);

			var commands = new List<CommandInfo> { command };

			// Add dummy commands to make sure position, velocity and acceleration 
			// are all represented so that DeviceType.UpdateHardwareUnits() and
			// ZaberDevice.UpdateResolutionUnits() will operate.
			if (!seenDimensions.Contains(Dimension.Length))
			{
				commands.Add(new CommandInfo
				{
					TextCommand = "Dummy pos",
					RequestUnit = UnitOfMeasure.Meter,
					RequestUnitScale = 1m
				});
			}

			if (!seenDimensions.Contains(Dimension.Velocity))
			{
				commands.Add(new CommandInfo
				{
					TextCommand = "Dummy vel",
					RequestUnit = UnitOfMeasure.MetersPerSecond,
					RequestUnitScale = 1m
				});
			}

			if (!seenDimensions.Contains(Dimension.Acceleration))
			{
				commands.Add(new CommandInfo
				{
					TextCommand = "Dummy accel",
					RequestUnit = UnitOfMeasure.MetersPerSecondSquared,
					RequestUnitScale = 1m
				});
			}

			var deviceType = new DeviceType
			{
				MotionType = MotionType.Linear,
				Commands = commands
			};

			var device = new ZaberDevice { DeviceType = deviceType };

			return device;
		}


		private ZaberDevice AddAxis(int aAxisNumber)
		{
			return AddAxis(_device, aAxisNumber);
		}


		private ZaberDevice AddAxis(ZaberDevice aParent, int aAxisNumber)
		{
			var axis = new ZaberDevice
			{
				DeviceType = aParent.DeviceType,
				DeviceNumber = aParent.DeviceNumber,
				AxisNumber = aAxisNumber
			};

			aParent.AddAxis(axis);
			return axis;
		}


		private void OnDeviceMessageReceived(object sender, DeviceMessageEventArgs e)
		{
			Assert.IsNull(_eventType,
				"Event type should not be set.");

			_eventType = "DataPacketReceived";
			_eventSource = sender;
			_eventArgs = e;
		}


		private void OnDeviceMessageSent(object sender, DeviceMessageEventArgs e)
		{
			Assert.IsNull(_eventType,
				"Event type should not be set.");

			_eventType = "DataPacketSent";
			_eventSource = sender;
			_eventArgs = e;
		}


		private enum Meta
		{
			Foo,
			Bar,
			Baz
		}

		private object _eventSource;
		private DeviceMessageEventArgs _eventArgs;
		private string _eventType;
		private MockRepository _mocks;
		private IZaberPort _port;
		private IEventRaiser _packetSentRaiser;
		private IEventRaiser _packetReceivedRaiser;
		private ZaberDevice _device;
	}
}

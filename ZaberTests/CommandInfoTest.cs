using System;
using System.Linq;
using NUnit.Framework;
using Zaber;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class CommandInfoTest
	{
		[Test]
		public void CommandToNumber()
		{
			// SETUP
			var command40 = new CommandInfo();
			var command55 = new CommandInfo();

			// EXEC
			command40.Command = Command.SetDeviceMode;
			command55.Number = 55;

			int command40Number = command40.Number;
			var command55Command = command55.Command;

			// VERIFY
			Assert.That(command40Number,
						Is.EqualTo(40),
						"command 40 number");
			Assert.That(command55Command,
						Is.EqualTo(Command.EchoData),
						"command 55 command");
		}


		[Test]
		public void EqualityAndHashCode()
		{
			var commandInfoA = new CommandInfo
			{
				Command = Command.Home,
				DataDescription = "AAAA",
				HasParameters = true,
				IsAxisCommand = true,
				IsBasic = true,
				IsCurrentPositionReturned = true,
				AccessLevel = 2,
				IsRequestRelativePosition = true,
				IsResponseRelativePosition = true,
				IsRetrySafe = true,
				RequestUnit = UnitOfMeasure.Data,
				RequestUnitScale = 1m,
				RequestUnitFunction = ScalingFunction.LinearResolution,
				ResponseUnit = UnitOfMeasure.Amperes,
				ResponseUnitFunction = ScalingFunction.Linear,
				ResponseUnitScale = 2m,
				ResponseDescription = "AAA",
				TextCommand = "AA",
				Name = "A",
				Number = 1
			};
			var commandInfoACopy = new CommandInfo
			{
				Command = Command.Home,
				DataDescription = "AAAA",
				HasParameters = true,
				IsAxisCommand = true,
				IsBasic = true,
				IsCurrentPositionReturned = true,
				AccessLevel = 2,
				IsRequestRelativePosition = true,
				IsResponseRelativePosition = true,
				IsRetrySafe = true,
				RequestUnit = UnitOfMeasure.Data,
				RequestUnitScale = 1m,
				RequestUnitFunction = ScalingFunction.LinearResolution,
				ResponseUnit = UnitOfMeasure.Amperes,
				ResponseUnitFunction = ScalingFunction.Linear,
				ResponseUnitScale = 2m,
				ResponseDescription = "AAA",
				TextCommand = "AA",
				Name = "A",
				Number = 1
			};
			var commandInfoB = new CommandInfo
			{
				Command = Command.EchoData,
				DataDescription = "BBBB",
				HasParameters = false,
				IsAxisCommand = false,
				IsBasic = false,
				IsCurrentPositionReturned = false,
				AccessLevel = 1,
				IsRequestRelativePosition = false,
				IsResponseRelativePosition = false,
				IsRetrySafe = false,
				RequestUnit = UnitOfMeasure.MetersPerSecond,
				RequestUnitScale = 4m,
				RequestUnitFunction = ScalingFunction.Linear,
				ResponseUnit = UnitOfMeasure.Millimeter,
				ResponseUnitFunction = ScalingFunction.Reciprocal,
				ResponseUnitScale = 9m,
				ResponseDescription = "BBB",
				TextCommand = "BB",
				Name = "B",
				Number = 2
			};

			// Check all properties, excluding read-only and obsolete ones.
			foreach (
				var property in
				typeof(CommandInfo).GetProperties()
								.Where(p =>
										   p.CanWrite
									   && (p.GetCustomAttributes(typeof(ObsoleteAttribute), false)
										 .Length
									   == 0)))
			{
				Assert.That(property.GetValue(commandInfoA, null),
							Is.EqualTo(property.GetValue(commandInfoACopy, null)));
				Assert.That(commandInfoA.Equals(commandInfoACopy));

				Assert.That(property.GetValue(commandInfoA, null),
							Is.Not.EqualTo(property.GetValue(commandInfoB, null)));
				Assert.That(!commandInfoB.Equals(commandInfoA));
			}

			Assert.That(commandInfoA.GetHashCode() == commandInfoACopy.GetHashCode());
			Assert.That(commandInfoA.GetHashCode() != commandInfoB.GetHashCode());
		}


		[Test]
		public void ReadOnlySetting()
		{
			// SETUP
			var commandInfo = new CommandInfo();
			var responseInfo = new ResponseInfo();
			var settingInfo = new SettingInfo();
			var readOnlySettingInfo = new ReadOnlySettingInfo();

			// EXEC
			Assert.That(commandInfo.IsReadOnlySetting,
						Is.False,
						"command info");
			Assert.That(responseInfo.IsReadOnlySetting,
						Is.False,
						"response info");
			Assert.That(settingInfo.IsReadOnlySetting,
						Is.False,
						"setting info");
			Assert.That(readOnlySettingInfo.IsReadOnlySetting,
						Is.True,
						"response info");
		}


		[Test]
		public void ResponseOnly()
		{
			// SETUP
			var commandInfo = new CommandInfo();
			var responseInfo = new ResponseInfo();
			var settingInfo = new SettingInfo();
			var readOnlySettingInfo = new ReadOnlySettingInfo();

			// EXEC
			Assert.That(commandInfo.IsResponseOnly,
						Is.False,
						"command info");
			Assert.That(responseInfo.IsResponseOnly,
						Is.True,
						"response info");
			Assert.That(settingInfo.IsResponseOnly,
						Is.False,
						"setting info");
			Assert.That(readOnlySettingInfo.IsResponseOnly,
						Is.False,
						"response info");
		}


		[Test]
		public void Setting()
		{
			// SETUP
			var commandInfo = new CommandInfo();
			var responseInfo = new ResponseInfo();
			var settingInfo = new SettingInfo();
			var readOnlySettingInfo = new ReadOnlySettingInfo();

			// EXEC
			Assert.That(commandInfo.IsSetting,
						Is.False,
						"command info");
			Assert.That(responseInfo.IsSetting,
						Is.False,
						"response info");
			Assert.That(settingInfo.IsSetting,
						Is.True,
						"setting info");
			Assert.That(readOnlySettingInfo.IsSetting,
						Is.True,
						"response info");
		}
	}
}

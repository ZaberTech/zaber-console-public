using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Zaber;
using Zaber.Units;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	[SetUICulture("en-US")]
	public class DeviceTypeTest
	{
		[Test]
		public void CopyConstructor()
		{
			// SETUP
			var deviceType = new DeviceType
			{
				DeviceId = 62,
				Name = "Flouroscope"
			};
			IList<CommandInfo> commands = new List<CommandInfo>
			{
				new CommandInfo { Command = Command.Renumber },
				new CommandInfo { TextCommand = "pos" }
			};
			deviceType.Commands = commands;
			const int peripheralId = 99999;
			var peripheralType = new DeviceType
			{
				PeripheralId = peripheralId,
				Name = "PER"
			};
			var peripherals = new List<DeviceType> { peripheralType };
			deviceType.PeripheralMap = peripherals.ToDictionary(dt => dt.PeripheralId);
			const MotionType expectedMotionType = MotionType.Linear;
			deviceType.MotionType = expectedMotionType;
			deviceType.Capabilities = new List<string> {"Foo", "Bar"};
			deviceType.TuningData = new TuningData
			{
				ForqueConstant = 0.17
			};

			// EXEC
			var copy = new DeviceType(deviceType);
			var binaryCommand = copy.GetCommandByNumber(Command.Renumber);
			var textCommand = copy.GetCommandByText("pos");
			var copyPeripheral = copy.GetPeripheralById(peripheralId);
			var copyMotionType = copy.MotionType;

			// VERIFY
			Assert.AreEqual("Flouroscope", copy.Name);
			Assert.AreEqual(62, copy.DeviceId);
			foreach (var cmd in deviceType.Commands)
			{
				Assert.That(copy.Commands.Contains(cmd));
			}

			foreach (var cmd in copy.Commands)
			{
				Assert.That(deviceType.Commands.Contains(cmd));
			}

			Assert.AreSame(commands[0], binaryCommand);
			Assert.AreSame(commands[1], textCommand);
			Assert.AreEqual(copyPeripheral, peripheralType);
			Assert.AreEqual(copyMotionType, expectedMotionType);
			Assert.IsTrue(copy.Equals(deviceType));
			Assert.IsTrue(copy.Capabilities.Contains("Foo"));
			Assert.IsTrue(copy.Capabilities.Contains("Bar"));
			Assert.AreEqual(2, copy.Capabilities.Count);
			Assert.AreEqual(0.17, copy.TuningData.ForqueConstant);
		}


		[Test]
		public void DuplicateCommandNumber()
		{
			// SETUP
			var deviceType = new DeviceType();
			var commands = new List<CommandInfo>
			{
				new CommandInfo(),
				new CommandInfo(),
				new CommandInfo()
			};
			commands[0].Command = Command.Home;
			commands[0].Name = "Where the heart is";
			commands[1].Command = Command.Stop;
			commands[1].Name = "In the name of love";
			commands[2].Command = Command.Stop;
			commands[2].Name = "Breakin' my heart";

			// EXEC
			string msg = null;
			try
			{
				deviceType.Commands = commands;
			}
			catch (ArgumentException ex)
			{
				msg = ex.Message;
			}

			// VERIFY
			Assert.AreEqual("More than one entry found for command Stop.\r\nParameter name: value",
							msg,
							"Message should match.");
		}


		[Test]
		public void GetCommandByNumber()
		{
			// SETUP
			var deviceType = DeviceTypeMother.CreateDeviceTypeWithCommands();

			// EXEC
			var command = deviceType.GetCommandByNumber(Command.Stop);
			var unknownCommand = deviceType.GetCommandByNumber(Command.LimitActive);

			// VERIFY
			Assert.AreEqual("In the name of love",
							command.Name,
							"Should get matching commandInfo back");
			Assert.Null(unknownCommand,
						"Should not find unknown command.");
		}


		[Test]
		public void GetCommandByText()
		{
			// SETUP
			var deviceType = DeviceTypeMother.CreateDeviceTypeWithCommands();

			// EXEC
			var command = deviceType.GetCommandByText("home");
			var setting = deviceType.GetCommandByText("get pos");
			var unknownCommand = deviceType.GetCommandByText("floop");

			// VERIFY
			Assert.Null(unknownCommand,
						"Should not find unknown command.");
		}


		[Test]
		public void LookUpPeripheral()
		{
			// SETUP
			var deviceType = DeviceTypeMother.CreateDeviceTypeWithPeripherals();

			// EXEC
			var peripheral = deviceType.GetPeripheralById(12345);

			// VERIFY
			Assert.AreEqual("BIG-STAGE",
							peripheral.Name,
							"Should get matching peripheral back");
			Assert.AreEqual(12345,
							peripheral.PeripheralId,
							"Incorrect peripheral returned. Should be peripheral 12345.");
		}


		[Test]
		public void NullPeripherals()
		{
			// SETUP
			var deviceType = new DeviceType
			{
				// EXEC
				PeripheralMap = null
			};
		}


		[Test]
		public void TestIsJoystick()
		{
			var deviceType = new DeviceType
			{
				DeviceId = 51000,
				Name = "Flouroscope"
			};

			// Device ID is not used to identify joysticks.
			Assert.IsFalse(deviceType.IsJoystick);

			var commands = new List<CommandInfo> { new CommandInfo { Command = Command.SetAxisDeviceNumber } };

			deviceType.Commands = commands;

			// Axis commands give positive result.
			Assert.IsTrue(deviceType.IsJoystick);

			commands = new List<CommandInfo> { new CommandInfo { Command = Command.LoadEventInstruction } };

			deviceType.Commands = commands;

			// Button commands give positive result.
			Assert.IsTrue(deviceType.IsJoystick);
		}


		[Test]
		public void TestIsRotaryDevice()
		{
			var deviceType = new DeviceType
			{
				DeviceId = 50001,
				Name = "X-RST120AK"
			};

			// Device ID and name are not used to identify rotary stages.
			Assert.IsFalse(deviceType.IsRotaryDevice);

			// Device name is not sufficient.
			Assert.IsFalse(deviceType.IsJoystick);

			deviceType.DeviceTypeUnitConversionInfo[Dimension.Angle] = new ParameterUnitConversion();

			// Having an angle unit conversion is necessary and sufficient.
			Assert.IsTrue(deviceType.IsRotaryDevice);
		}


		[Test]
		public void TextCommands()
		{
			// SETUP
			var asciiCommands = new List<CommandInfo>
			{
				new CommandInfo { TextCommand = "move abs" },
				new CommandInfo { TextCommand = "move rel" }
			};

			// EXEC
			var deviceType = new DeviceType { Commands = asciiCommands };

			// VERIFY
			Assert.That(deviceType.Commands.Count,
						Is.EqualTo(2),
						"command count");
		}


		[Test]
		public void ToStringWithName()
		{
			// SETUP
			var deviceType = new DeviceType
			{
				DeviceId = 62,
				Name = "Flouroscope"
			};

			// EXEC
			var toString = deviceType.ToString();

			// VERIFY
			Assert.AreEqual("Flouroscope",
							toString,
							"ToString() result should match");
		}


		[Test]
		public void ToStringWithoutName()
		{
			// SETUP
			var deviceType = new DeviceType { DeviceId = 62 };

			// EXEC
			var toString = deviceType.ToString();

			// VERIFY
			Assert.AreEqual("Zaber DeviceId 62, Firmware Version 0.00",
							toString,
							"ToString() result should match");
		}


		[Test]
		public void HashEquals()
		{
			var random = new Random();
			var seed1 = random.Next();
			var seed2 = random.Next();
			var deviceType1 = CreateHashTestDevice(seed1);
			var deviceType2 = CreateHashTestDevice(seed2);
			Assert.AreEqual(deviceType1.GetHashCode(), deviceType2.GetHashCode(), $"Hash codes not equal with random seeds {seed1} and {seed2}.");
		}


		private DeviceType CreateHashTestDevice(int aRandomSeed)
		{
			// Populate Simple properties.
			var deviceType = new DeviceType()
			{
				DeviceId = 12345,
				FirmwareVersion = new FirmwareVersion(7,8,9),
				MotionType = MotionType.Linear,
				Name = "Foo",
				PeripheralId = 56789,
				UnitConverter = ConversionTable.Default
			};


			// Populate command list.
			var commandData = new List<CommandInfo>
			{
				new CommandInfo() { TextCommand = "move abs {0}" },
				new CommandInfo() { TextCommand = "move rel {0}" },
				new CommandInfo() { TextCommand = "stop" },
			};

			// For all of the collection members, keep the content the same
			// but randomly permute the order added.
			var r = new Random(aRandomSeed);
			var commands = new List<CommandInfo>();
			while (commandData.Count > 0)
			{
				var i = r.Next(commandData.Count);
				commands.Add(commandData[i]);
				commandData.RemoveAt(i);
			}

			deviceType.Commands = commands;


			// Populate capabilties.
			var capData = new List<string>
			{
				"foo", "bar", "baz"
			};

			while (capData.Count > 0)
			{
				var i = r.Next(capData.Count);
				deviceType.Capabilities.Add(capData[i]);
				capData.RemoveAt(i);
			}


			// Populate default unit conversions.
			var deviceUnits = new List<Tuple<Dimension, IParameterConversionInfo>>
			{
				new Tuple<Dimension, IParameterConversionInfo>(Dimension.Length, new ParameterUnitConversion
				{
					Function = ScalingFunction.LinearResolution,
					Scale = 1.0m,
					ReferenceUnit = UnitConverter.Default.FindUnitBySymbol(UnitOfMeasure.MeterSymbol)
				}),
				new Tuple<Dimension, IParameterConversionInfo>(Dimension.Velocity, new ParameterUnitConversion
				{
					Function = ScalingFunction.LinearResolution,
					Scale = 1.6384m,
					ReferenceUnit = UnitConverter.Default.FindUnitBySymbol(UnitOfMeasure.MetersPerSecondSymbol)
				}),
				new Tuple<Dimension, IParameterConversionInfo>(Dimension.Acceleration, new ParameterUnitConversion
				{
					Function = ScalingFunction.LinearResolution,
					Scale = 16384m,
					ReferenceUnit = UnitConverter.Default.FindUnitBySymbol(UnitOfMeasure.MetersPerSecondSquaredSymbol)
				}),
			};

			while (deviceUnits.Count > 0)
			{
				var i = r.Next(deviceUnits.Count);
				deviceType.DeviceTypeUnitConversionInfo.Add(deviceUnits[i].Item1, deviceUnits[i].Item2);
				deviceUnits.RemoveAt(i);
			}


			// Populate tuning data.
			deviceType.TuningData = new TuningData
			{
				Inertia = new Zaber.Units.Measurement(1500m, UnitConverter.Default.FindUnitBySymbol("g")),
				ForqueConstant = 17.0,
				ControllerVersions = new Dictionary<string, int>
				{
					{ "1", 1 },
					{ "2", 2 }
				},
				GuidanceData = "{ 'Whatever': 'dude' }"
			};


			// Populate peripheral map.
			var peripherals = new List<Tuple<int, DeviceType>>
			{
				new Tuple<int, DeviceType>(1, new DeviceType { DeviceId = 123 }),
				new Tuple<int, DeviceType>(2, new DeviceType { DeviceId = 456 }),
				new Tuple<int, DeviceType>(3, new DeviceType { DeviceId = 789 }),
			};

			while (peripherals.Count > 0)
			{
				var i = r.Next(peripherals.Count);
				deviceType.PeripheralMap.Add(peripherals[i].Item1, peripherals[i].Item2);
				peripherals.RemoveAt(i);
			}

			return deviceType;
		}
	}
}

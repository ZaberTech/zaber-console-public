using NUnit.Framework;
using Zaber;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class DeviceMessageTest
	{
		[Test]
		public void Clone()
		{
			// SETUP
			var message = new DeviceMessage();
			message.NumericData = 42;
			message.CommandInfo = CreateConstantSpeed();

			// EXEC
			var clone = new DeviceMessage(message);

			// VERIFY
			Assert.That(clone.NumericData,
						Is.EqualTo(message.NumericData),
						"data");
			Assert.That(clone.CommandInfo,
						Is.EqualTo(message.CommandInfo),
						"command info");
		}


		[Test]
		public void FormatRequest()
		{
			// SETUP
			var packet = new DataPacket();
			packet.Command = Command.MoveAtConstantSpeed;
			packet.NumericData = 100;
			packet.DeviceNumber = 3;
			var message = new DeviceMessage(packet);
			message.CommandInfo = CreateConstantSpeed();

			var logText = message.FormatRequest();

			// VERIFY
			Assert.AreEqual("Device   3 requests Constant Speed(Velocity = 100)",
							logText,
							"log text should match");
		}


		[Test]
		public void FormatRequestNoData()
		{
			// SETUP
			var packet = new DataPacket();
			packet.Command = Command.Stop;
			packet.DeviceNumber = 100;
			var message = new DeviceMessage(packet);
			message.CommandInfo = CreateStop();

			// EXEC
			var logText = message.FormatRequest();

			// VERIFY
			Assert.AreEqual("Device 100 requests Stop",
							logText,
							"log text should match");
		}


		[Test]
		public void FormatText()
		{
			// SETUP
			var text = "/42 move abs 4000";
			var message = new DeviceMessage(text);

			// EXEC
			var request = message.FormatRequest();
			var response = message.FormatResponse();

			// VERIFY
			Assert.That(request,
						Is.EqualTo(text),
						"request");
			Assert.That(response,
						Is.EqualTo(text),
						"response");
		}


		[Test]
		public void LogErrorResponse()
		{
			// SETUP
			var packet = new DataPacket();
			packet.Command = Command.Error;
			packet.NumericData = (int) ZaberError.AliasInvalid;
			packet.DeviceNumber = 33;

			var message = new DeviceMessage(packet);
			message.CommandInfo = CreateError();

			// EXEC
			var logText = message.FormatResponse();

			// VERIFY
			Assert.AreEqual("Device  33 responds Error(AliasInvalid)",
							logText,
							"log text should match");
		}


		[Test]
		public void LogRequestWithId()
		{
			// SETUP
			var packet = new DataPacket();
			packet.Command = Command.MoveAtConstantSpeed;
			packet.NumericData = 100;
			packet.DeviceNumber = 3;
			var message = new DeviceMessage(packet);
			message.CommandInfo = CreateConstantSpeed();
			message.MessageId = 92;

			// EXEC
			var logText = message.FormatRequest();

			// VERIFY
			Assert.AreEqual("092: Device   3 requests Constant Speed(Velocity = 100)",
							logText,
							"log text should match");
		}


		[Test]
		public void LogRequestWithIdZero()
		{
			// SETUP
			var packet = new DataPacket();
			packet.Command = Command.MoveAtConstantSpeed;
			packet.NumericData = 100;
			packet.DeviceNumber = 3;
			var message = new DeviceMessage(packet);
			message.CommandInfo = CreateConstantSpeed();
			message.MessageId = 0; // No Id

			// EXEC
			var logText = message.FormatRequest();

			// VERIFY
			Assert.AreEqual("     Device   3 requests Constant Speed(Velocity = 100)",
							logText,
							"log text should match");
		}


		[Test]
		public void LogResponse()
		{
			// SETUP
			var packet = new DataPacket();
			packet.Command = Command.MoveAtConstantSpeed;
			packet.NumericData = 500;
			packet.DeviceNumber = 33;

			var message = new DeviceMessage(packet);
			message.CommandInfo = CreateConstantSpeed();

			// EXEC
			var logText = message.FormatResponse();

			// VERIFY
			Assert.AreEqual("Device  33 responds Constant Speed(Velocity = 500)",
							logText,
							"log text should match");
		}


		[Test]
		public void LogResponseNoData()
		{
			// SETUP
			var packet = new DataPacket();
			packet.Command = Command.Stop;
			packet.NumericData = 0;
			packet.DeviceNumber = 3;

			var message = new DeviceMessage(packet);
			message.CommandInfo = CreateStop();

			// EXEC
			var logText = message.FormatResponse();

			// VERIFY
			Assert.AreEqual("Device   3 responds Stop",
							logText,
							"log text should match");
		}


		[Test]
		public void LogUnknownErrorResponse()
		{
			// SETUP
			var packet = new DataPacket();
			packet.Command = Command.Error;
			packet.NumericData = 7777; // no error code uses this
			packet.DeviceNumber = 33;

			var message = new DeviceMessage(packet);
			message.CommandInfo = CreateError();

			// EXEC
			var logText = message.FormatResponse();

			// VERIFY
			Assert.AreEqual("Device  33 responds Error(Error Code = 7777)",
							logText,
							"log text should match");
		}


		[Test]
		public void LogUnknownResponse()
		{
			// SETUP
			var packet = new DataPacket();
			packet.Command = Command.SetCurrentPosition;
			packet.NumericData = 9904;
			packet.DeviceNumber = 3;

			var message = new DeviceMessage(packet);

			// Don't set command info

			// EXEC
			var logText = message.FormatResponse();

			// VERIFY
			Assert.AreEqual("Device   3 responds SetCurrentPosition(9904)",
							logText,
							"log text should match");
		}


		[Test]
		public void LogUnknownResponseWithId()
		{
			// SETUP
			var packet = new DataPacket();
			packet.Command = Command.SetCurrentPosition; // not in DeviceTypes list
			packet.NumericData = 9904;
			packet.DeviceNumber = 3;

			var message = new DeviceMessage(packet);
			message.MessageId = 0; // no id, but ids are enabled

			// EXEC
			var logText = message.FormatResponse();

			// VERIFY
			Assert.AreEqual("     Device   3 responds SetCurrentPosition(9904)",
							logText,
							"log text should match");
		}


		private CommandInfo CreateConstantSpeed()
		{
			var command = new CommandInfo();
			command.Command = Command.MoveAtConstantSpeed;
			command.Name = "Constant Speed";
			command.DataDescription = "Velocity";
			command.ResponseDescription = "Velocity";
			return command;
		}


		private CommandInfo CreateError()
		{
			var command = new CommandInfo();
			command.Command = Command.Error;
			command.Name = "Error";
			command.ResponseDescription = "Error Code";
			return command;
		}


		private CommandInfo CreateStop()
		{
			var command = new CommandInfo();
			command.Command = Command.Stop;
			command.Name = "Stop";
			return command;
		}
	}
}

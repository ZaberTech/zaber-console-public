﻿using System;
using System.Linq;
using NUnit.Framework;
using Zaber;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class DataPacketTest
	{
		[Test]
		public void AlertWithChecksum()
		{
			// EXEC
			var packet = new DataPacket("!01 2 IDLE FG:FA");

			// VERIFY
			Assert.AreEqual(MessageType.Alert, packet.MessageType, "message type");
			Assert.AreEqual(1, packet.DeviceNumber, "device number");
			Assert.AreEqual(2, packet.AxisNumber, "axis number");
			Assert.AreEqual("IDLE FG", packet.Body, "body");
			Assert.IsTrue(packet.IsIdle, "status");
			Assert.AreEqual("FG", packet.FlagText, "warning flag");
			Assert.IsTrue(packet.HasFault, "has fault warning flag");
			Assert.IsTrue(packet.IsChecksumInvalid, "check sum");
		}


		[Test]
		public void AlertWithFaultWarningFlag()
		{
			// EXEC
			var packet = new DataPacket("!01 2 IDLE FG");

			// VERIFY
			Assert.AreEqual(MessageType.Alert, packet.MessageType, "message type");
			Assert.AreEqual(1, packet.DeviceNumber, "device number");
			Assert.AreEqual(2, packet.AxisNumber, "axis number");
			Assert.AreEqual("IDLE FG", packet.Body, "body");
			Assert.IsTrue(packet.IsIdle, "status");
			Assert.AreEqual("FG", packet.FlagText, "warning flag");
			Assert.IsTrue(packet.HasFault, "has fault warning flag");
		}


		[Test]
		public void AlertWithWarningFlag()
		{
			// EXEC
			var packet = new DataPacket("!01 2 IDLE XY");

			// VERIFY
			Assert.AreEqual(MessageType.Alert, packet.MessageType, "message type");
			Assert.AreEqual(1, packet.DeviceNumber, "device number");
			Assert.AreEqual(2, packet.AxisNumber, "axis number");
			Assert.AreEqual("IDLE XY", packet.Body, "body");
			Assert.IsTrue(packet.IsIdle, "status");
			Assert.AreEqual("XY", packet.FlagText, "warning flag");
			Assert.IsFalse(packet.HasFault, "has fault warning flag");
		}


		[Test]
		public void BusyAlert()
		{
			// EXEC
			var packet = new DataPacket("!01 2 BUSY --");

			// VERIFY
			Assert.AreEqual(MessageType.Alert, packet.MessageType, "message type");
			Assert.AreEqual(1, packet.DeviceNumber, "device number");
			Assert.AreEqual(2, packet.AxisNumber, "axis number");
			Assert.AreEqual("BUSY --", packet.Body, "body");
			Assert.IsFalse(packet.IsIdle, "status");
			Assert.AreEqual(WarningFlags.None, packet.FlagText, "warning flag");
		}


		[Test]
		public void ChecksumBadFormat()
		{
			// SETUP
			var text = "@01 0 OK IDLE -- 23:--";

			// EXEC
			var packet = new DataPacket(text);
			var isChecksumInvalid = packet.IsChecksumInvalid;
			var exception = Assert.Throws<ZaberPortErrorException>(delegate { packet.ValidateChecksum(); });

			// VERIFY
			Assert.AreEqual(ZaberPortError.InvalidChecksum, exception.ErrorType, "error type");
			Assert.IsTrue(isChecksumInvalid, "is checksum invalid");
		}


		[Test]
		public void ChecksumBadValue()
		{
			// SETUP
			var text = "@01 0 OK IDLE -- 23:88";

			// EXEC
			var packet = new DataPacket(text);
			var isChecksumInvalid = packet.IsChecksumInvalid;
			var exception = Assert.Throws<ZaberPortErrorException>(delegate { packet.ValidateChecksum(); });

			// VERIFY
			Assert.AreEqual(ZaberPortError.InvalidChecksum, exception.ErrorType, "error type");
			Assert.IsTrue(isChecksumInvalid, "is checksum invalid");
		}


		[Test]
		public void ChecksumGood()
		{
			// SETUP
			var text = "@01 0 OK IDLE -- 23:58";
			var expectedBody = "OK IDLE -- 23";
			var expectedTextData = "23";

			// EXEC
			var packet = new DataPacket(text);
			var isChecksumInvalid = packet.IsChecksumInvalid;
			packet.ValidateChecksum();

			// VERIFY
			Assert.AreEqual(1, packet.DeviceNumber, "device number");
			Assert.AreEqual(23m, packet.NumericData, "data");
			Assert.AreEqual(text, packet.Text, "text");
			Assert.AreEqual(text, packet.FormatResponse(), "FormatResponse");
			Assert.AreEqual(expectedBody, packet.Body, "body");
			Assert.AreEqual(expectedTextData, packet.TextData, "text data");
			Assert.IsFalse(isChecksumInvalid, "is checksum invalid");
		}


		[Test]
		public void ChecksumOnlyCommand()
		{
			// SETUP
			var text = "/:00";

			// EXEC
			var packet = new DataPacket(text);
			var isChecksumInvalid = packet.IsChecksumInvalid;
			packet.ValidateChecksum();

			// VERIFY
			Assert.AreEqual(0, packet.DeviceNumber, "device number");
			Assert.AreEqual(string.Empty, packet.Body, "body");
			Assert.IsFalse(isChecksumInvalid, "is checksum invalid");
		}


		[Test]
		public void Comment()
		{
			// EXEC
			var packet = new DataPacket("#01 0 I am an info message");

			// VERIFY
			Assert.AreEqual(MessageType.Comment, packet.MessageType, "message type");
			Assert.AreEqual("I am an info message", packet.Body, "info message");
		}


		[Test]
		public void CopyText()
		{
			// SETUP
			var text = "@01 2 OK IDLE FQ 1000 2000";
			var expectedBody = "OK IDLE FQ 1000 2000";
			var expectedFlagText = "FQ";
			var expectedHasFault = true;
			var expectedTextData = "1000 2000";
			var expectedTextDataValues = new[] { "1000", "2000" };
			var expectedDataValues = new[] { 1000m, 2000m };
			var expectedDataValuesInt = new[] { 1000, 2000 };

			// EXEC
			var packet1 = new DataPacket(text);
			var packet2 = new DataPacket(packet1);

			// VERIFY
			Assert.AreEqual(1, packet2.DeviceNumber, "device number");
			Assert.AreEqual(2, packet2.AxisNumber, "axis number");
			Assert.IsFalse(packet2.IsError, "is error");
			Assert.IsTrue(packet2.IsIdle, "is idle");
			Assert.AreEqual(1000m, packet2.NumericData, "data");
			Assert.AreEqual(expectedDataValues, packet2.NumericValues, "data values");
			#pragma warning disable CS0618 // Type or member is obsolete
			Assert.AreEqual(1000, packet2.Data, "legacy data");
			Assert.AreEqual(expectedDataValues, packet2.NumericDataValues, "legacy data values");
			Assert.AreEqual(expectedDataValuesInt, packet2.DataValues, "legacy data values");
			#pragma warning restore CS0618 // Type or member is obsolete
			Assert.AreEqual(MessageType.Response, packet2.MessageType, "message type");
			Assert.AreEqual(text, packet2.Text, "text");
			Assert.AreEqual(text, packet2.FormatResponse(), "FormatResponse");
			Assert.AreEqual(text, packet2.FormatRequest(), "FormatRequest");
			Assert.AreEqual(expectedBody, packet2.Body, "body");
			Assert.AreEqual(expectedTextData, packet2.TextData, "text data");
			Assert.AreEqual(expectedTextDataValues, packet2.TextDataValues, "text data values");
			Assert.AreEqual(expectedFlagText, packet2.FlagText, "flag text");
			Assert.AreEqual(expectedHasFault, packet2.HasFault, "has fault");
		}


		[Test]
		public void DataList()
		{
			// SETUP
			var text = "@01 0 OK IDLE -- 1000 2000";
			var expectedTextData = "1000 2000";
			var expectedTextDataValues = new[] { "1000", "2000" };
			var expectedDataValues = new[] { 1000m, 2000m };
			var expectedDataValuesInt = new[] { 1000, 2000 };

			// EXEC
			var packet = new DataPacket(text);

			// VERIFY
			Assert.AreEqual(expectedTextData, packet.TextData, "text data");
			Assert.AreEqual(expectedTextDataValues, packet.TextDataValues, "text data values");
			Assert.AreEqual(expectedDataValues, packet.NumericValues, "data values");
			#pragma warning disable CS0618 // Type or member is obsolete
			Assert.AreEqual(expectedDataValues, packet.NumericDataValues, "legacy data values");
			Assert.AreEqual(expectedDataValuesInt, packet.DataValues, "legacy data values");                     
			#pragma warning restore CS0618 // Type or member is obsolete
		}


		[Test]
		public void DataIterator()
		{
			var packet = new DataPacket("@01 0 OK IDLE -- 2 3 NA 5");
			var expected = new decimal?[] {2m, 3m, null, 5m};

			var calls = 0;

			packet.ForEachNumericValue((i, x) =>
			{
				Assert.AreEqual(calls, i);
				Assert.AreEqual(expected[i], x);
				calls++;
			});

			Assert.AreEqual(4, calls);
		}


		[Test]
		public void DeviceAddressAndAxisNumber()
		{
			// EXEC
			var packet = new DataPacket("/2 1 move abs 10000");

			// VERIFY
			Assert.AreEqual(2, packet.DeviceNumber, "device number");
			Assert.AreEqual(1, packet.AxisNumber, "axis number");
			Assert.AreEqual(MessageIdType.None, packet.MessageIdType, "message ID type");
		}


		[Test]
		public void DeviceAddressAxisNumberAndMessageId()
		{
			// EXEC
			var packet = new DataPacket("/2 1 56 move abs 10000");

			// VERIFY
			Assert.AreEqual(2, packet.DeviceNumber, "device number");
			Assert.AreEqual(1, packet.AxisNumber, "axis number");
			Assert.AreEqual(56, packet.MessageId, "message ID");
			Assert.AreEqual(MessageIdType.Numeric, packet.MessageIdType, "message ID type");
		}


		[Test]
		public void DeviceAddressOnly()
		{
			// EXEC
			var packet = new DataPacket("/2 move abs 10000");

			// VERIFY
			Assert.AreEqual(2, packet.DeviceNumber, "device address");
			Assert.AreEqual(0, packet.AxisNumber, "axis number");
			Assert.AreEqual(MessageIdType.None, packet.MessageIdType, "message ID type");
		}


		[Test]
		public void FromBusyString()
		{
			// EXEC
			var packet = new DataPacket("@01 0 OK BUSY -- 0");

			// VERIFY
			Assert.IsFalse(packet.IsIdle, "is idle");
		}


		[Test]
		public void FromErrorString()
		{
			// EXEC
			var packet = new DataPacket("@01 0 RJ IDLE -- BADDATA");

			// VERIFY
			Assert.IsTrue(packet.IsError, "is error");
			Assert.AreEqual("BADDATA", packet.TextData, "text data");
		}


		[Test]
		public void FromMultipleDataString()
		{
			// EXEC
			var packet = new DataPacket("@01 0 OK IDLE -- 23 mm");

			// VERIFY
			Assert.AreEqual(23m, packet.NumericData, "data");
			Assert.AreEqual("23 mm", packet.TextData, "text data");
		}


		[Test]
		public void FromString()
		{
			// SETUP
			var text = "@01 2 OK IDLE -- 1000";
			var expectedBody = "OK IDLE -- 1000";
			var expectedTextData = "1000";

			// EXEC
			var packet = new DataPacket(text);

			// VERIFY
			Assert.AreEqual(1, packet.DeviceNumber, "device number");
			Assert.AreEqual(2, packet.AxisNumber, "axis number");
			Assert.IsFalse(packet.IsError, "is error");
			Assert.IsTrue(packet.IsIdle, "is idle");
			Assert.AreEqual(1000m, packet.NumericData, "data");
			Assert.AreEqual(MessageType.Response, packet.MessageType, "message type");
			Assert.AreEqual(text, packet.Text, "text");
			Assert.AreEqual(text, packet.FormatResponse(), "FormatResponse");
			Assert.AreEqual(text, packet.FormatRequest(), "FormatRequest");
			Assert.AreEqual(expectedBody, packet.Body, "body");
			Assert.AreEqual(expectedTextData, packet.TextData, "text data");
			Assert.IsTrue(packet.IsComplete);
			Assert.IsFalse(packet.IsContinuation);
		}


		[Test]
		public void FromStringWithFlags()
		{
			// SETUP
			var text1 = "@01 2 OK IDLE -- 1000";
			var text2 = "@01 2 OK IDLE WX 1000";
			var text3 = "@01 2 OK IDLE FY 1000";

			// EXEC
			var packet1 = new DataPacket(text1);
			var packet2 = new DataPacket(text2);
			var packet3 = new DataPacket(text3);
			var hasFault1 = packet1.HasFault;
			var hasFault2 = packet2.HasFault;
			var hasFault3 = packet3.HasFault;

			// VERIFY
			Assert.AreEqual(WarningFlags.None, packet1.FlagText, "no flag");
			Assert.AreEqual("WX", packet2.FlagText, "warning flag");
			Assert.AreEqual("FY", packet3.FlagText, "fault flag");
			Assert.IsFalse(hasFault1, "no flag has fault");
			Assert.IsFalse(hasFault2, "warning has fault");
			Assert.IsTrue(hasFault3, "fault has fault");
		}


		[Test]
		public void ValueNotAvailable()
		{
			// EXEC
			var packet = new DataPacket("@01 0 OK IDLE -- 1 2 NA 4");

			// VERIFY
			Assert.AreEqual(1, packet.NumericData);
			Assert.AreEqual(new decimal?[] { 1, 2, null, 4 }, packet.NumericValues);

			#pragma warning disable CS0618 // Type or member is obsolete
			Assert.AreEqual(1, packet.Data);
			Assert.AreEqual(new int[] { 1, 2, 0, 4 }, packet.DataValues);
			Assert.AreEqual(new decimal[] { 1, 2, 0, 4 }, packet.NumericDataValues);
			#pragma warning restore CS0618 // Type or member is obsolete

			Assert.AreEqual("1 2 NA 4", packet.TextData);
			Assert.AreEqual(new string[] { "1", "2", "NA", "4" }, packet.TextDataValues);
		}


		[Test]
		public void IdleAlert()
		{
			// EXEC
			var packet = new DataPacket("!01 2 IDLE --");

			// VERIFY
			Assert.AreEqual(MessageType.Alert, packet.MessageType, "message type");
			Assert.AreEqual(1, packet.DeviceNumber, "device number");
			Assert.AreEqual(2, packet.AxisNumber, "axis number");
			Assert.AreEqual("IDLE --", packet.Body, "body");
			Assert.IsTrue(packet.IsIdle, "status");
			Assert.AreEqual(WarningFlags.None, packet.FlagText, "warning flag");
		}


		[Test]
		public void MalformedText([Values("@2a 3 OK IDLE -- 0",
									  "!",
									  "3421578342",
									  "AAAA",
									  "?RM - RF /.*A QQ 4",
									  "#")]
								  string text) => Assert.Throws<MalformedTextException>(() =>
		{
			new DataPacket(
				text + "\r\n");
		});


		[Test]
		public void MessageTypeIsUnknownByDefault()
		{
			// EXEC
			var packet = new DataPacket();

			// VERIFY
			Assert.AreEqual(MessageType.Unknown, packet.MessageType, "message type");
		}


		[Test]
		public void NonnumericData()
		{
			// EXEC
			var packet = new DataPacket("@01 0 OK IDLE -- xy23");

			// VERIFY
			Assert.AreEqual(0m, packet.NumericData, "data");
		}


		[Test]
		public void NoNumber()
		{
			// EXEC
			var packet = new DataPacket("/move abs 10000");

			// VERIFY
			Assert.AreEqual(0, packet.DeviceNumber, "device number");
			Assert.AreEqual(0, packet.AxisNumber, "axis number");
			Assert.AreEqual(MessageIdType.None, packet.MessageIdType, "message ID type");
		}


		[Test]
		public void OtherAlert()
		{
			// EXEC
			var packet = new DataPacket("!01 2 track 12345");

			// VERIFY
			Assert.AreEqual(MessageType.Alert, packet.MessageType, "message type");
			Assert.AreEqual(1, packet.DeviceNumber, "device number");
			Assert.AreEqual(2, packet.AxisNumber, "axis number");
			Assert.AreEqual("track 12345", packet.Body, "body");
		}


		[Test]
		public void Request()
		{
			// EXEC
			var packet = new DataPacket("/01 move abs 10000");

			// VERIFY
			Assert.AreEqual(MessageType.Request, packet.MessageType, "message type");
			Assert.AreEqual(1, packet.DeviceNumber, "device number");
		}


		[Test]
		public void SuppressReplyMessageId()
		{
			// EXEC
			var packet = new DataPacket("/2 1 -- move abs 10000");

			// VERIFY
			Assert.AreEqual(2, packet.DeviceNumber, "device number");
			Assert.AreEqual(1, packet.AxisNumber, "axis number");
			Assert.AreEqual(MessageIdType.SuppressReply, packet.MessageIdType, "message ID type");
		}


		[Test]
		public void ToBeContinued()
		{
			var packet = new DataPacket(@"@01 0 OK IDLE -- 0\");

			Assert.IsFalse(packet.IsComplete);
			Assert.IsFalse(packet.IsContinuation);
			Assert.IsFalse(packet.Body.Contains(@"\"));
		}


		[Test]
		public void ToBeContinuedEmptyPayload()
		{
			var packet = new DataPacket(@"@01 0 OK IDLE -- \");

			Assert.IsFalse(packet.IsComplete);
			Assert.IsFalse(packet.IsContinuation);
			Assert.IsFalse(packet.Body.Contains(@"\"));
		}


		[Test]
		public void ToBeContinuedWithChecksum()
		{
			var packet = new DataPacket(@"@01 0 OK IDLE -- 0\:FE");

			Assert.IsFalse(packet.IsComplete);
			Assert.IsFalse(packet.IsContinuation);
			Assert.IsTrue(packet.IsChecksumInvalid);
			Assert.IsFalse(packet.Body.Contains(@"\"));
		}


		[Test]
		public void ContinuationMiddle()
		{
			var packet = new DataPacket(@"#01 0 cont 0\");

			Assert.IsFalse(packet.IsComplete);
			Assert.IsTrue(packet.IsContinuation);
			Assert.IsFalse(packet.Body.Contains(@"\"));
			Assert.IsFalse(packet.Body.Contains("cont"));
		}


		[Test]
		public void ContinuationEnd()
		{
			var packet = new DataPacket(@"#01 0 cont 0");

			Assert.IsTrue(packet.IsComplete);
			Assert.IsTrue(packet.IsContinuation);
			Assert.IsFalse(packet.Body.Contains("cont"));
		}


		[Test]
		public void Merge()
		{
			var messages = new string[]
			{
				@"@01 2 45 OK IDLE -- 1 2 3\",
				@"#01 2 45 cont 4 5 6\",
				"#01 2 45 cont 7 8 9"
			};

			var packet = DataPacket.JoinContinuations(messages.Select(str => new DataPacket(str)).ToList());

			Assert.AreEqual(1, packet.DeviceNumber);
			Assert.AreEqual(2, packet.AxisNumber);
			Assert.AreEqual(45, packet.MessageId);
			Assert.AreEqual(MessageType.Response, packet.MessageType);
			Assert.IsTrue(packet.IsComplete);
			Assert.IsFalse(packet.IsContinuation);
			Assert.AreEqual("OK IDLE -- 1 2 3 4 5 6 7 8 9", packet.Body);
			Assert.AreEqual(9, packet.NumericValues.Count);
			Assert.IsTrue(packet.NumericValues.Aggregate(true, (s, n) => s && (packet.NumericValues.IndexOf(n) == (int)n - 1)));
		}


		[Test]
		public void MergeNull()
		{
			Assert.Throws<ArgumentNullException>(() => DataPacket.JoinContinuations(null));
		}


		[Test]
		public void MergeEmpty()
		{
			Assert.Throws<InvalidOperationException>(() => DataPacket.JoinContinuations(new DataPacket[0]));
		}


		[Test]
		public void MergeWrongDevice()
		{
			var messages = new string[]
			{
				@"@01 2 45 OK IDLE -- 1 2 3\",
				"#02 2 45 cont 4 5 6"
			};

			Assert.Throws<InvalidOperationException>(() => DataPacket.JoinContinuations(messages.Select(str => new DataPacket(str)).ToList()));
		}


		[Test]
		public void MergeWrongAxis()
		{
			var messages = new string[]
			{
				@"@01 2 45 OK IDLE -- 1 2 3\",
				"#01 1 45 cont 4 5 6"
			};

			Assert.Throws<InvalidOperationException>(() => DataPacket.JoinContinuations(messages.Select(str => new DataPacket(str)).ToList()));
		}


		[Test]
		public void MergeWrongMessageID()
		{
			var messages = new string[]
			{
				@"@01 2 45 OK IDLE -- 1 2 3\",
				"#01 2 46 cont 4 5 6"
			};

			Assert.Throws<InvalidOperationException>(() => DataPacket.JoinContinuations(messages.Select(str => new DataPacket(str)).ToList()));
		}


		[Test]
		public void MergeMissingHeader()
		{
			var messages = new string[]
			{
				@"#01 2 45 cont 1 2 3\",
				"#01 2 45 cont 4 5 6"
			};

			Assert.Throws<InvalidOperationException>(() => DataPacket.JoinContinuations(messages.Select(str => new DataPacket(str)).ToList()));
		}


		[Test]
		public void MergeNotToBeContinued()
		{
			var messages = new string[]
			{
				"@01 2 45 OK IDLE -- 1 2 3",
				"#01 2 45 cont 4 5 6"
			};

			Assert.Throws<InvalidOperationException>(() => DataPacket.JoinContinuations(messages.Select(str => new DataPacket(str)).ToList()));
		}


		[Test]
		public void MergeMissingContinuation()
		{
			var messages = new string[]
			{
				@"@01 2 45 OK IDLE -- 1 2 3\",
			};

			Assert.Throws<InvalidOperationException>(() => DataPacket.JoinContinuations(messages.Select(str => new DataPacket(str)).ToList()));
		}
		

		[Test]
		public void MergeNotContinuation()
		{
			var messages = new string[]
			{
				"@01 2 45 OK IDLE -- 1 2 3",
				"@01 2 45 cont 4 5 6"
			};

			Assert.Throws<InvalidOperationException>(() => DataPacket.JoinContinuations(messages.Select(str => new DataPacket(str)).ToList()));
		}


		[Test]
		public void MergeMissingEnd()
		{
			var messages = new string[]
			{
				@"@01 2 45 OK IDLE -- 1 2 3\",
				@"#01 2 45 cont 4 5 6\"
			};

			Assert.Throws<InvalidOperationException>(() => DataPacket.JoinContinuations(messages.Select(str => new DataPacket(str)).ToList()));
		}
	}
}

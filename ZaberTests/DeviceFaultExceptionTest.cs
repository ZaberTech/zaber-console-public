﻿using NUnit.Framework;
using Zaber;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class DeviceFaultExceptionTest
	{
		[Test]
		public void Message()
		{
			// SETUP
			var deviceMessage = new DeviceMessage("@03 0 OK IDLE FX 0");
			var ex = new DeviceFaultException(deviceMessage);

			// EXEC
			var msg = ex.Message;

			// VERIFY
			Assert.IsTrue(msg.Contains("Fault FX reported by device"),
						  "message");
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using NUnit.Framework;
using TickingTest;
using Zaber;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	[SetUICulture("en-US")]
	public class ZaberPortFacadeTest
	{
		[SetUp]
		public void SetUp()
		{
			_mockPort = new MockPort();

			_portFacade = CreatePortFacade(_mockPort);

			_invalidatedCount = 0;
			_portFacade.Invalidated += delegate { _invalidatedCount++; };
		}


		[Test]
		public void AddDeviceType()
		{
			// SETUP
			var type123 = new DeviceType
			{
				DeviceId = 123,
				Commands = new List<CommandInfo>()
			};
			var keyValuePair =
				new KeyValuePair<Tuple<int, FirmwareVersion>, DeviceType>(
					new Tuple<int, FirmwareVersion>(type123.DeviceId, type123.FirmwareVersion),
					type123);

			// EXEC
			_portFacade.AddDeviceType(type123);

			// VERIFY
			Assert.That(_portFacade.DeviceTypeMap.Contains(keyValuePair));
		}


		[Test]
		public void AliasBinaryMode()
		{
			// EXPECT
			const string portName = "COM1";
			SetBinaryExpectations(_portFacade, _mockPort, 1, 27, 100, 0);

			var expectedCommands = new[] { Command.ReturnDeviceID, Command.Home };
			var type27 = _portFacade.GetDeviceType(27, 500);
			type27.Commands = expectedCommands.Select(cmd =>
														  new CommandInfo { Command = cmd })
										   .ToList();

			// EXEC
			_portFacade.Open(portName);

			// VERIFY
			Assert.AreEqual(3,
							_portFacade.Devices.Count,
							"Number of devices should match (includes an alias)");
			var alias = _portFacade.GetDeviceCollection(100);
			var allDevices = _portFacade.GetDeviceCollection(0);
			var aliasConversation = _portFacade.GetConversationCollection(100);
			var allConversations = _portFacade.GetConversationCollection(0);
			Assert.That(alias.DeviceType.Commands.Select(cmd => cmd.Command).ToArray(),
						Is.EquivalentTo(expectedCommands),
						"Alias device type should get member commands");
			Assert.AreEqual(100,
							alias.DeviceNumber,
							"Alias device number should match");
			Assert.AreEqual("Alias (1)",
							alias.DeviceType.Name,
							"Alias device type name should match");
			Assert.IsTrue(alias.Contains(_portFacade.GetDevice(1)),
						  "Alias should contain member device");
			Assert.IsTrue(allDevices.Contains(alias),
						  "'All devices' collection should contain alias device");
			Assert.IsTrue(allConversations.Contains(aliasConversation),
						  "'All devices' conversation collection should contain alias conversation");
			Assert.AreSame(_mockPort,
						   alias.Port,
						   "Port should match");
			_mockPort.Verify();
		}


		[Test]
		public void AliasChanged()
		{
			// EXPECT
			const string portName = "COM1";
			SetBinaryExpectations(_portFacade, _mockPort, 1, 42, 100, 0);

			// from set alias request
			_mockPort.Expect(1, Command.SetAliasNumber, 99);
			_mockPort.AddResponse(1, Command.SetAliasNumber, 99);

			// EXEC
			_portFacade.Open(portName);
			_portFacade.GetDevice(1).Send(Command.SetAliasNumber, 99);

			// VERIFY
			Assert.AreEqual(1,
							_invalidatedCount,
							"number of Invalidated events");
			_mockPort.Verify();
		}


		[Test]
		public void AliasQuery()
		{
			// EXPECT
			const byte messageId = 13;
			const int aliasNumber = 100;
			const string portName = "COM1";
			SetBinaryExpectations(_portFacade,
								  _mockPort,
								  1,
								  42,
								  aliasNumber,
								  (int) DeviceModes.EnableMessageIdsMode);

			_mockPort.Expect(1, Command.ReturnSetting, (int) Command.SetAliasNumber | (messageId << 24));
			_mockPort.AddResponse(1, Command.SetAliasNumber, aliasNumber | (messageId << 24));

			// EXEC
			_portFacade.Open(portName);

			_portFacade.GetDevice(1).Send(Command.ReturnSetting, (int) Command.SetAliasNumber, messageId);

			// VERIFY
			Assert.AreEqual(3,
							_portFacade.Devices.Count,
							"Number of devices should match (includes an alias)");
			var alias = (DeviceCollection) _portFacade.GetDevice(aliasNumber);
			Assert.IsTrue(alias.Contains(_portFacade.GetDevice(1)),
						  "Alias should contain member device");
			Assert.IsTrue(alias.AreMessageIdsEnabled,
						  "Message ids should be enabled.");
			Assert.AreEqual(0,
							_invalidatedCount,
							"number of Invalidated events");
			_mockPort.Verify();
		}


		[Test]
		public void AliasWithTwoMembers()
		{
			// EXPECT
			const string portName = "COM1";
			SetBinaryExpectations(_portFacade,
								  _mockPort,
								  1,
								  42,
								  100,
								  0,
								  2,
								  99,
								  100,
								  0);

			// EXEC
			_portFacade.Open(portName);

			// VERIFY
			Assert.AreEqual(4,
							_portFacade.Devices.Count,
							"Number of devices should match (includes an alias)");
			var alias = (DeviceCollection) _portFacade.GetDevice(100);
			Assert.IsTrue(alias.Contains(_portFacade.GetDevice(1)),
						  "Alias should contain member device 1");
			Assert.IsTrue(alias.Contains(_portFacade.GetDevice(2)),
						  "Alias should contain member device 2");
			Assert.AreEqual("Alias (1, 2)",
							alias.DeviceType.Name,
							"Alias device type name should match");
			_mockPort.Verify();
		}


		/// <summary>
		///     Make sure that the step size gets copied from the original device
		///     type so that it can calculate data for units of measure.
		/// </summary>
		[Test]
		public void CalculateDataAscii()
		{
			// SETUP
			var expectedDeviceType = _portFacade.GetDeviceType(27, 500);
			DeviceTypeMother.AddCommands(expectedDeviceType);
			expectedDeviceType.MotionType = MotionType.Linear;
			var position = new Measurement(1, UnitOfMeasure.Millimeter);
			const int expectedData = 1000;

			SetAsciiExpectations(_portFacade, _mockPort, expectedDeviceType);

			// EXEC
			_portFacade.Port.IsAsciiMode = true;
			_portFacade.Open("COM1");

			var device = _portFacade.GetDevice(1);
			var actualData = device.CalculateExactData("pos", position);

			// VERIFY
			Assert.That(actualData,
						Is.EqualTo(expectedData),
						"data");
			_mockPort.Verify();
		}


		[Test]
		public void Close()
		{
			// SETUP
			const string portName = "COM1";
			SetBinaryExpectations(_portFacade, _mockPort, 1, 42, 100, 0);


			// EXEC
			_portFacade.Open(portName);

			_portFacade.Close();
			var countAfterClose = _portFacade.Devices.Count;
			var conversationCountAfterClose = _portFacade.Conversations.Count;

			// VERIFY
			Assert.AreEqual(1,
							countAfterClose,
							"Number of devices after Close() should match");
			Assert.AreEqual(1,
							conversationCountAfterClose,
							"Number of conversations after Close() should match");
			Assert.IsFalse(_mockPort.IsOpen,
						   "Port should not be open.");
			_mockPort.Verify();
		}


		[Test]
		public void ControllerGetsCommandsFromPeripheralsWithoutUoms()
		{
			// SETUP
			var peripheral1 = new DeviceType
			{
				PeripheralId = 1,
				Commands = new List<CommandInfo>
				{
					new CommandInfo
					{
						TextCommand = "move abs",
						IsAxisCommand = true,
						RequestUnit = UnitOfMeasure.Meter,
						RequestUnitFunction = ScalingFunction.LinearResolution,
						RequestUnitScale = 15.625m
					}
				}
			};
			var peripheral2 = new DeviceType
			{
				PeripheralId = 2,
				Commands = new List<CommandInfo>
				{
					new CommandInfo
					{
						TextCommand = "move rel",
						IsAxisCommand = true,
						RequestUnit = UnitOfMeasure.MetersPerSecond,
						RequestUnitFunction = ScalingFunction.LinearResolution,
						RequestUnitScale = 15.9m,
						ResponseUnitFunction = ScalingFunction.LinearResolution,
						ResponseUnit = UnitOfMeasure.MetersPerSecond,
						ResponseUnitScale = 1543m
					}
				}
			};
			var controller = new DeviceType
			{
				DeviceId = 30112,
				Name = "Controller",
				PeripheralMap = new Dictionary<int, DeviceType>
				{
					{ 1, peripheral1 },
					{ 2, peripheral2 }
				}
			};

			// EXPECT
			_portFacade.Port.IsAsciiMode = true;
			_portFacade.DeviceTypeMap.Add(
				new KeyValuePair<Tuple<int, FirmwareVersion>, DeviceType>(
					new Tuple<int, FirmwareVersion>(30112, new FirmwareVersion(0)),
					controller));
			SetAsciiExpectations(_portFacade, _mockPort, controller);

			// EXEC
			_portFacade.Open("COM1");

			// VERIFY
			var commands = _portFacade.GetDevice(1).DeviceType.Commands;
			foreach (var command in commands)
			{
				Assert.That(command.RequestUnit, Is.Null);
				Assert.That(command.RequestUnitScale, Is.Null);
				Assert.That(command.ResponseUnit, Is.Null);
				Assert.That(command.ResponseUnitScale, Is.Null);
			}
		}


		[Test]
		public void ControllerWithPeripheralsHasPeripheralsCommands()
		{
			// SETUP
			var peripheral1 = new DeviceType
			{
				PeripheralId = 1,
				Commands = new List<CommandInfo>
				{
					new CommandInfo
					{
						TextCommand = "home",
						IsAxisCommand = true
					}
				}
			};
			var peripheral2 = new DeviceType
			{
				PeripheralId = 2,
				Commands = new List<CommandInfo>
				{
					new CommandInfo
					{
						TextCommand = "echo",
						IsAxisCommand = true
					}
				}
			};
			var controller = new DeviceType
			{
				DeviceId = 30111,
				Name = "Controller",
				PeripheralMap = new Dictionary<int, DeviceType>
				{
					{ 1, peripheral1 },
					{ 2, peripheral2 }
				}
			};

			// EXPECT
			_portFacade.Port.IsAsciiMode = true;
			_portFacade.DeviceTypeMap.Add(
				new KeyValuePair<Tuple<int, FirmwareVersion>, DeviceType>(
					new Tuple<int, FirmwareVersion>(30111, new FirmwareVersion(0)),
					controller));
			SetAsciiExpectations(_portFacade, _mockPort, controller);

			// EXEC
			_portFacade.Open("COM1");

			// VERIFY
			Assert.That(_portFacade.GetDevice(1).DeviceType.Commands.Count,
						Is.EqualTo(2));
		}


		[Test]
		public void Conversations()
		{
			// EXPECT
			const string portName = "COM1";
			SetBinaryExpectations(_portFacade,
								  _mockPort,
								  1,
								  42,
								  0,
								  0,
								  3,
								  99,
								  0,
								  0); // skip device number 2

			// EXEC
			_portFacade.Open(portName);

			// now loop through list
			var deviceTypes = new StringBuilder();
			foreach (var conversation in _portFacade.Conversations)
			{
				deviceTypes.AppendFormat("{0}, ",
										 conversation.Device.DeviceType);
			}

			var allDevices =
				_portFacade.GetConversationCollection(0);
			var conversation1 = _portFacade.GetConversation(1);
			var contains1 = _portFacade.ContainsConversation(1);
			var contains2 = _portFacade.ContainsConversation(2);
			var allDevicesContains1 = allDevices.Contains(conversation1);

			var countAfterOpen = _portFacade.Conversations.Count;
			_portFacade.Conversations.Clear();
			var countAfterClear = _portFacade.Conversations.Count;

			// VERIFY
			Assert.AreEqual(
				"All Devices, Zaber DeviceId 42, Firmware Version 5.00, Zaber DeviceId 99, Firmware Version 5.00, ",
				deviceTypes.ToString(),
				"List of device types from foreach should match");
			Assert.AreEqual(1,
							conversation1.Device.DeviceNumber,
							"device numbers should match");
			Assert.IsTrue(contains1,
						  "ContainsConversation(1)");
			Assert.IsFalse(contains2,
						   "ContainsConversation(2)");
			Assert.IsTrue(allDevicesContains1,
						  "All devices conversation contains conversation 1");
			Assert.AreEqual(countAfterOpen,
							countAfterClear,
							"PortFacade.Conversations should return a copy, so removing items does nothing.");
			_mockPort.Verify();
		}


		/// <summary>
		///     The two devices have different settings for message ids, so the second
		///     one is adjusted to match the first one.
		/// </summary>
		[Test]
		public void DetectMessageIdsConflicted()
		{
			// EXPECT
			const string portName = "COM1";

			_mockPort.Expect(0, Command.ReturnDeviceID, 0);
			_mockPort.AddResponse(1, Command.ReturnDeviceID, 42);
			_mockPort.AddResponse(2, Command.ReturnDeviceID, 99);

			_mockPort.Expect(1, Command.ReturnFirmwareVersion, 0);
			_mockPort.AddResponse(1, Command.ReturnFirmwareVersion, 567);
			_mockPort.Expect(2, Command.ReturnFirmwareVersion, 0);
			_mockPort.AddResponse(2, Command.ReturnFirmwareVersion, 567);

			_mockPort.Expect(1, Command.ReturnSetting, (int) Command.SetAliasNumber);
			_mockPort.AddResponse(1, Command.SetAliasNumber, 0);
			_mockPort.Expect(2, Command.ReturnSetting, (int) Command.SetAliasNumber);
			_mockPort.AddResponse(2, Command.SetAliasNumber, 0);

			_mockPort.Expect(1, Command.ReturnSetting, (int) Command.SetMicrostepResolution);
			_mockPort.AddResponse(1, Command.SetMicrostepResolution, 64);
			_mockPort.Expect(2, Command.ReturnSetting, (int) Command.SetMicrostepResolution);
			_mockPort.AddResponse(2, Command.SetMicrostepResolution, 64);

			_mockPort.Expect(1, Command.ReturnSetting, (int) Command.SetDeviceMode);
			_mockPort.AddResponse(1, Command.SetDeviceMode, (int) DeviceModes.DisablePowerLed);
			_mockPort.Expect(2, Command.ReturnSetting, (int) Command.SetDeviceMode);
			_mockPort.AddResponse(2, Command.SetDeviceMode, (int) DeviceModes.EnableMessageIdsMode);
			_mockPort.Expect(2, Command.SetDeviceMode, (int) DeviceModes.None);
			_mockPort.AddResponse(2, Command.SetDeviceMode, (int) DeviceModes.None);

			// EXEC
			_portFacade.Open(portName);

			var areEnabledOnFacade = _portFacade.AreMessageIdsEnabled;
			var areEnabledOnDevice1 = _portFacade.GetDevice(1).AreMessageIdsEnabled;
			var areEnabledOnDevice2 = _portFacade.GetDevice(2).AreMessageIdsEnabled;

			// VERIFY
			Assert.IsFalse(areEnabledOnFacade,
						   "Message ids should be disabled on port facade.");
			Assert.IsFalse(areEnabledOnDevice1,
						   "Message ids should be disabled on 1st device object.");
			Assert.IsFalse(areEnabledOnDevice2,
						   "Message ids should be disabled on 2nd device object.");
			_mockPort.Verify();
		}


		[Test]
		public void DetectMessageIdsDisabled()
		{
			// EXPECT
			const string portName = "COM1";
			SetBinaryExpectations(_portFacade,
								  _mockPort,
								  1,
								  42,
								  0,
								  (int) DeviceModes.DisablePowerLed,
								  2,
								  99,
								  0,
								  (int) DeviceModes.None);

			// EXEC
			_portFacade.Open(portName);

			var areEnabledOnFacade = _portFacade.AreMessageIdsEnabled;
			var areEnabledOnDevice1 = _portFacade.GetDevice(1).AreMessageIdsEnabled;

			// VERIFY
			Assert.IsFalse(areEnabledOnFacade,
						   "Message ids should be enabled on port facade.");
			Assert.IsFalse(areEnabledOnDevice1,
						   "Message ids should be enabled on device object.");
			_mockPort.Verify();
		}


		[Test]
		public void DetectMessageIdsEnabled()
		{
			// EXPECT
			const string portName = "COM1";
			SetBinaryExpectations(_portFacade,
								  _mockPort,
								  1,
								  42,
								  0,
								  (int) (DeviceModes.DisablePowerLed | DeviceModes.EnableMessageIdsMode),
								  2,
								  99,
								  0,
								  (int) DeviceModes.EnableMessageIdsMode);

			// EXEC
			_portFacade.Open(portName);

			var areEnabledOnFacade = _portFacade.AreMessageIdsEnabled;
			var areEnabledOnDevice1 = _portFacade.GetDevice(1).AreMessageIdsEnabled;

			// VERIFY
			Assert.IsTrue(areEnabledOnFacade,
						  "Message ids should be enabled on port facade.");
			Assert.IsTrue(areEnabledOnDevice1,
						  "Message ids should be enabled on device object.");
			_mockPort.Verify();
		}


		[Test]
		public void Devices()
		{
			// EXPECT
			const string portName = "COM1";
			SetBinaryExpectations(_portFacade,
								  _mockPort,
								  1,
								  42,
								  0,
								  0,
								  3,
								  99,
								  0,
								  0); // skip device number 2

			// EXEC
			_portFacade.Open(portName);

			// now loop through list
			var deviceTypes = new StringBuilder();
			foreach (var device in _portFacade.Devices)
			{
				if (device == null)
				{
					deviceTypes.Append("null, ");
				}
				else
				{
					deviceTypes.AppendFormat("{0}, ",
											 device.DeviceType);
				}
			}

			var countAfterOpen = _portFacade.Devices.Count;
			var contains1 = _portFacade.ContainsDevice(1);
			var contains2 = _portFacade.ContainsDevice(2);
			_portFacade.Devices.Clear();
			var countAfterClear = _portFacade.Devices.Count;

			var device0 = _portFacade.GetDevice(0);

			// VERIFY
			Assert.AreEqual(
				"All Devices, Zaber DeviceId 42, Firmware Version 5.00, Zaber DeviceId 99, Firmware Version 5.00, ",
				deviceTypes.ToString(),
				"List of device types from foreach should match");
			Assert.AreEqual(0,
							device0.DeviceNumber,
							"device numbers should match");
			Assert.AreEqual(countAfterOpen,
							countAfterClear,
							"PortFacade.Devices should return a copy, so removing items does nothing.");
			Assert.That(contains1,
						Is.True,
						"contains device 1");
			Assert.That(contains2,
						Is.False,
						"contains device 2");
			_mockPort.Verify();
		}


		[Test]
		public void DisableMessageIds()
		{
			// EXPECT
			const string portName = "COM1";

			SetBinaryExpectations(_portFacade,
								  _mockPort,
								  1,
								  42,
								  0,
								  (int) (DeviceModes.DisablePowerLed | DeviceModes.EnableMessageIdsMode),
								  2,
								  99,
								  0,
								  (int) DeviceModes.EnableMessageIdsMode);

			// Disable message ids
			_mockPort.Expect(1, Command.ReturnSetting, (int) Command.SetDeviceMode);
			_mockPort.AddResponse(1,
								  Command.SetDeviceMode,
								  (int) (DeviceModes.DisablePowerLed | DeviceModes.EnableMessageIdsMode));

			_mockPort.Expect(1,
							 Command.SetDeviceMode,
							 (int) DeviceModes.DisablePowerLed);
			_mockPort.AddResponse(1,
								  Command.SetDeviceMode,
								  (int) DeviceModes.DisablePowerLed);

			_mockPort.Expect(2, Command.ReturnSetting, (int) Command.SetDeviceMode);
			_mockPort.AddResponse(2,
								  Command.SetDeviceMode,
								  (int) DeviceModes.EnableMessageIdsMode);

			_mockPort.Expect(2,
							 Command.SetDeviceMode,
							 (int) DeviceModes.None);
			_mockPort.AddResponse(2,
								  Command.SetDeviceMode,
								  (int) DeviceModes.None);

			// EXEC
			_portFacade.Open(portName);

			var areMessageIdsEnabledAfterOpen = _portFacade.AreMessageIdsEnabled;

			// now turn off message ids
			_portFacade.AreMessageIdsEnabled = false;

			// VERIFY
			Assert.IsTrue(areMessageIdsEnabledAfterOpen,
						  "Message ids should be enabled after Open()");
			_mockPort.Verify();
		}


		[Test]
		public void EnableMessageIds()
		{
			// EXPECT
			const string portName = "COM1";
			SetBinaryExpectations(_portFacade,
								  _mockPort,
								  1,
								  42,
								  0,
								  (int) DeviceModes.DisablePowerLed,
								  2,
								  99,
								  0,
								  (int) DeviceModes.None);

			_mockPort.Expect(1, Command.ReturnSetting, (int) Command.SetDeviceMode);
			_mockPort.AddResponse(1, Command.SetDeviceMode, (int) DeviceModes.DisablePowerLed);

			_mockPort.Expect(1,
							 Command.SetDeviceMode,
							 (int) (DeviceModes.EnableMessageIdsMode | DeviceModes.DisablePowerLed));
			_mockPort.AddResponse(1,
								  Command.SetDeviceMode,
								  (int) (DeviceModes.EnableMessageIdsMode | DeviceModes.DisablePowerLed));

			_mockPort.Expect(2, Command.ReturnSetting, (int) Command.SetDeviceMode);
			_mockPort.AddResponse(2, Command.SetDeviceMode, (int) DeviceModes.None);

			_mockPort.Expect(2,
							 Command.SetDeviceMode,
							 (int) DeviceModes.EnableMessageIdsMode);
			_mockPort.AddResponse(2,
								  Command.SetDeviceMode,
								  (int) DeviceModes.EnableMessageIdsMode);

			// EXEC
			_portFacade.Open(portName);

			var areMessageIdsEnabledAfterOpen = _portFacade.AreMessageIdsEnabled;

			// now turn on message ids
			_portFacade.AreMessageIdsEnabled = true;

			var areEnabledOnDevice1 = _portFacade.GetDevice(1).AreMessageIdsEnabled;

			// VERIFY
			Assert.IsFalse(areMessageIdsEnabledAfterOpen,
						   "Message ids should not be enabled after Open()");
			Assert.IsTrue(areEnabledOnDevice1,
						  "Message ids should be enabled on device object.");
			_mockPort.Verify();
		}


		[Test]
		public void EnableMessageIdsWithError()
		{
			// EXPECT
			const string portName = "COM1";
			SetBinaryExpectations(_portFacade,
								  _mockPort,
								  1,
								  42,
								  0,
								  (int) DeviceModes.DisablePowerLed,
								  2,
								  99,
								  0,
								  (int) DeviceModes.None);

			_mockPort.Expect(1, Command.ReturnSetting, (int) Command.SetDeviceMode);
			_mockPort.AddResponse(1, Command.SetDeviceMode, (int) DeviceModes.DisablePowerLed);

			_mockPort.Expect(1,
							 Command.SetDeviceMode,
							 (int) (DeviceModes.EnableMessageIdsMode | DeviceModes.DisablePowerLed));
			_mockPort.AddResponse(1,
								  Command.SetDeviceMode,
								  (int) (DeviceModes.EnableMessageIdsMode | DeviceModes.DisablePowerLed));

			_mockPort.Expect(2, Command.ReturnSetting, (int) Command.SetDeviceMode);
			_mockPort.AddResponse(2, Command.Error, (int) ZaberError.Busy);

			// EXEC
			_portFacade.Open(portName);

			string msg = null;
			try
			{
				// now turn on message ids
				_portFacade.AreMessageIdsEnabled = true;
				Assert.Fail("should have thrown.");
			}
			catch (ErrorResponseException ex)
			{
				msg = ex.Message;
			}

			var areEnabledOnDevice1 = _portFacade.GetDevice(1).AreMessageIdsEnabled;

			// VERIFY
			Assert.That(_portFacade.AreMessageIdsEnabled,
						Is.False,
						"Message ids on facade after error");
			Assert.That(areEnabledOnDevice1,
						Is.False,
						"Message ids on device object after error.");
			Assert.That(msg,
						Is.EqualTo("Error response Busy received from device number 2."));
			_mockPort.Verify();
		}


		[Test]
		public void EnableMessageIdsWithTimeout()
		{
			// SETUP

			// EXPECT
			const string portName = "COM1";
			SetBinaryExpectations(_portFacade,
								  _mockPort,
								  1,
								  42,
								  0,
								  (int) DeviceModes.DisablePowerLed,
								  2,
								  99,
								  0,
								  (int) DeviceModes.None);

			_mockPort.Expect(1, Command.ReturnSetting, (int) Command.SetDeviceMode);

			// EXEC
			_portFacade.Open(portName);

			var threads = new EnableMessageIdsWithTimeoutThreads(_portFacade);
			TestFramework.RunOnce(threads);

			// VERIFY
			_mockPort.Verify();
		}


		[Test]
		public void Events()
		{
			// SETUP
			_portFacade.QueryTimeout = 10;
			const string portName = "COM1";

			var closingCount = 0;
			var closedCount = 0;
			var openedCount = 0;
			_portFacade.Closing += delegate { closingCount++; };
			_portFacade.Closed += delegate { closedCount++; };
			_portFacade.Opened += delegate { openedCount++; };

			SetBinaryExpectations(_portFacade, _mockPort,	1, 42, 0, 0, 2, 99, 0, 0);

			_mockPort.Expect(1, Command.Renumber, 21);
			_mockPort.AddResponse(1, Command.Renumber, 21);

			// after renumber, we should get a second set of queries
			// Note that the old device number 2 has been removed.
			SetBinaryExpectations(_portFacade,_mockPort, 21, 42, 0, 0);

			_portFacade.Open(portName);

			var closingCountAfterOpen = closedCount;
			var closedCountAfterOpen = closedCount;
			var openedCountAfterOpen = openedCount;
			var invalidatedCountAfterOpen = _invalidatedCount;
			var deviceCountAfterOpen = _portFacade.Devices.Count;

			Assert.AreEqual(0, closedCountAfterOpen, "Closed count after Open()");
			Assert.AreEqual(0, closingCountAfterOpen, "Closing count after Open()");
			Assert.AreEqual(1, openedCountAfterOpen, "Opened count after Open()");
			Assert.AreEqual(0, invalidatedCountAfterOpen, "Invalidated count after Open()");
			Assert.AreEqual(3, deviceCountAfterOpen, "Number of devices after Open() should match");

			_portFacade.GetDevice(1).Send(Command.Renumber, 21);

			var closingCountAfterRenumber = closingCount;
			var closedCountAfterRenumber = closedCount;
			var openedCountAfterRenumber = openedCount;
			var invalidatedCountAfterRenumber = _invalidatedCount;

			Assert.AreEqual(0, closedCountAfterRenumber, "Closed count after Renumber");
			Assert.AreEqual(0, closingCountAfterRenumber, "Closing count after Renumber");
			Assert.AreEqual(1, openedCountAfterRenumber, "Opened count after Renumber");
			Assert.AreEqual(1, invalidatedCountAfterRenumber, "Invalidated count after Renumber");

			_portFacade.Close();

			var closingCountAfterClose = closingCount;
			var closedCountAfterClose = closedCount;
			var openedCountAfterClose = openedCount;

			Assert.AreEqual(1, closedCountAfterClose, "Closed count after Close");
			Assert.AreEqual(1, closingCountAfterClose, "Closing count after Close");
			Assert.AreEqual(1, openedCountAfterClose, "Opened count after Close");

			_portFacade.Open(portName);

			var closingCountAfterReopen = closingCount;
			var closedCountAfterReopen = closedCount;
			var openedCountAfterReopen = openedCount;
			var invalidatedCountAfterReopen = _invalidatedCount;
			var deviceCountAfterReopen = _portFacade.Devices.Count;

			Assert.AreEqual(1, closedCountAfterReopen, "Closed count after reopen");
			Assert.AreEqual(1, closingCountAfterReopen, "Closing count after reopen");
			Assert.AreEqual(2, openedCountAfterReopen, "Opened count after reopen");
			Assert.AreEqual(1, invalidatedCountAfterReopen, "Invalidated count after reopen");
			Assert.AreEqual(2, deviceCountAfterReopen, "Number of devices after reopen should match");

			_mockPort.Verify();
		}


		[Test]
		public void GetConversationByAxisNumberMultiAxis()
		{
			// SETUP
			_portFacade.Port.IsAsciiMode = true;
			_mockPort.Expect("/get deviceid");
			_mockPort.AddResponse("@01 0 OK IDLE -- 98");
			_mockPort.Expect("/01 0 get version");
			_mockPort.AddResponse("@01 0 OK IDLE -- 0");
			_mockPort.Expect("/01 0 get system.axiscount");
			_mockPort.AddResponse("@01 0 OK IDLE -- 2");
			_mockPort.Expect("/01 0 get peripheralid");
			_mockPort.AddResponse("@01 1 OK IDLE -- 1 2");
			_mockPort.Expect("/01 0 get resolution");
			_mockPort.AddResponse("@01 0 OK IDLE -- 64");

			// EXEC
			_portFacade.Open("COM1");

			var conversation = _portFacade.GetConversation(1);
			var conversationAxis1 = conversation.Axes[0];
			var conversationGetAxis = _portFacade.GetConversation(1, 1);

			// VERIFY
			Assert.That(conversationGetAxis,
						Is.SameAs(conversationAxis1),
						"axis conversation from GetConversation()");
		}


		[Test]
		public void GetConversationByUnknownAxisNumberMultiAxis()
		{
			// SETUP
			const string expectedMessage1 =
				"Axis number 8 not found on device number 1.";
			const string expectedMessage2 =
				"Axis number -1 not found on device number 1.";
			_portFacade.Port.IsAsciiMode = true;
			_mockPort.Expect("/get deviceid");
			_mockPort.AddResponse("@01 0 OK IDLE -- 98");
			_mockPort.Expect("/01 0 get version");
			_mockPort.AddResponse("@01 0 OK IDLE -- 0");
			_mockPort.Expect("/01 0 get system.axiscount");
			_mockPort.AddResponse("@01 0 OK IDLE -- 2");
			_mockPort.Expect("/01 0 get peripheralid");
			_mockPort.AddResponse("@01 1 OK IDLE -- 1 2");
			_mockPort.Expect("/01 0 get resolution");
			_mockPort.AddResponse("@01 0 OK IDLE -- 64");

			// EXEC
			_portFacade.Open("COM1");

			var ex1 = Assert.Throws(typeof(KeyNotFoundException),
									delegate { _portFacade.GetConversation(1, 8); });
			var ex2 = Assert.Throws(typeof(KeyNotFoundException),
									delegate { _portFacade.GetConversation(1, -1); });

			// VERIFY
			Assert.That(ex1.Message,
						Is.EqualTo(expectedMessage1),
						"error message");
			Assert.That(ex2.Message,
						Is.EqualTo(expectedMessage2),
						"error message");
		}


		[Test]
		public void GetConversationByUnknownDeviceNumber()
		{
			// SETUP
			const string expectedMessage = "Device number 17 not found.";
			_portFacade.Port.IsAsciiMode = true;
			SetAsciiExpectations(_portFacade, _mockPort);

			// EXEC
			_portFacade.Open("COM1");

			var ex = Assert.Throws(typeof(KeyNotFoundException),
								   delegate { _portFacade.GetConversation(17); });

			// VERIFY
			Assert.That(ex.Message,
						Is.EqualTo(expectedMessage),
						"error message");
		}


		[Test]
		public void GetDeviceByAxisNumberMultiAxis()
		{
			// SETUP
			_portFacade.Port.IsAsciiMode = true;
			_mockPort.Expect("/get deviceid");
			_mockPort.AddResponse("@01 0 OK IDLE -- 98");
			_mockPort.Expect("/01 0 get version");
			_mockPort.AddResponse("@01 0 OK IDLE -- 0");
			_mockPort.Expect("/01 0 get system.axiscount");
			_mockPort.AddResponse("@01 0 OK IDLE -- 2");
			_mockPort.Expect("/01 0 get peripheralid");
			_mockPort.AddResponse("@01 1 OK IDLE -- 1 2");
			_mockPort.Expect("/01 0 get resolution");
			_mockPort.AddResponse("@01 0 OK IDLE -- 64");

			// EXEC
			_portFacade.Open("COM1");

			var device = _portFacade.GetDevice(1);
			var deviceAxis1 = device.Axes[0];
			var deviceGetAxis = _portFacade.GetDevice(1, 1);

			// VERIFY
			Assert.That(deviceGetAxis,
						Is.SameAs(deviceAxis1),
						"axis device from GetDevice()");
		}


		[Test]
		public void IgnoreOtherCommands()
		{
			// EXPECT
			const string portName = "COM1";

			_mockPort.Expect(0, Command.ReturnDeviceID, 0);

			// unrelated response should be ignored
			_mockPort.AddResponse(1, Command.MoveAtConstantSpeed, 99);

			// now raise the response to the ReturnDeviceId command
			_mockPort.AddResponse(1, Command.ReturnDeviceID, 42);

			_mockPort.Expect(1, Command.ReturnFirmwareVersion, 0);
			_mockPort.AddResponse(1, Command.ReturnFirmwareVersion, 0);

			_mockPort.Expect(1, Command.ReturnSetting, (int) Command.SetDeviceMode);
			_mockPort.AddResponse(1, Command.SetDeviceMode, 0);

			// EXEC
			_portFacade.Open(portName);

			// VERIFY
			Assert.AreEqual(42,
							_portFacade.GetDevice(1).DeviceType.DeviceId,
							"Device type should match");
			_mockPort.Verify();
		}


		[Test]
		public void IgnoreSecondResponse()
		{
			// EXPECT
			const string portName = "COM1";
			_mockPort.Expect(0, Command.ReturnDeviceID, 0);
			_mockPort.AddResponse(1, Command.ReturnDeviceID, 42);
			_mockPort.AddResponse(1, Command.ReturnDeviceID, 99);

			// second response for same device number should be ignored.

			_mockPort.Expect(1, Command.ReturnFirmwareVersion, 0);
			_mockPort.AddResponse(1, Command.ReturnFirmwareVersion, 0);

			_mockPort.Expect(1, Command.ReturnSetting, (int) Command.SetDeviceMode);
			_mockPort.AddResponse(1, Command.SetDeviceMode, (int) DeviceModes.DisablePowerLed);

			// EXEC
			_portFacade.Open(portName);
			var isCollisionDetected = _portFacade.IsCollisionDetected;

			// VERIFY
			Assert.AreEqual(42,
							_portFacade.GetDevice(1).DeviceType.DeviceId,
							"Device type should match");
			Assert.That(isCollisionDetected,
						Is.True,
						"collision detected");
			_mockPort.Verify();
		}


		[Test]
		[TestCaseSource(nameof(_invalidateTestCases))]
		public void Invalidate(int deviceNumber,
							   Command command,
							   int data,
							   bool isInvalidateExpected)
		{
			// SETUP
			var isInvalidated = false;
			_portFacade.Invalidated +=
				delegate { isInvalidated = true; };

			const string expectedPortName = "COM1";

			// send responses out of order
			SetBinaryExpectations(_portFacade,
								  _mockPort,
								  2,
								  99,
								  0,
								  0,
								  1,
								  42,
								  200,
								  0);

			// EXEC
			_portFacade.Open(expectedPortName);

			_mockPort.FireResponse((byte) deviceNumber, command, data);

			// VERIFY
			Assert.That(isInvalidated,
						Is.EqualTo(isInvalidateExpected),
						"is invalidated");
		}


		[Test]
		public void InvalidateOnInvalidationEnabled_ASCII()
		{
			var isInvalidated = false;
			_portFacade.Invalidated +=
				delegate { isInvalidated = true; };

			_portFacade.Port.IsAsciiMode = true;

			var device42 = new DeviceType { DeviceId = 42 };

			SetAsciiExpectations(_portFacade, _mockPort, device42);

			_portFacade.Open("COM1");

			// Invalidations that occur while invalidate is disabled should occur when it is re-enabled.
			_portFacade.IsInvalidateEnabled = false;
			_mockPort.Expect("/01 0 system restore");
			_portFacade.GetConversation(1).Send("system restore");
			Assert.IsFalse(isInvalidated);
			_portFacade.IsInvalidateEnabled = true;
			Assert.IsTrue(isInvalidated);
		}


		[Test]
		public void InvalidateOnInvalidationEnabled_Binary()
		{
			var isInvalidated = false;
			_portFacade.Invalidated +=
				delegate { isInvalidated = true; };

			const string expectedPortName = "COM1";

			// send responses out of order
			SetBinaryExpectations(_portFacade,
								  _mockPort,
								  2,
								  99,
								  0,
								  0,
								  1,
								  42,
								  200,
								  0);

			_portFacade.Open(expectedPortName);

			// Invalidations that occur while invalidate is disabled should occur when it is re-enabled.
			_portFacade.IsInvalidateEnabled = false;
			_mockPort.FireResponse(3, Command.RestoreSettings, 0);
			Assert.IsFalse(isInvalidated);
			_portFacade.IsInvalidateEnabled = true;
			Assert.IsTrue(isInvalidated);
		}


		[Test]
		public void KeepMessageIdsConsistent()
		{
			// EXPECT
			const string portName = "COM1";

			SetBinaryExpectations(_portFacade,
								  _mockPort,
								  1,
								  42,
								  0,
								  (int) DeviceModes.DisablePowerLed,
								  2,
								  99,
								  0,
								  (int) DeviceModes.None);

			// Disable message ids
			_mockPort.Expect(1, Command.SetDeviceMode, (int) DeviceModes.EnableMessageIdsMode);
			_mockPort.AddResponse(1,
								  Command.SetDeviceMode,
								  (int) DeviceModes.EnableMessageIdsMode);

			_mockPort.Expect(1,
							 Command.SetDeviceMode,
							 (int) DeviceModes.None);
			_mockPort.AddResponse(1,
								  Command.SetDeviceMode,
								  (int) DeviceModes.None);

			// EXEC
			_portFacade.Open(portName);

			var response = _portFacade.GetConversation(1)
								   .Request(Command.SetDeviceMode,
											(int) DeviceModes.EnableMessageIdsMode);

			// VERIFY
			Assert.That(response.NumericData,
						Is.EqualTo((int) DeviceModes.None),
						"device mode in response.");
			_mockPort.Verify();
		}


		/// <summary>
		///     Should open the port and ask for device ids from each device.
		/// </summary>
		[Test]
		public void LogEventsDuringOpen()
		{
			// SETUP
			var messageCount = 0;
			var allDevices = _portFacade.GetDevice(0);
			allDevices.MessageReceived +=
				delegate { messageCount++; };

			const string portName = "COM1";
			SetBinaryExpectations(_portFacade,
								  _mockPort,
								  1,
								  42,
								  0,
								  (int) DeviceModes.None,
								  2,
								  99,
								  0,
								  (int) DeviceModes.DisableAutoReply);

			// EXEC
			_portFacade.Open(portName);

			// VERIFY
			Assert.AreEqual(0,
							messageCount,
							"number of messages received");
			_mockPort.Verify();
		}


		[Test]
		public void MultiAxis()
		{
			// SETUP
			var type99 = new DeviceType
			{
				Commands = new List<CommandInfo>
				{
					new CommandInfo
					{
						TextCommand = "version",
						IsAxisCommand = false
					},
					new CommandInfo
					{
						TextCommand = "resolution",
						IsAxisCommand = true
					}
				},
				DeviceId = 99,
				FirmwareVersion = new FirmwareVersion(605),
				Name = "Example Controller"
			};
			var newTypes = new List<DeviceType>
			{
				new DeviceType(type99)
				{
					PeripheralId = 12345,
					FirmwareVersion = new FirmwareVersion(605)
				},
				new DeviceType(type99)
				{
					PeripheralId = 55555,
					FirmwareVersion = new FirmwareVersion(605)
				}
			};
			type99.PeripheralMap = newTypes.ToDictionary(dt => dt.PeripheralId);

			_portFacade.AddDeviceType(type99);

			_portFacade.Port.IsAsciiMode = true;
			SetAsciiExpectations(_portFacade, _mockPort, type99);

			string[] expectedAddresses = { "0 0", "1 0", "1 1", "1 2" };

			// EXEC
			_portFacade.Open("COM1");

			var device = _portFacade.GetDevice(1);
			var deviceAxis1 = device.Axes[0];
			var deviceAxis2 = device.Axes[1];

			var conversation = _portFacade.GetConversation(1);
			var conversationAxis1 = conversation.Axes[0];
			var conversationAxis2 = conversation.Axes[1];

			var versionCommandOnController = device.DeviceType.GetCommandByText("version");
			var versionCommandOnAxis = deviceAxis1.DeviceType.GetCommandByText("version");
			var resolutionCommandOnController = device.DeviceType.GetCommandByText("resolution");
			var resolutionCommandOnAxis = deviceAxis1.DeviceType.GetCommandByText("resolution");

			var deviceName = device.DeviceType.Name;

			var allConversations = _portFacade.AllConversations;
			var actualAddresses = allConversations.Select(c =>
															  $"{c.Device.DeviceNumber} {c.Device.AxisNumber}")
											   .ToArray();

			// VERIFY
			Assert.AreEqual(1, deviceAxis1.AxisNumber);
			Assert.AreEqual(2, deviceAxis2.AxisNumber);
			Assert.AreEqual(64, deviceAxis1.MicrostepResolution);
			Assert.AreEqual(64, deviceAxis2.MicrostepResolution);
			Assert.AreEqual(0, device.MicrostepResolution);
			Assert.AreEqual(12345, deviceAxis1.DeviceType.PeripheralId);
			Assert.AreEqual(55555, deviceAxis2.DeviceType.PeripheralId);
			Assert.AreEqual(0, device.DeviceType.PeripheralId);
			Assert.AreEqual(device.DeviceNumber, deviceAxis1.DeviceNumber);
			Assert.AreSame(device.Port, deviceAxis1.Port);
			Assert.AreEqual(device.FirmwareVersion, deviceAxis1.FirmwareVersion);
			Assert.AreSame(deviceAxis1, conversationAxis1.Device);
			Assert.AreSame(deviceAxis2, conversationAxis2.Device);
			Assert.AreEqual(expectedAddresses, actualAddresses);
			Assert.IsNotNull(versionCommandOnController);
			Assert.IsNotNull(resolutionCommandOnController);
			Assert.IsFalse(versionCommandOnController.IsAxisCommand);
			Assert.IsTrue(resolutionCommandOnController.IsAxisCommand);
			Assert.IsNull(versionCommandOnAxis);
			Assert.IsNotNull(resolutionCommandOnAxis);
			Assert.AreEqual("Example Controller", deviceName);
			Assert.IsTrue(device.IsController);
			Assert.IsFalse(device.IsPeripheral);
			Assert.IsFalse(device.IsIntegrated);
			Assert.IsFalse(deviceAxis1.IsController);
			Assert.IsTrue(deviceAxis1.IsPeripheral);
			Assert.IsFalse(deviceAxis1.IsIntegrated);
			Assert.IsFalse(deviceAxis2.IsController);
			Assert.IsTrue(deviceAxis2.IsPeripheral);
			Assert.IsFalse(deviceAxis2.IsIntegrated);

			_mockPort.Verify();
		}


		/// <summary>
		///     Should open the port and ask for device ids from each device.
		/// </summary>
		[Test]
		public void Open()
		{
			// SETUP
			const string expectedPortName = "COM1";

			// send responses out of order
			SetBinaryExpectations(_portFacade,
								  _mockPort,
								  2,
								  99,
								  0,
								  0,
								  1,
								  42,
								  0,
								  0);

			// EXEC
			_portFacade.Open(expectedPortName);

			var allDevices = (DeviceCollection) _portFacade.GetDevice(0);
			var device1AfterOpen = _portFacade.GetDevice(1);
			var device2AfterOpen = _portFacade.GetDevice(2);
			var countAfterOpen = _portFacade.Devices.Count;
			var allDevicesCountAfterOpen = allDevices.Count;
			var isDevice2InAllDevicesAfterOpen = allDevices.Contains(device2AfterOpen);
			var isCollisionDetected = _portFacade.IsCollisionDetected;

			// VERIFY
			Assert.AreEqual(expectedPortName, _mockPort.PortName);
			Assert.AreEqual(3, countAfterOpen);
			Assert.AreEqual(1, device1AfterOpen.DeviceNumber);
			Assert.AreEqual(99, device2AfterOpen.DeviceType.DeviceId);
			Assert.AreSame(_mockPort, device1AfterOpen.Port);
			Assert.AreEqual(0, allDevices.DeviceNumber);
			Assert.IsTrue(isDevice2InAllDevicesAfterOpen);
			Assert.AreEqual(2, allDevicesCountAfterOpen);
			Assert.IsFalse(isCollisionDetected);
			Assert.IsFalse(device2AfterOpen.IsController);
			Assert.IsFalse(device2AfterOpen.IsPeripheral);
			Assert.IsTrue(device2AfterOpen.IsIntegrated);

			_mockPort.Verify();
		}


		/// <summary>
		///     Should open the port and ask for device ids from each device.
		/// </summary>
		[Test]
		public void OpenAsciiMode()
		{
			// SETUP
			const string expectedPortName = "COM1";
			_portFacade.Port.IsAsciiMode = true;

			//EXPECT
			SetAsciiExpectations(_portFacade,
								 _mockPort,
								 new DeviceType { DeviceId = 42 },
								 new DeviceType { DeviceId = 99 });


			// EXEC
			_portFacade.Open(expectedPortName);

			var allDevices = (DeviceCollection) _portFacade.GetDevice(0);
			var device1AfterOpen = _portFacade.GetDevice(1);
			var device2AfterOpen = _portFacade.GetDevice(2);
			var countAfterOpen = _portFacade.Devices.Count;
			var allDevicesCountAfterOpen = allDevices.Count;
			var isType99InAllDevicesAfterOpen = allDevices.Contains(device2AfterOpen);
			var isCollisionDetected = _portFacade.IsCollisionDetected;

			// VERIFY
			Assert.AreEqual(expectedPortName, _mockPort.PortName);
			Assert.AreEqual(3, countAfterOpen);
			Assert.AreEqual(1, device1AfterOpen.DeviceNumber);
			Assert.AreEqual(99, device2AfterOpen.DeviceType.DeviceId);
			Assert.AreSame(_mockPort, device1AfterOpen.Port);
			Assert.AreEqual(0, allDevices.DeviceNumber);
			Assert.IsTrue(isType99InAllDevicesAfterOpen);
			Assert.AreEqual(2, allDevicesCountAfterOpen);
			Assert.IsFalse(isCollisionDetected);
			Assert.IsFalse(device2AfterOpen.IsController);
			Assert.IsFalse(device2AfterOpen.IsPeripheral);
			Assert.IsTrue(device2AfterOpen.IsIntegrated);

			_mockPort.Verify();
		}


		/// <summary>
		///     Should open the port and ask for device ids from each device.
		/// </summary>
		[Test]
		public void OpenSortsConversationCollections()
		{
			// SETUP
			const string expectedPortName = "COM1";

			// send responses out of order
			SetBinaryExpectations(_portFacade,
								  _mockPort,
								  2,
								  99,
								  100,
								  0,
								  1,
								  42,
								  100,
								  0);

			// EXEC
			_portFacade.Open(expectedPortName);

			var allConversations = _portFacade.GetConversationCollection(0);
			var alias100Conversation = _portFacade.GetConversationCollection(100);

			// VERIFY
			Assert.AreEqual(1,
							allConversations[0].Device.DeviceNumber,
							"first device number in allConversations");
			Assert.AreEqual(2,
							allConversations[1].Device.DeviceNumber,
							"second device number in allConversations");
			Assert.AreEqual(1,
							alias100Conversation[0].Device.DeviceNumber,
							"first device number in alias 100 conversations");
			Assert.AreEqual(2,
							alias100Conversation[1].Device.DeviceNumber,
							"second device number in alias 100 conversations");
			_mockPort.Verify();
		}


		/// <summary>
		///     Doesn't support the microstep resolution setting
		/// </summary>
		[Test]
		public void OpenWithJoystick()
		{
			// SETUP
			const string expectedPortName = "COM1";

			// send responses out of order
			SetBinaryExpectations(_mockPort,
								  new OpenExpectation
								  {
									  DeviceNumber = 1,
									  DeviceId = 42,
									  IsMicrostepResolutionSupported = false
								  },
								  new OpenExpectation
								  {
									  DeviceNumber = 2,
									  DeviceId = 99
								  });

			// EXEC
			_portFacade.Open(expectedPortName);
		}


		/// <summary>
		///     Should detect loopback and not query devices.
		/// </summary>
		[Test]
		public void OpenWithLoopbackAttached()
		{
			// SETUP
			// query device ids (includes response from loopback
			_mockPort.Expect(0, Command.ReturnDeviceID, 0);
			_mockPort.AddResponse(0, Command.ReturnDeviceID, 0);
			_mockPort.AddResponse(1, Command.ReturnDeviceID, 42);

			// EXEC
			string msg = null;
			try
			{
				_portFacade.Open("COMX");

				Assert.Fail("should have thrown.");
			}
			catch (LoopbackException ex)
			{
				msg = ex.Message;
			}

			// VERIFY
			Assert.AreEqual("Loopback connection detected on COMX.",
							msg,
							"message");
		}


		/// <summary>
		///     Should just open the port and not send anything.
		/// </summary>
		[Test]
		public void OpenWithoutQuery()
		{
			// SETUP
			var isOpened = false;
			_portFacade.Opened +=
				delegate { isOpened = true; };
			const string expectedPortName = "COM1";

			// EXEC
			_portFacade.OpenWithoutQuery(expectedPortName);

			var allDevices = (DeviceCollection) _portFacade.GetDevice(0);
			var countAfterOpen = _portFacade.Devices.Count;
			var allDevicesCountAfterOpen = allDevices.Count;

			// VERIFY
			Assert.AreEqual(expectedPortName,
							_mockPort.PortName,
							"port name");
			Assert.AreEqual(1,
							countAfterOpen,
							"Number of devices after Open() should match");
			Assert.AreSame(_mockPort,
						   allDevices.Port,
						   "Port should match");
			Assert.AreEqual(0,
							allDevices.DeviceNumber,
							"device number should match.");
			Assert.AreEqual(0,
							allDevicesCountAfterOpen,
							"Alldevices should contain 2 devices after open");
			Assert.That(isOpened,
						Is.True,
						"is opened");
			_mockPort.Verify();
		}


		[Test]
		public void OpenWithoutQueryOnNullPort()
		{
			// SETUP
			const string expectedPortName = "COM1";
			_portFacade.Port = null;

			// EXEC
			try
			{
				_portFacade.OpenWithoutQuery(expectedPortName);

				Assert.Fail("should have thrown");
			}
			catch (NullReferenceException ex)
			{
				Assert.That(ex.Message,
							Is.EqualTo("Object reference not set to an instance of an object."));
			}
		}


		/// <summary>
		///     Should open the port and ask for device ids from each device.
		/// </summary>
		[Test]
		public void OpenWithPortError() => TestFramework.RunOnce(new OpenWithPortErrorThreads());


		[Test]
		public void Port() => Assert.That(_portFacade.Port,
										  Is.SameAs(_mockPort));


		[Test]
		public void PortNames()
		{
			// ZC only runs on Windows, and the CI runner may not have serial ports at all.
			var portNames = _portFacade.GetPortNames();
			foreach (var name in portNames)
			{
				Assert.IsTrue(Regex.IsMatch(name, "COM\\d+"));
			}
		}


		[Test]
		[Description("Verify that a binary device with uninitialized ID will still be detected.")]
		public void QueryBinarydeviceWithUnitializedId()
		{
			_mockPort.Expect(0, Command.ReturnDeviceID, 0);
			_mockPort.AddResponse(1, Command.ReturnDeviceID, -1);
			_mockPort.Expect(1, Command.ReturnFirmwareVersion, 0);
			_mockPort.AddResponse(1, Command.ReturnFirmwareVersion, 799);
			_mockPort.Expect(1, Command.ReturnSerialNumber, 0);
			_mockPort.AddResponse(1, Command.ReturnSerialNumber, -1);
			_mockPort.Expect(1, Command.ReturnFirmwareBuild, 0);
			_mockPort.AddResponse(1, Command.ReturnFirmwareBuild, 0);
			_mockPort.Expect(1, Command.ReturnSetting, 48);
			_mockPort.AddResponse(1, Command.SetAliasNumber, 0);
			_mockPort.Expect(1, Command.ReturnSetting, 37);
			_mockPort.AddResponse(1, Command.Error, 53);
			_mockPort.Expect(1, Command.ReturnSetting, 66);
			_mockPort.AddResponse(1, Command.Error, 53);
			_mockPort.Expect(1, Command.ReturnSetting, 102);
			_mockPort.AddResponse(1, Command.SetMessageIDMode, 0);

			_portFacade.Open("COM1");
			var device = _portFacade.GetDevice(1);

			Assert.AreEqual(-1, device.DeviceType.DeviceId);
			_mockPort.Verify();
		}


		/// <summary>
		///     Should open the port and ask for device ids and firmware from
		///     each device.
		/// </summary>
		[Test]
		public void QueryFirmwareAscii()
		{
			// SETUP
			var expectedFirmware = new FirmwareVersion(605);
			_portFacade.Port.IsAsciiMode = true;
			var deviceType = new DeviceType
			{
				FirmwareVersion = expectedFirmware,
				DeviceId = 123
			};
			SetAsciiExpectations(_portFacade, _mockPort, deviceType);

			// EXEC
			_portFacade.Open("COM1");

			var device = _portFacade.GetDevice(1);

			// VERIFY
			Assert.That(device.FirmwareVersion,
						Is.EqualTo(expectedFirmware),
						"firmware");
			_mockPort.Verify();
		}


		/// <summary>
		///     Should open the port and ask for device ids and firmware from
		///     each device.
		/// </summary>
		[Test]
		public void QueryFirmwareBinary()
		{
			// SETUP
			var expectedFirmware = new FirmwareVersion(605);

			// send responses out of order
			SetBinaryExpectations(_mockPort,
								  new OpenExpectation
								  {
									  DeviceNumber = 1,
									  DeviceId = 123,
									  FirmwareVersion = expectedFirmware.ToInt()
								  });

			// EXEC
			_portFacade.Open("COM1");

			var device = _portFacade.GetDevice(1);

			// VERIFY
			Assert.That(device.FirmwareVersion,
						Is.EqualTo(expectedFirmware),
						"firmware");
			_mockPort.Verify();
		}


		[Test]
		public void QueryPeripheralAscii()
		{
			// SETUP
			const int expectedPeripheralId = 5432;
			var expectedDeviceType = _portFacade.DeviceTypeMap.Values.FirstOrDefault();

			expectedDeviceType.PeripheralMap = new Dictionary<int, DeviceType>
			{
				{
					expectedPeripheralId, new DeviceType
					{
						DeviceId = expectedDeviceType.DeviceId,
						PeripheralId = expectedPeripheralId,
						Commands = new List<CommandInfo>()
					}
				}
			};

			SetAsciiExpectations(_portFacade, _mockPort, expectedDeviceType);

			// EXEC
			_portFacade.Port.IsAsciiMode = true;
			_portFacade.Open("COM1");

			var device = _portFacade.GetDevice(1, 1);

			// VERIFY
			Assert.That(device.DeviceType.PeripheralId,
						Is.EqualTo(expectedPeripheralId),
						"peripheral id");
			_mockPort.Verify();
		}


		[Test]
		public void QueryPeripheralBinary()
		{
			// SETUP
			const int expectedPeripheralId = 5432;
			var expectedDeviceType = _portFacade.DeviceTypeMap.Values.FirstOrDefault();

			expectedDeviceType.Commands =
				new List<CommandInfo>(expectedDeviceType.Commands)
				{
					new SettingInfo { Command = Command.SetPeripheralID }
				};

			expectedDeviceType.PeripheralMap = new Dictionary<int, DeviceType>
			{
				{
					expectedPeripheralId, new DeviceType
					{
						DeviceId = expectedDeviceType.DeviceId,
						PeripheralId = expectedPeripheralId,
						Commands = new List<CommandInfo>()
					}
				}
			};

			SetBinaryExpectations(_mockPort,
								  new OpenExpectation
								  {
									  DeviceNumber = 1,
									  DeviceId = expectedDeviceType.DeviceId,
									  PeripheralId = expectedPeripheralId,
									  FirmwareVersion = 608
								  });

			// EXEC
			_portFacade.Open("COM1");

			var device = _portFacade.GetDevice(1);

			// VERIFY
			Assert.AreEqual(expectedPeripheralId, device.DeviceType.PeripheralId);
			Assert.IsTrue(device.IsController);
			Assert.IsTrue(device.IsPeripheral);
			Assert.IsFalse(device.IsIntegrated);

			_mockPort.Verify();
		}


		/// <summary>
		///     Should open the port and ask for device ids and resolution from
		///     each device.
		/// </summary>
		[Test]
		public void QueryResolutionAscii()
		{
			// SETUP
			const int expectedResolution = 100;
			_portFacade.Port.IsAsciiMode = true;
			_mockPort.Expect("/get deviceid");
			_mockPort.AddResponse("@01 0 OK IDLE -- 99");
			_mockPort.Expect("/01 0 get version");
			_mockPort.AddResponse("@01 0 OK IDLE -- 0");
			_mockPort.Expect("/01 0 get system.axiscount");
			_mockPort.AddResponse("@01 0 OK IDLE -- 1");
			_mockPort.Expect("/01 0 get peripheralid");
			_mockPort.AddResponse("@01 0 RJ IDLE -- BADCOMMAND");
			_mockPort.Expect("/01 0 get resolution");
			_mockPort.AddResponse("@01 0 OK IDLE -- 100");

			// EXEC
			_portFacade.Open("COM1");

			var device = _portFacade.GetDevice(1, 1);

			// VERIFY
			Assert.That(device.MicrostepResolution,
						Is.EqualTo(expectedResolution),
						"resolution");
			_mockPort.Verify();
		}


		/// <summary>
		///     Should open the port and ask for device ids and resolution from
		///     each device.
		/// </summary>
		[Test]
		public void QueryResolutionBinary()
		{
			// SETUP
			var expectedResolution = 100;

			// send responses out of order
			SetBinaryExpectations(_mockPort,
								  new OpenExpectation
								  {
									  DeviceNumber = 1,
									  DeviceId = 123,
									  FirmwareVersion = 500,
									  MicrostepResolution = expectedResolution
								  });

			// EXEC
			_portFacade.Open("COM1");

			var device = _portFacade.GetDevice(1);

			// VERIFY
			if (device.FirmwareVersion < 500)
			{
				expectedResolution = 64; // FW2.93 doesn not allow setting microstemp resolution and defaults to 64.
			}

			Assert.That(device.MicrostepResolution,
						Is.EqualTo(expectedResolution),
						"resolution");
			_mockPort.Verify();
		}


		[Test]
		public void RaisesClosedUnexpectedly()
		{
			var raised = false;

			// SETUP
			_portFacade.ClosedUnexpectedly += (sender, e) => { raised = true; };

			// EXEC
			_mockPort.FirePortClosedUnexpectedly();

			// VERIFY
			Assert.IsTrue(raised, "Event not raised");
		}


		[Test]
		public void RenumberAscii()
		{
			// SETUP
			const string portName = "COM1";
			_portFacade.Port.IsAsciiMode = true;

			var device42 = new DeviceType { DeviceId = 42 };
			var device99 = new DeviceType { DeviceId = 99 };

			// EXPECT
			SetAsciiExpectations(_portFacade, _mockPort, device42, device99);

			_mockPort.Expect("renumber");

			// we will raise the responses manually.

			// EXEC
			_portFacade.Open(portName);

			_portFacade.Port.Send("renumber");

			var invalidatedCountAfterRenumberRequest = _invalidatedCount;

			_mockPort.FireResponse("@01 0 OK IDLE -- 0");
			_mockPort.FireResponse("@02 0 OK IDLE -- 0");

			var invalidatedCountAfterRenumberResponse = _invalidatedCount;

			_portFacade.Close();

			// Imagine device with ID 99 is in fact the first on the chain.
			// So it becomes device 1.
			// Note this set of expectations has to be set after the port is closed because
			// SetAsciiExpectations uses the content of the device type map, which is cleared by closing.
			SetAsciiExpectations(_portFacade, _mockPort, device99, device42);

			_portFacade.Open(portName);
			var device1AfterReopen = _portFacade.GetDevice(1);
			var countAfterReopen = _portFacade.Devices.Count;
			var conversationCountAfterReopen = _portFacade.Conversations.Count;
			var allDevices = (DeviceCollection) _portFacade.GetDevice(0);
			var allDevicesCountAfterReopen = allDevices.Count;
			var isType99InAllDevicesAfterReopen = allDevices.Contains(device1AfterReopen);
			var allConversations = (ConversationCollection) _portFacade.GetConversation(0);
			var allConversationsCountAfterReopen = allConversations.Count;

			// VERIFY
			Assert.That(invalidatedCountAfterRenumberRequest,
						Is.EqualTo(0),
						"invalidated after renumber request");
			Assert.That(invalidatedCountAfterRenumberResponse,
						Is.EqualTo(1),
						"invalidated after renumber response");
			Assert.IsTrue(isType99InAllDevicesAfterReopen,
						  "Alldevices should contain type99 after renumber");
			Assert.AreEqual(2,
							allDevicesCountAfterReopen,
							"AllDevices should contain 2 devices after renumber");
			Assert.AreEqual(3,
							countAfterReopen,
							"Number of devices after Renumber() should match");
			Assert.AreEqual(2,
							allConversationsCountAfterReopen,
							"AllConversations should contain 2 conversation after renumber");
			Assert.AreEqual(3,
							conversationCountAfterReopen,
							"Number of conversations after Renumber() should match");
			Assert.That(device1AfterReopen.DeviceType.DeviceId,
						Is.EqualTo(99),
						"Device type after renumber.");
			_mockPort.Verify();
		}


		[Test]
		[TestCaseSource(nameof(_invalidatingASCIICommands))]
		public void RM5147_ASCIIKeyCommandsDoNotInvalidate(string aTargetCommand)
		{
			// SETUP
			const string portName = "COM1";
			_portFacade.Port.IsAsciiMode = true;

			var device42 = new DeviceType { DeviceId = 42 };
			SetAsciiExpectations(_portFacade, _mockPort, device42);

			_portFacade.Open(portName);
			var invalidatedCount1 = _invalidatedCount;
			_mockPort.Expect("/01 0 key 1 1 add 3 " + aTargetCommand);
			_mockPort.AddResponse("@01 0 OK IDLE -- 0");
			_portFacade.Port.Send("/01 0 key 1 1 add 3 " + aTargetCommand);
			var invalidatedCount2 = _invalidatedCount;
			_portFacade.Close();

			Assert.AreEqual(invalidatedCount1, invalidatedCount2);
		}


		[Test]
		public void RenumberBinary()
		{
			// SETUP
			const string portName = "COM1";

			// EXPECT
			SetBinaryExpectations(_portFacade,
								  _mockPort,
								  1,
								  42,
								  0,
								  0,
								  2,
								  99,
								  0,
								  0);

			// pretend that we've removed device 1 (type 42)
			_mockPort.Expect(0, Command.Renumber, 0);
			_mockPort.AddResponse(1,
								  Command.Renumber,
								  99); // device id

			SetBinaryExpectations(_portFacade,
								  _mockPort,
								  1,
								  99,
								  0,
								  0);

			// EXEC
			_portFacade.Open(portName);

			_portFacade.GetDevice(0).Send(Command.Renumber);

			var invalidatedCountAfterRenumber = _invalidatedCount;

			_portFacade.Close();
			_portFacade.Open(portName);
			var device1AfterReopen = _portFacade.GetDevice(1);
			var countAfterReopen = _portFacade.Devices.Count;
			var conversationCountAfterReopen = _portFacade.Conversations.Count;
			var allDevices = (DeviceCollection) _portFacade.GetDevice(0);
			var allDevicesCountAfterReopen = allDevices.Count;
			var isType99InAllDevicesAfterReopen = allDevices.Contains(device1AfterReopen);
			var allConversations = (ConversationCollection) _portFacade.GetConversation(0);
			var allConversationsCountAfterReopen = allConversations.Count;

			// VERIFY
			Assert.That(invalidatedCountAfterRenumber,
						Is.EqualTo(1),
						"invalidated after renumber");
			Assert.IsTrue(isType99InAllDevicesAfterReopen,
						  "Alldevices should contain type99 after renumber");
			Assert.AreEqual(1,
							allDevicesCountAfterReopen,
							"AllDevices should contain 1 device after renumber");
			Assert.AreEqual(2,
							countAfterReopen,
							"Number of devices after Renumber() should match");
			Assert.AreEqual(1,
							allConversationsCountAfterReopen,
							"AllConversations should contain 1 conversation after renumber");
			Assert.AreEqual(2,
							conversationCountAfterReopen,
							"Number of conversations after Renumber() should match");
			Assert.AreEqual(99,
							device1AfterReopen.DeviceType.DeviceId,
							"Device type should match after renumber.");
			_mockPort.Verify();
		}


		/// <summary>
		///     Here's the scenario:
		///     1. Open port with one device, message ids on.
		///     2. Connect new device with message ids off. (Software hasn't noticed yet.)
		///     3. Renumber all, new device responds first.
		///     4. Should turn off message ids on original device.
		///     5. Should show message ids turned off on port facade.
		/// </summary>
		[Test]
		public void RenumberWithMixedMessageIds()
		{
			// SETUP
			const string portName = "COM1";
			_portFacade.GetConversation(0).Timeout = 10;

			// EXPECT
			SetBinaryExpectations(_portFacade,
								  0,
								  _mockPort,
								  1,
								  42,
								  0,
								  (int) DeviceModes.EnableMessageIdsMode);

			// pretend that we've connected device 2 (type 99)
			// now we expect a renumber command
			_mockPort.Expect(0, Command.Renumber, 0 | (1 << 24)); // message id
			_mockPort.AddResponse(1,
								  Command.Renumber,
								  99); // device id with no message id
			_mockPort.AddResponse(2,
								  Command.Renumber,
								  42 | (1 << 24)); // device id with message id

			// this triggers the Invalidated event, so we Close and Open
			_mockPort.Expect(0, Command.ReturnDeviceID, 0);
			_mockPort.AddResponse(1, Command.ReturnDeviceID, 99);
			_mockPort.AddResponse(2, Command.ReturnDeviceID, 42);

			_mockPort.Expect(1, Command.ReturnFirmwareVersion, 0);
			_mockPort.AddResponse(1, Command.ReturnFirmwareVersion, 567);
			_mockPort.Expect(2, Command.ReturnFirmwareVersion, 0);
			_mockPort.AddResponse(2, Command.ReturnFirmwareVersion, 567);

			_mockPort.Expect(1, Command.ReturnSetting, (int) Command.SetAliasNumber);
			_mockPort.AddResponse(1, Command.SetAliasNumber, 0);
			_mockPort.Expect(2, Command.ReturnSetting, (int) Command.SetAliasNumber);
			_mockPort.AddResponse(2, Command.SetAliasNumber, 0);

			_mockPort.Expect(1, Command.ReturnSetting, (int) Command.SetMicrostepResolution);
			_mockPort.AddResponse(1, Command.SetMicrostepResolution, 64);
			_mockPort.Expect(2, Command.ReturnSetting, (int) Command.SetMicrostepResolution);
			_mockPort.AddResponse(2, Command.SetMicrostepResolution, 64);

			_mockPort.Expect(1, Command.ReturnSetting, (int) Command.SetDeviceMode);
			_mockPort.AddResponse(1, Command.SetDeviceMode, 0);
			_mockPort.Expect(2, Command.ReturnSetting, (int) Command.SetDeviceMode);
			_mockPort.AddResponse(2, Command.SetDeviceMode, (int) DeviceModes.EnableMessageIdsMode);

			_mockPort.Expect(2, Command.SetDeviceMode, (int) DeviceModes.None);
			_mockPort.AddResponse(2, Command.SetDeviceMode, (int) DeviceModes.None);

			// EXEC
			_portFacade.Open(portName);
			_portFacade.GetConversation(0).Request(Command.Renumber);

			var invalidatedCountAfterRenumber = _invalidatedCount;
			_portFacade.Close();
			_portFacade.Open(portName);

			var isEnabledOnDevice1 = _portFacade.GetDevice(1).AreMessageIdsEnabled;
			var isEnabledOnDevice2 = _portFacade.GetDevice(2).AreMessageIdsEnabled;
			var isEnabledOnPortFacade = _portFacade.AreMessageIdsEnabled;

			// VERIFY
			Assert.AreEqual(1,
							invalidatedCountAfterRenumber,
							"Number of Invalidated events after Renumber");
			Assert.AreEqual(false,
							isEnabledOnDevice1,
							"Are message ids enabled on device 1");
			Assert.AreEqual(false,
							isEnabledOnDevice2,
							"Are message ids enabled on device 2");
			Assert.AreEqual(false,
							isEnabledOnPortFacade,
							"Are message ids enabled on port facade");
			_mockPort.Verify();
		}


		[Test]
		public void RestoreSettings()
		{
			// SETUP
			const string portName = "COM1";

			// EXPECT
			SetBinaryExpectations(_portFacade,
								  _mockPort,
								  1,
								  42,
								  0,
								  0,
								  2,
								  99,
								  0,
								  0);

			_mockPort.Expect(1, Command.RestoreSettings, 0);
			_mockPort.AddResponse(1,
								  Command.RestoreSettings,
								  0);

			// EXEC
			_portFacade.Open(portName);

			_portFacade.GetDevice(1).Send(Command.RestoreSettings);

			// VERIFY
			Assert.AreEqual(1,
							_invalidatedCount,
							"Invalidated count");
			_mockPort.Verify();
		}


		[Test]
		public void RM4327_EnableASCIIMessageIdsWorksOnAllDevices()
		{
			_portFacade.Port.IsAsciiMode = true;
			SetAsciiExpectations(_portFacade,
								 _mockPort,
								 new DeviceType
								 {
									 DeviceId = 12345,
									 FirmwareVersion = new FirmwareVersion(6, 28)
								 },
								 new DeviceType
								 {
									 DeviceId = 54321,
									 FirmwareVersion = new FirmwareVersion(6, 28)
								 });

			_portFacade.Open("COM1");
			var allDevices = _portFacade.GetConversation(0);
			_portFacade.AreMessageIdsEnabled = false;
			Assert.IsFalse(allDevices.Device.AreAsciiMessageIdsEnabled);
			foreach (var conv in _portFacade.AllConversations)
			{
				Assert.IsFalse(conv.Device.AreAsciiMessageIdsEnabled);
			}

			_portFacade.AreMessageIdsEnabled = true;
			Assert.IsTrue(allDevices.Device.AreAsciiMessageIdsEnabled);
			foreach (var conv in _portFacade.AllConversations)
			{
				Assert.IsTrue(conv.Device.AreAsciiMessageIdsEnabled);
			}
		}


		[Test]
		public void SelectedConversation()
		{
			// SETUP
			Conversation updatedConversation = null;
			_portFacade.SelectedConversationChanged +=
				delegate { updatedConversation = _portFacade.SelectedConversation; };
			const string portName = "COM1";

			// EXPECT
			SetBinaryExpectations(_portFacade,
								  _mockPort,
								  1,
								  42,
								  0,
								  0,
								  2,
								  99,
								  0,
								  0);

			// EXEC
			_portFacade.Open(portName);
			var selectedAfterOpen = _portFacade.SelectedConversation;
			var allConversations = _portFacade.GetConversation(0);
			var conversation2 = _portFacade.GetConversation(2);
			_portFacade.SelectedConversation = conversation2;
			var selectedAfterChange = _portFacade.SelectedConversation;

			// VERIFY
			Assert.That(selectedAfterOpen,
						Is.SameAs(allConversations),
						"selected after open");
			Assert.That(selectedAfterChange,
						Is.SameAs(conversation2),
						"selected after change");
			Assert.That(updatedConversation,
						Is.SameAs(conversation2),
						"updated through event");
		}


		[Test]
		public void SetMessageIdsAfterReopen()
		{
			// EXPECT
			const string portName = "COM1";

			SetBinaryExpectations(_portFacade,
								  _mockPort,
								  1,
								  42,
								  0,
								  (int) (DeviceModes.DisablePowerLed | DeviceModes.EnableMessageIdsMode),
								  2,
								  99,
								  0,
								  (int) DeviceModes.EnableMessageIdsMode);

			SetBinaryExpectations(_portFacade,
								  _mockPort,
								  1,
								  42,
								  0,
								  (int) (DeviceModes.DisablePowerLed | DeviceModes.EnableMessageIdsMode),
								  2,
								  99,
								  0,
								  (int) DeviceModes.EnableMessageIdsMode);

			// EXEC
			_portFacade.Open(portName);

			_portFacade.Close();

			_portFacade.Open(portName);

			var areEnabledOnFacade = _portFacade.AreMessageIdsEnabled;
			var areEnabledOnDevice1 = _portFacade.GetDevice(1).AreMessageIdsEnabled;

			// VERIFY
			Assert.IsTrue(areEnabledOnFacade,
						  "Message ids should be enabled on port facade.");
			Assert.IsTrue(areEnabledOnDevice1,
						  "Message ids should be enabled on device object.");
			_mockPort.Verify();
		}


		[Test]
		public void SixNineNineDeviceIsAskedForBuildNumber()
		{
			// SETUP & EXPECT
			_portFacade.Port.IsAsciiMode = true;
			SetAsciiExpectations(_portFacade,
								 _mockPort,
								 new DeviceType
								 {
									 DeviceId = 99999,
									 FirmwareVersion = new FirmwareVersion(6, 99, 204)
								 });

			// EXEC
			_portFacade.Open("COM1");

			// VERIFY
			Assert.That(_portFacade.GetDevice(1).FirmwareVersion.Build, Is.EqualTo(204));
		}


		[Test]
		public void SixNineNineDeviceWithoutBuildNumberGetsDefaultValue()
		{
			// SETUP & EXPECT
			_portFacade.Port.IsAsciiMode = true;
			SetAsciiExpectations(_portFacade,
								 _mockPort,
								 new DeviceType
								 {
									 DeviceId = 99999,
									 FirmwareVersion = new FirmwareVersion(6, 99)
								 });

			// See SetAsciiExpectations: if the build number is 0, the fake 
			// device will respond with "BADCOMMAND", just like in real life.
			// If an exception isn't raised and the resulting ZaberDevice's 
			// build number is 0, then we're all good.

			// EXEC
			_portFacade.Open("COM1");

			// VERIFY
			Assert.That(_portFacade.GetDevice(1).FirmwareVersion.Build, Is.EqualTo(0));
		}


		[Test]
		public void SwitchPorts()
		{
			// SETUP
			var oldPort = _mockPort;
			var newPort = new MockPort();

			var invalidateCount = 0;
			_portFacade.Invalidated +=
				delegate { invalidateCount++; };

			// EXEC
			_portFacade.Port = newPort;

			// Response from old port should no longer be received.
			oldPort.FireResponse(2, Command.Renumber, 12345);

			// VERIFY
			Assert.That(invalidateCount,
						Is.EqualTo(0));
		}


		[Test]
		public void TestGlobalAsciiRenumberRejection()
		{
			// SETUP
			const string portName = "COM1";
			_portFacade.Port.IsAsciiMode = true;

			var device42 = new DeviceType { DeviceId = 42 };

			// EXPECT
			SetAsciiExpectations(_portFacade, _mockPort, device42);

			_mockPort.Expect("/42 renumber");

			// we will raise the responses manually.

			// EXEC
			_portFacade.Open(portName);

			_portFacade.Port.Send("/42 renumber"); // Devices reject this command if addressed; it's broadcast-only.

			var invalidatedCountAfterRenumberRequest = _invalidatedCount;

			_mockPort.FireResponse("@42 0 RJ IDLE -- BADDATA");

			var invalidatedCountAfterRenumberResponse = _invalidatedCount;

			_portFacade.Close();

			// No devices should have been invalidated because the command was rejected.
			Assert.AreEqual(invalidatedCountAfterRenumberRequest, invalidatedCountAfterRenumberResponse);
		}


		[Test]
		[Description("Test that an ASCII device with uninitialized ID will still be visible.")]
		public void TestOpenAsciiWith32BitDeviceId()
		{
			_mockPort.Expect("/get deviceid");
			_mockPort.AddResponse("@01 0 OK IDLE - 4294967295");
			_mockPort.Expect("/01 0 get version");
			_mockPort.AddResponse("@01 0 OK IDLE -- 7.99");
			_mockPort.Expect("/01 0 get system.access");
			_mockPort.AddResponse("@01 0 OK IDLE -- 2");
			_mockPort.Expect("/01 0 get system.serial");
			_mockPort.AddResponse("@01 0 OK IDLE -- 4294967295");
			_mockPort.Expect("/01 0 get version.build");
			_mockPort.AddResponse("@01 0 OK IDLE -- 1198");
			_mockPort.Expect("/01 0 help commands list");
			_mockPort.AddResponse("@01 0 OK IDLE -- 0");
			_mockPort.AddResponse("#01 0 ");
			_mockPort.Expect("/01 0 get system.axiscount");
			_mockPort.AddResponse("@01 0 OK IDLE -- 1");
			_mockPort.Expect("/01 0 get peripheralid");
			_mockPort.AddResponse("@01 0 RJ IDLE -- BADCOMMAND");
			_mockPort.Expect("/01 0 get resolution");
			_mockPort.AddResponse("@01 0 RJ IDLE -- BADCOMMAND");

			_portFacade.Port.IsAsciiMode = true;

			// Before the fix this would have aborted the detection after the first response.
			_portFacade.Open("COM1");

			var device = _portFacade.GetDevice(1);
			Assert.AreEqual(-1, device.DeviceType.DeviceId);
		}


		[Test]
		public void Trac1896_CloseClearsDeviceMap()
		{
			Assert.AreNotEqual(0, _portFacade.DeviceTypeMap.Count);

			// Open the port and verify the device map still has the device.
			SetBinaryExpectations(_portFacade, _mockPort, 1, 123, 0, 0);
			_portFacade.Open("COM1");
			Assert.AreNotEqual(0, _portFacade.DeviceTypeMap.Count);

			// Close the port and verify the device map is cleared.
			_portFacade.Close();
			Assert.AreEqual(0, _portFacade.DeviceTypeMap.Count);
		}


		[Test]
		public void UnknownDeviceId()
		{
			// EXPECT
			const string portName = "COM1";
			SetBinaryExpectations(_portFacade,
								  _mockPort,
								  1,
								  999999,
								  0,
								  0, // Unknown device id.
								  2,
								  99,
								  0,
								  0);

			// EXEC
			_portFacade.Open(portName);

			// VERIFY
			Assert.AreEqual(_portFacade.DefaultDeviceType.Commands,
							_portFacade.GetDevice(1).DeviceType.Commands,
							"Unknown device type should get default commands");
			Assert.AreEqual(999999,
							_portFacade.GetDevice(1).DeviceType.DeviceId,
							"DeviceId should match");
			_mockPort.Verify();
		}


		[Test]
		public void UnknownDeviceNumber()
		{
			// SETUP
			const string portName = "COM1";
			const byte knownDeviceNumber = 1;
			const byte unknownDeviceNumber = 5;
			_portFacade.AreDeviceNumbersValidated = true;
			_portFacade.IsInvalidateEnabled = false;

			// EXPECT
			SetBinaryExpectations(_portFacade, _mockPort, knownDeviceNumber, 42, 0, 0);
			_mockPort.ExpectedInvalidPacketCount = 1;

			// EXEC
			_portFacade.Open(portName);

			_mockPort.FireResponse(unknownDeviceNumber, Command.EchoData, 0);

			// VERIFY
			_mockPort.Verify();
		}


		[Test]
		public void UnknownDeviceNumberIgnored()
		{
			// SETUP
			const string portName = "COM1";
			const byte knownDeviceNumber = 1;
			const byte unknownDeviceNumber = 5;
			_portFacade.AreDeviceNumbersValidated = false;

			// EXPECT
			SetBinaryExpectations(_portFacade, _mockPort, knownDeviceNumber, 42, 0, 0);
			_mockPort.ExpectedInvalidPacketCount = 0;

			// EXEC
			_portFacade.Open(portName);

			_mockPort.FireResponse(unknownDeviceNumber, Command.EchoData, 0);

			// VERIFY
			_mockPort.Verify();
		}


		[Test]
		public void UsingBlockHappy()
		{
			// SETUP
			const string portName = "COM1";

			// EXPECT
			SetBinaryExpectations(_portFacade, _mockPort, 1, 42, 0, 0);

			_mockPort.Expect(0, Command.MoveRelative, 100);

			// EXEC
			using (_portFacade)
			{
				_portFacade.Open(portName);


				_portFacade.GetDevice(0).Send(Command.MoveRelative, 100);
			}

			var isOpen = _mockPort.IsOpen;

			// VERIFY
			Assert.AreEqual(false,
							isOpen,
							"isOpen");
			_mockPort.Verify();
		}


		[Test]
		public void UsingBlockSad()
		{
			// SETUP
			const string portName = "COM2";

			// EXPECT
			SetBinaryExpectations(_portFacade, _mockPort, 1, 42, 0, 0);

			_mockPort.Expect(0, Command.MoveRelative, 100);
			_mockPort.AddException(new AccessViolationException("Access denied on port COM2."));

			// EXEC
			_portFacade.Port = _mockPort;
			string msg = null;

			try
			{
				using (_portFacade)
				{
					_portFacade.Open(portName);

					_portFacade.GetDevice(0).Send(Command.MoveRelative, 100);

					Assert.Fail("Should have thrown.");
				}
			}
			catch (AccessViolationException ex)
			{
				msg = ex.Message;
			}

			var isOpen = _portFacade.IsOpen;

			// VERIFY
			Assert.AreEqual("Access denied on port COM2.",
							msg,
							"Message should match");
			Assert.AreEqual(false,
							isOpen,
							"isOpen");
			_mockPort.Verify();
		}


		/// <summary>
		///     Prepare the mock port for the open sequence in ASCII.
		/// </summary>
		/// <param name="devices">
		///     The types of the devices which will respond.
		///     They must be in order of device number (first parameter is device
		///     number 1, second parameter is device number 2, etc). If no
		///     arguments are given, a single default device will respond.
		/// </param>
		public static void SetAsciiExpectations(ZaberPortFacade aPortFacade, MockPort aPort,
												params DeviceType[] devices)
		{
			if (devices.Length == 0)
			{
				devices = new[]
				{
					new DeviceType
					{
						DeviceId = 99,
						FirmwareVersion = new FirmwareVersion(600)
					}
				};
			}

			aPort.Expect("/get deviceid");
			for (var i = 0; i < devices.Length; i++)
			{
				aPort.AddResponse($"@{i + 1:D2} 0 OK IDLE -- {devices[i].DeviceId}");
			}

			for (var i = 0; i < devices.Length; i++)
			{
				aPort.Expect($"/{i + 1:D2} 0 get version");
				aPort.AddResponse($"@{i + 1:D2} 0 OK IDLE -- {devices[i].FirmwareVersion}");

				if (devices[i].FirmwareVersion.ToInt() >= 606)
				{
					aPort.Expect($"/{i + 1:D2} 0 get system.access");

					// Hardcoded to max (2) because AccessLevel is a device instance property, not a device type property.
					aPort.AddResponse($"@{i + 1:D2} 0 OK IDLE -- 2");
				}

				if (devices[i].FirmwareVersion.ToInt() >= 615)
				{
					aPort.Expect($"/{i + 1:D2} 0 get system.serial");

					// Hardcoded to have no serial number since that's a device instance property.
					aPort.AddResponse($"@{i + 1:D2} 0 OK IDLE -- 0");
				}

				if ((devices[i].FirmwareVersion.Major >= 6) && (devices[i].FirmwareVersion.Minor >= 95))
				{
					aPort.Expect($"/{i + 1:D2} 0 get version.build");
					aPort.AddResponse(devices[i].FirmwareVersion.Build != 0
										  ? $"@{i + 1:D2} 0 OK IDLE -- {devices[i].FirmwareVersion.Build}"
										  : $"@{i + 1:D2} 0 RJ IDLE -- BADCOMMAND");
				}
			}

			for (var i = 0; i < devices.Length; i++)
			{
				var key = new Tuple<int, FirmwareVersion>(devices[i].DeviceId, devices[i].FirmwareVersion);

				// PortFacade will only ask for a list of commands if it 
				// doesn't already have one for the device.
				if (!aPortFacade.DeviceTypeMap.ContainsKey(key))
				{
					aPort.Expect($"/{i + 1:D2} 0 help commands list");
					aPort.AddResponse($"@{i + 1:D2} 0 OK IDLE -- 0");
					aPort.AddResponse($"#{i + 1:D2} 0");
				}
			}

			for (var i = 0; i < devices.Length; i++)
			{
				aPort.Expect($"/{i + 1:D2} 0 get system.axiscount");
				var axisCount = Math.Max(devices[i].PeripheralMap.Count, 1);
				aPort.AddResponse($"@{i + 1:D2} 0 OK IDLE -- {axisCount}");
			}

			for (var i = 0; i < devices.Length; i++)
			{
				aPort.Expect($"/{i + 1:D2} 0 get peripheralid");
				if ((devices[i].PeripheralMap != null)
				&& (devices[i].PeripheralMap.Count > 0))
				{
					var peripheralIds = string.Join(" ",
													from p in devices[i].PeripheralMap.Values
													select p.PeripheralId);
					aPort.AddResponse($"@{i + 1:D2} 0 OK IDLE -- {peripheralIds}");
				}
				else
				{
					aPort.AddResponse($"@{i + 1:D2} 0 RJ IDLE -- BADCOMMAND");
				}
			}

			for (var i = 0; i < devices.Length; i++)
			{
				aPort.Expect($"/{i + 1:D2} 0 get resolution");
				aPort.AddResponse(
					$"@{i + 1:D2} 0 OK IDLE -- {string.Join(" ", Enumerable.Repeat("64", Math.Max(devices[i].PeripheralMap.Count, 1)))}");
			}
		}


		public static void SetBinaryExpectations(ZaberPortFacade aPortFacade, MockPort aPort, params int[] responses)
			=> SetBinaryExpectations(aPortFacade, 500, aPort, responses);


		/// <summary>
		///     Prepare the mock port for the open sequence with message ids disabled.
		/// </summary>
		/// <param name="responses">
		///     Response contents to return during Open(). The contents are:
		///     deviceNum, deviceId, alias, mode, deviceNum, deviceId, alias, mode, ...
		/// </param>
		public static void SetBinaryExpectations(ZaberPortFacade aPortFacade, int aFwVersion, MockPort aPort,
												 params int[] responses)
		{
			var expectationCount = responses.Length / 4;
			var expectations = new OpenExpectation[expectationCount];
			for (var i = 0; i < expectationCount; i++)
			{
				expectations[i] = new OpenExpectation
				{
					DeviceNumber = (byte) responses[4 * i],
					DeviceId = responses[(4 * i) + 1],
					AliasNumber = responses[(4 * i) + 2],
					DeviceModes = (DeviceModes) responses[(4 * i) + 3],
					FirmwareVersion = aFwVersion
				};
			}

			Assert.That(aPortFacade.Port.IsAsciiMode,
						Is.False,
						"SetBinaryExpectations doesn't support ASCII mode anymore.");
			SetBinaryExpectations(aPort, expectations);
		}


		private class OpenWithPortErrorThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Open()
			{
				// SETUP
				_mockTimer = new MockTimeoutTimer();
				_port = new MockPort();
				_portFacade = CreatePortFacade(_port);
				_portFacade.QueryTimeoutTimer = _mockTimer;

				// EXPECT
				_port.Expect(0, Command.ReturnDeviceID, 0);

				// EXEC
				string msg = null;
				try
				{
					_portFacade.Open("COM1");

					Assert.Fail("should have thrown");
				}
				catch (ZaberPortErrorException ex)
				{
					msg = ex.Message;
				}

				var isOpen = _portFacade.IsOpen;
				var facadeState = _portFacade.CurrentState;

				// VERIFY
				AssertTick(2);
				Assert.AreEqual("Port error: Frame",
								msg,
								"error message");
				Assert.AreEqual(false,
								isOpen,
								"is open");
				Assert.AreEqual(ZaberPortFacadeState.Closed,
								facadeState,
								"facade state");
				_port.Verify();
			}


			[TestThread]
			public void Responses()
			{
				WaitForTick(1);
				_port.FirePortError(ZaberPortError.Frame);

				WaitForTick(2);
				_mockTimer.TimeoutNow();
			}

			#endregion

			#region -- Data --

			private MockPort _port;
			private ZaberPortFacade _portFacade;
			private MockTimeoutTimer _mockTimer;

			#endregion
		}

		private class EnableMessageIdsWithTimeoutThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			public EnableMessageIdsWithTimeoutThreads(ZaberPortFacade portFacade)
			{
				_portFacade = portFacade;
			}


			[TestThread]
			public void Request()
			{
				// SETUP
				_mockTimer = new MockTimeoutTimer();
				_portFacade.QueryTimeoutTimer = _mockTimer;

				// EXEC
				string msg = null;
				try
				{
					_portFacade.AreMessageIdsEnabled = true;
					Assert.Fail("should have thrown.");
				}
				catch (RequestTimeoutException ex)
				{
					msg = ex.Message;
				}

				AssertTick(1);

				// VERIFY
				Assert.That(msg,
							Is.EqualTo("Request timed out."));
				Assert.That(_portFacade.AreMessageIdsEnabled,
							Is.False,
							"message ids enabled after timeout");
			}


			[TestThread]
			public void Timeout()
			{
				WaitForTick(1);

				_mockTimer.TimeoutNow();
			}

			#endregion

			#region -- Data --

			private readonly ZaberPortFacade _portFacade;
			private MockTimeoutTimer _mockTimer;

			#endregion
		}

		private class OpenExpectation
		{
			#region -- Public Methods & Properties --

			public OpenExpectation()
			{
				DeviceId = 65535;
				MicrostepResolution = ZaberDevice.DefaultMicrostepResolution;
				IsMicrostepResolutionSupported = true;
				FirmwareVersion = 0;
			}


			public byte DeviceNumber { get; set; }

			public int DeviceId { get; set; }

			public int PeripheralId { get; set; }

			public int AliasNumber { get; set; }

			public DeviceModes DeviceModes { get; set; }

			public int MicrostepResolution { get; set; }

			public bool IsMicrostepResolutionSupported { get; set; }

			public int FirmwareVersion { get; set; }

			public uint SerialNumber { get; set; }

			#endregion
		}


		private static ZaberPortFacade CreatePortFacade(IZaberPort port)
		{
			var defaultDeviceType = new DeviceType { Commands = new List<CommandInfo>() };

			var portFacade = new ZaberPortFacade
			{
				DefaultDeviceType = defaultDeviceType,
				Port = port,
				QueryTimeout = 100
			};

			var type27 = new DeviceType
			{
				DeviceId = 27,
				FirmwareVersion = new FirmwareVersion(5, 0),
				Commands = new List<CommandInfo>()
			};
			portFacade.AddDeviceType(type27);
			var type82 = new DeviceType
			{
				DeviceId = 82,
				Commands = new List<CommandInfo>()
			};
			portFacade.AddDeviceType(type82);
			var type99 = new DeviceType
			{
				DeviceId = 99,
				Commands = new List<CommandInfo>(),
				MotionType = MotionType.Linear
			};
			portFacade.AddDeviceType(type99);

			var peripheral0 = new DeviceType
			{
				PeripheralId = 0,
				Commands = new List<CommandInfo>()
			};
			var type98 = new DeviceType
			{
				DeviceId = 98,
				PeripheralMap = new Dictionary<int, DeviceType> { { 0, peripheral0 } }
			};
			portFacade.AddDeviceType(type98);

			return portFacade;
		}


		/// <summary>
		///     Prepare the mock port for the open sequence with message ids disabled.
		/// </summary>
		/// <param name="responses">Response contents to return during Open().</param>
		private static void SetBinaryExpectations(MockPort aPort, params OpenExpectation[] responses)
		{
			aPort.Expect(0, Command.ReturnDeviceID, 0);
			foreach (var response in responses)
			{
				aPort.AddResponse(response.DeviceNumber,
								  Command.ReturnDeviceID,
								  response.DeviceId);
			}

			// sort the offsets because the follow-up requests are sent
			// in sorted order by device number.
			Array.Sort(responses,
					   (r1, r2) => r1.DeviceNumber.CompareTo(r2.DeviceNumber));

			// Firmware version.
			foreach (var response in responses)
			{
				var deviceNumber = response.DeviceNumber;
				aPort.Expect(deviceNumber,
							 Command.ReturnFirmwareVersion,
							 0);
				aPort.AddResponse(deviceNumber,
								  Command.ReturnFirmwareVersion,
								  response.FirmwareVersion);

				// Serial number.
				if (response.FirmwareVersion >= 607)
				{
					aPort.Expect(deviceNumber,
								 Command.ReturnSerialNumber,
								 0);
					aPort.AddResponse(deviceNumber,
									  Command.ReturnSerialNumber,
									  (int) response.SerialNumber);
				}
			}


			// Alias number.
			foreach (var response in responses)
			{
				if (response.FirmwareVersion >= 500)
				{
					var deviceNumber = response.DeviceNumber;
					aPort.Expect(deviceNumber,
								 Command.ReturnSetting,
								 (int) Command.SetAliasNumber);
					aPort.AddResponse(deviceNumber,
									  Command.SetAliasNumber,
									  response.AliasNumber);
				}
			}

			// Microstep resolution.
			foreach (var response in responses)
			{
				if (response.FirmwareVersion < 500)
				{
					continue; // FW 2.93 does not reply.
				}

				var deviceNumber = response.DeviceNumber;
				aPort.Expect(deviceNumber,
							 Command.ReturnSetting,
							 (int) Command.SetMicrostepResolution);
				if (response.IsMicrostepResolutionSupported)
				{
					aPort.AddResponse(deviceNumber,
									  Command.SetMicrostepResolution,
									  response.MicrostepResolution);
				}
				else
				{
					aPort.AddResponse(deviceNumber,
									  Command.Error,
									  (int) ZaberError.SettingInvalid);
				}
			}

			// Peripheral ID.
			foreach (var response in responses.Where(response =>
														 response.FirmwareVersion >= 603))
			{
				aPort.Expect(response.DeviceNumber,
							 Command.ReturnSetting,
							 (int) Command.SetPeripheralID);
				aPort.AddResponse(response.DeviceNumber,
								  Command.SetPeripheralID,
								  response.PeripheralId);
			}

			// Device Mode.
			foreach (var response in responses)
			{
				var deviceNumber = response.DeviceNumber;
				if (response.FirmwareVersion < 600)
				{
					aPort.Expect(deviceNumber,
								 Command.ReturnSetting,
								 (int) Command.SetDeviceMode);
					aPort.AddResponse(deviceNumber,
									  Command.SetDeviceMode,
									  (int) response.DeviceModes);
				}
				else
				{
					aPort.Expect(deviceNumber,
								 Command.ReturnSetting,
								 (int) Command.SetMessageIDMode);
					aPort.AddResponse(deviceNumber,
									  Command.SetMessageIDMode,
									  (response.DeviceModes
								   & DeviceModes.EnableMessageIdsMode)
								  != 0
										  ? 1
										  : 0);
				}
			}
		}


		private MockPort _mockPort;
		private ZaberPortFacade _portFacade;
		private int _invalidatedCount;

		private static readonly object[] _invalidateTestCases =
		{
			new object[] { 3, Command.Renumber, 99, true }, new object[] { 3, Command.RestoreSettings, 0, true },
			new object[] { 3, Command.SetAliasNumber, 0, true }, new object[] { 2, Command.SetAliasNumber, 0, false },
			new object[] { 1, Command.SetAliasNumber, 0, true }, new object[] { 2, Command.SetAliasNumber, 100, true },
			new object[] { 2, Command.SetAliasNumber, 200, true },

			//500 is an invalid alias number, so ignore it
			new object[] { 2, Command.SetAliasNumber, 500, false },

			//Alias 1 collides with single device, so ignore it
			new object[] { 2, Command.SetAliasNumber, 1, false }, new object[] { 2, Command.EchoData, 42, false }
		};

		private static readonly string[] _invalidatingASCIICommands =
		{
			"renumber",
			"system factoryreset",
			"set comm.address 4",
			"system restore",
			"set comm.protocol 0",
			"set comm.rs232.baud 9600",
			"set comm.usb.protocol 0",
			"set deviceid 12345",
			"set device.id 12345",
			"set peripheralid 12345",
			"set peripheral.id 12345",
			"set system.access 2",
			"tools setcomm 9600 0"
		};
}
}

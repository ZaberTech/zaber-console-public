﻿using System.Collections.Generic;
using NUnit.Framework;
using Zaber;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	internal class ASCIICommandInfoTests
	{
		[Test]
		public void AccessLevelMatchesLastNode()
		{
			//SETUP
			var node1 = new ASCIICommandNode();
			var node2 = new ASCIICommandNode(node1) { AccessLevel = 4 };

			//EXEC
			var cmdInfo = new ASCIICommandInfo(node2);

			//VERIFY
			Assert.AreEqual(cmdInfo.AccessLevel, 4);
		}


		[Test]
		public void IsBasicMatchesLastNode()
		{
			//SETUP
			var node1 = new ASCIICommandNode { IsBasic = false };
			var node2 = new ASCIICommandNode(node1) { IsBasic = true };

			//EXEC
			var cmdInfo = new ASCIICommandInfo(node2);

			//VERIFY
			Assert.AreEqual(node2.IsBasic, true);
		}


		[Test]
		public void NameAndNodeTextsMatch()
		{
			//SETUP
			var node1 = new ASCIICommandNode { NodeText = "one" };
			var node2 = new ASCIICommandNode(node1) { NodeText = "two" };
			var node3 = new ASCIICommandNode(node2)
			{
				ParamType = new ParameterType
				{
					IsNumeric = true,
					IsSigned = true
				}
			};

			//EXEC
			var cmdName = new ASCIICommandInfo(node3).Name;

			//VERIFY
			Assert.AreEqual(cmdName, "one two {0}");
		}


		[Test]
		public void NodeListAndBranchAreEqual()
		{
			//SETUP
			var node1 = new ASCIICommandNode();
			var node2 = new ASCIICommandNode(node1);
			var node3 = new ASCIICommandNode(node2);
			ASCIICommandNode[] nodes = { node1, node2, node3 };
			var branchList = new LinkedList<ASCIICommandNode>(nodes);

			//EXEC
			var node3Branch = node3.GetBranch();

			//VERIFY
			Assert.AreEqual(node3Branch, branchList);
		}
	}
}

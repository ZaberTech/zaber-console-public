﻿using System;
using System.IO;
using System.Text;
using NUnit.Framework;
using Zaber.Ports;
using ZaberTest.Testing;

namespace ZaberTest.Ports
{
	[TestFixture]
	[SetCulture("en-US")]
	public class EncodingSanityCheck
	{
		[Test]
		[Description("Ensures that the encoding used by the RawTcpPort doesn't change binary bytes.")]
		public void TestBinaryPassthrough()
		{
			var encoding = InvocationHelper.GetPrivateStaticProperty(typeof(RawTcpPort), "Encoding") as Encoding;
			TestEncoding(encoding);
		}


		private void TestEncoding(Encoding aEncoding)
		{
			var data = new byte[256];
			for (var i = 0; i < data.Length; i++)
			{
				data[i] = (byte) i;
			}

			var result = ReadData(aEncoding, data);
			CheckResults(data, result);
			result = WriteData(aEncoding, data);
			CheckResults(data, result);

			// Also try random data in an attempt to catch multi-byte encoding errors.
			data = new byte[100000];
			_seed = DateTime.Now.GetHashCode();
			var r = new Random(_seed.Value);
			for (var i = 0; i < 10; i++)
			{
				r.NextBytes(data);
				result = ReadData(aEncoding, data);
				CheckResults(data, result);
				result = WriteData(aEncoding, data);
				CheckResults(data, result);
			}
		}


		private byte[] ReadData(Encoding aEncoding, byte[] aData)
		{
			var chars = new char[aData.Length];

			using (var ms = new MemoryStream(aData))
			{
				using (var reader = new StreamReader(ms, aEncoding))
				{
					var numRead = reader.Read(chars, 0, aData.Length);
					Assert.AreEqual(aData.Length, numRead);
				}
			}

			var result = new byte[aData.Length];
			for (var i = 0; i < aData.Length; i++)
			{
				result[i] = (byte) chars[i];
			}

			return result;
		}


		private byte[] WriteData(Encoding aEncoding, byte[] aData)
		{
			var chars = new char[aData.Length];
			for (var i = 0; i < aData.Length; i++)
			{
				chars[i] = (char) aData[i];
			}

			using (var ms = new MemoryStream())
			{
				using (var writer = new StreamWriter(ms, aEncoding))
				{
					writer.Write(chars);
					writer.Flush();

					var result = new byte[ms.Length];
					Array.Copy(ms.GetBuffer(), result, ms.Length);
					return result;
				}
			}
		}


		private void CheckResults(byte[] aExpected, byte[] aActual)
		{
			Assert.AreEqual(aExpected.Length, aActual.Length);

			var seedStr = "";
			if (_seed.HasValue)
			{
				seedStr = $"Random seed = {_seed.Value}";
			}

			for (var i = 0; i < aExpected.Length; i++)
			{
				Assert.AreEqual(aExpected[i],
								aActual[i],
								$"Results differ at position {i}: {aExpected[i]} became {aActual[i]}. {seedStr}");
			}
		}


		private int? _seed;
	}
}

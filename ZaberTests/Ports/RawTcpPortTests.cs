﻿using System;
using System.Linq;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using Zaber.Ports;
using ZaberTest.Testing;

namespace ZaberTest.Ports
{
	[TestFixture]
	[SetCulture("en-US")]
	public class RawTcpPortTests
	{
		[SetUp]
		public void Setup()
		{
			_portNumber = TcpLoopback.GetAvailablePortNumber();
			_server = new TcpLoopback(_portNumber);
			_server.Start();
			_port = new RawTcpPort("localhost", _portNumber);
			_port.Open();
		}


		[TearDown]
		public void Teardown()
		{
			_port.Close();
			_port.Dispose();
			_server.Stop();

			// Only raise server exceptions if the test is failing, because normal client
			// disconnection can cause a bunch of different server exceptions.
			if ((TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed)
			&& (null != _server.Exception))
			{
				throw _server.Exception;
			}
		}


		[Test]
		public void TestASCIIReadWrite()
		{
			var asciiChars = Enumerable.Range(32, 127 - 32).Select(i => (char) i).ToArray();
			var seed = DateTime.Now.GetHashCode();
			var r = new Random(seed);
			for (var j = 0; j < 100; j++)
			{
				var data = new char[r.Next(2, 10000)];
				for (var i = 0; i < data.Length; i++)
				{
					data[i] = asciiChars[r.Next(asciiChars.Length)];
				}

				data[data.Length - 2] = '\r';
				data[data.Length - 1] = '\n';
				var str = new string(data);

				_port.Write(str);
				var received = _port.ReadLine();

				Assert.AreEqual(str.Length - 2, received.Length); // ReadLine trims line endings.
				Assert.AreEqual(str.Substring(0, str.Length - 2),
								received,
								$"Received '{received}' when expecting '{str}'. Random seed = {seed}.");
			}
		}


		[Test]
		public void TestBinaryReadWrite()
		{
			var seed = DateTime.Now.GetHashCode();
			var r = new Random(seed);
			for (var j = 0; j < 100; j++)
			{
				var data = new byte[r.Next(1, 10000)];
				r.NextBytes(data);
				_port.Write(data);
				var received = _port.Read(data.Length);

				Assert.AreEqual(data.Length, received.Length);
				for (var i = 0; i < data.Length; i++)
				{
					Assert.AreEqual(data[i],
									received[i],
									$"Received data differs at position {i}: {received[i]} should be {data[i]}. Random seed = {seed}.");
				}
			}
		}


		[Test]
		public void TestMixedRead()
		{
			var message = "@01 0 OK IDLE -- 0\r\n";
			_port.Write(message);
			var expectedheader = new byte[] { 0x40, 0x30, 0x31, 0x20, 0x30, 0x20 };
			var header = _port.Read(expectedheader.Length);
			Assert.AreEqual(expectedheader.Length, header.Length);
			for (var i = 0; i < header.Length; i++)
			{
				Assert.AreEqual(header[i], expectedheader[i]);
			}

			var remainder = _port.ReadLine();
			Assert.AreEqual("OK IDLE -- 0", remainder);
		}


		private int _portNumber;
		private TcpLoopback _server;
		private RawTcpPort _port;
	}
}

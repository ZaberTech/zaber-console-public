﻿using System;
using NUnit.Framework;
using Zaber;

namespace ZaberTest
{
	[TestFixture]
	[Category("active")]
	[SetCulture("en-US")]
	public class ConversionTableTest
	{
		[Test]
		public void Convert()
		{
			// SETUP
			var mm = UnitOfMeasure.Millimeter;
			var inch = UnitOfMeasure.Inch;
			var input = new Measurement(2.0m, inch);
			var expectedOutput = new Measurement(50.8m, mm);

			// EXEC
			var actualOutput = ConversionTable.Default.Convert(input, mm);

			// VERIFY
			Assert.That(actualOutput,
						Is.EqualTo(expectedOutput),
						"converted measurement");
		}


		[Test]
		public void ConvertBack()
		{
			// SETUP
			var mm = UnitOfMeasure.Millimeter;
			var inch = UnitOfMeasure.Inch;
			var input = new Measurement(50.8m, mm);
			var expectedOutput = new Measurement(2.0m, inch);

			// EXEC
			var actualOutput = ConversionTable.Default.Convert(input, inch);

			// VERIFY
			Assert.That(actualOutput.Unit,
						Is.EqualTo(inch),
						"converted unit");
			Assert.That(actualOutput.Value,
						Is.InRange(1.99999m, 2.00001m),
						"converted value");
		}


		[Test]
		public void SameUnit()
		{
			// SETUP
			var mm = UnitOfMeasure.Millimeter;
			var input = new Measurement(50.8m, mm);
			var expectedOutput = new Measurement(50.8m, mm);

			// EXEC
			var actualOutput = ConversionTable.Default.Convert(input, mm);

			// VERIFY
			Assert.That(actualOutput,
						Is.EqualTo(expectedOutput),
						"converted measurement");
		}


		[Test]
		public void UnknownFromUnit()
		{
			// SETUP
			var from = UnitOfMeasure.Degree;
			var to = UnitOfMeasure.Inch;

			// EXEC
			var ex = Assert.Throws(typeof(InvalidOperationException),
								   delegate
								   {
									   #pragma warning disable CS0612 // 'name' is obsolete				
									   ConversionTable.LinearPosition.Convert(new Measurement(1.0, from), to);
									   #pragma warning restore CS0612
								   });

			// VERIFY
			Assert.That(ex.Message,
						Is.EqualTo("Cannot convert from ° to in."),
						"error message");
		}


		[Test]
		public void UnknownToUnit()
		{
			// SETUP
			var to = UnitOfMeasure.Degree;
			var from = UnitOfMeasure.Inch;

			// EXEC
			var ex = Assert.Throws(typeof(InvalidOperationException),
								   delegate
								   {
									   #pragma warning disable CS0612 // 'name' is obsolete				
									   ConversionTable.RotaryPosition.Convert(new Measurement(1.0, from), to);
									   #pragma warning restore CS0612
								   });

			// VERIFY
			Assert.That(ex.Message,
						Is.EqualTo("Cannot convert from in to °."),
						"error message");
		}


		[Test]
		public void UnknownConversion()
		{
			// SETUP
			var from = UnitOfMeasure.Degree;
			var to = UnitOfMeasure.Inch;

			// EXEC
			var ex = Assert.Throws(typeof(InvalidOperationException),
								   delegate { ConversionTable.Default.Convert(new Measurement(1.0, from), to); });

			// VERIFY
			Assert.That(ex.Message,
						Is.EqualTo("Cannot convert from ° to in."),
						"error message");
		}
	}
}

﻿using System;
using System.Threading;
using NUnit.Framework;
using TickingTest;
using Zaber;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class ConversationTopicTest
	{
		[Test]
		public void Cancel() => TestFramework.RunOnce(new CancelThreads());


		[Test]
		public void CompletedEvent()
		{
			// SETUP
			var topic = new ConversationTopic();
			var eventCount = 0;
			topic.Completed += delegate { eventCount++; };

			// EXEC
			topic.Response = new DeviceMessage();

			// VERIFY
			Assert.AreEqual(1,
							eventCount,
							"event count");
		}


		[Test]
		public void CompletedEventIsCalledOnceOnly()
		{
			// SETUP
			var topic = new ConversationTopic();
			var eventCount = 0;
			topic.Completed += delegate { eventCount++; };

			// EXEC
			topic.Response = new DeviceMessage();
			topic.Cancel();
			topic.Dispose();

			// VERIFY
			Assert.AreEqual(1,
							eventCount,
							"event count");
		}


		[Test]
		public void Dispose() => TestFramework.RunOnce(new DisposeThreads());


		[Test]
		public void IsComplete()
		{
			// SETUP
			var topic = new ConversationTopic();

			// EXEC
			var isCompleteBefore = topic.IsComplete;
			topic.Response = new DeviceMessage();
			var isCompleteAfter = topic.IsComplete;

			// VERIFY
			Assert.AreEqual(false,
							isCompleteBefore,
							"is complete before");
			Assert.AreEqual(true,
							isCompleteAfter,
							"is complete after");
		}


		[Test]
		public void Timeout() => TestFramework.RunOnce(new TimeoutThreads());


		[Test]
		public void UseWaitHandle() => TestFramework.RunOnce(new UseWaitHandleThreads());


		[Test]
		public void ValidateCancel()
		{
			// SETUP
			var topic = new ConversationTopic();
			topic.Cancel();

			// EXEC
			var isValid = topic.IsValid;
			string msg = null;
			try
			{
				topic.Validate();

				Assert.Fail("Should have thrown.");
			}
			catch (RequestCanceledException ex)
			{
				msg = ex.Message;
			}

			// VERIFY
			Assert.AreSame("Request was canceled.",
						   msg,
						   "error message");
			Assert.AreEqual(false,
							isValid,
							"is valid");
		}


		[Test]
		public void ValidateErrorResponse()
		{
			// SETUP
			var topic = new ConversationTopic();
			var expectedResponse = new DeviceMessage();
			expectedResponse.Command = Command.Error;
			topic.Response = expectedResponse;

			// EXEC
			var isValid = topic.IsValid;
			DeviceMessage actualResponse = null;
			try
			{
				topic.Validate();

				Assert.Fail("Should have thrown.");
			}
			catch (ErrorResponseException ex)
			{
				actualResponse = ex.Response;
			}

			// VERIFY
			Assert.AreSame(expectedResponse,
						   actualResponse,
						   "error response");
			Assert.AreEqual(false,
							isValid,
							"is valid");
		}


		[Test]
		public void ValidateIncomplete()
		{
			// SETUP
			var topic = new ConversationTopic();

			// EXEC
			var isValid = topic.IsValid;
			string msg = null;
			try
			{
				topic.Validate();

				Assert.Fail("Should have thrown.");
			}
			catch (InvalidOperationException ex)
			{
				msg = ex.Message;
			}

			// VERIFY
			Assert.AreEqual("Validate cannot be called until the request is complete.",
							msg,
							"error message");
			Assert.AreEqual(false,
							isValid,
							"is valid");
		}


		[Test]
		public void ValidateReplacementResponse()
		{
			// SETUP
			var topic = new ConversationTopic();
			var expectedResponse = new DeviceMessage();
			topic.ReplacementResponse = expectedResponse;

			// EXEC
			var isValid = topic.IsValid;
			DeviceMessage actualResponse = null;
			try
			{
				topic.Validate();

				Assert.Fail("Should have thrown.");
			}
			catch (RequestReplacedException ex)
			{
				actualResponse = ex.ReplacementResponse;
			}

			// VERIFY
			Assert.AreSame(expectedResponse,
						   actualResponse,
						   "error response");
			Assert.AreEqual(false,
							isValid,
							"is valid");
		}


		[Test]
		public void ValidateResponse()
		{
			// SETUP
			var topic = new ConversationTopic();
			topic.Response = new DeviceMessage();

			// EXEC
			var isValid = topic.IsValid;
			topic.Validate();

			// VERIFY
			Assert.AreEqual(true,
							isValid,
							"is valid");
		}


		[Test]
		public void ValidateTextErrorResponse()
		{
			// SETUP
			var topic = new ConversationTopic();
			var expectedResponse = new DeviceMessage("@22 0 RJ I IDLE --");
			topic.Response = expectedResponse;

			// EXEC
			var isValid = topic.IsValid;
			var ex = Assert.Throws<ErrorResponseException>(() => topic.Validate());

			// VERIFY
			Assert.That(ex.Response,
						Is.SameAs(expectedResponse),
						"error response");
			Assert.That(ex.Message,
						Is.EqualTo("Error response: '@22 0 RJ I IDLE --'."),
						"error message");
			Assert.AreEqual(false,
							isValid,
							"is valid");
		}


		[Test]
		public void ValidateZaberPortError()
		{
			// SETUP
			var topic = new ConversationTopic();
			var expectedPortError = ZaberPortError.Frame;
			topic.ZaberPortError = expectedPortError;

			// EXEC
			var isValid = topic.IsValid;
			var actualPortError = ZaberPortError.None;
			try
			{
				topic.Validate();

				Assert.Fail("Should have thrown.");
			}
			catch (ZaberPortErrorException ex)
			{
				actualPortError = ex.ErrorType;
			}

			// VERIFY
			Assert.AreEqual(expectedPortError,
							actualPortError,
							"error type");
			Assert.AreEqual(false,
							isValid,
							"is valid");
		}


		[Test]
		public void WaitForPortError() => TestFramework.RunOnce(new WaitForPortErrorThreads());


		[Test]
		public void WaitForReplacement() => TestFramework.RunOnce(new WaitForReplacementThreads());


		[Test]
		public void WaitForResponse() => TestFramework.RunOnce(new WaitForResponseThreads());


		private class WaitForResponseThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Request()
			{
				// SETUP
				using (topic = new ConversationTopic())
				{
					// EXEC
					var isComplete = topic.Wait();

					// VERIFY
					AssertTick(1);
					Assert.AreEqual(true,
									isComplete,
									"is complete");
				}
			}


			[TestThread]
			public void Response()
			{
				// SETUP
				WaitForTick(1);

				// EXEC
				topic.Response = new DeviceMessage();
			}

			#endregion

			#region -- Data --

			private ConversationTopic topic;

			#endregion
		}

		private class UseWaitHandleThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Request()
			{
				// SETUP
				topic1 = new ConversationTopic();
				topic2 = new ConversationTopic();
				var handles = new WaitHandle[2];
				handles[0] = topic1.WaitHandle;
				handles[1] = topic2.WaitHandle;


				// EXEC
				var isComplete = WaitHandle.WaitAll(handles);

				// VERIFY
				AssertTick(2);
				Assert.AreEqual(true,
								isComplete,
								"is complete");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				topic1.Response = new DeviceMessage();

				WaitForTick(2);

				topic2.Response = new DeviceMessage();
			}

			#endregion

			#region -- Data --

			private ConversationTopic topic1;
			private ConversationTopic topic2;

			#endregion
		}

		private class TimeoutThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Request()
			{
				// SETUP
				topic = new ConversationTopic();
				mockTimer = new MockTimeoutTimer();


				// EXEC
				var isComplete = topic.Wait(mockTimer);

				// VERIFY
				AssertTick(1);
				Assert.AreEqual(false,
								isComplete,
								"is complete");
			}


			[TestThread]
			public void Response()
			{
				// SETUP
				WaitForTick(1);

				// EXEC
				mockTimer.TimeoutNow();
			}

			#endregion

			#region -- Data --

			private ConversationTopic topic;
			private MockTimeoutTimer mockTimer;

			#endregion
		}

		private class CancelThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Request()
			{
				// SETUP
				topic = new ConversationTopic();

				// EXEC
				var isComplete = topic.Wait();

				// VERIFY
				AssertTick(1);
				Assert.AreEqual(true,
								isComplete,
								"is complete");
			}


			[TestThread]
			public void Response()
			{
				// SETUP
				WaitForTick(1);

				// EXEC
				topic.Cancel();
			}

			#endregion

			#region -- Data --

			private ConversationTopic topic;

			#endregion
		}

		private class DisposeThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Request()
			{
				// SETUP
				topic = new ConversationTopic();

				// EXEC
				var isComplete = topic.Wait();

				// VERIFY
				AssertTick(1);
				Assert.AreEqual(true,
								isComplete,
								"is complete");
			}


			[TestThread]
			public void Response()
			{
				// SETUP
				WaitForTick(1);

				// EXEC
				topic.Dispose();
			}

			#endregion

			#region -- Data --

			private ConversationTopic topic;

			#endregion
		}

		private class WaitForPortErrorThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Request()
			{
				// SETUP
				topic = new ConversationTopic();

				// EXEC
				var isComplete = topic.Wait();

				// VERIFY
				AssertTick(1);
				Assert.AreEqual(true,
								isComplete,
								"is complete");
			}


			[TestThread]
			public void Response()
			{
				// SETUP
				WaitForTick(1);

				// EXEC
				topic.ZaberPortError = ZaberPortError.PacketTimeout;
			}

			#endregion

			#region -- Data --

			private ConversationTopic topic;

			#endregion
		}

		private class WaitForReplacementThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Request()
			{
				// SETUP
				topic = new ConversationTopic();

				// EXEC
				var isComplete = topic.Wait();

				// VERIFY
				AssertTick(1);
				Assert.AreEqual(true,
								isComplete,
								"is complete");
			}


			[TestThread]
			public void Response()
			{
				// SETUP
				WaitForTick(1);

				// EXEC
				topic.ReplacementResponse = new DeviceMessage();
			}

			#endregion

			#region -- Data --

			private ConversationTopic topic;

			#endregion
		}
	}
}

﻿using NUnit.Framework;
using ZaberTest.Testing;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class PortTestsTcp : PortTestsBase
	{
		protected override IPortStub CreatePort() => new TcpPortStub();
	}
}

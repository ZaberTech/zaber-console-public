﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Zaber;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class UnitOfMeasureTests
	{
		// Catch changes to static unit definitions across versions.
		[Test]
		public void UnitDefinitionRegressionTest()
		{
			foreach (var pair in _allUnits)
			{
				var symbol = pair.Key;
				var unit = pair.Value.Item1;
				var measurementType = pair.Value.Item2;
				var motionType = pair.Value.Item3;
				var derivative = pair.Value.Item4;

				Assert.AreEqual(symbol, unit.Abbreviation);
				Assert.AreEqual(unit,
								UnitOfMeasure.FindByAbbreviation(symbol),
								$"Lookup by abbreviation failed for {symbol}.");
				Assert.AreEqual(measurementType, unit.MeasurementType, $"Measurement type for {symbol} is wrong.");
				Assert.AreEqual(motionType, unit.MotionType, $"Motion type for {symbol} is wrong.");
				Assert.AreEqual(derivative, unit.Derivative, $"Derivative for {symbol} is not as expected.");
			}
		}


		private readonly Dictionary<string, Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>> _allUnits
			= new Dictionary<string, Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>>
			{
				{
					"-- data --",
					new Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>(
						UnitOfMeasure.Data,
						MeasurementType.Other,
						MotionType.Other,
						null)
				},
				{
					"µstep/s²",
					new Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>(
						UnitOfMeasure.MicrostepsPerSecondSquared,
						MeasurementType.Acceleration,
						MotionType.Other,
						null)
				},
				{
					"µm/s²",
					new Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>(
						UnitOfMeasure.MicrometersPerSecondSquared,
						MeasurementType.Acceleration,
						MotionType.Linear,
						null)
				},
				{
					"mm/s²",
					new Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>(
						UnitOfMeasure.MillimetersPerSecondSquared,
						MeasurementType.Acceleration,
						MotionType.Linear,
						null)
				},
				{
					"m/s²",
					new Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>(
						UnitOfMeasure.MetersPerSecondSquared,
						MeasurementType.Acceleration,
						MotionType.Linear,
						null)
				},
				{
					"in/s²",
					new Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>(
						UnitOfMeasure.InchesPerSecondSquared,
						MeasurementType.Acceleration,
						MotionType.Linear,
						null)
				},
				{
					"°/s²",
					new Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>(
						UnitOfMeasure.DegreesPerSecondSquared,
						MeasurementType.Acceleration,
						MotionType.Rotary,
						null)
				},
				{
					"rad/s²",
					new Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>(
						UnitOfMeasure.RadiansPerSecondSquared,
						MeasurementType.Acceleration,
						MotionType.Rotary,
						null)
				},
				{
					"µstep/s",
					new Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>(
						UnitOfMeasure.MicrostepsPerSecond,
						MeasurementType.Velocity,
						MotionType.Other,
						UnitOfMeasure.MicrostepsPerSecondSquared)
				},
				{
					"µm/s",
					new Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>(
						UnitOfMeasure.MicrometersPerSecond,
						MeasurementType.Velocity,
						MotionType.Linear,
						UnitOfMeasure.MicrometersPerSecondSquared)
				},
				{
					"mm/s",
					new Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>(
						UnitOfMeasure.MillimetersPerSecond,
						MeasurementType.Velocity,
						MotionType.Linear,
						UnitOfMeasure.MillimetersPerSecondSquared)
				},
				{
					"m/s",
					new Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>(
						UnitOfMeasure.MetersPerSecond,
						MeasurementType.Velocity,
						MotionType.Linear,
						UnitOfMeasure.MetersPerSecondSquared)
				},
				{
					"in/s",
					new Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>(
						UnitOfMeasure.InchesPerSecond,
						MeasurementType.Velocity,
						MotionType.Linear,
						UnitOfMeasure.InchesPerSecondSquared)
				},
				{
					"°/s",
					new Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>(
						UnitOfMeasure.DegreesPerSecond,
						MeasurementType.Velocity,
						MotionType.Rotary,
						UnitOfMeasure.DegreesPerSecondSquared)
				},
				{
					"rad/s",
					new Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>(
						UnitOfMeasure.RadiansPerSecond,
						MeasurementType.Velocity,
						MotionType.Rotary,
						UnitOfMeasure.RadiansPerSecondSquared)
				},
				{
					"RPM",
					new Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>(
						UnitOfMeasure.RevolutionsPerMinute,
						MeasurementType.Velocity,
						MotionType.Rotary,
						null)
				},
				{
					"µm",
					new Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>(
						UnitOfMeasure.Micrometer,
						MeasurementType.Position,
						MotionType.Linear,
						UnitOfMeasure.MicrometersPerSecond)
				},
				{
					"mm",
					new Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>(
						UnitOfMeasure.Millimeter,
						MeasurementType.Position,
						MotionType.Linear,
						UnitOfMeasure.MillimetersPerSecond)
				},
				{
					"m",
					new Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>(
						UnitOfMeasure.Meter,
						MeasurementType.Position,
						MotionType.Linear,
						UnitOfMeasure.MetersPerSecond)
				},
				{
					"in",
					new Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>(
						UnitOfMeasure.Inch,
						MeasurementType.Position,
						MotionType.Linear,
						UnitOfMeasure.InchesPerSecond)
				},
				{
					"°",
					new Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>(
						UnitOfMeasure.Degree,
						MeasurementType.Position,
						MotionType.Rotary,
						UnitOfMeasure.DegreesPerSecond)
				},
				{
					"rad",
					new Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>(
						UnitOfMeasure.Radian,
						MeasurementType.Position,
						MotionType.Rotary,
						UnitOfMeasure.RadiansPerSecond)
				},
				{
					"rev",
					new Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>(
						UnitOfMeasure.Revolution,
						MeasurementType.Position,
						MotionType.Rotary,
						UnitOfMeasure.RevolutionsPerMinute)
				},
				{
					"A",
					new Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>(
						UnitOfMeasure.Amperes,
						MeasurementType.Current,
						MotionType.None,
						null)
				},
				{
					"mA",
					new Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>(
						UnitOfMeasure.Milliamperes,
						MeasurementType.Current,
						MotionType.None,
						null)
				},
				{
					"%",
					new Tuple<UnitOfMeasure, MeasurementType, MotionType, UnitOfMeasure>(
						UnitOfMeasure.Percent,
						MeasurementType.Current,
						MotionType.None,
						null)
				}
			};
	}
}

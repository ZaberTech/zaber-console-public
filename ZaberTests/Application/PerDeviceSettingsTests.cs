﻿using NUnit.Framework;
using Zaber;
using Zaber.Application;

namespace ZaberTest.Application
{
	[TestFixture]
	[SetCulture("en-US")]
	public class PerDeviceSettingsTests
	{
		// Just tests that the DeviceIdentity property is read/write and that the
		// constructor sets it to null.
		[Test]
		public void TestIdentityAssignable()
		{
			var settings = new ConcreteDeviceSettings();
			Assert.IsNull(settings.DeviceIdentity);
			var id = new DeviceIdentity();
			settings.DeviceIdentity = id;
			Assert.IsTrue(ReferenceEquals(id, settings.DeviceIdentity));
			settings.DeviceIdentity = null;
			Assert.IsNull(settings.DeviceIdentity);
		}


		// Make a non-abstract subclass that adds nothing so we can test things.
		private class ConcreteDeviceSettings : PerDeviceSettings
		{
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Zaber.Application;

namespace ZaberTest.Application
{
	[TestFixture]
	[SetCulture("en-US")]
	public class IEnumerableExtensionsTests
	{
		[Test]
		public void JoinWithPunctuation()
		{
			var list = new List<int>();
			Assert.AreEqual(string.Empty, list.JoinWithPunctuation());

			list.Add(1);
			Assert.AreEqual("1", list.JoinWithPunctuation());

			list.Add(2);
			Assert.AreEqual("1 and 2", list.JoinWithPunctuation());

			list.Add(3);
			Assert.AreEqual("1, 2 and 3", list.JoinWithPunctuation());

			list.Add(4);
			Assert.AreEqual("1, 2, 3 or 4", list.JoinWithPunctuation("or"));
		}
	}
}

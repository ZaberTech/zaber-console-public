﻿using NUnit.Framework;
using Zaber;
using Zaber.Application;

namespace ZaberTest.Application
{
	[TestFixture]
	[SetCulture("en-US")]
	public class PerDeviceSettingsCollectionTests
	{
		[SetUp]
		public void Setup()
		{
			_allDevicesId = new DeviceIdentity { IsAllDevices = true };

			_hasSerialId = new DeviceIdentity { DeviceSerialNumber = 12345 };

			_isPeripheralId = new DeviceIdentity { PeripheralId = 12345 };

			_hasDeviceIdId = new DeviceIdentity { DeviceId = 12345 };

			_hasNumberId = new DeviceIdentity
			{
				AxisNumber = 1,
				DeviceNumber = 1
			};

			_settings = new ConcreteDeviceSettingsCollection();
		}


		[Test]
		public void TestClear()
		{
			AddIdsForward();
			Assert.AreEqual(5, _settings.PerDeviceSettings.Count);
			_settings.Clear();
			Assert.AreEqual(0, _settings.PerDeviceSettings.Count);
		}


		[Test]
		public void TestRetrievalAutomaticSpecificity_Forward()
		{
			AddIdsForward();
			TestRetrievalAutomaticSpecificity();
		}


		[Test]
		public void TestRetrievalAutomaticSpecificity_Reverse()
		{
			AddIdsReverse();
			TestRetrievalAutomaticSpecificity();
		}


		[Test]
		public void TestRetrievalManualSpecificity_ByDeviceNumber()
		{
			var controllerId = new DeviceIdentity
			{
				AxisNumber = 0,
				DeviceNumber = 1
			};

			var axis1Id = new DeviceIdentity
			{
				AxisNumber = 1,
				DeviceNumber = 1
			};

			var axis2Id = new DeviceIdentity
			{
				AxisNumber = 2,
				DeviceNumber = 1
			};

			var settings = new ConcreteDeviceSettingsCollection();
			settings.PerDeviceSettings.Add(new ConcreteDeviceSettings { DeviceIdentity = controllerId });
			settings.PerDeviceSettings.Add(new ConcreteDeviceSettings { DeviceIdentity = axis1Id });
			settings.PerDeviceSettings.Add(new ConcreteDeviceSettings { DeviceIdentity = axis2Id });

			var result =
				settings.FindOrCreateSettings(controllerId,
											  DeviceIdentityMask.DeviceNumber | DeviceIdentityMask.AxisNumber);
			Assert.IsTrue(ReferenceEquals(controllerId, result.DeviceIdentity));

			result = settings.FindOrCreateSettings(axis1Id,
												   DeviceIdentityMask.DeviceNumber | DeviceIdentityMask.AxisNumber);
			Assert.IsTrue(ReferenceEquals(axis1Id, result.DeviceIdentity));

			result = settings.FindOrCreateSettings(axis2Id,
												   DeviceIdentityMask.DeviceNumber | DeviceIdentityMask.AxisNumber);
			Assert.IsTrue(ReferenceEquals(axis2Id, result.DeviceIdentity));

			Assert.AreEqual(3, settings.PerDeviceSettings.Count);
		}


		[Test]
		public void TestRetrievalManualSpecificity_ByTypeAndSerial()
		{
			var deviceId1 = new DeviceIdentity { DeviceId = 12345 };

			var deviceId2 = new DeviceIdentity
			{
				DeviceId = 12345,
				DeviceSerialNumber = 23456
			};

			var settings = new ConcreteDeviceSettingsCollection();
			settings.PerDeviceSettings.Add(new ConcreteDeviceSettings { DeviceIdentity = deviceId1 });
			settings.PerDeviceSettings.Add(new ConcreteDeviceSettings { DeviceIdentity = deviceId2 });

			var result =
				settings.FindOrCreateSettings(deviceId1, DeviceIdentityMask.SerialNumber | DeviceIdentityMask.DeviceId);
			Assert.IsTrue(ReferenceEquals(deviceId1, result.DeviceIdentity));

			result = settings.FindOrCreateSettings(deviceId1,
												   DeviceIdentityMask.SerialNumber | DeviceIdentityMask.DeviceId);
			Assert.IsTrue(ReferenceEquals(deviceId1, result.DeviceIdentity));

			result = settings.FindOrCreateSettings(deviceId2,
												   DeviceIdentityMask.SerialNumber | DeviceIdentityMask.DeviceId);
			Assert.IsTrue(ReferenceEquals(deviceId2, result.DeviceIdentity));

			Assert.AreEqual(2, settings.PerDeviceSettings.Count);

			result = settings.FindOrCreateSettings(deviceId2,
												   DeviceIdentityMask.SerialNumber | DeviceIdentityMask.DeviceId);
			Assert.IsTrue(ReferenceEquals(deviceId2, result.DeviceIdentity));

			Assert.AreEqual(2, settings.PerDeviceSettings.Count);
		}


		// Make non-abstract subclasses that add nothing so we can test things.
		private class ConcreteDeviceSettings : PerDeviceSettings
		{
		}

		private class ConcreteDeviceSettingsCollection : PerDeviceSettingsCollection<ConcreteDeviceSettings>
		{
		}


		private void TestRetrievalAutomaticSpecificity()
		{
			Assert.IsNotNull(_settings.PerDeviceSettings);
			var count = _settings.PerDeviceSettings.Count;
			Assert.AreEqual(5, _settings.PerDeviceSettings.Count);

			var id = new DeviceIdentity();
			id.IsAllDevices = true;
			var result = _settings.FindOrCreateSettings(id);
			Assert.AreEqual(count, _settings.PerDeviceSettings.Count);
			Assert.IsTrue(ReferenceEquals(_allDevicesId, result.DeviceIdentity));

			id.IsAllDevices = false;
			id.DeviceSerialNumber = 12345;
			result = _settings.FindOrCreateSettings(id);
			Assert.AreEqual(count, _settings.PerDeviceSettings.Count);
			Assert.IsTrue(ReferenceEquals(_hasSerialId, result.DeviceIdentity));

			id.DeviceSerialNumber = null;
			id.PeripheralId = 12345;
			result = _settings.FindOrCreateSettings(id);
			Assert.AreEqual(count, _settings.PerDeviceSettings.Count);
			Assert.IsTrue(ReferenceEquals(_isPeripheralId, result.DeviceIdentity));

			id.PeripheralId = null;
			id.DeviceId = 12345;
			result = _settings.FindOrCreateSettings(id);
			Assert.AreEqual(count, _settings.PerDeviceSettings.Count);
			Assert.IsTrue(ReferenceEquals(_hasDeviceIdId, result.DeviceIdentity));

			id.DeviceId = 0;
			id.AxisNumber = 1;
			id.DeviceNumber = 1;
			result = _settings.FindOrCreateSettings(id);
			Assert.AreEqual(count, _settings.PerDeviceSettings.Count);
			Assert.IsTrue(ReferenceEquals(_hasNumberId, result.DeviceIdentity));

			id.AxisNumber = 2;
			result = _settings.FindOrCreateSettings(id);
			Assert.AreEqual(count + 1, _settings.PerDeviceSettings.Count);
		}


		private void AddIdsForward()
		{
			_settings.PerDeviceSettings.Add(new ConcreteDeviceSettings { DeviceIdentity = _allDevicesId });
			_settings.PerDeviceSettings.Add(new ConcreteDeviceSettings { DeviceIdentity = _hasSerialId });
			_settings.PerDeviceSettings.Add(new ConcreteDeviceSettings { DeviceIdentity = _isPeripheralId });
			_settings.PerDeviceSettings.Add(new ConcreteDeviceSettings { DeviceIdentity = _hasDeviceIdId });
			_settings.PerDeviceSettings.Add(new ConcreteDeviceSettings { DeviceIdentity = _hasNumberId });
		}


		private void AddIdsReverse()
		{
			_settings.PerDeviceSettings.Add(new ConcreteDeviceSettings { DeviceIdentity = _hasNumberId });
			_settings.PerDeviceSettings.Add(new ConcreteDeviceSettings { DeviceIdentity = _hasDeviceIdId });
			_settings.PerDeviceSettings.Add(new ConcreteDeviceSettings { DeviceIdentity = _isPeripheralId });
			_settings.PerDeviceSettings.Add(new ConcreteDeviceSettings { DeviceIdentity = _hasSerialId });
			_settings.PerDeviceSettings.Add(new ConcreteDeviceSettings { DeviceIdentity = _allDevicesId });
		}


		private void TestForwardData(ConcreteDeviceSettingsCollection aSettings)
		{
			Assert.IsNotNull(aSettings.PerDeviceSettings);
			var count = aSettings.PerDeviceSettings.Count;
			Assert.AreEqual(5, aSettings.PerDeviceSettings.Count);

			var id = new DeviceIdentity();
			id.IsAllDevices = true;
			var result = aSettings.FindOrCreateSettings(id);
			Assert.AreEqual(count, aSettings.PerDeviceSettings.Count);
			Assert.AreEqual(_allDevicesId, result.DeviceIdentity);

			id.IsAllDevices = false;
			id.DeviceSerialNumber = 12345;
			result = aSettings.FindOrCreateSettings(id);
			Assert.AreEqual(count, aSettings.PerDeviceSettings.Count);
			Assert.AreEqual(_hasSerialId, result.DeviceIdentity);

			id.DeviceSerialNumber = null;
			id.PeripheralId = 12345;
			result = aSettings.FindOrCreateSettings(id);
			Assert.AreEqual(count, aSettings.PerDeviceSettings.Count);
			Assert.AreEqual(_isPeripheralId, result.DeviceIdentity);

			id.PeripheralId = null;
			id.DeviceId = 12345;
			result = aSettings.FindOrCreateSettings(id);
			Assert.AreEqual(count, aSettings.PerDeviceSettings.Count);
			Assert.AreEqual(_hasDeviceIdId, result.DeviceIdentity);

			id.DeviceId = 0;
			id.AxisNumber = 1;
			id.DeviceNumber = 1;
			result = aSettings.FindOrCreateSettings(id);
			Assert.AreEqual(count, aSettings.PerDeviceSettings.Count);
			Assert.AreEqual(_hasNumberId, result.DeviceIdentity);
		}


		private DeviceIdentity _allDevicesId;
		private DeviceIdentity _hasSerialId;
		private DeviceIdentity _isPeripheralId;
		private DeviceIdentity _hasDeviceIdId;
		private DeviceIdentity _hasNumberId;
		private ConcreteDeviceSettingsCollection _settings;
	}
}

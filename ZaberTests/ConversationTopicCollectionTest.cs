﻿using System;
using NUnit.Framework;
using TickingTest;
using Zaber;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class ConversationTopicCollectionTest :
		ListTest<ConversationTopicCollection, ConversationTopic>
	{
		[Test]
		public void Cancel()
		{
			// SETUP
			var topic1 = new ConversationTopic();
			var topic2 = new ConversationTopic();
			var collection = new ConversationTopicCollection();
			collection.Add(topic1);
			collection.Add(topic2);

			// EXEC
			var wasCanceled = collection.IsCanceled;
			collection.Cancel();

			// VERIFY
			Assert.That(wasCanceled,
						Is.False,
						"was canceled");
			Assert.That(topic1.IsCanceled,
						Is.True,
						"topic 1 canceled");
			Assert.That(topic2.IsCanceled,
						Is.True,
						"topic 2 canceled");
			Assert.That(collection.IsCanceled,
						Is.True,
						"collection canceled");
		}


		[Test]
		public void Collection()
		{
			// SETUP
			var topic1 = new ConversationTopic();
			var topic2 = new ConversationTopic();
			var collection = new ConversationTopicCollection();

			// EXEC
			collection.Add(topic1);
			collection.Add(topic2);

			var contains1BeforeRemove = collection.Contains(topic1);

			collection.Remove(topic1);
			var contains1AfterRemove = collection.Contains(topic1);

			// VERIFY
			Assert.AreEqual(true,
							contains1BeforeRemove,
							"contains topic 1 before remove");
			Assert.AreEqual(false,
							contains1AfterRemove,
							"contains topic 1 after remove");
		}


		[Test]
		public void PortErrorReadOnly()
		{
			// SETUP
			var topic1 = new ConversationTopic();
			var topic2 = new ConversationTopic();
			var collection = new ConversationTopicCollection();
			collection.Add(topic1);
			collection.Add(topic2);

			// EXEC
			string msg = null;
			try
			{
				collection.ZaberPortError = ZaberPortError.PacketTimeout;

				Assert.Fail("Should have thrown");
			}
			catch (NotSupportedException ex)
			{
				msg = ex.Message;
			}

			// VERIFY
			Assert.AreEqual("Response properties are read-only in a ConversationTopicCollection.",
							msg,
							"error message");
		}


		[Test]
		public void PropertyGetIsDelegated()
		{
			// SETUP
			var topic1 = new ConversationTopic();
			var topic2 = new ConversationTopic();
			var collection = new ConversationTopicCollection();
			collection.Add(topic1);
			collection.Add(topic2);
			var expectedResponse = new DeviceMessage();

			// EXEC
			topic1.Response = expectedResponse;
			topic2.ZaberPortError = ZaberPortError.PacketTimeout;

			var collectionResponse = collection.Response;
			var collectionPortError = collection.ZaberPortError;

			// VERIFY
			Assert.AreEqual(expectedResponse,
							collectionResponse,
							"collection response");
			Assert.AreEqual(ZaberPortError.None,
							collectionPortError,
							"collection port error");
		}


		[Test]
		public void PropertyGetNotSupportedWhenEmpty()
		{
			// SETUP
			var collection = new ConversationTopicCollection();

			// EXEC
			string msg = null;
			try
			{
				var response = collection.Response;

				Assert.Fail("Should have thrown");
			}
			catch (InvalidOperationException ex)
			{
				msg = ex.Message;
			}

			// VERIFY
			Assert.IsTrue(msg.Contains("undefined for an empty collection"),
						  "error message");
		}


		[Test]
		public void ReplacementReadOnly()
		{
			// SETUP
			var topic1 = new ConversationTopic();
			var topic2 = new ConversationTopic();
			var collection = new ConversationTopicCollection();
			collection.Add(topic1);
			collection.Add(topic2);

			// EXEC
			string msg = null;
			try
			{
				collection.ReplacementResponse = new DeviceMessage();

				Assert.Fail("Should have thrown");
			}
			catch (NotSupportedException ex)
			{
				msg = ex.Message;
			}

			// VERIFY
			Assert.AreEqual("Response properties are read-only in a ConversationTopicCollection.",
							msg,
							"error message");
		}


		[Test]
		public void ReplacementResponse()
		{
			// SETUP
			var topic1 = new ConversationTopic();
			var topic2 = new ConversationTopic();
			topic1.ReplacementResponse = new DeviceMessage();
			var collection = new ConversationTopicCollection();
			collection.Add(topic1);
			collection.Add(topic2);

			// EXEC
			var replacement = collection.ReplacementResponse;

			// VERIFY
			Assert.That(replacement,
						Is.SameAs(topic1.ReplacementResponse));
		}


		[Test]
		public void ResponseReadOnly()
		{
			// SETUP
			var topic1 = new ConversationTopic();
			var topic2 = new ConversationTopic();
			var collection = new ConversationTopicCollection();
			collection.Add(topic1);
			collection.Add(topic2);

			// EXEC
			string msg = null;
			try
			{
				collection.Response = new DeviceMessage();

				Assert.Fail("Should have thrown");
			}
			catch (NotSupportedException ex)
			{
				msg = ex.Message;
			}

			// VERIFY
			Assert.AreEqual("Response properties are read-only in a ConversationTopicCollection.",
							msg,
							"error message");
		}


		[Test]
		public void ValidateFails()
		{
			// SETUP
			var topic1 = new ConversationTopic();
			var topic2 = new ConversationTopic();
			var collection = new ConversationTopicCollection();
			collection.Add(topic1);
			collection.Add(topic2);

			// EXEC
			topic1.Response = new DeviceMessage(); // success
			var replacementResponse = new DeviceMessage();
			replacementResponse.DeviceNumber = 2;
			topic2.ReplacementResponse = replacementResponse; // failed

			var isValid = collection.IsValid;
			string msg = null;
			try
			{
				collection.Validate();

				Assert.Fail("Should have thrown");
			}
			catch (RequestCollectionException ex)
			{
				msg = ex.Message;
			}

			// VERIFY
			Assert.AreEqual("Some requests failed: request replaced on device 2.",
							msg,
							"error message");
			Assert.AreEqual(false,
							isValid,
							"is valid");
		}


		[Test]
		public void WaitForEmptyCollection()
		{
			// SETUP
			var collection =
				new ConversationTopicCollection();

			// EXEC
			var isSuccessful = collection.Wait();

			// VERIFY
			Assert.AreEqual(true,
							isSuccessful,
							"isSuccessful");
		}


		[Test]
		public void WaitForEmptyCollectionWithTimer()
		{
			// SETUP
			var collection =
				new ConversationTopicCollection();

			// EXEC
			var isSuccessful = collection.Wait(new MockTimeoutTimer());

			// VERIFY
			Assert.AreEqual(true,
							isSuccessful,
							"isSuccessful");
		}


		[Test]
		public void WaitForResponses() => TestFramework.RunOnce(new WaitForResponsesThreads());


		private class WaitForResponsesThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Request()
			{
				// SETUP
				collection = new ConversationTopicCollection();
				topic1 = new ConversationTopic();
				topic2 = new ConversationTopic();
				collection.Add(topic1);
				collection.Add(topic2);

				// EXEC
				var isComplete = collection.Wait();

				// VERIFY
				AssertTick(2);
				Assert.AreEqual(true,
								isComplete,
								"is complete");
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				topic1.Response = new DeviceMessage();

				WaitForTick(2);

				topic2.Response = new DeviceMessage();
			}

			#endregion

			#region -- Data --

			private ConversationTopicCollection collection;
			private ConversationTopic topic1;
			private ConversationTopic topic2;

			#endregion
		}


		protected override ConversationTopicCollection CreateList()
		{
			var list = new ConversationTopicCollection();
			list.Add(new ConversationTopic());

			return list;
		}
	}
}

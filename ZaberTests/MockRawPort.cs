﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using NUnit.Framework;
using Zaber.Ports;

namespace ZaberTest
{
	public class MockRawPort : IRawPort
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Expect a binary packet.
		/// </summary>
		/// <param name="aPacket">Bytes to expect to be sent.</param>
		public void Expect(byte[] aPacket)
		{
			var expectation = new Expectation { ExpectedRequest = aPacket };

			_expectations.AddLast(expectation);
			Log.DebugFormat("expect {0}", FormatBytes(aPacket));
		}


		/// <summary>
		///     Expect an ASCII message to be sent.
		/// </summary>
		/// <param name="aMessage">Message that will be sent.</param>
		public void Expect(string aMessage) => Expect(Encoding.ASCII.GetBytes(aMessage));


		/// <summary>
		///     Add a response to be received after the last-added expectation is fulfilled.
		/// </summary>
		/// <param name="aPacket">Bytes that will be received.</param>
		public void AddResponse(byte[] aPacket)
			=> _expectations.Last.Value.Responses.Add(new Response { Packet = aPacket });


		/// <summary>
		///     Add a response to be received after the last-added expectation is fulfilled.
		/// </summary>
		/// <param name="aMessage">Message that will be received.</param>
		public void AddResponse(string aMessage) => AddResponse(Encoding.ASCII.GetBytes(aMessage));


		/// <summary>
		///     Add an exception to be raised in the write call when the last-added expectation is fulfilled.
		/// </summary>
		/// <param name="aException">Exception to throw.</param>
		public void AddWriteException(Exception aException) => _expectations.Last.Value.WriteException = aException;


		/// <summary>
		///     Add an exception to be raised in the next read call when the last-added expectation is fulfilled.
		/// </summary>
		/// <param name="aException">Exception to throw.</param>
		public void AddReadException(Exception aException)
			=> _expectations.Last.Value.Responses.Add(new Response { Exception = aException });


		/// <summary>
		///     Verify that all outstanding expectations have been fulfilled.
		/// </summary>
		public void Verify()
		{
			if (_expectations.Count > 0)
			{
				var sb = new StringBuilder();
				sb.AppendLine("Unmatched mock port expectations:");
				foreach (var expectation in _expectations)
				{
					if (null == expectation.ExpectedRequest)
					{
						sb.AppendLine("Read()");
					}
					else
					{
						sb.AppendLine("Write(" + FormatBytes(expectation.ExpectedRequest) + ")");
					}
				}

				Assert.Fail(sb.ToString());
			}

			Assert.That(ActualInvalidPacketCount,
						Is.EqualTo(ExpectedInvalidPacketCount),
						"invalid packet count");
		}


		public void Open()
		{
			if (_isOpen)
			{
				throw new InvalidOperationException("Port opened twice.");
			}

			_isOpen = true;
		}


		public void Close()
		{
			if (!_isOpen)
			{
				throw new InvalidOperationException("Port closed twice.");
			}

			_isOpen = false;
		}


		public void Write(byte[] aData) => Send(aData);

		public void Write(string aData) => Send(Encoding.ASCII.GetBytes(aData));


		public byte[] Read(int aCount)
		{
			Assert.IsTrue(_readResponses.Any(), "Unexpected read call.");
			var response = _readResponses[0];
			if (null != response.Exception)
			{
				_readResponses.RemoveAt(0);
				throw response.Exception;
			}

			Assert.IsNotNull(response.Packet, "No response data to return.");
			Assert.LessOrEqual(aCount,
							   response.Packet.Length,
							   "Response data quantity insufficient to match read request.");
			if (aCount == response.Packet.Length)
			{
				_readResponses.RemoveAt(0);
				return response.Packet;
			}

			var result = response.Packet.Take(aCount).ToArray();
			response.Packet = response.Packet.Skip(aCount).ToArray();
			return result;
		}


		public string ReadLine()
		{
			Assert.IsTrue(_readResponses.Any(), "Unexpected read call.");
			var response = _readResponses[0];
			_readResponses.RemoveAt(0);
			if (null != response.Exception)
			{
				throw response.Exception;
			}

			Assert.IsNotNull(response.Packet, "No response data to return.");
			return Encoding.ASCII.GetString(response.Packet);
		}


		public void ClearReceiveBuffer() => _readResponses.Clear();


		public void ClearTransmitBuffer()
		{
		}


		public IEnumerable<int> GetApplicableBaudRates() => SupportedBaudRates;

		public void Dispose() => Close();

		public int ExpectedInvalidPacketCount { get; set; }

		public int ActualInvalidPacketCount { get; private set; }

		public IEnumerable<int> SupportedBaudRates { get; set; } = RawRS232Port.SupportedSerialBaudRates;


		public int BaudRate { get; set; }

		public int ReadTimeout { get; set; } = int.MaxValue;

		#endregion

		#region -- Non-Public Methods --

		private void Send(byte[] aActualRequest)
		{
			if (0 == _expectations.Count)
			{
				Assert.Fail("Unexpected data packet: {0}", FormatBytes(aActualRequest));
			}

			var expectation = _expectations.First.Value;
			_expectations.RemoveFirst();

			Log.DebugFormat("sending {0}", FormatBytes(aActualRequest));

			var expectedCall =
				expectation.ExpectedRequest == null
					? "Read()"
					: FormatBytes(expectation.ExpectedRequest);
			var actualCall = FormatBytes(aActualRequest);

			Assert.AreEqual(expectedCall, actualCall, "port call");

			if (null != expectation.WriteException)
			{
				throw expectation.WriteException;
			}

			foreach (var response in expectation.Responses)
			{
				_readResponses.Add(response);
			}
		}


		private string FormatBytes(byte[] aPacket)
			=> "[" + string.Join(", ", aPacket.Select(b => string.Format("0x{0:X2}", b))) + "]";

		#endregion

		#region -- Data --

		private class Response
		{
			#region -- Public Methods & Properties --

			public byte[] Packet { get; set; }

			public Exception Exception { get; set; }

			#endregion
		}


		private class Expectation
		{
			#region -- Public Methods & Properties --

			public Expectation()
			{
				Responses = new List<Response>();
			}


			public byte[] ExpectedRequest { get; set; }

			public List<Response> Responses { get; }

			public Exception WriteException { get; set; }

			#endregion
		}


		private static readonly ILog Log = LogManager.GetLogger(typeof(MockRawPort));

		private readonly LinkedList<Expectation> _expectations = new LinkedList<Expectation>();
		private bool _isOpen;
		private readonly List<Response> _readResponses = new List<Response>();

		#endregion
	}
}

﻿using System.Linq;
using NUnit.Framework;
using Zaber;

namespace ZaberTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class SortOrderAttributeTests
	{
		[Test]
		public void TestSortByInstance()
		{
			var unsorted = new object[] { new Test4(), new Test2(), new Test1(), new Test3(), new Test1() };

			var sorted = SortOrderAttribute.SortInstances(unsorted).ToArray();

			Assert.AreEqual(5, sorted.Length);
			Assert.IsInstanceOf<Test1>(sorted[0]);
			Assert.IsInstanceOf<Test1>(sorted[1]);
			Assert.IsInstanceOf<Test2>(sorted[2]);
			Assert.IsInstanceOf<Test3>(sorted[3]);
			Assert.IsInstanceOf<Test4>(sorted[4]);
		}


		[Test]
		public void TestSortByType()
		{
			var unsorted = new[] { typeof(Test4), typeof(Test2), typeof(Test1), typeof(Test3), typeof(Test1) };

			var sorted = SortOrderAttribute.SortTypes(unsorted).ToArray();

			Assert.AreEqual(5, sorted.Length);
			Assert.AreEqual(typeof(Test1), sorted[0]);
			Assert.AreEqual(typeof(Test1), sorted[1]);
			Assert.AreEqual(typeof(Test2), sorted[2]);
			Assert.AreEqual(typeof(Test3), sorted[3]);
			Assert.AreEqual(typeof(Test4), sorted[4]);
		}


		[SortOrder(-1.5)]
		private class Test1
		{
		}

		private class Test2
		{
		}

		[SortOrder(1.0)]
		private class Test3
		{
		}

		[SortOrder(1000.0)]
		private class Test4
		{
		}
	}
}

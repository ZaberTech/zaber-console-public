﻿using System;
using System.Text;
using System.Threading;
using NUnit.Framework;
using Zaber;
using ZaberTest.Testing;

namespace ZaberTest
{
	public abstract class PortTestsBase
	{
		#region -- Public Methods & Properties --

		[Test]
		public void Send()
		{
			byte deviceNumber = 1;
			var command = Command.EchoData;
			var data = 42;

			var port = CreatePort();

			var expectedBytes = new byte[] { deviceNumber, (byte) command, (byte) data, 0, 0, 0 };
			var expectedMeasurement = new Measurement(data, UnitOfMeasure.Data);

			var packetSent = CatchPacketSentFrom(port,
												 delegate { port.Send(deviceNumber, command, data); });

			Assert.That(port.SentBytes,
						Is.EquivalentTo(expectedBytes));

			Assert.AreEqual(deviceNumber, packetSent.DeviceNumber);
			Assert.AreEqual(command, packetSent.Command);
			Assert.AreEqual(data, packetSent.NumericData);
			Assert.AreEqual(expectedMeasurement, packetSent.Measurement);
		}


		[Test]
		public void SendBinaryInAsciiMode()
		{
			byte deviceNumber = 1;
			var command = Command.EchoData;
			var data = 42;

			var port = CreatePort();
			port.IsAsciiMode = true;

			var ex = Assert.Throws(typeof(InvalidOperationException),
								   delegate { port.Send(deviceNumber, command, data); });

			Assert.AreEqual("Binary commands cannot be sent when the port is in ASCII mode.",
							ex.Message);
		}


		[Test]
		public void SendText()
		{
			var port = CreatePort();
			port.IsAsciiMode = true;

			var message = "/01 move abs 0";
			var expectedBytes = Encoding.ASCII.GetBytes(message + "\r\n");

			var packetSent = CatchPacketSentFrom(port,
												 delegate { port.Send(message); });

			Assert.IsNotNull(packetSent);
			Assert.AreEqual(message, packetSent.Text);
			Assert.That(port.SentBytes, Is.EquivalentTo(expectedBytes));
			Assert.IsNull(packetSent.Measurement);
		}


		[Test]
		public void SendTextMeasurement()
		{
			var port = CreatePort();
			port.IsAsciiMode = true;

			var message = "/01 move abs 4200";
			var expectedBytes = Encoding.ASCII.GetBytes(message + "\r\n");
			var measurement = new Measurement(4.2, UnitOfMeasure.Millimeter);

			var packetSent = CatchPacketSentFrom(port,
												 delegate { port.Send(message, measurement); });

			Assert.AreEqual(measurement, packetSent.Measurement);
		}


		[Test]
		public void SendTextInBinaryMode()
		{
			var port = CreatePort();
			port.IsAsciiMode = false;

			var message = "/01 move abs 0";
			var expectedBytes = Encoding.ASCII.GetBytes(message + "\r\n");

			var ex = Assert.Throws(typeof(InvalidOperationException),
								   delegate { port.Send(message); });

			Assert.AreEqual("ASCII commands cannot be sent when the port is in binary mode.", ex.Message);
		}


		[Test]
		public void SendChecksum()
		{
			var port = CreatePort();
			port.IsAsciiMode = true;
			port.AreChecksumsSent = true;

			var message = "/01";
			var expectedText = message + ":9F";
			var expectedBytes = Encoding.ASCII.GetBytes(expectedText + "\r\n");

			var packetSent = CatchPacketSentFrom(port,
												 delegate { port.Send(message); });

			Assert.IsNotNull(packetSent);
			Assert.AreEqual(expectedText, packetSent.Text);
			Assert.That(port.SentBytes,
						Is.EquivalentTo(expectedBytes));
		}


		[Test]
		public void AddSlash()
		{
			var port = CreatePort();
			port.IsAsciiMode = true;

			var message = "move abs 0";
			var expectedMessage = "/" + message;
			var expectedBytes = Encoding.ASCII.GetBytes(expectedMessage + "\r\n");

			var packetSent = CatchPacketSentFrom(port,
												 delegate { port.Send(message); });

			Assert.IsNotNull(packetSent);
			Assert.AreEqual(expectedMessage, packetSent.Text);
			Assert.That(port.SentBytes,
						Is.EquivalentTo(expectedBytes));
		}


		[Test]
		public void SendMeasurement()
		{
			byte deviceNumber = 1;
			var command = Command.MoveAbsolute;
			var data = 4200;
			var measurement = new Measurement(4.2, UnitOfMeasure.Millimeter);

			var port = CreatePort();

			var expectedBytes = new byte[] { deviceNumber, (byte) command, (byte) data, 0, 0, 0 };

			var packetSent = CatchPacketSentFrom(port,
												 delegate { port.Send(deviceNumber, command, data, measurement); });

			Assert.AreEqual(measurement, packetSent.Measurement);
		}


		[Test]
		public void SendDefaultData()
		{
			byte deviceNumber = 1;
			var command = Command.EchoData;
			var expectedData = 0;

			var port = CreatePort();

			var expectedBytes = new byte[] { deviceNumber, (byte) command, (byte) expectedData, 0, 0, 0 };

			var packetSent = CatchPacketSentFrom(port,
												 delegate { port.Send(deviceNumber, command); });

			Assert.That(port.SentBytes,
						Is.EquivalentTo(expectedBytes));
			Assert.AreEqual(deviceNumber, packetSent.DeviceNumber);
			Assert.AreEqual(command, packetSent.Command);
			Assert.AreEqual(expectedData, packetSent.NumericData);
		}


		[Test]
		public void SendDelayed()
		{
			byte deviceNumber = 1;
			var command = Command.EchoData;
			var data = 42;

			var port = CreatePort();

			var sentEvent = new ManualResetEvent(false);
			DataPacket packetSent = null;
			port.DataPacketSent +=
				delegate(object sender, DataPacketEventArgs args)
				{
					packetSent = args.DataPacket;
					sentEvent.Set();
				};

			var expectedBytes = new byte[] { deviceNumber, (byte) command, (byte) data, 0, 0, 0 };

			var isTimerEnabledBeforeSend = port.IsDelayTimerEnabled;
			port.SendDelayed(deviceNumber, command, data);

			var packetSentBeforeTimer = packetSent;
			var isTimerEnabledAfterSend = port.IsDelayTimerEnabled;

			port.TriggerDelayedSend();
			var isTimerEnabledAfterTrigger = port.IsDelayTimerEnabled;
			var isSent = sentEvent.WaitOne(1000);

			Assert.IsTrue(isSent);
			Assert.IsFalse(isTimerEnabledBeforeSend);
			Assert.IsNull(packetSentBeforeTimer);
			Assert.IsTrue(isTimerEnabledAfterSend);
			Assert.IsFalse(isTimerEnabledAfterTrigger);
			Assert.That(port.SentBytes,
						Is.EquivalentTo(expectedBytes),
						"bytes");
			Assert.AreEqual(deviceNumber, packetSent.DeviceNumber);
			Assert.AreEqual(command, packetSent.Command);
			Assert.AreEqual(data, packetSent.NumericData);
		}


		[Test]
		public void SendDelayedTwice()
		{
			byte deviceNumber = 1;
			var command = Command.EchoData;
			var data1 = 42;
			var data2 = 43;

			var port = CreatePort();

			var expectedBytes = new byte[]
			{
				deviceNumber, (byte) command, (byte) data1, 0, 0, 0, deviceNumber, (byte) command, (byte) data2, 0,
				0, 0
			};

			port.SendDelayed(deviceNumber, command, data1);
			port.SendDelayed(deviceNumber, command, data2);

			var isTimerEnabledAfterSecondSend = port.IsDelayTimerEnabled;

			port.TriggerDelayedSend();
			var isTimerEnabledAfterTrigger1 = port.IsDelayTimerEnabled;

			port.TriggerDelayedSend();
			var isTimerEnabledAfterTrigger2 = port.IsDelayTimerEnabled;

			Assert.IsTrue(isTimerEnabledAfterSecondSend);
			Assert.IsTrue(isTimerEnabledAfterTrigger1);
			Assert.IsFalse(isTimerEnabledAfterTrigger2);
			Assert.That(port.SentBytes,
						Is.EquivalentTo(expectedBytes),
						"bytes");
		}


		[Test]
		public void CancelDelayedPacket()
		{
			byte deviceNumber = 1;
			var command = Command.EchoData;
			var data = 42;

			var port = CreatePort();

			var packet = port.SendDelayed(deviceNumber, command, data);

			var wasCanceled = port.CancelDelayedPacket(packet);
			var isTimerEnabledAfterCancel = port.IsDelayTimerEnabled;

			Assert.IsFalse(isTimerEnabledAfterCancel);
			Assert.IsTrue(wasCanceled);
		}


		[Test]
		public void CancelAfterDelayedSend()
		{
			byte deviceNumber = 1;
			var command = Command.EchoData;
			var data = 42;

			var port = CreatePort();

			var packet = port.SendDelayed(deviceNumber, command, data);
			port.TriggerDelayedSend();

			var wasCanceled = port.CancelDelayedPacket(packet);

			Assert.IsFalse(wasCanceled);
		}


		[Test]
		public void CancelOneDelayedPacket()
		{
			byte deviceNumber = 1;
			var command = Command.EchoData;
			var data1 = 42;
			var data2 = 43;

			var port = CreatePort();

			var expectedBytes = new byte[] { deviceNumber, (byte) command, (byte) data2, 0, 0, 0 };

			var packet1 = port.SendDelayed(deviceNumber, command, data1);
			var packet2 = port.SendDelayed(deviceNumber, command, data2);

			var wasCanceled = port.CancelDelayedPacket(packet1);
			var isTimerEnabledAfterCancel = port.IsDelayTimerEnabled;

			port.TriggerDelayedSend();
			var isTimerEnabledAfterTrigger = port.IsDelayTimerEnabled;

			Assert.IsTrue(isTimerEnabledAfterCancel);
			Assert.IsFalse(isTimerEnabledAfterTrigger);
			Assert.That(port.SentBytes,
						Is.EquivalentTo(expectedBytes),
						"bytes");
			Assert.IsTrue(wasCanceled);
		}


		[Test]
		public void ReceiveBinary()
		{
			byte deviceNumber = 1;
			var command = Command.EchoData;
			var data = 42;

			var receivedEvent = new ManualResetEvent(false);

			var port = CreatePort();

			DataPacket packetReceived = null;
			port.DataPacketReceived +=
				delegate(object sender, DataPacketEventArgs args)
				{
					packetReceived = args.DataPacket;
					receivedEvent.Set();
				};

			var bytesToReceive = new byte[] { deviceNumber, (byte) command, (byte) data, 0, 0, 0 };

			var expectedMeasurement =
				new Measurement(data, UnitOfMeasure.Data);

			port.BytesToReceive.AddRange(bytesToReceive);

			port.ReceiveBytes();
			receivedEvent.WaitOne();

			Assert.AreEqual(deviceNumber, packetReceived.DeviceNumber);
			Assert.AreEqual(command, packetReceived.Command);
			Assert.AreEqual(data, packetReceived.NumericData);
			Assert.AreEqual(expectedMeasurement, packetReceived.Measurement);
		}


		[Test]
		public void ReceiveBinaryInAsciiMode()
		{
			byte deviceNumber = 1;
			var command = Command.EchoData;
			var data = 42;

			var receivedEvent = new ManualResetEvent(false);

			var port = CreatePort();

			DataPacket packetReceived = null;
			port.DataPacketReceived +=
				delegate(object sender, DataPacketEventArgs args)
				{
					packetReceived = args.DataPacket;
					receivedEvent.Set();
				};

			var bytesToReceive = new byte[] { deviceNumber, (byte) command, (byte) data, 0, 0, 0 };

			var expectedMeasurement =
				new Measurement(data, UnitOfMeasure.Data);

			port.BytesToReceive.AddRange(bytesToReceive);

			port.IsAsciiMode = true;
			port.ReceiveBytes();
			var wasReceived = receivedEvent.WaitOne(0);

			Assert.IsFalse(wasReceived);
			Assert.IsNull(packetReceived);
		}


		[Test]
		public void BadChecksum()
		{
			var port = CreatePort();
			port.IsStubPortOpen = true;
			port.IsAsciiMode = true;

			DataPacket packetReceived = null;
			port.DataPacketReceived +=
				delegate(object sender, DataPacketEventArgs args) { packetReceived = args.DataPacket; };

			var errorReceived = ZaberPortError.None;
			port.ErrorReceived +=
				delegate(object sender, ZaberPortErrorReceivedEventArgs args) { errorReceived = args.ErrorType; };

			var bytesToReceive = Encoding.ASCII.GetBytes("@01 0 OK IDLE -- 0:FF\r\n");

			port.BytesToReceive.AddRange(bytesToReceive);

			port.ReceiveBytes();

			Thread.Sleep(50);

			Assert.IsNull(packetReceived);
			Assert.AreEqual(ZaberPortError.InvalidChecksum, errorReceived);
		}


		[Test]
		public void ReceiveText()
		{
			var port = CreatePort();
			port.IsStubPortOpen = true;
			port.IsAsciiMode = true;

			DataPacket packetReceived = null;
			port.DataPacketReceived +=
				delegate(object sender, DataPacketEventArgs args) { packetReceived = args.DataPacket; };

			var errorReceived = ZaberPortError.None;
			port.ErrorReceived +=
				delegate(object sender, ZaberPortErrorReceivedEventArgs args) { errorReceived = args.ErrorType; };

			var bytesToReceive = Encoding.ASCII.GetBytes("@01 0 OK IDLE -- 0\r\n");

			port.BytesToReceive.AddRange(bytesToReceive);

			port.ReceiveBytes();

			Thread.Sleep(50);

			Assert.AreEqual("@01 0 OK IDLE -- 0", packetReceived.FormatResponse());
			Assert.AreEqual(ZaberPortError.None, errorReceived);
			Assert.AreEqual(1, packetReceived.DeviceNumber);
			Assert.AreEqual(0, packetReceived.NumericData);
		}


		[Test]
		public void ReportInvalidPacket()
		{
			var port = CreatePort();
			port.IsStubPortOpen = true;

			var errorType = ZaberPortError.None;
			port.ErrorReceived +=
				delegate(object sender, ZaberPortErrorReceivedEventArgs args) { errorType = args.ErrorType; };

			port.ReportInvalidPacket();

			Assert.AreEqual(ZaberPortError.InvalidPacket, errorType);
		}

		#endregion

		#region -- Non-Public Methods --

		protected abstract IPortStub CreatePort();


		private DataPacket CatchPacketSentFrom(IZaberPort port, Action sender)
		{
			var receivedEvent = new ManualResetEvent(false);
			DataPacket packetSent = null;
			port.DataPacketSent +=
				delegate(object o, DataPacketEventArgs args)
				{
					packetSent = args.DataPacket;
					receivedEvent.Set();
				};

			sender.Invoke();
			var isReceived = receivedEvent.WaitOne(1000);

			Assert.IsTrue(isReceived);

			return packetSent;
		}

		#endregion
	}
}

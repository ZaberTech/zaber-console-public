﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using NUnit.Framework;
using Zaber.Threading;

namespace ZaberTest.Threading
{
	[TestFixture]
	[SetCulture("en-US")]
	public class CounterTokenTests
	{
		[SetUp]
		public void Setup()
		{
			_source = new CounterTokenSource();
		}


		[Test]
		public void BasicCounting()
		{
			bool lastState = true;
			_source.ZeroStateChanged += (aSender, aArgs) =>
			{
				Assert.AreEqual(_source, aSender);
				lastState = aArgs.IsZero;
			};

			using (var _ = _source.GetToken())
			{
				Assert.IsFalse(lastState);
			}

			Assert.IsTrue(lastState);

			using (var _ = _source.GetToken())
			{
				Assert.IsFalse(lastState);

				var tokens = new Queue<IDisposable>();
				for (int i = 0; i < 1000; i++)
				{
					tokens.Enqueue(_source.GetToken());
					Assert.IsFalse(lastState);
				}

				while (tokens.Count > 0)
				{ 
					var token = tokens.Dequeue();
					token?.Dispose();
					Assert.IsFalse(lastState);
				}
			}

			Assert.IsTrue(lastState);
		}


		[Test]
		public void Multithreaded()
		{
			bool lastState = true;
			_source.ZeroStateChanged += (aSender, aArgs) =>
			{
				Assert.AreEqual(_source, aSender);

				// Interlocked.Exchange should not be needed here because the
				// source guarantees ordering of callbacks relative to
				// counter value changes.
				lastState = aArgs.IsZero; 
			};

			void ThreadMain()
			{
				var random = new Random();
				for (var i = 0; i < 10; i++)
				{
					using (var _ = _source.GetToken())
					{
						Assert.IsFalse(lastState);
						Thread.Sleep(random.Next(100));
						Assert.IsFalse(lastState);
					}
				}
			}

			var threads = new List<Thread>();
			for (int i = 0; i < 10; i++)
			{
				var thread = new Thread(ThreadMain);
				threads.Add(thread);
				thread.Start();
			}

			foreach (var thread in threads)
			{
				thread.Join();
			}

			Assert.IsTrue(lastState);
		}


		private CounterTokenSource _source;
	}
}

using System.Collections.Generic;
using System.Linq;
using Zaber;

namespace ZaberTest
{
	public static class DeviceTypeMother
	{
		#region -- Public Methods & Properties --

		public static DeviceType CreateDeviceTypeWithCommands()
		{
			var deviceType = new DeviceType();
			AddCommands(deviceType);

			return deviceType;
		}


		public static void AddCommands(DeviceType deviceType)
		{
			var commands = new List<CommandInfo>
			{
				new CommandInfo
				{
					Command = Command.Home,
					Name = "Where the heart is",
					IsRetrySafe = true
				},
				new CommandInfo
				{
					Command = Command.Stop,
					Name = "In the name of love",
					IsRetrySafe = true
				},
				new CommandInfo
				{
					Command = Command.MoveAbsolute,
					Name = "Move Absolute",
					IsRetrySafe = true,
					RequestUnit = UnitOfMeasure.Millimeter,
					RequestUnitScale = 15.625m,
					RequestUnitFunction = ScalingFunction.LinearResolution,
					ResponseUnit = UnitOfMeasure.Millimeter,
					ResponseUnitScale = 15.625m,
					ResponseUnitFunction = ScalingFunction.LinearResolution,
					IsCurrentPositionReturned = true
				},
				new CommandInfo
				{
					Command = Command.MoveRelative,
					Name = "Move Relative",
					RequestUnit = UnitOfMeasure.Millimeter,
					RequestUnitScale = 15.625m,
					RequestUnitFunction = ScalingFunction.LinearResolution,
					ResponseUnit = UnitOfMeasure.Millimeter,
					ResponseUnitScale = 15.625m,
					ResponseUnitFunction = ScalingFunction.LinearResolution
				},
				new CommandInfo
				{
					Command = Command.EchoData,
					Name = "Echo Data",
					IsRetrySafe = true
				},
				new CommandInfo
				{
					TextCommand = "home",
					IsRetrySafe = true
				},
				new CommandInfo
				{
					TextCommand = "pos",
					RequestUnit = UnitOfMeasure.Millimeter,
					RequestUnitScale = 15.625m,
					RequestUnitFunction = ScalingFunction.LinearResolution,
					ResponseUnit = UnitOfMeasure.Millimeter,
					ResponseUnitScale = 15.625m,
					ResponseUnitFunction = ScalingFunction.LinearResolution,
					IsRetrySafe = true,
					IsCurrentPositionReturned = true
				},
				new CommandInfo
				{
					TextCommand = "maxspeed",
					RequestUnit = UnitOfMeasure.MillimetersPerSecond,
					RequestUnitScale = 15.625m,
					RequestUnitFunction = ScalingFunction.LinearResolution,
					ResponseUnit = UnitOfMeasure.MillimetersPerSecond,
					ResponseUnitScale = 15.625m,
					ResponseUnitFunction = ScalingFunction.LinearResolution,
					IsRetrySafe = true
				},
				new CommandInfo
				{
					TextCommand = "accel",
					RequestUnit = UnitOfMeasure.MillimetersPerSecondSquared,
					RequestUnitScale = 15.625m,
					RequestUnitFunction = ScalingFunction.LinearResolution,
					ResponseUnit = UnitOfMeasure.MillimetersPerSecondSquared,
					ResponseUnitScale = 15.625m,
					ResponseUnitFunction = ScalingFunction.LinearResolution,
					IsRetrySafe = true
				}
			};

			deviceType.Commands = commands;
		}


		public static DeviceType CreateDeviceTypeWithPeripherals()
		{
			var deviceType = new DeviceType
			{
				DeviceId = 11111,
				Name = "A-Controller"
			};
			AddPeripherals(deviceType);

			return deviceType;
		}


		public static void AddPeripherals(DeviceType deviceType)
		{
			var peripherals = new List<DeviceType>
			{
				new DeviceType(),
				new DeviceType()
			};
			peripherals[0].PeripheralId = 0;
			peripherals[0].Name = "Safe Mode";
			peripherals[1].PeripheralId = 12345;
			peripherals[1].Name = "BIG-STAGE";

			deviceType.PeripheralMap = peripherals.ToDictionary(dt => dt.PeripheralId);
		}

		#endregion
	}
}

# Zaber Console

![Screenshot](documentation/images/ZaberConsole.png "Zaber Console screenshot")

Zaber Console is Zaber's publicly available tool for configuring and operating stages. 

It is currently supported for use on Windows 7 and Windows 10. It may also work on other versions of Windows
but this is untested.


## Documentation

The Zaber Console user guide is available on [Zaber's website](https://www.zaber.com/wiki/Software/Zaber_Console).

Some aspects of the program require knowledge of Zaber's [ASCII](https://www.zaber.com/protocol-manual) and/or
[Binary](https://www.zaber.com/protocol-manual?protocol=Binary) communication protocols.

If you do not need the source code, the installer for the latest version of the program can
be downloaded from [Zaber's software page](https://www.zaber.com/software).


## Compiling from Source

If you need to recompile the program from source:

1. Open `ZaberCSDemo/ZaberCSDemo.sln` with Visual Studio 2017 or later and set `ZaberConsoleGUI` as the startup project. 
2. Restore NuGet packages. Right-click on the top-level solution in the Solution
Explorer window, select `Manage NuGet packages for solution...`, and if you see a yellow banner at the top of
the window prompting you to do so, select the option to restore all packages.
3. Open a command prompt in the ZaberCSDemo directory and run `fake.bat update-database` to
download the device database, which is part of the solution but not in version control due to its size.
4. Compile.

The first time you launch the program from the debugger, you may get a warning saying that security
debugging is disabled. This is normal and does not affect program performance; click OK.

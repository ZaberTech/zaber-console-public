﻿# This is a Python3 script that regenerates the binary command/error/reply enums 
# in Zaber.dll from content in the device databases.

# It is recommended to use the public database as input so that internal commands
# or settings are not exposed in the public API.

import argparse
import io
import logging
import os
import sqlite3

from EnumBuilder import EnumBuilder, EnumValue, EnumValueAttribute
import FindDeviceDatabase as finder
from CSharpEnumFormatter import CSharpEnumFormatter
import SemanticDatabase as semantics

BASE_DIR = "Z:\Product\FW\device-database"

def parse_command_line():
    parser = argparse.ArgumentParser(description="Tool to generate C# enumeration representing Binary protocol codes.")
    parser.add_argument("dbfile", type=str, help="Name of the device database file to use.")
    parser.add_argument("--commands", type=str, default=None, help="Path to the file to write command codes to, which will be overwritten. Omit to output to the console.")
    parser.add_argument("--replies", type=str, default=None, help="Path to the file to write reply codes to, which will be overwritten. Omit to output to the console.")
    parser.add_argument("--errors", type=str, default=None, help="Path to the file to write error codes to, which will be overwritten. Omit to output to the console.")
    return parser.parse_args()


def main(args):
    dbfile = finder.locate_database(BASE_DIR, args.dbfile)
    logging.info(f"Generating command codes from {dbfile}...")
    output(generate_command_enum(dbfile), args.commands, ["System"])

    logging.info("Generating reply codes...")
    output(generate_reply_enum(dbfile), args.replies, ["System", "System.ComponentModel"])

    logging.info("Generating error codes...")
    output(generate_error_enum(dbfile), args.errors, ["System", "System.ComponentModel"], "int")

    logging.info("Done!")


def output(builder, dest, usings = [], base_class = "byte"):
    text = generate_cs_enum(builder, usings, base_class)
    if (dest):
        with open(dest, "wb") as fp:
            fp.write(text.encode("utf-8"))
    else:
        print(text)


def generate_command_enum(dbfile):
    builder = read_binary_commands_from_database(dbfile)
    add_command_documentation(builder)
    return builder


def generate_reply_enum(dbfile):
    builder = read_binary_replies_from_database(dbfile)
    add_reply_documentation(builder)
    return builder


def generate_error_enum(dbfile):
    builder = read_binary_errors_from_database(dbfile)
    add_alternate_error_spellings(builder)
    add_error_documentation(builder)
    return builder


def capitalize_first_letter(word):
    return word[:1].capitalize() + word[1:]


def capitalize_each_word(sentence):
    return " ".join([capitalize_first_letter(word) for word in sentence.split(" ")])


def code_safe_name(s):
    return s.replace(" ", "").replace("-", "")


def current_name_first(nameSet):
    names = [n for n in sorted(nameSet)]

    # If the command code has multiple names, make sure the first one is not legacy.
    if (len(names) > 1) and (semantics.get_legacy_command_name(names[0]) is not None):
        for i in range(1, len(names)):
            name = names[i]
            if semantics.get_legacy_command_name(name) is None:
                del names[i]
                names.insert(0, name)

    return names


def add_to_collection_in_dict(dict, key, newValue, defaultCollection):
        collection = dict.get(key, defaultCollection)
        collection.add(newValue)
        dict[key] = collection


def read_binary_commands_from_database(db_path):
    logging.debug(f"Reading command data from { str(db_path) }...")
    connection = sqlite3.connect(db_path)
    connection.row_factory = sqlite3.Row
    cursor = connection.cursor()

    namesByValue = {}

    # Add normal binary commands.
    cursor.execute("SELECT DISTINCT Command, Name FROM Console_AllBinaryCommands;")
    rows = cursor.fetchall()

    for row in rows:
        code = row["Command"]
        name = capitalize_each_word(row["Name"])
        add_to_collection_in_dict(namesByValue, code, name, set())

    # Add binary set and get commands.
    cursor.execute("SELECT DISTINCT Name, ReturnCommand, SetCommand FROM Console_AllBinarySettings;")
    rows = cursor.fetchall()
    for row in rows:
        getCode = row["ReturnCommand"]
        setCode = row["SetCommand"]
        name = capitalize_each_word(row["Name"])
        if getCode is not None:
            add_to_collection_in_dict(namesByValue, getCode, "Return " + name, set())

        if setCode is not None:
            add_to_collection_in_dict(namesByValue, setCode, "Set " + name, set())

    logging.info(f"Found {len(namesByValue)} values")
    if (len(namesByValue) < 1):
        logging.error("No binary command code data found.")
        return None
    
    # Add legacy names.
    for code, name in semantics.get_all_legacy_command_names().items():
        add_to_collection_in_dict(namesByValue, code, capitalize_each_word(name), set())

    # Generate enum builder.
    builder = EnumBuilder("Command", "Zaber")
    builder.description = \
        "Named constants for the common commands that most Binary protocol devices " \
        + "will support." + os.linesep \
        + "<c>Command</c> values and <c>byte</c> values are interchangeable, " \
        + "but you do need an explicit cast to convert from one to the other."

    for code, nameSet in namesByValue.items():
        names = current_name_first(nameSet)

        # Add the main enum name for this value.
        baseValue = EnumValue(code_safe_name(names[0]), code)
        builder.add_value(baseValue)

        replacementName = None
        replacementCode = semantics.get_recommended_command(code)
        if replacementCode != code:
            replacementName = current_name_first(namesByValue.get(replacementCode, [str(replacementCode)]))[0]
            replacementName = code_safe_name(replacementName)
        elif semantics.get_reply_description(code) is not None:
            # Here we could recommend using the ZaberReply code instead, but
            # it's convenient to be able to compare reply codes to command
            # codes without casting so we won't mark the old command codes that
            # are actually replies as obsolete.
            replacementName = None

        if replacementName is not None:
            baseValue.add_attribute(EnumValueAttribute("Obsolete", f'"Use {replacementName} instead."'))

        # Add any remaining names as legacy.
        baseName = baseValue.name
        for i in range(1, len(names)):
            name = names[i]
            legacyValue = EnumValue(code_safe_name(name), code)

            if replacementName is not None:
                legacyValue.add_attribute(EnumValueAttribute("Obsolete", f'"Use {replacementName} instead."'))
            else:
                legacyValue.add_attribute(EnumValueAttribute("Obsolete", f'"Use {baseName} instead."'))

            builder.add_value(legacyValue)

    return builder;


def read_binary_replies_from_database(db_path):
    logging.debug(f"Reading reply data from { str(db_path) }...")
    connection = sqlite3.connect(db_path)
    connection.row_factory = sqlite3.Row
    cursor = connection.cursor()

    cursor.execute("SELECT DISTINCT Reply, Name FROM Console_AllBinaryReplies WHERE Internal=0;")
    rows = cursor.fetchall()

    if (len(rows) < 1):
        logging.error("No binary reply code data found.")
        return None

    builder = EnumBuilder("ZaberReply", "Zaber")
    builder.description = "Named constants for the reply codes that Zaber devices will return."

    for row in rows:
        code = row["Reply"]
        name = code_safe_name(row["Name"])
        value = EnumValue(name, code)
        builder.add_value(value)

    return builder;


def read_binary_errors_from_database(db_path):
    logging.debug(f"Reading error data from { str(db_path) }...")
    connection = sqlite3.connect(db_path)
    connection.row_factory = sqlite3.Row
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM Console_AllBinaryErrors ORDER BY Code;")
    rows = cursor.fetchall()

    if (len(rows) < 1):
        logging.error("No binary error code data found.")
        return None

    builder = EnumBuilder("ZaberError", "Zaber")
    builder.description = "Named constants for the error codes that Zaber devices will return."

    # The database does not include a name for zero.
    builder.add_value(EnumValue("None", 0))

    for row in rows:
        code = row["Code"]
        name = code_safe_name(row["Name"])
        value = EnumValue(name, code)
        builder.add_value(value)

    return builder;


def add_command_documentation(builder):
    # Adds description attributes to command codes, including legacy values.

    logging.debug("Adding command descriptions...")

    for val in builder.values:
        paramDesc = semantics.get_command_param_description(val.value)
        returnDesc = semantics.get_command_return_description(val.value)

        if paramDesc is not None:
            val.add_attribute(EnumValueAttribute("ParameterDescription", f'"{paramDesc}"'))

        if returnDesc is not None:
            val.add_attribute(EnumValueAttribute("ReturnDescription", f'"{returnDesc}"'))

        description = f"Request data is {paramDesc or 'ignored'}. Response data is {returnDesc}."
        val.description = description


def add_reply_documentation(builder):
    logging.debug("Adding reply descriptions...")
    for val in builder.values:
        desc = semantics.get_reply_description(val.value, val.name)
        val.add_attribute(EnumValueAttribute("Description", f'"{desc}"'))

        dataDesc = semantics.get_reply_data_description(val.value)
        if dataDesc is not None:
            val.add_attribute(EnumValueAttribute("ReturnDescription", f'"{dataDesc}"'))
            desc += f" Return value is {dataDesc}."

        val.description = desc


def add_alternate_error_spellings(builder):
    logging.debug("Adding legacy error names...")
    for code, name in semantics.get_all_legacy_error_names().items():
        legacyName = EnumValue(code_safe_name(name), code)
        existingNames = builder.find_names(code) or []
        if (len(existingNames) > 0):
            legacyName.add_attribute(EnumValueAttribute("Obsolete", f'"Use {existingNames[0]} instead."'))
        builder.add_value(legacyName)


def add_error_documentation(builder):
    logging.debug("Adding error descriptions...")
    for val in builder.values:
        desc = semantics.get_error_description(val.value, val.name)
        val.add_attribute(EnumValueAttribute("Description", f'"{desc}"'))
        val.description = desc


def generate_cs_enum(builder, usings = [], base_class = "byte"):
    logging.debug("Generating code...")
    file = io.StringIO()
    file.write("// <auto-generated>" + os.linesep)
    file.write("// \tGenerated by ZaberCSDemo\GenerateBinaryEnums.py - DO NOT EDIT!" + os.linesep)
    file.write("// </auto-generated>" + os.linesep)
    file.write(os.linesep)

    formatter = CSharpEnumFormatter()
    formatter.base_class = base_class
    for using in usings:
        formatter.add_using(using)

    formatter.generate_enum_text(builder, file)
    return file.getvalue()


if ("__main__" == __name__):
    logging.basicConfig(level = 20)
    main(parse_command_line())

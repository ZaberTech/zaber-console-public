﻿# This is a Python3 script that downloads and decompresses the device database
# and places it in the location expected by the Visual Studio project and installer script.

import argparse
import logging
import lzma
import os
import tempfile
import urllib.request

import FindDeviceDatabase as finder


BASE_DIR = "Z:\Product\FW\device-database"
PUBLIC_FILE = "devices-public.sqlite"
MASTER_FILE = "devices-master.sqlite"
PUBLIC_URL = "https://www.zaber.com/software/device-database/devices-public.sqlite.lzma"
MASTER_URL = "https://zshared.izaber.com/device-database/devices-master.sqlite.lzma"


def parse_command_line():
    parser = argparse.ArgumentParser(description="Tool to download and decompress the device database.")
    parser.add_argument("--edition", "-e", type=str, help="Specify public or master database.")
    parser.add_argument("--dest", "-d", type=str, help="Path and file name to write to. Will be overwritten.")
    return parser.parse_args()


def main(args):
    if not args.dest:
        raise ValueError("Destination path must be given on the command line.")

    logging.info("Updating Device Database (" + args.edition + ")")

    if args.edition.lower() == "master":
        fileName = MASTER_FILE
        url = MASTER_URL
    else:
        fileName = PUBLIC_FILE
        url = PUBLIC_URL

    compressedFileName = fileName + ".lzma"

    try:
        path = finder.locate_database(BASE_DIR, compressedFileName)
        logging.info(f"Decompressing from {path}...")
        decompress(path, args.dest)
        success = True
    except:
        success = False

    if not success:
        logging.error("Local file access failed; trying download from " + url)
        with tempfile.TemporaryDirectory() as tmp:
            tmpFile = os.path.join(tmp, compressedFileName)
            download(url, tmpFile)
            decompress(tmpFile, args.dest)

    logging.info("Done!")


def download(url, dest):
    urllib.request.urlretrieve(url, dest)


def decompress(src, dest):
    AMEG = 1024 * 1024
    with lzma.open(src, "rb") as input_file:
        with open(dest, "wb") as output_file:
            data = input_file.read(AMEG)
            while len(data) > 0:
                output_file.write(data)
                data = input_file.read(AMEG)


if ("__main__" == __name__):
    logging.basicConfig(level = 20)
    main(parse_command_line())

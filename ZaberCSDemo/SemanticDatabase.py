﻿# This code encapsulates semantic data about binary commands, replies and
# error codes that should be moved into a localizable database.
# See Firmware ticket #4888.

# Old names for some commands, and reply names that were previously included with commands.
_LEGACY_COMMAND_NAMES = \
{
    5: "Read Register",
    6: "Set Active Register",
    7: "Write Register",
    8: "Move Tracking",
    9: "Limit Active",
    10: "Manual Move Tracking",
    11: "Manual Move",
    12: "Slip Tracking",
    13: "Unexpected Position",
    33: "Set Calibration Mode",
    44: "Set Maximum Range",
    50: "Return Device Id",
    66: "Set Peripheral Id",
    102: "Set Message Id Mode",
    122: "Set Baudrate",
    255: "Error"
}

def get_legacy_command_name(code, default = None):
    return _LEGACY_COMMAND_NAMES.get(code, default)

def get_all_legacy_command_names():
    return _LEGACY_COMMAND_NAMES



# Recommended replacements for old command codes.
_COMMAND_REPLACEMENTS = \
{
}

def get_recommended_command(code):
    return _COMMAND_REPLACEMENTS.get(code, code)


# Descriptions of command parameters and return values.
_COMMAND_PARAMS_AND_RETURNS = \
{
    0: (None, None),
    1: (None, "Final Position"),
    2: ("New Number", "Device ID"),
    5: ("Register Address", "Data"),
    6: ("Register Address", "Register Address"),
    7: ("Data", "Data"),
    8: (None, "Position"),
    9: (None, "Position"),
    10: (None, "Position"),
    11: (None, "Position"),
    12: (None, "Position"),
    13: (None, "Position"),
    16: ("Address", "Address"),
    17: ("Address", "Stored Position"),
    18: ("Address", "Final Position"),
    20: ("Absolute Position", "Final Position"),
    21: ("Relative Position", "Final Position"),
    22: ("Speed", "Speed"),
    23: (None, "Final Position"),
    25: ("Axis", "Axis"),
    26: ("Device Number", "Device Number"),
    27: ("Invert Status", "Invert Status"),
    28: ("Profile Number", "Profile Number"),
    29: ("Maximum Velocity", "Maximum Velocity"),
    30: ("Key Event", "Key Event"),
    31: ("Key Event", None),
    33: ("Calibration Mode", "Calibration Mode"),
    35: ("Data", "Data"),
    36: ("Peripheral ID", "Peripheral ID"),
    37: ("Microsteps", "Microsteps"),
    38: ("Value", "Value"),
    39: ("Value", "Value"),
    40: ("Mode", "Mode"),
    41: ("Speed", "Speed"),
    42: ("Speed", "Speed"),
    43: ("Acceleration", "Acceleration"),
    44: ("Range", "Range"),
    45: ("New Position", "New Position"),
    46: ("Range", "Range"),
    47: ("Offset", "Offset"),
    48: ("Alias Number", "Alias Number"),
    49: ("Lock State", "Lock State"),
    50: (None, "Device ID"),
    51: (None, "Version"),
    52: (None, "Voltage"),
    53: ("Setting Number", "Setting Value"),
    54: (None, "Status"),
    55: ("Data", "Data"),
    56: (None, "Build Number"),
    60: (None, "Position"),
    63: (None, "Serial Number"),
    65: ("Park State", "Park State"),
    66: ("Peripheral ID", "Peripheral ID"),
    67: (None, "Pin Count"),
    68: ("Pin Number", "Pin State"),
    69: (None, "Pin States"),
    70: (None, "Pin Count"),
    71: ("Pin Number", "Pin State"),
    72: (None, "Pin States"),
    73: ("See Description", "Same as Command Data"),
    74: ("Pin States", "Pin States"),
    75: (None, "Pin Count"),
    76: ("Pin Number", "Voltage"),
    77: (None, "Pin Count"),
    78: ("Index Number", "Final Position"),
    79: ("Distance", "Distance"),
    80: ("Distance", "Distance"),
    81: ("Filter Holder ID", "Filter Holder ID"),
    82: (None, "Encoder Count"),
    83: (None, "Calibrated Encoder Count"),
    84: (None, "Calibration Type"),
    85: (None, "Calibration Error"),
    86: ("Peripheral Serial Number", "Peripheral Serial Number"),
    87: ("Force", "Force"),
    88: (None, None),
    89: (None, "Encoder position in distance units"),
    101: ("Auto-Reply Disabled Mode", "Auto-Reply Disabled Mode"),
    102: ("Message ID Mode", "Message ID Mode"),
    103: ("Home Status", "Home Status"),
    104: ("Home Sensor Type", "Home Sensor Type"),
    105: ("Auto-Home Disabled Mode", "Auto-Home Disabled Mode"),
    106: ("Minimum Position", "Minimum Position"),
    107: ("Knob Disabled Mode", "Knob Disabled Mode"),
    108: ("Knob Direction", "Knob Direction"),
    109: ("Movement Mode", "Movement Mode"),
    110: ("Jog Size", "Jog Size"),
    111: ("Maximum Speed", "Maximum Speed"),
    112: ("Profile Number", "Profile Number"),
    113: ("Acceleration", "Acceleration"),
    114: ("Deceleration", "Deceleration"),
    115: ("Move Tracking Mode", "Move Tracking Mode"),
    116: ("Manual Move Tracking Disabled Mode", "Manual Move Tracking Disabled Mode"),
    117: ("Tracking Period in ms", "Tracking Period in ms"),
    118: ("Closed-Loop Mode", "Closed-Loop Mode"),
    119: ("Tracking Period in ms", "Tracking Period in ms"),
    120: ("Stall Timeout in ms", "Stall Timeout in ms"),
    121: ("Device Direction", "Device Direction"),
    122: ("Baud Rate", "Baud Rate"),
    123: ("Protocol", "Protocol"),
    124: ("Baud Rate", "Baud Rate"),
    255: (None, "Error Code"),
}

def get_command_param_description(code, default = None):
    info = _COMMAND_PARAMS_AND_RETURNS.get(code)
    if info is None:
        return default

    return info[0]

def get_command_return_description(code, default = None):
    info = _COMMAND_PARAMS_AND_RETURNS.get(code)
    if info is None:
        return default

    return info[1]


# Table of legacy names for some error codes. Included to
# avoid breaking old scripts.
_LEGACY_ERROR_NAMES = { \
    33: "Calibration Mode Invalid",
    36: "Peripheral Id Invalid",
    44: "Maximum Range Invalid",
    50: "Device Id Unknown",
    71: "Digital Ouput Pin Invalid", # Ugh - legacy typos.
    74: "Digital Ouput Mask Invalid",
    102: "Message Id Mode Invalid",
    122: "Baudrate Invalid",
    124: "Baudrate Or Protocol Invalid",
}

def get_all_legacy_error_names():
    return _LEGACY_ERROR_NAMES


# Descriptions of all binary error codes.
# Currently the descriptions also apply to the corresponding legacy names.
# This data was hand-copied from the FW6 binary protocol manual.
_ERROR_DESCRIPTIONS = { \
    0: "No error has occurred. This should never be returned by a device. It's only useful as a default value in code.",
    1: "Home - Device has traveled a long distance without triggering the home sensor. Device may be stalling or slipping.",
    2: "Renumbering data out of range.",
    5: "Read Register - Register address invalid.",
    14: "Power supply voltage too low.",
    15: "Power supply voltage too high.",
    18: "The position stored in the requested register is no longer valid. This is probably because the maximum range was reduced.",
    20: "Move Absolute - Target position out of range.",
    21: "Move Relative - Target position out of range.",
    22: "Constant velocity move. Velocity out of range.",
    25: "Set Active Axis - Data out of range. Must be 1, 2, or 3.",
    26: "Set Axis Device Number - Data out of range. Must be between 0 and 254 inclusive.",
    27: "Set Axis Inversion - Data out of range. Must be 0, 1, or -1.",
    28: "Set Axis Velocity Profile - Data out of range. Must be 0, 1, 2, or 3.",
    29: "Set Axis Velocity Scale - Data out of range. Must be between 0 and 65535.",
    30: "Load Event-Triggered Instruction - Data out of range. See command #30 for valid range.",
    31: "Return Event-Triggered Instruction - Data out of range. See command #31 and #30 for valid range.",
    33: "Must be 0, 1, or 2.",
    36: "Peripheral ID is invalid or not supported.",
    37: "Microstep resolution not supported.",
    38: "Run current out of range.",
    39: "Hold current out of range.",
    40: "Set Device Mode - one or more of the mode bits is invalid.",
    41: "Home speed out of range. The range of home speed is determined by the resolution.",
    42: "Target speed out of range. The range of target speed is determined by the resolution.",
    43: "Target acceleration out of range.",
    44: "Maximum position out of range.",
    45: "Current position out of range.",
    46: "Max relative move out of range. Must be between 0 and 16,777,215.",
    47: "Home offset out of range.",
    48: "Alias out of range.",
    49: "Lock state must be 1 (locked) or 0 (unlocked).",
    50: "The device ID is not included in the firmware's list.",
    53: "Return Setting - data entered is not a valid setting command number. Valid data are the command numbers of any 'Set...' or 'Return...' instructions.",
    64: "Command number not valid in this firmware version or on this device.",
    65: "Set Park State - State must be 0 or 1, or device cannot park because it is in motion.",
    67: "High temperature is detected inside device. Device may be overheating.",
    68: "The requested digital input pin does not exist.",
    71: "The requested digital output pin does not exist.",
    74: "Write All Digital Outputs - A digital output pin which does not exist was set to 1.",
    76: "The requested analog input pin does not exist.",
    78: "Move Index - Target index invalid. See command #78 for valid range.",
    79: "Distance invalid. See command #79 for valid range.",
    80: "Valid range is 0 - 2147483647.",
    81: "Filter Holder ID must be 25 or 32.",
    87: "Force Absolute - Force data out of range.",
    101: "Mode must be 0 or 1.",
    102: "Mode must be 0 or 1.",
    103: "Status must be 0 or 1.",
    104: "Type must be 0 or 1.",
    105: "Mode must be 0 or 1.",
    106: "Minimum position out of range.",
    107: "Mode must be 0 or 1.",
    108: "Direction must be 0 or 1.",
    109: "Mode must be 0 or 1.",
    111: "Maximum knob speed out of range. The range of valid speed is determined by the resolution.",
    112: "Profile must be 1 (Linear), 2 (Quadratic), or 3 (Cubic).",
    113: "Acceleration out of range.",
    114: "Deceleration out of range.",
    115: "Mode must be 0 or 1.",
    116: "Mode must be 0 or 1.",
    117: "Valid range is 10 - 65535.",
    118: "Valid modes are 0-6.",
    119: "Valid range is 0(Off), 10 - 65535.",
    120: "Valid range is 0 - 65535.",
    121: "Direction must be 0 or 1.",
    122: "Set Baud Rate - Value not supported.",
    123: "Set Protocol - Value not supported.",
    124: "Set Baud Rate and ASCII Protocol - Value not supported.",
    255: "Another command is executing and cannot be pre-empted. Either stop the previous command or wait until it finishes before trying again.",
    701: "Write Register - Register address invalid.",
    702: "Write Register - Value out of range.",
    1600: "Save Current Position register out of range (must be 0-15).",
    1601: "Save Current Position is not allowed unless the device has been homed.",
    1700: "Return Stored Position register out of range (must be 0-15).",
    1800: "Move to Stored Position register out of range (must be 0-15).",
    1801: "Move to Stored Position is not allowed unless the device has been homed.",
    2146: "Move Relative (command 20) exceeded maximum relative move range. Either move a shorter distance, or change the maximum relative move (command 46).",
    3600: "Must clear Lock State (command 49) first. See the Set Lock State command for details.",
    4001: "Set Device Mode - bit 1 is reserved in this device and must be 0.",
    4002: "Set Device Mode - bit 2 is reserved in this device and must be 0.",
    4008: "Set Device Mode - this is a linear actuator; Disable Auto Home is used for rotary actuators only.",
    4010: "Set Device Mode - bit 10 is reserved in this device and must be 0.",
    4011: "Set Device Mode - bit 11 is reserved in this device and must be 0.",
    4012: "Set Device Mode - this device has integrated home sensor with preset polarity; mode bit 12 cannot be changed by the user.",
    4013: "Set Device Mode - bit 13 is reserved in this device and must be 0.",
    4014: "Set Device Mode - bit 14 is reserved in this device and must be 0.",
    4015: "Set Device Mode - bit 15 is reserved in this device and must be 0.",
    6501: "Device is currently parked. Use Set Park State or Home to unpark device before requesting a move.",
}

def get_error_description(code, default = None):
    return _ERROR_DESCRIPTIONS.get(code, default)


# Descriptions of the reply-only codes.
_REPLY_DESCRIPTIONS = \
{
    8: "Indicates to the user that the device has been set to a position tracking mode and given a move instruction.",
    9: "Indicates to the user that the device has reached one of the limits of travel.",
    10: "A reply that is sent when the manual control knob is turned in velocity mode.",
    11: "A knob manual move in Displacement mode has completed.",
    12: "A reply that is sent when the device is slipping.",
    13: "A reply that is sent when the device stops at a position different from the requested location.",
    255: "An error has occurred."
}

def get_reply_description(code, default = None):
    return _REPLY_DESCRIPTIONS.get(code, default)


# Descriptions of the data payload of reply-only codes.
_REPLY_DATA_DESCRIPTIONS = \
{
    8: "Position",
    9: "Position",
    10: "Position",
    11: "Position",
    12: "Position",
    13: "Position",
    255: "Error Code"
}

def get_reply_data_description(code, default = None):
    return _REPLY_DATA_DESCRIPTIONS.get(code, default)


# Descriptions of device capability by name.
_CAPABILITY_DESCRIPTIONS = \
{
    "coprocessor": "The board has a coprocessor to handle axis-specific tasks.",
	"debug-commands": "Gives access to firmware-only debug commands. Should never be on for public products.",
	"discrete-drive": "The board's motor driver uses Zaber's discrete-drive technology.",
	"lamp": "The board has drivers for illuminator lamps (*NOT* the usual indicator LEDs!).",
	"limit-c-connector": "The board has a connector for limit sensor C.",
	"limit-d-connector": "The board has a connector for limit sensor D.",
	"motion-driver": "The board can control motors.",
	"multiaxis": "The board supports multiple axes.",
	"usb": "The board has a USB interface.",
	"auto-detect": "The peripheral can be detected by a controller.",
	"binary-all": "The product supports all binary commands.",
	"controller": "The product is a stand-alone controller.",
	"cyclic": "The product produces cyclic motion, reaching the same position while travelling in one direction.",
	"encoder": "The product has some encoder.",
	"encoder-1": "The product has an encoder on port 1.",
	"encoder-1-direct": "The encoder on port 1 measures the end-effector position directly.",
	"encoder-1-motor": "The encoder on port 1 measures the motor position.",
	"encoder-2": "The product has an encoder on port 2.",
	"encoder-2-analog": "The encoder on port 2 is analog, not digital.",
	"encoder-2-direct": "The encoder on port 2 measures the end-effector position directly.",
	"encoder-2-motor": "The encoder on port 2 measures the motor position.",
	"encoder-direct": "The product has an encoder that measures the end-effector position directly.",
	"encoder-motor": "The product has an encoder that measures the motor position.",
	"force": "The product has force commands and can move with a specified force.",
	"io": "The product has user IO pins.",
	"io-di": "The product has user digital input pins.",
	"io-do": "The product has user digital output pins.",
	"io-ai": "The product has user analog input pins.",
	"io-ao": "The product has user analog output pins.",
	"knob": "The product has a knob.",
	"led": "The product has at least one LED.",
	"limit-away": "The product has an away limit sensor connected.",
	"limit-c": "The product has a limit C sensor connected.",
	"limit-d": "The product has a limit D sensor connected.",
	"motion": "The product has motors.",
	"motor-bldc": "The product has a brushless DC motor.",
	"motor-stepper": "The product has a stepper motor.",
	"motor-three-phase-direct": "The product has a three-phase direct-drive motor (whether rotary or linear).",
	"motor-voicecoil": "The product has a voicecoil motor.",
}

def get_capability_description(name, default = None):
    return _CAPABILITY_DESCRIPTIONS.get(name, default)

﻿# Utility classes used to generate C# enums in the Zaber Console build.

import os
import textwrap

class CSharpEnumFormatter(object):

    def __init__(self):
        self._usings = []
        self._indent = "\t"
        self._nl = os.linesep
        self._base = None

    @property
    def usings(self):
        return self._usings

    def add_using(self, namespace):
        if not isinstance(namespace, str):
            raise ValueError("namespace must be a string.")

        self._usings.append(namespace)

    @property
    def indent(self):
        return self._indent

    @indent.setter
    def indent_size(self, s):
        self._indent = s

    @property
    def base_class(self):
        return self._base

    @base_class.setter
    def base_class(self, value):
        self._base = value

    def generate_enum_text(self, builder, file):
        indentLevel = 0
        indent = self.indent * indentLevel

        # Output using statements.
        usings = self.usings
        if len(usings) > 0:
            for using in usings:
                file.write(f"using {using};{self._nl}")
            file.write(self._nl)

        # Open namespace.
        if len(builder.namespace or "") > 0:
            file.write(f"namespace {builder.namespace}{self._nl}")
            file.write(f"\u007b{self._nl}")
            indentLevel += 1
            indent = self.indent * indentLevel

        # Enum description comments.
        if len(builder.description or "") > 0:
            file.write(f"{indent}/// <summary>{self._nl}")
            for line in textwrap.wrap(builder.description, 75 - len(indent), break_long_words=False):
                file.write(f"{indent}/// {self._indent}{line}{self._nl}")
            file.write(f"{indent}/// </summary>{self._nl}")

        # Enum type name and opening brace.
        if len(builder.typename or "") < 1:
            raise ValueError("Enum must have a type name.")

        base = ""
        if self.base_class is not None:
            base = f" : {self.base_class}"

        file.write(f"{indent}public enum {builder.typename}{base}{self._nl}")
        file.write(f"{indent}\u007b{self._nl}")
        indentLevel += 1
        indent = self.indent * indentLevel

        # Enum values, ordered by numeric value then by name.
        numbers = sorted(builder.values, key=lambda v: (v.value, v.name))
        name_collisions = {}
        for val in numbers:
            nameLower = val.name.lower()
            count = name_collisions.get(nameLower, 0)
            count += 1
            name_collisions[nameLower] = count

        for i, val in enumerate(numbers):
            if (i > 0):
                file.write(self._nl)

            self._write_enum_value(val, file, indent, name_collisions[val.name.lower()] > 1)

        # Enum closing brace.
        indentLevel -= 1
        indent = self.indent * indentLevel
        file.write(f"{indent}\u007d{self._nl}")

        # Close namespace.
        if len(builder.namespace or "") > 0:
            file.write("}")
            file.write(self._nl)
            indentLevel -= 1
            indent = self.indent * indentLevel


    def _write_enum_value(self, value, file, indent, add_pragma):
        # Output value description.
        desc = value.description or ""
        if len(desc) > 0:
            file.write(f"{indent}/// <summary>{self._nl}")
            for line in textwrap.wrap(desc, 75 - len(indent), break_long_words=False):
                file.write(f"{indent}/// {self._indent}{line}{self._nl}")
            file.write(f"{indent}/// </summary>{self._nl}")

        # Output attributes.
        for attr in value.attributes or []:
            file.write(f"{indent}[{attr.name}({attr.value})]{self._nl}")

        # Output pragma if this is a legacy name for the value.
        if add_pragma:
            file.write(f"{indent}#pragma warning disable CS3005 // Identifer different only in case is not CLS-compliant.{self._nl}")

        # Output actual name and value.
        file.write(f"{indent}{value.name} = {value.value},{self._nl}")

        # Output pragma close if this is a legacy name for the value.
        if add_pragma:
            file.write(f"{indent}#pragma warning restore CS3005{self._nl}")

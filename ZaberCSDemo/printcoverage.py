from bs4 import BeautifulSoup

with open("coverage_data/index.htm", 'r') as myfile:
    file = myfile.read().replace('\n', '')
    soup = BeautifulSoup(file, "html.parser")
    rows = (soup.find('tbody').find_all('tr'))

    for row in rows:
        headers = row.find_all('th')
        if ((headers is not None) and (len(headers) > 0) and ("ine coverage" in headers[0].text.strip())):
            cols = row.find_all('td')
            cols = [ele.text.strip() for ele in cols]
            print("Line coverage: " + str(cols[0]))
            exit()

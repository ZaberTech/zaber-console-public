﻿open System.IO

if not (File.Exists(@"packages\FAKE.4.64.11\tools\FakeLib.dll")) then
    printfn "FAKE could not be found at \"packages\FAKE.4.64.11\tools\FakeLib.dll\". \
            Please run \"Install-Package FAKE\" from the Package Manager Console in Visual Studio."
    exit 1

#r @"packages\FAKE.4.64.11\tools\FakeLib.dll"
let NUnitPath = @"packages\NUnit.ConsoleRunner.3.9.0\tools\nunit3-console.exe"


open Fake
open Fake.AssemblyInfoFile
open Fake.OpenCoverHelper
open Fake.ReportGeneratorHelper
open Fake.Testing
open System


// Workaround for VS2017 compatibility.
// See https://github.com/fsharp/FAKE/issues/1442
let hasVS2022 = directoryExists @"C:\Program Files\Microsoft Visual Studio\2022\Community\Msbuild\Current\Bin"
if not hasVS2022 then
    printfn "VS 2022 not found!"
    exit 1

setBuildParam "MSBuild" @"C:\Program Files\Microsoft Visual Studio\2022\Community\Msbuild\Current\Bin\MSBuild.exe"

// TYPES
type BuildNumber = {
    major : int;
    minor : int;
    build : int;
    revision : int
}


// INITIAL SETUP

// Default version numbers; filled in with real values during publish and distribution targets.
let mutable newBuildNumber = {
    major = 0;
    minor = 0;
    build = 0;
    revision = 0;
}

let mutable revertedBuildNumber = {
    major = 0;
    minor = 0;
    build = 0;
    revision = 0;
}

let Utf8Signature = [|0xEFuy; 0xBBuy; 0xBFuy|]


// HELPER FUNCTIONS & SUGAR

// productVersion() and fullBuildVersion() (re-)calculate nice string
// representations of buildNumber from whatever the value of buildNumber
// is at the time they are called.
let productVersion buildNumber = sprintf "%d.%d.%d" buildNumber.major buildNumber.minor buildNumber.build
let fullBuildNumber buildNumber = sprintf "%s.%d" (productVersion buildNumber) buildNumber.revision

let BuildRelease x = MSBuildRelease null "Build" [x] |> ignore

// Clean and recClean retry "CleanDir" many times until it works in order to
// sidestep a problem with Windows where ownership of files isn't released
// fast enough. It's hacky, but seems to be the only way. Stupid Windows.
let rec recClean x n =
    if n % 100 = 0 then
        printfn "Encountered UnauthorizedAccessException %d times. Trying some more..." n
    try
        CleanDir x
        printfn "Success."
    with
        | :? System.UnauthorizedAccessException -> recClean x (n + 1)
let Clean x =
    try
        CleanDir x
    with
        | :? System.UnauthorizedAccessException -> recClean x 1

let CopyDirectory target source =
    ensureDirectory target
    CopyDir target source (fun _ -> true)


// This function replaces the "set-all-versions" target in the old script.
let SetVersionNumbers buildNumber =

    if buildNumber.major = 0 then failwith "Error: SetVersionNumbers called with major = 0."

    for project in ["../ScriptRunner/ScriptRunner";
                    "../ScriptRunner/ScriptRunnerTest";
                    "../Zaber";
                    "../ZaberTests";
                    "../ZaberConsoleGUI";
                    "../ZaberConsoleGUITests";
                    "../plugins/CycleDemo/CycleDemo";
                    "../plugins/GCode/Core";
                    "../plugins/GCode/Plugin";
                    "../plugins/GCode/PluginTester";
                    "../plugins/GCode/Tests";
                    "../plugins/SensorLogger";
                    "../plugins/ServoTunerPlugin";
                    "../plugins/ServoTunerTests";
                    "../plugins/OscilloscopePlugin";
                    "../plugins/OscilloscopePluginTests";
                    "../plugins/SimpleControls";
                    "../plugins/SimpleControlsTests"] do
        printfn "Changing version number of %s to %s." project (fullBuildNumber buildNumber)
        CreateCSharpAssemblyInfo (project + "/Properties/VersionInfo.cs") [Attribute.Version (fullBuildNumber buildNumber)]

let RunPython (args : string) =
    Shell.Exec ("py", "-3 " + args)
    |> ignore

let WGet (args : string) =
    Shell.Exec ("wget", args)
    |> ignore

let AddHeader (file : string) (header: byte[]) =
    let data = ReadFileAsBytes file
    let newData = Array.append header data
    WriteBytesToFile file newData

// TARGETS

// Updates the generated binary command and error enums from the device database.
// Re-run this and check in the results whenever commands or errors are added or their help text changes.
// Note for new commands you will need to add them to CommandDetails.xml first, and for new
// error codes you will have to add the help text and backward-compatible names to
// the GeneratebinaryErrorEnum.py script first.
Target "Generate-Code" (fun _ ->
    RunPython "GenerateBinaryEnums.py devices-public.sqlite --commands ../Zaber/Command.cs --replies ../Zaber/ZaberReply.cs --errors ../Zaber/ZaberError.cs"
    RunPython "GenerateCapabilities.py devices-public.sqlite --output ../Zaber/Capability.cs"
)

// Updates the offline command help files.
// Re-run this and check in the results whenever commands are added or their help text changes.
Target "Generate-Offline-Help" (fun _ ->

    // Remove previously downloaded files to prevent wget from creating .1 copies,and to get rid
    // of files no longer needed if any.
    !! "../ZaberConsoleGUI/Help/Manuals/FW6/ASCII/*"
    ++ "../ZaberConsoleGUI/Help/Manuals/FW6/Binary/*"
    ++ "../ZaberConsoleGUI/Help/Manuals/Joystick/*"
     |> DeleteFiles

    !! "../ZaberConsoleGUI/Help/Manuals/FW7"
     |> CleanDirs

    CreateDir "../ZaberConsoleGUI/Help/Manuals/FW7"

    // Download the FW6 protocol manuals and images.
    WGet "-nd -H -k -p --restrict-file-names=windows --no-check-certificate -e robots=off -P ../ZaberConsoleGUI/Help/Manuals/FW6/ASCII \"http://www.zaber.com/wiki/Manuals/ASCII_Protocol_Manual?action=render\""
    WGet "-nd -H -k -p --restrict-file-names=windows --no-check-certificate -e robots=off -P ../ZaberConsoleGUI/Help/Manuals/FW6/Binary \"http://www.zaber.com/wiki/Manuals/Binary_Protocol_Manual?action=render\""
    WGet "-nd -H -k -p --restrict-file-names=windows --no-check-certificate -e robots=off -P ../ZaberConsoleGUI/Help/Manuals/Joystick \"http://www.zaber.com/wiki/Software/Zaber_Console/Joystick_Setup_Tab?action=render\""

    // Download and unzip the FW7 protocol manual content.
    WGet "--no-check-certificate -e robots=off  -P ../ZaberConsoleGUI/Help/Manuals/FW7/ \"https://www.zaber.com/software/protocol-manual/v1/dist.zip\""
    ZipHelper.Unzip "../ZaberConsoleGUI/Help/Manuals/FW7/" "../ZaberConsoleGUI/Help/Manuals/FW7/dist.zip"
    DeleteFile "../ZaberConsoleGUI/Help/Manuals/FW7/dist.zip"

    // Remove robots.txt and the old fixed-up copies.
    !! "../ZaberConsoleGUI/Help/Manuals/FW6/ASCII/robots.txt"
    ++ "../ZaberConsoleGUI/Help/Manuals/FW6/Binary/robots.txt"
    ++ "../ZaberConsoleGUI/Help/Manuals/Joystick/robots.txt"
    ++ "../ZaberConsoleGUI/Help/Manuals/FW6/ASCII/ASCII_Protocol_Manual.html"
    ++ "../ZaberConsoleGUI/Help/Manuals/FW6/Binary/Binary_Protocol_Manual.html"
    ++ "../ZaberConsoleGUI/Help/Manuals/Joystick/Joystick_Setup_Tab.html"
     |> DeleteFiles

    // Fix the downloaded filenames.
    Rename "../ZaberConsoleGUI/Help/Manuals/FW6/ASCII/ASCII_Protocol_Manual.html" "../ZaberConsoleGUI/Help/Manuals/FW6/ASCII/ASCII_Protocol_Manual@action=render"
    Rename "../ZaberConsoleGUI/Help/Manuals/FW6/Binary/Binary_Protocol_Manual.html" "../ZaberConsoleGUI/Help/Manuals/FW6/Binary/Binary_Protocol_Manual@action=render"
    Rename "../ZaberConsoleGUI/Help/Manuals/Joystick/Joystick_Setup_Tab.html" "../ZaberConsoleGUI/Help/Manuals/Joystick/Joystick_Setup_Tab@action=render"

    // Fix inner URLs in the main files.
    ReplaceInFiles [("@action=render",".html")] ["../ZaberConsoleGUI/Help/Manuals/FW6/ASCII/ASCII_Protocol_Manual.html";
"../ZaberConsoleGUI/Help/Manuals/FW6/Binary/Binary_Protocol_Manual.html";
"../ZaberConsoleGUI/Help/Manuals/Joystick/Joystick_Setup_Tab.html"]

    // Add UTF-8 signature bytes to start of HTML files.
    AddHeader "../ZaberConsoleGUI/Help/Manuals/FW6/ASCII/ASCII_Protocol_Manual.html" Utf8Signature
    AddHeader "../ZaberConsoleGUI/Help/Manuals/FW6/Binary/Binary_Protocol_Manual.html" Utf8Signature
    AddHeader "../ZaberConsoleGUI/Help/Manuals/Joystick/Joystick_Setup_Tab.html" Utf8Signature
)

// Updates the default device database in Zaber Console.
// Note this is not done automatically for the "dist" targets; you must manually
// run Update-Database or Update-Database-Dev before using a Dist-* target.
Target "Update-Database" (fun _ ->
    RunPython "GetDeviceDatabase.py -e public -d ../ZaberConsoleGUI/devices.sqlite"
)

Target "Update-Database-Dev" (fun _ ->
    RunPython "GetDeviceDatabase.py -e master -d ../ZaberConsoleGUI/devices.sqlite"
)


// Runs MSBuild to build the project. Basically does the same thing as the
// "build" button in Visual Studio.
Target "Build" (fun _ ->
    BuildRelease "ZaberCSDemo.sln"
)

// Runs MSBuild to build the plugin projects. Basically does the same thing as the
// "build" button in Visual Studio.
// TODO (Soleil 2018-02-23): Add sln files for the oscilloscope and servo tuner and add them to this list, or consider removing this target and the existing sln files.
Target "Build-Plugins" (fun _ ->
    BuildRelease "../plugins/CycleDemo/CycleDemo.sln"
    BuildRelease "../plugins/GCode/GCode.sln"
    BuildRelease "../plugins/SensorLogger/SensorLogger.sln"
    BuildRelease "../plugins/SimpleControls/SimpleControlsPlugin.sln"
)

// Builds a .chm file out of our Sandcastle project. The .chm contains a fullBuildNumber
// API reference to the Zaber libraries.
Target "Build-Zaber-Help" (fun _ ->
    BuildRelease "Zaber.shfbproj"
    CopyFile "../ZaberConsoleGUI/Help/SDKs/Zaber.chm" "../Zaber/Help/Zaber.chm"
)

Target "Build-Toolbox-Help" (fun _ ->
    BuildRelease "ZaberWpfToolbox.shfbproj"
    CopyFile "../ZaberConsoleGUI/Help/SDKs/ZaberWpfToolbox.chm" "../ZaberWpfToolbox/Help/ZaberWpfToolbox.chm"
)

Target "Build-Help" (fun _ -> ()) // Dummy to depend on individual help file builds.

Target "Test-All" (fun _ -> ()) // Dummy to run all tests.

// Lightweight clean of just the release solution.
Target "Clean" (fun _ ->
    MSBuildRelease null "Clean" ["ZaberCSDemo.sln"] |> ignore
)

// Cleans out all build products
// Note this will fail if anything (such as VS Explorer or the hosting process)
// has a lock on any of the files or directories.
Target "Clean-All" (fun _ ->
    MSBuildRelease null "Clean" ["ZaberCSDemo.sln"] |> ignore
    MSBuildDebug null "Clean" ["ZaberCSDemo.sln"] |> ignore

    !! "../dist"
    ++ "../package"
    ++ "Debug"
    ++ "../**/bin"
    ++ "../**/obj"
    ++ "../**/coverage"
    ++ "../**/_generated"
     |> DeleteDirs

    !! "../Zaber/Help/*.log"
    ++ "../ZaberWpfToolbox/Help/*.log"
     |> DeleteFiles
)


// Called during distibution builds to set assembly version numbers. This uses some special git commands
// to generate a fake build number using the git revision history of the current branch. In order to use
// the Publish and Dist targets you must have git and wget for windows installed and in your path, and
// be working from a git repo - this will not work from the old source code zip.
// You will also need to add these aliases to your .gitconfig file:
// [alias]
//    show-rev-number = !sh -c 'git rev-list HEAD | nl | awk \"{ if(\\$1 == "$0") { print \\$2 }}\"'
//    show-rev-list = !sh -c 'git rev-list HEAD | nl'
//    show-current-rev = !sh -c 'git rev-list HEAD | wc -l'
Target "Get-Version-Numbers" (fun _ ->
    // Get the base build number from the text file.
    let _tokens = (ReadFileAsString "buildnumber.txt"
        |> split '.' |> List.map Int32.Parse
    )

    // Get the revision number from Subversion.
    // There's probably an easier way to do this than a big XPath,
    // but this was how the old build script did it.
    // Let's not reinvent a working wheel,
    // even if it is powered by magic and hamsters.
    RunPython "git_status.py" |> ignore
    newBuildNumber <- {
      major = _tokens.[0];
      minor = _tokens.[1];
      build = _tokens.[2];
      revision = Int32.Parse(ReadLine "git_status.txt");
    }
    DeleteFile "git_status.txt"

    revertedBuildNumber <- {
      major = _tokens.[0];
      minor = _tokens.[1];
      build = 0;
      revision = 0;
    }

    if newBuildNumber.major = 0 then failwith "Error: Failed to read buildnumber.txt"
)


Target "Apply-Version-Number" (fun _ ->
    // Set the version number of all projects to the number from buildnumber.txt and git.
    SetVersionNumbers newBuildNumber
)


Target "Revert-Version-Number" (fun _ ->
    // Reset the version number to x.x.0.0 - actual released version numbers should not be in the code.
    SetVersionNumbers revertedBuildNumber
)

// Enable or disable code signing or stong naming in all project files.
// For this to work propery with Zaber Console, ALL AND ONLY the project files that
// should be strong named must have the SignAssembly property already in place and
// a valid reference to a strong naming key file outside the repository, and
let SetCodeSignEnable (state: bool) =

    let signStr = "<SignAssembly>" + state.ToString().ToLower() + "</SignAssembly>"
    !! "../**/**.csproj"
    |> RegexReplaceInFilesWithEncoding @"\<SignAssembly\>[a-zA-Z]*\<\/SignAssembly\>"
                                       signStr
                                       System.Text.Encoding.UTF8



Target "Enable-Code-Signing" (fun _ ->
    SetCodeSignEnable true
)


Target "Disable-Code-Signing" (fun _ ->
    SetCodeSignEnable false
)


// Helper for the Publish target. Builds and stages one edition of the program for the InnoSetup installer.
let publish_innosetup (aProductName : string) (aAssemblyName : string) (aDefines : string) (aInstallUrl : string) (aIconPath : string) (aPublishSuffix : string) =
    // Set icon.
    CopyFile "../ZaberConsoleGui/Zaber.ico" aIconPath

    // Publish Inno Setup package.
    MSBuild
        null
        "publish"
        ([
            ("Configuration", "Release");
            ("MapFileExtensions", "false");
            ("ApplicationVersion", (fullBuildNumber newBuildNumber));
            ("Verbosity", "minimal");
            ("ProductName", aProductName);
            ("AssemblyNameOverride", aAssemblyName);
            ("InstallUrl", aInstallUrl);
            ("DefineConstants", aDefines)
        ])
        ["../ZaberConsoleGUI/ZaberConsoleGUI.csproj"]
          |> ignore

    CopyDirectory ("../ZaberConsoleGUI/bin/Release/app.publish." + aPublishSuffix) "../ZaberConsoleGUI/bin/Release/app.publish"
    Clean "../ZaberConsoleGUI/bin/Release/app.publish"

    // Restore default icon.
    CopyFile "../ZaberConsoleGui/Zaber.ico" "../ZaberWpfToolbox/Resources/Icons/ZaberConsole_cyan.ico"


// Builds stable release files ready to package in installers.
Target "Publish-Stable" (fun _ ->
    publish_innosetup "Zaber Console" "ZaberConsole" "" "http://www.zaber.com/support/software.php?installer=inno" "../ZaberWpfToolbox/Resources/Icons/ZaberConsole_cyan.ico" "prod"
)

// Builds dev release files ready to package in installers.
Target "Publish-Dev" (fun _ ->
    publish_innosetup "Zaber Console (Inno Dev)" "ZaberConsoleDev" "DEV" "http://www.zaber.com/support/software.php?installer=inno&version=dev" "../ZaberWpfToolbox/Resources/Icons/ZaberConsole_white.ico" "dev"
)

// Builds nightly dev release files ready to package in installers.
Target "Publish-NightlyDev" (fun _ ->
    publish_innosetup "Zaber Console (Nightly Dev)" "ZaberConsoleNightlyDev" "DEV,NIGHTLY" "https://www.zaber.com/support/software-downloads.php?product=zaber_console_nightly_dev_installer&version=latest" "../ZaberWpfToolbox/Resources/Icons/ZaberConsole_nightly.ico" "nightlydev"
)

// Builds installer test release files ready to package in installers.
Target "Publish-InstallTest" (fun _ ->
    publish_innosetup "Zaber Console (INSTALL TEST)" "ZaberConsoleInstallTest" "TEST,INSTALLTEST" "http://www.zaber.com/support/software.php?installer=inno&version=test" "../ZaberWpfToolbox/Resources/Icons/ZaberConsole_black.ico" "test"
)

// Builds alpha release files ready to package in installers.
Target "Publish-Alpha" (fun _ ->
    publish_innosetup "Zaber Console (INTERNAL ALPHA)" "ZaberConsoleInternalAlpha" "TEST,ALPHA" "https://www.zaber.com/support/software-downloads.php?product=zaber_console_Alpha_InnoSetup_installer&version=latest" "../ZaberWpfToolbox/Resources/Icons/ZaberConsole_red.ico" "alpha"
)


Target "Publish" (fun _ -> ()) // Dummy to run all publish targets (see dependencies at bottom).


// Build installer for the stable edition.
// Copies files created by the "Publish" target into the "dist" folder.
// NOTE that for any of the Dist targets to work you must have the private
// software-keys repository checked out under ZaberCSDemo/packages/git/
// as the .csproj files contain relative paths to the strong naming key and code
// signing certificate files.
Target "Dist-Stable" (fun _ ->

    // Clean out directories
    !! "../dist/ZaberConsole"
      |> DeleteDirs

    CreateDir "../dist/ZaberConsole"

    // Build the Inno Setup installer.
    let ecode = Shell.Exec("../lib/InnoSetup5/ISCC.exe", "/Dstable ../Installer/zaber_console_installer.iss")
    if (ecode <> 0) then failwith "InnoSetup compiler failed."

    // Delete Inno Setup installer input to keep the bin folder in the zip clean.
    !! "../package/ZaberCSDemo/bin/app.publish.prod"
      |> DeleteDirs
)


// Build installers for the dev edition.
// Copies files created by the "Publish" target into the "dist" folder.
Target "Dist-Dev" (fun _ ->

    // Clean out directories
    !! "../dist/ZaberConsoleDev"
      |> DeleteDirs

    CreateDir "../dist/ZaberConsoleDev"

    // Build the Inno Setup installer.
    let ecode = Shell.Exec("../lib/InnoSetup5/ISCC.exe", "/Ddev ../Installer/zaber_console_installer.iss")
    if (ecode <> 0) then failwith "InnoSetup compiler failed."

    // Delete Inno Setup installer input to keep the bin folder in the zip clean.
    !! "../package/ZaberCSDemo/bin/app.publish.dev"
      |> DeleteDirs
)


// Build installers for the installer test edition.
// Copies files created by the "Publish" target into the "dist" folder.
Target "Dist-InstallTest" (fun _ ->

    // Clean out directories
    !! "../dist/ZaberConsoleInstallTest"
      |> DeleteDirs

    CreateDir "../dist/ZaberConsoleInstallTest"

    // Build the Inno Setup installer.
    let ecode = Shell.Exec("../lib/InnoSetup5/ISCC.exe", "/Dinstalltest ../Installer/zaber_console_installer.iss")
    if (ecode <> 0) then failwith "InnoSetup compiler failed."

    // Delete Inno Setup installer input to keep the bin folder in the zip clean.
    !! "../package/ZaberCSDemo/bin/app.publish.test"
      |> DeleteDirs
)


// Build installers for the internal alpha test edition.
// Copies files created by the "Publish" target into the "dist" folder.
Target "Dist-Alpha" (fun _ ->

    // Clean out directories
    !! "../dist/ZaberConsoleInternalAlpha"
      |> DeleteDirs

    CreateDir "../dist/ZaberConsoleInternalAlpha"

    // Build the Inno Setup installer.
    let ecode = Shell.Exec("../lib/InnoSetup5/ISCC.exe", "/Dinternalalpha ../Installer/zaber_console_installer.iss")
    if (ecode <> 0) then failwith "InnoSetup compiler failed."

    // Delete Inno Setup installer input to keep the bin folder in the zip clean.
    !! "../package/ZaberCSDemo/bin/app.publish.alpha"
      |> DeleteDirs
)


// Build installers for the nightly dev test edition.
// Copies files created by the "Publish" target into the "dist" folder.
Target "Dist-NightlyDev" (fun _ ->

    // Clean out directories
    !! "../dist/ZaberConsoleNightlyDev"
      |> DeleteDirs

    CreateDir "../dist/ZaberConsoleNightlyDev"

    // Build the Inno Setup installer.
    let ecode = Shell.Exec("../lib/InnoSetup5/ISCC.exe", "/Dnightlydev ../Installer/zaber_console_installer.iss")
    if (ecode <> 0) then failwith "InnoSetup compiler failed."

    // Delete Inno Setup installer input to keep the bin folder in the zip clean.
    !! "../package/ZaberCSDemo/bin/app.publish.nightlydev"
      |> DeleteDirs
)


let RunNUnit (aTestPath : string) =
    "test-results.txt" |> DeleteFile
    try
        NUnit3 (fun p -> { p with ToolPath=NUnitPath; Labels=All; ResultSpecs=["test-results.txt;transform=text-report.xslt"] }) [aTestPath] |> ignore
    with
        | :? Fake.UnitTestCommon.FailedTestsException ->
            ReadFileAsString "test-results.txt" |> printfn "%s"
            reraise()


Target "Test" (fun () ->
    RunNUnit "../ZaberTests/ZaberTests.nunit"
    RunNUnit "../ZaberWpfToolboxTests/ZaberWpfToolboxTests.nunit"
    RunNUnit "../ZaberConsoleGUITests/ZaberConsoleGUITests.nunit"
    ()
)

Target "Test-Plugins" (fun () ->
    RunNUnit "../plugins/GCode/Tests/Tests.nunit"
    RunNUnit "../plugins/SimpleControlsTests/SimpleControlsTests.nunit"
    RunNUnit "../plugins/ServoTunerTests/ServoTunerTests.nunit"
    RunNUnit "../plugins/OscilloscopePluginTests/OscilloscopePluginTests.nunit"
    ()
)


let CoverageTest (aOutDir : string) (aNameSpaces : string list) (aAssemblies: string list) =
    CreateDir aOutDir
    let path = aOutDir + "TestResults.xml"
    let args = String.Join(" ", aAssemblies)
    let filters = String.Join(" ", [|for ns in aNameSpaces -> "+[*" + ns + "*]*" |]) + " -[*OxyPlot*]* -[*Test*]* -[*]System.* -[*]XamlGenerated*"
    OpenCover (fun p -> { p with
                              ExePath = "packages/OpenCover.4.6.519/tools/OpenCover.Console.exe"
                              TestRunnerExePath = NUnitPath
                              Output = path
                              Register = Register
                              Filter = filters
                              TimeOut = (TimeSpan(0, 20, 0))
                              OptionalArguments = "-returntargetcode"
                        }
              ) args

    ReportGenerator (fun p -> { p with
                                  ExePath = "packages/ReportGenerator.3.1.2/tools/reportgenerator.exe"
                                  TargetDir = aOutDir
                              }
                    ) [ path ]


Target "Test-Coverage" (fun () ->
    let namespaces = ["Oscilloscope"; "ServoTuner"; "SimpleControls"; "ZaberConsole"; "WpfToolbox"; "Zaber"; "GCode"]
    let testAssemblies = [
        "../lib/Zaber.Units.Tests/bin/release/Zaber.Units.Tests.dll";
        "../lib/Zaber.Tuning.Tests/bin/release/Zaber.Tuning.Tests.dll";
        "../ZaberTests/bin/release/ZaberTests.dll";
        "../ZaberWpfToolboxTests/bin/release/ZaberWpfToolboxTests.dll";
        "../plugins/GCode/Tests/bin/release/Zaber.GCode.Tests.dll";
        "../ZaberConsoleGUITests/bin/release/ZaberConsoleGUITests.dll";
        "../ScriptRunner/ScriptRunnerTest/bin/release/ScriptRunnerTest.dll";
        "../plugins/SimpleControlsTests/bin/release/SimpleControlsTests.dll";
        "../plugins/SimpleControlsTests/bin/release/SimpleControlsTests.dll";
        "../plugins/OscilloscopePluginTests/bin/release/OscilloscopePluginTests.dll"; ]

    !! "coverage_data"
      |> DeleteDirs

    CoverageTest "coverage_data/" namespaces testAssemblies

    RunPython "printcoverage.py"
)


// DEPENDENCIES
// Syntax: http://fsharp.github.io/FAKE/apidocs/fake-additionalsyntax.html
// x ==> y means y depends on x and therefore x must run before y.
// x ?=> y means that if x is present, it must run before y but there is no dependency.
// x <=> y means y is dependent on all dependencies of x but does not imply a dependency between x and y.
// x =?> (y, condition) means y is dependent on x if the condition is true.

// Dependency chain for running tests.
"Build" ==> "Test"
"Build" ==> "Test-Coverage"
"Build" ==> "Build-Plugins"
"Build-Plugins" ==> "Test-Plugins"
"Test" ==> "Test-All"
"Test-Plugins" ==> "Test-All"

// Dependency chain for help file generation.
"Build" ==> "Build-Zaber-Help"
"Build" ==> "Build-Toolbox-Help"
"Build-Zaber-Help" ==> "Build-Help"
"Build-Toolbox-Help" ==> "Build-Help"

// Dependency chain for packaging for release. Applies version number from buildnumber.txt and then reverts it.
"Get-Version-Numbers" ==> "Apply-Version-Number"
"Apply-Version-Number" ?=> "Build"
"Apply-Version-Number" ==> "Revert-Version-Number"

// Dependency chain for code signing. Enables it before build when doing any dist target.
"Enable-Code-Signing" ?=> "Build"
"Enable-Code-Signing" ==> "Disable-Code-Signing"

// All of the publish targets require a build first. The build should only happen once though.
"Build-Plugins" ==> "Publish-Stable"
"Build-Plugins" ==> "Publish-Dev"
"Build-Plugins" ==> "Publish-NightlyDev"
"Build-Plugins" ==> "Publish-InstallTest"
"Build-Plugins" ==> "Publish-Alpha"

// The publish all target causes all the edition publish targets to run.
"Publish-Stable" ==> "Publish"
"Publish-Dev" ==> "Publish"
"Publish-NightlyDev" ==> "Publish"
"Publish-InstallTest" ==> "Publish"
"Publish-Alpha" ==> "Publish"

// Each dist target depends on its corresponding publish target.
"Publish-Stable" ==> "Dist-Stable"
"Publish-Dev" ==> "Dist-Dev"
"Publish-InstallTest" ==> "Dist-InstallTest"
"Publish-Alpha" ==> "Dist-Alpha"
"Publish-NightlyDev" ==> "Dist-NightlyDev"

// The revert version number target must come after all publish targets are finished.
"Publish-Stable" ?=> "Revert-Version-Number"
"Publish-Dev" ?=> "Revert-Version-Number"
"Publish-NightlyDev" ?=> "Revert-Version-Number"
"Publish-InstallTest" ?=> "Revert-Version-Number"
"Publish-Alpha" ?=> "Revert-Version-Number"

// Revert version number and code signing after the publish step.
"Revert-Version-Number" ==> "Dist-Stable"
"Revert-Version-Number" ==> "Dist-Dev"
"Revert-Version-Number" ==> "Dist-InstallTest"
"Revert-Version-Number" ==> "Dist-Alpha"
"Revert-Version-Number" ==> "Dist-NightlyDev"
"Disable-Code-Signing" ==> "Dist-Stable"
"Disable-Code-Signing" ==> "Dist-Dev"
"Disable-Code-Signing" ==> "Dist-InstallTest"
"Disable-Code-Signing" ==> "Dist-Alpha"
"Disable-Code-Signing" ==> "Dist-NightlyDev"

// Start build
RunTargetOrDefault "Test-Coverage"

traceEndBuild ()

﻿# Utility classes used to generate C# enums in the Zaber Console build.

class EnumValueAttribute(object):
    def __init__(self, name, value):
        self._name = name
        self._value = value

    @property
    def name(self):
        return self._name

    @property
    def value(self):
        return self._value


class EnumValue(object):
    def __init__(self, name, value):
        self._name = name
        self._value = value
        self._attributes = []
        self._description = None

    @property
    def name(self):
        return self._name

    @property
    def value(self):
        return self._value
    
    @property
    def attributes(self):
        return self._attributes

    @property
    def description(self):
        return self._description

    @description.setter
    def description(self, desc):
        self._description = desc

    def add_attribute(self, attr):
        if not isinstance(attr, EnumValueAttribute):
            raise ValueError("attr must be an EnumValueAttribute.")

        self._attributes.append(attr)



class EnumBuilder(object):

    def __init__(self, enum_name, namespace = None):
        self._namespace = namespace
        self._typename = enum_name
        self._values = []
        self._description = None
        # Possible future extension: add attributes to the enum itself.

    @property
    def namespace(self):
        return self._namespace

    @property
    def typename(self):
        return self._typename

    @property
    def description(self):
        return self._description

    @description.setter
    def description(self, desc):
        self._description = desc

    @property
    def values(self):
        return self._values

    def add_value(self, value):
        if not isinstance(value, EnumValue):
            raise ValueError("value must be an EnumValue.")

        self._values.append(value)

    def find_names(self, code):
        return [v.name for v in self._values if code == v.value]

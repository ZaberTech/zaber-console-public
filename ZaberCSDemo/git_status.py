﻿import os

revision = int(os.environ.get("CI_PIPELINE_ID") or "0")
revision = revision % 65536
print(f"Revision number: {revision}")
with open("git_status.txt", "w") as fp:
    fp.write(str(revision))

﻿import glob
import os
import re

def locate_database(search_dir: str, file_name: str):
    candidate = os.path.join(search_dir, file_name)
    if os.path.isfile(candidate):
        return candidate
 
    highest = 0 
    highest_path = None 
    for name in glob.glob(os.path.join(search_dir, "*")):
        match = re.match(r".*[/\\](\d+)$", name)
        if match and os.path.isdir(name) and int(match[1]) > highest:
            candidate = os.path.join(name, file_name)
            if os.path.isfile(candidate):
                highest = int(match[1])
                highest_path = candidate
 
    return highest_path

﻿# This is a Python3 script that regenerates the ASCII capability names 
# in Zaber.dll from content in the device databases.

import argparse
import logging
import pystache
import sqlite3

import FindDeviceDatabase as finder
import SemanticDatabase as semantics

BASE_DIR = "Z:\Product\FW\device-database"

def parse_command_line():
    parser = argparse.ArgumentParser(description="Tool to generate C# constant names for device capabilities.")
    parser.add_argument("dbfile", type=str, help="Name of the device database file to use.")
    parser.add_argument("--output", type=str, default=None, help="Path to the file to write output to, which will be overwritten. Omit to output to the console.")
    return parser.parse_args()


def main(args):
    dbfile = finder.locate_database(BASE_DIR, args.dbfile)
    logging.info(f"Generating capability names from {dbfile}...")
    capNames = read_capabilities_from_database(dbfile)

    items = []
    
    for name in capNames:
        code_name = code_safe_name(capitalize_each_word(name))
        items.append(\
        {
            "name": name,
            "code_name": code_name,
            "description": semantics.get_capability_description(name) or code_name
        })

    data = \
    {
        "class_name": "Capability",
        "class_description": "Constants representing device capability names, for use with <see cref=\"DeviceType.Capabilities\"/>.",
        "namespace": "Zaber",
        "items": sorted(items, key=lambda item: item["code_name"])
    }

    output(data, args.output)
    logging.info("Done!")


def capitalize_first_letter(word):
    return word[:1].capitalize() + word[1:]


def capitalize_each_word(sentence):
    return " ".join([capitalize_first_letter(word) for word in sentence.split("-")])


def code_safe_name(s):
    return s.replace(" ", "").replace("-", "")


def read_capabilities_from_database(db_path):
    logging.debug(f"Reading capability data from { str(db_path) }...")
    connection = sqlite3.connect(db_path)
    connection.row_factory = sqlite3.Row
    cursor = connection.cursor()

    cursor.execute("SELECT DISTINCT Capability FROM Console_Capabilities2;")
    rows = cursor.fetchall()

    return [row["Capability"] for row in rows]


def generate_cs_constants(class_data, template_file_name):
    logging.debug("Generating code...")

    with open(template_file_name, "rt") as fp:
        return pystache.render(fp.read(), class_data)


def output(data, dest):
    text = generate_cs_constants(data, "constants_class.cs.mustache")
    if (dest):
        with open(dest, "wb") as fp:
            fp.write(text.encode("utf-8"))
    else:
        print(text)


if ("__main__" == __name__):
    logging.basicConfig(level = 20)
    main(parse_command_line())

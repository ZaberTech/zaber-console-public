﻿using System;

namespace Zaber.Tuning
{
	/// <summary>
	///     Attribute to be used on tuner calculator classes to enable matching them
	///     against control modes supported by defaults.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
	public class TunerSelectionAttribute : Attribute
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Initialize the attribute with a single mode name.
		/// </summary>
		/// <param name="aAlgorithmName">Name of the tuning algorithm, including its version if there are more than one variation.</param>
		/// <param name="aInputModeName">Name of the mode supported. If null or empty, it is assumed no input data is needed.</param>
		/// <param name="aInputVersion">Version of the input mode supported.</param>
		/// <param name="aOutputModeName">Name of the output mode supported.</param>
		/// <param name="aOutputVersion">Version of the output mode supported.</param>
		public TunerSelectionAttribute(string aAlgorithmName, string aInputModeName, int aInputVersion,
									   string aOutputModeName, int aOutputVersion)
		{
			AlgorithmName = aAlgorithmName;

			if (string.IsNullOrWhiteSpace(aOutputModeName))
			{
				throw new ArgumentException(nameof(aOutputModeName) + " must contain non-whitespace.");
			}

			InputModeName = aInputModeName;
			OutputModeName = aOutputModeName;
			InputVersion = aInputVersion;
			OutputVersion = aOutputVersion;
		}


		/// <summary>
		///     Name for the tuning algorithm. This is arbitrary and serves to differentiate both
		///     different approaches to tuning and different versions of the same approach. This
		///     string is used to match tuning guidance data from the device database with the code
		///     that knows how to consume the data and produce output relevant to the device.
		///     This can be null if not relevant.
		/// </summary>
		public string AlgorithmName { get; set; }


		/// <summary>
		///     Name of the tuning mode of the output parameters. This is an alias for the
		///     set of parameters produced by the tuner, and may be either a firmware controller
		///     name or a name for a user-input parameter set from software.
		/// </summary>
		public string InputModeName { get; set; }


		/// <summary>
		///     Controller or software user interface version number for the input data.
		///     This is used to distinguish between breaking changes in implementations of the
		///     same basic algorithm.
		/// </summary>
		public int InputVersion { get; set; }


		/// <summary>
		///     Name of the tuning mode of the output parameters. This is an alias for the
		///     set of parameters produced by the tuner, and may be either a firmware controller
		///     name or a name for a user-input parameter set from software.
		/// </summary>
		public string OutputModeName { get; set; }


		/// <summary>
		///     Controller or software user interface version number for the output data.
		///     This is used to distinguish between breaking changes in implementations of the
		///     same basic algorithm.
		/// </summary>
		public int OutputVersion { get; set; }

		#endregion
	}
}

﻿using System.Collections.Generic;
using System.Reflection;

namespace Zaber.Tuning
{
	/// <summary>
	///     Hardcoded names of known Zaber tuning modes.
	/// </summary>
	public static class TuningModeNames
	{
		#region -- Public data --

		/// <summary>
		///     Name for the first feedforward PID tuning mode present in Zaber firmware 7.
		/// </summary>
		public const string FFPID = "ffpid";


		/// <summary>
		///     Name for PID standard form. (eq. 10.13, page 308, "Feedback Systems:
		///     An Introduction for Scientists and Engineers" by Astrom and Murray).
		///     Second order, continuous time, complex poles, damping fixed at 1/sqrt(2),
		///     tuned via Ti time constant, two zeroes at Nyquist, backwards differencing.
		/// </summary>
		public const string USERPID = "User PID";

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Returns a list of all constant strings defined in this class. Order is not guaranteed.
		/// </summary>
		public static string[] All
		{
			get
			{
				if (null == _all)
				{
					var temp = new List<string>();

					foreach (var field in typeof(TuningModeNames).GetFields(BindingFlags.Public | BindingFlags.Static))
					{
						if (field.IsLiteral && !field.IsInitOnly)
						{
							var s = field.GetValue(null) as string;
							temp.Add(s);
						}
					}

					_all = temp.ToArray();
				}

				return _all;
			}
		}

		#endregion

		#region -- Data --

		private static string[] _all;

		#endregion
	}
}

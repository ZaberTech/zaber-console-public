﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Zaber.Tuning.Servo;

namespace Zaber.Tuning
{
	/// <summary>
	///     Aids identification and selection of tuning algorithms according to algorithm
	///     names, input and output data set names and versions thereof.
	/// </summary>
	public class TunerSelector
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Identify and register all tuner types in a given assembly. The types found
		///     will become available via the query methods of this class.
		/// </summary>
		/// <param name="aAssembly">Assembly to scan for types. Each assembly should only be scanned once.</param>
		/// <returns>
		///     A list of concrete implementations of <see cref="ITuner" /> found in the given assembly,
		///     that have instances of <see cref="TunerSelectionAttribute" /> attached.
		/// </returns>
		public IEnumerable<Type> RegisterTunerTypes(Assembly aAssembly)
		{
			var result = new List<Type>();
			foreach (var t in aAssembly.GetExportedTypes())
			{
				if (!t.IsAbstract)
				{
					if (typeof(ITuner).IsAssignableFrom(t))
					{
						_tunerTypeSupportedOutputModes[t] = new List<Tuple<string, int>>();
						var hasAttribute = false;

						foreach (var attr in t.GetCustomAttributes(typeof(TunerSelectionAttribute), true))
						{
							hasAttribute = true;

							var tsa = attr as TunerSelectionAttribute;
							if (!_tunerTypesByMode.ContainsKey(tsa.OutputModeName))
							{
								_tunerTypesByMode[tsa.OutputModeName] = new List<Type>();
							}

							_tunerTypesByMode[tsa.OutputModeName].Add(t);

							if (!_tunerTypeSupportedInputModes.ContainsKey(t))
							{
								_tunerTypeSupportedInputModes[t] = new List<Tuple<string, int>>();
								_tunerTypeSupportedOutputModes[t] = new List<Tuple<string, int>>();
							}

							_tunerTypeSupportedInputModes[t]
							.Add(new Tuple<string, int>(tsa.InputModeName, tsa.InputVersion));
							_tunerTypeSupportedOutputModes[t]
							.Add(new Tuple<string, int>(tsa.OutputModeName, tsa.OutputVersion));

							if (!string.IsNullOrEmpty(tsa.AlgorithmName))
							{
								if (!_tunerTypesByAlgorithm.ContainsKey(tsa.AlgorithmName))
								{
									_tunerTypesByAlgorithm[tsa.AlgorithmName] = new List<Type>();
								}

								_tunerTypesByAlgorithm[tsa.AlgorithmName].Add(t);
							}
						}

						if (hasAttribute)
						{
							result.Add(t);
						}
					}
				}
			}

			return result;
		}


		/// <summary>
		///     Report all known tuner types that implement a given tuning algorithm, and optionally that
		///     also accept input data for one of a set of firmware controller versions.
		/// </summary>
		/// <param name="aAlgorithmName">Name of the tuning algorithm to match.</param>
		/// <param name="aSupportedControllerVersions">
		///     Optional. If not null, only tuners that can accept input
		///     data for one of the controller versions in this collection will be output - an empty collection will result in no
		///     output.
		/// </param>
		/// <returns>Zero or more matching tuner types.</returns>
		public IEnumerable<Type> FindTunerTypesByAlgorithmAndInput(string aAlgorithmName,
																   IDictionary<string, int> aSupportedControllerVersions
																	   = null)
		{
			_tunerTypesByAlgorithm.TryGetValue(aAlgorithmName, out var candidates);

			if (null != candidates)
			{
				foreach (var candidate in candidates)
				{
					if ((null == aSupportedControllerVersions)
					|| !string.IsNullOrEmpty(SelectInputMode(candidate, aSupportedControllerVersions)))
					{
						yield return candidate;
					}
				}
			}
		}


		/// <summary>
		///     Given a selection of tuning algorithm, find one of its input formats that matches those supported
		///     by a device.
		/// </summary>
		/// <param name="aTunerType">Tuner type we want to use.</param>
		/// <param name="aSupportedControllerVersions">Table of tuning modes and their versions supported by a device.</param>
		/// <returns>The input mode to make use of. If there is more than one match, the selection order is undefined.</returns>
		public string SelectInputMode(Type aTunerType, IDictionary<string, int> aSupportedControllerVersions)
		{
			if (_tunerTypeSupportedInputModes.ContainsKey(aTunerType))
			{
				foreach (var modePair in _tunerTypeSupportedInputModes[aTunerType])
				{
					if (aSupportedControllerVersions.ContainsKey(modePair.Item1)
					&& (modePair.Item2 == aSupportedControllerVersions[modePair.Item1]))
					{
						return modePair.Item1;
					}
				}
			}

			return null;
		}


		/// <summary>
		///     Report all known tuner types that implement a given tuning algorithm, and optionally that
		///     also output data for one of a set of firmware controller versions.
		/// </summary>
		/// <param name="aAlgorithmName">Name of the tuning algorithm to match.</param>
		/// <param name="aSupportedControllerVersions">
		///     Optional. If not null, only tuners that can output
		///     data for one of the controller versions in this collection will be output - an empty collection will result in no
		///     output.
		/// </param>
		/// <returns>Zero or more matching tuner types.</returns>
		public IEnumerable<Type> FindTunerTypesByAlgorithmAndOutput(string aAlgorithmName,
																	IDictionary<string, int>
																		aSupportedControllerVersions = null)
		{
			_tunerTypesByAlgorithm.TryGetValue(aAlgorithmName, out var candidates);

			if (null != candidates)
			{
				foreach (var candidate in candidates)
				{
					if ((null == aSupportedControllerVersions)
					|| !string.IsNullOrEmpty(SelectOutputMode(candidate, aSupportedControllerVersions)))
					{
						yield return candidate;
					}
				}
			}
		}


		/// <summary>
		///     Given a selection of tuning algorithm, find one of its output formats that matches those supported
		///     by a device.
		/// </summary>
		/// <param name="aTunerType">Tuner type we want to use.</param>
		/// <param name="aSupportedControllerVersions">Table of tuning modes and their versions supported by a device.</param>
		/// <returns>The output mode to make use of. If there is more than one match, the selection order is undefined.</returns>
		public string SelectOutputMode(Type aTunerType, IDictionary<string, int> aSupportedControllerVersions)
		{
			if (_tunerTypeSupportedOutputModes.ContainsKey(aTunerType))
			{
				foreach (var modePair in _tunerTypeSupportedOutputModes[aTunerType])
				{
					if (aSupportedControllerVersions.ContainsKey(modePair.Item1)
					&& (modePair.Item2 == aSupportedControllerVersions[modePair.Item1]))
					{
						return modePair.Item1;
					}
				}
			}

			return null;
		}


		/// <summary>
		///     Singleton instance of the tuner selector - by default, this registers all
		///     tuner types in the Zaber.Tuning library.
		/// </summary>
		public static TunerSelector Instance
		{
			get
			{
				lock (_syncRoot)
				{
					if (null == _theInstance)
					{
						_theInstance = new TunerSelector();
						_theInstance.RegisterTunerTypes(typeof(ITuner).Assembly);
					}
				}

				return _theInstance;
			}
		}

		#endregion

		#region -- Data --

		private static TunerSelector _theInstance;
		private static readonly object _syncRoot = new object();


		private readonly Dictionary<string, List<Type>> _tunerTypesByMode = new Dictionary<string, List<Type>>();
		private readonly Dictionary<string, List<Type>> _tunerTypesByAlgorithm = new Dictionary<string, List<Type>>();

		private readonly Dictionary<Type, List<Tuple<string, int>>> _tunerTypeSupportedInputModes =
			new Dictionary<Type, List<Tuple<string, int>>>();

		private readonly Dictionary<Type, List<Tuple<string, int>>> _tunerTypeSupportedOutputModes =
			new Dictionary<Type, List<Tuple<string, int>>>();

		#endregion
	}
}

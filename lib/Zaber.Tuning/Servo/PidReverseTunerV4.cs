﻿using Zaber.Units;

namespace Zaber.Tuning.Servo
{
	/// <summary>
	///     Tuner that converts device PID parameters back into user PID representation.
	/// </summary>
	[TunerSelection(ALGORITHM_NAME, TuningModeNames.FFPID, 4, TuningModeNames.USERPID, 2)]
	public class PidReverseTunerV4 : ITuner
	{
		#region -- Public data --

		/// <summary>
		///     Arbitrary name for both this tuning algorithm and the input parameter set.
		/// </summary>
		public const string ALGORITHM_NAME = "User PID v2 Reverse";

		#endregion

		#region -- Public Methods & Properties --

		#region -- ITuner implementation --

		/// <summary>
		///     Calculate tuning parameters for a given device and load.
		/// </summary>
		/// <param name="aDevice">Properties of the device being tuned.</param>
		/// <param name="aLoad">Unused; can be null.</param>
		/// <param name="aGuidance">User-set control parameters. See remarks.</param>
		/// <returns>A set of simplified PID tuning parameters to display to the user.</returns>
		public ITunerParameterSet CalculateParameters(DeviceProperties aDevice, LoadProperties aLoad,
													  ITunerParameterSet aGuidance)
		{
			var tunings = new TunerParameterSet(TuningModeNames.USERPID);
			var uc = aGuidance.UnitConverter;
			tunings.UnitConverter = uc;

			// Values read back are expected to be in device units.
			// Previous gain value on the device can be any arbitrary value and can differ from PID gain.
			// In order to calculate device tuning parameters in SI units we need to scale them
			// by the ratio of PID gain and the old gain. They also need to be scaled up by
			// gain_scaling because firmware does that to gains internally.
			var device_gain = aGuidance["gain"].Value;
			var pid_gain = PidTunerV4.CalcGain(aDevice, uc);
			var scaling = PidTunerV4.GAIN_SCALING * device_gain / pid_gain;

			var kp = aGuidance["kp"].Value * scaling;
			var ki = aGuidance["ki"].Value * scaling;
			var kd = aGuidance["kd"].Value * scaling;
			var fc = aGuidance["fc1"].Value;

			// Next we convert values from device units into real world units
			// After PID gain adjustment "ki" should be in 2*N/(m*servo_sample_period)
			// Factor of 2 comes from the fact that firmware integrates sum of current error and
			// previous error. We want to convert "ki" into N/(m*s)
			var servo_sample_rate = aDevice.ControlLoopFrequency.GetValueAs(uc.FindUnitBySymbol("Hz"));
			var factorOf2 = 2;
			ki = ki * servo_sample_rate * factorOf2;
			// After PID gain adjustment "kd" must be in N*servo_sample_period/(2*m)
			// Factor of 2 comes from normalization coefficient calculation in firmware.
			// We want to convert "kd" into N*s/m
			kd = kd / factorOf2 / servo_sample_rate;

			tunings.Add(PidTunerV3.PARAM_KP, new Measurement(kp, uc.FindUnitBySymbol("N/m")));
			tunings.Add(PidTunerV3.PARAM_KI, new Measurement(ki, uc.FindUnitBySymbol("N/m⋅s")));
			tunings.Add(PidTunerV3.PARAM_KD, new Measurement(kd, uc.FindUnitBySymbol("N⋅s/m")));
			tunings.Add(PidTunerV3.PARAM_FC, new Measurement(fc, uc.FindUnitBySymbol("Hz")));

			//   return tunings
			return tunings;
		}

		#endregion

		#endregion
	}
}

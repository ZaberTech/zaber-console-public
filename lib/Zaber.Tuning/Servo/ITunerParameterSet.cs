﻿using System.Collections.Generic;
using Zaber.Units;

namespace Zaber.Tuning.Servo
{
	/// <summary>
	///     General interface for sets of tuner parameters.
	///     Implementations of this class should be serializable.
	/// </summary>
	public interface ITunerParameterSet : IEnumerable<KeyValuePair<string, Measurement>>
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Tuning mode this set is applicable to.
		/// </summary>
		string ModeName { get; set; }

		/// <summary>
		///     Returns an unordered list of legal tuning parameter names for this set.
		///     Implementations should return this in a new container so that callers
		///     can use it to modift parameter values while iterating over the names.
		/// </summary>
		IEnumerable<string> ParameterNames { get; }

		/// <summary>
		///     Get or set a parameter value by name.
		/// </summary>
		/// <param name="aName">Name of the parameter to change.</param>
		/// <returns>Value of the parameter.</returns>
		/// <exception cref="KeyNotFoundException">The parameter name is not valid for this set type.</exception>
		Measurement this[string aName] { get; set; }

		/// <summary>
		///     Unit conversion table to use for any custom units used by parameters in this set.
		///     If null, use the default unit converter instead.
		/// </summary>
		IUnitConverter UnitConverter { get; }

		#endregion
	}
}

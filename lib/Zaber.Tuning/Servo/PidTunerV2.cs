﻿using System;
using System.Linq;
using System.Reflection;
using System.Xml;
using Zaber.Units;

namespace Zaber.Tuning.Servo
{
	/// <summary>
	///     Tuner that calculates PID tuning parameters for Zaber devices given the
	///     device properties and load properties.
	/// </summary>
	[TunerSelection(ALGORITHM_NAME, TuningModeNames.USERPID, 2, TuningModeNames.FFPID, 2)]
	public class PidTunerV2 : ITuner
	{
		#region -- Public data --

		/// <summary>
		///     All of the input parameters expected by <see cref="CalculateParameters" />.
		/// </summary>
		public static readonly string[] INPUT_PARAMETER_NAMES = { PARAM_KP, PARAM_KI, PARAM_KD, PARAM_FC };

		/// <summary>
		///     Arbitrary name for both this tuning algorithm and the input parameter set.
		/// </summary>
		public const string ALGORITHM_NAME = "User PID v2 Forward";

		/// <summary>
		///     Name of one of the PID tuning parameters, proportional gain. This one is required.
		/// </summary>
		public const string PARAM_KP = "kp";

		/// <summary>
		///     Name of one of the PID tuning parameters, integral gain. This one is required.
		/// </summary>
		public const string PARAM_KI = "ki";

		/// <summary>
		///     Name of one of the PID tuning parameters, derivative gain. This one is required.
		/// </summary>
		public const string PARAM_KD = "kd";

		/// <summary>
		///     Name of one of the PID tuning parameters, filter cutoff frequency. This one is required, but only as an input to
		///     the calculation.
		/// </summary>
		public const string PARAM_FC = "fc";

		/// <summary>
		///     Unit symbol name for Kp unit conversions.
		/// </summary>
		public const string PARAM_KP_UNITS = "DeviceUnits";

		#endregion

		#region -- Public Methods & Properties --

		#region -- ITuner implementation --

		/// <summary>
		///     Calculate tuning parameters for a given device and load.
		/// </summary>
		/// <param name="aDevice">Properties of the device being tuned.</param>
		/// <param name="aLoad">Unused; can be null.</param>
		/// <param name="aGuidance">User-set control parameters. See remarks.</param>
		/// <returns>
		///     A set of tuning parameters for the device, or null if no
		///     results could be calculated.
		/// </returns>
		/// <remarks>
		///     This implementation expects aGuidance to contain the following
		///     named measurements. Values must be convertible to the mentioned units. kp (N/m),
		///     ki (N/ms), kd (Ns/m) and fc (filter cutoff frequency, Hz). The guidance input
		///     must also have a suitable unit converter for all of these values.
		/// </remarks>
		public ITunerParameterSet CalculateParameters(DeviceProperties aDevice, LoadProperties aLoad,
													  ITunerParameterSet aGuidance)
		{
			var tunings = new TunerParameterSet(TuningModeNames.FFPID) { UnitConverter = aGuidance.UnitConverter };
			var uc = aGuidance.UnitConverter;

			// gain = calc_gain_pid(Device)
			var gain = CalcGain(aDevice, uc);

			// kp = Kp / gain_scaling
			var kp = GetParamAs(aGuidance, PARAM_KP, "N/m") / GAIN_SCALING;

			// ki = Ki / gain_scaling
			var ki = GetParamAs(aGuidance, PARAM_KI, "N/m⋅s") / GAIN_SCALING;

			// kd = Kd / gain_scaling
			var kd = GetParamAs(aGuidance, PARAM_KD, "N⋅s/m") / GAIN_SCALING;

			//   """
			//   The 10 kHz sample rate is shared across all of our devices at the moment,
			//   so it's hard-coded here. If we launch products with different sampling
			//   rates, then this will have to go into the device database.
			//   """

			// servo_sample_rate = 10000.0
			var servo_sample_rate = aDevice.ControlLoopFrequency.GetValueAs(uc.FindUnitBySymbol("Hz"));

			//   # Note: Fixed a bug here, where a minus sign was missing. ~2018-01-17 JB
			//   tf = np.exp(-2.* np.pi * low_pass_filter_cutoff_hz / servo_sample_rate)
			var low_pass_filter_cutoff_hz = GetParamAs(aGuidance, PARAM_FC, "Hz");
			var tf = Math.Exp((-2.0 * Math.PI * low_pass_filter_cutoff_hz) / servo_sample_rate);

			//   """
			//   Later we might want to add an option to enable feedforward, but for now it
			//   will be disabled by default for manual tuning.
			//   """
			// finv2 = 0.0
			// finv1 = 0.0
			// fcla = 0.0
			// fclg = 0.0
			var finv2 = 0.0;
			var finv1 = 0.0;
			var fcla = 0.0;
			var fclg = 0.0;

			//   """
			// For now, set turbo-mode parameters to all match the low-speed parameters,
			// except for the low-pass filter time constant which will be set slower by an
			// arbitrary ratio, up to a certain minimum time constant which is also set
			// arbitrarily.

			// We can also allow these parameters to be set independently in an "advanced"
			// mode, but for beginner users it's simpler to hide them. All of these
			// parameters can be identified by the presence of a.hs suffix.

			// The factor of 4x is an arbitrary magic number which is intended to be "slow
			// enough" to reduce noise in turbo mode.
			//   """

			//kd_hs = kd
			//ki_hs = ki
			//kp_hs = kp
			//fcla_hs = fcla
			//fclg_hs = fclg
			var kd_hs = kd;
			var ki_hs = ki;
			var kp_hs = kp;
			var fcla_hs = fcla;
			var fclg_hs = fclg;

			// # Determine the turbo mode low-pass filter time constant
			// hs_filter_ratio = 4.0
			var hs_filter_ratio = 4.0;

			// hs_filter_hz_minimum = 100.0
			var hs_filter_hz_minimum = 100.0;

			// low_pass_filter_cutoff_hz_hs = low_pass_filter_cutoff_hz / hs_filter_ratio
			var low_pass_filter_cutoff_hz_hs = low_pass_filter_cutoff_hz / hs_filter_ratio;

			// if low_pass_filter_cutoff_hz<hs_filter_hz_minimum:
			if (low_pass_filter_cutoff_hz < hs_filter_hz_minimum)
			{
				//    low_pass_filter_cutoff_hz_hs = low_pass_filter_cutoff_hz
				low_pass_filter_cutoff_hz_hs = low_pass_filter_cutoff_hz;
			}

			// elif low_pass_filter_cutoff_hz_hs<hs_filter_hz_minimum:
			else if (low_pass_filter_cutoff_hz_hs < hs_filter_hz_minimum)
			{
				//	low_pass_filter_cutoff_hz_hs = hs_filter_hz_minimum
				low_pass_filter_cutoff_hz_hs = hs_filter_hz_minimum;
			}

			// tf_hs = np.exp(-2.*np.pi*low_pass_filter_cutoff_hz_hs/servo_sample_rate) 
			var tf_hs = Math.Exp((-2.0 * Math.PI * low_pass_filter_cutoff_hz_hs) / servo_sample_rate);

			//   tunings = {# Normal mode parameters
			//              "gain":gain,
			//              "finv1":finv1,
			//              "finv2":finv2,
			//              "kd":kd,
			//              "ki":ki,
			//              "kp":kp,
			//              "tf":tf,
			//              "fcla":fcla,
			//              "fclg":fclg,
			//              # Turbo mode parameters
			//              "kd.hs":kd_hs,
			//              "ki.hs":ki_hs,
			//              "kp.hs":kp_hs,
			//              "tf.hs":tf_hs,
			//              "fcla.hs":fcla_hs,
			//              "fclg.hs":fclg_hs}

			tunings.Add("gain", new Measurement(gain * ASCII_SCALING, null));
			tunings.Add("finv1", new Measurement(finv1 * ASCII_SCALING, null));
			tunings.Add("finv2", new Measurement(finv2 * ASCII_SCALING, null));
			tunings.Add("kd", new Measurement(kd * ASCII_SCALING, null));
			tunings.Add("ki", new Measurement(ki * ASCII_SCALING, null));
			tunings.Add("kp", new Measurement(kp * ASCII_SCALING, null));
			tunings.Add("tf", new Measurement(tf * ASCII_SCALING, null));
			tunings.Add("fcla", new Measurement(fcla * ASCII_SCALING, null));
			tunings.Add("fclg", new Measurement(fclg * ASCII_SCALING, null));
			tunings.Add("kd.hs", new Measurement(kd_hs * ASCII_SCALING, null));
			tunings.Add("ki.hs", new Measurement(ki_hs * ASCII_SCALING, null));
			tunings.Add("kp.hs", new Measurement(kp_hs * ASCII_SCALING, null));
			tunings.Add("tf.hs", new Measurement(tf_hs * ASCII_SCALING, null));
			tunings.Add("fcla.hs", new Measurement(fcla_hs * ASCII_SCALING, null));
			tunings.Add("fclg.hs", new Measurement(fclg_hs * ASCII_SCALING, null));

			//   return tunings
			return tunings;
		}

		#endregion


		/// <summary>
		///     Get the tuning specific unit conversion table, which includes units of
		///     measure for PID parameters etc.
		/// </summary>
		public static IUnitConverter PidUnitTable
		{
			get
			{
				lock (_syncRoot)
				{
					if (null == _pidUnitTable)
					{
						// PID parameter sets use some custom units, so create an override conversion table.
						var parser = ComposedUnitParser.NewDefaultUnitParser();
						var uc = new UnitConverter(parser, parser.GetUnitRegistry(), UnitConverter.Default);

						var reader =
							XmlReader.Create(Assembly.GetAssembly(typeof(PidTunerV3))
												  .GetManifestResourceStream("Zaber.Tuning.Resources.PidUnits.xml"));
						var pidLib = new XmlUnitLibrary(reader);
						pidLib.LoadTo(parser.GetUnitRegistry(), parser, UnitConverter.Default);

						_pidUnitTable = uc;
					}
				}

				return _pidUnitTable;
			}
		}

		#endregion

		#region -- Helpers --

		private double CalcGain(DeviceProperties aDevice, IUnitConverter aConverter)
		{
			var encoder_resolution = 1.0;
			if (Dimension.Length == aDevice.PositionUnit.Dimension)
			{
				if (!aDevice.PositionUnit.TryConvertTo(aConverter.FindUnitBySymbol("m"), ref encoder_resolution))
				{
					throw new ArgumentException(
						$"Linear device position unit {aDevice.PositionUnit.Abbreviation} cannot be converted to meters.");
				}
			}
			else if (Dimension.Angle == aDevice.PositionUnit.Dimension)
			{
				if (!aDevice.PositionUnit.TryConvertTo(aConverter.FindUnitBySymbol("°"), ref encoder_resolution))
				{
					throw new ArgumentException(
						$"Rotary device position unit {aDevice.PositionUnit.Abbreviation} cannot be converted to degrees.");
				}
			}
			else
			{
				throw new ArgumentException(
					$"Device position unit {aDevice.PositionUnit.Abbreviation} is not recognized as a unit of length or angle.");
			}

			encoder_resolution = 1.0 / encoder_resolution; // Convert meters per count to counts per meter.

			//gain = 1.0 / (Device.encoder_resolution * Device.force_constant)
			var gain = 1.0 / (encoder_resolution * aDevice.ForqueConstant);

			//gain = gain * Device.one_amp_in_microamps
			gain *= 1000000.0;

			//gain *= gain_scaling
			gain *= GAIN_SCALING;

			//return gain
			return gain;
		}


		private double GetParamAs(ITunerParameterSet aParams, string aParamName, string aDesiredUnitSymbol)
		{
			if (!aParams.ParameterNames.Contains(aParamName))
			{
				throw new ArgumentException($"{aParamName} input value is required.");
			}

			var unit = aParams.UnitConverter.FindUnitBySymbol(aDesiredUnitSymbol);
			if (null == unit)
			{
				throw new ArgumentException(
					$"Unit symbol {aDesiredUnitSymbol} for input {aParamName} is not recognized.");
			}

			var result = 0.0;
			try
			{
				result = aParams[aParamName].GetValueAs(unit);
			}
			catch (ArgumentException aException)
			{
				throw new ArgumentException($"Input {aParamName} has unexpected units: {aException.Message}");
			}

			return result;
		}

		#endregion

		#region -- Members --

		internal static readonly double GAIN_SCALING = 1000.0;
		internal static readonly double ASCII_SCALING = 1000000000.0;
		private static readonly object _syncRoot = new object();
		private static UnitConverter _pidUnitTable;

		#endregion
	}
}

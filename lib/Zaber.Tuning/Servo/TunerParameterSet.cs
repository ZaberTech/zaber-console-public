﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Zaber.Units;

namespace Zaber.Tuning.Servo
{
	/// <summary>
	///     General-purpose base class for parameter set types. Implements
	///     the interface functionality but does not specify legal parameter names.
	/// </summary>
	public class TunerParameterSet : ITunerParameterSet, IXmlSerializable
	{
		#region -- Data --

		#region -- Private data --

		private readonly Dictionary<string, Measurement> _parameters = new Dictionary<string, Measurement>();

		#endregion

		#endregion

		#region -- Initialization --

		/// <summary>
		///     Base class constructor.
		/// </summary>
		public TunerParameterSet()
		{
			ModeName = string.Empty;
		}


		/// <summary>
		///     Base class constructor; initializes mode name.
		/// </summary>
		/// <param name="aModeName"></param>
		/// <exception cref="ArgumentException">Mode name is null or empty.</exception>
		public TunerParameterSet(string aModeName)
		{
			if (string.IsNullOrEmpty(aModeName))
			{
				throw new ArgumentException(nameof(aModeName) + " must be non-empty.");
			}

			ModeName = aModeName;
		}


		/// <summary>
		///     Add a named parameter to the set, with a default value.
		///     Subclasses can use this to define the list of legal parameter names.
		/// </summary>
		/// <param name="aName">Name of the parameter to add.</param>
		/// <param name="aValue">Optional initial value of the parameter. Defaults to 0 and dimensionless.</param>
		public void Add(string aName, Measurement aValue) => _parameters[aName] = aValue;


		/// <summary>
		///     Required for serialization.
		/// </summary>
		/// <param name="aObject">Item to add.</param>
		public void Add(KeyValuePair<string, Measurement> aObject)
		{
			// Silently discard mismatched data.
			if (ParameterNames.Contains(aObject.Key))
			{
				Add(aObject.Key, aObject.Value);
			}
		}


		/// <summary>
		///     Replace the unit conversion table with a new one, fixing up unit definitions
		///     to use the ones from the new table.
		/// </summary>
		/// <param name="aNewConverter">New unit converter to use.</param>
		public void ReplaceUnitConverter(IUnitConverter aNewConverter)
		{
			UnitConverter = aNewConverter;
			foreach (var name in ParameterNames.ToArray())
			{
				var oldMeasure = this[name];
				var newMeasure = oldMeasure.ResolveUnit(aNewConverter);
				if (null == newMeasure)
				{
					throw new ArgumentException(("Could not resolve unit symbol " + oldMeasure.Unit?.Abbreviation)
											?? "<null>");
				}

				this[name] = newMeasure;
			}
		}

		#endregion

		#region -- ITunerParameterSet implementation

		/// <summary>
		///     Tuning mode this set is applicable to.
		/// </summary>
		public string ModeName { get; set; }


		/// <summary>
		///     Returns an unordered list of legal tuning parameter names for this set.
		/// </summary>
		public IEnumerable<string> ParameterNames => new List<string>(_parameters.Keys);


		/// <summary>
		///     Get or set a parameter value by name.
		/// </summary>
		/// <param name="aName">Name of the parameter to change.</param>
		/// <returns>Value of the parameter. Pass in null to remove the parameter from the set.</returns>
		/// <exception cref="KeyNotFoundException">The parameter name is not valid for this set type.</exception>
		public Measurement this[string aName]
		{
			get => _parameters[aName];
			set
			{
				if (null != value)
				{
					_parameters[aName] = value;
				}
				else
				{
					if (_parameters.ContainsKey(aName))
					{
						_parameters.Remove(aName);
					}
				}
			}
		}


		/// <summary>
		///     Unit conversion table to use for any custom units used by parameters in this set.
		/// </summary>
		public IUnitConverter UnitConverter { get; set; }


		/// <summary>
		///     Enumerate the content of this set as KeyValuePairs.
		/// </summary>
		/// <returns>
		///     An unordered list of KeyValuePairs of strings and ints,
		///     where the string is the name of a tuning parameter and the int is its value.
		/// </returns>
		public IEnumerator<KeyValuePair<string, Measurement>> GetEnumerator() => _parameters.GetEnumerator();


		/// <summary>
		///     Enumerate the content of this set as KeyValuePairs.
		/// </summary>
		/// <returns>
		///     An unordered list of KeyValuePairs of strings and ints,
		///     where the string is the name of a tuning parameter and the int is its value.
		/// </returns>
		IEnumerator IEnumerable.GetEnumerator() => _parameters.GetEnumerator();

		#endregion

		#region -- IXmlSerializable implementation --

		/// <summary>
		///     Required by IXmlSerializable.
		/// </summary>
		/// <returns>null</returns>
		public XmlSchema GetSchema() => null;


		/// <summary>
		///     Deserialize an instance from XML. Note this always uses the global default unit
		///     converter to fix up units of measure. If a custom unit converter was used during
		///     serialization, you will have to instantiate this class and set the unit converter
		///     first, then call ReadXml() yourself.
		/// </summary>
		/// <param name="aReader">The XmlReader to read values from.</param>
		public void ReadXml(XmlReader aReader)
		{
			var uc = UnitConverter ?? Units.UnitConverter.Default;

			aReader.MoveToContent();
			aReader.ReadStartElement();
			var isEmptyElement = aReader.IsEmptyElement;
			if (!isEmptyElement)
			{
				aReader.ReadStartElement("Parameters");
				isEmptyElement = aReader.IsEmptyElement;
				while (XmlNodeType.EndElement != aReader.NodeType)
				{
					aReader.ReadStartElement("Parameter");
					var name = aReader.ReadElementString("Name");
					var valueStr = aReader.ReadElementString("Value");
					var value = double.Parse(valueStr, CultureInfo.InvariantCulture);
					var unitSymbol = aReader.ReadElementString("Unit");

					var unit = uc.FindUnitBySymbol(unitSymbol)
						   ?? new ScaledShiftedUnit(unitSymbol, "Serialization Placeholder", Unit.Dimensionless, 1.0);
					var measure = new Measurement(value, unit);

					this[name] = measure;

					aReader.ReadEndElement();
				}

				aReader.ReadEndElement();
			}

			aReader.ReadEndElement();
		}


		/// <summary>
		///     Serialize an instance to XML.
		/// </summary>
		/// <param name="aWriter">The XmlWriter to write values to.</param>
		public void WriteXml(XmlWriter aWriter)
		{
			aWriter.WriteStartElement("Parameters");

			foreach (var pair in _parameters)
			{
				aWriter.WriteStartElement("Parameter");
				aWriter.WriteElementString("Name", pair.Key);
				aWriter.WriteElementString("Value", pair.Value.Value.ToString(CultureInfo.InvariantCulture));
				aWriter.WriteElementString("Unit", pair.Value.Unit.Abbreviation);
				aWriter.WriteEndElement();
			}

			aWriter.WriteEndElement();
		}

		#endregion
	}
}

﻿using System;
using Zaber.Units;

namespace Zaber.Tuning.Servo
{
	#region -- ITuner implementation --

	/// <summary>
	///     Tuner that converts device PID parameters back into user PID representation.
	/// </summary>
	[TunerSelection(ALGORITHM_NAME, TuningModeNames.FFPID, 2, TuningModeNames.USERPID, 2)]
	public class PidReverseTunerV2 : ITuner
	{
		#region -- Public data --

		/// <summary>
		///     Arbitrary name for both this tuning algorithm and the input parameter set.
		/// </summary>
		public const string ALGORITHM_NAME = "User PID v2 Reverse";

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Calculate tuning parameters for a given device and load.
		/// </summary>
		/// <param name="aDevice">Properties of the device being tuned.</param>
		/// <param name="aLoad">Unused; can be null.</param>
		/// <param name="aGuidance">User-set control parameters. See remarks.</param>
		/// <returns>A set of simplified PID tuning parameters to display to the user.</returns>
		public ITunerParameterSet CalculateParameters(DeviceProperties aDevice, LoadProperties aLoad,
													  ITunerParameterSet aGuidance)
		{
			var tunings = new TunerParameterSet(TuningModeNames.USERPID);
			var uc = aGuidance.UnitConverter;
			tunings.UnitConverter = uc;

			// Values read back are expected to be in device units.
			var kp = (aGuidance["kp"].Value * PidTunerV3.GAIN_SCALING) / PidTunerV3.ASCII_SCALING;
			var ki = (aGuidance["ki"].Value * PidTunerV3.GAIN_SCALING) / PidTunerV3.ASCII_SCALING;
			var kd = (aGuidance["kd"].Value * PidTunerV3.GAIN_SCALING) / PidTunerV3.ASCII_SCALING;

			var servo_sample_rate = aDevice.ControlLoopFrequency.GetValueAs(uc.FindUnitBySymbol("Hz"));

			var tf = aGuidance["tf"].Value / PidTunerV2.ASCII_SCALING;
			var fc = (-servo_sample_rate * Math.Log(tf)) / (2.0 * Math.PI);

			tunings.Add(PidTunerV3.PARAM_KP, new Measurement(kp, uc.FindUnitBySymbol("N/m")));
			tunings.Add(PidTunerV3.PARAM_KI, new Measurement(ki, uc.FindUnitBySymbol("N/m⋅s")));
			tunings.Add(PidTunerV3.PARAM_KD, new Measurement(kd, uc.FindUnitBySymbol("N⋅s/m")));
			tunings.Add(PidTunerV3.PARAM_FC, new Measurement(fc, uc.FindUnitBySymbol("Hz")));

			//   return tunings
			return tunings;
		}

		#endregion
	}

	#endregion
}

﻿using System;
using System.Collections.Generic;
using Zaber.Units;

namespace Zaber.Tuning.Servo
{
	/// <summary>
	///     Extension methods for the ITunerParameterSet interface.
	/// </summary>
	public static class ITunerParameterSetExtensions
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Compare two parameter sets for equality.
		/// </summary>
		/// <param name="aLeft">This. The first set to compare.</param>
		/// <param name="aRight">The second set to compare.</param>
		/// <returns>
		///     True if the two sets have the same list of parameter names and
		///     have the same values for each name.
		/// </returns>
		public static bool IsIdenticalTo(this ITunerParameterSet aLeft, ITunerParameterSet aRight)
		{
			if (null != aRight)
			{
				if (aLeft.ModeName != aRight.ModeName)
				{
					return false;
				}

				var lNames = new HashSet<string>(aLeft.ParameterNames);
				var rNames = new HashSet<string>(aRight.ParameterNames);
				if (lNames.SetEquals(rNames))
				{
					foreach (var name in lNames)
					{
						var lValue = aLeft[name];
						var rValue = aRight[name];
						if (!lValue.Equals(rValue))
						{
							return false;
						}
					}

					return true;
				}
			}

			return false;
		}


		/// <summary>
		///     Duplicate an instance of a parameter set.
		/// </summary>
		/// <param name="aSet">This. The parameter set to duplicate.</param>
		/// <returns>A new instance of the same type with the same values in it.</returns>
		public static ITunerParameterSet Clone(this ITunerParameterSet aSet)
		{
			var clonable = aSet as ICloneable;
			if (null != clonable)
			{
				return clonable.Clone() as ITunerParameterSet;
			}

			// It is assumed that the type being duplicated has a hardcoded mode name and list of parameters.
			// Note the UnitConverter property is not copied; if it matters, implement IClonable.
			var clone = Activator.CreateInstance(aSet.GetType(), null) as ITunerParameterSet;
			clone.ModeName = aSet.ModeName;
			foreach (var name in aSet.ParameterNames)
			{
				clone[name] = new Measurement(aSet[name]);
			}

			return clone;
		}


		/// <summary>
		///     Merge two sets of tuner parameters.
		/// </summary>
		/// <param name="aSet">
		///     The LHS of the merge. The set name and unit converter of this
		///     side are preserved.
		/// </param>
		/// <param name="aOther">
		///     The RHS containing parameters to merge. Values from this side
		///     are added to the LHS, replacing existing ones if any.
		/// </param>
		/// <returns>The LHS, updated in place.</returns>
		/// <remarks>Use caution if the two sets are using different unit converters.</remarks>
		public static void Merge(this ITunerParameterSet aSet, ITunerParameterSet aOther)
		{
			foreach (var pair in aOther)
			{
				aSet[pair.Key] = pair.Value;
			}
		}

		#endregion
	}
}

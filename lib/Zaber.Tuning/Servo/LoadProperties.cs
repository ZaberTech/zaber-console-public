﻿using Zaber.Units;

namespace Zaber.Tuning.Servo
{
	/// <summary>
	///     Description of device load characteristics, for use with tuners.
	/// </summary>
	public class LoadProperties
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Static load inertia, NOT including the mass of the stage top. This must be convertible
		///     to kilograms for linear stages or kg-m^2 for rotary stages.
		/// </summary>
		public Measurement LoadInertia { get; set; }

		#endregion
	}
}

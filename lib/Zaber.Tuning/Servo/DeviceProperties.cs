﻿using System.Collections.Generic;
using Zaber.Units;

namespace Zaber.Tuning.Servo
{
	/// <summary>
	///     Description of device hardware passed to tuning calculators.
	/// </summary>
	public class DeviceProperties
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Dimension and physical size of native device position units. Must be convertible to
		///     either meters or degrees. This expresses the encoder resolution for most devices.
		/// </summary>
		public Unit PositionUnit { get; set; }

		/// <summary>
		///     Device's native force unit - must be convertible to Newtons for linear devices
		///     or Newton-meters for rotary devices.
		/// </summary>
		public Unit ForceUnit { get; set; }

		/// <summary>
		///     Number of passes of the device's control loop per second. Must be in frequency units.
		/// </summary>
		public Measurement ControlLoopFrequency { get; set; }

		/// <summary>
		///     Maximum force output of the device, if known.
		/// </summary>
		public Measurement MaximumForce { get; set; }

		/// <summary>
		///     Device driver current units. Must be convertible to DC amps.
		/// </summary>
		public Unit CurrentUnit { get; set; }

		/// <summary>
		///     Device's force constant. This is how many Newtons or Newton-Meters of force
		///     or torque the device will exert on the carriage when the driver current is 1 amp.
		/// </summary>
		public double ForqueConstant { get; set; }

		/// <summary>
		///     Inertia of the stage carriage, NOT including the user load mass. This must be convertible
		///     to kilograms for linear stages or kg-m^2 for rotary stages.
		/// </summary>
		public Measurement CarriageInertia { get; set; }

		/// <summary>
		///     List of tuning mode names supported by the device, with parameter setting names for each.
		/// </summary>
		public IDictionary<string, string[]> SupportedTuningModes { get; set; } = new Dictionary<string, string[]>();

		#endregion
	}
}

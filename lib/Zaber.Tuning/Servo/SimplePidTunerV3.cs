﻿using System;
using System.Linq;
using Zaber.Units;

namespace Zaber.Tuning.Servo
{
	/// <summary>
	///     Servo tuning algorithm for firmware 7 ffpid tuning mode.
	/// </summary>
	[TunerSelection(ALGORITHM_NAME, TuningModeNames.FFPID, 3, TuningModeNames.FFPID, 3)]
	public class SimplePidTunerV3 : ITuner
	{
		#region -- Public data --

		/// <summary>
		///     Identifier for this tuning algorithm. Must match names used in device database JSON data.
		/// </summary>
		public const string ALGORITHM_NAME = "servoPIDFFv1";

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Calculate tuning parameters for a given device and load.
		/// </summary>
		/// <param name="aDevice">Properties of the device being tuned.</param>
		/// <param name="aLoad">Information about the inertia of the total load on the stage.</param>
		/// <param name="aGuidance">User-set control parameters. See remarks.</param>
		/// <returns>
		///     A set of tuning parameters for the device, or null if no
		///     results could be calculated.
		/// </returns>
		public ITunerParameterSet CalculateParameters(DeviceProperties aDevice, LoadProperties aLoad,
													  ITunerParameterSet aGuidance)
		{
			// By default, pass through all parameters the device expects with ascii protocol scaling applied.
			var tunings = new TunerParameterSet(TuningModeNames.FFPID);
			string[] deviceParamNames = null;
			if (aDevice.SupportedTuningModes?.TryGetValue(TuningModeNames.FFPID, out deviceParamNames) ?? false)
			{
				foreach (var paramName in deviceParamNames)
				{
					if (aGuidance.ParameterNames.Contains(paramName))
					{
						var paramVal = aGuidance[paramName].Value;
						tunings.Add(paramName, new Measurement(paramVal * PidTunerV3.ASCII_SCALING, null));
					}
				}
			}

			// This code ported from revision 4725 of zc_servo_tuner_by_mass.py.

			// gain = calc_gain(Device, load)
			_unitConverter = aGuidance.UnitConverter;
			var inertiaUnit = GetInertiaUnit(aDevice);
			var deviceInertia = aDevice.CarriageInertia.GetValueAs(inertiaUnit);
			var totalInertia = aLoad.LoadInertia.GetValueAs(inertiaUnit) + deviceInertia;

			var bandwidth = Math.Sqrt(deviceInertia / totalInertia);

			var use_fc1 = true;
			var use_fc = false;
			var use_fc2 = true;

			var gain = CalculateGain(aDevice, totalInertia);

			var finv2 = CalcFinv2(aDevice, aLoad, totalInertia, gain);

			var fc1 = 0.0;
			var fc2 = 0.0;
			var tf = 0.0;
			if (aGuidance.ParameterNames.Contains("fc1"))
			{
				fc1 = aGuidance["fc1"].Value; // JSON data is assumed to be in hertz.
			}
			else if (aGuidance.ParameterNames.Contains("fc"))
			{
				use_fc1 = false;
				use_fc = true;
				fc1 = aGuidance["fc"].Value; // JSON data is assumed to be in hertz.
			}
			else
			{
				use_fc1 = false;
				use_fc = false;
				tf = aGuidance["tf"].Value;
			}

			if (aGuidance.ParameterNames.Contains("fc2"))
			{
				fc2 = aGuidance["fc2"].Value; // JSON data is assumed to be in hertz.
			}
			else
			{
				use_fc2 = false;
			}

			var kd = aGuidance["kd"].Value / _gainScaling;
			var ki = aGuidance["ki"].Value / _gainScaling;
			var kp = aGuidance["kp"].Value / _gainScaling;
			var fcla = aGuidance["fcla"].Value;
			var fclg = aGuidance["fclg"].Value;

			var fc1_hs = 0.0;
			var fc2_hs = 0.0;
			var tf_hs = 0.0;
			if (use_fc1)
			{
				fc1_hs = aGuidance["fc1.hs"].Value; // JSON data is assumed to be in hertz.
			}
			else if (use_fc)
			{
				fc1_hs = aGuidance["fc.hs"].Value; // JSON data is assumed to be in hertz.
			}
			else
			{
				tf_hs = aGuidance["tf.hs"].Value;
			}

			if (use_fc2)
			{
				fc2_hs = aGuidance["fc2.hs"].Value; // JSON data is assumed to be in hertz.
			}

			var kd_hs = aGuidance["kd.hs"].Value / _gainScaling;
			var ki_hs = aGuidance["ki.hs"].Value / _gainScaling;
			var kp_hs = aGuidance["kp.hs"].Value / _gainScaling;
			var fcla_hs = aGuidance["fcla.hs"].Value;
			var fclg_hs = aGuidance["fclg.hs"].Value;

			kd *= bandwidth;
			kp *= bandwidth * bandwidth;
			ki *= bandwidth * bandwidth * bandwidth;
			fc1 *= bandwidth;
			fc2 *= bandwidth;
			ShiftClosedLoopFeedForward(bandwidth, ref fcla, ref fclg);

			kd_hs *= bandwidth;
			kp_hs *= bandwidth * bandwidth;
			ki_hs *= bandwidth * bandwidth * bandwidth;
			fc1_hs *= bandwidth;
			fc2_hs *= bandwidth;
			ShiftClosedLoopFeedForward(bandwidth, ref fcla_hs, ref fclg_hs);

			tunings.Add("gain", new Measurement(gain * PidTunerV3.ASCII_SCALING, null));
			tunings.Add("finv2", new Measurement(finv2 * PidTunerV3.ASCII_SCALING, null));
			tunings.Add("kd", new Measurement(kd * PidTunerV3.ASCII_SCALING, null));
			tunings.Add("ki", new Measurement(ki * PidTunerV3.ASCII_SCALING, null));
			tunings.Add("kp", new Measurement(kp * PidTunerV3.ASCII_SCALING, null));
			tunings.Add("fcla", new Measurement(fcla * PidTunerV3.ASCII_SCALING, null));
			tunings.Add("fclg", new Measurement(fclg * PidTunerV3.ASCII_SCALING, null));
			tunings.Add("kd.hs", new Measurement(kd_hs * PidTunerV3.ASCII_SCALING, null));
			tunings.Add("ki.hs", new Measurement(ki_hs * PidTunerV3.ASCII_SCALING, null));
			tunings.Add("kp.hs", new Measurement(kp_hs * PidTunerV3.ASCII_SCALING, null));
			tunings.Add("fcla.hs", new Measurement(fcla_hs * PidTunerV3.ASCII_SCALING, null));
			tunings.Add("fclg.hs", new Measurement(fclg_hs * PidTunerV3.ASCII_SCALING, null));

			if (use_fc1)
			{
				tunings.Add("fc1", new Measurement(fc1 * PidTunerV3.ASCII_SCALING, null));
				tunings.Add("fc1.hs", new Measurement(fc1_hs * PidTunerV3.ASCII_SCALING, null));
			}
			else if (use_fc)
			{
				tunings.Add("fc", new Measurement(fc1 * PidTunerV3.ASCII_SCALING, null));
				tunings.Add("fc.hs", new Measurement(fc1_hs * PidTunerV3.ASCII_SCALING, null));
			}
			else
			{
				tunings.Add("tf", new Measurement(tf * PidTunerV3.ASCII_SCALING, null));
				tunings.Add("tf.hs", new Measurement(tf_hs * PidTunerV3.ASCII_SCALING, null));
			}

			if (use_fc2)
			{
				tunings.Add("fc2", new Measurement(fc2 * PidTunerV3.ASCII_SCALING, null));
				tunings.Add("fc2.hs", new Measurement(fc2_hs * PidTunerV3.ASCII_SCALING, null));
			}

			// return tunings
			return tunings;
		}

		#endregion

		#region -- Non-Public Methods --

		private Unit GetInertiaUnit(DeviceProperties aDevice)
		{
			if (Dimension.RotationalInertia == aDevice.CarriageInertia.Unit.Dimension)
			{
				return _unitConverter.FindUnitBySymbol("kg⋅m²");
			}

			return _unitConverter.FindUnitBySymbol("kg");
		}


		private double CalculateGain(DeviceProperties aDevice, double aTotalLoad)
		{
			//assert(load>=0)
			//total_inertia = Device.inertia + load
			if (aTotalLoad <= 0.0)
			{
				throw new ArgumentException("Total load inertia must be greater than zero.");
			}

			Unit basePositionUnit = null;
			basePositionUnit = _unitConverter.FindUnitBySymbol(Dimension.RotationalInertia == aDevice.CarriageInertia.Unit.Dimension ? "°" : "m");

			_encoderResolution = 1.0 / aDevice.PositionUnit.GetTransformationTo(basePositionUnit).Transform(1.0);

			//gain = total_inertia / (Device.encoder_resolution * Device.force_constant)
			var gain = aTotalLoad / (_encoderResolution * aDevice.ForqueConstant);

			//gain = gain* Device.one_amp_in_microamps
			gain *= 1000000.0;

			//gain *= gain_scaling
			gain *= _gainScaling;

			//return gain
			return gain;
		}


		//def calc_finv2(Device, load, gain):
		private double CalcFinv2(DeviceProperties aDevice, LoadProperties aLoad, double aTotalInertia, double aGain)
		{
			//    """
			//    Parts of this calculation might seem redundant as it cancels out with the
			//	  gain.However, it's done this way to remain correct even if the gain
			//    calculation method changes.
			//    """

			//	assert(load>=0)
			//	total_inertia = Device.inertia + load
			//	feedforward_prescaling = 2 * *14
			var feedforward_prescaling = (double) (1L << 14);

			//	servo_freq = 10000.0
			var servo_freq = aDevice.ControlLoopFrequency.GetValueAs(_unitConverter.FindUnitBySymbol("Hz"));

			//    ff = total_inertia* (servo_freq**2) / (Device.encoder_resolution \
			//                         * Device.force_constant* feedforward_prescaling)
			var ff = (aTotalInertia * (servo_freq * servo_freq))
				 / (_encoderResolution * aDevice.ForqueConstant * feedforward_prescaling);

			//	ff = ff* Device.one_amp_in_microamps
			ff *= 1000000.0;

			//	ff = ff / gain
			ff /= _gainScaling;

			//  return ff
			return ff;
		}


		private static void ShiftClosedLoopFeedForward(double aBandwidth, ref double a, ref double g)
		{
			var new_a = 0.0;
			var new_g = 0.0;

			var b = (a - g) / (1.0 - g);
			if ((a >= 0.0) && (b >= 0.0) && (a < 1.0) && (a > b) && (aBandwidth > 0.0))
			{
				new_a = Math.Pow(a, aBandwidth);
				var new_b = Math.Pow(b, aBandwidth);
				new_g = (new_a - new_b) / (1.0 - new_b);
				if (new_g < 0.0)
				{
					new_g = 0.0;
				}
			}

			a = new_a;
			g = new_g;
		}

		#endregion

		#region -- Data --

		private const double _gainScaling = 1000.0;


		private IUnitConverter _unitConverter;
		private double _encoderResolution;

		#endregion
	}
}

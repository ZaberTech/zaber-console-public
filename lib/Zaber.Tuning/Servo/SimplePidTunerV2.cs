﻿using System;
using System.Linq;
using Zaber.Units;

namespace Zaber.Tuning.Servo
{
	/// <summary>
	///     Servo tuning algorithm for firmware 7 ffpid tuning mode.
	/// </summary>
	[TunerSelection(ALGORITHM_NAME, TuningModeNames.FFPID, 2, TuningModeNames.FFPID, 2)]
	public class SimplePidTunerV2 : ITuner
	{
		#region -- Public data --

		/// <summary>
		///     Identifier for this tuning algorithm. Must match names used in device database JSON data.
		/// </summary>
		public const string ALGORITHM_NAME = "servoPIDFFv1";

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Calculate tuning parameters for a given device and load.
		/// </summary>
		/// <param name="aDevice">Properties of the device being tuned.</param>
		/// <param name="aLoad">Information about the inertia of the total load on the stage.</param>
		/// <param name="aGuidance">User-set control parameters. See remarks.</param>
		/// <returns>
		///     A set of tuning parameters for the device, or null if no
		///     results could be calculated.
		/// </returns>
		public ITunerParameterSet CalculateParameters(DeviceProperties aDevice, LoadProperties aLoad,
													  ITunerParameterSet aGuidance)
		{
			// By default, pass through all parameters the device expects with ascii protocol scaling applied.
			var tunings = new TunerParameterSet(TuningModeNames.FFPID);
			string[] deviceParamNames = null;
			if (aDevice.SupportedTuningModes?.TryGetValue(TuningModeNames.FFPID, out deviceParamNames) ?? false)
			{
				foreach (var paramName in deviceParamNames)
				{
					if (aGuidance.ParameterNames.Contains(paramName))
					{
						var paramVal = aGuidance[paramName].Value;
						tunings.Add(paramName, new Measurement(paramVal * PidTunerV2.ASCII_SCALING, null));
					}
				}
			}

			// gain = calc_gain(Device, load)
			_unitConverter = aGuidance.UnitConverter;
			var gain = CalculateGain(aDevice, aLoad);

			//finv2 = calc_finv2(Device, load, gain)
			var finv2 = CalcFinv2(aDevice, aLoad, gain);

			//parameter_table = get_parameter_table(Device)
			//slider_range = np.linspace(0.0, 1.0, parameter_table["_table_length"])
			//# Normal mode parameters
			//		kd_interp = interp1d(slider_range, parameter_table["kd"])
			var kd_interp = aGuidance["kd"].Value;

			//ki_interp = interp1d(slider_range, parameter_table["ki"])
			var ki_interp = aGuidance["ki"].Value;

			//kp_interp = interp1d(slider_range, parameter_table["kp"])
			var kp_interp = aGuidance["kp"].Value;


			//kd = kd_interp(slider_pos) / gain_scaling
			var kd = kd_interp / _gainScaling;

			//ki = ki_interp(slider_pos) / gain_scaling
			var ki = ki_interp / _gainScaling;

			//kp = kp_interp(slider_pos) / gain_scaling
			var kp = kp_interp / _gainScaling;

			//# Turbo mode parameters
			//kd_hs_interp = interp1d(slider_range, parameter_table["kd"])
			var kd_hs_interp = aGuidance["kd.hs"].Value;

			//ki_hs_interp = interp1d(slider_range, parameter_table["ki"])
			var ki_hs_interp = aGuidance["ki.hs"].Value;

			//kp_hs_interp = interp1d(slider_range, parameter_table["kp"])
			var kp_hs_interp = aGuidance["kp.hs"].Value;


			//kd_hs = kd_hs_interp(slider_pos) / gain_scaling
			var kd_hs = kd_hs_interp / _gainScaling;

			//ki_hs = ki_hs_interp(slider_pos) / gain_scaling
			var ki_hs = ki_hs_interp / _gainScaling;

			//kp_hs = kp_hs_interp(slider_pos) / gain_scaling
			var kp_hs = kp_hs_interp / _gainScaling;

			//tunings = {# Normal mode parameters
			//            "gain":gain,
			tunings.Add("gain", new Measurement(gain * PidTunerV2.ASCII_SCALING, null));

			//            "finv2":finv2,
			tunings.Add("finv2", new Measurement(finv2 * PidTunerV2.ASCII_SCALING, null));

			//            "kd":kd,
			tunings.Add("kd", new Measurement(kd * PidTunerV2.ASCII_SCALING, null));

			//            "ki":ki,
			tunings.Add("ki", new Measurement(ki * PidTunerV2.ASCII_SCALING, null));

			//            "kp":kp,
			tunings.Add("kp", new Measurement(kp * PidTunerV2.ASCII_SCALING, null));

			//            # Turbo mode parameters
			//            "kd.hs":kd_hs,
			tunings.Add("kd.hs", new Measurement(kd_hs * PidTunerV2.ASCII_SCALING, null));

			//            "ki.hs":ki_hs,
			tunings.Add("ki.hs", new Measurement(ki_hs * PidTunerV2.ASCII_SCALING, null));

			//            "kp.hs":kp_hs,
			tunings.Add("kp.hs", new Measurement(kp_hs * PidTunerV2.ASCII_SCALING, null));

			// return tunings
			return tunings;
		}

		#endregion

		#region -- Non-Public Methods --

		//def calc_gain(Device, load):
		private double CalculateGain(DeviceProperties aDevice, LoadProperties aLoad)
		{
			//assert(load>=0)
			//total_inertia = Device.inertia + load

			Unit basePositionUnit = null;
			Unit baseInertiaUnit = null;
			if (Dimension.RotationalInertia == aLoad.LoadInertia.Unit.Dimension)
			{
				baseInertiaUnit = _unitConverter.FindUnitBySymbol("kg⋅m²");
				basePositionUnit = _unitConverter.FindUnitBySymbol("°");
			}
			else
			{
				baseInertiaUnit = _unitConverter.FindUnitBySymbol("kg");
				basePositionUnit = _unitConverter.FindUnitBySymbol("m");
			}

			_totalInertia = aLoad.LoadInertia.GetValueAs(baseInertiaUnit);
			_totalInertia += aDevice.CarriageInertia.GetValueAs(baseInertiaUnit);
			if (_totalInertia <= 0.0)
			{
				throw new ArgumentException("Total load inertia must be greater than zero.");
			}

			_encoderResolution = 1.0 / aDevice.PositionUnit.GetTransformationTo(basePositionUnit).Transform(1.0);

			//gain = total_inertia / (Device.encoder_resolution * Device.force_constant)
			var gain = _totalInertia / (_encoderResolution * aDevice.ForqueConstant);

			//gain = gain* Device.one_amp_in_microamps
			gain *= 1000000.0;

			//gain *= gain_scaling
			gain *= _gainScaling;

			//return gain
			return gain;
		}


		//def calc_finv2(Device, load, gain):
		private double CalcFinv2(DeviceProperties aDevice, LoadProperties aLoad, double aGain)
		{
			//    """
			//    Parts of this calculation might seem redundant as it cancels out with the
			//	  gain.However, it's done this way to remain correct even if the gain
			//    calculation method changes.
			//    """

			//	assert(load>=0)
			//	total_inertia = Device.inertia + load
			//	feedforward_prescaling = 2 * *14
			var feedforward_prescaling = (double) (1L << 14);

			//	servo_freq = 10000.0
			var servo_freq = aDevice.ControlLoopFrequency.GetValueAs(_unitConverter.FindUnitBySymbol("Hz"));

			//    ff = total_inertia* (servo_freq**2) / (Device.encoder_resolution \
			//                         * Device.force_constant* feedforward_prescaling)
			var ff = (_totalInertia * (servo_freq * servo_freq))
				 / (_encoderResolution * aDevice.ForqueConstant * feedforward_prescaling);

			//	ff = ff* Device.one_amp_in_microamps
			ff *= 1000000.0;

			//	ff = ff / gain
			ff /= aGain;

			//  return ff
			return ff;
		}

		#endregion

		#region -- Data --

		private readonly double _gainScaling = 1000.0;

		private IUnitConverter _unitConverter;
		private double _totalInertia;
		private double _encoderResolution;

		#endregion
	}
}

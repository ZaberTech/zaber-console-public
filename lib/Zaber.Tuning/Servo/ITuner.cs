﻿namespace Zaber.Tuning.Servo
{
	/// <summary>
	///     Interface for classes that can calculate tuner parameters for a given mass.
	/// </summary>
	public interface ITuner
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Calculate tuning parameters for a given device and load.
		/// </summary>
		/// <param name="aDevice">Properties of the device being tuned.</param>
		/// <param name="aLoad">Properties of the physical load on the device.</param>
		/// <param name="aGuidance">
		///     User-set control parameters. Expectations for the content
		///     of this set vary by implementation - see the documentation for the
		///     specific ITuner implementation being used.
		/// </param>
		/// <returns>
		///     A set of tuning parameters for the device, or null if no
		///     results could be calculated.
		/// </returns>
		/// <exception cref="System.ArgumentException">
		///     It is not possible to calculate useful
		///     results with the given data. The exception message is expected to be user-readable.
		/// </exception>
		ITunerParameterSet CalculateParameters(DeviceProperties aDevice, LoadProperties aLoad,
											   ITunerParameterSet aGuidance);

		#endregion
	}
}

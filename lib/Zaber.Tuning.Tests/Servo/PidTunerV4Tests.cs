﻿using NUnit.Framework;
using Zaber.Tuning.Servo;
using Zaber.Units;

namespace Zaber.Tuning.Tests.Servo
{
	[TestFixture]
	[SetCulture("en-US")]
	public class PidTunerV4Tests
	{
		[SetUp]
		public void Setup() => _device = TunerTestHelper.CreateDmqDeviceProperties();


		[Test]
		[Description("Test that the PID tuner produces output that varies with input.")]
		public void TestCalculate()
		{
			var tuner = new PidTunerV4();
			var uc = PidTunerV4.PidUnitTable;
			var inputs = TunerTestHelper.CreateEmptySet(TuningModeNames.USERPID, uc, TunerTestHelper.USERPID_SETTINGS);

			inputs[PidTunerV3.PARAM_KP] = new Measurement(1.0, uc.FindUnitBySymbol("N/m"));
			inputs[PidTunerV3.PARAM_KI] = new Measurement(1.0, uc.FindUnitBySymbol("N/m⋅s"));
			inputs[PidTunerV3.PARAM_KD] = new Measurement(1.0, uc.FindUnitBySymbol("N⋅s/m"));
			inputs[PidTunerV3.PARAM_FC] = new Measurement(12.0, uc.FindUnitBySymbol("kHz"));

			var set1 = tuner.CalculateParameters(_device, null, inputs);
			Assert.IsTrue(TunerTestHelper.SetHasParameters(set1, TunerTestHelper.FFPID_SETTINGS_V3));
			Assert.IsTrue(TunerTestHelper.SetIsNotEmpty(set1));

			// Not testing particular output values here.

			// Vary some of the inputs and verify the outputs are different.
			inputs[PidTunerV3.PARAM_KP] = new Measurement(0.5, uc.FindUnitBySymbol("N/m"));
			var set2 = tuner.CalculateParameters(_device, null, inputs);
			Assert.IsTrue(TunerTestHelper.SetHasParameters(set2, TunerTestHelper.FFPID_SETTINGS_V3));
			Assert.IsTrue(TunerTestHelper.SetIsNotEmpty(set2));
			Assert.IsFalse(set1.IsIdenticalTo(set2));

			inputs[PidTunerV3.PARAM_KI] = new Measurement(0.5, uc.FindUnitBySymbol("N/m⋅s"));
			var set3 = tuner.CalculateParameters(_device, null, inputs);
			Assert.IsTrue(TunerTestHelper.SetHasParameters(set3, TunerTestHelper.FFPID_SETTINGS_V3));
			Assert.IsTrue(TunerTestHelper.SetIsNotEmpty(set3));
			Assert.IsFalse(set1.IsIdenticalTo(set3));
			Assert.IsFalse(set2.IsIdenticalTo(set3));

			inputs[PidTunerV3.PARAM_KD] = new Measurement(0.5, uc.FindUnitBySymbol("N⋅s/m"));
			var set4 = tuner.CalculateParameters(_device, null, inputs);
			Assert.IsTrue(TunerTestHelper.SetHasParameters(set4, TunerTestHelper.FFPID_SETTINGS_V3));
			Assert.IsTrue(TunerTestHelper.SetIsNotEmpty(set4));
			Assert.IsFalse(set1.IsIdenticalTo(set4));
			Assert.IsFalse(set2.IsIdenticalTo(set4));
			Assert.IsFalse(set3.IsIdenticalTo(set4));

			inputs[PidTunerV3.PARAM_FC] = new Measurement(20.0, uc.FindUnitBySymbol("kHz"));
			var set5 = tuner.CalculateParameters(_device, null, inputs);
			Assert.IsTrue(TunerTestHelper.SetHasParameters(set5, TunerTestHelper.FFPID_SETTINGS_V3));
			Assert.IsTrue(TunerTestHelper.SetIsNotEmpty(set5));
			Assert.IsFalse(set1.IsIdenticalTo(set5));
			Assert.IsFalse(set2.IsIdenticalTo(set5));
			Assert.IsFalse(set3.IsIdenticalTo(set5));
			Assert.IsFalse(set4.IsIdenticalTo(set5));
		}


		private DeviceProperties _device;
	}
}

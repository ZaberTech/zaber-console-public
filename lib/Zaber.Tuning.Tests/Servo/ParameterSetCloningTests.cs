﻿using System;
using System.Linq;
using NUnit.Framework;
using Zaber.Tuning.Servo;
using Zaber.Units;

namespace Zaber.Tuning.Tests.Servo
{
	[TestFixture]
	[SetCulture("en-US")]
	public class ParameterSetCloningTests
	{
		[Test]
		public void TestCloneIiraParameterSet()
		{
			var found = false;

			foreach (var type in typeof(ITunerParameterSet).Assembly.GetExportedTypes())
			{
				if (typeof(ITunerParameterSet).IsAssignableFrom(type) && !type.IsAbstract)
				{
					TestClone(type);
					found = true;
				}
			}

			Assert.IsTrue(found, "Didn't find any tuner set types.");
		}


		private void TestClone(Type aSetType)
		{
			var set1 = Activator.CreateInstance(aSetType, null) as ITunerParameterSet;
			Assert.IsNotNull(set1, "Failed to instantiate " + aSetType.FullName);

			var uc = set1.UnitConverter ?? UnitConverter.Default;

			// If the parameter set type doesn't self-populate, create some test parameters.
			if (set1.ParameterNames.Count() == 0)
			{
				set1["Foo"] = new Measurement(0.0, uc.FindUnitBySymbol("g"));
				set1["Bar"] = new Measurement(0.0, uc.FindUnitBySymbol("m"));
			}

			var i = 1;
			foreach (var name in set1.ParameterNames)
			{
				set1[name] = new Measurement(i++, set1[name].Unit);
			}

			var set2 = set1.Clone();
			Assert.IsNotNull(set2, "Failed to clone " + aSetType.FullName);
			Assert.IsTrue(set1.IsIdenticalTo(set2),
						  "Cloned set not identical to original for type " + aSetType.FullName);
			Assert.IsTrue(set2.IsIdenticalTo(set1),
						  "Original set not identical to clone for type " + aSetType.FullName);
		}
	}
}

﻿using System;
using System.Linq;
using NUnit.Framework;
using Zaber.Tuning.Servo;
using Zaber.Units;

namespace Zaber.Tuning.Tests.Servo
{
	[TestFixture]
	[SetCulture("en-US")]
	public class TunerParameterSetTests
	{
		[Test]
		public void TestDefaults()
		{
			var set = new TunerParameterSet("Foo");
			Assert.IsNotNull(set);
			Assert.AreEqual("Foo", set.ModeName);
			Assert.AreEqual(0, set.ParameterNames.Count());
		}


		[Test]
		public void TestEditable()
		{
			var set = new TunerParameterSet("Foo");

			foreach (var name in new[] { "Foo", "Bar", "Baz", "Quux" })
			{
				set[name] = new Measurement(name.GetHashCode(), null);
			}

			foreach (var name in set.ParameterNames)
			{
				Assert.AreEqual(name.GetHashCode(), (int) Math.Round(set[name].Value));
			}
		}
	}
}

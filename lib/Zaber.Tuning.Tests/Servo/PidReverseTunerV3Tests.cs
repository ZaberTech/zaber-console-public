﻿using NUnit.Framework;
using Zaber.Tuning.Servo;
using Zaber.Units;

namespace Zaber.Tuning.Tests.Servo
{
	[TestFixture]
	[SetCulture("en-US")]
	public class PidReverseTunerV3Tests
	{
		[SetUp]
		public void Setup() => _device = TunerTestHelper.CreateDmqDeviceProperties();


		[Test]
		[Description("Test that the reverse PID tuner produces output that varies with input.")]
		public void TestCalculate()
		{
			var tuner = new PidReverseTunerV3();
			var uc = PidTunerV3.PidUnitTable;
			var inputs = TunerTestHelper.CreateEmptySet(TuningModeNames.FFPID, uc, TunerTestHelper.FFPID_SETTINGS_V3);

			var x = 1.0;
			foreach (var name in _device.SupportedTuningModes[TuningModeNames.FFPID])
			{
				inputs[name] = new Measurement(x, null);
				x += 1.0;
			}

			var set1 = tuner.CalculateParameters(_device, null, inputs);
			Assert.IsTrue(TunerTestHelper.SetHasParameters(set1, TunerTestHelper.USERPID_SETTINGS));
			Assert.IsTrue(TunerTestHelper.SetIsNotEmpty(set1));

			// Not testing particular output values here.

			// Vary some of the inputs and verify the outputs are different.
			inputs["kp"] = new Measurement(x += 1.0, null);
			var set2 = tuner.CalculateParameters(_device, null, inputs);
			Assert.IsTrue(TunerTestHelper.SetHasParameters(set2, TunerTestHelper.USERPID_SETTINGS));
			Assert.IsTrue(TunerTestHelper.SetIsNotEmpty(set2));
			Assert.IsFalse(set1.IsIdenticalTo(set2));

			inputs["ki"] = new Measurement(x += 1.0, null);
			var set3 = tuner.CalculateParameters(_device, null, inputs);
			Assert.IsTrue(TunerTestHelper.SetHasParameters(set3, TunerTestHelper.USERPID_SETTINGS));
			Assert.IsTrue(TunerTestHelper.SetIsNotEmpty(set3));
			Assert.IsFalse(set1.IsIdenticalTo(set3));
			Assert.IsFalse(set2.IsIdenticalTo(set3));

			inputs["kd"] = new Measurement(x += 1.0, null);
			var set4 = tuner.CalculateParameters(_device, null, inputs);
			Assert.IsTrue(TunerTestHelper.SetHasParameters(set4, TunerTestHelper.USERPID_SETTINGS));
			Assert.IsTrue(TunerTestHelper.SetIsNotEmpty(set4));
			Assert.IsFalse(set1.IsIdenticalTo(set4));
			Assert.IsFalse(set2.IsIdenticalTo(set4));
			Assert.IsFalse(set3.IsIdenticalTo(set4));

			inputs["fc1"] = new Measurement(x += 1.0, null);
			var set5 = tuner.CalculateParameters(_device, null, inputs);
			Assert.IsTrue(TunerTestHelper.SetHasParameters(set5, TunerTestHelper.USERPID_SETTINGS));
			Assert.IsTrue(TunerTestHelper.SetIsNotEmpty(set5));
			Assert.IsFalse(set1.IsIdenticalTo(set5));
			Assert.IsFalse(set2.IsIdenticalTo(set5));
			Assert.IsFalse(set3.IsIdenticalTo(set5));
			Assert.IsFalse(set4.IsIdenticalTo(set5));
		}


		private DeviceProperties _device;
	}
}

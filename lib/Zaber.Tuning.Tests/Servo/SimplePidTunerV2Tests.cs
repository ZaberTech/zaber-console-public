﻿using System.Linq;
using NUnit.Framework;
using Zaber.Tuning.Servo;
using Zaber.Units;

namespace Zaber.Tuning.Tests.Servo
{
	[TestFixture]
	[SetCulture("en-US")]
	public class SimplePidTunerV2Tests
	{
		[SetUp]
		public void Setup()
		{
			_device = TunerTestHelper.CreateDmqDeviceProperties();

			// Replace expected parameter set with the v2 ones.
			_device.SupportedTuningModes[TuningModeNames.FFPID] = TunerTestHelper.FFPID_SETTINGS_V2;

			_load = TunerTestHelper.CreateLoad(0);
		}


		[Test]
		[Description("Test that the simple tuner produces output that varies with input.")]
		public void TestCalculate()
		{
			var tuner = new SimplePidTunerV2();
			var uc = PidTunerV2.PidUnitTable;
			var inputs = TunerTestHelper.CreateEmptySet(TuningModeNames.FFPID, uc, TunerTestHelper.FFPID_SETTINGS_V2);

			inputs["finv1"] = new Measurement(1.0, null);
			inputs["finv2"] = new Measurement(1.0, null);
			inputs["fcla"] = new Measurement(0.993, null);
			inputs["fclg"] = new Measurement(0.272, null);
			inputs["ki"] = new Measurement(67.0, null);
			inputs["kp"] = new Measurement(29000.0, null);
			inputs["kd"] = new Measurement(8000000.0, null);
			inputs["tf"] = new Measurement(0.21, null);
			inputs["gain"] = new Measurement(1000000.0, null);
			inputs["gain_hs"] = inputs["gain"];
			inputs["tf.hs"] = inputs["tf"];
			inputs["kd.hs"] = inputs["kd"];
			inputs["kp.hs"] = inputs["kp"];
			inputs["ki.hs"] = inputs["ki"];
			inputs["fclg.hs"] = inputs["fclg"];
			inputs["fcla.hs"] = inputs["fcla"];

			var set1 = tuner.CalculateParameters(_device, _load, inputs);
			Assert.IsTrue(TunerTestHelper.SetHasParameters(set1, TunerTestHelper.FFPID_SETTINGS_V2));
			Assert.IsTrue(TunerTestHelper.SetIsNotEmpty(set1));

			// Not testing particular output values here.
		}


		[Test]
		[Description("Test that the simple tuner passes through extra data expected by the device, defaulting to 0 if no input data matches")]
		public void TestDataPassThrough()
		{
			var tuner = new SimplePidTunerV2();
			var uc = PidTunerV2.PidUnitTable;
			var inputs = TunerTestHelper.CreateEmptySet(TuningModeNames.FFPID, uc, TunerTestHelper.FFPID_SETTINGS_V2);

			// Add some made-up parameter names to the list expected by the device.
			var names = _device.SupportedTuningModes[TuningModeNames.FFPID].ToList();
			names.Add("foo");
			names.Add("bar");
			_device.SupportedTuningModes[TuningModeNames.FFPID] = names.ToArray();

			// Insert an input value for one of the made-up names.
			inputs["bar"] = new Measurement(1000.0, null);

			var set1 = tuner.CalculateParameters(_device, _load, inputs);

			// Verify that the output passes though set values for params that are in the tuning
			// mode, and doesn't generate defaults for those that are not.
			Assert.IsFalse(set1.ParameterNames.Contains("foo"));
			Assert.IsTrue(set1.ParameterNames.Contains("bar"));
			Assert.Less(1.0, set1["bar"].Value);
		}


		[Test]
		[Description("Test that gain and Kp vary as expected with inertia changes.")]
		public void RM7087_GainVersusInertia()
		{
			var tuner = new SimplePidTunerV2();
			var uc = PidTunerV3.PidUnitTable;
			var grams = uc.FindUnitBySymbol("g");
			var inputs = TunerTestHelper.CreateEmptySet(TuningModeNames.FFPID, uc, TunerTestHelper.FFPID_SETTINGS_V2);

			// Set up real numbers for an LDQ.
			inputs["kp"] = new Measurement(44686.03, null);
			_device.ForqueConstant = 15.83;
			_device.MaximumForce = new Measurement(67.1192, uc.FindUnitBySymbol("N"));
			_device.PositionUnit = uc.FindUnitBySymbol("nm");
			_device.CarriageInertia = new Measurement(1500.0, grams);

			double scaling = 1000000000.0;

			_load.LoadInertia = new Measurement(0.0, grams);
			var output = tuner.CalculateParameters(_device, _load, inputs);
			Assert.AreEqual(0.099 * scaling, output["gain"].Value, 0.05 * scaling);
			Assert.AreEqual(44.35 * scaling, output["kp"].Value, 1 * scaling);

			_load.LoadInertia = new Measurement(1500.0, grams);
			_device.CarriageInertia = new Measurement(1500, grams);
			output = tuner.CalculateParameters(_device, _load, inputs);
			Assert.AreEqual(0.198 * scaling, output["gain"].Value, 0.05 * scaling);
			// V2 does not change Kp based on load.
		}


		private DeviceProperties _device;
		private LoadProperties _load;
	}
}

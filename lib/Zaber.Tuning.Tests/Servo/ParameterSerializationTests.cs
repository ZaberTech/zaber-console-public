﻿using System;
using System.Linq;
using NUnit.Framework;
using Zaber.Tuning.Servo;
using Zaber.Units;
using ZaberTest.Testing;

namespace Zaber.Tuning.Tests.Servo
{
	[TestFixture]
	[SetCulture("en-US")]
	public class ParameterSerializationTests
	{
		[Test]
		public void TestSerializeAllParameterTypes()
		{
			foreach (var type in typeof(ITunerParameterSet).Assembly.GetExportedTypes())
			{
				if (!type.IsAbstract && typeof(ITunerParameterSet).IsAssignableFrom(type))
				{
					// Create a set.
					var set = Activator.CreateInstance(type, null) as ITunerParameterSet;
					Assert.IsNotNull(set, "Failed to instantiate " + type.Name);

					var uc = set.UnitConverter ?? UnitConverter.Default;

					// If the parameter set type doesn't self-populate, create some test parameters.
					if (set.ParameterNames.Count() == 0)
					{
						set["Foo"] = new Measurement(0.0, uc.FindUnitBySymbol("g"));
						set["Bar"] = new Measurement(0.0, uc.FindUnitBySymbol("m"));
					}

					// Fill in the set with non-default data to test comparison.
					// Note that these are integer test values because XML serialization
					// does not guarantee perfect preservation of floats.
					foreach (var name in set.ParameterNames)
					{
						var unit = set[name].Unit;
						if (Unit.Dimensionless != unit)
						{
							var alternates = uc.FindUnits(unit.Dimension, Prefix.All).ToArray();
							if (alternates.Length > 0)
							{
								unit = alternates[(name.GetHashCode() & 0x7FFFFFFF) % alternates.Length];
							}
						}

						set[name] = new Measurement(name.GetHashCode(), unit);
					}

					string xml = null;
					try
					{
						xml = ObjectToXmlString.Serialize(set);
					}
					catch (Exception aException)
					{
						throw new Exception("Failed to serialize " + type.Name, aException);
					}

					Assert.IsFalse(string.IsNullOrEmpty(xml), "No serialization output from " + type.Name);

					ITunerParameterSet set2 = null;
					try
					{
						set2 = ObjectToXmlString.Deserialize(xml, set.GetType()) as ITunerParameterSet;
					}
					catch (Exception aException)
					{
						throw new Exception("Failed to deserialize " + type.Name, aException);
					}

					Assert.IsNotNull(set2, "No deserialization result for " + type.Name);
					Assert.IsTrue(set.IsIdenticalTo(set2),
								  "Deserialized set is different from original for " + type.Name);
				}
			}
		}
	}
}

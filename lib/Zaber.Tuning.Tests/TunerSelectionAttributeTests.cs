﻿using NUnit.Framework;

namespace Zaber.Tuning.Tests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class TunerSelectionAttributeTests
	{
		[Test]
		public void TestConstructor()
		{
			var attr = new TunerSelectionAttribute("Foo", "Bar", 1, "Baz", 1);
			Assert.AreEqual("Foo", attr.AlgorithmName);
			Assert.AreEqual("Bar", attr.InputModeName);
			Assert.AreEqual("Baz", attr.OutputModeName);
		}


		[Test]
		public void TestConstructorNoInput1()
		{
			var attr = new TunerSelectionAttribute("Foo", null, 1, "Baz", 1);
			Assert.AreEqual("Foo", attr.AlgorithmName);
			Assert.IsNull(attr.InputModeName);
			Assert.AreEqual("Baz", attr.OutputModeName);
		}


		[Test]
		public void TestConstructorNoInput2()
		{
			var attr = new TunerSelectionAttribute("Foo", string.Empty, 1, "Baz", 1);
			Assert.AreEqual("Foo", attr.AlgorithmName);
			Assert.AreEqual(string.Empty, attr.InputModeName);
			Assert.AreEqual("Baz", attr.OutputModeName);
		}
	}
}

﻿using System.Collections.Generic;
using System.Linq;
using Zaber.Tuning.Servo;
using Zaber.Units;

namespace Zaber.Tuning.Tests
{
	/// <summary>
	///     Helper class for servo tuner tests.
	/// </summary>
	public class TunerTestHelper
	{
		#region -- Public data --

		// All device settings expected to be in an output set.
		public static readonly string[] FFPID_SETTINGS_V2 =
		{
			"finv1", "finv2", "fcla", "fcla.hs", "fclg", "fclg.hs", "ki", "ki.hs", "kp", "kp.hs", "kd", "kd.hs",
			"tf", "tf.hs", "gain"
		};

		public static readonly string[] FFPID_SETTINGS_V3 =
		{
			"finv1", "finv2", "fcla", "fcla.hs", "fclg", "fclg.hs", "ki", "ki.hs", "kp", "kp.hs", "kd", "kd.hs",
			"fc1", "fc1.hs", "fc2", "fc2.hs", "gain"
		};

		public static readonly string[] FFPID_SETTINGS_V3_SIMPLE =
		{
			"finv2", "fcla", "fcla.hs", "fclg", "fclg.hs", "ki", "ki.hs", "kp", "kp.hs", "kd", "kd.hs", "fc1",
			"fc1.hs", "fc2", "fc2.hs", "gain"
		};


		// All parameters expected in a user PID set.
		public static readonly string[] USERPID_SETTINGS = { "kp", "ki", "kd", "fc" };

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Create an example set of load properties.
		/// </summary>
		/// <param name="aLoadGrams">Load intertia in grams or gram-square meters.</param>
		/// <param name="aUnit">The unit of measure to use. Defaults to grams.</param>
		/// <returns>A new instance of <see cref="LoadProperties" /> </returns>
		public static LoadProperties CreateLoad(int aLoadGrams, Unit aUnit = null)
		{
			var result = new LoadProperties();

			if (null == aUnit)
			{
				aUnit = PidTunerV3.PidUnitTable.FindUnits(Dimension.Inertia)
							   .Where(u => u.Abbreviation == "g")
							   .FirstOrDefault();

				// Temporary workaround until Zaber.Units supports duplicate symbols.
				if (null == aUnit)
				{
					aUnit = new BaseUnit("g", "grams") { Dimension = Dimension.Inertia };
				}
			}

			result.LoadInertia = new Measurement(aLoadGrams, aUnit);

			return result;
		}


		/// <summary>
		///     Create a DeviceProperties representing chracteristics of a DMQ.
		/// </summary>
		/// <returns>
		///     A completed instance of <see cref="DeviceProperties" /> with values
		///     representing the X-DMQ.
		/// </returns>
		public static DeviceProperties CreateDmqDeviceProperties()
		{
			var d = new DeviceProperties();
			var uc = UnitConverter.Default;
			d.ControlLoopFrequency = new Measurement(10, uc.FindUnitBySymbol("kHz"));
			d.PositionUnit = new ScaledShiftedUnit(uc.FindUnitBySymbol("m"), 1.0 / 5000000.0);
			d.ForceUnit = new ScaledShiftedUnit(uc.FindUnitBySymbol("N"), 1.0 / 40.0);
			d.CurrentUnit = new ScaledShiftedUnit(uc.FindUnitBySymbol("A"), 1.0 / 50.0);
			d.SupportedTuningModes = new Dictionary<string, string[]> { { TuningModeNames.FFPID, FFPID_SETTINGS_V3 } };
			d.ForqueConstant = 12.5;
			d.CarriageInertia = new Measurement(93.0, uc.FindUnitBySymbol("g"));
			d.MaximumForce = new Measurement(34, uc.FindUnitBySymbol("N"));

			return d;
		}


		/// <summary>
		///     Returns true if a parameter set contains any nonzero values.
		/// </summary>
		/// <param name="aSet">Set to test.</param>
		/// <returns>True if any parameter in the set has a nonzero quantity.</returns>
		public static bool SetIsNotEmpty(ITunerParameterSet aSet) => aSet.Where(pair => pair.Value.Value != 0.0).Any();


		/// <summary>
		///     Returns true if a parameter set has all and only the given set of parameter names.
		/// </summary>
		/// <param name="aSet">The set to test.</param>
		/// <param name="aParamNames">The list of expected parameter names.</param>
		/// <returns>True if the set has all and only the expected names, and all of them have non-null measurements.</returns>
		public static bool SetHasParameters(ITunerParameterSet aSet, IEnumerable<string> aParamNames)
		{
			var expected = new HashSet<string>(aParamNames);
			if (aSet.ParameterNames.Count() != expected.Count)
			{
				return false;
			}

			// If the counts are equal, then we only need to test that all names in set A are in set B.
			foreach (var name in aSet.ParameterNames)
			{
				if (!expected.Contains(name))
				{
					return false;
				}

				if (null == aSet[name])
				{
					return false;
				}
			}

			return true;
		}


		/// <summary>
		///     Initialize a new tuning parameter set with all zeroes and no units.
		/// </summary>
		/// <param name="aModeName">Tuning mode name to assign to the set.</param>
		/// <param name="aUnitConverter">Unit converter to assign to the set.</param>
		/// <param name="aParamNames">List of parameter names to create.</param>
		/// <returns>An intiialized parameter set.</returns>
		public static ITunerParameterSet CreateEmptySet(string aModeName, IUnitConverter aUnitConverter,
														IEnumerable<string> aParamNames)
		{
			var result = new TunerParameterSet(aModeName) { UnitConverter = aUnitConverter };

			foreach (var name in aParamNames)
			{
				result[name] = new Measurement(0.0, null);
			}

			return result;
		}

		#endregion
	}
}

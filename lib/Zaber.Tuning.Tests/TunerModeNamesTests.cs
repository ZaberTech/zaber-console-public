﻿using System.Linq;
using NUnit.Framework;

namespace Zaber.Tuning.Tests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class TunerModeNamesTests
	{
		[Test]
		public void TestAll() => Assert.Contains(TuningModeNames.FFPID, TuningModeNames.All.ToList());
	}
}

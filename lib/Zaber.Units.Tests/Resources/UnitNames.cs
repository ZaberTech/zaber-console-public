﻿using System.Collections.Generic;
using System.Linq;

namespace Zaber.Units.Tests
{
	internal static class UnitNames
	{
		#region -- Public data --

		public static IEnumerable<string> LengthSI = new[] { "m" };

		public static IEnumerable<string> LengthOther = new[] { "ft", "in", "thou", "mil", "mi", "yd", "ly", "AU" };

		public static IEnumerable<string> AreaSI = new[] { "ha" };

		public static IEnumerable<string> AreaOther = new[] { "m^2", "ac" };

		public static IEnumerable<string> VolumeSI = new[] { "l" };

		public static IEnumerable<string> VolumeOther = new[] { "qt", "c", "pt", "fl.oz", "gal" };

		public static IEnumerable<string> TimeSI = new[] { "s" };

		public static IEnumerable<string> TimeOther = new[] { "min", "h", "d", "y", "wk" };

		public static IEnumerable<string> FrequencySI = new[] { "Hz" };

		public static IEnumerable<string> LinearSpeedSI = new[] { "m/s" };

		public static IEnumerable<string> LinearSpeedOther = new[] { "km/h", "mph", "in/s" };

		public static IEnumerable<string> LinearAccelerationSI = new[] { "m/s^2" };

		public static IEnumerable<string> LinearAccelerationOther = new[] { "in/s^2", "gn" };

		public static IEnumerable<string> AngleSI = new[] { "rad", "°", "grad" };

		public static IEnumerable<string> AngleOther = new[] { "rot.", "turn", "rev" };

		public static IEnumerable<string> AngularSpeedSI = new[] { "rad/s", "°/s", "rpm" };

		public static IEnumerable<string> AngularSpeedOther = new[] { "rpm" };

		public static IEnumerable<string> AngularAcceleration = new[] { "rad/s^2", "°/s^2" };

		public static IEnumerable<string> TemperatureSI = new[] { "°K" };

		public static IEnumerable<string> TemperatureOther = new[] { "°C", "°F" };

		public static IEnumerable<string> Quantity = new[] { "units" };

		public static IEnumerable<string> Ratio = new[] { "%", "10ths", "100ths", "doz." };

		public static IEnumerable<string> WeightSI = new[] { "g" };

		public static IEnumerable<string> WeightOther = new[] { "lb", "oz" };

		public static IEnumerable<string> ForceSI = new[] { "N" };

		public static IEnumerable<string> ForceOther = new[] { "lbf", "gf", "dyn" };

		public static IEnumerable<string> TorqueSI = new[] { "Nm" };

		public static IEnumerable<string> TorqueOther = new[] { "lbf-ft" };

		public static IEnumerable<string> PowerSI = new[] { "W" };

		public static IEnumerable<string> PowerOther = new[] { "hp" };

		public static IEnumerable<string> EnergySI = new[] { "J" };

		public static IEnumerable<string> EnergyOther = new[] { "erg", "BTU" };

		public static IEnumerable<string> Data = new[]
		{
			"b", "B", "nyb", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"
		};

		public static IEnumerable<string> AcCurrentSI = new[] { "Apk", "Ap-p", "Arms" };

		public static IEnumerable<string> DcCurrentSI = new[] { "A" };

		// Random selection of nonsense units that should not exist, as a sanity check.
		public static IEnumerable<string> Invalid = new[]
		{
			"kft", "mmile", "Myd", "nAU", "Mmi/h", "mmin", "kgal", "Mqt", "krot.", "Mlb", "µdyn"
		};

		#endregion

		#region -- Public Methods & Properties --

		public static IEnumerable<string> GenerateSIPrefixedNames(IEnumerable<string> aUnitAbbrevs)
		{
			foreach (var abbr in aUnitAbbrevs)
			{
				foreach (var prefix in Prefix.All.Select(p => p.Abbreviation))
				{
					yield return prefix + abbr;
				}
			}
		}

		#endregion
	}
}

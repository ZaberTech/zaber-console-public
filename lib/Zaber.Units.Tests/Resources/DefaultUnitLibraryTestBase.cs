﻿using NUnit.Framework;

namespace Zaber.Units.Tests
{
	public abstract class DefaultUnitLibraryTestBase
	{
		#region -- Public Methods & Properties --

		[SetUp]
		public void Setup()
		{
			PreSetup();

			var parser = ComposedUnitParser.NewDefaultUnitParser();
			Parser = parser;
			Registry = parser.GetUnitRegistry();

			Parser.LoadDefaultUnitLibrary(Registry);

			PostSetup();
		}


		protected IUnitParser Parser { get; private set; }


		protected IUnitRegistry Registry { get; private set; }

		#endregion

		#region -- Non-Public Methods --

		protected virtual void PreSetup()
		{
		}


		protected virtual void PostSetup()
		{
		}

		#endregion

		#region -- Data --

		#endregion
	}
}

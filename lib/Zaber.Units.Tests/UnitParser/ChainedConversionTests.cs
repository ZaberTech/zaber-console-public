﻿using System.Reflection;
using System.Xml;
using NUnit.Framework;

namespace Zaber.Units.Tests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class ChainedConversionTests
	{
		[SetUp]
		public void Setup()
		{
			var p1 = ComposedUnitParser.NewDefaultUnitParser();
			_parser1 = p1;
			_registry1 = p1.GetUnitRegistry();

			var p2 = ComposedUnitParser.NewDefaultUnitParser();
			_parser2 = p2;
			_registry2 = p2.GetUnitRegistry();

			var reader = XmlReader.Create(Assembly.GetExecutingAssembly()
											   .GetManifestResourceStream(
													   "Zaber.Units.Tests.Resources.StandardUnits.xml"));
			var library = new XmlUnitLibrary(reader);
			library.LoadTo(_registry1, _parser1);

			reader = XmlReader.Create(Assembly.GetExecutingAssembly()
										   .GetManifestResourceStream("Zaber.Units.Tests.Resources.CustomUnits.xml"));
			library = new XmlUnitLibrary(reader);
			library.LoadTo(_registry2, _parser2);
		}


		[Test]
		public void TestCustomToStandard_NoFallback()
		{
			var customUnit = _parser2.TryParseUnit("qb");
			Assert.IsNotNull(customUnit);

			var standardUnit = _parser1.TryParseUnit("ft");
			Assert.IsNotNull(standardUnit);

			var conv = customUnit.GetTransformationTo(standardUnit);
			var feet = conv.Transform(1.0);
			Assert.AreEqual(1.5, feet, 0.001);
		}


		[Test]
		public void TestStandardToCustom()
		{
			var customUnit = _parser2.TryParseUnit("qb");
			Assert.IsNotNull(customUnit);

			var standardUnit = _parser1.TryParseUnit("ft");
			Assert.IsNotNull(standardUnit);

			var conv = standardUnit.GetTransformationTo(customUnit);
			var cubits = conv.Transform(1.5);
			Assert.AreEqual(1.0, cubits, 0.001);
		}


		private IUnitParser _parser1;
		private IUnitRegistry _registry1;
		private IUnitParser _parser2;
		private IUnitRegistry _registry2;
	}
}

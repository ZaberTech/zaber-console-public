﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Zaber.Units.Tests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class BasicUnitConversionTests : DefaultUnitLibraryTestBase
	{
		[Test]
		public void TestAllConversionsExistsWithinDimensions()
		{
			foreach (var dim in Dimension.AllDimensions)
			{
				var unitNamesForGroup = new List<string>();

				foreach (var unit in Registry.RegisteredUnits.Values.Where(u => dim == u.Dimension))
				{
					if (unit.UseSIPrefixes)
					{
						unitNamesForGroup.AddRange(UnitNames.GenerateSIPrefixedNames(new[] { unit.Abbreviation }));
					}
					else
					{
						unitNamesForGroup.Add(unit.Abbreviation);
					}
				}

				TestAllConversionsExist(unitNamesForGroup);
			}
		}


		[Test]
		public void TestCommonValueConversions()
		{
			Assert.AreEqual(1000, Convert(1, "km", "m"), 0.000001);

			//Assert.AreEqual(10, Convert(0.1, "s", "Hz"), 0.000001); // Lib doesn't do inversions yet.
			Assert.AreEqual(212, Convert(100, "°C", "°F"), 0.000001);
			Assert.AreEqual(2.4710538146716532, Convert(1, "ha", "ac"), 0.000001);
			Assert.AreEqual(4.54609, Convert(1, "gal", "l"), 0.000001);
			Assert.AreEqual(86400, Convert(1, "d", "s"), 0.000001);
			Assert.AreEqual(0.44704, Convert(1, "mph", "m/s"), 0.000001);
			Assert.AreEqual(9.80665, Convert(1, "gn", "m/s^2"), 0.000001);
			Assert.AreEqual(360.0, Convert(1, "rot.", "°"), 0.000001);
			Assert.AreEqual(0.10471975511965999, Convert(1, "rpm", "rad/s"), 0.000001);
			Assert.AreEqual(100, Convert(1, "units", "%"), 0.000001);
			Assert.AreEqual(2.2046226218487757, Convert(1, "kg", "lb"), 0.000001);
			Assert.AreEqual(8192, Convert(1, "KiB", "b"), 0.000001);
		}


		[Test]
		public void TestSimplify()
		{
			// kg * m / s^2 should produce N
			var kilos = Parser.TryParseUnit("kg");
			var meters = Parser.TryParseUnit("m");
			var seconds = Parser.TryParseUnit("s");

			var s2 = seconds * seconds;

			var joules = (kilos * meters * meters) / s2;

			Assert.AreEqual(Parser.TryParseUnit("J"), joules);
		}


		private void TestAllConversionsExist(IEnumerable<string> aGroup)
		{
			var unitNames = aGroup.ToArray();
			foreach (var t in unitNames)
			{
				foreach (var t1 in unitNames)
				{
					var name1 = t;
					var name2 = t1;
					Assert.IsNotNull(GetConversion(name1, name2), $"No conversion from {name1} to {name2} exists.");
				}
			}
		}


		private QuantityTransformation GetConversion(string aFromUnit, string aToUnit)
		{
			Unit sourceUnit = null;
			Unit destUnit = null;

			sourceUnit = Parser.TryParseUnit(aFromUnit);
			Assert.IsNotNull(sourceUnit);
			destUnit = Parser.TryParseUnit(aToUnit);
			Assert.IsNotNull(destUnit);

			var transformation = sourceUnit.GetTransformationTo(destUnit);
			Assert.IsNotNull(transformation);
			return transformation;
		}


		private double Convert(double aValue, string aFromUnit, string aToUnit)
			=> GetConversion(aFromUnit, aToUnit).Transform(aValue);
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using NUnit.Framework;

namespace Zaber.Units.Tests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class DimensionTests
	{
		[Test]
		public void TestAllDefaultDimensionsInList()
		{
			var allNames = new HashSet<string>(Dimension.AllDimensions.Select(d => d.Name));
			var seen = new HashSet<string>();

			foreach (var dim in typeof(Dimension).GetProperties(BindingFlags.Public | BindingFlags.Static))
			{
				if (typeof(Dimension).IsAssignableFrom(dim.PropertyType))
				{
					var d = dim.GetValue(null, null) as Dimension;
					Assert.IsNotNull(d, "Failed to retrieve property " + d.Name);
					Assert.IsTrue(allNames.Contains(d.Name), "Property " + d.Name + " was not added to AllDimensions.");
					seen.Add(d.Name);
				}
			}

			Assert.AreEqual(allNames.Count, seen.Count, "AllDimensions count not the same as public property count.");
		}


		[Test]
		public void TestCanAddNew()
		{
			var count = Dimension.AllDimensions.Count();
			Assert.IsNull(Dimension.GetByName("FooBar"));
			new Dimension("FooBar");
			Assert.IsNotNull(Dimension.GetByName("FooBar"));
			Assert.AreEqual(count + 1, Dimension.AllDimensions.Count());
		}


		[Test]
		public void TestExceptionOnClash()
		{
			Assert.IsNotNull(Dimension.GetByName("Length"));
			Assert.Throws<ArgumentException>(() => { new Dimension("Length"); });
		}
	}
}

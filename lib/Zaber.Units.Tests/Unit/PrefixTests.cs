﻿using System;
using System.Linq;
using NUnit.Framework;

namespace Zaber.Units.Tests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class PrefixTests
	{
		[Test]
		public void TestDistinct()
		{
			var abbrevs = Prefix.All.Select(p => p.Abbreviation).Distinct();
			Assert.AreEqual(Prefix.All.Count(), abbrevs.Count());
			var names = Prefix.All.Select(p => p.Name).Distinct();
			Assert.AreEqual(Prefix.All.Count(), names.Count());
			var exp = Prefix.All.Select(p => p.Exponent).Distinct();
			Assert.AreEqual(Prefix.All.Count(), exp.Count());
			var fac = Prefix.All.Select(p => p.Factor).Distinct();
			Assert.AreEqual(Prefix.All.Count(), fac.Count());
		}


		[Test]
		public void TestExponentsAgree()
		{
			foreach (var p in Prefix.All)
			{
				Assert.AreEqual(Math.Pow(10, p.Exponent), p.Factor, 0.0001 * p.Factor);
			}
		}


		[Test]
		public void TestPowersOfThousand()
		{
			Assert.Less(Prefix.PowersOfThousand.Count(), Prefix.All.Count());
			foreach (var p in Prefix.PowersOfThousand)
			{
				Assert.IsTrue((p.Exponent % 3) == 0);
				Assert.IsTrue(((int) Math.Round(Math.Log10(p.Factor)) % 3) == 0);
			}
		}
	}
}

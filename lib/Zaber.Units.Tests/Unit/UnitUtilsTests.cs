﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Zaber.Units.Tests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class UnitUtilsTests
	{
		[OneTimeSetUp]
		public void Setup() 
			=> _lengthUnits = UnitConverter.Default.FindUnits(Dimension.Length, Prefix.All).ToList();


		[Test]
		public void DiscardRelativeToCoherent()
		{
			Assert.IsTrue(_lengthUnits.Any(u => "µm" == u.Abbreviation));
			Assert.IsTrue(_lengthUnits.Any(u => "Mm" == u.Abbreviation));

			var result = UnitUtils.DiscardExtremeUnits(_lengthUnits, -3, 3).ToList();
			Assert.IsFalse(result.Any(u => "µm" == u.Abbreviation));
			Assert.IsFalse(result.Any(u => "Mm" == u.Abbreviation));
			Assert.IsTrue(result.Any(u => "m" == u.Abbreviation));
			Assert.IsTrue(result.Any(u => "mm" == u.Abbreviation));
			Assert.IsTrue(result.Any(u => "km" == u.Abbreviation));
		}


		[Test]
		public void DiscardRelativeToSpecificLarge()
		{
			Assert.IsTrue(_lengthUnits.Any(u => "µm" == u.Abbreviation));
			Assert.IsTrue(_lengthUnits.Any(u => "mm" == u.Abbreviation));
			Assert.IsTrue(_lengthUnits.Any(u => "Mm" == u.Abbreviation));
			Assert.IsTrue(_lengthUnits.Any(u => "Gm" == u.Abbreviation));

			var kilometer = UnitConverter.Default.FindUnitBySymbol("km");
			var result = UnitUtils.DiscardExtremeUnits(_lengthUnits, -3, 3, kilometer).ToList();
			Assert.IsFalse(result.Any(u => "µm" == u.Abbreviation));
			Assert.IsFalse(result.Any(u => "mm" == u.Abbreviation));
			Assert.IsTrue(result.Any(u => "m" == u.Abbreviation));
			Assert.IsTrue(result.Any(u => "km" == u.Abbreviation));
			Assert.IsTrue(result.Any(u => "Mm" == u.Abbreviation));
			Assert.IsFalse(result.Any(u => "Gm" == u.Abbreviation));
		}


		[Test]
		public void DiscardRelativeToSpecificSmall()
		{
			Assert.IsTrue(_lengthUnits.Any(u => "nm" == u.Abbreviation));
			Assert.IsTrue(_lengthUnits.Any(u => "µm" == u.Abbreviation));
			Assert.IsTrue(_lengthUnits.Any(u => "mm" == u.Abbreviation));
			Assert.IsTrue(_lengthUnits.Any(u => "Mm" == u.Abbreviation));
			Assert.IsTrue(_lengthUnits.Any(u => "Gm" == u.Abbreviation));

			var millimeter = UnitConverter.Default.FindUnitBySymbol("mm");
			var result = UnitUtils.DiscardExtremeUnits(_lengthUnits, -3, 3, millimeter).ToList();
			Assert.IsFalse(result.Any(u => "nm" == u.Abbreviation));
			Assert.IsTrue(result.Any(u => "µm" == u.Abbreviation));
			Assert.IsTrue(result.Any(u => "mm" == u.Abbreviation));
			Assert.IsTrue(result.Any(u => "m" == u.Abbreviation));
			Assert.IsFalse(result.Any(u => "km" == u.Abbreviation));
			Assert.IsFalse(result.Any(u => "Mm" == u.Abbreviation));
		}


		private List<Unit> _lengthUnits;
	}
}

﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Zaber.Units.Tests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class BasicUnitExistenceTests : DefaultUnitLibraryTestBase
	{
		[Test]
		public void TestExpectedAcCurrentUnits()
		{
			TestAllUnitsExist(Parser, UnitNames.GenerateSIPrefixedNames(UnitNames.AcCurrentSI));
			TestAllUnitsHaveDimension(Parser,
									  UnitNames.GenerateSIPrefixedNames(UnitNames.AcCurrentSI),
									  Dimension.AlternatingCurrent);
			TestFindUnitsProduces(Dimension.AlternatingCurrent, UnitNames.AcCurrentSI);
			TestFindUnitsProduces(Dimension.AlternatingCurrent,
								  UnitNames.GenerateSIPrefixedNames(UnitNames.AcCurrentSI),
								  Prefix.All);
		}


		[Test]
		public void TestExpectedAngularAccelerationUnits()
		{
			TestAllUnitsExist(Parser, UnitNames.GenerateSIPrefixedNames(UnitNames.AngularAcceleration));
			TestAllUnitsHaveDimension(Parser,
									  UnitNames.GenerateSIPrefixedNames(UnitNames.AngularAcceleration),
									  Dimension.AngularAcceleration);
			TestFindUnitsProduces(Dimension.AngularAcceleration, UnitNames.AngularAcceleration);
			TestFindUnitsProduces(Dimension.AngularAcceleration,
								  UnitNames.GenerateSIPrefixedNames(UnitNames.AngularAcceleration),
								  Prefix.All);
		}


		[Test]
		public void TestExpectedAngularSpeedUnits()
		{
			BasicUnitTests(Parser, UnitNames.AngularSpeedSI, UnitNames.AngularSpeedOther, Dimension.AngularVelocity);
			TestFindUnitsProduces(Dimension.AngularVelocity,
								  UnitNames.AngularSpeedSI.Concat(UnitNames.AngularSpeedOther));
		}


		[Test]
		public void TestExpectedAngularUnits()
		{
			BasicUnitTests(Parser, UnitNames.AngleSI, UnitNames.AngleOther, Dimension.Angle);
			TestFindUnitsProduces(Dimension.Angle, UnitNames.AngleSI.Concat(UnitNames.AngleOther));
		}


		[Test]
		public void TestExpectedAreaUnits()
		{
			BasicUnitTests(Parser, UnitNames.AreaSI, UnitNames.AreaOther, Dimension.Area);
			TestFindUnitsProduces(Dimension.Area, UnitNames.AreaSI.Concat(UnitNames.AreaOther));
		}


		[Test]
		public void TestExpectedDataUnits()
		{
			TestAllUnitsExist(Parser, UnitNames.Data);
			TestAllUnitsHaveDimension(Parser, UnitNames.Data, Dimension.Data);
			TestFindUnitsProduces(Dimension.Data, UnitNames.Data);
		}


		[Test]
		public void TestExpectedDcCurrentUnits()
		{
			TestAllUnitsExist(Parser, UnitNames.GenerateSIPrefixedNames(UnitNames.DcCurrentSI));
			TestAllUnitsHaveDimension(Parser,
									  UnitNames.GenerateSIPrefixedNames(UnitNames.DcCurrentSI),
									  Dimension.DirectCurrent);
			TestFindUnitsProduces(Dimension.DirectCurrent, UnitNames.DcCurrentSI);
			TestFindUnitsProduces(Dimension.DirectCurrent,
								  UnitNames.GenerateSIPrefixedNames(UnitNames.DcCurrentSI),
								  Prefix.All);
		}


		[Test]
		public void TestExpectedEnergyUnits()
		{
			BasicUnitTests(Parser, UnitNames.EnergySI, UnitNames.EnergyOther, Dimension.Energy);
			TestFindUnitsProduces(Dimension.Energy, UnitNames.EnergySI.Concat(UnitNames.EnergyOther));
		}


		[Test]
		public void TestExpectedForceUnits()
		{
			BasicUnitTests(Parser, UnitNames.ForceSI, UnitNames.ForceOther, Dimension.Force);
			TestFindUnitsProduces(Dimension.Force, UnitNames.ForceSI.Concat(UnitNames.ForceOther));
		}


		[Test]
		public void TestExpectedFrequencyUnits()
		{
			TestAllUnitsExist(Parser, UnitNames.GenerateSIPrefixedNames(UnitNames.FrequencySI));
			TestAllUnitsHaveDimension(Parser,
									  UnitNames.GenerateSIPrefixedNames(UnitNames.FrequencySI),
									  Dimension.Frequency);
			TestFindUnitsProduces(Dimension.Frequency, UnitNames.FrequencySI);
			TestFindUnitsProduces(Dimension.Frequency,
								  UnitNames.GenerateSIPrefixedNames(UnitNames.FrequencySI),
								  Prefix.All);
		}


		[Test]
		public void TestExpectedLengthUnits()
		{
			BasicUnitTests(Parser, UnitNames.LengthSI, UnitNames.LengthOther, Dimension.Length);
			TestFindUnitsProduces(Dimension.Length, UnitNames.LengthSI.Concat(UnitNames.LengthOther));
		}


		[Test]
		public void TestExpectedLinearAccelerationUnits()
		{
			BasicUnitTests(Parser,
						   UnitNames.LinearAccelerationSI,
						   UnitNames.LinearAccelerationOther,
						   Dimension.Acceleration);
			TestFindUnitsProduces(Dimension.Acceleration,
								  UnitNames.LinearAccelerationSI.Concat(UnitNames.LinearAccelerationOther));
		}


		[Test]
		public void TestExpectedLinearSpeedUnits()
		{
			BasicUnitTests(Parser, UnitNames.LinearSpeedSI, UnitNames.LinearSpeedOther, Dimension.Velocity);
			TestFindUnitsProduces(Dimension.Velocity, UnitNames.LinearSpeedSI.Concat(UnitNames.LinearSpeedOther));
		}


		[Test]
		public void TestExpectedPowerUnits()
		{
			BasicUnitTests(Parser, UnitNames.PowerSI, UnitNames.PowerOther, Dimension.Power);
			TestFindUnitsProduces(Dimension.Power, UnitNames.PowerSI.Concat(UnitNames.PowerOther));
		}


		[Test]
		public void TestExpectedQuantityUnits()
		{
			TestAllUnitsExist(Parser, UnitNames.Quantity);
			TestAllUnitsHaveDimension(Parser, UnitNames.Quantity, Dimension.Unknown);
			TestFindUnitsProduces(Dimension.Unknown, UnitNames.Quantity);
		}


		[Test]
		public void TestExpectedRatioUnits()
		{
			TestAllUnitsExist(Parser, UnitNames.Ratio);
			TestAllUnitsHaveDimension(Parser, UnitNames.Ratio, Dimension.Ratio);
			TestFindUnitsProduces(Dimension.Ratio, UnitNames.Ratio);
		}


		[Test]
		public void TestExpectedTemperatureUnits()
		{
			BasicUnitTests(Parser, UnitNames.TemperatureSI, UnitNames.TemperatureOther, Dimension.Temperature);
			TestFindUnitsProduces(Dimension.Temperature, UnitNames.TemperatureSI.Concat(UnitNames.TemperatureOther));
		}


		[Test]
		public void TestExpectedTimeUnits()
		{
			BasicUnitTests(Parser, UnitNames.TimeSI, UnitNames.TimeOther, Dimension.Time);
			TestFindUnitsProduces(Dimension.Time, UnitNames.TimeSI.Concat(UnitNames.TimeOther));
		}


		[Test]
		public void TestExpectedTorqueUnits()
		{
			BasicUnitTests(Parser, UnitNames.TorqueSI, UnitNames.TorqueOther, Dimension.Torque);
			TestFindUnitsProduces(Dimension.Torque, UnitNames.TorqueSI.Concat(UnitNames.TorqueOther));
		}


		[Test]
		public void TestExpectedVolumeUnits()
		{
			BasicUnitTests(Parser, UnitNames.VolumeSI, UnitNames.VolumeOther, Dimension.Volume);
			TestFindUnitsProduces(Dimension.Volume, UnitNames.VolumeSI.Concat(UnitNames.VolumeOther));
		}


		[Test]
		public void TestExpectedWeightUnits()
		{
			BasicUnitTests(Parser, UnitNames.WeightSI, UnitNames.WeightOther, Dimension.Weight);
			TestFindUnitsProduces(Dimension.Weight, UnitNames.WeightSI.Concat(UnitNames.WeightOther));
		}


		[Test]
		public void TestInvalidUnitsDoNotExist()
		{
			foreach (var abbrev in UnitNames.Invalid)
			{
				var unit = Parser.TryParseUnit(abbrev);
				Assert.IsNull(unit, "Unit should not exist for abbreviation " + abbrev);
			}
		}


		// TODO (Soleil 2018-02-07) Re-enable this test when we have support for having the same unit symbol in multiple dimensions.
		//[Test]
		public void TestAllDimensionsHaveUnits()
		{
			foreach (var dimension in Dimension.AllDimensions)
			{
				var units = Registry.FindUnits(dimension);
				Assert.Less(0, units.Count(), "Default table has no units for dimension " + dimension.Name);
			}
		}


		private static void BasicUnitTests(IUnitParser aParser, IEnumerable<string> aSIUnitAbbrevs,
										   IEnumerable<string> aOtherUnitAbbrevs, Dimension aDimension)
		{
			TestAllUnitsExist(aParser, UnitNames.GenerateSIPrefixedNames(aSIUnitAbbrevs));
			TestAllUnitsHaveDimension(aParser, UnitNames.GenerateSIPrefixedNames(aSIUnitAbbrevs), aDimension);
			TestAllUnitsExist(aParser, aOtherUnitAbbrevs);
			TestAllUnitsHaveDimension(aParser, aOtherUnitAbbrevs, aDimension);
		}


		private static void TestAllUnitsExist(IUnitParser aParser, IEnumerable<string> aUnitAbbreviations)
		{
			foreach (var abbrev in aUnitAbbreviations)
			{
				var unit = aParser.TryParseUnit(abbrev);
				Assert.IsNotNull(unit, "Failed to find a unit for abbreviation " + abbrev);
			}
		}


		private static void TestAllUnitsHaveDimension(IUnitParser aParser, IEnumerable<string> aUnitAbbreviations,
													  Dimension aDimension)
		{
			foreach (var abbrev in aUnitAbbreviations)
			{
				var unit = aParser.TryParseUnit(abbrev);
				Assert.AreEqual(aDimension,
								unit.Dimension,
								$"Unit {unit.Name ?? unit.Abbreviation} has wrong dimension.");
			}
		}


		private void TestFindUnitsProduces(Dimension aDimension, IEnumerable<string> aExpectedSet,
										   IEnumerable<Prefix> aPrefixes = null)
		{
			var found = new HashSet<string>(Registry.FindUnits(aDimension, aPrefixes).Select(u => u.Abbreviation));
			Assert.IsTrue(found.SetEquals(aExpectedSet));
		}
	}
}

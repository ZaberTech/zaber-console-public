﻿using System.Reflection;
using System.Xml;
using NUnit.Framework;

namespace Zaber.Units.Tests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class TestLoadCustomOverrides
	{
		[SetUp]
		public void Setup()
		{
			// Load the standard unit lib.
			var parser1 = ComposedUnitParser.NewDefaultUnitParser();
			parser1.LoadDefaultUnitLibrary(parser1.GetUnitRegistry());
			var table1 = new UnitConverter(parser1, parser1.GetUnitRegistry());

			// Load legacy Zaber Console unit definitions.
			var reader = XmlReader.Create(Assembly.GetExecutingAssembly()
											   .GetManifestResourceStream(
													   "Zaber.Units.Tests.Resources.CustomOverrides.xml"));
			var legaLib = new XmlUnitLibrary(reader);
			var parser2 = ComposedUnitParser.NewDefaultUnitParser();
			legaLib.LoadTo(parser2.GetUnitRegistry(), parser2, table1);

			// Use the standard unit library as the fallback.
			_chainedConverter = new UnitConverter(parser2, parser2.GetUnitRegistry(), table1);
		}


		[Test]
		public void TestDerivativesOfPrefixOverrides()
		{
			var u = _chainedConverter.FindUnitBySymbol("µStep");
			Assert.IsNotNull(u);
			var s = _chainedConverter.FindUnitBySymbol("s");
			Assert.IsNotNull(s);
			var us = u / s;
			Assert.IsNotNull(us);
			Assert.AreEqual(_chainedConverter.FindUnitBySymbol("µStep/s"), us);
			var uss = us / s;
			Assert.IsNotNull(uss);
			Assert.AreEqual(_chainedConverter.FindUnitBySymbol("µStep/s²"), uss);
		}


		[Test]
		public void TestExponentOverrides()
		{
			var mms2 = _chainedConverter.FindUnitBySymbol("mm/s²");
			Assert.IsNotNull(mms2);
			var ins2a = _chainedConverter.FindUnitBySymbol("in/s^2");
			Assert.IsNotNull(ins2a);

			var conversion = mms2.GetTransformationTo(ins2a);
			Assert.IsNotNull(conversion);
			var result = conversion.Transform(25.4);
			Assert.AreEqual(1.0, result, 0.001);

			var ins2b = _chainedConverter.FindUnitBySymbol("in/s²");
			conversion = ins2a.GetTransformationTo(ins2b);
			Assert.IsNotNull(conversion);
			result = conversion.Transform(1.0);
			Assert.AreEqual(1.0, result, 0.001);
		}


		[Test]
		public void TestPrefixOverrides()
		{
			var us = _chainedConverter.FindUnitBySymbol("µStep");
			Assert.IsNotNull(us);
		}


		private IUnitConverter _chainedConverter;
	}
}

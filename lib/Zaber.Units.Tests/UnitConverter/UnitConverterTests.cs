﻿using System.Reflection;
using System.Xml;
using NUnit.Framework;

namespace Zaber.Units.Tests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class UnitConverterTests
	{
		[SetUp]
		public void Setup()
		{
			var p1 = ComposedUnitParser.NewDefaultUnitParser();
			_parser1 = p1;
			_registry1 = p1.GetUnitRegistry();

			var p2 = ComposedUnitParser.NewDefaultUnitParser();
			_parser2 = p2;
			_registry2 = p2.GetUnitRegistry();

			var reader = XmlReader.Create(Assembly.GetExecutingAssembly()
											   .GetManifestResourceStream(
													   "Zaber.Units.Tests.Resources.StandardUnits.xml"));
			var library = new XmlUnitLibrary(reader);
			library.LoadTo(_registry1, _parser1);

			reader = XmlReader.Create(Assembly.GetExecutingAssembly()
										   .GetManifestResourceStream("Zaber.Units.Tests.Resources.CustomUnits.xml"));
			library = new XmlUnitLibrary(reader);
			library.LoadTo(_registry2, _parser2);

			_converter1 = new UnitConverter(_parser1, _registry1);
			_converter2 = new UnitConverter(_parser2, _registry2);
		}


		[Test]
		public void TestConvertCustomToStandard()
		{
			_converter2.Parent = _converter1;
			var feet = _converter2.Convert(1.0, _converter2.FindUnitBySymbol("qb"), _converter2.FindUnitBySymbol("ft"));
			Assert.AreEqual(1.5, feet, 0.001);
		}


		[Test]
		public void TestConvertStandardToCustom()
		{
			_converter1.Parent = _converter2;
			var cubits =
				_converter1.Convert(1.5, _converter1.FindUnitBySymbol("ft"), _converter1.FindUnitBySymbol("qb"));
			Assert.AreEqual(1.0, cubits, 0.001);
		}


		[Test]
		public void TestCustomToStandard()
		{
			_converter2.Parent = _converter1;
			var customUnit = _converter2.FindUnitBySymbol("qb");
			Assert.IsNotNull(customUnit);

			var standardUnit = _converter2.FindUnitBySymbol("ft");
			Assert.IsNotNull(standardUnit);

			var conv = customUnit.GetTransformationTo(standardUnit);
			var feet = conv.Transform(1.0);
			Assert.AreEqual(1.5, feet, 0.001);
		}


		[Test]
		public void TestDefaultInstanceExists() => Assert.IsNotNull(UnitConverter.Default);


		[Test]
		public void TestStandardToCustom()
		{
			_converter1.Parent = _converter2;
			var customUnit = _converter1.FindUnitBySymbol("qb");
			Assert.IsNotNull(customUnit);

			var standardUnit = _converter1.FindUnitBySymbol("ft");
			Assert.IsNotNull(standardUnit);

			var conv = standardUnit.GetTransformationTo(customUnit);
			var cubits = conv.Transform(1.5);
			Assert.AreEqual(1.0, cubits, 0.001);
		}


		private IUnitParser _parser1;
		private IUnitRegistry _registry1;
		private IUnitConverter _converter1;

		private IUnitParser _parser2;
		private IUnitRegistry _registry2;
		private IUnitConverter _converter2;
	}
}

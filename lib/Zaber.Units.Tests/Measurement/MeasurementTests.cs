﻿using System;
using System.Linq;
using NUnit.Framework;

namespace Zaber.Units.Tests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class MeasurementTest
	{
		[SetUp]
		public void Setup()
		{
			_micrometer = _defConv.FindUnitBySymbol("µm");
			_millimeter = _defConv.FindUnitBySymbol("mm");
			_degree = _defConv.FindUnitBySymbol("°");
			_second = _defConv.FindUnitBySymbol("s");
		}


		[Test]
		public void ConstructMeasurement()
		{
			var decimalMeasurement = new Measurement(1.0m, _micrometer);
			var doubleMeasurement = new Measurement(1.0, _micrometer);
			var intMeasurement = new Measurement(1, _micrometer);
			var measurement1 = new Measurement(10, _millimeter);
			var measurement2 = new Measurement(measurement1, _micrometer);

			Assert.AreEqual(measurement2.Value, measurement1.Value * 1000.0, 0.0001);
			Assert.AreEqual(decimalMeasurement, doubleMeasurement);
			Assert.AreEqual(decimalMeasurement, intMeasurement);
		}


		[Test]
		public void MeasurementArithmetic()
		{
			var measurement1 = new Measurement(0.2, _millimeter);
			var measurement2 = new Measurement(0.3, _millimeter);
			var expectedSum = new Measurement(0.5, _millimeter);
			var expectedDiff = new Measurement(0.1, _millimeter);
			var ratio = 2.0;
			var expectedProduct = new Measurement(0.4, _millimeter);
			var expectedTriple = new Measurement(0.6, _millimeter);
			var expectedNegative = new Measurement(-0.2, _millimeter);
			var expectedQuotient = new Measurement(0.1, _millimeter);


			var sum = measurement1 + measurement2;
			var diff = measurement2 - measurement1;
			var product1 = ratio * measurement1;
			var product2 = measurement1 * ratio;
			var triple = 3.0 * measurement1;
			var negative = -measurement1;
			var quotient = measurement1 / ratio;

			Assert.AreEqual(expectedSum, sum, "Sum");
			Assert.IsTrue(expectedDiff.IsSimilar(diff, 0.0001), "Difference");
			Assert.AreEqual(expectedProduct, product1, "Product1");
			Assert.AreEqual(expectedProduct, product2, "Product2");
			Assert.IsTrue(expectedTriple.IsSimilar(triple, 0.0001), "Triple");
			Assert.AreEqual(expectedNegative, negative, "Negative");
			Assert.AreEqual(expectedQuotient, quotient, "Quotient");
		}


		[Test]
		public void MeasurementToString()
		{
			var measurement = new Measurement(0.2, _millimeter);
			var s = measurement.ToString();
			Assert.AreEqual("0.2 mm", s);
		}


		[Test]
		public void MeasurementToStringNoDimension()
		{
			var measurement = new Measurement(0.2, null);
			var s = measurement.ToString();
			Assert.AreEqual("0.2", s);
		}


		[Test]
		public void MeasurementValueConversion()
		{
			var measurement1 = new Measurement(0.2, _millimeter);
			var result = measurement1.GetValueAs(_micrometer);
			Assert.AreEqual(200.0, result, 0.0001);
		}


		[Test]
		public void MixedMeasurementSum()
		{
			var measurement1 = new Measurement(0.2, _millimeter);
			var measurement2 = new Measurement(300, _micrometer);

			// result takes units of first argument
			var expectedSum = new Measurement(0.5, _millimeter);

			var sum = measurement1 + measurement2;

			Assert.AreEqual(expectedSum.Value, sum.Value, 0.00001);
			Assert.AreEqual(expectedSum.Unit.Abbreviation, sum.Unit.Abbreviation);
			Assert.AreEqual(expectedSum.Unit, sum.Unit);
			Assert.IsTrue(expectedSum.IsSimilar(sum, 0.00001));
		}


		[Test]
		public void IncompatibleMeasurementSum()
		{
			var measurement1 = new Measurement(0.2, _degree);
			var measurement2 = new Measurement(300, _micrometer);

			var ex = Assert.Throws<ArgumentException>(delegate
			{
				#pragma warning disable CS0219 // Unused variable
				var sum = measurement1 + measurement2;
				#pragma warning restore CS0219
			});

			Assert.AreEqual("Units of measure ° and µm cannot be added together.", ex.Message);
		}


		[Test]
		public void KnownMeasurementProduct()
		{
			// Multiple two measurements with the same dimension but different scale.
			var measurement1 = new Measurement(1000, _millimeter);
			var measurement2 = new Measurement(3000000, _micrometer);
			var prod = measurement1 * measurement2;
			Assert.AreEqual(3000000, prod.Value, 1);
			Assert.AreEqual("mm^2", prod.Unit.Abbreviation);

			// The resulting dimension is a derived one and not a member of the converter table.
			// Try to replace it with one from the table and find its dimension.
			prod = prod.ResolveUnit(_defConv);
			Assert.AreEqual(Dimension.Area, prod.Unit.Dimension);

			// The resulting value is large; try to make the number more readable by changing units.
			prod = prod.NormalizeValue(_defConv.FindUnits(prod.Unit.Dimension, Prefix.All));
			Assert.AreEqual(3, prod.Value, 0.0001);
			Assert.AreEqual(_defConv.FindUnitBySymbol("m^2"), prod.Unit);
		}


		[Test]
		public void UnknownMeasurementProduct()
		{
			// Multiple two measurements with different dimensions where the product is not a standard unit.
			var measurement1 = new Measurement(1, _millimeter);
			var measurement2 = new Measurement(3, _degree);
			var prod = measurement1 * measurement2;
			Assert.AreEqual(3, prod.Value, 0.0001);
			Assert.AreEqual("°*mm", prod.Unit.Abbreviation);
		}


		[Test]
		public void KnownMeasurementQuotient()
		{
			// Multiple two measurements with the same dimension but different scale.
			var measurement1 = new Measurement(1000, _millimeter);
			var measurement2 = new Measurement(0.5, _second);
			var prod = measurement1 / measurement2;
			Assert.AreEqual(2000, prod.Value, 0.1);
			Assert.AreEqual("mm/s", prod.Unit.Abbreviation);

			// The resulting dimension is a derived one and not a member of the converter table.
			// Try to replace it with one from the table and find its dimension.
			prod = prod.ResolveUnit(_defConv);
			Assert.AreEqual(Dimension.Velocity, prod.Unit.Dimension);

			// The resulting value is large; try to make the number more readable by changing units. (restrict to metric)
			prod = prod.NormalizeValue(_defConv.FindUnits(prod.Unit.Dimension, Prefix.All).Where(u => u.UseSIPrefixes));
			Assert.AreEqual(2, prod.Value, 0.0001);
			Assert.AreEqual(_defConv.FindUnitBySymbol("m/s"), prod.Unit);
		}


		[Test]
		public void UnknownMeasurementQuotient()
		{
			// Multiple two measurements with different dimensions where the product is not a standard unit.
			var measurement1 = new Measurement(1, _millimeter);
			var measurement2 = new Measurement(3, _degree);
			var prod = measurement1 / measurement2;
			Assert.AreEqual(1.0 / 3.0, prod.Value, 0.0001);
			Assert.AreEqual("mm/°", prod.Unit.Abbreviation);
		}


		[Test]
		public void ParseCanonicalMeasurement()
		{
			TestParse("1 m/s", 1, _defConv.FindUnitBySymbol("m/s"), Dimension.Velocity);
			TestParse("10.3 km^2", 10.3, _defConv.FindUnitBySymbol("km^2"), Dimension.Area);
			TestParse("-1e-06 m", -1.0 / 1000000.0, _defConv.FindUnitBySymbol("m"), Dimension.Length);
		}


		[Test]
		public void ParseMeasurementNoWhitespace()
		{
			TestParse("1m/s", 1, _defConv.FindUnitBySymbol("m/s"), Dimension.Velocity);
			TestParse("10.3km^2", 10.3, _defConv.FindUnitBySymbol("km^2"), Dimension.Area);
			TestParse("-1e-06m", -1.0 / 1000000.0, _defConv.FindUnitBySymbol("m"), Dimension.Length);
		}


		[Test]
		public void ParseMeasurementExtraWhitespace()
		{
			TestParse("1 m / s", 1, _defConv.FindUnitBySymbol("m/s"), Dimension.Velocity);
			TestParse("10.3 km ^ 2", 10.3, _defConv.FindUnitBySymbol("km^2"), Dimension.Area);
			TestParse("-1e-06 m", -1.0 / 1000000.0, _defConv.FindUnitBySymbol("m"), Dimension.Length);
		}


		[Test]
		public void ParseMeasurementLongNames()
		{
			TestParse("1 Meters per Second", 1, _defConv.FindUnitBySymbol("m/s"), Dimension.Velocity);

			// Names are currently not generated for compound units.
			//TestParse("10.3 square kilometers", 10.3, _defConv.FindUnitBySymbol("km^2"), Dimension.Area);
			TestParse("-1e-06 meter", -1.0 / 1000000.0, _defConv.FindUnitBySymbol("m"), Dimension.Length);
		}


		[Test]
		public void ParseMeasurementExtraWords()
		{
			TestParse("1 m/s of speed", 1, _defConv.FindUnitBySymbol("m/s"), Dimension.Velocity);
			TestParse("10.3 km^2 in area", 10.3, _defConv.FindUnitBySymbol("km^2"), Dimension.Area);
		}


		private void TestParse(string aInput, double aExpectedValue, Unit aExpectedUnit, Dimension aExpectedDimension)
		{
			var m1 = Measurement.Parse(aInput, _defConv);
			Assert.IsNotNull(m1);
			Assert.AreEqual(aExpectedValue, m1.Value, (0.0001 * Math.Abs(aExpectedValue)) + 0.0001);
			Assert.AreEqual(aExpectedUnit, m1.Unit);
			Assert.AreEqual(aExpectedDimension, m1.Unit?.Dimension);
		}


		private readonly IUnitConverter _defConv = UnitConverter.Default;
		private Unit _micrometer;
		private Unit _millimeter;
		private Unit _degree;
		private Unit _second;
	}
}

﻿using System.Linq;
using NUnit.Framework;

namespace Zaber.Units.Tests.Transformations
{
	[TestFixture]
	[SetCulture("en-US")]
	public class SequentialQuantityTransformTests
	{
		[SetUp]
		public void Setup()
		{
			_unit1 = new BaseUnit("f", "Foo");
			_unit2 = new BaseUnit("b", "Bar");
			_unit3 = new BaseUnit("z", "Baz");
			_unit4 = new BaseUnit("q", "Quux");
		}


		[Test]
		public void TestChain()
		{
			var t1 = new LinearQuantityTransformation(_unit1, _unit2, 2.0, -5.0);
			var t2 = new LinearQuantityTransformation(_unit2, _unit3, 1.5, 8.0);
			var t3 = new SequentialQuantityTransformation(t1);
			var t4 = new SequentialQuantityTransformation(t2);
			var t = t3.Chain(t4) as SequentialQuantityTransformation;
			Assert.IsNotNull(t);
			Assert.AreEqual(2, t.TransformationSequence.Count());
			Assert.AreEqual(_unit1, t.SourceUnit);
			Assert.AreEqual(_unit3, t.TargetUnit);
			var x = t.Transform(1.0);
			Assert.AreEqual(3.5, x);
			x = t.Transform(1.5);
			Assert.AreEqual(5.0, x);
		}


		[Test]
		public void TestChainCombineLinear()
		{
			var t1 = new LinearQuantityTransformation(_unit1, _unit2, 2.0, -5.0);
			var t2 = new LinearQuantityTransformation(_unit2, _unit3, 1.5, 8.0);
			var t3 = new LinearQuantityTransformation(_unit3, _unit4, 2.0, 1.0);
			var t4 = new SequentialQuantityTransformation(t1, t2);
			var t = t4.Chain(t3) as SequentialQuantityTransformation;
			Assert.IsNotNull(t);
			Assert.AreEqual(2, t.TransformationSequence.Count());
			Assert.AreEqual(_unit1, t.SourceUnit);
			Assert.AreEqual(_unit4, t.TargetUnit);
			var x = t.Transform(1.5);
			Assert.AreEqual(11.0, x);
		}


		[Test]
		public void TestChainCombineReciprocal()
		{
			var t1 = new ReciprocalQuantityTransformation(_unit1, _unit2, 2.0);
			var t2 = new ReciprocalQuantityTransformation(_unit2, _unit3, 4.0);
			var t3 = new ReciprocalQuantityTransformation(_unit3, _unit4, 8.0);
			var t4 = new SequentialQuantityTransformation(t1, t2);
			var t = t4.Chain(t3) as SequentialQuantityTransformation;
			Assert.IsNotNull(t);
			Assert.AreEqual(2, t.TransformationSequence.Count());
			Assert.AreEqual(_unit1, t.SourceUnit);
			Assert.AreEqual(_unit4, t.TargetUnit);
			var x = t.Transform(1.0);
			Assert.AreEqual(4.0, x);
		}


		[Test]
		public void TestConversion()
		{
			var t1 = new LinearQuantityTransformation(_unit1, _unit2, 2.0, -5.0);
			var t2 = new LinearQuantityTransformation(_unit2, _unit3, 3.0, 7.0);
			var t = new SequentialQuantityTransformation(t1, t2);
			Assert.AreEqual(_unit1, t.SourceUnit);
			Assert.AreEqual(_unit3, t.TargetUnit);
			var x = t.Transform(1.0);
			Assert.AreEqual(-2.0, x);
			x = t.Transform(11.0);
			Assert.AreEqual(58.0, x);
		}


		[Test]
		public void TestReverse()
		{
			var t1 = new LinearQuantityTransformation(_unit1, _unit2, 2.0, -5.0);
			var t2 = new LinearQuantityTransformation(_unit2, _unit3, 3.0, 7.0);
			var t3 = new SequentialQuantityTransformation(t1, t2);
			var t = t3.Reverse();
			Assert.AreEqual(_unit3, t.SourceUnit);
			Assert.AreEqual(_unit1, t.TargetUnit);
			var x = t.Transform(-2.0);
			Assert.AreEqual(1.0, x);
			x = t.Transform(58.0);
			Assert.AreEqual(11.0, x);
		}


		private Unit _unit1, _unit2, _unit3, _unit4;
	}
}

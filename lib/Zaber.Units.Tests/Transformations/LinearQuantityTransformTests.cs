﻿using System.Linq;
using NUnit.Framework;

namespace Zaber.Units.Tests.Transformations
{
	[TestFixture]
	[SetCulture("en-US")]
	public class LinearQuantityTransformTests
	{
		[SetUp]
		public void Setup()
		{
			_unit1 = new BaseUnit("f", "Foo");
			_unit2 = new BaseUnit("b", "Bar");
			_unit3 = new BaseUnit("z", "Baz");
		}


		[Test]
		public void TestCannotChainWithReciprocal()
		{
			var t1 = new LinearQuantityTransformation(_unit1, _unit2, 2.0, 1.0);
			var t2 = new ReciprocalQuantityTransformation(_unit2, _unit3, 18.0);
			var t = t1.Chain(t2);
			Assert.IsTrue(t is SequentialQuantityTransformation);
			Assert.AreEqual(_unit1, t.SourceUnit);
			Assert.AreEqual(_unit3, t.TargetUnit);
			Assert.AreEqual(2, (t as SequentialQuantityTransformation).TransformationSequence.Count());
			var x = t.Transform(4.0);
			Assert.AreEqual(2.0, x);
		}


		[Test]
		public void TestChainWithLinear()
		{
			var t1 = new LinearQuantityTransformation(_unit1, _unit2, 2.0, -5.0);
			var t2 = new LinearQuantityTransformation(_unit2, _unit3, 1.5, 8.0);
			var t = t1.Chain(t2);
			Assert.IsTrue(t is LinearQuantityTransformation);
			var x = t.Transform(1.0);
			Assert.AreEqual(3.5, x);
			x = t.Transform(1.5);
			Assert.AreEqual(5.0, x);
			Assert.AreEqual(3.0, (t as LinearQuantityTransformation).Factor);
			Assert.AreEqual(0.5, (t as LinearQuantityTransformation).Offset);
		}


		[Test]
		public void TestChainWithReciprocal()
		{
			var t1 = new LinearQuantityTransformation(_unit1, _unit2, 2.0, 0.0);
			var t2 = new ReciprocalQuantityTransformation(_unit2, _unit3, 4.0);
			var t = t1.Chain(t2);
			Assert.IsTrue(t is ReciprocalQuantityTransformation);
			var x = t.Transform(4.0);
			Assert.AreEqual(0.5, x);
			x = t.Transform(-2.0);
			Assert.AreEqual(-1.0, x);
			Assert.AreEqual(2.0, (t as ReciprocalQuantityTransformation).Numerator);
		}


		[Test]
		public void TestConversion()
		{
			var t = new LinearQuantityTransformation(_unit1, _unit2, 2.0, -5.0);
			var x = t.Transform(1.0);
			Assert.AreEqual(-3.0, x);
			x = t.Transform(2.0);
			Assert.AreEqual(-1.0, x);
		}


		[Test]
		public void TestReverse()
		{
			var t = new LinearQuantityTransformation(_unit1, _unit2, 2.0, -5.0).Reverse();
			var x = t.Transform(-3.0);
			Assert.AreEqual(1.0, x);
			x = t.Transform(-1.0);
			Assert.AreEqual(2.0, x);
		}


		private Unit _unit1, _unit2, _unit3;
	}
}

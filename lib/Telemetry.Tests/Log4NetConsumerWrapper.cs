﻿using System.Collections.Generic;
using Zaber.Telemetry.Consumers;

namespace Zaber.Telemetry.Tests
{
	public class Log4NetConsumerWrapper : Log4NetConsumer
	{
		public List<string> Messages { get; } = new List<string>();


		protected override void Log(string aMessage)
			=> Messages.Add(aMessage);
	}
}

﻿using System;
using System.Collections.Generic;

namespace Zaber.Telemetry.Tests
{
	public class TestConsumer : ILogConsumer
	{
		public List<Tuple<string, string, string>> Events { get; } = new List<Tuple<string, string, string>>();

		public int FlushCount { get; set; }

		#region -- ILogConsumer -- 

		public void LogEvent(string aCategory, string aEventName, string aDetails = null)
			=> Events.Add(new Tuple<string, string, string>(aCategory, aEventName, aDetails));


		public void Flush()
			=> FlushCount += 1;

		#endregion
	}
}

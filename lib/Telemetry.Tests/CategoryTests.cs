﻿using NUnit.Framework;

namespace Zaber.Telemetry.Tests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class CategoryTests
	{
		[SetUp]
		public void Setup()
		{
			_router = new Router();
			_consumer = new TestConsumer();
			_router.Configure(_consumer);
		}


		[Test]
		public void TestConstructor()
		{
			var logger = _router.GetEventLogger("Foo") as Category;
			Assert.IsNotNull(logger);
			Assert.IsTrue(logger is IEventLog);
			Assert.AreEqual(_router, logger.Parent);
			Assert.AreEqual("Foo", logger.CategoryName);
		}


		[Test]
		public void TestLogEvent()
		{
			var logger = _router.GetEventLogger("Foo");
			logger.LogEvent("Bar");
			logger.LogEvent("Bar", "Baz");

			Assert.AreEqual(0, _consumer.FlushCount);
			Assert.AreEqual(2, _consumer.Events.Count);
			foreach (var evt in _consumer.Events)
			{
				Assert.AreEqual("Foo", evt.Item1);
				Assert.AreEqual("Bar", evt.Item2);
			}

			Assert.IsNull(_consumer.Events[0].Item3);
			Assert.AreEqual("Baz", _consumer.Events[1].Item3);
		}


		private Router _router;
		private TestConsumer _consumer;
	}
}

﻿using NUnit.Framework;

namespace Zaber.Telemetry.Tests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class DevNullTests
	{
		[Test]
		public void TestClassIsUsable()
		{
			var dn = new DevNull();
			Assert.IsTrue(dn is ILogConsumer);
			dn.LogEvent("Foo", "Bar");
			dn.LogEvent(null, "Bar", "Baz");
			dn.Flush();
		}
	}
}

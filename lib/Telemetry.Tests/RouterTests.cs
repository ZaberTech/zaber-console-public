﻿using NUnit.Framework;

namespace Zaber.Telemetry.Tests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class RouterTests
	{
		[SetUp]
		public void Setup()
		{
			_router = new Router();
			_consumer = new TestConsumer();
			_router.Configure(_consumer);
		}


		[Test]
		public void TestCategorization()
		{
			var log1 = _router.GetEventLogger("Foo");
			var log2 = _router.GetEventLogger("Bar");
			Assert.AreNotEqual(log1, log2);
			log1.LogEvent("Event 1");
			log2.LogEvent("Event 2", "Detail");
			Assert.AreEqual(2, _consumer.Events.Count);
			Assert.AreEqual("Foo", _consumer.Events[0].Item1);
			Assert.AreEqual("Bar", _consumer.Events[1].Item1);
			Assert.AreEqual("Event 1", _consumer.Events[0].Item2);
			Assert.AreEqual("Event 2", _consumer.Events[1].Item2);
			Assert.IsNull(_consumer.Events[0].Item3);
			Assert.AreEqual("Detail", _consumer.Events[1].Item3);
		}


		[Test]
		public void TestReport()
		{
			_router.Report();
			Assert.AreEqual(1, _consumer.FlushCount);
		}


		private Router _router;
		private TestConsumer _consumer;
	}
}

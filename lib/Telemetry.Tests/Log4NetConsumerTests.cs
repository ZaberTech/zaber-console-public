﻿using System.Text.RegularExpressions;
using NUnit.Framework;

namespace Zaber.Telemetry.Tests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class Log4NetConsumerTests
	{
		[SetUp]
		public void Setup()
		{
			_consumer = new Log4NetConsumerWrapper();
			_router = new Router();
			_router.Configure(_consumer);
		}


		[Test]
		public void TestOutputOnLogEvent()
		{
			Assert.AreEqual(0, _consumer.Messages.Count);
			var log = _router.GetEventLogger("Foo");
			log.LogEvent("Bar", "Baz");
			Assert.AreEqual(1, _consumer.Messages.Count);
			Assert.IsTrue(Regex.Match(_consumer.Messages[0], ".*Foo.*Bar.*Baz.*").Success);
			log.LogEvent("Quux");
			Assert.AreEqual(2, _consumer.Messages.Count);
			Assert.IsTrue(Regex.Match(_consumer.Messages[1], ".*Foo.*Quux.*").Success);
		}


		[Test]
		public void TestReport()
		{
			var log = _router.GetEventLogger("Foo");
			log.LogEvent("Bar", "Baz");
			log.LogEvent("Quux");
			log.LogEvent("Bar");
			_consumer.Messages.Clear();
			_router.Report();
			var output = string.Join("; ", _consumer.Messages);
			Assert.IsTrue(output.Contains("'Foo / Bar' occurred 2 time(s)"));
			Assert.IsTrue(output.Contains("'Foo / Quux' occurred 1 time(s)"));
		}


		[Test]
		public void TestReportWithNulls()
		{
			var log = _router.GetEventLogger((string) null);
			log.LogEvent(null, null);
			_consumer.Messages.Clear();
			_router.Report();
			var output = string.Join("; ", _consumer.Messages);
			Assert.IsTrue(output.Contains("'Global / Unnamed' occurred 1 time(s)"));
		}


		private Router _router;
		private Log4NetConsumerWrapper _consumer;
	}
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using log4net;

namespace Zaber.Telemetry.Consumers
{
	/// <summary>
	///     Basic implementation of an event logger that just counts each type of event
	///     and outputs the events and the summary to log4net.
	/// </summary>
	public class Log4NetConsumer : ILogConsumer
	{
		#region -- ILogConsumer --

		/// <inheritdoc cref="ILogConsumer.LogEvent(string, string, string)" />
		public void LogEvent(string aCategory, string aEventName, string aDetails = null)
		{
			var eventName = string.IsNullOrEmpty(aEventName) ? "Unnamed" : aEventName;

			Log($"Event: {aCategory} / {eventName}{(aDetails is null ? string.Empty : $" ({aDetails})")}");

			lock (_syncRoot)
			{
				if (!_cats.TryGetValue(aCategory, out var counts))
				{
					counts = new Dictionary<string, int>();
					_cats[aCategory] = counts;
				}

				if (counts.TryGetValue(eventName, out var count))
				{
					counts[eventName] = count + 1;
				}
				else
				{
					counts[eventName] = 1;
				}
			}
		}


		/// <inheritdoc cref="ILogConsumer.Flush" />
		public void Flush()
		{
			Dictionary<string, Dictionary<string, int>> cats = null;

			lock (_syncRoot)
			{
				cats = _cats;
				_cats = new Dictionary<string, Dictionary<string, int>>();
			}

			Log("Event counts:");

			foreach (var counts in cats.OrderBy(p => p.Key))
			{
				foreach (var pair in counts.Value.OrderBy(c => c.Key))
				{
					Log($"Event '{counts.Key} / {pair.Key}' occurred {pair.Value} time(s).");
				}
			}
		}

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     This method exists to facilitate testing.
		/// </summary>
		protected virtual void Log(string aMessage) => _log.Info(aMessage);

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private readonly object _syncRoot = new object();
		private Dictionary<string, Dictionary<string, int>> _cats = new Dictionary<string, Dictionary<string, int>>();

		#endregion
	}
}

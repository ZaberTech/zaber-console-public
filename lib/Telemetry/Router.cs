﻿using System;
using System.Collections.Generic;

namespace Zaber.Telemetry
{
	/// <summary>
	///     Helper class for gathering application usage data.
	/// </summary>
	public class Router
	{
		/// <summary>
		///     Configure the logging system. This should normally only be done once
		///     at system startup.
		/// </summary>
		/// <param name="aConsumer">
		///     A configured instance of a log consumer
		///     to use. If null, disables recording of log data.
		/// </param>
		public void Configure(ILogConsumer aConsumer)
		{
			lock (_syncRoot)
			{
				_consumer = aConsumer ?? new DevNull();
			}
		}


		/// <summary>
		///     Synchronously report any remaining log data and generate any
		///     session reports at the end of a session.
		/// </summary>
		public void Report()
		{
			lock (_syncRoot)
			{
				_consumer.Flush();
			}
		}


		/// <summary>
		///     Get an event logger instance that will categorize its events under the
		///     name of a type.
		/// </summary>
		/// <param name="aType">
		///     Type to use as a category. Typically this should
		///     be the type that is requesting the logger, as in log4net.
		/// </param>
		/// <returns>A logger instance.</returns>
		public IEventLog GetEventLogger(Type aType)
			=> GetEventLogger(aType.FullName);


		/// <summary>
		///     Get an event logger instance that will log under a specified
		///     category.
		/// </summary>
		/// <param name="aCategory">
		///     Name of the category to use. If not
		///     specified, will default to "Global".
		/// </param>
		/// <returns>A logger instance.</returns>
		public IEventLog GetEventLogger(string aCategory)
		{
			var category = string.IsNullOrEmpty(aCategory) ? "Global" : aCategory;

			lock (_syncRoot)
			{
				if (!_loggers.TryGetValue(category, out var logger))
				{
					logger = new Category(this, category);
					_loggers[category] = logger;
				}

				return logger;
			}
		}


		/// <summary>
		///     Optional globally accessible instance; enables obtaining
		///     log interfaces in static members as in log4net.
		/// </summary>
		public static Router Instance { get; } = new Router();


		/// <summary>
		///     Route a client log event. This is called by implementations of
		///     <see cref="IEventLog" />. This routing is done here rather than in
		///     those classes so that the consumer of events can be changed after
		///     loggers are obtained (ie dynamically disable logging).
		/// </summary>
		internal void LogEvent(string aCategory, string aEventname, string aDetails)
			=> _consumer.LogEvent(aCategory, aEventname, aDetails);


		private readonly object _syncRoot = new object();
		private ILogConsumer _consumer = new DevNull();
		private readonly Dictionary<string, IEventLog> _loggers = new Dictionary<string, IEventLog>();
	}
}

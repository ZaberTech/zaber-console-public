﻿namespace Zaber.Telemetry
{
	/// <summary>
	///     Dummy implementation of a log consumer for use when
	///     telemetry is disabled.
	/// </summary>
	public class DevNull : ILogConsumer
	{
		/// <inheritdoc cref="ILogConsumer.LogEvent(string, string, string)" />
		public void LogEvent(string aCategory, string aEventName, string aDetails = null)
		{
		}


		/// <inheritdoc cref="ILogConsumer.Flush" />
		public void Flush()
		{
		}
	}
}

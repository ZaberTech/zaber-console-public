﻿namespace Zaber.Telemetry
{
	/// <summary>
	///     Event log interface that keeps track of the assigned category.
	/// </summary>
	public class Category : IEventLog
	{
		/// <summary>
		///     Initialize the event logger with routing information.
		/// </summary>
		/// <param name="aParent">Log dispatcher instance to route through.</param>
		/// <param name="aCategory">Category requested by the client.</param>
		internal Category(Router aParent, string aCategory)
		{
			Parent = aParent;
			CategoryName = aCategory;
		}


		/// <summary>
		///     The category assigned to this log by the user's request for a logger.
		/// </summary>
		public string CategoryName { get; }

		/// <summary>
		///     Reference to the event router that created this object.
		/// </summary>
		public Router Parent { get; }

		#region -- IEventLog --

		/// <inheritdoc cref="IEventLog.LogEvent(string, string)" />
		public void LogEvent(string aEventName, string aDetails = null)
			=> Parent.LogEvent(CategoryName, aEventName, aDetails);

		#endregion
	}
}

﻿namespace Zaber.Telemetry
{
	/// <summary>
	///     Interface for logging events such as user use of application features.
	///     This is for data gathering, not for debugging.
	/// </summary>
	public interface IEventLog
	{
		/// <summary>
		///     Note the occurrence of an event. Records the name and may
		///     also record the time and total count.
		/// </summary>
		/// <param name="aEventName">Name of the event that has occurred.</param>
		/// <param name="aDetails">
		///     Optional details about the context in time of
		///     this particular event. Should not be considered to change the
		///     identity of the event for logging purposes.
		/// </param>
		void LogEvent(string aEventName, string aDetails = null);
	}
}

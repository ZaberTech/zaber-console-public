﻿namespace Zaber.Telemetry
{
	/// <summary>
	///     Interface for log data consumers.
	/// </summary>
	public interface ILogConsumer
	{
		/// <summary>
		///     Record an event.
		/// </summary>
		/// <param name="aCategory">
		///     Event category. May be null or empty for
		///     global scope events.
		/// </param>
		/// <param name="aEventName">Name or typeof the event. Required.</param>
		/// <param name="aDetails">Optional parameters or details about the specific event instance.</param>
		void LogEvent(string aCategory, string aEventName, string aDetails = null);


		/// <summary>
		///     Force synchronous saving of any pending log data, and reset the internal
		///     state of the consumer.
		/// </summary>
		void Flush();
	}
}

﻿using System.Windows.Controls;

namespace Zaber.Units.UI.WPF.Controls
{
	/// <summary>
	///     Interaction logic for MeasurementBox.xaml
	/// </summary>
	public partial class MeasurementBox : UserControl
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Initializes the control.
		/// </summary>
		public MeasurementBox()
		{
			InitializeComponent();
		}

		#endregion
	}
}

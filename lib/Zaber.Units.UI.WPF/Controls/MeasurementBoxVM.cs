﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using ZaberWpfToolbox;

namespace Zaber.Units.UI.WPF.Controls
{
	/// <summary>
	///     ViewModel for a numeric entry box with attached unit of measure selector, and
	///     unit conversion context menu.
	/// </summary>
	public class MeasurementBoxVM : ObservableObject
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Unitialize the measurement control with no legal units of measure.
		/// </summary>
		public MeasurementBoxVM()
		{
		}


		/// <summary>
		///     Initialize the measurement control with units from a given dimension of measurement.
		///     By default, no unit will be selected.
		/// </summary>
		/// <param name="aConverter">The unit converter to get unit definitions from.</param>
		/// <param name="aDimension">The dimension to find units for.</param>
		public MeasurementBoxVM(IUnitConverter aConverter, Dimension aDimension)
		{
			UnitConverter = aConverter;
			PopulateUnits(aDimension);
		}


		/// <summary>
		///     Populate the unit selection box with all known units for a given dimension.
		///     You must set <see cref="UnitConverter" /> to a valid converter before calling this.
		///     This will change the selected unit if the previously selected one is not in the new list;
		///     otherwise it will leave it as-is.
		/// </summary>
		/// <param name="aDimension">The dimension to find units for.</param>
		public void PopulateUnits(Dimension aDimension)
		{
			Units.Clear();

			if (null != UnitConverter)
			{
				foreach (var unit in UnitConverter.FindUnits(aDimension, Prefix.All))
				{
					Units.Add(unit);
				}

				if ((null != SelectedUnit) && !Units.Contains(SelectedUnit))
				{
					var u = UnitConverter.FindUnitBySymbol(SelectedUnit.Abbreviation);
					SelectedUnit = u;
					PopulateConversions();
				}
			}

			OnPropertyChanged(nameof(Units));
		}


		/// <summary>
		///     Populate the unit selection box with a specific list of units, which can be of differing dimensions.
		///     For conversions to work correctly, you should first set <see cref="UnitConverter" /> to a converter
		///     that can find all of the units you're using. This method will replace <see cref="SelectedUnit" /> with
		///     its equivalent from the new list if possible, or to null otherwise.
		/// </summary>
		/// <param name="aUnitList">The new list of units to ovver.</param>
		public void PopulateUnits(IEnumerable<Unit> aUnitList)
		{
			Units.Clear();
			foreach (var unit in aUnitList)
			{
				Units.Add(unit);
			}

			if ((null != SelectedUnit) && !Units.Contains(SelectedUnit))
			{
				var u = UnitConverter?.FindUnitBySymbol(SelectedUnit.Abbreviation);
				SelectedUnit = u;
				PopulateConversions();
			}

			OnPropertyChanged(nameof(Units));
		}


		/// <summary>
		///     Try to convert the current measurement quantity to a new unit.
		/// </summary>
		/// <param name="aUnit">The new unit of measure. This must be present in <see cref="Units" />.</param>
		/// <exception cref="ArgumentException">aUnit was not a valid conversion target or no unit is currently selected.</exception>
		/// <exception cref="InvalidOperationException">The conversion attempt failed.</exception>
		public void ConvertToNewUnit(Unit aUnit)
		{
			if (!Units.Contains(aUnit))
			{
				throw new ArgumentException(
					$"Unit conversion target {aUnit.Abbreviation} is not in the list of applicable units.");
			}

			if (null == SelectedUnit)
			{
				throw new ArgumentException("There is no unit selected to convert from.");
			}

			var v = (double) Quantity;
			if (!SelectedUnit.TryConvertTo(aUnit, ref v))
			{
				throw new InvalidOperationException(
					$"No unit conversion exists from {SelectedUnit.Abbreviation} to {aUnit.Abbreviation}.");
			}

			_quantity = (decimal) v;
			_selectedUnit = aUnit;
			OnPropertyChanged(nameof(Quantity));
			OnPropertyChanged(nameof(SelectedUnit));
			PopulateConversions();
		}


		/// <summary>
		///     Get or set the current quantity and unit together. Get returns a new instance every time.
		///     Set will always look up the unit in this VM's unit converter if possible, to ensure consistency.
		///     It will only use the provided unit instance if the lookup fails. It does not check if the
		///     provided unit is in the list of legal units.
		/// </summary>
		public Measurement Measurement
		{
			get => new Measurement(Quantity, SelectedUnit);
			set
			{
				if (null != value)
				{
					Quantity = (decimal) value.Value;
					var unit = value.Unit;
					unit = UnitConverter?.FindUnitBySymbol(unit.Abbreviation);
					SelectedUnit = unit;
				}
				else
				{
					Quantity = 0m;
				}
			}
		}


		/// <summary>
		///     The numeric value of the text in the text box.
		/// </summary>
		public decimal Quantity
		{
			get => _quantity;
			set
			{
				Set(ref _quantity, value, nameof(Quantity));
				OnPropertyChanged(nameof(Quantity));
			}
		}


		/// <summary>
		///     The unit converter to use for the conversion context menu. All units of measure in <see cref="Units" />
		///     must come from this converter. If null, units can still be selected but conversion cannot.
		/// </summary>
		/// <remarks>
		///     Set this before populating any units of measure; changing it afterwards will not
		///     purge existing units from another converter, possibly resulting in conversion errors.
		/// </remarks>
		public IUnitConverter UnitConverter
		{
			get => _unitConverter;
			set => Set(ref _unitConverter, value, nameof(UnitConverter));
		}


		/// <summary>
		///     Units that are legal to select for this measurement.
		/// </summary>
		public ObservableCollection<Unit> Units
		{
			get => _units;
			set => Set(ref _units, value, nameof(Units));
		}


		/// <summary>
		///     The currently selected unit of measure.
		/// </summary>
		public Unit SelectedUnit
		{
			get => _selectedUnit;
			set
			{
				Set(ref _selectedUnit, value, nameof(SelectedUnit));
				PopulateConversions();
			}
		}


		/// <summary>
		///     Units that are legal to convert to, given the currently selected unit.
		/// </summary>
		public ObservableCollection<Unit> LegalConversions
		{
			get => _legalConversions;
			set => Set(ref _legalConversions, value, nameof(LegalConversions));
		}


		/// <summary>
		///     Command invoked by clicking on a unit in the conversion menu.
		/// </summary>
		public ICommand ConvertCommand
		{
			get
			{
				return _convertCommand
				   ?? (_convertCommand = new RelayCommand(u =>
					   {
						   var unit = u as Unit;
						   if (null != unit)
						   {
							   try
							   {
								   ConvertToNewUnit(unit);
							   }
							   catch (InvalidOperationException)
							   {
							   }
							   catch (ArgumentException)
							   {
							   }
						   }
					   }));
			}
		}

		#endregion

		#region -- Non-Public Methods --

		private void PopulateConversions()
		{
			var newList = new ObservableCollection<Unit>();

			if ((null != SelectedUnit) && (null != UnitConverter))
			{
				// Note this won't find inversions (time<->freq conversions, for example).
				foreach (var unit in Units)
				{
					if ((SelectedUnit.Abbreviation != unit.Abbreviation)
					&& (null != SelectedUnit.GetTransformationTo(unit)))
					{
						newList.Add(unit);
					}
				}
			}

			LegalConversions = newList;
		}

		#endregion

		#region -- Data --

		private decimal _quantity;
		private IUnitConverter _unitConverter;
		private ObservableCollection<Unit> _units = new ObservableCollection<Unit>();
		private Unit _selectedUnit;
		private ObservableCollection<Unit> _legalConversions = new ObservableCollection<Unit>();
		private ICommand _convertCommand;

		#endregion
	}
}

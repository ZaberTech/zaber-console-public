﻿using System.ComponentModel.Composition;
using System.Windows;

namespace Zaber.Units.UI.WPF
{
	/// <summary>
	///     Code partial of the top-level resource dictionary. This only exists in order to
	///     attach the Export attibute that Zaber Console's Plugin Manager will use to locate
	///     this resource when loading the plugin.
	/// </summary>
	[Export("PluginResourceDictionary", typeof(ResourceDictionary))]
	public partial class MainResourceDictionary : ResourceDictionary
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Initializes the control.
		/// </summary>
		public MainResourceDictionary()
		{
			InitializeComponent();
		}

		#endregion
	}
}

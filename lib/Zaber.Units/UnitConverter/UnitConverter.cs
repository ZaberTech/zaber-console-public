﻿using System;
using System.Linq;
using System.Text;

namespace Zaber.Units
{
	/// <summary>
	///     Convenience class that combines a unit registry with a unit parser to
	///     represent a unit conversion table as a single entity.
	/// </summary>
	public class UnitConverter : IUnitConverter
	{
		#region -- Public Methods & Properties --

		#region -- Initialization --

		/// <summary>
		///     Default constructor. Initializes properties.
		/// </summary>
		/// <param name="aParser">Unit symbol parser.</param>
		/// <param name="aRegistry">Unit lookup table.</param>
		/// <param name="aFallback">Optional - fallback converter.</param>
		public UnitConverter(IUnitParser aParser, IUnitRegistry aRegistry, IUnitConverter aFallback = null)
		{
			Parser = aParser ?? throw new ArgumentNullException(nameof(aParser));
			Registry = aRegistry ?? throw new ArgumentNullException(nameof(aRegistry));
			Parent = aFallback;
		}

		#endregion

		#region -- Debugging helpers --

		/// <summary>
		///     Override of ToString() for debugging assistance.
		/// </summary>
		/// <returns>A string containing all the unit abbreviations in the converter's registry.</returns>
		public override string ToString()
		{
			if ((null == Registry) || (Registry.RegisteredUnits.Count == 0))
			{
				return "Empty Unitconverter";
			}

			var sb = new StringBuilder();
			var dict = Registry.RegisteredUnits.Values.GroupBy(u => u.Dimension.Name)
							.ToDictionary(u => u.Key, u => u.ToList());
			foreach (var item in dict.OrderBy(item => item.Key))
			{
				sb.AppendLine(string.Join(",", item.Value.Select(u => u.Abbreviation ?? "<null>")));
			}

			return sb.ToString();
		}

		#endregion

		#region -- Default singleton --

		/// <summary>
		///     Default unit converter instance; contains the unit conversions defined in the Zaber.Units
		///     library and is suitable for use as a fallback unit converter everywhere, without creating
		///     extra instances of the default converter.
		///     This singleton will be automatically created on first read. The default value can be overridden
		///     by setting the value to a user-supplied converter before any attempts to retrieve the value are
		///     made; setting the value twice causes an exception because that would mean inconsistencies within
		///     the same program.
		/// </summary>
		public static IUnitConverter Default
		{
			get
			{
				if (null == _theInstance)
				{
					var p1 = ComposedUnitParser.NewDefaultUnitParser();
					var library = XmlUnitLibrary.NewDefaultLibrary();
					library.LoadTo(p1.GetUnitRegistry(), p1);
					_theInstance = new UnitConverter(p1, p1.GetUnitRegistry());
				}

				return _theInstance;
			}
			set
			{
				if (null != _theInstance)
				{
					throw new InvalidProgramException("Default unit converter instance should not be set twice.");
				}

				_theInstance = value;
			}
		}

		#endregion

		#endregion

		#region -- Data --

		#region -- Fields --

		private static IUnitConverter _theInstance;

		#endregion

		#endregion

		#region -- IUnitConverter implementation --

		/// <summary>
		///     Fallback for lookups that fail at this level. This allows more
		///     specific conversion tables to fall back to more generic tables
		///     for units they don't recognize.
		/// </summary>
		public IUnitConverter Parent { get; set; }


		/// <summary>
		///     Unit string parsing interface.
		/// </summary>
		public IUnitParser Parser { get; }


		/// <summary>
		///     Unit definition lookup interface.
		/// </summary>
		public IUnitRegistry Registry { get; }

		#endregion
	}
}

﻿namespace Zaber.Units
{
	/// <summary>
	///     Interface for classes that combines a unit registry with a unit parser to
	///     represent a unit conversion table as a single entity.
	/// </summary>
	public interface IUnitConverter
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Unit string parsing interface.
		/// </summary>
		IUnitParser Parser { get; }


		/// <summary>
		///     Unit definition lookup interface.
		/// </summary>
		IUnitRegistry Registry { get; }


		/// <summary>
		///     Fallback for lookups that fail at this level. This allows more
		///     specific conversion tables to fall back to more generic tables
		///     for units they don't recognize. This allows the creation of
		///     context-specific unit definitions that override base definitions
		///     of the same name, without having to duplicate the entire basic unit
		///     table. This value can be changed and subsequent queries will
		///     return updated results.
		/// </summary>
		IUnitConverter Parent { get; set; }

		#endregion
	}
}

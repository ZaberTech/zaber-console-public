﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Zaber.Units
{
	/// <summary>
	///     Extension methods for the IUnitConverter interface.
	/// </summary>
	public static class IUnitConverterExtensions
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Find all unit definitions known to this converter chain,
		///     optionally filtering by dimension.
		/// </summary>
		/// <param name="aConverter">this.</param>
		/// <param name="aDimension">Optional. The dimension to find units for.</param>
		/// <param name="aSiPrefixes">
		///     Optional. List of prefixes to apply to units that support
		///     SI prefixes. If you pass in <see cref="Prefix.All" /> it will generate all prefixed versions of
		///     the SI units it returns.
		/// </param>
		/// <returns>Zero or more unit definitions. Ordering is not guaranteed. Uniqueness of abbreviation is guaranteed.</returns>
		public static IEnumerable<Unit> FindUnits(this IUnitConverter aConverter, Dimension aDimension = null,
												  IEnumerable<Prefix> aSiPrefixes = null)
		{
			var found = new Dictionary<string, Unit>();

			var table = aConverter;
			while (null != table)
			{
				foreach (var unit in table.Registry.FindUnits(aDimension, aSiPrefixes))
				{
					if (!found.ContainsKey(unit.Abbreviation))
					{
						found[unit.Abbreviation] = unit;
					}
				}

				table = table.Parent;
			}

			return found.Values;
		}


		/// <summary>
		///     Parses the unit string, using fallback tables as needed.
		/// </summary>
		/// <param name="aConverter">this</param>
		/// <param name="aSymbol">The unit abbreviation to parse.</param>
		/// <returns>The parsed unit or null if unsuccessful.</returns>
		public static Unit FindUnitBySymbol(this IUnitConverter aConverter, string aSymbol)
		{
			var table = aConverter;
			Unit result = null;

			while ((null != table) && (null == result))
			{
				result = table.Parser.TryParseUnit(aSymbol ?? string.Empty);

				// If the unit was parsed but isn't in the lookup table, it must be a derived
				// unit; try to fix up its dimension.
				if ((null != result)
				&& (Dimension.Unknown == result.Dimension)
				&& !table.Registry.RegisteredUnits.ContainsKey(result.Abbreviation))
				{
					var baseUnit = result.TransformationToCoherent?.TargetUnit;
					if (null != baseUnit)
					{
						var fixedupBase = baseUnit;
						if (table.Registry.RegisteredUnits.TryGetValue(baseUnit.Abbreviation, out fixedupBase))
						{
							baseUnit = fixedupBase;
						}

						result.Dimension = baseUnit.Dimension;
					}
				}

				table = table.Parent;
			}

			return result;
		}


		/// <summary>
		///     Convert a value between two units of measure.
		/// </summary>
		/// <param name="aConverter">this</param>
		/// <param name="aValue">The qualtity to convert.</param>
		/// <param name="aFromUnit">The units of the existing quantity.</param>
		/// <param name="aToUnit">The desired units of the converted quantity.</param>
		/// <returns>The converted value.</returns>
		/// <exception cref="InvalidOperationException">No conversion between the two units exists.</exception>
		public static double Convert(this IUnitConverter aConverter, double aValue, Unit aFromUnit, Unit aToUnit)
		{
			var transformation = aFromUnit.GetTransformationTo(aToUnit);
			if (null == transformation)
			{
				throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture,
																  "Cannot convert from {0} to {1}.",
																  aFromUnit,
																  aToUnit));
			}

			return transformation.Transform(aValue);
		}

		#endregion
	}
}

// This is a derivative work of Henning Dieterichs's Units of Measure Library for .NET.
// See the accompanying README for details.

namespace Zaber.Units
{
	/// <summary>
	///     Interface implemented by units that may be non-proportional - that is, units with offsets as well as scale
	///     factors relative to their base units.
	/// </summary>
	public interface ICouldBeUnproportional
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Gets whether this unit is unproportional. <c>true</c>, if a proportional transformation to the coherent unit is not
		///     possible, otherwise <c>false</c>.
		/// </summary>
		bool IsUnproportional { get; }

		/// <summary>
		///     Gets a proportional transformation to the most possible coherent unit.
		///     So k�F (kilo fahrenheit) will be transformed to �F, but not to K (which is the actual coherent unit).
		/// </summary>
		QuantityTransformation ProportionalTransformationToCoherent { get; }

		#endregion
	}
}

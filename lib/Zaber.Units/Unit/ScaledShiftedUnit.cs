// This is a derivative work of Henning Dieterichs's Units of Measure Library for .NET.
// See the accompanying README for details.

using System;

namespace Zaber.Units
{
	/// <summary>
	///     A unit that is derived from another by a scale factor and offset.
	/// </summary>
	public sealed class ScaledShiftedUnit : Unit, ICouldBeUnproportional
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Constructor - initializes the unit.
		/// </summary>
		/// <param name="aAbbrev">Abbreviation for this unit.</param>
		/// <param name="aName">Full name of the unit.</param>
		/// <param name="aBasedOn">The unit this one will be derived from.</param>
		/// <param name="aScale">The scale factor to apply when converting from the base unit.</param>
		/// <param name="aOffset">The offset to add when converting from the base unit.</param>
		public ScaledShiftedUnit(string aAbbrev, string aName, Unit aBasedOn, double aScale, double aOffset)
			: base(aAbbrev, aName)
		{
			if (null == aBasedOn)
			{
				throw new ArgumentNullException("underlyingUnit");
			}

			if ((1.0 == Factor) && (0.0 == Offset))
			{
				throw new ArgumentOutOfRangeException("Factor",
													  "Either factor or offset has to be different from default.");
			}

			UnderlyingUnit = aBasedOn;
			Factor = aScale;
			Offset = aOffset;
			Dimension = aBasedOn.Dimension;
		}


		/// <summary>
		///     Constructor - initializes the unit. Does not set the abbreviation or name.
		/// </summary>
		/// <param name="aBasedOn">The unit this one will be derived from.</param>
		/// <param name="aScale">The scale factor to apply when converting from the base unit.</param>
		/// <param name="aOffset">The offset to add when converting from the base unit.</param>
		public ScaledShiftedUnit(Unit aBasedOn, double aScale, double aOffset)
			: this(null, null, aBasedOn, aScale, aOffset)
		{
		}


		/// <summary>
		///     Constructor - initializes the unit. Sets a scale but no offset.
		/// </summary>
		/// <param name="aAbbrev">Abbreviation for this unit.</param>
		/// <param name="aName">Full name of the unit.</param>
		/// <param name="aBasedOn">The unit this one will be derived from.</param>
		/// <param name="aScale">The scale factor to apply when converting from the base unit.</param>
		public ScaledShiftedUnit(string aAbbrev, string aName, Unit aBasedOn, double aScale)
			: this(aAbbrev, aName, aBasedOn, aScale, 0.0)
		{
		}


		/// <summary>
		///     Constructor - initializes the unit. Sets a scale but no offset. Does not set the abbreviation or name.
		/// </summary>
		/// <param name="aBasedOn">The unit this one will be derived from.</param>
		/// <param name="aScale">The scale factor to apply when converting from the base unit.</param>
		public ScaledShiftedUnit(Unit aBasedOn, double aScale)
			: this(null, null, aBasedOn, aScale, 0.0)
		{
		}


		/// <summary>
		///     Get a unique hash code for this unit.
		/// </summary>
		/// <returns>A hash of the salient properties of this unit.</returns>
		public override int GetHashCode() => Factor.GetHashCode() ^ Offset.GetHashCode() ^ UnderlyingUnit.GetHashCode();


		/// <summary>
		///     The unit this unit is derived from.
		/// </summary>
		public Unit UnderlyingUnit { get; }


		/// <summary>
		///     The scale factor applied when converting from the <see cref="UnderlyingUnit" />.
		/// </summary>
		public double Factor { get; }


		/// <summary>
		///     The offset added when converting from the <see cref="UnderlyingUnit" />.
		/// </summary>
		public double Offset { get; }

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Helper to convert to a string representation.
		/// </summary>
		/// <param name="aFormat">Unused.</param>
		/// <param name="aFormatProvider">Unused.</param>
		/// <returns>A string representation of this unit.</returns>
		protected override string DefinitionToString(string aFormat, IFormatProvider aFormatProvider)
		{
			var factor = "";
			if (1.0 != Factor)
			{
				factor = " * " + Factor;
			}

			var offset = "";
			if (0.0 != Offset)
			{
				offset = " + " + Offset;
			}

			return $"({UnderlyingUnit}){factor}{offset}";
		}


		/// <summary>
		///     Determine if this unit is a coherent unit.
		/// </summary>
		/// <returns>True if coherent.</returns>
		protected override bool GetIsCoherent() => false;


		/// <summary>
		///     Get the transformation from this unit to the corresponding coherent unit.
		/// </summary>
		/// <returns>A new transformation.</returns>
		protected override QuantityTransformation GetTransformationToCoherent()
		{
			var transformationToUnderlyingUnit = new LinearQuantityTransformation(this, UnderlyingUnit, Factor, Offset);
			return transformationToUnderlyingUnit.Chain(UnderlyingUnit.TransformationToCoherent);
		}


		/// <summary>
		///     Determine if two units identify the same measure of the same quantity.
		/// </summary>
		/// <param name="aOther">The other unit to compare to.</param>
		/// <returns>True if the two units have the same salient properties.</returns>
		protected override bool IsSame(Unit aOther)
		{
			var ssu = aOther as ScaledShiftedUnit;
			return (null != ssu)
			   && (ssu.Factor == Factor)
			   && (ssu.Offset == Offset)
			   && (ssu.UnderlyingUnit == UnderlyingUnit);
		}

		#endregion


		#region -- ICouldBeUnproportional implementation --

		/// <summary>
		///     Gets whether this unit is unproportional. <c>true</c>, if a proportional transformation to the coherent unit is not
		///     possible, otherwise <c>false</c>.
		/// </summary>
		public bool IsUnproportional => Offset != 0.0;


		/// <summary>
		///     Gets a proportional transformation to the most possible coherent unit.
		///     So k�F (kilo fahrenheit) will be transformed to �F, but not to K (which is the actual coherent unit).
		/// </summary>
		public QuantityTransformation ProportionalTransformationToCoherent
		{
			get
			{
				//if this is unproportional, this unit is assumed to be coherent 
				//(because a proportional transformation to the underlaying unit is not possible)
				if (IsUnproportional)
				{
					return new LinearQuantityTransformation(this, this, 1, 0);
				}

				if (UnderlyingUnit is ICouldBeUnproportional)
				{
					var transformationToUnderlyingUnit =
						new LinearQuantityTransformation(this, UnderlyingUnit, Factor, Offset);
					return transformationToUnderlyingUnit.Chain(
						((ICouldBeUnproportional) UnderlyingUnit).ProportionalTransformationToCoherent);
				}

				return TransformationToCoherent;
			}
		}

		#endregion
	}
}

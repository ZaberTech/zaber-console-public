// This is a derivative work of Henning Dieterichs's Units of Measure Library for .NET.
// See the accompanying README for details.

using System;
using System.Globalization;
using System.Linq;

namespace Zaber.Units
{
	/// <summary>
	///     Represents the base of all units of measure.
	/// </summary>
	public abstract class Unit : IEquatable<Unit>, IFormattable
	{
		#region -- Public data --

		/// <summary>
		///     The DimensionlessUnit is a special unit which is a neutral element for multiplication and division.
		/// </summary>
		public static readonly Unit Dimensionless = new BaseUnit("1", "Dimensionless")
		{
			UseSIPrefixes = false,
			Dimension = Dimension.None
		};

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Constructor. Initializes appreviation and name.
		/// </summary>
		/// <param name="aAbbrev">Unit symbol.</param>
		/// <param name="aName">Long name.</param>
		public Unit(string aAbbrev, string aName)
		{
			Abbreviation = aAbbrev;
			_name = aName;
		}


		/// <summary>
		///     Checks whether this unit can be converted to <paramref name="aTargetUnit" />.
		/// </summary>
		/// <param name="aTargetUnit">The target unit.</param>
		/// <returns><c>true</c>, if they can be converted, otherwise <c>false</c>.</returns>
		public bool IsUnit(Unit aTargetUnit) => GetTransformationTo(aTargetUnit) != null;


		/// <summary>
		///     Converts <paramref name="aValue" /> from this unit to <paramref name="aTargetUnit" />.
		/// </summary>
		/// <param name="aTargetUnit">The target unit.</param>
		/// <param name="aValue">The value to convert.</param>
		/// <returns><c>true</c>, if successful, otherwise <c>false</c></returns>
		public bool TryConvertTo(Unit aTargetUnit, ref double aValue)
		{
			var transformation = GetTransformationTo(aTargetUnit);
			if (null != transformation)
			{
				aValue = transformation.Transform(aValue);
				return true;
			}

			return false;
		}


		/// <summary>
		///     Gets a transformation from this unit to <paramref name="aTargetUnit" />
		/// </summary>
		/// <param name="aTargetUnit">The target unit.</param>
		/// <returns>A transformation to convert quantities from this unit to <paramref name="aTargetUnit" /></returns>
		public virtual QuantityTransformation GetTransformationTo(Unit aTargetUnit)
		{
			var thisToCoherent = GetTransformationToCoherent();
			var targetToCoherent = aTargetUnit.GetTransformationToCoherent();

			var coherentToTarget = targetToCoherent.Reverse();

			return thisToCoherent.Chain(coherentToTarget);
		}


		/// <summary>
		///     Performs a power operation.
		/// </summary>
		/// <example>dm.Pow(3) is equal to liter.</example>
		/// <param name="aExponent">the exponent.</param>
		/// <returns>The new unit.</returns>
		public Unit Pow(int aExponent)
		{
			var parts = new UnitPartCollection { { this, aExponent } };

			return DerivedUnit.GetUnitFromParts(parts.ToArray());
		}


		/// <summary>
		///     Multiply two units.
		/// </summary>
		/// <param name="u1">LHS of the multiplication.</param>
		/// <param name="u2">RHS of the multiplication.</param>
		/// <returns>A new unit that is the product of the two.</returns>
		public static Unit operator *(Unit u1, Unit u2)
		{
			if (null == u1)
			{
				throw new ArgumentNullException(nameof(u1));
			}

			if (null == u2)
			{
				throw new ArgumentNullException(nameof(u2));
			}

			var parts = new UnitPartCollection
			{
				{ u1, 1 },
				{ u2, 1 }
			};

			return DerivedUnit.GetUnitFromParts(parts.ToArray());
		}


		/// <summary>
		///     Divide one unit by another.
		/// </summary>
		/// <param name="u1">Numerator of the division.</param>
		/// <param name="u2">Denominator of the division.</param>
		/// <returns>A new derived unit.</returns>
		public static Unit operator /(Unit u1, Unit u2)
		{
			if (null == u1)
			{
				throw new ArgumentNullException(nameof(u1));
			}

			if (null == u2)
			{
				throw new ArgumentNullException(nameof(u2));
			}

			var parts = new UnitPartCollection
			{
				{ u1, 1 },
				{ u2, -1 }
			};

			return DerivedUnit.GetUnitFromParts(parts.ToArray());
		}


		/// <summary>
		///     Format this unit as a string.
		/// </summary>
		/// <returns>String representation of the unit.</returns>
		public sealed override string ToString() => ToString(null, CultureInfo.CurrentCulture);


		/// <summary>
		///     Format this unit as a string.
		/// </summary>
		/// <param name="aFormat">
		///     Format string containing a combination of these flags:
		///     E - enclose denominator in brackets.
		///     D - use a denominator.
		///     I - ignore exponent if equal to 1.
		///     L - use long form.
		///     F - use definition.
		/// </param>
		/// <param name="aFormatProvider">Culture info to inform the formatting.</param>
		/// <returns>String representation of the unit.</returns>
		public string ToString(string aFormat, IFormatProvider aFormatProvider)
		{
			if (string.IsNullOrEmpty(aFormat))
			{
				aFormat = "eDIlF";
			}

			if (null == aFormatProvider)
			{
				aFormatProvider = CultureInfo.CurrentCulture;
			}

			string result = null;

			if (aFormat.Contains('L'))
			{
				result = GetName(aFormatProvider);
			}

			if (null == result)
			{
				result = Abbreviation;
			}

			if ((null == result) && !aFormat.Contains("f"))
			{
				result = DefinitionToString(aFormat, aFormatProvider);
			}

			return result;
		}


		/// <summary>
		///     The abbreviation of this unit.
		/// </summary>
		public string Abbreviation { get; internal set; }


		/// <summary>
		///     The localized name of this unit.
		/// </summary>
		public string Name => GetName(CultureInfo.CurrentCulture);


		/// <summary>
		///     Whether or not this unit can have the SI multiplier prefixes in front of it. Inherited from
		///     based units. Can be affected by calculations.
		/// </summary>
		public bool UseSIPrefixes { get; set; } = true;


		/// <summary>
		///     Dimension of this unit. Inherited from base units. Can be affected by calculations.
		/// </summary>
		public Dimension Dimension { get; set; } = Dimension.Unknown;

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Get the long name of the unit.
		/// </summary>
		/// <param name="aFormatProvider">Unused.</param>
		/// <returns>The long name of the unit.</returns>
		protected virtual string GetName(IFormatProvider aFormatProvider) => _name;


		/// <summary>
		///     Get the unit definition as a string.
		/// </summary>
		/// <param name="aFormat">Unused.</param>
		/// <param name="aFormatProvider">Unused.</param>
		/// <returns>Null.</returns>
		protected virtual string DefinitionToString(string aFormat, IFormatProvider aFormatProvider) => null;

		#endregion

		#region -- Data --

		private readonly object _syncRoot = new object();
		private readonly string _name;
		private bool? _isCoherent;
		private QuantityTransformation _transformationToCoherent;

		#endregion

		#region -- Coherence --

		/// <summary>
		///     Invalidate the unit cache.
		/// </summary>
		protected void RenderCacheInvalid()
		{
			lock (_syncRoot)
			{
				_isCoherent = null;
				_transformationToCoherent = null;
			}
		}


		/// <summary>
		///     Determine whether this unit is coherent.
		/// </summary>
		/// <returns>True if coherent.</returns>
		protected abstract bool GetIsCoherent();


		/// <summary>
		///     Gets whether this unit is coherent (see http://en.wikipedia.org/wiki/Metric_system#Coherence).
		/// </summary>
		public bool IsCoherent
		{
			get
			{
				lock (_syncRoot)
				{
					if (!_isCoherent.HasValue)
					{
						_isCoherent = GetIsCoherent();
					}

					return _isCoherent.Value;
				}
			}
		}


		/// <summary>
		///     Get the transformation from this unit to the corresponding coherent unit.
		/// </summary>
		/// <returns>A new transformation.</returns>
		protected abstract QuantityTransformation GetTransformationToCoherent();


		/// <summary>
		///     Gets a transformation which converts a value in this unit to the coherent unit.
		///     For example, the unit "kilometer" will return a transformation to "meter".
		/// </summary>
		/// <remarks>
		///     Because some units (�F) are not proportional to their base unit, derived units assume these nonproportional units
		///     as coherent.
		///     So �F will return a transformation to K, but km/�F will return a transformation only to m/�F.
		///     This is, since the transformation from �F to K can be expressed with K(�F) = a*�F+b and from km to m with m(km) =
		///     c*km.
		///     So km/�F to m/K would be m/K(km/�F) = m(km)/K(�F) = c*km / (a*�F+b).
		///     Only if b = 0, this expression can be formed to: m/K(km/�F) = c/a * km/�F. Because km/�F is a single value (e.g. 5
		///     km/�F), only in this case
		///     this value can be converted to m/K: m/K(5) = c/a * 5. In all other cases, this is not possible, as 5 km/1�F is
		///     different from 10 km/2�F.
		/// </remarks>
		public QuantityTransformation TransformationToCoherent
		{
			get
			{
				lock (_syncRoot)
				{
					return _transformationToCoherent ?? (_transformationToCoherent = GetTransformationToCoherent());
				}
			}
		}

		#endregion

		#region Equality

		/// <summary>
		///     Checks wether this unit is equal to <paramref name="aOther" />.
		/// </summary>
		/// <param name="aOther">The other object.</param>
		/// <returns><c>true</c>, if equal, otherwise <c>false</c>. See <see cref="Equals(Unit)" />.</returns>
		public sealed override bool Equals(object aOther)
		{
			var u = aOther as Unit;
			return (u != null) && Equals(u);
		}


		/// <summary>
		///     Get a unique hash code for this unit.
		/// </summary>
		/// <returns>A hash of the salient properties of this unit.</returns>
		public abstract override int GetHashCode();


		/// <summary>
		///     Checks wether this unit is equal to <paramref name="aOther" />.
		/// </summary>
		/// <param name="aOther">The other unit.</param>
		/// <returns><c>true</c>, if both units have the same coherent unit and their transformation is equal.</returns>
		public bool Equals(Unit aOther)
		{
			if (null == aOther)
			{
				return false;
			}

			if (IsCoherent && aOther.IsCoherent)
			{
				return IsSame(aOther);
			}

			return TransformationToCoherent.Equals(aOther.TransformationToCoherent);
		}


		/// <summary>
		///     Determine if two units identify the same measure of the same quantity.
		/// </summary>
		/// <param name="aOther">The other unit to compare to.</param>
		/// <returns>True if the two units have the same abbreviation.</returns>
		protected abstract bool IsSame(Unit aOther);


		/// <summary>
		///     Test for equality between two units.
		/// </summary>
		/// <param name="u1">LHS of the comparison.</param>
		/// <param name="u2">RHS of the comparison.</param>
		/// <returns>True if equivalent.</returns>
		public static bool Equals(Unit u1, Unit u2)
		{
			if (object.Equals(u1, null) || object.Equals(u2, null))
			{
				return object.Equals(u1, u2);
			}

			return u1.Equals(u2);
		}


		/// <summary>
		///     Test for equality between two units.
		/// </summary>
		/// <param name="u1">LHS of the comparison.</param>
		/// <param name="u2">RHS of the comparison.</param>
		/// <returns>True if equivalent.</returns>
		public static bool operator ==(Unit u1, Unit u2) => Equals(u1, u2);


		/// <summary>
		///     Test for inequality between two units.
		/// </summary>
		/// <param name="u1">LHS of the comparison.</param>
		/// <param name="u2">RHS of the comparison.</param>
		/// <returns>True if not equivalent.</returns>
		public static bool operator !=(Unit u1, Unit u2) => !Equals(u1, u2);

		#endregion
	}
}

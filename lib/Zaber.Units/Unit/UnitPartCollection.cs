﻿// This is a derivative work of Henning Dieterichs's Units of Measure Library for .NET.
// See the accompanying README for details.

using System;
using System.Collections;
using System.Collections.Generic;

namespace Zaber.Units
{
	internal sealed class UnitPartCollection : ICollection<UnitPart>
	{
		#region -- Public Methods & Properties --

		public UnitPartCollection(IEnumerable<UnitPart> aParts)
		{
			foreach (var part in aParts)
			{
				Add(part);
			}
		}


		public UnitPartCollection()
		{
		}


		public void Add(Unit aUnit, int aExponent)
		{
			var du = aUnit as DerivedUnit;
			if (null != du)
			{
				foreach (var part in du.Parts)
				{
					Add(new UnitPart(part.Unit, part.Exponent * aExponent));
				}
			}
			else
			{
				Add(new UnitPart(aUnit, aExponent));
			}
		}


		public void Add(UnitPart aItem)
		{
			if (null == aItem)
			{
				throw new ArgumentNullException("item");
			}

			if (aItem.Unit.Equals(Unit.Dimensionless))
			{
				return;
			}

			if (!_parts.TryGetValue(aItem.Unit, out var old))
			{
				_parts.Add(aItem.Unit, aItem);
			}
			else
			{
				_parts.Remove(old.Unit);
				var exponent = old.Exponent + aItem.Exponent;
				if (exponent != 0)
				{
					_parts.Add(old.Unit, new UnitPart(old.Unit, exponent));
				}
			}
		}


		public void Clear() => _parts.Clear();


		public bool Contains(UnitPart aItem) => _parts.ContainsKey(aItem.Unit);


		public void CopyTo(UnitPart[] aParts, int aIndex) => _parts.Values.CopyTo(aParts, aIndex);


		public bool Remove(Unit aUnit, int aExponent)
		{
			Add(aUnit, -aExponent);

			return true;
		}


		public bool Remove(UnitPart aItem)
		{
			Add(new UnitPart(aItem.Unit, -aItem.Exponent));

			return true;
		}


		public IEnumerator<UnitPart> GetEnumerator() => _parts.Values.GetEnumerator();


		public int Count => _parts.Count;


		public bool IsReadOnly => false;

		#endregion

		#region -- Interfaces --

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

		#endregion

		#region -- Data --

		private readonly Dictionary<Unit, UnitPart> _parts = new Dictionary<Unit, UnitPart>();

		#endregion
	}
}

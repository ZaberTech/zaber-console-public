﻿// This is a derivative work of Henning Dieterichs's Units of Measure Library for .NET.
// See the accompanying README for details.

using System;
using System.Collections.Generic;
using System.Linq;

namespace Zaber.Units
{
	/// <summary>
	///     Represents a mathematical prefix like "kilo" or "Mega".
	/// </summary>
	public sealed class Prefix
	{
		#region -- Data --

		#region -- Private fields --

		private static readonly Dictionary<string, Prefix> _prefixes;

		#endregion

		#endregion

		#region -- Public constants --

		//taken from http://en.wikipedia.org/wiki/Metric_prefix

		/// <summary>
		///     Predefined SI prefix.
		/// </summary>
		public static readonly Prefix Yotta;

		/// <summary>
		///     Predefined SI prefix.
		/// </summary>
		public static readonly Prefix Zetta;

		/// <summary>
		///     Predefined SI prefix.
		/// </summary>
		public static readonly Prefix Exa;

		/// <summary>
		///     Predefined SI prefix.
		/// </summary>
		public static readonly Prefix Peta;

		/// <summary>
		///     Predefined SI prefix.
		/// </summary>
		public static readonly Prefix Tera;

		/// <summary>
		///     Predefined SI prefix.
		/// </summary>
		public static readonly Prefix Giga;

		/// <summary>
		///     Predefined SI prefix.
		/// </summary>
		public static readonly Prefix Mega;

		/// <summary>
		///     Predefined SI prefix.
		/// </summary>
		public static readonly Prefix Kilo;

		/// <summary>
		///     Predefined SI prefix.
		/// </summary>
		public static readonly Prefix Hecto;

		/// <summary>
		///     Predefined SI prefix.
		/// </summary>
		public static readonly Prefix Deca;

		/// <summary>
		///     Lack of an AI prefix. This previx has no name,
		///     no symbol, and a scale factor of one.
		/// </summary>
		public static readonly Prefix None;

		/// <summary>
		///     Predefined SI prefix.
		/// </summary>
		public static readonly Prefix Deci;

		/// <summary>
		///     Predefined SI prefix.
		/// </summary>
		public static readonly Prefix Centi;

		/// <summary>
		///     Predefined SI prefix.
		/// </summary>
		public static readonly Prefix Milli;

		/// <summary>
		///     Predefined SI prefix.
		/// </summary>
		public static readonly Prefix Micro;

		/// <summary>
		///     Predefined SI prefix.
		/// </summary>
		public static readonly Prefix Nano;

		/// <summary>
		///     Predefined SI prefix.
		/// </summary>
		public static readonly Prefix Pico;

		/// <summary>
		///     Predefined SI prefix.
		/// </summary>
		public static readonly Prefix Femto;

		/// <summary>
		///     Predefined SI prefix.
		/// </summary>
		public static readonly Prefix Atto;

		/// <summary>
		///     Predefined SI prefix.
		/// </summary>
		public static readonly Prefix Zepto;

		/// <summary>
		///     Predefined SI prefix.
		/// </summary>
		public static readonly Prefix Yocto;

		#endregion

		#region -- Public properties --

		/// <summary>
		///     Gets the abbreviation of the prefix.
		/// </summary>
		public string Abbreviation { get; }


		/// <summary>
		///     Gets the full name of the prefix.
		/// </summary>
		public string Name { get; }


		/// <summary>
		///     Gets the factor of the prefix, e.g. 1000 for "kilo".
		/// </summary>
		public double Factor { get; }


		/// <summary>
		///     Gets the base-10 exponent of the prefix, ie 3 for Kilo.
		/// </summary>
		public double Exponent { get; }


		/// <summary>
		///     Gets all SI prefixes (http://en.wikipedia.org/wiki/Metric_prefix).
		/// </summary>
		public static IEnumerable<Prefix> All => _prefixes.Values;


		/// <summary>
		///     Gets SI prefixes that correspond to multiple of 1000.
		/// </summary>
		public static IEnumerable<Prefix> PowersOfThousand
			=> _prefixes.Values.Where(p => ((int) Math.Round(p.Exponent) % 3) == 0);

		#endregion

		#region -- Public methods --

		/// <summary>
		///     Convert a prefix to its scale factor.
		/// </summary>
		/// <param name="aPrefix">The prefix to convert.</param>
		/// <returns>The scale factor of the given prefix.</returns>
		public static implicit operator double(Prefix aPrefix) => aPrefix.Factor;


		/// <summary>
		///     Convert to a human-readable string representation.
		/// </summary>
		/// <returns>A formatted string representation of this prefix.</returns>
		public override string ToString() => $"{Name} ({Abbreviation})";


		/// <summary>
		///     Gets a prefix by its abbreviation.
		/// </summary>
		/// <param name="aAbbrev">The abbreviation of the prefix.</param>
		/// <returns>The associated prefix, if no prefix was found, <c>null</c>.</returns>
		public static Prefix Get(string aAbbrev)
		{
			if (_prefixes.TryGetValue(aAbbrev, out var p))
			{
				return p;
			}

			return null;
		}

		#endregion

		#region -- Private methods --

		static Prefix()
		{
			_prefixes = new Dictionary<string, Prefix>();
			Yotta = FromPower("Y", "Yotta", 24);
			Zetta = FromPower("Z", "Zetta", 21);
			Exa = FromPower("E", "Exa", 18);
			Peta = FromPower("P", "Peta", 15);
			Tera = FromPower("T", "Tera", 12);
			Giga = FromPower("G", "Giga", 9);
			Mega = FromPower("M", "Mega", 6);
			Kilo = FromPower("k", "Kilo", 3);
			Hecto = FromPower("h", "Hecto", 2);
			Deca = FromPower("da", "Deca", 1);
			None = FromPower(string.Empty, string.Empty, 0);
			Deci = FromPower("d", "Deci", -1);
			Centi = FromPower("c", "Centi", -2);
			Milli = FromPower("m", "Milli", -3);
			Micro = FromPower("µ", "Micro", -6);
			Nano = FromPower("n", "Nano", -9);
			Pico = FromPower("p", "Pico", -12);
			Femto = FromPower("f", "Femto", -15);
			Atto = FromPower("a", "Atto", -18);
			Zepto = FromPower("z", "Zepto", -21);
			Yocto = FromPower("y", "Yocto", -24);
		}


		private Prefix(string aAbbrev, string aName, double aExponent)
		{
			Abbreviation = aAbbrev;
			Name = aName;
			Factor = Math.Pow(10, aExponent);
			Exponent = aExponent;

			_prefixes.Add(aAbbrev, this);
		}


		private static Prefix FromPower(string aAbbrev, string aName, int aTenthPower)
			=> new Prefix(aAbbrev, aName, aTenthPower);

		#endregion
	}
}

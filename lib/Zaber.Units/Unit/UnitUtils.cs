﻿using System;
using System.Collections.Generic;

namespace Zaber.Units
{
	/// <summary>
	///     Miscellaneous helper methods for units and converters.
	/// </summary>
	public static class UnitUtils
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Given a collection of units, discard those whose scale factors relative to
		///     their coherent unit or a given reference unit fall outside the given range of
		///     powers of 10. Note that it is assumed any constant offset in the unit
		///		conversion will have a relatively small effect on the exponent.
		/// </summary>
		/// <param name="aUnitList">Collection to filter.</param>
		/// <param name="aMinExponent">
		///     The smallest conversion scale factor base 10 log to allow, for example -6 to allow micro-
		///     but not nano-.
		/// </param>
		/// <param name="aMaxExponent">
		///     The largest conversion scale factor base 10 log to allow, for example 6 to allow mega- but
		///     not giga-.
		/// </param>
		/// <param name="aRelativeTo">
		///     Optional unit to measure scale factor relative to;
		///     each unit's coherent unit is used by default. If this is given, it is assumed
		///     that all units in the collection can be converted to this unit.
		/// </param>
		/// <returns>An enumerable with possibly some units omitted.</returns>
		public static IEnumerable<Unit> DiscardExtremeUnits(IEnumerable<Unit> aUnitList,
															double aMinExponent,
															double aMaxExponent,
															Unit aRelativeTo = null)
		{
			foreach (var unit in aUnitList)
			{
				var scale = 1.0;
				if (aRelativeTo is null)
				{
					scale = unit.TransformationToCoherent.Transform(1.0);
				}
				else
				{
					var conversion = unit.GetTransformationTo(aRelativeTo);
					scale = conversion.Transform(1.0);
				}

				var exponent = Math.Log10(scale);

				if ((exponent >= aMinExponent) && (exponent <= aMaxExponent))
				{
					yield return unit;
				}
			}
		}

		#endregion
	}
}

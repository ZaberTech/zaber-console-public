// This is a derivative work of Henning Dieterichs's Units of Measure Library for .NET.
// See the accompanying README for details.

using System;

namespace Zaber.Units
{
	/// <summary>
	///     Describes an inverse transformation f(value) = Numerator / value.
	/// </summary>
	public class ReciprocalQuantityTransformation : QuantityTransformation
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Constructor - initializes relevant values.
		/// </summary>
		/// <param name="aSourceUnit">Source unit for the transformation.</param>
		/// <param name="aTargetUnit">Target unit for the transformation.</param>
		/// <param name="aNumerator">Numerator for the reciproval transformation.</param>
		public ReciprocalQuantityTransformation(Unit aSourceUnit, Unit aTargetUnit, double aNumerator)
			: base(aSourceUnit, aTargetUnit)
		{
			Numerator = aNumerator;
		}


		/// <summary>
		///     Transforms a value from source unit to target unit.
		/// </summary>
		/// <param name="aValue">The value to transform in the source unit.</param>
		/// <returns>The transformed value in the target unit.</returns>
		/// <remarks>
		///     Use this override when having a large range of exponents is
		///     more important than having lots of significant digits of precision.
		/// </remarks>
		public override double Transform(double aValue)
		{
			try
			{
				return Numerator / aValue;
			}
			catch (DivideByZeroException)
			{
				if (HandleDivideByZero)
				{
					return 0.0;
				}

				throw;
			}
		}


		/// <summary>
		///     Transforms a value from source unit to target unit.
		/// </summary>
		/// <param name="aValue">The value to transform in the source unit.</param>
		/// <returns>The transformed value in the target unit.</returns>
		/// <remarks>
		///     Use this override when having a lots of significant digits of precision
		///     is more important than having a large range of exponents. Note that
		///     the precision of the conversion factors is limited to about 15
		///     significant digits.
		/// </remarks>
		public override decimal Transform(decimal aValue)
		{
			try
			{
				return (decimal) Numerator / aValue;
			}
			catch (DivideByZeroException)
			{
				if (HandleDivideByZero)
				{
					return 0m;
				}

				throw;
			}
		}


		/// <summary>
		///     Combine with another transformation.
		/// </summary>
		/// <param name="aTransform">The other transform to compose this one with.</param>
		/// <returns>A new transform, or null if the two are incompatible.</returns>
		public override QuantityTransformation Chain(QuantityTransformation aTransform)
		{
			if (TargetUnit == aTransform.SourceUnit)
			{
				if (aTransform is ReciprocalQuantityTransformation rqt)
				{
					return new LinearQuantityTransformation(SourceUnit, rqt.TargetUnit, rqt.Numerator / Numerator, 0.0);
				}

				if (aTransform is LinearQuantityTransformation lqt && (lqt.Offset == 0.0))
				{
					return new ReciprocalQuantityTransformation(SourceUnit, lqt.TargetUnit, Numerator * lqt.Factor)
					{
						HandleDivideByZero = HandleDivideByZero
					};
				}

				return new SequentialQuantityTransformation(this, aTransform);
			}

			return null;
		}


		/// <summary>
		///     Generate the inverse of the current transform.
		/// </summary>
		/// <returns>A new transform that reverses the current one.</returns>
		public override QuantityTransformation Reverse()
			=> new ReciprocalQuantityTransformation(TargetUnit, SourceUnit, Numerator)
			{
				HandleDivideByZero = HandleDivideByZero
			};


		/// <summary>
		///     Compare transforms for equality.
		/// </summary>
		/// <param name="aOther">The transform to compare to.</param>
		/// <returns>True if the two transforms are equivalent, or false otherwise.</returns>
		public override bool Equals(QuantityTransformation aOther)
			=> aOther is ReciprocalQuantityTransformation rqt
		   && (Numerator == rqt.Numerator)
		   && (HandleDivideByZero == rqt.HandleDivideByZero)
		   && base.Equals(aOther);


		/// <summary>
		///     Get a unique hash code for this object.
		/// </summary>
		/// <returns>A hash of the identifying factors for this transform.</returns>
		public override int GetHashCode()
			=> base.GetHashCode() ^ Numerator.GetHashCode() ^ HandleDivideByZero.GetHashCode();


		/// <summary>
		///     Represent as a string.
		/// </summary>
		/// <returns>A human-readable string representation of this transform.</returns>
		public override string ToString() => string.Format("{2} / {0} => {1}", SourceUnit, TargetUnit, Numerator);


		/// <summary>
		///     The factor.
		/// </summary>
		public double Numerator { get; }


		/// <summary>
		///     Set this to true to automatically eat divide by zero exceptions and return zero instead.
		/// </summary>
		public bool HandleDivideByZero { get; set; }

		#endregion
	}
}

// This is a derivative work of Henning Dieterichs's Units of Measure Library for .NET.
// See the accompanying README for details.

namespace Zaber.Units
{
	/// <summary>
	///     Describes a linear transformation f(value) = Factor * value + Offset.
	/// </summary>
	public class LinearQuantityTransformation : QuantityTransformation
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Constructor - initializes relevant values.
		/// </summary>
		/// <param name="aSourceUnit">Source unit for the transformation.</param>
		/// <param name="aTargetUnit">Target unit for the transformation.</param>
		/// <param name="aScale">Scale factor part of the transformation.</param>
		/// <param name="aOffset">Additive offset part of the transformation.</param>
		public LinearQuantityTransformation(Unit aSourceUnit, Unit aTargetUnit, double aScale, double aOffset)
			: base(aSourceUnit, aTargetUnit)
		{
			Factor = aScale;
			Offset = aOffset;
		}


		/// <summary>
		///     Transforms a value from source unit to target unit.
		/// </summary>
		/// <param name="aValue">The value to transform in the source unit.</param>
		/// <returns>The transformed value in the target unit.</returns>
		/// <remarks>
		///     Use this override when having a large range of exponents is
		///     more important than having lots of significant digits of precision.
		/// </remarks>
		public override double Transform(double aValue) => (Factor * aValue) + Offset;


		/// <summary>
		///     Transforms a value from source unit to target unit.
		/// </summary>
		/// <param name="aValue">The value to transform in the source unit.</param>
		/// <returns>The transformed value in the target unit.</returns>
		/// <remarks>
		///     Use this override when having a lots of significant digits of precision
		///     is more important than having a large range of exponents. Note that
		///     the precision of the conversion factors is limited to about 15
		///     significant digits.
		/// </remarks>
		public override decimal Transform(decimal aValue) => ((decimal) Factor * aValue) + (decimal) Offset;


		/// <summary>
		///     Combine with another transformation.
		/// </summary>
		/// <param name="aTransform">The other transform to compose this one with.</param>
		/// <returns>A new transform, or null if the two are incompatible.</returns>
		public override QuantityTransformation Chain(QuantityTransformation aTransform)
		{
			if (TargetUnit == aTransform.SourceUnit)
			{
				if (aTransform is LinearQuantityTransformation lqt)
				{
					// this(x) := (o + f*x)
					// lqt(x) := (lqt.o + lqt.f*x)
					// this.Chain(lqt) = lqt(this(x)) = f*lqt.f*x + (lqt.o + lqt.f*o)
					return new LinearQuantityTransformation(SourceUnit,
															lqt.TargetUnit,
															Factor * lqt.Factor,
															lqt.Offset + (lqt.Factor * Offset));
				}

				if (aTransform is ReciprocalQuantityTransformation rqt && (Offset == 0.0))
				{
					return new ReciprocalQuantityTransformation(SourceUnit, rqt.TargetUnit, rqt.Numerator / Factor)
					{
						HandleDivideByZero = rqt.HandleDivideByZero
					};
				}

				return new SequentialQuantityTransformation(this, aTransform);
			}

			return null;
		}


		/// <summary>
		///     Generate the inverse of the current transform.
		/// </summary>
		/// <returns>A new transform that reverses the current one.</returns>
		public override QuantityTransformation Reverse()
			=> new LinearQuantityTransformation(TargetUnit, SourceUnit, 1 / Factor, -Offset / Factor);


		/// <summary>
		///     Compare transforms for equality.
		/// </summary>
		/// <param name="aOther">The transform to compare to.</param>
		/// <returns>True if the two transforms are equivalent, or false otherwise.</returns>
		public override bool Equals(QuantityTransformation aOther)
			=> aOther is LinearQuantityTransformation lqt
		   && (Factor == lqt.Factor)
		   && (Offset == lqt.Offset)
		   && base.Equals(aOther);


		/// <summary>
		///     Get a unique hash code for this object.
		/// </summary>
		/// <returns>A hash of the identifying factors for this transform.</returns>
		public override int GetHashCode() => base.GetHashCode() ^ Factor.GetHashCode() ^ Offset.GetHashCode();


		/// <summary>
		///     Represent as a string.
		/// </summary>
		/// <returns>A human-readable string representation of this transform.</returns>
		public override string ToString()
		{
			//TargetUnit = SourceUnit * Factor + Offset;
			var factor = "";
			if (Factor != 1)
			{
				factor = " * " + Factor;
			}

			var offset = "";
			if (Offset != 0)
			{
				offset = " + " + Offset;
			}

			return string.Format("{0}{2}{3} => {1}", SourceUnit, TargetUnit, factor, offset);
		}


		/// <summary>
		///     The factor.
		/// </summary>
		public double Factor { get; }

		/// <summary>
		///     The offset.
		/// </summary>
		public double Offset { get; }

		#endregion
	}
}

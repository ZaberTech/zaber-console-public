﻿using System;

namespace Zaber.Units
{
	/// <summary>
	///     Provides an arbitrary user-defined unit conversion function.
	/// </summary>
	public class UserQuantityTransformation : QuantityTransformation
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Constructor - initializes relevant values.
		/// </summary>
		/// <param name="aSourceUnit">Source unit for the transformation.</param>
		/// <param name="aTargetUnit">Target unit for the transformation.</param>
		/// <param name="aForward">Forward conversion function.</param>
		/// <param name="aReverse">Reverse conversion function.</param>
		public UserQuantityTransformation(Unit aSourceUnit, Unit aTargetUnit, Func<double, double> aForward,
										  Func<double, double> aReverse)
			: base(aSourceUnit, aTargetUnit)
		{
			_forward = aForward;
			_reverse = aReverse;
		}


		/// <summary>
		///     Transforms a value from source unit to target unit.
		/// </summary>
		/// <param name="aValue">The value to transform in the source unit.</param>
		/// <returns>The transformed value in the target unit.</returns>
		public override double Transform(double aValue)
		{
			if (null == _forward)
			{
				throw new ArgumentNullException("Forward transform not specified.");
			}

			return _forward(aValue);
		}


		/// <summary>
		///     Transforms a value from source unit to target unit.
		/// </summary>
		/// <param name="aValue">The value to transform in the source unit.</param>
		/// <returns>The transformed value in the target unit.</returns>
		public override decimal Transform(decimal aValue) => (decimal) Transform((double) aValue);


		/// <summary>
		///     Combine with another transformation.
		/// </summary>
		/// <param name="aTransform">The other transform to compose this one with.</param>
		/// <returns>A new transform, or null if the two are incompatible.</returns>
		public override QuantityTransformation Chain(QuantityTransformation aTransform)
		{
			if (TargetUnit == aTransform.SourceUnit)
			{
				return new SequentialQuantityTransformation(this, aTransform);
			}

			return null;
		}


		/// <summary>
		///     Generate the inverse of the current transform.
		/// </summary>
		/// <returns>A new transform that reverses the current one.</returns>
		public override QuantityTransformation Reverse()
		{
			if (null == _reverse)
			{
				throw new ArgumentNullException("Reverse transform not specified.");
			}

			return new UserQuantityTransformation(TargetUnit, SourceUnit, _reverse, _forward);
		}


		/// <summary>
		///     Compare transforms for equality.
		/// </summary>
		/// <param name="aOther">The transform to compare to.</param>
		/// <returns>True if the two transforms are equivalent, or false otherwise.</returns>
		public override bool Equals(QuantityTransformation aOther)
			=> aOther is UserQuantityTransformation uqt
		   && (_forward == uqt._forward)
		   && (_reverse == uqt._reverse)
		   && base.Equals(aOther);


		/// <summary>
		///     Get a unique hash code for this object.
		/// </summary>
		/// <returns>A hash of the identifying factors for this transform.</returns>
		public override int GetHashCode() => base.GetHashCode() ^ _forward.GetHashCode() ^ _reverse.GetHashCode();


		/// <summary>
		///     Represent as a string.
		/// </summary>
		/// <returns>A human-readable string representation of this transform.</returns>
		public override string ToString() => $"f({SourceUnit}) => {TargetUnit}";

		#endregion

		#region -- Data --

		private readonly Func<double, double> _forward;
		private readonly Func<double, double> _reverse;

		#endregion
	}
}

// This is a derivative work of Henning Dieterichs's Units of Measure Library for .NET.
// See the accompanying README for details.

using System;

namespace Zaber.Units
{
	/// <summary>
	///     Describes a transformation for quantities. A quantity is a number with an unit.
	/// </summary>
	public abstract class QuantityTransformation : IEquatable<QuantityTransformation>
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Constructor - initializes relevant values.
		/// </summary>
		/// <param name="aSourceUnit">Source unit for the transformation.</param>
		/// <param name="aTargetUnit">Target unit for the transformation.</param>
		public QuantityTransformation(Unit aSourceUnit, Unit aTargetUnit)
		{
			SourceUnit = aSourceUnit ?? throw new ArgumentNullException("sourceUnit");
			TargetUnit = aTargetUnit ?? throw new ArgumentNullException("targetUnit");
		}


		/// <summary>
		///     Transforms a value from source unit to target unit.
		/// </summary>
		/// <param name="aValue">The value to transform in the source unit.</param>
		/// <returns>The transformed value in the target unit.</returns>
		/// <remarks>
		///     Use this override when having a large range of exponents is
		///     more important than having lots of significant digits of precision.
		/// </remarks>
		public abstract double Transform(double aValue);


		/// <summary>
		///     Transforms a value from source unit to target unit.
		/// </summary>
		/// <param name="aValue">The value to transform in the source unit.</param>
		/// <returns>The transformed value in the target unit.</returns>
		/// <remarks>
		///     Use this override when having a lots of significant digits of precision
		///     is more important than having a large range of exponents. Note that
		///     the precision of the conversion factors is limited to about 15
		///     significant digits.
		/// </remarks>
		public abstract decimal Transform(decimal aValue);


		/// <summary>
		///     Chains two transformations. This is only possible if this target unit is equal to
		///     the others source unit.
		/// </summary>
		/// <param name="other">The other transformation.</param>
		/// <returns>The chained transformation.</returns>
		public abstract QuantityTransformation Chain(QuantityTransformation other);


		/// <summary>
		///     Reverses this transformation, so that reversed(this(value)) = value. The source and
		///     target units are exchanged.
		/// </summary>
		/// <returns>The reversed transformation.</returns>
		public abstract QuantityTransformation Reverse();


		/// <summary>
		///     Compare transforms for equality.
		/// </summary>
		/// <param name="aOther">The transform to compare to.</param>
		/// <returns>True if the two transforms are equivalent, or false otherwise.</returns>
		public virtual bool Equals(QuantityTransformation aOther) => TargetUnit.Equals(aOther.TargetUnit);


		/// <summary>
		///     Compare transforms for equality.
		/// </summary>
		/// <param name="aOther">The transform to compare to.</param>
		/// <returns>True if the two transforms are equivalent, or false otherwise.</returns>
		public sealed override bool Equals(object aOther)
		{
			if (aOther is QuantityTransformation qt)
			{
				return Equals(qt);
			}

			return false;
		}


		/// <summary>
		///     Get a unique hash code for this object.
		/// </summary>
		/// <returns>A hash of the identifying factors for this transform.</returns>
		public override int GetHashCode() => SourceUnit.GetHashCode() ^ TargetUnit.GetHashCode();


		/// <summary>
		///     Represent as a string.
		/// </summary>
		/// <returns>A human-readable string representation of this transform.</returns>
		public override string ToString() => $"{SourceUnit:eDIlf} => {TargetUnit:eDIlf}";


		/// <summary>
		///     The source unit.
		/// </summary>
		public Unit SourceUnit { get; }


		/// <summary>
		///     The target unit.
		/// </summary>
		public Unit TargetUnit { get; }

		#endregion
	}
}

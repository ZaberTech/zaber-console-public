// This is a derivative work of Henning Dieterichs's Units of Measure Library for .NET.
// See the accompanying README for details.

using System.Collections.Generic;
using System.Linq;

namespace Zaber.Units
{
	/// <summary>
	///     Describes a sequence of linear transformations that can't easily be combined.
	///     TODO: It might be better to just use an expression tree evaluation at this point.
	/// </summary>
	public class SequentialQuantityTransformation : QuantityTransformation
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Constructor - initializes relevant values.
		/// </summary>
		public SequentialQuantityTransformation(params QuantityTransformation[] aSequence)
			: this(aSequence as IEnumerable<QuantityTransformation>)
		{
		}


		/// <summary>
		///     Constructor - initializes relevant values.
		/// </summary>
		public SequentialQuantityTransformation(IEnumerable<QuantityTransformation> aSequence)
			: base(aSequence.First().SourceUnit, aSequence.Last().TargetUnit)
		{
			TransformationSequence = aSequence.ToList();
		}


		/// <summary>
		///     Transforms a value from source unit to target unit.
		/// </summary>
		/// <param name="aValue">The value to transform in the source unit.</param>
		/// <returns>The transformed value in the target unit.</returns>
		/// <remarks>
		///     Use this override when having a large range of exponents is
		///     more important than having lots of significant digits of precision.
		/// </remarks>
		public override double Transform(double aValue)
		{
			var v = aValue;

			foreach (var t in TransformationSequence)
			{
				v = t.Transform(v);
			}

			return v;
		}


		/// <summary>
		///     Transforms a value from source unit to target unit.
		/// </summary>
		/// <param name="aValue">The value to transform in the source unit.</param>
		/// <returns>The transformed value in the target unit.</returns>
		/// <remarks>
		///     Use this override when having a lots of significant digits of precision
		///     is more important than having a large range of exponents. Note that
		///     the precision of the conversion factors is limited to about 15
		///     significant digits.
		/// </remarks>
		public override decimal Transform(decimal aValue)
		{
			var v = aValue;

			foreach (var t in TransformationSequence)
			{
				v = t.Transform(v);
			}

			return v;
		}


		/// <summary>
		///     Combine with another transformation.
		/// </summary>
		/// <param name="aTransform">The other transform to compose this one with.</param>
		/// <returns>A new transform, or null if the two are incompatible.</returns>
		public override QuantityTransformation Chain(QuantityTransformation aTransform)
		{
			if (TargetUnit == aTransform.SourceUnit)
			{
				var newList = new List<QuantityTransformation>(TransformationSequence);

				if (aTransform is SequentialQuantityTransformation sqt)
				{
					// Just concatenate two sequences if possible.
					// TODO Future improvement: Try to optimize by combining adjacent transforms.
					newList.AddRange(sqt.TransformationSequence);
				}
				else
				{
					// Try to combine the new transform with the last one in the existing sequence.
					var newEnd = newList[newList.Count - 1].Chain(aTransform);
					if ((newEnd != null) && !(newEnd is SequentialQuantityTransformation))
					{
						newList[newList.Count - 1] = newEnd;
					}
					else
					{
						// Not composable - just add it to the end instead.
						newList.Add(aTransform);
					}
				}

				return new SequentialQuantityTransformation(newList);
			}

			return null;
		}


		/// <summary>
		///     Generate the inverse of the current transform.
		/// </summary>
		/// <returns>A new transform that reverses the current one.</returns>
		public override QuantityTransformation Reverse()
		{
			var newList = new List<QuantityTransformation>();

			foreach (var t in TransformationSequence)
			{
				newList.Insert(0, t.Reverse());
			}

			return new SequentialQuantityTransformation(newList);
		}


		/// <summary>
		///     Compare transforms for equality.
		/// </summary>
		/// <param name="aOther">The transform to compare to.</param>
		/// <returns>True if the two transforms are equivalent, or false otherwise.</returns>
		public override bool Equals(QuantityTransformation aOther)
			=> aOther is SequentialQuantityTransformation sqt
		   && base.Equals(aOther)
		   && TransformationSequence.SequenceEqual(sqt.TransformationSequence);


		/// <summary>
		///     Get a unique hash code for this object.
		/// </summary>
		/// <returns>A hash of the identifying factors for this transform.</returns>
		public override int GetHashCode() => base.GetHashCode() ^ TransformationSequence.GetHashCode();


		/// <summary>
		///     Represent as a string.
		/// </summary>
		/// <returns>A human-readable string representation of this transform.</returns>
		public override string ToString()
			=> $"({string.Join(", ", TransformationSequence.Select(t => t.ToString()))})";


		/// <summary>
		///     The ordered sequence of transformations to apply.
		/// </summary>
		public IEnumerable<QuantityTransformation> TransformationSequence { get; }

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using Zaber.Units.Properties;

namespace Zaber.Units
{
	/// <summary>
	///     Helper class for identifying the dimensions of units.
	/// </summary>
	public class Dimension : IEquatable<Dimension>
	{
		#region -- Data --

		#region -- Fields --

		private static readonly Dictionary<string, Dimension> _dimensions = new Dictionary<string, Dimension>();

		#endregion

		#endregion

		#region -- Static API --

		/// <summary>
		///     Static constructor. Initializes the default set of dimensions.
		/// </summary>
		static Dimension()
		{
			None = new Dimension(Resources.STR_DIM_NONE);
			Unknown = new Dimension(Resources.STR_DIM_UNKNOWN);
			Length = new Dimension(Resources.STR_DIM_LENGTH);
			Area = new Dimension(Resources.STR_DIM_AREA);
			Volume = new Dimension(Resources.STR_DIM_VOLUME);
			Time = new Dimension(Resources.STR_DIM_TIME);
			Frequency = new Dimension(Resources.STR_DIM_FREQUENCY);
			Velocity = new Dimension(Resources.STR_DIM_VELOCITY);
			Acceleration = new Dimension(Resources.STR_DIM_ACCELERATION);
			Angle = new Dimension(Resources.STR_DIM_ANGLE);
			AngularVelocity = new Dimension(Resources.STR_DIM_ANGULAR_VELOCITY);
			AngularAcceleration = new Dimension(Resources.STR_DIM_ANGULAR_ACCELERATION);
			Temperature = new Dimension(Resources.STR_DIM_TEMPERATURE);
			Ratio = new Dimension(Resources.STR_DIM_RATIO);
			Weight = new Dimension(Resources.STR_DIM_WEIGHT);
			Force = new Dimension(Resources.STR_DIM_FORCE);
			Torque = new Dimension(Resources.STR_DIM_TORQUE);
			Inertia = new Dimension(Resources.STR_DIM_INERTIA);
			RotationalInertia = new Dimension(Resources.STR_DIM_ROTATIONAL_INERTIA);
			Energy = new Dimension(Resources.STR_DIM_ENERGY);
			Power = new Dimension(Resources.STR_DIM_POWER);
			Data = new Dimension(Resources.STR_DIM_DATA);
			AlternatingCurrent = new Dimension(Resources.STR_DIM_CURRENTAC);
			DirectCurrent = new Dimension(Resources.STR_DIM_CURRENTDC);
		}


		/// <summary>
		///     List of all known dimensions. Note there is no ordering constraint on the returned collection.
		/// </summary>
		public static IEnumerable<Dimension> AllDimensions => _dimensions.Values;


		/// <summary>
		///     Look up a Dimension by its name.
		/// </summary>
		/// <param name="aName">Dame to look for.</param>
		/// <returns>The corresponding code identifier for the dimension, or null if not found.</returns>
		/// <remarks>Dimension names are case-insensitive; comparison is made using the invariant culture.</remarks>
		public static Dimension GetByName(string aName)
		{
			_dimensions.TryGetValue(aName.ToLowerInvariant(), out var d);
			return d;
		}


		#region ---- Default dimension definitions --

		/// <summary>
		///     Identifier for the dimension of dimensionless quantities such as pure numbers.
		///     Note this is not the same as having an unknown or unnamed dimension, as a quantity
		///     can still have some combination of units that there is just no name for, which makes
		///     it not a pure number.
		/// </summary>
		public static Dimension None { get; }

		/// <summary>
		///     Identifier for the dimension of units that have no known dimension. This can result from
		///     a failure to specify the dimension for a basic unit, or from a calculation that produces
		///     a unit that cannot be simplified to a unit that has a known dimension.
		/// </summary>
		public static Dimension Unknown { get; }

		/// <summary>
		///     Identifier for the dimension of ratios. This one is kind of odd but exists because of
		///     the common usage of "%" as a unit symbol representing a scale factor.
		/// </summary>
		public static Dimension Ratio { get; }

		/// <summary>
		///     Identifier for the unit dimension of length. Note that length is one-dimensional
		///     whereas distance and position can be multidimensional; treat those as vectors of lengths.
		/// </summary>
		public static Dimension Length { get; }

		/// <summary>
		///     Identifier for the unit dimension of area.
		/// </summary>
		public static Dimension Area { get; }

		/// <summary>
		///     Identifier for the unit dimension of volume.
		/// </summary>
		public static Dimension Volume { get; }

		/// <summary>
		///     Identifier for the unit dimension of time.
		/// </summary>
		public static Dimension Time { get; }

		/// <summary>
		///     Identifier for the unit dimension of frequency.
		/// </summary>
		public static Dimension Frequency { get; }

		/// <summary>
		///     Identifier for the unit dimension of linear speed. Note that velocity is normally
		///     considered speed with direction, whereas speed is directionless. Consider velocity
		///     to be a vector of speeds or a speed at an angle.
		/// </summary>
		public static Dimension Velocity { get; }

		/// <summary>
		///     Identifier for the unit dimension of linear acceleration.
		/// </summary>
		public static Dimension Acceleration { get; }

		/// <summary>
		///     Identifier for the unit dimension of angle.
		/// </summary>
		public static Dimension Angle { get; }

		/// <summary>
		///     Identifier for the unit dimension of angular speed. Note that people commonly say
		///     "angular velocity" when they mean this, but that is not correct since velocity can
		///     be multidimensional. Even angular velocity can be a vector of speeds, for example on
		///     the surface of a sphere.
		/// </summary>
		public static Dimension AngularVelocity { get; }

		/// <summary>
		///     Identifier for the unit dimension of angular acceleration.
		/// </summary>
		public static Dimension AngularAcceleration { get; }

		/// <summary>
		///     Identifier for the unit dimension of temperature.
		/// </summary>
		public static Dimension Temperature { get; }

		/// <summary>
		///     Identifier for the unit dimension of weight.
		/// </summary>
		public static Dimension Weight { get; }

		/// <summary>
		///     Identifier for the unit dimension of force.
		/// </summary>
		public static Dimension Force { get; }

		/// <summary>
		///     Identifier for the unit dimension of torque.
		/// </summary>
		public static Dimension Torque { get; }

		/// <summary>
		///     Identifier for the unit dimension of linear intertia.
		/// </summary>
		public static Dimension Inertia { get; }

		/// <summary>
		///     Identifier for the unit dimension of rotational inertia (moment of inertia or angular mass).
		/// </summary>
		public static Dimension RotationalInertia { get; }

		/// <summary>
		///     Identifier for the unit dimension of work energy.
		/// </summary>
		public static Dimension Energy { get; }

		/// <summary>
		///     Identifier for the unit dimension of power.
		/// </summary>
		public static Dimension Power { get; }

		/// <summary>
		///     Identifier for the unit dimension of data storage.
		/// </summary>
		public static Dimension Data { get; }

		/// <summary>
		///     Identifier for the unit dimension of AC current.
		/// </summary>
		public static Dimension AlternatingCurrent { get; }

		/// <summary>
		///     Identifier for the unit dimension of DC current.
		/// </summary>
		public static Dimension DirectCurrent { get; }

		#endregion

		#endregion

		#region -- Instance API --

		/// <summary>
		///     Instance constructor. Initializes a new dimension and adds it to the static table.
		/// </summary>
		/// <param name="aName">Name of the dimension. Must be unique. Should be capitalized.</param>
		/// <exception cref="ArgumentException">The given dimension name is null, empty or already exists.</exception>
		public Dimension(string aName)
		{
			Name = aName;

			var name = aName.ToLowerInvariant();
			if (string.IsNullOrEmpty(name))
			{
				throw new ArgumentNullException(nameof(aName));
			}

			if (_dimensions.ContainsKey(name))
			{
				throw new ArgumentException(string.Format(Resources.ERR_DUPLICATE_DIMENSION, name));
			}

			_dimensions[name] = this;
		}


		/// <summary>
		///     Unique, human-readable name for the dimension.
		/// </summary>
		public string Name { get; }


		/// <summary>
		///     Override string conversion to return dimension name.
		/// </summary>
		/// <returns>
		///     <see cref="Name" />
		/// </returns>
		public override string ToString() => Name;


		#region ---- IEquatable implementation --

		/// <summary>
		///     Test for equality to another Dimension instance.
		/// </summary>
		/// <param name="aOther">The other instance to check.</param>
		/// <returns>True if the two dimensions identify the same quantity.</returns>
		public bool Equals(Dimension aOther) => (null != aOther) && (Name == aOther.Name);

		#endregion

		#endregion
	}
}

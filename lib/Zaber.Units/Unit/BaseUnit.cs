// This is a derivative work of Henning Dieterichs's Units of Measure Library for .NET.
// See the accompanying README for details.

using System;

namespace Zaber.Units
{
	/// <summary>
	///     A base unit, which can be only identified by its abbreviation. See http://en.wikipedia.org/wiki/SI_base_unit.
	/// </summary>
	public sealed class BaseUnit : Unit
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Constructor. Initializes name and abbreviation.
		/// </summary>
		/// <param name="aAbbrev">The abbreviation (unit symbol) for this unit.</param>
		/// <param name="aName">The full name of the unit.</param>
		public BaseUnit(string aAbbrev, string aName)
			: base(aAbbrev, aName)
		{
			if (null == aAbbrev)
			{
				throw new ArgumentNullException("unitAbbreviation");
			}
		}


		/// <summary>
		///     Constructor. Initializes abbreviation and sets name to null.
		/// </summary>
		/// <param name="aAbbrev">The abbreviation (unit symbol) for this unit.</param>
		public BaseUnit(string aAbbrev)
			: this(aAbbrev, null)
		{
		}


		/// <summary>
		///     Get a unique hash code for this unit.
		/// </summary>
		/// <returns>A hash of the salient properties of this unit.</returns>
		public override int GetHashCode() => Abbreviation.GetHashCode();

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Determine if this unit is a coherent unit.
		/// </summary>
		/// <returns>True if conherent.</returns>
		protected override bool GetIsCoherent() => true;


		/// <summary>
		///     Get the transformation from this unit to the corresponding coherent unit.
		/// </summary>
		/// <returns>A new transformation.</returns>
		protected override QuantityTransformation GetTransformationToCoherent()
			=> new LinearQuantityTransformation(this, this, 1, 0);


		/// <summary>
		///     Determine if two units identify the same measure of the same quantity.
		/// </summary>
		/// <param name="aOther">The other unit to compare to.</param>
		/// <returns>True if the two units have the same abbreviation.</returns>
		protected override bool IsSame(Unit aOther)
		{
			var bu = aOther as BaseUnit;
			if (null != bu)
			{
				if (aOther.Abbreviation == Abbreviation)
				{
					return true;
				}
			}

			return false;
		}

		#endregion
	}
}

// This is a derivative work of Henning Dieterichs's Units of Measure Library for .NET.
// See the accompanying README for details.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zaber.Units
{
	/// <summary>
	///     A derived unit. See http://en.wikipedia.org/wiki/SI_derived_unit.
	/// </summary>
	public sealed class DerivedUnit : Unit
	{
		#region -- Public Methods & Properties --

		private DerivedUnit(UnitPart[] aParts)
			: this(null, null, aParts)
		{
		}


		private DerivedUnit(string aAbbrev, string aName, UnitPart[] aParts)
			: base(aAbbrev, aName)
		{
			if (null == aParts)
			{
				throw new ArgumentNullException("parts");
			}

			Parts = aParts.OrderBy(u => u.Exponent).ThenBy(u => u.Unit.ToString()).ToArray();

			if (null == aAbbrev)
			{
				Abbreviation = DefinitionToString("DI", null).Replace(" ", "");
			}
		}


		/// <summary>
		///     Gets a new unit from unit parts.
		/// </summary>
		/// <param name="aAbbrev">The abbreviation of the new unit.</param>
		/// <param name="aName">The name of the new unit.</param>
		/// <param name="aParts">The unit parts.</param>
		/// <returns>
		///     Unit.DimensionlessUnit, if the length of parts is 0. If the length is 1 and the exponent is 1, the first part
		///     is returned, otherwise a DerivedUnit.
		/// </returns>
		public static Unit GetUnitFromParts(string aAbbrev, string aName, UnitPart[] aParts)
		{
			if (0 == aParts.Length)
			{
				return Dimensionless;
			}

			if ((1 == aParts.Length) && (aParts.First().Exponent == 1))
			{
				return aParts.First().Unit;
			}

			return new DerivedUnit(aAbbrev, aName, aParts);
		}


		/// <summary>
		///     Gets a new unit from unit parts.
		/// </summary>
		/// <param name="aParts">The unit parts.</param>
		/// <returns>
		///     Unit.DimensionlessUnit, if the length of parts is 0. If the length is 1 and the exponent is 1, the first part
		///     is returned, otherwise a DerivedUnit.
		/// </returns>
		public static Unit GetUnitFromParts(UnitPart[] aParts) => GetUnitFromParts(null, null, aParts);


		/// <summary>
		///     Get a unique hash code for this unit.
		/// </summary>
		/// <returns>A hash of the salient properties of this unit.</returns>
		public override int GetHashCode() => Parts.Aggregate(0, (hashcode, p) => hashcode ^ p.GetHashCode());


		/// <summary>
		///     Tries to generate a dimension name for this unit from its parts.
		/// </summary>
		/// <returns>A new or existing <see cref="Dimension" />generated from the composition of this unit.</returns>
		public Dimension GenerateDimension()
		{
			// Generate a dimension name if needed.
			var partsByName = new Dictionary<string, int>();
			foreach (var part in Parts)
			{
				var dn = part.Unit.Dimension.Name;
				if (!partsByName.ContainsKey(dn))
				{
					partsByName[dn] = 0;
				}

				partsByName[dn] = partsByName[dn] + part.Exponent;
			}

			var orderedParts = partsByName.OrderBy(pair => pair.Value).ThenBy(pair => pair.Key);

			var sb = new StringBuilder();
			var firstUnit = orderedParts.Take(1).First();
			sb.Append(firstUnit.Key);

			var sign = Math.Sign(firstUnit.Value);
			foreach (var part in orderedParts.Skip(1))
			{
				if (Math.Sign(part.Value) == sign)
				{
					sb.Append(" * " + part.Key.ToLower());
				}
				else
				{
					sign = Math.Sign(part.Value);
					sb.Append(" / " + part.Key.ToLower());
				}

				var exp = Math.Abs(part.Value);
				if (exp != 1)
				{
					sb.AppendFormat("^{0}", exp);
				}
			}

			var dimName = sb.ToString();
			var dim = Dimension.GetByName(dimName) ?? new Dimension(dimName);

			return dim;
		}


		/// <summary>
		///     The components of this compound unit.
		/// </summary>
		public UnitPart[] Parts { get; }

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Determine if this unit is coherent.
		/// </summary>
		/// <returns>True if coherent.</returns>
		protected override bool GetIsCoherent() => IsSame(GetTransformationToCoherent().TargetUnit);


		/// <summary>
		///     Get the transformation from this unit to the corresponding coherent unit.
		/// </summary>
		/// <returns>A new transformation.</returns>
		protected override QuantityTransformation GetTransformationToCoherent()
		{
			//sqrkm(sqrm) = (km(sqrt(sqrm)))^2
			//km(m) := x*m; h(s) := y*s
			//kmh(m/s) = km(m)/hour(s) = km(1)*m/(hour(1)*s) = m/s * km(1)/h(1)
			// e.g. x * km^3 * hour^2 => m^3 * s^2 (=> cm^3 * ms^2)

			double factor = 1;

			var parts = new UnitPartCollection();

			foreach (var part in Parts
				.Select(p => new
							{
								Transformation = p.Unit is ICouldBeUnproportional
									? (LinearQuantityTransformation) ((ICouldBeUnproportional) p.Unit).ProportionalTransformationToCoherent
									: (LinearQuantityTransformation) p.Unit.TransformationToCoherent,
								p.Exponent
							}))
			{
				factor *= Math.Pow(part.Transformation.Factor, part.Exponent);
				parts.Add(part.Transformation.TargetUnit, part.Exponent);
			}

			return new LinearQuantityTransformation(this, GetUnitFromParts(parts.ToArray()), factor, 0);
		}


		/// <summary>
		///     Determine if two units identify the same measure of the same quantity.
		/// </summary>
		/// <param name="aOther">The other unit to compare to.</param>
		/// <returns>True if the two units have the same composition.</returns>
		protected override bool IsSame(Unit aOther)
		{
			var du = aOther as DerivedUnit;
			if (null == du)
			{
				return false;
			}

			if (Parts.Length != du.Parts.Length)
			{
				return false;
			}

			for (var i = 0; i < Parts.Length; i++)
			{
				if (!Parts[i].Equals(du.Parts[i]))
				{
					return false;
				}
			}

			return true;
		}


		/// <summary>
		///     Helper for converting a compound unit to a unit symbol string.
		/// </summary>
		/// <param name="aFormat">
		///     A string containing flags: E to enclose denominators in braces,
		///     D to use denominators, I to ignore exponents if equal to 1, L to use long form, F to use definition.
		/// </param>
		/// <param name="aFormatProvider">Unused.</param>
		/// <returns>A string representation of this unit.</returns>
		protected override string DefinitionToString(string aFormat, IFormatProvider aFormatProvider)
		{
			//  e/E: Enclose Denominator in braces. d/D use denominator. i/I ignore exponent if equal to 1. 
			//  l/L use abbreviation/long form. f/F use definition

			var encloseDenominatorInBraces = aFormat.Contains("E");
			var useDenominator = aFormat.Contains("D");

			var positive = Parts.Where(p => (p.Exponent > 0) | !useDenominator)
							 .Select(p => p.ToString(false, aFormat.Contains("I")));
			var negative = Parts.Where(p => (p.Exponent < 0) && useDenominator)
							 .Select(p => p.ToString(true, aFormat.Contains("I")));

			var positiveStr = positive.ToArray();
			var negativeStr = negative.ToArray();

			var result = "";

			result = positiveStr.Length > 0 ? string.Join(" * ", positiveStr) : "1";

			if (negativeStr.Length > 0)
			{
				result += " / ";
				if ((negativeStr.Length > 1) && encloseDenominatorInBraces)
				{
					result += "(";
				}

				result += string.Join(" * ", negativeStr);

				if ((negativeStr.Length > 1) && encloseDenominatorInBraces)
				{
					result += ")";
				}
			}

			return result;
		}

		#endregion
	}
}

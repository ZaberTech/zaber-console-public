// This is a derivative work of Henning Dieterichs's Units of Measure Library for .NET.
// See the accompanying README for details.

using System;

namespace Zaber.Units
{
	/// <summary>
	///     This class represents a part of a derived unit, e.g. km/h or m^3.
	/// </summary>
	public sealed class UnitPart : IEquatable<UnitPart>, IFormattable
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Creates a new UnitPart
		/// </summary>
		/// <param name="aUnit">The unit of this part</param>
		/// <param name="aExponent">The exponent of this part</param>
		public UnitPart(Unit aUnit, int aExponent)
		{
			if (0 == aExponent)
			{
				throw new ArgumentOutOfRangeException("exponent");
			}

			if (null == aUnit)
			{
				throw new ArgumentNullException("unit");
			}

			Unit = aUnit;
			Exponent = aExponent;
		}


		/// <summary>
		///     Formats this part into a readable string.
		/// </summary>
		/// <param name="aInvertExponent">
		///     This parameter inverts the exponent. This is useful if this unit part is part of a
		///     denominator.
		/// </param>
		/// <param name="aIgnoreExponentOne">True if unity exponents should be omitted.</param>
		/// <returns>The formatted string in the following format: unit[^exponent]. The last term is skipped if exponent is 1</returns>
		public string ToString(bool aInvertExponent, bool aIgnoreExponentOne)
		{
			var p = aInvertExponent ? -Exponent : Exponent;

			var result = Unit.ToString();
			if ((1 == p) && aIgnoreExponentOne)
			{
				return result;
			}

			return result + "^" + p;
		}


		/// <summary>
		///     Format as a string, using default options (don't invert exponent, ignore exponent = 1).
		/// </summary>
		/// <returns>The unit with exponent as a string.</returns>
		public override string ToString() => ToString(false, true);


		/// <summary>
		///     Format as a localized string.
		/// </summary>
		/// <param name="aFormat">
		///     String containing zero or one instances of the 'I' flag, which means to ignore
		///     exponent = 1.
		/// </param>
		/// <param name="aFormatProvider">Unused.</param>
		/// <returns></returns>
		public string ToString(string aFormat, IFormatProvider aFormatProvider)
		{
			var ignoreExponentOne = aFormat.Contains("I");
			return ToString(false, ignoreExponentOne);
		}


		/// <summary>
		///     Equality test.
		/// </summary>
		/// <param name="aOther">The other UnitPart to compare to.</param>
		/// <returns>True if equivalent.</returns>
		public override bool Equals(object aOther) => aOther is UnitPart && Equals((UnitPart) aOther);


		/// <summary>
		///     Equality test.
		/// </summary>
		/// <param name="aOther">The other UnitPart to compare to.</param>
		/// <returns>True if equivalent.</returns>
		public bool Equals(UnitPart aOther) => (aOther.Exponent == Exponent) && aOther.Unit.Equals(Unit);


		/// <summary>
		///     Get a unique hash code for this unit.
		/// </summary>
		/// <returns>A hash of the salient properties of this unit part.</returns>
		public override int GetHashCode() => Unit.GetHashCode() ^ Exponent;


		/// <summary>
		///     The underlaying unit
		/// </summary>
		public Unit Unit { get; }


		/// <summary>
		///     The exponent of the unit.
		/// </summary>
		/// <example>
		///     An exponent of -2 means 1/Unit^2
		/// </example>
		public int Exponent { get; }

		#endregion
	}
}

﻿// This is a derivative work of Henning Dieterichs's Units of Measure Library for .NET.
// See the accompanying README for details.

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace Zaber.Units
{
	/// <summary>
	///     Imports unit definitions from XML documents to IUnitRegistry objects.
	/// </summary>
	public class XmlUnitLibrary
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Creates a new >XmlUnitLibrary, loading the xml document from a stream.
		/// </summary>
		/// <param name="aXmlStrem">The stream</param>
		public XmlUnitLibrary(Stream aXmlStrem)
			: this(XmlReader.Create(aXmlStrem))
		{
		}


		/// <summary>
		///     Creates a new XmlUnitLibrary from the passed xml document.
		///     The document has to be compliant with the XSD schema (namespace http://www.hediet.de/xsd/unitlibrary/1.0).
		/// </summary>
		/// <param name="aXmlReader">The xml reader.</param>
		public XmlUnitLibrary(XmlReader aXmlReader)
		{
			var settings = new XmlReaderSettings { ValidationType = ValidationType.Schema };
			settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;

			var schema = XmlSchema.Read(
				Assembly.GetExecutingAssembly().GetManifestResourceStream("Zaber.Units.UnitsOfMeasureLibrary.xsd"),
				(_, aArgs) => throw aArgs.Exception);

			settings.Schemas.Add(schema);

			Exception lastException = null;

			settings.ValidationEventHandler += (sender, e) => { lastException = e.Exception; };

			var validationReader = XmlReader.Create(aXmlReader, settings);

			_document = XDocument.Load(validationReader);

			if (null != lastException)
			{
				throw lastException;
			}
		}


		private XmlUnitLibrary() //private constructor for loading without verification
		{
			var reader = XmlReader.Create(Assembly.GetExecutingAssembly()
											   .GetManifestResourceStream("Zaber.Units.DefaultUnitLibrary.xml"));
			_document = XDocument.Load(reader);
		}


		/// <summary>
		///     Creates a default unit library, using the inbuilt xml document.
		/// </summary>
		/// <returns>The library.</returns>
		public static XmlUnitLibrary NewDefaultLibrary() => new XmlUnitLibrary();


		/// <summary>
		///     Loads all unit definitions within this xml document to the specified IUnitRegistry.
		///     Multiple libraries can be loaded into one registry.
		/// </summary>
		/// <param name="aRegistry">The registry.</param>
		/// <param name="aParser">
		///     The parser used for resolving unit references. The parser should be able to parse all units
		///     passed to the registry.
		/// </param>
		/// <param name="aFallback">
		///     Optional. A fallback lookup table for unit definitions outside the current
		///     XML file. Note that this fallback must be made the fallback of the UnitConverter that will
		///     contain aRegistery and aParser or exceptions will result.
		/// </param>
		public void LoadTo(IUnitRegistry aRegistry, IUnitParser aParser, IUnitConverter aFallback = null)
		{
			if (null == aRegistry)
			{
				throw new ArgumentNullException(nameof(aRegistry));
			}

			if (null == aParser)
			{
				throw new ArgumentNullException(nameof(aParser));
			}

			foreach (var item in _document.Elements().First().Elements())
			{
				Unit newUnit = null;

				var name = item.Attribute("Name").Value;
				var abbr = item.Attribute("Abbr").Value;
				var useSI = true;
				var dim = Dimension.Unknown;

				if ("BaseUnit" == item.Name.LocalName)
				{
					newUnit = new BaseUnit(abbr, name);
				}
				else if ("ScaledShiftedUnit" == item.Name.LocalName)
				{
					var unitSymbol = item.Attribute("UnderlyingUnit").Value;

					var underlyingUnit = aParser.TryParseUnit(unitSymbol);
					if ((null == underlyingUnit) && (null != aFallback))
					{
						underlyingUnit = aFallback.FindUnitBySymbol(unitSymbol);
					}

					if (null == underlyingUnit)
					{
						throw new FormatException(
							$"Could not find a definition for underlying unit '{unitSymbol}' when constructing '{abbr}'");
					}

					useSI = underlyingUnit.UseSIPrefixes;
					dim = underlyingUnit.Dimension;

					double offset = 0;
					if (item.Attribute("Offset") != null)
					{
						offset = double.Parse(item.Attribute("Offset").Value, NumberFormatInfo.InvariantInfo);
					}

					newUnit = new ScaledShiftedUnit(abbr,
													name,
													underlyingUnit,
													double.Parse(item.Attribute("Factor").Value,
																 NumberFormatInfo.InvariantInfo),
													offset);
				}
				else if (item.Name.LocalName == "DerivedUnit")
				{
					var unitParts = new List<UnitPart>();
					foreach (var unitPartElement in item.Elements())
					{
						if (unitPartElement.Name.LocalName == "UnitPart")
						{
							var unitSymbol = unitPartElement.Attribute("Unit").Value;
							var unitPart = aParser.TryParseUnit(unitSymbol);
							if ((null == unitPart) && (null != aFallback))
							{
								unitPart = aFallback.FindUnitBySymbol(unitSymbol);
							}

							if (null == unitPart)
							{
								throw new FormatException(
									$"Could not find a definition for underlying unit '{unitSymbol}' when constructing '{abbr}'");
							}

							var exponent = int.Parse(unitPartElement.Attribute("Exponent").Value,
													 NumberFormatInfo.InvariantInfo);
							unitParts.Add(new UnitPart(unitPart, exponent));
						}
					}

					newUnit = DerivedUnit.GetUnitFromParts(abbr, name, unitParts.ToArray());
				}

				if (null != newUnit)
				{
					var attr = item.Attribute("UseSIPrefixes");
					if (null != attr)
					{
						useSI = bool.Parse(attr.Value);
					}

					newUnit.UseSIPrefixes = useSI;

					attr = item.Attribute("Dimension");
					if (null != attr)
					{
						dim = Dimension.GetByName(attr.Value) ?? new Dimension(attr.Value);
					}
					else if (newUnit is DerivedUnit)
					{
						dim = (newUnit as DerivedUnit).GenerateDimension();
					}

					newUnit.Dimension = dim;

					aRegistry.RegisteredUnits[abbr] = newUnit;
				}
			}
		}

		#endregion

		#region -- Data --

		private readonly XDocument _document;

		#endregion
	}
}

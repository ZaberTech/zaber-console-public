// This is a derivative work of Henning Dieterichs's Units of Measure Library for .NET.
// See the accompanying README for details.

using System.Collections.Generic;

namespace Zaber.Units
{
	/// <summary>
	///     A parser which compares the string to parse with all abbreviations of the registered units.
	/// </summary>
	public class RegisteredUnitParser : IUnitParser, IUnitRegistry
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Creates a new RegisteredUnitParser. 1 is registered by default to  <see cref="Unit.Dimensionless" />.
		/// </summary>
		public RegisteredUnitParser()
		{
			RegisteredUnits = new Dictionary<string, Unit> { ["1"] = Unit.Dimensionless };
		}


		#region -- IUnitParser implementation --

		/// <summary>
		///     Parses a unit string.
		/// </summary>
		/// <param name="aUnitStr">The string to parse.</param>
		/// <param name="aResultUnitAbbrev">
		///     The abbreviation of the returned unit. Will be ignored, if unitStr references a
		///     registered unit.
		/// </param>
		/// <param name="aResultUnitName">The name of the returned unit. Will be ignored, if unitStr references a registered unit.</param>
		/// <param name="aEnableException">Specifies whether exceptions should be thrown or null should be returned.</param>
		/// <returns>The parsed unit, or null if parsing failed and throwFormatException is false.</returns>
		public Unit ParseUnit(string aUnitStr, string aResultUnitAbbrev, string aResultUnitName, bool aEnableException)
		{
			if (RegisteredUnits.TryGetValue(aUnitStr, out var unit))
			{
				return unit;
			}

			return UnitParserExtension.ReturnNullOrThrowFormatException(
				$"Unit with abbreviation \"{aUnitStr}\" not found",
				aEnableException);
		}

		#endregion

		#region -- IUnitRegistry implementation --

		/// <summary>
		///     All registered units.
		/// </summary>
		public IDictionary<string, Unit> RegisteredUnits { get; }

		#endregion

		#endregion
	}
}

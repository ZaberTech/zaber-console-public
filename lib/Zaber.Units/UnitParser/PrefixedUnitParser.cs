// This is a derivative work of Henning Dieterichs's Units of Measure Library for .NET.
// See the accompanying README for details.

using System;
using System.Linq;

namespace Zaber.Units
{
	/// <summary>
	///     Parses the following formats: km, MHz, �s (m, Hz and s have to be registered).
	///     All avaible prefixes are defined in <see cref="Prefix" />.
	/// </summary>
	public class PrefixedUnitParser : IUnitParser
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Creates a new PrefixedUnitParser.
		/// </summary>
		/// <param name="aRegisteredUnitParser">The parser used for resolving the underlaying units.</param>
		public PrefixedUnitParser(IUnitParser aRegisteredUnitParser)
		{
			_registeredUnitParser = aRegisteredUnitParser ?? throw new ArgumentNullException("registeredUnitParser");
		}


		#region -- IUnitParser implementation --

		/// <summary>
		///     Parses a unit string.
		/// </summary>
		/// <param name="aUnitStr">The string to parse.</param>
		/// <param name="aResultUnitAbbrev">
		///     The abbreviation of the returned unit. Will be ignored, if unitStr references a
		///     registered unit.
		/// </param>
		/// <param name="aResultUnitName">The name of the returned unit. Will be ignored, if unitStr references a registered unit.</param>
		/// <param name="aEnableException">Specifies whether exceptions should be thrown or null should be returned.</param>
		/// <returns>The parsed unit, or null if parsing failed and throwFormatException is false.</returns>
		public Unit ParseUnit(string aUnitStr, string aResultUnitAbbrev, string aResultUnitName, bool aEnableException)
		{
			if (null == aUnitStr)
			{
				throw new ArgumentNullException(nameof(aUnitStr));
			}

			if (aUnitStr.Length > 1)
			{
				foreach (var prefix in Prefix.All.Where(p => aUnitStr.StartsWith(p.Abbreviation)))
				{
					var unit = _registeredUnitParser.ParseUnit(aUnitStr.Substring(prefix.Abbreviation.Length),
															   null,
															   null,
															   aEnableException);

					if ((null != unit) && unit.UseSIPrefixes)
					{
						if ((null == aResultUnitName) && (null != unit.Name))
						{
							aResultUnitName = prefix.Name + unit.Name.ToLowerInvariant();
						}

						if (null == aResultUnitAbbrev)
						{
							aResultUnitAbbrev = aUnitStr;
						}

						var newUnit =
							new ScaledShiftedUnit(aResultUnitAbbrev, aResultUnitName, unit, prefix.Factor)
							{
								Dimension = unit.Dimension,
								UseSIPrefixes = false
							};

						return newUnit;
					}
				}

				return UnitParserExtension.ReturnNullOrThrowFormatException(
					$"Could not parse unit '{aUnitStr}' even after looking at prefixes.",
					aEnableException);
			}

			return UnitParserExtension.ReturnNullOrThrowFormatException($"Input string \"{aUnitStr}\" is to short",
				aEnableException);
		}

		#endregion

		#endregion

		#region -- Data --

		private readonly IUnitParser _registeredUnitParser;

		#endregion
	}
}

﻿// This is a derivative work of Henning Dieterichs's Units of Measure Library for .NET.
// See the accompanying README for details.

using System.Collections.Generic;
using System.Linq;

namespace Zaber.Units
{
	/// <summary>
	///     A composed parser, which delegates the parsing to multiple other parsers.
	/// </summary>
	public class ComposedUnitParser : IUnitParser
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Creates a new, empty composed unit parser.
		/// </summary>
		public ComposedUnitParser()
		{
			UnitParsers = new List<IUnitParser>();
		}


		/// <summary>
		///     Returns a preconfigured composed parser with all necessary parsers.
		/// </summary>
		/// <remarks>
		///     The registered parsers are: RegisteredUnitParser, PrefixedUnitParser, ScaledShiftedUnitParser and
		///     DerivedUnitParser.
		/// </remarks>
		/// <returns>The composed unit parser.</returns>
		public static ComposedUnitParser NewDefaultUnitParser()
		{
			var parser = new ComposedUnitParser();
			var rup = new RegisteredUnitParser();
			var pup = new PrefixedUnitParser(rup);

			var basicParser = new ComposedUnitParser();
			basicParser.UnitParsers.Add(rup);
			basicParser.UnitParsers.Add(pup);

			parser.UnitParsers.Add(rup);
			parser.UnitParsers.Add(pup);

			parser.UnitParsers.Add(new ScaledShiftedUnitParser(basicParser));
			parser.UnitParsers.Add(new DerivedUnitParser(basicParser));

			return parser;
		}


		/// <summary>
		///     Gets the first parser within UnitParsers which implements IUnitRegistry.
		/// </summary>
		/// <returns>The first parser within UnitParsers which implements IUnitRegistry. If no parser is found, null is returned.</returns>
		public IUnitRegistry GetUnitRegistry() => UnitParsers.OfType<IUnitRegistry>().FirstOrDefault();


		#region -- IUnitParser implementation --

		/// <summary>
		///     Parses a unit string.
		/// </summary>
		/// <param name="aUnitStr">The string to parse.</param>
		/// <param name="aResultUnitAbbrev">
		///     The abbreviation of the returned unit. Will be ignored, if unitStr references a
		///     registered unit.
		/// </param>
		/// <param name="aResultUnitName">The name of the returned unit. Will be ignored, if unitStr references a registered unit.</param>
		/// <param name="aEnableException">Specifies whether exceptions should be thrown or null should be returned.</param>
		/// <returns>The parsed unit, or null if parsing failed and throwFormatException is false.</returns>
		public Unit ParseUnit(string aUnitStr, string aResultUnitAbbrev, string aResultUnitName, bool aEnableException)
		{
			foreach (var parser in UnitParsers)
			{
				var unit = parser.ParseUnit(aUnitStr, null, null, false);
				if (null != unit)
				{
					return unit;
				}
			}

			return UnitParserExtension.ReturnNullOrThrowFormatException(
				$"None of the registered parsers is able to parse the string \"{aUnitStr}\"",
				aEnableException);
		}

		#endregion


		/// <summary>
		///     All registered parsers.
		/// </summary>
		public IList<IUnitParser> UnitParsers { get; }

		#endregion
	}
}

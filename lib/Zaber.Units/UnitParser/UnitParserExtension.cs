// This is a derivative work of Henning Dieterichs's Units of Measure Library for .NET.
// See the accompanying README for details.

using System;

namespace Zaber.Units
{
	/// <summary>
	///     Extension methods for unit parsers.
	/// </summary>
	public static class UnitParserExtension
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Handle the switch between enabling exceptions or returning nulls on errors.
		/// </summary>
		/// <param name="aMessage">Message to put in the exception if thrown.</param>
		/// <param name="aInnerException">Inner exception for the exception if thrown.</param>
		/// <param name="aEnableException">
		///     True to enable exception; false to return null
		///     without throwing an exception.
		/// </param>
		/// <returns>Null or throws an exception.</returns>
		public static Unit ReturnNullOrThrowFormatException(string aMessage, Exception aInnerException,
															bool aEnableException)
		{
			if (aEnableException)
			{
				throw new FormatException(aMessage, aInnerException);
			}

			return null;
		}


		/// <summary>
		///     Handle the switch between enabling exceptions or returning nulls on errors.
		/// </summary>
		/// <param name="aMessage">Message to put in the exception if thrown.</param>
		/// <param name="aEnableException">
		///     True to enable exception; false to return null
		///     without throwing an exception.
		/// </param>
		/// <returns>Null or throws an exception.</returns>
		public static Unit ReturnNullOrThrowFormatException(string aMessage, bool aEnableException)
			=> ReturnNullOrThrowFormatException(aMessage, null, aEnableException);


		/// <summary>
		///     Parses the unit string. If unsuccessful, an exception will be thrown.
		/// </summary>
		/// <param name="parser">this</param>
		/// <param name="unitStr">The string to parse.</param>
		/// <returns>The parsed unit.</returns>
		public static Unit ParseUnit(this IUnitParser parser, string unitStr)
			=> parser.ParseUnit(unitStr, null, null, true);


		/// <summary>
		///     Parses the unit string. If unsuccessful, no exception will be thrown.
		/// </summary>
		/// <param name="parser">this</param>
		/// <param name="unitStr">The string to parse.</param>
		/// <returns>The parsed unit or null if unsuccessful.</returns>
		public static Unit TryParseUnit(this IUnitParser parser, string unitStr)
			=> TryParse(parser, unitStr, null, null);


		/// <summary>
		///     Parses the unit string. If unsuccessful, no exception will be thrown.
		/// </summary>
		/// <param name="aParser">this</param>
		/// <param name="aUnitStr">The string to parse.</param>
		/// <param name="aResultUnitAbbrev">
		///     The abbreviation of the returned unit. Will be ignored, if unitStr references a
		///     registered unit.
		/// </param>
		/// <param name="aResultUnitName">The name of the returned unit. Will be ignored, if unitStr references a registered unit.</param>
		/// <returns>The parsed unit or null if unsuccessful.</returns>
		public static Unit TryParse(this IUnitParser aParser, string aUnitStr, string aResultUnitAbbrev,
									string aResultUnitName)
			=> aParser.ParseUnit(aUnitStr, aResultUnitAbbrev, aResultUnitName, false);


		/// <summary>
		///     Loads the default unit collection included with this library into the specified parser and registry.
		/// </summary>
		/// <param name="aParser">this. The parser that will be used to parse units.</param>
		/// <param name="aRegistry">A unit registry to populate with the units found.</param>
		public static void LoadDefaultUnitLibrary(this IUnitParser aParser, IUnitRegistry aRegistry)
		{
			var lib = XmlUnitLibrary.NewDefaultLibrary();
			lib.LoadTo(aRegistry, aParser);
		}

		#endregion
	}
}

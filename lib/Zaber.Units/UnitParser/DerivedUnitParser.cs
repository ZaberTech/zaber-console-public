// This is a derivative work of Henning Dieterichs's Units of Measure Library for .NET.
// See the accompanying README for details.

using System;
using System.Collections.Generic;

namespace Zaber.Units
{
	/// <summary>
	///     Parses the following formats: km/h; m^3; A^1*s^1*V^-1*m^-1; A * s / V * m.
	/// </summary>
	public class DerivedUnitParser : IUnitParser
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Creates a new DerivedUnitParser.
		/// </summary>
		/// <param name="aAbbreviatedUnitParser">The parser to parse all abbreviated units, e.g. m or V.</param>
		public DerivedUnitParser(IUnitParser aAbbreviatedUnitParser)
		{
			_abbreviatedUnitParser = aAbbreviatedUnitParser ?? throw new ArgumentNullException("abbreviatedUnitParser");
		}


		#region -- IUnitParser implementation --

		/// <summary>
		///     Parses a unit string.
		/// </summary>
		/// <param name="aUnitStr">The string to parse.</param>
		/// <param name="aResultUnitAbbrev">
		///     The abbreviation of the returned unit. Will be ignored, if unitStr references a
		///     registered unit.
		/// </param>
		/// <param name="aResultUnitName">The name of the returned unit. Will be ignored, if unitStr references a registered unit.</param>
		/// <param name="aEnableException">Specifies whether exceptions should be thrown or null should be returned.</param>
		/// <returns>The parsed unit, or null if parsing failed and throwFormatException is false.</returns>
		public Unit ParseUnit(string aUnitStr, string aResultUnitAbbrev, string aResultUnitName, bool aEnableException)
		{
			if (null == aUnitStr)
			{
				throw new ArgumentNullException("unitStr");
			}

			aUnitStr = aUnitStr.Replace("/", " / ").Replace("*", " * ");
			var items = aUnitStr.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

			//km * m^2 / h^-1 * s^2
			if ((items.Length % 2) == 0)
			{
				return UnitParserExtension.ReturnNullOrThrowFormatException($"Invalid format: \"{aUnitStr}\"",
					aEnableException);
			}

			var onDenominator = false;

			var parts = new List<UnitPart>();

			for (var i = 0; i < items.Length; i += 2)
			{
				var part = GetUnitPart(items[i], onDenominator);
				if (null == part)
				{
					return UnitParserExtension.ReturnNullOrThrowFormatException(null, aEnableException);
				}

				if (i != (items.Length - 1))
				{
					var op = items[i + 1];

					if ((op != "*") && (op != "/"))
					{
						return UnitParserExtension.ReturnNullOrThrowFormatException(null, aEnableException);
					}

					if (op == "/")
					{
						if (onDenominator)
						{
							return UnitParserExtension.ReturnNullOrThrowFormatException(null, aEnableException);
						}

						onDenominator = true;
					}
				}

				parts.Add(part);
			}

			return DerivedUnit.GetUnitFromParts(aResultUnitAbbrev, aResultUnitName, parts.ToArray());
		}

		#endregion

		#endregion

		#region -- Non-Public Methods --

		private UnitPart GetUnitPart(string aStr, bool aNegative = false)
		{
			var items = aStr.Split('^');
			if (items.Length < 3)
			{
				var u = _abbreviatedUnitParser.ParseUnit(items[0], null, null, false);

				if (u != null)
				{
					var exponent = 1;

					if ((items.Length == 1) || int.TryParse(items[1], out exponent))
					{
						if (aNegative)
						{
							exponent *= -1;
						}

						return new UnitPart(u, exponent);
					}
				}
			}

			return null;
		}

		#endregion

		#region -- Data --

		private readonly IUnitParser _abbreviatedUnitParser;

		#endregion
	}
}

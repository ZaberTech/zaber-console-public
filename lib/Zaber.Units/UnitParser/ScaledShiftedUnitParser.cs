// This is a derivative work of Henning Dieterichs's Units of Measure Library for .NET.
// See the accompanying README for details.

using System;
using System.Globalization;

namespace Zaber.Units
{
	/// <summary>
	///     Parses the following formats: 60 s; 60 min;
	/// </summary>
	public class ScaledShiftedUnitParser : IUnitParser
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Creates a new ScaledShiftedUnitParser.
		/// </summary>
		/// <param name="aAbbreviatedUnitParser">The parser used for resolving the underlaying units.</param>
		public ScaledShiftedUnitParser(IUnitParser aAbbreviatedUnitParser)
		{
			_abbreviatedUnitParser = aAbbreviatedUnitParser ?? throw new ArgumentNullException("abbreviatedUnitParser");
		}


		#region -- IUnitParser implementation --

		/// <summary>
		///     Parses a unit string.
		/// </summary>
		/// <param name="aUnitStr">The string to parse.</param>
		/// <param name="aResultUnitAbbrev">
		///     The abbreviation of the returned unit. Will be ignored, if unitStr references a
		///     registered unit.
		/// </param>
		/// <param name="aResultUnitName">The name of the returned unit. Will be ignored, if unitStr references a registered unit.</param>
		/// <param name="aEnableException">Specifies whether exceptions should be thrown or null should be returned.</param>
		/// <returns>The parsed unit, or null if parsing failed and throwFormatException is false.</returns>
		public Unit ParseUnit(string aUnitStr, string aResultUnitAbbrev, string aResultUnitName, bool aEnableException)
		{
			var items = aUnitStr.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

			if (items.Length != 2)
			{
				return UnitParserExtension.ReturnNullOrThrowFormatException(null, aEnableException);
			}

			var unit = _abbreviatedUnitParser.ParseUnit(items[1], null, null, aEnableException);

			if (null == unit)
			{
				return null;
			}

			try
			{
				var factor = double.Parse(items[0], NumberStyles.Float, CultureInfo.InvariantCulture); //double azAZ
				return new ScaledShiftedUnit(aResultUnitAbbrev, aResultUnitName, unit, factor);
			}
			catch (FormatException ex)
			{
				return UnitParserExtension.ReturnNullOrThrowFormatException(null, ex, aEnableException);
			}
		}

		#endregion

		#endregion

		#region -- Data --

		private readonly IUnitParser _abbreviatedUnitParser;

		#endregion
	}
}

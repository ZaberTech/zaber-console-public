﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Zaber.Units
{
	/// <summary>
	///     Extension methods for unit registries.
	/// </summary>
	public static class UnitRegistryExtensions
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Find all known units, optionally filtering by dimension and optionally expanding SI prefixes.
		/// </summary>
		/// <param name="aRegistry">this.</param>
		/// <param name="aDimension">Optional. The dimension to search for.</param>
		/// <param name="aSiPrefixes">
		///     Optional. List of prefixes to apply to units that support
		///     SI prefixes. If you pass in <see cref="Prefix.All" /> it will generate all prefixed versions of
		///     the SI units it returns.
		/// </param>
		/// <returns>All registered units with the given dimension. Ordering is not guaranteed.</returns>
		public static IEnumerable<Unit> FindUnits(this IUnitRegistry aRegistry, Dimension aDimension = null,
												  IEnumerable<Prefix> aSiPrefixes = null)
		{
			Func<Unit, bool> filter = u => true;
			if (aDimension != null)
			{
				filter = u => aDimension == u.Dimension;
			}

			foreach (var unit in aRegistry.RegisteredUnits.Values.Where(filter))
			{
				if ((null != aSiPrefixes) && unit.UseSIPrefixes)
				{
					foreach (var prefix in aSiPrefixes)
					{
						var unitName = unit.Name;
						if (!string.IsNullOrEmpty(prefix.Name))
						{
							string prefixedUnitName = null;
							if (!string.IsNullOrEmpty(unitName))
							{
								// When prepending an SI scaling prefix to a unit name, change the
								// first letter of the unit name to lowercase (Kilo + Meters = Kilometers).
								// Note this is English specific and will need revisiting if we add
								// localization support.
								prefixedUnitName =
									prefix.Name + char.ToLowerInvariant(unitName[0]) + unitName.Substring(1);
							}

							yield return new ScaledShiftedUnit(prefix.Abbreviation + unit.Abbreviation,
															   prefixedUnitName,
															   unit,
															   prefix.Factor,
															   0.0) { UseSIPrefixes = false };
						}
						else
						{
							yield return unit;
						}
					}
				}
				else
				{
					yield return unit;
				}
			}
		}

		#endregion
	}
}

﻿// This is a derivative work of Henning Dieterichs's Units of Measure Library for .NET.
// See the accompanying README for details.

using System.Collections.Generic;

namespace Zaber.Units
{
	/// <summary>
	///     Interface for objects that contain collections of units.
	/// </summary>
	public interface IUnitRegistry
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Lookup table for units by abbreviation.
		/// </summary>
		IDictionary<string, Unit> RegisteredUnits { get; }

		#endregion
	}
}

﻿// This is a derivative work of Henning Dieterichs's Units of Measure Library for .NET.
// See the accompanying README for details.

using System;
using System.Collections.Generic;

namespace Zaber.Units
{
	/// <summary>
	///     A cached unit parser, which delegates the parsing and caches the result.
	/// </summary>
	public class CachedUnitParser : IUnitParser
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Creates a new cached unit parser.
		/// </summary>
		/// <param name="aUnderlyingUnitParser">The unit parser to delegate to.</param>
		public CachedUnitParser(IUnitParser aUnderlyingUnitParser)
		{
			_underlyingUnitParser = aUnderlyingUnitParser ?? throw new ArgumentNullException("underlyingUnitParser");
		}


		/// <summary>
		///     Clears the cache.
		/// </summary>
		public void ClearCache()
		{
			lock (_cache)
			{
				_cache.Clear();
			}
		}


		#region -- IUnitParser implementation --

		/// <summary>
		///     Parses a unit string.
		/// </summary>
		/// <param name="aUnitStr">The string to parse.</param>
		/// <param name="aResultUnitAbbrev">
		///     The abbreviation of the returned unit. Will be ignored, if unitStr references a
		///     registered unit.
		/// </param>
		/// <param name="aResultUnitName">The name of the returned unit. Will be ignored, if unitStr references a registered unit.</param>
		/// <param name="aEnableException">Specifies whether exceptions should be thrown or null should be returned.</param>
		/// <returns>The parsed unit, or null if parsing failed and throwFormatException is false.</returns>
		public Unit ParseUnit(string aUnitStr, string aResultUnitAbbrev, string aResultUnitName, bool aEnableException)
		{
			lock (_cache)
			{
				if (!_cache.TryGetValue(aUnitStr, out var result))
				{
					result = _underlyingUnitParser.ParseUnit(aUnitStr,
															 aResultUnitAbbrev,
															 aResultUnitName,
															 aEnableException);
					_cache[aUnitStr] = result;
				}

				if ((null == result) && aEnableException)
				{
					throw new FormatException($"Could not parse unit \"{aUnitStr}\"");
				}

				return result;
			}
		}

		#endregion

		#endregion

		#region -- Data --

		private readonly IUnitParser _underlyingUnitParser;
		private readonly Dictionary<string, Unit> _cache = new Dictionary<string, Unit>();

		#endregion
	}
}

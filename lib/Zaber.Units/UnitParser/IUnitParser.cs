// This is a derivative work of Henning Dieterichs's Units of Measure Library for .NET.
// See the accompanying README for details.

namespace Zaber.Units
{
	/// <summary>
	///     Interface for objects capable of parsing unit strings into unit instances.
	/// </summary>
	public interface IUnitParser
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Parses a unit string.
		/// </summary>
		/// <param name="aUnitStr">The string to parse.</param>
		/// <param name="aResultUnitAbbrev">
		///     The abbreviation of the returned unit. Will be ignored, if unitStr references a
		///     registered unit.
		/// </param>
		/// <param name="aResultUnitName">The name of the returned unit. Will be ignored, if unitStr references a registered unit.</param>
		/// <param name="aEnableException">Specifies whether exceptions should be thrown or null should be returned.</param>
		/// <returns>The parsed unit, or null if parsing failed and throwFormatException is false.</returns>
		Unit ParseUnit(string aUnitStr, string aResultUnitAbbrev, string aResultUnitName, bool aEnableException);

		#endregion
	}
}

﻿// This is a derivative work of Henning Dieterichs's Units of Measure Library for .NET.
// See the accompanying README for details.

using System;

namespace Zaber.Units
{
	/// <summary>
	///     Attribute that specifies the unit of measure abbreviation for the quantity it is associated with.
	/// </summary>
	[AttributeUsage(AttributeTargets.Field
				| AttributeTargets.Property
				| AttributeTargets.Parameter
				| AttributeTargets.ReturnValue)]
	public sealed class UnitAttribute : Attribute
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Attribute constructor. Initializes the unit abbreviation.
		/// </summary>
		/// <param name="aSymbol">The unit abbreviation, e.g. "m" or "Meter"</param>
		public UnitAttribute(string aSymbol)
		{
			Symbol = aSymbol;
		}


		/// <summary>
		///     The unit of measure abbreviation.
		/// </summary>
		public string Symbol { get; }

		#endregion
	}
}

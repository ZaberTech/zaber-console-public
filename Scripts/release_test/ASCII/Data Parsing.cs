#template(simple)

// This script tests behavior of current and legacy data parsing.
// Build warnings should be emitted about the use of the DataPacket.Data member.
// Run this script against a FW7 ASCII device that has a non-integer system.voltage value.

var response = Conversation.Request("get system.voltage");

Output.WriteLine("system.voltage = " + response.TextData);
Output.WriteLine("system.voltage parsed = " + response.NumericData.ToString());
Output.WriteLine("system.voltage legacy = " + response.Data.ToString());

if (response.NumericData == Math.Round(response.NumericData))
{
	throw new Exception("Sanity check failed: system.voltage looks like an integer on this device. Try again in a second?");
}

if (response.Data == 0)
{
	throw new Exception("Fail: value with decimal places should have been rounded.");
}

if (response.Data != (int)Math.Round(response.NumericData))
{
	throw new Exception("Fail: value with decimal places wasn't rounded to nearest integer.");
}

response = Conversation.Request("tools echo 1000000000000 -1000000000000 0.4 -0.4");

Output.Write("Echoed data:");
for (int i = 0; i < 4; i++)
{
	Output.Write(" " + response.TextDataValues[i]);
}
Output.WriteLine();

Output.Write("Echoed data parsed:");
for (int i = 0; i < 4; i++)
{
	Output.Write(" " + response.NumericDataValues[i].ToString());
}
Output.WriteLine();

Output.Write("Echoed data legacy:");
for (int i = 0; i < 4; i++)
{
	Output.Write(" " + response.DataValues[i].ToString());
}
Output.WriteLine();

if (response.NumericData != 1000000000000m)
{
	throw new Exception("Fail: Value larger than 32 bit integer not parsed correctly.");
}

if (response.Data != int.MaxValue)
{
	throw new Exception("Fail: Value larger than 32 bit integer should have been clamped.");
}

if (response.NumericDataValues.Count != 4)
{
	throw new Exception("Fail: Incorrect number of numeric values received.");
}

if (response.DataValues.Count != 4)
{
	throw new Exception("Fail: Incorrect number of legacy numeric values received.");
}

if ((response.NumericDataValues[1] != -1000000000000m) ||
	(response.NumericDataValues[2] != 0.4m) ||
	(response.NumericDataValues[3] != -0.4m))
{
	throw new Exception("Fail: Numeric values parsed incorrectly.");
}

if ((response.DataValues[1] != int.MinValue) ||
	(response.DataValues[2] != 0) ||
	(response.DataValues[2] != 0))
{
	throw new Exception("Fail: Negacy numeric values rounded or clamped incorrectly.");
}

Output.WriteLine("Test passed.");

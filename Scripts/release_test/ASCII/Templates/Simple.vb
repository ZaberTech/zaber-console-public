' This is the simplest template. It just expects a series of commands
' with conditional and looping logic.
' There are several properties and methods available from the 
' PlugInBase class. See the Zaber library's help file for full 
' details.
'
' Input & Output let you read and write data for the user to see.
' Log(message, exception) lets you write to the application log file.
' PortFacade gives access to all the devices and conversations. 
' Conversation is the conversation that was selected by the user.
' IsCanceled is set to true when the user cancels a script.
' Sleep(milliseconds) lets you pause the script
' CheckForCancellation() throws an exception if the user canceled
'     the script
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.IO
Imports Zaber
Imports Zaber.PlugIns

Public Class Simple
    Inherits PlugInBase

    Public Overrides Sub Run()
        ' $INSERT-SCRIPT-HERE$
    End Sub
End Class

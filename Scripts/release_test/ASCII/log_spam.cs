// The purpose of this script is to test script output log performance. 
// Once the number of message log lines specified in the Options tab 
// is reached, performance should be fairly consistent. Before lines were
// expired from the log, performance would continue to get worse as more
// text was output. It's not necessary to run this script to completion;
// just long enough to see the log update rate level off.

#template(simple)

for (int i = 0; i < 1000000; ++i)
{
	Output.WriteLine(string.Format("This is line {0}.", i));
	System.Threading.Thread.Sleep(1);
}

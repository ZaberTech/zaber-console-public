/* This file is a demonstration script that shows the basic features.
 * For a full explanation and more documentation, see the Zaber wiki:
 * http://www.zaber.com/wiki/Software/Zaber_Console
 */
/* The template declaration wraps the script in common code that's
 * the same for most scripts. See the Help tab for more info.
 */
#template(Simple)

// First check the current position and display it
int currentPosition = 
    Conversation.Request("get pos").Data; 
Output.WriteLine("Current position is {0} microsteps.", currentPosition);

// Ask user for instructions
Output.WriteLine("How many moves would you like to make?");
String line = Input.ReadLine();
int moveCount = Convert.ToInt32(line);

Output.WriteLine("How many microsteps would you like in each move?");
line = Input.ReadLine();
int distance = Convert.ToInt32(line);
int totalDistance = moveCount * distance;

// Check that there is room for the requested moves
if (totalDistance > currentPosition)
{
    // complain to user
    Output.WriteLine("Not enough room for that.");

    // skip the rest of the script
    return;
}

// Loop through all the requested moves
for (int i = 0; i < moveCount; i++)
{
    Conversation.Request("move rel", -distance);
    Conversation.PollUntilIdle();
    int newPosition = Conversation.Request("get pos").Data;
    Output.WriteLine("New position is {0} microsteps.", newPosition);
}
// Return to starting position
Conversation.Request("move rel", moveCount*distance);
Conversation.PollUntilIdle();
int finalPosition = Conversation.Request("get pos").Data;
Output.WriteLine("New position is {0} microsteps.", finalPosition);
/* This template doesn't include a method declaration, so
 * you can define more than one method in your script.
 * Remember: you must declare a Run method like this:
 * public override void Run()
 * The same properties and methods are available from the 
 * PlugInBase class:
 * Input, Output, Log(message, exception), PortFacade, Conversation, 
 * IsCanceled, Sleep(milliseconds), and CheckForCancellation().
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Zaber;
using Zaber.PlugIns;

namespace ScriptRunner.Templates
{
    class Methods : PlugInBase
    {
        // $INSERT-SCRIPT-HERE$
    }
}

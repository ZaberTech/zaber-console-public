/* 
 * This is a production script for test cycling on all connected devices
 * 
 * - This script is compatible with both T-Series and A-Series devices.
 * - This script is compatible with both Binary and ASCII protocol.
 * - This script can work on one or multiple devices
 *
 * The following steps are carried out:
 * 1. Ask user for speed
 * 2. ASCII: If an axis is idle, move axis
 * 3. Binary: Move all devices. When a reply indicates device has stopped, move device
 * 
 */
#template(methods)

public override void Run()
{
	// Check that port is open
	if(!PortFacade.IsOpen)
	{
		Output.WriteLine("Serial Port is not open.");
		return;
	}
	
	// Get desired speed from user
	uint? inputSpeed = GetSpeed();
	if(inputSpeed == null)
	{
		return;
	}
	int speed = (int)inputSpeed;
	
	if(PortFacade.Port.IsAsciiMode)
	{
		while(!IsCanceled)
		{
			foreach(Conversation conversation in PortFacade.AllConversations)
			{
				// only process individual axis
				if(!conversation.Device.IsSingleDevice
					|| conversation.Device.AxisNumber == 0)
				{
					continue;
				}
				
				// if axis is IDLE
				if(conversation.Request("").IsIdle)
				{
					int minpos = conversation.Request("get limit.min").Data;
					int pos = conversation.Request("get pos").Data;
					
					if(IsCanceled)
					{
						break;
					}
					
					// move axis in -ve direction if position is greater than minimum position, +ve otherwise
					if( pos > minpos )
					{
						Output.WriteLine("Moving device " + conversation.Device.DeviceNumber + " axis " + conversation.Device.AxisNumber + " -ve");
						conversation.Request("move vel", -speed);
					} else {
						Output.WriteLine("Moving device " + conversation.Device.DeviceNumber + " axis " + conversation.Device.AxisNumber + " +ve");
						conversation.Request("move vel", speed);
					}
				}
			}
		}
	}
	else
	{
		PortListener portListener = new PortListener(PortFacade.Port);
		
		try
		{
			PortFacade.GetConversation(0).Request(Command.MoveAtConstantSpeed, -speed);
			
			while(!IsCanceled)
			{
				DataPacket packet = (DataPacket)portListener.NextResponse();
				byte device = packet.DeviceNumber;
				Command command = packet.Command;
				int data = packet.Data;
				
				// if device has stopped
				if(command == Command.LimitActive
					|| command == Command.Home
					|| command == Command.Stop
					|| command == Command.UnexpectedPosition)
				{
					Conversation conversation = PortFacade.GetConversation(device);
					int maxpos = conversation.Request(Command.ReturnSetting, (int)Command.SetMaximumPosition).Data;
					int pos = conversation.Request(Command.ReturnCurrentPosition).Data;
					
					if(IsCanceled)
					{
						break;
					}
					
					// move device in +ve direction if position is less than maximum position, -ve otherwise
					if( pos < maxpos )
					{
						Output.WriteLine("Moving device " + conversation.Device.DeviceNumber + " +ve");
						conversation.Request(Command.MoveAtConstantSpeed, speed);
					} else {
						Output.WriteLine("Moving device " + conversation.Device.DeviceNumber + " -ve");
						conversation.Request(Command.MoveAtConstantSpeed, -speed);
					}
				}
			}
		}
		finally
		{
			portListener.Stop();
		}
	}
}

/*
 * Obtain speed from user
 *
 * Keep asking user until a valid number is given
 *
 * Returns null if:
 * - Script Editor is not open, or
 * - Input is empty
 */
public uint? GetSpeed()
{
	while(true)
	{
		Output.WriteLine("Input desired speed and press Enter, or press Enter to cancel: ");
		string input = Input.ReadLine();
		
		// Check that Script Editor is open
		if(input == null)
		{
			Output.WriteLine("Please run this script in Script Editor.");
			return null;
		}
		
		// If user simply hits Enter (with empty input), exit script.
		if(input.Length == 0)
		{
			Output.WriteLine("Cancelled.");
			return null;
		}
		
		// Try parsing number and return if successful
		uint number;
		if(UInt32.TryParse(input, out number))
		{
			Output.WriteLine("Selected speed: {0}", number);
			return number;	// success
		}
		else
		{
			Output.WriteLine("\"{0}\" is not a number.", input);
		}
	}
}

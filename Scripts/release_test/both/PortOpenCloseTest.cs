#template(methods)

// This script is a test to ensure port open/close operations used in 
// legacy production scripts still work as expected.

// DO NOT USE with a network-to-serial bridge! You cannot change the baud
// rate of the bridge so you will lose the connection.

// This is NOT a reference for best practices - in particular you no longer
// need to specify the port name to reopen the same port - pass in no argument for this case.

public override void Run()
{
	if (!PortFacade.Port.IsOpen)
	{
		Output.WriteLine("Port is closed; opening.");
		PortFacade.Port.Open(PortFacade.Port.PortName);
	}
	
	Output.WriteLine("Port name: " + PortFacade.Port.PortName);
	
	PrintPortStatus();
	
	for (int i = 0; i < 2; i++)
	{
		if (PortFacade.Port.IsAsciiMode)
		{
			Output.WriteLine("Switching to Binary.");
			PortFacade.Port.Send("/0 tools setcomm 9600 1");
			PortFacade.Close();
			Sleep(2000); // These delays are mainly to give the UI time to refresh.
			PortFacade.Port.IsAsciiMode = false;
			PortFacade.Port.BaudRate = 9600;
			PortFacade.Open(PortFacade.Port.PortName);
		}
		else
		{
			Output.WriteLine("Switching to ASCII.");
			PortFacade.Port.Send(0, Command.ConvertToAscii, 115200);
			PortFacade.Close();
			Sleep(2000);
			PortFacade.Port.IsAsciiMode = true;
			PortFacade.Port.BaudRate = 115200;
			PortFacade.Open(PortFacade.Port.PortName);
		}
		
		Sleep(2000);
		PrintPortStatus();
	}
	
	Output.WriteLine("Finished.");
}


private void PrintPortStatus()
{
	if (!PortFacade.Port.IsOpen)
	{
		Output.WriteLine("Port: Closed.");
	}
	else
	{
		Output.WriteLine(string.Format("Port: {0} {1}", PortFacade.Port.IsAsciiMode ? "ASCII" : "Binary", PortFacade.Port.BaudRate));
		Output.Write(string.Format("{0} devices found: ", PortFacade.Conversations.Count - 1));
		foreach (var conv in PortFacade.Conversations)
		{
			if (!(conv is ConversationCollection))
			{
				Output.Write(conv.Device.Description + " ");
			}
			
			Output.WriteLine();
		}
	}
}


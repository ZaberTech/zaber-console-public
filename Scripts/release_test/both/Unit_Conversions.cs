// Test for unit of measure features in the script API. Requires visual confirmation
// of correct movement distances. 

#template(methods)

private delegate void TestFunc(params string[] aArgs);

public override void Run()
{
	Output.WriteLine("Selected device supports these units of position:");
	foreach (var uom in Conversation.Device.GetUnitsOfMeasure(MeasurementType.Position))
	{
		Output.WriteLine(uom.Abbreviation);
	}
	Output.WriteLine();
	
	Output.WriteLine("Testing Conversation.RequestInUnits.");
	Output.WriteLine("Enter a distance with optional unit symbol separated by a space, or nothing to end this part.");
	TestLoop(TestRequestInUnits);
}


private void TestLoop(TestFunc aFunc)
{
	if (PortFacade.Port.IsAsciiMode)
	{
		Conversation.Send("home");
		Conversation.PollUntilIdle();
	}
	else
	{
		Conversation.Send(Command.Home, 0);
	}
	


	while (true)
	{
		var input = Input.ReadLine();
		if (string.IsNullOrEmpty(input))
		{
			break;
		}
		
		input = input.Trim();
		aFunc(input.Split(' '));
	}
}


private void TestRequestInUnits(params string[] aArgs)
{
	double distance = 0.0;
	try
	{
		distance = double.Parse(aArgs[0]);
	}
	catch (Exception aException)
	{
		Output.WriteLine(aException.Message);
		return;
	}
	
	string symbol = (aArgs.Length > 1) ? aArgs[1] : null;
	
	UnitOfMeasure uom = null;
	if (!string.IsNullOrEmpty(symbol))
	{
		uom = Conversation.Device.UnitConverter.FindUnitByAbbreviation(symbol);
	}
	
	if (null != uom)
	{
		Output.WriteLine(string.Format("Moving to {0} {1}.", distance, uom.Abbreviation));
		try
		{
			if (PortFacade.Port.IsAsciiMode)
			{
				Conversation.RequestInUnits("move abs", distance, uom);
				Conversation.PollUntilIdle();
			}
			else
			{
				Conversation.RequestInUnits(Command.MoveAbsolute, distance, uom);
			}
		}
		catch (Exception aException)
		{
			Output.WriteLine(aException.Message);
		}
	}
	else if (string.IsNullOrEmpty(symbol))
	{
		Output.WriteLine(string.Format("Moving to {0} microsteps.", distance));
		if (PortFacade.Port.IsAsciiMode)
		{
			Conversation.Request("move abs", (int)distance);
			Conversation.PollUntilIdle();
		}
		else
		{
			Conversation.Request(Command.MoveAbsolute, (int)distance);
		}
	}
	else
	{
		Output.WriteLine(string.Format("Could not identify unit symbol {0}.", symbol));
	}
}

/* This template doesn't include a method declaration, so
 * you can define more than one method in your script.
 * Remember: you must declare a Run method like this:
 * function Run()
 * The same properties and methods are available from the 
 * PlugInBase class:
 * Input, Output, Log(message, exception), PortFacade, Conversation, 
 * IsCanceled, Sleep(milliseconds), and CheckForCancellation().
 */
import System
import System.Collections.Generic
import System.Text
import System.IO
import Zaber
import Zaber.PlugIns

class Methods extends PlugInBase
{
    // $INSERT-SCRIPT-HERE$
}
﻿using System;
using System.Linq;
using NUnit.Framework;
using Zaber;
using Zaber.Testing;
using ZaberConsole;
using ZaberTest;
using ZaberTest.Testing;

namespace ZaberConsoleGUITests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class DevicePollerTests
	{
		[SetUp]
		public void Setup()
		{
			_dispatchTimer = new MockDispatcherTimer();
			DispatcherStack.Push(new ImmediateDispatcher());
			DispatcherTimerStack.Push(_dispatchTimer);

			_port = new MockPort();
			_port.IsAsciiMode = true;
			_port.Open("COM1");
			var device = new ZaberDevice();
			device.Port = _port;
			device.DeviceNumber = 1;
			device.DeviceType = DeviceTypeMother.CreateDeviceTypeWithCommands();
			device.DeviceType.FirmwareVersion = new FirmwareVersion(7, 1);
			_conversation = new Conversation(device);
		}


		[TearDown]
		public void Teardown()
		{
			Conversation.ResetMessageIdReservations();
			DispatcherTimerStack.Reset();
			DispatcherStack.Pop();
		}


		[Test]
		public void TestSingleDevice()
		{
			var poller = new DevicePoller(_conversation)
			{
				Command = "get pos",
				Interval = TimeSpan.FromMilliseconds(100),
				TimeoutTimer = new TimeoutTimer() {Timeout = 5000}
			};

			poller.PollResponseReceived += (aSender, aConversation, responses) =>
			{
				var resp = responses.ToArray();
				Assert.AreEqual(1, resp.Length);
				Assert.AreEqual(12345m, resp[0].NumericData);
				Assert.AreEqual(_conversation, aConversation);
				Assert.AreEqual(poller, aSender);
			};

			for (int i = 0; i < 3; i++)
			{
				_port.Expect("/01 0 get pos");
				_port.AddResponse("@01 0 OK IDLE -- 12345");
			}

			poller.Enabled = true;

			for (int i = 0; i < 3; i++)
			{
				_dispatchTimer.FireTickEvent();
				Assert.IsTrue(poller.WaitForPollComplete(TimeSpan.FromSeconds(20)));
			}

			_port.Verify();
		}



		[Test]
		public void TestMultiDevice()
		{
			var device2 = new ZaberDevice();
			device2.Port = _port;
			device2.DeviceNumber = 2;
			device2.DeviceType = DeviceTypeMother.CreateDeviceTypeWithCommands();
			var devices = new DeviceCollection();
			devices.DeviceType = new DeviceType();
			devices.Port = _port;
			devices.Add(_conversation.Device);
			devices.Add(device2);
			var convCollection = new ConversationCollection(devices);
			convCollection.Add(_conversation);
			convCollection.Add(new Conversation(device2));

			var poller = new DevicePoller(convCollection)
			{
				Command = "get pos",
				Interval = TimeSpan.FromMilliseconds(100),
				TimeoutTimer = new TimeoutTimer() { Timeout = 5000 }
			};

			poller.PollResponseReceived += (aSender, aConversation, responses) =>
			{
				var resp = responses.OrderBy(r => r.DeviceNumber).ToArray();
				Assert.AreEqual(2, resp.Length);
				Assert.AreEqual(12345m, resp[0].NumericData);
				Assert.AreEqual(67890m, resp[1].NumericData);
				Assert.AreEqual(convCollection, aConversation);
				Assert.AreEqual(poller, aSender);
			};

			_port.Expect("/00 0 get pos");
			_port.AddResponse("@01 0 OK IDLE -- 12345");
			_port.AddResponse("@02 0 OK IDLE -- 67890");

			poller.Enabled = true;

			_dispatchTimer.FireTickEvent();
			Assert.IsTrue(poller.WaitForPollComplete(TimeSpan.FromSeconds(20)));

			_port.Verify();
		}


		[Test]
		public void TestErrorResponse()
		{
			var poller = new DevicePoller(_conversation)
			{
				Command = "get pos",
				Interval = TimeSpan.FromMilliseconds(100),
				TimeoutTimer = new TimeoutTimer() { Timeout = 5000 }
			};

			poller.PollResponseReceived += (s, c, responses) =>
			{
				var resp = responses.ToArray();
				Assert.AreEqual(1, resp.Length);
				Assert.IsTrue(resp[0].IsError);
			};

			_port.Expect("/01 0 get pos");
			_port.AddResponse("@01 0 RJ IDLE -- BADCOMMAND");

			poller.Enabled = true;

			_dispatchTimer.FireTickEvent();
			Assert.IsTrue(poller.WaitForPollComplete(TimeSpan.FromSeconds(20)));

			_port.Verify();
		}


		[Test]
		public void TestMessageID()
		{
			_conversation.Device.AreAsciiMessageIdsEnabled = true;
			Conversation.ReserveMessageId(47);

			var poller = new DevicePoller(_conversation)
			{
				Command = "get pos",
				Interval = TimeSpan.FromMilliseconds(100),
				TimeoutTimer = new TimeoutTimer() { Timeout = 5000 },
				ReservedMessageId = 47
			};

			poller.PollResponseReceived += (s, c, responses) =>
			{
				var resp = responses.ToArray();
				Assert.AreEqual(1, resp.Length);
				Assert.AreEqual(9876m, resp[0].NumericData);
			};

			_port.Expect("/01 0 47 get pos");
			_port.AddResponse("@01 0 47 OK IDLE -- 9876");

			poller.Enabled = true;

			_dispatchTimer.FireTickEvent();
			Assert.IsTrue(poller.WaitForPollComplete(TimeSpan.FromSeconds(20)));

			_port.Verify();
		}



		[Test]
		public void TestManualOperation()
		{
			var poller = new DevicePoller(_conversation)
			{
				Command = "get pos",
			};

			poller.PollResponseReceived += (aSender, aConversation, responses) =>
			{
				var resp = responses.ToArray();
				Assert.AreEqual(1, resp.Length);
				Assert.AreEqual(12345m, resp[0].NumericData);
				Assert.AreEqual(_conversation, aConversation);
				Assert.AreEqual(poller, aSender);
			};

			_port.Expect("/01 0 get pos");
			_port.AddResponse("@01 0 OK IDLE -- 12345");

			poller.PollNow();
			Assert.IsTrue(poller.WaitForPollComplete(TimeSpan.FromSeconds(20)));

			_port.Verify();
		}


		private MockDispatcherTimer _dispatchTimer;
		private MockPort _port;
		private Conversation _conversation;
	}
}

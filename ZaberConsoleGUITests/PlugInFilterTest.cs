﻿using System;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;
using ZaberConsole.Plugins;

namespace ZaberConsoleTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class PlugInFilterTest
	{
		[Test]
		public void NoVersions()
		{
			// SETUP
			var folder = new DirectoryInfo("plugIns");
			var files = BuildFileInfos(folder, "foo.dll", "bar.dll");

			// EXEC
			var filter = new PlugInFilter(folder);
			var filteredFiles = filter.Filter(files);

			// VERIFY
			Assert.That(filteredFiles,
						Is.EquivalentTo(files).Using(fileComparison));
		}


		[Test]
		public void PlugInNameDifferentFolder()
		{
			var folder = new DirectoryInfo("plugIns");
			var filter = new PlugInFilter(folder);
			Assert.That(filter.GetPlugInName(new FileInfo("otherFolder/foo/bar.dll")),
						Is.EqualTo("bar"));
		}


		[Test]
		public void PlugInNameInvalidVersion()
		{
			var folder = new DirectoryInfo("plugIns");
			var filter = new PlugInFilter(folder);
			Assert.That(filter.GetPlugInName(new FileInfo("plugIns/foo-23bar.dll")),
						Is.EqualTo("foo-23bar"));
		}


		[Test]
		public void PlugInNameSimple()
		{
			var folder = new DirectoryInfo("plugIns");
			var filter = new PlugInFilter(folder);
			Assert.That(filter.GetPlugInName(new FileInfo("plugIns/foo.dll")),
						Is.EqualTo("foo"));
		}


		[Test]
		public void PlugInNameSubfolder()
		{
			var folder = new DirectoryInfo("plugIns");
			var filter = new PlugInFilter(folder);
			Assert.That(filter.GetPlugInName(new FileInfo("plugIns/foo/bar.dll")),
						Is.EqualTo(@"foo\bar"));
		}


		[Test]
		public void PlugInNameVersion()
		{
			var folder = new DirectoryInfo("plugIns");
			var filter = new PlugInFilter(folder);
			Assert.That(filter.GetPlugInName(new FileInfo("plugIns/foo-1.0.23.400.dll")),
						Is.EqualTo("foo"));
		}


		/// <summary>
		///     Can cause problems if we load another copy of the Zaber.dll
		///     library.
		/// </summary>
		[Test]
		public void SkipZaberLibrary()
		{
			var folder = new DirectoryInfo("plugIns");
			var files =
				BuildFileInfos(folder, "foo.dll", "zaBer.dll");
			var expectedFiles =
				BuildFileInfos(folder, "foo.dll");

			// EXEC
			var filter = new PlugInFilter(folder);
			var filteredFiles = filter.Filter(files);

			// VERIFY
			Assert.That(filteredFiles,
						Is.EquivalentTo(expectedFiles).Using(fileComparison),
						"filtered files");
		}


		[Test]
		public void VersionComparedToNoVersion()
		{
			// SETUP
			var folder = new DirectoryInfo("plugIns");
			var files =
				BuildFileInfos(folder, "foo-1.30.99.105.dll", "foo.dll");
			var expectedFiles =
				BuildFileInfos(folder, "foo-1.30.99.105.dll");

			// EXEC
			var filter = new PlugInFilter(folder);
			var filteredFiles = filter.Filter(files);

			// VERIFY
			Assert.That(filteredFiles,
						Is.EquivalentTo(expectedFiles).Using(fileComparison));
		}


		[Test]
		public void Versions()
		{
			// SETUP
			var folder = new DirectoryInfo("plugIns");
			var files =
				BuildFileInfos(folder, "foo-2.3.99.105.dll", "foo-1.0.12.55.dll");
			var expectedFiles =
				BuildFileInfos(folder, "foo-2.3.99.105.dll");

			// EXEC
			var filter = new PlugInFilter(folder);
			var filteredFiles = filter.Filter(files);

			// VERIFY
			Assert.That(filteredFiles,
						Is.EquivalentTo(expectedFiles).Using(fileComparison));
		}


		[Test]
		public void VersionsDifferentNumberOfDigits()
		{
			// SETUP
			var folder = new DirectoryInfo("plugIns");
			var files =
				BuildFileInfos(folder, "foo-1.30.99.105.dll", "foo-1.5.12.55.dll");
			var expectedFiles =
				BuildFileInfos(folder, "foo-1.30.99.105.dll");

			// EXEC
			var filter = new PlugInFilter(folder);
			var filteredFiles = filter.Filter(files);

			// VERIFY
			Assert.That(filteredFiles,
						Is.EquivalentTo(expectedFiles).Using(fileComparison));
		}


		[Test]
		public void VersionsDifferentNumberOfPiecesA()
		{
			// SETUP
			var folder = new DirectoryInfo("plugIns");
			var files =
				BuildFileInfos(folder, "foo-1.30.99.105.dll", "foo-1.30.dll");
			var expectedFiles =
				BuildFileInfos(folder, "foo-1.30.99.105.dll");

			// EXEC
			var filter = new PlugInFilter(folder);
			var filteredFiles = filter.Filter(files);

			// VERIFY
			Assert.That(filteredFiles,
						Is.EquivalentTo(expectedFiles).Using(fileComparison));
		}


		[Test]
		public void VersionsDifferentNumberOfPiecesB()
		{
			// SETUP
			var folder = new DirectoryInfo("plugIns");
			var files =
				BuildFileInfos(folder, "foo-1.30.dll", "foo-1.30.99.105.dll");
			var expectedFiles =
				BuildFileInfos(folder, "foo-1.30.99.105.dll");

			// EXEC
			var filter = new PlugInFilter(folder);
			var filteredFiles = filter.Filter(files);

			// VERIFY
			Assert.That(filteredFiles,
						Is.EquivalentTo(expectedFiles).Using(fileComparison));
		}


		private IEnumerable<FileInfo> BuildFileInfos(DirectoryInfo folder,
													 params string[] fileNames)
		{
			var files = new List<FileInfo>();
			foreach (var fileName in fileNames)
			{
				files.Add(new FileInfo(Path.Combine(folder.FullName, fileName)));
			}

			return files;
		}


		private readonly Comparison<FileInfo> fileComparison =
			delegate(FileInfo a, FileInfo b) { return a.FullName.CompareTo(b.FullName); };
	}
}

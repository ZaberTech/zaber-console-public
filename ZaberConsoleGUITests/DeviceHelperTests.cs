using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Zaber;
using ZaberConsole;
using ZaberConsole.Dialogs.Calibration;
using ZaberTest;

namespace ZaberConsoleGUITests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class DeviceHelperTests
	{
		[SetUp]
		public void Setup()
		{
			_callbacks = new List<Tuple<float, string, DeviceHelper.ReportType>>();
			_portFacade = CreatePort(true);
			_portFacade.Open("COM1");
		}


		[Test]
		public void TestReadAllCalibrationTables_Bootloader()
		{
			var conv = FindConversation(_portFacade, 30211);
			conv.Device.IsInBootloaderMode = true;

			var result = DeviceHelper.ReadAllCalibrationTables(conv, ReportCallback);
			Assert.IsTrue(0 == result.Count());
			CheckCallbackReceived("bootloader", DeviceHelper.ReportType.Error);
			CheckWorkCompleted(DeviceHelper.ReportType.Error);
		}


		[Test]
		public void TestReadAllCalibrationTables_CommandRejected()
		{
			var conv = FindConversation(_portFacade, 30211);

			AddExpectation(_portFacade, "/01 0 calibration list", "@01 0 RJ IDLE -- BADCOMMAND");
			AddExpectation(_portFacade, "/01 0 ", "@01 0 OK IDLE -- 0");
			AddExpectation(_portFacade, "/01 0 get calibration.type", "@01 0 RJ IDLE -- BADCOMMAND");

			var result = DeviceHelper.ReadAllCalibrationTables(conv, ReportCallback);
			Assert.IsTrue(0 == result.Count());
			CheckCallbackReceived("responded with an error", DeviceHelper.ReportType.Error);
			CheckWorkCompleted(DeviceHelper.ReportType.Error);
		}


		[Test]
		public void TestReadAllCalibrationTables_Garbled()
		{
			var conv = FindConversation(_portFacade, 30211);

			AddExpectation(_portFacade, "/01 0 calibration list", "@01 0 OK IDLE -- 0", "#01 0 serial flubadub");
			AddExpectation(_portFacade, "/01 0 ", "@01 0 OK IDLE -- 0");

			var result = DeviceHelper.ReadAllCalibrationTables(conv, ReportCallback).ToArray();
			Assert.IsTrue(0 == result.Count());
			CheckCallbackReceived("unexpected data", DeviceHelper.ReportType.Error);
			CheckWorkCompleted(DeviceHelper.ReportType.Error);
		}


		[Test]
		[TestCase(631)]
		[TestCase(711)]
		public void TestReadAllCalibrationTables_Integrated_Empty(int aFirmwareVersion)
		{
			var conv = FindConversation(_portFacade, 50081);
			conv.Device.DeviceType.FirmwareVersion = new FirmwareVersion(aFirmwareVersion);
			conv.Device.DeviceType.MotionType = MotionType.Linear;

			AddExpectation(_portFacade, "/03 0 calibration list", "@03 0 RJ IDLE -- 0");
			AddExpectation(_portFacade, "/03 0 ", "@03 0 OK IDLE -- 0");
			AddExpectation(_portFacade, "/03 0 get calibration.type", "@03 0 OK IDLE -- 0");

			var result = DeviceHelper.ReadAllCalibrationTables(conv, ReportCallback);
			Assert.IsTrue(0 == result.Count());
			CheckWorkCompleted();
		}


		[Test]
		[TestCase(631)]
		[TestCase(711)]
		public void TestReadAllCalibrationTables_Integrated_NonEmpty(int aFirmwareVersion)
		{
			var fwVersion = new FirmwareVersion(aFirmwareVersion);
			var conv = FindConversation(_portFacade, 50081);
			conv.Device.DeviceType.MotionType = MotionType.Linear;
			conv.Device.DeviceType.FirmwareVersion = fwVersion;

			AddExpectation(_portFacade, "/03 0 calibration list", "@03 0 RJ IDLE -- 0");
			AddExpectation(_portFacade, "/03 0 ", "@03 0 OK IDLE -- 0");
			AddExpectation(_portFacade, "/03 0 get calibration.type", "@03 0 OK IDLE -- 1");
			AddExpectation(_portFacade, "/03 0 get system.serial", "@03 0 OK IDLE -- 12345");
			AddCalibrationReadExpectations(_portFacade, 3, 0, fwVersion);

			var result = DeviceHelper.ReadAllCalibrationTables(conv, ReportCallback);
			Assert.IsTrue(1 == result.Count());
			CheckWorkCompleted();
			var table = result.First();
			Assert.AreEqual(12345, table.SerialNumber);
			CheckCalibrationData(table, 12345, 1, 9, (aFirmwareVersion >= 700) ? 17 : 0);
		}


		[Test]
		public void TestReadAllCalibrationTables_FW6_MultiAxisController_Empty()
		{
			var conv = FindConversation(_portFacade, 30222);
			Assert.AreEqual(6, conv.Device.DeviceType.FirmwareVersion.Major);

			AddExpectation(_portFacade, "/02 0 calibration list", "@02 0 OK IDLE -- 0");
			AddExpectation(_portFacade, "/02 0 ", "@02 0 OK IDLE -- 0");

			var result = DeviceHelper.ReadAllCalibrationTables(conv, ReportCallback);
			Assert.IsTrue(0 == result.Count());
			CheckWorkCompleted();
		}


		[Test]
		public void TestReadAllCalibrationTables_FW7_Controller()
		{
			var conv = FindConversation(_portFacade, 30222);
			conv.Device.DeviceType.FirmwareVersion = new FirmwareVersion(7, 11);

			AddExpectation(_portFacade, "/02 0 calibration list", "@02 0 RJ IDLE -- 0");
			AddExpectation(_portFacade, "/02 0 ", "@02 0 OK IDLE -- 0");
			AddExpectation(_portFacade, "/02 0 get calibration.type", "@02 0 OK IDLE -- 0");

			var result = DeviceHelper.ReadAllCalibrationTables(conv, ReportCallback);
			Assert.IsTrue(0 == result.Count());
			CheckWorkCompleted();
		}


		[Test]
		public void TestReadAllCalibrationTables_FW6_MultiAxisController_NonEmpty()
		{
			var conv = FindConversation(_portFacade, 30222);
			Assert.AreEqual(6, conv.Device.DeviceType.FirmwareVersion.Major);

			AddExpectation(_portFacade,
						   "/02 0 calibration list",
						   "@02 0 OK IDLE -- 0",
						   "#02 0 serial 23456",
						   "#02 0 serial 34567");
			AddExpectation(_portFacade, "/02 0 ", "@02 0 OK IDLE -- 0");
			AddCalibrationReadExpectations(_portFacade, 2, 0, new FirmwareVersion(6, 31), 23456);
			AddCalibrationReadExpectations(_portFacade, 2, 0, new FirmwareVersion(6, 31), 34567);

			var result = DeviceHelper.ReadAllCalibrationTables(conv, ReportCallback).ToArray();
			Assert.IsTrue(2 == result.Length);
			CheckWorkCompleted();
			Assert.AreEqual(23456, result[0].SerialNumber);
			Assert.AreEqual(34567, result[1].SerialNumber);
			CheckCalibrationData(result[0], 23456, 1, 9);
			CheckCalibrationData(result[1], 34567, 1, 9);
		}


		[Test]
		public void TestReadAllCalibrationTables_FW6_Axis()
		{
			var conv = FindConversation(_portFacade, 30211);
			Assert.AreEqual(6, conv.Device.DeviceType.FirmwareVersion.Major);
			conv.Device.DeviceType.MotionType = MotionType.Linear;
			conv.Device.AxisNumber = 1;

			AddExpectation(_portFacade, "/01 1 calibration list", "@01 1 RJ IDLE -- 0");
			AddExpectation(_portFacade, "/01 1 ", "@01 1 OK IDLE -- 0");
			AddExpectation(_portFacade, "/01 1 get calibration.type", "@01 1 OK IDLE -- 0");

			var result = DeviceHelper.ReadAllCalibrationTables(conv, ReportCallback);
			Assert.IsTrue(0 == result.Count());
			CheckWorkCompleted();
		}


		[Test]
		public void TestReadAllCalibrationTables_FW7_Axis_NotAutoDetect()
		{
			var conv = FindConversation(_portFacade, 30211);
			conv.Device.DeviceType.FirmwareVersion = new FirmwareVersion(7, 11);
			conv.Device.DeviceType.MotionType = MotionType.Linear;
			conv.Device.AxisNumber = 1;

			AddExpectation(_portFacade, "/01 1 calibration list", "@01 1 RJ IDLE -- 0");
			AddExpectation(_portFacade, "/01 1 ", "@01 1 OK IDLE -- 0");
			AddExpectation(_portFacade, "/01 1 get calibration.type", "@01 1 OK IDLE -- 2");
			AddCalibrationReadExpectations(_portFacade, 1, 1, conv.Device.DeviceType.FirmwareVersion);

			var result = DeviceHelper.ReadAllCalibrationTables(conv, ReportCallback).ToList();
			Assert.IsTrue(1 == result.Count);
			CheckCalibrationData(result[0], 1, 2, 9, 17);
			CheckWorkCompleted();
		}


		[Test]
		public void TestReadAllCalibrationTables_FW7_Axis_AutoDetect()
		{
			var conv = FindConversation(_portFacade, 30222).Axes[1];
			conv.Device.DeviceType.FirmwareVersion = new FirmwareVersion(7, 11);
			conv.Device.DeviceType.MotionType = MotionType.Linear;
			conv.Device.DeviceType.Capabilities.Add(Capability.AutoDetect);

			AddExpectation(_portFacade, "/02 2 calibration list", "@02 2 RJ IDLE -- 0");
			AddExpectation(_portFacade, "/02 2 ", "@02 2 OK IDLE -- 0");
			AddExpectation(_portFacade, "/02 2 get calibration.type", "@02 2 OK IDLE -- 2");
			AddExpectation(_portFacade, "/02 2 get peripheral.serial", "@02 2 OK IDLE -- 54321");
			AddCalibrationReadExpectations(_portFacade, 2, 2, conv.Device.DeviceType.FirmwareVersion);

			var result = DeviceHelper.ReadAllCalibrationTables(conv, ReportCallback).ToList();
			Assert.IsTrue(1 == result.Count);
			CheckCalibrationData(result[0], 54321, 2, 9, 17);
			CheckWorkCompleted();
		}


		[Test]
		public void TestReadAllCalibrationTables_NotSingleDevice()
		{
			var conv = _portFacade.Conversations.First();
			Assert.IsTrue(conv is ConversationCollection);
			var result = DeviceHelper.ReadAllCalibrationTables(conv, ReportCallback);
			Assert.IsTrue(0 == result.Count());
			CheckCallbackReceived("All Devices", DeviceHelper.ReportType.Error);
		}


		[Test]
		public void TestReadAllCalibrationTables_NullConversation()
		{
			var result = DeviceHelper.ReadAllCalibrationTables(null, ReportCallback);
			Assert.IsTrue(0 == result.Count());
			CheckCallbackReceived("No device selected", DeviceHelper.ReportType.Error);
			CheckWorkCompleted(DeviceHelper.ReportType.Error);
		}


		[Test]
		public void TestReadAllCalibrationTables_OldFirmware()
		{
			var conv = FindConversation(_portFacade, 30211);
			conv.Device.DeviceType.FirmwareVersion = new FirmwareVersion(6, 21);

			var result = DeviceHelper.ReadAllCalibrationTables(conv, ReportCallback);
			Assert.IsTrue(0 == result.Count());
			CheckCallbackReceived("does not support", DeviceHelper.ReportType.Error);
			CheckWorkCompleted(DeviceHelper.ReportType.Error);
		}


		[Test]
		public void TestReadAllCalibrationTables_FW6_SingleAxisController_Empty()
		{
			var conv = FindConversation(_portFacade, 30211);
			Assert.AreEqual(6, conv.Device.DeviceType.FirmwareVersion.Major);

			AddExpectation(_portFacade, "/01 0 calibration list", "@01 0 OK IDLE -- 0");
			AddExpectation(_portFacade, "/01 0 ", "@01 0 OK IDLE -- 0");

			var result = DeviceHelper.ReadAllCalibrationTables(conv, ReportCallback);
			Assert.IsTrue(0 == result.Count());
			CheckWorkCompleted();
		}


		[Test]
		public void TestReadAllCalibrationTables_FW6_SingleAxisController_NonEmpty()
		{
			var conv = FindConversation(_portFacade, 30211);
			Assert.AreEqual(6, conv.Device.DeviceType.FirmwareVersion.Major);

			AddExpectation(_portFacade, "/01 0 calibration list", "@01 0 OK IDLE -- 0", "#01 0 serial 23456");
			AddExpectation(_portFacade, "/01 0 ", "@01 0 OK IDLE -- 0");
			AddCalibrationReadExpectations(_portFacade, 1, 0, new FirmwareVersion(6, 31), 23456);

			var result = DeviceHelper.ReadAllCalibrationTables(conv, ReportCallback).ToArray();
			Assert.IsTrue(1 == result.Length);
			CheckWorkCompleted();
			Assert.AreEqual(23456, result[0].SerialNumber);
			CheckCalibrationData(result[0], 23456, 1, 9);
		}


		[Test]
		[Description("Test that reading calibration from a device in bootloader mode produces expected messages.")]
		public void TestReadCalibrationTable_Bootloader()
		{
			var conv = FindConversation(_portFacade, 30211);
			conv.Device.IsInBootloaderMode = true;

			var result = DeviceHelper.ReadCalibrationTable(conv, 0, ReportCallback);
			Assert.IsNull(result);
			CheckCallbackReceived("bootloader", DeviceHelper.ReportType.Error);
			CheckWorkCompleted(DeviceHelper.ReportType.Error);
		}


		[Test]
		[Description("Test that rejected commands are reported when reading calibration data.")]
		public void TestReadCalibrationTable_CommandRejected()
		{
			var conv = FindConversation(_portFacade, 30211);

			AddExpectation(_portFacade, "/01 0 calibration print 0", "@01 0 RJ IDLE -- BADCOMMAND");
			AddExpectation(_portFacade, "/01 0 ", "@01 0 OK IDLE -- 0");

			var result = DeviceHelper.ReadCalibrationTable(conv, 0, ReportCallback);
			Assert.IsNull(result);
			CheckCallbackReceived("Error while reading calibration data", DeviceHelper.ReportType.Error);
			CheckWorkCompleted(DeviceHelper.ReportType.Error);
		}


		[Test]
		[Description("Test normal calibration data read from a FW6 integrated device.")]
		public void TestReadCalibrationTable_Integrated_FW6()
		{
			var conv = FindConversation(_portFacade, 50081);
			AddCalibrationReadExpectations(_portFacade, 3, 0, conv.Device.FirmwareVersion);

			var result = DeviceHelper.ReadCalibrationTable(conv, null, ReportCallback);
			CheckWorkCompleted();
			Assert.IsNotNull(result);
			CheckCalibrationData(result, 0, 1, 9);
		}


		[Test]
		[Description("Test normal calibration data read from a FW7 integrated device.")]
		public void TestReadCalibrationTable_Integrated_FW7()
		{
			var conv = FindConversation(_portFacade, 50081);
			conv.Device.DeviceType.FirmwareVersion = new FirmwareVersion(7, 1);

			AddCalibrationReadExpectations(_portFacade, 3, 0, conv.Device.FirmwareVersion);

			var result = DeviceHelper.ReadCalibrationTable(conv, null, ReportCallback);
			CheckWorkCompleted();
			Assert.IsNotNull(result);
			CheckCalibrationData(result, 0, 1, 9, 17);
		}


		[Test]
		[Description("Test that reading calibration data from a FW6 peripheral produces expected messages.")]
		public void TestReadCalibrationTable_FW6_Axis()
		{
			var conv = FindConversation(_portFacade, 30211);
			conv.Device.DeviceType.MotionType = MotionType.Linear;
			conv.Device.AxisNumber = 1;

			AddExpectation(_portFacade, "/01 1 calibration print 0", "@01 1 RJ IDLE -- BADCOMMAND");
			AddExpectation(_portFacade, "/01 1 ", "@01 1 OK IDLE -- 0");

			var result = DeviceHelper.ReadCalibrationTable(conv, 0, ReportCallback);
			Assert.IsNull(result);
			CheckCallbackReceived("Error while reading", DeviceHelper.ReportType.Error);
			CheckWorkCompleted(DeviceHelper.ReportType.Error);
		}


		[Test]
		[Description("Test that reading calibration data from a FW7 controller produces expected results.")]
		public void TestReadCalibrationTable_FW7_Controller()
		{
			var conv = FindConversation(_portFacade, 30211);
			conv.Device.DeviceType.MotionType = MotionType.None;
			conv.Device.DeviceType.FirmwareVersion = new FirmwareVersion(7, 11);

			AddExpectation(_portFacade, "/01 0 calibration print", "@01 0 OK IDLE -- 0");
			AddExpectation(_portFacade, "/01 0 ", "@01 0 OK IDLE -- 0");

			var result = DeviceHelper.ReadCalibrationTable(conv, null, ReportCallback);
			Assert.IsNull(result);
			CheckWorkCompleted();
		}


		[Test]
		[Description("Test that reading calibration data from a FW7 peripheral produces expected results.")]
		public void TestReadCalibrationTable_FW7_Axis_Empty()
		{
			var conv = FindConversation(_portFacade, 30211);
			conv.Device.DeviceType.MotionType = MotionType.Linear;
			conv.Device.AxisNumber = 1;
			conv.Device.DeviceType.FirmwareVersion = new FirmwareVersion(7, 11);

			AddExpectation(_portFacade, "/01 1 calibration print", "@01 1 OK IDLE -- 0");
			AddExpectation(_portFacade, "/01 1 ", "@01 1 OK IDLE -- 0");

			var result = DeviceHelper.ReadCalibrationTable(conv, null, ReportCallback);
			Assert.IsNull(result);
			CheckWorkCompleted();
		}


		[Test]
		[Description("Test that reading calibration data from a FW7 peripheral produces expected results.")]
		public void TestReadCalibrationTable_FW7_Axis()
		{
			var conv = FindConversation(_portFacade, 30211);
			conv.Device.DeviceType.MotionType = MotionType.Linear;
			conv.Device.AxisNumber = 1;
			conv.Device.DeviceType.FirmwareVersion = new FirmwareVersion(7, 11);

			AddCalibrationReadExpectations(_portFacade, 1, 1, new FirmwareVersion(7, 11));

			var result = DeviceHelper.ReadCalibrationTable(conv, null, ReportCallback);
			Assert.IsNotNull(result);
			CheckCalibrationData(result, 0, 1, 9, 17);
			CheckWorkCompleted();
		}


		[Test]
		[Description("Test that reading calibration data from a multi-axis device produces expected messages.")]
		public void TestReadCalibrationTable_NotSingleDevice()
		{
			var conv = _portFacade.Conversations.First();
			Assert.IsTrue(conv is ConversationCollection);

			var result = DeviceHelper.ReadCalibrationTable(conv, 0, ReportCallback);
			Assert.IsNull(result);
			CheckCallbackReceived("All Devices", DeviceHelper.ReportType.Error);
			CheckWorkCompleted(DeviceHelper.ReportType.Error);
		}


		[Test]
		[Description("Test that reading calibration without a conversation selected produces expected messages.")]
		public void TestReadCalibrationTable_NullConversation()
		{
			var result = DeviceHelper.ReadCalibrationTable(null, 0, ReportCallback);
			Assert.IsNull(result);
			CheckCallbackReceived("No device selected", DeviceHelper.ReportType.Error);
			CheckWorkCompleted(DeviceHelper.ReportType.Error);
		}


		[Test]
		[Description(
			"Test that reading calibration from a device that doesn't support calibration produces expected messages and output.")]
		public void TestReadCalibrationTable_PreCalibration()
		{
			var conv = FindConversation(_portFacade, 30211);
			conv.Device.DeviceType.FirmwareVersion = new FirmwareVersion(6, 21);

			var result = DeviceHelper.ReadCalibrationTable(conv, 0, ReportCallback);
			Assert.IsNull(result);
			CheckCallbackReceived("does not support", DeviceHelper.ReportType.Error);
			CheckWorkCompleted(DeviceHelper.ReportType.Error);
		}


		[Test]
		[Description("Test normal calibration data read from a FW6 controller.")]
		public void TestReadCalibrationTable_FW6_Controller()
		{
			var conv = FindConversation(_portFacade, 30211);

			AddCalibrationReadExpectations(_portFacade, 1, 0, conv.Device.FirmwareVersion, 12345);

			var result = DeviceHelper.ReadCalibrationTable(conv, 12345, ReportCallback);
			CheckWorkCompleted();
			Assert.IsNotNull(result);
			CheckCalibrationData(result, 12345, 1, 9);
		}


		[Test]
		[Description("Test that writing calibration data with a bad data version produces expected messages.")]
		public void TestWriteCalibrationTable_BadDataVersion()
		{
			var conv = FindConversation(_portFacade, 30211);
			var table = CreateTestTable(12345, 1, 9);
			table.DataVersion = 0;

			DeviceHelper.WriteCalibrationTable(conv, table, ReportCallback);
			CheckCallbackReceived("table data version", DeviceHelper.ReportType.Error);
			CheckWorkCompleted(DeviceHelper.ReportType.Error);
		}


		[Test]
		[Description("Test that writing calibration to a device in bootloader mode produces expected messages.")]
		public void TestWriteCalibrationTable_Bootloader()
		{
			var conv = FindConversation(_portFacade, 30211);
			conv.Device.IsInBootloaderMode = true;
			var table = CreateTestTable(12345, 1, 9);

			DeviceHelper.WriteCalibrationTable(conv, table, ReportCallback);
			CheckCallbackReceived("bootloader", DeviceHelper.ReportType.Error);
			CheckWorkCompleted(DeviceHelper.ReportType.Error);
		}


		[Test]
		[Description("Test that rejection of a command during calibration write is properly reported.")]
		public void TestWriteCalibrationTable_CommandRejected()
		{
			var conv = FindConversation(_portFacade, 30211);
			var table = CreateTestTable(12345, 1, 9);
			conv.Device.DeviceType.MotionType = MotionType.None;

			// The non-protocol-compliant response is to trip the catch-all case in the helper's exception handler.
			AddExpectation(_portFacade, "/01 0 calibration load start 12345 1 9", "@01 0 RJ IDLE -- BADUSER");

			DeviceHelper.WriteCalibrationTable(conv, table, ReportCallback);
			CheckCallbackReceived("Error while transferring", DeviceHelper.ReportType.Error);
			CheckWorkCompleted(DeviceHelper.ReportType.Error);
		}


		[Test]
		[Description("Test that writing calibration to a full device produces expected messages.")]
		public void TestWriteCalibrationTable_DeviceFull()
		{
			var conv = FindConversation(_portFacade, 30211);
			var table = CreateTestTable(12345, 1, 9);

			AddExpectation(_portFacade, "/01 0 calibration load start 12345 1 9", "@01 0 RJ IDLE -- FULL");

			DeviceHelper.WriteCalibrationTable(conv, table, ReportCallback);
			CheckCallbackReceived("no more room", DeviceHelper.ReportType.Warning);
			CheckWorkCompleted(DeviceHelper.ReportType.Warning);
		}


		[Test]
		[TestCase(631)]
		[TestCase(711)]
		[Description("Test normal calibration write to an integrated device.")]
		public void TestWriteCalibrationTable_Integrated(int aFirmwareVersion)
		{
			var conv = FindConversation(_portFacade, 50081);
			conv.Device.DeviceType.FirmwareVersion = new FirmwareVersion(aFirmwareVersion);
			var table = CreateTestTable(12345, 1, 9, (aFirmwareVersion >= 700) ? 17 : 0);

			AddCalibrationWriteExpectations(_portFacade, conv, true, false, table);
			DeviceHelper.WriteCalibrationTable(conv, table, ReportCallback);
			CheckWorkCompleted();
		}


		[Test]
		[Description("Test that writing calibration to a peripheral produces expected messages.")]
		public void TestWriteCalibrationTable_NotController()
		{
			var conv = FindConversation(_portFacade, 30211);
			conv.Device.DeviceType.MotionType = MotionType.Linear;
			conv.Device.AxisNumber = 1;
			var table = CreateTestTable(12345, 1, 9);

			DeviceHelper.WriteCalibrationTable(conv, table, ReportCallback);
			CheckCallbackReceived("not supported on peripherals", DeviceHelper.ReportType.Warning);
			CheckWorkCompleted(DeviceHelper.ReportType.Warning);
		}


		[Test]
		[Description("Test that writing calibration to a multi-axis device produces expected messages.")]
		public void TestWriteCalibrationTable_NotSingleDevice()
		{
			var conv = _portFacade.Conversations.First();
			Assert.IsTrue(conv is ConversationCollection);
			var table = CreateTestTable(12345, 1, 9);

			DeviceHelper.WriteCalibrationTable(conv, table, ReportCallback);
			CheckCallbackReceived("All Devices", DeviceHelper.ReportType.Error);
			CheckWorkCompleted(DeviceHelper.ReportType.Error);
		}


		[Test]
		[Description("Test that writing calibration to a null conversation produces expected messages.")]
		public void TestWriteCalibrationTable_NullConversation()
		{
			DeviceHelper.WriteCalibrationTable(null, new CalibrationTableVM(), ReportCallback);
			CheckCallbackReceived("No device selected", DeviceHelper.ReportType.Error);
			CheckWorkCompleted(DeviceHelper.ReportType.Error);
		}


		[Test]
		[Description("Test that writing a null calibration table produces expected messages.")]
		public void TestWriteCalibrationTable_NullTable()
		{
			var conv = FindConversation(_portFacade, 30211);

			DeviceHelper.WriteCalibrationTable(conv, null, ReportCallback);
			CheckCallbackReceived("null calibration table", DeviceHelper.ReportType.Error);
			CheckWorkCompleted(DeviceHelper.ReportType.Error);
		}


		[Test]
		[Description("Test that writing calibration to a device that doesn't support it produces expected messages.")]
		public void TestWriteCalibrationTable_PreCalibration()
		{
			var conv = FindConversation(_portFacade, 30211);
			conv.Device.DeviceType.FirmwareVersion = new FirmwareVersion(6, 21);
			var table = CreateTestTable(12345, 1, 9);

			DeviceHelper.WriteCalibrationTable(conv, table, ReportCallback);
			CheckCallbackReceived("does not support", DeviceHelper.ReportType.Error);
			CheckWorkCompleted(DeviceHelper.ReportType.Error);
		}


		[Test]
		[TestCase(631)]
		[TestCase(711)]
		[Description("Test normal calibration write to a controller.")]
		public void TestWriteCalibrationTable_Controller(int aFirmwareVersion)
		{
			var conv = FindConversation(_portFacade, 30211);
			conv.Device.DeviceType.FirmwareVersion = new FirmwareVersion(aFirmwareVersion);
			var table = CreateTestTable(12345, 1, 9, (aFirmwareVersion  >= 700) ? 17 : 0);

			AddCalibrationWriteExpectations(_portFacade, conv, false, false, table);
			DeviceHelper.WriteCalibrationTable(conv, table, ReportCallback);
			CheckWorkCompleted();
		}


		[Test]
		public void ListCalibratedAxes()
		{
			var conv = FindConversation(_portFacade, 30211);

			AddExpectation(_portFacade, "/01 0 get calibration.type", "@01 0 OK IDLE -- 0 0 NA 1");

			var result = DeviceHelper.ListCalibratedAxes(conv).ToList();
			Assert.AreEqual(1, result.Count);
			Assert.AreEqual(4, result.First());
		}


		[Test]
		public void ListCalibratedAxes_Rejected()
		{
			var conv = FindConversation(_portFacade, 30211);

			AddExpectation(_portFacade, "/01 0 get calibration.type", "@01 0 RJ IDLE -- 0");

			var result = DeviceHelper.ListCalibratedAxes(conv).ToList();
			Assert.IsTrue(0 == result.Count);
		}


		private void ReportCallback(float aProgress, string aMessage, DeviceHelper.ReportType aReportType)
			=> _callbacks.Add(new Tuple<float, string, DeviceHelper.ReportType>(aProgress, aMessage, aReportType));


		private void CheckWorkCompleted(DeviceHelper.ReportType aReportType = DeviceHelper.ReportType.Info)
		{
			var lastReport = _callbacks.LastOrDefault();
			Assert.IsNotNull(lastReport);
			Assert.AreEqual(1.0f, lastReport.Item1);

			if ((DeviceHelper.ReportType.Error != aReportType) && (DeviceHelper.ReportType.Error == lastReport.Item3))
			{
				Assert.Fail("Unexpected error report: " + lastReport.Item2);
			}

			Assert.AreEqual(aReportType, lastReport.Item3);
		}


		private void CheckCallbackReceived(string aMessagePart, DeviceHelper.ReportType aReportType)
		{
			var found = false;

			foreach (var report in _callbacks)
			{
				if (aReportType == report.Item3)
				{
					if (report.Item2.Contains(aMessagePart))
					{
						found = true;
						break;
					}
				}
			}

			Assert.IsTrue(found,
						  $"An expected {aReportType} callback was not made. Expected message = '{aMessagePart}'");
		}


		private static void AddCalibrationReadExpectations(ZaberPortFacade aPortFacade, int aDeviceNum, int aAxisNum, 
														   FirmwareVersion aVersion, int? aSerialNumber = null)
		{
			var num = string.Format("{0:00}", aDeviceNum);
			var command = $"/{num} {aAxisNum} calibration print";
			if (aSerialNumber.HasValue)
			{
				command += " " + aSerialNumber.Value;
			}

			if (aVersion.Major < 7)
			{
				AddExpectation(aPortFacade,
							   command,
							   $"@{num} {aAxisNum} OK IDLE -- 1 9",
							   $"#{num} {aAxisNum} data 0 1 2 3",
							   $"#{num} {aAxisNum} data 4 5 6 7",
							   $"#{num} {aAxisNum} data 8 9 10 11",
							   $"#{num} {aAxisNum} data 12 13 14 15",
							   $"#{num} {aAxisNum} data 16 17 18 19",
							   $"#{num} {aAxisNum} data 20 21 22 23",
							   $"#{num} {aAxisNum} data 24 25 26 27",
							   $"#{num} {aAxisNum} data 28 29 30 31",
							   $"#{num} {aAxisNum} data 32 33 34 35",
							   $"#{num} {aAxisNum} data 36 37 38 39",
							   $"#{num} {aAxisNum} data 40 41 42 43",
							   $"#{num} {aAxisNum} data 44 45 46 47",
							   $"#{num} {aAxisNum} data 48 49 50 51",
							   $"#{num} {aAxisNum} data 52 53 54 55",
							   $"#{num} {aAxisNum} data 56 57 58 59",
							   $"#{num} {aAxisNum} data 60 61 32767 -32768"); // Large values express valid data range.
			}
			else
			{
				AddExpectation(aPortFacade,
							   command,
							   $"@{num} {aAxisNum} OK IDLE -- 1 9 17",
							   $"#{num} {aAxisNum} data 0 1 2 3",
							   $"#{num} {aAxisNum} data 4 5 6 7",
							   $"#{num} {aAxisNum} data 8 9 10 11",
							   $"#{num} {aAxisNum} data 12 13 14 15",
							   $"#{num} {aAxisNum} data 16 17 18 19",
							   $"#{num} {aAxisNum} data 20 21 22 23",
							   $"#{num} {aAxisNum} data 24 25 26 27",
							   $"#{num} {aAxisNum} data 28 29 30 31",
							   $"#{num} {aAxisNum} data 32 33 34 35",
							   $"#{num} {aAxisNum} data 36 37 38 39",
							   $"#{num} {aAxisNum} data 40 41 42 43",
							   $"#{num} {aAxisNum} data 44 45 46 47",
							   $"#{num} {aAxisNum} data 48 49 50 51",
							   $"#{num} {aAxisNum} data 52 53 54 55",
							   $"#{num} {aAxisNum} data 56 57 58 59",
							   $"#{num} {aAxisNum} data 60 61 536870911 -536870912");
			}

			AddExpectation(aPortFacade, $"/{num} {aAxisNum} ", $"@{aDeviceNum:00} {aAxisNum} OK IDLE -- 0");
		}


		private static void CheckCalibrationData(CalibrationTableVM aTable, uint aSerialNum, int aType, int aWidth,
												 int aOffset = 0)
		{
			Assert.AreEqual(aSerialNum, aTable.SerialNumber);
			Assert.AreEqual(aType, aTable.CalibrationType);
			Assert.AreEqual(1, aTable.DataVersion);
			Assert.AreEqual(DateTime.Now.DayOfYear, aTable.TimeStamp.DayOfYear);
			Assert.AreEqual(aWidth, aTable.TableWidth);
			Assert.AreEqual(aOffset, aTable.TableOffset);
			Assert.LessOrEqual(64, aTable.Data.Count);

			var expectedData = GenerateTestData(aOffset == 0 ? 6 : 7).ToArray();
			for (var i = 0; i < aTable.Data.Count; i++)
			{
				Assert.AreEqual(expectedData[i], aTable.Data[i]);
			}
		}


		private static CalibrationTableVM CreateTestTable(uint aSerialNum, int aType, int aWidth, int aOffset = 0)
		{
			var t = new CalibrationTableVM();

			t.SerialNumber = aSerialNum;
			t.CalibrationType = aType;
			t.TableWidth = aWidth;
			t.TableOffset = aOffset;

			foreach (var d in GenerateTestData(aOffset == 0 ? 6 : 7))
			{
				t.Data.Add(d);
			}

			return t;
		}


		private static IEnumerable<int> GenerateTestData(int aFirmwareMajorVersion)
		{
			for (var i = 0; i < 62; ++i)
			{
				yield return i;
			}

			if (aFirmwareMajorVersion < 7)
			{
				yield return short.MaxValue;
				yield return short.MinValue;
			}
			else
			{
				yield return (1 << 29) - 1;
				yield return -(1 << 29);
			}
		}


		private static void AddCalibrationWriteExpectations(ZaberPortFacade aPortFacade, Conversation aConversation,
															bool aIsIntegrated, bool aAddWpFlagAtEnd,
															CalibrationTableVM aTable)
		{
			var num = string.Format("{0:00}", aConversation.Device.DeviceNumber);
			var axis = aConversation.Device.AxisNumber;

			var command = $"/{num} {axis} calibration load start";
			if (!aIsIntegrated && (aConversation.Device.FirmwareVersion < 700))
			{
				command += " " + aTable.SerialNumber;
			}

			command += $" {aTable.CalibrationType} {aTable.TableWidth}";

			if (aTable.TableOffset != 0) // FW7
			{
				command += $" {aTable.TableOffset}";
			}

			AddExpectation(aPortFacade, command, $"@{num} {axis} OK IDLE -- 64");
			for (var i = 0; i < 64; i += 4)
			{
				command =
					$"/{num} {axis} calibration load {aTable.Data[i]} {aTable.Data[i + 1]} {aTable.Data[i + 2]} {aTable.Data[i + 3]}";
				var left = 60 - i;
				var leftStr = left.ToString();
				var flags = WarningFlags.None;
				if (0 == left)
				{
					leftStr = "DONE";
					if (aAddWpFlagAtEnd)
					{
						flags = WarningFlags.InvalidCalibrationType;
					}
				}

				AddExpectation(aPortFacade, command, $"@{num} {axis} OK IDLE {flags} {leftStr}");
			}
		}


		private static void AddExpectation(ZaberPortFacade aPortFacade, string aExpect, params string[] aResponses)
			=> AddExpectation(aPortFacade.Port as MockPort, aExpect, aResponses);


		private static void AddExpectation(MockPort aPort, string aExpect, params string[] aResponses)
		{
			aPort.Expect(aExpect);
			foreach (var response in aResponses)
			{
				aPort.AddResponse(response);
			}
		}


		private static Conversation FindConversation(ZaberPortFacade aPortFacade, int aDeviceId)
		{
			foreach (var conv in aPortFacade.Conversations)
			{
				if (aDeviceId == conv.Device.DeviceType.DeviceId)
				{
					return conv;
				}
			}

			return null;
		}


		// TODO centralize this to avoid code duplication.
		private static ZaberPortFacade CreatePort(bool aAsciiMode)
		{
			var mockPort = new MockPort();
			mockPort.IsAsciiMode = aAsciiMode;

			var defaultDeviceType = new DeviceType { Commands = new List<CommandInfo>() };

			var portFacade = new ZaberPortFacade
			{
				DefaultDeviceType = defaultDeviceType,
				Port = mockPort,
				QueryTimeout = 100
			};

			var peripheral0 = new DeviceType
			{
				PeripheralId = 0,
				Commands = new List<CommandInfo>()
			};

			var peripheral1 = new DeviceType
			{
				PeripheralId = 1,
				Commands = new List<CommandInfo>()
				{
					new SettingInfo { TextCommand = "peripheral.serial", IsAxisCommand = true }
				}
			};

			var XMCB1 = new DeviceType
			{
				DeviceId = 30211,
				FirmwareVersion = new FirmwareVersion(6, 22),
				Name = "X-MCB1 Controller",
				MotionType = MotionType.None,
				PeripheralMap = new Dictionary<int, DeviceType> { { 0, peripheral0 } }
			};

			var XMCB2 = new DeviceType
			{
				DeviceId = 30222,
				FirmwareVersion = new FirmwareVersion(6, 22),
				Name = "X-MCB2 Controller",
				MotionType = MotionType.None,
				PeripheralMap = new Dictionary<int, DeviceType>
				{
					{ 0, peripheral0 },
					{ 1, peripheral1 }
				}
			};

			var LHM = new DeviceType
			{
				DeviceId = 50081,
				FirmwareVersion = new FirmwareVersion(6, 22),
				PeripheralMap = new Dictionary<int, DeviceType>(),
				Name = "X-LHM100A Stage",
				MotionType = MotionType.Linear
			};

			portFacade.AddDeviceType(XMCB1);
			portFacade.AddDeviceType(XMCB2);
			portFacade.AddDeviceType(LHM);

			ZaberPortFacadeTest.SetAsciiExpectations(portFacade, mockPort, XMCB1, XMCB2, LHM);

			return portFacade;
		}


		private List<Tuple<float, string, DeviceHelper.ReportType>> _callbacks;
		private ZaberPortFacade _portFacade;
	}
}

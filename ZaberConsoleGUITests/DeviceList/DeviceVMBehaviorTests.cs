using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Rhino.Mocks;
using Rhino.Mocks.Interfaces;
using Zaber;
using Zaber.Testing;
using Zaber.Units;
using ZaberConsole.DeviceList;
using ZaberTest;
using ZaberTest.Testing;
using ZaberWpfToolbox.Dialogs;

namespace ZaberConsoleGUITests.DeviceList
{
	using Measurement = Zaber.Measurement;

	[TestFixture]
	[Category("active")]
	[SetCulture("en-US")]
	[SetUICulture("en-US")]
	public class DeviceVMBehaviorTests
	{
		[SetUp]
		public void SetUp()
		{
			DispatcherStack.Push(new ImmediateDispatcher());

			_mocks = new MockRepository();
			_port = _mocks.StrictMock<IZaberPort>();

			_device = new ZaberDevice
			{
				DeviceNumber = 3,
				DeviceType = new DeviceType()
			};

			_device.DeviceType.Capabilities.Add(Capability.Controller);

			_commands = new List<CommandInfo>
			{
				new CommandInfo
				{
					Command = Command.MoveAtConstantSpeed,
					Name = "Constant Speed",
					DataDescription = "Velocity",
					ResponseDescription = "Velocity"
				},
				new CommandInfo
				{
					Command = Command.Stop,
					Name = "Stop",
					IsCurrentPositionReturned = true
				},
				new CommandInfo
				{
					TextCommand = "pos",
					Name = "Current Position",
					IsCurrentPositionReturned = true
				},
				new SettingInfo
				{
					TextCommand = "peripheral.id",
					AccessLevel = 0
				}
			};

			_axisCommands = _commands.Select(cmd => {
					var newCmd = cmd.Clone();
					newCmd.IsAxisCommand = true;
					return newCmd;
				}).ToList();

			_device.DeviceType.Commands = _commands;

			_conversation = new Conversation(_device);

			_port.IsAsciiMode = false;
			LastCall.PropertyBehavior();
			_port.IsAsciiMode = false;

			_port.DataPacketReceived += null;
			_packetReceivedRaiser =
				LastCall.IgnoreArguments().Repeat.Any().GetEventRaiser();
			_port.DataPacketSent += null;
			_packetSentRaiser =
				LastCall.IgnoreArguments().Repeat.Any().GetEventRaiser();
			_port.ErrorReceived += null;
			_errorReceivedRaiser =
				LastCall.IgnoreArguments().Repeat.Any().GetEventRaiser();
		}


		[TearDown]
		public void Teardown() => DispatcherStack.Pop();


		[Test]
		public void BusyAfterKnobFinishesNoNotification()
		{
			// SETUP
			const string expectedError = "Error response (Busy) from device 3";

			var trackingPacket = new DataPacket
			{
				Command = Command.ManualMoveTracking,
				NumericData = 1234,
				DeviceNumber = _device.DeviceNumber
			};

			var busyPacket = new DataPacket
			{
				Command = Command.Error,
				NumericData = (int) ZaberError.Busy,
				DeviceNumber = _device.DeviceNumber
			};

			var echoPacket = new DataPacket
			{
				Command = Command.EchoData,
				NumericData = 99,
				DeviceNumber = _device.DeviceNumber
			};

			// EXPECT
			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;
			var view = new DeviceVM(null, _conversation);

			// Manual move
			_packetReceivedRaiser.Raise(_port, new DataPacketEventArgs(trackingPacket));

			// Manual move is now over, because a successful response is received.
			_packetReceivedRaiser.Raise(_port, new DataPacketEventArgs(echoPacket));

			// This should not set the notification
			_packetReceivedRaiser.Raise(_port, new DataPacketEventArgs(busyPacket));

			var errorAfterBusy = view.ErrorMessage;
			var notificationAfterBusy = view.Notification;

			// VERIFY
			Assert.AreEqual(expectedError,
							errorAfterBusy,
							"Error after busy response");
			Assert.IsNull(notificationAfterBusy,
							"Notification after busy response");
			_mocks.VerifyAll();
		}


		[Test]
		public void DeviceNumber()
		{
			// SETUP
			_device.DeviceNumber = 3;
			var expectedDeviceNumber = "03";

			// EXPECT
			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;
			var view = new DeviceVM(null, _conversation);
			var deviceNumber = view.DeviceNumber;

			// VERIFY
			Assert.That(deviceNumber,
						Is.EqualTo(expectedDeviceNumber),
						"device number");
			_mocks.VerifyAll();
		}


		[Test]
		public void DeviceNumberWithAxis()
		{
			// SETUP
			_device.DeviceNumber = 3;
			_device.AxisNumber = 2;
			var expectedDeviceNumber = "03 axis 2";

			// EXPECT
			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;
			var view = new DeviceVM(null, _conversation);
			var deviceNumber = view.DeviceNumber;

			// VERIFY
			Assert.That(deviceNumber,
						Is.EqualTo(expectedDeviceNumber),
						"device number");
			_mocks.VerifyAll();
		}


		[Test]
		public void LogErrorResponse()
		{
			// SETUP
			var packet = new DataPacket
			{
				Command = Command.Error,
				NumericData = (int) ZaberError.CommandInvalid,
				DeviceNumber = _device.DeviceNumber
			};

			_isResponseReceived = false;

			// EXPECT
			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;
			var view = new DeviceVM(null, _conversation);
			view.RefreshRequired += view_ResponseReceived;

			_packetReceivedRaiser.Raise(_port, new DataPacketEventArgs(packet));

			var errorTextAfterError = view.ErrorMessage;

			// now send a regular response to clear the error
			packet.Command = Command.SetCurrentPosition;
			_packetReceivedRaiser.Raise(_port, new DataPacketEventArgs(packet));

			var errorTextAfterRegularResponse = view.ErrorMessage;

			// VERIFY
			Assert.AreEqual("Error response (CommandInvalid) from device 3",
							errorTextAfterError,
							"Error text should match after error");
			Assert.AreEqual(string.Empty,
							errorTextAfterRegularResponse,
							"Error text should match after regular response");
			_mocks.VerifyAll();
		}


		[Test]
		public void LogPortError()
		{
			// SETUP
			var eventArgs =
				new ZaberPortErrorReceivedEventArgs(ZaberPortError.PacketTimeout);

			_isResponseReceived = false;

			// EXPECT
			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;
			var view = new DeviceVM(null, _conversation);
			view.RefreshRequired += view_ResponseReceived;

			_errorReceivedRaiser.Raise(_port, eventArgs);

			var errorText = view.ErrorMessage;

			// VERIFY
			Assert.AreEqual("Port error: PacketTimeout",
							errorText,
							"Error text should match after error");
			Assert.IsTrue(_isResponseReceived,
						  "Response event should have occurred.");
			_mocks.VerifyAll();
		}


		[Test]
		public void LogResponse()
		{
			// SETUP
			_device.DeviceNumber = 33;

			var packet = new DataPacket
			{
				Command = Command.MoveAtConstantSpeed,
				NumericData = 500,
				DeviceNumber = _device.DeviceNumber
			};

			_isResponseReceived = false;

			// EXPECT
			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;
			var view = new DeviceVM(null, _conversation);
			view.RefreshRequired += view_ResponseReceived;

			_packetReceivedRaiser.Raise(_port, new DataPacketEventArgs(packet));

			var errorText = view.ErrorMessage;

			// VERIFY
			Assert.IsTrue(string.IsNullOrEmpty(errorText));
			Assert.IsTrue(_isResponseReceived, "Response event should have occurred.");
			_mocks.VerifyAll();
		}


		[Test]
		public void LogUnknownErrorResponse()
		{
			// SETUP
			var packet = new DataPacket
			{
				Command = Command.Error,
				NumericData = 66,
				DeviceNumber = _device.DeviceNumber
			};

			_isResponseReceived = false;

			// EXPECT
			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;
			var view = new DeviceVM(null, _conversation);
			view.RefreshRequired += view_ResponseReceived;

			_packetReceivedRaiser.Raise(_port, new DataPacketEventArgs(packet));

			var errorTextAfterError = view.ErrorMessage;

			// now send a regular response to clear the error
			packet.Command = Command.SetCurrentPosition;
			_packetReceivedRaiser.Raise(_port, new DataPacketEventArgs(packet));

			var errorTextAfterRegularResponse = view.ErrorMessage;

			// VERIFY
			Assert.AreEqual("Error response (code = 66) from device 3",
							errorTextAfterError,
							"Error text should match after error");
			Assert.AreEqual(string.Empty,
							errorTextAfterRegularResponse,
							"Error text should match after regular response");
			_mocks.VerifyAll();
		}


		/// <summary>
		///     Scenario: The user has (maybe accidentally) turned the manual
		///     control knob. Most commands will respond with the Busy error. The
		///     device view should detect this scenario and notify the user that
		///     they should centre the control knob.
		/// </summary>
		[Test]
		public void NotifyTurnedKnob()
		{
			// SETUP
			var expectedNotification =
				"The manual control knob is turned.\n" + "That command will not work until you center it.";
			var expectedError = "Error response (Busy) from device 3";

			var trackingPacket = new DataPacket
			{
				Command = Command.ManualMoveTracking,
				NumericData = 1234,
				DeviceNumber = _device.DeviceNumber
			};

			var busyPacket = new DataPacket
			{
				Command = Command.Error,
				NumericData = (int) ZaberError.Busy,
				DeviceNumber = _device.DeviceNumber
			};

			var echoPacket = new DataPacket
			{
				Command = Command.EchoData,
				NumericData = 99,
				DeviceNumber = _device.DeviceNumber
			};

			// EXPECT
			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;
			var view = new DeviceVM(null, _conversation);

			_packetReceivedRaiser.Raise(_port, new DataPacketEventArgs(trackingPacket));
			_packetReceivedRaiser.Raise(_port, new DataPacketEventArgs(busyPacket));

			var errorAfterBusy = view.ErrorMessage;
			var notificationAfterBusy = view.Notification;

			_packetReceivedRaiser.Raise(_port, new DataPacketEventArgs(trackingPacket));

			var errorAfterTracking = view.ErrorMessage;
			var notificationAfterTracking = view.Notification;

			_packetReceivedRaiser.Raise(_port, new DataPacketEventArgs(echoPacket));

			var errorAfterEcho = view.ErrorMessage;

			// VERIFY
			Assert.AreEqual(expectedError,
							errorAfterBusy,
							"Error after busy response");
			Assert.AreEqual(expectedNotification,
							notificationAfterBusy,
							"Notification after busy response");
			Assert.AreEqual(expectedError,
							errorAfterTracking,
							"Error after second tracking response");
			Assert.AreEqual(expectedNotification,
							notificationAfterTracking,
							"Notification after second tracking response");
			Assert.AreEqual(string.Empty,
							errorAfterEcho,
							"Error after echo response");
			_mocks.VerifyAll();
		}


		[Test]
		public void Position()
		{
			// SETUP
			_device.DeviceNumber = 33;

			var packet = new DataPacket
			{
				Command = Command.Stop,
				NumericData = 500,
				DeviceNumber = _device.DeviceNumber
			};

			// EXPECT
			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;
			var view = new DeviceVM(null, _conversation);

			_packetReceivedRaiser.Raise(_port, new DataPacketEventArgs(packet));

			var position = view.Position;
			var uom = view.SelectedUnit;

			// VERIFY
			Assert.That(position,
						Is.EqualTo("500"),
						"position");
			Assert.That(uom,
						Is.EqualTo(UnitOfMeasure.Data),
						"unit of measure");
			_mocks.VerifyAll();
		}


		[Test]
		public void PositionReadText()
		{
			// SETUP
			_device.DeviceNumber = 33;
			_port.IsAsciiMode = true;

			var requestPacket = new DataPacket("/33 0 get pos");
			var responsePacket = new DataPacket("@33 0 OK IDLE -- 500");

			// EXPECT
			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;
			var view = new DeviceVM(null, _conversation);

			_packetSentRaiser.Raise(_port,
									new DataPacketEventArgs(requestPacket));
			_packetReceivedRaiser.Raise(_port,
										new DataPacketEventArgs(responsePacket));

			var position = view.Position;

			// VERIFY
			Assert.That(position,
						Is.EqualTo("500"),
						"position");
			_mocks.VerifyAll();
		}


		[Test]
		public void PositionUnits()
		{
			// SETUP
			_device.DeviceNumber = 33;
			var deviceType = _device.DeviceType;
			deviceType.MotionType = MotionType.Linear;

			deviceType.DeviceTypeUnitConversionInfo[Dimension.Length] = new ParameterUnitConversion
			{
				Function = ScalingFunction.LinearResolution,
				ReferenceUnit = UnitOfMeasure.Millimeter.Unit,
				Scale = 15.625m
			};

			var packet = new DataPacket
			{
				DeviceNumber = _device.DeviceNumber,
				Command = Command.Stop,
				NumericData = 500
			};

			// EXPECT
			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;
			var view = new DeviceVM(null, _conversation) { SelectedUnit = UnitOfMeasure.Millimeter };

			_packetReceivedRaiser.Raise(_port, new DataPacketEventArgs(packet));

			var position = view.Position;

			// VERIFY
			Assert.That(position,
						Is.EqualTo("0.5"),
						"position");
			_mocks.VerifyAll();
		}


		[Test]
		public void UpdatePositionInSelectedUnits()
		{
			_device.Port = _port;
			var deviceType = _device.DeviceType;
			deviceType.MotionType = MotionType.Linear;
			deviceType.DeviceTypeUnitConversionInfo[Dimension.Length] = new ParameterUnitConversion
			{
				Function = ScalingFunction.LinearResolution,
				ReferenceUnit = UnitOfMeasure.Millimeter.Unit,
				Scale = 15.625m
			};

			var view = new DeviceVM(null, _conversation) { SelectedUnit = UnitOfMeasure.Millimeter };
			view.UpdatePositionInSelectedUnits(500m);
			Assert.AreEqual("0.5", view.Position);
		}


		[Test]
		public void RegularBusyNoNotification()
		{
			// SETUP
			var expectedError = "Error response (Busy) from device 3";

			var busyPacket = new DataPacket
			{
				Command = Command.Error,
				NumericData = (int) ZaberError.Busy,
				DeviceNumber = _device.DeviceNumber
			};

			// EXPECT
			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;
			var view = new DeviceVM(null, _conversation);

			_packetReceivedRaiser.Raise(_port, new DataPacketEventArgs(busyPacket));

			var errorAfterBusy = view.ErrorMessage;
			var notificationAfterBusy = view.Notification;

			// VERIFY
			Assert.AreEqual(expectedError,
							errorAfterBusy,
							"Error text");
			Assert.IsTrue(string.IsNullOrEmpty(notificationAfterBusy), "Notification");
			_mocks.VerifyAll();
		}


		[Test]
		public void SkipNotificationOnDeviceCollections()
		{
			// SETUP
			var deviceType = _device.DeviceType;
			var devices = new DeviceCollection();
			_device = devices;
			_device.DeviceNumber = 3;
			_device.DeviceType = deviceType;
			_conversation = new ConversationCollection(devices);

			var trackingPacket = new DataPacket
			{
				Command = Command.ManualMoveTracking,
				NumericData = 1234,
				DeviceNumber = _device.DeviceNumber
			};

			var busyPacket = new DataPacket
			{
				Command = Command.Error,
				NumericData = (int) ZaberError.Busy,
				DeviceNumber = _device.DeviceNumber
			};

			// EXPECT
			_mocks.ReplayAll();

			// EXEC
			_device.Port = _port;
			var view = new DeviceVM(null, _conversation);

			_packetReceivedRaiser.Raise(_port, new DataPacketEventArgs(trackingPacket));
			_packetReceivedRaiser.Raise(_port, new DataPacketEventArgs(busyPacket));

			var errorAfterBusy = view.ErrorMessage;
			var notificationAfterBusy = view.Notification;

			// VERIFY
			Assert.IsNotEmpty(errorAfterBusy, "Error after busy response");
			Assert.IsTrue(string.IsNullOrEmpty(notificationAfterBusy), "Notification after busy response");
			_mocks.VerifyAll();
		}


		[Test]
		public void BadAccessLevelPeripheralIdNotEditable()
		{
			_device.DeviceNumber = 3;
			_device.Port = _port;
			var view = new DeviceVM(null, _conversation);

			// Make the write access level for peripheral ID higher than the device's level.
			var setting = _device.DeviceType.Commands.First(c => "peripheral.id" == c.TextCommand);
			setting.AccessLevel = 4;
			_device.AccessLevel = 1;

			view.SetSupportedPeripherals(_testPeripherals);

			Assert.IsNotEmpty(view.ControllerName);
			Assert.IsNotEmpty(view.PeripheralName);
			Assert.IsTrue(view.ShowPeripheralName);
			Assert.IsFalse(view.CanEditPeripheralName);
		}


		[Test]
		public void EditablePeripheralIdsByName()
		{
			_device.DeviceNumber = 3;
			_device.Port = _port;
			var view = new DeviceVM(null, _conversation);

			CustomMessageBoxVM mbox = null;
			view.RequestDialogOpen += (aSender, aArgs) =>
			{
				mbox = aArgs as CustomMessageBoxVM;
				mbox.DialogResult = true;
			};

			view.SetSupportedPeripherals(_testPeripherals);

			Assert.IsNotEmpty(view.ControllerName);
			Assert.IsNotEmpty(view.PeripheralName);
			Assert.IsTrue(view.ShowPeripheralName);
			Assert.IsTrue(view.CanEditPeripheralName);

			var messageReceived = false;
			_port.Expect(p =>
			{
				p.Send(3, Command.SetPeripheralID, 2, new Measurement(2, UnitOfMeasure.Data));
			}).IgnoreArguments().WhenCalled(i =>
			{
				messageReceived = true;
				_packetReceivedRaiser.Raise(_port, new DataPacketEventArgs(new DataPacket() { DeviceNumber = 3, Command=Command.SetPeripheralID, NumericData = 2m}));
			});

			_mocks.Replay(_port);

			view.ChangePeripheralCommand.Execute("Bar");
			Assert.IsTrue(messageReceived);
			Assert.IsNotNull(mbox);
			Assert.IsTrue(mbox.Message.Contains("Bar"));
		}


		[Test]
		public void EditablePeripheralIdsByNumber()
		{
			_device.DeviceNumber = 3;
			_device.Port = _port;
			var view = new DeviceVM(null, _conversation);

			CustomMessageBoxVM mbox = null;
			view.RequestDialogOpen += (aSender, aArgs) =>
			{
				mbox = aArgs as CustomMessageBoxVM;
				mbox.DialogResult = true;
			};

			view.SetSupportedPeripherals(_testPeripherals);

			Assert.IsNotEmpty(view.ControllerName);
			Assert.IsNotEmpty(view.PeripheralName);
			Assert.IsTrue(view.ShowPeripheralName);
			Assert.IsTrue(view.CanEditPeripheralName);

			var messageReceived = false;
			_port.Expect(p =>
			{
				p.Send(3, Command.SetPeripheralID, 1, new Measurement(1, UnitOfMeasure.Data));
			}).IgnoreArguments().WhenCalled(i =>
			{
				messageReceived = true;
				_packetReceivedRaiser.Raise(_port, new DataPacketEventArgs(new DataPacket() { DeviceNumber = 3, Command = Command.SetPeripheralID, NumericData = 1m }));
			});

			_mocks.Replay(_port);

			view.ChangePeripheralCommand.Execute("1");
			Assert.IsTrue(messageReceived);
			Assert.IsNotNull(mbox);
			Assert.IsTrue(mbox.Message.Contains("Foo"));
		}


		[Test]
		public void EditablePeripheralIdsByNumberUnknown()
		{
			_device.DeviceNumber = 3;
			_device.Port = _port;
			var view = new DeviceVM(null, _conversation);

			CustomMessageBoxVM mbox = null;
			view.RequestDialogOpen += (aSender, aArgs) =>
			{
				mbox = aArgs as CustomMessageBoxVM;
				mbox.DialogResult = true;
			};

			view.SetSupportedPeripherals(_testPeripherals);

			Assert.IsNotEmpty(view.ControllerName);
			Assert.IsNotEmpty(view.PeripheralName);
			Assert.IsTrue(view.ShowPeripheralName);
			Assert.IsTrue(view.CanEditPeripheralName);

			var messageReceived = false;
			_port.Expect(p =>
			{
				p.Send(3, Command.SetPeripheralID, 333, new Measurement(333, UnitOfMeasure.Data));
			}).IgnoreArguments().WhenCalled(i =>
			{
				messageReceived = true;
				_packetReceivedRaiser.Raise(_port, new DataPacketEventArgs(new DataPacket() { DeviceNumber = 3, Command = Command.SetPeripheralID, NumericData = 333m }));
			});

			_mocks.Replay(_port);

			view.ChangePeripheralCommand.Execute("333");
			Assert.IsTrue(messageReceived);
			Assert.IsNotNull(mbox);
			Assert.IsTrue(mbox.Message.Contains("333"));
		}


		[Test]
		public void EditablePeripheralIdsCancel()
		{
			_device.DeviceNumber = 3;
			_device.Port = _port;
			var view = new DeviceVM(null, _conversation);

			CustomMessageBoxVM mbox = null;
			view.RequestDialogOpen += (aSender, aArgs) =>
			{
				mbox = aArgs as CustomMessageBoxVM;
				mbox.DialogResult = false;
			};

			view.SetSupportedPeripherals(_testPeripherals);

			Assert.IsNotEmpty(view.ControllerName);
			Assert.IsNotEmpty(view.PeripheralName);
			Assert.IsTrue(view.ShowPeripheralName);
			Assert.IsTrue(view.CanEditPeripheralName);

			view.ChangePeripheralCommand.Execute("1");
			Assert.IsNotNull(mbox);
			Assert.IsNull(view.PeripheralName);
		}


		[Test]
		public void ReadOnlyPeripheralIdNotEditable()
		{
			_device.DeviceNumber = 3;
			_device.Port = _port;
			var view = new DeviceVM(null, _conversation);

			// Replace the peripheral ID setting with a read-only version.
			var commands = new List<CommandInfo>(_device.DeviceType.Commands);
			var setting = commands.First(c => "peripheral.id" == c.TextCommand);
			commands.Remove(setting);
			var newSetting = new ReadOnlySettingInfo();
			InvocationHelper.InvokePrivateStaticMethod(typeof(SettingInfo),
													   "CopyProperties",
													   setting,
													   newSetting);
			commands.Add(newSetting);
			_device.DeviceType.Commands = commands;

			view.SetSupportedPeripherals(_testPeripherals);

			Assert.IsNotEmpty(view.ControllerName);
			Assert.IsNotEmpty(view.PeripheralName);
			Assert.IsTrue(view.ShowPeripheralName);
			Assert.IsFalse(view.CanEditPeripheralName);
		}


		[Test]
		public void PendingPeripheralIdChangeShowsActivate()
		{
			_device.DeviceNumber = 3;
			_device.Port = _port;
			AddAxis(_device, 123);
			_conversation = new Conversation(_device.Axes[0]);
			var view = new DeviceVM(null, _conversation);

			Assert.IsTrue(view.IsPeripheralIdSet);
			Assert.IsTrue(view.IsActivated);
			Assert.IsTrue(string.IsNullOrEmpty(view.ActivateButtonLabel));
			Assert.IsTrue(string.IsNullOrEmpty(view.ActivateButtonToolTip));
			Assert.IsFalse(view.ShowAxisInfoButton);

			view.SetPendingPeripheralId(456);
			view.HasPeripheralConnectionIssue = true;

			Assert.IsTrue(view.IsPeripheralIdSet);
			Assert.IsFalse(view.IsActivated);
			Assert.IsFalse(view.CanEditPeripheralName);
			Assert.AreEqual(ZaberConsole.Properties.Resources.STR_BUTTON_ACTIVATE, view.ActivateButtonLabel);
			Assert.IsFalse(string.IsNullOrEmpty(view.ActivateButtonToolTip));
			Assert.IsFalse(view.ShowAxisInfoButton);
			Assert.AreEqual(ZaberConsole.Properties.Resources.STR_AXIS_DISCONNECTED, view.AxisInfoMessage);
			Assert.AreEqual(ZaberConsole.Properties.Resources.STR_TIP_AXIS_DISCONNECTED, view.AxisInfoTooltip);
		}


		[Test]
		public void PendingPeripheralIdChangeShowsNewName()
		{
			_device.DeviceNumber = 3;
			_device.Port = _port;
			AddAxis(_device, 123);
			_conversation = new Conversation(_device.Axes[0]);
			var view = new DeviceVM(null, _conversation);

			view.SetPendingPeripheralId(456);
			view.HasPeripheralConnectionIssue = true;

			Assert.AreEqual("456", view.PeripheralName);
		}


		[Test]
		public void PendingPeripheralIdChangeDoesNotClearUserText()
		{
			_device.DeviceNumber = 3;
			_device.Port = _port;
			AddAxis(_device, 123);
			_conversation = new Conversation(_device.Axes[0]);
			var view = new DeviceVM(null, _conversation);

			view.PeripheralName = "Foo";
			view.SetPendingPeripheralId(456);
			view.HasPeripheralConnectionIssue = true;
			Assert.AreEqual("Foo", view.PeripheralName);
		}


		[Test]
		public void PendingSerialNumberChangeShowsActivate()
		{
			_device.DeviceNumber = 3;
			_device.Port = _port;
			AddAxis(_device, 123);
			_device.SerialNumber = 12345;
			_conversation = new Conversation(_device.Axes[0]);
			var view = new DeviceVM(null, _conversation);

			Assert.IsTrue(view.IsPeripheralIdSet);
			Assert.IsTrue(view.IsActivated);
			Assert.IsTrue(string.IsNullOrEmpty(view.ActivateButtonLabel));
			Assert.IsTrue(string.IsNullOrEmpty(view.ActivateButtonToolTip));
			Assert.IsFalse(view.ShowAxisInfoButton);

			view.SetPendingPeripheralId(123);
			view.HasPeripheralConnectionIssue = true;

			Assert.IsTrue(view.IsPeripheralIdSet);
			Assert.IsFalse(view.IsActivated);
			Assert.IsFalse(view.CanEditPeripheralName);
			Assert.AreEqual(ZaberConsole.Properties.Resources.STR_BUTTON_ACTIVATE, view.ActivateButtonLabel);
			Assert.IsFalse(string.IsNullOrEmpty(view.ActivateButtonToolTip));
			Assert.IsFalse(view.ShowAxisInfoButton);
			Assert.AreEqual(ZaberConsole.Properties.Resources.STR_AXIS_DISCONNECTED, view.AxisInfoMessage);
			Assert.AreEqual(ZaberConsole.Properties.Resources.STR_TIP_AXIS_DISCONNECTED, view.AxisInfoTooltip);
		}


		[Test]
		public void PeripheralReconnectedHidesActivateButton()
		{
			_device.DeviceNumber = 3;
			_device.Port = _port;
			AddAxis(_device, 123);
			_device.SerialNumber = 12345;
			_conversation = new Conversation(_device.Axes[0]);
			var view = new DeviceVM(null, _conversation);

			view.SetPendingPeripheralId(123);

			Assert.IsTrue(view.IsPeripheralIdSet);
			Assert.IsTrue(view.IsActivated);
			Assert.IsTrue(string.IsNullOrEmpty(view.ActivateButtonLabel));
			Assert.IsTrue(string.IsNullOrEmpty(view.ActivateButtonToolTip));
			Assert.IsFalse(view.ShowAxisInfoButton);
			Assert.AreEqual(ZaberConsole.Properties.Resources.STR_INFO_AXIS_CONNECTED, view.AxisInfoMessage);
			Assert.AreEqual(ZaberConsole.Properties.Resources.STR_TIP_AXIS_CONNECTED, view.AxisInfoTooltip);
		}


		[Test]
		public void UnpluggingDetectablePeripheralShowsReactivate()
		{
			_device.DeviceNumber = 3;
			_device.Port = _port;
			AddAxis(_device, 123);
			_device.Axes[0].DeviceType.Capabilities.Add(Capability.AutoDetect);
			_conversation = new Conversation(_device.Axes[0]);
			var view = new DeviceVM(null, _conversation);

			view.SetPendingPeripheralId(0);
			view.HasPeripheralConnectionIssue = true;

			Assert.IsTrue(view.IsPeripheralIdSet);
			Assert.IsFalse(view.IsActivated);
			Assert.IsTrue(view.CanEditPeripheralName);
			Assert.IsTrue(string.IsNullOrEmpty(view.ActivateButtonLabel));
			Assert.IsTrue(string.IsNullOrEmpty(view.ActivateButtonToolTip));
			Assert.AreEqual(ZaberConsole.Properties.Resources.STR_INFO_RECONNECT_PERIPHERAL, view.AxisInfoMessage);
			Assert.AreEqual(ZaberConsole.Properties.Resources.STR_TIP_RECONNECT_PERIPHERAL, view.AxisInfoTooltip);
		}


		[Test]
		public void UnpluggingUndetectablePeripheralShowsReconnect()
		{
			_device.DeviceNumber = 3;
			_device.Port = _port;
			AddAxis(_device, 123);
			_conversation = new Conversation(_device.Axes[0]);
			var view = new DeviceVM(null, _conversation);

			view.SetPendingPeripheralId(0);
			view.HasPeripheralConnectionIssue = true;

			Assert.IsTrue(view.IsPeripheralIdSet);
			Assert.IsFalse(view.IsActivated);
			Assert.IsTrue(view.CanEditPeripheralName);
			Assert.AreEqual(ZaberConsole.Properties.Resources.STR_BUTTON_REACTIVATE, view.ActivateButtonLabel);
			Assert.AreEqual(ZaberConsole.Properties.Resources.STR_TOOLTIP_REACTIVATE, view.ActivateButtonToolTip);
			Assert.AreEqual(ZaberConsole.Properties.Resources.STR_INFO_REACTIVATE_PERIPHERAL, view.AxisInfoMessage);
			Assert.AreEqual(ZaberConsole.Properties.Resources.STR_TIP_RECONNECT_PERIPHERAL, view.AxisInfoTooltip);
		}


		[Test]
		public void ActivateButtonActivates()
		{
			_port.IsAsciiMode = true;
			_device.DeviceNumber = 3;
			_device.Port = _port;
			AddAxis(_device, 123);
			_device.Axes[0].DeviceNumber = 3;
			_conversation = new Conversation(_device.Axes[0]);
			var view = new DeviceVM(null, _conversation);

			view.RequestDialogOpen += (sender, vm) =>
			{
				var mbox = vm as CustomMessageBoxVM;
				Assert.IsNotNull(mbox);
				Assert.AreEqual(
					ZaberConsole.Properties.Resources.STR_DLGTITLE_CONFIRM_PERIPHERAL_CHANGE, 
					mbox.Title);
				Assert.IsTrue(mbox.Message.Contains(view.ActivateButtonToolTip));
				Assert.AreEqual("Cancel", mbox.Options.First(o => false == (bool)o.Identifier).Label);
				Assert.AreEqual(view.ActivateButtonLabel, mbox.Options.First(o => true == (bool)o.Identifier).Label);
				mbox.DialogResult = true;
			};

			view.SetPendingPeripheralId(456);
			view.HasPeripheralConnectionIssue = true;

			var messageReceived = false;
			_port.Expect(p =>
			{
				p.Send("03 1 activate", null);
			}).IgnoreArguments().WhenCalled(i =>
			{
				messageReceived = true;
				_packetReceivedRaiser.Raise(_port, new DataPacketEventArgs(new DataPacket("@03 1 OK IDLE -- 0")));
			});

			_mocks.Replay(_port);

			view.ActivateButtonCommand.Execute(null);
			Assert.IsTrue(messageReceived);
		}


		[Test]
		public void TypingAfterUnpluggingShowsActivateWhichSetsPeripheralId()
		{
			_port.IsAsciiMode = true;
			_device.DeviceNumber = 3;
			_device.Port = _port;
			AddAxis(_device, 123);
			_device.Axes[0].DeviceNumber = 3;
			_device.Axes[0].DeviceType.Commands = _axisCommands;
			_conversation = new Conversation(_device.Axes[0]);
			var view = new DeviceVM(null, _conversation);
			view.SetSupportedPeripherals(_testPeripherals);

			view.SetPendingPeripheralId(0);
			view.PeripheralName = "2";

			Assert.AreEqual(ZaberConsole.Properties.Resources.STR_BUTTON_ACTIVATE, view.ActivateButtonLabel);
			Assert.AreEqual(ZaberConsole.Properties.Resources.STR_TOOLTIP_ACTIVATE_PID, view.ActivateButtonToolTip);

			view.RequestDialogOpen += (sender, vm) =>
			{
				var mbox = vm as CustomMessageBoxVM;
				Assert.IsNotNull(mbox);
				Assert.AreEqual(
					ZaberConsole.Properties.Resources.STR_DLGTITLE_CONFIRM_PERIPHERAL_CHANGE, 
					mbox.Title);
				mbox.DialogResult = true;
			};

			var messageReceived = false;
			_port.Expect(p =>
			{
				p.Send("03 1 set peripheralid 2", null);
			}).IgnoreArguments().WhenCalled(i =>
			{
				messageReceived = true;
				_packetReceivedRaiser.Raise(_port, new DataPacketEventArgs(new DataPacket("@03 1 OK IDLE -- 0")));
			});

			_mocks.Replay(_port);

			view.ActivateButtonCommand.Execute(null);
			Assert.IsTrue(messageReceived);
		}


		[Test]
		public void HandleActivateCommandRejected()
		{
			_port.IsAsciiMode = true;
			_device.DeviceNumber = 3;
			_device.Port = _port;
			AddAxis(_device, 123);
			_device.Axes[0].DeviceNumber = 3;
			_conversation = new Conversation(_device.Axes[0]);
			var view = new DeviceVM(null, _conversation);

			int messageBoxCount = 0;

			view.RequestDialogOpen += (sender, vm) =>
			{
				messageBoxCount++;
				var mbox = vm as CustomMessageBoxVM;
				Assert.IsNotNull(mbox);
				if (mbox.Message.Contains(view.ActivateButtonToolTip))
				{
					Assert.AreEqual(
						ZaberConsole.Properties.Resources.STR_DLGTITLE_CONFIRM_PERIPHERAL_CHANGE,
						mbox.Title);
					Assert.IsTrue(mbox.Message.Contains(view.ActivateButtonToolTip));
					Assert.AreEqual("Cancel", mbox.Options.First(o => false == (bool)o.Identifier).Label);
					Assert.AreEqual(view.ActivateButtonLabel, mbox.Options.First(o => true == (bool)o.Identifier).Label);
				}
				else
				{
					Assert.AreEqual("Warning", mbox.Title);
					Assert.AreEqual(ZaberConsole.Properties.Resources.ERR_PID_REJECTED, mbox.Message);
				}

				mbox.DialogResult = true;
			};

			view.SetPendingPeripheralId(456);
			view.HasPeripheralConnectionIssue = true;

			var messageReceived = false;
			_port.Expect(p =>
			{
				p.Send("03 1 activate", null);
			}).IgnoreArguments().WhenCalled(i =>
			{
				messageReceived = true;
				_packetReceivedRaiser.Raise(_port, new DataPacketEventArgs(new DataPacket("@03 1 RJ IDLE -- BADCOMMAND")));
			});

			_mocks.Replay(_port);

			view.ActivateButtonCommand.Execute(null);
			Assert.IsTrue(messageReceived);
			Assert.AreEqual(2, messageBoxCount);
		}


		[Test]
		public void HandleSetInvalidPeripheralIDRejected()
		{
			_port.IsAsciiMode = true;
			_device.DeviceNumber = 3;
			_device.Port = _port;
			AddAxis(_device, 123);
			_device.Axes[0].DeviceNumber = 3;
			_device.Axes[0].DeviceType.Commands = _axisCommands;
			_conversation = new Conversation(_device.Axes[0]);
			var view = new DeviceVM(null, _conversation);
			view.SetSupportedPeripherals(_testPeripherals);

			view.SetPendingPeripheralId(0);
			view.PeripheralName = "5";
			Assert.IsTrue(view.CanEditPeripheralName);

			int messageBoxCount = 0;

			view.RequestDialogOpen += (sender, vm) =>
			{
				messageBoxCount++;
				var mbox = vm as CustomMessageBoxVM;
				Assert.IsNotNull(mbox);
				if (ZaberConsole.Properties.Resources.STR_DLGTITLE_CONFIRM_PERIPHERAL_CHANGE == mbox.Title)
				{
					Assert.AreEqual(string.Format(ZaberConsole.Properties.Resources.STR_CONFIRM_PID_NOT_SUPPORTED, 5), mbox.Message);
				}
				else
				{
					Assert.AreEqual("Warning", mbox.Title);
					Assert.AreEqual(ZaberConsole.Properties.Resources.ERR_PID_REJECTED, mbox.Message);
				}

				mbox.DialogResult = true;
			};

			var messageReceived = false;
			_port.Expect(p =>
			{
				p.Send("03 1 set peripheralid 2", null);
			}).IgnoreArguments().WhenCalled(i =>
			{
				messageReceived = true;
				_packetReceivedRaiser.Raise(_port, new DataPacketEventArgs(new DataPacket("@03 1 RJ IDLE -- BADDATA")));
			});

			_mocks.Replay(_port);

			view.ActivateButtonCommand.Execute(null);
			Assert.IsTrue(messageReceived);
			Assert.AreEqual(2, messageBoxCount);
		}


		[Test]
		public void ControlsInactiveIfPeripheralIdReadOnly()
		{
			_port.IsAsciiMode = true;
			_device.DeviceNumber = 3;
			_device.Port = _port;
			AddAxis(_device, 123);
			_device.Axes[0].DeviceNumber = 3;

			var pidSetting = new ReadOnlySettingInfo()
			{
				TextCommand = "peripheral.id",
				IsAxisCommand = true,
			};

			var newCommands = _device.Axes[0].DeviceType.Commands;
			newCommands.Add(pidSetting);
			_device.Axes[0].DeviceType.Commands = newCommands;

			_conversation = new Conversation(_device.Axes[0]);
			var view = new DeviceVM(null, _conversation);
			view.SetSupportedPeripherals(_testPeripherals);

			Assert.IsFalse(view.CanEditPeripheralName);
			Assert.IsNull(view.ActivateButtonLabel);
		}


		[Test]
		public void ControllerShowsInfoIconOnFZFlag()
		{
			var portFacade = SetupAndOpenPort(CreateDeviceType(2));
			var conv = portFacade.GetConversation(1);
			var view = new DeviceVM(portFacade, conv);

			Assert.IsFalse(view.ShowAxisInfoButton);

			var mockPort = portFacade.Port as MockPort;
			mockPort.Expect("/01 0 get pos");
			mockPort.AddResponse("@01 0 OK IDLE FZ 0");
			conv.Device.Send("get pos");

			Assert.AreEqual(ZaberConsole.Properties.Resources.STR_CONTROLLER_AXIS_DISCONNECTED, view.AxisInfoMessage);
			Assert.AreEqual(ZaberConsole.Properties.Resources.STR_TIP_CONTROLLER_AXIS_DISCONNECTED, view.AxisInfoTooltip);

			mockPort.Expect("/01 0 get pos");
			mockPort.AddResponse("@01 0 OK IDLE -- 0");
			conv.Device.Send("get pos");

			Assert.IsFalse(view.ShowAxisInfoButton);
		}


		[Test]
		public void AxisShowsInfoIconOnFZFlag()
		{
			var portFacade = SetupAndOpenPort(CreateDeviceType(2));
			var conv = portFacade.GetConversation(1, 1);
			var view = new DeviceVM(portFacade, conv);

			Assert.IsFalse(view.ShowAxisInfoButton);

			var mockPort = portFacade.Port as MockPort;
			mockPort.Expect("/01 1 get pos");
			mockPort.AddResponse("@01 1 OK IDLE FZ 0");
			conv.Device.Send("get pos");

			Assert.AreEqual(ZaberConsole.Properties.Resources.STR_AXIS_DISCONNECTED, view.AxisInfoMessage);
			Assert.AreEqual(ZaberConsole.Properties.Resources.STR_TIP_AXIS_DISCONNECTED, view.AxisInfoTooltip);

			mockPort.Expect("/01 1 get pos");
			mockPort.AddResponse("@01 1 OK IDLE -- 0");
			conv.Device.Send("get pos");

			Assert.IsFalse(view.ShowAxisInfoButton);
		}


		private void view_ResponseReceived(object sender, EventArgs e) => _isResponseReceived = true;


		private void AddAxis(ZaberDevice aDevice, int aPeripheralId)
		{
			var axisType = new DeviceType()
			{
				DeviceId = aDevice.DeviceType.DeviceId,
				FirmwareVersion = aDevice.DeviceType.FirmwareVersion,
				PeripheralId = aPeripheralId 
			};

			var axisDevice = new ZaberDevice()
			{
				DeviceType = axisType,
				AxisNumber = 1,
				Port = aDevice.Port
			};

			aDevice.AddAxis(axisDevice);
		}


		private DeviceType CreateDeviceType(params int[] aPeripheralIds)
		{
			var peripheralMap = aPeripheralIds.ToDictionary(
				id => id,
				id => new DeviceType
				{
					PeripheralId = id,
					FirmwareVersion = new FirmwareVersion(7, 12),
					Commands = new List<CommandInfo>()
				});

			return new DeviceType
			{
				DeviceId = 30311,
				Name = "X-MCC1 Controller",
				FirmwareVersion = new FirmwareVersion(7, 12),
				MotionType = MotionType.None,
				PeripheralMap = peripheralMap
			};
		}


		private ZaberPortFacade SetupAndOpenPort(params DeviceType[] aDeviceTypes)
		{
			var mockPort = new MockPort { IsAsciiMode = true };

			var defaultDeviceType = new DeviceType { Commands = new List<CommandInfo>() };

			var portFacade = new ZaberPortFacade
			{
				DefaultDeviceType = defaultDeviceType,
				Port = mockPort,
				QueryTimeout = 100
			};

			foreach (var dType in aDeviceTypes)
			{
				portFacade.AddDeviceType(dType);
			}

			ZaberPortFacadeTest.SetAsciiExpectations(portFacade, mockPort, aDeviceTypes);

			portFacade.Open("COM1");
			portFacade.AreMessageIdsEnabled = true;
			return portFacade;
		}


		private MockRepository _mocks;
		private IZaberPort _port;
		private ZaberDevice _device;
		private Conversation _conversation;
		private IList<CommandInfo> _commands;
		private IList<CommandInfo> _axisCommands;
		private IEventRaiser _packetReceivedRaiser;
		private IEventRaiser _packetSentRaiser;
		private IEventRaiser _errorReceivedRaiser;
		private bool _isResponseReceived;


		private static readonly Tuple<int, string>[] _testPeripherals =
		{
			new Tuple<int, string>(0, "Saef Moed"), new Tuple<int, string>(1, "Foo"),
			new Tuple<int, string>(2, "Bar")
		};
	}
}

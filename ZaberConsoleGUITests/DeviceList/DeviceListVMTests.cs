﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using NUnit.Framework;
using Zaber;
using Zaber.Testing;
using ZaberConsole.DeviceList;
using ZaberTest;

namespace ZaberConsoleGUITests.DeviceList
{
	[TestFixture]
	[SetCulture("en-US")]
	public class DeviceListVMTests
	{
		[SetUp]
		public void Setup()
		{
			_port = new MockPort();

			_device = new ZaberDevice
			{
				DeviceNumber = 3,
				DeviceType = new DeviceType() { FirmwareVersion = new FirmwareVersion(6, 31) }
			};

			_conversation = new Conversation(_device);
			_port.IsAsciiMode = true;
			_device.Port = _port;

			_vm = new DeviceListVM();
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
		}


		[TearDown]
		public void Teardown()
		{
			_listener.Unlisten(_vm);
			_vm = null;
		}


		[Test]
		public void TestDevicesProperty()
		{
			_listener.TestObservableProperty(_vm, () => _vm.Devices, _ =>
			 {
				 var list = new ObservableCollection<DeviceVM>();
				 list.Add(new DeviceVM(null, _conversation));
				 return list;
			 });
			Assert.AreEqual(0, _listener.GetNumNotifications(), "Unexpected side effect notifications.");
		}


		[Test]
		public void TestEnabledProperty()
		{
			_listener.TestObservableProperty(_vm, () => _vm.Enabled);
			Assert.AreEqual(0, _listener.GetNumNotifications(), "Unexpected side effect notifications.");
		}


		[Test]
		public void TestSelectedDeviceProperty()
		{
			_listener.TestObservableProperty(_vm, () => _vm.SelectedDevice, _ => new DeviceVM(null, _conversation));
			Assert.AreEqual(0, _listener.GetNumNotifications(), "Unexpected side effect notifications.");
		}


		private IZaberPort _port;
		private ZaberDevice _device;
		private Conversation _conversation;
		private DeviceListVM _vm;
		private ObservableObjectTester _listener;
	}
}

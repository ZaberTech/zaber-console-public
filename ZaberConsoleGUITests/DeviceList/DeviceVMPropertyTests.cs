﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Zaber;
using Zaber.Testing;
using ZaberConsole.DeviceList;
using ZaberTest;

namespace ZaberConsoleGUITests.DeviceList
{
	[TestFixture]
	[SetCulture("en-US")]
	public class BaseDeviceVMTests
	{
		[SetUp]
		public void Setup()
		{
			_port = new MockPort();

			_device = new ZaberDevice
			{
				DeviceNumber = 3,
				DeviceType = new DeviceType() { FirmwareVersion = new FirmwareVersion(6, 31) }
			};

			_conversation = new Conversation(_device);
			_port.IsAsciiMode = true;
			_device.Port = _port;
			_vm = new DeviceVM(null, _conversation);
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
		}


		[TearDown]
		public void Teardown()
		{
			_listener?.Unlisten(_vm);
			_vm = null;
		}


		[Test]
		public void TestApplicableUnitsProperty()
		{
			_listener.TestObservableProperty(_vm, () => _vm.UnitsOfMeasure, aList =>
				(aList.Count > 0) 
					? new ObservableCollection<object>()
					: new ObservableCollection<object>() { UnitOfMeasure.Data });
			Assert.AreEqual(0, _listener.GetNumNotifications(), "Unexpected side effect notifications.");
		}


		[Test]
		public void TestDeviceNumberProperty()
		{
			_listener.TestObservableProperty(_vm, () => _vm.DeviceNumber);
			Assert.AreEqual(0, _listener.GetNumNotifications(), "Unexpected side effect notifications.");
		}


		[Test]
		public void TestDeviceTypeProperty()
		{
			_listener.TestObservableProperty(_vm, () => _vm.DeviceType);
			Assert.AreEqual(0, _listener.GetNumNotifications(), "Unexpected side effect notifications.");
		}


		[Test]
		public void TestIsActiveProperty()
		{
			_listener.TestObservableProperty(_vm, () => _vm.IsActivated);
			Assert.AreEqual(0, _listener.GetNumNotifications(), "Unexpected side effect notifications.");
		}


		[Test]
		public void TestIsPeripheralIdSetProperty()
		{
			_listener.TestObservableProperty(_vm, () => _vm.IsPeripheralIdSet);
			Assert.AreEqual(0, _listener.GetNumNotifications(), "Unexpected side effect notifications.");
		}


		[Test]
		public void TestActivateButtonLabelProperty()
		{
			_listener.TestObservableProperty(_vm, () => _vm.ActivateButtonLabel);
			Assert.AreEqual(0, _listener.GetNumNotifications(), "Unexpected side effect notifications.");
		}


		[Test]
		public void TestActivateButtonToolTipProperty()
		{
			_listener.TestObservableProperty(_vm, () => _vm.ActivateButtonToolTip);
			Assert.AreEqual(0, _listener.GetNumNotifications(), "Unexpected side effect notifications.");
		}


		[Test]
		public void TestAxisInfoMessageProperty()
		{
			_listener.TestObservableProperty(_vm, () => _vm.AxisInfoMessage);
			Assert.AreEqual(0, _listener.GetNumNotifications(), "Unexpected side effect notifications.");
		}


		[Test]
		public void TestAxisInfoTooltipProperty()
		{
			_listener.TestObservableProperty(_vm, () => _vm.AxisInfoTooltip);
			Assert.AreEqual(0, _listener.GetNumNotifications(), "Unexpected side effect notifications.");
		}


		[Test]
		public void TestErrorMessageProperty()
		{
			_listener.TestObservableProperty(_vm, () => _vm.ErrorMessage);
			Assert.AreEqual(0, _listener.GetNumNotifications(), "Unexpected side effect notifications.");
		}


		[Test]
		public void TestPositionProperty()
		{
			_listener.TestObservableProperty(_vm, () => _vm.Position);
			Assert.AreEqual(0, _listener.GetNumNotifications(), "Unexpected side effect notifications.");
		}


		[Test]
		public void TestSelectedUnitProperty()
		{
			_listener.TestObservableProperty(
				_vm,						 
				() => _vm.SelectedUnit,
				 uom =>
				 {
					 if ((UnitOfMeasure)uom == UnitOfMeasure.Meter)
					 {
						 return UnitOfMeasure.Micrometer;
					 }

					 return UnitOfMeasure.Meter;
				 });

			Assert.AreEqual(0, _listener.GetNumNotifications(), "Unexpected side effect notifications.");
		}


		private IZaberPort _port;
		private ZaberDevice _device;
		private Conversation _conversation;
		private DeviceVM _vm;
		private ObservableObjectTester _listener;
	}
}

﻿using System;
using System.Windows.Input;
using NUnit.Framework;
using Zaber;
using Zaber.Testing;
using ZaberConsole.DeviceList;
using ZaberTest;
using ZaberWpfToolbox;

namespace ZaberConsoleGUITests.DeviceList
{
	using CommandGetter = Func<QuickControlsVM, ICommand>;

	[TestFixture]
	[SetCulture("en-US")]
	public class QuickControlsVMTests
	{
		[SetUp]
		public void Setup()
		{
			DispatcherStack.Push(new ImmediateDispatcher());

			_port = new MockPort();

			_device = new ZaberDevice
			{
				DeviceNumber = 1,
				DeviceType = new DeviceType() { FirmwareVersion = new FirmwareVersion(6, 31) }
			};

			_conversation = new Conversation(_device);
			_device.Port = _port;
			_vm = new QuickControlsVM(_conversation);
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
		}


		private void SetupAscii()
		{
			_port.IsAsciiMode = true;
			_vm = new QuickControlsVM(_conversation);
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
		}


		private void SetupBinary()
		{
			_vm = new QuickControlsVM(_conversation);
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
		}


		[TearDown]
		public void Teardown()
		{
			_listener?.Unlisten(_vm);
			_vm = null;

			DispatcherStack.Pop();
		}


		[Test]
		public void ObservableProperties()
		{
			SetupAscii();
			_listener.TestObservableProperty(_vm, () => _vm.ShowQuickControlsPopup);
			_listener.TestObservableProperty(_vm, () => _vm.QuickControlsPopupMessage);

			var changeCommand = new Func<ICommand, ICommand>(aCmd => new RelayCommand(_ => { }));

			_listener.TestObservableProperty(_vm, () => _vm.MoveLeftToEndCommand, changeCommand);
			_listener.TestObservableProperty(_vm, () => _vm.MoveLeftCommand, changeCommand);
			_listener.TestObservableProperty(_vm, () => _vm.JogLeftCommand, changeCommand);
			_listener.TestObservableProperty(_vm, () => _vm.StopCommand, changeCommand);
			_listener.TestObservableProperty(_vm, () => _vm.JogRightCommand, changeCommand);
			_listener.TestObservableProperty(_vm, () => _vm.MoveRightCommand, changeCommand);
			_listener.TestObservableProperty(_vm, () => _vm.MoveRightToEndCommand, changeCommand);
			_listener.TestObservableProperty(_vm, () => _vm.HomeCommand, changeCommand);
		}


		[Test]
		[TestCaseSource(nameof(_asciiCases))]
		public void AsciiButtonCommands(CommandGetter aCommandSource, string aExpected)
		{
			SetupAscii();
			_port.Expect($"/01 0 {aExpected}");
			_port.AddResponse("@01 0 OK IDLE -- 0");
			var command = aCommandSource(_vm);
			Assert.IsTrue(command.CanExecute(null));
			command.Execute(null);
			Assert.IsTrue(_vm.LastTask?.Wait(1000));
			_port.Verify();
		}


		[Test]
		[TestCaseSource(nameof(_binaryCases))]
		public void BinaryButtonCommands(CommandGetter aCommandSource, Command aExpected, int aData)
		{
			SetupBinary();
			_port.Expect(1, aExpected, aData);
			_port.AddResponse(1, aExpected, 0);
			var command = aCommandSource(_vm);
			Assert.IsTrue(command.CanExecute(null));
			command.Execute(null);
			Assert.IsTrue(_vm.LastTask?.Wait(1000));
			_port.Verify();
		}


		[Test]
		public void BinaryMoveMaxButtonCommand()
		{
			SetupBinary();
			_port.Expect(1, Command.ReturnSetting, (int)Command.SetMaximumPosition);
			_port.AddResponse(1, Command.SetMaximumPosition, 100000);
			_port.Expect(1, Command.MoveAbsolute, 100000);
			_port.AddResponse(1, Command.MoveAbsolute, 100000);
			Assert.IsTrue(_vm.MoveRightToEndCommand.CanExecute(null));
			_vm.MoveRightToEndCommand.Execute(null);
			Assert.IsTrue(_vm.LastTask?.Wait(1000));
			_port.Verify();
		}


		private static readonly object[] _asciiCases = new object[]
		{
			new object[] { new CommandGetter(aVM => aVM.StopCommand), "stop"},
			new object[] { new CommandGetter(aVM => aVM.HomeCommand), "home"},
			new object[] { new CommandGetter(aVM => aVM.MoveLeftCommand), "move vel -5000"},
			new object[] { new CommandGetter(aVM => aVM.MoveLeftToEndCommand), "move min"},
			new object[] { new CommandGetter(aVM => aVM.MoveRightCommand), "move vel 5000"},
			new object[] { new CommandGetter(aVM => aVM.MoveRightToEndCommand), "move max"}
		};


		private static readonly object[] _binaryCases = new object[]
		{
			new object[] { new CommandGetter(aVM => aVM.StopCommand), Command.Stop, 0},
			new object[] { new CommandGetter(aVM => aVM.HomeCommand), Command.Home, 0},
			new object[] { new CommandGetter(aVM => aVM.MoveLeftCommand), Command.MoveAtConstantSpeed, -5000},
			new object[] { new CommandGetter(aVM => aVM.MoveLeftToEndCommand), Command.MoveAbsolute, 0},
			new object[] { new CommandGetter(aVM => aVM.MoveRightCommand), Command.MoveAtConstantSpeed, 5000}
		};


		private MockPort _port;
		private ZaberDevice _device;
		private Conversation _conversation;
		private QuickControlsVM _vm;
		private ObservableObjectTester _listener;


	}
}

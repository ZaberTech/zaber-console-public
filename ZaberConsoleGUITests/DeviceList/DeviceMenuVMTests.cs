﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using NUnit.Framework;
using Rhino.Mocks;
using Zaber;
using Zaber.Testing;
using ZaberConsole.DeviceList;
using ZaberConsole.Dialogs.Calibration;
using ZaberConsole.Dialogs.FirmwareUpdater;
using ZaberTest;

namespace ZaberConsoleGUITests.DeviceList
{
	[TestFixture]
	[SetCulture("en-US")]
	public class DeviceMenuVMTests
	{
		[SetUp]
		public void Setup()
		{
			DispatcherStack.Push(new ImmediateDispatcher());
			_port = new MockPort();
			_port.IsAsciiMode = true;

			_portFacade = new ZaberPortFacade
			{
				DefaultDeviceType = new DeviceType { Commands = new List<CommandInfo>() },
				Port = _port,
				QueryTimeout = 100
			};

			var peripheral0 = new DeviceType
			{
				PeripheralId = 0,
				Commands = new List<CommandInfo>()
			};

			var XMCB1 = new DeviceType
			{
				DeviceId = 30211,
				FirmwareVersion = new FirmwareVersion(7, 10),
				Name = "X-MCB1 Controller",
				MotionType = MotionType.None,
				PeripheralMap = new Dictionary<int, DeviceType> { { 0, peripheral0 } }
			};

			XMCB1.Capabilities.Add(Capability.EncoderDirect);

			var LHM = new DeviceType
			{
				DeviceId = 50081,
				FirmwareVersion = new FirmwareVersion(6, 10),
				PeripheralMap = new Dictionary<int, DeviceType>(),
				Name = "X-LHM100A Stage",
				MotionType = MotionType.Linear
			};

			_portFacade.AddDeviceType(XMCB1);
			_portFacade.AddDeviceType(LHM);

			ZaberPortFacadeTest.SetAsciiExpectations(_portFacade, _port, XMCB1, LHM);
			_portFacade.Open("COM1");
		}


		[TearDown]
		public void Teardown()
		{
			DispatcherStack.Pop();
		}


		[Test]
		public void FirmwareUpdaterPresent()
		{
			var conv = _portFacade.GetConversation(1);
			Assert.IsTrue(conv.Device.CanUpdateFirmware, "Sanity check");
			AddCalibrationExpectations(conv.Device, false);
			var vm = new DeviceMenuVM(_portFacade, conv);
			vm.Task?.Wait();
			var menuItem =
				vm.MenuItems.First(item => item.Title == ZaberConsole.Properties.Resources.LABEL_UPDATE_FIRMWARE);
			Assert.IsTrue(menuItem.ToolTip == ZaberConsole.Properties.Resources.TIP_UPDATE_FIRMWARE);
		}


		[Test]
		public void FirmwareUpdaterAbsent()
		{
			var conv = _portFacade.GetConversation(1, 1);
			Assert.IsFalse(conv.Device.CanUpdateFirmware, "Sanity check");
			var vm = new DeviceMenuVM(_portFacade, conv);
			vm.Task?.Wait();
			Assert.IsFalse(
				vm.MenuItems.Any(item => item.Title == ZaberConsole.Properties.Resources.LABEL_UPDATE_FIRMWARE));
		}


		[Test]
		public void FirmwareUpdaterCommand()
		{
			var conv = _portFacade.GetConversation(1);
			AddCalibrationExpectations(conv.Device, false);
			var vm = new DeviceMenuVM(_portFacade, conv);
			vm.Task?.Wait();
			var menuItem =
				vm.MenuItems.First(item => item.Title == ZaberConsole.Properties.Resources.LABEL_UPDATE_FIRMWARE);

			bool called = false;

			vm.RequestDialogOpen += (aSender, aArgs) =>
			{
				called = true;
				Assert.AreEqual(vm, aSender);
				Assert.IsTrue(aArgs is FirmwareUpdateDialogVM);
			};

			Assert.IsNotNull(menuItem.Command);
			Assert.IsTrue(menuItem.Command.CanExecute(null));
			menuItem.Command.Execute(null);
			Assert.IsTrue(called);
		}


		[Test]
		public void CalibrationPresent()
		{
			var conv = _portFacade.GetConversation(1);
			Assert.IsTrue(conv.Device.CanStoreCalibration, "Sanity check");
			AddCalibrationExpectations(conv.Device, true);
			var vm = new DeviceMenuVM(_portFacade, conv);
			vm.Task?.Wait();
			var menuItem =
				vm.MenuItems.First(item => item.Title == ZaberConsole.Properties.Resources.LABEL_OPEN_CALIBRATION);
			Assert.IsTrue(menuItem.ToolTip == ZaberConsole.Properties.Resources.TIP_OPEN_CALIBRATION);
		}


		[Test]
		public void CalibrationAbsent()
		{
			var conv = _portFacade.GetConversation(2);
			Assert.IsFalse(conv.Device.CanStoreCalibration, "Sanity check");
			AddCalibrationExpectations(conv.Device, false);
			var vm = new DeviceMenuVM(_portFacade, conv);
			vm.Task?.Wait();
			Assert.IsFalse(
				vm.MenuItems.Any(item => item.Title == ZaberConsole.Properties.Resources.LABEL_OPEN_CALIBRATION));
		}


		[Test]
		public void CalibrationCommand()
		{
			var conv = _portFacade.GetConversation(1);
			AddCalibrationExpectations(conv.Device, false);
			var vm = new DeviceMenuVM(_portFacade, conv);
			vm.Task?.Wait();
			var menuItem =
				vm.MenuItems.First(item => item.Title == ZaberConsole.Properties.Resources.LABEL_OPEN_CALIBRATION);

			bool called = false;

			vm.RequestDialogOpen += (aSender, aArgs) =>
			{
				called = true;
				Assert.AreEqual(vm, aSender);
				Assert.IsTrue(aArgs is CalibrationDialogVM);
			};

			AddCalibrationExpectations(conv.Device, true);

			Assert.IsNotNull(menuItem.Command);
			Assert.IsTrue(menuItem.Command.CanExecute(null));
			menuItem.Command.Execute(null);
			Assert.IsTrue(called);
			vm.Task?.Wait();
		}


		[Test]
		public void AlertCalibrationAbsent_OnAxis()
		{
			var conv = _portFacade.GetConversation(2);
			conv.Device.DeviceType.FirmwareVersion = new FirmwareVersion(7, 10);
			conv.Device.DeviceType.Capabilities.Add(Capability.EncoderDirect);
			Assert.IsTrue(conv.Device.CanStoreCalibration, "Sanity check");
			Assert.IsTrue(conv.Device.ShouldBeCalibrated, "Sanity check");
			AddCalibrationExpectations(conv.Device, false);

			var vm = new DeviceMenuVM(_portFacade, conv);
			vm.Task?.Wait();
			Assert.IsTrue(vm.ShowAlert);

			var menuItem =
				vm.MenuItems.First(item => item.Title == ZaberConsole.Properties.Resources.LABEL_OPEN_CALIBRATION);
			Assert.IsFalse(string.IsNullOrEmpty(menuItem.IconUrl));
			Assert.AreEqual(ZaberConsole.Properties.Resources.TIP_NEEDS_CALIBRATION, menuItem.ToolTip);
		}


		[Test]
		public void AlertCalibrationAbsent_OnController()
		{
			var controller = _portFacade.GetConversation(1);
			var peripheral = _portFacade.GetConversation(1, 1);
			controller.Device.DeviceType.FirmwareVersion = new FirmwareVersion(6, 31);
			peripheral.Device.DeviceType.Name = "X-LSQ-DE50";
			Assert.IsTrue(controller.Device.CanStoreCalibration, "Sanity check");
			Assert.IsTrue(peripheral.Device.ShouldBeCalibrated, "Sanity check");
			AddCalibrationExpectations(controller.Device, false);

			var vm = new DeviceMenuVM(_portFacade, controller);
			vm.Task?.Wait();
			Assert.IsTrue(vm.ShowAlert);

			var menuItem =
				vm.MenuItems.First(item => item.Title == ZaberConsole.Properties.Resources.LABEL_OPEN_CALIBRATION);
			Assert.IsFalse(string.IsNullOrEmpty(menuItem.IconUrl));
			Assert.IsTrue(menuItem.ToolTip.Contains("should be calibrated"));
		}


		private void AddCalibrationExpectations(ZaberDevice aDevice, bool aCalibrationExists)
		{
			if (aDevice.CanStoreCalibration)
			{
				_port.Expect($"/{aDevice.DeviceNumber:00} {aDevice.AxisNumber} get calibration.type");
				_port.AddResponse($"@{aDevice.DeviceNumber:00} {aDevice.AxisNumber} OK IDLE -- {(aCalibrationExists ? '1' : '0')}");
			}
			else
			{
				_port.Expect($"/{aDevice.DeviceNumber:00} 0 calibration list");
				_port.AddResponse($"@{aDevice.DeviceNumber:00} 0 OK IDLE -- 0");
				if (aCalibrationExists)
				{
					_port.AddResponse($"#{aDevice.DeviceNumber:00} 0 serial {aDevice.SerialNumber}");
				}
			}
		}


		private MockPort _port;
		private ZaberPortFacade _portFacade;
	}
}

﻿using System;
using System.Collections.ObjectModel;
using NUnit.Framework;
using Zaber.Testing;
using ZaberConsole.Dialogs.Calibration;

namespace ZaberConsoleGUITests.Dialogs.Calibration
{
	[TestFixture]
	[SetCulture("en-US")]
	public class CalibrationTableTests
	{
		[SetUp]
		public void Setup() => _table = new CalibrationTableVM();


		[TearDown]
		public void TearDown() => _listener?.Unlisten(_table);


		[Test]
		public void TestDefaults()
		{
			Assert.AreEqual(1, _table.DataVersion);
			Assert.AreEqual(DateTime.Now.DayOfYear, _table.TimeStamp.DayOfYear);
			Assert.AreEqual(0, _table.SerialNumber);
			Assert.AreEqual(0, _table.CalibrationType);
			Assert.AreEqual(0, _table.TableWidth);
			Assert.AreEqual("Empty Table", _table.Description);
			Assert.IsNotNull(_table.Data);
			Assert.AreEqual(0, _table.Data.Count);
		}


		[Test]
		public void TestObservableProperties()
		{
			_listener = new ObservableObjectTester();
			_listener.Listen(_table);
			_listener.TestObservableProperty(_table, () => _table.DataVersion);
			_listener.TestObservableProperty(_table,
											 () => _table.TimeStamp,
											 date => { return date + new TimeSpan(1, 0, 0); });

			_listener.TestObservableProperty(_table, () => _table.SerialNumber);
			_listener.TestObservableProperty(_table, () => _table.CalibrationType);
			_listener.TestObservableProperty(_table, () => _table.TableWidth);
			_listener.TestObservableProperty(_table, () => _table.Description);
			_listener.TestObservableProperty(_table,
											 () => _table.Data,
											 list =>
											 {
												 var newList = new ObservableCollection<int>();
												 if (list.Count != 1)
												 {
													 newList.Add(0);
												 }

												 return newList;
											 });
		}


		private CalibrationTableVM _table;
		private ObservableObjectTester _listener;
	}
}

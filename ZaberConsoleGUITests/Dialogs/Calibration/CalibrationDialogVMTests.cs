﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using NUnit.Framework;
using Zaber;
using Zaber.Testing;
using ZaberConsole.Dialogs.Calibration;
using ZaberTest;

namespace ZaberConsoleGUITests.Dialogs.Calibration
{
	[TestFixture]
	[SetCulture("en-US")]
	public class CalibrationDialogVMTests
	{
		[SetUp]
		public void Setup()
		{
			var port = CreatePort(true);
			_vm = new CalibrationDialogVM(port, null);
		}


		[TearDown]
		public void TearDown() => _listener?.Unlisten(_vm);


		[Test]
		public void TestDefaults()
		{
			Assert.AreEqual(0, _vm.DeviceTables.Count);
			Assert.IsFalse(_vm.Busy);

			var command = _vm.WindowClosingCommand;
			Assert.IsNotNull(command);
			Assert.IsTrue(command.CanExecute(null));

			command = _vm.WindowLoadedCommand;
			Assert.IsNotNull(command);
			Assert.IsTrue(command.CanExecute(null));

			command = _vm.LoadCsvCommand;
			Assert.IsNotNull(command);
			Assert.IsTrue(command.CanExecute(null));

			command = _vm.SaveCsvCommand;
			Assert.IsNotNull(command);
			Assert.IsFalse(command.CanExecute(null));

			Assert.IsNull(_vm.Exception);
		}


		[Test]
		public void TestNonObservableProperties()
		{
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
			_listener.TestNonObservableProperty(_vm, () => _vm.Exception, null, new Exception());
		}


		[Test]
		public void TestObservableProperties()
		{
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
			_listener.TestObservableProperty(_vm,
											 () => _vm.DeviceTables,
											 list =>
											 {
												 var newList = new ObservableCollection<CalibrationTableVM>();
												 if (list.Count != 1)
												 {
													 newList.Add(new CalibrationTableVM());
												 }

												 return newList;
											 });

			_listener.TestObservableProperty(_vm, () => _vm.Busy);
		}


		// TODO centralize this to avoid code duplication.
		private ZaberPortFacade CreatePort(bool aAsciiMode)
		{
			var mockPort = new MockPort();
			mockPort.IsAsciiMode = aAsciiMode;

			var defaultDeviceType = new DeviceType { Commands = new List<CommandInfo>() };

			var portFacade = new ZaberPortFacade
			{
				DefaultDeviceType = defaultDeviceType,
				Port = mockPort,
				QueryTimeout = 100
			};

			var XMCB2 = new DeviceType
			{
				DeviceId = 30222,
				FirmwareVersion = new FirmwareVersion(6, 22),
				PeripheralMap = new Dictionary<int, DeviceType>(),
				Name = "X-MCB2 Controller",
				MotionType = MotionType.Other
			};

			portFacade.AddDeviceType(XMCB2);

			if (aAsciiMode)
			{
				ZaberPortFacadeTest.SetAsciiExpectations(portFacade, mockPort, XMCB2);
			}
			else
			{
				ZaberPortFacadeTest.SetBinaryExpectations(portFacade, mockPort);
			}

			return portFacade;
		}


		private CalibrationDialogVM _vm;
		private ObservableObjectTester _listener;
	}
}

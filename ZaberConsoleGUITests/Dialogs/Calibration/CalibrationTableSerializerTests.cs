﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;
using ZaberConsole.Dialogs.Calibration;
using ZaberTest;
using ZaberWpfToolbox.Dialogs;

namespace ZaberConsoleGUITests.Dialogs.Calibration
{
	[TestFixture]
	[SetCulture("en-US")]
	public class CalibrationTableSerializerTests
	{
		[SetUp]
		public void Setup()
		{
			if (File.Exists(OUTPUT_FILE))
			{
				File.Delete(OUTPUT_FILE);
			}

			_resolvedInputPath = TestDataFileHelper.GetDeployedPath(INPUT_FILE);
		}


		[TearDown]
		public void Teardown()
		{
			if (File.Exists(OUTPUT_FILE))
			{
				File.Delete(OUTPUT_FILE);
			}
		}


		[Test]
		[Description("Test prompting to save calibration data to a CSV file.")]
		public void PromptWriteTest()
		{
			var tables = new List<CalibrationTableVM>();
			for (var i = 0; i < 5; ++i)
			{
				tables.Add(MakeRandomTable());
			}

			Assert.IsFalse(File.Exists(OUTPUT_FILE));
			CalibrationTableSerializer.PromptSaveToCsv(Path.GetTempPath(),
													   tables,
													   aVM =>
													   {
														   var dlg = aVM as FileBrowserDialogParams;
														   var mbvm = aVM as MessageBoxParams;
														   if (null != dlg)
														   {
															   Assert.AreEqual(
																   Path.GetTempPath(),
																   dlg.InitialDirectory);
															   dlg.Filename = OUTPUT_FILE;
															   dlg.DialogResult = true;
														   }
														   else if (null != mbvm)
														   {
															   Assert.IsTrue(mbvm.Message.Contains(OUTPUT_FILE));
														   }
														   else
														   {
															   Assert.Fail("Unexpected callback");
														   }
													   });

			Assert.IsTrue(File.Exists(OUTPUT_FILE));
			var fi = new FileInfo(OUTPUT_FILE);
			Assert.Greater(fi.Length, 1);

			// Try reading the file back in.
			var list = new List<CalibrationTableVM>(CalibrationTableSerializer.LoadFromCsv(OUTPUT_FILE));
			Assert.AreEqual(tables.Count, list.Count);
			for (var i = 0; i < tables.Count; ++i)
			{
				Assert.AreEqual(tables[i].DataVersion, list[i].DataVersion);
				Assert.AreEqual(tables[i].Description, list[i].Description);
				Assert.AreEqual(tables[i].CalibrationType, list[i].CalibrationType);
				Assert.AreEqual(tables[i].Data.Count, list[i].Data.Count);
				Assert.AreEqual(tables[i].SerialNumber, list[i].SerialNumber);
				Assert.AreEqual(tables[i].TableWidth, list[i].TableWidth);
				Assert.AreEqual(tables[i].TableOffset, list[i].TableOffset);
				Assert.AreEqual(tables[i].TimeStamp, list[i].TimeStamp);
				Assert.AreEqual(Path.GetFileName(OUTPUT_FILE), list[i].Filename);
				for (var j = 0; j < 64; ++j)
				{
					Assert.AreEqual(tables[i].Data[j], list[i].Data[j]);
				}
			}
		}


		[Test]
		[Description("Test reading calibration from FW6-compatible CSV file.")]
		public void ReadTest_FW6()
		{
			var fw6FilePath = TestDataFileHelper.GetDeployedPath(FW6_INPUT_FILE);
			var results = CalibrationTableSerializer.LoadFromCsv(fw6FilePath);
			Assert.IsNotNull(results);
			var list = results.ToList();
			ReadTest_Common(list);

			Assert.AreEqual(Path.GetFileName(fw6FilePath), list[0].Filename);
			Assert.AreEqual(Path.GetFileName(fw6FilePath), list[1].Filename);
			Assert.AreEqual(0, list[0].TableOffset);
			Assert.AreEqual(0, list[1].TableOffset);
		}


		[Test]
		[Description("Test reading calibration from FW7-compatible CSV file.")]
		public void ReadTest_FW7()
		{
			var results = CalibrationTableSerializer.LoadFromCsv(_resolvedInputPath);
			Assert.IsNotNull(results);
			var list = results.ToList();
			ReadTest_Common(list);

			Assert.AreEqual(Path.GetFileName(_resolvedInputPath), list[0].Filename);
			Assert.AreEqual(Path.GetFileName(_resolvedInputPath), list[1].Filename);
			Assert.AreEqual(124, list[0].TableOffset);
			Assert.AreEqual(525, list[1].TableOffset);
		}


		[Test]
		[Description("Test writing to a CSV file.")]
		public void WriteTest()
		{
			var tables = new List<CalibrationTableVM>();
			for (var i = 0; i < 5; ++i)
			{
				tables.Add(MakeRandomTable());
			}

			Assert.IsFalse(File.Exists(OUTPUT_FILE));
			CalibrationTableSerializer.SaveToCsv(OUTPUT_FILE, tables);
			Assert.IsTrue(File.Exists(OUTPUT_FILE));
			var fi = new FileInfo(OUTPUT_FILE);
			Assert.Greater(fi.Length, 1);

			// Try reading the file back in.
			var list = new List<CalibrationTableVM>(CalibrationTableSerializer.LoadFromCsv(OUTPUT_FILE));
			Assert.AreEqual(tables.Count, list.Count);
			for (var i = 0; i < tables.Count; ++i)
			{
				Assert.AreEqual(tables[i].DataVersion, list[i].DataVersion);
				Assert.AreEqual(tables[i].Description, list[i].Description);
				Assert.AreEqual(tables[i].CalibrationType, list[i].CalibrationType);
				Assert.AreEqual(tables[i].Data.Count, list[i].Data.Count);
				Assert.AreEqual(tables[i].SerialNumber, list[i].SerialNumber);
				Assert.AreEqual(tables[i].TableWidth, list[i].TableWidth);
				Assert.AreEqual(tables[i].TableOffset, list[i].TableOffset);
				Assert.AreEqual(tables[i].TimeStamp, list[i].TimeStamp);
				Assert.AreEqual(Path.GetFileName(OUTPUT_FILE), list[i].Filename);
				for (var j = 0; j < 64; ++j)
				{
					Assert.AreEqual(tables[i].Data[j], list[i].Data[j]);
				}
			}
		}


		private void ReadTest_Common(IEnumerable<CalibrationTableVM> aResults)
		{
			var results = aResults.ToArray();
			Assert.AreEqual(2, results.Length);

			Assert.AreEqual(1, results[0].DataVersion);
			Assert.AreEqual("Test 1", results[0].Description);
			Assert.AreEqual(1, results[0].CalibrationType);
			Assert.AreEqual(64, results[0].Data.Count);
			Assert.AreEqual(99999, results[0].SerialNumber);
			Assert.AreEqual(15, results[0].TableWidth);
			Assert.AreEqual(new DateTime(2016, 3, 29), results[0].TimeStamp);
			Assert.IsFalse(results[0].IsSelected);

			Assert.AreEqual(1, results[1].DataVersion);
			Assert.AreEqual("Test 2", results[1].Description);
			Assert.AreEqual(1, results[1].CalibrationType);
			Assert.AreEqual(64, results[1].Data.Count);
			Assert.AreEqual(99998, results[1].SerialNumber);
			Assert.AreEqual(15, results[1].TableWidth);
			Assert.AreEqual(new DateTime(2016, 3, 29), results[1].TimeStamp);
			Assert.IsFalse(results[1].IsSelected);
		}


		private CalibrationTableVM MakeRandomTable()
		{
			var r = new Random();
			var table = new CalibrationTableVM();

			table.DataVersion = 1;
			table.Description = r.Next().ToString();
			table.CalibrationType = r.Next();
			table.SerialNumber = (uint) r.Next();
			table.TableWidth = r.Next();
			table.TableOffset = r.Next();
			table.TimeStamp = new DateTime(r.Next(1990, 2010), r.Next(1, 11), r.Next(1, 28));
			for (var i = 0; i < 64; ++i)
			{
				table.Data.Add((byte) r.Next());
			}

			return table;
		}


		private static readonly string OUTPUT_FILE =
			Path.Combine(Path.GetTempPath(), "CalibrationTableSerializerTests_temp.csv");

		private static readonly string FW6_INPUT_FILE = "Dialogs\\Calibration\\LRQ300DL-E01T3.csv";
		private static readonly string INPUT_FILE = "Dialogs\\Calibration\\LRQ300DL-E01T3_FW7.csv";
		private string _resolvedInputPath;
	}
}

﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using NUnit.Framework;
using Zaber.Testing;
using ZaberConsole;
using ZaberConsole.Properties;
using ZaberTest;
using ZaberTest.Testing;
using ZaberWpfToolbox.Dialogs;

namespace ZaberConsoleGUITests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class SoftwareUpdaterTests
	{
		[SetUp]
		public void Setup()
		{
			DispatcherStack.Push(new ImmediateDispatcher());
			BackgroundWorkerStack.Push(typeof(ImmediateForegroundWorker));

			_dataDir = Path.Combine(TestDataFileHelper.GetDeployedPath("."), "_programUpdaterTests_data");
			if (File.Exists(_dataDir))
			{
				File.Delete(_dataDir);
			}

			Directory.CreateDirectory(_dataDir);

			_webDir = Path.Combine(TestDataFileHelper.GetDeployedPath("."), "_programUpdaterTests_web");
			Directory.CreateDirectory(_webDir);

			var webFileName = string.Format(FILENAME_FORMAT, "1.0.0.0");
			_webFile = Path.Combine(_webDir, webFileName);
			File.Copy(TestDataFileHelper.GetDeployedPath(Path.Combine("data", SOURCE_FILENAME)),
					  _webFile,
					  true);
		}


		[TearDown]
		public void Teardown()
		{
			while (_vm.IsBusy)
			{
				Thread.Sleep(100);
			}
			_vm.Shutdown();

			BackgroundWorkerStack.Pop();
			DispatcherStack.Pop();

			if (null != _vm)
			{
				_listener?.Unlisten(_vm);
			}

			var success = false;
			var tries = 0;
			while (!success)
			{
				TempFileManager.TryDeleteFiles();
				try
				{
					if (Directory.Exists(_dataDir))
					{
						FileSystemHelper.DeleteDirectoryRecursive(_dataDir);
					}
					else if (File.Exists(_dataDir))
					{
						File.Delete(_dataDir);
					}

					FileSystemHelper.DeleteDirectoryRecursive(_webDir);
					success = true;
				}
				catch (IOException)
				{
					Thread.Sleep(100);
					tries++;
				}

				if (tries > 500)
				{
					throw new IOException("Cannot delete temporary test files!");
				}
			}
		}


		[Test]
		public void RM6247_TestMissingDownloadDir()
		{
			FileSystemHelper.DeleteDirectoryRecursive(_dataDir);

			// Before the main fix this would have thrown an exception because the dir didn't exist.
			CreateVM();

			// If we can create the directory, it should now exist.
			Assert.IsTrue(Directory.Exists(_dataDir));
			Assert.AreEqual(SoftwareUpdaterVM.State.UpToDate, _vm.CurrentState);
		}


		[Test]
		public void RM6247_TestMissingDownloadDirAndCannotCreate()
		{
			FileSystemHelper.DeleteDirectoryRecursive(_dataDir);

			// Prevent the directory from being created.
			using (File.Create(_dataDir))
			{
				// Before the fix's additional error handling this would have thrown an exception also.
				CreateVM();
			}

			// If we can't create the dir, the updater should disable itself.
			Assert.AreEqual(SoftwareUpdaterVM.State.Error, _vm.CurrentState);
			Assert.IsFalse(_vm.ButtonCommand.CanExecute(null));
		}


		[Test]
		public void TestChangeAutoUpdateSetting()
		{
			CreateVM();
			CreateListener();
			Assert.AreEqual(_settings.AutomaticallyCheckForUpdates, _vm.AutoCheckForUpdates);
			_listener.ClearNotifications();
			_vm.AutoCheckForUpdates = !_vm.AutoCheckForUpdates;
			Assert.AreEqual(_settings.AutomaticallyCheckForUpdates, _vm.AutoCheckForUpdates);
			Assert.IsTrue(_listener.PropertyWasChanged(nameof(_vm.AutoCheckForUpdates)));
			_listener.ClearNotifications();
			_vm.AutoCheckForUpdates = !_vm.AutoCheckForUpdates;
			Assert.AreEqual(_settings.AutomaticallyCheckForUpdates, _vm.AutoCheckForUpdates);
			Assert.IsTrue(_listener.PropertyWasChanged(nameof(_vm.AutoCheckForUpdates)));
		}


		[Test]
		public void TestDefaults()
		{
			CreateVM();
			Assert.IsFalse(_vm.IsBusy);
			Assert.AreEqual(1.0, _vm.ProgressMaximum);
			Assert.AreEqual(0.0, _vm.Progress);
			Assert.IsTrue(string.IsNullOrEmpty(_vm.ErrorMessage));
			Assert.AreEqual(SoftwareUpdaterVM.State.UpToDate, _vm.CurrentState);
			Assert.IsNotNull(_vm.ButtonCommand);
			Assert.IsTrue(_vm.ButtonCommand.CanExecute(null));
		}


		[Test]
		public void TestInitWithAutoUpdateDisabled()
		{
			_settings.AutomaticallyCheckForUpdates = false;
			CreateVM();
			Assert.AreEqual(SoftwareUpdaterVM.State.Uncertain, _vm.CurrentState);
		}


		[Test]
		public void TestDetectAlreadyHaveNewInstaller()
		{
			var sourceFile = TestDataFileHelper.GetDeployedPath(Path.Combine("data", SOURCE_FILENAME));
			var keeper = Path.Combine(_dataDir, string.Format(FILENAME_FORMAT, "1.0.0.2"));
			File.Copy(sourceFile, keeper, true);

			CreateVMWrapper(new Version(1, 0, 0, 1), new Version(1, 0, 0, 2));
			Assert.AreEqual(SoftwareUpdaterVM.State.UpdateReady, _vm.CurrentState);

			_vm.CheckForUpdates();
			Assert.AreEqual(SoftwareUpdaterVM.State.UpdateReady, _vm.CurrentState);
		}


		[Test]
		public void TestCheckTriggeredWhenAutoCheckEnabled()
		{
			_settings.AutomaticallyCheckForUpdates = false;
			CreateVM();
			Assert.IsFalse(_vm.AutoCheckForUpdates, "Precondition check");
			Assert.AreEqual(SoftwareUpdaterVM.State.Uncertain, _vm.CurrentState);
			CreateListener();

			_vm.AutoCheckForUpdates = true;
			Assert.IsTrue(_listener.PropertyWasChanged(nameof(_vm.CurrentState)));
			Assert.AreEqual(SoftwareUpdaterVM.State.UpdateAvailable, _vm.CurrentState);
		}


		[Test]
		public void TestDetectHaveLatestVersion()
		{
			CreateVMWrapper(new Version(1, 0, 0, 1), new Version(1, 0, 0, 0));
			Assert.AreEqual(SoftwareUpdaterVM.State.UpToDate, _vm.CurrentState);

			var numStateChanges = 0;
			PropertyChangedEventHandler stateWatcher = (_, aArgs) =>
			{
				if (nameof(_vm.CurrentState) == aArgs.PropertyName)
				{
					if (0 == numStateChanges)
					{
						Assert.AreEqual(SoftwareUpdaterVM.State.CheckingForUpdates, _vm.CurrentState);
					}
					else if (1 == numStateChanges)
					{
						Assert.AreEqual(SoftwareUpdaterVM.State.UpToDate, _vm.CurrentState);
					}
					else
					{
						Assert.Fail("Unexpected state change.");
					}

					numStateChanges++;
				}
			};

			_vm.PropertyChanged += stateWatcher;

			// Click the check for updates button.
			Assert.IsTrue(_vm.ButtonCommand.CanExecute(null));
			_vm.ButtonCommand.Execute(null);

			_vm.PropertyChanged -= stateWatcher;
		}


		[Test]
		public void TestDetectNewVersionAvailable()
		{
			CreateVMWrapper(new Version(1, 0, 0, 1), new Version(1, 0, 0, 2));
			Assert.AreEqual(SoftwareUpdaterVM.State.UpToDate, _vm.CurrentState);

			var numStateChanges = 0;
			PropertyChangedEventHandler stateWatcher = (_, aArgs) =>
			{
				if (nameof(_vm.CurrentState) == aArgs.PropertyName)
				{
					if (0 == numStateChanges)
					{
						Assert.AreEqual(SoftwareUpdaterVM.State.CheckingForUpdates, _vm.CurrentState);
					}
					else if (1 == numStateChanges)
					{
						Assert.AreEqual(SoftwareUpdaterVM.State.UpdateAvailable, _vm.CurrentState);
					}
					else
					{
						Assert.Fail("Unexpected state change.");
					}

					numStateChanges++;
				}
			};

			_vm.PropertyChanged += stateWatcher;

			// Click the check for updates button.
			Assert.IsTrue(_vm.ButtonCommand.CanExecute(null));
			_vm.ButtonCommand.Execute(null);

			_vm.PropertyChanged -= stateWatcher;
		}


		[Test]
		public void TestDownloadNewVersion()
		{
			// Deploy a newer download.
			var sourceFile = TestDataFileHelper.GetDeployedPath(Path.Combine("data", SOURCE_FILENAME));
			var destFileName = string.Format(FILENAME_FORMAT, "1.2.3.4");
			File.Copy(sourceFile, Path.Combine(_webDir, destFileName), true);
			Assert.AreEqual(0, CountFilesInDownloadDir());

			CreateVMWrapper(new Version(1, 1, 3, 4), new Version(1, 2, 3, 4));
			Assert.AreEqual(SoftwareUpdaterVM.State.UpToDate, _vm.CurrentState);

			// Click the check for updates button.
			Assert.IsTrue(_vm.ButtonCommand.CanExecute(null));
			_vm.ButtonCommand.Execute(null);
			Assert.AreEqual(SoftwareUpdaterVM.State.UpdateAvailable, _vm.CurrentState);

			var numStateChanges = 0;
			PropertyChangedEventHandler stateWatcher = (_, aArgs) =>
			{
				if (nameof(_vm.CurrentState) == aArgs.PropertyName)
				{
					if (0 == numStateChanges)
					{
						Assert.AreEqual(SoftwareUpdaterVM.State.Downloading, _vm.CurrentState);
					}
					else if (1 == numStateChanges)
					{
						Assert.AreEqual(SoftwareUpdaterVM.State.UpdateReady, _vm.CurrentState);
					}
					else
					{
						Assert.Fail("Unexpected state change.");
					}

					numStateChanges++;
				}
			};

			_vm.PropertyChanged += stateWatcher;

			// Click the download button.
			Assert.IsTrue(_vm.ButtonCommand.CanExecute(null));
			_vm.ButtonCommand.Execute(null);

			// Because downloading is async third-party code, we just have to wait for it.
			var waitCount = 0;
			while (_vm.IsBusy)
			{
				Thread.Sleep(100);
				waitCount++;
				Assert.Less(waitCount, 500, "Download took too long.");
			}

			_vm.PropertyChanged -= stateWatcher;

			// Verify the file was downloaded.
			Assert.AreEqual(1, CountFilesInDownloadDir());
			Assert.IsTrue(File.Exists(Path.Combine(_dataDir, destFileName)));

			// Verify clicking the button prompts to launch the installer (but don't do it)
			RequestDialogChange dialogHandler = (_, aVM) =>
			{
				if (aVM is MessageBoxParams mb)
				{
					Assert.AreEqual(Resources.STR_SW_UPDATE_READY, mb.Message);
					mb.Result = MessageBoxResult.No;
				}
				else
				{
					Assert.Fail("Unexpected dialog box type.");
				}
			};

			_vm.RequestDialogOpen += dialogHandler;
			Assert.IsTrue(_vm.ButtonCommand.CanExecute(null));
			_vm.ButtonCommand.Execute(null);
			_vm.RequestDialogOpen -= dialogHandler;
		}


		[Test]
		public void TestNewerInstallerPreserved()
		{
			// Simulate having downloaded multiple installers for different versions.
			var sourceFile = TestDataFileHelper.GetDeployedPath(Path.Combine("data", SOURCE_FILENAME));
			var keeper = Path.Combine(_dataDir, string.Format(FILENAME_FORMAT, "4.3.2.1"));
			File.Copy(sourceFile, keeper, true);
			File.Copy(sourceFile, Path.Combine(_dataDir, string.Format(FILENAME_FORMAT, "1.2.3.4")), true);
			File.Copy(sourceFile, Path.Combine(_dataDir, string.Format(FILENAME_FORMAT, "4.3.2.0")), true);

			// Start the updater with an almost-latest version.
			CreateVM(new Version(4, 3, 2, 0));

			// Only the latest file should remain.
			Assert.AreEqual(1, CountFilesInDownloadDir());
			Assert.IsTrue(File.Exists(keeper));

			// And the button state should reflect that we are ready to install a new version.
			Assert.AreEqual(SoftwareUpdaterVM.State.UpdateReady, _vm.CurrentState);
		}


		[Test]
		public void TestOldInstallersCleanedUp()
		{
			// Simulate having downloaded multiple installers for different versions.
			var sourceFile = TestDataFileHelper.GetDeployedPath(Path.Combine("data", SOURCE_FILENAME));
			File.Copy(sourceFile, Path.Combine(_webDir, string.Format(FILENAME_FORMAT, "1.2.3.4")), true);
			File.Copy(sourceFile, Path.Combine(_webDir, string.Format(FILENAME_FORMAT, "4.3.2.1")), true);
			File.Copy(sourceFile, Path.Combine(_webDir, string.Format(FILENAME_FORMAT, "4.3.2.0")), true);

			// Start the updater with a later version than all files.
			CreateVM(new Version(4, 3, 2, 2));

			// All files should have been deleted.
			Assert.AreEqual(0, CountFilesInDownloadDir());

			// And the button should be ready to check for updates.
			Assert.AreEqual(SoftwareUpdaterVM.State.Uncertain, _vm.CurrentState);
		}


		// Overrides the nontestable methods of the software updater.
		private class SoftwareUpdaterWrapper : SoftwareUpdaterVM
		{
			#region -- Public Methods & Properties --

			public SoftwareUpdaterWrapper(string aDataDir, Version aCurrentVersion, SoftwareUpdaterSettings aSettings)
				: base(aDataDir, aCurrentVersion, aSettings)
			{
			}


			public Version LatestVersion { get; set; }

			public string WebDir { get; set; }

			#endregion

			#region -- Non-Public Methods --

			protected override Version GetLatestAvailableVersionNumber() => LatestVersion;


			protected override Uri GetDownloadUri(Version aVersionToDownload)
				=> new Uri(Path.Combine(WebDir, string.Format(FILENAME_FORMAT, aVersionToDownload)));

			#endregion
		}


		private void CreateVM(Version aVersion = null)
			=> _vm = new SoftwareUpdaterVM(_dataDir, aVersion ?? new Version(1, 2, 3, 4), _settings);


		private void CreateVMWrapper(Version aVersion, Version aLatestVersion) => _vm =
			new SoftwareUpdaterWrapper(_dataDir, aVersion, _settings)
			{
				LatestVersion = aLatestVersion,
				WebDir = _webDir
			};


		private void CreateListener()
		{
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
		}


		private int CountFilesInDownloadDir() => Directory.EnumerateFiles(_dataDir).Count();


		private DateTime GetFileTimestamp(string aPath) => File.GetLastWriteTimeUtc(aPath);


		private readonly SoftwareUpdaterSettings _settings = new SoftwareUpdaterSettings();
		private SoftwareUpdaterVM _vm;
		private ObservableObjectTester _listener;

		private string _dataDir;
		private string _webDir;
		private string _webFile;
		private static readonly string SOURCE_FILENAME = "fake_installer.exe";
		private static readonly string FILENAME_FORMAT = Configuration.InstallerBaseName + "-{0}.exe";
	}
}

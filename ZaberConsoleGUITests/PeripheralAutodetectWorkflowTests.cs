﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using Zaber;
using Zaber.Testing;
using ZaberConsole;
using ZaberConsole.DeviceList;
using ZaberTest;
using ZaberTest.Testing;

namespace ZaberConsoleGUITests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class PeripheralAutodetectWorkflowTests
	{
		[SetUp]
		public void Setup()
		{
			DispatcherStack.Push(new ImmediateDispatcher());
			for (int i = 0; i < 4; i++) // 4 pollers.
			{
				DispatcherTimerStack.Push(new MockDispatcherTimer());
			}
		}


		[TearDown]
		public void Teardown()
		{
			_workflow?.Stop();
			DispatcherTimerStack.Reset(TestContext.CurrentContext.Result.Outcome == ResultState.Success);
			DispatcherStack.Pop();
		}


		[Test]
		[Description("Check that the port is invalidated when a peripheral ID change is detected.")]
		public void InvalidateOnInitialConnection()
		{
			SetupAndOpenPort(CreateDeviceType(0));
			_portFacade.GetDevice(1).DeviceType.Capabilities.Add(Capability.AutoDetector);
			var view = new MockDeviceView(1, 1);
			InitializeWorkflow(view);

			var invalidationCount = 0;
			_portFacade.Invalidated += (_, __) => { invalidationCount++; };

			// Kicking the main poller when the peripheral ID is zero should trigger a peripheral ID poll.
			var msgIdPos = GetPollerMessageID("get pos");
			_mockPort.ReadExpectationsFromString($"/00 0 {msgIdPos:00} get pos");
			_mockPort.ReadExpectationsFromString($"@01 0 {msgIdPos:00} OK IDLE -- 0");
			var msgIdPid = GetPollerMessageID("get peripheral.id");
			_mockPort.ReadExpectationsFromString($"/01 0 {msgIdPid:00} get peripheral.id");
			_mockPort.ReadExpectationsFromString($"@01 0 {msgIdPid:00} OK IDLE -- 123");

			Assert.AreEqual(0, invalidationCount);
			PollAndWait("get pos");
			WaitForPollActionQueue();
			WaitForPollComplete("get peripheral.id");
			Assert.AreEqual(1, invalidationCount);
			_mockPort.Verify();
		}


		[Test]
		[Description("Check that peripheral ID is only polled on auto-detect capable controllers.")]
		public void PeripheralIdPolledOnSmartControllers()
		{
			SetupAndOpenPort(CreateDeviceType(0));
			var view = new MockDeviceView(1, 1);
			InitializeWorkflow(view);

			var msgIdPos = GetPollerMessageID("get pos");
			_mockPort.ReadExpectationsFromString($"/00 0 {msgIdPos:00} get pos");
			_mockPort.ReadExpectationsFromString($"@01 0 {msgIdPos:00} OK IDLE -- 0");
			PollAndWait("get pos");
			WaitForPollActionQueue();
			_mockPort.Verify();

			_portFacade.GetDevice(1).DeviceType.Capabilities.Add(Capability.AutoDetector);
			msgIdPos = GetPollerMessageID("get pos");
			_mockPort.ReadExpectationsFromString($"/00 0 {msgIdPos:00} get pos");
			_mockPort.ReadExpectationsFromString($"@01 0 {msgIdPos:00} OK IDLE -- 0");
			var msgIdPid = GetPollerMessageID("get peripheral.id");
			_mockPort.ReadExpectationsFromString($"/01 0 {msgIdPid:00} get peripheral.id");
			_mockPort.ReadExpectationsFromString($"@01 0 {msgIdPid:00} OK IDLE -- 0");

			PollAndWait("get pos");
			WaitForPollActionQueue();
			WaitForPollComplete("get peripheral.id");
			_mockPort.Verify();
		}


		[Test]
		[Description("Check that pending detectable peripheral ID and warnings are passed to the view when FZ is detected.")]
		public void ReadPendingId()
		{
			SetupAndOpenPort(CreateDeviceType(66034));
			_portFacade.GetDevice(1).Axes[0].DeviceType.Capabilities.Add(Capability.AutoDetect);
			var view = new MockDeviceView(1, 1);
			InitializeWorkflow(view);
			SetSynchronousMode(true);

			// The default poller mutex timeout of one second can cause the serial number
			// poll to be skipped in this test.
			SetContentionTimeouts(TimeSpan.FromSeconds(10));

			// Kicking the main poller when the peripheral ID is nonzero and FZ is present
			// should trigger polling the pending peripheral ID.
			var msgIdPos = GetPollerMessageID("get pos");
			_mockPort.ReadExpectationsFromString($"/00 0 {msgIdPos:00} get pos");
			_mockPort.ReadExpectationsFromString($"@01 0 {msgIdPos:00} OK IDLE FZ 0");
			var msgIdWarnings = GetPollerMessageID("warnings");
			_mockPort.ReadExpectationsFromString($"/01 1 {msgIdWarnings:00} warnings");
			_mockPort.ReadExpectationsFromString($"@01 1 {msgIdWarnings:00} OK IDLE FZ 01 FZ");
			var msgIdPid = GetPollerMessageID("get peripheral.id.pending");
			_mockPort.ReadExpectationsFromString($"/01 0 {msgIdPid:00} get peripheral.id.pending");
			_mockPort.ReadExpectationsFromString($"@01 0 {msgIdPid:00} OK IDLE FZ 63211");

			Assert.IsFalse(view.PendingIdUpdates.Any());

			PollAndWait("get pos");
			WaitForPollActionQueue();
			WaitForPollComplete("get peripheral.id.pending");

			Assert.AreEqual(1, view.PendingIdUpdates.Count);
			Assert.AreEqual(63211, view.PendingIdUpdates[0]);
			Assert.AreEqual(true, view.HasPeripheralConnectionIssue);
			_mockPort.Verify();
		}


		[Test]
		[Description("Check that position updates are fed to the view under normal conditions.")]
		public void UpdatePosition()
		{
			var deviceType = CreateDeviceType(66034);
			deviceType.Capabilities.Add(Capability.AutoDetector);
			SetupAndOpenPort(deviceType);
			var view = new MockDeviceView(1, 1);
			InitializeWorkflow(view);

			// Kicking the main poller when the peripheral ID is nonzero and FZ is present
			// should trigger polling the pending peripheral ID and then pending serial number.
			var msgIdPos = GetPollerMessageID("get pos");
			_mockPort.ReadExpectationsFromString($"/00 0 {msgIdPos:00} get pos");
			_mockPort.ReadExpectationsFromString($"@01 0 {msgIdPos:00} OK IDLE -- 555");
			var msgIdPid = GetPollerMessageID("get peripheral.id");
			_mockPort.ReadExpectationsFromString($"/01 0 {msgIdPid:00} get peripheral.id");
			_mockPort.ReadExpectationsFromString($"@01 0 {msgIdPid:00} OK IDLE -- 66034");

			Assert.IsFalse(view.PositionUpdates.Any());

			PollAndWait("get pos");
			WaitForPollActionQueue();
			WaitForPollComplete("get peripheral.id");

			Assert.AreEqual(1, view.PositionUpdates.Count);
			Assert.AreEqual(555, view.PositionUpdates[0]);
			_mockPort.Verify();
		}


		[Test]
		[Description("Check that position updates still work when some devices reject polling.")]
		public void UpdatePositionsWithRejection()
		{
			SetupAndOpenPort(CreateDeviceType(7), CreateDeviceType(8));
			var view1 = new MockDeviceView(1, 1);
			var view2 = new MockDeviceView(2, 1);
			InitializeWorkflow(view1, view2);

			var msgIdPos = GetPollerMessageID("get pos");
			_mockPort.ReadExpectationsFromString($"/00 0 {msgIdPos:00} get pos");
			_mockPort.ReadExpectationsFromString($"@01 0 {msgIdPos:00} RJ IDLE -- BADCOMMAND");
			_mockPort.ReadExpectationsFromString($"@02 0 {msgIdPos:00} OK IDLE -- 555");

			Assert.IsFalse(view1.PositionUpdates.Any());
			Assert.IsFalse(view2.PositionUpdates.Any());

			PollAndWait("get pos");

			Assert.AreEqual(0, view1.PositionUpdates.Count);
			Assert.AreEqual(1, view2.PositionUpdates.Count);
			Assert.AreEqual(555, view2.PositionUpdates[0]);
			_mockPort.Verify();
		}


		[Test]
		[Description("Check that position updates still work when some devices don't support message IDs.")]
		public void UpdatePositionsWithNoMessageIdSupport()
		{
			SetupAndOpenPort(CreateDeviceType(7), CreateDeviceType(8));
			var view1 = new MockDeviceView(1, 1);
			var view2 = new MockDeviceView(2, 1);
			InitializeWorkflow(view1, view2);

			var msgIdPos = GetPollerMessageID("get pos");
			_mockPort.ReadExpectationsFromString($"/00 0 {msgIdPos:00} get pos");
			_mockPort.ReadExpectationsFromString($"@01 0 RJ IDLE -- BADCOMMAND");
			_mockPort.ReadExpectationsFromString($"@02 0 {msgIdPos:00} OK IDLE -- 555");

			Assert.IsFalse(view1.PositionUpdates.Any());
			Assert.IsFalse(view2.PositionUpdates.Any());

			PollAndWait("get pos");

			Assert.AreEqual(0, view1.PositionUpdates.Count);
			Assert.AreEqual(1, view2.PositionUpdates.Count);
			Assert.AreEqual(555, view2.PositionUpdates[0]);
			_mockPort.Verify();
		}


		[Test]
		[Description("Check that message filtering works correctly.")]
		public void MessageFilter()
		{
			SetupAndOpenPort(CreateDeviceType(0));
			var view = new MockDeviceView(1, 1);
			InitializeWorkflow(view);

			var filteredIds = new BitArray(256);
			foreach (var poller in GetAllPollers())
			{
				filteredIds[poller.ReservedMessageId.Value] = true;
			}

			for (int i = 1; i < 256; i++)
			{
				var msg = $"/01 0 {i:00} foo";
				Assert.AreEqual(filteredIds[i], 
					_workflow.IsPollingMessage(new DeviceMessage(msg)),
					$"Filtering incorrect for message ID {i} and '/'");

				msg = $"@01 0 {i:00} OK IDLE -- foo";
				Assert.AreEqual(filteredIds[i],
					_workflow.IsPollingMessage(new DeviceMessage(msg)),
					$"Filtering incorrect for message ID {i} and '@'");

				msg = $"#01 0 {i:00} foo";
				Assert.AreEqual(filteredIds[i],
					_workflow.IsPollingMessage(new DeviceMessage(msg)),
					$"Filtering incorrect for message ID {i} and '#'");

				msg = $"!01 0 {i:00} foo";
				Assert.AreEqual(filteredIds[i],
					_workflow.IsPollingMessage(new DeviceMessage(msg)),
					$"Filtering incorrect for message ID {i} and '!'");
			}
		}


		[Test]
		[Description("Check that port closing stops the poller.")]
		public void PortCloseStopsPolling()
		{
			SetupAndOpenPort(CreateDeviceType(0));
			var view = new MockDeviceView(1, 1);
			InitializeWorkflow(view);

			Assert.IsTrue(GetAllPollers().Any());

			_portFacade.Close();

			Assert.IsFalse(GetAllPollers().Any());
		}


		private DeviceType CreateDeviceType(params int[] aPeripheralIds)
		{
			var peripheralMap = aPeripheralIds.ToDictionary(
				id => id, 
				id => new DeviceType
				{
					PeripheralId = id,
					FirmwareVersion = new FirmwareVersion(7, 12),
					Commands = new List<CommandInfo>()
				});

			return new DeviceType
			{
				DeviceId = 30311,
				Name = "X-MCC1 Controller",
				FirmwareVersion = new FirmwareVersion(7, 12),
				MotionType = MotionType.None,
				PeripheralMap = peripheralMap
			};
		}


		private void InitializeWorkflow(params IDeviceView[] aViewList)
		{
			_workflow = new PeripheralAutodetectWorkflow(_portFacade, aViewList);
			_workflow.PositionPollInterval = TimeSpan.FromDays(1); // We will be controlling timing manually.
			_workflow.Start();
		}


		private void SetupAndOpenPort(params DeviceType[] aDeviceTypes)
		{
			_mockPort = new MockPort { IsAsciiMode = true };

			var defaultDeviceType = new DeviceType { Commands = new List<CommandInfo>() };

			_portFacade = new ZaberPortFacade
			{
				DefaultDeviceType = defaultDeviceType,
				Port = _mockPort,
				QueryTimeout = 100
			};

			foreach (var dType in aDeviceTypes)
			{
				_portFacade.AddDeviceType(dType);
			}

			ZaberPortFacadeTest.SetAsciiExpectations(_portFacade, _mockPort, aDeviceTypes);

			_portFacade.Open("COM1");
			_portFacade.AreMessageIdsEnabled = true;
		}


		private void PollAndWait(string aPollCommand)
		{
			var poller = FindPoller(aPollCommand);
			var timer = InvocationHelper.InvokePrivateMethod(poller, "GetTimer", null) as MockDispatcherTimer;
			timer.FireTickEvent();
			Assert.IsTrue(poller.WaitForPollComplete(TimeSpan.FromSeconds(30)));
		}


		private void WaitForPollComplete(string aPollCommand)
		{
			var poller = FindPoller(aPollCommand);
			Assert.IsTrue(poller.WaitForPollComplete(TimeSpan.FromSeconds(30)));
		}


		private byte GetPollerMessageID(string aPollCommand)
		{
			var poller = FindPoller(aPollCommand);
			return poller.ReservedMessageId.Value;
		}


		private void SetContentionTimeouts(TimeSpan aTimeout)
		{
			foreach (var poller in GetAllPollers())
			{
				poller.ContentionTimeout = aTimeout;
			}
		}


		private void WaitForPollActionQueue() 
			=> InvocationHelper.InvokePrivateMethod(_workflow, "WaitForPollActionQueue", TimeSpan.FromSeconds(20));


		private DevicePoller FindPoller(string aPollCommand) 
			=> GetAllPollers().FirstOrDefault(p => aPollCommand == p.Command);


		private IEnumerable<DevicePoller> GetAllPollers() 
			=> InvocationHelper.InvokePrivateMethod(_workflow, "GetAllPollers", null) as IEnumerable<DevicePoller>;

		private void SetSynchronousMode(bool aSync)
			=> InvocationHelper.InvokePrivateMethod(_workflow, "SetPollSynchronously", aSync);



		private class MockDeviceView : IDeviceView
		{
			public MockDeviceView(byte aDeviceNumber, int aAxisNumber)
			{
				_deviceNumber = aDeviceNumber;
				_axisNumber = aAxisNumber;
			}

			public byte DeviceNumber => _deviceNumber;

			public int AxisNumber => _axisNumber;

			public bool PollingEnabled { get; set; }

			public bool HasPeripheralConnectionIssue { get; set; }

			public IList<decimal?> PositionUpdates { get; } = new List<decimal?>();

			public void UpdatePositionInSelectedUnits(decimal? aPositionInDeviceUnits)
			{
				PositionUpdates.Add(aPositionInDeviceUnits);
			}

			public IList<int> PendingIdUpdates { get; } = new List<int>();

			public void SetPendingPeripheralId(int aPendingId)
			{
				PendingIdUpdates.Add(aPendingId);
			}

			private byte _deviceNumber;
			private int _axisNumber;
		}


		private MockPort _mockPort;
		private ZaberPortFacade _portFacade;
		private PeripheralAutodetectWorkflow _workflow;
	}
}

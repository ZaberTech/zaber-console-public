﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Zaber;
using ZaberConsole;
using ZaberTest;

namespace ZaberConsoleTest
{
	[TestFixture]
	[SetCulture("en-US")]
	internal class SqlitePeripheralDictionaryTest
	{
		[SetUp]
		public void Init()
		{
			_dict = new SqlitePeripheralDictionary(
				TestDataFileHelper.GetDeployedPath(DatabaseFileName),
				30221,
				new FirmwareVersion(617),
				new FirmwareVersion(617));
		}


		[TearDown]
		public void Cleanup() => _dict.Dispose();


		[Test]
		public void AccessByIndex()
		{
			// SETUP
			// Peripheral ID 40011 (LSQ075A-T3).
			const int lsqKey = 40011;

			// EXEC
			var device = _dict[lsqKey];

			// VERIFY
			Assert.AreEqual(30221, device.DeviceId);
			Assert.AreEqual(617, device.FirmwareVersion.ToInt());
			Assert.AreEqual(40011, device.PeripheralId);
		}


		[Test]
		public void CanClear() => _dict.Clear();


		[Test]
		public void CanNotAdd()
		{
			// SETUP
			const int key = 3;
			var value = new DeviceType();
			var kvp = new KeyValuePair<int, DeviceType>(3, new DeviceType());

			// EXEC & VERIFY
			Assert.Throws<NotSupportedException>(() =>
													 _dict.Add(key, value));
			Assert.Throws<NotSupportedException>(() =>
													 _dict.Add(kvp));
		}


		[Test]
		public void Contains()
		{
			// SETUP
			const int key = 40011;

			// EXEC & VERIFY
			Assert.IsTrue(_dict.ContainsKey(key));

			// MORE SETUP
			var value = _dict[key];

			// EXEC & VERIFY AGAIN
			Assert.IsTrue(_dict.Contains(new KeyValuePair<int, DeviceType>(key, value)));
		}


		[Test]
		public void CopyToNotImplemented()
		{
			// SETUP
			var array = new KeyValuePair<int, DeviceType>[10];

			// EXEC & VERIFY
			Assert.Throws<NotImplementedException>(() =>
													   _dict.CopyTo(array, 0));
		}


		[Test]
		public void DoesNotContain()
		{
			// SETUP
			const int key = 999999;

			// EXEC & VERIFY
			Assert.IsFalse(_dict.ContainsKey(key));

			// MORE SETUP
			var value = new DeviceType();

			// EXEC & VERIFY AGAIN
			Assert.IsFalse(_dict.Contains(new KeyValuePair<int, DeviceType>(key, value)));
		}


		[Test]
		public void EnumeratorAndCountGiveSameNumber()
		{
			// SETUP
			var enumeratorCount = 0;

			// EXEC
			foreach (var kvp in _dict)
			{
				enumeratorCount += 1;
			}

			Assert.AreEqual(enumeratorCount, _dict.Count);
		}


		[Test]
		public void EnumeratorEnumerates()
		{
			// SETUP
			var entryCount = 0;

			// EXEC
			// foreach calls GetEnumerator().
			foreach (var kvp in _dict)
			{
				entryCount += 1;
			}

			// VERIFY
			// Assert that the enumerator enumerated over at least 3 entries.
			Assert.Greater(entryCount, 2);
		}


		[Test]
		public void RemoveNotSupported()
		{
			// SETUP
			const int key = 999999;
			var value = new DeviceType();
			var kvp = new KeyValuePair<int, DeviceType>(key, value);

			// EXEC & VERIFY
			Assert.Throws<NotSupportedException>(() =>
													 _dict.Remove(key));
			Assert.Throws<NotSupportedException>(() =>
													 _dict.Remove(kvp));
		}


		[Test]
		public void ThrowsKeyNotFoundException()
		{
			// SETUP
			const int key = 999999;

			// EXEC & VERIFY
			Assert.Throws<KeyNotFoundException>(() =>
			{
				var type = _dict[key];
			});
		}


		[Test]
		public void TryGetValueFails()
		{
			// SETUP
			const int key = 999999;
			DeviceType value;

			// EXEC
			var succeeded = _dict.TryGetValue(key, out value);

			// VERIFY
			Assert.IsFalse(succeeded);
			Assert.IsNull(value);
		}


		[Test]
		public void TryGetValueSucceeds()
		{
			// SETUP
			const int key = 40011;
			DeviceType value;

			// EXEC
			var succeeded = _dict.TryGetValue(key, out value);

			// VERIFY
			Assert.IsTrue(succeeded);
			Assert.AreEqual(617, value.FirmwareVersion.ToInt());
			Assert.AreEqual(30221, value.DeviceId);
		}


		private const string DatabaseFileName = "devices.sqlite";
		private SqlitePeripheralDictionary _dict;
	}
}

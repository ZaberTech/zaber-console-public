﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Zaber;
using ZaberConsole;
using ZaberTest;

namespace ZaberConsoleTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class SqliteDeviceDictionaryTest
	{
		[SetUp]
		public void Init()
		{
			_dict = new SqliteDeviceDictionary(TestDataFileHelper.GetDeployedPath(DatabaseFileName));
		}


		[TearDown]
		public void Cleanup() => _dict.Dispose();


		[Test]
		public void AccessByIndex()
		{
			// SETUP
			// Device ID 20011 (A-LSQ075A), FW version 6.17.
			var aLsqKey = new Tuple<int, FirmwareVersion>(20011, new FirmwareVersion(617));

			// EXEC
			var device = _dict[aLsqKey];

			// VERIFY
			Assert.AreEqual(device.DeviceId, 20011);
			Assert.AreEqual(device.FirmwareVersion.ToInt(), 617);
		}


		[Test]
		public void CanClear() => _dict.Clear();


		[Test]
		public void CanNotAdd()
		{
			// SETUP
			var key = new Tuple<int, FirmwareVersion>(1, new FirmwareVersion(2));
			var value = new DeviceType();
			var kvp =
				new KeyValuePair<Tuple<int, FirmwareVersion>, DeviceType>(key, value);

			// EXEC & VERIFY
			Assert.Throws<NotSupportedException>(() =>
													 _dict.Add(key, value));
			Assert.Throws<NotSupportedException>(() =>
													 _dict.Add(kvp));
		}


		[Test]
		public void Contains()
		{
			// SETUP
			var key = new Tuple<int, FirmwareVersion>(21043, new FirmwareVersion(617));

			// EXEC & VERIFY
			Assert.IsTrue(_dict.ContainsKey(key));

			// MORE SETUP
			var value = _dict[key];

			// EXEC & VERIFY AGAIN
			Assert.IsTrue(_dict.Contains(new KeyValuePair<Tuple<int, FirmwareVersion>, DeviceType>(key, value)));
		}


		[Test]
		public void CopyToNotImplemented()
		{
			// SETUP
			var array =
				new KeyValuePair<Tuple<int, FirmwareVersion>, DeviceType>[10];

			// EXEC & VERIFY
			Assert.Throws<NotImplementedException>(() =>
													   _dict.CopyTo(array, 0));
		}


		[Test]
		public void DoesNotContain()
		{
			// SETUP
			var key = new Tuple<int, FirmwareVersion>(99999999, new FirmwareVersion(999));

			// EXEC & VERIFY
			Assert.IsFalse(_dict.ContainsKey(key));

			// MORE SETUP
			var value = new DeviceType();

			// EXEC & VERIFY AGAIN
			Assert.IsFalse(_dict.Contains(new KeyValuePair<Tuple<int, FirmwareVersion>, DeviceType>(key, value)));
		}


		[Test]
		public void EnumeratorEnumerates()
		{
			// SETUP
			var entryCount = 0;

			// EXEC
			// foreach calls GetEnumerator().
			foreach (var kvp in _dict)
			{
				entryCount += 1;

				// Terminate once we're satisfied - otherwise this test takes a long time and chews memory.
				if (entryCount > 2)
				{
					break;
				}
			}

			// VERIFY
			// Assert that the collection has at least 3 entries.
			Assert.Greater(entryCount, 2);
		}


		[Test]
		public void GetAdvancedBinaryCommands()
		{
			var commands = _dict.GetAdvancedBinaryCommands();
			Assert.Greater(commands.Count, 0);

			var commandsSeen = false;
			var settingsSeen = false;
			var readOnlySeen = false;
			foreach (var ci in commands)
			{
				if (ci is ReadOnlySettingInfo)
				{
					readOnlySeen = true;
				}
				else if (ci is SettingInfo)
				{
					settingsSeen = true;
				}
				else
				{
					commandsSeen = true;
				}
			}

			Assert.IsTrue(commandsSeen);
			Assert.IsTrue(settingsSeen);
			Assert.IsTrue(readOnlySeen);
		}


		[Test]
		public void RemoveNotSupported()
		{
			// SETUP
			var key = new Tuple<int, FirmwareVersion>(999999, new FirmwareVersion(999));
			var value = new DeviceType();
			var kvp =
				new KeyValuePair<Tuple<int, FirmwareVersion>, DeviceType>(key, value);

			// EXEC & VERIFY
			Assert.Throws<NotSupportedException>(() =>
													 _dict.Remove(key));
			Assert.Throws<NotSupportedException>(() =>
													 _dict.Remove(kvp));
		}


		[Test]
		public void TryGetValueFails()
		{
			// SETUP
			var key = new Tuple<int, FirmwareVersion>(99999, new FirmwareVersion(999));
			DeviceType value;

			// EXEC
			var succeeded = _dict.TryGetValue(key, out value);

			// VERIFY
			Assert.IsFalse(succeeded);
			Assert.IsNull(value);
		}


		[Test]
		public void TryGetValueSucceeds()
		{
			// SETUP
			var key = new Tuple<int, FirmwareVersion>(20011, new FirmwareVersion(617));
			DeviceType value;

			// EXEC
			var succeeded = _dict.TryGetValue(key, out value);

			// VERIFY
			Assert.IsTrue(succeeded);
			Assert.AreEqual(value.FirmwareVersion.ToInt(), 617);
			Assert.AreEqual(value.DeviceId, 20011);
		}


		private const string DatabaseFileName = "devices.sqlite";
		private SqliteDeviceDictionary _dict;
	}
}

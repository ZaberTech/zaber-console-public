﻿using NUnit.Framework;
using Zaber.Ports;
using Zaber.Testing;
using ZaberConsole.Ports;

namespace ZaberConsoleGUITests.Ports
{
	[TestFixture]
	[SetCulture("en-US")]
	public class TcpPortFactoryTests
	{
		[SetUp]
		public void Setup()
		{
			_listener = new ObservableObjectTester();
			_factory = new TcpPortFactory();
			_listener.Listen(_factory);
		}


		[TearDown]
		public void Teardown() => _listener.Unlisten(_factory);


		[Test]
		public void TestConstructor()
		{
			Assert.IsTrue(_factory.Settings is TcpPortSettings);
			Assert.AreEqual("localhost", (_factory.Settings as TcpPortSettings).HostName);
			Assert.AreEqual(23105, (_factory.Settings as TcpPortSettings).PortNumber);
			Assert.AreEqual("localhost", _factory.HostName);
			Assert.AreEqual(23105, _factory.PortNumber);
		}


		[Test]
		public void TestCreatePort()
		{
			var port = _factory.CreatePort();
			Assert.IsNotNull(port);
			Assert.AreEqual("localhost:23105", port.PortName);
			Assert.IsTrue(port is TcpPort);

			_factory.Settings = new TcpPortSettings
			{
				HostName = "1.2.3.4",
				PortNumber = 56
			};

			port = _factory.CreatePort();
			Assert.IsNotNull(port);
			Assert.AreEqual("1.2.3.4:56", port.PortName);
		}


		[Test]
		public void TestCreateRawPort()
		{
			var port = _factory.CreateRawPort();
			Assert.IsNotNull(port);
			Assert.IsTrue(port is RawTcpPort);
		}


		[Test]
		public void TestSettingsAreUpdatesByControls()
		{
			_factory.HostName = "1.2.3.4";
			Assert.AreEqual("1.2.3.4", (_factory.Settings as TcpPortSettings).HostName);
			Assert.AreEqual(23105, (_factory.Settings as TcpPortSettings).PortNumber);
			_factory.PortNumber = 56;
			Assert.AreEqual("1.2.3.4", (_factory.Settings as TcpPortSettings).HostName);
			Assert.AreEqual(56, (_factory.Settings as TcpPortSettings).PortNumber);
		}


		[Test]
		public void TestSettingsPropagateToControls()
		{
			_factory.Settings = new TcpPortSettings
			{
				HostName = "1.2.3.4",
				PortNumber = 56
			};

			Assert.IsTrue(_listener.PropertyWasChanged(nameof(_factory.HostName)));
			Assert.AreEqual("1.2.3.4", _factory.HostName);
			Assert.IsTrue(_listener.PropertyWasChanged(nameof(_factory.PortNumber)));
			Assert.AreEqual(56, _factory.PortNumber);
		}


		[Test]
		public void TestViewProperties()
		{
			_listener.TestObservableProperty(_factory, () => _factory.HostName);
			_listener.TestObservableProperty(_factory, () => _factory.PortNumber);
		}


		private TcpPortFactory _factory;
		private ObservableObjectTester _listener;
	}
}

﻿using System.ComponentModel;
using System.Linq;
using System.Reflection;
using NUnit.Framework;
using ZaberConsole;
using ZaberConsole.Ports;
using ZaberTest.Testing;

namespace ZaberConsoleGUITests.Ports
{
	[TestFixture]
	[SetCulture("en-US")]
	public class SanityChecks
	{
		[Test]
		public void TestEverythingHasExpectedAttributes()
		{
			var factoryTypes = ReflectionHelper
						   .FindImplementationsOf(typeof(IPortFactory), Assembly.GetAssembly(typeof(IPortFactory)))
						   .ToArray();
			var settingsTypes = ReflectionHelper
							.FindImplementationsOf(typeof(PortSettings), Assembly.GetAssembly(typeof(PortSettings)))
							.ToArray();
			Assert.Less(0, factoryTypes.Length);
			Assert.Less(0, settingsTypes.Length);
			Assert.AreEqual(factoryTypes.Length, settingsTypes.Length);

			foreach (var type in settingsTypes)
			{
				var factoryTypeAttr = type.GetFirstAttributeOfType<PortFactoryTypeAttribute>();
				Assert.IsNotNull(factoryTypeAttr);
				Assert.IsFalse(factoryTypeAttr.FactoryType.IsAbstract);
				Assert.IsTrue(typeof(IPortFactory).IsAssignableFrom(factoryTypeAttr.FactoryType));
				Assert.IsTrue(factoryTypes.Contains(factoryTypeAttr.FactoryType));
			}

			foreach (var type in factoryTypes)
			{
				var displayNameAttr = type.GetFirstAttributeOfType<DisplayNameAttribute>();
				Assert.IsNotNull(displayNameAttr);
				Assert.IsFalse(string.IsNullOrEmpty(displayNameAttr.DisplayName));
			}
		}


		[Test]
		public void TestSerialAttributesHaveNotChanged()
		{
			// Settings classes serialized in user settings must lead to the correct factory type.
			var factoryTypeAttr = typeof(TcpPortSettings).GetFirstAttributeOfType<PortFactoryTypeAttribute>();
			Assert.IsNotNull(factoryTypeAttr);
			Assert.AreEqual(typeof(TcpPortFactory), factoryTypeAttr.FactoryType);
			var displayNameAttr = factoryTypeAttr.FactoryType.GetFirstAttributeOfType<DisplayNameAttribute>();
			Assert.IsNotNull(displayNameAttr);
			Assert.AreEqual(TcpPortFactory.PORT_TYPE_NAME, displayNameAttr.DisplayName);
			Assert.AreEqual("TCP/IP", TcpPortFactory.PORT_TYPE_NAME);
		}


		[Test]
		public void TestTcpAttributesHaveNotChanged()
		{
			// Settings classes serialized in user settings must lead to the correct factory type.
			var factoryTypeAttr = typeof(TcpPortSettings).GetFirstAttributeOfType<PortFactoryTypeAttribute>();
			Assert.IsNotNull(factoryTypeAttr);
			Assert.AreEqual(typeof(TcpPortFactory), factoryTypeAttr.FactoryType);
			var displayNameAttr = factoryTypeAttr.FactoryType.GetFirstAttributeOfType<DisplayNameAttribute>();
			Assert.IsNotNull(displayNameAttr);
			Assert.AreEqual(TcpPortFactory.PORT_TYPE_NAME, displayNameAttr.DisplayName);
			Assert.AreEqual("TCP/IP", TcpPortFactory.PORT_TYPE_NAME);
		}
	}
}

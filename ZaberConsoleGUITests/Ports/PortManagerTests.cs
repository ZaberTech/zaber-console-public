﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Zaber.Ports;
using Zaber.Testing;
using ZaberConsole.Ports;
using ZaberTest.Testing;
using ZaberWpfToolbox.Dialogs;

namespace ZaberConsoleGUITests.Ports
{
	[TestFixture]
	[SetCulture("en-US")]
	public class PortManagerTests
	{
		[SetUp]
		public void Setup()
		{
			_localSerialPorts = new HashSet<string>(RS232Port.GetSerialPortNames());
			var portNumber = 1000;
			_nonExistantPortName = $"COM{portNumber}";
			while (_localSerialPorts.Contains(_nonExistantPortName))
			{
				portNumber++;
				_nonExistantPortName = $"COM{portNumber}";
			}

			_settings = new PortSettingsCollection();
			_settings.LastSelectedIndex = 1;
			_settings.RecentConnections.Add(new SerialPortSettings
			{
				Name = "My serial port",
				SerialPortName = _nonExistantPortName
			});
			_settings.RecentConnections.Add(new TcpPortSettings
			{
				Name = "Bridge",
				HostName = "localhost",
				PortNumber = 1234
			});
			_settings.RecentConnections.Add(new SerialPortSettings
			{
				Name = "My other serial port",
				SerialPortName = _localSerialPorts.FirstOrDefault() ?? "COM1" // Might not exist.
			});
			_settings.RecentConnections.Add(new TcpPortSettings
			{
				Name = "Bridge 2",
				HostName = "localhost",
				PortNumber = 5678
			});

			var factories = new List<Type>
			{
				typeof(SerialPortFactory),
				typeof(TcpPortFactory)
			};

			_manager = new PortManagerVM(_settings, factories);

			_watcher = new ObservableObjectTester();
			_watcher.Listen(_manager);
		}


		[TearDown]
		public void Teardown() => _watcher.Unlisten(_manager);


		[Test]
		public void TestChangedEventInvokedMinimally()
		{
			var tcpSettings = _manager.SelectedConnection;
			Assert.IsTrue(tcpSettings is TcpPortSettings);

			var changed = false;
			_manager.SelectedPortSettingsChanged += () => { changed = true; };

			_manager.SelectedConnection = tcpSettings;
			Assert.IsFalse(changed);

			_manager.SelectedConnection = tcpSettings.Clone();
			Assert.IsFalse(changed);

			_manager.SelectedConnection = null;
			Assert.IsTrue(changed);
			changed = false;

			_manager.SelectedConnection = null;
			Assert.IsFalse(changed);

			_manager.SelectedConnection = tcpSettings;
			Assert.IsTrue(changed);
		}


		[Test]
		public void TestChangingSelection()
		{
			var settings1 = _manager.SelectedConnection;
			Assert.IsTrue(settings1 is TcpPortSettings);
			var settings2 =
				_manager.DisplayedConnections.FirstOrDefault(s => s is TcpPortSettings && !s.DataEquals(settings1));
			Assert.IsNotNull(settings2);

			_manager.SelectedConnection = settings2;
			Assert.IsTrue(_watcher.PropertyWasChanged(nameof(_manager.SelectedConnection)));
			Assert.IsFalse(_watcher.PropertyWasChanged(nameof(_manager.PortFactory)));
			Assert.IsNotNull(_manager.PortFactory);

			_watcher.ClearNotifications();
			_manager.SelectedConnection = settings1;
			Assert.IsTrue(_watcher.PropertyWasChanged(nameof(_manager.SelectedConnection)));
			Assert.IsFalse(_watcher.PropertyWasChanged(nameof(_manager.PortFactory)));
			Assert.IsNotNull(_manager.PortFactory);

			_watcher.ClearNotifications();
			_manager.SelectedConnection = null;
			Assert.IsTrue(_watcher.PropertyWasChanged(nameof(_manager.SelectedConnection)));
			Assert.IsTrue(_watcher.PropertyWasChanged(nameof(_manager.PortFactory)));
			Assert.IsNull(_manager.PortFactory);
		}


		[Test]
		public void TestChangingSelectionChangesType()
		{
			if (!_manager.DisplayedConnections.Any(s => s is SerialPortSettings))
			{
				Assert.Pass("No local serial ports to test with.");
			}

			var tcpSettings = _manager.SelectedConnection;
			Assert.IsTrue(tcpSettings is TcpPortSettings);
			var serialSettings = _manager.DisplayedConnections.FirstOrDefault(s => s is SerialPortSettings);
			Assert.IsNotNull(serialSettings);

			_manager.SelectedConnection = serialSettings;
			Assert.IsTrue(_watcher.PropertyWasChanged(nameof(_manager.SelectedConnection)));
			Assert.IsTrue(_watcher.PropertyWasChanged(nameof(_manager.PortFactory)));
			Assert.IsTrue(_manager.PortFactory is SerialPortFactory);
		}


		[Test]
		public void TestConstructor()
		{
			Assert.IsTrue(_manager.PortFactory is TcpPortFactory);
			Assert.Less(0, _manager.DisplayedConnections.Count);
			Assert.IsNotNull(_manager.SelectedConnection);
			Assert.AreEqual("Bridge", _manager.SelectedConnection.Name);

			var connection = _manager.DisplayedConnections
								  .Where(c => c is SerialPortSettings sp && (sp.SerialPortName == _nonExistantPortName))
								  .FirstOrDefault();
			Assert.IsNull(connection);

			Assert.IsTrue(_manager.DisplayedConnections.Contains(_manager.SelectedConnection));
			connection = _manager.DisplayedConnections
							  .Where(c => c is SerialPortSettings sp && (sp.SerialPortName == "COM1"))
							  .FirstOrDefault();
			Assert.AreEqual(_localSerialPorts.Contains("COM1"), null != connection);

			// Verify any other local serial ports were added.
			foreach (var portName in _localSerialPorts)
			{
				connection = _manager.DisplayedConnections
								  .Where(c => c is SerialPortSettings sp && (sp.SerialPortName == portName))
								  .FirstOrDefault();
				Assert.IsNotNull(connection, $"Port {portName} was not added.");
			}

			Assert.AreEqual("Bridge", _manager.SelectedConnectionInfo);

			Assert.AreEqual(_manager.DisplayedConnections.Count, _settings.RecentConnections.Count);
			Assert.AreEqual(0, _settings.LastSelectedIndex);
		}


		[Test]
		public void TestDefaultSelectionOnPortListRefresh()
		{
			var defaultSelection = _manager.SelectedConnection;
			Assert.IsNotNull(defaultSelection);

			_manager.SelectedConnection = null;
			_manager.RefreshSerialPorts();
			Assert.AreEqual(defaultSelection, _manager.SelectedConnection);

			var nonDefaultSelection =
				_manager.DisplayedConnections.Where(c => c.Name != defaultSelection.Name).First();
			Assert.IsNotNull(nonDefaultSelection);
			_manager.SelectedConnection = nonDefaultSelection;
			_manager.RefreshSerialPorts();
			Assert.AreEqual(nonDefaultSelection, _manager.SelectedConnection);
		}


		[Test]
		public void TestObservableProperties()
			=> _watcher.TestObservableProperty(_manager, () => _manager.IsDropDownOpen);


		[Test]
		public void TestOpenDialogNoChanges()
		{
			RequestDialogChange dialogHandler = (aSender, aVM) =>
			{
				var dlg = aVM as PortSettingsDialogVM;
				Assert.IsNotNull(dlg);
				InvocationHelper.SetPrivateProperty(dlg, nameof(dlg.SaveChanges), false);
			};

			_manager.RequestDialogOpen += dialogHandler;
			_manager.ManageConnectionsCommand.Execute(null);
			_manager.RequestDialogOpen -= dialogHandler;

			Assert.AreEqual(0, _watcher.GetNumNotifications());
		}


		[Test]
		public void TestOpenDialogWithChanges()
		{
			RequestDialogChange dialogHandler = (aSender, aVM) =>
			{
				var dlg = aVM as PortSettingsDialogVM;
				Assert.IsNotNull(dlg);
				dlg.SelectedPort = null;
				dlg.PortList.Clear();
				InvocationHelper.SetPrivateProperty(dlg, nameof(dlg.SaveChanges), true);
			};

			_manager.RequestDialogOpen += dialogHandler;
			_manager.ManageConnectionsCommand.Execute(null);
			_manager.RequestDialogOpen -= dialogHandler;

			Assert.IsNull(_manager.SelectedConnection);
			Assert.AreEqual(_localSerialPorts.Count, _manager.DisplayedConnections.Count);
			Assert.AreEqual(-1, _settings.LastSelectedIndex);
			Assert.AreEqual(_localSerialPorts.Count, _settings.RecentConnections.Count);
		}


		[Test]
		public void TestSaveSelection()
		{
			var originalIndex = _settings.LastSelectedIndex;
			var tcpSettings = _manager.SelectedConnection;
			Assert.IsTrue(tcpSettings is TcpPortSettings);
			Assert.AreEqual(tcpSettings, _settings.RecentConnections[_settings.LastSelectedIndex]);

			var serialSettings = _manager.DisplayedConnections.FirstOrDefault(s => s is SerialPortSettings);
			if (null != serialSettings)
			{
				_manager.SelectedConnection = serialSettings;
				_manager.SaveCurrentSettingsToRecentList();
				Assert.AreEqual(serialSettings, _settings.RecentConnections[_settings.LastSelectedIndex]);
			}

			_manager.SelectedConnection = null;
			_manager.SaveCurrentSettingsToRecentList();
			Assert.AreEqual(-1, _settings.LastSelectedIndex);

			_manager.SelectedConnection = tcpSettings;
			_manager.SaveCurrentSettingsToRecentList();
			Assert.AreEqual(originalIndex, _settings.LastSelectedIndex);
		}


		[Test]
		public void TestShowConnectionDetails()
		{
			_manager.ShowConnectionDetails("Foo bar");
			Assert.IsTrue(_watcher.PropertyWasChanged(nameof(_manager.SelectedConnectionInfo)));
			Assert.AreEqual("Bridge Foo bar", _manager.SelectedConnectionInfo);
			_watcher.ClearNotifications();
			_manager.ShowConnectionDetails(null);
			Assert.IsTrue(_watcher.PropertyWasChanged(nameof(_manager.SelectedConnectionInfo)));
			Assert.AreEqual("Bridge", _manager.SelectedConnectionInfo);
		}


		private HashSet<string> _localSerialPorts;
		private string _nonExistantPortName;
		private PortManagerVM _manager;
		private PortSettingsCollection _settings;
		private ObservableObjectTester _watcher;
	}
}

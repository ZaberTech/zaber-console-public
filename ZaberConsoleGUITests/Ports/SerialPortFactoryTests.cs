﻿using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using NUnit.Framework;
using Zaber;
using Zaber.Ports;
using Zaber.Testing;
using ZaberConsole.Ports;

namespace ZaberConsoleGUITests.Ports
{
	[TestFixture]
	[SetCulture("en-US")]
	public class SerialPortFactoryTests
	{
		[SetUp]
		public void Setup()
		{
			_listener = new ObservableObjectTester();
			_factory = new SerialPortFactory();
			_listener.Listen(_factory);
		}


		[TearDown]
		public void Teardown() => _listener.Unlisten(_factory);


		[Test]
		public void TestConstructor()
		{
			Assert.IsNotNull(_factory.AvailablePorts);
			foreach (var name in _factory.AvailablePorts)
			{
				Assert.IsTrue(Regex.IsMatch(name, "COM\\d+"));
			}

			if (_factory.AvailablePorts.Count > 0)
			{
				Assert.IsTrue(_factory.Settings is SerialPortSettings);
				Assert.AreEqual(_factory.AvailablePorts[0], _factory.PortDisplayName);
				Assert.AreEqual(_factory.PortDisplayName, (_factory.Settings as SerialPortSettings).SerialPortName);
			}
		}


		[Test]
		public void TestCreatePort()
		{
			if (_factory.AvailablePorts.Count > 0)
			{
				var port = _factory.CreatePort();
				Assert.IsNotNull(port);
				Assert.AreEqual(_factory.PortDisplayName, port.PortName);
				Assert.IsTrue(port is TSeriesPort);
			}
			else
			{
				Assert.Pass("Skipped because there are no serial ports.");
			}
		}


		[Test]
		public void TestCreateRawPort()
		{
			if (_factory.AvailablePorts.Count > 0)
			{
				var port = _factory.CreateRawPort();
				Assert.IsNotNull(port);
				Assert.IsTrue(port is RawRS232Port);
			}
			else
			{
				Assert.Pass("Skipped because there are no serial ports.");
			}
		}


		[Test]
		public void TestSettingsAreUpdatesByControls()
		{
			_factory.AvailablePorts = new ObservableCollection<string>
			{
				"Foo",
				"Bar"
			};
			_factory.PortDisplayName = "Foo";
			Assert.AreEqual("Foo", (_factory.Settings as SerialPortSettings).SerialPortName);
			_factory.PortDisplayName = "Bar";
			Assert.AreEqual("Bar", (_factory.Settings as SerialPortSettings).SerialPortName);
			_factory.PortDisplayName = "Baz"; // Doesn't exist; previous selection should remain.
			Assert.AreEqual("Bar", (_factory.Settings as SerialPortSettings).SerialPortName);
		}


		[Test]
		public void TestSettingsPropagateToControls()
		{
			_factory.AvailablePorts = new ObservableCollection<string>
			{
				"Foo",
				"Bar"
			};
			_factory.PortDisplayName = "Foo";
			_listener.ClearNotifications();
			_factory.Settings = new SerialPortSettings { SerialPortName = "Bar" };
			Assert.IsTrue(_listener.PropertyWasChanged(nameof(_factory.PortDisplayName)));
		}


		[Test]
		public void TestViewProperties()
		{
			_listener.TestObservableProperty(_factory,
											 () => _factory.AvailablePorts,
											 list =>
											 {
												 var newList = new ObservableCollection<string>(list);
												 newList.Insert(0, "Foo");
												 return newList;
											 });

			_factory.PortDisplayName = "Foo";
			Assert.IsTrue(_listener.PropertyWasChanged(nameof(_factory.PortDisplayName)));
		}


		private SerialPortFactory _factory;
		private ObservableObjectTester _listener;
	}
}

﻿using System;
using System.Linq;
using NUnit.Framework;
using Zaber.Ports;
using ZaberConsole;

namespace ZaberConsoleGUITests.Ports
{
	[TestFixture]
	[SetCulture("en-US")]
	public class DeviceDetectorTests
	{
		[SetUp]
		public void Setup() => _helper = new DeviceDetectorTestHelper();


		[Test]
		public void TestDetectASCII()
		{
			var port = _helper.RawPortForProtocolDetection();
			_helper.AddASCIIDevice(port, 1);
			port.AddReadException(new TimeoutException());
			var result = DeviceDetector.DetectProtocolAndBaudRate(port);
			port.Verify();
			Assert.AreEqual(Protocol.ASCII, result.Item1);
			Assert.AreEqual(DeviceDetectorTestHelper.DEFAULT_BAUD_RATE, result.Item2);
		}


		[Test]
		public void TestDetectAsciiAtLastBaudRate()
		{
			_helper.BaudRates = RawRS232Port.SupportedSerialBaudRates.OrderBy(r => r).ToArray();
			var port = _helper.RawPortForProtocolDetection();
			port.AddReadException(new TimeoutException());
			_helper.AddMagicPing(port);
			port.AddReadException(new TimeoutException());
			_helper.AddMagicPing(port);
			port.AddReadException(new TimeoutException());
			_helper.AddMagicPing(port);
			port.AddReadException(new TimeoutException());
			_helper.AddMagicPing(port);
			_helper.AddASCIIDevice(port, 5);
			port.AddReadException(new TimeoutException());

			var result = DeviceDetector.DetectProtocolAndBaudRate(port);
			port.Verify();
			Assert.AreEqual(Protocol.ASCII, result.Item1);
			Assert.AreEqual(_helper.BaudRates.Last(), result.Item2);
		}


		[Test]
		public void TestDetectBinary()
		{
			var port = _helper.RawPortForProtocolDetection();
			_helper.AddBinaryDevice(port, 1);
			port.AddReadException(new TimeoutException());
			var result = DeviceDetector.DetectProtocolAndBaudRate(port);
			port.Verify();
			Assert.AreEqual(Protocol.Binary, result.Item1);
			Assert.AreEqual(DeviceDetectorTestHelper.DEFAULT_BAUD_RATE, result.Item2);
		}


		[Test]
		public void TestDetectBinaryAtLastBaudRate()
		{
			_helper.BaudRates = RawRS232Port.SupportedSerialBaudRates.OrderBy(r => -r).ToArray();
			var port = _helper.RawPortForProtocolDetection();
			port.AddReadException(new TimeoutException());
			_helper.AddMagicPing(port);
			port.AddReadException(new TimeoutException());
			_helper.AddMagicPing(port);
			port.AddReadException(new TimeoutException());
			_helper.AddMagicPing(port);
			port.AddReadException(new TimeoutException());
			_helper.AddMagicPing(port);
			_helper.AddBinaryDevice(port, 5);
			port.AddReadException(new TimeoutException());

			var result = DeviceDetector.DetectProtocolAndBaudRate(port);
			port.Verify();
			Assert.AreEqual(Protocol.Binary, result.Item1);
			Assert.AreEqual(_helper.BaudRates.Last(), result.Item2);
		}


		[Test]
		public void TestDetectNoDevices()
		{
			var port = _helper.RawPortWithNoDevices();
			var result = DeviceDetector.DetectProtocolAndBaudRate(port);
			port.Verify();
			Assert.AreEqual(Protocol.None, result.Item1);
			Assert.AreEqual(0, result.Item2);
		}


		[Test]
		public void TestMixedResponsesASCIIFirst()
		{
			var port = _helper.RawPortForProtocolDetection();
			_helper.AddASCIIDevice(port, 2);
			_helper.AddBinaryDevice(port, 1);
			port.AddReadException(new TimeoutException());
			Assert.Throws<MismatchedDevicesException>(() => { DeviceDetector.DetectProtocolAndBaudRate(port); });
			port.Verify();
		}


		[Test]
		public void TestMixedResponsesBinaryFirst()
		{
			var port = _helper.RawPortForProtocolDetection();
			_helper.AddBinaryDevice(port, 1);
			_helper.AddASCIIDevice(port, 2);
			port.AddReadException(new TimeoutException());
			Assert.Throws<MismatchedDevicesException>(() => { DeviceDetector.DetectProtocolAndBaudRate(port); });
			port.Verify();
		}


		// TODO (Soleil 2019-08): Test chain flattening. 
		// Not done now because it hasn't yet been updated to work with TCP/IP ports.


		private DeviceDetectorTestHelper _helper;
	}
}

﻿using System;
using Zaber;
using Zaber.Ports;
using ZaberConsole.Ports;

namespace ZaberConsoleGUITests.Ports
{
	public class MockPortFactory : IPortFactory
	{
		#region -- Public Methods & Properties --

		public MockPortFactory(IZaberPort aPort, IRawPort aRawPort)
		{
			_port = aPort;
			_rawPort = aRawPort;
		}


		public IZaberPort CreatePort() => _port;

		public IRawPort CreateRawPort() => _rawPort;

		public PortSettings Settings
		{
			get => throw new NotImplementedException();
			set => throw new NotImplementedException();
		}

		#endregion

		#region -- Data --

		private readonly IZaberPort _port;
		private readonly IRawPort _rawPort;

		#endregion
	}
}

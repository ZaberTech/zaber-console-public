﻿using NUnit.Framework;
using ZaberConsole.Ports;

namespace ZaberConsoleGUITests.Ports
{
	[TestFixture]
	[SetCulture("en-US")]
	public class PortSettingsCollectionTests
	{
		[SetUp]
		public void Setup() => _settings = new PortSettingsCollection();


		[Test]
		public void TestDefaults()
		{
			Assert.AreEqual(-1, _settings.LastSelectedIndex);
			Assert.IsNotNull(_settings.RecentConnections);
			Assert.AreEqual(0, _settings.RecentConnections.Count);
		}


		[Test]
		public void TestGetSelected()
		{
			_settings.RecentConnections.Add(new SerialPortSettings { Name = "Foo" });
			_settings.RecentConnections.Add(new TcpPortSettings { Name = "Bar" });
			var portSettings = _settings.GetLastSelectedSettings();
			Assert.IsNull(portSettings);
			_settings.LastSelectedIndex = 0;
			portSettings = _settings.GetLastSelectedSettings();
			Assert.IsTrue(portSettings is SerialPortSettings);
			Assert.AreEqual("Foo", portSettings.Name);
			_settings.LastSelectedIndex = 1;
			portSettings = _settings.GetLastSelectedSettings();
			Assert.IsTrue(portSettings is TcpPortSettings);
			Assert.AreEqual("Bar", portSettings.Name);
			_settings.LastSelectedIndex = 2;
			portSettings = _settings.GetLastSelectedSettings();
			Assert.IsNull(portSettings);
		}


		private PortSettingsCollection _settings;
	}
}

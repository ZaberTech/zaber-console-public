﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Resources;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using Zaber.Testing;
using ZaberConsole.Ports;

namespace ZaberConsoleGUITests.Ports
{
	[TestFixture]
	[SetCulture("en-US")]
	public class PortSettingDialogTests
	{
		[SetUp]
		public void Setup()
		{
			DispatcherStack.Push(new ImmediateDispatcher());

			var factories = new List<Type>
			{
				typeof(TcpPortFactory),
				typeof(SerialPortFactory)
			};

			_dialog = new PortSettingsDialogVM(factories);
			_watcher = new ObservableObjectTester();
			_watcher.Listen(_dialog);
		}


		[TearDown]
		public void Teardown()
		{
			DispatcherStack.Pop();
			_watcher.Unlisten(_dialog);
		}


		[Test]
		public void TestAddButton()
		{
			_dialog.PortList = CreatePortList();
			_dialog.SelectedPort = _dialog.PortList[0];
			_dialog.AddButtonCommand.Execute(null);
			Assert.AreEqual(3, _dialog.PortList.Count);
			Assert.AreNotEqual(_dialog.PortList[0], _dialog.SelectedPort);
			Assert.AreEqual(_dialog.PortList[2], _dialog.SelectedPort);
			Assert.IsTrue(_dialog.SelectedPort is SerialPortSettings);

			_dialog.SelectedPort = _dialog.PortList[1];
			_dialog.AddButtonCommand.Execute(null);
			Assert.AreEqual(4, _dialog.PortList.Count);
			Assert.AreEqual(_dialog.PortList[3], _dialog.SelectedPort);
			Assert.IsTrue(_dialog.SelectedPort is TcpPortSettings);
		}


		[Test]
		public void TestCancelButton()
		{
			var numCallbacks = 0;

			EventHandler callback = (aSender, aArgs) => { numCallbacks++; };

			_dialog.DialogClosing += callback;
			_dialog.CancelButtonCommand.Execute(null);
			_dialog.DialogClosing -= callback;
			Assert.AreEqual(1, numCallbacks);
			Assert.IsFalse(_dialog.SaveChanges);
		}


		[Test]
		public void TestChangingFactoryType()
		{
			_dialog.PortList = new ObservableCollection<PortSettings>();
			_dialog.SelectedFactoryType = typeof(SerialPortFactory);
			Assert.IsTrue(_dialog.SettingsEditor is SerialPortFactory);
			_dialog.SelectedFactoryType = typeof(TcpPortFactory);
			Assert.IsTrue(_dialog.SettingsEditor is TcpPortFactory);
			_dialog.SelectedFactoryType = null;
			Assert.IsNull(_dialog.SettingsEditor);
		}


		[Test]
		public void TestChangingSelection()
		{
			_dialog.PortList = CreatePortList();
			Assert.IsNull(_dialog.SelectedPort);
			_dialog.SelectedPort = _dialog.PortList[0];
			Assert.IsTrue(_dialog.SettingsEditor is SerialPortFactory);
			_dialog.SelectedPort = _dialog.PortList[1];
			Assert.IsTrue(_dialog.SettingsEditor is TcpPortFactory);
			_dialog.SelectedPort = null;
			Assert.IsNull(_dialog.SettingsEditor);
		}


		[Test]
		public void TestConstructor()
		{
			Assert.IsFalse(_dialog.SaveChanges);
			Assert.IsNull(_dialog.PortList);
			Assert.IsNull(_dialog.SelectedPort);
			Assert.IsNull(_dialog.SettingsEditor);
			Assert.IsFalse(_dialog.IsDropDownOpen);
			Assert.IsFalse(_dialog.IsNewConnection);

			var types = _dialog.FactoryTypes.ToArray();
			Assert.AreEqual(2, types.Length);
			Assert.IsTrue(types[0] == typeof(SerialPortFactory));
			Assert.IsTrue(types[1] == typeof(TcpPortFactory));

			Assert.IsFalse(_dialog.IsTestInProgress);
			Assert.IsNull(_dialog.ConnectionTestResult);
			Assert.IsNull(_dialog.ConnectionTestMessage);
			Assert.IsNull(_dialog.Exception);
			Assert.IsTrue(_dialog.IsModal);
		}


		[Test]
		public void TestDeleteButton()
		{
			_dialog.PortList = CreatePortList();
			_dialog.SelectedPort = _dialog.PortList[0];
			Assert.IsTrue(_dialog.SelectedPort is SerialPortSettings);
			_dialog.DeleteButtonCommand.Execute(null);
			Assert.AreEqual(1, _dialog.PortList.Count);
			Assert.AreEqual(_dialog.PortList[0], _dialog.SelectedPort);
			Assert.IsTrue(_dialog.SelectedPort is TcpPortSettings);
		}


		[Test]
		public void TestObservableProperties()
		{
			_watcher.TestObservableProperty(_dialog,
											() => _dialog.PortList,
											_ =>
											{
												var list = CreatePortList();
												list.Add(new SerialPortSettings());
												return list;
											});

			_dialog.PortList = CreatePortList();
			_watcher.ClearNotifications();
			_watcher.TestObservableProperty(_dialog,
											() => _dialog.SelectedPort,
											_ => { return new TcpPortSettings(); });

			_watcher.TestObservableProperty(_dialog, () => _dialog.IsDropDownOpen);
			_watcher.TestObservableProperty(_dialog,
											() => _dialog.SelectedFactoryType,
											_ => { return typeof(TcpPortFactory); });
		}


		[Test]
		public void TestSaveButton()
		{
			var numCallbacks = 0;

			EventHandler callback = (aSender, aArgs) => { numCallbacks++; };

			_dialog.DialogClosing += callback;
			_dialog.SaveButtonCommand.Execute(null);
			_dialog.DialogClosing -= callback;
			Assert.AreEqual(1, numCallbacks);
			Assert.IsTrue(_dialog.SaveChanges);
		}


		[Test]
		public void TestTestButtonAscii()
		{
			var helper = new DeviceDetectorTestHelper();
			var port = helper.RawPortForProtocolDetection();
			helper.AddASCIIDevice(port, 1);
			port.AddReadException(new TimeoutException());
			var factory = new MockPortFactory(null, port);
			_dialog.SettingsEditor = factory;

			PushTestButton();

			Assert.IsTrue(_dialog.ConnectionTestResult);
			var match = Regex.Match(_dialog.ConnectionTestMessage,
									GetResourceStringAsRegex("STR_CONNECTION_TEST_ASCII"));
			Assert.IsTrue(match.Success);
			port.Verify();
		}


		[Test]
		public void TestTestButtonBinary()
		{
			var helper = new DeviceDetectorTestHelper();
			var port = helper.RawPortForProtocolDetection();
			helper.AddBinaryDevice(port, 1);
			port.AddReadException(new TimeoutException());
			var factory = new MockPortFactory(null, port);
			_dialog.SettingsEditor = factory;

			PushTestButton();

			Assert.IsTrue(_dialog.ConnectionTestResult);
			var match = Regex.Match(_dialog.ConnectionTestMessage,
									GetResourceStringAsRegex("STR_CONNECTION_TEST_BINARY"));
			Assert.IsTrue(match.Success);
			port.Verify();
		}


		[Test]
		public void TestTestButtonError()
		{
			var helper = new DeviceDetectorTestHelper();
			var port = helper.RawPortForProtocolDetection();
			port.AddWriteException(new AggregateException());
			var factory = new MockPortFactory(null, port);
			_dialog.SettingsEditor = factory;

			PushTestButton();

			Assert.IsFalse(_dialog.ConnectionTestResult);
			var match = Regex.Match(_dialog.ConnectionTestMessage,
									GetResourceStringAsRegex("STR_CONNECTION_TEST_ERROR"));
			Assert.IsTrue(match.Success);
			port.Verify();
		}


		[Test]
		public void TestTestButtonMixed()
		{
			var helper = new DeviceDetectorTestHelper();
			var port = helper.RawPortForProtocolDetection();
			helper.AddASCIIDevice(port, 1);
			helper.AddBinaryDevice(port, 2);
			port.AddReadException(new TimeoutException());
			var factory = new MockPortFactory(null, port);
			_dialog.SettingsEditor = factory;

			PushTestButton();

			Assert.IsFalse(_dialog.ConnectionTestResult);
			var match = Regex.Match(_dialog.ConnectionTestMessage,
									GetResourceStringAsRegex("STR_CONNECTION_TEST_ERROR"));
			Assert.IsTrue(match.Success);
			port.Verify();
		}


		[Test]
		public void TestTestButtonNoDevices()
		{
			var helper = new DeviceDetectorTestHelper();
			var port = helper.RawPortWithNoDevices();
			var factory = new MockPortFactory(null, port);
			_dialog.SettingsEditor = factory;

			PushTestButton();

			Assert.IsTrue(_dialog.ConnectionTestResult);
			var match = Regex.Match(_dialog.ConnectionTestMessage,
									GetResourceStringAsRegex("STR_CONNECTION_TEST_NO_DEVICES"));
			Assert.IsTrue(match.Success);
			port.Verify();
		}


		private void PushTestButton()
		{
			_watcher.ClearNotifications();
			_dialog.TestButtonCommand.Execute(null);

			var start = DateTime.Now;
			while (_dialog.IsTestInProgress)
			{
				// TODO (Soleil 2019-08) Wrap Task.Factory.StartNew so we can control Task execution in tests.
				Assert.Greater(10000, (DateTime.Now - start).TotalMilliseconds, "Timed out waiting for test task.");
				Thread.Sleep(100);
			}

			Assert.IsTrue(_watcher.PropertyWasChanged(nameof(_dialog.ConnectionTestMessage)));
			Assert.IsTrue(_watcher.PropertyWasChanged(nameof(_dialog.IsTestInProgress)));
		}


		private ObservableCollection<PortSettings> CreatePortList()
		{
			var result = new ObservableCollection<PortSettings>
			{
				new SerialPortSettings
				{
					Name = "My serial port",
					SerialPortName = "COM1"
				},
				new TcpPortSettings
				{
					Name = "My TCP port",
					HostName = "localhost",
					PortNumber = 1234
				}
			};

			return result;
		}


		private string GetResourceStringAsRegex(string aResourceName)
		{
			var asm = typeof(IPortFactory).Assembly;
			var resMan = new ResourceManager("ZaberConsole.Properties.Resources", asm);
			var str = resMan.GetString(aResourceName);
			var re = Regex.Replace(str, "{.*?}", ".*");
			return re;
		}


		private PortSettingsDialogVM _dialog;
		private ObservableObjectTester _watcher;
	}
}

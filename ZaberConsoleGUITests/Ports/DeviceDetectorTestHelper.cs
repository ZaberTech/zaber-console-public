﻿using System;
using System.Collections.Generic;
using ZaberConsole;
using ZaberTest;

namespace ZaberConsoleGUITests.Ports
{
	public class DeviceDetectorTestHelper
	{
		#region -- Public data --

		public const int DEFAULT_BAUD_RATE = 115200;

		#endregion

		#region -- Public Methods & Properties --

		public MockRawPort RawPortWithNoDevices()
		{
			var port = RawPortForProtocolDetection();
			port.AddReadException(new TimeoutException());
			return port;
		}


		public MockRawPort RawPortForProtocolDetection()
		{
			var port = new MockRawPort();
			port.SupportedBaudRates = BaudRates;
			AddMagicPing(port);
			return port;
		}


		public void AddMagicPing(MockRawPort aPort) => aPort.Expect(DeviceDetector.MAGIC_PING);


		public void AddBinaryDevice(MockRawPort aPort, byte aDeviceNumber)
			=> aPort.AddResponse(new byte[] { aDeviceNumber, 0x37, 0x2F, 0x0D, 0x0A, 0x00 });


		public void AddASCIIDevice(MockRawPort aPort, byte aDeviceNumber)
			=> aPort.AddResponse(string.Format("@{0:00} 0 OK IDLE -- 0", aDeviceNumber));


		public IEnumerable<int> BaudRates { get; set; } = new[] { DEFAULT_BAUD_RATE };

		#endregion
	}
}

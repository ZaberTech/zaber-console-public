﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using NUnit.Framework;
using Zaber.Testing;
using ZaberConsole;
using ZaberTest;
using ZaberTest.Testing;
using ZaberWpfToolbox.Dialogs;

namespace ZaberConsoleGUITests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class DatabaseUpdaterTests
	{
		[SetUp]
		public void Setup()
		{
			_settings = new DatabaseUpdaterSettings();

			DispatcherStack.Push(new ImmediateDispatcher());
			BackgroundWorkerStack.Push(typeof(ImmediateForegroundWorker));
			ThreadTypeStack.Push(typeof(SynchronousThread));

			_dataDir = Path.Combine(TestDataFileHelper.GetDeployedPath("."), "_databaseUpdaterTests_data");
			if (File.Exists(_dataDir))
			{
				File.Delete(_dataDir);
			}

			Directory.CreateDirectory(_dataDir);

			_webDir = Path.Combine(TestDataFileHelper.GetDeployedPath("."), "_databaseUpdaterTests_web");
			Directory.CreateDirectory(_webDir);

			_bundledFile = TestDataFileHelper.GetDeployedPath(Path.Combine("data", FILENAME));
			_webFile = Path.Combine(_webDir, FILENAME + ".lzma");
			FileSystemHelper.CopyFileAndSetWriteTime(
				TestDataFileHelper.GetDeployedPath(Path.Combine("data", FILENAME + ".lzma")),
				_webFile,
				File.GetLastWriteTimeUtc(_bundledFile));
		}


		[TearDown]
		public void Teardown()
		{
			while (_vm.IsBusy)
			{
				Thread.Sleep(100);
			}
			_vm.Shutdown();

			ThreadTypeStack.Pop();
			BackgroundWorkerStack.Pop();
			DispatcherStack.Pop();

			if (null != _vm)
			{
				_listener?.Unlisten(_vm);
			}

			var success = false;
			var tries = 0;
			while (!success)
			{
				TempFileManager.TryDeleteFiles();
				try
				{
					if (Directory.Exists(_dataDir))
					{
						FileSystemHelper.DeleteDirectoryRecursive(_dataDir);
					}
					else if (File.Exists(_dataDir))
					{
						File.Delete(_dataDir);
					}

					FileSystemHelper.DeleteDirectoryRecursive(_webDir);
					success = true;
				}
				catch (IOException)
				{
					Thread.Sleep(100);
					tries++;
				}

				if (tries > 500)
				{
					throw new IOException("Cannot delete temporary test files!");
				}
			}
		}


		[Test]
		public void RM6247_TestMissingDownloadDir()
		{
			FileSystemHelper.DeleteDirectoryRecursive(_dataDir);

			// Before the main fix this would have thrown an exception because the dir didn't exist.
			CreateVM();

			// If we can create the directory, it should now exist.
			Assert.IsTrue(Directory.Exists(_dataDir));
			Assert.AreEqual(DatabaseUpdaterVM.State.UpToDate, _vm.CurrentState);
		}


		[Test]
		public void RM6247_TestMissingDownloadDirAndCannotCreate()
		{
			FileSystemHelper.DeleteDirectoryRecursive(_dataDir);

			// Prevent the directory from being created.
			using (File.Create(_dataDir))
			{
				// Before the fix's additional error handling this would have thrown an exception also.
				CreateVM();
			}

			// If we can't create the dir, the updater should disable itself.
			Assert.AreEqual(DatabaseUpdaterVM.State.Error, _vm.CurrentState);
			Assert.IsFalse(_vm.ButtonCommand.CanExecute(null));
		}


		[Test]
		public void TestButtonWhenNoUpdatesAvailable()
		{
			var keeper = Path.Combine(_dataDir, FILENAME + ".1");
			var timestamp = DateTime.UtcNow;
			FileSystemHelper.CopyFileAndSetWriteTime(_bundledFile, keeper, timestamp);
			CreateVM();
			CreateListener();
			Assert.AreEqual(keeper, _vm.LatestDatabasePath);

			Assert.AreEqual(DatabaseUpdaterVM.State.UpToDate, _vm.CurrentState);

			Assert.IsTrue(_vm.ButtonCommand.CanExecute(null));
			_listener.ClearNotifications();

			_vm.ButtonCommand.Execute(null);
			Assert.IsTrue(_listener.PropertyWasChanged(nameof(_vm.CurrentState)));
			if (Configuration.IsPublicBuild)
			{
				Assert.AreEqual(DatabaseUpdaterVM.State.UpToDate, _vm.CurrentState);
			}
			else
			{
				Assert.AreEqual(DatabaseUpdaterVM.State.UpdateAvailable, _vm.CurrentState);
			}

			// Pressing the button again should have the same result.
			_listener.ClearNotifications();
			_vm.ButtonCommand.Execute(null);

			Assert.IsTrue(_listener.PropertyWasChanged(nameof(_vm.CurrentState)));
			if (Configuration.IsPublicBuild)
			{
				Assert.AreEqual(DatabaseUpdaterVM.State.UpToDate, _vm.CurrentState);
			}
			else
			{
				Assert.AreEqual(DatabaseUpdaterVM.State.UpdateReady, _vm.CurrentState);
			}
		}


		[Test]
		public void TestButtonWhenUpdatesAreAvailable()
		{
			var keeper = Path.Combine(_dataDir, FILENAME + ".1");
			var timestamp = File.GetLastWriteTimeUtc(_bundledFile) + TimeSpan.FromMilliseconds(10);
			FileSystemHelper.CopyFileAndSetWriteTime(_bundledFile, keeper, timestamp);
			CreateVM();
			CreateListener();
			Assert.AreEqual(keeper, _vm.LatestDatabasePath);
			Assert.AreEqual(1, CountFilesInDownloadDir());

			// Make sure a newer file is available for download.
			File.SetLastWriteTimeUtc(_webFile, DateTime.UtcNow + TimeSpan.FromSeconds(1));

			Assert.AreEqual(DatabaseUpdaterVM.State.UpToDate, _vm.CurrentState);

			Assert.IsTrue(_vm.ButtonCommand.CanExecute(null));
			_listener.ClearNotifications();

			// Clicking the button should say an update is available but change no files.
			_vm.ButtonCommand.Execute(null);
			Assert.IsTrue(_listener.PropertyWasChanged(nameof(_vm.CurrentState)));
			Assert.AreEqual(DatabaseUpdaterVM.State.UpdateAvailable, _vm.CurrentState);
			Assert.AreEqual(1, CountFilesInDownloadDir());
			Assert.IsTrue(File.Exists(keeper));

			// Pressing the button again should download the file.
			_listener.ClearNotifications();
			_vm.ButtonCommand.Execute(null);

			Assert.IsTrue(_listener.PropertyWasChanged(nameof(_vm.CurrentState)));
			Assert.AreEqual(DatabaseUpdaterVM.State.UpdateReady, _vm.CurrentState);
			Assert.AreEqual(2, CountFilesInDownloadDir());
			Assert.IsTrue(File.Exists(keeper));

			// Now switch to the new download.
			Assert.AreNotEqual(keeper, _vm.LatestDatabasePath);
			Assert.IsTrue(File.Exists(_vm.LatestDatabasePath));
			_vm.CleanupOldDatabaseFiles();
			Assert.IsFalse(File.Exists(keeper));
			Assert.AreEqual(1, CountFilesInDownloadDir());

			_vm.NotifyDatabaseInUse(_vm.LatestDatabasePath);
			Assert.AreEqual(DatabaseUpdaterVM.State.UpToDate, _vm.CurrentState);
		}


		[Test]
		public void TestCheckForUpdatesWhenAlreadyHaveNewerFile()
		{
			var keeper = Path.Combine(_dataDir, FILENAME + ".1");
			var timestamp = DateTime.UtcNow;
			FileSystemHelper.CopyFileAndSetWriteTime(_bundledFile, keeper, timestamp);
			CreateVM();
			CreateListener();
			Assert.AreEqual(keeper, _vm.LatestDatabasePath);

			_vm.CheckForUpdates(timestamp);
			Assert.IsTrue(_listener.PropertyWasChanged(nameof(_vm.CurrentState)));
			Assert.AreEqual(DatabaseUpdaterVM.State.UpToDate, _vm.CurrentState);
		}


		[Test]
		public void TestCheckForUpdatesWhenUpdateNeeded()
		{
			var keeper = Path.Combine(_dataDir, FILENAME + ".1");
			var timestamp = new DateTime(1972, 3, 20);
			FileSystemHelper.CopyFileAndSetWriteTime(_bundledFile, keeper, timestamp);
			CreateVM();
			CreateListener();
			Assert.AreEqual(_bundledFile, _vm.LatestDatabasePath);

			// Make sure the downloadable file appears newer than the recommended file.
			File.SetLastWriteTimeUtc(_bundledFile, DateTime.UtcNow);

			_vm.CheckForUpdates(timestamp);
			Assert.IsTrue(_listener.PropertyWasChanged(nameof(_vm.CurrentState)));
			Assert.AreEqual(DatabaseUpdaterVM.State.UpdateAvailable, _vm.CurrentState);
		}


		[Test]
		public void TestCheckTriggeredWhenAutoCheckEnabled()
		{
			var keeper = Path.Combine(_dataDir, FILENAME + ".1");
			FileSystemHelper.CopyFileAndSetWriteTime(_bundledFile, keeper, new DateTime(1972, 3, 20));
			_settings.AutomaticallyCheckForUpdates = false;
			CreateVM();
			Assert.IsFalse(_vm.AutoCheckForUpdates, "Precondition check");
			CreateListener();

			Assert.AreEqual(DatabaseUpdaterVM.State.Uncertain, _vm.CurrentState);

			_vm.AutoCheckForUpdates = true;
			Assert.IsTrue(_listener.PropertyWasChanged(nameof(_vm.CurrentState)));
			if (Configuration.IsPublicBuild)
			{
				Assert.AreEqual(DatabaseUpdaterVM.State.UpToDate, _vm.CurrentState);
			}
			else
			{
				Assert.AreEqual(DatabaseUpdaterVM.State.UpdateAvailable, _vm.CurrentState);
			}
		}


		[Test]
		public void TestCleanupFilesOlderThanBundled()
		{
			FileSystemHelper.CopyFileAndSetWriteTime(_bundledFile,
													 Path.Combine(_dataDir, FILENAME + ".1"),
													 new DateTime(1972, 3, 20));
			FileSystemHelper.CopyFileAndSetWriteTime(_bundledFile,
													 Path.Combine(_dataDir, FILENAME + ".2"),
													 new DateTime(2000, 1, 1));
			Assert.AreEqual(2, CountFilesInDownloadDir());
			CreateVM();
			Assert.AreEqual(0, CountFilesInDownloadDir());
		}


		[Test]
		[Apartment(ApartmentState.STA)]
		public void TestCopyUrl()
		{
			CreateVM();
			Assert.IsTrue(_vm.GetUrlCommand.CanExecute(null));
			_vm.GetUrlCommand.Execute(null);
			var url = Clipboard.GetText();
			Assert.AreEqual(Configuration.DatabaseUrl, url);
		}


		[Test]
		public void TestDefaults()
		{
			CreateVM();
			Assert.AreEqual(DatabaseUpdaterVM.State.UpToDate, _vm.CurrentState);
			Assert.IsNotNull(_vm.ButtonCommand);
			Assert.IsFalse(_vm.IsBusy);
			Assert.AreEqual(1.0, _vm.ProgressMaximum);
			Assert.AreEqual(0.0, _vm.Progress);
			Assert.IsFalse(_vm.UsingImportedDatabase);
			Assert.IsNull(_vm.ErrorMessage);
		}


		[Test]
		public void TestDownloadReplacesImport()
		{
			var import = Path.Combine(_dataDir, FILENAME + ".1");
			FileSystemHelper.CopyFileAndSetWriteTime(_bundledFile, import, new DateTime(1972, 03, 20));
			_settings.ImportedDatabasePath = import;
			CreateVM();
			Assert.AreEqual(import, _vm.LatestDatabasePath);
			Assert.AreEqual(1, CountFilesInDownloadDir());

			// Make sure a newer file is available for download.
			File.SetLastWriteTimeUtc(_webFile, DateTime.UtcNow + TimeSpan.FromSeconds(1));

			// Clicking the button should say an update is available but change no files.
			_vm.ButtonCommand.Execute(null);
			Assert.AreEqual(DatabaseUpdaterVM.State.UpdateAvailable, _vm.CurrentState);
			Assert.AreEqual(1, CountFilesInDownloadDir());

			// Pressing the button again should download the file and make it the recommended file.
			_vm.ButtonCommand.Execute(null);

			Assert.AreEqual(2, CountFilesInDownloadDir());
			Assert.IsTrue(File.Exists(import));
			Assert.AreNotEqual(import, _vm.LatestDatabasePath);

			// Doing a cleanup should delete the imported file.
			_vm.CleanupOldDatabaseFiles();
			Assert.IsFalse(File.Exists(import));
			Assert.AreEqual(1, CountFilesInDownloadDir());
		}


		[Test]
		public void RM6338_TestAutoDownload()
		{
			_settings.AutomaticallyDownloadUpdates = true;
			CreateVM();

			// Checking for updates should download the file in this case.
			Assert.AreEqual(0, CountFilesInDownloadDir());
			_vm.CheckForUpdates(new DateTime(1972, 3, 20));
			Assert.AreEqual(DatabaseUpdaterVM.State.UpdateReady, _vm.CurrentState);
			Assert.AreEqual(1, CountFilesInDownloadDir());
		}


		[Test]
		public void TestExport()
		{
			CreateVM();
			Assert.AreEqual(0, CountFilesInDownloadDir());
			Assert.AreEqual(_bundledFile, _vm.LatestDatabasePath);

			var destFile = Path.Combine(_dataDir, "foo.sqlite");

			RequestDialogChange dialogHandler = (_, dlg) =>
			{
				if (dlg is FileBrowserDialogParams dlgVM)
				{
					dlgVM.Filename = destFile;
					dlgVM.DialogResult = true;
				}
				else
				{
					Assert.Fail("Unexpected dialog,");
				}
			};

			_vm.RequestDialogOpen += dialogHandler;

			Assert.IsTrue(_vm.ExportCommand.CanExecute(null));
			_vm.ExportCommand.Execute(null);
			_vm.RequestDialogOpen -= dialogHandler;
			Assert.AreEqual(1, CountFilesInDownloadDir());
			Assert.IsTrue(File.Exists(destFile));
			Assert.AreEqual(new FileInfo(_bundledFile).Length, new FileInfo(destFile).Length);
			Assert.AreEqual(File.GetLastWriteTimeUtc(_bundledFile), File.GetLastWriteTimeUtc(destFile));
			Assert.AreEqual(DatabaseUpdaterVM.State.UpToDate, _vm.CurrentState);

			Assert.AreEqual(Path.GetDirectoryName(destFile), _settings.LastExportSettings.Directory);
			Assert.AreEqual(Path.GetFileName(destFile), _settings.LastExportSettings.Filename);
		}


		[Test]
		public void TestExportCompressed()
		{
			CreateVM();
			Assert.AreEqual(0, CountFilesInDownloadDir());
			Assert.AreEqual(_bundledFile, _vm.LatestDatabasePath);

			var destFile = Path.Combine(_dataDir, "foo.sqlite.lzma");

			RequestDialogChange dialogHandler = (_, dlg) =>
			{
				if (dlg is FileBrowserDialogParams dlgVM)
				{
					dlgVM.Filename = destFile;
					dlgVM.DialogResult = true;
				}
				else
				{
					Assert.Fail("Unexpected dialog,");
				}
			};

			_vm.RequestDialogOpen += dialogHandler;

			Assert.IsTrue(_vm.ExportCommand.CanExecute(null));
			_vm.ExportCommand.Execute(null);
			_vm.RequestDialogOpen -= dialogHandler;
			Assert.AreEqual(1, CountFilesInDownloadDir());
			Assert.IsTrue(File.Exists(destFile));
			Assert.Greater(new FileInfo(_bundledFile).Length, new FileInfo(destFile).Length);
			Assert.AreEqual(File.GetLastWriteTimeUtc(_bundledFile), File.GetLastWriteTimeUtc(destFile));
			Assert.AreEqual(DatabaseUpdaterVM.State.UpToDate, _vm.CurrentState);

			Assert.AreEqual(Path.GetDirectoryName(destFile), _settings.LastExportSettings.Directory);
			Assert.AreEqual(Path.GetFileName(destFile), _settings.LastExportSettings.Filename);
		}


		[Test]
		public void TestFindLatestFileAndDeleteOthers()
		{
			var keeper = Path.Combine(_dataDir, FILENAME + ".2");
			FileSystemHelper.CopyFileAndSetWriteTime(_bundledFile,
													 Path.Combine(_dataDir, FILENAME + ".1"),
													 new DateTime(1972, 3, 20));
			FileSystemHelper.CopyFileAndSetWriteTime(_bundledFile, keeper, DateTime.UtcNow);
			FileSystemHelper.CopyFileAndSetWriteTime(_bundledFile,
													 Path.Combine(_dataDir, FILENAME + ".3"),
													 new DateTime(2000, 1, 1));
			Assert.AreEqual(3, CountFilesInDownloadDir());
			CreateVM();
			Assert.AreEqual(1, CountFilesInDownloadDir());
			Assert.AreEqual(keeper, _vm.LatestDatabasePath);
			Assert.IsTrue(File.Exists(keeper));
			Assert.IsTrue(File.Exists(_bundledFile)); // Make sure the bundled file wasn't deleted.
		}


		[Test]
		public void TestImport()
		{
			_settings.AutomaticallyCheckForUpdates = true;

			var keeper = Path.Combine(_dataDir, FILENAME + ".2");
			FileSystemHelper.CopyFileAndSetWriteTime(_bundledFile, keeper, DateTime.UtcNow);
			CreateVM();
			Assert.AreEqual(1, CountFilesInDownloadDir());
			Assert.AreEqual(keeper, _vm.LatestDatabasePath);

			// We're importing an older file here, but because it's a manual import
			// it should become the recommended file and other files should be removed.
			RequestDialogChange dialogHandler = (_, dlg) =>
			{
				if (dlg is FileBrowserDialogParams dlgVM)
				{
					dlgVM.Filename = _bundledFile;
					dlgVM.DialogResult = true;
				}
				else
				{
					Assert.Fail("Unexpected dialog,");
				}
			};

			_vm.RequestDialogOpen += dialogHandler;

			Assert.IsTrue(_vm.ImportCommand.CanExecute(null));
			_vm.ImportCommand.Execute(null);
			_vm.RequestDialogOpen -= dialogHandler;
			Assert.AreEqual(2, CountFilesInDownloadDir());

			var newFile = _vm.LatestDatabasePath;
			Assert.AreNotEqual(keeper, newFile);
			Assert.IsTrue(File.Exists(newFile));
			Assert.AreEqual(new FileInfo(_bundledFile).Length, new FileInfo(newFile).Length);
			Assert.AreEqual(File.GetLastWriteTimeUtc(_bundledFile), File.GetLastWriteTimeUtc(newFile));
			Assert.AreEqual(DatabaseUpdaterVM.State.UpdateReady, _vm.CurrentState);

			_vm.CleanupOldDatabaseFiles();
			Assert.AreEqual(1, CountFilesInDownloadDir());
			Assert.IsTrue(File.Exists(newFile));

			// Update checks should not be disabled though.
			Assert.IsTrue(_settings.AutomaticallyCheckForUpdates);
			Assert.AreEqual(Path.GetDirectoryName(_bundledFile), _settings.LastImportSettings.Directory);
			Assert.AreEqual(Path.GetFileName(_bundledFile), _settings.LastImportSettings.Filename);
		}


		[Test]
		public void TestImportComprssed()
		{
			_settings.AutomaticallyCheckForUpdates = true;
			CreateVM();
			Assert.AreEqual(0, CountFilesInDownloadDir());
			Assert.AreEqual(_bundledFile, _vm.LatestDatabasePath);

			// We're importing an older file here, but because it's a manual import
			// it should become the recommended file and other files should be removed.
			RequestDialogChange dialogHandler = (_, dlg) =>
			{
				if (dlg is FileBrowserDialogParams dlgVM)
				{
					dlgVM.Filename = _webFile;
					dlgVM.DialogResult = true;
				}
				else
				{
					Assert.Fail("Unexpected dialog,");
				}
			};

			_vm.RequestDialogOpen += dialogHandler;

			Assert.IsTrue(_vm.ImportCommand.CanExecute(null));
			_vm.ImportCommand.Execute(null);
			_vm.RequestDialogOpen -= dialogHandler;
			Assert.AreEqual(1, CountFilesInDownloadDir());

			var newFile = _vm.LatestDatabasePath;
			Assert.AreNotEqual(_bundledFile, newFile);
			Assert.IsTrue(File.Exists(newFile));
			Assert.AreEqual(new FileInfo(_bundledFile).Length,
							new FileInfo(newFile).Length); // Assumes webfile is compressed bundled file.
			Assert.AreEqual(File.GetLastWriteTimeUtc(_bundledFile), File.GetLastWriteTimeUtc(newFile));
			Assert.AreEqual(DatabaseUpdaterVM.State.UpdateReady, _vm.CurrentState);

			_vm.CleanupOldDatabaseFiles();
			Assert.AreEqual(1, CountFilesInDownloadDir());
			Assert.IsTrue(File.Exists(newFile));

			// Update checks should not be disabled though.
			Assert.IsTrue(_settings.AutomaticallyCheckForUpdates);
			Assert.AreEqual(Path.GetDirectoryName(_webFile), _settings.LastImportSettings.Directory);
			Assert.AreEqual(Path.GetFileName(_webFile), _settings.LastImportSettings.Filename);
		}


		[Test]
		public void TestInitWithNoExistingFiles()
		{
			CreateVM();
			Assert.AreEqual(0, CountFilesInDownloadDir());
			Assert.AreEqual(_bundledFile, _vm.LatestDatabasePath);
		}


		[Test]
		public void TestInitWithAutoUpdateCheckDisabled()
		{
			_settings.AutomaticallyCheckForUpdates = false;
			CreateVM();
			Assert.AreEqual(DatabaseUpdaterVM.State.Uncertain, _vm.CurrentState);
		}


		[Test]
		public void TestKeepImportEvenIfOlder()
		{
			_settings.ImportedDatabasePath = Path.Combine(_dataDir, FILENAME + ".1");
			FileSystemHelper.CopyFileAndSetWriteTime(_bundledFile,
													 _settings.ImportedDatabasePath,
													 new DateTime(1972, 3, 20));
			FileSystemHelper.CopyFileAndSetWriteTime(_bundledFile,
													 Path.Combine(_dataDir, FILENAME + ".2"),
													 DateTime.Now);
			Assert.AreEqual(2, CountFilesInDownloadDir());
			CreateVM();
			Assert.AreEqual(1, CountFilesInDownloadDir());
			Assert.AreEqual(_settings.ImportedDatabasePath, _vm.LatestDatabasePath);
			Assert.IsTrue(File.Exists(_settings.ImportedDatabasePath));
		}


		[Test]
		public void TestObservableProperties()
		{
			CreateVM();
			CreateListener();
			_listener.TestObservableProperty(_vm, () => _vm.Progress);
			_listener.TestObservableProperty(_vm, () => _vm.ProgressMaximum);
			_listener.TestObservableProperty(_vm, () => _vm.ErrorMessage);
			_listener.TestObservableProperty(_vm,
											 () => _vm.CurrentState,
											 x =>
											 {
												 return (DatabaseUpdaterVM.State) (((int) x + 1)
																			   % Enum
																			 .GetValues(typeof(DatabaseUpdaterVM.State))
																			 .Length);
											 });
		}


		// TODO (Soleil 2019-09): The following code paths of the DatabaseUpdaterVM are not tested:
		// * Verify the intermediate messages displayed temporarily during update checks and downloads.
		//   This could be done by having the ObservableObjectTester record all values assigned to a property,
		//   or by listening to the property changed event and checking for the expected value in the 
		//   event handler.
		// * Test behavior when cancelling a download. 
		//   This can't be done with the existing test dispatcher and background worker implementations, and
		//   can't be done reliably with the default implementations because it's timing dependent; the
		//   cancel button press has to occur while the download is in progress.
		// * If possible, test error handling during update checks, during downloads, and test error 
		//   handling during file move/delete operations. Not sure how to accomplish any of these.


		private void CreateVM()
			=> _vm = new DatabaseUpdaterVM(_dataDir, FILENAME, new Uri(_webFile).AbsoluteUri, _bundledFile, _settings);


		private void CreateListener()
		{
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
		}


		private int CountFilesInDownloadDir() => Directory.EnumerateFiles(_dataDir).Count();


		private DateTime GetFileTimestamp(string aPath) => File.GetLastWriteTimeUtc(aPath);


		private DatabaseUpdaterSettings _settings;
		private DatabaseUpdaterVM _vm;
		private ObservableObjectTester _listener;

		private string _dataDir;
		private string _webDir;
		private string _bundledFile;
		private string _webFile;
		private static readonly string FILENAME = "foo.sqlite";
	}
}

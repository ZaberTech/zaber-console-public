﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using ZaberConsole;
using ZaberTest.Testing;

namespace ZaberConsoleTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class FileUtilTest
	{
		[SetUp]
		public void Setup()
		{
			_tempDir = Path.GetFullPath(Path.Combine(Path.GetTempPath(), "FileUtilTest"));
			GC.WaitForPendingFinalizers();
			FileSystemHelper
			.DeleteDirectoryRecursive(_tempDir); // Get rid of garbage from aborted previous runs, if any.
			Directory.CreateDirectory(_tempDir);
		}


		[TearDown]
		public void Teardown()
		{
			GC.WaitForPendingFinalizers(); // Should prevent most file locking problems.
			FileSystemHelper.DeleteDirectoryRecursive(_tempDir);
		}


		[Test]
		public void TestDetectConflict()
		{
			var path1 = CreateTempFile("source\\file1", 123, 1);
			var path2 = CreateTempFile("source\\subdir\\file2", 234, 2);
			var path3 = CreateTempFile("dest\\file1", 345, 3);
			var path4 = CreateTempFile("dest\\subdir\\file2", 456, 4);

			var output = FileUtil.CopyDirectory(Path.Combine(_tempDir, "source"),
												Path.Combine(_tempDir, "dest"),
												FileUtil.CollisionResolveMode.PreviewCollisions);

			Assert.IsTrue(output.Any());
			var results = new HashSet<string>(output);
			Assert.IsTrue(results.Contains(path3));
			Assert.IsTrue(results.Contains(path4));
			Assert.AreEqual(results.Count, 2);
		}


		[Test]
		public void TestForceOverwrite()
		{
			var path1 = CreateTempFile("source\\file1", 123, 1);
			var path2 = CreateTempFile("dest\\file1", 123, 2);
			Assert.IsFalse(FileUtil.FilesAreEqual(new FileInfo(path1), new FileInfo(path2)));

			var output = FileUtil.CopyDirectory(Path.Combine(_tempDir, "source"),
												Path.Combine(_tempDir, "dest"),
												FileUtil.CollisionResolveMode.ForceOverwrite);

			Assert.IsFalse(output.Any());

			Assert.IsTrue(FileUtil.FilesAreEqual(new FileInfo(path1), new FileInfo(path2)));
		}


		[Test]
		public void TestGetPathInDirectory_InDirectory()
		{
			var path = FileUtil.GetPathInDirectory("c:/Awesome Directory", "c:/Awesome Directory/a.txt");
			Assert.AreEqual("a.txt", path);
		}


		[Test]
		public void TestGetPathInDirectory_NotInDirectory()
		{
			var path = FileUtil.GetPathInDirectory("c:/Awesome Directory 2", "c:/Awesome Directory/a.txt");
			Assert.AreEqual("c:/Awesome Directory/a.txt", path);
		}


		[Test]
		public void TestIsInDirectory()
		{
			Assert.IsTrue(FileUtil.IsInDirectory("c:/Awesome Directory", "c:/Awesome Directory/a.txt"));
			Assert.IsTrue(FileUtil.IsInDirectory("c:/Awešome Directořy", "c:/Awešome Directořy/a.txt"));

			Assert.IsFalse(FileUtil.IsInDirectory("c:/Awešome Directořy", "c:/Awesome Directory/a.txt"));
			Assert.IsFalse(FileUtil.IsInDirectory("c:/Awesome Directory", "c:/Awesome Directory 2/a.txt"));
			Assert.IsFalse(FileUtil.IsInDirectory("c:/Awesome Directory", "c:/a.txt"));
		}


		[Test]
		public void TestNoConflictIfSame()
		{
			var path1 = CreateTempFile("source\\file1", 123, 1);
			var path2 = CreateTempFile("source\\subdir\\file2", 234, 2);
			var path3 = CreateTempFile("dest\\file1", 123, 1);
			var path4 = CreateTempFile("dest\\subdir\\file2", 234, 2);

			var output = FileUtil.CopyDirectory(Path.Combine(_tempDir, "source"),
												Path.Combine(_tempDir, "dest"),
												FileUtil.CollisionResolveMode.PreviewCollisions);

			Assert.IsFalse(output.Any());
		}


		[Test]
		public void TestNormalizePath()
		{
			var normalized = FileUtil.NormalizePath(@"c:/Awešome Directory\even///more\\awesome/\");
			Assert.AreEqual(@"c:\Awešome Directory\even\more\awesome", normalized);
		}


		[Test]
		public void TestRecursiveCopy()
		{
			var path1 = CreateTempFile("source\\file1", 123, 1);
			var path2 = CreateTempFile("source\\subdir1\\file2", 234, 2);
			var path3 = CreateTempFile("source\\subdir1\\subdir2\\file3", 345, 3);

			var output = FileUtil.CopyDirectory(Path.Combine(_tempDir, "source"),
												Path.Combine(_tempDir, "dest"),
												FileUtil.CollisionResolveMode.ForceOverwrite);

			Assert.IsFalse(output.Any());

			Assert.IsTrue(FileUtil.FilesAreEqual(new FileInfo(path1),
												 new FileInfo(Path.Combine(_tempDir, "dest\\file1"))));
			Assert.IsTrue(FileUtil.FilesAreEqual(new FileInfo(path2),
												 new FileInfo(Path.Combine(_tempDir, "dest\\subdir1\\file2"))));
			Assert.IsTrue(FileUtil.FilesAreEqual(new FileInfo(path3),
												 new FileInfo(
													 Path.Combine(_tempDir, "dest\\subdir1\\subdir2\\file3"))));
		}


		[Test]
		public void TestSkipConflicts()
		{
			var path1 = CreateTempFile("source\\file1", 123, 1);
			var path3 = CreateTempFile("source\\file2", 123, 1);
			var path2 = CreateTempFile("dest\\file1", 234, 2);
			Assert.IsFalse(FileUtil.FilesAreEqual(new FileInfo(path1), new FileInfo(path2)));

			var output = FileUtil.CopyDirectory(Path.Combine(_tempDir, "source"),
												Path.Combine(_tempDir, "dest"),
												FileUtil.CollisionResolveMode.SkipCollisions);

			Assert.IsFalse(output.Any());

			Assert.IsFalse(FileUtil.FilesAreEqual(new FileInfo(path1), new FileInfo(path2)));
			Assert.IsTrue(FileUtil.FilesAreEqual(new FileInfo(path3),
												 new FileInfo(Path.Combine(_tempDir, "dest", "file2"))));
		}


		[Test]
		public void TestSkipOverwriteIfIdentical()
		{
			var path1 = CreateTempFile("source\\file1", 123, 1);
			var path2 = CreateTempFile("dest\\file1", 123, 1);
			var fi1 = new FileInfo(path2);
			Assert.IsTrue(FileUtil.FilesAreEqual(new FileInfo(path1), fi1));

			Thread.Sleep(1); // Force a delay so file timestamp would be different if the file was copied.
			var output = FileUtil.CopyDirectory(Path.Combine(_tempDir, "source"),
												Path.Combine(_tempDir, "dest"),
												FileUtil.CollisionResolveMode.ForceOverwrite);

			Assert.IsFalse(output.Any());

			Assert.IsTrue(FileUtil.FilesAreEqual(new FileInfo(path1), new FileInfo(path2)));

			var fi2 = new FileInfo(path2);
			Assert.AreEqual(fi1.LastWriteTime, fi1.LastWriteTime);
		}


		private string CreateTempFile(string aRelativePath, int aSize, int aContentSeed)
		{
			var filePath = Path.Combine(_tempDir, aRelativePath);
			var dirName = Path.GetDirectoryName(filePath);
			if (Path.GetFullPath(dirName) != Path.GetFullPath(_tempDir))
			{
				Directory.CreateDirectory(dirName);
			}

			var r = new Random(aContentSeed);
			var data = new byte[aSize];
			r.NextBytes(data);

			using (var file = File.Create(filePath))
			{
				file.Write(data, 0, aSize);
			}

			return filePath;
		}


		private string _tempDir;
	}
}

﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Zaber;
using ZaberConsole;
using ZaberTest;
using ZaberTest.Testing;

namespace ZaberConsoleGUITests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class AsciiCommandHelperTests
	{
		[Test]
		public void TestBaseCases()
		{
			// Null input.
			var result = AsciiCommandHelper.ListAsciiCommandCompletions(null);
			Assert.IsNotNull(result);
			Assert.AreEqual(0, result.Count());

			// Empty input;
			result = AsciiCommandHelper.ListAsciiCommandCompletions(new List<CommandInfo>());
			Assert.IsNotNull(result);
			Assert.AreEqual(0, result.Count());
		}


		[Test]
		public void TestBasicParams()
		{
			var commands = AsciiCommandBuilder.ReadCommandDefinitionsFromJsonFile(PATH_BASIC_PARAMS);
			var result = AsciiCommandHelper.ListAsciiCommandCompletions(commands);
			AssertUtils.SequenceEqual(new[]
									  {
										  "joystick calibrate deadbands save", "joystick calibrate deadbands start",
										  "joystick calibrate limits save", "joystick calibrate limits start",
										  "move abs [pos]", "servo [set] get type"
									  },
									  result);
		}


		[Test]
		public void TestLiterals()
		{
			var commands = AsciiCommandBuilder.ReadCommandDefinitionsFromJsonFile(PATH_LITERALS);
			var result = AsciiCommandHelper.ListAsciiCommandCompletions(commands);
			AssertUtils.SequenceEqual(new[] { "help", "help commands", "help commands list", "home", "move min" },
									  result);
		}


		[Test]
		public void TestRepeatedParams()
		{
			var commands = AsciiCommandBuilder.ReadCommandDefinitionsFromJsonFile(PATH_REPEATED_PARAMS);
			var result = AsciiCommandHelper.ListAsciiCommandCompletions(commands);
			AssertUtils.SequenceEqual(new[]
									  {
										  "io set ao port [values...]", "io set do port [values...]",
										  "key [num] [action] add [device] [cmd...]",
										  "stream [num] on [axes...] line abs [pos...]"
									  },
									  result);
		}


		private static readonly string PATH_LITERALS = TestDataFileHelper.GetDeployedPath("data\\cmdliterals.json");
		private static readonly string PATH_BASIC_PARAMS = TestDataFileHelper.GetDeployedPath("data\\cmdbasics.json");

		private static readonly string PATH_REPEATED_PARAMS =
			TestDataFileHelper.GetDeployedPath("data\\cmdrepeated.json");
	}
}

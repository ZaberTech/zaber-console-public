﻿using NUnit.Framework;
using ZaberConsole.Web;

namespace ZaberConsoleTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class AsyncWebRequestTest
	{
		//[Test]
		//public void TestSynchronous()
		//{
		//	AsyncWebRequest req = new AsyncWebRequest(GOOD_URL, 5000);
		//	req.Begin();
		//	Stream data = req.GetResponse(true);
		//	Assert.IsFalse(req.Error, "Unexpected error: " + req.ErrorMessage);
		//	Assert.IsNotNull(data, "No response stream");
		//	Assert.Less(0L, data.Length, "No response data");
		//}


		//[Test]
		//public void TestAsynchronous()
		//{
		//	DateTime start = DateTime.Now;
		//	AsyncWebRequest req = new AsyncWebRequest(GOOD_URL, 10000);
		//	req.Begin();

		//	bool wasFalse = false;
		//	bool wasTrue = false;
		//	while ((DateTime.Now - start).Seconds < 7)
		//	{
		//		wasFalse |= !req.IsFinished;
		//		wasTrue |= req.IsFinished;
		//		if (wasTrue)
		//		{
		//			break;
		//		}

		//		Thread.Yield();
		//	}

		//	Assert.IsTrue(wasFalse, "Request completed too fast.");
		//	Assert.IsTrue(wasTrue, "Request did not succeed.");

		//	Stream data = req.GetResponse();
		//	Assert.IsFalse(req.Error, "Unexpected error: " + req.ErrorMessage);
		//	Assert.IsNotNull(data, "No response stream");
		//	Assert.Less(0L, data.Length, "No response data");
		//}


		[Test]
		public void Test404()
		{
			var req = new AsyncWebRequest(BAD_URL, 5000);
			req.Begin();
			var data = req.GetResponse(true);
			Assert.IsTrue(req.Error, "Expected an error message.");
			Assert.IsNull(data, "No response stream should have been provided.");
		}


		//private static readonly string GOOD_URL = "https://dashboard.izaber.com/dashboard";
		private static readonly string BAD_URL = "https://badurl.izaber.com/"; // 404
	}
}

﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using NUnit.Framework;
using Zaber;
using Zaber.PlugIns;
using Zaber.Testing;
using ZaberConsole.Plugins.Terminal;
using ZaberTest;
using ZaberWpfToolbox;

namespace ZaberConsoleGUITests.PluginTests.Terminal
{
	[TestFixture]
	[SetCulture("en-US")]
	public class TerminalPluginVMTests
	{
		[SetUp]
		public void Setup()
		{
			DispatcherStack.Push(new ImmediateDispatcher());

			_vm = new TerminalPluginVM();
		}


		[TearDown]
		public void Teardown()
		{
			_listener?.Unlisten(_vm);
			DispatcherStack.Pop();
		}


		[Test]
		public void TestControlsProperty()
		{
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
			_listener.TestObservableProperty(_vm,
											 () => _vm.Controls,
											 aOldList =>
											 {
												 if (null == aOldList)
												 {
													 return new ObservableCollection<ObservableObject>();
												 }

												 var result = new ObservableCollection<ObservableObject>();
												 if (aOldList.Count != 1)
												 {
													 result.Add(new AdvancedAsciiCommandVM());
												 }

												 return result;
											 });
		}


		[Test]
		public void TestIsPlugin()
		{
			var plugInAttributes = _vm.GetType().GetCustomAttributes(typeof(PlugInAttribute), true);
			Assert.AreEqual(1, plugInAttributes.Length);
			var attr = plugInAttributes[0] as PlugInAttribute;
			Assert.IsNotNull(attr);
		}


		[Test]
		public void TestPopulateAscii()
		{
			var port = CreatePort(true);
			_vm.PortFacade = port;
			Assert.AreEqual(port, _vm.PortFacade);
			Assert.AreEqual(0, _vm.Controls.Count);

			port.Open("COM1");
			Assert.AreEqual(1, _vm.Controls.Count);
			Assert.IsTrue(_vm.Controls[0] is AdvancedAsciiCommandVM);
			var control = _vm.Controls[0] as AdvancedAsciiCommandVM;
			Assert.AreEqual(port, control.PortFacade);
		}


		[Test]
		public void TestPopulateBinary()
		{
			var port = CreatePort(false);
			_vm.PortFacade = port;
			Assert.AreEqual(port, _vm.PortFacade);
			Assert.AreEqual(0, _vm.Controls.Count);

			port.Open("COM1");
			Assert.AreEqual(2, _vm.Controls.Count);

			var cmdVm = _vm.Controls.OfType<AdvancedBinaryCommandVM>().FirstOrDefault();
			var regVm = _vm.Controls.OfType<AdvancedBinaryRegisterVM>().FirstOrDefault();

			Assert.IsNotNull(cmdVm);
			Assert.IsNotNull(regVm);

			Assert.AreEqual(port, cmdVm.PortFacade);
			Assert.AreEqual(port, regVm.PortFacade);
		}


		private ZaberPortFacade CreatePort(bool aAsciiMode)
		{
			var mockPort = new MockPort();
			mockPort.IsAsciiMode = aAsciiMode;

			var defaultDeviceType = new DeviceType { Commands = new List<CommandInfo>() };

			var portFacade = new ZaberPortFacade
			{
				DefaultDeviceType = defaultDeviceType,
				Port = mockPort,
				QueryTimeout = 100
			};

			var XMCB2 = new DeviceType
			{
				DeviceId = 30222,
				PeripheralMap = new Dictionary<int, DeviceType>(),
				Name = "X-MCB2 Controller",
				MotionType = MotionType.Other
			};

			portFacade.AddDeviceType(XMCB2);

			if (aAsciiMode)
			{
				ZaberPortFacadeTest.SetAsciiExpectations(portFacade, mockPort, XMCB2);
			}
			else
			{
				ZaberPortFacadeTest.SetBinaryExpectations(portFacade, mockPort);
			}

			return portFacade;
		}


		private TerminalPluginVM _vm;
		private ObservableObjectTester _listener;
	}
}

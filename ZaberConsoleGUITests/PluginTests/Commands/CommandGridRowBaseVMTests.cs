﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Zaber.Testing;
using ZaberConsole.Plugins.CommandList;

namespace ZaberConsoleTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class CommandGridRowBaseVMTests
	{
		[SetUp]
		public void Setup()
		{
			_vm = new CommandGridRowBaseVM();
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
		}


		[TearDown]
		public void Teardown()
		{
			_listener.Unlisten(_vm);
			_vm = null;
		}


		[Test]
		public void TestDataProperty()
		{
			_listener.TestObservableProperty(_vm, () => _vm.Data, _ => new CommandGridRowBaseVM());
			Assert.AreEqual(0, _listener.GetNumNotifications(), "Unexpected side effect notifications.");
		}


		[Test]
		public void TestHelpUrlProperty()
		{
			Assert.IsTrue(string.IsNullOrEmpty(_vm.HelpUrl));
			_vm.HelpUrl = "Help!";
			Assert.AreEqual("Help!", _vm.HelpUrl);
			_vm.HelpUrl = null;
			Assert.IsNull(_vm.HelpUrl);
		}


		[Test]
		public void TestNameProperty()
		{
			_listener.TestObservableProperty(_vm, () => _vm.Name);
			Assert.AreEqual(0, _listener.GetNumNotifications(), "Unexpected side effect notifications.");
		}


		[Test]
		public void TestSelectedUnitProperty()
		{
			_listener.TestObservableProperty(_vm, () => _vm.SelectedUnit, unit => null == unit ? 1 : (int) unit + 1);
			Assert.AreEqual(0, _listener.GetNumNotifications(), "Unexpected side effect notifications.");
		}


		[Test]
		public void TestToolTipProperty()
		{
			_listener.TestObservableProperty(_vm, () => _vm.ToolTip);
			Assert.AreEqual(0, _listener.GetNumNotifications(), "Unexpected side effect notifications.");
		}


		[Test]
		public void TestUnitsProperty()
		{
			_listener.TestObservableProperty(_vm,
											 () => _vm.Units,
											 oldCollection =>
											 {
												 var result = new ObservableCollection<object>();

												 if ((null == oldCollection) || (0 == oldCollection.Count))
												 {
													 result.Add(1);
												 }

												 return result;
											 });

			Assert.AreEqual(0, _listener.GetNumNotifications(), "Unexpected side effect notifications.");
		}


		[Test]
		public void TestVisibleProperty()
		{
			_listener.TestObservableProperty(_vm, () => _vm.Visible);
			Assert.AreEqual(0, _listener.GetNumNotifications(), "Unexpected side effect notifications.");
		}


		private CommandGridRowBaseVM _vm;
		private ObservableObjectTester _listener;
	}
}

﻿using System.Collections.Generic;
using NUnit.Framework;
using Zaber;
using ZaberConsole.Plugins.CommandList;

namespace ZaberConsoleGUITests.PluginTests.CommandListTests
{
	[TestFixture]
	[Category("active")]
	[SetCulture("en-US")]
	internal class ASCIICommandNodeVMTests
	{
		[SetUp]
		public void Setup()
		{
			_node = new ASCIICommandNode
			{
				AccessLevel = 3,
				IsBasic = false,
				NodeText = "foo"
			};

			_paramnode = new ASCIICommandNode
			{
				AccessLevel = 4,
				IsBasic = true,
				NodeText = "bar",
				ParamType = new ParameterType(new List<string>(new[] { "a", "b", "c" }))
			};

			_nodeVM = new ASCIICommandNodeVM(_node);
		}


		[TearDown]
		public void TearDown()
		{
			_nodeVM = null;
			_node = null;
		}


		[Test]
		public void ContentAndNodeTextMatch() => Assert.AreEqual(_node.NodeText, _nodeVM.Content);


		[Test]
		public void TestClone()
		{
			//SETUP
			ASCIICommandNodeVM _clone;

			//EXEC
			_clone = _nodeVM.Clone();

			//VERIFY
			Assert.AreSame(_nodeVM.NodeInfo,
						   _clone.NodeInfo,
						   "Node and clone do not refer to the same ASCIICommandNode object");

			Assert.AreEqual(_nodeVM.Content, _clone.Content, "Node and clone contents are not equal");
			Assert.AreNotSame(_nodeVM.Content, _clone.Content, "Node and clone contents refer to the same object");

			//Assert.AreNotSame(_nodeVM)
		}


		private ASCIICommandNode _node;
		private ASCIICommandNode _paramnode;
		private ASCIICommandNodeVM _nodeVM;
	}
}

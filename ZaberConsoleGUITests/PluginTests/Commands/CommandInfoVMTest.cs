﻿using System.Collections.Generic;
using NUnit.Framework;
using TickingTest;
using Zaber;
using Zaber.Testing;
using ZaberConsole.Plugins.CommandList;
using ZaberConsole.Plugins.SettingsList;
using ZaberTest;
using ZaberTest.Testing;

namespace ZaberConsoleTest
{
	[TestFixture]
	[Category("active")]
	[SetCulture("en-US")]
	[SetUICulture("en-US")]
	public class CommandInfoVMTest
	{
		[SetUp]
		public void SetUp()
		{
			_port = new MockPort();
			_commandVM = CreateCommandView(_port);
			_settingVM = CreateSettingView(_port);

			DispatcherStack.Push(new ImmediateDispatcher());
			BackgroundWorkerStack.Push(typeof(ImmediateForegroundWorker));
		}


		[TearDown]
		public void Teardown()
		{
			BackgroundWorkerStack.Pop();
			DispatcherStack.Pop();
		}


		private static CommandInfoVM CreateCommandView(IZaberPort port, TimeoutTimer aTimer = null)
		{
			var device = new ZaberDevice
			{
				DeviceNumber = 3,
				DeviceType = new DeviceType(),
				Port = port
			};

			var commands = new List<CommandInfo>
			{
				new SettingInfo
				{
					Command = Command.SetTargetSpeed,
					Name = "Set Target Speed",
					DataDescription = "Speed",
					ResponseDescription = "Speed",
					RequestUnit = UnitOfMeasure.MillimetersPerSecond,
					RequestUnitFunction = ScalingFunction.LinearResolution,
					RequestUnitScale = 15.625m,
					ResponseUnit = UnitOfMeasure.MillimetersPerSecond,
					ResponseUnitFunction = ScalingFunction.LinearResolution,
					ResponseUnitScale = 15.625m
				}
			};
			device.DeviceType.Commands = commands;

			var timer = aTimer;
			if (null == timer)
			{
				timer = new TimeoutTimer { Timeout = 10000 };
			}

			var view = new CommandInfoVM(commands[0], new Conversation(device), timer);

			return view;
		}


		private static SettingInfoVM CreateSettingView(IZaberPort port, TimeoutTimer aTimer = null)
		{
			var device = new ZaberDevice
			{
				DeviceNumber = 3,
				DeviceType = new DeviceType(),
				Port = port
			};

			var commands = new List<CommandInfo>
			{
				new SettingInfo
				{
					Command = Command.SetTargetSpeed,
					Name = "Set Target Speed",
					DataDescription = "Speed",
					ResponseDescription = "Speed",
					RequestUnit = UnitOfMeasure.MillimetersPerSecond,
					RequestUnitFunction = ScalingFunction.LinearResolution,
					RequestUnitScale = 15.625m,
					ResponseUnit = UnitOfMeasure.MillimetersPerSecond,
					ResponseUnitFunction = ScalingFunction.LinearResolution,
					ResponseUnitScale = 15.625m,
					ParamType = AsciiCommandBuilder.StandardParamTypes["Int"]
				}
			};
			device.DeviceType.Commands = commands;

			var timer = aTimer;
			if (null == timer)
			{
				timer = new TimeoutTimer { Timeout = 10000 };
			}

			var view = new SettingInfoVM(commands[0] as SettingInfo, new Conversation(device), timer);

			return view;
		}


		[Test]
		public void RefreshWithMessageId()
		{
			// SETUP
			_settingVM.Conversation.Device.AreMessageIdsEnabled = true;

			// EXPECT
			_port.Expect(3, Command.ReturnSetting, (int) Command.SetTargetSpeed | (1 << 24));
			_port.AddResponse(3, Command.SetTargetSpeed, 770 | (1 << 24));

			// EXEC
			_settingVM.Refresh();
			var dataVM = _settingVM.Data as SettingValueVM;
			var settingValue = dataVM.Value;

			// VERIFY
			Assert.AreEqual("770", settingValue, "Value should match");
			_port.Verify();
		}


		[Test]
		public void RefreshReadOnly()
		{
			// SETUP
			CreateViewForReadOnlySetting(ref _settingVM);

			// EXPECT
			_port.Expect(3, Command.ReturnPowerSupplyVoltage, 0);
			_port.AddResponse(3, Command.ReturnPowerSupplyVoltage, 770);

			// EXEC
			_settingVM.Refresh();
			var dataVM = _settingVM.Data as SettingValueVM;
			var settingValue = dataVM.Value;

			// VERIFY
			Assert.AreEqual("770", settingValue, "Value should match");
			_port.Verify();
		}


		[Test]
		public void RefreshConversationCollectionMatchingValues()
		{
			// SETUP
			var view = new SettingInfoVM(_settingVM.SettingInfo,
										 CreateConversationCollection(_settingVM),
										 new TimeoutTimer { Timeout = 1000 });

			// EXPECT
			_port.Expect(0, Command.ReturnSetting, (int) Command.SetTargetSpeed);
			_port.AddResponse(1, Command.SetTargetSpeed, 770);
			_port.AddResponse(2, Command.SetTargetSpeed, 770);

			// EXEC
			view.Refresh();
			var dataVM = view.Data as SettingValueVM;
			var settingValue = dataVM.Value;

			// VERIFY
			Assert.AreEqual("770\r\n", settingValue, "Value should match");
			_port.Verify();
		}


		[Test]
		public void RefreshConversationCollectionDifferentValues()
		{
			// SETUP
			var view = new SettingInfoVM(_settingVM.SettingInfo,
										 CreateConversationCollection(_settingVM),
										 new TimeoutTimer { Timeout = 1000 });

			// EXPECT
			_port.Expect(0, Command.ReturnSetting, (int) Command.SetTargetSpeed);
			_port.AddResponse(1, Command.SetTargetSpeed, 770);
			_port.AddResponse(2, Command.SetTargetSpeed, 880);

			// EXEC
			var dataVM = view.Data as SettingValueVM;
			var valueBeforeRefresh = dataVM.Value;
			view.Refresh();
			var settingValue = dataVM.Value;

			// VERIFY
			Assert.AreEqual("\r\n", valueBeforeRefresh, "data value before refresh");
			Assert.AreEqual("1: 770\r\n2: 880", settingValue, "data value after refresh");
			_port.Verify();
		}


		[Test]
		public void RefreshConversationCollectionErrorResponse()
		{
			// SETUP
			var view = new SettingInfoVM(_settingVM.SettingInfo,
										 CreateConversationCollection(_settingVM),
										 new TimeoutTimer { Timeout = 1000 });

			// EXPECT
			_port.Expect(0, Command.ReturnSetting, (int) Command.SetTargetSpeed);
			_port.AddResponse(1, Command.SetTargetSpeed, 770);
			_port.AddResponse(2, Command.Error, (int) ZaberError.Busy);

			// EXEC
			view.Refresh();
			var dataVM = view.Data as SettingValueVM;
			var settingValue = dataVM.Value;
			var errorText = view.ToolTip;

			// VERIFY
			Assert.AreEqual("1: 770\r\n2: ", settingValue, "data value");
			Assert.AreEqual(
				"Some requests failed:\r\n2: Error occurred: Busy. Another command is executing and cannot be pre-empted. Either stop the previous command or wait until it finishes before trying again.",
				errorText,
				"error text");
			_port.Verify();
		}


		[Test]
		public void RefreshConversationCollectionInvalidSetting()
		{
			// SETUP
			var view = new SettingInfoVM(_settingVM.SettingInfo,
										 CreateConversationCollection(_settingVM),
										 new TimeoutTimer { Timeout = 1000 });

			// EXPECT
			_port.Expect(0, Command.ReturnSetting, (int) Command.SetTargetSpeed);
			_port.AddResponse(1, Command.SetTargetSpeed, 770);
			_port.AddResponse(2, Command.Error, (int) ZaberError.SettingInvalid);

			// EXEC
			view.Refresh();
			var dataVM = view.Data as SettingValueVM;
			var settingValue = dataVM.Value;
			var errorText = view.ToolTip;

			// VERIFY
			Assert.AreEqual("1: 770\r\n2: n/a", settingValue, "data value");
			Assert.AreEqual("", errorText, "error text");
			_port.Verify();
		}


		[Test]
		public void RefreshConversationCollectionInvalidSettingReadOnly()
		{
			// SETUP
			CreateViewForReadOnlySetting(ref _settingVM);
			var view = new SettingInfoVM(_settingVM.SettingInfo,
										 CreateConversationCollection(_settingVM),
										 new TimeoutTimer { Timeout = 1000 });

			// EXPECT
			_port.Expect(0, Command.ReturnPowerSupplyVoltage, 0);
			_port.AddResponse(2, Command.ReturnPowerSupplyVoltage, 140);
			_port.AddResponse(1, Command.Error, (int) ZaberError.SettingInvalid);

			// EXEC
			view.Refresh();
			var dataVM = view.Data as SettingValueVM;
			var settingValue = dataVM.Value;
			var errorText = view.ToolTip;

			// VERIFY
			Assert.AreEqual("1: n/a\r\n2: 140", settingValue, "data value");
			Assert.AreEqual("", errorText, "error text");
			_port.Verify();
		}


		private static ConversationCollection CreateConversationCollection(MeasurementVM view)
		{
			var port = view.Conversation.Device.Port;
			var deviceType = view.Conversation.Device.DeviceType;

			var devices = new DeviceCollection
			{
				DeviceNumber = 0,
				DeviceType = deviceType,
				Port = port
			};

			var device1 = new ZaberDevice
			{
				DeviceNumber = 1,
				DeviceType = deviceType,
				Port = port
			};

			var device2 = new ZaberDevice
			{
				DeviceNumber = 2,
				DeviceType = deviceType,
				Port = port
			};

			devices.Add(device1);
			devices.Add(device2);

			var conversations = new ConversationCollection(devices);
			var conversation1 = new Conversation(device1);
			conversations.Add(conversation1);
			var conversation2 = new Conversation(device2);
			conversations.Add(conversation2);
			return conversations;
		}


		[Test]
		public void RefreshWithErrorResponse()
		{
			// SETUP
			CommandInfo settingInfo = new ReadOnlySettingInfo
			{
				Command = Command.ReturnPowerSupplyVoltage,
				Name = "Return Power Supply Voltage"
			};

			var conversation = _commandVM.Conversation;

			_commandVM = new CommandInfoVM(settingInfo, conversation);

			// EXPECT
			_port.Expect(3, Command.ReturnPowerSupplyVoltage, 0);
			_port.AddResponse(3, Command.Error, (int) ZaberError.Busy);

			// EXEC
			_commandVM.Request(string.Empty);
			var dataVM = _commandVM.Data as CommandArgumentsVM;
			var settingValue = dataVM.Value;
			var errorText = _commandVM.ToolTip;

			// VERIFY
			Assert.AreEqual("",
							settingValue,
							"Value should match");
			Assert.AreEqual(
				"Error occurred: Busy. Another command is executing and cannot be pre-empted. Either stop the previous command or wait until it finishes before trying again.",
				errorText,
				"error text");
			_port.Verify();
		}


		[Test]
		public void RefreshWithInvalidSetting()
		{
			// SETUP
			SettingInfo settingInfo = new ReadOnlySettingInfo
			{
				Command = Command.ReturnPowerSupplyVoltage,
				Name = "Return Power Supply Voltage"
			};

			var conversation = _settingVM.Conversation;

			_settingVM = new SettingInfoVM(settingInfo, conversation);

			// EXPECT
			_port.Expect(3, Command.ReturnPowerSupplyVoltage, 0);
			_port.AddResponse(3, Command.Error, (int) ZaberError.SettingInvalid);

			// EXEC
			var dataVM = _settingVM.Data as SettingValueVM;
			dataVM.ReadCommand.Execute(null);
			var settingValue = dataVM.Value;
			var errorText = _commandVM.ToolTip;

			// VERIFY
			Assert.AreEqual("n/a",
							settingValue,
							"Value should match");
			Assert.IsTrue(string.IsNullOrEmpty(errorText), "error text");
			_port.Verify();
		}


		[Test]
		public void Update() => TestFramework.RunOnce(new UpdateThreads());


		private class UpdateThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Request()
			{
				_port = new MockPort();
				var view = CreateSettingView(_port);

				// EXPECT
				_port.Expect(3, Command.SetTargetSpeed, RequestData);

				// EXEC
				var dataVM = view.Data as SettingValueVM;
				var buttonBeforeUpdate = dataVM.WriteButtonLabel;
				var topic = view.Request(RequestText);

				AssertTick(1);

				// VERIFY
				Assert.IsNull(topic, "topic should be null");
				Assert.AreEqual(ResponseText, dataVM.Value, "Final value should match");
				Assert.AreEqual("Write", buttonBeforeUpdate, "button name before update should match");
				Assert.AreEqual("Write", dataVM.WriteButtonLabel, "button name after update should match");
				Assert.AreEqual("", view.ToolTip, "Error text");
				_port.Verify();
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				_port.FireResponse(3, Command.SetTargetSpeed, ResponseData);
			}

			#endregion

			#region -- Data --

			private const int RequestData = 900;

			private const string RequestText = "900";

			// slightly adjusted value in response
			private const int ResponseData = 901;
			private const string ResponseText = "901";
			private MockPort _port;

			#endregion
		}


		[Test]
		public void UpdateWithTimeout() => TestFramework.RunOnce(new UpdateWithTimeoutThreads());


		private class UpdateWithTimeoutThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Request()
			{
				_port = new MockPort();
				_mockTimer = new MockTimeoutTimer();
				var view = CreateSettingView(_port, _mockTimer);

				// EXPECT
				_port.Expect(3, Command.SetTargetSpeed, RequestData);

				// EXEC
				var dataVM = view.Data as SettingValueVM;
				var buttonBeforeUpdate = dataVM.WriteButtonLabel;
				var topic = view.Request(RequestText);

				AssertTick(1);

				var intermediateButton = dataVM.WriteButtonLabel;
				var intermediateSettingValue = dataVM.Value;
				var errorText = view.ToolTip;

				view.WaitForResponse(topic);

				AssertTick(2);

				var finalSettingValue = dataVM.Value;
				var finalButton = dataVM.WriteButtonLabel;

				// VERIFY
				Assert.IsNotNull(topic,
								 "topic should not be null");
				Assert.AreEqual("",
								intermediateSettingValue,
								"Intermediate value should match (not updated)");
				Assert.AreEqual(ResponseText,
								finalSettingValue,
								"Final value should match");
				Assert.AreEqual("Write",
								buttonBeforeUpdate,
								"Button name before update");
				Assert.AreEqual("Writing",
								intermediateButton,
								"Intermediate button name");
				Assert.AreEqual("Write",
								finalButton,
								"Final button name");
				Assert.IsTrue(string.IsNullOrEmpty(errorText), "Error text");
				_port.Verify();
			}


			[TestThread]
			public void Response()
			{
				WaitForTick(1);

				_mockTimer.TimeoutNow();

				WaitForTick(2);

				_port.FireResponse(3, Command.SetTargetSpeed, ResponseData);
			}

			#endregion

			#region -- Data --

			private const int RequestData = 900;

			private const string RequestText = "900";

			// slightly adjusted value in response
			private const int ResponseData = 901;
			private const string ResponseText = "901";
			private MockPort _port;
			private MockTimeoutTimer _mockTimer;

			#endregion
		}


		[Test]
		public void UpdateWithMessageId()
		{
			// SETUP
			_settingVM.Conversation.Device.AreMessageIdsEnabled = true;

			// EXPECT
			_port.Expect(3, Command.SetTargetSpeed, 900 | (1 << 24));
			_port.AddResponse(3, Command.SetTargetSpeed, 901 | (1 << 24));

			// EXEC
			var topic = _settingVM.Request("900");

			var dataVM = _settingVM.Data as SettingValueVM;
			var finalSettingValue = dataVM.Value;

			// VERIFY
			Assert.AreEqual("901", finalSettingValue, "Final value should match");
			_port.Verify();
		}


		[Test]
		public void SendCommand()
		{
			// SETUP
			CreateViewForBinaryCommand(ref _commandVM);

			// EXPECT
			_port.Expect(3, Command.MoveRelative, 500);
			_port.AddResponse(3, Command.MoveRelative, 1500);

			// EXEC
			var topic = _commandVM.Request("500");

			var dataVM = _commandVM.Data as CommandArgumentsVM;
			var finalSettingValue = dataVM.Value;

			// VERIFY
			Assert.AreEqual("",
							finalSettingValue,
							"Final value should match (not updated for command)");
			_port.Verify();
		}


		[Test]
		public void SendTextCommand()
		{
			// SETUP
			_port.IsAsciiMode = true;
			CreateViewForAsciiCommand(ref _commandVM);

			// EXPECT
			_port.Expect("03 0 move rel 500");
			_port.AddResponse("@3 0 OK BUSY -- 0");

			// EXEC
			_commandVM.Request("500");

			var dataVM = _commandVM.Data as CommandArgumentsVM;
			var finalSettingValue = dataVM.Value;

			// VERIFY
			Assert.AreEqual("",
							finalSettingValue,
							"Final value should match (not updated for command)");
			_port.Verify();
		}


		[Test]
		public void SendTextCommandWithoutData()
		{
			// SETUP
			_port.IsAsciiMode = true;
			CreateViewForAsciiCommand(ref _commandVM);
			_commandVM.CommandInfo.TextCommand = "home";

			// EXPECT
			_port.Expect("03 0 home");
			_port.AddResponse("@3 0 OK BUSY -- 0");

			// EXEC
			_commandVM.Request("");

			// VERIFY
			_port.Verify();
		}


		[Test]
		public void SendTextCommandWithTextData()
		{
			// SETUP
			_port.IsAsciiMode = true;
			CreateViewForAsciiCommand(ref _commandVM);
			_commandVM.CommandInfo.TextCommand = "tools echo";

			// EXPECT
			_port.Expect("03 0 tools echo x y z");
			_port.AddResponse("@3 0 OK IDLE -- 0");

			// EXEC
			_commandVM.Request("x y z");
			var errorText = _commandVM.ToolTip;

			// VERIFY
			Assert.That(errorText,
						Is.Empty,
						"error text");
			_port.Verify();
		}


		[Test]
		public void SendTextCommandWithUnits()
		{
			// SETUP
			_port.IsAsciiMode = true;
			CreateViewForAsciiCommand(ref _commandVM);

			_commandVM.UnitOfMeasure = UnitOfMeasure.Millimeter;

			// EXPECT
			_port.Expect("03 0 move rel 5001");
			_port.AddResponse("@3 0 OK BUSY -- 0");

			// EXEC
			_commandVM.Request("5.0007");
			var errorText = _commandVM.ToolTip;
			var dataVM = _commandVM.Data as CommandArgumentsVM;
			var finalSettingValue = dataVM.Value;

			// VERIFY
			Assert.That(errorText,
						Is.Empty,
						"error text");
			Assert.AreEqual("",
							finalSettingValue,
							"Final value should match (not updated for command)");
			_port.Verify();
		}


		[Test]
		public void SendCommandToCollectionWithErrorResponse()
		{
			// SETUP
			CreateViewForBinaryCommand(ref _commandVM);
			_commandVM = new CommandInfoVM(_commandVM.CommandInfo,
										   CreateConversationCollection(_commandVM));

			// EXPECT
			_port.Expect(0, Command.MoveRelative, 500);
			_port.AddResponse(1, Command.MoveRelative, 1500);
			_port.AddResponse(2, Command.Error, (int) ZaberError.RelativePositionInvalid);

			// EXEC
			var topic = _commandVM.Request("500");

			var dataVM = _commandVM.Data as CommandArgumentsVM;
			var finalSettingValue = dataVM.Value;
			var errorText = _commandVM.ToolTip;

			// VERIFY
			Assert.AreEqual("",
							finalSettingValue,
							"Final value should match (not updated for command)");
			Assert.AreEqual(
				"Some requests failed:\r\n2: Error occurred: RelativePositionInvalid. Move Relative - Target position out of range.",
				errorText,
				"error text");
			_port.Verify();
		}


		/// <summary>
		///     When the conversation collection is empty, it should
		///     just always treat the status as unknown.
		/// </summary>
		[Test]
		public void SendCommandToEmptyCollection() => TestFramework.RunOnce(new SendCommandToEmptyCollectionThreads());


		private class SendCommandToEmptyCollectionThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Request()
			{
				// SETUP
				_mockTimer = new MockTimeoutTimer();
				_port = new MockPort();
				var view = CreateCommandView(_port);

				CreateViewForBinaryCommand(ref view);
				var conversations = CreateConversationCollection(view);
				conversations.Clear();
				view = new CommandInfoVM(view.CommandInfo,
										 conversations,
										 _mockTimer);

				// EXPECT
				_port.Expect(0, Command.MoveRelative, 500);

				// EXEC
				var dataVM = view.Data as CommandArgumentsVM;
				var buttonBeforeUpdate = dataVM.ButtonLabel;
				var topic = view.Request("500");

				var buttonAfterResponse = dataVM.ButtonLabel;
				var finalSettingValue = dataVM.Value;
				var errorText = view.ToolTip;

				// VERIFY
				Assert.AreEqual("",
								finalSettingValue,
								"Final value should match (not updated for command)");
				Assert.AreEqual("",
								errorText,
								"error text");
				Assert.AreEqual("Send",
								buttonBeforeUpdate,
								"button name before update should match");
				Assert.AreEqual("Send",
								buttonAfterResponse,
								"button name after response should match");
				_port.Verify();
			}


			[TestThread]
			public void Timer()
			{
				WaitForTick(1);
				_mockTimer.TimeoutNow();
			}

			#endregion

			#region -- Data --

			private MockPort _port;
			private MockTimeoutTimer _mockTimer;

			#endregion
		}


		#if false // TODO fix test
		[Test]
        public void SendCommandWithTimeout()
        {
            TestFramework.RunOnce(new SendCommandWithTimeoutThreads());
        }

        private class SendCommandWithTimeoutThreads : MultithreadedTestCase
        {
            private MockPort _port;
            private MockTimeoutTimer _mockTimer;

            [TestThread]
            public void Request()
            {
                // SETUP
                _mockTimer = new MockTimeoutTimer();
                _port = new MockPort();
                var view = CreateCommandView(_port, _mockTimer);

                CreateViewForBinaryCommand(ref view);

                // EXPECT
                _port.Expect(3, Command.MoveRelative, 500);

				// EXEC
				var dataVM = view.Data as CommandArgumentsVM;
                var buttonBeforeUpdate = dataVM.ButtonLabel;
                var topic = view.Request("500");

                AssertTick(1);

                var errorText = view.ToolTip;
                var intermediateButton = dataVM.ButtonLabel;
                var intermediateSettingValue = dataVM.Value;

                view.WaitForResponse(topic);

                AssertTick(2);

                var finalButton = dataVM.ButtonLabel;
                var finalSettingValue = dataVM.Value;

                // VERIFY
                Assert.AreEqual("", finalSettingValue, "Final value should match (not updated for command)");
                Assert.AreEqual("", errorText, "error text");
                Assert.AreEqual("Send", buttonBeforeUpdate, "button name before update should match");
                Assert.AreEqual("Sending", intermediateButton, "button name after timeout should match");
                Assert.AreEqual("Send", finalButton, "button name after response should match");
                _port.Verify();
            }

            [TestThread]
            public void Timer()
            {
                WaitForTick(1);
                _mockTimer.TimeoutNow();
                WaitForTick(2);
                _port.FireResponse(3, Command.MoveRelative, 1500);
            }
        }
#endif


		[Test]
		public void SendCommandToCollectionWithInvalidCommand()
		{
			// SETUP
			CreateViewForBinaryCommand(ref _commandVM);
			_commandVM = new CommandInfoVM(_commandVM.CommandInfo,
										   CreateConversationCollection(_commandVM));

			// EXPECT
			_port.Expect(0, Command.MoveRelative, 500);
			_port.AddResponse(1, Command.MoveRelative, 1500);
			_port.AddResponse(2, Command.Error, (int) ZaberError.CommandInvalid);

			// EXEC
			var topic = _commandVM.Request("500");

			var dataVM = _commandVM.Data as CommandArgumentsVM;
			var finalSettingValue = dataVM.Value;
			var errorText = _commandVM.ToolTip;

			// VERIFY
			Assert.AreEqual("",
							finalSettingValue,
							"Final value should match (not updated for command)");
			Assert.AreEqual("",
							errorText,
							"error text");
			_port.Verify();
		}


		[Test]
		public void SendCommandWithReplacedRequest()
		{
			// SETUP
			CreateViewForBinaryCommand(ref _commandVM);
			_commandVM.Conversation.Device.AreMessageIdsEnabled = true;

			// EXPECT
			_port.Expect(3, Command.MoveRelative, 500 | (1 << 24));
			_port.AddResponse(3, Command.Stop, 1500); // response to different request.

			// EXEC
			var topic = _commandVM.Request("500");

			var dataVM = _commandVM.Data as CommandArgumentsVM;
			var finalSettingValue = dataVM.Value;
			var errorText = _commandVM.ToolTip;

			// VERIFY
			Assert.AreEqual("",
							finalSettingValue,
							"Final value should match (not updated for command)");
			Assert.AreEqual("Request was replaced by Stop command.",
							errorText,
							"Error text");
			_port.Verify();
		}


		[Test]
		public void SendCommandWithPortError()
		{
			// SETUP
			CreateViewForBinaryCommand(ref _commandVM);

			// EXPECT
			_port.Expect(3, Command.MoveRelative, 500);
			_port.AddPortError(ZaberPortError.PacketTimeout);

			// EXEC
			var topic = _commandVM.Request("500");

			var dataVM = _commandVM.Data as CommandArgumentsVM;
			var finalSettingValue = dataVM.Value;
			var errorText = _commandVM.ToolTip;

			// VERIFY
			Assert.AreEqual("",
							finalSettingValue,
							"Final value should match (not updated for command)");
			Assert.AreEqual("PacketTimeout error occurred.",
							errorText,
							"Error text");
			_port.Verify();
		}


		[Test]
		public void SendCommandWithNonNumericValue()
		{
			// SETUP
			CreateViewForBinaryCommand(ref _commandVM);

			// EXEC
			var dataVM = _commandVM.Data as CommandArgumentsVM;
			var buttonBeforeUpdate = dataVM.ButtonLabel;
			var topic = _commandVM.Request("XYZ"); // Not a number

			var buttonAfterResponse = dataVM.ButtonLabel;
			var finalSettingValue = dataVM.Value;
			var errorText = _commandVM.ToolTip;

			// VERIFY
			Assert.AreEqual("",
							finalSettingValue,
							"Final value should match (not updated after error)");
			Assert.AreEqual("Send",
							buttonBeforeUpdate,
							"button before update");
			Assert.AreEqual("Send",
							buttonAfterResponse,
							"Button after response");
			Assert.AreEqual("Invalid number.",
							errorText,
							"Error text");
			_port.Verify();
		}


		[Test]
		public void RequestReadOnlySetting()
		{
			// SETUP
			CreateViewForReadOnlySetting(ref _settingVM);

			// EXEC
			var dataVM = _settingVM.Data as SettingValueVM;
			var topic = _settingVM.Request("123");

			var finalSettingValue = dataVM.Value;
			var errorText = _settingVM.ToolTip;

			// VERIFY
			Assert.IsFalse(dataVM.IsWritable);
			Assert.AreEqual("",
							finalSettingValue,
							"Final value should match (not updated after error)");
			Assert.AreEqual("This setting is read-only.",
							errorText,
							"Error text");
			Assert.AreEqual(null,
							topic,
							"topic");
			_port.Verify();
		}


		[Test]
		public void RequestBlankDataForCommandThatUsesData()
		{
			// SETUP
			CreateViewForBinaryCommand(ref _commandVM);

			// EXEC
			var dataVM = _commandVM.Data as CommandArgumentsVM;
			var buttonBeforeRequest = dataVM.ButtonLabel;
			var topic = _commandVM.Request("");

			var buttonAfterResponse = dataVM.ButtonLabel;
			var finalSettingValue = dataVM.Value;
			var errorText = _commandVM.ToolTip;

			// VERIFY
			Assert.AreEqual("",
							finalSettingValue,
							"Final value should match (not updated after error)");
			Assert.AreEqual("Send",
							buttonBeforeRequest,
							"button name before update should match");
			Assert.AreEqual("Send",
							buttonAfterResponse,
							"button name after response should match");
			Assert.AreEqual("This command requires a data value.",
							errorText,
							"Error text");
			Assert.AreEqual(null,
							topic,
							"topic");
			_port.Verify();
		}


		[Test]
		public void RequestBlankDataForCommandThatIgnoresData()
		{
			// SETUP
			CreateViewForBinaryCommand(ref _commandVM);
			_commandVM.CommandInfo.Command = Command.Home;
			_commandVM.CommandInfo.Name = "Home";
			_commandVM.CommandInfo.DataDescription = null;

			// EXPECT
			_port.Expect(3, Command.Home, 0);
			_port.AddResponse(3, Command.Home, 0);

			// EXEC
			var dataVM = _commandVM.Data as CommandArgumentsVM;
			var buttonBeforeRequest = dataVM.ButtonLabel;
			var topic = _commandVM.Request("");

			var buttonAfterResponse = dataVM.ButtonLabel;
			var finalSettingValue = dataVM.Value;
			var errorText = _commandVM.ToolTip;

			// VERIFY
			Assert.AreEqual("",
							finalSettingValue,
							"Final value");
			Assert.AreEqual("Send",
							buttonBeforeRequest,
							"button name before update should match");
			Assert.AreEqual("Send",
							buttonAfterResponse,
							"button name after response should match");
			Assert.AreEqual("",
							errorText,
							"Error text");
			Assert.AreEqual(null,
							topic,
							"topic");
			_port.Verify();
		}


		[Test]
		public void UpdateWithErrorResponse()
		{
			// EXPECT
			_port.Expect(3, Command.SetTargetSpeed, 900);
			_port.AddResponse(3, Command.Error, (int) ZaberError.SpeedInvalid);

			// EXEC
			var dataVM = _settingVM.Data as SettingValueVM;
			var buttonBeforeUpdate = dataVM.WriteButtonLabel;
			var topic = _settingVM.Request("900");

			var buttonAfterResponse = dataVM.WriteButtonLabel;
			var finalSettingValue = dataVM.Value;
			var errorText = _settingVM.ToolTip;

			_settingVM.Clear();
			var errorTextAfterClear = _settingVM.ToolTip;

			// VERIFY
			Assert.AreEqual("",
							finalSettingValue,
							"Final value should match (not updated)");
			Assert.AreEqual("Write",
							buttonBeforeUpdate,
							"button name before update should match");
			Assert.AreEqual("Write",
							buttonAfterResponse,
							"button name after response should match");
			Assert.AreEqual(
				"Error occurred: SpeedInvalid. Target speed out of range. The range of target speed is determined by the resolution.",
				errorText,
				"error text");
			Assert.IsTrue(string.IsNullOrEmpty(errorTextAfterClear), "error text after clear");
			Assert.AreEqual(null,
							topic,
							"topic");
			_port.Verify();
		}


		[Test]
		public void UpdateAsciiSetting()
		{
			// SETUP
			CreateViewForAsciiSetting(ref _settingVM);
			_port.IsAsciiMode = true;
			var dataVM = _settingVM.Data as SettingValueVM;
			dataVM.Value = "900";


			// EXPECT
			_port.Expect("03 0 set pos 900");
			_port.AddResponse("@03 0 OK IDLE -- 0");

			// EXEC
			var topic = _settingVM.Request("900");
			var finalSettingValue = dataVM.Value;

			// VERIFY
			Assert.AreEqual(null,
							topic,
							"topic");
			Assert.AreEqual("900",
							finalSettingValue,
							"Final value should match (not updated)");
			_port.Verify();
		}


		[Test]
		public void UpdateAsciiSettingWithoutParamType()
		{
			// SETUP
			CreateViewForAsciiSetting(ref _settingVM);
			_port.IsAsciiMode = true;
			_settingVM.SettingInfo.ParamType = null; // Occurs when All Devices is selected.
			var dataVM = _settingVM.Data as SettingValueVM;
			dataVM.Value = "900";

			// EXPECT
			_port.Expect("03 0 set pos 900");
			_port.AddResponse("@03 0 OK IDLE -- 0");

			// EXEC
			var topic = _settingVM.Request("900");
			var finalSettingValue = dataVM.Value;

			// VERIFY
			Assert.AreEqual("900", finalSettingValue, "Final value should match (not updated)");
			_port.Verify();
		}


		[Test]
		public void UpdateAsciiTokenSetting()
		{
			CreateViewForAsciiSetting(ref _settingVM);
			_settingVM.SettingInfo.ParamType = AsciiCommandBuilder.StandardParamTypes["Token"];

			_port.IsAsciiMode = true;
			var dataVM = _settingVM.Data as SettingValueVM;
			dataVM.Value = "9001";

			_port.Expect("03 0 set pos 9001");
			_port.AddResponse("@03 0 OK IDLE -- 0");

			var topic = _settingVM.Request("9001");
			var finalSettingValue = dataVM.Value;

			Assert.IsNull(topic);
			Assert.AreEqual("9001", finalSettingValue);
			_port.Verify();
		}


		[Test]
		public void UpdateAsciiBooleanSetting()
		{
			CreateViewForAsciiSetting(ref _settingVM);
			_settingVM.SettingInfo.ParamType = AsciiCommandBuilder.StandardParamTypes["Bool"];

			_port.IsAsciiMode = true;
			var dataVM = _settingVM.Data as SettingValueVM;
			dataVM.Value = "1";
			// Currently only 0 and 1 are supported; not true/false.

			_port.Expect("03 0 set pos 1");
			_port.AddResponse("@03 0 OK IDLE -- 0");

			var topic = _settingVM.Request("1");
			var finalSettingValue = dataVM.Value;

			Assert.IsNull(topic);
			Assert.AreEqual("1", finalSettingValue);
			_port.Verify();
		}


		[Test]
		public void UpdateAsciiSettingWithDecimalPlaces()
		{
			CreateViewForAsciiSetting(ref _settingVM);
			_settingVM.SettingInfo.ParamType = new ParameterType
			{
				IsNumeric = true,
				DecimalPlaces = 2
			};

			_port.IsAsciiMode = true;
			var dataVM = _settingVM.Data as SettingValueVM;
			dataVM.Value = "900.155";

			_port.Expect("03 0 set pos 900.16");
			_port.AddResponse("@03 0 OK IDLE -- 0");

			Assert.IsTrue(dataVM.WriteCommand.CanExecute(null));
			dataVM.WriteCommand.Execute(null);
			_port.Verify();

			_port.Expect("03 0 get pos");
			_port.AddResponse("@03 0 OK IDLE -- 900.16");

			Assert.IsTrue(dataVM.ReadCommand.CanExecute(null));
			dataVM.ReadCommand.Execute(null);

			Assert.AreEqual("900.16", dataVM.Value);
			_port.Verify();
		}


		[Test]
		public void UpdateWithUnknownErrorResponse()
		{
			// EXPECT
			_port.Expect(3, Command.SetTargetSpeed, 900);
			_port.AddResponse(3, Command.Error, 7777);

			// EXEC
			var topic = _commandVM.Request("900");

			var dataVM = _commandVM.Data as CommandArgumentsVM;
			var finalSettingValue = dataVM.Value;
			var errorText = _commandVM.ToolTip;

			_commandVM.Clear();
			var errorTextAfterClear = _commandVM.ToolTip;

			// VERIFY
			Assert.AreEqual("",
							finalSettingValue,
							"Final value should match (not updated)");
			Assert.AreEqual("Error occurred: 7777.",
							errorText,
							"error text");
			Assert.IsTrue(string.IsNullOrEmpty(errorTextAfterClear), "error text after clear");
			Assert.AreEqual(null,
							topic,
							"topic");
			_port.Verify();
		}


		[Test]
		public void Name()
		{
			// SETUP
			var commandInfo1 = new SettingInfo { Name = "Set Foo" };

			var commandInfo2 = new CommandInfo { Name = "Bar" };
			SettingInfo commandInfo3 = new ReadOnlySettingInfo { Name = "Return Baz" };

			// This is a command that happens to start with the word Return
			var commandInfo4 = new CommandInfo { Name = "Return Bizz" };

			var setting1 = new SettingInfoVM(commandInfo1, null);
			var setting2 = new CommandInfoVM(commandInfo2, null);
			var setting3 = new SettingInfoVM(commandInfo3, null);
			var setting4 = new CommandInfoVM(commandInfo4, null);

			// EXEC
			var name1 = setting1.Name;
			var name2 = setting2.Name;
			var name3 = setting3.Name;
			var name4 = setting4.Name;

			// VERIFY
			Assert.AreEqual("Foo",
							name1,
							"'Set' should be removed from the start of the name.");
			Assert.AreEqual("Bar",
							name2,
							"Name should be left alone if no 'Set'.");
			Assert.AreEqual("Baz",
							name3,
							"'Return' should be removed from the start of the name.");
			Assert.AreEqual("Return Bizz",
							name4,
							"Name should be left alone if it's not a setting");
		}


		[Test]
		public void SendCommandWithUnits()
		{
			// SETUP
			CreateViewForBinaryCommand(ref _commandVM);
			_commandVM.UnitOfMeasure = UnitOfMeasure.Millimeter;

			// EXPECT
			_port.Expect(3, Command.MoveRelative, 5001);
			_port.AddResponse(3, Command.MoveRelative, 5001);

			// EXEC
			_commandVM.Request("5.0007");
			var errorText = _commandVM.ToolTip;

			// VERIFY
			Assert.That(errorText,
						Is.Empty,
						"error text");
			_port.Verify();
		}


		[Test]
		public void SendCommandWithVelocityUnits()
		{
			// SETUP
			CreateViewForBinaryCommand(ref _commandVM);
			var deviceType = _commandVM.Conversation.Device.DeviceType;
			deviceType.MotionType = MotionType.Linear;
			var commandInfo = _commandVM.CommandInfo;
			commandInfo.Command = Command.MoveAtConstantSpeed;
			commandInfo.RequestUnit = UnitOfMeasure.MillimetersPerSecond;
			commandInfo.RequestUnitScale = 1.666666666666666667m;
			_commandVM.UnitOfMeasure = UnitOfMeasure.MillimetersPerSecond;

			// since we changed the command, we need to refresh the device
			// type's list of commands.
			deviceType.Commands = deviceType.Commands;

			// EXPECT
			_port.Expect(3, Command.MoveAtConstantSpeed, 1000);
			_port.AddResponse(3, Command.MoveAtConstantSpeed, 1000);

			// EXEC
			_commandVM.Request("9.375");
			var errorText = _commandVM.ToolTip;

			// VERIFY
			Assert.That(errorText,
						Is.Empty,
						"error text");
			_port.Verify();
		}


		[Test]
		public void RefreshWithUnits()
		{
			// SETUP
			var settingInfo = new SettingInfo
			{
				Command = Command.SetCurrentPosition,
				Name = "Current Position",
				DataDescription = "Current Position",
				IsRequestRelativePosition = false,
				IsBasic = true,
				IsCurrentPositionReturned = true,
				ResponseUnit = UnitOfMeasure.Millimeter,
				ResponseUnitFunction = ScalingFunction.LinearResolution,
				ResponseUnitScale = 15.625m,
				RequestUnit = UnitOfMeasure.Millimeter,
				RequestUnitFunction = ScalingFunction.LinearResolution,
				RequestUnitScale = 15.625m
			};

			var conversation = _settingVM.Conversation;
			var commands = new List<CommandInfo>(conversation.Device.DeviceType.Commands) { settingInfo };
			conversation.Device.DeviceType.Commands = commands;

			var view = new SettingInfoVM(settingInfo, conversation) { UnitOfMeasure = UnitOfMeasure.Inch };

			// EXPECT
			_port.Expect(3, Command.ReturnSetting, (int) Command.SetCurrentPosition);
			_port.AddResponse(3, Command.SetCurrentPosition, 254);

			// EXEC
			view.Refresh();
			var dataVM = view.Data as SettingValueVM;
			var settingValue = dataVM.Value;

			// VERIFY
			Assert.AreEqual("0.01", settingValue, "Value should match");
			_port.Verify();
		}


		[Test]
		public void RefreshAsciiSettingWithUnits()
		{
			// SETUP
			CreateViewForAsciiSetting(ref _settingVM);
			_port.IsAsciiMode = true;
			_settingVM.Conversation.Device.Port = _port; // reset IsTrackingRequests.

			var deviceType = _settingVM.Conversation.Device.DeviceType;
			deviceType.MotionType = MotionType.Linear;
			_settingVM.Conversation.Device.DeviceType = deviceType; //recalc units
			_settingVM.UnitOfMeasure = UnitOfMeasure.Inch;

			// EXPECT
			_port.Expect("/03 0 get pos");
			_port.AddResponse("@03 0 OK IDLE -- 254");

			// EXEC
			var dataVM = _settingVM.Data as SettingValueVM;
			dataVM.ReadCommand.Execute(null);
			var settingValue = dataVM.Value;

			// VERIFY
			Assert.AreEqual("0.01",
							settingValue,
							"Value should match");
			_port.Verify();
		}


		[Test]
		public void RefreshMultiAxisAsciiSettingWithUnits()
		{
			// SETUP
			CreateViewForAsciiSetting(ref _settingVM);
			_port.IsAsciiMode = true;
			_commandVM.Conversation.Device.Port = _port; // reset IsTrackingRequests.

			var deviceType = _settingVM.Conversation.Device.DeviceType;
			deviceType.MotionType = MotionType.Linear;
			_settingVM.Conversation.Device.DeviceType = deviceType; //recalc units
			_settingVM.UnitOfMeasure = UnitOfMeasure.Inch;

			// EXPECT
			_port.Expect("/03 0 get pos");
			_port.AddResponse("@03 0 OK IDLE -- 254 508");

			// EXEC
			var dataVM = _settingVM.Data as SettingValueVM;
			dataVM.ReadCommand.Execute(null);
			var settingValue = dataVM.Value;

			// VERIFY
			Assert.AreEqual("0.01 0.02",
							settingValue,
							"Value should match");
			_port.Verify();
		}


		[Test]
		public void RefreshAsciiSetting()
		{
			// SETUP
			CreateViewForAsciiSetting(ref _settingVM);
			_port.IsAsciiMode = true;

			// EXPECT
			_port.Expect("03 0 get pos");
			_port.AddResponse("@03 0 OK IDLE -- 254");

			// EXEC
			var dataVM = _settingVM.Data as SettingValueVM;
			dataVM.ReadCommand.Execute(null);
			var settingValue = dataVM.Value;

			// VERIFY
			Assert.AreEqual("254",
							settingValue,
							"Value should match");
			_port.Verify();
		}


		[Test]
		public void RefreshWithVelocityUnits()
		{
			// SETUP
			var settingInfo = new SettingInfo
			{
				Command = Command.SetHomeSpeed,
				Name = "Home Speed",
				DataDescription = "Speed",
				ResponseDescription = "Speed",
				RequestUnit = UnitOfMeasure.MillimetersPerSecond,
				RequestUnitFunction = ScalingFunction.LinearResolution,
				RequestUnitScale = 15.625m,
				ResponseUnit = UnitOfMeasure.MillimetersPerSecond,
				ResponseUnitFunction = ScalingFunction.LinearResolution,
				ResponseUnitScale = 15.625m
			};

			var conversation = _settingVM.Conversation;
			var deviceType = conversation.Device.DeviceType;
			var commands = new List<CommandInfo>(deviceType.Commands) { settingInfo };
			deviceType.Commands = commands;
			deviceType.MotionType = MotionType.Linear;

			GiveDevicePositionCommand(deviceType);

			var view = new SettingInfoVM(settingInfo, conversation) { UnitOfMeasure = UnitOfMeasure.InchesPerSecond };

			// EXPECT
			_port.Expect(3, Command.ReturnSetting, (int) Command.SetHomeSpeed);
			_port.AddResponse(3, Command.SetHomeSpeed, 254);

			// EXEC
			view.Refresh();
			var dataVM = view.Data as SettingValueVM;
			var settingValue = dataVM.Value;

			// VERIFY
			Assert.AreEqual("0.01", settingValue, "Value should match");
			_port.Verify();
		}


		/// <summary>
		///     Adds the "Get position" command to a DeviceType's list of commands.
		///     This command is necessary for any UOM conversion to work.
		/// </summary>
		/// <param name="device">The device to add the command to.</param>
		private static void GiveDevicePositionCommand(DeviceType device)
		{
			var settingInfo = new SettingInfo
			{
				Command = Command.SetCurrentPosition,
				Number = 45,
				Name = "Current Position",
				DataDescription = "Position",
				ResponseDescription = "Position",
				RequestUnit = UnitOfMeasure.Millimeter,
				RequestUnitFunction = ScalingFunction.LinearResolution,
				RequestUnitScale = 15.625m,
				ResponseUnit = UnitOfMeasure.Millimeter,
				ResponseUnitFunction = ScalingFunction.LinearResolution,
				ResponseUnitScale = 15.625m
			};
			var commands = new List<CommandInfo>(device.Commands) { settingInfo };
			device.Commands = commands;
		}


		private static void CreateViewForBinaryCommand(ref CommandInfoVM view)
		{
			var commandInfo = new CommandInfo
			{
				Command = Command.MoveRelative,
				Name = "Move Relative",
				Number = 21,
				DataDescription = "Relative Position",
				RequestUnit = UnitOfMeasure.Millimeter,
				RequestUnitFunction = ScalingFunction.LinearResolution,
				RequestUnitScale = 15.625m
			};

			var conversation = view.Conversation;
			var commands = new List<CommandInfo>(conversation.Device.DeviceType.Commands) { commandInfo };
			var deviceType = conversation.Device.DeviceType;
			deviceType.Commands = commands;
			deviceType.MotionType = MotionType.Linear;

			view = new CommandInfoVM(commandInfo, conversation);
		}


		private static void CreateViewForAsciiCommand(ref CommandInfoVM view)
		{
			var conversation = view.Conversation;
			var deviceType = conversation.Device.DeviceType;
			deviceType.MotionType = MotionType.Linear;
			var commandInfo = new CommandInfo
			{
				Name = "move rel",
				TextCommand = "move rel",
				HasParameters = true,
				IsBasic = true,
				AccessLevel = 1,
				IsRequestRelativePosition = false,
				RequestUnit = UnitOfMeasure.Millimeter,
				RequestUnitFunction = ScalingFunction.LinearResolution,
				RequestUnitScale = 15.625m
			};
			deviceType.Commands = new List<CommandInfo>(deviceType.Commands) { commandInfo };

			view = new CommandInfoVM(commandInfo, conversation);
		}


		private static void CreateViewForAsciiSetting(ref SettingInfoVM view)
		{
			var commandInfo = new SettingInfo
			{
				TextCommand = "pos",
				IsRequestRelativePosition = false,
				IsBasic = true,
				IsCurrentPositionReturned = true,
				ResponseUnit = UnitOfMeasure.Millimeter,
				ResponseUnitFunction = ScalingFunction.LinearResolution,
				ResponseUnitScale = 15.625m,
				RequestUnit = UnitOfMeasure.Millimeter,
				RequestUnitFunction = ScalingFunction.LinearResolution,
				RequestUnitScale = 15.625m,
				ParamType = AsciiCommandBuilder.StandardParamTypes["Int"]
			};

			var conversation = view.Conversation;
			var commands = new List<CommandInfo>(conversation.Device.DeviceType.Commands) { commandInfo };
			conversation.Device.DeviceType.Commands = commands;

			view = new SettingInfoVM(commandInfo, conversation);
		}


		private static void CreateViewForReadOnlySetting(ref SettingInfoVM view)
		{
			SettingInfo settingInfo = new ReadOnlySettingInfo
			{
				Command = Command.ReturnPowerSupplyVoltage,
				Name = "Return Power Supply Voltage"
			};

			var conversation = view.Conversation;

			view = new SettingInfoVM(settingInfo, conversation);
		}


		private MockPort _port;
		private CommandInfoVM _commandVM;
		private SettingInfoVM _settingVM;
	}
}

﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Zaber.Testing;
using ZaberConsole.Plugins.CommandList;

namespace ZaberConsoleTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class CommandGridVMTests
	{
		[SetUp]
		public void Setup()
		{
			_vm = new CommandGridVM(true);
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
		}


		[TearDown]
		public void Teardown()
		{
			_listener.Unlisten(_vm);
			_vm = null;
		}


		[Test]
		public void TestRowsProperty()
		{
			_listener.TestObservableProperty(_vm,
											 () => _vm.Rows,
											 _ =>
											 {
												 var list = new ObservableCollection<CommandGridRowBaseVM>();
												 list.Add(new CommandGridRowBaseVM());
												 return list;
											 });
			Assert.AreEqual(0, _listener.GetNumNotifications(), "Unexpected side effect notifications.");
		}


		[Test]
		public void TestSelectedRowProperty()
		{
			_listener.TestObservableProperty(_vm, () => _vm.SelectedRow, _ => new CommandGridRowBaseVM());
			Assert.AreEqual(0, _listener.GetNumNotifications(), "Unexpected side effect notifications.");
		}


		private CommandGridVM _vm;
		private ObservableObjectTester _listener;
	}
}

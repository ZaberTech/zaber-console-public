﻿using System.IO;
using System.Xml.Serialization;
using NUnit.Framework;
using Zaber;
using ZaberConsole.Plugins.CommandList;
using ZaberTest;
using ZaberTest.Testing;

namespace ZaberConsoleGUITests.PluginTests.CommandListTests
{
	[TestFixture]
	[SetCulture("en-US")]
	internal class FavoriteCommandSettingsTests
	{
		[SetUp]
		public void Setup()
		{
			_allDevicesId = new DeviceIdentity { IsAllDevices = true };

			_hasSerialId = new DeviceIdentity { DeviceSerialNumber = 12345 };

			_isPeripheralId = new DeviceIdentity { PeripheralId = 12345 };

			_hasDeviceIdId = new DeviceIdentity { DeviceId = 12345 };

			_hasNumberId = new DeviceIdentity
			{
				AxisNumber = 1,
				DeviceNumber = 1
			};

			_settings = new FavoriteCommandSettings();
		}


		[Test]
		public void TestDeserialize()
		{
			var ser = new XmlSerializer(typeof(FavoriteCommandSettings));
			FavoriteCommandSettings settings = null;
			using (var reader =
				new StreamReader(TestDataFileHelper.GetDeployedPath("PluginTests\\Commands\\FavoriteCommands.xml")))
			{
				settings = ser.Deserialize(reader) as FavoriteCommandSettings;
			}

			Assert.IsNotNull(settings);
			Assert.AreEqual(3, settings.PerDeviceSettings.Count);

			var device = settings.PerDeviceSettings[0];
			Assert.AreEqual(20043, device.DeviceIdentity.DeviceId);
			Assert.AreEqual("home", device.FavoriteAsciiCommands[0]);

			device = settings.PerDeviceSettings[1];
			Assert.AreEqual(30221, device.DeviceIdentity.DeviceId);
			Assert.AreEqual(48313, device.DeviceIdentity.PeripheralId);
			Assert.AreEqual("move abs {0}", device.FavoriteAsciiCommands[0]);
			Assert.AreEqual("move", device.ExpandedAsciiGroups[0]);

			device = settings.PerDeviceSettings[2];
			Assert.AreEqual(30221, device.DeviceIdentity.DeviceId);
			Assert.AreEqual(44112, device.DeviceIdentity.PeripheralId);
			Assert.AreEqual("stop", device.FavoriteAsciiCommands[0]);
		}


		[Test]
		public void TestSerialize()
		{
			AddIds();
			TestForwardData(_settings); // Sanity check.

			// Serialize to a string.
			var data = ObjectToXmlString.Serialize(_settings);

			// Deserialize back to a new instance.
			Assert.IsFalse(string.IsNullOrEmpty(data));
			var newSettings = ObjectToXmlString.Deserialize<FavoriteCommandSettings>(data);

			// Test we got the same data back.
			TestForwardData(newSettings);
		}


		private void AddIds()
		{
			_settings.PerDeviceSettings.Add(new FavoriteCommandDeviceSettings { DeviceIdentity = _allDevicesId });
			_settings.PerDeviceSettings.Add(new FavoriteCommandDeviceSettings { DeviceIdentity = _hasSerialId });
			_settings.PerDeviceSettings.Add(new FavoriteCommandDeviceSettings { DeviceIdentity = _isPeripheralId });
			_settings.PerDeviceSettings.Add(new FavoriteCommandDeviceSettings { DeviceIdentity = _hasDeviceIdId });
			_settings.PerDeviceSettings.Add(new FavoriteCommandDeviceSettings { DeviceIdentity = _hasNumberId });
		}


		private void TestForwardData(FavoriteCommandSettings aSettings)
		{
			Assert.IsNotNull(aSettings.PerDeviceSettings);
			var count = aSettings.PerDeviceSettings.Count;
			Assert.AreEqual(5, aSettings.PerDeviceSettings.Count);

			var id = new DeviceIdentity();
			id.IsAllDevices = true;
			var result = aSettings.FindOrCreateSettings(id);
			Assert.AreEqual(count, aSettings.PerDeviceSettings.Count);
			Assert.AreEqual(_allDevicesId, result.DeviceIdentity);

			id.IsAllDevices = false;
			id.DeviceSerialNumber = 12345;
			result = aSettings.FindOrCreateSettings(id);
			Assert.AreEqual(count, aSettings.PerDeviceSettings.Count);
			Assert.AreEqual(_hasSerialId, result.DeviceIdentity);

			id.DeviceSerialNumber = null;
			id.PeripheralId = 12345;
			result = aSettings.FindOrCreateSettings(id);
			Assert.AreEqual(count, aSettings.PerDeviceSettings.Count);
			Assert.AreEqual(_isPeripheralId, result.DeviceIdentity);

			id.PeripheralId = null;
			id.DeviceId = 12345;
			result = aSettings.FindOrCreateSettings(id);
			Assert.AreEqual(count, aSettings.PerDeviceSettings.Count);
			Assert.AreEqual(_hasDeviceIdId, result.DeviceIdentity);

			id.DeviceId = 0;
			id.AxisNumber = 1;
			id.DeviceNumber = 1;
			result = aSettings.FindOrCreateSettings(id);
			Assert.AreEqual(count, aSettings.PerDeviceSettings.Count);
			Assert.AreEqual(_hasNumberId, result.DeviceIdentity);
		}


		private DeviceIdentity _allDevicesId;
		private DeviceIdentity _hasSerialId;
		private DeviceIdentity _isPeripheralId;
		private DeviceIdentity _hasDeviceIdId;
		private DeviceIdentity _hasNumberId;
		private FavoriteCommandSettings _settings;
	}
}

﻿using System.IO;
using NUnit.Framework;
using ZaberConsole.Plugins.Scripts;

namespace ZaberConsoleTest.PluginTests.Scripting
{
	[TestFixture]
	[SetCulture("en-US")]
	public class ScriptViewTest
	{
		[Test]
		public void Name()
		{
			// SETUP
			var scriptFolder = new DirectoryInfo("X:\\ScriptFolder");
			var scriptFile = new FileInfo("X:\\ScriptFolder\\SomeScript.cs");

			// EXEC
			var view = new ScriptListRowVM(scriptFile, scriptFolder, false);
			var name = view.Name;

			// VERIFY
			Assert.AreEqual("SomeScript.cs",
							name,
							"name");
		}


		[Test]
		public void NameInSubfolder()
		{
			// SETUP
			var scriptFolder = new DirectoryInfo("X:\\ScriptFolder");
			var scriptFile = new FileInfo("X:\\ScriptFolder\\Samples\\SomeScript.cs");

			// EXEC
			var view = new ScriptListRowVM(scriptFile, scriptFolder, false);
			var name = view.Name;

			// VERIFY
			Assert.AreEqual("Samples\\SomeScript.cs",
							name,
							"name");
		}
	}
}

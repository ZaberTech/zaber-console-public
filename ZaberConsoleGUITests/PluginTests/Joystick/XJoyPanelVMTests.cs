﻿using System;
using System.Collections.Generic;
using System.Windows;
using MvvmDialogs.ViewModels;
using NUnit.Framework;
using Rhino.Mocks;
using Zaber;
using Zaber.Testing;
using ZaberConsole.Plugins.Joystick;
using ZaberTest;
using ZaberTest.Testing;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Dialogs;

/// <summary>
/// Tests the XJoyPanelVM in ASCII. TODO add more tests (there isn't much to test).
/// </summary>
namespace ZaberConsoleGUITests.XJoyPluginTests
{
	[TestFixture]
	[Category("JoystickPlugin")]
	[SetCulture("en-US")]
	public class XJoyPanelVMTests
	{
		/// <summary>
		///     This setup is exactly the sme as JoystickPluginTests.cs
		///     This is to initiate a JoystickPluginVM who has the XJoyPanelVM member to be tested.
		/// </summary>
		[SetUp]
		public void SetUp()
		{
			var mockDispatcher = MockRepository.GenerateStrictMock<IDispatcher>();

			mockDispatcher.Stub(dispatcher => dispatcher.Invoke(null))
					   .IgnoreArguments()
					   .Repeat.Any()
					   .Do((Action<Action>) (action => action.Invoke()));
			mockDispatcher.Stub(dispatcher => dispatcher.BeginInvoke(null))
					   .IgnoreArguments()
					   .Repeat.Any()
					   .Do((Action<Action>) (action => action.Invoke()));

			DispatcherStack.Push(mockDispatcher);

			SetupPort();
			_plugin = new JoystickPluginVM();
			_plugin.PortFacade = _portFacade;
			_plugin.OnTabShown();
			_plugin.PortFacade.Open("COM3");
			JoystickPluginTests.SetupPortExpectations(_mockPort, "initial_command_responses.txt");

			var conversationList = _plugin.PortFacade.Conversations as List<Conversation>;

			BackgroundWorkerStack.Push(typeof(ImmediateForegroundWorker));
			_plugin.Conversation = conversationList.Find(x => x.Device.DeviceType.DeviceId == 51000);
			Assert.AreEqual("Axes 1 , 2", _plugin.Joysticks[3].TargetLockstepInfo);
			BackgroundWorkerStack.Pop();

			_panel = _plugin.XJoyPanel;
			_invocationCount = 0;
		}


		[TearDown]
		public void Teardown() => DispatcherStack.Pop();


		[Test]
		public void ChangeSettingsTest()
		{
			_mockPort.Expect("/01 0 set comm.alert 1");
			_mockPort.AddResponse("@01 0 OK IDLE -- 0");

			_plugin.Conversation.Request("set comm.alert 1");
			Assert.IsTrue(_panel.AlertsOn);

			_mockPort.Expect("/01 0 set joy.debug 1");
			_mockPort.AddResponse("@01 0 OK IDLE -- 0");

			_plugin.Conversation.Request("set joy.debug 1");
			Assert.IsTrue(_panel.DebugOn);
		}


		[Test]
		public void ClearWindowTest()
		{
			_panel.RequestDialogOpen += RequestDialogOpen_Counter;

			var cmd = _panel.ShowClearWindowCommand;
			Assert.IsTrue(cmd.CanExecute(null));
			cmd.Execute(null);

			Assert.AreEqual(1, _invocationCount);
			Assert.IsInstanceOf(typeof(ClearKeysWindowVM), _window);
		}


		[Test]
		public void SendAsciiCommandTest()
		{
			_mockPort.Expect("/01 0 ascii command");
			var cmd = _panel.SendAsciiCommand;
			cmd.Execute("ascii command");

			_mockPort.Verify();
		}


		[Test]
		public void ShowCalibrationWizardTest()
		{
			_panel.RequestDialogOpen += RequestDialogOpen_Counter;

			var cmd = _panel.OpenCalibrationWizardCommand;
			Assert.IsTrue(cmd.CanExecute(null));
			cmd.Execute(null);

			Assert.AreEqual(1, _invocationCount);
			Assert.IsInstanceOf(typeof(CalibrationWizardWindowVM), _window);
		}


		[Test]
		public void ShowRestoreModalWindowTest()
		{
			_panel.RequestDialogOpen += RequestDialogOpen_Counter;

			var cmd = _panel.OpenRestoreWindowCommand;
			Assert.IsTrue(cmd.CanExecute(null));
			cmd.Execute(null);

			Assert.AreEqual(1, _invocationCount);
			Assert.IsInstanceOf(typeof(CustomMessageBoxVM), _window);

			var restoreWindowNo = _window as CustomMessageBoxVM;
			restoreWindowNo.DialogResult = MessageBoxResult.No;
			restoreWindowNo.RequestClose();

			//Shouldn't fail due to an unexpected data packet.
		}


		[Test]
		public void XJoyPanelInitializationTest()
		{
			Assert.IsTrue(_panel.IsXJOY);
			Assert.IsTrue(_panel.IsAsciiMode);
			Assert.IsFalse(_panel.DebugOn);
			Assert.IsFalse(_panel.AlertsOn);
		}


		private void SetupPort()
		{
			_mockPort = new MockPort();
			_mockPort.IsAsciiMode = true;

			var defaultDeviceType = new DeviceType { Commands = new List<CommandInfo>() };

			_portFacade = new ZaberPortFacade
			{
				DefaultDeviceType = defaultDeviceType,
				Port = _mockPort,
				QueryTimeout = 100
			};

			var joyCommands = new List<CommandInfo> { new CommandInfo { Command = Command.SetAxisDeviceNumber } };

			joyCommands.AddRange(
				AsciiCommandBuilder.ReadCommandDefinitionsFromJsonFile(AsciiCommandBuilder.JOYSTICK_COMMAND_FILE));
			joyCommands.AddRange(
				AsciiCommandBuilder.ReadCommandDefinitionsFromJsonFile(AsciiCommandBuilder.KEY_COMMAND_FILE));

			var XJOY3 = new DeviceType
			{
				DeviceId = 51000,
				Name = "X-JOY3 Joystick",
				MotionType = MotionType.Other,
				Commands = joyCommands,
				FirmwareVersion = new FirmwareVersion(6, 14) // Because the expectations include message IDs.
			};

			var ASR100BT3 = new DeviceType
			{
				PeripheralId = 44012,
				MotionType = MotionType.Linear,
				Name = "X-MCB2 + ASR100BT3"
			};

			var ASR120BT3 = new DeviceType
			{
				PeripheralId = 44022,
				MotionType = MotionType.Linear,
				Name = "X-MCB2 + ASR120BT3"
			};

			var XLRM = new DeviceType
			{
				DeviceId = 50004,
				MotionType = MotionType.Linear,
				Name = "X-LRM"
			};

			var XMCB2 = new DeviceType
			{
				DeviceId = 30222,
				PeripheralMap = new Dictionary<int, DeviceType>
				{
					{ 44012, ASR100BT3 },
					{ 44022, ASR120BT3 }
				},
				Name = "X-MCB2 Controller",
				MotionType = MotionType.Other
			};

			_portFacade.AddDeviceType(XMCB2);
			_portFacade.AddDeviceType(XLRM);
			_portFacade.AddDeviceType(XJOY3);

			ZaberPortFacadeTest.SetAsciiExpectations(_portFacade, _mockPort, XJOY3, XMCB2, XLRM);
		}


		private void RequestDialogOpen_Counter(object sender, IDialogViewModel aVM)
		{
			_invocationCount++;
			_window = aVM;
		}


		private XJoyPanelVM _panel;
		public JoystickPluginVM _plugin;
		private MockPort _mockPort;
		private ZaberPortFacade _portFacade;
		private int _invocationCount;
		private IDialogViewModel _window;
	}
}

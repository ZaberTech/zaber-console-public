﻿using NUnit.Framework;
using ZaberConsole.Plugins.Joystick.Key;

/// <summary>
/// Tests the KeyInfoComparer IEqualityComparer interface used for determining if a grid has unwritten changes.
/// Tests Binary and ASCII.
/// </summary>
namespace ZaberConsoleGUITests.XJoyPluginTests
{
	[TestFixture]
	[Category("JoystickPlugin")]
	[Category("JoystickPluginBinary")]
	[SetCulture("en-US")]
	internal class KeyComparerTest
	{
		[SetUp]
		public void SetUp()
		{
			_this = new KeyCommandInfo();
			_that = new KeyCommandInfo();
			_addCommand = new KeyCommandInfo { IsAddCommand = true };
		}


		[TearDown]
		public void TearDown()
		{
			_this = null;
			_that = null;
			_addCommand = null;
		}


		[Test]
		public void ASCIIKeyComparerTest()
		{
			_this.CommandName = "move rel 500";
			_that.CommandName = "move rel 500";
			Assert.That(_this, Is.EqualTo(_that).Using(_compare));

			_that.CommandName = "different";
			Assert.That(_this, Is.Not.EqualTo(_that).Using(_compare));

			_that.CommandName = "move rel 500";
			_this.TargetAxis = "1";
			_this.TargetDeviceNumber = "2";
			_that.TargetAxis = "1";
			_that.TargetDeviceNumber = "2";
			Assert.That(_this, Is.EqualTo(_that).Using(_compare));

			_that.TargetAxis = "9";
			Assert.That(_this, Is.Not.EqualTo(_that).Using(_compare));

			_that.TargetAxis = "1";
			_that.TargetDeviceNumber = "9";
			Assert.That(_this, Is.Not.EqualTo(_that).Using(_compare));

			_that.TargetDeviceNumber = "2";
			Assert.That(_this, Is.EqualTo(_that).Using(_compare));

			_that.CommandName = "";
			_this.CommandName = "";
			Assert.That(_this, Is.EqualTo(_that).Using(_compare));
		}


		[Test]
		public void BinaryKeyComparerTest()
		{
			_this.TargetDeviceNumber = "42";
			_that.TargetDeviceNumber = "42";
			_this.BinaryData = "5000";
			_that.BinaryData = "5000";
			_this.BinaryCommand = "21";
			_that.BinaryCommand = "21";
			Assert.That(_this, Is.EqualTo(_that).Using(_compare));

			_that.TargetDeviceNumber = "40";
			Assert.That(_this, Is.Not.EqualTo(_that).Using(_compare));
			_that.TargetDeviceNumber = "42";
			Assert.That(_this, Is.EqualTo(_that).Using(_compare));

			_that.BinaryData = "3111";
			Assert.That(_this, Is.Not.EqualTo(_that).Using(_compare));
			_that.BinaryData = "5000";
			Assert.That(_this, Is.EqualTo(_that).Using(_compare));

			_that.BinaryCommand = "20";
			Assert.That(_this, Is.Not.EqualTo(_that).Using(_compare));
			_that.BinaryCommand = "21";
			Assert.That(_this, Is.EqualTo(_that).Using(_compare));
		}


		[Test]
		public void NullKeyComparerTest()
		{
			Assert.That(_this, Is.EqualTo(_that).Using(_compare));
			Assert.That(_addCommand, Is.EqualTo(_addCommand).Using(_compare));
		}


		public KeyCommandInfo _this;
		public KeyCommandInfo _that;
		public KeyCommandInfo _addCommand;
		public KeyInfoComparer _compare = new KeyInfoComparer();
	}
}

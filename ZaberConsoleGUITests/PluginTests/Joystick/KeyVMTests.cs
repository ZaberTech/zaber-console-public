﻿using System.Collections.Generic;
using NUnit.Framework;
using ZaberConsole;
using ZaberConsole.Plugins.Joystick.Key;

/// <summary>
/// Tests the KeyVM view model for ASCII.
/// </summary>
namespace ZaberConsoleGUITests.XJoyPluginTests
{
	[TestFixture]
	[Category("JoystickPlugin")]
	[SetCulture("en-US")]
	public class KeyVMTests
	{
		[SetUp]
		public void SetUp()
		{
			_key = new KeyVM(1, Protocol.ASCII);
			Initialize(_key);
		}


		[TearDown]
		public void Teardown() => _key = null;


		[Test]
		public void ParseKeyInfoTest()
		{
			Assert.AreEqual(_key.KeyCommandGrids[1].AlertsEnabled, false);
			Assert.AreEqual(_key.KeyCommandGrids[2].AlertsEnabled, true);
			Assert.AreEqual(_key.KeyCommandGrids[3].AlertsEnabled, true);
			Assert.AreEqual(_key.KeyCommandGrids[4].AlertsEnabled, false);

			Assert.AreEqual(_key.KeyCommandGrids[1].KeyCommands.Count, 3);
			Assert.AreEqual(_key.KeyCommandGrids[2].KeyCommands.Count, 1);
			Assert.AreEqual(_key.KeyCommandGrids[3].KeyCommands.Count, 1);
			Assert.AreEqual(_key.KeyCommandGrids[4].KeyCommands.Count, 2);

			Assert.AreEqual(_key.KeyCommandGrids[1].KeyCommands[1].CommandName, "tools storepos 1 current");
			Assert.AreEqual(_key.KeyCommandGrids[4].KeyCommands[1].CommandName, "joystick 1 maxspeed 44800");
			Assert.AreEqual(_key.KeyCommandGrids[4].KeyCommands[1].TargetAxis, "9");
			Assert.AreEqual(_key.KeyCommandGrids[4].KeyCommands[1].TargetDeviceNumber, "05");

			Assert.IsFalse(_key.HasUnsavedChanges);
		}


		// Test correct handling of the 6.17 key info format, which lacked the disambiguating "cmd" word.
		[Test]
		public void ParseOldKeyInfoTest()
		{
			var vm = new KeyVM(1, Protocol.ASCII);
			var keyCommands = new List<string>();
			keyCommands.Add("OK IDLE -- alerts disabled");
			keyCommands.Add("02 1 move abs 50000");
			keyCommands.Add("03 2 stop");

			vm.ParseKeyInfoAscii(keyCommands, 1);

			Assert.AreEqual(vm.KeyCommandGrids[1].KeyCommands[1].CommandName, "move abs 50000");
			Assert.AreEqual(vm.KeyCommandGrids[1].KeyCommands[2].CommandName, "stop");
			Assert.AreEqual(vm.KeyCommandGrids[1].KeyCommands[1].TargetAxis, "1");
			Assert.AreEqual(vm.KeyCommandGrids[1].KeyCommands[1].TargetDeviceNumber, "02");
			Assert.AreEqual(vm.KeyCommandGrids[1].KeyCommands[2].TargetAxis, "2");
			Assert.AreEqual(vm.KeyCommandGrids[1].KeyCommands[2].TargetDeviceNumber, "03");
			Assert.IsFalse(vm.HasUnsavedChanges);
		}


		[Test]
		public void SelectionChangedTest()
		{
			var firstSelectedItem = _key.KeyCommandGrids[1].KeyCommands[1];
			_key.KeyCommandGrids[1].SelectedItem = firstSelectedItem;
			Assert.AreEqual(_key.SelectedItem, _key.KeyCommandGrids[1].KeyCommands[1]);

			_key.KeyCommandGrids[4].SelectedItem = _key.KeyCommandGrids[4].KeyCommands[1];
			Assert.AreEqual(_key.SelectedItem, _key.KeyCommandGrids[4].KeyCommands[1]);
			Assert.IsNull(_key.KeyCommandGrids[1].SelectedItem);
		}


		[Test]
		public void UnsavedChangesTest_AddingAndDeleting()
		{
			var addRow = _key.KeyCommandGrids[1].KeyCommands[0];
			addRow.CommandName = "home";
			addRow.TargetAxis = "5";
			addRow.TargetDeviceNumber = "3";

			Assert.IsFalse(_key.HasUnsavedChanges);
			_key.KeyCommandGrids[1].AddRowCommand.Execute(addRow);
			Assert.IsTrue(_key.HasUnsavedChanges);
			_key.KeyCommandGrids[1].DeleteRowCommand.Execute(_key.KeyCommandGrids[1].KeyCommands[3]);
			Assert.IsFalse(_key.HasUnsavedChanges);
		}


		[Test]
		public void UnsavedChangesTest_ChangingInformation()
		{
			Assert.IsFalse(_key.HasUnsavedChanges);
			_key.KeyCommandGrids[4].KeyCommands[1].CommandName = "changed";
			Assert.IsTrue(_key.HasUnsavedChanges);
			_key.KeyCommandGrids[4].KeyCommands[1].CommandName = "joystick 1 maxspeed 44800";
		}


		private static void Initialize(KeyVM aKeyVM)
		{
			var keyCommands1 = new List<string>();
			var keyCommands2 = new List<string>();
			var keyCommands3 = new List<string>();
			var keyCommands4 = new List<string>();

			var alertsOff = "OK IDLE -- alerts disabled";
			var alertsOn = "OK IDLE -- alerts enabled";
			var command1 = "cmd 00 0 tools storepos 1 current";
			var command2 = "cmd 02 0 move rel 500";
			var command3 = "cmd 05 9 joystick 1 maxspeed 44800";

			keyCommands1.Add(alertsOff);
			keyCommands1.Add(command1);
			keyCommands1.Add(command2);

			keyCommands2.Add(alertsOn);
			keyCommands3.Add(alertsOn);

			keyCommands4.Add(alertsOff);
			keyCommands4.Add(command3);

			aKeyVM.ParseKeyInfoAscii(keyCommands1, 1);
			aKeyVM.ParseKeyInfoAscii(keyCommands2, 2);
			aKeyVM.ParseKeyInfoAscii(keyCommands3, 3);
			aKeyVM.ParseKeyInfoAscii(keyCommands4, 4);
		}


		public KeyVM _key;
	}
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using NUnit.Framework;
using Zaber;
using Zaber.Testing;
using ZaberConsole;
using ZaberConsole.Plugins.Joystick;
using ZaberTest;
using ZaberTest.Testing;

/// <summary>
/// High level tests for the JoystickPluginVM in ASCII mode.
/// </summary>
namespace ZaberConsoleGUITests.XJoyPluginTests
{
	[TestFixture]
	[Category("JoystickPlugin")]
	[SetCulture("en-US")]
	public class JoystickPluginTests
	{
		[SetUp]
		public void SetUp()
		{
			DispatcherStack.Push(new ImmediateDispatcher());
			BackgroundWorkerStack.Push(typeof(ImmediateForegroundWorker));

			SetupPort();
			_plugin = new JoystickPluginVM();
			_plugin.PortFacade = _portFacade;
			_plugin.OnTabShown();
			_plugin.PortFacade.Open("COM3");
			SetupPortExpectations(_mockPort, "initial_command_responses.txt");
		}


		[TearDown]
		public void Teardown()
		{
			_plugin = null;
			BackgroundWorkerStack.Pop();
			DispatcherStack.Pop();
		}


		[Test]
		public void ChangingKeyCommandToInvalidCausingErrorTest()
		{
			//TODO
		}


		[Test]
		public void ClearKeysWindowAsciiTest_2Keys()
		{
			WaitForInitialQuery();

			SetupPortExpectations(_mockPort, "clearing_2_keys.txt");

			var clearKeysWindow = new ClearKeysWindowVM(_plugin, Protocol.ASCII);

			clearKeysWindow.KeysToDelete[1] = true;
			clearKeysWindow.KeysToDelete[2] = true;

			var cmd = clearKeysWindow.ClearCommand;
			cmd.Execute(null);
			Assert.AreEqual(1, _plugin.Keys[2].KeyCommandGrids[4].KeyCommands.Count);
			Assert.IsFalse(clearKeysWindow.KeysToDelete[1]);
			Assert.IsFalse(clearKeysWindow.KeysToDelete[2]);

			for (var i = 1; i < 5; i++)
			{
				Assert.AreEqual(1, _plugin.Keys[1].KeyCommandGrids[i].KeyCommands.Count);
				Assert.AreEqual(1, _plugin.Keys[2].KeyCommandGrids[i].KeyCommands.Count);
			}

			_mockPort.Verify();
		}


		[Test]
		public void ClearKeysWindowAsciiTest_AllKeys()
		{
			WaitForInitialQuery();
			SetupPortExpectations(_mockPort, "clearing_all_keys.txt");

			var clearKeysWindow = new ClearKeysWindowVM(_plugin, Protocol.ASCII);

			var selectCmd = clearKeysWindow.SelectAllCommand;
			selectCmd.Execute(true);

			var clearCmd = clearKeysWindow.ClearCommand;
			clearCmd.Execute(null);
			Assert.AreEqual(1, _plugin.Keys[8].KeyCommandGrids[4].KeyCommands.Count);

			for (var i = 1; i < 9; i++)
			{
				Assert.IsFalse(clearKeysWindow.KeysToDelete[i]);
				Assert.AreEqual(1, _plugin.Keys[i].KeyCommandGrids[1].KeyCommands.Count);
				Assert.AreEqual(1, _plugin.Keys[i].KeyCommandGrids[2].KeyCommands.Count);
				Assert.AreEqual(1, _plugin.Keys[i].KeyCommandGrids[3].KeyCommands.Count);
				Assert.AreEqual(1, _plugin.Keys[i].KeyCommandGrids[4].KeyCommands.Count);
			}

			_mockPort.Verify();
		}


		[Test]
		public void ClickedOnJoystickViewTest()
		{
			var conversationList = _plugin.PortFacade.Conversations as List<Conversation>;
			_plugin.Conversation = conversationList.Find(x => x.Device.DeviceType.DeviceId == 51000);

			SetupPortExpectations(_mockPort, "axis_queries.txt");

			var cmd = _plugin.ClickedJoystickButtonCommand;
			cmd.Execute(_plugin.XJoyPanel.Buttons[9]); //the 9th button is the joystick.

			Assert.IsTrue(_plugin.JoystickVisible);
			Assert.IsFalse(_plugin.KeyVisible);
			Assert.IsTrue(_plugin.SelectedTab[1]);
		}


		[Test]
		public void ClickedOnKeyViewTest()
		{
			var conversationList = _plugin.PortFacade.Conversations as List<Conversation>;
			_plugin.Conversation = conversationList.Find(x => x.Device.DeviceType.DeviceId == 51000);

			_mockPort.ReadExpectationsFromString("/01 0 33 key 5 1 info");
			_mockPort.AddResponse("@01 0 33 OK IDLE-- alerts disabled");
			_mockPort.ReadExpectationsFromString("/01 0 34 key 5 2 info");
			_mockPort.AddResponse("@01 0 34 OK IDLE-- alerts disabled");
			_mockPort.ReadExpectationsFromString("/01 0 35 key 5 3 info");
			_mockPort.AddResponse("@01 0 35 OK IDLE-- alerts disabled");
			_mockPort.ReadExpectationsFromString("/01 0 36 key 5 4 info");
			_mockPort.AddResponse("@01 0 36 OK IDLE-- alerts disabled");

			var cmd = _plugin.ClickedKeyButtonCommand;
			cmd.Execute(_plugin.XJoyPanel.Buttons[5]);

			Assert.IsTrue(_plugin.KeyVisible);
			Assert.IsFalse(_plugin.JoystickVisible);
			Assert.AreEqual(_plugin.KeyDisplay, _plugin.Keys[5]);
		}


		[Test]
		public void RM7086_ClickedOnKeyTerminatesDespitePolling()
		{
			var conversationList = _plugin.PortFacade.Conversations as List<Conversation>;
			_plugin.Conversation = conversationList.Find(x => x.Device.DeviceType.DeviceId == 51000);

			_mockPort.ReadExpectationsFromString("/01 0 33 key 5 1 info");
			_mockPort.AddResponse("@01 0 33 OK IDLE-- alerts disabled");
			_mockPort.ReadExpectationsFromString("/01 0 34 key 5 2 info");
			_mockPort.AddResponse("@01 0 34 OK IDLE-- alerts disabled");
			_mockPort.ReadExpectationsFromString("/01 0 35 key 5 3 info");
			_mockPort.AddResponse("@01 0 35 OK IDLE-- alerts disabled");
			_mockPort.ReadExpectationsFromString("/01 0 36 key 5 4 info");
			_mockPort.AddResponse("@01 0 36 OK IDLE-- alerts disabled");

			var gate = new ManualResetEvent(false);

			void PollingThread()
			{
				var startTime = DateTime.Now;
				var id = 0;
				while (!gate.WaitOne(50))
				{
					_mockPort.FireResponse($"@01 0 {(id % 30 + 1):02} OK IDLE -- 0");
					id++;
					if ((DateTime.Now - startTime).TotalSeconds > 10.0)
					{
						Assert.Fail("Key info read was stalled by polling messages.");
					}
				}
			};

			var thread = new Thread(PollingThread);
			thread.Start();

			_plugin.ClickedKeyButtonCommand.Execute(_plugin.XJoyPanel.Buttons[5]);
			gate.Set();
			thread.Join();

			Assert.IsTrue(_plugin.KeyVisible);
			Assert.IsFalse(_plugin.JoystickVisible);
			Assert.AreEqual(_plugin.KeyDisplay, _plugin.Keys[5]);
		}


		/// <summary>
		///     We currently don't have any controllers that can have more than 1 virtual axis or lockstep.
		///     This is mocking how a device SHOULD behave if it had virtual.numvirtual > 1.
		/// </summary>
		[Test]
		public void How2VirtualAxesAndLockstepShouldBehaveTest()
		{
			WaitForInitialQuery();

			_mockPort.Expect("/01 0 joystick 1 info");
			_mockPort.AddResponse("@01 0 OK IDLE -- 2 virtual 2 6900 1 1 51");
			_mockPort.Expect("/02 0 get virtual.numvirtual");
			_mockPort.AddResponse("@02 0 OK IDLE WR 2");
			_mockPort.Expect("/02 0 get lockstep.numgroups");
			_mockPort.AddResponse("@02 0 OK IDLE WR 2");

			_mockPort.Expect("/02 0 virtual 1 info");
			_mockPort.AddResponse("@02 0 OK IDLE WR 1 2 28.5 600000");
			_mockPort.Expect("/02 0 virtual 2 info");
			_mockPort.AddResponse("@02 0 OK IDLE WR 3 2 1 5 600000");

			_mockPort.Expect("/02 0 lockstep 1 info");
			_mockPort.AddResponse("@02 0 OK IDLE -- 1 2 525 0");
			_mockPort.Expect("/02 0 lockstep 2 info");
			_mockPort.AddResponse("@02 0 OK IDLE -- 1 3 525 0");

			var readCmd = _plugin.ReadJoystickInfoCommand;
			readCmd.Execute(_plugin.Joysticks[1]);
			Assert.AreEqual("Axes 3 , 2 (Ratio : 1 to 5)", _plugin.Joysticks[1].TargetVirtualAxisInfo);

			Assert.AreEqual(_plugin.Joysticks[1].TargetVirtualAxisInfo, "Axes 3 , 2 (Ratio : 1 to 5)");
			Assert.AreEqual(_plugin.Joysticks[1].TargetLockstepInfo, "Axes 1 , 2");

			_plugin.Joysticks[1].ViewInfo.VirtualIndex = "1";
			_plugin.Joysticks[1].ViewInfo.LockstepIndex = "2";

			Assert.AreEqual(_plugin.Joysticks[1].TargetVirtualAxisInfo, "Axes 1 , 2 (Angle : 28.5 degrees)");
			Assert.AreEqual(_plugin.Joysticks[1].TargetLockstepInfo, "Axes 1 , 3");

			_mockPort.Verify();
		}


		[Test]
		public void InitialJoystickParseTest()
		{
			WaitForInitialQuery();

			Assert.AreEqual(_plugin.Joysticks[1].ViewInfo.TargetDeviceNumber, "52");
			Assert.AreEqual(_plugin.Joysticks[1].ViewInfo.IsTargetingNormalAxis, true);
			Assert.AreEqual(_plugin.Joysticks[1].ViewInfo.TargetAxis, "1");
			Assert.AreEqual(_plugin.Joysticks[1].ViewInfo.SpeedSelected, "Squared (Default)");
			Assert.AreEqual(_plugin.Joysticks[1].ViewInfo.MaxSpeed, "44880");
			Assert.AreEqual(_plugin.Joysticks[1].ViewInfo.IsInverted, false);
			Assert.AreEqual(_plugin.Joysticks[1].ViewInfo.Resolution, "50");
			Assert.AreEqual(_plugin.Joysticks[1].HasUnsavedChanges, false);

			Assert.AreEqual(_plugin.Joysticks[2].ViewInfo.TargetDeviceNumber, "3");
			Assert.AreEqual(_plugin.Joysticks[2].ViewInfo.IsTargetingNormalAxis, true);
			Assert.AreEqual(_plugin.Joysticks[2].ViewInfo.TargetAxis, "1");
			Assert.AreEqual(_plugin.Joysticks[2].ViewInfo.SpeedSelected, "Squared (Default)");
			Assert.AreEqual(_plugin.Joysticks[2].ViewInfo.MaxSpeed, "44880");
			Assert.AreEqual(_plugin.Joysticks[2].ViewInfo.IsInverted, false);
			Assert.AreEqual(_plugin.Joysticks[2].ViewInfo.Resolution, "50");
			Assert.AreEqual(_plugin.Joysticks[2].HasUnsavedChanges, false);

			Assert.AreEqual(_plugin.Joysticks[3].ViewInfo.TargetDeviceNumber, "2");
			Assert.AreEqual(_plugin.Joysticks[3].ViewInfo.IsTargetingNormalAxis, true);
			Assert.AreEqual(_plugin.Joysticks[3].ViewInfo.TargetAxis, "1");
			Assert.AreEqual(_plugin.Joysticks[3].ViewInfo.SpeedSelected, "Squared (Default)");
			Assert.AreEqual(_plugin.Joysticks[3].ViewInfo.MaxSpeed, "44880");
			Assert.AreEqual(_plugin.Joysticks[3].ViewInfo.IsInverted, false);
			Assert.AreEqual(_plugin.Joysticks[3].ViewInfo.Resolution, "50");
			Assert.AreEqual(_plugin.Joysticks[3].HasUnsavedChanges, false);

			_mockPort.Verify();
		}


		[Test]
		public void InitialKeyParseTest()
		{
			WaitForInitialQuery();

			var keyCommands1 = _plugin.Keys[1].KeyCommandGrids[1].KeyCommands;
			var keyAlertsVM = _plugin.Keys[2].KeyCommandGrids[1];

			Assert.AreEqual(_plugin.Keys[1].HasUnsavedChanges, false);
			Assert.AreEqual(_plugin.Keys[1].ID, "1");

			Assert.AreEqual(keyCommands1[0].IsAddCommand, true);
			Assert.AreEqual(keyCommands1[0].TargetDeviceNumber, null);
			Assert.AreEqual(keyCommands1[0].TargetAxis, null);
			Assert.AreEqual(keyCommands1[0].CommandName, null);

			Assert.AreEqual(keyCommands1[1].TargetDeviceNumber, "00");
			Assert.AreEqual(keyCommands1[1].TargetAxis, "0");
			Assert.AreEqual(keyCommands1[1].CommandName, "home");
			Assert.AreEqual(keyCommands1[1].IsAddCommand, false);

			Assert.AreEqual(keyAlertsVM.AlertsEnabled, true);
			Assert.AreEqual(keyAlertsVM.KeyCommands.Count, 1);

			_mockPort.Verify();
		}


		[Test]
		public void InversionArrowsTest()
		{
			WaitForInitialQuery();

			SetupPortExpectations(_mockPort, "axis_queries.txt");

			var cmd = _plugin.ClickedJoystickButtonCommand;
			cmd.Execute(_plugin.XJoyPanel.Buttons[9]); //key 9 is the joystick

			Assert.IsFalse(_plugin.XJoyPanel.InvertedAxes[1]);

			_plugin.Joysticks[1].ViewInfo.IsInverted = true;
			Assert.IsTrue(_plugin.XJoyPanel.InvertedAxes[1]);

			_plugin.Joysticks[1].ViewInfo.IsInverted = false;
			Assert.IsFalse(_plugin.XJoyPanel.InvertedAxes[1]);

			_plugin.SelectedTab[3] = true;
			_plugin.SelectedTab[1] = false;

			Assert.IsFalse(_plugin.XJoyPanel.InvertedAxes[3]);

			_plugin.Joysticks[3].ViewInfo.IsInverted = true;

			Assert.IsTrue(_plugin.XJoyPanel.InvertedAxes[3]);
			_mockPort.Verify();
		}


		[Test]
		public void JoystickLockstepTargetingInfoTest()
		{
			WaitForInitialQuery();

			Assert.AreEqual(_plugin.Joysticks[1].TargetLockstepInfo, "Not Set-up");
			Assert.AreEqual(_plugin.Joysticks[2].TargetLockstepInfo, "Not Set-up");
			Assert.AreEqual(_plugin.Joysticks[3].TargetLockstepInfo, "Axes 1 , 2");
			Assert.AreEqual(_plugin.Joysticks[1].ViewInfo.LockstepIndex, "1");
			Assert.AreEqual(_plugin.Joysticks[2].ViewInfo.LockstepIndex, "1");
			Assert.AreEqual(_plugin.Joysticks[3].ViewInfo.LockstepIndex, "1");

			_mockPort.Verify();
		}


		[Test]
		public void JoystickTargetingInfoTest()
		{
			WaitForInitialQuery();

			Assert.AreEqual(_plugin.Joysticks[1].TargetDeviceName, "Not Connected");
			Assert.AreEqual(_plugin.Joysticks[1].TargetAxisDeviceName, "Not Connected");

			Assert.AreEqual(_plugin.Joysticks[2].TargetDeviceName, "X-LRM");
			Assert.AreEqual(_plugin.Joysticks[2].TargetAxisDeviceName, "Single Axis Device");

			Assert.AreEqual(_plugin.Joysticks[3].TargetDeviceName, "X-MCB2 Controller");
			Assert.AreEqual(_plugin.Joysticks[3].TargetAxisDeviceName, "X-MCB2 + ASR100BT3");

			_mockPort.Verify();
		}


		[Test]
		public void JoystickVirtualTargetingInfoTest()
		{
			WaitForInitialQuery();

			Assert.AreEqual(_plugin.Joysticks[1].ViewInfo.VirtualIndex, "1");
			Assert.AreEqual(_plugin.Joysticks[2].ViewInfo.VirtualIndex, "1");
			Assert.AreEqual(_plugin.Joysticks[3].ViewInfo.VirtualIndex, "1");
			Assert.AreEqual(_plugin.Joysticks[1].TargetVirtualAxisInfo, "Not Set-up");
			Assert.AreEqual(_plugin.Joysticks[2].TargetVirtualAxisInfo, "Not Set-up");
			Assert.AreEqual(_plugin.Joysticks[3].TargetVirtualAxisInfo, "Axes 1 , 2 (Angle : 28.5 degrees)");

			_mockPort.Verify();

			//TODO write a test to go to advanced tab and back while clearing/enabling vaxis.
		}


		[Test]
		public void LeftPanelRedJoystickTest()
		{
			var conversationList = _plugin.PortFacade.Conversations as List<Conversation>;
			_plugin.Conversation = conversationList.Find(x => x.Device.DeviceType.DeviceId == 51000);

			SetupPortExpectations(_mockPort, "axis_queries.txt");

			var cmd = _plugin.ClickedJoystickButtonCommand;
			cmd.Execute(_plugin.XJoyPanel.Buttons[9]); //key 9 is the joystick

			Assert.IsTrue(_plugin.XJoyPanel.Buttons[9].IsSelected);

			for (var i = 1; i < 9; i++)
			{
				Assert.IsFalse(_plugin.XJoyPanel.Buttons[i].IsSelected);
			}
		}


		[Test]
		public void LeftPanelRedKeyTest()
		{
			var conversationList = _plugin.PortFacade.Conversations as List<Conversation>;
			_plugin.Conversation = conversationList.Find(x => x.Device.DeviceType.DeviceId == 51000);

			SetupPortExpectations(_mockPort, "axis_queries.txt");

			var cmd = _plugin.ClickedJoystickButtonCommand;
			cmd.Execute(_plugin.XJoyPanel.Buttons[1]);

			Assert.IsTrue(_plugin.XJoyPanel.Buttons[1].IsSelected);

			for (var i = 2; i < 10; i++)
			{
				Assert.IsFalse(_plugin.XJoyPanel.Buttons[i].IsSelected);
			}
		}


		[Test]
		public void ReadJoystickTest()
		{
			WaitForInitialQuery();

			_mockPort.Verify();

			//wait for initial querying to finish; initial querying happens on a background worker thread
			Assert.IsFalse(_plugin.Joysticks[1].HasUnsavedChanges);
			Assert.AreEqual(_plugin.Joysticks[1].ViewInfo.MaxSpeed, "44880");
			_mockPort.Expect("/01 0 joystick 1 info");
			_mockPort.AddResponse("@01 0 OK IDLE -- 4 3 6900 1 1 51");
			var cmd = _plugin.ReadJoystickInfoCommand;
			cmd.Execute(_plugin.Joysticks[1]);
			Assert.AreEqual("6900", _plugin.Joysticks[1].ViewInfo.MaxSpeed);
			_mockPort.Verify();
			Assert.AreEqual(_plugin.Joysticks[1].ViewInfo.MaxSpeed, "6900");
		}


		[Test]
		public void ReadKeyTest()
		{
			WaitForInitialQuery();

			_mockPort.Expect("/01 0 33 key 1 1 info");
			_mockPort.AddResponse("@01 0 33 OK IDLE -- alerts disabled");
			_mockPort.AddResponse("#01 0 33 cmd 00 0 home");
			_mockPort.Expect("/01 0 34 key 1 2 info");
			_mockPort.AddResponse("@01 0 34 OK IDLE -- alerts disabled");
			_mockPort.AddResponse("#01 0 34 cmd 02 1 move rel 500");
			_mockPort.Expect("/01 0 35 key 1 3 info");
			_mockPort.AddResponse("@01 0 35 OK IDLE -- alerts disabled");
			_mockPort.Expect("/01 0 36 key 1 4 info");
			_mockPort.AddResponse("@01 0 36 OK IDLE -- alerts disabled");
			_mockPort.AddResponse("#01 0 36 cmd 02 1 move rel 500");

			var cmd = _plugin.ReadKeyInfoCommand;
			cmd.Execute(_plugin.Keys[1]);
			_mockPort.Verify();

			Assert.AreEqual(_plugin.Keys[1].KeyCommandGrids[4].KeyCommands[1].CommandName, "move rel 500");
			Assert.AreEqual(_plugin.Keys[1].KeyCommandGrids[4].KeyCommands[1].TargetAxis, "1");
			Assert.AreEqual(_plugin.Keys[1].KeyCommandGrids[4].KeyCommands[1].TargetDeviceNumber, "02");
			Assert.IsFalse(_plugin.Keys[1].HasUnsavedChanges);
		}


		[Test]
		public void StartupViewTest()
		{
			Assert.AreEqual("Select a Zaber joystick device in the upper panel.", _plugin.DisplayInformation);
			Assert.IsFalse(_plugin.ControlsEnabled);

			var conversationList = _plugin.PortFacade.Conversations as List<Conversation>;
			_plugin.Conversation =
				conversationList.Find(x => x.Device.DeviceType.DeviceId
									   == 51000); //find the x-joy conversation if it exists

			Assert.AreEqual("Click on a key or the joystick button in the left panel.", _plugin.DisplayInformation);
			Assert.IsTrue(_plugin.ControlsEnabled);
			Assert.IsNull(_plugin.KeyDisplay);
			Assert.AreEqual(_plugin.KeySelected, 0);
		}


		[Test]
		public void StartupVoidTest()
		{
			//Make sure no exceptions are thrown.
			_plugin.Conversation = null;
			Assert.IsNull(_plugin.Conversation);

			Assert.IsFalse(_plugin.ControlsEnabled);
		}


		[Test]
		public void SwitchConversationTest()
		{
			var conversationList = _plugin.PortFacade.Conversations as List<Conversation>;
			_plugin.Conversation = conversationList.Find(x => x.Device.DeviceType.DeviceId == 51000);

			_plugin.Conversation = conversationList.Find(x => x.Device.DeviceType.DeviceId != 51000);
			Assert.IsFalse(_plugin.ControlsEnabled);
			Assert.AreEqual("Select a Zaber joystick device in the upper panel.", _plugin.DisplayInformation);

			// Add expectations for all of the keys and axes.
			GenerateAllKeyExpectations(_mockPort, 33);
			SetupPortExpectations(_mockPort, "initial_query_footer.txt");

			_plugin.Conversation = conversationList.Find(x => x.Device.DeviceType.DeviceId == 51000);
			Assert.AreEqual("Click on a key or the joystick button in the left panel.", _plugin.DisplayInformation);
			Assert.IsTrue(_plugin.ControlsEnabled);
			Assert.IsNull(_plugin.KeyDisplay);
			Assert.AreEqual(_plugin.KeySelected, 0);
		}


		[Test]
		public void SwitchTabTest()
		{
			Assert.IsFalse(_plugin.ControlsEnabled);
			Assert.AreEqual("Select a Zaber joystick device in the upper panel.", _plugin.DisplayInformation);

			var conversationList = _plugin.PortFacade.Conversations as List<Conversation>;
			_plugin.Conversation = conversationList.Find(x => x.Device.DeviceType.DeviceId == 51000);
			Assert.AreEqual("Click on a key or the joystick button in the left panel.", _plugin.DisplayInformation);
			Assert.IsTrue(_plugin.ControlsEnabled);

			_plugin.OnTabHidden();
			Assert.IsFalse(_plugin.ControlsEnabled);

			GenerateAllKeyExpectations(_mockPort, 33);
			SetupPortExpectations(_mockPort, "initial_query_footer.txt");

			_plugin.OnTabShown();
			Assert.AreEqual("Click on a key or the joystick button in the left panel.", _plugin.DisplayInformation);
			Assert.IsTrue(_plugin.ControlsEnabled);

			_plugin.PortFacade.Close();
			Assert.IsFalse(_plugin.ControlsEnabled);
			_plugin.DisplayInformation = "Connect an X-JOY3 joystick and open the port to start using this plugin.";
		}


		[Test]
		public void WriteJoystickTest()
		{
			WaitForInitialQuery();

			_plugin.Joysticks[3].ViewInfo.TargetDeviceNumber = "2";
			_plugin.Joysticks[3].ViewInfo.TargetAxis = "2";
			_plugin.Joysticks[3].ViewInfo.SpeedSelected = "Linear";
			_plugin.Joysticks[3].ViewInfo.MaxSpeed = "55555";
			_plugin.Joysticks[3].ViewInfo.IsInverted = true;
			_plugin.Joysticks[3].ViewInfo.Resolution = "22";
			Assert.IsTrue(_plugin.Joysticks[3].HasUnsavedChanges);

			SetupPortExpectations(_mockPort, "write_joystick_responses_expectation.txt");
			var cmd = _plugin.WriteJoystickInfoCommand;
			cmd.Execute(_plugin.Joysticks[3]);
			Assert.IsFalse(_plugin.Joysticks[3].HasUnsavedChanges);
			_mockPort.Verify();
		}


		[Test]
		public void WriteKeyTest()
		{
			WaitForInitialQuery();

			_plugin.Keys[1].KeyCommandGrids[1].KeyCommands[0].CommandName = "move rel 500";
			_plugin.Keys[1].KeyCommandGrids[1].KeyCommands[0].TargetAxis = "1";
			_plugin.Keys[1].KeyCommandGrids[1].KeyCommands[0].TargetDeviceNumber = "5";
			var cmd = _plugin.Keys[1].KeyCommandGrids[1].AddRowCommand;
			cmd.Execute(_plugin.Keys[1].KeyCommandGrids[1].KeyCommands[0]);
			Assert.AreEqual(_plugin.Keys[1].KeyCommandGrids[1].KeyCommands[2].CommandName, "move rel 500");
			Assert.AreEqual(_plugin.Keys[1].KeyCommandGrids[1].KeyCommands[2].TargetAxis, "1");
			Assert.AreEqual(_plugin.Keys[1].KeyCommandGrids[1].KeyCommands[2].TargetDeviceNumber, "5");
			Assert.IsTrue(_plugin.Keys[1].HasUnsavedChanges);

			SetupPortExpectations(_mockPort, "write_key_responses_expectations.txt");
			var cmd2 = _plugin.WriteKeyInfoCommand;
			cmd2.Execute(_plugin.Keys[1]);

			Thread.Sleep(2500);
			_mockPort.Verify();

			Assert.IsFalse(_plugin.Keys[1].HasUnsavedChanges);
		}


		[Test]
		public void WritingInvalidJoystickInfoTest()
		{
			//TODO
		}


		private void SetupPort()
		{
			_mockPort = new MockPort();
			_mockPort.IsAsciiMode = true;

			var defaultDeviceType = new DeviceType { Commands = new List<CommandInfo>() };

			_portFacade = new ZaberPortFacade
			{
				DefaultDeviceType = defaultDeviceType,
				Port = _mockPort,
				QueryTimeout = 100
			};

			var joyCommands = new List<CommandInfo> { new CommandInfo { Command = Command.SetAxisDeviceNumber } };

			joyCommands.AddRange(
				AsciiCommandBuilder.ReadCommandDefinitionsFromJsonFile(AsciiCommandBuilder.JOYSTICK_COMMAND_FILE));
			joyCommands.AddRange(
				AsciiCommandBuilder.ReadCommandDefinitionsFromJsonFile(AsciiCommandBuilder.KEY_COMMAND_FILE));

			var XJOY3 = new DeviceType
			{
				DeviceId = 51000,
				Name = "X-JOY3 Joystick",
				MotionType = MotionType.Other,
				Commands = joyCommands,
				FirmwareVersion = new FirmwareVersion(6, 14) // Because the expectations include message IDs.
			};

			var ASR100BT3 = new DeviceType
			{
				PeripheralId = 44012,
				MotionType = MotionType.Linear,
				Name = "X-MCB2 + ASR100BT3"
			};

			var ASR120BT3 = new DeviceType
			{
				PeripheralId = 44022,
				MotionType = MotionType.Linear,
				Name = "X-MCB2 + ASR120BT3"
			};

			var XLRM = new DeviceType
			{
				DeviceId = 50004,
				MotionType = MotionType.Linear,
				Name = "X-LRM"
			};

			var XMCB2 = new DeviceType
			{
				DeviceId = 30222,
				PeripheralMap = new Dictionary<int, DeviceType>
				{
					{ 44012, ASR100BT3 },
					{ 44022, ASR120BT3 }
				},
				Name = "X-MCB2 Controller",
				MotionType = MotionType.Other
			};

			_portFacade.AddDeviceType(XMCB2);
			_portFacade.AddDeviceType(XLRM);
			_portFacade.AddDeviceType(XJOY3);

			ZaberPortFacadeTest.SetAsciiExpectations(_portFacade, _mockPort, XJOY3, XMCB2, XLRM);
		}


		private void WaitForInitialQuery()
		{
			var conversationList = _plugin.PortFacade.Conversations as List<Conversation>;
			_plugin.Conversation = conversationList.Find(x => x.Device.DeviceType.DeviceId == 51000);
			Assert.AreEqual("Axes 1 , 2", _plugin.Joysticks[3].TargetLockstepInfo);
			_mockPort.Verify();
		}


		/// <summary>
		///     Add expectations and responses from a port after reading a text file that is representative
		///     of what appears on the log on Zaber Console after the initial query of a joystick.
		///     Format of the filename is "\\filename.txt". Put the txt file in the ZaberConsoleGuiTests/XJoyPluginTests folder and
		///     add
		///     it as a content resource.
		/// </summary>
		internal static void SetupPortExpectations(MockPort mockPort, string filename)
		{
			var lowerDirectory = TestContext.CurrentContext.TestDirectory;
			var path = Path.Combine(lowerDirectory, "PluginTests", "Joystick", filename);
			mockPort.ReadExpectationsFromFile(path);
		}


		private static void GenerateAllKeyExpectations(MockPort aPort, int aFirstMessageId = 1)
		{
			var nextId = aFirstMessageId;
			for (var key = 1; key <= 8; key++)
			{
				for (var action = 1; action <= 4; action++)
				{
					aPort.ReadExpectationsFromString($"/01 0 {nextId} key {key} {action} info");
					aPort.AddResponse($"@01 0 {nextId} OK IDLE-- alerts disabled");
					nextId++;
					if (nextId > 99)
					{
						nextId = 1;
					}
				}
			}
		}


		private JoystickPluginVM _plugin;
		private MockPort _mockPort;
		private ZaberPortFacade _portFacade;
	}
}

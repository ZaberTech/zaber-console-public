﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using Zaber;
using Zaber.Testing;
using ZaberConsole;
using ZaberConsole.Plugins.Joystick;
using ZaberConsole.Plugins.Joystick.Key;
using ZaberTest;

/// <summary>
/// High level tests for the JoystickPluginVM in Binary mode.
/// </summary>
namespace ZaberConsoleGUITests.XJoyPluginTests
{
	[TestFixture]
	[Category("JoystickPluginBinary")]
	[SetCulture("en-US")]
	public class JoystickPluginTestsBinary
	{
		[SetUp]
		public void SetUp()
		{
			DispatcherStack.Push(new ImmediateDispatcher());
			BackgroundWorkerStack.Push(typeof(ImmediateForegroundWorker));

			SetupPort();
			_plugin = new JoystickPluginVM();
			_plugin.PortFacade = _portFacade;
			_portFacade.IsInvalidateEnabled = true;
			_plugin.OnTabShown();
			_plugin.PortFacade.Open("COM3");
			SetupPortExpectations();
		}


		[TearDown]
		public void Teardown()
		{
			_plugin = null;
			BackgroundWorkerStack.Pop();
			DispatcherStack.Pop();
		}


		[Test]
		public void ClearKeysWindowBinaryTest_AllKeys()
		{
			WaitForInitialQuery();
			var clearKeysWindow = new ClearKeysWindowVM(_plugin, Protocol.Binary);

			//clearing keys 1 and 2.
			for (var j = 1; j < 9; j++)
			{
				for (var i = 1; i < 5; i++)
				{
					_mockPort.Expect(1, Command.LoadEventInstruction, (j * 10) + i);
					_mockPort.AddResponse(1, Command.LoadEventInstruction, (j * 10) + i);
					_mockPort.Expect(255, Command.Error, 0);
					_mockPort.AddResponse(255, Command.Error, 0);
				}
			}

			var selectCmd = clearKeysWindow.SelectAllCommand;
			selectCmd.Execute(true);

			var clearCmd = clearKeysWindow.ClearCommand;
			clearCmd.Execute(null);

			var counter = 0;
			while (counter < 4000)
			{
				Thread.Sleep(1000);
				counter += 1000;
			}

			for (var i = 1; i < 9; i++)
			{
				Assert.IsFalse(clearKeysWindow.KeysToDelete[i]);
				Assert.IsTrue(_plugin.Keys[i].KeyCommandGrids[1].IsEmptyCommand);
				Assert.IsTrue(_plugin.Keys[i].KeyCommandGrids[2].IsEmptyCommand);
				Assert.IsTrue(_plugin.Keys[i].KeyCommandGrids[3].IsEmptyCommand);
				Assert.IsTrue(_plugin.Keys[i].KeyCommandGrids[4].IsEmptyCommand);
			}
		}


		[Test]
		public void ClearKeysWindowBinaryTest_TwoKeys()
		{
			WaitForInitialQuery();
			var clearKeysWindow = new ClearKeysWindowVM(_plugin, Protocol.Binary);

			//clearing keys 1 and 2.
			for (var j = 1; j < 3; j++)
			{
				for (var i = 1; i < 5; i++)
				{
					_mockPort.Expect(1, Command.LoadEventInstruction, (j * 10) + i);
					_mockPort.AddResponse(1, Command.LoadEventInstruction, (j * 10) + i);
					_mockPort.Expect(255, Command.Error, 0);
					_mockPort.AddResponse(255, Command.Error, 0);
				}
			}

			clearKeysWindow.KeysToDelete[1] = true;
			clearKeysWindow.KeysToDelete[2] = true;

			var cmd = clearKeysWindow.ClearCommand;
			cmd.Execute(null);

			var counter = 0;
			while (counter < 4000)
			{
				Thread.Sleep(1000);
				counter += 1000;
			}

			Assert.IsFalse(clearKeysWindow.KeysToDelete[1]);
			Assert.IsFalse(clearKeysWindow.KeysToDelete[2]);

			Assert.IsTrue(_plugin.Keys[1].KeyCommandGrids[1].IsEmptyCommand);
			Assert.IsTrue(_plugin.Keys[1].KeyCommandGrids[2].IsEmptyCommand);
			Assert.IsTrue(_plugin.Keys[1].KeyCommandGrids[3].IsEmptyCommand);
			Assert.IsTrue(_plugin.Keys[1].KeyCommandGrids[4].IsEmptyCommand);
			Assert.IsTrue(_plugin.Keys[2].KeyCommandGrids[1].IsEmptyCommand);
			Assert.IsTrue(_plugin.Keys[2].KeyCommandGrids[2].IsEmptyCommand);
			Assert.IsTrue(_plugin.Keys[2].KeyCommandGrids[3].IsEmptyCommand);
			Assert.IsTrue(_plugin.Keys[2].KeyCommandGrids[4].IsEmptyCommand);
		}


		[Test]
		public void InitialQueryAccurateKeyInformationTestBinary()
		{
			WaitForInitialQuery();

			var firstInfo = _plugin.Keys[1].KeyCommandGrids[1].BinaryInfo;
			var secondInfo = _plugin.Keys[8].KeyCommandGrids[1].BinaryInfo;
			var errorInfo = _plugin.Keys[8].KeyCommandGrids[4].BinaryInfo;
			var randomInfo = _plugin.Keys[5].KeyCommandGrids[3].BinaryInfo;

			var firstInfoExpect = new KeyCommandInfo
			{
				TargetDeviceNumber = "1",
				BinaryCommand = "25",
				BinaryData = "1"
			};

			var secondInfoExpect = new KeyCommandInfo
			{
				TargetDeviceNumber = "1",
				BinaryCommand = "25",
				BinaryData = "8"
			};

			var errorInfoExpect = new KeyCommandInfo
			{
				TargetDeviceNumber = "255",
				BinaryCommand = "255",
				BinaryData = "0"
			};

			var randomInfoExpect = new KeyCommandInfo
			{
				TargetDeviceNumber = "1",
				BinaryCommand = "29",
				BinaryData = "44880"
			};

			Assert.That(firstInfo, Is.EqualTo(firstInfoExpect).Using(new KeyInfoComparer()));
			Assert.That(secondInfo, Is.EqualTo(secondInfoExpect).Using(new KeyInfoComparer()));
			Assert.That(errorInfo, Is.EqualTo(errorInfoExpect).Using(new KeyInfoComparer()));
			Assert.That(randomInfo, Is.EqualTo(randomInfoExpect).Using(new KeyInfoComparer()));
		}


		[Test]
		public void InitialQueryStateTestBinary()
		{
			WaitForInitialQuery();

			Assert.AreEqual(0, _plugin.InitialCounter);
			Assert.AreEqual(0, _plugin.BinaryCounter);
			Assert.AreEqual(0, _plugin.PacketsReceivedBeforeFinish);
		}


		[Test]
		public void InitialQueryViewTestBinary()
		{
			WaitForInitialQuery();

			Assert.IsTrue(_plugin.ControlsEnabled);
			Assert.IsFalse(_plugin.KeyVisible);
			Assert.IsFalse(_plugin.JoystickVisible);
			Assert.AreEqual("Click on a key or the joystick button in the left panel.", _plugin.DisplayInformation);
		}


		[Test]
		public void KeyReadTestBinary()
		{
			WaitForInitialQuery();

			_mockPort.Expect(1, Command.ReturnEventInstruction, 11);
			_mockPort.AddResponse(5, Command.MoveRelative, 500);
			_mockPort.Expect(1, Command.ReturnEventInstruction, 12);
			_mockPort.AddResponse(1, Command.MoveRelative, 100);
			_mockPort.Expect(1, Command.ReturnEventInstruction, 13);
			_mockPort.AddResponse(255, Command.Error, 0);
			_mockPort.Expect(1, Command.ReturnEventInstruction, 14);
			_mockPort.AddResponse(1, Command.ReturnEventInstruction, 11);

			var cmd = _plugin.ReadKeyInfoCommand;
			cmd.Execute(_plugin.Keys[1]);

			var counter = 0;
			while ((_plugin.Keys[1].KeyCommandGrids[1].BinaryInfo.BinaryData != "500") && (counter < 7000))
			{
				Thread.Sleep(1000);
				counter += 1000;
			}

			Thread.Sleep(200);
			_mockPort.Verify();

			Assert.IsFalse(_plugin.Keys[1].HasUnsavedChanges);

			Assert.AreEqual("500", _plugin.Keys[1].KeyCommandGrids[1].BinaryInfo.BinaryData);
			Assert.AreEqual("21", _plugin.Keys[1].KeyCommandGrids[1].BinaryInfo.BinaryCommand);
			Assert.AreEqual("5", _plugin.Keys[1].KeyCommandGrids[1].BinaryInfo.TargetDeviceNumber);

			Assert.AreEqual("100", _plugin.Keys[1].KeyCommandGrids[2].BinaryInfo.BinaryData);
			Assert.AreEqual("21", _plugin.Keys[1].KeyCommandGrids[2].BinaryInfo.BinaryCommand);
			Assert.AreEqual("1", _plugin.Keys[1].KeyCommandGrids[2].BinaryInfo.TargetDeviceNumber);

			Assert.AreEqual("0", _plugin.Keys[1].KeyCommandGrids[3].BinaryInfo.BinaryData);
			Assert.AreEqual("255", _plugin.Keys[1].KeyCommandGrids[3].BinaryInfo.BinaryCommand);
			Assert.AreEqual("255", _plugin.Keys[1].KeyCommandGrids[3].BinaryInfo.TargetDeviceNumber);

			Assert.AreEqual("11", _plugin.Keys[1].KeyCommandGrids[4].BinaryInfo.BinaryData);
			Assert.AreEqual("31", _plugin.Keys[1].KeyCommandGrids[4].BinaryInfo.BinaryCommand);
			Assert.AreEqual("1", _plugin.Keys[1].KeyCommandGrids[4].BinaryInfo.TargetDeviceNumber);

			Assert.AreEqual(0, _plugin.InitialCounter);
			Assert.AreEqual(0, _plugin.BinaryCounter);
			Assert.AreEqual(0, _plugin.PacketsReceivedBeforeFinish);
		}


		[Test]
		public void WriteKeyTestBinary()
		{
			WaitForInitialQuery();

			//preparing expectations for writing
			_mockPort.Expect(1, Command.LoadEventInstruction, 11);
			_mockPort.AddResponse(1, Command.LoadEventInstruction, 11);
			_mockPort.Expect(5, Command.MoveRelative, 500);
			_mockPort.AddResponse(5, Command.MoveRelative, 500);

			_mockPort.Expect(1, Command.LoadEventInstruction, 12);
			_mockPort.AddResponse(1, Command.LoadEventInstruction, 12);
			_mockPort.Expect(1, Command.MoveRelative, 100);
			_mockPort.AddResponse(1, Command.MoveRelative, 100);

			_mockPort.Expect(1, Command.LoadEventInstruction, 13);
			_mockPort.AddResponse(1, Command.LoadEventInstruction, 13);
			_mockPort.Expect(255, Command.Error, 0);
			_mockPort.AddResponse(255, Command.Error, 0);

			_mockPort.Expect(1, Command.LoadEventInstruction, 14);
			_mockPort.AddResponse(1, Command.LoadEventInstruction, 14);
			_mockPort.Expect(1, Command.ReturnEventInstruction, 11);
			_mockPort.AddResponse(1, Command.ReturnEventInstruction, 11);

			var binaryInfo4 = _plugin.Keys[1].KeyCommandGrids[4].BinaryInfo;
			var binaryInfo3 = _plugin.Keys[1].KeyCommandGrids[3].BinaryInfo;
			var binaryInfo2 = _plugin.Keys[1].KeyCommandGrids[2].BinaryInfo;
			var binaryInfo1 = _plugin.Keys[1].KeyCommandGrids[1].BinaryInfo;

			binaryInfo1.BinaryData = "500";
			binaryInfo1.BinaryCommand = "21";
			binaryInfo1.TargetDeviceNumber = "5";

			binaryInfo2.BinaryData = "100";
			binaryInfo2.BinaryCommand = "21";
			binaryInfo2.TargetDeviceNumber = "1";

			binaryInfo3.BinaryData = "0";
			binaryInfo3.BinaryCommand = "255";
			binaryInfo3.TargetDeviceNumber = "255";

			binaryInfo4.BinaryData = "11";
			binaryInfo4.BinaryCommand = "31";
			binaryInfo4.TargetDeviceNumber = "1";

			Assert.IsTrue(_plugin.Keys[1].HasUnsavedChanges);

			var cmd = _plugin.WriteKeyInfoCommand;
			cmd.Execute(_plugin.Keys[1]);

			var counter = 0;
			while (_plugin.Keys[1].HasUnsavedChanges && (counter < 4000))
			{
				Thread.Sleep(1000);
				counter += 1000;
			}

			_mockPort.Verify();

			Assert.IsFalse(_plugin.Keys[1].HasUnsavedChanges);

			Assert.AreEqual("500", binaryInfo1.BinaryData);
			Assert.AreEqual("21", binaryInfo1.BinaryCommand);
			Assert.AreEqual("5", binaryInfo1.TargetDeviceNumber);

			Assert.AreEqual("100", binaryInfo2.BinaryData);
			Assert.AreEqual("21", binaryInfo2.BinaryCommand);
			Assert.AreEqual("1", binaryInfo2.TargetDeviceNumber);

			Assert.AreEqual("0", binaryInfo3.BinaryData);
			Assert.AreEqual("255", binaryInfo3.BinaryCommand);
			Assert.AreEqual("255", binaryInfo3.TargetDeviceNumber);

			Assert.AreEqual("11", binaryInfo4.BinaryData);
			Assert.AreEqual("31", binaryInfo4.BinaryCommand);
			Assert.AreEqual("1", binaryInfo4.TargetDeviceNumber);

			Assert.AreEqual(0, _plugin.InitialCounter);
			Assert.AreEqual(0, _plugin.BinaryCounter);
			Assert.AreEqual(0, _plugin.PacketsReceivedBeforeFinish);

			//preparing expectations for writing round 2, making sure that
			//2 consecutive writes doesnt' break anything.
			_mockPort.Expect(1, Command.LoadEventInstruction, 11);
			_mockPort.AddResponse(1, Command.LoadEventInstruction, 11);
			_mockPort.Expect(5, Command.MoveRelative, 500);
			_mockPort.AddResponse(5, Command.MoveRelative, 500);

			_mockPort.Expect(1, Command.LoadEventInstruction, 12);
			_mockPort.AddResponse(1, Command.LoadEventInstruction, 12);
			_mockPort.Expect(1, Command.MoveRelative, 100);
			_mockPort.AddResponse(1, Command.MoveRelative, 100);

			_mockPort.Expect(1, Command.LoadEventInstruction, 13);
			_mockPort.AddResponse(1, Command.LoadEventInstruction, 13);
			_mockPort.Expect(255, Command.Error, 0);
			_mockPort.AddResponse(255, Command.Error, 0);

			_mockPort.Expect(1, Command.LoadEventInstruction, 14);
			_mockPort.AddResponse(1, Command.LoadEventInstruction, 14);
			_mockPort.Expect(1, Command.ReturnEventInstruction, 11);
			_mockPort.AddResponse(1, Command.ReturnEventInstruction, 11);

			var cmd1 = _plugin.WriteKeyInfoCommand;
			cmd.Execute(_plugin.Keys[1]);

			Thread.Sleep(2000);

			_mockPort.Verify();

			Assert.IsFalse(_plugin.Keys[1].HasUnsavedChanges);

			Assert.AreEqual("500", binaryInfo1.BinaryData);
			Assert.AreEqual("21", binaryInfo1.BinaryCommand);
			Assert.AreEqual("5", binaryInfo1.TargetDeviceNumber);

			Assert.AreEqual("100", binaryInfo2.BinaryData);
			Assert.AreEqual("21", binaryInfo2.BinaryCommand);
			Assert.AreEqual("1", binaryInfo2.TargetDeviceNumber);

			Assert.AreEqual("0", binaryInfo3.BinaryData);
			Assert.AreEqual("255", binaryInfo3.BinaryCommand);
			Assert.AreEqual("255", binaryInfo3.TargetDeviceNumber);

			Assert.AreEqual("11", binaryInfo4.BinaryData);
			Assert.AreEqual("31", binaryInfo4.BinaryCommand);
			Assert.AreEqual("1", binaryInfo4.TargetDeviceNumber);

			Assert.AreEqual(0, _plugin.InitialCounter);
			Assert.AreEqual(0, _plugin.BinaryCounter);
			Assert.AreEqual(0, _plugin.PacketsReceivedBeforeFinish);
		}


		//TODO FIX : This fails for some reason, Joysticks[2].ViewInfo is null.  [Test]
		public void JoystickInitialQueryAccuracyTest()
		{
			WaitForInitialQuery();

			Assert.AreEqual("N/A", _plugin.Joysticks[1].ViewInfo.Resolution);

			Assert.AreEqual("44880", _plugin.Joysticks[1].ViewInfo.MaxSpeed);
			Assert.AreEqual("44880", _plugin.Joysticks[2].ViewInfo.MaxSpeed);
			Assert.AreEqual("44880", _plugin.Joysticks[3].ViewInfo.MaxSpeed);

			Assert.AreEqual("2", _plugin.Joysticks[1].ViewInfo.TargetDeviceNumber);
			Assert.AreEqual("2", _plugin.Joysticks[2].ViewInfo.TargetDeviceNumber);
			Assert.AreEqual("2", _plugin.Joysticks[3].ViewInfo.TargetDeviceNumber);

			Assert.AreEqual(false, _plugin.Joysticks[1].ViewInfo.IsInverted);
			Assert.AreEqual(false, _plugin.Joysticks[2].ViewInfo.IsInverted);
			Assert.AreEqual(false, _plugin.Joysticks[3].ViewInfo.IsInverted);

			Assert.AreEqual("Squared (Default)", _plugin.Joysticks[1].ViewInfo.SpeedSelected);
			Assert.AreEqual("Squared (Default)", _plugin.Joysticks[2].ViewInfo.SpeedSelected);
			Assert.AreEqual("Squared (Default)", _plugin.Joysticks[3].ViewInfo.SpeedSelected);

			Assert.IsFalse(_plugin.Joysticks[1].HasUnsavedChanges);
		}


		private class OpenExpectation
		{
			#region -- Public Methods & Properties --

			public OpenExpectation()
			{
				DeviceId = 65535;
				MicrostepResolution = ZaberDevice.DefaultMicrostepResolution;
				IsMicrostepResolutionSupported = true;
				FirmwareVersion = 0;
			}


			public byte DeviceNumber { get; set; }

			public int DeviceId { get; set; }

			public int PeripheralId { get; set; }

			public int AliasNumber { get; set; }

			public DeviceModes DeviceModes { get; set; }

			public int MicrostepResolution { get; }

			public bool IsMicrostepResolutionSupported { get; }

			public int FirmwareVersion { get; set; }

			#endregion
		}


		/// <summary>
		///     Sets up the plugin to query the port and wait until the querying is finished.
		/// </summary>
		private void WaitForInitialQuery()
		{
			var conversationList = _plugin.PortFacade.Conversations as List<Conversation>;
			_plugin.Conversation = conversationList.Find(x => x.Device.DeviceType.DeviceId == 51000);

			_mockPort.Verify(); //Verifying makes tests fail for some reason.
		}


		/// <summary>
		///     Sets up the port to detect a few mock devices.
		///     The mock devices include:
		///     A joystick,
		///     2 peripherals,
		///     A 2 axis controller,
		///     and a linear stage.
		/// </summary>
		private void SetupPort()
		{
			_mockPort = new MockPort();
			_mockPort.IsAsciiMode = false;

			var defaultDeviceType = new DeviceType { Commands = new List<CommandInfo>() };

			_portFacade = new ZaberPortFacade
			{
				DefaultDeviceType = defaultDeviceType,
				Port = _mockPort,
				QueryTimeout = 100
			};

			var XJOY3 = new DeviceType
			{
				DeviceId = 51000,
				Name = "X-JOY3 Joystick",
				MotionType = MotionType.Other,
				FirmwareVersion = new FirmwareVersion(500),
				Commands = new List<CommandInfo> { new CommandInfo { Command = Command.SetAxisDeviceNumber } }
			};

			var ASR100BT3 = new DeviceType
			{
				PeripheralId = 44012,
				MotionType = MotionType.Linear,
				FirmwareVersion = new FirmwareVersion(500),
				Name = "X-MCB2 + ASR100BT3"
			};

			var ASR120BT3 = new DeviceType
			{
				PeripheralId = 44022,
				MotionType = MotionType.Linear,
				FirmwareVersion = new FirmwareVersion(500),
				Name = "X-MCB2 + ASR120BT3"
			};

			var XLRM = new DeviceType
			{
				DeviceId = 50004,
				MotionType = MotionType.Linear,
				FirmwareVersion = new FirmwareVersion(500),
				Name = "X-LRM"
			};

			var XMCB2 = new DeviceType
			{
				DeviceId = 30222,
				PeripheralMap = new Dictionary<int, DeviceType>
				{
					{ 44012, ASR100BT3 },
					{ 44022, ASR120BT3 }
				},
				Name = "X-MCB2 Controller",
				FirmwareVersion = new FirmwareVersion(500),
				MotionType = MotionType.Other
			};

			var TJOY3 = new DeviceType
			{
				DeviceId = 3003,
				Name = "T-JOY3 Joystick",
				MotionType = MotionType.Other,
				FirmwareVersion = new FirmwareVersion(500),
				Commands = new List<CommandInfo> { new CommandInfo { Command = Command.SetAxisDeviceNumber } }
			};

			_portFacade.AddDeviceType(XMCB2);
			_portFacade.AddDeviceType(XLRM);
			_portFacade.AddDeviceType(XJOY3);
			_portFacade.AddDeviceType(ASR100BT3);
			_portFacade.AddDeviceType(ASR120BT3);

			SetBinaryExpectations(1,
								  51000,
								  0,
								  0,
								  2,
								  30222,
								  0,
								  0,
								  3,
								  50004,
								  0,
								  0,
								  4,
								  3003,
								  0,
								  0);
		}


		/// <summary>
		///     Initial joystick settings for all 3 axes are the same.
		/// </summary>
		private void SetupPortExpectations()
		{
			for (var i = 1; i < 4; i++)
			{
				_mockPort.Expect(1, Command.SetActiveAxis, i);
				_mockPort.AddResponse(1, Command.SetActiveAxis, i);

				_mockPort.Expect(1, Command.ReturnSetting, 26);
				_mockPort.AddResponse(1, Command.SetAxisDeviceNumber, 2);

				_mockPort.Expect(1, Command.ReturnSetting, 28);
				_mockPort.AddResponse(1, Command.SetAxisVelocityProfile, 2);

				_mockPort.Expect(1, Command.ReturnSetting, 29);
				_mockPort.AddResponse(1, Command.SetAxisVelocityScale, 44880);

				_mockPort.Expect(1, Command.ReturnSetting, 27);
				_mockPort.AddResponse(1, Command.SetAxisInversion, 1);
			}

			for (var i = 1; i < 9; i++)
			{
				_mockPort.Expect(1, Command.ReturnEventInstruction, (i * 10) + 1);
				_mockPort.AddResponse(1, Command.SetActiveAxis, i);

				_mockPort.Expect(1, Command.ReturnEventInstruction, (i * 10) + 2);
				_mockPort.AddResponse(1, Command.SetAxisVelocityScale, 10000);

				_mockPort.Expect(1, Command.ReturnEventInstruction, (i * 10) + 3);
				_mockPort.AddResponse(1, Command.SetAxisVelocityScale, 44880);

				_mockPort.Expect(1, Command.ReturnEventInstruction, (i * 10) + 4);
				_mockPort.AddResponse(255, Command.Error, 0);
			}
		}


		/// <summary>
		///     Prepare the mock port for the open sequence with message ids disabled.
		/// </summary>
		/// <param name="responses">
		///     Response contents to return during Open(). The contents are:
		///     deviceNum, deviceId, alias, mode, deviceNum, deviceId, alias, mode, ...
		/// </param>
		private void SetBinaryExpectations(params int[] responses)
		{
			var expectationCount = responses.Length / 4;
			var expectations = new OpenExpectation[expectationCount];
			for (var i = 0; i < expectationCount; i++)
			{
				expectations[i] = new OpenExpectation
				{
					DeviceNumber = (byte) responses[4 * i],
					DeviceId = responses[(4 * i) + 1],
					AliasNumber = responses[(4 * i) + 2],
					DeviceModes = (DeviceModes) responses[(4 * i) + 3],
					FirmwareVersion = 500
				};
			}

			Assert.That(_portFacade.Port.IsAsciiMode,
						Is.False,
						"SetBinaryExpectations doesn't support ASCII mode anymore.");
			SetBinaryExpectations(expectations);
		}


		/// <summary>
		///     Prepare the mock port for the open sequence with message ids disabled.
		/// </summary>
		/// <param name="responses">Response contents to return during Open().</param>
		private void SetBinaryExpectations(params OpenExpectation[] responses)
		{
			_mockPort.Expect(0, Command.ReturnDeviceID, 0);
			foreach (var response in responses)
			{
				_mockPort.AddResponse(response.DeviceNumber,
									  Command.ReturnDeviceID,
									  response.DeviceId);
			}

			// sort the offsets because the follow-up requests are sent
			// in sorted order by device number.
			Array.Sort(responses,
					   (r1, r2) => r1.DeviceNumber.CompareTo(r2.DeviceNumber));

			// Firmware version.
			foreach (var response in responses)
			{
				var deviceNumber = response.DeviceNumber;
				_mockPort.Expect(deviceNumber,
								 Command.ReturnFirmwareVersion,
								 0);
				_mockPort.AddResponse(deviceNumber,
									  Command.ReturnFirmwareVersion,
									  response.FirmwareVersion);
			}

			// Alias number.
			foreach (var response in responses)
			{
				if (response.FirmwareVersion >= 500)
				{
					var deviceNumber = response.DeviceNumber;
					_mockPort.Expect(deviceNumber,
									 Command.ReturnSetting,
									 (int) Command.SetAliasNumber);
					_mockPort.AddResponse(deviceNumber,
										  Command.SetAliasNumber,
										  response.AliasNumber);
				}
			}

			// Microstep resolution.
			foreach (var response in responses)
			{
				if (response.FirmwareVersion < 500)
				{
					continue; // FW 2.93 does not reply.
				}

				var deviceNumber = response.DeviceNumber;
				_mockPort.Expect(deviceNumber,
								 Command.ReturnSetting,
								 (int) Command.SetMicrostepResolution);
				if (response.IsMicrostepResolutionSupported)
				{
					_mockPort.AddResponse(deviceNumber,
										  Command.SetMicrostepResolution,
										  response.MicrostepResolution);
				}
				else
				{
					_mockPort.AddResponse(deviceNumber,
										  Command.Error,
										  (int) ZaberError.SettingInvalid);
				}
			}

			// Peripheral ID.
			foreach (var response in responses.Where(response =>
														 response.FirmwareVersion >= 603))
			{
				_mockPort.Expect(response.DeviceNumber,
								 Command.ReturnSetting,
								 (int) Command.SetPeripheralID);
				_mockPort.AddResponse(response.DeviceNumber,
									  Command.SetPeripheralID,
									  response.PeripheralId);
			}

			// Device Mode.
			foreach (var response in responses)
			{
				var deviceNumber = response.DeviceNumber;
				if (response.FirmwareVersion < 600)
				{
					_mockPort.Expect(deviceNumber,
									 Command.ReturnSetting,
									 (int) Command.SetDeviceMode);
					_mockPort.AddResponse(deviceNumber,
										  Command.SetDeviceMode,
										  (int) response.DeviceModes);
				}
				else
				{
					_mockPort.Expect(deviceNumber,
									 Command.ReturnSetting,
									 (int) Command.SetMessageIDMode);
					_mockPort.AddResponse(deviceNumber,
										  Command.SetMessageIDMode,
										  (response.DeviceModes
									   & DeviceModes.EnableMessageIdsMode)
									  != 0
											  ? 1
											  : 0);
				}
			}
		}


		public JoystickPluginVM _plugin;
		private MockPort _mockPort;
		private ZaberPortFacade _portFacade;
	}
}

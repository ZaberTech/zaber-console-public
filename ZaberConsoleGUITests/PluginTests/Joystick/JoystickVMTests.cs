﻿using System.Collections.Generic;
using NUnit.Framework;
using Zaber;
using ZaberConsole;
using ZaberConsole.Plugins.Joystick;
using ZaberConsole.Plugins.Joystick.Stick;

/// <summary>
/// Tests for the JoystickVM in ASCII mode.
/// </summary>

namespace ZaberConsoleGUITests.XJoyPluginTests
{
	[TestFixture]
	[Category("JoystickPlugin")]
	[SetCulture("en-US")]
	public class JoystickVMTests
	{
		[SetUp]
		public void SetUp()
		{
			var test = new JoystickPluginVM();
			var allDevices = new ZaberDevice { DeviceType = new DeviceType() };
			var device1 = new ZaberDevice { DeviceType = new DeviceType() };
			var device2 = new ZaberDevice { DeviceType = new DeviceType() };
			var axis1 = new ZaberDevice { DeviceType = new DeviceType() };
			var axis2 = new ZaberDevice { DeviceType = new DeviceType() };

			allDevices.DeviceNumber = 0;
			allDevices.DeviceType.Name = "All Devices";
			device1.DeviceType.Name = "Multi Axis Device";
			device2.DeviceType.Name = "Single Axis Device";
			device1.DeviceNumber = 1;
			device2.DeviceNumber = 2;

			axis1.DeviceType.Name = "Axis 1";
			axis2.DeviceType.Name = "Axis 2";
			axis1.AxisNumber = 1;
			axis2.AxisNumber = 2;

			device1.AddAxis(axis1);
			device1.AddAxis(axis2);

			device2.AddAxis(axis1);

			_connectedDevices.Add(device1);
			_connectedDevices.Add(device2);
			_connectedDevices.Add(allDevices);

			_joystick = new JoystickVM("1", Protocol.ASCII);
			_joystick.ConnectedDevices = _connectedDevices;
			_joystick.VirtualAxesCollection = _thisVirtualAxesCollection;
			_joystick.LockstepCollection = _thisLockstepCollection;

			_joystick.ParseJoystickInfo("OK IDLE -- 2 1 10000 2 1 50");
		}


		[TearDown]
		public void TearDown() => _joystick = null;


		[Test]
		public void LockstepHasUnsavedChangesTest()
		{
			var lockstepInfo = "OK IDLE -- 3 lockstep 1 10000 2 0 50";
			_joystick.ParseJoystickInfo(lockstepInfo);

			_joystick.ViewInfo.IsTargetingVirtualAxis = true;
			_joystick.ViewInfo.IsTargetingLockstep = false;
			Assert.IsTrue(_joystick.HasUnsavedChanges);

			_joystick.ViewInfo.IsTargetingVirtualAxis = false;
			_joystick.ViewInfo.IsTargetingLockstep = true;
			Assert.IsFalse(_joystick.HasUnsavedChanges);

			_joystick.ViewInfo.TargetDeviceNumber = "0";
			Assert.IsTrue(_joystick.HasUnsavedChanges);
			_joystick.ViewInfo.TargetDeviceNumber = "3";
			Assert.IsFalse(_joystick.HasUnsavedChanges);

			_joystick.ViewInfo.Resolution = "99";
			Assert.IsTrue(_joystick.HasUnsavedChanges);
			_joystick.ViewInfo.Resolution = "50";
			Assert.IsFalse(_joystick.HasUnsavedChanges);

			_joystick.ViewInfo.LockstepIndex = "5";
			Assert.IsTrue(_joystick.HasUnsavedChanges);
			_joystick.ViewInfo.LockstepIndex = "1";
			Assert.IsFalse(_joystick.HasUnsavedChanges);

			_joystick.ViewInfo.TargetAxis = "99";
			Assert.IsFalse(_joystick.HasUnsavedChanges);

			_joystick.ViewInfo.SpeedSelected = "Linear";
			Assert.IsTrue(_joystick.HasUnsavedChanges);
			_joystick.ViewInfo.SpeedSelected = "Squared (Default)";
			Assert.IsFalse(_joystick.HasUnsavedChanges);

			_joystick.ViewInfo.MaxSpeed = "9999";
			Assert.IsTrue(_joystick.HasUnsavedChanges);
			_joystick.ViewInfo.MaxSpeed = "10000";
			Assert.IsFalse(_joystick.HasUnsavedChanges);

			_joystick.ViewInfo.IsInverted = true;
			Assert.IsTrue(_joystick.HasUnsavedChanges);
			_joystick.ViewInfo.IsInverted = false;
			Assert.IsFalse(_joystick.HasUnsavedChanges);
		}


		[Test]
		public void LockstepInfoLabelTest()
		{
			_thisLockstepCollection.Add(2, new Dictionary<int, string> { { 1, "lockstep info" } });

			_thisLockstepCollection.Add(1, new Dictionary<int, string>());
			_thisLockstepCollection.Add(5, new Dictionary<int, string>());
			_thisLockstepCollection.Add(0, new Dictionary<int, string>());
			_thisLockstepCollection.Add(99, new Dictionary<int, string>());

			_joystick.ViewInfo.TargetDeviceNumber = "2";
			_joystick.ViewInfo.LockstepIndex = "1";
			_joystick.SetLockstepInfo();
			Assert.AreEqual("lockstep info", _joystick.TargetLockstepInfo);

			_joystick.ViewInfo.TargetDeviceNumber = "5";
			_joystick.SetLockstepInfo();
			Assert.AreEqual("Not Set-up", _joystick.TargetLockstepInfo);

			_joystick.ViewInfo.TargetDeviceNumber = "2";
			_joystick.ViewInfo.LockstepIndex = "2";
			_joystick.SetLockstepInfo();
			Assert.AreEqual("Not Set-up", _joystick.TargetLockstepInfo);

			_joystick.ViewInfo.TargetDeviceNumber = "99";
			_joystick.ViewInfo.LockstepIndex = "9";
			_joystick.SetLockstepInfo();
			Assert.AreEqual("Not Set-up", _joystick.TargetLockstepInfo);

			_joystick.ViewInfo.TargetDeviceNumber = "0";
			_joystick.SetLockstepInfo();
			Assert.AreEqual("All Devices", _joystick.TargetLockstepInfo);

			_joystick.ViewInfo.LockstepIndex = "1";
			_joystick.SetLockstepInfo();
			Assert.AreEqual("All Devices", _joystick.TargetLockstepInfo);

			_joystick.ViewInfo.TargetDeviceNumber = "2";
			_joystick.SetLockstepInfo();
			Assert.AreEqual("lockstep info", _joystick.TargetLockstepInfo);
		}


		[Test]
		public void NormalHasUnsavedChangesTest()
		{
			var normalInfo = "OK IDLE -- 2 1 10000 2 1 50";

			_joystick.ParseJoystickInfo(normalInfo);
			_joystick.ViewInfo.IsTargetingNormalAxis = false;
			_joystick.ViewInfo.IsTargetingLockstep = true;
			Assert.IsTrue(_joystick.HasUnsavedChanges);
			Assert.IsTrue(_joystick.ViewInfo.TargetAxisChanged);
			Assert.IsTrue(_joystick.ViewInfo.TargetLockstepChanged);

			_joystick.ViewInfo.IsTargetingNormalAxis = true;
			_joystick.ViewInfo.IsTargetingLockstep = false;
			Assert.IsFalse(_joystick.HasUnsavedChanges);
			Assert.IsFalse(_joystick.ViewInfo.TargetAxisChanged);
			Assert.IsFalse(_joystick.ViewInfo.TargetLockstepChanged);

			_joystick.ViewInfo.TargetDeviceNumber = "0";
			Assert.IsTrue(_joystick.HasUnsavedChanges);
			Assert.IsTrue(_joystick.ViewInfo.TargetDeviceChanged);
			_joystick.ViewInfo.TargetDeviceNumber = "2";
			Assert.IsFalse(_joystick.HasUnsavedChanges);
			Assert.IsFalse(_joystick.ViewInfo.TargetDeviceChanged);


			_joystick.ViewInfo.Resolution = "99";
			Assert.IsTrue(_joystick.HasUnsavedChanges);
			Assert.IsTrue(_joystick.ViewInfo.ResolutionChanged);
			_joystick.ViewInfo.Resolution = "50";
			Assert.IsFalse(_joystick.HasUnsavedChanges);
			Assert.IsFalse(_joystick.ViewInfo.ResolutionChanged);

			_joystick.ViewInfo.VirtualIndex = "5";
			Assert.IsFalse(_joystick.HasUnsavedChanges);
			Assert.IsFalse(_joystick.ViewInfo.TargetVirtualChanged);

			_joystick.ViewInfo.SpeedSelected = "Linear";
			Assert.IsTrue(_joystick.HasUnsavedChanges);
			Assert.IsTrue(_joystick.ViewInfo.ControlSpeedChanged);
			_joystick.ViewInfo.SpeedSelected = "Squared (Default)";
			Assert.IsFalse(_joystick.HasUnsavedChanges);
			Assert.IsFalse(_joystick.ViewInfo.ControlSpeedChanged);

			_joystick.ViewInfo.MaxSpeed = "9999";
			Assert.IsTrue(_joystick.HasUnsavedChanges);
			Assert.IsTrue(_joystick.ViewInfo.MaxSpeedChanged);
			_joystick.ViewInfo.MaxSpeed = "10000";
			Assert.IsFalse(_joystick.HasUnsavedChanges);
			Assert.IsFalse(_joystick.ViewInfo.MaxSpeedChanged);

			_joystick.ViewInfo.IsInverted = false;
			Assert.IsTrue(_joystick.ViewInfo.InvertChanged);
			Assert.IsTrue(_joystick.HasUnsavedChanges);
			_joystick.ViewInfo.IsInverted = true;
			Assert.IsFalse(_joystick.HasUnsavedChanges);
			Assert.IsFalse(_joystick.ViewInfo.InvertChanged);
		}


		[Test]
		public void ParseJoystickLockstepInfoTest()
		{
			var lockstepInfo = "OK IDLE -- 3 lockstep 1 10000 2 0 50";

			_joystick.ParseJoystickInfo(lockstepInfo);

			Assert.IsFalse(_joystick.ViewInfo.IsTargetingNormalAxis);
			Assert.IsTrue(_joystick.ViewInfo.IsTargetingLockstep);
			Assert.IsFalse(_joystick.ViewInfo.IsTargetingVirtualAxis);

			Assert.AreEqual("3", _joystick.ViewInfo.TargetDeviceNumber);
			Assert.AreEqual("1", _joystick.ViewInfo.LockstepIndex);
			Assert.AreEqual("10000", _joystick.ViewInfo.MaxSpeed);
			Assert.AreEqual("Squared (Default)", _joystick.ViewInfo.SpeedSelected);
			Assert.AreEqual("50", _joystick.ViewInfo.Resolution);
		}


		[Test]
		public void ParseJoystickNormalInfoTest()
		{
			var normalInfo = "OK IDLE -- 2 1 10000 2 1 50";

			_joystick.ParseJoystickInfo(normalInfo);

			Assert.IsTrue(_joystick.ViewInfo.IsTargetingNormalAxis);
			Assert.IsFalse(_joystick.ViewInfo.IsTargetingLockstep);
			Assert.IsFalse(_joystick.ViewInfo.IsTargetingVirtualAxis);

			Assert.AreEqual("2", _joystick.ViewInfo.TargetDeviceNumber);
			Assert.AreEqual("1", _joystick.ViewInfo.TargetAxis);
			Assert.AreEqual("10000", _joystick.ViewInfo.MaxSpeed);
			Assert.AreEqual("Squared (Default)", _joystick.ViewInfo.SpeedSelected);
			Assert.AreEqual("50", _joystick.ViewInfo.Resolution);
		}


		[Test]
		public void ParseJoystickVirtualInfoTest()
		{
			var virtualInfo = "OK IDLE -- 3 virtual 1 10000 2 0 50";

			_joystick.ParseJoystickInfo(virtualInfo);

			Assert.IsFalse(_joystick.ViewInfo.IsTargetingNormalAxis);
			Assert.IsFalse(_joystick.ViewInfo.IsTargetingLockstep);
			Assert.IsTrue(_joystick.ViewInfo.IsTargetingVirtualAxis);

			Assert.AreEqual("3", _joystick.ViewInfo.TargetDeviceNumber);
			Assert.AreEqual("1", _joystick.ViewInfo.VirtualIndex);
			Assert.AreEqual("10000", _joystick.ViewInfo.MaxSpeed);
			Assert.AreEqual("Squared (Default)", _joystick.ViewInfo.SpeedSelected);
			Assert.AreEqual("50", _joystick.ViewInfo.Resolution);
		}


		[Test]
		public void RM7081_ResetTargetDevice()
		{
			var normalInfo = "OK IDLE -- 2 1 10000 2 1 50";
			_joystick.ParseJoystickInfo(normalInfo);

			// Precondition check
			Assert.IsNotNull(_joystick.TargetDevice?.DeviceNumber);

			// Before the fix, setting the target number to an invalid device number would
			// have left the target device object unchanged.
			_joystick.ViewInfo.TargetDeviceNumber = "17";
			Assert.IsNull(_joystick.TargetDevice);
		}


		[Test]
		public void TargetAxisInfoLabelTest()
		{
			_joystick.ViewInfo.TargetDeviceNumber = "1";
			_joystick.ViewInfo.TargetAxis = "1";
			Assert.AreEqual(_joystick.TargetAxisDeviceName, "Axis 1");
			_joystick.ViewInfo.TargetAxis = "2";
			Assert.AreEqual(_joystick.TargetAxisDeviceName, "Axis 2");
			_joystick.ViewInfo.TargetAxis = "0";
			Assert.AreEqual(_joystick.TargetAxisDeviceName, "All Axes");
			_joystick.ViewInfo.TargetAxis = "8";
			Assert.AreEqual(_joystick.TargetAxisDeviceName, "Not Connected");

			_joystick.ViewInfo.TargetDeviceNumber = "2";
			_joystick.ViewInfo.TargetAxis = "1";
			Assert.AreEqual(_joystick.TargetAxisDeviceName, "Single Axis Device");
			_joystick.ViewInfo.TargetAxis = "0";
			Assert.AreEqual(_joystick.TargetAxisDeviceName, "Single Axis Device");
			_joystick.ViewInfo.TargetAxis = "2";
			Assert.AreEqual(_joystick.TargetAxisDeviceName, "Not Connected");

			_joystick.ViewInfo.TargetDeviceNumber = "0";
			_joystick.ViewInfo.TargetAxis = "0";
			Assert.AreEqual(_joystick.TargetAxisDeviceName, "All Devices");
			_joystick.ViewInfo.TargetAxis = "1";
			Assert.AreEqual(_joystick.TargetAxisDeviceName, "All Devices");

			_joystick.ViewInfo.TargetDeviceNumber = "4";
			_joystick.ViewInfo.TargetAxis = "0";
			Assert.AreEqual(_joystick.TargetAxisDeviceName, "Not Connected");
			_joystick.ViewInfo.TargetAxis = "1";
			Assert.AreEqual(_joystick.TargetAxisDeviceName, "Not Connected");
		}


		[Test]
		public void TargetDeviceInfoLabelTest()
		{
			_joystick.ViewInfo.TargetDeviceNumber = "4";
			Assert.AreEqual(_joystick.TargetDeviceName, "Not Connected");
			_joystick.ViewInfo.TargetDeviceNumber = "1";
			Assert.AreEqual(_joystick.TargetDeviceName, "Multi Axis Device");
			_joystick.ViewInfo.TargetDeviceNumber = "2";
			Assert.AreEqual(_joystick.TargetDeviceName, "Single Axis Device");
			_joystick.ViewInfo.TargetDeviceNumber = "0";
			Assert.AreEqual(_joystick.TargetDeviceName, "All Devices");
		}


		[Test]
		public void VirtualAxisInfoLabelTest()
		{
			_thisVirtualAxesCollection.Add(2, new Dictionary<int, string> { { 1, "virtual axis info" } });

			_thisVirtualAxesCollection.Add(1, new Dictionary<int, string>());
			_thisVirtualAxesCollection.Add(5, new Dictionary<int, string>());
			_thisVirtualAxesCollection.Add(0, new Dictionary<int, string>());
			_thisVirtualAxesCollection.Add(99, new Dictionary<int, string>());


			_joystick.ViewInfo.TargetDeviceNumber = "2";
			_joystick.ViewInfo.VirtualIndex = "1";
			_joystick.SetVirtualAxisInfo();
			Assert.AreEqual("virtual axis info", _joystick.TargetVirtualAxisInfo);

			_joystick.ViewInfo.TargetDeviceNumber = "5";
			_joystick.SetVirtualAxisInfo();
			Assert.AreEqual("Not Set-up", _joystick.TargetVirtualAxisInfo);

			_joystick.ViewInfo.TargetDeviceNumber = "2";
			_joystick.ViewInfo.VirtualIndex = "2";
			_joystick.SetVirtualAxisInfo();
			Assert.AreEqual("Not Set-up", _joystick.TargetVirtualAxisInfo);

			_joystick.ViewInfo.TargetDeviceNumber = "99";
			_joystick.ViewInfo.VirtualIndex = "9";
			_joystick.SetVirtualAxisInfo();
			Assert.AreEqual("Not Set-up", _joystick.TargetVirtualAxisInfo);

			_joystick.ViewInfo.TargetDeviceNumber = "0";
			_joystick.SetVirtualAxisInfo();
			Assert.AreEqual("All Devices", _joystick.TargetVirtualAxisInfo);

			_joystick.ViewInfo.VirtualIndex = "1";
			_joystick.SetVirtualAxisInfo();
			Assert.AreEqual("All Devices", _joystick.TargetVirtualAxisInfo);

			_joystick.ViewInfo.TargetDeviceNumber = "2";
			_joystick.SetVirtualAxisInfo();
			Assert.AreEqual("virtual axis info", _joystick.TargetVirtualAxisInfo);
		}


		[Test]
		public void VirtualHasUnsavedChangesTest()
		{
			var virtualInfo = "OK IDLE -- 3 virtual 1 10000 2 0 50";
			_joystick.ParseJoystickInfo(virtualInfo);

			_joystick.ViewInfo.IsTargetingNormalAxis = true;
			_joystick.ViewInfo.IsTargetingVirtualAxis = false;
			Assert.IsTrue(_joystick.HasUnsavedChanges);

			_joystick.ViewInfo.IsTargetingNormalAxis = false;
			_joystick.ViewInfo.IsTargetingVirtualAxis = true;
			Assert.IsFalse(_joystick.HasUnsavedChanges);

			_joystick.ViewInfo.TargetDeviceNumber = "0";
			Assert.IsTrue(_joystick.HasUnsavedChanges);
			_joystick.ViewInfo.TargetDeviceNumber = "3";
			Assert.IsFalse(_joystick.HasUnsavedChanges);

			_joystick.ViewInfo.Resolution = "99";
			Assert.IsTrue(_joystick.HasUnsavedChanges);
			_joystick.ViewInfo.Resolution = "50";
			Assert.IsFalse(_joystick.HasUnsavedChanges);

			_joystick.ViewInfo.VirtualIndex = "5";
			Assert.IsTrue(_joystick.HasUnsavedChanges);
			_joystick.ViewInfo.VirtualIndex = "1";
			Assert.IsFalse(_joystick.HasUnsavedChanges);

			_joystick.ViewInfo.TargetAxis = "99";
			Assert.IsFalse(_joystick.HasUnsavedChanges);

			_joystick.ViewInfo.SpeedSelected = "Linear";
			Assert.IsTrue(_joystick.HasUnsavedChanges);
			_joystick.ViewInfo.SpeedSelected = "Squared (Default)";
			Assert.IsFalse(_joystick.HasUnsavedChanges);

			_joystick.ViewInfo.MaxSpeed = "9999";
			Assert.IsTrue(_joystick.HasUnsavedChanges);
			_joystick.ViewInfo.MaxSpeed = "10000";
			Assert.IsFalse(_joystick.HasUnsavedChanges);

			_joystick.ViewInfo.IsInverted = true;
			Assert.IsTrue(_joystick.HasUnsavedChanges);
			_joystick.ViewInfo.IsInverted = false;
			Assert.IsFalse(_joystick.HasUnsavedChanges);
		}


		public JoystickVM _joystick;

		public Dictionary<int, Dictionary<int, string>> _thisVirtualAxesCollection =
			new Dictionary<int, Dictionary<int, string>>();

		public Dictionary<int, Dictionary<int, string>> _thisLockstepCollection =
			new Dictionary<int, Dictionary<int, string>>();

		public List<ZaberDevice> _connectedDevices = new List<ZaberDevice>();
	}
}

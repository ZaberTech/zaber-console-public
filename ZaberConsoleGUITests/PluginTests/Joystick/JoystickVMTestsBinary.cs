﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZaberConsoleGUITests.XJoyPluginTests
{
    /// <summary>
    /// Testing JoystickVM in binary --low priority, this class is pretty much the same in binary.
    /// </summary>
    [TestFixture, Category("JoystickPluginBinary")]
    class JoystickVMTestsBinary
    {
    }
}

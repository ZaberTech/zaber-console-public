﻿using System;
using System.Collections.ObjectModel;
using NUnit.Framework;
using Zaber.Testing;
using ZaberConsole.Plugins.Joystick.Key;

namespace ZaberConsoleGUITests.XJoyPluginTests
{
	/// <summary>
	///     Tests the KeyCommandGridVM view model in ASCII.
	/// </summary>
	[TestFixture]
	[Category("JoystickPlugin")]
	[SetCulture("en-US")]
	public class KeyCommandGridTests
	{
		[SetUp]
		public void SetUp()
		{
			_grid = new KeyCommandGridVM(1);
			Initialize(_grid);
			_grid.AlertsEnabled = false;
			_grid.PreviousAlertsEnabled = false;
		}


		[TearDown]
		public void Teardown()
		{
			_listener?.Unlisten(_grid);
			_grid = null;
		}


		[Test]
		public void AddCommandTest()
		{
			var toAdd = new KeyCommandInfo
			{
				TargetDeviceNumber = "1",
				TargetAxis = "1",
				CommandName = "home"
			};
			_grid.AddCommandAscii(toAdd);

			Assert.AreEqual(_grid.KeyCommands.IndexOf(toAdd), _grid.KeyCommands.Count - 1);
			Assert.AreEqual(_grid.KeyCommands[0].IsAddCommand, true);
			Assert.AreEqual(_grid.HasUnsavedChanges, false);

			var invocationCount = 0;

			EventHandler handler = (aSender, aArgs) => { ++invocationCount; };

			_grid.CommandsChangedEvent += handler;
			toAdd.CommandName = "changed";
			_grid.CommandsChangedEvent -= handler;
			Assert.AreEqual(1, invocationCount);
		}


		[Test]
		public void AddingLargeNumberRowsTest()
		{
			var addRow = _grid.KeyCommands[0];
			addRow.CommandName = "move rel 500";
			addRow.TargetDeviceNumber = "5000000000000000";

			var oldCount = _grid.KeyCommands.Count;

			_grid.AddRowCommand.Execute(addRow); //shouldn't work, target device number is too large.

			Assert.AreEqual(oldCount, _grid.KeyCommands.Count);
		}


		[Test]
		public void AddingNewRow_HasBeenChangedTest()
		{
			_grid.KeyCommands[0].CommandName = "add this command";
			_grid.KeyCommands[0].TargetDeviceNumber = "1";
			var addCmd = _grid.AddRowCommand;
			addCmd.Execute(_grid.KeyCommands[0]); //this is the row for adding stuff.

			Assert.IsTrue(_grid.KeyCommands[4].HasBeenChanged);
		}


		[Test]
		public void AddRowsTest()
		{
			var addRow = _grid.KeyCommands[0];
			addRow.CommandName = "move rel 500";

			_grid.AddRowCommand.Execute(addRow); //shouldn't work
			Assert.AreEqual(_grid.KeyCommands.Count, 4);

			addRow.TargetAxis = "1";
			addRow.TargetDeviceNumber = "1";

			_grid.AddRowCommand.Execute(addRow);
			Assert.AreEqual(_grid.KeyCommands[4].CommandName.Equals("move rel 500"), true);
			Assert.IsNull(addRow.CommandName);
			Assert.AreEqual(_grid.HasUnsavedChanges, true);
		}


		[Test]
		public void ChangeNamesUnsavedStateTest()
		{
			_grid.KeyCommands[1].CommandName = "changed";
			Assert.AreEqual(_grid.HasUnsavedChanges, true);
			_grid.KeyCommands[1].CommandName = "home";
			Assert.AreEqual(_grid.HasUnsavedChanges, false);

			_grid.KeyCommands[1].TargetDeviceNumber = "5";
			Assert.AreEqual(_grid.HasUnsavedChanges, true);
			_grid.KeyCommands[1].TargetDeviceNumber = "2";
			Assert.AreEqual(_grid.HasUnsavedChanges, false);

			_grid.KeyCommands[1].TargetAxis = "5";
			Assert.AreEqual(_grid.HasUnsavedChanges, true);
			_grid.KeyCommands[1].TargetAxis = "1";
			Assert.AreEqual(_grid.HasUnsavedChanges, false);
		}


		[Test]
		public void EditingRow_HasBeenChangedTest()
		{
			Assert.IsFalse(_grid.KeyCommands[3].HasBeenChanged);
			_grid.KeyCommands[3].TargetAxis = "8";
			Assert.IsTrue(_grid.KeyCommands[3].HasBeenChanged);
		}


		[Test]
		public void FormatCommandsTest()
		{
			var returnedCommands = _grid.FormatCommands();

			StringAssert.Contains(returnedCommands[0], "02 1 home");
			StringAssert.Contains(returnedCommands[1], "02 1 tools storepos 1 current");
			StringAssert.Contains(returnedCommands[2], "02 1 home");
		}


		[Test]
		public void InitializeTest()
		{
			Assert.AreEqual(_grid.KeyCommands.Count, 4);
			Assert.AreEqual(_grid.HasUnsavedChanges, false);

			var addRow = _grid.KeyCommands[0];
			Assert.AreEqual(addRow.IsAddCommand, true);
		}


		[Test]
		public void NullAddCommandRowTest()
		{
			var addCommandRow = _grid.KeyCommands[0];
			Assert.AreEqual(addCommandRow.IsAddCommand, true);
			Assert.IsNull(addCommandRow.TargetAxis);
			Assert.IsNull(addCommandRow.TargetDeviceNumber);
			Assert.IsNull(addCommandRow.CommandName);
		}


		[Test]
		public void OrderOfRows_HasBeenChangedTest()
		{
			var keyCommandToInsert = _grid.KeyCommands[3];
			_grid.KeyCommands.Remove(keyCommandToInsert);
			_grid.KeyCommands.Insert(1, keyCommandToInsert);

			Assert.IsFalse(_grid.KeyCommands[1].HasBeenChanged);
			Assert.IsTrue(_grid.KeyCommands[2].HasBeenChanged);
			Assert.IsTrue(_grid.KeyCommands[3].HasBeenChanged);
		}


		[Test]
		public void TestKeyCommandsProperty()
		{
			_listener = new ObservableObjectTester();
			_listener.Listen(_grid);
			_listener.TestObservableProperty(_grid,
											 () => _grid.KeyCommands,
											 _ => new ObservableCollection<KeyCommandInfo>());
			Assert.AreEqual(0, _listener.GetNumNotifications(), "Unexpected side effect notifications.");
		}


		public void DeleteRowsTest()
		{
			var addRow = _grid.KeyCommands[0];
			_grid.DeleteRowCommand.Execute(_grid.KeyCommands[1]);

			Assert.AreEqual(_grid.KeyCommands.Count, 4);
			Assert.AreEqual(_grid.KeyCommands[3].CommandName.Equals("move rel 500"), true);
			Assert.AreEqual(_grid.HasUnsavedChanges, true);
		}


		private void Initialize(KeyCommandGridVM aGrid)
		{
			var homeCommand = new KeyCommandInfo
			{
				TargetDeviceNumber = "2",
				TargetAxis = "1",
				CommandName = "home"
			};
			var toolsCommand = new KeyCommandInfo
			{
				TargetDeviceNumber = "2",
				TargetAxis = "1",
				CommandName = "tools storepos 1 current"
			};
			var homeCommand2 = new KeyCommandInfo
			{
				TargetDeviceNumber = "2",
				TargetAxis = "1",
				CommandName = "home"
			};

			aGrid.AddCommandAscii(homeCommand);
			aGrid.AddCommandAscii(toolsCommand);
			aGrid.AddCommandAscii(homeCommand2);
		}


		public KeyCommandGridVM _grid;

		private ObservableObjectTester _listener;
	}
}

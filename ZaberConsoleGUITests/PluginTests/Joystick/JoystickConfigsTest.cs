﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Zaber;
using Zaber.Testing;
using ZaberConsole;
using ZaberConsole.Plugins.Joystick;
using ZaberConsole.Plugins.Joystick.JoystickConfig;
using ZaberTest;
using ZaberTest.Testing;
using ZaberWpfToolbox.Dialogs;
using ZaberWpfToolboxTests;

namespace ZaberConsoleGUITests.XJoyPluginTests
{
	[TestFixture]
	[Category("JoystickPlugin")]
	[SetCulture("en-US")]
	public class JoystickConfigsTest
	{
		[SetUp]
		public void SetUp()
		{
			DispatcherStack.Push(new ImmediateDispatcher());
			BackgroundWorkerStack.Push(typeof(ImmediateForegroundWorker));

			SetupPort();
			_plugin = new JoystickPluginVM { PortFacade = _portFacade };

			_plugin.OnTabShown();
			_plugin.PortFacade.Open("COM3");
			SetupPortExpectations(_mockPort, "\\joystick_configs_test_initialization_basic.txt");

			JoystickConfigurationVM.CONFIG_FILE = Path.Combine(TestContext.CurrentContext.TestDirectory,
															   "XJoyPluginTests",
															   "InitialJoystickConfiguration.yaml");
			_configVM = new JoystickConfigurationVM(_plugin, Protocol.ASCII);
		}


		[TearDown]
		public void Teardown()
		{
			BackgroundWorkerStack.Pop();
			DispatcherStack.Pop();
		}


		[Test]
		public void DeserializationTest()
		{
			WaitForInitialQuery();

			var saveCmd = _configVM.SaveCurrentSettingsCommand;
			_configVM.SaveConfigTitle = "Testing Case ASCII";
			saveCmd.Execute(null);

			_configVM.SelectedItem =
				_configVM.VisibleConfigurations.Single(configView => configView.Description == "Testing Case ASCII");

			Assert.IsTrue(_plugin.Joysticks[1].OldInfo.IsTargetingVirtualAxis);
			_plugin.Joysticks[1].OldInfo.IsTargetingNormalAxis = true;
			_plugin.Joysticks[1].OldInfo.IsTargetingVirtualAxis = false;
			_plugin.Joysticks[1].OldInfo.Resolution = "9999";
			_plugin.Joysticks[1].OldInfo.IsInverted = true;
			_plugin.Joysticks[1].OldInfo.MaxSpeed = "9999";
			_plugin.Joysticks[1].OldInfo.VirtualIndex = "5";
			_plugin.Joysticks[1].OldInfo.SpeedSelected = "dummy";
			_plugin.Keys[1].KeyCommandGrids[2].ClearCommands();
			_plugin.Keys[8].KeyCommandGrids[3].ClearCommands();
			_plugin.Keys[2].KeyCommandGrids[1].PreviousAlertsEnabled = false;
			_plugin.Keys[8].KeyCommandGrids[1].PreviousAlertsEnabled = false;

			SetupPortExpectations(_mockPort, "\\load_joystick_configs.txt");
			var loadCmd = _configVM.LoadCommand;
			loadCmd.Execute(null);

			var counter = 0;
			while (!_plugin.Joysticks[1].OldInfo.IsTargetingVirtualAxis && (counter < 10))
			{
				DispatcherUtil.SleepAndDoSyncEvents(500);
				counter++;
			}

			_mockPort.Verify();

			Assert.IsTrue(_plugin.Joysticks[1].OldInfo.IsTargetingVirtualAxis);
			Assert.AreEqual("50", _plugin.Joysticks[1].OldInfo.Resolution);
			Assert.AreEqual("44880", _plugin.Joysticks[1].OldInfo.MaxSpeed);
			Assert.IsFalse(_plugin.Joysticks[1].OldInfo.IsInverted);
			Assert.AreEqual("1", _plugin.Joysticks[1].OldInfo.VirtualIndex);
			Assert.AreEqual("Squared (Default)", _plugin.Joysticks[1].OldInfo.SpeedSelected);
			Assert.AreEqual("stop", _plugin.Keys[1].KeyCommandGrids[2].KeyCommands[1].CommandName);
			Assert.AreEqual("0", _plugin.Keys[1].KeyCommandGrids[2].KeyCommands[1].TargetAxis);
			Assert.AreEqual("00", _plugin.Keys[1].KeyCommandGrids[2].KeyCommands[1].TargetDeviceNumber);
			Assert.AreEqual("joystick 3 maxspeed 44880", _plugin.Keys[8].KeyCommandGrids[3].KeyCommands[1].CommandName);
			Assert.AreEqual("0", _plugin.Keys[8].KeyCommandGrids[3].KeyCommands[1].TargetAxis);
			Assert.AreEqual("01", _plugin.Keys[8].KeyCommandGrids[3].KeyCommands[1].TargetDeviceNumber);

			Assert.IsTrue(_plugin.Keys[2].KeyCommandGrids[1].PreviousAlertsEnabled);
			Assert.IsTrue(_plugin.Keys[8].KeyCommandGrids[1].PreviousAlertsEnabled);
		}


		[Test]
		public void ExportTest()
		{
			WaitForInitialQuery();

			var saveCmd = _configVM.SaveCurrentSettingsCommand;
			_configVM.SaveConfigTitle = "Testing Case ASCII";
			saveCmd.Execute(null);

			_configVM.SelectedItem =
				_configVM.VisibleConfigurations.Single(configView => configView.Description == "Testing Case ASCII");

			var fileName = Path.GetTempFileName();
			if (File.Exists(fileName))
			{
				File.Delete(fileName);
			}

			RequestDialogChange del1 = (aSender, aVM) =>
			{
				Assert.IsTrue(ReferenceEquals(aSender, _configVM));
				var dlg = aVM as FileBrowserDialogParams;
				Assert.IsNotNull(dlg);
				dlg.Filename = fileName;
				dlg.DialogResult = true;
			};

			_configVM.RequestDialogOpen += del1;
			var exportCmd = _configVM.ExportCommand;
			Assert.IsTrue(exportCmd.CanExecute(null));
			Assert.IsFalse(File.Exists(fileName));
			exportCmd.Execute(null);
			_configVM.RequestDialogOpen -= del1;

			Assert.IsTrue(File.Exists(fileName));

			File.Delete(fileName);
		}


		[Test]
		public void ImportTest()
		{
			WaitForInitialQuery();

			var cfgCount = _configVM.VisibleConfigurations.Count;

			var saveCmd = _configVM.SaveCurrentSettingsCommand;
			_configVM.SaveConfigTitle = "Testing Case ASCII";
			saveCmd.Execute(null);

			Assert.AreEqual(cfgCount + 1, _configVM.VisibleConfigurations.Count);

			RequestDialogChange del1 = (aSender, aVM) =>
			{
				Assert.IsTrue(ReferenceEquals(aSender, _configVM));
				var dlg = aVM as FileBrowserDialogParams;
				Assert.IsNotNull(dlg);
				dlg.Filename = TestContext.CurrentContext.TestDirectory
						   + "\\PluginTests\\Joystick"
						   + "\\JoystickConfigurationsImportTest.yaml";
				dlg.DialogResult = true;
			};

			_configVM.RequestDialogOpen += del1;
			var exportCmd = _configVM.ImportCommand;
			Assert.IsTrue(exportCmd.CanExecute(null));
			exportCmd.Execute(null);
			_configVM.RequestDialogOpen -= del1;

			Assert.AreEqual(cfgCount + 3, _configVM.VisibleConfigurations.Count);
			Assert.AreEqual("ASCII", _configVM.VisibleConfigurations[cfgCount + 1].Protocol);
			Assert.AreEqual("Binary", _configVM.VisibleConfigurations[cfgCount + 2].Protocol);
		}


		[Test]
		public void SerializationTest()
		{
			WaitForInitialQuery();

			var cmd = _configVM.SaveCurrentSettingsCommand;
			_configVM.SaveConfigTitle = "Testing Case ASCII";
			cmd.Execute(null);

			_configVM.SelectedItem =
				_configVM.VisibleConfigurations.Single(configView => configView.Description == "Testing Case ASCII");
			var expectedOutput = ReadFile("\\JoystickConfigurationsTestASCII.yaml");
			expectedOutput = expectedOutput.Replace("ToBeReplaced", DateTime.Now.ToLongDateString());
			expectedOutput = expectedOutput.Replace("\r\n", "\n"); // Ignore line ending differences.

			Assert.AreEqual(expectedOutput, _configVM.DisplayConfigInformation.Replace("\r\n", "\n"));
		}


		/// <summary>
		///     Add expectations and responses from a port after reading a text file that is representative
		///     of what appears on the log on Zaber Console after the initial query of a joystick.
		///     Format of the filename is "\\filename.txt". Put the txt file in the ZaberConsoleGuiTests/XJoyPluginTests folder and
		///     add
		///     it as a content resource.
		/// </summary>
		public static void SetupPortExpectations(MockPort mockPort, string filename)
		{
			try
			{
				var lowerDirectory = TestContext.CurrentContext.TestDirectory;
				var path = lowerDirectory + "\\PluginTests\\Joystick" + filename;
				var file = new StreamReader(path);
				string line;

				while ((line = file.ReadLine()) != null)
				{
					switch (line[0])
					{
						case '/':
							mockPort.Expect(line);
							break;
						case '@':
							mockPort.AddResponse(line);
							break;
						case '#':
							mockPort.AddResponse(line);
							break;
					}
				}
			}

			catch
			{
				Assert.Fail(
					"File not found. Please put the correct expectation text files inside the XJoyPluginTests folder.");
			}
		}


		/// <summary>
		///     Reads all of the text of a file into a string.
		/// </summary>
		/// <param name="filename">
		///     Format of the filename is "\\filename.txt". Put the txt file in the
		///     ZaberConsoleGuiTests/XJoyPluginTests folder.
		/// </param>
		/// <returns>A string that is all of the text in the file that was read from.</returns>
		public static string ReadFile(string filename)
		{
			try
			{
				//Traverse up 2 directories from the executing directory to the ZaberConsoleGUITests directory, then 
				//go to the XJoyPluginTests directory where the text files are. 
				var path = TestContext.CurrentContext.TestDirectory;
				var allText = File.ReadAllText(path + "\\PluginTests\\Joystick" + filename);
				return allText;
			}

			catch
			{
				Assert.Fail(
					"File not found. Please put the correct expectation text files inside the XJoyPluginTests folder.");
				return null;
			}
		}


		private void SetupPort()
		{
			_mockPort = new MockPort();
			_mockPort.IsAsciiMode = true;

			var defaultDeviceType = new DeviceType { Commands = new List<CommandInfo>() };

			_portFacade = new ZaberPortFacade
			{
				DefaultDeviceType = defaultDeviceType,
				Port = _mockPort,
				QueryTimeout = 100
			};

			var joyCommands = new List<CommandInfo> { new CommandInfo { Command = Command.SetAxisDeviceNumber } };

			joyCommands.AddRange(
				AsciiCommandBuilder.ReadCommandDefinitionsFromJsonFile(AsciiCommandBuilder.JOYSTICK_COMMAND_FILE));
			joyCommands.AddRange(
				AsciiCommandBuilder.ReadCommandDefinitionsFromJsonFile(AsciiCommandBuilder.KEY_COMMAND_FILE));

			var XJOY3 = new DeviceType
			{
				DeviceId = 51000,
				Name = "X-JOY3 Joystick",
				MotionType = MotionType.Other,
				Commands = joyCommands,
				FirmwareVersion = new FirmwareVersion(6, 14) // Because the expectations include message IDs.
			};

			var ASR100BT3 = new DeviceType
			{
				PeripheralId = 44012,
				MotionType = MotionType.Linear,
				Name = "X-MCB2 + ASR100BT3"
			};

			var ASR120BT3 = new DeviceType
			{
				PeripheralId = 44022,
				MotionType = MotionType.Linear,
				Name = "X-MCB2 + ASR120BT3"
			};

			var XLRM = new DeviceType
			{
				DeviceId = 50004,
				MotionType = MotionType.Linear,
				Name = "X-LRM"
			};

			var XMCB2 = new DeviceType
			{
				DeviceId = 30222,
				PeripheralMap = new Dictionary<int, DeviceType>
				{
					{ 44012, ASR100BT3 },
					{ 44022, ASR120BT3 }
				},
				Name = "X-MCB2 Controller",
				MotionType = MotionType.Other
			};

			_portFacade.AddDeviceType(XMCB2);
			_portFacade.AddDeviceType(XLRM);
			_portFacade.AddDeviceType(XJOY3);

			ZaberPortFacadeTest.SetAsciiExpectations(_portFacade, _mockPort, XJOY3, XMCB2, XLRM);
		}


		private void WaitForInitialQuery()
		{
			var conversationList = _plugin.PortFacade.Conversations as List<Conversation>;
			_plugin.Conversation = conversationList.Find(x => x.Device.DeviceType.DeviceId == 51000);
			Assert.AreEqual("4", _plugin.Joysticks[3]?.OldInfo?.TargetDeviceNumber);
			_mockPort.Verify();
		}


		private JoystickPluginVM _plugin;
		private MockPort _mockPort;
		private ZaberPortFacade _portFacade;
		private JoystickConfigurationVM _configVM;
	}
}

﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using NUnit.Framework;
using Zaber;
using ZaberConsole.Plugins.CommandList;
using ZaberConsole.Plugins.Joystick.Key;

/// <summary>
/// Tests the KeyCommandGridVM view model for Binary.
/// </summary>
namespace ZaberConsoleGUITests.XJoyPluginTests
{
	[TestFixture]
	[Category("JoystickPluginBinary")]
	[SetCulture("en-US")]
	public class KeyCommandGridTestsBinary
	{
		[SetUp]
		public void SetUp()
		{
			_grid = new KeyCommandGridVM(1);
			Initialize(_grid, _connectedDevices, _binaryCommands);
			_grid.ConnectedDevices = _connectedDevices;
			_grid.BinaryCommands = _binaryCommands;

			_grid.AlertsEnabled = false;
			_grid.PreviousAlertsEnabled = false;
		}


		[TearDown]
		public void Teardown() => _grid = null;


		[Test]
		public void AddBinaryCommandTest()
		{
			var toAdd = new KeyCommandInfo
			{
				TargetDeviceNumber = "4",
				BinaryCommand = "21",
				BinaryData = "5000"
			};
			_grid.ClearCommands();
			_grid.AddCommandBinary(toAdd);
			Assert.AreEqual(2, _grid.KeyCommands.Count);
			Assert.AreEqual(toAdd, _grid.BinaryInfo);
			Assert.IsFalse(_grid.HasUnsavedChanges);
		}


		[Test]
		public void ClearButtonTest()
		{
			Assert.IsFalse(_grid.IsEmptyCommand);

			var cmd = _grid.ClearBinaryInfoCommand;
			cmd.Execute(null);

			Assert.AreEqual("0", _grid.BinaryInfo.BinaryData);
			Assert.AreEqual("255", _grid.BinaryInfo.BinaryCommand);
			Assert.AreEqual("255", _grid.BinaryInfo.TargetDeviceNumber);

			Assert.IsTrue(_grid.IsEmptyCommand);
		}


		[Test]
		public void ClearButtonThenReadTest()
		{
			Assert.IsFalse(_grid.IsEmptyCommand);

			var cmd = _grid.ClearBinaryInfoCommand;
			cmd.Execute(null);

			Assert.AreEqual("0", _grid.BinaryInfo.BinaryData);
			Assert.AreEqual("255", _grid.BinaryInfo.BinaryCommand);
			Assert.AreEqual("255", _grid.BinaryInfo.TargetDeviceNumber);

			Assert.IsTrue(_grid.IsEmptyCommand);

			_grid.ClearCommands();
			_grid.AddCommandBinary(new KeyCommandInfo
			{
				BinaryData = "99",
				BinaryCommand = "99",
				TargetDeviceNumber = "99"
			});

			Assert.AreEqual("99", _grid.BinaryInfo.BinaryData);
			Assert.AreEqual("99", _grid.BinaryInfo.BinaryCommand);
			Assert.AreEqual("99", _grid.BinaryInfo.TargetDeviceNumber);

			Assert.IsFalse(_grid.IsEmptyCommand);
			Assert.IsFalse(_grid.HasUnsavedChanges);
		}


		[Test]
		public void SelectedBinaryCommandTest()
		{
			Assert.AreEqual("Move Relative", _grid.SelectedCommandBinary.CommandInfo.Name);

			_grid.BinaryInfo.BinaryCommand = "169";
			Assert.IsNull(_grid.SelectedCommandBinary);

			_grid.BinaryInfo.BinaryCommand = "20";
			Assert.AreEqual("Move Absolute", _grid.SelectedCommandBinary.CommandInfo.Name);
		}


		[Test]
		public void SelectingChangesCommandTest()
		{
			Assert.AreEqual("21", _grid.BinaryInfo.BinaryCommand);

			_grid.SelectedCommandBinary = _grid.BinaryCommands.Single(x => x.CommandInfo.Name.Equals("Move Absolute"));
			Assert.AreEqual("20", _grid.BinaryInfo.BinaryCommand);

			_grid.SelectedCommandBinary = _grid.BinaryCommands.Single(x => x.CommandInfo.Name.Equals("Move Relative"));
			Assert.AreEqual("21", _grid.BinaryInfo.BinaryCommand);
		}


		[Test]
		public void TargetingDeviceTest()
		{
			Assert.AreEqual("Device Not Currently Connected", _grid.TargetDeviceNameBinary);

			_grid.BinaryInfo.TargetDeviceNumber = "0";
			Assert.AreEqual("All Devices", _grid.TargetDeviceNameBinary);

			_grid.BinaryInfo.TargetDeviceNumber = "1";
			Assert.AreEqual("Multi Axis Device", _grid.TargetDeviceNameBinary);

			_grid.BinaryInfo.TargetDeviceNumber = "2";
			Assert.AreEqual("Single Axis Device", _grid.TargetDeviceNameBinary);
		}


		[Test]
		public void UnsavedChangesTest()
		{
			Assert.IsFalse(_grid.HasUnsavedChanges);

			_grid.BinaryInfo.BinaryCommand = "11";
			Assert.IsTrue(_grid.HasUnsavedChanges);
			_grid.BinaryInfo.BinaryCommand = "21";
			Assert.IsFalse(_grid.HasUnsavedChanges);

			_grid.BinaryInfo.BinaryData = "11";
			Assert.IsTrue(_grid.HasUnsavedChanges);
			_grid.BinaryInfo.BinaryData = "5000";
			Assert.IsFalse(_grid.HasUnsavedChanges);

			_grid.BinaryInfo.TargetDeviceNumber = "11";
			Assert.IsTrue(_grid.HasUnsavedChanges);
			_grid.BinaryInfo.TargetDeviceNumber = "4";
			Assert.IsFalse(_grid.HasUnsavedChanges);
		}


		private static void Initialize(KeyCommandGridVM aGrid,
									   List<ZaberDevice> connectedDevices,
									   ObservableCollection<CommandInfoVM> binaryCommands)
		{
			connectedDevices.Clear();
			binaryCommands.Clear();
			var homeCommand = new KeyCommandInfo
			{
				TargetDeviceNumber = "4",
				BinaryCommand = "21",
				BinaryData = "5000"
			};

			aGrid.AddCommandBinary(homeCommand);

			var allDevices = new ZaberDevice { DeviceType = new DeviceType() };
			var device1 = new ZaberDevice { DeviceType = new DeviceType() };
			var device2 = new ZaberDevice { DeviceType = new DeviceType() };
			var axis1 = new ZaberDevice { DeviceType = new DeviceType() };
			var axis2 = new ZaberDevice { DeviceType = new DeviceType() };

			allDevices.DeviceNumber = 0;
			allDevices.DeviceType.Name = "All Devices";
			device1.DeviceType.Name = "Multi Axis Device";
			device2.DeviceType.Name = "Single Axis Device";
			device1.DeviceNumber = 1;
			device2.DeviceNumber = 2;

			connectedDevices.Add(device1);
			connectedDevices.Add(device2);
			connectedDevices.Add(allDevices);

			var absCommand = new CommandInfo();
			absCommand.Name = "Move Absolute";
			absCommand.Command = Command.MoveAbsolute;
			var errorCommandVM = new CommandInfoVM(absCommand, null);
			binaryCommands.Add(errorCommandVM);

			var relCommand = new CommandInfo();
			relCommand.Name = "Move Relative";
			relCommand.Command = Command.MoveRelative;
			var relCommandVM = new CommandInfoVM(relCommand, null);
			binaryCommands.Add(relCommandVM);
		}


		public KeyCommandGridVM _grid;
		public List<ZaberDevice> _connectedDevices = new List<ZaberDevice>();
		public ObservableCollection<CommandInfoVM> _binaryCommands = new ObservableCollection<CommandInfoVM>();
	}
}

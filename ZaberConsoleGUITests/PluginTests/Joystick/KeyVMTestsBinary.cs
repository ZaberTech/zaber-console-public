﻿using NUnit.Framework;
using Zaber;
using ZaberConsole;
using ZaberConsole.Plugins.Joystick.Key;

/// <summary>
/// Tests the KeyVM view model specifically for Binary.
/// </summary>
namespace ZaberConsoleGUITests.XJoyPluginTests
{
	[TestFixture]
	[Category("JoystickPluginBinary")]
	[SetCulture("en-US")]
	internal class KeyVMTestsBinary
	{
		[SetUp]
		public void SetUp()
		{
			_key = new KeyVM(1, Protocol.Binary);
			Initialize(_key);
		}


		[TearDown]
		public void Teardown() => _key = null;


		[Test]
		public void InitialParseKeyBinaryTest()
		{
			var event1Binary = _key.KeyCommandGrids[1].BinaryInfo;
			var event2Binary = _key.KeyCommandGrids[2].BinaryInfo;
			var event3Binary = _key.KeyCommandGrids[3].BinaryInfo;
			var event4Binary = _key.KeyCommandGrids[4].BinaryInfo;

			var keyInfo1 = new KeyCommandInfo
			{
				TargetDeviceNumber = "1",
				BinaryCommand = "21",
				BinaryData = "-500"
			};

			var keyInfo2 = new KeyCommandInfo
			{
				TargetDeviceNumber = "2",
				BinaryCommand = "20",
				BinaryData = "500"
			};

			var keyInfo3 = new KeyCommandInfo
			{
				TargetDeviceNumber = "3",
				BinaryCommand = "25",
				BinaryData = "3"
			};

			var keyInfo4 = new KeyCommandInfo
			{
				TargetDeviceNumber = "4",
				BinaryCommand = "26",
				BinaryData = "25"
			};

			Assert.That(event1Binary, Is.EqualTo(keyInfo1).Using(new KeyInfoComparer()));
			Assert.That(event2Binary, Is.EqualTo(keyInfo2).Using(new KeyInfoComparer()));
			Assert.That(event3Binary, Is.EqualTo(keyInfo3).Using(new KeyInfoComparer()));
			Assert.That(event4Binary, Is.EqualTo(keyInfo4).Using(new KeyInfoComparer()));
		}


		[Test]
		public void InitialStateTest()
		{
			Assert.IsFalse(_key.HasUnsavedChanges);
			Assert.IsFalse(_key.IsAsciiMode);

			for (var i = 1; i < 5; i++)
			{
				Assert.AreEqual(i, _key.KeyCommandGrids[i].ID);
			}
		}


		[Test]
		public void UnsavedChangesTest()
		{
			_key.KeyCommandGrids[1].BinaryInfo.BinaryCommand = "99";
			Assert.IsTrue(_key.HasUnsavedChanges);
			_key.KeyCommandGrids[1].BinaryInfo.BinaryCommand = "21";
			Assert.IsFalse(_key.HasUnsavedChanges);

			_key.KeyCommandGrids[2].BinaryInfo.BinaryData = "99";
			Assert.IsTrue(_key.HasUnsavedChanges);
			_key.KeyCommandGrids[2].BinaryInfo.BinaryData = "500";
			Assert.IsFalse(_key.HasUnsavedChanges);

			_key.KeyCommandGrids[3].BinaryInfo.TargetDeviceNumber = "99";
			Assert.IsTrue(_key.HasUnsavedChanges);
			_key.KeyCommandGrids[3].BinaryInfo.TargetDeviceNumber = "3";
			Assert.IsFalse(_key.HasUnsavedChanges);

			_key.KeyCommandGrids[4].BinaryInfo.BinaryCommand = "99";
			Assert.IsTrue(_key.HasUnsavedChanges);
			_key.KeyCommandGrids[4].BinaryInfo.BinaryCommand = "26";
			Assert.IsFalse(_key.HasUnsavedChanges);
		}


		private static void Initialize(KeyVM aKeyVM)
		{
			var keyCommands1 = new DataPacket
			{
				DeviceNumber = 1,
				Command = Command.MoveRelative,
				NumericData = -500
			};

			var keyCommands2 = new DataPacket
			{
				DeviceNumber = 2,
				Command = Command.MoveAbsolute,
				NumericData = 500
			};

			var keyCommands3 = new DataPacket
			{
				DeviceNumber = 3,
				Command = Command.SetActiveAxis,
				NumericData = 3
			};

			var keyCommands4 = new DataPacket
			{
				DeviceNumber = 4,
				Command = Command.SetAxisDeviceNumber,
				NumericData = 25
			};

			aKeyVM.ParseKeyInfoBinary(keyCommands1, 1);
			aKeyVM.ParseKeyInfoBinary(keyCommands2, 2);
			aKeyVM.ParseKeyInfoBinary(keyCommands3, 3);
			aKeyVM.ParseKeyInfoBinary(keyCommands4, 4);
		}


		public KeyVM _key;
	}
}

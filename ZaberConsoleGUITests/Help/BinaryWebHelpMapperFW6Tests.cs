﻿using NUnit.Framework;
using ZaberConsole.Help;

namespace ZaberConsoleGUITests.Help
{
	[TestFixture]
	[SetCulture("en-US")]
	public class BinaryWebHelpMapperFW6Tests
	{
		[SetUp]
		public void Setup()
		{
			var mapper = new BinaryWebHelpMapperFW6(false);
			mapper.WaitForDocumentLoad();
			_mapper = mapper;
		}


		[Test]
		[Description("FW6 Binary help finds selected command links.")]
		public void TestCommands()
		{
			Assert.IsFalse(_mapper.GetProfilingData().HasValue);
			Assert.IsTrue(_mapper.GetCommandHelpPath("1").EndsWith("#Home_-_Cmd_1"));
			Assert.IsTrue(_mapper.GetCommandHelpPath("22").EndsWith("#Move_At_Constant_Speed_-_Cmd_22"));
			Assert.IsNull(_mapper.GetCommandHelpPath("3"));
		}


		[Test]
		[Description("FW6 Binary help finds selected setting links.")]
		public void TestSettings()
		{
			Assert.IsTrue(_mapper.GetSettingHelpPath("43").EndsWith("#Set_Acceleration_-_Cmd_43"));
			Assert.IsTrue(_mapper.GetSettingHelpPath("70").EndsWith("#Return_Digital_Output_Count_-_Cmd_70"));
			Assert.IsNull(_mapper.GetSettingHelpPath("3"));
		}


		private IWebHelpMapper _mapper;
	}
}

﻿using System;
using System.IO;
using NUnit.Framework;
using ZaberConsole.Help;
using ZaberTest;
using ZaberTest.Testing;

namespace ZaberConsoleGUITests.Help
{
	[TestFixture]
	[SetCulture("en-US")]
	public class WebHelpUtilsTests
	{
		[SetUp]
		public void Setup()
		{
			// This name deliberately contains all characters that are legal in Windows
			// file names but not in URLs.
			var destDir = Path.Combine(TestDataFileHelper.GetDeployedPath("."), "{(help)+[file]}, test & data");
			Directory.CreateDirectory(destDir);
			_helpDeployed = Path.Combine(destDir, FILENAME);
			File.Copy(_helpSource, _helpDeployed);
		}


		[TearDown]
		public void Teardown() => FileSystemHelper.DeleteDirectoryRecursive(Path.GetDirectoryName(_helpDeployed));


		[Test]
		public void TestLoadFromNonUrlSafePath()
		{
			var uri = new Uri(Path.GetFullPath(_helpDeployed)).AbsoluteUri;
			var doc = WebHelpUtils.ReadHtmlDocument(uri);
			Assert.IsNotNull(doc);
		}


		private static readonly string FILENAME = "ascii-7.html";
		private readonly string _helpSource = TestDataFileHelper.GetDeployedPath(Path.Combine("data", FILENAME));
		private string _helpDeployed;
	}
}

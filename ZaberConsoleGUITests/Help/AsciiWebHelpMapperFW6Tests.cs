﻿using NUnit.Framework;
using ZaberConsole.Help;

namespace ZaberConsoleGUITests.Help
{
	[TestFixture]
	[SetCulture("en-US")]
	public class AsciiWebHelpMapperFW6Tests
	{
		[SetUp]
		public void Setup()
		{
			var mapper = new AsciiWebHelpMapperFW6(false);
			mapper.WaitForDocumentLoad();
			_mapper = mapper;
		}


		[Test]
		[Description("FW6 ASCII help finds selected command links.")]
		public void TestCommands()
		{
			Assert.IsFalse(_mapper.GetProfilingData().HasValue);
			Assert.IsTrue(_mapper.GetCommandHelpPath("home").EndsWith("#home"));
			Assert.IsTrue(_mapper.GetCommandHelpPath("trigger dist").EndsWith("#trigger_dist"));
			Assert.IsTrue(_mapper.GetCommandHelpPath("trigger foo").EndsWith("#trigger"));
			Assert.IsNull(_mapper.GetCommandHelpPath("foo bar"));
		}


		[Test]
		[Description("FW6 ASCII help finds selected setting links.")]
		public void TestSettings()
		{
			Assert.IsTrue(_mapper.GetSettingHelpPath("deviceid").EndsWith("#deviceid"));
			Assert.IsTrue(_mapper.GetSettingHelpPath("motion.index.dist").EndsWith("#motion.index.dist"));
			Assert.IsNull(_mapper.GetSettingHelpPath("foo.bar"));
		}


		private IWebHelpMapper _mapper;
	}
}

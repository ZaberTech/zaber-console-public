﻿using System.Collections.Generic;
using NUnit.Framework;
using ZaberConsole.Help;

namespace ZaberConsoleGUITests.Help
{
	[TestFixture]
	[SetCulture("en-US")]
	public class AsciiWebHelpMapperFW7Tests
	{
		[SetUp]
		public void Setup()
		{
			var paths = new Dictionary<string, string>
			{
				{ "home", "home" },
				{ "trigger x action x none", "trigger.number.action.none" },
				{ "trigger", "trigger" }
			};

			var mapper = new AsciiWebHelpMapperFW7(false, ("foo", new object[] { "bar" }), paths);
			mapper.WaitForDocumentLoad();
			_mapper = mapper;
		}


		[Test]
		[Description("FW7 Binary help finds selected command links.")]
		public void TestCommands()
		{
			var ditaVals = _mapper.GetProfilingData();
			Assert.IsTrue(ditaVals.HasValue);
			Assert.AreEqual("foo", ditaVals.Value.Item1);
			Assert.AreEqual("bar", ditaVals.Value.Item2[0]);
			Assert.IsTrue(_mapper.GetCommandHelpPath("home").EndsWith("#topic_command_home"));
			Assert.IsTrue(_mapper.GetCommandHelpPath("trigger x action x none")
							  .EndsWith("#topic_command_trigger_action_none"));
			Assert.IsTrue(_mapper.GetCommandHelpPath("trigger x action").EndsWith("#topic_command_trigger"));
			Assert.IsNull(_mapper.GetCommandHelpPath("foo bar"));
		}


		[Test]
		[Description("FW7 Binary help finds selected setting links.")]
		public void TestSettings()
		{
			Assert.IsTrue(_mapper.GetSettingHelpPath("accel").EndsWith("#topic_setting_accel"));
			Assert.IsTrue(_mapper.GetSettingHelpPath("motion.index.dist").EndsWith("#topic_setting_motion_index_dist"));
			Assert.IsNull(_mapper.GetSettingHelpPath("foo.bar"));
		}


		private IWebHelpMapper _mapper;
	}
}

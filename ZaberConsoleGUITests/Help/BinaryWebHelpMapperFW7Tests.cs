﻿using NUnit.Framework;
using ZaberConsole.Help;

namespace ZaberConsoleGUITests.Help
{
	[TestFixture]
	[SetCulture("en-US")]
	public class BinaryWebHelpMapperFW7Tests
	{
		[SetUp]
		public void Setup()
		{
			var mapper = new BinaryWebHelpMapperFW7(false, ("foo", new object[] { "bar" }));
			mapper.WaitForDocumentLoad();
			_mapper = mapper;
		}


		[Test]
		[Description("FW7 Binary help finds selected command links.")]
		public void TestCommands()
		{
			var ditaVals = _mapper.GetProfilingData();
			Assert.IsTrue(ditaVals.HasValue);
			Assert.AreEqual("foo", ditaVals.Value.Item1);
			Assert.AreEqual("bar", ditaVals.Value.Item2[0]);
			Assert.IsTrue(_mapper.GetCommandHelpPath("1").EndsWith("#topic_action_001_home"));
			Assert.IsTrue(_mapper.GetCommandHelpPath("22").EndsWith("#topic_action_022_move_at_constant_speed"));
			Assert.IsNull(_mapper.GetCommandHelpPath("3"));
		}


		[Test]
		[Description("FW7 Binary help finds selected setting links.")]
		public void TestSettings()
		{
			Assert.IsTrue(_mapper.GetSettingHelpPath("43").EndsWith("#topic_set_043_acceleration"));
			Assert.IsTrue(_mapper.GetSettingHelpPath("79").EndsWith("#topic_set_079_index_distance"));
			Assert.IsNull(_mapper.GetSettingHelpPath("3"));
		}


		private IWebHelpMapper _mapper;
	}
}

﻿using System.IO;
using NUnit.Framework;
using TickingTest;
using ZaberConsole;

namespace ZaberConsoleTest
{
	[TestFixture]
	[SetCulture("en-US")]
	public class BlockingStreamTest
	{
		[Test]
		public void Read() => TestFramework.RunOnce(new ReadThreads());


		private class ReadThreads : MultithreadedTestCase
		{
			#region -- Public Methods & Properties --

			[TestThread]
			public void Read()
			{
				// SETUP
				_stream = new BlockingStream(new MemoryStream());
				var reader = new StreamReader(_stream);

				// EXEC
				var actualText = reader.ReadLine();

				// VERIFY
				AssertTick(1);
				Assert.AreEqual(EXPECTED_TEXT,
								actualText,
								"text");
			}


			[TestThread]
			public void Write()
			{
				// SETUP
				WaitForTick(1);
				var writer = new StreamWriter(_stream);
				writer.AutoFlush = true;

				// EXEC
				writer.WriteLine(EXPECTED_TEXT);
			}

			#endregion

			#region -- Data --

			private const string EXPECTED_TEXT = "Watson, come here!";
			private Stream _stream;

			#endregion
		}
	}
}

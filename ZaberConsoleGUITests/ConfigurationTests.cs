﻿using System.IO;
using NUnit.Framework;
using ZaberConsole;

namespace ZaberConsoleGUITests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class ConfigurationTests
	{
		[Test]
		public void IsUnderProgramInstallDir()
		{
			Assert.IsNotEmpty(Configuration.ProgramInstallDir, "Sanity check");

			var installDir = Configuration.ProgramInstallDir;
			Assert.IsTrue(Configuration.IsInProgramInstallDir(installDir));
			Assert.IsFalse(Configuration.IsInProgramInstallDir(Path.GetPathRoot(installDir)));
			Assert.IsTrue(Configuration.IsInProgramInstallDir(Path.Combine(installDir, "foo")));
			Assert.IsTrue(Configuration.IsInProgramInstallDir(Path.Combine(installDir, "foo", "bar")));
		}
	}
}

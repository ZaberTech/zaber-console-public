﻿namespace ZaberConsole.Plugins.Oscilloscope
{
	/// <summary>
	///     Holds constants for each movement type.
	/// </summary>
	public enum MoveType
	{
		/// <summary>
		///     Relative movement type.
		/// </summary>
		Relative,

		/// <summary>
		///     Absolute movement type.
		/// </summary>
		Absolute,

		/// <summary>
		///     Force movement type.
		/// </summary>
		Force
	}
}
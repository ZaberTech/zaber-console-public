﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using DataAccess;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.Oscilloscope
{
	/// <summary>
	///     ViewModel for one oscilloscope capture - contains one or more traces, and can
	///     be exported to disk.
	/// </summary>
	public class PlotVM : ObservableObject
	{
		/// <summary>
		///     Exports the data in this plot to a CSV file. The output file is always overwritten.
		/// </summary>
		/// <param name="aPath">Absolute or relative path to the file to write to.</param>
		public void Export(string aPath)
		{
			// For free-running scope captures each data point might have a different
			// time index. Create a unified list of samples grouped by channel and then
			// order them by time, leaving blanks for rows without samples.
			var corr = new Dictionary<double, string[]>();
			for (var i = 0; i < DataSeries.Count; ++i)
			{
				foreach (var point in DataSeries[i].GetConvertedData())
				{
					if (!corr.ContainsKey(point.X))
					{
						corr[point.X] = new string[DataSeries.Count];
					}

					corr[point.X][i] = point.Y.ToString();
				}
			}

			var sorted = corr.OrderBy(p => p.Key).ToArray();

			// Create spreadsheet columns.
			var columns = new Column[1 + DataSeries.Count];
			columns[0] = new Column("Time (ms)", sorted.Length);
			for (var i = 0; i < DataSeries.Count; ++i)
			{
				var series = DataSeries[i];
				var colName = series.SelectedSetting.TextCommand;
				if (null != series.SelectedUnit)
				{
					colName += " (" + series.SelectedUnit.Abbreviation + ")";
				}

				columns[1 + i] = new Column(colName, sorted.Length);
			}

			// Populate the columns with the data.
			for (var i = 0; i < sorted.Length; ++i)
			{
				var sample = sorted[i];
				columns[0].Values[i] = sample.Key.ToString();
				for (var j = 0; j < sample.Value.Length; j++)
				{
					columns[1 + j].Values[i] = sample.Value[j];
				}
			}

			// Write the data to the file.
			var data = new MutableDataTable { Columns = columns };
			data.SaveCSV(aPath);
		}


		/// <summary>
		///     Trigger a full data update of all lines in the plot.
		/// </summary>
		public void TriggerRefresh() => RefreshTrigger++;


		/// <summary>
		///     Timestamp for when the data was captured.
		/// </summary>
		public DateTime CaptureTime { get; set; }

		#region -- View data --

		/// <summary>
		///     Button for when the center graph button is clicked.
		/// </summary>
		public RelayCommand CenterGraphCommand => new RelayCommand(x => { CenterGraph += 1; });


		/// <summary>
		///     Integer flag that when raised, centers the graph.
		/// </summary>
		public int CenterGraph
		{
			get => _counter;
			set => Set(ref _counter, value, nameof(CenterGraph));
		}


		/// <summary>
		///     Integer flag that when raised, centers the graph.
		/// </summary>
		public int PanGraph
		{
			get => _counterPan;
			set => Set(ref _counterPan, value, nameof(PanGraph));
		}


		/// <summary>
		///     Observable property for views to sync Title.
		/// </summary>
		public string Title
		{
			get => _title;
			set => Set(ref _title, value, nameof(Title));
		}


		/// <summary>
		///     Observable property for views to sync DataSeries.
		/// </summary>
		public ObservableCollection<ChartSeries> DataSeries
		{
			get => _dataSeries;
			set => Set(ref _dataSeries, value, nameof(DataSeries));
		}


		/// <summary>
		///     Change this value to trigger a graph data update.
		/// </summary>
		public int RefreshTrigger
		{
			get => _refreshTrigger;
			set => Set(ref _refreshTrigger, value, nameof(RefreshTrigger));
		}

		#endregion

		#region -- Fields --

		private string _title;
		private int _counter;
		private int _counterPan;
		private int _refreshTrigger;

		private ObservableCollection<ChartSeries> _dataSeries = new ObservableCollection<ChartSeries>();

		#endregion
	}
}

﻿using System;
using System.Windows.Input;
using MvvmDialogs.ViewModels;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.Oscilloscope
{
	/// <summary>
	///     Provides implementation for a user-input box.
	/// </summary>
	public class InputBoxVM : ObservableObject, IUserDialogViewModel
	{
		/// <summary>
		///     The string in which the user's response is stored.
		/// </summary>
		public string ResponseText
		{
			get => _responseText;
			set => Set(ref _responseText, value, nameof(ResponseText));
		}


		/// <summary>
		///     The title of the input box window.
		/// </summary>
		public string Title
		{
			get => _title;
			set => Set(ref _title, value, nameof(Title));
		}


		/// <summary>
		///     The result of the dialog box. True if the OK button was clicked.
		/// </summary>
		public bool DialogResult { get; set; }


		/// <summary>
		///     The command to be called when the "OK" button is clicked.
		/// </summary>
		public ICommand ClickCommand => OnDemandRelayCommand(ref _okCommand,
															 _ =>
															 {
																 DialogResult = true;
																 RequestClose();
															 });

		/// <summary>
		///     Handles exceptions not caught by the InputBox.
		/// </summary>
		public Exception Exception { get; set; }


		/// <summary>
		///     Determines whether the dialog is Modal or not.
		/// </summary>
		public bool IsModal => true;


		/// <summary>
		///     The event fired when the dialog is closed.
		/// </summary>
		public event EventHandler DialogClosing;


		/// <summary>
		///     The event fired when the dialog is brought forwards to the front.
		/// </summary>
		public event EventHandler BringToFront;


		/// <summary>
		///     Requests the dialog to close.
		/// </summary>
		public void RequestClose() => DialogClosing?.Invoke(this, null);


		/// <summary>
		///     Requests bringing the dialogue forwards.
		/// </summary>
		public void RequestBringToFront() => BringToFront?.Invoke(this, null);


		private string _responseText;
		private string _title;
		private RelayCommand _okCommand;
	}
}

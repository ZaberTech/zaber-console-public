﻿using System;
using System.Windows.Media;

namespace ZaberConsole.Plugins.Oscilloscope
{
	/// <summary>
	///     Helper class to provide random color generation for graphing.
	/// </summary>
	public class ColorHelper
	{
		/// <summary>
		///     Returns a random color.
		/// </summary>
		/// <returns>A random color.</returns>
		public static Color GetRandomColor()
		{
			var c = Color.FromRgb(Convert.ToByte(rnd.Next(256)),
								  Convert.ToByte(rnd.Next(256)),
								  Convert.ToByte(rnd.Next(256)));
			return c;
		}


		/// <summary>
		///     Returns a setting-specific color calculated from a variable's hash.
		/// </summary>
		/// <param name="setting">The setting for which to get a color from.</param>
		/// <returns></returns>
		public static Color GetColorFromSetting(string setting)
		{
			var hashI = (setting ?? string.Empty).GetHashCode();

			// Get the first digit in the hash.
			var i = hashI;
			while (i >= 10)
			{
				i /= 10;
			}

			// Shift the hash to the right by that amount.
			hashI = hashI >> i;

			// Ensure that the hash has the correct number of values to be a hexadecimal.
			var hashS = "#" + Convert.ToString(hashI, 16).PadLeft(8, '0');

			// Convert the hexadecimal into a color.
			var col = (Color) ColorConverter.ConvertFromString(hashS);

			// Do some randomization to encourage less bright hues.
			col.A = 255;
			col.B = Clamp(Convert.ToInt32(col.B * 1));
			col.R = Clamp(Convert.ToInt32(col.R * 1));
			col.G = Clamp(Convert.ToInt32(col.G * 0.75));

			return col;
		}


		// Clamps a channel value.
		private static byte Clamp(int valueToClamp) => (byte) Math.Min(Math.Max(valueToClamp, 0), 250);


		private static readonly Random rnd = new Random();
	}
}

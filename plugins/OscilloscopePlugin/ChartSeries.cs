using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Media;
using OxyPlot;
using Xceed.Wpf.Toolkit;
using Zaber;
using Zaber.Units;
using ZaberWpfToolbox;
using Measurement = Zaber.Units.Measurement;

namespace ZaberConsole.Plugins.Oscilloscope
{
	/// <summary>
	///     Zaber representation of one data series (one line) for a chart.
	/// </summary>
	public class ChartSeries : ObservableObject
	{
		#region -- Configuration --

		/// <summary>
		///     Initializes the series with a data limit.
		/// </summary>
		/// <param name="startCaptureTime">The starting time of the capture.</param>
		/// <param name="timeRange">The time upon which data older than shall be removed.</param>
		public ChartSeries(DateTime startCaptureTime, int timeRange)
		{
			_timeSpanToKeep = 1000 * timeRange;
			_startCaptureTime = startCaptureTime;
			_rawData = new List<Tuple<double, double>>();
			_convertedData = new List<DataPoint>();
		}


		/// <summary>
		///     Append new data to the sample series. This may cause old data to be discarded.
		/// </summary>
		/// <param name="aNewData"></param>
		public void Append(IEnumerable<Tuple<double, double>> aNewData)
		{
			var converted = Convert(aNewData);

			lock (_lock)
			{
				_rawData.AddRange(aNewData);
				_convertedData.AddRange(converted);
			}
		}


		/// <summary>
		///     Clears collected data.
		/// </summary>
		public void Clear()
		{
			lock (_lock)
			{
				_rawData.Clear();
				_convertedData.Clear();
			}
		}


		/// <summary>
		///     Expires all data older than the setting specified in the oscilloscope capture settings.
		/// </summary>
		public void Expire()
		{
			var current = DateTime.UtcNow;
			var elapsedMs = (current - _startCaptureTime).TotalMilliseconds;

			lock (_lock)
			{
				_rawData = _rawData.SkipWhile(o => o.Item1 < (elapsedMs - _timeSpanToKeep)).ToList();
				_convertedData = _convertedData.SkipWhile(o => o.X < (elapsedMs - _timeSpanToKeep)).ToList();
			}
		}


		/// <summary>
		///     Raw data returned by the device. Used as the source for unit conversions on the plots.
		///     This is an ordered list of x,y pairs, where x is the time index and y is the sample value.
		/// </summary>
		public IEnumerable<Tuple<double, double>> GetRawData()
		{
			lock (_lock)
			{
				return _rawData.ToArray();
			}
		}


		/// <summary>
		///     Retrieve the unit converted data.
		/// </summary>
		public IEnumerable<DataPoint> GetConvertedData()
		{
			lock (_lock)
			{
				return _convertedData.ToArray();
			}
		}

		#endregion

		#region -- View data --

		/// <summary>
		///     If true, the specific row is a dummy channel.
		/// </summary>
		public bool IsDummy
		{
			get => _isDummy;
			set => Set(ref _isDummy, value, nameof(IsDummy));
		}


		/// <summary>
		///     Post-capture toggle for showing or hiding the plot.
		/// </summary>
		public bool Visible
		{
			get => _visible;
			set => Set(ref _visible, value, nameof(Visible));
		}


		/// <summary>
		///     Pre-capture toggle for whether or not to capture this setting.
		/// </summary>
		public bool DoCapture
		{
			get => _doCapture;
			set => Set(ref _doCapture, value, nameof(DoCapture));
		}


		/// <summary>
		///     Currently selected setting.
		/// </summary>
		public SettingInfoWrapper SelectedSetting
		{
			get => _selectedSetting;
			set
			{
				Set(ref _selectedSetting, value, nameof(SelectedSetting));

				if (Colors.White == _selectedColor)
				{
					OnPropertyChanged(nameof(SelectedColor)); // Trigger update of name-based color.
				}

				PopulateUnits();
			}
		}


		/// <summary>
		///     Observable property for views to sync Units.
		/// </summary>
		public ObservableCollection<UnitOfMeasure> Units
		{
			get => _units;
			set => Set(ref _units, value, nameof(Units));
		}


		/// <summary>
		///     Observable property for views to sync SelectedUnit.
		/// </summary>
		public UnitOfMeasure SelectedUnit
		{
			get => _selectedUnit;
			set
			{
				_selectedUnit = value;

				// Update the unit conversion function.
				_convertFunc = null;
				if ((null != _selectedSetting) && (null != _selectedUnit))
				{
					_convertFunc = _device
							   ?.GetConversionToParameterUnits(_selectedUnit.Unit,
															   _selectedSetting.Setting.GetResponseConversion())
							   ?.Reverse();
				}

				// Convert any existing data.
				lock (_lock)
				{
					var newConverted = Convert(_rawData).ToList();
					_convertedData = newConverted;
				}

				// Fire the notification after converting the data because subscribers might try to read it immediately.
				OnPropertyChanged(nameof(SelectedUnit));
			}
		}


		/// <summary>
		///     The standard colors to show in the channels view for each channel.
		/// </summary>
		public static ObservableCollection<ColorItem> StandardColors { get; set; } = new ObservableCollection<ColorItem>
		{
			new ColorItem(Colors.Red, "Red"),
			new ColorItem(Colors.Blue, "Blue"),
			new ColorItem(Colors.Yellow, "Yellow"),
			new ColorItem(Colors.Green, "Green"),
			new ColorItem(Colors.Pink, "Pink"),
			new ColorItem(Colors.Orange, "Orange"),
			new ColorItem(Colors.Purple, "Purple"),
			new ColorItem(Colors.Black, "Black"),
			new ColorItem(Colors.Gray, "Gray"),
			new ColorItem(Colors.Cyan, "Cyan")
		};


		/// <summary>
		///     The currently selected color in the color picker.
		/// </summary>
		public Color SelectedColor
		{
			get
			{
				if (Colors.White == _selectedColor)
				{
					return ColorHelper.GetColorFromSetting(SelectedSetting?.TextCommand);
				}

				return _selectedColor;
			}
			set => Set(ref _selectedColor, value, nameof(SelectedColor));
		}


		/// <summary>
		///     The fill color for the bullet point in the plot's legend.
		/// </summary>
		public Brush FillColor
		{
			get => _fillColor;
			set => Set(ref _fillColor, value, nameof(FillColor));
		}


		/// <summary>
		///     Populates the units array.
		/// </summary>
		public void PopulateUnits()
		{
			if ((null == _device?.UnitConverter) || (null == SelectedSetting))
			{
				Units = null;
				SelectedUnit = null;
				return;
			}

			var settingInfo = SelectedSetting;
			if (null != settingInfo?.Setting.RequestUnit)
			{
				var u = (_device?.UnitConverter.FindUnitsByDimension(settingInfo.Setting.RequestUnit.Unit.Dimension))
				.Where(o => (o.Abbreviation != "step/dT²") && (o.Abbreviation != "step"));
				Units = new ObservableCollection<UnitOfMeasure>(u);
				SortUnits();
				SelectedUnit = Units.FirstOrDefault();
			}
			else
			{
				Units = new ObservableCollection<UnitOfMeasure>();
			}
		}

		#endregion

		#region -- Helpers --

		/// <summary>
		///     Configure the list of available settings and the unit conversion function from a
		///     device instance.
		/// </summary>
		/// <param name="aDevice">Device to associate this data series with. A reference to this device is stored.</param>
		public void PopulateSettingsAndUnits(ZaberDevice aDevice)
		{
			if ((null == aDevice) || aDevice is DeviceCollection)
			{
				SelectedSetting = null;
				_device = null;
				_convertFunc = null;
				return;
			}

			_device = aDevice;
		}


		private void SortUnits()
		{
			var baseUnit = Units.First().Unit;
			var sortedQuantitiesList = new List<Tuple<double, UnitOfMeasure>>();

			foreach (var uom in Units)
			{
				var u = uom.Unit;
				var m = new Measurement(1, u);
				var mx = m.Value;

				if (m.Unit.TryConvertTo(baseUnit, ref mx))
				{
					sortedQuantitiesList.Add(new Tuple<double, UnitOfMeasure>(mx, uom));
				}
			}

			// Sort by conversion scale first, then alphabetically if scales are the same.
			sortedQuantitiesList.Sort((a, b) =>
			{
				var result = a.Item1.CompareTo(b.Item1);
				if (result == 0)
				{
					result = a.Item2.Abbreviation.CompareTo(b.Item2.Abbreviation);
				}

				return result;
			});

			Units = new ObservableCollection<UnitOfMeasure>(sortedQuantitiesList.Select(q => q.Item2));
		}


		private IEnumerable<DataPoint> Convert(IEnumerable<Tuple<double, double>> aNewData)
		{
			if (null != aNewData)
			{
				var settingInfo = SelectedSetting?.Setting;

				foreach (var d in aNewData)
				{
					var x = d.Item1;
					var y = _convertFunc?.Transform(d.Item2) ?? d.Item2;

					// TODO (Soleil 2018-03-14): Currently we are doing redundant unit conversions on
					// previously recorded data. Another performance improvement would be to go back to
					// caching already converted data, and only do conversions on new data - or find
					// a way to do incremental conversion in the view codebehind.
					yield return new DataPoint(x, y);
				}
			}
		}

		#endregion

		#region -- Fields --

		private readonly object _lock = new object();
		private bool _isDummy;
		private Color _selectedColor;
		private Brush _fillColor = new SolidColorBrush { Color = Color.FromRgb(255, 0, 0) };
		private readonly int _timeSpanToKeep = 1000;
		private ZaberDevice _device;
		private QuantityTransformation _convertFunc;
		private bool _visible = true;
		private bool _doCapture = true;
		private SettingInfoWrapper _selectedSetting;
		private ObservableCollection<UnitOfMeasure> _units;
		private UnitOfMeasure _selectedUnit;
		private List<Tuple<double, double>> _rawData;
		private List<DataPoint> _convertedData;
		private readonly DateTime _startCaptureTime;

		#endregion
	}
}

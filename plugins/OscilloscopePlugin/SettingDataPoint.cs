﻿namespace ZaberConsole.Plugins.Oscilloscope
{
	/// <summary>
	///     Provides implementation for storing a data point of collected data.
	/// </summary>
	public class SettingDataPoint
	{
		/// <summary>
		///     The setting name that this data point corresponds to.
		/// </summary>
		public string SettingName { get; set; }


		/// <summary>
		///     The X data point.
		/// </summary>
		public double X { get; set; }


		/// <summary>
		///     The Y data point.
		/// </summary>
		public double Y { get; set; }
	}
}

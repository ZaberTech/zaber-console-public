﻿using System;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.Oscilloscope
{
	/// <summary>
	///     A specified trigger mode.
	/// </summary>
	public enum TriggerMode
	{
		/// <summary>
		///     Manual trigger type.
		/// </summary>
		MANUAL,

		/// <summary>
		///     Continuous capture type.
		/// </summary>
		CONTINUOUS
	}

	/// <summary>
	///     Implements a base class for all trigger types.
	/// </summary>
	[Serializable]
	public class TriggerType : ObservableObject
	{
		/// <summary>
		///     The TriggerMode type.
		/// </summary>
		public TriggerMode TriggerMode { get; set; }


		/// <summary>
		///     The Trigger Type's name to be displayed in the combo box.
		/// </summary>
		public string Name { get; set; }


		/// <summary>
		///     Returns true if the trigger mode is selectable.
		/// </summary>
		public bool IsSelectable
		{
			get => _isSelectable;
			set => Set(ref _isSelectable, value, nameof(IsSelectable));
		}


		/// <summary>
		///     The trigger type's tooltip to display in the combo box.
		/// </summary>
		public string ToolTip
		{
			get => _toolTip;
			set => Set(ref _toolTip, value, nameof(ToolTip));
		}


		private bool _isSelectable = true;
		private string _toolTip;
	}
}

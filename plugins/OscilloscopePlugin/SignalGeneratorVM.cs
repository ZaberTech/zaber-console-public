﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Zaber;
using Zaber.Telemetry;
using Zaber.Units;
using Zaber.Units.UI.WPF.Controls;
using ZaberConsole.Plugins.Oscilloscope.Settings;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.Oscilloscope
{
	/// <summary>
	///     Oscilloscope helper class for managing signal generation tasks.
	/// </summary>
	public class SignalGeneratorVM : ObservableObject, ISignalGenerator
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Restore the model's state from saved settings.
		///     This is called at init time, after the conversation is changed,
		///     and after settings are reset.
		/// </summary>
		/// <param name="aSettings">Previous settings for this class; can be null.</param>
		public void LoadSettings(OscilloscopeSignalSettings aSettings)
		{
			// Selected device may have changed.
			PopulateMovementTypes();

			if (aSettings is null)
			{
				return;
			}

			// Load saved settings or create defaults.
			var uc = Conversation?.Device.UnitConverter.UnitConverter ?? UnitConverter.Default;
			var isRotary = Conversation?.Device.DeviceType.IsRotaryDevice ?? false;

			_abs.MeasurementSettings = aSettings.AbsMeasurementSettings;
			_rel.MeasurementSettings = aSettings.RelMeasurementSettings;
			_force.MeasurementSettings = aSettings.ForceMeasurementSettings;

			// Ensure loaded settings are compatible with the selected device.
			if (null != Conversation)
			{
				void ValidateSavedUnitSymbol(MovementType aMove)
				{
					if (!uc.FindUnits(aMove.Dimension)
						.Select(u => u.Abbreviation)
						.Contains(aMove.MeasurementSettings.SavedUnitAbbreviation))
					{
						var measurement = aMove.MeasurementSettings;
						measurement.SavedUnitAbbreviation = null;
						aMove.MeasurementSettings = measurement;
					}
				}

				ValidateSavedUnitSymbol(_abs);
				ValidateSavedUnitSymbol(_rel);
				ValidateSavedUnitSymbol(_force);
			}

			// Restore the last selected move type.
			switch (aSettings.MovementType)
			{
				case MoveType.Absolute:
					_selectedMovementType = _abs;
					break;

				case MoveType.Force:
					_selectedMovementType = _force;
					break;

				case MoveType.Relative:
					_selectedMovementType = _rel;
					break;
			}

			OnPropertyChanged(nameof(SelectedMovementType));

			// Repopulate the signal measurement box.
			var stepMeasurement = CreateSignalMeasurementBox(aSettings.MovementType, aSettings.StepSizeUnits);
			stepMeasurement.Quantity = aSettings.StepSize;
			SelectedSignalStepMeasurement = stepMeasurement;

			IsReturnToStartChecked = aSettings.ReturnToStart;
		}


		/// <summary>
		///     Save the current state of the model to a settings object.
		/// </summary>
		/// <returns>A new instance of the serializable settings class for this model.</returns>
		public OscilloscopeSignalSettings SaveSettings()
		{
			var oss = new OscilloscopeSignalSettings
			{
				MovementType = SelectedMovementType?.MoveMode ?? MoveType.Relative,
				StepSize = SelectedSignalStepMeasurement?.Quantity ?? 1m,
				StepSizeUnits = SelectedSignalStepMeasurement?.SelectedUnit?.Abbreviation ?? UnitOfMeasure.CentimeterSymbol,
				ReturnToStart = IsReturnToStartChecked,
				RelMeasurementSettings = _rel.MeasurementSettings,
				AbsMeasurementSettings = _abs.MeasurementSettings,
				ForceMeasurementSettings = _force.MeasurementSettings
			};


			return oss;
		}


		/// <summary>
		///     Do any preparation needed before starting signal generation. This must be
		///     called by the data recorder before calling <see cref="Start" />.
		/// </summary>
		public void Prepare()
		{
			_returnPosition = null;

			var device = Conversation?.Device;
			if (null == device)
			{
				return;
			}

			// Save the return position if return to start is enabled.
			if (IsReturnToStartChecked)
			{
				_returnPosition = Conversation.Request("get pos").NumericData;
			}
		}


		/// <summary>
		///     Stgarts generating signals; called when scope capture starts.
		/// </summary>
		public void Start()
		{
			var device = Conversation?.Device;
			if (device is null || _selectedMovementType is null)
			{
				return;
			}

			if (_isGenerating)
			{
				throw new InvalidOperationException("Signal generator was started twice in a row.");
			}

			// Generate and send the signal.
			_isGenerating = true;
			string cmd = null;
			switch (_selectedMovementType.MoveMode)
			{
				case MoveType.Absolute:
					cmd = device.FormatAsciiCommand("move abs {0}", SelectedSignalStepMeasurement.Measurement);
					break;

				case MoveType.Relative:
					cmd = device.FormatAsciiCommand("move rel {0}", SelectedSignalStepMeasurement.Measurement);
					break;

				case MoveType.Force:
					cmd = device.FormatAsciiCommand("force abs {0}", SelectedSignalStepMeasurement.Measurement);
					break;
			}

			_eventLog.LogEvent("Signal generator used", cmd);

			var topic = Conversation.StartTopic();
			Conversation.Send(cmd, topic.MessageId);
		}


		/// <summary>
		///     Halt signal generation. This may trigger a return to the start position.
		///     Note this does not send a stop command because velocity moves are currently
		///     not supported as signals.
		/// </summary>
		public void Stop()
		{
			if (_isGenerating && IsReturnToStartChecked && _returnPosition.HasValue)
			{
				Conversation?.Request("move abs", _returnPosition.Value);
			}

			_isGenerating = false;
		}


		/// <summary>
		///     Conversation with the selected device.
		///     Note no actions are triggered by the setter because we are
		///     relying on the parent always calling LoadSettings() after the
		///     value changes.
		/// </summary>
		public Conversation Conversation { get; set; }

		/// <summary>
		///     List of Movement Types to display in the step-mode signal source combo box.
		/// </summary>
		public ObservableCollection<MovementType> MovementTypes
		{
			get => _movementTypes;
			private set => Set(ref _movementTypes, value, nameof(MovementTypes));
		}


		/// <summary>
		///     The currently selected Movement Type in the signal generator controls.
		/// </summary>
		public MovementType SelectedMovementType
		{
			get => _selectedMovementType;
			set
			{
				// Save previously entered values.
				if (null != _selectedMovementType)
				{
					var measurement = new MeasurementSettings
					{
						SavedUnitAbbreviation = SelectedSignalStepMeasurement.SelectedUnit?.Abbreviation,
						SavedQuantity = SelectedSignalStepMeasurement.Quantity
					};

					_selectedMovementType.MeasurementSettings = measurement;
				}

				Set(ref _selectedMovementType, value, nameof(SelectedMovementType));

				var mv = CreateSignalMeasurementBox(_selectedMovementType.MoveMode,
													_selectedMovementType.MeasurementSettings.SavedUnitAbbreviation);
				mv.Quantity = _selectedMovementType.MeasurementSettings.SavedQuantity;
				SelectedSignalStepMeasurement = mv;
			}
		}


		/// <summary>
		///     The selected Movement Type's unit of measurement
		/// </summary>
		public MeasurementBoxVM SelectedSignalStepMeasurement
		{
			get => _unitType;
			set => Set(ref _unitType, value, nameof(SelectedSignalStepMeasurement));
		}


		/// <summary>
		///     The checkbox value controlling whether or not to return to the original position.
		/// </summary>
		public bool IsReturnToStartChecked
		{
			get => _isReturnToStartChecked;
			set => Set(ref _isReturnToStartChecked, value, nameof(IsReturnToStartChecked));
		}

		#endregion

		#region -- Non-Public Methods --

		// Strip out units too large or small to be useful.
		private IEnumerable<Unit> FilterOutLargeOrSmallUnits(Dimension aDimension, IEnumerable<Unit> aUnitList)
		{
			// Somewhat arbitrary choices based on dimension.
			if (Dimension.Length == aDimension)
			{
				return UnitUtils.DiscardExtremeUnits(aUnitList, -12, 3);
			}
			if (Dimension.Angle == aDimension)
			{
				return UnitUtils.DiscardExtremeUnits(aUnitList, -3, 3);
			}
			else // Force
			{
				return UnitUtils.DiscardExtremeUnits(aUnitList, -6, 6);
			}
		}


		private MeasurementBoxVM CreateSignalMeasurementBox(MoveType aMoveType, string aDefaultUnitSymbol)
		{
			MeasurementBoxVM measurementBox = null;
			IEnumerable<Unit> units = null;

			var isRotary = Conversation?.Device.DeviceType.IsRotaryDevice ?? false;
			IEnumerable<Prefix> prefixes = null;
			var defaultDimension = isRotary ? Dimension.Angle : Dimension.Length;

			switch (aMoveType)
			{
				case MoveType.Absolute:
					prefixes = _usefulPrefixesDistance;
					break;

				case MoveType.Relative:
					prefixes = _usefulPrefixesDistance;
					break;

				case MoveType.Force:
					prefixes = _usefulPrefixesForce;
					defaultDimension = isRotary ? Dimension.Torque : Dimension.Force;
					break;

				default:
					throw new ArgumentOutOfRangeException(nameof(aMoveType), aMoveType, "Selected signal type is not supported.");
			}

			var uc = Conversation?.Device.UnitConverter.UnitConverter ?? UnitConverter.Default;

			var moveCommands = Conversation?.Device.DeviceType.Commands
				.Where(cmd => _relevantCommands.Contains(cmd.TextCommand))
				.Cast<ASCIICommandInfo>()
				.ToArray() ?? new ASCIICommandInfo[0];
			var canConvert = moveCommands.Length > 0 && moveCommands.All(cmd => cmd.Nodes.Count >= 3 && cmd.Nodes.ElementAt(2).RequestUnitScale.HasValue);
			units = canConvert
				? uc.FindUnits(defaultDimension, prefixes)
				: new List<Unit>() { UnitOfMeasure.Data.Unit };

			measurementBox = new MeasurementBoxVM(uc, defaultDimension);
			measurementBox.PopulateUnits(FilterOutLargeOrSmallUnits(defaultDimension, units));
			measurementBox.SelectedUnit =
				measurementBox.Units.FirstOrDefault(u => aDefaultUnitSymbol == u.Abbreviation)
			?? measurementBox.Units.FirstOrDefault();

			return measurementBox;
		}


		private void PopulateMovementTypes()
		{
			var newMoves = new ObservableCollection<MovementType>();
			var previousSelection = _selectedMovementType?.DisplayName;

			// Determine what types of moves to allow based on device capabilities.
			var commands = Conversation?.Device.DeviceType.Commands;
			if (null != commands)
			{
				var asciiCmds = commands.Where(c => c is ASCIICommandInfo).Cast<ASCIICommandInfo>().ToList();

				var moveRelCmd = asciiCmds.FirstOrDefault(c => "move rel {0}" == c.TextCommand);
				if (null != moveRelCmd)
				{
					_rel.Dimension = moveRelCmd.Nodes.First(n => n.IsParameter).RequestUnit?.Unit.Dimension;
					newMoves.Add(_rel);
				}

				var moveAbsCmd = asciiCmds.FirstOrDefault(c => "move abs {0}" == c.TextCommand);
				if (null != moveAbsCmd)
				{
					_abs.Dimension = moveAbsCmd.Nodes.First(n => n.IsParameter).RequestUnit?.Unit.Dimension;
					newMoves.Add(_abs);
				}

				var moveForceCmd = asciiCmds.FirstOrDefault(c => "force abs {0}" == c.TextCommand);
				if (null != moveForceCmd)
				{
					_force.Dimension = moveForceCmd.Nodes.First(n => n.IsParameter).RequestUnit?.Unit.Dimension;
					newMoves.Add(_force);
				}
			}

			MovementTypes = newMoves;
			_selectedMovementType =
				newMoves.FirstOrDefault(m => m.DisplayName == previousSelection)
			?? newMoves.FirstOrDefault();


			// Set up measurement box by carrying over previous selections if possible.
			var isRotary = Conversation?.Device.DeviceType.IsRotaryDevice ?? false;
			var selectedMoveType = _selectedMovementType?.MoveMode ?? MoveType.Relative;
			var selectedUnitSymbol = SelectedSignalStepMeasurement?.SelectedUnit?.Abbreviation
								 ?? (isRotary ? UnitOfMeasure.DegreeSymbol : UnitOfMeasure.CentimeterSymbol);
			var stepMeasurement = CreateSignalMeasurementBox(selectedMoveType, selectedUnitSymbol);
			stepMeasurement.Quantity = SelectedSignalStepMeasurement?.Quantity ?? 1;
			SelectedSignalStepMeasurement = stepMeasurement;
		}

		#endregion

		#region -- Data --

		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Oscilloscope tab");

		private static readonly Prefix[] _usefulPrefixesDistance =
		{
			Prefix.Milli, Prefix.Centi, Prefix.None, Prefix.Micro, Prefix.Nano
		};

		private static readonly Prefix[] _usefulPrefixesForce = { Prefix.None, Prefix.Milli };

		private static readonly string[] _relevantCommands = { "move abs {0}", "move rel {0}", "force abs {0}" };

		private readonly MovementType _rel = new MovementType
		{
			DisplayName = "Move relative",
			Dimension = Dimension.Length,
			MoveMode = MoveType.Relative
		};

		private readonly MovementType _abs = new MovementType
		{
			DisplayName = "Move absolute",
			Dimension = Dimension.Length,
			MoveMode = MoveType.Absolute
		};

		private readonly MovementType _force = new MovementType
		{
			DisplayName = "Force move",
			Dimension = Dimension.Force,
			MoveMode = MoveType.Force
		};

		private decimal? _returnPosition;
		private MovementType _selectedMovementType;
		private MeasurementBoxVM _unitType;
		private bool _isReturnToStartChecked;
		private bool _isGenerating;
		private ObservableCollection<MovementType> _movementTypes = new ObservableCollection<MovementType>();

		#endregion
	}
}

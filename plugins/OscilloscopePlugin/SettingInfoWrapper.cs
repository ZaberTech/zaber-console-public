﻿using System.Collections.Generic;
using Zaber;

namespace ZaberConsole.Plugins.Oscilloscope
{
	/// <summary>
	///     Provides implementation for settings that includes recently used items, and item descriptors.
	/// </summary>
	public class SettingInfoWrapper
	{
		/// <summary>
		///     Default constructor for the setting info wrapper.
		/// </summary>
		/// <param name="settingInfo"></param>
		public SettingInfoWrapper(SettingInfo settingInfo)
		{
			Setting = settingInfo;
			SetDisplayName();
		}


		/// <summary>
		///     Sets the display name based off of the text command.
		/// </summary>
		public void SetDisplayName()
		{
			if (TextCommand != null)
			{
				var x = _displayNames.TryGetValue(TextCommand, out var s);

				if (x)
				{
					DisplayName = s + " (" + DisplayName + ")";
					HasDescription = true;
				}
				else
				{
					DisplayName = null;
					HasDescription = false;
				}
			}
		}


		/// <summary>
		///     Sets the display name based off of the text command.
		/// </summary>
		public void SetName()
		{
			if (TextCommand != null)
			{
				var x = _displayNames.TryGetValue(TextCommand, out var s);

				if (x)
				{
					DisplayName = s + " (" + DisplayName + ")";
					HasDescription = true;
				}
				else
				{
					DisplayName = TextCommand;
					HasDescription = false;
				}
			}
		}


		/// <summary>
		///     The wrapped setting info.
		/// </summary>
		public SettingInfo Setting { get; }


		/// <summary>
		///     The ASCII command that retrieves the setting.
		/// </summary>
		public string TextCommand => Setting?.TextCommand;


		/// <summary>
		///     User interface control property - if true, displays as a divider in the list.
		/// </summary>
		public bool IsDivider { get; set; }


		/// <summary>
		///     The name to display on the plugin's combo box.
		/// </summary>
		public string DisplayName { get; set; } = "a";


		/// <summary>
		///     Returns true if this setting has a descriptive field detailing what the setting is.
		/// </summary>
		public bool HasDescription { get; set; }


		private readonly Dictionary<string, string> _displayNames = new Dictionary<string, string>
		{
			{ "pos", "Trajectory Position" },
			{ "encoder.pos", "Measured Position" },
			{ "encoder.pos.error", "Measured Position Error" },
			{ "encoder.count", "Encoder Count" },
			{ "encoder.count.calibrated", "Calibrated Encoder Count" },
			{ "encoder.error", "Position Error)" },
			{ "driver.temperature", "Driver Temperature" },
			{ "system.voltage", "Supply Voltage" },
			{ "motion.accelonly", "Trajectory Acceleration" },
			{ "motion.decelonly", "Trajectory Deceleration" },
			{ "maxspeed", "Maximum Trajectory Speed" },
			{ "force.average", "Recent Force Output" },
			{ "system.temperature", "Controller Temperature" },
			{ "system.current", "Supply Current" },
			{ "limit.home.state", "Home Sensor Status" },
			{ "limit.away.state", "Away Sensor Status" },
			{ "limit.c.state", "Sensor C Status" },
			{ "limit.d.state", "Sensor D Status" }
		};
	}
}

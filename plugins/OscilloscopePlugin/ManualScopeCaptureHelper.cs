﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using Zaber;
using Zaber.Units;
using ZaberWpfToolbox;
using Measurement = Zaber.Units.Measurement;

namespace ZaberConsole.Plugins.Oscilloscope
{
	/// <summary>
	///     Helper for performing scope captures. This is only for one-shot capture mode, where the
	///     data is recorded by firmware.
	/// </summary>
	public class ManualScopeCaptureHelper : ScopeDataRecorderBase
	{
		/// <summary>
		///     Initializes the helper with a particular device. This does not trigger device communication.
		///     The device is assumed to support the scope commands.
		/// </summary>
		/// <param name="aDispatcher">Dispatcher to use for UI thread interaction.</param>
		/// <param name="aControllerConversation">Conversation with the controller to capture on.</param>
		/// <param name="aAxisConversation">Conversation with the axis to capture on.</param>
		/// <param name="aOutputBuffer">The buffer in which to store data collected.</param>
		public ManualScopeCaptureHelper(IDispatcher aDispatcher,
										Conversation aControllerConversation,
										Conversation aAxisConversation,
										IProducerConsumerCollection<SettingDataPoint> aOutputBuffer)
		{
			_dispatcher = aDispatcher;
			_controller = aControllerConversation;
			_axis = aAxisConversation;
			Output = aOutputBuffer;
		}


		/// <summary>
		///     Stops the capture.
		/// </summary>
		public override void Stop()
		{
			Task task = null;

			lock (_syncRoot)
			{
				task = _captureTask;
				_captureTask = null;
			}

			task?.Wait();

			IsInterrupted = true;
		}


		/// <summary>
		///     Initiate scope capture and wait for it to finish.
		/// </summary>
		public override void Start()
		{
			IsCapturing = true;

			SignalGenerator?.Prepare();

			lock (_syncRoot)
			{
				if (_captureTask is null)
				{
					// Note that we're saving the continuation task here, which handles error checking.
					_captureTask = Task.Factory.StartNew(DoCapture).ContinueWith(OnCaptureCompleted);
				}
				else
				{
					throw new InvalidProgramException(
						"Previous capture has not yet completed; start button should have been disabled.");
				}
			}
		}


		private void DoCapture()
		{
			// The device database currently doesn't present formatting information
			// for the scope settings, so we do the formatting ourselves.
			var uc = _axis.Device.UnitConverter.UnitConverter;
			var sampleRate = SampleRate.GetValueAs(uc.FindUnitBySymbol("Hz"));
			sampleRate = Math.Max(sampleRate, 0.001); // Arbitrary lower limit of 1 mHz.

			_samplePeriodInSeconds = 1.0 / sampleRate;

			try
			{
				_controller.Request("scope clear");

				AlterSetting("scope.timebase", new Measurement(_samplePeriodInSeconds, uc.FindUnitBySymbol("s")));
				AlterSetting("scope.delay", StartDelay);
			}
			catch (ErrorResponseException ere)
			{
				_log.Error("Scope capture aborted by error.", ere);
				IsInterrupted = true;
				IsCapturing = false;
				HasError = true;
				return;
			}

			foreach (var setting in SettingsToCapture)
			{
				try
				{
					_axis.Request($"scope add {setting}");
				}
				catch (ErrorResponseException ere)
				{
					// A full response to scope add is OK; it just means we won't capture more channels.
					if (!(ere.Response?.Body ?? string.Empty).Contains("FULL"))
					{
						throw;
					}

					// But we should stop trying to add more channels.
					break;
				}
			}

			SignalGenerator?.Start();

			DeviceMessage reply;
			try
			{
				reply = _controller.Request("scope start");
				if ((null == reply) || reply.IsError)
				{
					throw new DeviceFaultException("Device rejected scope start command.");
				}
			}
			catch (ErrorResponseException ere)
			{
				_log.Error("Scope capture aborted by error.", ere);
				IsInterrupted = true;
				IsCapturing = false;
				HasError = true;
				SignalGenerator?.Stop();
				return;
			}

			// If we get to this point, scope capture has started on the device.

			_receivedDataCount = 0;
			_captureChannels = null;
			_captureData = null;
			_startDelayInMilliseconds =
				StartDelay.GetValueAs(_axis.Device.UnitConverter.UnitConverter.FindUnitBySymbol("ms"));
			_controller.Device.MessageReceived += HandleScopeCaptureMessage;
			var printing = false;
			_captureMessages.Clear();

			// Send print command until it is accepted or an error occurs.
			while (!printing && !IsInterrupted)
			{
				try
				{
					// TODO: Send blank command after scope print is accepted. Ticket #6144.
					reply = _controller.Request("scope print");

					if ((null == reply) || reply.IsError)
					{
						_controller.Device.MessageReceived -= HandleScopeCaptureMessage;
						throw new DeviceFaultException("Device rejected scope print command.");
					}

					printing = true;
					_lastMessageReceivedTime = DateTime.UtcNow;
					break;
				}
				catch (ErrorResponseException aException)
				{
					if (aException.Response.Body.Contains("STATUSBUSY"))
					{
						Thread.Sleep(500);
					}
					else
					{
						_controller.Device.MessageReceived -= HandleScopeCaptureMessage;
						IsCapturing = false;
						throw aException;
					}
				}
				catch (Exception)
				{
					_controller.Device.MessageReceived -= HandleScopeCaptureMessage;
					IsCapturing = false;
					throw;
				}
			}

			// Handle case when user clicked the stop button before capture finished.
			if (IsInterrupted)
			{
				try
				{
					_controller.Request("scope stop");

					if (!printing)
					{
						reply = _controller.Request("scope print");
						if ((null != reply) && !reply.IsError)
						{
							_lastMessageReceivedTime = DateTime.UtcNow;
							printing = true;
						}
					}
				}
				catch (ErrorResponseException ere)
				{
					_log.Error("Error stopping manual scope capture: " + ere);
				}
			}

			// Wait for info messages to stop coming in.
			// To avoid missing messages, for now we just queue them up until the receive timeout occurs.
			while (printing && IsCapturing && ((DateTime.UtcNow - _lastMessageReceivedTime).TotalMilliseconds < 1000))
			{
				Thread.Sleep(100);
			}

			_controller.Device.MessageReceived -= HandleScopeCaptureMessage;
			IsCapturing = false;
			SignalGenerator?.Stop();
		}


		private void OnCaptureCompleted(Task aTask)
		{
			lock (_syncRoot)
			{
				_captureTask = null;
			}

			// Rethrow unhandled exceptions from the capture task on the UI thread
			// so the nice error dialog will be triggered properly.
			if (aTask.Exception != null)
			{
				IsInterrupted = true;
				IsCapturing = false;
				HasError = true;

				_dispatcher.BeginInvoke(() => throw aTask.Exception);

				SignalGenerator?.Stop();
			}
		}


		private decimal GetSetting(string aName)
		{
			var reply = _controller.Request($"get {aName}");
			if ((null == reply) || reply.IsError)
			{
				throw new DeviceFaultException($"Failed to read setting {aName}");
			}

			return reply.NumericData;
		}


		private void HandleScopeCaptureMessage(object aSender, DeviceMessageEventArgs aArgs)
		{
			if (ProcessScopeCaptureMessage(aArgs.DeviceMessage))
			{
				_lastMessageReceivedTime = DateTime.UtcNow;
			}
		}


		private bool ProcessScopeCaptureMessage(DeviceMessage aMessage)
		{
			var period = _samplePeriodInSeconds * 1000.0;
			var channelCount = _captureChannels?.Length ?? 0;
			var dataCount = 0;
			if ((null != _captureData) && (_captureData.Length > 0))
			{
				dataCount = _captureData[0].Length;
			}

			if (MessageType.Comment == aMessage.MessageType)
			{
				// The most frequent message will be data, so check for that first.
				var matches = Regex.Matches(aMessage.Body, @"data\ (-?[0-9]+(?:\.[0-9]*)?)");
				if (1 == matches.Count)
				{
					if (null == _captureData)
					{
						IsCapturing = false;
						throw new InvalidProgramException(
							"Device communication error: Received scope channel data before data count header.");
					}

					if (_receivedDataCount >= dataCount)
					{
						IsCapturing = false;
						throw new InvalidProgramException(
							"Device communication error: Received more scope data than expected.");
					}

					var data = double.Parse(matches[0].Groups[1].Value, CultureInfo.InvariantCulture);

					_captureData[_currentChannelIndex][_receivedDataCount] = data;
					var d = new SettingDataPoint
					{
						SettingName = SettingsToCapture[_currentChannelIndex],
						X = (_receivedDataCount * period) + _startDelayInMilliseconds,
						Y = data
					};

					if (!Output.TryAdd(d))
					{
						IsCapturing = false;
						_log.Error("Scope capture was aborted because the input queue rejected data.");
						return false;
					}

					_receivedDataCount++;
					return true;
				}

				// First info message should be the data and channel counts.
				matches = Regex.Matches(aMessage.Body, "count ([0-9]+) chan ([0-9]+)");
				if ((1 == matches.Count) && (3 == matches[0].Groups.Count))
				{
					dataCount = int.Parse(matches[0].Groups[1].Value, CultureInfo.InvariantCulture);
					channelCount = int.Parse(matches[0].Groups[2].Value, CultureInfo.InvariantCulture);
					if (null != _captureChannels)
					{
						IsCapturing = false;
						throw new InvalidProgramException(
							"Device communication error: Received two scope data headers.");
					}

					_captureChannels = new string[channelCount];
					_captureData = new double[channelCount][];
					for (var i = 0; i < channelCount; ++i)
					{
						_captureData[i] = new double[dataCount];
					}

					_currentChannelIndex = 0;

					return true;
				}

				// A channel name message heads the data for that channel.
				matches = Regex.Matches(aMessage.Body, @"chan ([0-9]) ([a-z0-9_\.]+)(?: axis ([0-9]+))?");
				if ((1 == matches.Count) && (matches[0].Groups.Count >= 3))
				{
					if (null == _captureChannels)
					{
						IsCapturing = false;
						throw new InvalidProgramException(
							"Device communication error: Received scope channel info before channel count header.");
					}

					var channelIndex =
						int.Parse(matches[0].Groups[1].Value, CultureInfo.InvariantCulture) - 1; // FW channel numbers are 1-based.
					if (channelIndex >= channelCount)
					{
						IsCapturing = false;
						throw new InvalidProgramException(
							"Device communication error: Received a scope channel index that was out of range.");
					}

					// TODO: Handle axis info (could have the same setting on multiple axes). See ticket #6145.
					_captureChannels[channelIndex] = matches[0].Groups[2].Value;
					_currentChannelIndex = channelIndex;
					_receivedDataCount = 0;
					return true;
				}
			}

			return false; // Message was not scope data.
		}


		private void SetSetting(string aName, string aValue)
		{
			var reply = _controller.Request($"set {aName} {aValue}");
			if ((null == reply) || reply.IsError)
			{
				throw new DeviceFaultException($"Failed to write setting {aName}");
			}
		}


		private void AlterSetting(string aSettingName, Measurement aNewValue)
		{
			// Handle quantity requirements of being in ms with one decimal place.
			var quantity = aNewValue.Value;
			if (!aNewValue.Unit.TryConvertTo(UnitConverter.Default.FindUnitBySymbol("ms"), ref quantity))
			{
				throw new InvalidProgramException($"Failed to convert units for {aSettingName}.");
			}

			// TODO use formatting information from database when available. See ticket #6146.
			quantity = Math.Round(quantity, 1);

			SetSetting(aSettingName, quantity.ToString());
		}


		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private readonly IDispatcher _dispatcher;
		private Task _captureTask;
		private readonly object _syncRoot = new object();

		private readonly Conversation _controller;
		private readonly Conversation _axis;
		private List<string> _settingsToCapture = new List<string>();
		private string[] _captureChannels;
		private double[][] _captureData;
		private int _receivedDataCount;
		private int _currentChannelIndex;
		private double _startDelayInMilliseconds;
		private double _samplePeriodInSeconds = 0.0001;
		private DateTime _lastMessageReceivedTime;
		private readonly Queue<DeviceMessage> _captureMessages = new Queue<DeviceMessage>();
	}
}

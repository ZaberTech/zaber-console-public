﻿namespace ZaberConsole.Plugins.Oscilloscope
{
	/// <summary>
	///     Interaction logic for OscilloscopeView.xaml
	/// </summary>
	public partial class OscilloscopeView
	{
		/// <summary>
		///     Initializes the control.
		/// </summary>
		public OscilloscopeView()
		{
			InitializeComponent();
		}
	}
}

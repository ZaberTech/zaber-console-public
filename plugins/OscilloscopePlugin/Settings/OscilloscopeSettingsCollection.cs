﻿using System;
using Zaber.Application;

namespace ZaberConsole.Plugins.Oscilloscope.Settings
{
	/// <summary>
	///     Used to save and restore user's scope settings across program sessions.
	/// </summary>
	[Serializable]
	public class OscilloscopeSettingsCollection : PerDeviceSettingsCollection<OscilloscopeSettings>
	{
	}
}

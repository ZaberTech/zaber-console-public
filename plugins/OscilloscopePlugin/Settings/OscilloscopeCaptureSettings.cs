﻿using System;

namespace ZaberConsole.Plugins.Oscilloscope.Settings
{
	/// <summary>
	///     Class used to remember the sample rate and delay selected for each device.
	/// </summary>
	[Serializable]
	public class OscilloscopeCaptureSettings
	{
		/// <summary>
		///     The specified delay before capture.
		/// </summary>
		public decimal Delay { get; set; }


		/// <summary>
		///     The specified rate at which to gather data from channels.
		/// </summary>
		public decimal SampleRate { get; set; }


		/// <summary>
		///     The specified trigger type (continuous or manual).
		/// </summary>
		public TriggerType TriggerType { get; set; }


		/// <summary>
		///     The specified unit prefix in use for the sample rate.
		/// </summary>
		public string SampleRateUnits { get; set; }


		/// <summary>
		///     The units of the delay measurement.
		/// </summary>
		public string DelayUnits { get; set; }


		/// <summary>
		///     The maximum amount of data to display on screen.
		/// </summary>
		public int MaximumDataToCapture { get; set; }
	}
}

﻿using System;

namespace ZaberConsole.Plugins.Oscilloscope.Settings
{
	/// <summary>
	///     Provides implementation for saving and loading measurement settings.
	/// </summary>
	[Serializable]
	public struct MeasurementSettings
	{
		/// <summary>
		///     The saved value for the unit's abbreviation.
		/// </summary>
		public string SavedUnitAbbreviation { get; set; }


		/// <summary>
		///     The saved value for the quantity of units.
		/// </summary>
		public decimal SavedQuantity { get; set; }
	}
}

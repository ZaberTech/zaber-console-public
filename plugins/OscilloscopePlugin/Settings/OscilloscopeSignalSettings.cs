﻿using System;

namespace ZaberConsole.Plugins.Oscilloscope.Settings
{
	/// <summary>
	///     Class used to remember the signal generator's settings.
	/// </summary>
	[Serializable]
	public class OscilloscopeSignalSettings
	{
		/// <summary>
		///     Set to true if the signal generator is enabled.
		/// </summary>
		public bool IsEnabled { get; set; }


		/// <summary>
		///     The movement type selected.
		/// </summary>
		public MoveType MovementType { get; set; }


		/// <summary>
		///     The size of the steps in decimal form.
		/// </summary>
		public decimal StepSize { get; set; }


		/// <summary>
		///     The associated units for the step size.
		/// </summary>
		public string StepSizeUnits { get; set; }


		/// <summary>
		///     True if the return to start checkbox is checked. False otherwise.
		/// </summary>
		public bool ReturnToStart { get; set; }


		/// <summary>
		///     The saved measurement settings for the absolute move mode.
		/// </summary>
		public MeasurementSettings AbsMeasurementSettings { get; set; }


		/// <summary>
		///     The saved measurement settings for the relative move mode.
		/// </summary>
		public MeasurementSettings RelMeasurementSettings { get; set; }


		/// <summary>
		///     The saved measurement settings for the force move mode.
		/// </summary>
		public MeasurementSettings ForceMeasurementSettings { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using Zaber.Application;

namespace ZaberConsole.Plugins.Oscilloscope.Settings
{
	/// <summary>
	///     Class used to remember previously selected tuner settings for each device.
	/// </summary>
	[Serializable]
	public class OscilloscopeSettings : PerDeviceSettings
	{
		/// <summary>
		///     Ordered list of the last settings selected for the current device, and whether or not to capture them.
		/// </summary>
		public List<OscilloscopeChannelSettings> SelectedSettings { get; set; }


		/// <summary>
		///     The specified delay and sample-rate to be saved.
		/// </summary>
		public OscilloscopeCaptureSettings CaptureSettings { get; set; }


		/// <summary>
		///     The specified options and measurements for the signal section.
		/// </summary>
		public OscilloscopeSignalSettings SignalSettings { get; set; }


		/// <summary>
		///     The recently used settings to save.
		/// </summary>
		public OscilloscopeRecentlyUsedSettings RecentlyUsedSettings { get; set; }


		/// <summary>
		///     The selected setting display mode.
		/// </summary>
		public SettingsDisplayModes SettingDisplayMode { get; set; }
	}
}

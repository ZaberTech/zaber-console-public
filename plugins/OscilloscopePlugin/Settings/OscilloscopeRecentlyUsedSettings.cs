﻿using System.Collections.Generic;

namespace ZaberConsole.Plugins.Oscilloscope.Settings
{
	/// <summary>
	///     Provides implementation for saving the recently used settings for a device.
	/// </summary>
	public class OscilloscopeRecentlyUsedSettings
	{
		/// <summary>
		///     The names of all recently used settings.
		/// </summary>
		public List<string> RecentlyUsedSettingNames { get; set; }
	}
}

﻿using System;

namespace ZaberConsole.Plugins.Oscilloscope.Settings
{
	/// <summary>
	///     Class used to remember previously selected tuner settings for each device.
	/// </summary>
	[Serializable]
	public class OscilloscopeChannelSettings
	{
		/// <summary>
		///     Whether or not to capture this channel next time.
		/// </summary>
		public bool DoCapture { get; set; }


		/// <summary>
		///     Name of the setting to capture.
		/// </summary>
		public string SettingName { get; set; }


		/// <summary>
		///     The hex string for the setting's color.
		/// </summary>
		public string ColorHex { get; set; }


		/// <summary>
		///     The abbreviation for the channel's selected unit.
		/// </summary>
		public string UnitAbbreviation { get; set; }
	}
}

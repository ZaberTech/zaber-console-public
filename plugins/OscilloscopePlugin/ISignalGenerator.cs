﻿namespace ZaberConsole.Plugins.Oscilloscope
{
	/// <summary>
	///     Interface that a scope data recorder uses to control the signal generator.
	///     This exists because some data recorders may need to do some setup communication
	///     when started, and by the time that finishes the results of the signal generator
	///     may already be too far along to capture the desired data.
	/// </summary>
	public interface ISignalGenerator
	{
		/// <summary>
		///     Do any preparation needed before starting signal generation. This must be
		///     called by the data recorder before calling <see cref="Start" />.
		/// </summary>
		void Prepare();


		/// <summary>
		///     Initiate signal generation. Data recorders should call this either immediately
		///     before or immediately after starting to record data.
		/// </summary>
		void Start();


		/// <summary>
		///     Stop signal generation. Data recorders should call this as soon as they have
		///     stopped recording, but the oscilloscope will also call this when a capture ends.
		///     Multiple calls are OK.
		/// </summary>
		void Stop();
	}
}

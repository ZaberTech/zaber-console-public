﻿using System.Windows;

namespace ZaberConsole.Plugins.Oscilloscope
{
	/// <summary>
	///     Provides implementation for connecting data bindings.
	///     Sourced from Thomas Levasque, 2011:
	///     https://www.thomaslevesque.com/2011/03/21/wpf-how-to-bind-to-data-when-the-datacontext-is-not-inherited/.
	/// </summary>
	public class BindingProxy : Freezable
	{
		/// <summary>
		///     The dependency property for the bound data.
		/// </summary>
		public static readonly DependencyProperty DataProperty =
			DependencyProperty.Register("Data", typeof(object), typeof(BindingProxy), new UIPropertyMetadata(null));


		/// <summary>
		///     The data to bind to.
		/// </summary>
		public object Data
		{
			get => GetValue(DataProperty);
			set => SetValue(DataProperty, value);
		}


		/// <summary>
		///     Creates the instance core.
		/// </summary>
		/// <returns></returns>
		protected override Freezable CreateInstanceCore() => new BindingProxy();
	}
}

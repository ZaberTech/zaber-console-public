﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using log4net;
using Zaber;
using Zaber.PlugIns;
using Zaber.Telemetry;
using Zaber.Threading;
using Zaber.Units;
using Zaber.Units.UI.WPF.Controls;
using ZaberConsole.Plugins.Oscilloscope.Properties;
using ZaberConsole.Plugins.Oscilloscope.Settings;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Dialogs;

namespace ZaberConsole.Plugins.Oscilloscope
{
	/// <summary>
	///     Top-level ViewModel for the Oscilloscope plugin. This defines the overall behavior of the tab.
	/// </summary>
	[PlugIn(Name = "Oscilloscope", Description = "Graph device settings over time.")]
	public class OscilloscopeVM : ObservableObject, IDialogClient
	{
		#region -- Plugin API --

		/// <summary>
		///     Default constructor - initializes the plugin.
		/// </summary>
		public OscilloscopeVM()
		{
			_dispatcher = DispatcherHelper.Dispatcher;

			// Initialize the channel group headers.
			ChannelGroupHeader = new SectionHeaderVM
			{
				Title = "Channels",
				ShowEnableCheckbox = false,
				Help = "Select the device settings to record and display."
			};

			SignalGroupHeader = new SectionHeaderVM
			{
				Title = "Signal Generator",
				ShowEnableCheckbox = true,
				Enabled = false,
				Help =
					"Configure a device movement or sequence to run while capturing data. If used with a configurable trigger, this action should be set up so as to meet the trigger conditions."
			};

			SettingsGroupHeader = new SectionHeaderVM
			{
				Title = "Settings",
				ShowEnableCheckbox = false,
				Enabled = false,
				Help = "Configure data collection settings."
			};

			// Channels list always has a dummy entry at the end for the + button.
			Channels.Add(new ChartSeries(DateTime.UtcNow, _timeRange) { IsDummy = true });

			// Initialize the MeasurementBoxVMs.
			DelayMeasurement = new MeasurementBoxVM(UnitConverter.Default, Dimension.Time)
			{
				SelectedUnit = UnitConverter.Default.FindUnitBySymbol("s"),
				Quantity = 0
			};

			// Remove unnecessary unit types.
			var units = DelayMeasurement.UnitConverter.FindUnits(DelayMeasurement.Measurement.Unit.Dimension,
																 _usefulPrefixesDelay);
			DelayMeasurement.PopulateUnits(UnitUtils.DiscardExtremeUnits(units, -3, 5));

			// Initialize the sample rate.
			SampleRateMeasurement = new MeasurementBoxVM(UnitConverter.Default, Dimension.Frequency)
			{
				SelectedUnit = UnitConverter.Default.FindUnitBySymbol("Hz"),
				Quantity = 100
			};

			// Remove unnecessary unit types.
			units = SampleRateMeasurement.UnitConverter.FindUnits(SampleRateMeasurement.Measurement.Unit.Dimension,
																  _usefulPrefixesFrequency);
			SampleRateMeasurement.PopulateUnits(units);

			// Set the callback function for Quantity changes in the sample rate measurement box.
			// These are part of a workaround for coercing the entered sample rate to be a legal value.
			// A legal value for sample rate is a period in 100 microsecond increments.
			SampleRateMeasurement.PropertyChanged += OnSampleRatePropertyChanged;

			// Initialize the trigger types.
			_manual = new TriggerType
			{
				TriggerMode = TriggerMode.MANUAL,
				Name = "Manual",
				ToolTip = Resources.STR_MANUAL_TOOLTIP
			};

			_continuous = new TriggerType
			{
				TriggerMode = TriggerMode.CONTINUOUS,
				Name = "Auto",
				ToolTip = Resources.STR_MANUAL_ENABLED
			};

			TriggerModes = new ObservableCollection<TriggerType>
			{
				_manual,
				_continuous
			};

			SignalGenerator = new SignalGeneratorVM();

			// Set the selected trigger type default.
			SelectedTriggerType = _continuous;

			// Initialize the capture button text.
			CaptureButtonString = Resources.STR_START_CAPTURE;

			// Initialize charts.
			Charts = new ObservableCollection<PlotVM>();

			// Set the capture button's tooltip.
			CanCaptureTooltip = Resources.STR_DISABLED_CAPTURE_TIP;
			CaptureDisabledMessage = Resources.STR_SELECT_AXIS;

			// Initialize constants.
			MaximumDataSetting = 10;

			StatusString = Resources.STR_STATUS_WAITING;

			SettingsMode = SettingsDisplayModes.BASIC;

			// Load settings.
			LoadSettings();

			// Fix the width of the left side panel.
			LeftSideWidth = 499;
			LeftSideWidth = 330;
		}


		/// <summary>
		///     Reference to the Plugin Manager. Used to detect user settings reset events.
		/// </summary>
		[PlugInProperty]
		public IPlugInManager PluginManager
		{
			get => _pluginManager;
			set
			{
				if (null != _pluginManager)
				{
					_pluginManager.SettingsResetEvent -= OnSettingsReset;
				}

				_pluginManager = value;

				if (null != _pluginManager)
				{
					_pluginManager.SettingsResetEvent += OnSettingsReset;
				}
			}
		}


		/// <summary>
		///     The currently selected device conversation in the Device List. This property
		///     is driven by the Plugin Manager.
		/// </summary>
		[PlugInProperty]
		public Conversation Conversation
		{
			get => _conversation;
			set
			{
				SaveSettings();
				_conversation = value;
				SignalGenerator.Conversation = _conversation;

				if (null != _conversation)
				{
					// Get the available settings.
					AvailableSettings = GetAvailableSettings();
					_recentlyUsedSettings.Clear();

					LoadSettings();
					UpdateChannels();
					UpdateCanCapture();
					OnPropertyChanged(nameof(IsAxisSelected));

					// Enable or disable manual capture mode depending on if the device supports the scope command.
					if (DoesDeviceHaveScopeCommands())
					{
						_manual.IsSelectable = true;
						_manual.ToolTip = Resources.STR_MANUAL_TOOLTIP;
					}
					else
					{
						_manual.IsSelectable = false;
						_manual.ToolTip = Resources.STR_MANUAL_DISABLED;
						SelectedTriggerType = _continuous;
					}

					if ((1 == Channels.Count) && (0 != AvailableSettings.Count))
					{
						// Add some default channels.
						AddSetting("pos");
						AddSetting("encoder.pos");
					}
				}
			}
		}


		/// <summary>
		///     Selected unit of measure for the currently selected device, if applicable.
		///     This property is driven by the Plugin Manager.
		/// </summary>
		[PlugInProperty]
		public UnitOfMeasure MasterUnitOfMeasure
		{
			set => _dispatcher.BeginInvoke(() =>
			{
				// Save the selected unit to use as the default for new plots.
				_deviceSelectedPositionUnit = value.Unit;

				// Change the units in the measurement boxes.
				if (null != SignalGenerator.SelectedMovementType)
				{
					if ((MoveType.Absolute == SignalGenerator.SelectedMovementType.MoveMode)
					|| (MoveType.Relative == SignalGenerator.SelectedMovementType.MoveMode))
					{
						SignalGenerator.SelectedSignalStepMeasurement.SelectedUnit = value.Unit;
					}
				}
			});
		}


		/// <summary>
		///     Port accessor. This property is driven by the Plugin Manager in Zaber Console.
		/// </summary>
		[PlugInProperty]
		public ZaberPortFacade PortFacade
		{
			get => _portFacade;
			set
			{
				if (null != _portFacade)
				{
					_portFacade.Closed -= Port_OpenOrClosed;
					_portFacade.Opened -= Port_OpenOrClosed;
				}

				_portFacade = value;

				if (null != _portFacade)
				{
					_portFacade.Closed += Port_OpenOrClosed;
					_portFacade.Opened += Port_OpenOrClosed;
				}
			}
		}


		/// <summary>
		///     Saves user settings on exit.
		/// </summary>
		[PlugInMethod(PlugInMethodAttribute.EventType.PluginDeactivated)]
		public void OnClosing()
		{
			StopCapture();
			Properties.Settings.Default.Save();
		}

		#endregion

		#region -- View properties --

		/// <summary>
		///     Determines whether or not the time range setting should be enabled or disabled.
		/// </summary>
		public bool TimeRangeSettingEnabled
		{
			get => _timeRangeSettingEnabled;
			set
			{
				Set(ref _timeRangeSettingEnabled, value, nameof(TimeRangeSettingEnabled));
				TimeRangeTooltip = TimeRangeSettingEnabled ? Resources.STR_TIMERANGE_ENABLED : Resources.STR_TIMERANGE_DISABLED;
			}
		}


		/// <summary>
		///     The tooltip to display on the time range setting text box.
		/// </summary>
		public string TimeRangeTooltip
		{
			get => _timeRangeTooltip;
			set => Set(ref _timeRangeTooltip, value, nameof(TimeRangeTooltip));
		}


		/// <summary>
		///     Convenience property to determine if the port is currently open.
		/// </summary>
		public bool IsPortOpen => PortFacade?.Port?.IsOpen ?? false;


		/// <summary>
		///     Convenience property for determining whether movement commands will
		///     have any effect on the currently selected device.
		/// </summary>
		public bool IsAxisSelected
		{
			get
			{
				// Disable the scope in binary mode until we add support.
				if (IsPortOpen && PortFacade.Port.IsAsciiMode && (null != Conversation))
				{
					var device = Conversation.Device;

					if (device.IsSingleDevice 
					&& (device.Axes.Count < 2) 
					&& (MotionType.None != device.DeviceType.MotionType))
					{
						return true;
					}
				}

				return false;
			}
		}


		/// <summary>
		///     Overlay string for when the capture controls are disabled.
		/// </summary>
		public string CaptureDisabledMessage
		{
			get => _captureDisabledMessage;
			set => Set(ref _captureDisabledMessage, value, nameof(CaptureDisabledMessage));
		}


		/// <summary>
		///     The string display for the capture button.
		/// </summary>
		public string CaptureButtonString
		{
			get => _captureButtonString;
			set => Set(ref _captureButtonString, value, nameof(CaptureButtonString));
		}


		/// <summary>
		///     The width size of the UI's left grid cell.
		/// </summary>
		public int LeftSideWidth
		{
			get => _leftSideWidth;
			set => Set(ref _leftSideWidth, value, nameof(LeftSideWidth));
		}


		/// <summary>
		///     Controls whether the scope's capture button is active or not.
		/// </summary>
		public bool CanCapture
		{
			get => _canCapture;
			set => Set(ref _canCapture, value, nameof(CanCapture));
		}


		/// <summary>
		///     The tooltip string for the capture button.
		/// </summary>
		public string CanCaptureTooltip
		{
			get => _canCaptureTooltip;
			set => Set(ref _canCaptureTooltip, value, nameof(CanCaptureTooltip));
		}


		/// <summary>
		///     Selectable settings to plot for the current device and axis.
		/// </summary>
		public ObservableCollection<SettingInfoWrapper> AvailableSettings
		{
			get => _availableSettings;
			set => Set(ref _availableSettings, value, nameof(AvailableSettings));
		}


		/// <summary>
		///     The currently selected Trigger Type
		/// </summary>
		public TriggerType SelectedTriggerType
		{
			get => _selectedTriggerType;
			set
			{
				Set(ref _selectedTriggerType, value, nameof(SelectedTriggerType));

				if (null != SelectedTriggerType)
				{
					TimeRangeSettingEnabled = SelectedTriggerType.TriggerMode != TriggerMode.MANUAL;
				}

				ClampSampleRate(SampleRateMeasurement);
			}
		}


		/// <summary>
		///     List of trigger types to display in the Trigger combo box.
		/// </summary>
		public ObservableCollection<TriggerType> TriggerModes { get; set; }


		/// <summary>
		///     Controls the checkbox for whether or not the graph should pan as it collects data.
		/// </summary>
		public bool IsPanGraphSelected
		{
			get => _isPanGraphSelected;
			set => Set(ref _isPanGraphSelected, value, nameof(IsPanGraphSelected));
		}


		/// <summary>
		///     Currently selected settings mode to display for available settings.
		/// </summary>
		public SettingsDisplayModes SettingsMode
		{
			get => _settingsMode;
			set
			{
				Set(ref _settingsMode, value, nameof(_settingsMode));

				RefreshAvailableSettings();
			}
		}


		/// <summary>
		///     The recording status string.
		/// </summary>
		public string StatusString
		{
			get => _statusString;
			set => Set(ref _statusString, value, nameof(StatusString));
		}


		/// <summary>
		///     Delay time measurement for data gathering
		/// </summary>
		public MeasurementBoxVM DelayMeasurement
		{
			get => _delay;
			set => Set(ref _delay, value, nameof(DelayMeasurement));
		}


		/// <summary>
		///     Sample rate mesaurement for data gathering
		/// </summary>
		public MeasurementBoxVM SampleRateMeasurement
		{
			get => _sampleRate;
			set => Set(ref _sampleRate, value, nameof(SampleRateMeasurement));
		}


		/// <summary>
		///     The signal generator helper.
		/// </summary>
		public SignalGeneratorVM SignalGenerator { get; set; }


		/// <summary>
		///     Header information for the channels control group.
		/// </summary>
		public SectionHeaderVM ChannelGroupHeader { get; }


		/// <summary>
		///     Header information for the signal generator control group.
		/// </summary>
		public SectionHeaderVM SignalGroupHeader { get; }


		/// <summary>
		///     Header information for the settings control group.
		/// </summary>
		public SectionHeaderVM SettingsGroupHeader { get; }


		/// <summary>
		///     Maximum samples to be kept in memory.
		/// </summary>
		public int MaximumDataSetting
		{
			get => _timeRange;
			set => Set(ref _timeRange, value, nameof(MaximumDataSetting));
		}


		/// <summary>
		///     List of all plots currently in memory.
		/// </summary>
		public ObservableCollection<PlotVM> Charts
		{
			get => _charts;
			set => Set(ref _charts, value, nameof(Charts));
		}


		/// <summary>
		///     Currently visible plot.
		/// </summary>
		public PlotVM SelectedChart
		{
			get => _selectedChart;
			set => Set(ref _selectedChart, value, nameof(SelectedChart));
		}


		/// <summary>
		///     List of channels to capture for next plot.
		/// </summary>
		public ObservableCollection<ChartSeries> Channels
		{
			get => _channels;
			set => Set(ref _channels, value, nameof(Channels));
		}


		/// <summary>
		///     Command invoked by the Start Capture button.
		/// </summary>
		public ICommand CaptureCommand
			=> OnDemandRelayCommand(ref _captureCommand,
				_ =>
				{
					if (null != Conversation)
					{
						if (CaptureButtonString == Resources.STR_STOP_CAPTURE)
						{
							StopCapture();
							UpdateRecentlyUsedSettings();
						}
						else
						{
							_startCaptureTime = DateTime.UtcNow;
							CaptureButtonString = Resources.STR_STOP_CAPTURE;
							CanCaptureTooltip = Resources.STR_STOP_CAPTURE_TIP;
							StatusString = Resources.STR_STATUS_RECORDING;
							StartCapture();
						}
					}
				});


		/// <summary>
		///     Command to rename one of the plot tabs.
		/// </summary>
		public ICommand RenameTabCommand 
			=> OnDemandRelayCommand(ref _renameTabCommand,
				 aObject =>
				 {
					 if (aObject is PlotVM plot)
					 {
						 var d = new InputBoxVM
						 {
							 Title = "Enter new plot name"
						 };

						 RequestDialogOpen?.Invoke(this, d);

						 if (d.DialogResult)
						 {
							 plot.Title = d.ResponseText;
						 }
					 }
				 });


		/// <summary>
		///     Command to close one of the plot tabs.
		/// </summary>
		public ICommand ClosePlotCommand 
			=> OnDemandRelayCommand(ref _closePlotCommand,
				 aObject =>
				 {
					 if (aObject is PlotVM plot)
					 {
						 _dispatcher.BeginInvoke(() =>
						 {
							 var index = Charts.IndexOf(plot);
							 Charts.Remove(plot);

							 if (plot == SelectedChart)
							 {
								 // If closing the capture in progress, stop capturing.
								 if (_helper != null)
								 {
									 StopCapture();
								 }

								 index = Math.Max(index, 0);
								 SelectedChart = index < Charts.Count ? Charts[index] : null;
							 }
						 });
					 }
				 });


		/// <summary>
		///     Command to add a new channel selection to the list.
		/// </summary>
		public ICommand AddOrRemoveChannelCommand 
			=> OnDemandRelayCommand(ref _addOrRemoveChannelCommand,
				  aRow =>
				  {
					  var row = aRow as ChartSeries;

					  // This is used as a flag to identify the dummy row.
					  if (row.IsDummy) 
					  {
						  AddSetting("pos");
					  }
					  else
					  {
						  Channels.Remove(row);
						  UpdateCanCapture();
					  }
				  },
				  aRow =>
				  {
					  var row = aRow as ChartSeries;
					  return IsDeviceScopable;
				  });


		/// <summary>
		///     Command to be called when the clear graph button is clicked.
		/// </summary>
		public ICommand ClearGraphCommand 
			=> OnDemandRelayCommand(ref _clearGraphCommand,
				  arow =>
				  {
					  foreach (var cs in SelectedChart.DataSeries)
					  {
						  cs.Clear();
					  }

					  SelectedChart.TriggerRefresh();
				  });


		/// <summary>
		///     Command invoked by the export button.
		/// </summary>
		public RelayCommand ExportCommand
			=> OnDemandRelayCommand(ref _exportCommand, p => { PromptExportData(p as PlotVM); });


		/// <summary>
		///     Command invoked by the export all button.
		/// </summary>
		public RelayCommand ExportAllCommand
			=> OnDemandRelayCommand(ref _exportAllCommand, _ => { PromptExportData(Charts.ToArray()); });

		#endregion

		#region -- IDialogClient implementation --

		/// <summary>
		///     Implementing VM should fire this event when it wants the main window
		///     to open a dialog using the VM it supplies.
		/// </summary>
		public event RequestDialogChange RequestDialogOpen;

		/// <summary>
		///     Implementing VM should fire this event if it wants to programmatically
		///     close the dialog it previously opened.
		/// </summary>
		#pragma warning disable CS0067 // Event is never used.
		public event RequestDialogChange RequestDialogClose;
		#pragma warning restore CS0067

		#endregion

		#region -- Functionality --

		private void RefreshAvailableSettings()
		{
			if (Conversation is null)
			{
				return;
			}

			var channelsCommands = Channels.Select(channel => channel.SelectedSetting?.TextCommand).ToArray();

			var recentlyUsedSettings = _recentlyUsedSettings;
			switch (_settingsMode)
			{
				case SettingsDisplayModes.ALL:
					AvailableSettings = GetAllAvailableSettings();
					break;

				case SettingsDisplayModes.BASIC:
					AvailableSettings = GetAvailableSettings();
					break;
			}

			if (_recentlyUsedSettings.Count != 0)
			{
				InsertDivider(0);
			}

			foreach (var settingInfoWrapper in recentlyUsedSettings)
			{
				AvailableSettings.Insert(0, settingInfoWrapper);
			}

			foreach (var channel in Channels
								.Select((channel, index) => new
									{
										Channel = channel,
										Index = index
									})
								.Where(channel => !channel.Channel.IsDummy)
								.Reverse()
								.ToArray())
			{
				var oldSelection = channelsCommands[channel.Index];
				var newSelection = AvailableSettings
							   .Where(setting => setting.TextCommand == oldSelection)
							   .FirstOrDefault();
				if (newSelection != null)
				{
					channel.Channel.SelectedSetting = newSelection;
				}
				else
				{
					Channels.RemoveAt(channel.Index);
				}
			}

			OnPropertyChanged(nameof(Channels));
		}


		private void AddSetting(string settingName)
		{
			if (Conversation is null)
			{
				return;
			}

			var setting = AvailableSettings?
					  .Select(s => s)
					  ?
				  .Where(s => s.TextCommand == settingName)
					  ?.FirstOrDefault();

			if (setting is null)
			{
				return;
			}

			var newSeries = new ChartSeries(_startCaptureTime, _timeRange);
			newSeries.PopulateSettingsAndUnits(Conversation.Device);
			newSeries.SelectedSetting = setting;

			var i = Channels.Count - 1;
			if ((i >= 0) && (i < ChartSeries.StandardColors.Count) && ChartSeries.StandardColors[i].Color.HasValue)
			{
				newSeries.SelectedColor = ChartSeries.StandardColors[i].Color.Value;
			}
			else
			{
				newSeries.SelectedColor = Colors.White; // Flag value to replace with generated color.
			}

			Channels.Insert(Channels.Count - 1, newSeries);
			UpdateCanCapture();
		}


		private void OnSettingsReset(object aSender, EventArgs aArgs)
		{
			var settings = Properties.Settings.Default;
			settings.Reset();
			LoadSettings();
		}


		private bool DoesDeviceHaveScopeCommands()
		{
			var cmds = _conversation.Device.DeviceType.Commands;
			return cmds.Select(o => o).Where(o => (o.TextCommand ?? string.Empty).Contains("scope")).Any();
		}


		// Called when the port open state changes.
		private void Port_OpenOrClosed(object sender, EventArgs e) => _dispatcher.BeginInvoke(() =>
		{
			OnPropertyChanged(nameof(IsPortOpen));
			StopCapture();

			if (PortFacade.Port.IsOpen && !PortFacade.Port.IsAsciiMode)
			{
				CaptureDisabledMessage = Resources.STR_ASCII_ONLY;
			}
			else
			{
				CaptureDisabledMessage = Resources.STR_SELECT_AXIS;
			}
		});


		private void StopCapture()
		{
			if (CaptureButtonString == Resources.STR_STOP_CAPTURE)
			{
				if (null != _helper)
				{
					_helper.Stop();
					SignalGenerator.Stop();
				}

				CaptureButtonString = Resources.STR_START_CAPTURE;
				StatusString = Resources.STR_STATUS_WAITING;
			}
		}


		private ObservableCollection<SettingInfoWrapper> GetAvailableSettings()
		{
			var AvailableSettings = new ObservableCollection<SettingInfo>(Conversation.Device.DeviceType.Commands
																				   .Where(cmd => cmd.IsSetting)
																				   .Select(cmd => cmd as SettingInfo)
																				   .Where(cmd => null
																							 != cmd.TextCommand));

			var col = new ObservableCollection<SettingInfoWrapper>();

			foreach (var si in AvailableSettings)
			{
				var siw = new SettingInfoWrapper(si) { DisplayName = si.TextCommand };

				siw.SetDisplayName();
				col.Add(siw);
			}

			var ret = col.Select(cmd => cmd).Where(cmd => cmd.DisplayName != null).OrderBy(o => o.DisplayName);
			var x = new ObservableCollection<SettingInfoWrapper>(ret);

			return x;
		}


		private ObservableCollection<SettingInfoWrapper> GetAllAvailableSettings()
		{
			var AvailableSettings = new ObservableCollection<SettingInfo>(Conversation.Device.DeviceType.Commands
																				   .Where(cmd => cmd.IsSetting)
																				   .Select(cmd => cmd as SettingInfo)
																				   .Where(cmd => null
																							 != cmd.TextCommand));

			var col = new ObservableCollection<SettingInfoWrapper>();

			foreach (var si in AvailableSettings)
			{
				var siw = new SettingInfoWrapper(si) { DisplayName = si.TextCommand };

				siw.SetName();
				col.Add(siw);
			}

			var ret = col.Select(cmd => cmd).Where(cmd => cmd.DisplayName != null).OrderBy(o => o.DisplayName).ToList();

			// Place settings with a description at the top of the list.
			for (var i = 0; i < ret.Count; i++)
			{
				var siw = ret[i];
				if (siw.HasDescription)
				{
					ret.Remove(siw);
					ret.Insert(0, siw);
				}
			}

			return new ObservableCollection<SettingInfoWrapper>(ret);
		}


		private void UpdateCanCapture()
		{
			if (_helper?.IsCapturing ?? false)
			{
				CanCapture = true;
				CanCaptureTooltip = Resources.STR_STOP_CAPTURE_TIP;
			}
			else if (Channels.Count != 1)
			{
				CanCapture = true;
				CanCaptureTooltip = Resources.STR_ENABLED_CAPTURE_TIP;
			}
			else
			{
				CanCapture = false;
				CanCaptureTooltip = Resources.STR_DISABLED_CAPTURE_TIP;
			}
		}


		private void OnSampleRatePropertyChanged(object aSender, PropertyChangedEventArgs aArgs)
		{
			if (!(aSender is MeasurementBoxVM userMeasurement))
			{
				return;
			}

			if ((aArgs.PropertyName != nameof(MeasurementBoxVM.Quantity))
			&& (aArgs.PropertyName != nameof(MeasurementBoxVM.SelectedUnit)))
			{
				return;
			}

			ClampSampleRate(userMeasurement);
		}


		private void ClampSampleRate(MeasurementBoxVM aUserMeasurement)
		{
			if (_isInClampCallback)
			{
				return;
			}

			_isInClampCallback = true;

			// Convert user-entered value to Hz without changing display.
			var hz = aUserMeasurement.Quantity;
			var rateConverter = new MeasurementBoxVM(UnitConverter.Default, Dimension.Frequency)
			{
				Quantity = hz,
				SelectedUnit = aUserMeasurement.SelectedUnit
			};
			rateConverter.ConvertToNewUnit(UnitConverter.Default.FindUnitBySymbol("Hz"));
			hz = rateConverter.Quantity;

			// Clamp value in Hz according to other settings.
			if (SelectedTriggerType.TriggerMode == TriggerMode.CONTINUOUS)
			{
				hz = Math.Max(1, Math.Min(200, hz));
			}
			else if (SelectedTriggerType.TriggerMode == TriggerMode.MANUAL)
			{
				// Reciprocate to obtain the period.
				if (hz == 0)
				{
					hz = 1;
				}
				else
				{
					var y = 1m / hz;

					// Round to the nearest 100 microseconds (as specified by firmware).
					y = decimal.Round(y, 4, MidpointRounding.AwayFromZero);

					y = Math.Max(0.0001m, y);

					// Convert back from a period to a frequency.
					hz = 1m / y;
					hz = Math.Max(1, hz);
				}
			}

			// Finally, convert this measure back to the unit selected by the user.
			rateConverter.Quantity = hz;
			rateConverter.SelectedUnit = UnitConverter.Default.FindUnitBySymbol("Hz");
			rateConverter.ConvertToNewUnit(aUserMeasurement.SelectedUnit);

			// Disallow rounding to zero.
			var newValue = Math.Round(rateConverter.Quantity, 3);
			newValue = newValue > 0 ? newValue : rateConverter.Quantity;

			// Post the clamped value back to the control.
			_dispatcher.BeginInvoke(() =>
			{
				aUserMeasurement.Quantity = newValue;
				_isInClampCallback = false;
			});
		}


		private bool IsDeviceScopable
			=> (null != Conversation) && !(Conversation.Device is DeviceCollection);


		private void LoadSettings()
		{
			var dummy = Channels.LastOrDefault() ?? new ChartSeries(DateTime.UtcNow, _timeRange) { IsDummy = true };
			Channels.Clear();

			var collection = Properties.Settings.Default.DeviceSettings;
			if (null != collection)
			{
				var id = Conversation?.Device?.GetIdentity();
				if (null != id)
				{
					var settings = collection.FindSettings(id);
					if (null != settings)
					{
						SettingsMode = SettingsDisplayModes.ALL;
						var uc = _conversation.Device.UnitConverter.UnitConverter;

						// Reloading recently used settings.
						if (null != settings.RecentlyUsedSettings)
						{
							var s = settings.RecentlyUsedSettings.RecentlyUsedSettingNames;
							var recent = new Queue<SettingInfoWrapper>();

							foreach (var x in s)
							{
								// Get the setting.
								var setting = AvailableSettings.Where(o => o.TextCommand == x).FirstOrDefault();
								if (null != setting)
								{
									recent.Enqueue(setting);
								}
							}

							// Add the divider.
							if (recent.Count != 0)
							{
								InsertDivider(0);
							}

							// Add the settings.
							foreach (var siw in recent)
							{
								_recentlyUsedSettings.Enqueue(siw);
							}

							// Ensure that there aren't more settings than the defined recently used settings size.
							while (_recentlyUsedSettings.Count > RECENT_SETTINGS_SIZE)
							{
								_recentlyUsedSettings.Dequeue();
							}

							foreach (var siw in _recentlyUsedSettings)
							{
								AvailableSettings.Insert(0, siw);
							}
						}

						// Reloading capture settings.
						if (null != settings.CaptureSettings)
						{
							var s = settings.CaptureSettings;

							if (null != s.TriggerType)
							{
								switch (s.TriggerType.TriggerMode)
								{
									case TriggerMode.CONTINUOUS:
										SelectedTriggerType = _continuous;
										break;

									case TriggerMode.MANUAL:
										SelectedTriggerType = _manual;
										break;
								}
							}

							if (null != s.SampleRateUnits)
							{
								var unitPrefix = s.SampleRateUnits;

								var sr = new MeasurementBoxVM(uc, Dimension.Frequency)
								{
									Quantity = s.SampleRate,
									SelectedUnit = uc.FindUnitBySymbol(unitPrefix)
								};

								// Remove unnecessary unit types.
								var units = sr.UnitConverter.FindUnits(sr.Measurement.Unit.Dimension,
																	   _usefulPrefixesFrequency);
								sr.PopulateUnits(units);

								sr.PropertyChanged += OnSampleRatePropertyChanged;
								SampleRateMeasurement = sr;
							}

							if (null != settings.CaptureSettings.DelayUnits)
							{
								var unitPrefix = s.DelayUnits;

								var d = new MeasurementBoxVM(uc, Dimension.Time)
								{
									Quantity = s.Delay,
									SelectedUnit = uc.FindUnitBySymbol(unitPrefix)
								};

								// Remove unnecessary unit types.
								var units = d.UnitConverter.FindUnits(d.Measurement.Unit.Dimension,
																	  _usefulPrefixesDelay);
								d.PopulateUnits(UnitUtils.DiscardExtremeUnits(units, -3, 5));

								DelayMeasurement = d;
							}
						}

						// Reloading signal settings.
						SignalGenerator.LoadSettings(settings.SignalSettings);
						SignalGroupHeader.Enabled = settings.SignalSettings?.IsEnabled ?? false;

						SettingsMode = settings.SettingDisplayMode;

						// Reloading channels.
						foreach (var channel in settings.SelectedSettings)
						{
							var newSeries = new ChartSeries(_startCaptureTime, _timeRange);
							newSeries.PopulateSettingsAndUnits(Conversation?.Device);
							newSeries.DoCapture = channel.DoCapture;

							var select = AvailableSettings
									 .Where(s => (s != null) && (s.TextCommand == channel.SettingName))
									 .FirstOrDefault();
							if (select != null)
							{
								newSeries.SelectedSetting = AvailableSettings
														.Where(s
																   => (s != null)
																  && (s.TextCommand == channel.SettingName))
														.FirstOrDefault();
							}

							newSeries.SelectedColor = (Color) ColorConverter.ConvertFromString(channel.ColorHex);

							if ((channel.UnitAbbreviation != null) && (channel.UnitAbbreviation != "null") &&
								newSeries.SelectedSetting.Setting.ResponseUnitScale.HasValue)
							{
								var unit =
									_conversation.Device.UnitConverter.UnitConverter.FindUnitBySymbol(
										channel.UnitAbbreviation);
								if (null != unit)
								{
									newSeries.SelectedUnit = new UnitOfMeasure(unit);
								}
							}

							Channels.Add(newSeries);
						}
					}
					else
					{
						SignalGenerator.LoadSettings(null);
					}
				}
			}

			Channels.Add(dummy);
			_addOrRemoveChannelCommand?.FireCanExecuteChanged();
		}


		private void SaveSettings()
		{
			var collection = Properties.Settings.Default.DeviceSettings;
			if (collection is null)
			{
				collection = new OscilloscopeSettingsCollection();
				Properties.Settings.Default.DeviceSettings = collection;
			}

			var id = Conversation?.Device?.GetIdentity();
			if (null != id)
			{
				var settings = collection.FindOrCreateSettings(id);
				settings.SelectedSettings = Channels.Where(c => null != c.SelectedSetting)
												 .Select(c => new OscilloscopeChannelSettings
													 {
														 DoCapture = c.DoCapture,
														 SettingName = c.SelectedSetting.TextCommand,
														 ColorHex = c.SelectedColor.ToString(),
														 UnitAbbreviation = c.SelectedUnit?.Unit?.Abbreviation ?? "null"
													 })
												 .ToList();

				var ocs = new OscilloscopeCaptureSettings
				{
					Delay = DelayMeasurement.Quantity,
					SampleRate = SampleRateMeasurement.Quantity,
					TriggerType = SelectedTriggerType,
					MaximumDataToCapture = MaximumDataSetting
				};

				ocs.SampleRateUnits = SampleRateMeasurement.SelectedUnit is null ? SampleRateMeasurement.Units.FirstOrDefault().Abbreviation : SampleRateMeasurement.SelectedUnit.Abbreviation;

				ocs.DelayUnits = DelayMeasurement.SelectedUnit is null ? DelayMeasurement.Units.FirstOrDefault().Abbreviation : DelayMeasurement.SelectedUnit.Abbreviation;

				settings.CaptureSettings = ocs;

				var oss = SignalGenerator.SaveSettings();
				oss.IsEnabled = SignalGroupHeader.Enabled;
				settings.SignalSettings = oss;

				var orus = new OscilloscopeRecentlyUsedSettings
				{
					RecentlyUsedSettingNames = _recentlyUsedSettings.Select(o => o.TextCommand).ToList()
				};

				settings.RecentlyUsedSettings = orus;
				settings.SettingDisplayMode = SettingsMode;
			}
		}


		// Sanitize the channel list when the selected device changes.
		private void UpdateChannels()
		{
			var dummy = Channels.Last();
			if (!IsDeviceScopable)
			{
				Channels.Clear();
				Channels.Add(dummy);
			}
			else
			{
				foreach (var c in Channels)
				{
					if (c != dummy)
					{
						c.PopulateSettingsAndUnits(Conversation.Device);
					}
				}
			}

			_addOrRemoveChannelCommand?.FireCanExecuteChanged();
		}


		private void InsertDivider(int index)
			=> AvailableSettings.Insert(index, new SettingInfoWrapper(null) { IsDivider = true });


		private List<SettingInfoWrapper> GetRecentlyUsedSettings()
			=> AvailableSettings.Take(_recentlyUsedSettings.Count - 1).ToList();


		private void UpdateRecentlyUsedSettings()
		{
			// Save the selected settings.
			var selectedMap = new Dictionary<ChartSeries, SettingInfoWrapper>();

			// Save the selected units.
			var savedUnits = new Dictionary<ChartSeries, UnitOfMeasure>();

			// Save the colors.
			var savedColors = new Dictionary<ChartSeries, Color>();

			foreach (var cs in Channels)
			{
				if (!cs.IsDummy)
				{
					savedColors[cs] = cs.SelectedColor;
					savedUnits[cs] = cs.SelectedUnit;
					selectedMap[cs] = cs.SelectedSetting;
				}
			}

			for (var i = _recentlyUsedSettings.Count - 1; i >= 0; i--)
			{
				AvailableSettings.RemoveAt(i);
			}

			if (0 == _recentlyUsedSettings.Count)
			{
				InsertDivider(0);
			}

			foreach (var s in Channels)
			{
				if (!s.IsDummy)
				{
					if (!_recentlyUsedSettings.Contains(selectedMap[s]))
					{
						var siw = new SettingInfoWrapper(selectedMap[s].Setting)
						{
							DisplayName = selectedMap[s].TextCommand
						};

						switch (SettingsMode)
						{
							case SettingsDisplayModes.ALL:
								siw.SetName();
								break;

							case SettingsDisplayModes.BASIC:
								siw.SetDisplayName();
								break;
						}

						_recentlyUsedSettings.Enqueue(siw);
						while (_recentlyUsedSettings.Count > RECENT_SETTINGS_SIZE)
						{
							_recentlyUsedSettings.Dequeue();
						}
					}
				}
			}

			foreach (var si in _recentlyUsedSettings)
			{
				AvailableSettings.Insert(0, si);
			}

			foreach (var cs in Channels)
			{
				if (!cs.IsDummy)
				{
					var chan = selectedMap[cs];
					var set = AvailableSettings.Where(o => o.TextCommand == chan.TextCommand).First();
					cs.SelectedSetting = set;
					cs.SelectedColor = savedColors[cs];
					cs.SelectedUnit = savedUnits[cs];
				}
			}
		}


		private void PlotUpdateWorker_DoWork(object aSender, DoWorkEventArgs aArgs)
		{
			var plot = aArgs.Argument as PlotVM;
			var lastSampleCount = 0;
			var settingCount = _helper.SettingsToCapture.Count;
			_plotRefreshing = false;
			var statusShown = false;

			// Continue polling until the queue is empty AND the capture helper is no longer running.
			while (_helper.IsCapturing || (lastSampleCount > 0))
			{
				// Pull all available data from the queue.
				var samples = new List<SettingDataPoint>();

				while (_helper.Output.TryTake(out var sample))
				{
					samples.Add(sample);
				}

				// Add the new samples to the recorded data array and drop old data.
				lastSampleCount = samples.Count;
				if (lastSampleCount > 0)
				{
					if (!statusShown)
					{
						statusShown = true;
						PluginManager?.ShowStatusText(this, null);
						PluginManager?.ShowStatusText(this, Resources.STR_STATUS_TRANSFERRING, STATUS_DURATION);
					}

					foreach (var d in samples)
					{
						var cs = plot.DataSeries.Where(s => s.SelectedSetting.TextCommand == d.SettingName)
								  .FirstOrDefault();
						var t = new Tuple<double, double>(d.X, d.Y);

						if (TriggerMode.CONTINUOUS == SelectedTriggerType.TriggerMode)
						{
							cs.Expire();
						}

						cs.Append(new Tuple<double, double>[1] { t });
					}

					// Avoid queuing up multiple BeginInvoke callbacks for better UI performance.
					if (!_plotRefreshing)
					{
						_plotRefreshing = true;
						_dispatcher.BeginInvoke(() =>
						{
							plot.TriggerRefresh();
							_plotRefreshing = false;
						});
					}
				}

				// Limit refresh rate to 100Hz.
				Thread.Sleep(10);
			}
		}


		private void PlotUpdateWorker_RunWorkerCompleted(object aSender, RunWorkerCompletedEventArgs aArgs)
		{
			PluginManager?.ShowStatusText(this, null);
			if (null != aArgs.Error)
			{
				PluginManager?.ShowStatusText(this, Resources.STR_CAPTURE_FAILED, STATUS_DURATION);
				_log.Error("Scope capture data recording aborted with an error.", aArgs.Error);
			}

			_dispatcher.BeginInvoke(() =>
			{
				if (!_helper.IsInterrupted)
				{
					StopCapture();
				}

				UpdateCanCapture();
			});

			_plotRefreshing = false;
			if (_helper.HasError)
			{
				CaptureButtonString = Resources.STR_START_CAPTURE;
				StatusString = Resources.STR_STATUS_WAITING;
				PluginManager?.ShowStatusText(this, Resources.STR_CAPTURE_FAILED, STATUS_DURATION);
			}
		}


		// Perform a high-resolution capture.
		private void StartCapture()
		{
			var channels = Channels.Where(c => c.DoCapture && (null != c.SelectedSetting))
								.Select(c => c.SelectedSetting);
			if (!channels.Any())
			{
				return;
			}

			var uc = UnitConverter.Default;

			// TODO handle the case where the user selects the same setting multiple times with different
			// units of measure. It should only get captured once, and then multiple plots created from it.

			var plot = new PlotVM
			{
				CaptureTime = DateTime.UtcNow,
				Title = "Capture " + _captureNumber
			};

			_captureNumber++;

			string statusBarText = null;
			switch (SelectedTriggerType.TriggerMode)
			{
				case TriggerMode.MANUAL:
				{
					var controller = Conversation;
					if (controller.Device.AxisNumber != 0) // If an axis is selected, we also need the controller.
					{
						controller = PortFacade.GetConversation(controller.Device.DeviceNumber);
					}

					_helper =
						new ManualScopeCaptureHelper(_dispatcher,
													 controller,
													 Conversation,
													 new ConcurrentQueue<SettingDataPoint>())
						{
							SettingsToCapture =
								channels.Select(s => s.TextCommand).ToList(), // TODO change to SettingInfo?
							SampleRate = SampleRateMeasurement.Measurement,
							StartDelay = DelayMeasurement.Measurement
						};

					statusBarText = Resources.STR_STATUS_WAITING_FOR_FW;
				}
					break;

				case TriggerMode.CONTINUOUS:
					_helper = new AutoScopeCaptureHelper(_dispatcher, new ConcurrentQueue<SettingDataPoint>())
					{
						SettingsToCapture = channels.Select(s => s.TextCommand).ToList(),
						DeviceConversation = Conversation,
						SampleRate = SampleRateMeasurement.Measurement,
						StartDelay = DelayMeasurement.Measurement
					};
					break;
			}

			_helper.SignalGenerator = SignalGroupHeader.Enabled ? SignalGenerator : null;


			foreach (var name in _helper.SettingsToCapture)
			{
				var originalSettings = Channels.Where(c => c.SelectedSetting.TextCommand == name).FirstOrDefault();
				if (null == originalSettings)
				{
					continue;
				}

				var chart = new ChartSeries(_startCaptureTime, _timeRange);

				chart.PopulateSettingsAndUnits(Conversation.Device);
				chart.SelectedSetting = AvailableSettings.Where(s => s.TextCommand == name).FirstOrDefault();
				chart.Units = originalSettings.Units;
				chart.SelectedUnit =
					originalSettings
					.Units.Where(u => _deviceSelectedPositionUnit?.Abbreviation
								  == originalSettings.SelectedUnit.Abbreviation)
					.FirstOrDefault()
				?? originalSettings.SelectedUnit;
				chart.SelectedColor = originalSettings.SelectedColor;

				plot.DataSeries.Add(chart);
			}

			Charts.Add(plot);
			SelectedChart = plot;
			plot.CenterGraph += 1;

			CaptureButtonString = Resources.STR_STOP_CAPTURE;

			if (plot.DataSeries.Count == 1)
			{
				plot.Title += " (" + plot.DataSeries[0].SelectedSetting.TextCommand + ")";
			}

			StatusString = Resources.STR_STATUS_WAITING;
			if (null != statusBarText)
			{
				PluginManager?.ShowStatusText(this, statusBarText, STATUS_DURATION);
			}

			_eventLog.LogEvent("Capture started",
							   $"Mode = {SelectedTriggerType.TriggerMode}; Settings = [{string.Join(", ", _helper.SettingsToCapture)}]");

			try
			{
				var plotUpdateWorker = BackgroundWorkerManager.CreateWorker();
				plotUpdateWorker.DoWork += PlotUpdateWorker_DoWork;
				plotUpdateWorker.RunWorkerCompleted += PlotUpdateWorker_RunWorkerCompleted;

				_helper.Start();

				plotUpdateWorker.Run(plot);
			}
			catch (Exception aException)
			{
				_log.Error("Scope capture failed.", aException);
				PluginManager?.ShowStatusText(this, Resources.STR_CAPTURE_FAILED, STATUS_DURATION);
			}
		}


		private void PromptExportData(params PlotVM[] aDataSets)
		{
			var filePattern = string.Empty;
			if (aDataSets.Length < 1)
			{
				return;
			}

			if (aDataSets.Length == 1)
			{
				filePattern = aDataSets[0].Title + ".csv";
			}
			else
			{
				filePattern = "{time} {title}.csv";
			}
			
			var dlg = new FileBrowserDialogParams
			{
				Action = FileBrowserDialogParams.FileActionType.Save,
				AddExtension = true,
				CheckFileExists = false,
				DefaultExtension = ".csv",
				Filename = filePattern,
				Filter = "CSV spreadsheet files|*.csv|All files|*.*",
				InitialDirectory = Properties.Settings.Default.LastExportFolder,
				PromptForOverwrite = false, // We handle overwrite detection below.
				Title = "Save plot to CSV"
			};

			RequestDialogOpen?.Invoke(this, dlg);
			if (!dlg.DialogResult.HasValue || !dlg.DialogResult.Value)
			{
				return;
			}

			filePattern = dlg.Filename;
			for (var fileIndex = 0; fileIndex < aDataSets.Length; ++fileIndex)
			{
				var plot = aDataSets[fileIndex];
				var filename = filePattern.Replace("{title}", plot.Title)
									   .Replace("{time}", plot.CaptureTime.ToString("yyyy-MM-dd HH-mm-ss"));
				if (File.Exists(filename))
				{
					var mbvm = new MessageBoxParams
					{
						Caption = "Replace file?",
						Message = $"File {filename} already exists. Overwrite?",
						Icon = MessageBoxImage.Question,
						Buttons = MessageBoxButton.YesNoCancel
					};

					RequestDialogOpen?.Invoke(this, mbvm);
					if (MessageBoxResult.Cancel == mbvm.Result)
					{
						return;
					}

					if (MessageBoxResult.No == mbvm.Result)
					{
						continue;
					}
				}

				string errorMessage = null;
				try
				{
					plot.Export(filename);
					_eventLog.LogEvent("Export data");
				}
				catch (Exception aException)
				{
					_log.Error("Failed to save plot to CSV file " + filename, aException);
					errorMessage = aException.Message;
				}

				if (null != errorMessage)
				{
					var mbvm = new MessageBoxParams
					{
						Caption = "Error saving file",
						Message = $"There was an error when exporting file {filename}: {errorMessage}",
						Icon = MessageBoxImage.Error,
						Buttons = MessageBoxButton.OK
					};

					var okCancels = true;
					if ((aDataSets.Length > 1) && (fileIndex < (aDataSets.Length - 1)))
					{
						mbvm.Message =
							string.Format("There was an error when exporting file {0}: {1}"
									  + Environment.NewLine
									  + "Do you want to continue exporting files?",
										  filename,
										  errorMessage);
						mbvm.Buttons = MessageBoxButton.YesNo;
						okCancels = false;
					}

					RequestDialogOpen?.Invoke(this, mbvm);

					if (okCancels || (MessageBoxResult.Yes != mbvm.Result))
					{
						return;
					}
				}
			}

			if (aDataSets.Length > 0) // and export was successful because we got this far
			{
				Properties.Settings.Default.LastExportFolder = Path.GetDirectoryName(filePattern);
			}
		}

		#endregion

		#region -- Fields --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Oscilloscope tab");

		private static readonly int RECENT_SETTINGS_SIZE = 5;
		private static readonly int STATUS_DURATION = 10;

		private readonly IDispatcher _dispatcher;
		private bool _canCapture;
		private bool _timeRangeSettingEnabled = true;
		private string _canCaptureTooltip;
		private int _timeRange;

		private Conversation _conversation;
		private RelayCommand _captureCommand;
		private RelayCommand _renameTabCommand;
		private RelayCommand _closePlotCommand;
		private RelayCommand _addOrRemoveChannelCommand;
		private RelayCommand _exportCommand;
		private RelayCommand _exportAllCommand;
		private RelayCommand _clearGraphCommand;
		private ZaberPortFacade _portFacade;
		private MeasurementBoxVM _delay;
		private MeasurementBoxVM _sampleRate;
		private bool _isInClampCallback;
		private Unit _deviceSelectedPositionUnit;
		private bool _isPanGraphSelected;
		private ScopeDataRecorderBase _helper;
		private TriggerType _selectedTriggerType;
		private string _captureButtonString;
		private string _captureDisabledMessage;
		private string _timeRangeTooltip;
		private ObservableCollection<PlotVM> _charts = new ObservableCollection<PlotVM>();
		private PlotVM _selectedChart;
		private ObservableCollection<ChartSeries> _channels = new ObservableCollection<ChartSeries>();
		private DateTime _startCaptureTime;
		private int _captureNumber;
		private int _leftSideWidth;
		private readonly TriggerType _manual;
		private readonly TriggerType _continuous;
		private string _statusString;
		private IPlugInManager _pluginManager;
		private SettingsDisplayModes _settingsMode;
		private ObservableCollection<SettingInfoWrapper> _availableSettings;
		private readonly Queue<SettingInfoWrapper> _recentlyUsedSettings = new Queue<SettingInfoWrapper>();
		private volatile bool _plotRefreshing;

		private static readonly Prefix[] _usefulPrefixesFrequency = { Prefix.Kilo, Prefix.None };

		private static readonly Prefix[] _usefulPrefixesDelay = { Prefix.Milli, Prefix.None };

		#endregion
	}
}

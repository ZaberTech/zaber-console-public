﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;
using OxyPlot.Wpf;

namespace ZaberConsole.Plugins.Oscilloscope
{
	/// <summary>
	///     Interaction logic for ChartControl.xaml
	/// </summary>
	public partial class ChartView
	{
		/// <summary>
		///     Bind HasFocus to an observable boolean property on your VM to be able to force or check
		///     whether the attached control has focus.
		/// </summary>
		public static readonly DependencyProperty DataSeriesProperty =
			DependencyProperty.RegisterAttached("DataSeries",
												typeof(IEnumerable<ChartSeries>),
												typeof(ChartView),
												new FrameworkPropertyMetadata(OnDataSeriesChanged));


		/// <summary>
		///     Bind CenterGraph Command to an integer that when increased, will center the graph.
		/// </summary>
		public static readonly DependencyProperty CenterGraphCommandProperty =
			DependencyProperty.RegisterAttached("CenterGraph",
												typeof(int),
												typeof(ChartView),
												new FrameworkPropertyMetadata(OnCenterGraphCommand));


		/// <summary>
		///     Bind RefreshTrigger Command to an integer that when increased, will update all of the graph data.
		/// </summary>
		public static readonly DependencyProperty RefreshTriggerProperty =
			DependencyProperty.RegisterAttached("RefreshTrigger",
												typeof(int),
												typeof(ChartView),
												new FrameworkPropertyMetadata(OnRefreshTriggered));


		/// <summary>
		///     Initializes the control.
		/// </summary>
		public ChartView()
		{
			InitializeComponent();
		}


		/// <summary>
		///     List of commands available to the AutocompleteBox.
		/// </summary>
		public IEnumerable<ChartSeries> DataSeries
		{
			get => GetValue(DataSeriesProperty) as IEnumerable<ChartSeries>;
			set => SetValue(DataSeriesProperty, value);
		}


		/// <summary>
		///     The integer flag for tracking when the center graph button is clicked.
		/// </summary>
		public int CenterGraph
		{
			get => (int) GetValue(CenterGraphCommandProperty);
			set => SetValue(CenterGraphCommandProperty, value);
		}


		/// <summary>
		///     The integer flag for tracking when the center graph button is clicked.
		/// </summary>
		public int RefreshTrigger
		{
			get => (int) GetValue(RefreshTriggerProperty);
			set => SetValue(RefreshTriggerProperty, value);
		}


		private static void OnCenterGraphCommand(DependencyObject aElement, DependencyPropertyChangedEventArgs aArgs)
		{
			var cv = aElement as ChartView;
			cv._plot.ResetAllAxes();
		}


		private static void OnRefreshTriggered(DependencyObject aElement, DependencyPropertyChangedEventArgs aArgs)
		{
			var cv = aElement as ChartView;
			foreach (var pair in cv._lineMap)
			{
				var cs = pair.Key;
				var ls = pair.Value;
				ls.ItemsSource = cs.GetConvertedData();
			}

			cv._plot.InvalidateFlag += 1;
		}


		private static void OnDataSeriesChanged(DependencyObject aElement, DependencyPropertyChangedEventArgs aArgs)
		{
			if (aElement is ChartView me)
			{
				me.UpdateSeries(aArgs.NewValue as IEnumerable<ChartSeries>);
			}
		}


		private void UpdateSeries(IEnumerable<ChartSeries> aNewCollection)
		{
			// Guard below algorithms against nulls.
			if (null == aNewCollection)
			{
				aNewCollection = new ChartSeries[] { };
			}

			// Identify and remove old series.
			var removed = new List<ChartSeries>();
			var hashMap = new HashSet<ChartSeries>(aNewCollection);
			foreach (var series in _lineMap.Keys)
			{
				if (!hashMap.Contains(series))
				{
					removed.Add(series);
				}
			}

			foreach (var series in removed)
			{
				series.PropertyChanged -= Series_PropertyChanged;
				_plot.Series.Remove(_lineMap[series]);
				_lineMap.Remove(series);
			}

			// Identify and add new series.
			// Note ordering will be changed by this algorithm.
			foreach (var series in aNewCollection)
			{
				var c = series.SelectedColor;
				series.FillColor = new SolidColorBrush { Color = c };

				if (!_lineMap.ContainsKey(series))
				{
					series.PropertyChanged += Series_PropertyChanged;
					var line = new LineSeries
					{
						Color = c,
						ItemsSource = series.GetConvertedData(),
						YAxisKey = "Vertical",
						XAxisKey = "Horizontal"
					};
					_lineMap[series] = line;
					_plot.Series.Add(line);
				}
				else
				{
					_lineMap[series].Color = c;
				}
			}

			_plot.InvalidateFlag += 1;
		}


		private void Series_PropertyChanged(object aSender, PropertyChangedEventArgs aArgs)
		{
			var series = aSender as ChartSeries;
			if ((null == series) || !_lineMap.ContainsKey(series))
			{
				return;
			}

			var line = _lineMap[series];
			if (aArgs.PropertyName == nameof(ChartSeries.Visible))
			{
				line.Visibility = series.Visible ? Visibility.Visible : Visibility.Hidden;
			}
			else if (aArgs.PropertyName == nameof(ChartSeries.SelectedUnit))
			{
				line.ItemsSource = series.GetConvertedData();
				_plot.InvalidateFlag += 1;
			}
		}


		private readonly Dictionary<ChartSeries, LineSeries> _lineMap = new Dictionary<ChartSeries, LineSeries>();
	}
}

﻿using Zaber.Units;
using ZaberConsole.Plugins.Oscilloscope.Settings;

namespace ZaberConsole.Plugins.Oscilloscope
{
	/// <summary>
	///     This class defines a single type of movement available for the "step" signal source.
	/// </summary>
	public class MovementType
	{
		/// <summary>
		///     The pretty string to be displayed in the combo box.
		/// </summary>
		public string DisplayName { get; set; }


		/// <summary>
		///     The movement type's measurement dimension.
		/// </summary>
		public Dimension Dimension { get; set; }


		/// <summary>
		///     The selected MoveType.
		/// </summary>
		public MoveType MoveMode { get; set; }


		/// <summary>
		///     The saved measurement settings for this movement type.
		/// </summary>
		public MeasurementSettings MeasurementSettings { get; set; }
	}
}

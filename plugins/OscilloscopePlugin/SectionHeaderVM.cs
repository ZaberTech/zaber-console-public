﻿using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.Oscilloscope
{
	/// <summary>
	///     VM for oscilloscopy section headers.
	/// </summary>
	public class SectionHeaderVM : ObservableObject
	{
		/// <summary>
		///     Whether the enable checkbox is checked.
		/// </summary>
		public bool Enabled
		{
			get => _enabled;
			set => Set(ref _enabled, value, nameof(Enabled));
		}


		/// <summary>
		///     Whether the enable checkbox is visible.
		/// </summary>
		public bool ShowEnableCheckbox
		{
			get => _showEnableCheckbox;
			set => Set(ref _showEnableCheckbox, value, nameof(ShowEnableCheckbox));
		}


		/// <summary>
		///     Title for the section.
		/// </summary>
		public string Title
		{
			get => _title;
			set => Set(ref _title, value, nameof(Title));
		}


		/// <summary>
		///     Help text for the help button. If empty, the button will be hidden.
		/// </summary>
		public string Help
		{
			get => _help;
			set => Set(ref _help, value, nameof(Help));
		}


		private bool _enabled;
		private bool _showEnableCheckbox;
		private string _title;
		private string _help;
	}
}

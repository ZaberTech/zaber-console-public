﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using log4net;
using Zaber;
using Zaber.Units;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.Oscilloscope
{
	/// <summary>
	///     Provides helper functions for continuous capture.
	/// </summary>
	public class AutoScopeCaptureHelper : ScopeDataRecorderBase
	{
		#region -- Public methods --

		/// <summary>
		///     Creates a new instance, initializing thread properties.
		/// </summary>
		public AutoScopeCaptureHelper(IDispatcher dispatcher,
									  IProducerConsumerCollection<SettingDataPoint> aOutputBuffer)
		{
			_dispatcher = dispatcher;
			_worker.DoWork += ContinuousCaptureWork;
			_worker.ProgressChanged += ContinuousCaptureProgress;
			_worker.RunWorkerCompleted += ContinuousCaptureComplete;
			_worker.WorkerReportsProgress = true;
			_worker.WorkerSupportsCancellation = true;

			Output = aOutputBuffer;
			_charts = new Dictionary<string, ChartSeries>();
		}


		/// <summary>
		///     Conversation from the Oscilloscope view model.
		/// </summary>
		public Conversation DeviceConversation { get; set; }


		/// <summary>
		///     Starts the asynchronous thread.
		/// </summary>
		public override void Start()
		{
			SignalGenerator?.Prepare();
			IsCapturing = true;
			_worker.RunWorkerAsync();
		}


		/// <summary>
		///     Stops recording of data and halts the thread.
		/// </summary>
		public override void Stop()
		{
			_worker.CancelAsync();
			IsCapturing = false;
			IsInterrupted = true;
		}

		#endregion

		#region -- Helpers --

		private void ContinuousCaptureWork(object sender, DoWorkEventArgs e)
		{
			var uc = DeviceConversation.Device.UnitConverter.UnitConverter;
			var timeout = new TimeoutTimer { Timeout = 500 };
			var sampleRateHz = SampleRate.GetValueAs(uc.FindUnitBySymbol("Hz"));
			sampleRateHz = Math.Max(sampleRateHz, 0.001); // Arbitrary lower limit of 1 mHz.
			var timeBase = (long) (1000.0 / sampleRateHz);
			var startDelay = (int) StartDelay.GetValueAs(uc.FindUnitBySymbol("ms"));

			// Kick off the signal generator.
			SignalGenerator?.Start();

			var stopWatch = new Stopwatch();
			stopWatch.Start();

			// Wait for the specified start delay.
			if (startDelay > 0)
			{
				Thread.Sleep(startDelay);
			}

			while (!_worker.CancellationPending && IsCapturing)
			{
				var loopStart = stopWatch.ElapsedMilliseconds;
				var newData = new SettingDataPoint[SettingsToCapture.Count];
				var error = false;

				for (var i = 0; i < SettingsToCapture.Count; i++)
				{
					var s = SettingsToCapture[i];
					DeviceMessage dm = null;

					// Request the setting. Use message IDs if enabled to reduce collision with other commands.
					try
					{
						var topic = DeviceConversation.StartTopic();
						DeviceConversation.Send("get " + s, topic.MessageId);
						if (!topic.Wait(timeout))
						{
							throw new RequestTimeoutException();
						}

						topic.Validate();
						dm = topic.Response;
					}
					catch (Exception aException)
					{
						// Because message IDs aren't used everywhere in ZC right now, we will skip a data point if an error occurs.
						// This prevents blips in the graph (provided message IDs are enabled), but results in data loss.
						error = true;
						_log.Debug("Oscilloscope skipping data points due to communication error.", aException);
						break;
					}

					// Interpret as a double.
					var replyData = (double) dm.NumericData;
					double time = stopWatch.ElapsedMilliseconds;

					// For the command string s, add the data point.
					var d = new SettingDataPoint
					{
						SettingName = s,
						X = time,
						Y = replyData
					};

					newData[i] = d;
				}

				// Skip datapoints and sleeps on error in order to try to get another data point ASAP.
				if (!error)
				{
					// Enqueue the new set of samples for the UI thread.
					foreach (var d in newData)
					{
						if (!Output.TryAdd(d))
						{
							IsCapturing = false;
							HasError = true;
							IsInterrupted = true;
							_log.Error("Scope capture was aborted because the input queue rejected data.");
						}
					}

					// Sleep according to the specified sample rate.
					var sampleTime = stopWatch.ElapsedMilliseconds - loopStart;
					var timeToSleep = timeBase - sampleTime;
					if (timeToSleep > 0)
					{
						Thread.Sleep((int) timeToSleep);
					}
				}

				// Slow down the sample rate if the UI thread isn't consuming data fast enough.
				// Possible future improvement:
				// This hard-limit method of delaying might produce irregularly spaced samples 
				// depending on how the UI thread performs. If we want more regularly spaced samples,
				// we could implement some sort of gradual reduction of the sample rate based on 
				// the queue length instead.
				while (!_worker.CancellationPending && (Output.Count > 100))
				{
					Thread.Sleep(10);
				}
			}

			stopWatch.Stop();

			e.Cancel = true;
			_worker.Dispose();
		}


		private void ContinuousCaptureProgress(object sender, ProgressChangedEventArgs e)
		{
			// TODO Future expansion: Report actual average sample rate here. Ticket #6135.
		}


		private void ContinuousCaptureComplete(object sender, RunWorkerCompletedEventArgs e)
		{
			if (e.Cancelled)
			{
				_log.Debug("Asyncronous continuous capture thread was cancelled.");
			}
			else if (e.Error != null)
			{
				_log.Error("Asyncronous continuous capture thread ended with an error. Error message: " + e.Error);
			}
			else
			{
				_log.Debug("Asyncronous continuous capture thread completed without error.");
			}

			SignalGenerator?.Stop();
		}

		#endregion

		#region -- Fields --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private IDispatcher _dispatcher;
		private Dictionary<string, ChartSeries> _charts;
		private ObservableCollection<ChartSeries> _dataSeries = new ObservableCollection<ChartSeries>();
		private readonly BackgroundWorker _worker = new BackgroundWorker();

		#endregion
	}
}

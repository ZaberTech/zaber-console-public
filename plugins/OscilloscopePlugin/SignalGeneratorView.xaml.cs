﻿namespace ZaberConsole.Plugins.Oscilloscope
{
	/// <summary>
	///     Interaction logic for SignalGeneratorView.xaml
	/// </summary>
	public partial class SignalGeneratorView
	{
		/// <summary>
		///     View constructor. Initializes the control.
		/// </summary>
		public SignalGeneratorView()
		{
			InitializeComponent();
		}
	}
}

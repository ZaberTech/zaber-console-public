﻿namespace ZaberConsole.Plugins.Oscilloscope
{
	/// <summary>
	///     Enumeration corresponding to the settings to display in the Oscilloscope Plugin.
	/// </summary>
	public enum SettingsDisplayModes
	{
		/// <summary>
		///     Basic settings mode.
		/// </summary>
		BASIC,

		/// <summary>
		///     All settings mode.
		/// </summary>
		ALL
	}
}

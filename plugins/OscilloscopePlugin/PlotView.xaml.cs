﻿namespace ZaberConsole.Plugins.Oscilloscope
{
	/// <summary>
	///     Interaction logic for PlotView.xaml
	/// </summary>
	public partial class PlotView
	{
		/// <summary>
		///     Initializes the view.
		/// </summary>
		public PlotView()
		{
			InitializeComponent();
		}
	}
}

﻿namespace ZaberConsole.Plugins.Oscilloscope
{
	/// <summary>
	///     Provides implementation for a user-input box.
	/// </summary>
	public partial class InputBox
	{
		/// <summary>
		///     Creates a box that accepts input from the user.
		/// </summary>
		public InputBox()
		{
			InitializeComponent();
		}
	}
}

﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Zaber.Units;

namespace ZaberConsole.Plugins.Oscilloscope
{
	/// <summary>
	///     Provides basic implementation for a scope capture helper class.
	/// </summary>
	public abstract class ScopeDataRecorderBase
	{
		/// <summary>
		///     Starts the capture.
		/// </summary>
		public abstract void Start();


		/// <summary>
		///     Stops the capture.
		/// </summary>
		public abstract void Stop();


		/// <summary>
		///     Queue via which data is output.
		/// </summary>
		public IProducerConsumerCollection<SettingDataPoint> Output { get; protected set; }


		/// <summary>
		///     Number of data captures performed per second by the scope capture command.
		/// </summary>
		public Measurement SampleRate
		{
			get => _sampleRate;
			set
			{
				if (Dimension.Frequency != value.Unit.Dimension)
				{
					throw new ArgumentException(nameof(SampleRate) + " must be a frequency measurement.");
				}

				_sampleRate = value;
			}
		}


		/// <summary>
		///     The scope capture delay - the number of ticks firmware should wait before recording each sample.
		///     This will be quantized to the time resolution supported by firmware.
		/// </summary>
		public Measurement StartDelay
		{
			get => _startDelay;
			set
			{
				if (Dimension.Time != value.Unit.Dimension)
				{
					throw new ArgumentException(nameof(StartDelay) + " must be a time measurement.");
				}

				_startDelay = value;
			}
		}


		/// <summary>
		///     Indicates whether data recording is in progress.
		/// </summary>
		public bool IsCapturing
		{
			// A backing store is used for this property because it needs to be volatile.
			get => _isCapturing;
			protected set => _isCapturing = value;
		}


		/// <summary>
		///     If true, the capture was stopped via the "stop capture" button.
		/// </summary>
		public bool IsInterrupted
		{
			// A backing store is used for this property because it needs to be volatile.
			get => _isInterrupted;
			protected set => _isInterrupted = value;
		}


		/// <summary>
		///     If true, the capture was stopped by an error. <see cref="IsInterrupted" />
		///     will also be true when this is true.
		/// </summary>
		public bool HasError { get; protected set; }


		/// <summary>
		///     The list of settings for the thread to capture.
		/// </summary>
		public List<string> SettingsToCapture { get; set; }


		/// <summary>
		///     The user-configured signal generator to use, if any.
		/// </summary>
		public ISignalGenerator SignalGenerator { get; set; }


		// Producer thread store data here for consumer (UI) thread to read and update the display.
		// Content items are one row (first index) for each setting, in the order specified in
		// SettingsToCapture. Columns (second index) are 0 for X coordinates, 1 for Y coordinates.
		private Measurement _startDelay;
		private Measurement _sampleRate;
		private volatile bool _isCapturing;
		private volatile bool _isInterrupted;
	}
}

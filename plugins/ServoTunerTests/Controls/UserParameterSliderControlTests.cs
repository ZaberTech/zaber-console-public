﻿using System;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using Zaber.Testing;
using ZaberConsole.Plugins.ServoTuner.Methods.ParameterControls;

namespace ServoTunerTests.Controls
{
	[TestFixture]
	[SetCulture("en-US")]
	public class UserParameterSliderControlTests
	{
		[SetUp]
		public void Setup()
		{
			_data = LoadData(TEST_JSON);
			_vm = new UserParameterSliderControlVM();
		}


		[TearDown]
		public void Teardown() => _listener?.Unlisten(_vm);


		[Test]
		public void TestConfigureAndInterpolate()
		{
			_vm.Configure(_data);
			Assert.AreEqual("Max", _vm.MaximumLabel);
			Assert.AreEqual("Min", _vm.MinimumLabel);
			Assert.AreEqual("i", _vm.Title);
			Assert.AreEqual(0.25, _vm.SliderPosition);

			var data = _vm.GetTunerParameters();
			Assert.IsNotNull(data);
			Assert.IsNotNull(data["x"]);
			Assert.IsNotNull(data["y"]);
			Assert.IsNotNull(data["z"]);
			Assert.IsNotNull(data["w"]);
			Assert.AreEqual(1.25, data["x"].Value);
			Assert.AreEqual(1.5, data["y"].Value);
			Assert.AreEqual(1.75, data["z"].Value);
			Assert.AreEqual(5.0, data["w"].Value);

			_vm.SliderPosition = 0.75;
			data = _vm.GetTunerParameters();
			Assert.AreEqual(1.75, data["x"].Value);
			Assert.AreEqual(2.5, data["y"].Value);
			Assert.AreEqual(3.25, data["z"].Value);
			Assert.AreEqual(5.0, data["w"].Value);
		}


		[Test]
		public void TestConfigureWithoutDefaultValue()
		{
			var data = LoadData(TEST_JSON.Replace("'DefaultValue': 0.25,", string.Empty));
			_vm.Configure(data);
			Assert.AreEqual("Max", _vm.MaximumLabel);
			Assert.AreEqual("Min", _vm.MinimumLabel);
			Assert.AreEqual("i", _vm.Title);
			Assert.AreEqual(0.5, _vm.SliderPosition);
		}


		[Test]
		public void TestGetUserSettings()
		{
			_vm.Configure(_data);
			Assert.AreNotEqual(0.75, _vm.SliderPosition);
			_vm.SliderPosition = 0.75;
			var settings = _vm.UserSettings.ToList();
			Assert.IsNotNull(settings);
			Assert.AreEqual(0, settings.Count); // See ticket RM #6968.
		}


		[Test]
		public void TestObservableProperties()
		{
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
			_listener.TestObservableProperty(_vm, () => _vm.Title);
			_listener.TestObservableProperty(_vm, () => _vm.MinimumLabel);
			_listener.TestObservableProperty(_vm, () => _vm.MaximumLabel);
			_listener.TestObservableProperty(_vm, () => _vm.SliderPosition);
		}


		[Test]
		public void TestSetUserSettings()
		{
			_vm.Configure(_data);
			var pos = _vm.SliderPosition;
			Assert.AreNotEqual(0.75, _vm.SliderPosition);
			var entry = new Tuple<string, object>("SliderPosition", 0.75);
			_vm.UserSettings = new[] { entry };
			// Reloading old user settings should not affect slider position - see ticket RM #6968.
			Assert.AreEqual(pos, _vm.SliderPosition); 
		}


		private JToken LoadData(string aData)
		{
			using (var reader = new JsonTextReader(new StringReader(aData)))
			{
				return JObject.Load(reader);
			}
		}


		private const string TEST_JSON = @"
        { 
          'DataType': 'Interpolated',
          'MaxValueLabel': 'Max',
          'MinValueLabel': 'Min',
          'DefaultValue': 0.25,
          'ParameterName': 'i',
          'Values': 
          {
            'x': [ 1, 2 ],
            'y': [ 1, 2, 3 ],
            'z': [ 1, 2, 3, 4 ],
			'w': 5
	      }
        }";


		private UserParameterSliderControlVM _vm;
		private JToken _data;
		private ObservableObjectTester _listener;
	}
}

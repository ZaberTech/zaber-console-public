﻿using Zaber.Tuning;
using Zaber.Tuning.Servo;
using Zaber.Units;
using ZaberConsole.Plugins.ServoTuner.Methods;
using ZaberTest.Testing;

namespace ServoTunerTests
{
	public class ParameterSetHelper
	{
		public static ITunerParameterSet MakeParams()
		{
			var parms = new TunerParameterSet(TuningModeNames.FFPID);
			var i = 1;
			foreach (var name in new[] { "Foo", "Bar", "Baz" })
			{
				parms[name] = new Measurement(i, null);
				i++;
			}

			return parms;
		}


		public static ITunerParameterSet MakePidParams(DeviceProperties aDeviceProperties = null)
			=> InvocationHelper.InvokePrivateStaticMethod<PidMethodVM>("CreateUserPidParameterSet", aDeviceProperties)
				as ITunerParameterSet;
	}
}

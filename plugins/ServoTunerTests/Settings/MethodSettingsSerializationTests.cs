﻿using System;
using NUnit.Framework;
using Zaber.Application;
using ZaberTest.Testing;

namespace ServoTunerTests.Settings
{
	[TestFixture]
	[SetCulture("en-US")]
	public class MethodSettingsSerializationTests
	{
		// Test that each settings type can be XML serialized and deserialized, with default values.
		[Test]
		public void TestSettingsSerialization()
		{
			foreach (var type in typeof(PerDeviceSettings).Assembly.GetExportedTypes())
			{
				if (!type.IsAbstract && typeof(PerDeviceSettings).IsAssignableFrom(type))
				{
					var settings1 = Activator.CreateInstance(type, null) as PerDeviceSettings;
					Assert.IsNotNull(settings1, "Failed to instantiate " + type.Name);

					string xml = null;
					try
					{
						xml = ObjectToXmlString.Serialize(settings1);
					}
					catch (Exception aException)
					{
						throw new Exception("Failed to serialize " + type.Name, aException);
					}

					Assert.IsFalse(string.IsNullOrEmpty(xml), "No XML output for " + type.Name);

					PerDeviceSettings settings2 = null;
					try
					{
						settings2 = ObjectToXmlString.Deserialize<PerDeviceSettings>(xml);
					}
					catch (Exception aException)
					{
						throw new Exception("Failed to deserialize " + type.Name, aException);
					}

					Assert.IsNotNull(settings2, "No object deserialized for " + type.Name);
					Assert.AreEqual(settings1.GetType(),
									settings2.GetType(),
									"Deserialized type is wrong for " + type.Name);
				}
			}
		}
	}
}

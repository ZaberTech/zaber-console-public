﻿using NUnit.Framework;
using Zaber.Testing;
using ZaberConsole.Plugins.ServoTuner;

namespace ServoTunerTests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class ParameterListRowVMTests
	{
		[SetUp]
		public void Setup() => _vm = new ParameterListRowVM("foo", 1234);


		[TearDown]
		public void Teardown() => _listener?.Unlisten(_vm);


		[Test]
		public void TestConstructor()
		{
			Assert.AreEqual("foo", _vm.Name);
			Assert.AreEqual(1234, _vm.Value);
		}


		[Test]
		public void TestConstructorDefaults()
		{
			var vm = new ParameterListRowVM("bar");
			Assert.AreEqual("bar", vm.Name);
			Assert.AreEqual(0, vm.Value);
		}


		[Test]
		public void TestObservableProperties()
		{
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
			_listener.TestObservableProperty(_vm, () => _vm.Name);
			_listener.TestObservableProperty(_vm, () => _vm.Value);
		}


		private ParameterListRowVM _vm;
		private ObservableObjectTester _listener;
	}
}

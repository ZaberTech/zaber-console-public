﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Zaber.Testing;
using Zaber.Tuning;
using Zaber.Tuning.Servo;
using Zaber.Units;
using ZaberConsole.Plugins.ServoTuner;

namespace ServoTunerTests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class ParameterListVMTests
	{
		[SetUp]
		public void Setup()
		{
			_params = new TunerParameterSet(TuningModeNames.FFPID)
			{
				{ "Foo", new Measurement(1.0, null) },
				{ "Bar", new Measurement(1.0, null) },
				{ "Baz", new Measurement(1.0, null) }
			};

			_vm = new ParameterListVM(_params);
			_vm.ParameterEdited += OnParameterEdited;
			_editList.Clear();
		}


		[TearDown]
		public void Teardown()
		{
			_vm.ParameterEdited -= OnParameterEdited;
			_listener?.Unlisten(_vm);
		}


		[Test]
		public void TestConstructor()
		{
			Assert.IsTrue(_vm.IsEditable);
			Assert.IsNotNull(_vm.GetEditedParameters());
			var expected = new HashSet<string>(_params.ParameterNames);
			var actual = new HashSet<string>(_vm.Parameters.Select(row => row.Name));
			Assert.IsTrue(expected.SetEquals(actual));
			foreach (var row in _vm.Parameters)
			{
				Assert.AreEqual(_params[row.Name].Value, row.Value);
			}
		}


		[Test]
		public void TestEditNotification()
		{
			foreach (var row in _vm.Parameters)
			{
				var prevValue = _params[row.Name];
				row.Value += 1;
				Assert.AreEqual(1, _editList.Count);
				Assert.AreEqual(row.Name, _editList[0]);
				Assert.AreEqual(prevValue.Value + 1, (double) row.Value, 0.0001);
				Assert.AreEqual(prevValue.Value + 1, _params[row.Name].Value, 0.0001);
				_editList.Clear();
			}
		}


		[Test]
		public void TestObservableProperties()
		{
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
			_listener.TestObservableProperty(_vm, () => _vm.IsEditable);
		}


		private void OnParameterEdited(object aSender, string aParamName)
		{
			Assert.IsTrue(ReferenceEquals(_vm, aSender));
			_editList.Add(aParamName);
		}


		private ParameterListVM _vm;
		private TunerParameterSet _params;
		private readonly List<string> _editList = new List<string>();
		private ObservableObjectTester _listener;
	}
}

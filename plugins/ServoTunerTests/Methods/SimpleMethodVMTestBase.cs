﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Zaber;
using Zaber.Tuning.Servo;
using Zaber.Tuning.Tests;
using Zaber.Units;
using ZaberConsole.Plugins.ServoTuner.Methods;
using ZaberTest;
using Measurement = Zaber.Units.Measurement;

namespace ServoTunerTests.Methods
{
	public abstract class SimpleMethodVMTestBase
	{
		[Test]
		public void TestConstructor()
		{
			Assert.IsFalse(_vm.CanRead);
			Assert.IsNotNull(_vm.CarriageInertia);
			Assert.IsNotNull(_vm.LoadInertia);
			Assert.IsNull(_vm.DefaultCarriageInertia);
		}


		[Test]
		[Description("Test that invoking the tuning calculation produces nonzero results.")]
		public void TestCalculate()
		{
			var props = TunerTestHelper.CreateDmqDeviceProperties();
			var conv = _portFacade.Conversations.Where(c => c.Device.DeviceType.DeviceId == 50419).FirstOrDefault();
			Assert.IsNotNull(conv, "Test setup failure creating port facade.");
			_vm.SetTargetDevice(conv, props);

			_vm.CarriageInertia.Measurement =
				new Measurement(100m, _vm.CarriageInertia.UnitConverter.FindUnitBySymbol("g"));
			_vm.LoadInertia.Measurement = new Measurement(0m, _vm.LoadInertia.UnitConverter.FindUnitBySymbol("g"));

			var parms = _vm.DeviceParameters;
			Assert.IsNotNull(parms);

			var sum1 = 0.0;
			foreach (var name in parms.ParameterNames)
			{
				sum1 += parms[name].Value;
			}

			Assert.AreNotEqual(0.0, sum1);
		}


		[Test]
		[Description("Test that changing the load mass produces different output.")]
		public void TestChangingMass()
		{
			var props = TunerTestHelper.CreateDmqDeviceProperties();
			var conv = _portFacade.Conversations.Where(c => c.Device.DeviceType.DeviceId == 50419).FirstOrDefault();
			Assert.IsNotNull(conv, "Test setup failure creating port facade.");
			_vm.SetTargetDevice(conv, props);

			_vm.CarriageInertia.Measurement =
				new Measurement(100m, _vm.CarriageInertia.UnitConverter.FindUnitBySymbol("g"));
			_vm.LoadInertia.Measurement = new Measurement(0m, _vm.LoadInertia.UnitConverter.FindUnitBySymbol("g"));

			var parms1 = _vm.DeviceParameters;
			Assert.IsNotNull(parms1);


			_vm.LoadInertia.Measurement = new Measurement(1m, _vm.LoadInertia.UnitConverter.FindUnitBySymbol("kg"));
			var parms2 = _vm.DeviceParameters;
			Assert.IsNotNull(parms2);

			Assert.IsFalse(parms1.IsIdenticalTo(parms2));
		}


		[Test]
		[Description("Test that changing the mass unit produces different output.")]
		public void TestChangingUnit()
		{
			var props = TunerTestHelper.CreateDmqDeviceProperties();
			var conv = _portFacade.Conversations.Where(c => c.Device.DeviceType.DeviceId == 50419).FirstOrDefault();
			Assert.IsNotNull(conv, "Test setup failure creating port facade.");
			_vm.SetTargetDevice(conv, props);

			_vm.CarriageInertia.Measurement =
				new Measurement(100m, _vm.CarriageInertia.UnitConverter.FindUnitBySymbol("g"));
			_vm.LoadInertia.Measurement = new Measurement(0m, _vm.LoadInertia.UnitConverter.FindUnitBySymbol("g"));

			var parms1 = _vm.DeviceParameters;
			Assert.IsNotNull(parms1);


			_vm.CarriageInertia.SelectedUnit = _vm.CarriageInertia.UnitConverter.FindUnitBySymbol("oz");
			var parms2 = _vm.DeviceParameters;
			Assert.IsNotNull(parms2);

			Assert.IsFalse(parms1.IsIdenticalTo(parms2));
		}


		[Test]
		[Description("Test that a recommended acceleration is displayed and varies with load.")]
		public void TestAccelerationRecommended()
		{
			var props = TunerTestHelper.CreateDmqDeviceProperties();
			var conv = _portFacade.Conversations.Where(c => c.Device.DeviceType.DeviceId == 50419).FirstOrDefault();
			Assert.IsNotNull(conv, "Test setup failure creating port facade.");
			_vm.CarriageInertia.Measurement =
				new Measurement(1500m, _vm.CarriageInertia.UnitConverter.FindUnitBySymbol("g"));
			_vm.LoadInertia.Measurement = new Measurement(0m, _vm.LoadInertia.UnitConverter.FindUnitBySymbol("g"));
			_vm.SetTargetDevice(conv, props);

			var recommendation = _vm.RecommendedAcceleration;
			Assert.IsFalse(string.IsNullOrEmpty(recommendation));
			Assert.IsFalse(string.IsNullOrEmpty(_vm.CurrentAcceleration));

			// Change the total load and verify that the recommendation changes.
			_vm.LoadInertia.Quantity = 1000;
			var rec2 = _vm.RecommendedAcceleration;
			Assert.IsFalse(string.IsNullOrEmpty(rec2));
			Assert.AreNotEqual(recommendation, rec2);
		}


		[Test]
		[Description("Test that the default carriage inertia is displayed and can be reset.")]
		public void TestRestoreDefaultCarriageInertia()
		{
			var props = TunerTestHelper.CreateDmqDeviceProperties();
			var conv = _portFacade.Conversations.Where(c => c.Device.DeviceType.DeviceId == 50419).FirstOrDefault();
			Assert.IsNotNull(conv, "Test setup failure creating port facade.");
			_vm.SetTargetDevice(conv, props);

			_vm.CarriageInertia.Measurement =
				new Measurement(600m, _vm.CarriageInertia.UnitConverter.FindUnitBySymbol("g"));
			_vm.LoadInertia.Measurement = new Measurement(0m, _vm.LoadInertia.UnitConverter.FindUnitBySymbol("g"));

			Assert.AreEqual("100 g", _vm.DefaultCarriageInertia);

			var cmd = _vm.RestoreDefaultCarriageInertiaCommand;
			Assert.IsNotNull(cmd);
			cmd.Execute(null);
			Assert.AreEqual(100m, _vm.CarriageInertia.Quantity);
			Assert.AreEqual("g", _vm.CarriageInertia.SelectedUnit.Abbreviation);
		}


		protected void SetupPort(int aControllerVersion)
		{
			_mockPort = new MockPort { IsAsciiMode = true };

			var defaultDeviceType = new DeviceType { Commands = new List<CommandInfo>() };

			_portFacade = new ZaberPortFacade
			{
				DefaultDeviceType = defaultDeviceType,
				Port = _mockPort,
				QueryTimeout = 100
			};

			_ampsPeak = new BaseUnit(UnitOfMeasure.AmperesPeakSymbol) { Dimension = Dimension.AlternatingCurrent };

			var inertiaUnit = new BaseUnit("g") { Dimension = Dimension.Inertia };

			var rotationalInertiaUnit = new BaseUnit("gm^2") { Dimension = Dimension.RotationalInertia };

			var dmqCommands = new List<CommandInfo>
			{
				new CommandInfo
				{
					TextCommand = "move abs {0}",
					RequestUnit = UnitOfMeasure.Meter,
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 5000000.0m
				},
				new CommandInfo
				{
					TextCommand = "move vel {0}",
					RequestUnit = UnitOfMeasure.MetersPerSecond,
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 8192000.0m
				},
				new SettingInfo
				{
					TextCommand = "accel",
					RequestUnit = UnitOfMeasure.MetersPerSecondSquared,
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 819.2m
				},
				new SettingInfo
				{
					TextCommand = "driver.current.run",
					RequestUnit = new UnitOfMeasure(_ampsPeak),
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 50.0m
				},
				new CommandInfo
				{
					TextCommand = "force abs {0}",
					RequestUnit = ConversionTable.Default.FindUnitByAbbreviation(UnitOfMeasure.NewtonsSymbol),
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 39.6825396825397m
				}
			};

			var servoProps = new TuningData
			{
				Inertia = new Measurement(100, inertiaUnit),
				GuidanceData =
					File.ReadAllText(Path.Combine(TestContext.CurrentContext.TestDirectory, "tuningtable_ldq_V3.json")),
				ControllerVersions = new Dictionary<string, int> { { "ffpid", aControllerVersion } }
			};

			var dmq = new DeviceType
			{
				DeviceId = 50419,
				Name = "X-DMQ12P-DE52 Stage",
				MotionType = MotionType.Linear,
				FirmwareVersion = new FirmwareVersion(6, 25),
				Commands = dmqCommands,
				TuningData = servoProps
			};

			_portFacade.AddDeviceType(dmq);

			ZaberPortFacadeTest.SetAsciiExpectations(_portFacade, _mockPort, dmq);
			_mockPort.ReadExpectationsFromString("/01 0 get accel");
			_mockPort.ReadExpectationsFromString("@01 0 OK IDLE -- 722000");

			_portFacade.Open("COM3");
			var conv = _portFacade.GetConversation(1);
			conv.Device.MicrostepResolution = 1; // Not covered by SetAsciiExpectations.
			conv.Device.DeviceType.UnitConverter.RegisterUnit(_ampsPeak); // No database.
			conv.Device.DeviceType.UnitConverter.RegisterUnit(inertiaUnit);
			conv.Device.DeviceType.UnitConverter.RegisterUnit(rotationalInertiaUnit);
		}


		protected MockPort _mockPort;
		protected ZaberPortFacade _portFacade;
		protected Unit _ampsPeak;
		protected SimpleCalculatorMethodVM _vm;
	}
}

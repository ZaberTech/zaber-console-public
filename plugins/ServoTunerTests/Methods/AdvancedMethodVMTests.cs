﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Zaber;
using Zaber.Testing;
using Zaber.Tuning;
using Zaber.Tuning.Servo;
using Zaber.Tuning.Tests;
using Zaber.Units;
using ZaberConsole.Plugins.ServoTuner;
using ZaberConsole.Plugins.ServoTuner.Methods;
using ZaberTest;

namespace ServoTunerTests.Methods
{
	[TestFixture]
	[SetCulture("en-US")]
	public class AdvancedMethodVMTests
	{
		[SetUp]
		public void Setup()
		{
			DispatcherStack.Push(new ImmediateDispatcher());

			_vm = new AdvancedMethodVM();
			_deviceProperties = TunerTestHelper.CreateDmqDeviceProperties();
			SetupPort();
		}


		[TearDown]
		public void Teardown()
		{
			_portFacade.Close();
			_listener?.Unlisten(_vm);
			DispatcherStack.Pop();
		}


		[Test]
		public void TestConstructor()
		{
			Assert.IsFalse(_vm.CanRead);
			Assert.IsFalse(_vm.CanWrite);
			Assert.IsNotEmpty(_vm.TuningModeName);
			Assert.IsNotNull(_vm.EditingParameters);
			Assert.IsNull(_vm.EditingParameters.GetEditedParameters());
			Assert.IsTrue(_vm.EditingParameters.IsEditable);
		}


		[Test]
		public void TestObservableProperties()
		{
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
			_listener.TestObservableProperty(_vm, () => _vm.CanRead);
			_listener.TestObservableProperty(_vm, () => _vm.CanWrite);
			_listener.TestObservableProperty(_vm, () => _vm.TuningModeName);
			_listener.TestObservableProperty(_vm,
											 () => _vm.EditingParameters,
											 c => new ParameterListVM(
												 new TunerParameterSet(TuningModeNames.FFPID)));
		}


		[Test]
		public void TestReadWrite()
		{
			var conv = _portFacade.Conversations.Where(c => c.Device.DeviceType.DeviceId == 50419).FirstOrDefault();
			Assert.IsNotNull(conv, "Test setup failure creating port facade.");
			_vm.SetTargetDevice(conv, _deviceProperties);

			var parms = ParameterSetHelper.MakePidParams(_deviceProperties);
			_vm.DeviceParameters = parms;
			Assert.IsTrue(parms.IsIdenticalTo(_vm.DeviceParameters));
		}


		private void SetupPort()
		{
			_mockPort = new MockPort { IsAsciiMode = true };

			var defaultDeviceType = new DeviceType { Commands = new List<CommandInfo>() };

			_portFacade = new ZaberPortFacade
			{
				DefaultDeviceType = defaultDeviceType,
				Port = _mockPort,
				QueryTimeout = 100
			};

			_ampsPeak = new BaseUnit(UnitOfMeasure.AmperesPeakSymbol) { Dimension = Dimension.AlternatingCurrent };

			var dmqCommands = new List<CommandInfo>
			{
				new CommandInfo
				{
					TextCommand = "move abs {0}",
					RequestUnit = UnitOfMeasure.Meter,
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 5000000.0m
				},
				new CommandInfo
				{
					TextCommand = "move vel {0}",
					RequestUnit = UnitOfMeasure.MetersPerSecond,
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 8192000.0m
				},
				new SettingInfo
				{
					TextCommand = "accel",
					RequestUnit = UnitOfMeasure.MetersPerSecondSquared,
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 819.2m
				},
				new SettingInfo
				{
					TextCommand = "driver.current.run",
					RequestUnit = new UnitOfMeasure(_ampsPeak),
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 50.0m
				},
				new CommandInfo
				{
					TextCommand = "force abs {0}",
					RequestUnit = ConversionTable.Default.FindUnitByAbbreviation(UnitOfMeasure.NewtonsSymbol),
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 39.6825396825397m
				}
			};

			var dmq = new DeviceType
			{
				DeviceId = 50419,
				Name = "X-DMQ12P-DE52 Stage",
				MotionType = MotionType.Linear,
				FirmwareVersion = new FirmwareVersion(6, 25),
				Commands = dmqCommands
			};

			_portFacade.AddDeviceType(dmq);

			ZaberPortFacadeTest.SetAsciiExpectations(_portFacade, _mockPort, dmq);

			_portFacade.Open("COM3");
			var conv = _portFacade.GetConversation(1);
			conv.Device.MicrostepResolution = 1; // Not covered by SetAsciiExpectations.
			conv.Device.DeviceType.UnitConverter.RegisterUnit(_ampsPeak); // No database.
		}


		private MockPort _mockPort;
		private ZaberPortFacade _portFacade;
		private Unit _ampsPeak;
		private AdvancedMethodVM _vm;
		private DeviceProperties _deviceProperties;
		private ObservableObjectTester _listener;
	}
}

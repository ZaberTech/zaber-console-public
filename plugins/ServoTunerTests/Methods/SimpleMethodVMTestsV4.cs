﻿using System.Linq;
using NUnit.Framework;
using Zaber.Testing;
using Zaber.Tuning.Servo;
using Zaber.Tuning.Tests;
using Zaber.Units;
using ZaberConsole.Plugins.ServoTuner.Methods;
using ZaberTest.Testing;

namespace ServoTunerTests.Methods
{
	[TestFixture]
	[SetCulture("en-US")]
	public class SimpleMethodVMTestsV4 : SimpleMethodVMTestBase
	{
		[SetUp]
		public void Setup()
		{
			DispatcherStack.Push(new ImmediateDispatcher());
			_vm = new SimpleCalculatorMethodVM();
			SetupPort(4);
		}


		[TearDown]
		public void Teardown()
		{
			_portFacade.Close();
			DispatcherStack.Pop();
		}


		[Test]
		[Description("Test that the factory-defined carriage mass is used.")]
		public void RM7087_TestCalculateUsesFactoryCarriageInertia()
		{
			var props = TunerTestHelper.CreateDmqDeviceProperties();
			var conv = _portFacade.Conversations.FirstOrDefault(c => c.Device.DeviceType.DeviceId == 50419);
			Assert.IsNotNull(conv, "Test setup failure creating port facade.");
			_vm.SetTargetDevice(conv, props);

			var dummy = new DummyTuner();
			dummy.Result = new TunerParameterSet();
			InvocationHelper.SetPrivateField(_vm, "_tuner", dummy);

			var grams = _vm.CarriageInertia.UnitConverter.FindUnitBySymbol("g");
			_vm.CarriageInertia.Measurement =
				new Measurement(50m, grams);
			_vm.LoadInertia.Measurement = new Measurement(200m, grams);

			var _ = _vm.DeviceParameters;

			// Device carriage inertia should come from the device database via the DeviceType.
			Assert.AreEqual(new Measurement(100m, grams), dummy.DevicePropeties.CarriageInertia);

			// Load inertia should be the sum of the user-entered values minus the database carriage inertia.
			Assert.AreEqual(new Measurement(150m, grams), dummy.LoadProperties.LoadInertia);
		}
	}
}

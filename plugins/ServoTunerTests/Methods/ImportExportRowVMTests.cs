﻿using NUnit.Framework;
using Zaber.Testing;
using Zaber.Tuning;
using Zaber.Tuning.Servo;
using ZaberConsole.Plugins.ServoTuner.Methods;

namespace ServoTunerTests.Methods
{
	[TestFixture]
	[SetCulture("en-US")]
	public class ImportExportRowVMTests
	{
		[SetUp]
		public void Setup()
		{
			_params = ParameterSetHelper.MakeParams();
			_vm = new ImportExportRowVM(_params, "foo");
		}


		[TearDown]
		public void Teardown() => _listener?.Unlisten(_vm);


		[Test]
		public void TestConstructor()
		{
			Assert.IsTrue(_params.IsIdenticalTo(_vm.ParamSet));
			Assert.AreEqual("foo", _vm.Name);
			Assert.AreEqual(TuningModeNames.FFPID, _vm.Mode);

			var values = _vm.Parameters;
			foreach (var name in _params.ParameterNames)
			{
				Assert.IsTrue(values.Contains(name));
			}
		}


		[Test]
		public void TestObservableProperties()
		{
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
			_listener.TestObservableProperty(_vm, () => _vm.Name);
		}


		private ITunerParameterSet _params;
		private ImportExportRowVM _vm;
		private ObservableObjectTester _listener;
	}
}

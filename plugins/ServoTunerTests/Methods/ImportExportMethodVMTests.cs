﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Zaber;
using Zaber.Testing;
using Zaber.Tuning;
using Zaber.Tuning.Servo;
using Zaber.Tuning.Tests;
using Zaber.Units;
using ZaberConsole.Plugins.ServoTuner.Methods;
using ZaberTest;
using ZaberWpfToolbox.Dialogs;
using Measurement = Zaber.Units.Measurement;

namespace ServoTunerTests.Methods
{
	[TestFixture]
	[SetCulture("en-US")]
	public class ImportExportMethodVMTests
	{
		[SetUp]
		public void Setup()
		{
			DispatcherStack.Push(new ImmediateDispatcher());

			_vm = new ImportExportMethodVM();
			_deviceProperties = TunerTestHelper.CreateDmqDeviceProperties();
			SetupPort();
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
		}


		[TearDown]
		public void Teardown()
		{
			_portFacade.Close();
			_listener?.Unlisten(_vm);
			DispatcherStack.Pop();
		}


		[Test]
		public void TestConstructor()
		{
			Assert.IsFalse(_vm.CanRead);
			Assert.IsFalse(_vm.CanWrite);
			Assert.IsNull(_vm.CurrentFilePath);
			Assert.IsNull(_vm.Rows);
			Assert.IsNull(_vm.SelectedRow);
			Assert.IsTrue(_vm.IsSaved);
		}


		[Test]
		public void TestDeleteRow()
		{
			var conv = _portFacade.Conversations.Where(c => c.Device.DeviceType.DeviceId == 50419).FirstOrDefault();
			Assert.IsNotNull(conv, "Test setup failure creating port facade.");
			_vm.SetTargetDevice(conv, _deviceProperties);

			// Generate some data and import it.
			var set = CreateControllerPidTestParams();
			_listener.ClearNotifications();
			_vm.DeviceParameters = set;
			Assert.IsTrue(_listener.PropertyWasChanged(nameof(_vm.Rows)));
			set = CreateControllerPidTestParams();
			_vm.DeviceParameters = set;

			// Delete a row.
			_vm.DeleteRowCommand.Execute(_vm.Rows[0]);
			Assert.AreEqual(1, _vm.Rows.Count);
			Assert.AreEqual(TuningModeNames.FFPID, _vm.Rows[0].Mode);
		}


		[Test]
		public void TestMoveRowDown()
		{
			var conv = _portFacade.Conversations.Where(c => c.Device.DeviceType.DeviceId == 50419).FirstOrDefault();
			Assert.IsNotNull(conv, "Test setup failure creating port facade.");
			_vm.SetTargetDevice(conv, _deviceProperties);

			// Generate some data and import it.
			var set = CreateControllerPidTestParams();
			_listener.ClearNotifications();
			_vm.DeviceParameters = set;
			Assert.IsTrue(_listener.PropertyWasChanged(nameof(_vm.Rows)));
			set = CreateControllerPidTestParams();
			_vm.DeviceParameters = set;

			// Move the first row down.
			Assert.IsTrue(_vm.MoveDownCommand.CanExecute(_vm.Rows[0]));
			_vm.MoveDownCommand.Execute(_vm.Rows[0]);
			Assert.AreEqual(2, _vm.Rows.Count);
			Assert.AreEqual(TuningModeNames.FFPID, _vm.Rows[1].Mode);

			// Verify the second row can't be moved down.
			Assert.IsFalse(_vm.MoveDownCommand.CanExecute(_vm.Rows[1]));
		}


		[Test]
		public void TestMoveRowUp()
		{
			var conv = _portFacade.Conversations.Where(c => c.Device.DeviceType.DeviceId == 50419).FirstOrDefault();
			Assert.IsNotNull(conv, "Test setup failure creating port facade.");
			_vm.SetTargetDevice(conv, _deviceProperties);

			// Generate some data and import it.
			var set = CreateControllerPidTestParams();
			_listener.ClearNotifications();
			_vm.DeviceParameters = set;
			Assert.IsTrue(_listener.PropertyWasChanged(nameof(_vm.Rows)));
			set = CreateControllerPidTestParams();
			_vm.DeviceParameters = set;

			// Move the second row up.
			Assert.IsTrue(_vm.MoveUpCommand.CanExecute(_vm.Rows[1]));
			_vm.MoveUpCommand.Execute(_vm.Rows[1]);
			Assert.AreEqual(2, _vm.Rows.Count);
			Assert.AreEqual(TuningModeNames.FFPID, _vm.Rows[0].Mode);

			// Verify the first row can't be moved up.
			Assert.IsFalse(_vm.MoveUpCommand.CanExecute(_vm.Rows[0]));
		}


		[Test]
		public void TestNewFile()
		{
			var conv = _portFacade.Conversations.Where(c => c.Device.DeviceType.DeviceId == 50419).FirstOrDefault();
			Assert.IsNotNull(conv, "Test setup failure creating port facade.");
			_vm.SetTargetDevice(conv, _deviceProperties);

			// Generate some data and import it.
			var set = CreateControllerPidTestParams();
			_vm.DeviceParameters = set;
			set = CreateControllerPidTestParams();
			_vm.DeviceParameters = set;

			// Verify the VM properties were updated by the import.
			Assert.AreEqual(2, _vm.Rows.Count);
			Assert.IsFalse(_vm.IsSaved);

			// Dialog box handler to discard the test data.
			_vm.RequestDialogOpen += (aParent, aChild) =>
			{
				if (aChild is CustomMessageBoxVM mbox)
				{
					if (mbox.Message.Contains("unsaved"))
					{
						mbox.DialogResult = 3;
					}
				}
			};

			_listener.ClearNotifications();

			// Execute the new file command.
			_vm.NewFileCommand.Execute(null);

			// Verify the VM state changed to reflect a successful clear.
			Assert.IsTrue(_listener.PropertyWasChanged(nameof(_vm.IsSaved)));
			Assert.IsTrue(_listener.PropertyWasChanged(nameof(_vm.Rows)));
			Assert.AreEqual(0, _vm.Rows.Count);
			Assert.IsNull(_vm.SelectedRow);
		}


		[Test]
		public void TestObservableProperties()
		{
			_listener.TestObservableProperty(_vm, () => _vm.CanRead);
			_listener.TestObservableProperty(_vm, () => _vm.CanWrite);
			_listener.TestObservableProperty(_vm, () => _vm.CurrentFilePath);
			_listener.TestObservableProperty(_vm,
											 () => _vm.Rows,
											 c => new ObservableCollection<ImportExportRowVM>
											 {
												 new ImportExportRowVM(
													 new TunerParameterSet(TuningModeNames.FFPID))
											 });

			_listener.ClearNotifications();
			_listener.TestObservableProperty(_vm,
											 () => _vm.SelectedRow,
											 r => new ImportExportRowVM(
												 new TunerParameterSet(TuningModeNames.FFPID)));

			_listener.TestObservableProperty(_vm, () => _vm.IsSaved);
		}


		[Test]
		public void TestOpenFile()
		{
			var conv = _portFacade.Conversations.Where(c => c.Device.DeviceType.DeviceId == 50419).FirstOrDefault();
			Assert.IsNotNull(conv, "Test setup failure creating port facade.");
			_vm.SetTargetDevice(conv, _deviceProperties);

			var path = Path.Combine(TestContext.CurrentContext.TestDirectory, "Methods", "three.csv");

			var errorOccurred = false;

			// Dialog box handler to load the test file.
			_vm.RequestDialogOpen += (aParent, aChild) =>
			{
				if (aChild is FileBrowserDialogParams dlg)
				{
					dlg.Filename = path;
					dlg.DialogResult = true;
				}

				if (aChild is MessageBoxParams mbox)
				{
					errorOccurred = true;
				}
			};

			_listener.ClearNotifications();

			// Open the file.
			_vm.OpenFileCommand.Execute(null);

			// Verify error message box did not appear.
			Assert.IsFalse(errorOccurred);

			// Verify the VM state changed to reflect a successful load.
			Assert.IsTrue(_listener.PropertyWasChanged(nameof(_vm.Rows)));
			Assert.IsTrue(_listener.PropertyWasChanged(nameof(_vm.CurrentFilePath)));
			Assert.IsNotNull(_vm.Rows);
			Assert.IsNull(_vm.SelectedRow);
			Assert.IsTrue(_vm.IsSaved);
			Assert.AreEqual(path, _vm.CurrentFilePath);

			// Verify the right data was loaded.
			Assert.AreEqual(3, _vm.Rows.Count);
			var rows = _vm.Rows.ToArray();
			Assert.AreEqual("live", rows[0].Name);
			Assert.AreEqual("staging", rows[1].Name);
			Assert.AreEqual("Preset 1", rows[2].Name);
			Assert.AreEqual(TuningModeNames.FFPID, rows[1].Mode);
			Assert.AreEqual(1, rows[1].ControllerVersion);
			Assert.IsFalse(string.IsNullOrEmpty(rows[0].Parameters));
			Assert.IsFalse(string.IsNullOrEmpty(rows[1].Parameters));
			Assert.IsFalse(string.IsNullOrEmpty(rows[2].Parameters));
			Assert.IsNotNull(rows[0].ParamSet);
			Assert.IsNotNull(rows[1].ParamSet);
			Assert.IsNotNull(rows[2].ParamSet);
		}


		[Test]
		public void TestOutputWithoutConversion()
		{
			var conv = _portFacade.Conversations.Where(c => c.Device.DeviceType.DeviceId == 50419).FirstOrDefault();
			Assert.IsNotNull(conv, "Test setup failure creating port facade.");
			_vm.SetTargetDevice(conv, _deviceProperties);

			// Generate some data and import it.
			_listener.ClearNotifications();
			_vm.DeviceParameters = CreateControllerPidTestParams();
			Assert.IsTrue(_listener.PropertyWasChanged(nameof(_vm.Rows)));

			// Select the row that doesn't need conversion.
			_vm.SelectedRow = _vm.Rows[0];
			Assert.IsTrue(_vm.CanWrite);

			_listener.ClearNotifications();
			var cp = _vm.DeviceParameters;
			Assert.IsNotNull(cp);
			Assert.IsTrue(cp.IsIdenticalTo(_vm.Rows[0].ParamSet));
		}


		[Test]
		public void TestSave()
		{
			var conv = _portFacade.Conversations.Where(c => c.Device.DeviceType.DeviceId == 50419).FirstOrDefault();
			Assert.IsNotNull(conv, "Test setup failure creating port facade.");
			_vm.SetTargetDevice(conv, _deviceProperties);

			var path = Path.Combine(TestContext.CurrentContext.TestDirectory, "Methods", "save_output.csv");
			if (!File.Exists(path))
			{
				using (var stream = File.Create(path))
				{
				}
			}

			// Generate some data and import it.
			var set = CreateControllerPidTestParams();
			_vm.DeviceParameters = set;
			set = CreateControllerPidTestParams();
			_vm.DeviceParameters = set;

			// Verify the VM properties were updated by the import.
			Assert.AreEqual(2, _vm.Rows.Count);
			Assert.IsFalse(_vm.IsSaved);

			_listener.ClearNotifications();

			// Save the file.
			_vm.SaveAsCommand.Execute(path);

			// Verify the VM state changed to reflect a successful load.
			Assert.IsTrue(_listener.PropertyWasChanged(nameof(_vm.IsSaved)));

			// Verify the file now exists.
			Assert.IsTrue(File.Exists(path));

			// Clear the VM content and read the file back.
			_vm.Rows.Clear();
			_vm.IsSaved = true;

			var errorOccurred = false;

			// Dialog box handler to reopen the test file.
			_vm.RequestDialogOpen += (aParent, aChild) =>
			{
				if (aChild is FileBrowserDialogParams dlg)
				{
					dlg.Filename = path;
					dlg.DialogResult = true;
				}

				if (aChild is MessageBoxParams mbox)
				{
					errorOccurred = true;
				}
			};

			_vm.OpenFileCommand.Execute(null);
			Assert.IsFalse(errorOccurred);
			Assert.AreEqual(2, _vm.Rows.Count);
			Assert.AreEqual(TuningModeNames.FFPID, _vm.Rows[0].Mode);
			VerifyTestParams(_vm.Rows[0].ParamSet);

			if (File.Exists(path))
			{
				File.Delete(path);
			}
		}


		[Test]
		public void TestSaveAs()
		{
			var conv = _portFacade.Conversations.Where(c => c.Device.DeviceType.DeviceId == 50419).FirstOrDefault();
			Assert.IsNotNull(conv, "Test setup failure creating port facade.");
			_vm.SetTargetDevice(conv, _deviceProperties);

			var path = Path.Combine(TestContext.CurrentContext.TestDirectory, "Methods", "save_as_output.csv");
			if (File.Exists(path))
			{
				File.Delete(path);
			}

			// Generate some data and import it.
			var set = CreateControllerPidTestParams();
			_vm.DeviceParameters = set;
			set = CreateControllerPidTestParams();
			_vm.DeviceParameters = set;

			// Verify the VM properties were updated by the import.
			Assert.AreEqual(2, _vm.Rows.Count);
			Assert.IsFalse(_vm.IsSaved);

			// Dialog box handler to save the test file.
			_vm.RequestDialogOpen += (aParent, aChild) =>
			{
				if (aChild is FileBrowserDialogParams dlg)
				{
					dlg.Filename = path;
					dlg.DialogResult = true;
				}

				if (aChild is CustomMessageBoxVM mbox)
				{
					if (mbox.Message.Contains("unsaved"))
					{
						mbox.DialogResult = 3;
					}
				}
			};

			_listener.ClearNotifications();

			// Save the file.
			_vm.SaveAsCommand.Execute(null);

			// Verify the VM state changed to reflect a successful load.
			Assert.IsTrue(_listener.PropertyWasChanged(nameof(_vm.IsSaved)));

			// Verify the file now exists.
			Assert.IsTrue(File.Exists(path));

			// Clear the VM content and read the file back.
			_vm.Rows.Clear();
			_vm.OpenFileCommand.Execute(null);
			Assert.AreEqual(2, _vm.Rows.Count);
			Assert.AreEqual(TuningModeNames.FFPID, _vm.Rows[0].Mode);
			VerifyTestParams(_vm.Rows[0].ParamSet);

			if (File.Exists(path))
			{
				File.Delete(path);
			}
		}


		private ITunerParameterSet CreatePidParams()
		{
			var set = ParameterSetHelper.MakePidParams(_deviceProperties);
			var uc = set.UnitConverter;
			set[PidTunerV3.PARAM_KP] =
				new Measurement(0.33231133818075104, uc.FindUnitBySymbol(PidTunerV3.PARAM_KP_UNITS));
			set[PidTunerV3.PARAM_KI] = new Measurement(0.024967174622703502, uc.FindUnitBySymbol("s"));
			set[PidTunerV3.PARAM_FC] = new Measurement(1.0 / 7.5283137491669882e-05, uc.FindUnitBySymbol("Hz"));
			set[PidTunerV3.PARAM_KD] = new Measurement(0.0030317830002577501, uc.FindUnitBySymbol("s"));
			return set;
		}


		private static ITunerParameterSet CreateUserPidTestParams()
		{
			var set = ParameterSetHelper.MakePidParams();

			foreach (var name in set.ParameterNames)
			{
				set[name] = new Measurement(name.GetHashCode(), set[name].Unit);
			}

			return set;
		}


		private static ITunerParameterSet CreateControllerPidTestParams()
		{
			var set = TunerTestHelper.CreateEmptySet(TuningModeNames.FFPID,
													 UnitConverter.Default,
													 TunerTestHelper.FFPID_SETTINGS_V3);

			foreach (var name in set.ParameterNames)
			{
				set[name] = new Measurement(name.GetHashCode(), set[name].Unit);
			}

			return set;
		}


		private static void VerifyTestParams(ITunerParameterSet aSet)
		{
			foreach (var name in aSet.ParameterNames)
			{
				Assert.AreEqual(name.GetHashCode(), (int) Math.Round(aSet[name].Value));
			}
		}


		private void SetupPort()
		{
			_mockPort = new MockPort { IsAsciiMode = true };

			var defaultDeviceType = new DeviceType { Commands = new List<CommandInfo>() };

			_portFacade = new ZaberPortFacade
			{
				DefaultDeviceType = defaultDeviceType,
				Port = _mockPort,
				QueryTimeout = 100
			};

			_ampsPeak = new BaseUnit(UnitOfMeasure.AmperesPeakSymbol) { Dimension = Dimension.AlternatingCurrent };

			var dmqCommands = new List<CommandInfo>
			{
				new CommandInfo
				{
					TextCommand = "move abs {0}",
					RequestUnit = UnitOfMeasure.Meter,
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 5000000.0m
				},
				new CommandInfo
				{
					TextCommand = "move vel {0}",
					RequestUnit = UnitOfMeasure.MetersPerSecond,
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 8192000.0m
				},
				new SettingInfo
				{
					TextCommand = "accel",
					RequestUnit = UnitOfMeasure.MetersPerSecondSquared,
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 819.2m
				},
				new SettingInfo
				{
					TextCommand = "driver.current.run",
					RequestUnit = new UnitOfMeasure(_ampsPeak),
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 50.0m
				},
				new CommandInfo
				{
					TextCommand = "force abs {0}",
					RequestUnit = ConversionTable.Default.FindUnitByAbbreviation(UnitOfMeasure.NewtonsSymbol),
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 39.6825396825397m
				}
			};

			var inertiaUnit = new BaseUnit("g") { Dimension = Dimension.Inertia };

			var servoProps = new TuningData
			{
				Inertia = new Measurement(100, inertiaUnit),
				GuidanceData =
					File.ReadAllText(Path.Combine(TestContext.CurrentContext.TestDirectory, "tuningtable_ldq_V3.json")),
				ControllerVersions = new Dictionary<string, int> { { "ffpid", 1 } }
			};

			var dmq = new DeviceType
			{
				DeviceId = 50419,
				Name = "X-DMQ12P-DE52 Stage",
				MotionType = MotionType.Linear,
				FirmwareVersion = new FirmwareVersion(6, 25),
				Commands = dmqCommands,
				TuningData = servoProps
			};

			_portFacade.AddDeviceType(dmq);

			ZaberPortFacadeTest.SetAsciiExpectations(_portFacade, _mockPort, dmq);

			_portFacade.Open("COM3");
			var conv = _portFacade.GetConversation(1);
			conv.Device.MicrostepResolution = 1; // Not covered by SetAsciiExpectations.
			conv.Device.DeviceType.UnitConverter.RegisterUnit(_ampsPeak); // No database.
		}


		private MockPort _mockPort;
		private ZaberPortFacade _portFacade;
		private Unit _ampsPeak;
		private ImportExportMethodVM _vm;
		private DeviceProperties _deviceProperties;
		private ObservableObjectTester _listener;
	}
}

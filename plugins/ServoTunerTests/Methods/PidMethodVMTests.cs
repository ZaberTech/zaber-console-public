﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Zaber;
using Zaber.Testing;
using Zaber.Tuning.Servo;
using Zaber.Tuning.Tests;
using Zaber.Units;
using ZaberConsole.Plugins.ServoTuner.Methods;
using ZaberTest;
using ZaberTest.Testing;
using Measurement = Zaber.Units.Measurement;

namespace ServoTunerTests.Methods
{
	[TestFixture]
	[SetCulture("en-US")]
	public class PidMethodVMTests
	{
		[SetUp]
		public void Setup()
		{
			DispatcherStack.Push(new ImmediateDispatcher());
			_vm = new PidMethodVM();
		}


		[TearDown]
		public void Teardown()
		{
			_portFacade?.Close();
			DispatcherStack.Pop();
		}


		[Test]
		[Description("Test default values.")]
		public void TestConstructor()
		{
			Assert.IsFalse(_vm.CanRead);
			Assert.IsFalse(_vm.CanWrite);

			Assert.Less(0, _vm.UserParams.Count);
			var parms = ParameterSetHelper.MakePidParams();
			foreach (var param in _vm.UserParams)
			{
				Assert.IsTrue(parms.ParameterNames.Contains(param.Name));
				Assert.IsFalse(string.IsNullOrEmpty(param.Help));
				Assert.IsNotNull(param.Value);
			}

			foreach (var param in parms.ParameterNames)
			{
				Assert.IsTrue(_vm.UserParams.Select(p => p.Name).Contains(param));
			}
		}


		[Test]
		[Description("Test that invoking the reverse conversion command produces nonzero output.")]
		public void TestInputV3()
		{
			SetupPort(3);
			var props = TunerTestHelper.CreateDmqDeviceProperties();
			var conv = _portFacade.Conversations.Where(c => c.Device.DeviceType.DeviceId == 50419).FirstOrDefault();
			Assert.IsNotNull(conv, "Test setup failure creating port facade.");
			_vm.SetTargetDevice(conv, props);

			// Setting the previous PID properties should invoke a view update.
			var p = ParameterSetHelper.MakePidParams(props);
			p["kd"] = new Measurement(392915749, null);
			p["ki"] = new Measurement(99925871.0, null);
			p["kp"] = new Measurement(32393425.0, null);
			p["fc1"] = new Measurement(1073741824.0, null);
			_vm.DeviceParameters = p;

			// Check that the reverse-calculated value has nonzero data.
			var nonZero = false;
			foreach (var vm in _vm.UserParams)
			{
				nonZero |= vm.Value.Quantity != 0m;
			}

			Assert.IsTrue(nonZero);
		}


		[Test]
		[Description("Test that invoking the reverse conversion command produces nonzero output.")]
		public void TestInputV4()
		{
			SetupPort(4);
			var props = TunerTestHelper.CreateDmqDeviceProperties();
			var conv = _portFacade.Conversations.Where(c => c.Device.DeviceType.DeviceId == 50419).FirstOrDefault();
			Assert.IsNotNull(conv, "Test setup failure creating port facade.");
			_vm.SetTargetDevice(conv, props);

			// Setting the previous PID properties should invoke a view update.
			var p = ParameterSetHelper.MakePidParams(props);
			p["kd"] = new Measurement(0.392915749, null);
			p["ki"] = new Measurement(0.099925871, null);
			p["kp"] = new Measurement(0.032393425, null);
			p["fc1"] = new Measurement(1.073741824, null);
			p["gain"] = new Measurement(0.2, null);
			_vm.DeviceParameters = p;

			// Check that the reverse-calculated value has nonzero data.
			var nonZero = false;
			foreach (var vm in _vm.UserParams)
			{
				nonZero |= vm.Value.Quantity != 0m;
			}

			Assert.IsTrue(nonZero);
		}


		[Test]
		[Description("Test that invoking the forward conversion command produces nonzero output.")]
		public void TestOutputV3()
		{
			SetupPort(3);
			TestOutputCommon();
		}


		[Test]
		[Description("Test that invoking the forward conversion command produces nonzero output.")]
		public void TestOutputV4()
		{
			SetupPort(4);
			TestOutputCommon();
		}


		private void TestOutputCommon()
		{
			var props = TunerTestHelper.CreateDmqDeviceProperties();
			var conv = _portFacade.Conversations.Where(c => c.Device.DeviceType.DeviceId == 50419).FirstOrDefault();
			Assert.IsNotNull(conv, "Test setup failure creating port facade.");
			_vm.SetTargetDevice(conv, props);

			var pidParams =
				InvocationHelper.InvokePrivateStaticMethod<PidMethodVM>("CreateUserPidParameterSet", props) as
					TunerParameterSet;

			var upa = _vm.UserParams.Select(p => p.Name).ToArray();
			var i = Array.IndexOf(upa, PidTunerV3.PARAM_KP);
			var pvm = _vm.UserParams[i].Value;
			var uc = pvm.UnitConverter;
			pvm.Measurement = new Measurement(0.33231133818075104, uc.FindUnitBySymbol(PidTunerV3.PARAM_KP_UNITS));

			i = Array.IndexOf(upa, PidTunerV3.PARAM_KI);
			pvm = _vm.UserParams[i].Value;
			uc = pvm.UnitConverter;
			pvm.Measurement = new Measurement(0.024967174622703502, uc.FindUnitBySymbol("mN/m⋅s"));

			i = Array.IndexOf(upa, PidTunerV3.PARAM_KD);
			pvm = _vm.UserParams[i].Value;
			uc = pvm.UnitConverter;
			pvm.Measurement = new Measurement(1.0 / 7.5283137491669882e-05, uc.FindUnitBySymbol("mN⋅s/m"));

			i = Array.IndexOf(upa, PidTunerV3.PARAM_FC);
			pvm = _vm.UserParams[i].Value;
			uc = pvm.UnitConverter;
			pvm.Measurement = new Measurement(0.0030317830002577501, uc.FindUnitBySymbol("kHz"));

			// Check that the calculated value has nonzero data.
			var readBack = _vm.DeviceParameters;
			Assert.IsNotNull(readBack);

			var nonZero = false;
			foreach (var name in readBack.ParameterNames)
			{
				var val = readBack[name].Value;
				nonZero |= val != 0.0;
			}

			Assert.IsTrue(nonZero);
		}


		private void SetupPort(int aControllerVersion)
		{
			_mockPort = new MockPort { IsAsciiMode = true };

			var defaultDeviceType = new DeviceType { Commands = new List<CommandInfo>() };

			_portFacade = new ZaberPortFacade
			{
				DefaultDeviceType = defaultDeviceType,
				Port = _mockPort,
				QueryTimeout = 100
			};

			_ampsPeak = new BaseUnit(UnitOfMeasure.AmperesPeakSymbol) { Dimension = Dimension.AlternatingCurrent };

			var dmqCommands = new List<CommandInfo>
			{
				new CommandInfo
				{
					TextCommand = "move abs {0}",
					RequestUnit = UnitOfMeasure.Meter,
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 5000000.0m
				},
				new CommandInfo
				{
					TextCommand = "move vel {0}",
					RequestUnit = UnitOfMeasure.MetersPerSecond,
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 8192000.0m
				},
				new SettingInfo
				{
					TextCommand = "accel",
					RequestUnit = UnitOfMeasure.MetersPerSecondSquared,
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 819.2m
				},
				new SettingInfo
				{
					TextCommand = "driver.current.run",
					RequestUnit = new UnitOfMeasure(_ampsPeak),
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 50.0m
				},
				new CommandInfo
				{
					TextCommand = "force abs {0}",
					RequestUnit = ConversionTable.Default.FindUnitByAbbreviation(UnitOfMeasure.NewtonsSymbol),
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 39.6825396825397m
				}
			};

			var dmq = new DeviceType
			{
				DeviceId = 50419,
				Name = "X-DMQ12P-DE52 Stage",
				MotionType = MotionType.Linear,
				FirmwareVersion = new FirmwareVersion(6, 25),
				Commands = dmqCommands,
				TuningData = new TuningData
				{
					ForqueConstant = 15.1,
					Inertia = new Measurement(100.0, UnitConverter.Default.FindUnitBySymbol("g")),
					ControllerVersions = new Dictionary<string, int> { { "ffpid", aControllerVersion } }
				}
			};

			_portFacade.AddDeviceType(dmq);

			ZaberPortFacadeTest.SetAsciiExpectations(_portFacade, _mockPort, dmq);

			_portFacade.Open("COM3");
			var conv = _portFacade.GetConversation(1);
			conv.Device.MicrostepResolution = 1; // Not covered by SetAsciiExpectations.
			conv.Device.DeviceType.UnitConverter.RegisterUnit(_ampsPeak); // No database.
		}


		private MockPort _mockPort;
		private ZaberPortFacade _portFacade;
		private Unit _ampsPeak;
		private PidMethodVM _vm;
	}
}

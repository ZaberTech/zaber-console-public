﻿using Zaber.Tuning.Servo;

namespace ServoTunerTests.Methods
{
	internal class DummyTuner : ITuner
	{
		public ITunerParameterSet CalculateParameters(DeviceProperties aDevice, LoadProperties aLoad,
													  ITunerParameterSet aGuidance)
		{
			DevicePropeties = aDevice;
			LoadProperties = aLoad;
			Guidance = aGuidance;
			return Result;
		}


		public DeviceProperties DevicePropeties { get; private set; }
		public LoadProperties LoadProperties { get; private set; }
		public ITunerParameterSet Guidance { get; private set; }
		public ITunerParameterSet Result { get; set; }
	};
}

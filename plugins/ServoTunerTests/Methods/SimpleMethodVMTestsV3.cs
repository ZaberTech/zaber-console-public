﻿using NUnit.Framework;
using Zaber.Testing;
using ZaberConsole.Plugins.ServoTuner.Methods;

namespace ServoTunerTests.Methods
{
	[TestFixture]
	[SetCulture("en-US")]
	public class SimpleMethodVMTestsV3 : SimpleMethodVMTestBase
	{
		[SetUp]
		public void Setup()
		{
			DispatcherStack.Push(new ImmediateDispatcher());
			_vm = new SimpleCalculatorMethodVM();
			SetupPort(3);
		}


		[TearDown]
		public void Teardown()
		{
			_portFacade.Close();
			DispatcherStack.Pop();
		}
	}
}

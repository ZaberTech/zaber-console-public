﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Zaber;
using Zaber.Testing;
using Zaber.Tuning;
using Zaber.Tuning.Servo;
using Zaber.Tuning.Tests;
using Zaber.Units;
using ZaberConsole.Plugins.ServoTuner;
using ZaberConsole.Plugins.ServoTuner.Methods;
using ZaberTest;
using ZaberTest.Testing;
using ZaberWpfToolboxTests;
using DescriptionAttribute = NUnit.Framework.DescriptionAttribute;
using Measurement = Zaber.Units.Measurement;
// ReSharper disable StringLiteralTypo

namespace ServoTunerTests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class ServoTunerPluginVMTests
	{
		[SetUp]
		public void SetUp()
		{
			DispatcherStack.Push(new ImmediateDispatcher());
			BackgroundWorkerStack.Push(typeof(ImmediateForegroundWorker));

			_amps = new BaseUnit(UnitOfMeasure.AmperesSymbol) { Dimension = Dimension.DirectCurrent };

			SetupPort();

			_vm = new ServoTunerPluginVM { PortFacade = _portFacade };

			_portFacade.Open("COM3");
			_conversation = _portFacade.GetConversation(1);
			_conversation.Device.MicrostepResolution = 1; // Not covered by SetAsciiExpectations.
			_conversation.Device.DeviceType.UnitConverter.RegisterUnit(_amps); // No database.
		}


		[TearDown]
		public void Teardown()
		{
			BackgroundWorkerStack.Pop();
			DispatcherStack.Pop();
			_mockPort.Verify();
			_listener?.Unlisten(_vm);
		}


		[Test]
		[Description("Test that changing values in a preset informs the user that the live values no longer match a preset, and that undoing the change also updates the information.")]
		public void TestActivePresetUnsetAfterWriteToPreset()
		{
			_mockPort.ReadExpectationsFromFile(Path.Combine(TestContext.CurrentContext.TestDirectory, "dmq_scan.txt"));
			var method = new TestMethodVM();

			_vm.SelectedTunerType = method;
			_vm.Conversation = _conversation;
			_vm.OnShown();

			// Wait for device query thread to end.
			DispatcherUtil.DoEventsSync();

			// Read preset 2.
			_mockPort.ReadExpectationsFromString(SCAN_PRESET_2);
			_vm.SelectedEditablePreset = _vm.EditablePresets.First(p => p.PresetId == "2");
			_vm.ReadParametersCommand.Execute(null);

			// Change a value to one that doesn't match live and write it back.
			var data = method.DeviceParameters;
			data["finv1"] = new Measurement(3.0, data["finv1"].Unit);
			_mockPort.ReadExpectationsFromString(UPDATE_PRESET_2);
			_mockPort.ReadExpectationsFromString(SCAN_PRESET_2_AFTER_CHANGE);
			_vm.WriteParametersCommand.Execute(null);

			// Check that the label now indicates that the live params are not saved in a preset.
			Assert.IsNull(_vm.ActualActivePresetName);

			// Change back to the original value and check that the re-scan is done.
			data = method.DeviceParameters;
			data["finv1"] = new Measurement(2.0, data["finv1"].Unit);
			_mockPort.ReadExpectationsFromString(UPDATE_PRESET_2_AGAIN);
			_mockPort.ReadExpectationsFromString(SCAN_PRESET_2);
			_vm.WriteParametersCommand.Execute(null);

			Assert.AreEqual("Preset 2", _vm.ActualActivePresetName);
		}


		[Test]
		[Description("Test that the corrrect preset is identified when a different axis is selected.")]
		public void TestActivePresetIdentifiedAfterAxisChange()
		{
			_mockPort.ReadExpectationsFromFile(Path.Combine(TestContext.CurrentContext.TestDirectory,
				"lda_scan_for_preset_matching.txt"));
			var method = new TestMethodVM();
			_vm.SelectedTunerType = method;
			_vm.Conversation = _portFacade.GetConversation(2, 1);
			_vm.OnShown();

			// Wait for device query thread to end.
			DispatcherUtil.DoEventsSync();

			// Axis 1 live values do not match any preset.
			Assert.IsNull(_vm.ActualActivePresetName);

			// Change the live values to match axis 1 preset 2 and write.
			_mockPort.ReadExpectationsFromString(
			  @"/02 1 servo live get type
				@02 1 OK IDLE -- ffpid
				/02 1 servo live get gain
				@02 1 OK IDLE -- 0.000000000");
			_vm.ReadParametersCommand.Execute(null);

			// Change a value to match preset 2.
			var data = method.DeviceParameters;
			data["gain"] = new Measurement(2.0, data["gain"].Unit);
			_mockPort.ReadExpectationsFromString(
			  @"/02 1 servo staging get type
				@02 1 OK IDLE -- ffpid
				/02 1 servo staging set gain 2.000000000
				@02 1 OK IDLE -- 0
				/02 1 servo live load staging
				@02 1 OK IDLE -- 0
				/02 1 servo live get type
				@02 1 OK IDLE -- ffpid
				/02 1 servo live get gain
				@02 1 OK IDLE -- 2.000000000");
			_vm.WriteParametersCommand.Execute(null);

			// Check that the label now indicates that the live params patch preset 2.
			Assert.AreEqual("Preset 2", _vm.ActualActivePresetName);

			// Now select the other axis.
			_mockPort.ReadExpectationsFromFile(Path.Combine(TestContext.CurrentContext.TestDirectory,
				"ldm_scan_for_preset_matching.txt"));
			_vm.Conversation = _portFacade.GetConversation(2, 2);

			// The live values on axis 2 do not match any presets. 
			Assert.IsNull(_vm.ActualActivePresetName);

			// Now change to a value that matches a preset on axis 2.
			data["gain"] = new Measurement(3.0, data["gain"].Unit);
			_mockPort.ReadExpectationsFromString(
			  @"/02 2 servo staging get type
				@02 2 OK IDLE -- ffpid
				/02 2 servo staging set gain 3.000000000
				@02 2 OK IDLE -- 0
				/02 2 servo live load staging
				@02 2 OK IDLE -- 0
				/02 2 servo live get type
				@02 2 OK IDLE -- ffpid
				/02 2 servo live get gain
				@02 2 OK IDLE -- 3.000000000");
			_vm.WriteParametersCommand.Execute(null);

			// Display should now indicate that preset 1 matches the live values.
			Assert.AreEqual("Preset 1", _vm.ActualActivePresetName);
		}


		[Test]
		[Description("Test that all built-in tuning methods are present in the UI.")]
		public void TestAvailableTunerTypes()
		{
			_mockPort.ReadExpectationsFromFile(Path.Combine(TestContext.CurrentContext.TestDirectory,
															"dmq_scan_by_simple_tab.txt"));
			_vm.Conversation = _conversation;
			_vm.OnShown();

			// Wait for device query thread to end.
			DispatcherUtil.DoEventsSync();

			Assert.Less(0, _vm.TunerTypes.Count);
			var tunerTypes = _vm.TunerTypes.Select(tt => tt.GetType());
			Assert.IsTrue(tunerTypes.Contains(typeof(ImportExportMethodVM)));
			Assert.IsTrue(tunerTypes.Contains(typeof(SimpleCalculatorMethodVM)));
			Assert.IsTrue(tunerTypes.Contains(typeof(PidMethodVM)));
			Assert.IsTrue(tunerTypes.Contains(typeof(AdvancedMethodVM)));
		}


		[Test]
		[Description("Test that the label on the driver disable button changes when a relevant command is sent externally.")]
		public void TestDriverStateChangeCaught()
		{
			_mockPort.ReadExpectationsFromFile(Path.Combine(TestContext.CurrentContext.TestDirectory,
															"dmq_scan_by_simple_tab.txt"));
			_vm.Conversation = _conversation;
			_vm.OnShown();

			// Wait for device query thread to end.
			DispatcherUtil.DoEventsSync();

			// Sanity check
			Assert.IsTrue(_vm.PanicButtonLabel.ToLower().Contains("disable"));

			// Change driver state without using the plugin.
			_mockPort.ReadExpectationsFromString("/01 0 driver disable");
			_mockPort.ReadExpectationsFromString("@01 0 OK IDLE -- 0");
			_conversation.Request("driver disable");
			DispatcherUtil.DoEventsSync();

			// Check that the label changed.
			Assert.IsTrue(_vm.PanicButtonLabel.ToLower().Contains("enable"));

			// Change it back.
			_mockPort.ReadExpectationsFromString("/01 0 driver enable");
			_mockPort.ReadExpectationsFromString("@01 0 OK IDLE -- 0");
			_conversation.Request("driver enable");
			DispatcherUtil.DoEventsSync();

			// Check that the label changed.
			Assert.IsTrue(_vm.PanicButtonLabel.ToLower().Contains("disable"));
		}


		[Test]
		[Description("Test that UI controls have expected values after normal device scan.")]
		public void TestInitialQuery()
		{
			_mockPort.ReadExpectationsFromFile(Path.Combine(TestContext.CurrentContext.TestDirectory,
															"dmq_scan_by_simple_tab.txt"));
			_vm.Conversation = _conversation;
			_vm.OnShown();

			// Wait for device query thread to end.
			DispatcherUtil.DoEventsSync();

			// Check resulting data.
			Assert.IsTrue(_vm.IsPortOpen);
			Assert.IsTrue(_vm.IsDeviceTunable);
			Assert.IsTrue(string.IsNullOrEmpty(_vm.WarningMessage));

			Assert.IsTrue(_vm.PanicButtonLabel.ToLower().Contains("disable"));
			Assert.AreEqual(3, _vm.EditablePresets.Count());
			Assert.AreEqual(2, _vm.ActivatablePresets.Count());
			Assert.AreEqual(2, _vm.DefaultablePresets.Count());
			Assert.AreEqual(_vm.SelectedEditablePreset, _vm.EditablePresets.First());
			Assert.AreEqual("1", _vm.SelectedDefaultablePreset.PresetId);
			Assert.AreEqual("1", _vm.ActualDefaultPreset.PresetId);
			Assert.AreEqual("Preset 2", _vm.ActualActivePresetName);
			Assert.AreEqual("2", _vm.SelectedActivatablePreset.PresetId);
		}


		[Test]
		[Description("Test that UI controls have expected values after peripheral scan.")]
		public void TestInitialQueryPeripheral()
		{
			_mockPort.ReadExpectationsFromFile(Path.Combine(TestContext.CurrentContext.TestDirectory,
				"lda_peripheral_scan_by_simple_tab.txt"));
			_vm.Conversation = _portFacade.GetConversation(2, 1);
			_vm.OnShown();

			// Wait for device query thread to end.
			DispatcherUtil.DoEventsSync();

			// Check resulting data.
			Assert.IsTrue(_vm.IsPortOpen);
			Assert.IsTrue(_vm.IsDeviceTunable);
			Assert.IsTrue(string.IsNullOrEmpty(_vm.WarningMessage));

			Assert.IsTrue(_vm.PanicButtonLabel.ToLower().Contains("disable"));
			Assert.AreEqual(10, _vm.EditablePresets.Count());
			Assert.AreEqual(9, _vm.ActivatablePresets.Count());
			Assert.AreEqual(9, _vm.DefaultablePresets.Count());
			Assert.AreEqual(_vm.SelectedEditablePreset, _vm.EditablePresets.First());
			Assert.AreEqual("1", _vm.SelectedDefaultablePreset.PresetId);
			Assert.AreEqual("1", _vm.ActualDefaultPreset.PresetId);
			Assert.AreEqual("Preset 4", _vm.ActualActivePresetName);
			Assert.AreEqual("4", _vm.SelectedActivatablePreset.PresetId);
		}


		[Test]
		[Description("Test that UI controls are disabled if the device has no tuning data.")]
		public void DisabledIfNoTuningData()
		{
			_mockPort.ReadExpectationsFromString("/01 0 get driver.enabled\r\n@01 0 OK IDLE -- 1");
			_conversation.Device.DeviceType.TuningData = null;
			_vm.Conversation = _conversation;
			_vm.OnShown();

			DispatcherUtil.DoEventsSync();

			Assert.IsFalse(_vm.IsDeviceTunable);
		}


		[Test]
		[Description("Test that property changed notifications are sent for non-computed observable properties.")]
		public void TestObservableProperties()
		{
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);

			_listener.TestObservableProperty(_vm, () => _vm.ActualActivePresetName);

			_listener.TestObservableProperty(_vm,
											 () => _vm.ActualDefaultPreset,
											 p => new PresetInfo { PresetId = (p?.PresetId ?? "") + " foo" });

			_listener.TestObservableProperty(_vm,
											 () => _vm.SelectedActivatablePreset,
											 p => new PresetInfo { PresetId = (p?.PresetId ?? "") + " foo" });

			_listener.TestObservableProperty(_vm,
											 () => _vm.SelectedDefaultablePreset,
											 p => new PresetInfo { PresetId = (p?.PresetId ?? "") + " foo" });

			_listener.TestObservableProperty(_vm,
											 () => _vm.SelectedEditablePreset,
											 p => new PresetInfo { PresetId = (p?.PresetId ?? "") + " foo" });

			_listener.TestObservableProperty(_vm, () => _vm.CanRead);
			_listener.TestObservableProperty(_vm, () => _vm.CanWrite);
			_listener.TestObservableProperty(_vm, () => _vm.PanicButtonLabel);

			_listener.TestObservableProperty(_vm, () => _vm.SelectedTunerType, _ => new TestMethodVM());

			_listener.TestObservableProperty(_vm,
											 () => _vm.TunerTypes,
											 coll => new ObservableCollection<CalculatorMethodBase>
											 {
												 new TestMethodVM()
											 });
		}


		[Test]
		[Description("Test that writing inserts defaults when a parameter set is missing some device parameters.")]
		public void TestWriteZeroesExtraParams()
		{
			_mockPort.ReadExpectationsFromFile(Path.Combine(TestContext.CurrentContext.TestDirectory, "ldq_scan.txt"));
			var method = new TestMethodVM();

			_vm.SelectedTunerType = method;
			_vm.Conversation = _conversation;
			_vm.OnShown();

			// Wait for device query thread to end.
			DispatcherUtil.DoEventsSync();

			// Create a dummy data set that is missing some expected parameters.
			var names = TunerTestHelper.FFPID_SETTINGS_V3.Where(n => "gain" != n);
			var testData = TunerTestHelper.CreateEmptySet(TuningModeNames.FFPID, PidTunerV3.PidUnitTable, names);
			foreach (var name in names)
			{
				testData[name] = new Measurement(10.0, null);
			}

			// Write the preset and test that the missing name gets zero written.
			_vm.SelectedEditablePreset = _vm.EditablePresets.First(p => p.PresetId == "2");
			_mockPort.ReadExpectationsFromString(SCAN_DEFAULTS);
			_mockPort.ReadExpectationsFromString(UPDATE_WITH_DEFAULTED_GAIN);
			_mockPort.ReadExpectationsFromString(SCAN_PRESET_DEFAULTED_GAIN);
			method.DeviceParameters = testData;
			_vm.WriteParametersCommand.Execute(null);
		}


		[DisplayName("Unit Test")]
		[SortOrder(-1.0)]
		private class TestMethodVM : CalculatorMethodBase
		{
			public TestMethodVM()
			{
				CanRead = CanWrite = true;
			}


			public override void SetTargetDevice(Conversation aConversation, DeviceProperties aServoProperties)
			{
				Conversation = aConversation;
				DeviceProperties = aServoProperties;
			}


			public override ITunerParameterSet DeviceParameters { get; set; }

			internal Conversation Conversation { get; private set; }

			internal DeviceProperties DeviceProperties { get; private set; }
		}


		private const string SCAN_DEFAULTS = @"
			/01 0 servo default get type
			@01 0 OK IDLE -- ffpid
			/01 0 servo default get finv1
			@01 0 OK IDLE -- 0.0
			/01 0 servo default get finv2
			@01 0 OK IDLE -- 6.1
			/01 0 servo default get finvr
			@01 0 OK IDLE -- 100000
			/01 0 servo default get fcla
			@01 0 OK IDLE -- 10.5
			/01 0 servo default get fcla.hs
			@01 0 OK IDLE -- 9.4
			/01 0 servo default get fclg
			@01 0 OK IDLE -- 0.5
			/01 0 servo default get fclg.hs
			@01 0 OK IDLE -- 0.3
			/01 0 servo default get ki
			@01 0 OK IDLE -- 0.2
			/01 0 servo default get ki.hs
			@01 0 OK IDLE -- 0.1
			/01 0 servo default get kp
			@01 0 OK IDLE -- 44.3
			/01 0 servo default get kp.hs
			@01 0 OK IDLE -- 28.6
			/01 0 servo default get kd
			@01 0 OK IDLE -- 8018.8
			/01 0 servo default get kd.hs
			@01 0 OK IDLE -- 7955.5
			/01 0 servo default get fc1
			@01 0 OK IDLE -- 591.6
			/01 0 servo default get fc1.hs
			@01 0 OK IDLE -- 487.8
			/01 0 servo default get fc2
			@01 0 OK IDLE -- 1183.2
			/01 0 servo default get fc2.hs
			@01 0 OK IDLE -- 975.7
			/01 0 servo default get gain
			@01 0 OK IDLE -- 0.1";

		private const string SCAN_PRESET_2 = @"
			/01 0 servo 2 get type
			@01 0 OK IDLE -- ffpid
			/01 0 servo 2 get finv1
			@01 0 OK IDLE -- 2
			/01 0 servo 2 get finv2
			@01 0 OK IDLE -- 6103515648
			/01 0 servo 2 get fcla
			@01 0 OK IDLE -- 994587840
			/01 0 servo 2 get fcla.hs
			@01 0 OK IDLE -- 997435840
			/01 0 servo 2 get fclg
			@01 0 OK IDLE -- 161297104
			/01 0 servo 2 get fclg.hs
			@01 0 OK IDLE -- 161040368
			/01 0 servo 2 get ki
			@01 0 OK IDLE -- 38407812
			/01 0 servo 2 get ki.hs
			@01 0 OK IDLE -- 4322004
			/01 0 servo 2 get kp
			@01 0 OK IDLE -- 24156209152
			/01 0 servo 2 get kp.hs
			@01 0 OK IDLE -- 5577973248
			/01 0 servo 2 get kd
			@01 0 OK IDLE -- 10558143201280
			/01 0 servo 2 get kd.hs
			@01 0 OK IDLE -- 5091669049344
			/01 0 servo 2 get fc1
			@01 0 OK IDLE -- 207879568
			/01 0 servo 2 get fc1.hs
			@01 0 OK IDLE -- 675231936
			/01 0 servo 2 get gain
			@01 0 OK IDLE -- 99337744";

		private const string UPDATE_PRESET_2 = @"
			/01 0 servo staging get type
			@01 0 OK IDLE -- ffpid
			/01 0 servo staging set finv1 3.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set finv2 6103515648.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set fcla 994587840.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set fcla.hs 997435840.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set fclg 161297104.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set fclg.hs 161040368.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set ki 38407812.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set ki.hs 4322004.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set kp 24156209152.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set kp.hs 5577973248.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set kd 10558143201280.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set kd.hs 5091669049344.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set fc1 207879568.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set fc1.hs 675231936.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set gain 99337744.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo 2 load staging
			@01 0 OK IDLE -- 0";

		private const string UPDATE_PRESET_2_AGAIN = @"
			/01 0 servo staging get type
			@01 0 OK IDLE -- ffpid
			/01 0 servo staging set finv1 2.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set finv2 6103515648.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set fcla 994587840.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set fcla.hs 997435840.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set fclg 161297104.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set fclg.hs 161040368.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set ki 38407812.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set ki.hs 4322004.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set kp 24156209152.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set kp.hs 5577973248.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set kd 10558143201280.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set kd.hs 5091669049344.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set fc1 207879568.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set fc1.hs 675231936.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set gain 99337744.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo 2 load staging
			@01 0 OK IDLE -- 0";

		private const string SCAN_PRESET_2_AFTER_CHANGE = @"
			/01 0 servo 2 get type
			@01 0 OK IDLE -- ffpid
			/01 0 servo 2 get finv1
			@01 0 OK IDLE -- 3
			/01 0 servo 2 get finv2
			@01 0 OK IDLE -- 6103515648
			/01 0 servo 2 get fcla
			@01 0 OK IDLE -- 994587840
			/01 0 servo 2 get fcla.hs
			@01 0 OK IDLE -- 997435840
			/01 0 servo 2 get fclg
			@01 0 OK IDLE -- 161297104
			/01 0 servo 2 get fclg.hs
			@01 0 OK IDLE -- 161040368
			/01 0 servo 2 get ki
			@01 0 OK IDLE -- 38407812
			/01 0 servo 2 get ki.hs
			@01 0 OK IDLE -- 4322004
			/01 0 servo 2 get kp
			@01 0 OK IDLE -- 24156209152
			/01 0 servo 2 get kp.hs
			@01 0 OK IDLE -- 5577973248
			/01 0 servo 2 get kd
			@01 0 OK IDLE -- 10558143201280
			/01 0 servo 2 get kd.hs
			@01 0 OK IDLE -- 5091669049344
			/01 0 servo 2 get fc1
			@01 0 OK IDLE -- 207879568
			/01 0 servo 2 get fc1.hs
			@01 0 OK IDLE -- 675231936
			/01 0 servo 2 get gain
			@01 0 OK IDLE -- 99337744";

		private const string UPDATE_WITH_DEFAULTED_GAIN = @"
			/01 0 servo staging get type
			@01 0 OK IDLE -- ffpid
			/01 0 servo staging set finv1 10.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set finv2 10.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set fcla 10.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set fcla.hs 10.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set fclg 10.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set fclg.hs 10.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set ki 10.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set ki.hs 10.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set kp 10.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set kp.hs 10.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set kd 10.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set kd.hs 10.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set fc1 10.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set fc1.hs 10.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set fc2 10.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set fc2.hs 10.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set finvr 100000.000000000
			@01 0 OK IDLE -- 0
			/01 0 servo staging set gain 0.100000000
			@01 0 OK IDLE -- 0
			/01 0 servo 2 load staging
			@01 0 OK IDLE -- 0";

		private const string SCAN_PRESET_DEFAULTED_GAIN = @"
			/01 0 servo 2 get type
			@01 0 OK IDLE -- ffpid
			/01 0 servo 2 get finv1
			@01 0 OK IDLE -- 10
			/01 0 servo 2 get finv2
			@01 0 OK IDLE -- 10
			/01 0 servo 2 get finvr
			@01 0 OK IDLE -- 100000
			/01 0 servo 2 get fcla
			@01 0 OK IDLE -- 10
			/01 0 servo 2 get fcla.hs
			@01 0 OK IDLE -- 10
			/01 0 servo 2 get fclg
			@01 0 OK IDLE -- 10
			/01 0 servo 2 get fclg.hs
			@01 0 OK IDLE -- 10
			/01 0 servo 2 get ki
			@01 0 OK IDLE -- 10
			/01 0 servo 2 get ki.hs
			@01 0 OK IDLE -- 10
			/01 0 servo 2 get kp
			@01 0 OK IDLE -- 10
			/01 0 servo 2 get kp.hs
			@01 0 OK IDLE -- 10
			/01 0 servo 2 get kd
			@01 0 OK IDLE -- 10
			/01 0 servo 2 get kd.hs
			@01 0 OK IDLE -- 10
			/01 0 servo 2 get fc1
			@01 0 OK IDLE -- 10
			/01 0 servo 2 get fc1.hs
			@01 0 OK IDLE -- 10
			/01 0 servo 2 get fc2
			@01 0 OK IDLE -- 10
			/01 0 servo 2 get fc2.hs
			@01 0 OK IDLE -- 10
			/01 0 servo 2 get gain
			@01 0 OK IDLE -- 0.1";


		private void SetupPort()
		{
			_mockPort = new MockPort { IsAsciiMode = true };

			var defaultDeviceType = new DeviceType { Commands = new List<CommandInfo>() };

			_portFacade = new ZaberPortFacade
			{
				DefaultDeviceType = defaultDeviceType,
				Port = _mockPort,
				QueryTimeout = 100
			};

			var dmqCommands = new List<CommandInfo>
			{
				new CommandInfo
				{
					TextCommand = "move abs {0}",
					RequestUnit = UnitOfMeasure.Meter,
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 5000000.0m
				},
				new CommandInfo
				{
					TextCommand = "move vel {0}",
					RequestUnit = UnitOfMeasure.MetersPerSecond,
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 8192000.0m
				},
				new SettingInfo
				{
					TextCommand = "accel",
					RequestUnit = UnitOfMeasure.MetersPerSecondSquared,
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 819.2m
				},
				new SettingInfo
				{
					TextCommand = "driver.current.servo",
					RequestUnit = new UnitOfMeasure(_amps),
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 50.0m
				},
				new CommandInfo
				{
					TextCommand = "force abs {0}",
					RequestUnit = ConversionTable.Default.FindUnitByAbbreviation(UnitOfMeasure.NewtonsSymbol),
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 39.6825396825397m
				}
			};

			dmqCommands.AddRange(
				AsciiCommandBuilder.ReadCommandDefinitionsFromJsonFile(AsciiCommandBuilder.SERVO_COMMAND_FILE));

			var dmq = new DeviceType
			{
				DeviceId = 50419,
				Name = "X-DMQ12P-DE52 Stage",
				MotionType = MotionType.Linear,
				FirmwareVersion = new FirmwareVersion(7, 1),
				Commands = dmqCommands,
				TuningData = new TuningData
				{
					ForqueConstant = 15.1,
					Inertia = new Measurement(100.0, UnitConverter.Default.FindUnitBySymbol("g")),
					ControllerVersions = new Dictionary<string, int> { { "ffpid", 3 } }
				},
				DeviceTypeUnitConversionInfo =
				{
					{
						Dimension.Force, new ParameterUnitConversion
						{
							Function = ScalingFunction.Linear,
							ReferenceUnit = UnitConverter.Default.FindUnitBySymbol("N"),
							Scale = 1000000m
						}
					}
				}
			};

			var ldaCommands = dmqCommands.Select(cmd =>
			{
				var newCmd = cmd.Clone();
				newCmd.IsAxisCommand = true;
				return newCmd;
			}).ToList();

			var lda = new DeviceType
			{
				PeripheralId = 49006,
				Name = "LDA150A",
				MotionType = MotionType.Linear,
				FirmwareVersion = new FirmwareVersion(7, 10),
				Commands = ldaCommands,
				TuningData = new TuningData
				{
					ForqueConstant = 0.17,
					Inertia = new Measurement(430.0, UnitConverter.Default.FindUnitBySymbol("g")),
					ControllerVersions = new Dictionary<string, int> { { "ffpid", 4 } }
				},
				DeviceTypeUnitConversionInfo =
				{
					{
						Dimension.Force, new ParameterUnitConversion
						{
							Function = ScalingFunction.Linear,
							ReferenceUnit = UnitConverter.Default.FindUnitBySymbol("N"),
							Scale = 1000000m
						}
					}
				}
			};

			var ldm = new DeviceType
			{
				PeripheralId = 70244,
				Name = "LDM060C",
				MotionType = MotionType.Linear,
				FirmwareVersion = new FirmwareVersion(7, 10),
				Commands = ldaCommands,
				TuningData = new TuningData
				{
					ForqueConstant = 0.387,
					Inertia = new Measurement(1740.0, UnitConverter.Default.FindUnitBySymbol("g")),
					ControllerVersions = new Dictionary<string, int> { { "ffpid", 4 } }
				},
				DeviceTypeUnitConversionInfo =
				{
					{
						Dimension.Force, new ParameterUnitConversion
						{
							Function = ScalingFunction.Linear,
							ReferenceUnit = UnitConverter.Default.FindUnitBySymbol("N"),
							Scale = 1000000m
						}
					}
				}
			};

			var mcc = new DeviceType
			{
				DeviceId = 30321,
				Name = "X-MCC2",
				MotionType = MotionType.None,
				FirmwareVersion = new FirmwareVersion(7, 10),
				PeripheralMap = new Dictionary<int, DeviceType> { { 49006, lda }, { 70244, ldm } }
			};

			_portFacade.AddDeviceType(dmq);
			_portFacade.AddDeviceType(mcc);

			ZaberPortFacadeTest.SetAsciiExpectations(_portFacade, _mockPort, dmq, mcc);
		}


		private MockPort _mockPort;
		private Conversation _conversation;
		private ZaberPortFacade _portFacade;
		private ServoTunerPluginVM _vm;
		private Unit _amps;
		private ObservableObjectTester _listener;
	}
}

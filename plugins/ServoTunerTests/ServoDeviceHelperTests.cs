﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Zaber;
using Zaber.Tuning;
using Zaber.Tuning.Tests;
using Zaber.Units;
using ZaberConsole.Plugins.ServoTuner;
using ZaberTest;
using ZaberTest.Testing;
using Measurement = Zaber.Units.Measurement;

namespace ServoTunerTests
{
	public abstract class ServoDeviceHelperTests
	{
		[SetUp]
		public void Setup()
		{
			_port = new MockPort();
			_device = CreateDevice(1, _port);
			_device.DeviceType.FirmwareVersion = new FirmwareVersion(6, 14);
			_conversation = new Conversation(_device);
		}


		[Test]
		public void ReadDevicePresetNames()
		{
			_port.ReadExpectationsFromString(PRESETS_LOG);
			var result = ServoDeviceHelper.ReadDevicePresetNames(_conversation)?.ToList();
			Assert.IsNotNull(result);
			Assert.AreEqual(11, result.Count);
			Assert.AreEqual("live", result[0]);
			for (var i = 0; i < 10; ++i)
			{
				Assert.AreEqual(i.ToString(), result[i + 1]);
			}
		}


		[Test]
		public void ReadDeviceTuningModes()
		{
			_port.ReadExpectationsFromString(MODES_LOG);
			var result = ServoDeviceHelper.ReadDeviceTuningModes(_conversation)?.ToList();
			Assert.IsNotNull(result);
			Assert.AreEqual(1, result.Count);
			Assert.AreEqual("ffpid", result.First());
		}


		[Test]
		public void ReadReadTuningModeParameterNames()
		{
			_port.ReadExpectationsFromString(PARAMS_LOG);
			var result = ServoDeviceHelper.ReadTuningModeParameterNames(_conversation, TuningModeNames.FFPID)?.ToList();
			Assert.IsNotNull(result);
			Assert.AreEqual(TunerTestHelper.FFPID_SETTINGS_V3.Length, result.Count);
			foreach (var name in result)
			{
				Assert.IsTrue(TunerTestHelper.FFPID_SETTINGS_V3.Contains(name));
			}
		}


		[Test]
		public void TestCreateEmptyParams()
		{
			var p = ServoDeviceHelper.CreateEmptyParams("Foo", "Bar", "Baz", "Quux");
			Assert.IsNotNull(p);
			Assert.AreEqual("Foo", p.ModeName);
			Assert.IsNull(p.UnitConverter);
			Assert.IsFalse(p.ParameterNames.Contains("Foo"));
			Assert.IsNotNull(p["Bar"]);
			Assert.IsNotNull(p["Baz"]);
			Assert.IsNotNull(p["Quux"]);
			Assert.AreEqual(0.0, p["Bar"].Value);
			Assert.AreEqual(0.0, p["Baz"].Value);
			Assert.AreEqual(0.0, p["Quux"].Value);
			Assert.AreEqual(Dimension.None, p["Bar"].Unit.Dimension);
			Assert.AreEqual(Dimension.None, p["Baz"].Unit.Dimension);
			Assert.AreEqual(Dimension.None, p["Quux"].Unit.Dimension);
		}


		[Test]
		public void TestReadDeviceParametersFromLiveSet()
		{
			var paramList = new Dictionary<string, string[]> { { "mode1", new[] { "a", "b", "c" } } };

			_port.ReadExpectationsFromString(READ_LIVE_PARAMS_LOG);
			var result = ServoDeviceHelper.ReadDeviceParameters(_conversation, "live", paramList);
			Assert.AreEqual("mode1", result.ModeName);
			Assert.AreEqual(3, result.ParameterNames.Count());
			Assert.IsTrue(result.ParameterNames.Contains("a"));
			Assert.IsTrue(result.ParameterNames.Contains("b"));
			Assert.IsTrue(result.ParameterNames.Contains("c"));
			Assert.AreEqual(1.1, result["a"].Value);
			Assert.AreEqual(2.2, result["b"].Value);
			Assert.AreEqual(3.3, result["c"].Value);
			Assert.AreEqual(Dimension.None, result["a"].Unit.Dimension);
			Assert.AreEqual(Dimension.None, result["b"].Unit.Dimension);
			Assert.AreEqual(Dimension.None, result["c"].Unit.Dimension);
		}


		[Test]
		public void TestReadDeviceParametersFromPreset()
		{
			var paramList = new Dictionary<string, string[]> { { "mode1", new[] { "a", "b", "c" } } };

			_port.ReadExpectationsFromString(READ_PRESET_PARAMS_LOG);
			var result = ServoDeviceHelper.ReadDeviceParameters(_conversation, "3", paramList);
			Assert.AreEqual("mode1", result.ModeName);
			Assert.AreEqual(3, result.ParameterNames.Count());
			Assert.IsTrue(result.ParameterNames.Contains("a"));
			Assert.IsTrue(result.ParameterNames.Contains("b"));
			Assert.IsTrue(result.ParameterNames.Contains("c"));
			Assert.AreEqual(1.1, result["a"].Value);
			Assert.AreEqual(2.2, result["b"].Value);
			Assert.AreEqual(3.3, result["c"].Value);
			Assert.AreEqual(Dimension.None, result["a"].Unit.Dimension);
			Assert.AreEqual(Dimension.None, result["b"].Unit.Dimension);
			Assert.AreEqual(Dimension.None, result["c"].Unit.Dimension);
		}


		[Test]
		public void TestReadDeviceParametersWithoutPrescan()
		{
			_port.ReadExpectationsFromString(READ_UNKNOWN_PARAMS_LOG);
			var result = ServoDeviceHelper.ReadDeviceParameters(_conversation, "live");
			Assert.AreEqual("mode2", result.ModeName);
			Assert.AreEqual(3, result.ParameterNames.Count());
			Assert.IsTrue(result.ParameterNames.Contains("x"));
			Assert.IsTrue(result.ParameterNames.Contains("y"));
			Assert.IsTrue(result.ParameterNames.Contains("z"));
			Assert.AreEqual(1.1, result["x"].Value);
			Assert.AreEqual(2.2, result["y"].Value);
			Assert.AreEqual(3.3, result["z"].Value);
			Assert.AreEqual(Dimension.None, result["x"].Unit.Dimension);
			Assert.AreEqual(Dimension.None, result["y"].Unit.Dimension);
			Assert.AreEqual(Dimension.None, result["z"].Unit.Dimension);
		}


		[Test]
		public void TestReadDeviceTuningModesAndParameters()
		{
			_port.ReadExpectationsFromString(MODES_AND_PARAMS_LOG);
			var result = ServoDeviceHelper.ReadDeviceTuningModesAndParameters(_conversation);
			Assert.IsNotNull(result);
			Assert.AreEqual(2, result.Count);
			Assert.IsTrue(result.ContainsKey("mode1"));
			Assert.IsTrue(result.ContainsKey("mode2"));
			Assert.AreEqual(3, result["mode1"].Count());
			Assert.AreEqual(3, result["mode2"].Count());
			Assert.IsTrue(result["mode1"].Contains("a"));
			Assert.IsTrue(result["mode1"].Contains("b"));
			Assert.IsTrue(result["mode1"].Contains("c"));
			Assert.IsTrue(result["mode2"].Contains("x"));
			Assert.IsTrue(result["mode2"].Contains("y"));
			Assert.IsTrue(result["mode2"].Contains("z"));
		}


		[Test]
		public void TestWriteDeviceParameters()
		{
			var p = ServoDeviceHelper.CreateEmptyParams(TuningModeNames.FFPID, TunerTestHelper.FFPID_SETTINGS_V3);
			var i = 0.0;
			var sb = new StringBuilder();
			sb.AppendLine(WRITE_LIVE_PARAMS_LOG);
			foreach (var name in p.ParameterNames)
			{
				p[name] = new Measurement(i, p[name].Unit);
				var number = i.ToString("N9", CultureInfo.InvariantCulture);
				sb.AppendLine($"/01 0 servo live set {name} {number}");
				sb.AppendLine("@01 0 OK IDLE -- 0");
				i += 1.0;
			}

			// NOTE this test is sensitive parameter name iteration order being consistent.
			_port.ReadExpectationsFromString(sb.ToString());
			ServoDeviceHelper.WriteDeviceParameters(_conversation, p, "live");
		}


		[Test]
		public void TestWriteDeviceParametersToPreset()
		{
			var p = ServoDeviceHelper.CreateEmptyParams(TuningModeNames.FFPID, TunerTestHelper.FFPID_SETTINGS_V3);
			var i = 0.0;
			var sb = new StringBuilder();
			sb.AppendLine(WRITE_PRESET_PARAMS_LOG);
			foreach (var name in p.ParameterNames)
			{
				p[name] = new Measurement(i, p[name].Unit);
				var number = i.ToString("N9", CultureInfo.InvariantCulture);
				sb.AppendLine($"/01 0 servo 2 set {name} {number}");
				sb.AppendLine("@01 0 OK IDLE -- 0");
				i += 1.0;
			}

			// NOTE this test is sensitive parameter name iteration order being consistent.
			_port.ReadExpectationsFromString(sb.ToString());
			ServoDeviceHelper.WriteDeviceParameters(_conversation, p, "2");
		}


		private const string MODES_LOG = @"
			/01 0 servo print types
			@01 0 OK IDLE -- 0
			/01 0 
			#01 0 type ffpid
			@01 0 OK IDLE -- 0";

		private const string PRESETS_LOG = @"
			/01 0 servo print paramsets
			@01 0 OK IDLE -- 0
			/01 0 
			#01 0 paramset live
			#01 0 paramset 0
			#01 0 paramset 1
			#01 0 paramset 2
			#01 0 paramset 3
			#01 0 paramset 4
			#01 0 paramset 5
			#01 0 paramset 6
			#01 0 paramset 7
			#01 0 paramset 8
			#01 0 paramset 9
			@01 0 OK IDLE -- 0";

		private const string PARAMS_LOG = @"
			/01 0 servo print params ffpid
			@01 0 OK IDLE -- 0
			/01 0 
			#01 0 ffpid kp
			#01 0 ffpid ki
			#01 0 ffpid kd
			#01 0 ffpid fc1
			#01 0 ffpid fc2
			#01 0 ffpid gain
			#01 0 ffpid kp.hs
			#01 0 ffpid ki.hs
			#01 0 ffpid kd.hs
			#01 0 ffpid fc1.hs
			#01 0 ffpid fc2.hs
			#01 0 ffpid fcla
			#01 0 ffpid fclg
			#01 0 ffpid fcla.hs
			#01 0 ffpid fclg.hs
			#01 0 ffpid finv1
			#01 0 ffpid finv2
			@01 0 OK IDLE -- 0";

		private const string MODES_AND_PARAMS_LOG = @"
			/01 0 servo print types
			@01 0 OK IDLE -- 0
			/01 0 
			#01 0 type mode1
			#01 0 type mode2
			@01 0 OK IDLE -- 0
			/01 0 servo print params mode1
			@01 0 OK IDLE -- 0
			/01 0 
			#01 0 mode1 a
			#01 0 mode1 b
			#01 0 mode1 c
			@01 0 OK IDLE -- 0
			/01 0 servo print params mode2
			@01 0 OK IDLE -- 0
			/01 0 
			#01 0 mode2 x
			#01 0 mode2 y
			#01 0 mode2 z
			@01 0 OK IDLE -- 0";

		private const string READ_PRESET_PARAMS_LOG = @"
			/01 0 servo 3 get type
			@01 0 OK IDLE -- mode1
			/01 0 servo 3 get a
			@01 0 OK IDLE -- 1.1
			/01 0 servo 3 get b
			@01 0 OK IDLE -- 2.2
			/01 0 servo 3 get c
			@01 0 OK IDLE -- 3.3";

		private const string READ_LIVE_PARAMS_LOG = @"
			/01 0 servo live get type
			@01 0 OK IDLE -- mode1
			/01 0 servo live get a
			@01 0 OK IDLE -- 1.1
			/01 0 servo live get b
			@01 0 OK IDLE -- 2.2
			/01 0 servo live get c
			@01 0 OK IDLE -- 3.3";

		private const string READ_UNKNOWN_PARAMS_LOG = @"
			/01 0 servo live get type
			@01 0 OK IDLE -- mode2
			/01 0 servo print params mode2
			@01 0 OK IDLE -- 0
			/01 0 
			#01 0 mode2 x
			#01 0 mode2 y
			#01 0 mode2 z
			@01 0 OK IDLE -- 0
			/01 0 servo live get x
			@01 0 OK IDLE -- 1.1
			/01 0 servo live get y
			@01 0 OK IDLE -- 2.2
			/01 0 servo live get z
			@01 0 OK IDLE -- 3.3";

		private const string WRITE_PRESET_PARAMS_LOG = @"
			/01 0 servo 2 get type
			@01 0 OK IDLE -- iira
			/01 0 servo 2 set type ffpid
			@01 0 OK IDLE -- 0";

		private const string WRITE_LIVE_PARAMS_LOG = @"
			/01 0 servo live get type
			@01 0 OK IDLE -- iira
			/01 0 servo live set type ffpid
			@01 0 OK IDLE -- 0";


		private static ZaberDevice CreateDevice(byte deviceNumber, IZaberPort port)
		{
			var device = new ZaberDevice
			{
				Port = port,
				DeviceNumber = deviceNumber
			};

			var devType = DeviceTypeMother.CreateDeviceTypeWithCommands();
			var commands = devType.Commands.ToList();
			commands.AddRange(
				AsciiCommandBuilder.ReadCommandDefinitionsFromJsonFile(AsciiCommandBuilder.SERVO_COMMAND_FILE));
			devType.Commands = commands;

			device.DeviceType = devType;
			return device;
		}


		private MockPort _port;
		private ZaberDevice _device;
		private Conversation _conversation;
	}
}

﻿using System.Linq;
using System.Threading;
using Zaber;

namespace CycleDemo
{
	public class OneAtATimeController : Controller
	{
		public OneAtATimeController(ZaberPortFacade aPort)
			: base(aPort)
		{
		}


		public override void Run(decimal? aRequestedSpeed)
		{
			Reset(aRequestedSpeed);
			while (CycleState == CycleState.Running)
			{
				foreach (var device in Devices)
				{
					Move(device);
					WaitOne(device, Timeout.Infinite);

					if (CycleState != CycleState.Running)
					{
						break;
					}

					Move(device);
					WaitOne(device, Timeout.Infinite);

					if (CycleState != CycleState.Running)
					{
						break;
					}
				}

				if (!Devices.Any())
				{
					Cancel();
				}
			}
		}
	}
}

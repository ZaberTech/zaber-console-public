﻿namespace CycleDemo
{
	/// <summary>
	///     Represents a 2-tuple, or pair.
	///     The Tuple class is only available starting at .NET v4 framework.
	///     This class is implemented for projects targeting previous .NET versions.
	/// </summary>
	/// <typeparam name="T">The type of the tuple's first component.</typeparam>
	/// <typeparam name="U">The type of the tuple's second component.</typeparam>
	public class Tuple<T, U>
	{
		public Tuple(T item1, U item2)
		{
			Item1 = item1;
			Item2 = item2;
		}


		/// <summary>
		///     Returns a value that indicates whether the current Tuple
		///     <T, U>
		///         object is equal to a specified object.
		///         The obj parameter is considered to be equal to the current instance under the following conditions:
		///         - It is a Tuple
		///         <T, U>
		///             object.
		///             - Its two components are of the same types as the current instance.
		///             - Its two components have the same values as those of the current instance.
		/// </summary>
		/// <param name="aObject">The object to compare with this instance.</param>
		/// <returns>true if the current instance is equal to the specified object; otherwise, false.</returns>
		public override bool Equals(object aObject)
		{
			if ((aObject is null) || (GetType() != aObject.GetType()))
			{
				return false;
			}

			return Equals((Tuple<T, U>) aObject);
		}


		/// <summary>
		///     Returns a value that indicates whether the current Tuple<T, U> object is equal to another Tuple<T, U>.
		/// </summary>
		/// <param name="aOther">The tuple to compare with this instance.</param>
		/// <returns>true if the current instance is equal to the specified Tuple; otherwise, false.</returns>
		public bool Equals(Tuple<T, U> aOther) => Item1.Equals(aOther.Item1) && Item2.Equals(aOther.Item2);


		/// <summary>
		///     Returns the hash code for the current Tuple<T1, T2> object.
		/// </summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		public override int GetHashCode() => Item1.GetHashCode() ^ Item2.GetHashCode();


		public T Item1 { get; }

		public U Item2 { get; }
	}

	/// <summary>
	///     Creates a new 2-tuple, or pair.
	///     The Tuple class is only available starting at .NET v4 framework.
	///     This class is implemented for projects targeting previous .NET versions.
	/// </summary>
	public static class Tuple
	{
		public static Tuple<T, U> Create<T, U>(T aItem1, U aItem2) => new Tuple<T, U>(aItem1, aItem2);
	}
}

﻿using System.Linq;
using System.Threading;
using Zaber;

namespace CycleDemo
{
	public class UnsynchronizedController : Controller
	{
		public UnsynchronizedController(ZaberPortFacade aPort)
			: base(aPort)
		{
		}


		public override void Run(decimal? aRequestedSpeed)
		{
			Reset(aRequestedSpeed);
			while (CycleState == CycleState.Running)
			{
				if (Devices.Any())
				{
					var device = WaitAny(Timeout.Infinite);

					if (CycleState == CycleState.Running)
					{
						Move(device);
					}
				}
				else
				{
					Cancel();
				}
			}
		}
	}
}

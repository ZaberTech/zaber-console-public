﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;
using CycleDemo.Properties;
using Zaber;
using Zaber.PlugIns;
using Zaber.Telemetry;
using ZaberWpfToolbox;

namespace CycleDemo
{
	[PlugIn("Cycle", Description = "Continually cycles multiple axes back and forth.")]
	public partial class Cycle : UserControl
	{
		#region -- Initialization and Cleanup --

		public Cycle()
		{
			InitializeComponent();
		}


		/// <summary>
		///     Reference to the Plugin Manager. Used to detect user settings reset events.
		/// </summary>
		[PlugInProperty]
		public IPlugInManager PluginManager
		{
			get => _pluginManager;
			set
			{
				if (null != _pluginManager)
				{
					_pluginManager.SettingsResetEvent -= OnSettingsReset;
				}

				_pluginManager = value;

				if (null != _pluginManager)
				{
					_pluginManager.SettingsResetEvent += OnSettingsReset;
				}
			}
		}


		[PlugInProperty]
		public ZaberPortFacade PortFacade
		{
			get => _portFacade;
			set
			{
				if (_portFacade != null)
				{
					_portFacade.Opened -= PortFacade_Opened;
					_portFacade.Closed -= PortFacade_Closed;
				}

				_portFacade = value;
				if (_portFacade != null)
				{
					_portFacade.Opened += PortFacade_Opened;
					_portFacade.Closed += PortFacade_Closed;
				}
			}
		}


		[PlugInMethod(PlugInMethodAttribute.EventType.PluginActivated)]
		public void Cycle_Load()
		{
			EnsureControllersInitialized();

			synchColumn.DataSource = Enum.GetValues(typeof(SynchronizationStyle));
			synchColumn.ValueType = typeof(SynchronizationStyle);

			RestoreScenariosFromSettings();
		}


		[PlugInMethod(PlugInMethodAttribute.EventType.PluginDeactivated)]
		public void Cycle_FormClosing()
		{
			Cleanup();

			var settings = Settings.Default;
			if (settings.Scenarios is null)
			{
				settings.Scenarios = new CycleScenarioCollection();
			}

			settings.Scenarios.Clear();
			foreach (CycleScenario scenario in cycleScenarioBindingSource)
			{
				settings.Scenarios.Add(scenario);
			}

			Settings.Default.Save();
		}


		private void EnsureControllersInitialized()
		{
			if ((null != PortFacade) && PortFacade.IsOpen && (0 == _controllers.Count))
			{
				_controllers[SynchronizationStyle.OneAtATime] = new OneAtATimeController(PortFacade);
				_controllers[SynchronizationStyle.Shortest] = new ShortestController(PortFacade);
				_controllers[SynchronizationStyle.Longest] = new LongestController(PortFacade);
				_controllers[SynchronizationStyle.None] = new UnsynchronizedController(PortFacade);
				_previousController = _controllers[SynchronizationStyle.OneAtATime];
			}
		}


		private void PortFacade_Opened(object aSender, EventArgs aArgs)
		{
			EnsureControllersInitialized();
			configureDevices.Enabled = true;
			_dispatcher.BeginInvoke(ResetMessageCount);
		}


		private void PortFacade_Closed(object aSender, EventArgs aArgs)
			=> _dispatcher.BeginInvoke(Cleanup);


		private void Cleanup()
		{
			foreach (var controller in _controllers.Values)
			{
				controller.Disconnect();
			}

			_controllers.Clear();
			_activeController = null;
			_previousController = null;
			configureDevices.Enabled = false;
		}

		#endregion

		#region -- User interface callbacks --

		private void OnSettingsReset(object aSender, EventArgs aArgs)
		{
			var settings = Settings.Default;
			settings.Reset();
			RestoreScenariosFromSettings();
		}


		private void OnScenarioGridCellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
		{
			_lastMouseDownRow = e.RowIndex;
			_lastMouseDownColumn = e.ColumnIndex;
		}


		private void OnScenarioGridCellContentClick(object aSender, DataGridViewCellEventArgs aArgs)
		{
			if (0 == aArgs.ColumnIndex)
			{
				if (!PortFacade.IsOpen)
				{
					MessageBox.Show("The port is closed.", "Run Cycle");
					return;
				}

				var scenario = (CycleScenario) cycleScenarioBindingSource[aArgs.RowIndex];
				if (scenario.IsRunning)
				{
					_activeController.Cancel();
				}
				else if (_activeController != null)
				{
					_queuedScenario = scenario;
					_activeController.Cancel();
				}
				else
				{
					// To prevent drag-drop from accidentally triggering runs, we will only accept 
					// start clicks in the same cell where the last mouse down event happened.
					if ((aArgs.ColumnIndex == _lastMouseDownColumn) && (aArgs.RowIndex == _lastMouseDownRow))
					{
						RunScenario(scenario);
					}
				}
			}
		}


		private void OnScenarioGridCellValidating(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if ("Speed" == scenarioGrid.Columns[e.ColumnIndex].HeaderText)
			{
				var txt = e.FormattedValue.ToString();
				if (!string.IsNullOrEmpty(txt))
				{
					if (!long.TryParse(txt, out var dummy))
					{
						scenarioGrid.Rows[e.RowIndex].ErrorText = "Speed must be an integer or empty.";
						e.Cancel = true;
					}
				}
			}
		}


		private void OnScenarioGridCellEndEdit(object sender, DataGridViewCellEventArgs e)
			=> scenarioGrid.Rows[e.RowIndex].ErrorText = string.Empty;


		private void OnConfigureDevicesClick(object aSender, EventArgs aArgs)
		{
			var controller = _activeController ?? _previousController;
			controller.ConfigureDevices();
			messageText.Text = controller.Messages;
		}

		#endregion

		#region -- Worker threads --

		private void OnPollTimerTick(object aSender, EventArgs aArgs)
		{
			_activeController?.Poll();
		}


		private void OnBackgroundWorkerDoWork(object aSender, DoWorkEventArgs aArgs)
		{
			var scenario = (CycleScenario) aArgs.Argument;
			_activeController = _controllers[scenario.SynchronizationStyle];
			_activeController.MessageAdded += OnActiveControllerMessageAdded;
			_eventLog.LogEvent("Scenario started", scenario.SynchronizationStyle.ToString());
			_activeController.Run(scenario.TargetSpeed);
			aArgs.Result = scenario;
		}


		private void OnBackgroundWorkerRunWorkerCompleted(object aSender, RunWorkerCompletedEventArgs aArgs)
		{
			var scenario = (CycleScenario) aArgs.Result;
			scenario.IsRunning = false;
			pollTimer.Enabled = false;
			_portFacade.IsInvalidateEnabled = true;
			_previousController = _activeController;
			_activeController = null;

			if (_previousController != null)
			{
				_previousController.MessageAdded -= OnActiveControllerMessageAdded;
			}

			cycleScenarioBindingSource.ResetBindings(false);
			if (_queuedScenario != null)
			{
				scenario = _queuedScenario;
				_queuedScenario = null;
				RunScenario(scenario);
			}
		}

		#endregion

		#region -- Helpers --

		private void RestoreScenariosFromSettings()
		{
			cycleScenarioBindingSource.Clear();
			var settings = Settings.Default;
			if (settings.Scenarios != null)
			{
				foreach (var scenario in settings.Scenarios)
				{
					scenario.IsRunning = false;
					cycleScenarioBindingSource.Add(scenario);
				}
			}
		}


		private void RunScenario(CycleScenario aScenario)
		{
			aScenario.IsRunning = true;
			pollTimer.Enabled = true;
			_portFacade.IsInvalidateEnabled = false;
			ResetMessageCount();
			backgroundWorker.RunWorkerAsync(aScenario);
		}


		private void OnActiveControllerMessageAdded(object aSender, EventArgs aArgs)
		{
			Interlocked.Increment(ref _messageCount);
			if (InvokeRequired)
			{
				BeginInvoke(new Action(UpdateMessagesTab));
			}
			else
			{
				UpdateMessagesTab();
			}
		}


		private void UpdateMessagesTab()
		{
			messagesTab.Text = $"{_messageCount} Message{(_messageCount == 1 ? "" : "s")}";

			var controller = _activeController ?? _previousController;
			if (controller != null)
			{
				messageText.Text = controller.Messages;
			}
		}


		private void ResetMessageCount()
		{
			_messageCount = 0;
			messagesTab.Text = "No Messages";
		}

		#endregion

		#region -- Data --

		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Cycle tab");

		private IPlugInManager _pluginManager;
		private readonly IDispatcher _dispatcher = DispatcherHelper.Dispatcher;
		private ZaberPortFacade _portFacade;
		private Controller _activeController;
		private Controller _previousController;
		private CycleScenario _queuedScenario;

		private readonly Dictionary<SynchronizationStyle, Controller> _controllers =
			new Dictionary<SynchronizationStyle, Controller>();

		private int _messageCount;
		private int _lastMouseDownRow = -1;
		private int _lastMouseDownColumn = -1;

		#endregion
	}
}

﻿using System.Linq;
using System.Threading;
using Zaber;

namespace CycleDemo
{
	public class ShortestController : Controller
	{
		public ShortestController(ZaberPortFacade aPort)
			: base(aPort)
		{
		}


		public override void Run(decimal? aRequestedSpeed)
		{
			Reset(aRequestedSpeed);
			while (CycleState == CycleState.Running)
			{
				foreach (var device in Devices)
				{
					Move(device);
				}

				if (Devices.Any())
				{
					WaitForMovement();
				}
				else
				{
					Cancel();
				}
			}
		}


		protected virtual void WaitForMovement() => WaitAny(Timeout.Infinite);
	}
}

﻿using System.Threading;
using Zaber;

namespace CycleDemo
{
	public class CycleDevice
	{
		public CycleDevice(Conversation aConversation)
		{
			Conversation = aConversation;
			MovementFinishedEvent = new ManualResetEvent(true);
		}


		public Conversation Conversation { get; }

		public ZaberDevice Device => Conversation.Device;

		public decimal TargetSpeed { get; set; }

		public bool IsHomed { get; set; }

		public ManualResetEvent MovementFinishedEvent { get; }
	}
}

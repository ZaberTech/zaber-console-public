﻿using System.Threading;
using Zaber;

namespace CycleDemo
{
	public class LongestController : ShortestController
	{
		public LongestController(ZaberPortFacade aPort)
			: base(aPort)
		{
		}


		protected override void WaitForMovement() => WaitAll(Timeout.Infinite);
	}
}

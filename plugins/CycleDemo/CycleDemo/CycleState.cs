﻿namespace CycleDemo
{
	public enum CycleState
	{
		Running,
		Stopping,
		Stopped
	}
}

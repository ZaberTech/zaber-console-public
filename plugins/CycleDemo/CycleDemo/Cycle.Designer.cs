﻿namespace CycleDemo
{
    partial class Cycle
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Cycle));
			this.scenarioGrid = new System.Windows.Forms.DataGridView();
			this.RunCancel = new System.Windows.Forms.DataGridViewButtonColumn();
			this.targetSpeedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.synchColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
			this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.cycleScenarioBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
			this.pollTimer = new System.Windows.Forms.Timer(this.components);
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.scenariosTab = new System.Windows.Forms.TabPage();
			this.messagesTab = new System.Windows.Forms.TabPage();
			this.configureDevices = new System.Windows.Forms.Button();
			this.messageText = new System.Windows.Forms.TextBox();
			this.helpTab = new System.Windows.Forms.TabPage();
			this.helpText = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.scenarioGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cycleScenarioBindingSource)).BeginInit();
			this.tabControl1.SuspendLayout();
			this.scenariosTab.SuspendLayout();
			this.messagesTab.SuspendLayout();
			this.helpTab.SuspendLayout();
			this.SuspendLayout();
			// 
			// scenarioGrid
			// 
			this.scenarioGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.scenarioGrid.AutoGenerateColumns = false;
			this.scenarioGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
			this.scenarioGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.scenarioGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RunCancel,
            this.targetSpeedDataGridViewTextBoxColumn,
            this.synchColumn,
            this.Description});
			this.scenarioGrid.DataSource = this.cycleScenarioBindingSource;
			this.scenarioGrid.Location = new System.Drawing.Point(3, 3);
			this.scenarioGrid.Name = "scenarioGrid";
			this.scenarioGrid.Size = new System.Drawing.Size(522, 310);
			this.scenarioGrid.TabIndex = 3;
			this.scenarioGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.OnScenarioGridCellContentClick);
			this.scenarioGrid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.OnScenarioGridCellEndEdit);
			this.scenarioGrid.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.OnScenarioGridCellMouseDown);
			this.scenarioGrid.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.OnScenarioGridCellValidating);
			// 
			// RunCancel
			// 
			this.RunCancel.DataPropertyName = "ButtonName";
			this.RunCancel.HeaderText = "Run/Cancel";
			this.RunCancel.Name = "RunCancel";
			this.RunCancel.ReadOnly = true;
			this.RunCancel.Width = 71;
			// 
			// targetSpeedDataGridViewTextBoxColumn
			// 
			this.targetSpeedDataGridViewTextBoxColumn.DataPropertyName = "TargetSpeed";
			this.targetSpeedDataGridViewTextBoxColumn.HeaderText = "Speed";
			this.targetSpeedDataGridViewTextBoxColumn.Name = "targetSpeedDataGridViewTextBoxColumn";
			this.targetSpeedDataGridViewTextBoxColumn.ToolTipText = "Leave blank to use each device\'s default speed";
			this.targetSpeedDataGridViewTextBoxColumn.Width = 63;
			// 
			// synchColumn
			// 
			this.synchColumn.DataPropertyName = "SynchronizationStyle";
			this.synchColumn.HeaderText = "Synch.";
			this.synchColumn.Name = "synchColumn";
			this.synchColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.synchColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			this.synchColumn.ToolTipText = "How to synchronize multiple devices";
			this.synchColumn.Width = 65;
			// 
			// Description
			// 
			this.Description.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.Description.DataPropertyName = "Description";
			this.Description.HeaderText = "Description";
			this.Description.Name = "Description";
			this.Description.ToolTipText = "Notes on when to use this scenario";
			// 
			// cycleScenarioBindingSource
			// 
			this.cycleScenarioBindingSource.DataSource = typeof(CycleDemo.CycleScenario);
			// 
			// backgroundWorker
			// 
			this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.OnBackgroundWorkerDoWork);
			this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.OnBackgroundWorkerRunWorkerCompleted);
			// 
			// pollTimer
			// 
			this.pollTimer.Interval = 30000;
			this.pollTimer.Tick += new System.EventHandler(this.OnPollTimerTick);
			// 
			// tabControl1
			// 
			this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tabControl1.Controls.Add(this.scenariosTab);
			this.tabControl1.Controls.Add(this.messagesTab);
			this.tabControl1.Controls.Add(this.helpTab);
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(536, 345);
			this.tabControl1.TabIndex = 5;
			// 
			// scenariosTab
			// 
			this.scenariosTab.Controls.Add(this.scenarioGrid);
			this.scenariosTab.Location = new System.Drawing.Point(4, 22);
			this.scenariosTab.Name = "scenariosTab";
			this.scenariosTab.Padding = new System.Windows.Forms.Padding(3);
			this.scenariosTab.Size = new System.Drawing.Size(528, 319);
			this.scenariosTab.TabIndex = 0;
			this.scenariosTab.Text = "Scenarios";
			this.scenariosTab.UseVisualStyleBackColor = true;
			// 
			// messagesTab
			// 
			this.messagesTab.Controls.Add(this.configureDevices);
			this.messagesTab.Controls.Add(this.messageText);
			this.messagesTab.Location = new System.Drawing.Point(4, 22);
			this.messagesTab.Name = "messagesTab";
			this.messagesTab.Padding = new System.Windows.Forms.Padding(3);
			this.messagesTab.Size = new System.Drawing.Size(528, 319);
			this.messagesTab.TabIndex = 1;
			this.messagesTab.Text = "No Messages";
			this.messagesTab.UseVisualStyleBackColor = true;
			// 
			// configureDevices
			// 
			this.configureDevices.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.configureDevices.Enabled = false;
			this.configureDevices.Location = new System.Drawing.Point(6, 290);
			this.configureDevices.Name = "configureDevices";
			this.configureDevices.Size = new System.Drawing.Size(107, 23);
			this.configureDevices.TabIndex = 6;
			this.configureDevices.Text = "Configure Devices";
			this.configureDevices.UseVisualStyleBackColor = true;
			this.configureDevices.Click += new System.EventHandler(this.OnConfigureDevicesClick);
			// 
			// messageText
			// 
			this.messageText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.messageText.Location = new System.Drawing.Point(3, 3);
			this.messageText.Multiline = true;
			this.messageText.Name = "messageText";
			this.messageText.ReadOnly = true;
			this.messageText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.messageText.Size = new System.Drawing.Size(522, 281);
			this.messageText.TabIndex = 7;
			// 
			// helpTab
			// 
			this.helpTab.Controls.Add(this.helpText);
			this.helpTab.Location = new System.Drawing.Point(4, 22);
			this.helpTab.Name = "helpTab";
			this.helpTab.Padding = new System.Windows.Forms.Padding(3);
			this.helpTab.Size = new System.Drawing.Size(528, 319);
			this.helpTab.TabIndex = 2;
			this.helpTab.Text = "Help";
			this.helpTab.UseVisualStyleBackColor = true;
			// 
			// helpText
			// 
			this.helpText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.helpText.Location = new System.Drawing.Point(3, 3);
			this.helpText.Multiline = true;
			this.helpText.Name = "helpText";
			this.helpText.ReadOnly = true;
			this.helpText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.helpText.Size = new System.Drawing.Size(522, 313);
			this.helpText.TabIndex = 8;
			this.helpText.Text = resources.GetString("helpText.Text");
			// 
			// Cycle
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.tabControl1);
			this.Name = "Cycle";
			this.Size = new System.Drawing.Size(536, 345);
			((System.ComponentModel.ISupportInitialize)(this.scenarioGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cycleScenarioBindingSource)).EndInit();
			this.tabControl1.ResumeLayout(false);
			this.scenariosTab.ResumeLayout(false);
			this.messagesTab.ResumeLayout(false);
			this.messagesTab.PerformLayout();
			this.helpTab.ResumeLayout(false);
			this.helpTab.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView scenarioGrid;
        private System.Windows.Forms.BindingSource cycleScenarioBindingSource;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.Timer pollTimer;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage scenariosTab;
        private System.Windows.Forms.TabPage messagesTab;
        private System.Windows.Forms.Button configureDevices;
        private System.Windows.Forms.TextBox messageText;
        private System.Windows.Forms.TabPage helpTab;
        private System.Windows.Forms.TextBox helpText;
        private System.Windows.Forms.DataGridViewButtonColumn RunCancel;
        private System.Windows.Forms.DataGridViewTextBoxColumn targetSpeedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn synchColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
    }
}

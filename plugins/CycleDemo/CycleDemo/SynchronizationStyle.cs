﻿namespace CycleDemo
{
	public enum SynchronizationStyle
	{
		None,
		OneAtATime,
		Shortest,
		Longest
	}
}

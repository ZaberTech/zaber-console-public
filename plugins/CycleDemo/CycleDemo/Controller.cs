﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using log4net;
using Zaber;
using Zaber.Telemetry;

namespace CycleDemo
{
	/// <summary>
	///     Control the movements of all devices according to the selected
	///     synchronization style.
	/// </summary>
	public abstract class Controller
	{
		#region -- Events --

		public event EventHandler MessageAdded;

		#endregion

		#region -- Initialization and cleanup --

		public Controller(ZaberPortFacade aPort)
		{
			PortFacade = aPort;

			if (!PortFacade.Port.IsAsciiMode)
			{
				//Handle the binary replies
				PortFacade.GetDevice(0).MessageReceived += OnDeviceMessageReceived;
			}
		}


		public ZaberPortFacade PortFacade { get; }


		public CycleState CycleState
		{
			get => _cycleState;
			private set => _cycleState = value;
		}


		public void ConfigureDevices()
		{
			var isChanged = false;
			_isConfiguring = true;

			try
			{
				if (_deviceList.Count == 0)
				{
					BuildDeviceList();
				}

				if (!PortFacade.Port.IsAsciiMode)
				{
					foreach (var cycleDevice in _deviceList)
					{
						var conversation = cycleDevice.Conversation;

						if (IsRotationStage(conversation))
						{
							var deviceMode = (DeviceModes) conversation.Request(Command.ReturnSetting,
																				(int) Command.SetDeviceMode)
																	.NumericData;

							if (IsAutoHomeEnabled(deviceMode))
							{
								if (_cycleState == CycleState.Running)
								{
									conversation.Request(Command.Stop);
								}

								conversation.Request(Command.SetDeviceMode,
													 (int) (deviceMode | DeviceModes.DisableAutoHome));

								isChanged = true;
								_messageWriter.WriteLine("{0:HH:mm} Disabled auto home on device number {1}.",
														 DateTime.Now,
														 conversation.Device.DeviceNumber);
							}
						}
					}
				}
			}
			finally
			{
				_isConfiguring = false;
			}

			if (!isChanged)
			{
				_messageWriter.WriteLine("No configuration changes needed.");
			}
			else
			{
				Poll(); //kick off the movement
			}
		}


		public void Disconnect()
		{
			if (!PortFacade.Port.IsAsciiMode)
			{
				PortFacade.GetDevice(0).MessageReceived -= OnDeviceMessageReceived;
			}

			_cycleState = CycleState.Stopped;

			if (_asciiStatusPollThread != null)
			{
				// abort thread and wait for termination
				_asciiStatusPollThread.Abort();
				_asciiStatusPollThread.Join();
			}

			if (_movementFinishedEvents != null)
			{
				foreach (var movementFinishedEvent in _movementFinishedEvents)
				{
					movementFinishedEvent.Set();
				}
			}
		}

		#endregion

		#region -- Movement control --

		public abstract void Run(decimal? aRequestedSpeed);


		public virtual void Cancel()
		{
			_eventLog.LogEvent("Scenario stopped");
			_cycleState = CycleState.Stopping;
			var allConversations = PortFacade.GetConversation(0);

			var topic = allConversations.StartTopic();
			if (PortFacade.Port.IsAsciiMode)
			{
				allConversations.Device.Send("stop", topic.MessageId);
			}
			else
			{
				allConversations.Device.Send(Command.Stop, 0, topic.MessageId);
				topic.WaitHandle.WaitOne(1000);
			}

			// release all the threads.
			if (_movementFinishedEvents != null)
			{
				foreach (var ev in _movementFinishedEvents)
				{
					ev.Set();
				}
			}

			_cycleState = CycleState.Stopped;
		}


		/// <summary>
		///     Called at the start of every run.
		/// </summary>
		/// <param name="aRequestedSpeed"></param>
		protected void Reset(decimal? aRequestedSpeed)
		{
			_messageWriter = new StringWriter();
			_messageWriter.WriteLine("{0:HH:mm} Started cycling", DateTime.Now);
			_cycleState = CycleState.Stopped;

			// wait until thread terminates
			_asciiStatusPollThread?.Join();

			_deviceMap.Clear();
			_deviceList.Clear();
			BuildDeviceList();

			foreach (var cycleDevice in _deviceList)
			{
				var conversation = cycleDevice.Conversation;
				cycleDevice.TargetSpeed = 0m;

				if (aRequestedSpeed.HasValue)
				{
					cycleDevice.TargetSpeed = aRequestedSpeed.Value;
				}
				else
				{
					// grab the running speed from the device
					if (PortFacade.Port.IsAsciiMode)
					{
						cycleDevice.TargetSpeed = conversation.Request("get maxspeed").NumericData;
					}
					else
					{
						cycleDevice.TargetSpeed = conversation
											  .Request(Command.ReturnSetting, (int) Command.SetTargetSpeed)
											  .NumericData;
					}
				}

				var reqConfigure = false; // Does the device require configuration
				if (!PortFacade.Port.IsAsciiMode)
				{
					var deviceMode = (DeviceModes) (int) conversation
													 .Request(Command.ReturnSetting, (int) Command.SetDeviceMode)
													 .NumericData;
					cycleDevice.IsHomed = IsHomed(deviceMode);
					reqConfigure = IsRotationStage(conversation) && IsAutoHomeEnabled(deviceMode);
				}

				if (reqConfigure)
				{
					_messageWriter.WriteLine(
						"Device number {0} {1} requires configuration. Click the "
					+ "Configure Devices button to set it up for CycleDemo.",
						conversation.Device.DeviceNumber,
						conversation.Device.AxisNumber);
				}
			}

			_movementFinishedEvents = _deviceList.Select(device => device.MovementFinishedEvent).ToArray();

			_cycleState = CycleState.Running;

			if (PortFacade.Port.IsAsciiMode)
			{
				_asciiStatusPollThread = new Thread(AsciiStatusPoll)
				{
					Name = "CycleDemo ASCII poll thread",
					Priority = ThreadPriority.Lowest
				};

				_asciiStatusPollThread.Start();
			}

			MessageAdded?.Invoke(this, EventArgs.Empty);
		}


		protected ManualResetEvent Move(ZaberDevice aDevice)
		{
			var devTup = Tuple.Create(aDevice.DeviceNumber, aDevice.AxisNumber);
			var cycleDevice = _deviceMap[devTup];

			lock (cycleDevice)
			{
				cycleDevice.MovementFinishedEvent.Reset();
				cycleDevice.TargetSpeed *= -1m;

				try
				{
					if (PortFacade.Port.IsAsciiMode)
					{
						cycleDevice.Conversation.Request("move vel", cycleDevice.TargetSpeed);
					}
					else
					{
						aDevice.Send(Command.MoveAtConstantSpeed, (int) cycleDevice.TargetSpeed);
					}
				}
				catch (ErrorResponseException)
				{
					_messageWriter.WriteLine("{0:HH:mm}: Stopping because of a device error.", DateTime.Now);
					Cancel();
					MessageAdded?.Invoke(this, EventArgs.Empty);
				}
			}

			if (!PortFacade.Port.IsAsciiMode)
			{
				Thread.Sleep(100);
			}

			return cycleDevice.MovementFinishedEvent;
		}

		#endregion

		#region -- Synchronization --

		protected bool WaitAll(int aTimeoutMs) => WaitHandle.WaitAll(_movementFinishedEvents, aTimeoutMs);


		protected ZaberDevice WaitAny(int aTimeoutMs)
		{
			var index = WaitHandle.WaitAny(_movementFinishedEvents, aTimeoutMs);

			if (index == WaitHandle.WaitTimeout)
			{
				return null;
			}

			return _deviceList[index].Device;
		}


		protected bool WaitOne(ZaberDevice aDevice, int aTimeoutMs)
		{
			var devTup = Tuple.Create(aDevice.DeviceNumber, aDevice.AxisNumber);
			var cycleDevice = _deviceMap[devTup];
			return cycleDevice.MovementFinishedEvent.WaitOne(aTimeoutMs);
		}


		public void Poll()
		{
			if (!PortFacade.Port.IsAsciiMode)
			{
				foreach (var cycleDevice in _deviceMap.Values)
				{
					cycleDevice.Device.Send(Command.MoveAtConstantSpeed, (int) cycleDevice.TargetSpeed);
					Thread.Sleep(100);
				}
			}
		}


		private void AsciiStatusPoll()
		{
			while (_cycleState == CycleState.Running)
			{
				foreach (var cycleDevice in _deviceList)
				{
					// This lock and the one in Move() are to fix a thread sync problem that
					// was causing devices to stutter on rare occasions.
					lock (cycleDevice)
					{
						string commError = null;
						DeviceMessage reply = null;

						try
						{
							reply = cycleDevice.Conversation.Request("warnings");
						}
						catch (Exception e)
						{
							_log.Error("Cycling stopped due to communication error.", e);
							commError = e.Message;
						}

						if (commError != null)
						{
							_messageWriter.WriteLine("{0:HH:mm} device {1} axis {2} communication error: {3}",
													 DateTime.Now,
													 cycleDevice.Device.DeviceNumber,
													 cycleDevice.Device.AxisNumber,
													 commError);

							MessageAdded?.Invoke(this, EventArgs.Empty);

							_cycleState = CycleState.Stopping;
							cycleDevice.MovementFinishedEvent.Set();
						}
						else
						{
							// NOTE: Cannot not stop at NI flag because
							// ShortestController triggers NI flags
							// on all axes that are not the shortest one
							var stopFlags = new List<string> { WarningFlags.ManualControl };

							var flags = reply.TextData.Split(' ').ToList();

							var foundFlags = flags.Intersect(stopFlags);

							if (foundFlags.Count() > 0)
							{
								_cycleState = CycleState.Stopping;
								_messageWriter.WriteLine("{0:HH:mm} device {1} axis {2} reported warning(s) {3}.",
														 DateTime.Now,
														 cycleDevice.Device.DeviceNumber,
														 cycleDevice.Device.AxisNumber,
														 foundFlags.Aggregate((i, j) => i + ", " + j));

								MessageAdded?.Invoke(this, EventArgs.Empty);

								cycleDevice.MovementFinishedEvent.Set();
							}
							else if (reply.IsIdle)
							{
								cycleDevice.MovementFinishedEvent.Set();
							}
						}
					}
				}
			}
		}

		#endregion

		#region -- Communication --

		private void OnDeviceMessageReceived(object aSender, DeviceMessageEventArgs aArgs)
		{
			if (_cycleState == CycleState.Stopped)
			{
				return;
			}

			var devTup = Tuple.Create(aArgs.DeviceMessage.DeviceNumber, aArgs.DeviceMessage.AxisNumber);
			if (!_deviceMap.TryGetValue(devTup, out var cycleDevice))
			{
				return;
			}

			var isMovementFinished = false;
			switch (aArgs.DeviceMessage.Command)
			{
				case Command.Home:
					isMovementFinished = true;
					if (!cycleDevice.IsHomed)
					{
						cycleDevice.IsHomed = true;
					}
					else
					{
						_messageWriter.WriteLine("{0:HH:mm} device number {1} triggered the home sensor.",
												 DateTime.Now,
												 aArgs.DeviceMessage.DeviceNumber);

						MessageAdded?.Invoke(this, EventArgs.Empty);
					}

					break;

				case Command.LimitActive:
					isMovementFinished = (aArgs.DeviceMessage.NumericData <= 0m) ^ (cycleDevice.TargetSpeed > 0m);
					break;

				case Command.Stop:
					if (!_isConfiguring)
					{
						_cycleState = CycleState.Stopping;
					}

					isMovementFinished = true;
					break;

				case Command.SetTargetSpeed:
					var newTargetSpeed = aArgs.DeviceMessage.NumericData * Math.Sign(cycleDevice.TargetSpeed);
					cycleDevice.TargetSpeed = newTargetSpeed;
					break;

				case Command.Error:
				default:
					break;
			}

			if (isMovementFinished)
			{
				cycleDevice.MovementFinishedEvent.Set();
			}
		}


		public string Messages => _messageWriter.ToString();


		public IEnumerable<ZaberDevice> Devices => _deviceList.Select(cycleDevice => cycleDevice.Device);

		#endregion

		#region -- Helpers --

		private static bool IsAutoHomeEnabled(DeviceModes aMode)
			=> (aMode & DeviceModes.DisableAutoHome) == DeviceModes.None;


		private static bool IsHomed(DeviceModes aMode) => (aMode & DeviceModes.HomeStatus) != DeviceModes.None;


		private static bool IsRotationStage(Conversation aConversation)
		{
			var name = aConversation.Device.DeviceType.Name;
			return (name != null) && name.StartsWith("T-RS");
		}


		private void BuildDeviceList()
		{
			foreach (var conversation in PortFacade.AllConversations.Where(
				conversation => conversation.Device.IsSingleDevice))
			{
				var device = conversation.Device;
				var isActuator = false;

				if (PortFacade.Port.IsAsciiMode)
				{
					isActuator = (device.AxisNumber > 0) && (device.DeviceType.GetCommandByText("home") != null);
				}
				else
				{
					isActuator = device.DeviceType.GetCommandByNumber(Command.Home) != null;
				}

				if (isActuator)
				{
					var cycleDevice = new CycleDevice(conversation);
					var devTup = Tuple.Create(device.DeviceNumber, device.AxisNumber);
					_deviceMap[devTup] = cycleDevice;
					_deviceList.Add(cycleDevice);
				}

				_messageWriter.WriteLine("{0:HH:mm} device number {1} {2} isActuator {3}",
										 DateTime.Now,
										 device.DeviceNumber,
										 device.AxisNumber,
										 isActuator);
			}
		}

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Cycle tab");

		private volatile CycleState _cycleState = CycleState.Stopped;

		private readonly Dictionary<Tuple<byte, int>, CycleDevice> _deviceMap =
			new Dictionary<Tuple<byte, int>, CycleDevice>();

		private readonly List<CycleDevice> _deviceList = new List<CycleDevice>();
		private ManualResetEvent[] _movementFinishedEvents;
		private StringWriter _messageWriter = new StringWriter();
		private volatile bool _isConfiguring;
		private Thread _asciiStatusPollThread;

		#endregion
	}
}

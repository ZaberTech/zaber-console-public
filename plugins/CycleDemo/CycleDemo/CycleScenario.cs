﻿using System;

namespace CycleDemo
{
	[Serializable]
	public class CycleScenario
	{
		public bool IsRunning { get; set; }

		public string ButtonName => IsRunning ? "Cancel" : "Run";

		public string Description { get; set; }

		public SynchronizationStyle SynchronizationStyle { get; set; }

		public decimal? TargetSpeed { get; set; }
	}
}

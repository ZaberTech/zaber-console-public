﻿using NUnit.Framework;
using SimpleControlsPlugin;
using Zaber;
using Zaber.Testing;

namespace SimpleControlsTests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class AxisPropertiesControlVMTests
	{
		[SetUp]
		public void Setup() => _vm = new AxisPropertyControlVM();


		[TearDown]
		public void TearDown() => _listener?.Unlisten(_vm);


		[Test]
		public void TestAcceptsNegativeNumbers()
		{
			_vm.SelectedActionType = ActionType.Absolute_Position;
			_vm.IsRotaryDevice = false;
			Assert.IsFalse(_vm.CanAcceptNegativeValues);

			_vm.SelectedActionType = ActionType.Relative_Position;
			Assert.IsFalse(_vm.CanAcceptNegativeValues);

			_vm.SelectedActionType = ActionType.Velocity;
			Assert.IsFalse(_vm.CanAcceptNegativeValues);

			_vm.SelectedActionType = ActionType.Absolute_Position;
			_vm.IsRotaryDevice = true;
			Assert.IsTrue(_vm.CanAcceptNegativeValues);
		}


		[Test]
		public void TestButtons()
		{
			var callCount = 0;
			var arg = 0m;
			var uom = UnitOfMeasure.Data;

			void Handler(object sender, decimal val, UnitOfMeasure unit)
			{
				++callCount;
				arg = val;
				uom = unit;
			}

			_vm.ActionInvoked += Handler;

			_vm.UnitOfMeasure = UnitOfMeasure.Amperes;
			_vm.Quantity = 4m;
			var cmd = _vm.LeftButtonCommand;
			Assert.IsTrue(cmd.CanExecute(null));
			cmd.Execute(null);
			Assert.AreEqual(1, callCount);
			Assert.AreEqual(-4m, arg);
			Assert.AreEqual(UnitOfMeasure.Amperes, uom);

			_vm.UnitOfMeasure = UnitOfMeasure.Degree;
			_vm.Quantity = 2m;
			cmd = _vm.RightButtonCommand;
			Assert.IsTrue(cmd.CanExecute(null));
			cmd.Execute(null);
			Assert.AreEqual(2, callCount);
			Assert.AreEqual(2m, arg);
			Assert.AreEqual(UnitOfMeasure.Degree, uom);
		}


		[Test]
		public void TestCanMoveBackwards()
		{
			_vm.SelectedActionType = ActionType.Absolute_Position;
			_vm.IsRotaryDevice = false;
			Assert.IsFalse(_vm.CanMoveBackwards);

			_vm.SelectedActionType = ActionType.Relative_Position;
			Assert.IsTrue(_vm.CanMoveBackwards);

			_vm.SelectedActionType = ActionType.Velocity;
			Assert.IsTrue(_vm.CanMoveBackwards);

			_vm.SelectedActionType = ActionType.Absolute_Position;
			_vm.IsRotaryDevice = true;
			Assert.IsFalse(_vm.CanMoveBackwards);
		}


		[Test]
		public void TestConstructor()
		{
			Assert.AreEqual(ActionType.Relative_Position, _vm.SelectedActionType);
			Assert.AreEqual(UnitOfMeasure.Data, _vm.MasterUnit);
			Assert.AreEqual(UnitOfMeasure.Data, _vm.UnitOfMeasure);
			Assert.AreEqual(0.0, _vm.Quantity);
			Assert.IsTrue(string.IsNullOrEmpty(_vm.ErrorMessage));
		}


		[Test]
		public void TestMasterUnitChange()
		{
			_vm.SelectedActionType = ActionType.Relative_Position;
			_vm.MasterUnit = UnitOfMeasure.Degree;
			Assert.AreEqual(UnitOfMeasure.Degree, _vm.UnitOfMeasure);

			_vm.SelectedActionType = ActionType.Velocity;
			Assert.AreEqual(UnitOfMeasure.DegreesPerSecond, _vm.UnitOfMeasure);

			_vm.MasterUnit = UnitOfMeasure.Meter;
			Assert.AreEqual(UnitOfMeasure.MetersPerSecond, _vm.UnitOfMeasure);

			_vm.SelectedActionType = ActionType.Absolute_Position;
			Assert.AreEqual(UnitOfMeasure.Meter, _vm.UnitOfMeasure);
		}


		[Test]
		public void TestObservableProperties()
		{
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
			_listener.TestObservableProperty(_vm,
											 () => _vm.SelectedActionType,
											 action =>
											 {
												 if (ActionType.Velocity == action)
												 {
													 return ActionType.Absolute_Position;
												 }

												 return ActionType.Velocity;
											 });

			_listener.TestObservableProperty(_vm,
											 () => _vm.UnitOfMeasure,
											 unit =>
											 {
												 if (UnitOfMeasure.Data == unit)
												 {
													 return UnitOfMeasure.Millimeter;
												 }

												 return UnitOfMeasure.Data;
											 });

			_listener.TestObservableProperty(_vm, () => _vm.Quantity);
			_listener.TestObservableProperty(_vm, () => _vm.ErrorMessage);
		}


		[Test]
		public void TestToolTipUpdates()
		{
			// Verify absolute move right message contains quantity and unit, and left is emtpy.
			_vm.SelectedActionType = ActionType.Absolute_Position;
			_vm.Quantity = 1m;
			_vm.UnitOfMeasure = UnitOfMeasure.Data;
			var prevMsgL = _vm.LeftButtonToolTip;
			var prevMsgR = _vm.RightButtonToolTip;
			Assert.AreNotEqual(prevMsgL, prevMsgR);
			Assert.IsTrue(string.IsNullOrEmpty(prevMsgL));
			Assert.IsTrue(prevMsgR.Contains(_vm.Quantity.ToString()));
			Assert.IsTrue(prevMsgR.Contains("microsteps"));

			// Verify changing the action type updates the messages and they both contain the quantity.
			_vm.SelectedActionType = ActionType.Relative_Position;
			var msgL = _vm.LeftButtonToolTip;
			var msgR = _vm.RightButtonToolTip;
			Assert.AreNotEqual(prevMsgL, msgL);
			Assert.AreNotEqual(prevMsgR, msgR);
			prevMsgL = msgL;
			prevMsgR = msgR;
			Assert.AreNotEqual(msgR, msgL);
			Assert.IsTrue(msgL.Contains(_vm.Quantity.ToString()));
			Assert.IsTrue(msgL.Contains("microsteps"));
			Assert.IsTrue(msgL.Contains(_vm.Quantity.ToString()));
			Assert.IsTrue(msgL.Contains("microsteps"));

			// Verify changing to the third action type updates the messages and they both contain the quantity.
			_vm.SelectedActionType = ActionType.Velocity;
			msgL = _vm.LeftButtonToolTip;
			msgR = _vm.RightButtonToolTip;
			Assert.AreNotEqual(prevMsgL, msgL);
			Assert.AreNotEqual(prevMsgR, msgR);
			prevMsgL = msgL;
			prevMsgR = msgR;
			Assert.AreNotEqual(msgR, msgL);
			Assert.IsTrue(msgL.Contains(_vm.Quantity.ToString()));
			Assert.IsTrue(msgL.Contains("microsteps"));
			Assert.IsTrue(msgL.Contains(_vm.Quantity.ToString()));
			Assert.IsTrue(msgL.Contains("microsteps"));

			// Verify changing the quantity updates the messages to contain the new quantity.
			_vm.Quantity = 2m;
			msgL = _vm.LeftButtonToolTip;
			msgR = _vm.RightButtonToolTip;
			Assert.AreNotEqual(prevMsgL, msgL);
			Assert.AreNotEqual(prevMsgR, msgR);
			prevMsgL = msgL;
			prevMsgR = msgR;
			Assert.AreNotEqual(msgR, msgL);
			Assert.IsTrue(msgL.Contains(_vm.Quantity.ToString()));
			Assert.IsTrue(msgL.Contains("microsteps"));
			Assert.IsTrue(msgL.Contains(_vm.Quantity.ToString()));
			Assert.IsTrue(msgL.Contains("microsteps"));

			// Verify changing the unit of measure updates the messages to contain the new unit.
			_vm.UnitOfMeasure = UnitOfMeasure.Meter;
			msgL = _vm.LeftButtonToolTip;
			msgR = _vm.RightButtonToolTip;
			Assert.AreNotEqual(prevMsgL, msgL);
			Assert.AreNotEqual(prevMsgR, msgR);
			prevMsgL = msgL;
			prevMsgR = msgR;
			Assert.AreNotEqual(msgR, msgL);
			Assert.IsTrue(msgL.Contains(_vm.Quantity.ToString()));
			Assert.IsTrue(msgL.Contains(UnitOfMeasure.Meter.ToString()));
			Assert.IsTrue(msgL.Contains(_vm.Quantity.ToString()));
			Assert.IsTrue(msgL.Contains(UnitOfMeasure.Meter.ToString()));
		}


		private AxisPropertyControlVM _vm;
		private ObservableObjectTester _listener;
	}
}

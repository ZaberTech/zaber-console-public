﻿using NUnit.Framework;
using SimpleControlsPlugin;
using SimpleControlsPlugin.Settings;
using Zaber;

namespace SimpleControlsTests.Settings
{
	[TestFixture]
	[SetCulture("en-US")]
	public class ControlGroupSettingsTests
	{
		// Just tests that defaults are set as expected.
		[Test]
		public void TestConstructor()
		{
			var grp = new ControlGroupSettings();
			Assert.AreEqual(ActionType.Absolute_Position, grp.ActionType);
			Assert.AreEqual(0.0, grp.Quantity);
			Assert.AreEqual(UnitOfMeasure.Data.Abbreviation, grp.Units);
		}
	}
}

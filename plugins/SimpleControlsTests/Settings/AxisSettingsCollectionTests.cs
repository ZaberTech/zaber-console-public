﻿using System.IO;
using System.Xml.Serialization;
using NUnit.Framework;
using SimpleControlsPlugin;
using SimpleControlsPlugin.Settings;
using Zaber;
using ZaberTest;
using ZaberTest.Testing;

namespace SimpleControlsTests.Settings
{
	[TestFixture]
	[SetCulture("en-US")]
	public class AxisSettingsCollectionTests
	{
		[SetUp]
		public void Setup()
		{
			_allDevicesId = new DeviceIdentity { IsAllDevices = true };

			_hasSerialId = new DeviceIdentity { DeviceSerialNumber = 12345 };

			_isPeripheralId = new DeviceIdentity { PeripheralId = 12345 };

			_hasDeviceIdId = new DeviceIdentity { DeviceId = 12345 };

			_hasNumberId = new DeviceIdentity
			{
				AxisNumber = 1,
				DeviceNumber = 1
			};

			_settings = new AxisSettingsCollection();
		}


		[Test]
		public void TestDeserialize()
		{
			var ser = new XmlSerializer(typeof(AxisSettingsCollection));
			AxisSettingsCollection settings = null;
			using (var reader =
				new StreamReader(TestDataFileHelper.GetDeployedPath("Settings\\AxisSettingsCollection.xml")))
			{
				settings = ser.Deserialize(reader) as AxisSettingsCollection;
			}

			Assert.IsNotNull(settings);
			Assert.AreEqual(2, settings.PerDeviceSettings.Count);

			var device = settings.PerDeviceSettings[0];
			Assert.IsTrue(device.DeviceIdentity.IsAllDevices);
			var groups = device.ControlGroups;
			Assert.AreEqual(3, groups.Count);
			foreach (var group in groups)
			{
				Assert.AreEqual(ActionType.Velocity, group.ActionType);
				Assert.AreEqual(UnitOfMeasure.Data.ToString(), group.Units);
			}

			Assert.AreEqual(50000.0, groups[0].Quantity);
			Assert.AreEqual(10000.0, groups[1].Quantity);
			Assert.AreEqual(5000.0, groups[2].Quantity);

			device = settings.PerDeviceSettings[1];
			Assert.IsFalse(device.DeviceIdentity.IsAllDevices);
			Assert.AreEqual(20043, device.DeviceIdentity.DeviceId);
			groups = device.ControlGroups;
			Assert.AreEqual(3, groups.Count);
			Assert.AreEqual(50.0, groups[0].Quantity);
			Assert.AreEqual(10.0, groups[1].Quantity);
			Assert.AreEqual(10.0, groups[2].Quantity);
			Assert.AreEqual(ActionType.Absolute_Position, groups[0].ActionType);
			Assert.AreEqual(ActionType.Relative_Position, groups[1].ActionType);
			Assert.AreEqual(ActionType.Velocity, groups[2].ActionType);
			foreach (var group in groups)
			{
				Assert.AreEqual(UnitOfMeasure.Millimeter.Abbreviation, group.Units);
			}
		}


		[Test]
		public void TestSerialize()
		{
			AddIds();
			TestForwardData(_settings); // Sanity check.

			// Serialize to a string.
			var ser = new XmlSerializer(typeof(AxisSettingsCollection));
			var data = ObjectToXmlString.Serialize(_settings);

			// Deserialize back to a new instance.
			Assert.IsFalse(string.IsNullOrEmpty(data));
			var newSettings = ObjectToXmlString.Deserialize<AxisSettingsCollection>(data);

			// Test we got the same data back.
			TestForwardData(newSettings);
		}


		private void AddIds()
		{
			_settings.PerDeviceSettings.Add(new AxisSettings { DeviceIdentity = _allDevicesId });
			_settings.PerDeviceSettings.Add(new AxisSettings { DeviceIdentity = _hasSerialId });
			_settings.PerDeviceSettings.Add(new AxisSettings { DeviceIdentity = _isPeripheralId });
			_settings.PerDeviceSettings.Add(new AxisSettings { DeviceIdentity = _hasDeviceIdId });
			_settings.PerDeviceSettings.Add(new AxisSettings { DeviceIdentity = _hasNumberId });
		}


		private void TestForwardData(AxisSettingsCollection aSettings)
		{
			Assert.IsNotNull(aSettings.PerDeviceSettings);
			var count = aSettings.PerDeviceSettings.Count;
			Assert.AreEqual(5, aSettings.PerDeviceSettings.Count);

			var id = new DeviceIdentity { IsAllDevices = true };
			var result = aSettings.FindOrCreateSettings(id);
			Assert.AreEqual(count, aSettings.PerDeviceSettings.Count);
			Assert.AreEqual(_allDevicesId, result.DeviceIdentity);

			id.IsAllDevices = false;
			id.DeviceSerialNumber = 12345;
			result = aSettings.FindOrCreateSettings(id);
			Assert.AreEqual(count, aSettings.PerDeviceSettings.Count);
			Assert.AreEqual(_hasSerialId, result.DeviceIdentity);

			id.DeviceSerialNumber = null;
			id.PeripheralId = 12345;
			result = aSettings.FindOrCreateSettings(id);
			Assert.AreEqual(count, aSettings.PerDeviceSettings.Count);
			Assert.AreEqual(_isPeripheralId, result.DeviceIdentity);

			id.PeripheralId = null;
			id.DeviceId = 12345;
			result = aSettings.FindOrCreateSettings(id);
			Assert.AreEqual(count, aSettings.PerDeviceSettings.Count);
			Assert.AreEqual(_hasDeviceIdId, result.DeviceIdentity);

			id.DeviceId = 0;
			id.AxisNumber = 1;
			id.DeviceNumber = 1;
			result = aSettings.FindOrCreateSettings(id);
			Assert.AreEqual(count, aSettings.PerDeviceSettings.Count);
			Assert.AreEqual(_hasNumberId, result.DeviceIdentity);
		}


		private DeviceIdentity _allDevicesId;
		private DeviceIdentity _hasSerialId;
		private DeviceIdentity _isPeripheralId;
		private DeviceIdentity _hasDeviceIdId;
		private DeviceIdentity _hasNumberId;
		private AxisSettingsCollection _settings;
	}
}

﻿using NUnit.Framework;
using SimpleControlsPlugin.Settings;

namespace SimpleControlsTests.Settings
{
	[TestFixture]
	[SetCulture("en-US")]
	public class AxisSettingsTests
	{
		// Just tests that defaults are expected.
		[Test]
		public void TestConstructor()
		{
			var settings = new AxisSettings();
			Assert.IsNotNull(settings.ControlGroups);
			Assert.AreEqual(0, settings.ControlGroups.Count);
		}
	}
}

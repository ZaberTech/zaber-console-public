﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using NUnit.Framework;
using SimpleControlsPlugin;
using Zaber;
using Zaber.Testing;
using ZaberTest;

namespace SimpleControlsTests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class SimpleControlsVMTests
	{
		[SetUp]
		public void Setup()
		{
			DispatcherStack.Push(new ImmediateDispatcher());
			BackgroundWorkerStack.Push(typeof(ImmediateForegroundWorker));

			_vm = new SimpleControlsVM();
			SetupPort();
			_vm.PortFacade = _portFacade;
		}


		[TearDown]
		public void Teardown()
		{
			_listener?.Unlisten(_vm);
			BackgroundWorkerStack.Pop();
			DispatcherStack.Pop();
		}


		[Test]
		public void TestConstructor()
		{
			Assert.IsNotNull(_vm.AxisControls);
			Assert.AreEqual(3, _vm.AxisControls.Count);
		}


		[Test]
		public void TestConversationChange()
		{
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
			_portFacade.Open("COM1");
			_listener.ClearNotifications();
			_vm.Conversation = _portFacade.GetConversation(1, 1); // Should be an axis.
			Assert.IsTrue(_listener.GetPropertiesChanged().Contains("IsMotionCapable"));

			_listener.ClearNotifications();
			_vm.Conversation = _portFacade.AllConversations.First(); // All devices
			Assert.IsTrue(_listener.GetPropertiesChanged().Contains("IsMotionCapable"));
		}


		[Test]
		public void TestDisabledOnNonMovableDevice()
		{
			_portFacade.Open("COM1");
			_vm.Conversation = _portFacade.GetConversation(1, 1); // Should be a motion axis.
			Assert.IsTrue(_vm.IsMotionCapable);

			_vm.Conversation = _portFacade.GetConversation(2, 1); // Should be an LED axis.
			Assert.IsFalse(_vm.IsMotionCapable);
		}


		[Test]
		public void TestHomeCommandAscii()
		{
			_portFacade.Open("COM1");
			_vm.Conversation = _portFacade.GetConversation(1, 1); // Should be an axis.
			_mockPort.Expect("/01 1 home");
			_mockPort.AddResponse("@01 1 OK IDLE -- 0");

			var cmd = _vm.HomeCommand;
			Assert.IsTrue(cmd.CanExecute(null));
			cmd.Execute(null);

			_mockPort.Verify();
		}


		[Test]
		public void TestInvokeAbsoluteMove()
		{
			_portFacade.Open("COM1");
			_vm.Conversation = _portFacade.GetConversation(1, 1); // Should be an axis.
			_mockPort.Expect("/01 1 move abs 1000");
			_mockPort.AddResponse("@01 1 OK BUSY -- 0");
			_mockPort.Expect("/01 1 move abs -1000000");
			_mockPort.AddResponse("@01 1 OK BUSY -- 0");

			_vm.AxisControls[0].SelectedActionType = ActionType.Absolute_Position;
			_vm.MasterUnitOfMeasure = UnitOfMeasure.Millimeter;

			_vm.AxisControls[0].Quantity = 10m;

			var cmd = _vm.AxisControls[0].RightButtonCommand;
			Assert.IsTrue(cmd.CanExecute(null));
			cmd.Execute(null);

			_vm.MasterUnitOfMeasure = UnitOfMeasure.Meter;

			cmd = _vm.AxisControls[0].LeftButtonCommand;
			Assert.IsTrue(cmd.CanExecute(null));
			cmd.Execute(null);

			_mockPort.Verify();
		}


		[Test]
		public void TestInvokeRelativeMove()
		{
			_portFacade.Open("COM1");
			_vm.Conversation = _portFacade.GetConversation(1, 1); // Should be an axis.
			_mockPort.Expect("/01 1 move rel 1000");
			_mockPort.AddResponse("@01 1 OK BUSY -- 0");
			_mockPort.Expect("/01 1 move rel -1000000");
			_mockPort.AddResponse("@01 1 OK BUSY -- 0");

			_vm.AxisControls[0].SelectedActionType = ActionType.Relative_Position;
			_vm.MasterUnitOfMeasure = UnitOfMeasure.Millimeter;

			_vm.AxisControls[0].Quantity = 10m;

			var cmd = _vm.AxisControls[0].RightButtonCommand;
			Assert.IsTrue(cmd.CanExecute(null));
			cmd.Execute(null);

			_vm.MasterUnitOfMeasure = UnitOfMeasure.Meter;

			cmd = _vm.AxisControls[0].LeftButtonCommand;
			Assert.IsTrue(cmd.CanExecute(null));
			cmd.Execute(null);

			_mockPort.Verify();
		}


		[Test]
		public void TestInvokeVelocityMove()
		{
			_portFacade.Open("COM1");
			_vm.Conversation = _portFacade.GetConversation(1, 1); // Should be an axis.
			_mockPort.Expect("/01 1 move vel 104858");
			_mockPort.AddResponse("@01 1 OK BUSY -- 0");
			_mockPort.Expect("/01 1 move vel -104857600");
			_mockPort.AddResponse("@01 1 OK BUSY -- 0");

			_vm.AxisControls[0].SelectedActionType = ActionType.Velocity;
			_vm.MasterUnitOfMeasure = UnitOfMeasure.Millimeter;

			_vm.AxisControls[0].Quantity = 10m;

			var cmd = _vm.AxisControls[0].RightButtonCommand;
			Assert.IsTrue(cmd.CanExecute(null));
			cmd.Execute(null);

			_vm.MasterUnitOfMeasure = UnitOfMeasure.Meter;

			cmd = _vm.AxisControls[0].LeftButtonCommand;
			Assert.IsTrue(cmd.CanExecute(null));
			cmd.Execute(null);

			_mockPort.Verify();
		}


		[Test]
		public void TestMasterUnitChange()
		{
			_portFacade.Open("COM1");
			_vm.Conversation = _portFacade.GetConversation(1, 1); // Should be an axis.

			_vm.MasterUnitOfMeasure = UnitOfMeasure.Inch;
			Assert.AreEqual(UnitOfMeasure.Inch, _vm.AxisControls[0].MasterUnit);

			_vm.MasterUnitOfMeasure = UnitOfMeasure.Millimeter;
			Assert.AreEqual(UnitOfMeasure.Millimeter, _vm.AxisControls[0].MasterUnit);
		}


		[Test]
		public void TestObservableProperties()
		{
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
			_listener.TestObservableProperty(_vm,
											 () => _vm.AxisControls,
											 list => new ObservableCollection<AxisPropertyControlVM>());
		}


		[Test]
		public void TestPortOpenClosed()
		{
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
			Assert.AreEqual(0, _listener.GetNumNotifications());
			_portFacade.Open("COM1");
			Assert.AreEqual(1, _listener.GetNumNotifications());
			Assert.IsTrue(_listener.GetPropertiesChanged().Contains("IsPortOpen"));
			_listener.ClearNotifications();
			Assert.AreEqual(0, _listener.GetNumNotifications());
			_portFacade.Close();
			Assert.AreEqual(1, _listener.GetNumNotifications());
			Assert.IsTrue(_listener.GetPropertiesChanged().Contains("IsPortOpen"));
		}


		[Test]
		public void TestStopCommandAscii()
		{
			_portFacade.Open("COM1");
			_vm.Conversation = _portFacade.GetConversation(1, 1); // Should be an axis.
			_mockPort.Expect("/01 1 stop");
			_mockPort.AddResponse("@01 1 OK IDLE -- 0");

			var cmd = _vm.StopCommand;
			Assert.IsTrue(cmd.CanExecute(null));
			cmd.Execute(null);

			_mockPort.Verify();
		}


		[Test]
		public void TestUnitPropagation()
		{
			_vm.AxisControls = new ObservableCollection<AxisPropertyControlVM>
			{
				new AxisPropertyControlVM { SelectedActionType = ActionType.Absolute_Position },
				new AxisPropertyControlVM { SelectedActionType = ActionType.Relative_Position },
				new AxisPropertyControlVM { SelectedActionType = ActionType.Velocity }
			};

			_vm.MasterUnitOfMeasure = UnitOfMeasure.Degree;
			Assert.AreEqual(UnitOfMeasure.Degree, _vm.AxisControls[0].UnitOfMeasure);
			Assert.AreEqual(UnitOfMeasure.Degree, _vm.AxisControls[1].UnitOfMeasure);
			Assert.AreEqual(UnitOfMeasure.DegreesPerSecond, _vm.AxisControls[2].UnitOfMeasure);

			_vm.MasterUnitOfMeasure = UnitOfMeasure.Millimeter;
			Assert.AreEqual(UnitOfMeasure.Millimeter, _vm.AxisControls[0].UnitOfMeasure);
			Assert.AreEqual(UnitOfMeasure.Millimeter, _vm.AxisControls[1].UnitOfMeasure);
			Assert.AreEqual(UnitOfMeasure.MillimetersPerSecond, _vm.AxisControls[2].UnitOfMeasure);
		}


		private void SetupPort()
		{
			_mockPort = new MockPort { IsAsciiMode = true };

			// Set up dummy port facade.
			var defaultDeviceType = new DeviceType { Commands = new List<CommandInfo>() };

			_portFacade = new ZaberPortFacade
			{
				DefaultDeviceType = defaultDeviceType,
				Port = _mockPort,
				QueryTimeout = 100
			};

			var cmds = new List<CommandInfo>
			{
				new CommandInfo
				{
					TextCommand = "move abs",
					RequestUnit = UnitOfMeasure.Meter,
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 100000.0m
				},
				new CommandInfo
				{
					TextCommand = "move rel",
					RequestUnit = UnitOfMeasure.Meter,
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 100000.0m
				},
				new CommandInfo
				{
					TextCommand = "move vel",
					RequestUnit = UnitOfMeasure.MetersPerSecond,
					RequestUnitFunction = ScalingFunction.LinearResolution,
					RequestUnitScale = 163840.0m
				},
				new SettingInfo
				{
					TextCommand = "pos",
					RequestUnit = UnitOfMeasure.Meter,
					RequestUnitFunction = ScalingFunction.Linear,
					RequestUnitScale = 100000.0m
				},
				new CommandInfo
				{
					TextCommand = "home"
				}
			};

			var peripheral0 = new DeviceType
			{
				PeripheralId = 0,
				MotionType = MotionType.Linear,
				Commands = cmds
			};

			var type98 = new DeviceType
			{
				DeviceId = 98,
				PeripheralMap = new Dictionary<int, DeviceType> { { 0, peripheral0 } }
			};

			_portFacade.AddDeviceType(type98);

			cmds = new List<CommandInfo>
			{
				new CommandInfo { TextCommand = "lamp on" },
				new CommandInfo { TextCommand = "lamp off" }
			};

			var led = new DeviceType
			{
				PeripheralId = 1,
				MotionType = MotionType.Other,
				Commands = cmds
			};

			var type99 = new DeviceType
			{
				DeviceId = 99,
				PeripheralMap = new Dictionary<int, DeviceType> { { 1, led } }
			};

			_portFacade.AddDeviceType(type99);

			ZaberPortFacadeTest.SetAsciiExpectations(_portFacade, _mockPort, type98, type99);
		}


		private SimpleControlsVM _vm;
		private MockPort _mockPort;
		private ZaberPortFacade _portFacade;
		private ObservableObjectTester _listener;
	}
}

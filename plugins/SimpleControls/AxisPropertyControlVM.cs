﻿using System;
using System.Windows.Input;
using Zaber;
using ZaberWpfToolbox;

namespace SimpleControlsPlugin
{
	/// <summary>
	///     ViewModel for the movement control groups in the Simple Controls plugin.
	/// </summary>
	public class AxisPropertyControlVM : ObservableObject
	{
		/// <summary>
		///     Event type for the event fired when a movement button is clicked.
		/// </summary>
		/// <param name="aSender">The control sending the event - ie this VM.</param>
		/// <param name="aValue">The user-entered movement amount.</param>
		/// <param name="aUnits">The unit of measure for the movement amount.</param>
		public delegate void ActionInvokedHandler(object aSender, decimal aValue, UnitOfMeasure aUnits);

		/// <summary>
		///     Event handler invoked when the user clicks a movement button. The parent VM
		///     hooks into this event to handle the actual movement at its level. This eliminates
		///     the need for the control groups to know about the port and conversation.
		/// </summary>
		public event ActionInvokedHandler ActionInvoked;


		/// <summary>
		///     Master unit of measure passed down from the device list.
		/// </summary>
		public UnitOfMeasure MasterUnit
		{
			get => _masterUnit;
			set
			{
				_masterUnit = value;
				UpdateUnits();
			}
		}


		/// <summary>
		///     Inform the controls that the selected device is a rotary stage. This affects
		///     whether negative absolute positions are accepted.
		/// </summary>
		public bool IsRotaryDevice
		{
			get => _isRotaryDevice;
			set
			{
				_isRotaryDevice = value;
				OnPropertyChanged(nameof(CanAcceptNegativeValues));
				if (!CanAcceptNegativeValues)
				{
					Quantity = Math.Max(0m, Quantity);
				}
			}
		}

		#region-- View-bound properties --

		/// <summary>
		///     Currently selected movement type for this group.
		/// </summary>
		public ActionType SelectedActionType
		{
			get => _selectedActionType;
			set
			{
				Set(ref _selectedActionType, value, nameof(SelectedActionType));

				if (!CanAcceptNegativeValues)
				{
					Quantity = Math.Max(0m, Quantity);
				}

				OnPropertyChanged(nameof(CanAcceptNegativeValues));
				OnPropertyChanged(nameof(CanMoveBackwards));
				UpdateUnits();
			}
		}


		/// <summary>
		///     Currently selected unit of measure for this group.
		/// </summary>
		public UnitOfMeasure UnitOfMeasure
		{
			get => _unitOfMeasure;
			set => Set(ref _unitOfMeasure, value, nameof(UnitOfMeasure));
		}


		/// <summary>
		///     User-entered movement amount.
		/// </summary>
		public decimal Quantity
		{
			get => _quantity;
			set
			{
				if (value < 0m && !CanAcceptNegativeValues)
				{
					value = 0m;
					ErrorMessage = "Number must be positive or zero.";
				}
				else
				{
					ErrorMessage = string.Empty;
				}

				Set(ref _quantity, value, nameof(Quantity));

				OnPropertyChanged(nameof(LeftButtonToolTip));
				OnPropertyChanged(nameof(RightButtonToolTip));
			}
		}


		/// <summary>
		///     True if negative values are accepted for the current motion type.
		/// </summary>
		public bool CanAcceptNegativeValues => IsRotaryDevice && ActionType.Absolute_Position == SelectedActionType;


		/// <summary>
		///     True if the reverse motion button should be shown.
		/// </summary>
		public bool CanMoveBackwards => ActionType.Absolute_Position != SelectedActionType;


		/// <summary>
		///     Error message from the device, if any.
		/// </summary>
		public string ErrorMessage
		{
			get => _errorMsg;
			set => Set(ref _errorMsg, value, nameof(ErrorMessage));
		}


		/// <summary>
		///     Command invoked by the move left/backward button.
		/// </summary>
		public ICommand LeftButtonCommand => new RelayCommand(_ => FireAction(-1m));


		/// <summary>
		///     Command invoked by the move right/forward button.
		/// </summary>
		public ICommand RightButtonCommand => new RelayCommand(_ => FireAction(1m));


		/// <summary>
		///     Observable property for views to sync RightButtonToolTip.
		/// </summary>
		public string RightButtonToolTip
		{
			get
			{
				if (!string.IsNullOrEmpty(_rightButtonToolTip))
				{
					string unitStr = null;
					unitStr = UnitOfMeasure.DataSymbol == UnitOfMeasure.Abbreviation ? "microsteps" : UnitOfMeasure.ToString();

					return string.Format(_rightButtonToolTip, Quantity, unitStr);
				}

				return _rightButtonToolTip;
			}
		}


		/// <summary>
		///     Observable property for views to sync LeftButtonToolTip.
		/// </summary>
		public string LeftButtonToolTip
		{
			get
			{
				if (!string.IsNullOrEmpty(_leftButtonToolTip))
				{
					string unitStr = null;
					unitStr = UnitOfMeasure.DataSymbol == UnitOfMeasure.Abbreviation ? "microsteps" : UnitOfMeasure.ToString();

					return string.Format(_leftButtonToolTip, Quantity, unitStr);
				}

				return _leftButtonToolTip;
			}
		}

		#endregion

		#region -- Functionality --

		// Handle master unit of measure changes.
		private void UpdateUnits()
		{
			var derivative = null == MasterUnit ? UnitOfMeasure.Data : MasterUnit.Derivative ?? UnitOfMeasure.Data;
			var secondDerivative = derivative.Derivative ?? UnitOfMeasure.Data;

			// Master UOMs are always units of position, so we don't need to
			// handle all possible combinations here.
			switch (SelectedActionType)
			{
				case ActionType.Absolute_Position:
					UnitOfMeasure = MasterUnit;
					_leftButtonToolTip = string.Empty;
					_rightButtonToolTip = "Move to {0} {1} relative to home.";
					break;

				case ActionType.Relative_Position:
					UnitOfMeasure = MasterUnit;
					_leftButtonToolTip = "Move backward by {0} {1}.";
					_rightButtonToolTip = "Move forward by {0} {1}.";
					break;

				case ActionType.Velocity:
					UnitOfMeasure = derivative;
					_leftButtonToolTip = "Move backward at {0} {1}.";
					_rightButtonToolTip = "Move forward at {0} {1}.";
					break;

				default:
					throw new InvalidOperationException("Unrecognized action type.");
			}

			OnPropertyChanged(nameof(LeftButtonToolTip));
			OnPropertyChanged(nameof(RightButtonToolTip));
		}


		// Handle movement button presses.
		private void FireAction(decimal aScale) => ActionInvoked?.Invoke(this, aScale * Quantity, UnitOfMeasure);

		#endregion

		#region -- Data --

		private ActionType _selectedActionType = ActionType.Relative_Position;
		private UnitOfMeasure _masterUnit = UnitOfMeasure.Data;
		private UnitOfMeasure _unitOfMeasure = UnitOfMeasure.Data;
		private decimal _quantity;
		private string _errorMsg = string.Empty;
		private bool _isRotaryDevice;

		private string _rightButtonToolTip = string.Empty;
		private string _leftButtonToolTip = string.Empty;

		#endregion
	}
}

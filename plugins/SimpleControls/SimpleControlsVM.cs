﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Windows.Input;
using log4net;
using SimpleControlsPlugin.Settings;
using Zaber;
using Zaber.PlugIns;
using Zaber.Telemetry;
using Zaber.Threading;
using ZaberWpfToolbox;

namespace SimpleControlsPlugin
{
	/// <summary>
	///     Top-level model for the simple controls plugins. Creates the axis control groups
	///     and defined the behaviors for all buttons.
	/// </summary>
	[PlugIn(Name = "Simple", Description = "Basic axis movement controls.")]
	public class SimpleControlsVM : ObservableObject
	{
		#region -- Initialization --

		/// <summary>
		///     Default constructor. Creates the default set of controls.
		/// </summary>
		public SimpleControlsVM()
		{
			CreateDefaultControls();
		}

		#endregion

		#region -- Public helper methods --

		/// <summary>
		///     Try a user-defined communication function and convert all of the common communication
		///     error conditions into an error message that the caller will handle. Other exceptions
		///     are not handled and will still propagate upward.
		/// </summary>
		/// <param name="aSendCommandAction">Function to send a command.</param>
		/// <param name="aReportErrorAction">Function to handle an error message from a handled exception.</param>
		public static void TryCommandAndHandleCommunicationErrors(Action aSendCommandAction, Action<string> aReportErrorAction)
		{
			try
			{
				aSendCommandAction();
			}
			catch (ErrorResponseException aErrorException)
			{
				aReportErrorAction("The last command resulted in a device error: " + aErrorException.Response.FormatResponse());
			}
			catch (RequestCollectionException aCollectionException)
			{
				aReportErrorAction("The last command resulted in device error(s): " + aCollectionException.Message);
			}
			catch (RequestReplacedException)
			{
				aReportErrorAction("The last command sent by this control was pre-empted by another command before completion.");
			}
			catch (ConversationException aConvException)
			{
				aReportErrorAction("The last command resulted in an error: " + aConvException.Message);
			}
			catch (TimeoutException aTimeoutException)
			{
				aReportErrorAction("The command timed out: " + aTimeoutException.Message);
			}
		}

		#endregion

		#region -- Plugin API --

		/// <summary>
		///     Reference to the Plugin Manager. Used to detect user settings reset events.
		/// </summary>
		[PlugInProperty]
		public IPlugInManager PluginManager
		{
			get => _pluginManager;
			set
			{
				if (null != _pluginManager)
				{
					_pluginManager.SettingsResetEvent -= OnSettingsReset;
				}

				_pluginManager = value;

				if (null != _pluginManager)
				{
					_pluginManager.SettingsResetEvent += OnSettingsReset;
				}
			}
		}


		/// <summary>
		///     Port accessor. This property is driven by the Plugin Manager in Zaber Console.
		/// </summary>
		[PlugInProperty]
		public ZaberPortFacade PortFacade
		{
			get => _portFacade;
			set
			{
				if (null != _portFacade)
				{
					_portFacade.Closed -= Port_OpenOrClosed;
					_portFacade.Opened -= Port_OpenOrClosed;
				}

				_portFacade = value;

				if (null != _portFacade)
				{
					_portFacade.Closed += Port_OpenOrClosed;
					_portFacade.Opened += Port_OpenOrClosed;
				}
			}
		}


		/// <summary>
		///     The currently selected device conversation in the Device List. This property
		///     is driven by the Plugin Manager.
		/// </summary>
		[PlugInProperty]
		public Conversation Conversation
		{
			get => _conversation;
			set
			{
				// Save settings for the previously selected device.
				if (null != _conversation)
				{
					SaveSettings(_conversation.Device.GetIdentity());
				}

				_conversation = value;

				_dispatcher.BeginInvoke(() =>
				{
					HomeErrorMessage = null;
					StopErrorMessage = null;
					foreach (var group in AxisControls)
					{
						group.ErrorMessage = null;
					}
				});

				RestoreSettings();
			}
		}


		/// <summary>
		///     Selected unit of measure for the currently selected device, if applicable.
		///     This property is driven by the Plugin Manager.
		/// </summary>
		[PlugInProperty]
		public UnitOfMeasure MasterUnitOfMeasure
		{
			set => _dispatcher.BeginInvoke(() =>
			{
				foreach (var vm in AxisControls)
				{
					vm.MasterUnit = value;
				}
			});
		}


		/// <summary>
		///     Saves user settings on exit.
		/// </summary>
		[PlugInMethod(PlugInMethodAttribute.EventType.PluginDeactivated)]
		public void OnClosing() => Properties.Settings.Default.Save();

		#endregion

		#region -- View-bound properties --

		/// <summary>
		///     Convenience property to determine if the port is currently open.
		/// </summary>
		public bool IsPortOpen => PortFacade?.Port?.IsOpen ?? false;


		/// <summary>
		///     Convenience property for determining whether movement commands will
		///     have any effect.
		/// </summary>
		public bool IsMotionCapable
			=> ((PortFacade?.Conversations.Count ?? 0) > 1) &&
			   (Conversation?.Device.DeviceType.IsMotionCapable ?? false);


		/// <summary>
		///     Collection of all visible control groups.
		/// </summary>
		public ObservableCollection<AxisPropertyControlVM> AxisControls
		{
			get => _axisControls;
			set => Set(ref _axisControls, value, nameof(AxisControls));
		}


		/// <summary>
		///     Command invoked by the home button.
		/// </summary>
		public ICommand HomeCommand => OnDemandRelayCommand(ref _homeCommand, _ =>
		{
			if (null != Conversation && IsPortOpen)
			{
				_eventLog.LogEvent("Home button used");

				Func<DeviceMessage> sendCommand = () =>
				{
					if (PortFacade.Port.IsAsciiMode)
					{
						return Conversation.Request("home");
					}
					else
					{
						return Conversation.Request(Command.Home);
					}
				};

				HomeErrorMessage = null;
				RunDeviceCommandAsync(sendCommand, msg => HomeErrorMessage = msg);
			}
		});

		/// <summary>
		///		Error messages associated with the Home button.
		/// </summary>
		public string HomeErrorMessage
		{
			get => _homeErrorMessage;
			set => Set(ref _homeErrorMessage, value, nameof(HomeErrorMessage));
		}


		/// <summary>
		///     Command invoked by the stop button.
		/// </summary>
		public ICommand StopCommand => OnDemandRelayCommand(ref _stopCommand, _ =>
		{
			if (null != Conversation && IsPortOpen)
			{
				_eventLog.LogEvent("Stop button used");

				Func<DeviceMessage> sendCommand = () =>
				{
					if (PortFacade.Port.IsAsciiMode)
					{
						return Conversation.Request("stop");
					}
					else
					{
						return Conversation.Request(Command.Stop);
					}
				};

				StopErrorMessage = null;
				RunDeviceCommandAsync(sendCommand, msg => StopErrorMessage = msg);
			}
		});


		/// <summary>
		///     Error messages associated with the Stop button.
		/// </summary>
		public string StopErrorMessage
		{
			get => _stopErrorMessage;
			set => Set(ref _stopErrorMessage, value, nameof(StopErrorMessage));
		}

		#endregion

		#region -- Event handling --

		private void OnSettingsReset(object aSender, EventArgs aArgs)
		{
			var settings = Properties.Settings.Default;
			settings.Reset();
			settings.ControlSettings = new AxisSettingsCollection();
			RestoreSettings();
		}


		// Called when the port open state changes.
		private void Port_OpenOrClosed(object sender, EventArgs e)
		{
			_unitCache.Clear();
			_dispatcher.BeginInvoke(() => { OnPropertyChanged(nameof(IsPortOpen)); });
		}


		// Called when the user clicks one of the movement buttons in a control group.
		private void Axis_ActionInvoked(object aSender, decimal aValue, UnitOfMeasure aUnits)
		{
			var vm = aSender as AxisPropertyControlVM;
			if (Conversation is null || vm is null)
			{
				return;
			}

			DeviceMessage response = null;

			// Delegate for sending either an ASCII or binary command.
			Func<DeviceMessage> sendCommand = null;

			// Determine what command to send.
			if (PortFacade.Port.IsAsciiMode)
			{
				var command = "move ";
				switch (vm.SelectedActionType)
				{
					case ActionType.Absolute_Position:
						command += "abs";
						break;

					case ActionType.Relative_Position:
						command += "rel";
						break;

					case ActionType.Velocity:
						command += "vel";
						break;

					default:
						throw new InvalidOperationException("Unrecognized action type.");
				}

				sendCommand = () =>
				{
					var cmdForLog = $"{command} {aValue}{aUnits?.Abbreviation ?? string.Empty}";
					_log.Info("Sending: " + cmdForLog);
					response = Conversation.RequestInUnits(command, aValue, aUnits);
					_eventLog.LogEvent("Move button used", cmdForLog);
					return response;
				};
			}
			else
			{
				var command = Command.MoveRelative;
				switch (vm.SelectedActionType)
				{
					case ActionType.Absolute_Position:
						command = Command.MoveAbsolute;
						break;

					case ActionType.Relative_Position:
						break;

					case ActionType.Velocity:
						command = Command.MoveAtConstantSpeed;
						break;

					default:
						throw new InvalidOperationException("Unrecognized action type.");
				}

				sendCommand = () =>
				{
					var cmdForLog = $"{command} {aValue}{aUnits?.Abbreviation ?? string.Empty}";
					_log.Info("Sending: " + cmdForLog);
					response = Conversation.RequestInUnits(command, aValue, aUnits);
					_eventLog.LogEvent("Move button used", cmdForLog);
					return response;
				};
			}

			RunDeviceCommandAsync(sendCommand, msg => ReportAsyncCommandError(msg, vm));
		}


		private void RunDeviceCommandAsync(Func<DeviceMessage> aSendAction, Action<string> aErrorAction)
		{
			// Create a background thread to send the command in order to avoid locking the UI.
			var worker = BackgroundWorkerManager.CreateWorker();
			worker.WorkerSupportsCancellation = true;
			worker.DoWork += (aLocalSender, aArgs) =>
			{
				var errorMessage = string.Empty;

				// Abort if already cancelled.
				if (worker.CancellationPending)
				{
					return;
				}

				TryCommandAndHandleCommunicationErrors(
					() =>
					{
						var response = aSendAction();

						if (null != response && response.IsError)
						{
							errorMessage = "The last command resulted in a device error: " + response.FormatResponse();
						}
					},
					aMessage =>
					{
						errorMessage = aMessage;
					});

				// Don't update UI if cancelled.
				if (!worker.CancellationPending)
				{
					aErrorAction(errorMessage);
				}

				// If this worker instance is still the latest one, clear the saved reference to prevent cancellation after completion.
				lock (_workerMutex)
				{
					if (_lastCommandWorker == worker)
					{
						_lastCommandWorker = null;
					}
				}
			};

			lock (_workerMutex)
			{
				// Cancel the previous worker if it's still around.
				if (null != _lastCommandWorker)
				{
					_lastCommandWorker.CancelAsync();
					_log.Warn("Cancelled a previous command worker.");
				}

				// Save a reference to the new worker so we can cancel it later if needed.
				_lastCommandWorker = worker;
			}

			// Send the new command to the device.
			worker.Run();
		}


		private void ReportAsyncCommandError(string aErrorMessage, AxisPropertyControlVM vm)
		{
			_dispatcher.BeginInvoke(() =>
			{
				vm.ErrorMessage = aErrorMessage;

				if (!string.IsNullOrEmpty(aErrorMessage))
				{
					_log.Info(aErrorMessage);
				}
			});
		}

		#endregion

		#region -- Helpers --

		private void SaveSettings(DeviceIdentity aKey)
		{
			if (null == aKey)
			{
				return;
			}

			var settings = Properties.Settings.Default.ControlSettings.FindOrCreateSettings(aKey);
			settings.ControlGroups.Clear();
			foreach (var group in _axisControls)
			{
				// Save current control values in the user settings.
				var groupSettings = new ControlGroupSettings
				{
					ActionType = group.SelectedActionType,
					Quantity = group.Quantity,
					Units = group.MasterUnit.Abbreviation
				};
				settings.ControlGroups.Add(groupSettings);
			}
		}


		private void LoadSettings(DeviceIdentity aKey)
		{
			if (null == aKey)
			{
				return;
			}

			if (null == Properties.Settings.Default.ControlSettings)
			{
				Properties.Settings.Default.ControlSettings = new AxisSettingsCollection();
			}

			ObservableCollection<AxisPropertyControlVM> newControls = null;

			var settings = Properties.Settings.Default.ControlSettings.FindSettings(aKey);
			if (null != settings)
			{
				newControls = new ObservableCollection<AxisPropertyControlVM>();

				for (var i = 0; i < settings.ControlGroups.Count; ++i)
				{
					var groupSettings = settings.ControlGroups[i];

					// Add new control groups as necesary to match the settings.
					while (i >= newControls.Count)
					{
						newControls.Add(new AxisPropertyControlVM());
					}

					// Transfer the saved settings to the controls.
					var controls = newControls[i];
					controls.SelectedActionType = groupSettings.ActionType;
					controls.Quantity = groupSettings.Quantity;
					controls.MasterUnit = UnitOfMeasure.FindByAbbreviation(groupSettings.Units) ?? UnitOfMeasure.Data;
					controls.ActionInvoked += Axis_ActionInvoked;
				}
			}

			// This function can be called on random threads, so update the property
			// immediately but defer the UI refresh to the UI thread.
			if (null != newControls)
			{
				_axisControls = newControls;
				if (_axisControls.Count < 1)
				{
					CreateDefaultControls();
				}

				_dispatcher.BeginInvoke(() => { OnPropertyChanged(nameof(AxisControls)); });
			}

			// Constrain the units to match what is available on the selected device.
			foreach (var controls in _axisControls)
			{
				ICollection<UnitOfMeasure> applicableUnits = new List<UnitOfMeasure>();
				if (null != Conversation)
				{
					applicableUnits = GetUnits(Conversation.Device,
											   controls.SelectedActionType == ActionType.Velocity
												   ? MeasurementType.Velocity
												   : MeasurementType.Position);
				}

				if (!applicableUnits.Contains(controls.UnitOfMeasure))
				{
					controls.UnitOfMeasure = UnitOfMeasure.Data;
				}
			}
		}


		private void RestoreSettings()
		{
			// Restore settings for the newly selected device.
			if (null != Conversation)
			{
				var dev = Conversation.Device;
				LoadSettings(dev.GetIdentity());

				// Change minimum movement clamping for rotary stage absolute positions.
				foreach (var group in AxisControls)
				{
					group.IsRotaryDevice = dev.DeviceType.IsRotaryDevice;
				}
			}

			// Update UI state to reflect new selection.
			_dispatcher.BeginInvoke(() => { OnPropertyChanged(nameof(IsMotionCapable)); });
		}


		private void CreateDefaultControls()
		{
			_axisControls.Add(new AxisPropertyControlVM
			{
				SelectedActionType = ActionType.Absolute_Position,
				Quantity = 50m,
				MasterUnit = UnitOfMeasure.Millimeter
			});

			_axisControls.Add(new AxisPropertyControlVM
			{
				SelectedActionType = ActionType.Relative_Position,
				Quantity = 10m,
				MasterUnit = UnitOfMeasure.Millimeter
			});

			_axisControls.Add(new AxisPropertyControlVM
			{
				SelectedActionType = ActionType.Velocity,
				Quantity = 10m,
				MasterUnit = UnitOfMeasure.Millimeter
			});

			foreach (var vm in _axisControls)
			{
				vm.ActionInvoked += Axis_ActionInvoked;
			}
		}


		private ICollection<UnitOfMeasure> GetUnits(ZaberDevice aDevice, MeasurementType aType)
		{
			var identityDevice = aDevice;
			if (1 == aDevice.Axes.Count && 0 == aDevice.AxisNumber)
			{
				// Device with single axis, get UoM from axis
				identityDevice = aDevice.Axes[0];
			}

			// Cache units because lookup can be expensive.
			var key = new Tuple<DeviceIdentity, MeasurementType>(aDevice.GetIdentity(), aType);
			if (!_unitCache.TryGetValue(key, out var units))
			{
				units = identityDevice.GetUnitsOfMeasure(aType);
				_unitCache[key] = units;
			}

			return units;
		}

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Simple tab");

		private IPlugInManager _pluginManager;
		private readonly IDispatcher _dispatcher = DispatcherHelper.Dispatcher;
		private IBackgroundWorker _lastCommandWorker;

		private readonly object _workerMutex = new object();
		private ZaberPortFacade _portFacade;
		private Conversation _conversation;
		private RelayCommand _homeCommand;
		private RelayCommand _stopCommand;

		private string _homeErrorMessage;
		private string _stopErrorMessage;

		private ObservableCollection<AxisPropertyControlVM> _axisControls =
			new ObservableCollection<AxisPropertyControlVM>();

		private readonly Dictionary<Tuple<DeviceIdentity, MeasurementType>, ICollection<UnitOfMeasure>> _unitCache =
			new Dictionary<Tuple<DeviceIdentity, MeasurementType>, ICollection<UnitOfMeasure>>();

		#endregion
	}
}

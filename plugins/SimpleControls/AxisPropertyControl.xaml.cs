﻿using ZaberWpfToolbox;

namespace SimpleControlsPlugin
{
	/// <summary>
	///     Interaction logic for AxisPropertyControl.xaml
	/// </summary>
	public partial class AxisPropertyControl : BaseControl
	{
		/// <summary>
		///     Default constructor. Initializes component.
		/// </summary>
		public AxisPropertyControl()
		{
			InitializeComponent();
		}
	}
}

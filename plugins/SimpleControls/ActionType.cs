﻿using System.ComponentModel;

namespace SimpleControlsPlugin
{
	/// <summary>
	///     Different motion types that can be triggered. This is separate from the MeasurementType
	///     enum because we want to differentiate between relative and absolute position.
	/// </summary>
	public enum ActionType
	{
		/// <summary>
		///     Move to absolute position.
		/// </summary>
		[Description("Move to Absolute Position")]
		Absolute_Position,

		/// <summary>
		///     Move some amount relative to current position.
		/// </summary>
		[Description("Move by Relative Distance")]
		Relative_Position,

		/// <summary>
		///     Move at a specified velocity.
		/// </summary>
		[Description("Move at Velocity")]
		Velocity
	}
}

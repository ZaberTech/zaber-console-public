﻿using ZaberWpfToolbox;

namespace SimpleControlsPlugin
{
	/// <summary>
	///     Interaction logic for SimpleControls.xaml
	/// </summary>
	public partial class SimpleControls : BaseControl
	{
		/// <summary>
		///     Default constructor. Initializes the component.
		/// </summary>
		public SimpleControls()
		{
			InitializeComponent();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using Zaber.Application;

namespace SimpleControlsPlugin.Settings
{
	/// <summary>
	///     Helper class for saving all the settings for one axis across sessions.
	/// </summary>
	[Serializable]
	public class AxisSettings : PerDeviceSettings
	{
		/// <summary>
		///     List of control groups and their settings.
		/// </summary>
		public List<ControlGroupSettings> ControlGroups { get; set; } = new List<ControlGroupSettings>();
	}
}

﻿using System;
using Zaber.Application;

namespace SimpleControlsPlugin.Settings
{
	/// <summary>
	///     Managed saving per-axis control settings across Zaber Console sessions.
	/// </summary>
	[Serializable]
	public class AxisSettingsCollection : PerDeviceSettingsCollection<AxisSettings>
	{
	}
}

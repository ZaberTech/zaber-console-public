﻿using System;
using Zaber;

namespace SimpleControlsPlugin.Settings
{
	/// <summary>
	///     Helper for saving user-entered movement modes and values per device across sessions.
	///     This represents the settings for one group of controls.
	/// </summary>
	[Serializable]
	public class ControlGroupSettings
	{
		/// <summary>
		///     The type of motion for this group's controls.
		/// </summary>
		public ActionType ActionType { get; set; }


		/// <summary>
		///     The distance or velocity to move.
		/// </summary>
		public decimal Quantity { get; set; }


		/// <summary>
		///     The abbreviation of the last selected unit of measure.
		/// </summary>
		public string Units { get; set; } = UnitOfMeasure.Data.Abbreviation;
	}
}

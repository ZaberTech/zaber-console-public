﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Zaber;
using Zaber.Tuning.Servo;
using Measurement = Zaber.Units.Measurement;

namespace ZaberConsole.Plugins.ServoTuner
{
	/// <summary>
	///     Helper class for interacting with servo devices.
	/// </summary>
	public static class ServoDeviceHelper
	{
		/// <summary>
		///     Enumerate all servo tuning modes supported by the device, and the parameters of
		///     each mode.
		/// </summary>
		/// <param name="aConversation">The device conversation to use. Must be a single device.</param>
		/// <returns>
		///     A dictionary in which the keys are tuning mode names and the values are arrays of
		///     setting names, representing settings related to control loop tuning.
		/// </returns>
		/// <remarks>
		///     This function only works with firmware 7 and up. The only tunable firmware
		///     6 devices were early models of the X-DMQ, which are not supported for tuning in
		///     Zaber Console.
		/// </remarks>
		public static IDictionary<string, string[]> ReadDeviceTuningModesAndParameters(Conversation aConversation)
		{
			var result = new Dictionary<string, string[]>();

			foreach (var modeName in ReadDeviceTuningModes(aConversation))
			{
				var settings = ReadTuningModeParameterNames(aConversation, modeName);
				result[modeName] = settings.ToArray();
			}

			return result;
		}


		/// <summary>
		///     Obtain a list of all presets available on the device.
		/// </summary>
		/// <param name="aConversation">The device to ask.</param>
		/// <returns>Zero or more preset names, not including the live set.</returns>
		public static IEnumerable<string> ReadDevicePresetNames(Conversation aConversation)
		{
			var timer = new TimeoutTimer { Timeout = 500 };

			foreach (var body in DeviceListener.ReceiveInfoListContent(aConversation,
																	   "servo print paramsets",
																	   timer))
			{
				var parts = body.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
				if ("paramset" == parts[0])
				{
					yield return parts[1];
				}
			}
		}


		/// <summary>
		///     Obtain a list of all servo tuning modes (PID, etc) supported by a device.
		/// </summary>
		/// <param name="aConversation">The device to ask.</param>
		/// <returns>Zero or more tuning modes.</returns>
		public static IEnumerable<string> ReadDeviceTuningModes(Conversation aConversation)
		{
			var timer = new TimeoutTimer { Timeout = 500 };

			foreach (var body in DeviceListener.ReceiveInfoListContent(aConversation, 
																		"servo print types", 
																		timer))
			{
				var parts = body.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

				// The output format starts with the mode name, then the setting name.
				if ("type" == parts[0])
				{
					yield return parts[1];
				}
			}
		}


		/// <summary>
		///     Ask a device for the names of all settings relevant to a tuning mode.
		/// </summary>
		/// <param name="aConversation">The device conversation to use.</param>
		/// <param name="aModeName">The name of the tuning mode to ask about.</param>
		/// <returns>Zero or more setting names.</returns>
		public static IEnumerable<string> ReadTuningModeParameterNames(Conversation aConversation, string aModeName)
		{
			var timer = new TimeoutTimer { Timeout = 500 };

			foreach (var body in DeviceListener.ReceiveInfoListContent(aConversation,
																	   "servo print params " + aModeName,
																	   timer))
			{
				var parts = body.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

				// The output format starts with the mode name, then the setting name.
				if (parts[0] == aModeName)
				{
					yield return parts[1];
				}
			}
		}


		/// <summary>
		///     Create a parameter set with all of the parameter names given, and with each
		///     parameter having a zeroed, unitless measurement.
		/// </summary>
		/// <param name="aModeName">Tuning mode name for the set.</param>
		/// <param name="aNameList">List of parameter names to add.</param>
		/// <returns>A new parameter set.</returns>
		public static ITunerParameterSet CreateEmptyParams(string aModeName, params string[] aNameList)
		{
			var result = new TunerParameterSet(aModeName);

			foreach (var name in aNameList)
			{
				result[name] = new Measurement(0.0, null);
			}

			return result;
		}


		/// <summary>
		///     Read the actual values of the current tuning parameters from the device.
		/// </summary>
		/// <param name="aConversation">The device conversation to use.</param>
		/// <param name="aPresetName">Name of the preset to read from.</param>
		/// <param name="aParamNameTable">
		///     Optional table of cached setting names for each tuning mode name.
		///     If not provided, the device will be queried for the list of settings, which is a bit slower.
		/// </param>
		/// <returns>
		///     A tuning parameter set populated with the current tuning mode name, tuning
		///     setting names and current values from the device.
		/// </returns>
		/// <remarks>This method does not trap device or port error exceptions; caller is responsible for exception handling.</remarks>
		public static ITunerParameterSet ReadDeviceParameters(Conversation aConversation, string aPresetName,
															  IDictionary<string, string[]> aParamNameTable = null)
		{
			if (null == aConversation)
			{
				throw new ArgumentNullException(nameof(aConversation) + " must not be null.");
			}

			if (string.IsNullOrEmpty(aPresetName))
			{
				throw new ArgumentNullException(nameof(aPresetName) + " must not be null or empty.");
			}

			// Get the current control mode of the device.
			var cmd = aConversation.Device.FormatAsciiCommand("servo {0} get type", aPresetName);
			var response = aConversation.Request(cmd);
			var modeName = response.TextData;

			// Get the names of the parameters to read.
			string[] paramNames = null;
			if ((aParamNameTable != null) && aParamNameTable.ContainsKey(modeName))
			{
				paramNames = aParamNameTable[modeName];
			}
			else
			{
				paramNames = ReadTuningModeParameterNames(aConversation, modeName).ToArray();
			}

			var result = new TunerParameterSet(modeName);

			foreach (var name in paramNames)
			{
				cmd = aConversation.Device.FormatAsciiCommand("servo {0} get {1}", aPresetName, name);
				response = aConversation.Request(cmd);
				result[name] = new Measurement(response.NumericData, null);
			}

			return result;
		}


		/// <summary>
		///     Write tuning values to the device, changing its tuning mode if necessary.
		/// </summary>
		/// <param name="aConversation">The device conversation to use.</param>
		/// <param name="aParams">
		///     The parameter set to write. This must have its mode name set to
		///     a tuning mode name supported by the device, and all parameters contained in the set must
		///     be supported by the device and already be expressed in the device's native units (no
		///     unit conversion is done here).
		/// </param>
		/// <param name="aPresetName">Name of the parameter set to store to.</param>
		/// <remarks>
		///     Early versions firmware 7 do not support the "servo set type" command, so attempting
		///     to write a parameter set with a mode name other than the device's current mode will cause an error.
		///     Caller is responsible for handling exceptions.
		/// </remarks>
		public static void WriteDeviceParameters(Conversation aConversation, ITunerParameterSet aParams,
												 string aPresetName)
		{
			// Set the tuning mode. if necessary.
			var cmd = aConversation.Device.FormatAsciiCommand("servo {0} get type", aPresetName);
			var response = aConversation.Request(cmd);
			if (aParams.ModeName != response.TextData)
			{
				// NOTE this command is not supported in early versions of firmware 7.
				cmd = aConversation.Device.FormatAsciiCommand("servo {0} set type {1}",
															  aPresetName,
															  aParams.ModeName.ToLowerInvariant());
				aConversation.Request(cmd);
			}

			// Transfer the tuning parameters.
			foreach (var paramName in aParams.ParameterNames)
			{
				var val = aParams[paramName].Value.ToString("N9", CultureInfo.InvariantCulture);
				cmd = aConversation.Device.FormatAsciiCommand("servo {0} set {1} {2}",
															  aPresetName,
															  paramName,
															  val);
				aConversation.Request(cmd);
			}
		}
	}
}

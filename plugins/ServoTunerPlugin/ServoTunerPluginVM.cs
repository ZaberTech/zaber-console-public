﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Input;
using log4net;
using MvvmDialogs.ViewModels;
using Zaber;
using Zaber.PlugIns;
using Zaber.Telemetry;
using Zaber.Threading;
using Zaber.Tuning.Servo;
using Zaber.Units;
using ZaberConsole.Plugins.ServoTuner.Methods;
using ZaberConsole.Plugins.ServoTuner.Properties;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Dialogs;
using ZaberWpfToolbox.Resources;
using ZaberWpfToolbox.Settings;

namespace ZaberConsole.Plugins.ServoTuner
{
	using CacheKey = Tuple<byte, int>;
	using Measurement = Zaber.Units.Measurement;

	/// <summary>
	///     Top-level view for this plugin. Implements the Zaber Console plugin interface
	///     and contains all functional pieces relevant to the servo tuning plugin.
	/// </summary>
	[PlugIn(Name = "Servo Tuning", Description = "Servo feedback tuning helper.")]
	public class ServoTunerPluginVM : ObservableObject, IDialogClient
	{
		#region -- Initialization --

		/// <summary>
		///     Initializes a new instance.
		/// </summary>
		public ServoTunerPluginVM()
		{
			_dispatcher = DispatcherHelper.Dispatcher;
			_deviceScanner = BackgroundWorkerManager.CreateWorker();
			_deviceScanner.WorkerSupportsCancellation = true;
			_deviceScanner.DoWork += DeviceScanner_DoWork;
			_deviceScanner.RunWorkerCompleted += DeviceScanner_OnCompleted;
			CreateTuningMethods();
			LoadSettings();
		}

		#endregion

		#region -- Plugin interface --

		/// <summary>
		///     Reference to the Plugin Manager. Used to detect user settings reset events.
		/// </summary>
		[PlugInProperty]
		public IPlugInManager PluginManager
		{
			get => _pluginManager;
			set
			{
				if (null != _pluginManager)
				{
					_pluginManager.SettingsResetEvent -= OnSettingsReset;
				}

				_pluginManager = value;

				if (null != _pluginManager)
				{
					_pluginManager.SettingsResetEvent += OnSettingsReset;
				}
			}
		}


		/// <summary>
		///     Port accessor. This property is driven by the Plugin Manager in Zaber Console.
		/// </summary>
		[PlugInProperty]
		public ZaberPortFacade PortFacade
		{
			get => _portFacade;
			set
			{
				if (null != _portFacade)
				{
					_portFacade.Closed -= Port_OpenOrClosed;
					_portFacade.Opened -= Port_OpenOrClosed;
				}

				_portFacade = value;

				if (null != _portFacade)
				{
					_portFacade.Closed += Port_OpenOrClosed;
					_portFacade.Opened += Port_OpenOrClosed;
				}
			}
		}


		/// <summary>
		///     The currently selected device conversation in the Device List. This property
		///     is driven by the Plugin Manager.
		/// </summary>
		[PlugInProperty]
		public Conversation Conversation
		{
			get => _conversation;
			set
			{
				if (null != _conversation)
				{
					_conversation.Device.MessageReceived -= Device_MessageReceived;
				}

				SaveSettings();
				_conversation = value;
				LoadSettings();

				if (null != _conversation)
				{
					_conversation.Device.MessageReceived += Device_MessageReceived;
				}

				if (_isVisible)
				{
					ScanDevice();
				}
			}
		}


		/// <summary>
		///     Saves user settings on exit.
		/// </summary>
		[PlugInMethod(PlugInMethodAttribute.EventType.PluginDeactivated)]
		public void OnClosing()
		{
			foreach (var tuner in TunerTypes)
			{
				tuner.OnClosing();
			}

			SaveSettings();
		}


		[PlugInMethod(PlugInMethodAttribute.EventType.PluginShown)]
		public void OnShown()
		{
			_isVisible = true;

			ScanDevice();
		}


		[PlugInMethod(PlugInMethodAttribute.EventType.PluginHidden)]
		public void OnHidden() => _isVisible = false;

		#endregion

		#region -- IDialogClient implementation --

		/// <summary>
		///     Event to invoke in order to open a dialog box.
		/// </summary>
		public event RequestDialogChange RequestDialogOpen;

		/// <summary>
		///     Event to invoke in order to close an existing dialog box.
		/// </summary>
		public event RequestDialogChange RequestDialogClose;


		// Bubble up dialog events from child controls.
		private void Client_RequestDialogOpen(object aSender, IDialogViewModel aVM)
			=> RequestDialogOpen?.Invoke(aSender, aVM);


		// Bubble up dialog events from child controls.
		private void Client_RequestDialogClose(object aSender, IDialogViewModel aVM)
			=> RequestDialogClose?.Invoke(aSender, aVM);

		#endregion

		#region -- View data --

		/// <summary>
		///     Convenience property to determine if the port is currently open.
		/// </summary>
		public bool IsPortOpen => PortFacade?.Port?.IsOpen ?? false;


		/// <summary>
		///     Convenience property to determine if the device is tunable.
		/// </summary>
		public bool IsDeviceTunable => ((_tuningModes?.Count ?? 0) > 0) && (null != _deviceProperties);


		/// <summary>
		///     Warning message displayed when the device is not tunable.
		/// </summary>
		public string WarningMessage
		{
			get => _warningMessage;
			set => Set(ref _warningMessage, value, nameof(WarningMessage));
		}


		/// <summary>
		///     Whether the read button is enabled. If the value is false locally it overrides any value set by the tabs.
		/// </summary>
		public bool CanRead
		{
			get => _canRead && (SelectedTunerType?.CanRead ?? false);
			set => Set(ref _canRead, value, nameof(CanRead));
		}


		/// <summary>
		///     Whether the write button is enabled. If the value is false locally it overrides any value set by the tabs.
		/// </summary>
		public bool CanWrite
		{
			get => _canWrite && (SelectedTunerType?.CanWrite ?? false);
			set => Set(ref _canWrite, value, nameof(CanWrite));
		}


		/// <summary>
		///     Whether the write default button is enabled.
		/// </summary>
		public bool CanWriteDefault => IsDeviceTunable && (null != _factoryPresets) && _factoryPresets.Any();


		/// <summary>
		///     Tooltip message for the read button. If the value is set locally it overrides any value set by the tabs.
		/// </summary>
		public string ReadButtonTip
		{
			get => _readButtonTip ?? SelectedTunerType?.ReadButtonTip ?? Resources.STR_DEFAULT_READ_TIP;
			set => Set(ref _readButtonTip, value, nameof(ReadButtonTip));
		}


		/// <summary>
		///     ToolTip for the write button. If the value is set locally it overrides any value set by the tabs.
		/// </summary>
		public string WriteButtonTip
		{
			get => _writeButtonTip ?? SelectedTunerType?.WriteButtonTip ?? Resources.STR_DEFAULT_WRITE_TIP;
			set => Set(ref _writeButtonTip, value, nameof(WriteButtonTip));
		}


		/// <summary>
		///     List of available tuner types for the selected device.
		/// </summary>
		public ObservableCollection<CalculatorMethodBase> TunerTypes
		{
			get => _tunerTypes;
			set => Set(ref _tunerTypes, value, nameof(TunerTypes));
		}


		/// <summary>
		///     Currently selected tuner type.
		/// </summary>
		public CalculatorMethodBase SelectedTunerType
		{
			get => _selectedTunerType;
			set
			{
				var different = !ReferenceEquals(value, _selectedTunerType);
				var previousTuner = _selectedTunerType;

				if (null != previousTuner)
				{
					previousTuner.PropertyChanged -= SelectedTuner_OnPropertyChanged;
				}

				Set(ref _selectedTunerType, value, nameof(SelectedTunerType));

				if (null != _selectedTunerType)
				{
					_selectedTunerType.PropertyChanged += SelectedTuner_OnPropertyChanged;
				}

				if (different)
				{
					// Disconnect the previously selected tuner from the device cleanly.
					previousTuner?.SetTargetDevice(null, null);

					// Connect the newly selected tuner to the device.
					_selectedTunerType?.SetTargetDevice(Conversation, _deviceProperties);

					OnPropertyChanged(nameof(CanRead));
					OnPropertyChanged(nameof(CanWrite));
					OnPropertyChanged(nameof(ReadButtonTip));
					OnPropertyChanged(nameof(WriteButtonTip));
				}
			}
		}


		/// <summary>
		///     Presets that the user can read into the editing area or write from there, if writable.
		/// </summary>
		public IEnumerable<PresetInfo> EditablePresets
		{
			get
			{
				yield return _livePreset;

				foreach (var preset in ActivatablePresets)
				{
					yield return preset;
				}
			}
		}


		/// <summary>
		///     Presets that can be loaded into the live set.
		/// </summary>
		public IEnumerable<PresetInfo> ActivatablePresets { get; private set; } = new PresetInfo[0];


		/// <summary>
		///     Presets that can be made the power-on default.
		/// </summary>
		public IEnumerable<PresetInfo> DefaultablePresets => ActivatablePresets;


		/// <summary>
		///     Currently selected preset for editing.
		/// </summary>
		public PresetInfo SelectedEditablePreset
		{
			get => _selectedEditablePreset;
			set
			{
				Set(ref _selectedEditablePreset, value, nameof(SelectedEditablePreset));
				UpdateCanWrite();
			}
		}


		/// <summary>
		///     Currently selected preset for changing the active set.
		/// </summary>
		public PresetInfo SelectedActivatablePreset
		{
			get => _selectedActiveatablePreset;
			set => Set(ref _selectedActiveatablePreset, value, nameof(SelectedActivatablePreset));
		}


		/// <summary>
		///     Currently selected preset for changing the default set.
		/// </summary>
		public PresetInfo SelectedDefaultablePreset
		{
			get => _selectedDefaultablePreset;
			set => Set(ref _selectedDefaultablePreset, value, nameof(SelectedDefaultablePreset));
		}


		/// <summary>
		///     The preset that is actually the default on the device.
		/// </summary>
		public PresetInfo ActualDefaultPreset
		{
			get => _actualDefaultPreset;
			set => Set(ref _actualDefaultPreset, value, nameof(ActualDefaultPreset));
		}


		/// <summary>
		///     The name of the preset that is actually live on the device, or null if the live
		///     parameter set does not match any of the presets.
		/// </summary>
		public string ActualActivePresetName
		{
			get => _actualActivePresetName;
			set => Set(ref _actualActivePresetName, value, nameof(ActualActivePresetName));
		}


		/// <summary>
		///     The lan
		/// </summary>
		public string PanicButtonLabel
		{
			get => _panicButtonLabel;
			set => Set(ref _panicButtonLabel, value, nameof(PanicButtonLabel));
		}


		/// <summary>
		///     Invoked when the button is clicked to transfer calculated parameters to the device.
		/// </summary>
		public ICommand WriteParametersCommand
			=> OnDemandRelayCommand(ref _writeParametersCommand,
									_ => OnWriteParametersClicked());

		/// <summary>
		///     Command for restoring default tuning parameter values to whichever parameter set is selected.
		/// </summary>
		public ICommand WriteDefaultsCommand
			=> OnDemandRelayCommand(ref _writeDefaultsCommand,
									_ => OnWriteDefaultsClicked());

		/// <summary>
		///     Invoked when the button is clicked to transfer parameters from the device to the calculation buffer.
		/// </summary>
		public ICommand ReadParametersCommand
			=> OnDemandRelayCommand(ref _readParametersCommand,
									_ => OnReadParametersClicked());


		public ICommand ActivatePresetCommand
			=> OnDemandRelayCommand(ref _activatePresetCommand,
									_ => OnActivatePresetClicked());

		public ICommand ChangeDefaultPresetCommand
			=> OnDemandRelayCommand(ref _changeDefaultPresetCommand,
									_ => OnChangeDefaultPresetClicked());

		public ICommand PanicButtonCommand
			=> OnDemandRelayCommand(ref _panicButtonCommand,
									_ => OnPanicButtonClicked());


		public ICommand OpenScopeCommand
			=> OnDemandRelayCommand(ref _openScopeCommand,
									_ =>
									{
										PluginManager.ShowPluginTab("Oscilloscope");
										_eventLog.LogEvent("Jumped to Oscilloscope");
									});

		#endregion

		#region -- Private functionality --

		private void OnSettingsReset(object aSender, EventArgs aArgs)
		{
			var settings = Settings.Default;
			settings.Reset();
			settings.DoNotShowAgainSettings = new DoNotShowAgainSettingsCollection();
			foreach (var method in TunerTypes)
			{
				method.ClearSettings();
			}

			LoadSettings();
			ScanDevice();
		}


		// Called when the port open state changes.
		private void Port_OpenOrClosed(object sender, EventArgs e)
		{
			_tuningModeInfoCache.Clear();
			_presetDataCache.Clear();

			_dispatcher.BeginInvoke(() => { OnPropertyChanged(nameof(IsPortOpen)); });
		}


		private void OnWriteParametersClicked()
		{
			if (IsDeviceTunable && CanWrite)
			{
				ITunerParameterSet parms = null;
				try
				{
					parms = SelectedTunerType.DeviceParameters;
				}
				catch (Exception aException)
				{
					var mbvm = new MessageBoxParams
					{
						Caption = "Conversion Error",
						Message = aException.Message,
						Icon = MessageBoxImage
						.Information,
						Buttons = MessageBoxButton.OK
					};

					RequestDialogOpen?.Invoke(this, mbvm);
				}

				if (null != parms)
				{
					var worker = BackgroundWorkerManager.CreateWorker();
					worker.DoWork += (aSender, aArgs) =>
					{
						PluginManager?.ShowStatusText(this,
													  Resources
													  .STR_WRITE_IN_PROGRESS,
													  DEFAULT_STATUS_DISPLAY_TIME);

						// If the data set is missing any parameters the device expects, fill
						// them with the default values from the device.
						SetMissingParametersToDefault(parms);

						// Remove any parameters the device doesn't expect.
						var parmsToRemove = new List<string>();
						foreach (var paramName in parms.ParameterNames)
						{
							if (!_deviceProperties.SupportedTuningModes[parms.ModeName].Contains(paramName))
							{
								parmsToRemove.Add(paramName);
							}
						}

						if (parmsToRemove.Count > 0)
						{
							_log.Info("Discarding some tuning params because the device doesn't expect them: "
								  + string.Join(", ", parmsToRemove));

							foreach (var paramName in parmsToRemove)
							{
								parms[paramName] = null;
							}
						}

						var preset = SelectedEditablePreset;
						WriteDeviceParameters(parms, preset);

						// Invalidate the cached data for ths selected preset to force a refresh.
						UpdatePresetCache(preset, null);

						// If the selected preset or the live preset was written to, update the information about which
						// preset matches the live values.
						if ((preset.DisplayName == ActualActivePresetName)
						|| string.IsNullOrEmpty(ActualActivePresetName)
						|| (preset.PresetId == _livePreset.PresetId))
						{
							PresetInfo whichPresetIsLive = null;
							var live = GetDevicePresetContents(_livePreset);

							foreach (var candidate in ActivatablePresets)
							{
								var data = GetDevicePresetContents(candidate);
								if (live.IsIdenticalTo(data))
								{
									whichPresetIsLive = candidate;
									break;
								}
							}

							_dispatcher.BeginInvoke(() =>
							{
								SelectedActivatablePreset = whichPresetIsLive;
								ActualActivePresetName = whichPresetIsLive?.DisplayName;
							});
						}

						PluginManager?.ShowStatusText(this,
													  Resources.STR_SUCCESS,
													  DEFAULT_STATUS_DISPLAY_TIME);
						var tuningMethodName = StaticResources
										   .GetDisplayName.Convert(SelectedTunerType.GetType(),
																   null,
																   null,
																   null) as string
										   ?? "<unknown>";

						_eventLog.LogEvent("Wrote parameters to device",
										   $"From {tuningMethodName} to {preset.DisplayName}");
					};

					worker.Run();
				}
			}
		}


		private void SetMissingParametersToDefault(ITunerParameterSet aParamSet)
		{
			var missing = _deviceProperties.SupportedTuningModes[aParamSet.ModeName]
										.Where(paramName => !aParamSet.ParameterNames.Contains(paramName))
										.ToList();

			if (missing.Any())
			{
				var defaultParamValues = GetDevicePresetContents(GetDefaultPreset());
				if (defaultParamValues is null)
				{
					_log.Error("Device does not have a default parameter set!");
				}

				foreach (var paramName in missing)
				{
					if (defaultParamValues?.ParameterNames.Contains(paramName) ?? false)
					{
						aParamSet[paramName] = defaultParamValues[paramName];
					}
					else
					{
						_log.Warn($"Database-specified param {paramName} did not have a default value on the device.");
						aParamSet[paramName] = new Measurement(0, null);
					}
				}
			}
		}


		private void OnWriteDefaultsClicked()
		{
			if (IsDeviceTunable && CanWrite)
			{
				var preset = SelectedEditablePreset;

				// Future expansion: If a device has multiple factory parameter sets, 
				// select the one that has the same controller mode as the preset being written to.
				var defaultPreset = GetDefaultPreset();

				PluginManager?.ShowStatusText(this,
											  Resources.STR_WRITE_IN_PROGRESS,
											  DEFAULT_STATUS_DISPLAY_TIME);

				var successText = Resources.STR_SUCCESS;

				try
				{
					Conversation.Request(Conversation
									 .Device.FormatAsciiCommand("servo {0} load {1}",
																preset.PresetId,
																defaultPreset.PresetId));

					// Invalidate the cached data for ths selected preset to force a refresh.
					UpdatePresetCache(preset, null);
				}
				catch (ErrorResponseException)
				{
					successText = Resources.STR_FAILED;

					var mbvm = new MessageBoxParams
					{
						Caption = "Device error",
						Message = "The device rejected an attempt to restore default tuning parameters.",
						Icon = MessageBoxImage.Information,
						Buttons = MessageBoxButton.OK
					};

					RequestDialogOpen?.Invoke(this, mbvm);
				}

				PluginManager?.ShowStatusText(this,
											  successText,
											  DEFAULT_STATUS_DISPLAY_TIME);
				_eventLog.LogEvent("Reset parameters to default",
								   preset.DisplayName);

				// If the active preset or the live preset was written to, update the information about which
				// preset matches the live values.
				if ((preset.DisplayName == ActualActivePresetName)
				|| string.IsNullOrEmpty(ActualActivePresetName)
				|| (preset.PresetId == _livePreset.PresetId))
				{
					PresetInfo whichPresetIsLive = null;
					var live = GetDevicePresetContents(_livePreset);

					foreach (var candidate in ActivatablePresets)
					{
						var data = GetDevicePresetContents(candidate);
						if (live.IsIdenticalTo(data))
						{
							whichPresetIsLive = candidate;
							break;
						}
					}

					_dispatcher.BeginInvoke(() =>
					{
						SelectedActivatablePreset = whichPresetIsLive;
						ActualActivePresetName = whichPresetIsLive?.DisplayName;
					});
				}
			}
		}


		private void OnReadParametersClicked()
		{
			if (IsDeviceTunable && CanRead)
			{
				var worker = BackgroundWorkerManager.CreateWorker();
				worker.DoWork += (aSender, aArgs) =>
				{
					PluginManager?.ShowStatusText(this,
												  Resources.STR_READ_IN_PROGRESS,
												  DEFAULT_STATUS_DISPLAY_TIME);

					try
					{
						var preset = SelectedEditablePreset;
						var parms = ReadDeviceParameters(preset);
						UpdatePresetCache(preset, parms);
						SelectedTunerType.DeviceParameters = parms;

						PluginManager?.ShowStatusText(this, null);
						PluginManager?.ShowStatusText(this,
													  Resources.STR_SUCCESS,
													  DEFAULT_STATUS_DISPLAY_TIME);
						var tuningMethodName = StaticResources
										   .GetDisplayName.Convert(SelectedTunerType.GetType(),
																   null,
																   null,
																   null) as string
										   ?? "<unknown>";

						_eventLog.LogEvent("Read parameters from device",
										   $"From {preset.DisplayName} to {tuningMethodName}");
					}
					catch (Exception aException)
					{
						var mbvm = new MessageBoxParams
						{
							Caption = "Conversion Error",
							Message = aException.Message,
							Icon = MessageBoxImage.Information,
							Buttons = MessageBoxButton.OK
						};

						RequestDialogOpen?.Invoke(this, mbvm);
					}
				};

				worker.Run();
			}
		}


		private void OnActivatePresetClicked()
		{
			if (IsDeviceTunable)
			{
				try
				{
					var cmd = Conversation
						  .Device.FormatAsciiCommand("servo {0} load {1}",
													 "live",
													 SelectedActivatablePreset
													 .PresetId);
					Conversation.Request(cmd);
					ActualActivePresetName = SelectedActivatablePreset.DisplayName;
					UpdatePresetCache(_livePreset,
									  GetDevicePresetContents(SelectedActivatablePreset));
					_eventLog.LogEvent("Activated preset",
									   SelectedActivatablePreset.DisplayName);
				}
				catch (Exception aException)
				{
					var mbvm = new MessageBoxParams
					{
						Caption = "Device Error",
						Message = aException.Message,
						Icon = MessageBoxImage.Information,
						Buttons = MessageBoxButton.OK
					};

					RequestDialogOpen?.Invoke(this, mbvm);
				}
			}
		}


		private void OnChangeDefaultPresetClicked()
		{
			if (IsDeviceTunable)
			{
				try
				{
					Conversation.Request(Conversation
									 .Device.FormatAsciiCommand("servo set startup {0}",
																SelectedDefaultablePreset
																.PresetId));
					ActualDefaultPreset = SelectedDefaultablePreset;
					_eventLog.LogEvent("Set startup preset",
									   SelectedDefaultablePreset.DisplayName);
				}
				catch (Exception aException)
				{
					var mbvm = new MessageBoxParams
					{
						Caption = "Device Error",
						Message = aException.Message,
						Icon = MessageBoxImage.Information,
						Buttons = MessageBoxButton.OK
					};

					RequestDialogOpen?.Invoke(this, mbvm);
				}
			}
		}


		private void OnPanicButtonClicked()
		{
			try
			{
				var response = Conversation.Request("get driver.enabled");
				Conversation.Request(0m == response.NumericData ? "driver enable" : "driver disable");

				_eventLog.LogEvent("Hit the panic button");
			}
			catch (Exception aException)
			{
				var mbvm = new MessageBoxParams
				{
					Caption = "Device Error",
					Message = aException.Message,
					Icon = MessageBoxImage.Information,
					Buttons = MessageBoxButton.OK
				};

				RequestDialogOpen?.Invoke(this, mbvm);
			}
		}


		private void SelectedTuner_OnPropertyChanged(object aSender, PropertyChangedEventArgs aArgs)
		{
			// This technique for bubbling up property changed events relies on these properties having the same names in the tab VM classes.
			if (_bubbledPropertyNames.Contains(aArgs.PropertyName))
			{
				OnPropertyChanged(aArgs.PropertyName);
			}
		}


		// Invoked when a message is received from the selected device. Used to detect other
		// tabs causing stage changes that should invalidate parts of the UI.
		private void Device_MessageReceived(object aSender, DeviceMessageEventArgs aArgs)
		{
			// Rejected messages are assumed to have had no effect on the device.
			var response = aArgs.DeviceMessage;
			var command = response?.Request;

			// Ignore messages that aren't correlated with a command - we need to know what the command was.
			if ((null == response) || (null == command))
			{
				return;
			}

			// Rejected messages are assumed to have had no effect on the device.
			if (aArgs.DeviceMessage.IsError)
			{
				return;
			}

			Action action = null;

			// Check for commands we care about.
			if (command.Body.Contains("driver enable"))
			{
				action = () => { PanicButtonLabel = Resources.STR_DISABLE_DRIVER; };
			}
			else if (command.Body.Contains("driver disable"))
			{
				action = () => { PanicButtonLabel = Resources.STR_ENABLE_DRIVER; };
			}

			if (null != action)
			{
				_dispatcher.BeginInvoke(action);
			}
		}


		private void SaveSettings()
		{
			var device = Conversation?.Device;
			if (null != device)
			{
				if (null == _userSettings)
				{
					var settingsCollection = Settings.Default.ServoTunerPluginSettings;
					_userSettings = settingsCollection.FindOrCreateSettings(device.GetIdentity());
				}

				_userSettings.MethodName = SelectedTunerType?.GetType().FullName;
				_userSettings.ParameterSetName = SelectedEditablePreset?.DisplayName;
			}

			Settings.Default.Save();
		}


		private void LoadSettings()
		{
			_userSettings = null;
			var device = Conversation?.Device;
			if (null != device)
			{
				var settingsCollection = Settings.Default.ServoTunerPluginSettings;
				if (null == settingsCollection)
				{
					settingsCollection = new SavedTunerSettingsCollection();
					Settings.Default.ServoTunerPluginSettings = settingsCollection;
				}

				_userSettings = settingsCollection.FindOrCreateSettings(device.GetIdentity());

				// ScanDevice() will always be called after LoadSettings, so prevent
				// accidental carry-over of properties from previous device.
				_deviceProperties = null;

				// We have to ensure the backing field is set before ScanDevice() needs it,
				// which is why this is an Invoke rather than a BeginInvoke.
				_dispatcher.Invoke(() =>
				{
					SelectedTunerType =
						TunerTypes.Where(t => t.GetType().FullName == _userSettings?.MethodName)
							   .FirstOrDefault()
					?? SelectedTunerType ?? _tunerTypes.FirstOrDefault();
				});

				// Other user settings are applied in ScanDevice() later.
			}
		}


		private void ScanDevice()
		{
			// If a scan is in progress, cancel it before starting a new one.
			if (_deviceScanner.IsBusy)
			{
				lock (_deviceScanner)
				{
					_redoDeviceScan = true;
					_deviceScanner.CancelAsync();
				}
			}
			else
			{
				_redoDeviceScan = false;
				_deviceScanner.Run();
			}
		}


		private void DeviceScanner_DoWork(object sender, DoWorkEventArgs e)
		{
			// Find out if the device has any tuning modes.
			_tuningModes = new Dictionary<string, string[]>();
			_deviceProperties = null;

			_dispatcher.Invoke(() =>
			{
				CanRead = CanWrite = false;
				ReadButtonTip = WriteButtonTip = Resources.STR_WAIT_FOR_SCAN_TO_FINISH;
			});

			if (IsPortOpen
			&& PortFacade.Port.IsAsciiMode
			&& (null != Conversation)
			&& Conversation.Device.IsSingleDevice)
			{
				var panicButtonLabel = Resources.STR_DISABLE_DRIVER;

				try
				{
					var response = Conversation.Request("get driver.enabled");
					if (0m == response.NumericData)
					{
						panicButtonLabel = Resources.STR_ENABLE_DRIVER;
					}
				}
				catch (Exception)
				{
				}

				_dispatcher.BeginInvoke(() => { PanicButtonLabel = panicButtonLabel; });

				var device = Conversation.Device;
				var cacheKey = GetCacheKey(device);

				// Devices without tuning modes are not tunable.
				_tuningModeInfoCache.TryGetValue(cacheKey, out _tuningModes);
				if (_tuningModes is null)
				{
					_tuningModes = device.DeviceType.TuningData is null
						? new Dictionary<string, string[]>() // Devices without DB tuning data are not tunable.
						: ServoDeviceHelper.ReadDeviceTuningModesAndParameters(Conversation);

					_tuningModeInfoCache[cacheKey] = _tuningModes;
				}
			}

			if (_deviceScanner.CancellationPending)
			{
				return;
			}

			if (_tuningModes.Count < 1)
			{
				// Not tunable - clear and disable controls.
				_dispatcher.BeginInvoke(() =>
				{
					WarningMessage = GetWarningMessage(Conversation?.Device);
					OnPropertyChanged(nameof(IsDeviceTunable));
					OnPropertyChanged(nameof(CanWriteDefault));
					SelectedTunerType.SetTargetDevice(Conversation, null);

					if (!(IsPortOpen && PortFacade.Port.IsAsciiMode))
					{
						ReadButtonTip = WriteButtonTip = "The port must be open in ASCII mode to use this feature.";
					}
					else
					{
						ReadButtonTip = WriteButtonTip =
							"This feature only works with axes that support servo commands and are tunable.";
					}
				});
			}
			else // Device is tunable.
			{
				// Find out how many presets it has and read them out.
				PresetInfo newLivePreset = null;
				PresetInfo newStagingPreset = null;
				var newFactoryPresets = new List<PresetInfo>();
				var newUserPresets = new List<PresetInfo>();
				foreach (var paramSetName in ServoDeviceHelper.ReadDevicePresetNames(Conversation))
				{
					if ("live" == paramSetName)
					{
						newLivePreset = new PresetInfo { PresetId = paramSetName };
					}
					else if ("staging" == paramSetName)
					{
						newStagingPreset = new PresetInfo { PresetId = paramSetName };
					}
					else if (paramSetName.StartsWith("default"))
					{
						newFactoryPresets.Add(new PresetInfo
						{
							IsWritable = false,
							PresetId = paramSetName
						});
					}
					else
					{
						newUserPresets.Add(new PresetInfo { PresetId = paramSetName });
					}
				}

				// Find the previously selected presets in the user settings, if any.
				var selectedPreset = _userSettings?.ParameterSetName;

				if (_deviceScanner.CancellationPending)
				{
					return;
				}

				// Find out which preset is the power-on default on the device.
				var response = Conversation.Request("servo get startup");
				string defaultPresetName = null;
				if ((null != response) && !response.IsError)
				{
					defaultPresetName = response.TextData;
				}

				if (_deviceScanner.CancellationPending)
				{
					return;
				}

				// Calculate device model for tuners.
				CalculateDeviceModel();

				if (_deviceScanner.CancellationPending)
				{
					return;
				}

				// Display what we know so far.
				_dispatcher.BeginInvoke(() =>
				{
					_livePreset = newLivePreset;
					_stagingPreset = newStagingPreset;
					ActivatablePresets = newUserPresets;
					_factoryPresets = newFactoryPresets;
					OnPropertyChanged(nameof(EditablePresets));
					OnPropertyChanged(nameof(ActivatablePresets));
					OnPropertyChanged(nameof(DefaultablePresets));
					OnPropertyChanged(nameof(CanWriteDefault));

					SelectedDefaultablePreset =
						DefaultablePresets.Where(p => defaultPresetName == p.PresetId).FirstOrDefault()
					?? DefaultablePresets.FirstOrDefault();
					ActualDefaultPreset = SelectedDefaultablePreset;

					// Display scanning message while retrieving all preset values.
					ActualActivePresetName = Resources.STR_SCANNING_PRESETS;
				});

				// Check to see if the live data matches any of the presets.
				PresetInfo whichPresetIsLive = null;
				var live = GetDevicePresetContents(newLivePreset);

				foreach (var candidate in ActivatablePresets)
				{
					if (_deviceScanner.CancellationPending)
					{
						return;
					}

					var data = GetDevicePresetContents(candidate);
					if (live.IsIdenticalTo(data))
					{
						whichPresetIsLive = candidate;
						break;
					}
				}

				_dispatcher.BeginInvoke(() =>
				{
					SelectedEditablePreset =
						EditablePresets.Where(p => selectedPreset == p.DisplayName).FirstOrDefault()
					?? EditablePresets.FirstOrDefault();

					// Display a message about what preset is live.
					ActualActivePresetName = whichPresetIsLive?.DisplayName;
					SelectedActivatablePreset = ActivatablePresets
											.Where(p => p.DisplayName == whichPresetIsLive?.DisplayName)
											.FirstOrDefault();
					CanRead = true;
					ReadButtonTip = null;
					UpdateCanWrite();
					OnPropertyChanged(nameof(IsDeviceTunable));

					// Configure the last selected tuner for this device.
					// Note user settings are always loaded before scanning the device, so the
					// SelectedTunerType value should be correct already.
					// This should be the last thing done in this worker thread because the
					// tab may now start doing its own device scanning.
					SelectedTunerType.SetTargetDevice(Conversation, _deviceProperties);
				});
			}
		}


		private void DeviceScanner_OnCompleted(object aSender, RunWorkerCompletedEventArgs aArgs)
		{
			var checkErrors = true;

			// If the device selection changed during a scan, redo the scan.
			lock (_deviceScanner)
			{
				if (_redoDeviceScan)
				{
					_redoDeviceScan = false;
					checkErrors = false;
					_deviceScanner.Run();
				}
			}

			if (checkErrors && (null != aArgs.Error))
			{
				var mbvm = new MessageBoxParams
				{
					Buttons = MessageBoxButton.OK,
					Caption = "Error while scanning device",
					Icon = MessageBoxImage.Error,
					Message =
						"There was an error while detecting tuning properties of this device. It may not be compatible with this tuning software."
				};

				_log.Error(mbvm.Message, aArgs.Error);

				RequestDialogOpen?.Invoke(this, mbvm);
			}
		}


		private void UpdateCanWrite()
		{
			if (!IsDeviceTunable)
			{
				CanWrite = false;
				WriteButtonTip = GetWarningMessage(Conversation?.Device);
			}
			else
			{
				var selectedPreset = SelectedEditablePreset;
				if ((null == selectedPreset) || !selectedPreset.IsWritable)
				{
					CanWrite = false;
					WriteButtonTip = Resources.STR_PRESET_NOT_WRITABLE;
				}
				else
				{
					CanWrite = true;
					WriteButtonTip = null;
				}
			}
		}


		private void CalculateDeviceModel()
		{
			var model = new DeviceProperties();

			var dev = Conversation.Device;
			var deviceConverter = dev.UnitConverter;

			// Find the fundamental position unit of the device.
			// Note only linear devices are currently supported.
			var meter = UnitConverter.Default.FindUnitBySymbol("m");
			var posTransform = dev.GetConversionToDeviceUnits(meter);
			if (null != posTransform)
			{
				model.PositionUnit = new ScaledShiftedUnit(meter, 1.0 / posTransform.Transform(1.0));
			}

			if (model.PositionUnit is null)
			{
				// This device isn't in the database.
				_deviceProperties = null;
				return;
			}

			var newton = UnitConverter.Default.FindUnitBySymbol("N");
			var forceUnitsPerNewton = 1000000.0; // Default for all current devices except DMQ, which will have the force abs command.
			var forceTransform = dev.GetConversionToDeviceUnits(newton);
			if (null != forceTransform)
			{
				// If the device has a force abs command, use the scale factor from that instead.
				forceUnitsPerNewton = forceTransform.Transform(1.0);
			}
			// We are not bailing out if the force transform was not found because the default is
			// correct for all existing products.

			// TODO support newton-meters for rotary devices. Requires adding torque to legacy unit system, which is needed anyway.
			model.ForceUnit = new ScaledShiftedUnit(newton, 1.0 / forceUnitsPerNewton);

			// Loop update rate is 10kHz for all devices at present.
			model.ControlLoopFrequency = new Measurement(10.0, UnitConverter.Default.FindUnitBySymbol("kHz"));

			model.SupportedTuningModes = _tuningModes;

			if (null != dev.DeviceType.TuningData)
			{
				model.CarriageInertia = new Measurement(dev.DeviceType.TuningData.Inertia);
				model.ForqueConstant = dev.DeviceType.TuningData.ForqueConstant;

				// Get maximum force output if possible.
				try
				{
					var runCurrentSetting = dev.DeviceType.GetCommandByText("driver.current.servo");
					if (null != runCurrentSetting)
					{
						var response = Conversation.Request("get driver.current.servo");
						var runCurrentInAmps =
							dev.ConvertData(response.NumericData, UnitOfMeasure.Amperes, runCurrentSetting);
						var maxForce = model.ForqueConstant * (double) runCurrentInAmps.Value;
						model.MaximumForce = new Measurement(maxForce, newton);
					}
				}
				catch (ErrorResponseException aErrorException)
				{
					_log.Warn("Device rejected a command when trying to calculate maximum force output.",
							  aErrorException);
				}
				catch (ZaberPortErrorException aPortException)
				{
					_log.Error("Communication error when trying to calculate maximum force output.", aPortException);
				}
				catch (DivideByZeroException aDivideException)
				{
					_log.Error("Division by zero when trying to calculate maximum force output.", aDivideException);
				}

				if (null == model.MaximumForce)
				{
					_log.Warn("Failed to read device's maximum force output.");
				}
			}
			else
			{
				// Forque constant is always needed for tuning.
				model = null;
			}

			_deviceProperties = model;
		}


		private ITunerParameterSet ReadDeviceParameters(PresetInfo aPreset = null)
		{
			ITunerParameterSet result = null;

			string errorMessage = null;
			try
			{
				// TODO (Soleil 2018-03-13): Add alternate way to read factory defaults.
				result = ServoDeviceHelper.ReadDeviceParameters(Conversation, aPreset.PresetId, _tuningModes);
			}
			catch (ErrorResponseException aErrorResponse)
			{
				errorMessage =
					"The device responded with an error while attempting to read tuning settings. Results may be incomplete.";
				errorMessage += Environment.NewLine + Environment.NewLine;
				errorMessage += "The error was: " + aErrorResponse.Message;
			}
			catch (ZaberPortErrorException aPortError)
			{
				errorMessage =
					"There was a communication error while reading device tuning settings. Results may be incomplete. Error message: "
				+ aPortError.Message;
			}

			if (null != errorMessage)
			{
				var mbvm = new MessageBoxParams
				{
					Message = errorMessage,
					Caption = "Error reading from device",
					Icon = MessageBoxImage.Exclamation,
					Buttons = MessageBoxButton.OK
				};

				RequestDialogOpen?.Invoke(this, mbvm);
			}

			return result;
		}


		private void WriteDeviceParameters(ITunerParameterSet aParams, PresetInfo aPreset)
		{
			string errorMessage = null;

			if (!aPreset.IsWritable)
			{
				errorMessage = "The selected parameter set is read-only.";
			}
			else
			{
				try
				{
					ServoDeviceHelper.WriteDeviceParameters(Conversation, aParams, _stagingPreset.PresetId);
					Conversation.Request(Conversation.Device.FormatAsciiCommand("servo {0} load {1}",
																				aPreset.PresetId,
																				_stagingPreset.PresetId));
				}
				catch (ErrorResponseException aErrorResponse)
				{
					errorMessage = "The device responded with an error while attempting to write tuning parameters.";
					errorMessage += Environment.NewLine + Environment.NewLine;
					errorMessage += "The error was: " + aErrorResponse.Message;
				}
				catch (ZaberPortErrorException aPortError)
				{
					errorMessage =
						"There was a communication error while writing device tuning parameters. Device tuning may be in an unstable state. Error message: "
					+ aPortError.Message;
				}
			}

			if (null != errorMessage)
			{
				var mbvm = new MessageBoxParams
				{
					Message = errorMessage,
					Caption = "Error writing to device",
					Icon = MessageBoxImage.Exclamation,
					Buttons = MessageBoxButton.OK
				};

				RequestDialogOpen?.Invoke(this, mbvm);
			}
		}


		private void CreateTuningMethods()
		{
			_tunerTypes.Clear();

			var assembliesToScan = new[] { typeof(ITunerParameterSet).Assembly, GetType().Assembly };

			var types = new List<Type>();

			foreach (var asm in assembliesToScan)
			{
				foreach (var t in asm.GetExportedTypes())
				{
					if (!t.IsAbstract)
					{
						if (typeof(CalculatorMethodBase).IsAssignableFrom(t))
						{
							types.Add(t);
						}
					}
				}
			}

			var sortedTypes = SortOrderAttribute.SortTypes(types);

			foreach (var t in sortedTypes)
			{
				var vm = Activator.CreateInstance(t) as CalculatorMethodBase;
				if (null != vm)
				{
					_tunerTypes.Add(vm);
				}

				if (vm is IDialogClient client)
				{
					client.RequestDialogOpen += Client_RequestDialogOpen;
					client.RequestDialogClose += Client_RequestDialogClose;
				}
			}

			SelectedTunerType = _tunerTypes.FirstOrDefault();
		}


		private string GetDisabledMessage()
		{
			var msg = new StringBuilder();
			msg.Append("This tab works with devices that have servo tuning commands");
			if (!IsPortOpen)
			{
				msg.AppendLine(".");
				msg.AppendLine("Open the port and select a servo device such as an X-DMQ.");
			}
			else if (!PortFacade.Port.IsAsciiMode)
			{
				msg.AppendLine(" and are using the ASCII protocol.");
				msg.AppendLine("You are currently using the Binary protocol.");
				msg.AppendLine("Switch to ASCII and select a servo device such as an X-DMQ.");
			}
			else
			{
				msg.AppendLine(".");
				msg.AppendLine("Select a servo device such as an X-DMQ.");
			}

			return msg.ToString();
		}


		// Gets the content of a device preset. This will return cached data if it exists,
		// else it will query the device and cache the result.
		private ITunerParameterSet GetDevicePresetContents(PresetInfo aPreset)
		{
			if (!IsPortOpen || !IsDeviceTunable)
			{
				return null;
			}

			var cacheKey = GetCacheKey(Conversation.Device);
			if (!_presetDataCache.ContainsKey(cacheKey))
			{
				_presetDataCache[cacheKey] = new Dictionary<string, ITunerParameterSet>();
			}

			var deviceCache = _presetDataCache[cacheKey];

			if (!deviceCache.ContainsKey(aPreset.DisplayName))
			{
				var data = ServoDeviceHelper.ReadDeviceParameters(Conversation, aPreset.PresetId, _tuningModes);
				deviceCache[aPreset.DisplayName] = data;
			}

			return deviceCache[aPreset.DisplayName];
		}


		/// <summary>
		///     Forcibly update the cached preset data after a write.
		/// </summary>
		/// <param name="aPreset">Preset to update cache for.</param>
		/// <param name="aData">New data to cache, or null to invalidate the cached info.</param>
		private void UpdatePresetCache(PresetInfo aPreset, ITunerParameterSet aData)
		{
			if (!IsPortOpen || !IsDeviceTunable)
			{
				return;
			}

			var cacheKey = GetCacheKey(Conversation.Device);
			if (!_presetDataCache.ContainsKey(cacheKey))
			{
				_presetDataCache[cacheKey] = new Dictionary<string, ITunerParameterSet>();
			}

			var deviceCache = _presetDataCache[cacheKey];

			if (null != aData)
			{
				deviceCache[aPreset.DisplayName] = aData;
			}
			else if (deviceCache.ContainsKey(aPreset.DisplayName))
			{
				deviceCache.Remove(aPreset.DisplayName);
			}
		}


		private string GetWarningMessage(ZaberDevice device)
		{
			if (null == device)
			{
				return null;
			}

			if ((device.FirmwareVersion.Major < 7) && device.Description.Contains("DMQ"))
			{
				// RM #6159 - special case for FW6 DMQs.
				return Resources.STR_DMQ_NOT_TUNABLE;
			}

			return Resources.STR_DEVICE_NOT_TUNABLE;
		}


		private PresetInfo GetDefaultPreset()
		{
			return _factoryPresets.FirstOrDefault(p => p.PresetId.ToLower().Contains("default"));
		}


		private static CacheKey GetCacheKey(ZaberDevice aDevice) 
			=> (aDevice is null) ? null : new CacheKey(aDevice.DeviceNumber, aDevice.AxisNumber);

		#endregion

		#region -- Private fields --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Servo Tuner tab");

		private static readonly int DEFAULT_STATUS_DISPLAY_TIME = 2;

		private IPlugInManager _pluginManager;
		private readonly IDispatcher _dispatcher = DispatcherHelper.Dispatcher;
		private readonly IBackgroundWorker _deviceScanner;
		private volatile bool _redoDeviceScan;
		private string _warningMessage;
		private RelayCommand _openScopeCommand;
		private RelayCommand _writeParametersCommand;
		private RelayCommand _writeDefaultsCommand;
		private RelayCommand _readParametersCommand;
		private RelayCommand _activatePresetCommand;
		private RelayCommand _changeDefaultPresetCommand;
		private RelayCommand _panicButtonCommand;

		private ZaberPortFacade _portFacade;
		private Conversation _conversation;
		private SavedTunerSettings _userSettings;

		private static readonly HashSet<string> _bubbledPropertyNames = new HashSet<string>
		{
			nameof(CanRead),
			nameof(CanWrite),
			nameof(ReadButtonTip),
			nameof(WriteButtonTip)
		};

		private IDictionary<string, string[]> _tuningModes;

		// Cache of tuning modes and their parameter names, by device number and axis number.
		private readonly Dictionary<CacheKey, IDictionary<string, string[]>> _tuningModeInfoCache =
			new Dictionary<CacheKey, IDictionary<string, string[]>>();

		// Cache of preset contents read from each device, keyed by device number.
		private readonly Dictionary<CacheKey, Dictionary<string, ITunerParameterSet>> _presetDataCache =
			new Dictionary<CacheKey, Dictionary<string, ITunerParameterSet>>();

		private ObservableCollection<CalculatorMethodBase> _tunerTypes =
			new ObservableCollection<CalculatorMethodBase>();

		private CalculatorMethodBase _selectedTunerType;

		private DeviceProperties _deviceProperties;

		private bool _canRead;
		private bool _canWrite;
		private string _readButtonTip;
		private string _writeButtonTip;
		private string _panicButtonLabel;
		private bool _isVisible;

		// Sources of information about presets.
		private PresetInfo _livePreset;
		private PresetInfo _stagingPreset;

		private IEnumerable<PresetInfo> _factoryPresets = new PresetInfo[0];
		private PresetInfo _actualDefaultPreset; // Preset that is actually the power-on default in the device.
		private string _actualActivePresetName; // Preset that is actually live on the device.

		// User selected presets from above sources.
		private PresetInfo _selectedEditablePreset; // The one to read and write from the UI.
		private PresetInfo _selectedActiveatablePreset; // The one active/live on the device, if any.
		private PresetInfo _selectedDefaultablePreset; // The one that will be the power-on default.

		#endregion
	}
}

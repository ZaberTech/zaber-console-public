﻿using System.Collections.ObjectModel;
using Zaber.Units;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.ServoTuner
{
	/// <summary>
	///     ViewModel for a row in the <see cref="ParameterListVM" />.
	/// </summary>
	public class ParameterListRowVM : ObservableObject
	{
		/// <summary>
		///     Convenience constructor to initialize all properties in one line.
		/// </summary>
		/// <param name="aName"></param>
		/// <param name="aValue"></param>
		public ParameterListRowVM(string aName, decimal aValue = 0, string aDescription = null)
		{
			Name = aName;
			Value = aValue;
			Description = aDescription;
		}


		/// <summary>
		///     Name of a parameter.
		/// </summary>
		public string Name
		{
			get => _name;
			set => Set(ref _name, value, nameof(Name));
		}


		/// <summary>
		///     Value of the parameter.
		/// </summary>
		public decimal Value
		{
			get => _value;
			set => Set(ref _value, value, nameof(Value));
		}


		/// <summary>
		///     Observable property for views to sync Description.
		/// </summary>
		public string Description
		{
			get => _description;
			set => Set(ref _description, value, nameof(Description));
		}


		/// <summary>
		///     Units of measure applicable to this parameter, if any.
		/// </summary>
		public ObservableCollection<Unit> ApplicableUnits
		{
			get => _applicableUnits;
			set => Set(ref _applicableUnits, value, nameof(ApplicableUnits));
		}


		/// <summary>
		///     Currently selected unit of measure.
		/// </summary>
		public Unit SelectedUnit
		{
			get => _selectedUnit;
			set => Set(ref _selectedUnit, value, nameof(SelectedUnit));
		}


		private decimal _value;
		private string _name;
		private string _description;
		private ObservableCollection<Unit> _applicableUnits;
		private Unit _selectedUnit;
	}
}

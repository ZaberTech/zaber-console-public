﻿using System;
using System.Collections.Generic;
using Zaber.Application;
using Zaber.Serialization;

namespace ZaberConsole.Plugins.ServoTuner.Methods
{
	/// <summary>
	///     Saved settings for the Simple calculation method, for one device.
	/// </summary>
	[Serializable]
	public class SimpleMethodSettings : PerDeviceSettings
	{
		/// <summary>
		///     Last entered value for the load mass.
		/// </summary>
		public decimal LoadMassQuantity { get; set; }


		/// <summary>
		///     Unit symbol for the last selected load mass unit.
		/// </summary>
		public string LoadMassUnitSymbol { get; set; }


		/// <summary>
		///     Last entered value for the stage carriage mass.
		/// </summary>
		public decimal CarriageMassQuantity { get; set; }


		/// <summary>
		///     Unit symbol for the last selected carriage mass unit.
		/// </summary>
		public string CarriageMassUnitSymbol { get; set; }


		/// <summary>
		///     Saved settings of the dynamic user controls that are generated from the database guidance data.
		/// </summary>
		public SerializableDictionary<string, List<SerializableTuple<string, object>>> ControlSettings { get; set; }
	}
}

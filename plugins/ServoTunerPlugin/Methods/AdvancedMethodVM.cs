﻿using System.ComponentModel;
using System.Linq;
using Zaber;
using Zaber.Tuning.Servo;

namespace ZaberConsole.Plugins.ServoTuner.Methods
{
	[DisplayName(DISPLAY_NAME)]
	[SortOrder(100.0)]
	public class AdvancedMethodVM : CalculatorMethodBase
	{
		#region -- Initialization --

		public AdvancedMethodVM()
		{
			TuningModeName = "none";
			ShowNewParameters(null);
		}

		#endregion

		#region -- Helpers --

		private void ShowNewParameters(ITunerParameterSet aSet)
		{
			EditingParameters = new ParameterListVM(aSet)
			{
				IsEditable = true,
				ShowDescriptions = false
			};
		}

		#endregion

		#region -- Base class overrides --

		/// <summary>
		///     Tuning parameters read from the device or ready to write to the device.
		/// </summary>
		public override ITunerParameterSet DeviceParameters
		{
			get => EditingParameters.GetEditedParameters();
			set => ShowNewParameters(value);
		}


		/// <summary>
		///     Informs the tuning method of a newly selected device.
		/// </summary>
		/// <param name="aConversation">Conversation with the device.</param>
		/// <param name="aServoProperties">Servo-related properties of the device.</param>
		public override void SetTargetDevice(Conversation aConversation, DeviceProperties aServoProperties)
		{
			if ((null != aServoProperties) && (aServoProperties.SupportedTuningModes.Count > 0))
			{
				var different = false;

				if (!aServoProperties.SupportedTuningModes.ContainsKey(TuningModeName))
				{
					different = true;
					TuningModeName = aServoProperties.SupportedTuningModes.Keys.First();
				}

				// If we've switched to a new mode, create and display an empty set for that mode.
				if (different)
				{
					var paramNames = aServoProperties.SupportedTuningModes[TuningModeName];

					ShowNewParameters(ServoDeviceHelper.CreateEmptyParams(TuningModeName, paramNames));
				}

				CanRead = true;
				CanWrite = true;
				ReadButtonTip = DEFAULT_READ_TIP;
				WriteButtonTip = DEFAULT_WRITE_TIP;
			}
			else
			{
				CanRead = false;
				CanWrite = false;
				ReadButtonTip = WriteButtonTip = NOT_SUPPORTED_TIP;
			}
		}

		#endregion

		#region -- View data --

		/// <summary>
		///     View property for the current tuning mode of the device preset.
		/// </summary>
		public string TuningModeName
		{
			get => _tuningModeName;
			set => Set(ref _tuningModeName, value, nameof(TuningModeName));
		}


		/// <summary>
		///     View property for the parameter set being edited.
		/// </summary>
		public ParameterListVM EditingParameters
		{
			get => _editingParameters;
			set => Set(ref _editingParameters, value, nameof(EditingParameters));
		}

		#endregion

		#region -- Fields --

		public const string DISPLAY_NAME = "Advanced";
		private string _tuningModeName = "none";
		private ParameterListVM _editingParameters;

		#endregion
	}
}

﻿using System;

namespace ZaberConsole.Plugins.ServoTuner.Methods.ParameterControls
{
	/// <summary>
	///     Attribute used to match dynamic controls on the simple tuner tab to
	///     input data from the device database.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
	public class TunerUserParameterControlAttribute : Attribute
	{
		/// <summary>
		///     Convenience constructor to initialize properties.
		/// </summary>
		/// <param name="aDataType">JSON user parameter data type supported by the control.</param>
		public TunerUserParameterControlAttribute(string aDataType)
		{
			DataType = aDataType;
		}


		/// <summary>
		///     JSON data type supported by the control.
		/// </summary>
		public string DataType { get; set; }
	}
}

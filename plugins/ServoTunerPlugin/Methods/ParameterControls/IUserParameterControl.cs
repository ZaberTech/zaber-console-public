﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Zaber.Tuning.Servo;

namespace ZaberConsole.Plugins.ServoTuner.Methods.ParameterControls
{
	/// <summary>
	///     Interface for ViewModels that know how to display user parameters from the
	///     JSON in the device database.
	/// </summary>
	public interface IUserParameterControl
	{
		/// <summary>
		///     Configure the controls given the user parameter data from the database.
		///     The control is reponsible for parsing and saving the data. It must not
		///     save a reference to the argument.
		/// </summary>
		/// <param name="aParameterData">User parameter data for the control.</param>
		void Configure(JToken aParameterData);


		/// <summary>
		///     Get the current values of the controls as inputs for the relevant
		///     tuning algorithm.
		/// </summary>
		/// <returns>A parameter set representing the current state of the controls.</returns>
		ITunerParameterSet GetTunerParameters();


		/// <summary>
		///     Access to persistent user settings for this control. The implementations of
		///     <see cref="IUserParameterControl" /> do not have to persist their own settings;
		///     the containing VM will handle that through this property. The getter will be
		///     called when settings are being saved, and the setter will be called when uset
		///     settings are being loaded. The setter implementation should handle the case of
		///     some values being missing or out of range, and default them.
		/// </summary>
		IEnumerable<Tuple<string, object>> UserSettings { get; set; }
	}
}

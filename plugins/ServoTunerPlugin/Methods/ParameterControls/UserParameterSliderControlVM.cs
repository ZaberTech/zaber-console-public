﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using log4net;
using Newtonsoft.Json.Linq;
using Zaber.Tuning.Servo;
using Zaber.Units;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.ServoTuner.Methods.ParameterControls
{
	/// <summary>
	///     User control VM for interpolable tuner user params. Presents a slider and
	///     interpolates arrays of data based on slider position.
	/// </summary>
	[TunerUserParameterControl("Interpolated")]
	public class UserParameterSliderControlVM : ObservableObject, IUserParameterControl
	{
		#region -- IUserParameterControl implementation --

		/// <inheritdoc cref="IUserParameterControl.Configure(JToken)" />
		public void Configure(JToken aParameterData)
		{
			Title = aParameterData.Value<string>("ParameterName");
			MinimumLabel = aParameterData.Value<string>("MinValueLabel") ?? "Min";
			MaximumLabel = aParameterData.Value<string>("MaxValueLabel") ?? "Max";
			SliderPosition = aParameterData.Value<double?>("DefaultValue") ?? DEFAULT_SLIDER_POSITION;
			var values = aParameterData["Values"];
			foreach (var token in values)
			{
				var prop = token as JProperty;
				var name = prop.Name;
				var value = prop.Value;
				try
				{
					var values2 = value.Values<double>();
					_values[name] = values2.ToArray();
				}
				catch (InvalidOperationException)
				{
					try
					{
						var value2 = value.Value<double>();
						_values[name] = new[] { value2 };
					}
					catch (InvalidOperationException)
					{
						_log.Error("Could not extract JSON value for property " + name);
					}
				}
			}
		}


		/// <inheritdoc cref="IUserParameterControl.GetTunerParameters" />
		public ITunerParameterSet GetTunerParameters()
		{
			var result = new TunerParameterSet("Interpolated");

			var i = SliderPosition;
			foreach (var pair in _values)
			{
				result.Add(pair.Key, new Measurement(Interpolate(pair.Value, i), null));
			}

			return result;
		}


		/// <inheritdoc cref="IUserParameterControl.UserSettings" />
		public IEnumerable<Tuple<string, object>> UserSettings
		{
			// Slider deliberately does not persist value in user settings - see ticket RM #6968.
			get => new Tuple<string, object>[0];
			set { }
		}

		#endregion

		#region -- View properties --

		/// <summary>
		///     Title for the control.
		/// </summary>
		public string Title
		{
			get => _title;
			set => Set(ref _title, value, nameof(Title));
		}


		/// <summary>
		///     Label for the left end of the slider.
		/// </summary>
		public string MinimumLabel
		{
			get => _minimumLabel;
			set => Set(ref _minimumLabel, value, nameof(MinimumLabel));
		}


		/// <summary>
		///     Label for the right end of the slider.
		/// </summary>
		public string MaximumLabel
		{
			get => _maximumLabel;
			set => Set(ref _maximumLabel, value, nameof(MaximumLabel));
		}


		/// <summary>
		///     Current value of the slider.
		/// </summary>
		public double SliderPosition
		{
			get => _sliderPosition;
			set => Set(ref _sliderPosition, value, nameof(SliderPosition));
		}

		#endregion

		#region -- Private members --

		private double Interpolate(double[] aValues, double aPosition)
		{
			if (aValues.Length < 1)
			{
				throw new ArgumentException("Tuning parameter " + Title + " has no values.");
			}

			if (aValues.Length == 1)
			{
				return aValues[0];
			}

			var i = aPosition * (aValues.Length - 1);
			var index = (int) Math.Floor(i);
			if (index == (aValues.Length - 1))
			{
				return aValues[index];
			}

			var low = aValues[index];
			var high = aValues[index + 1];
			i -= index;
			var result = low + (i * (high - low));
			return result;
		}


		private const double DEFAULT_SLIDER_POSITION = 0.5;
		private const string SLIDER_SETTING_NAME = "SliderPosition";

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private string _title;
		private string _minimumLabel;
		private string _maximumLabel;
		private double _sliderPosition;
		private readonly Dictionary<string, double[]> _values = new Dictionary<string, double[]>();

		#endregion
	}
}

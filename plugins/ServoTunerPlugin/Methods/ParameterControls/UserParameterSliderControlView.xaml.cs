﻿namespace ZaberConsole.Plugins.ServoTuner.Methods.ParameterControls
{
	/// <summary>
	///     Interaction logic for UserParameterSliderControlView.xaml
	/// </summary>
	public partial class UserParameterSliderControlView
	{
		/// <summary>
		///     Initializes the control.
		/// </summary>
		public UserParameterSliderControlView()
		{
			InitializeComponent();
		}
	}
}

﻿using Zaber;
using Zaber.Tuning.Servo;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.ServoTuner.Methods
{
	/// <summary>
	///     Base class for all tuner calculation method VMs.
	/// </summary>
	public abstract class CalculatorMethodBase : ObservableObject
	{
		#region -- Public API --

		/// <summary>
		///     Called by the plugin tab when the selected device changes.
		/// </summary>
		/// <param name="aConversation">
		///     Global conversation instance for the newly selected device. This is provided even if the
		///     device is not a servo stage.
		/// </param>
		/// <param name="aServoProperties">
		///     Calculated servo properties of the device. This is only provided if the device is
		///     tunable and the properties can be determined.
		/// </param>
		public abstract void SetTargetDevice(Conversation aConversation, DeviceProperties aServoProperties);


		/// <summary>
		///     Returns true to indicate that the method supports importing the current parameters from the device.
		///     That is, if this returns true then you can expect to be able to set <see cref="DeviceParameters" />.
		/// </summary>
		public bool CanRead
		{
			get => _canRead;
			protected set => Set(ref _canRead, value, nameof(CanRead));
		}


		/// <summary>
		///     Returns true to indicate that the method can output parameters for the current device.
		///     That is, if this returns true then you can expect to be able to get from <see cref="DeviceParameters" /> .
		/// </summary>
		public bool CanWrite
		{
			get => _canWrite;
			protected set => Set(ref _canWrite, value, nameof(CanWrite));
		}


		/// <summary>
		///     Tooltip for the read button.
		/// </summary>
		public string ReadButtonTip
		{
			get => _readButtonTip;
			set => Set(ref _readButtonTip, value, nameof(ReadButtonTip));
		}


		/// <summary>
		///     Tooltip for the write button.
		/// </summary>
		public string WriteButtonTip
		{
			get => _writeButtonTip;
			set => Set(ref _writeButtonTip, value, nameof(WriteButtonTip));
		}


		/// <summary>
		///     Get tuning parameters to write to the device, or set parameters that have been read from the device.
		///     Throws an exception (with a human-readable help message) if the operation is not valid at this time or
		///     if there is a calculation error. The expectatation is that get will be called when the user wants to
		///     write their currently edited values to the device, and set will be called when they read from the device.
		///     The device communication is handled by the plugin tab, not the method. Call  <see cref="CanRead" />  to see if
		///     setting this
		///     is legal, and <see cref="CanWrite" /> to see if getting is legal.
		/// </summary>
		public abstract ITunerParameterSet DeviceParameters { get; set; }


		/// <summary>
		///     Invoked when the plugin is closing; allows methods to prompt the user to save data.
		/// </summary>
		public virtual void OnClosing()
		{
		}


		/// <summary>
		///     Invoked when user settings are reset. The method should clear all saved settings.
		/// </summary>
		public virtual void ClearSettings()
		{
		}

		#endregion

		#region -- Fields --

		protected const string DEFAULT_READ_TIP = "Read and display the selected tuning value set from the device.";
		protected const string DEFAULT_WRITE_TIP = "Write the entered tuning values to the device.";
		protected const string NOT_SUPPORTED_TIP = "The currently selected device is not supported by this tab.";

		private bool _canRead;
		private bool _canWrite;
		private string _readButtonTip = DEFAULT_READ_TIP;
		private string _writeButtonTip = DEFAULT_WRITE_TIP;

		#endregion
	}
}

﻿<ztb:BaseControl x:Class="ZaberConsole.Plugins.ServoTuner.Methods.SimpleCalculatorMethodView"
                 xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
                 xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
                 xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
                 xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                 xmlns:ztb="clr-namespace:ZaberWpfToolbox;assembly=ZaberWpfToolbox"
                 xmlns:ztbc="clr-namespace:ZaberWpfToolbox.Converters;assembly=ZaberWpfToolbox"
                 xmlns:ztbr="clr-namespace:ZaberWpfToolbox.Resources;assembly=ZaberWpfToolbox"
                 xmlns:xctk="http://schemas.xceed.com/wpf/xaml/toolkit"
                 mc:Ignorable="d"
                 d:DesignHeight="300"
                 d:DesignWidth="300">

	<ztb:BaseControl.Resources>
		<ResourceDictionary>
			<Image x:Key="_rightArrowIcon"
				   Width="16"
				   Source="pack://application:,,,/ZaberWpfToolbox;component/Resources/Icons/64px-black-arrow-down-right.png" />

			<ztbc:SequentialValueConverter x:Key="_stringVisibleIfNotEmpty">
				<ztbc:StringEmptyConverter NegateResult="True" />
				<ztbc:ConfigurableBooleanToVisibilityConverter />
			</ztbc:SequentialValueConverter>
		</ResourceDictionary>
	</ztb:BaseControl.Resources>

	<ScrollViewer HorizontalScrollBarVisibility="Auto"
				  VerticalScrollBarVisibility="Auto"
				  VerticalAlignment="Stretch"
				  HorizontalAlignment="Stretch">

		<Grid>
			<Grid.RowDefinitions>
				<RowDefinition Height="Auto" />
				<RowDefinition Height="Auto" />
				<RowDefinition Height="*" />
			</Grid.RowDefinitions>

			<Grid.ColumnDefinitions>
				<ColumnDefinition Width="*" />
				<ColumnDefinition Width="Auto" />
			</Grid.ColumnDefinitions>

			<TextBlock Grid.ColumnSpan="3"
					   TextWrapping="Wrap"
					   Text="Calculate tuning parameters for a simple static load." />

			<GroupBox Grid.Row="1"
					  Header="Load">
				<Grid>
					<Grid.RowDefinitions>
						<RowDefinition Height="Auto" />
						<RowDefinition Height="Auto" />
						<RowDefinition Height="Auto" />
					</Grid.RowDefinitions>

					<Grid.ColumnDefinitions>
						<ColumnDefinition Width="Auto" />
						<ColumnDefinition Width="Auto" />
					</Grid.ColumnDefinitions>

					<Label Content="Load mass:"
						   VerticalAlignment="Center"
						   ToolTip="Enter the inertia of the load carried by the stage." />

					<ContentPresenter Grid.Column="1"
									   MinWidth="100"
									   Content="{Binding LoadInertia}" />

					<Label Grid.Row="1"
						   Content="Stage moving mass:"
						   VerticalAlignment="Center"
						   ToolTip="Enter the inertia of the moving part of the stage. You can find this in the product specifications as 'Moving Mass'." />

					<ContentPresenter Grid.Row="1"
									  Grid.Column="1"
									  MinWidth="100"
									  Content="{Binding CarriageInertia}" />

					<TextBlock Grid.Row="2"
							   Grid.ColumnSpan="2"
							   VerticalAlignment="Center"
							   TextWrapping="Wrap"
							   Margin="3"
							   Visibility="{Binding DefaultCarriageInertia, Converter={StaticResource _stringVisibleIfNotEmpty}}"
							   MaxWidth="{Binding ActualWidth, RelativeSource={RelativeSource AncestorType=GroupBox}}">
						<Run Text="(Use the default moving mass of" />
						<Hyperlink Command="{Binding RestoreDefaultCarriageInertiaCommand, Mode=OneTime}"
								   ToolTip="Click to use this value.">
							<Run Text="{Binding DefaultCarriageInertia, Mode=OneWay}" />
						</Hyperlink>
						<Run Text="unless the stage carriage has been customized.)" />
					</TextBlock>
				</Grid>
			</GroupBox>

			<GroupBox Grid.Row="1"
					  Grid.Column="1"
					  Header="Acceleration"
					  Visibility="{Binding CanChangeAcceleration, Converter={x:Static ztbr:StaticResources.ConvertBoolToVisibility}}">
				<Grid>
					<Grid.RowDefinitions>
						<RowDefinition Height="Auto" />
						<RowDefinition Height="Auto" />
						<RowDefinition Height="Auto" />
					</Grid.RowDefinitions>

					<Grid.ColumnDefinitions>
						<ColumnDefinition Width="Auto" />
						<ColumnDefinition Width="Auto" />
						<ColumnDefinition Width="Auto" />
					</Grid.ColumnDefinitions>

					<Grid Grid.ColumnSpan="3">
						<Grid.RowDefinitions>
							<RowDefinition Height="Auto" />
							<RowDefinition Height="Auto" />
						</Grid.RowDefinitions>

						<Grid.ColumnDefinitions>
							<ColumnDefinition Width="Auto" />
							<ColumnDefinition Width="Auto" />
						</Grid.ColumnDefinitions>

						<TextBlock VerticalAlignment="Center"
								   HorizontalAlignment="Right"
								   Margin="3"
								   Text="Current acceleration:" />

						<TextBlock Grid.Column="1"
								   VerticalAlignment="Center"
								   HorizontalAlignment="Left"
								   Margin="3"
								   Text="{Binding CurrentAcceleration}"
								   ToolTip="The current acceleration setting on the device." />

						<TextBlock Grid.Row="1"
								   VerticalAlignment="Center"
								   HorizontalAlignment="Right"
								   Margin="3"
								   Text="Recommended maximum:" />

						<TextBlock Grid.Row="1"
								   Grid.Column="1"
								   VerticalAlignment="Center"
								   HorizontalAlignment="Left"
								   Margin="3"
								   Text="{Binding RecommendedAcceleration}"
								   ToolTip="Based on the load quantity you have entered, we recommend making the device's acceleration setting no higher than this value, to reduce overshoot." />
					</Grid>

					<xctk:IconButton Grid.Row="1"
									 Height="26"
									 Width="26"
									 HorizontalAlignment="Left"
									 Icon="{StaticResource _rightArrowIcon}"
									 Command="{Binding CopyRecommendedAccelerationCommand}"
									 IsEnabled="{Binding RecommendedAcceleration, Converter={x:Static ztbr:StaticResources.DetectNonemptyString}}"
									 ToolTip="Copy the recommended acceleration to the box at right, where you can change it or write it to the device." />

					<ContentPresenter Grid.Row="1"
									  Grid.Column="1"
									  Margin="3"
									  VerticalAlignment="Center"
									  HorizontalAlignment="Center"
									  Content="{Binding NewAcceleration}"
									  Width="150"
									  ToolTip="Enter an acceleration value and click the Write button to change the device's target acceleration rate." />

					<Button Grid.Row="1"
							Grid.Column="2"
							Command="{Binding ChangeAccelerationCommand}"
							Content="Write"
							Height="26"
							ToolTip="Transfers the acceleration value you have entered to the device's 'accel' setting." />

				</Grid>
			</GroupBox>

			<ItemsControl Grid.Row="2"
						  Grid.ColumnSpan="2"
						  IsTabStop="False"
						  ItemsSource="{Binding UserParameterControls}">
				<ItemsControl.ItemTemplate>
					<DataTemplate>
						<GroupBox Header="{Binding Title}">
							<ContentPresenter Content="{Binding}" />
						</GroupBox>
					</DataTemplate>
				</ItemsControl.ItemTemplate>
			</ItemsControl>

		</Grid>
	</ScrollViewer>
</ztb:BaseControl>
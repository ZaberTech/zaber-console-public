﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using DataAccess;
using log4net;
using Zaber;
using Zaber.Telemetry;
using Zaber.Tuning;
using Zaber.Tuning.Servo;
using Zaber.Units;
using ZaberConsole.Plugins.ServoTuner.Properties;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Dialogs;
using Measurement = Zaber.Units.Measurement;

namespace ZaberConsole.Plugins.ServoTuner.Methods
{
	[DisplayName(DISPLAY_NAME)]
	[Description("Load tuning parameters from a CSV file.")]
	[SortOrder(50.0)]
	public class ImportExportMethodVM : CalculatorMethodBase, IDialogClient
	{
		#region -- Initialization --

		public ImportExportMethodVM()
		{
			_dispatcher = DispatcherHelper.Dispatcher;
			LoadUserSettings();
		}

		#endregion

		#region -- Base class overrides --

		/// <summary>
		///     Called by the plugin tab when the selected device changes.
		/// </summary>
		/// <param name="aConversation">
		///     Global conversation instance for the newly selected device. This is provided even if the
		///     device is not a servo stage.
		/// </param>
		/// <param name="aServoProperties">
		///     Calculated servo properties of the device. This is only provided if the device is
		///     tunable and the properties can be determined.
		/// </param>
		public override void SetTargetDevice(Conversation aConversation, DeviceProperties aServoProperties)
		{
			SaveUserSettings();
			_deviceProperties = aServoProperties;
			_lastDevice = aConversation?.Device;
			LoadUserSettings();
			UpdateCanReadWrite();
		}


		/// <summary>
		///     Get tuning parameters to write to the device, or set parameters that have been read from the device.
		///     Throws an exception (with a human-readable help message) if the operation is not valid at this time or
		///     if there is a calculation error. The expectatation is that get will be called when the user wants to
		///     write their currently edited values to the device, and set will be called when they read from the device.
		///     The device communication is handled by the plugin tab, not the method. Call  <see cref="CanRead" />  to see if
		///     setting this
		///     is legal, and <see cref="CanWrite" /> to see if getting is legal.
		/// </summary>
		public override ITunerParameterSet DeviceParameters
		{
			get
			{
				var parms = SelectedRow?.ParamSet;
				parms = parms?.Clone();

				if (!_deviceProperties.SupportedTuningModes.ContainsKey(parms.ModeName))
				{
					throw new InvalidDataException(
						"The tuning mode of the selected row is not supported by the device.");
				}

				return parms;
			}
			set
			{
				if (null == value)
				{
					return;
				}

				var newRow = CreateRow(value);
				newRow.ControllerVersion = _lastDevice.DeviceType.TuningData.ControllerVersions[value.ModeName];
				_dispatcher.BeginInvoke(() =>
				{
					if (null == Rows)
					{
						Rows = new ObservableCollection<ImportExportRowVM>();
					}

					Rows.Add(newRow);
				});
			}
		}


		/// <summary>
		///     Invoked when the plugin is closing; allows methods to prompt the user to save data.
		/// </summary>
		public override void OnClosing()
		{
			ConfirmDiscardData();
			SaveUserSettings();
		}


		/// <summary>
		///     Invoked when user settings are reset. The method should clear all saved settings.
		/// </summary>
		public override void ClearSettings() => Settings.Default.ImportExportMethodSettings?.Clear();

		#endregion

		#region -- IDialogClient implementation --

		/// <summary>
		///     Event to invoke in order to open a dialog box.
		/// </summary>
		public event RequestDialogChange RequestDialogOpen;

		/// <summary>
		///     Event to invoke in order to close an existing dialog box.
		/// </summary>
		#pragma warning disable CS0067 // Event is never used.
		public event RequestDialogChange RequestDialogClose;
		#pragma warning restore CS0067

		#endregion

		#region -- View data --

		/// <summary>
		///     Observable property for views to sync CurrentFilePath.
		/// </summary>
		public string CurrentFilePath
		{
			get => _currentFilePath;
			set => Set(ref _currentFilePath, value, nameof(CurrentFilePath));
		}


		/// <summary>
		///     Observable property for views to sync Rows.
		/// </summary>
		public ObservableCollection<ImportExportRowVM> Rows
		{
			get => _rows;
			set
			{
				if (null != _rows)
				{
					_rows.CollectionChanged -= Rows_CollectionChanged;
				}

				Set(ref _rows, value, nameof(Rows));

				if (null != _rows)
				{
					_rows.CollectionChanged += Rows_CollectionChanged;
				}
			}
		}


		/// <summary>
		///     Observable property for views to sync SelectedRow.
		/// </summary>
		public ImportExportRowVM SelectedRow
		{
			get => _selectedRow;
			set
			{
				Set(ref _selectedRow, value, nameof(SelectedRow));
				UpdateCanReadWrite();
			}
		}


		private void UpdateCanReadWrite()
		{
			var r = false;
			var w = false;

			if (null != _deviceProperties)
			{
				// The CSV method can read any tuning mode.
				r = true;
				ReadButtonTip = "Read the device's tuning data into a new row in the table.";

				if (null != SelectedRow)
				{
					var mode = SelectedRow.Mode;
					if (_deviceProperties.SupportedTuningModes.ContainsKey(mode))
					{
						// We can write if the selected row's mode is directly supported by the device.
						var supportedVersion = _lastDevice.DeviceType.TuningData.ControllerVersions[mode];
						if (supportedVersion == SelectedRow.ControllerVersion)
						{
							w = true;
							WriteButtonTip = "Write the selected row to the device.";
						}
						else if (supportedVersion < SelectedRow.ControllerVersion)
						{
							WriteButtonTip =
								"The selected row contains data for a newer servo control loop version than the device is running. You may need a firmware update to use this data.";
						}
						else // Device version greater than data version.
						{
							WriteButtonTip =
								"The selected row contains data for an older servo control loop implementation than the device is running, and cannot be converted to a newer rerpresentation.";
						}
					}
					else
					{
						WriteButtonTip =
							"The selected device does not support the tuning mode represented in the selected table row.";
					}
				}
				else
				{
					WriteButtonTip = "There is no table row selected to write to the device.";
				}
			}
			else
			{
				ReadButtonTip = WriteButtonTip = NOT_SUPPORTED_TIP;
			}

			CanRead = r;
			CanWrite = w;
		}


		/// <summary>
		///     Indicated whether the current file content is saved.
		/// </summary>
		public bool IsSaved
		{
			get => _isSaved;
			set => Set(ref _isSaved, value, nameof(IsSaved));
		}


		/// <summary>
		///     Command invoked by the New File button.
		/// </summary>
		public ICommand NewFileCommand => OnDemandRelayCommand(ref _newFileCommand, _ => NewFile());

		/// <summary>
		///     Command invoked by the Open File button.
		/// </summary>
		public ICommand OpenFileCommand => OnDemandRelayCommand(ref _openFileCommand, _ => OpenFile());


		/// <summary>
		///     Command invoked by the Save and Save As buttons.
		/// </summary>
		public ICommand SaveAsCommand => OnDemandRelayCommand(ref _saveAsCommand, aPath => SaveFileAs(aPath as string));


		/// <summary>
		///     Context menu command to move the current row up.
		/// </summary>
		public ICommand MoveUpCommand
			=> OnDemandRelayCommand(ref _moveUpCommand,
									aRow =>
									{
										var row = aRow as ImportExportRowVM;
										var pos = Rows.IndexOf(row);
										if (pos > 0)
										{
											Rows.Remove(row);
											Rows.Insert(pos - 1, row);
										}
									},
									aRow =>
									{
										var row = aRow as ImportExportRowVM;
										var pos = Rows.IndexOf(row);
										return pos > 0;
									});


		/// <summary>
		///     Context menu command to move the current row down.
		/// </summary>
		public ICommand MoveDownCommand
			=> OnDemandRelayCommand(ref _moveDownCommand,
									aRow =>
									{
										var row = aRow as ImportExportRowVM;
										var pos = Rows.IndexOf(row);
										if (pos < (Rows.Count - 1))
										{
											Rows.Remove(row);
											Rows.Insert(pos + 1, row);
										}
									},
									aRow =>
									{
										var row = aRow as ImportExportRowVM;
										var pos = Rows.IndexOf(row);
										return pos < (Rows.Count - 1);
									});


		/// <summary>
		///     Command handler for the delete row context menu command.
		/// </summary>
		public ICommand DeleteRowCommand
			=> OnDemandRelayCommand(ref _deleteRowCommand,
									aRow =>
									{
										if (aRow is ImportExportRowVM row)
										{
											Rows.Remove(row);
										}
									});

		#endregion

		#region -- Helpers --

		private void LoadUserSettings()
		{
		}


		private void SaveUserSettings() => Settings.Default.Save();


		/// <summary>
		///     Clears the table and file name after prompting to save existing data.
		/// </summary>
		private void NewFile()
		{
			if (!ConfirmDiscardData())
			{
				return;
			}

			_dispatcher.BeginInvoke(() =>
			{
				SelectedRow = null;
				Rows = new ObservableCollection<ImportExportRowVM>();
				CurrentFilePath = null;
				IsSaved = true;
			});
		}


		private void OpenFile()
		{
			if (!ConfirmDiscardData())
			{
				return;
			}

			var settingsCollection = Settings.Default.ImportExportMethodSettings;
			ImportExportMethodSettings settings = null;
			if (null != _lastDevice)
			{
				settings = settingsCollection?.FindOrCreateSettings(_lastDevice.GetIdentity());
			}

			var dlg = new FileBrowserDialogParams
			{
				Action = FileBrowserDialogParams.FileActionType.Open,
				CheckFileExists = true,
				Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*",
				InitialDirectory = settings?.LastAccessPath,
				Title = "Open CSV files"
			};
			RequestDialogOpen?.Invoke(this, dlg);
			if (!dlg.DialogResult.HasValue || !dlg.DialogResult.Value)
			{
				return;
			}

			var rows = new ObservableCollection<ImportExportRowVM>();
			var isSaved = true;
			string errorMessage = null;

			try
			{
				var data = DataTable.New.ReadLazy(dlg.Filename);

				if (!data.ColumnNames.Contains("Name"))
				{
					errorMessage = "This file is not in the expected format; there is no 'Name' column.";
				}
				else if (!data.ColumnNames.Contains("Controller Type"))
				{
					errorMessage = "This file is not in the expected format; there is no 'Controller Type' column.";
				}
				else if (!data.ColumnNames.Contains("Controller Version"))
				{
					errorMessage = "This file is not in the expected format; there is no 'Controller Version' column.";
				}
				else
				{
					foreach (var row in data.Rows)
					{
						var name = row["Name"];
						var mode = row["Controller Type"];
						var version = int.Parse(row["Controller Version"]);
						var set = new TunerParameterSet(mode);
						if (TuningModeNames.USERPID == mode)
						{
							set.UnitConverter = PidTunerV3.PidUnitTable;
						}
						else
						{
							set.UnitConverter = _lastDevice?.UnitConverter.UnitConverter ?? UnitConverter.Default;
						}

						foreach (var paramName in row.ColumnNames)
						{
							// Assume columns other than the administrative ones are data (tuning parameters).
							if (("Name" == paramName)
							|| ("Controller Type" == paramName)
							|| ("Controller Version" == paramName))
							{
								continue;
							}

							if (paramName.EndsWith("units"))
							{
								continue;
							}

							var paramStr = row[paramName];

							if (double.TryParse(paramStr,
												NumberStyles.Float,
												CultureInfo.CurrentCulture,
												out var paramValue)
							|| double.TryParse(paramStr,
											   NumberStyles.Float,
											   CultureInfo.InvariantCulture,
											   out paramValue))
							{
								var unitColumnName = $"{paramName} units";
								Unit unit = null;
								if (row.ColumnNames.Contains(unitColumnName))
								{
									var abbrev = row[unitColumnName];
									unit = set.UnitConverter.FindUnitBySymbol(abbrev);
									if (null == unit)
									{
										unit = Unit.Dimensionless;
										errorMessage =
											$"Failed to identify unit for symbol '{abbrev}' in column '{unitColumnName}'.";
										break;
									}
								}

								set[paramName] = new Measurement(paramValue, unit);
							}
							else
							{
								errorMessage =
									$"Could not parse a value in column {paramName}: '{paramStr}' could not be interpreted as a number.";
								break;
							}
						}

						if (null != errorMessage)
						{
							break;
						}

						var newRow = CreateRow(set, name);
						newRow.ControllerVersion = version;
						newRow.PropertyChanged += Row_PropertyChanged;
						rows.Add(newRow);
					}
				}

				if (null != settings)
				{
					settings.LastAccessPath = Path.GetDirectoryName(dlg.Filename);
				}
			}
			catch (IOException aIoException)
			{
				_log.Error("Failed to read CSV data from " + dlg.Filename, aIoException);
				errorMessage = $"There was an error trying to read the file: {aIoException.Message}";
			}


			_dispatcher.BeginInvoke(() =>
			{
				SelectedRow = null;
				Rows = rows;
				CurrentFilePath = dlg.Filename;
				IsSaved = isSaved;
			});

			_eventLog.LogEvent("Loaded parameters from a file");

			if (!string.IsNullOrEmpty(errorMessage))
			{
				var mbvm = new MessageBoxParams
				{
					Caption = "Error reading file",
					Message = errorMessage,
					Icon = MessageBoxImage.Information,
					Buttons = MessageBoxButton.OK
				};

				RequestDialogOpen?.Invoke(this, mbvm);
			}
		}


		private bool SaveFileAs(string aPath)
		{
			var settingsCollection = Settings.Default.ImportExportMethodSettings;
			ImportExportMethodSettings settings = null;
			if (null != _lastDevice)
			{
				settings = settingsCollection?.FindOrCreateSettings(_lastDevice.GetIdentity());
			}

			if (string.IsNullOrEmpty(aPath) || !File.Exists(aPath))
			{
				var dlg = new FileBrowserDialogParams
				{
					Action = FileBrowserDialogParams.FileActionType.Save,
					CheckFileExists = false,
					Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*",
					InitialDirectory = settings?.LastAccessPath,
					Title = "Save CSV file",
					PromptForOverwrite = true
				};
				RequestDialogOpen?.Invoke(this, dlg);
				if (!dlg.DialogResult.HasValue || !dlg.DialogResult.Value)
				{
					return false;
				}

				aPath = dlg.Filename;
			}

			var rows = Rows as IEnumerable<ImportExportRowVM> ?? new ImportExportRowVM[0];
			var count = rows.Count();
			var nameCol = new Column("Name", count);
			var modeCol = new Column("Controller Type", count);
			var versionCol = new Column("Controller Version", count);
			var paramColumns = new Dictionary<string, Column>();

			var i = 0;
			foreach (var row in rows)
			{
				nameCol.Values[i] = row.Name;
				modeCol.Values[i] = row.Mode;
				versionCol.Values[i] = row.ControllerVersion.ToString();

				var set = row.ParamSet;
				foreach (var paramName in set.ParameterNames)
				{
					var paramValue = set[paramName];

					if (!paramColumns.ContainsKey(paramName))
					{
						paramColumns[paramName] = new Column(paramName, count);
					}

					var column = paramColumns[paramName];
					column.Values[i] = paramValue.Value.ToString();

					if (Unit.Dimensionless != paramValue.Unit)
					{
						var unitColumnName = $"{paramName} units";

						if (!paramColumns.ContainsKey(unitColumnName))
						{
							paramColumns[unitColumnName] = new Column(unitColumnName, count);
						}

						var unitColumn = paramColumns[unitColumnName];
						unitColumn.Values[i] = paramValue.Unit.Abbreviation;
					}
				}

				i++;
			}

			var cols = new List<Column>
			{
				nameCol,
				modeCol,
				versionCol
			};

			foreach (var colName in paramColumns.Keys.OrderBy(n => n))
			{
				cols.Add(paramColumns[colName]);
			}

			var data = new MutableDataTable { Columns = cols.ToArray() };
			var success = false;

			try
			{
				data.SaveCSV(aPath);
				success = true;
				if (null != settings)
				{
					settings.LastAccessPath = Path.GetDirectoryName(aPath);
				}

				_dispatcher.BeginInvoke(() =>
				{
					CurrentFilePath = aPath;
					IsSaved = true;
				});

				_eventLog.LogEvent("Saved parameters to a file");
			}
			catch (IOException aException)
			{
				_log.Error("Failed to save CSV data to " + aPath, aException);
				var mbvm = new MessageBoxParams
				{
					Caption = "Error saving file",
					Message = "There was an error saving data to " + aPath + ": " + aException.Message,
					Icon = MessageBoxImage.Error,
					Buttons = MessageBoxButton.OK
				};

				RequestDialogOpen?.Invoke(this, mbvm);
			}

			return success;
		}


		private void Row_PropertyChanged(object aSender, PropertyChangedEventArgs aArgs) => IsSaved = false;


		private void Rows_CollectionChanged(object aSender, NotifyCollectionChangedEventArgs aArgs) => IsSaved = false;


		private ImportExportRowVM CreateRow(ITunerParameterSet aSet, string aName = null)
		{
			var row = new ImportExportRowVM(aSet, aName);
			return row;
		}


		private bool ConfirmDiscardData()
		{
			if (IsSaved)
			{
				return true;
			}

			var mbvm = new CustomMessageBoxVM(
				"You have unsaved CSV data in the Servo Tuning tab. Save it now or discard it?",
				"Unsaved data");
			if (!string.IsNullOrEmpty(_currentFilePath) && File.Exists(_currentFilePath))
			{
				mbvm.AddOption("Save", 1);
			}

			mbvm.AddOption("Save As", 2);
			mbvm.AddOption("Discard", 3);
			mbvm.DialogResult = 0;

			RequestDialogOpen?.Invoke(this, mbvm);

			switch ((int) mbvm.DialogResult)
			{
				case 0:
					return false;
				case 1:
					return SaveFileAs(_currentFilePath);
				case 2:
					return SaveFileAs(null);
				case 3:
					return true;
				default:
					return false;
			}
		}

		#endregion

		#region -- Fields --

		public const string DISPLAY_NAME = "Import / Export";
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Servo Tuner tab");

		private readonly IDispatcher _dispatcher;
		private RelayCommand _newFileCommand;
		private RelayCommand _openFileCommand;
		private RelayCommand _saveAsCommand;
		private RelayCommand _moveUpCommand;
		private RelayCommand _moveDownCommand;
		private RelayCommand _deleteRowCommand;

		private DeviceProperties _deviceProperties;
		private ZaberDevice _lastDevice;

		private string _currentFilePath;
		private bool _isSaved = true;

		private ObservableCollection<ImportExportRowVM> _rows;
		private ImportExportRowVM _selectedRow;

		#endregion
	}
}

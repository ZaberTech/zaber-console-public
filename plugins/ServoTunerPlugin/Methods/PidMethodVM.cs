﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using log4net;
using Zaber;
using Zaber.Tuning;
using Zaber.Tuning.Servo;
using Zaber.Units;
using Zaber.Units.UI.WPF.Controls;
using ZaberConsole.Plugins.ServoTuner.Properties;
using ZaberWpfToolbox;
using Measurement = Zaber.Units.Measurement;

namespace ZaberConsole.Plugins.ServoTuner.Methods
{
	[DisplayName(DISPLAY_NAME)]
	[SortOrder(10.0)]
	public class PidMethodVM : CalculatorMethodBase
	{
		#region -- Initialization --

		public PidMethodVM()
		{
			_dispatcher = DispatcherHelper.Dispatcher;

			var parms = CreateUserPidParameterSet();
			UserParams = new ObservableCollection<PidMethodUserParamVM>();
			for (var i = 0; i < _paramOrder.Length; ++i)
			{
				var newVM = new PidMethodUserParamVM();
				ConfigureParameterVM(newVM, _help[i], parms, _paramOrder[i]);
				UserParams.Add(newVM);
			}

			LoadUserSettings();
		}

		#endregion

		#region -- View data --

		/// <summary>
		///     The user-editable input parameters.
		/// </summary>
		public ObservableCollection<PidMethodUserParamVM> UserParams { get; }

		#endregion

		public const string DISPLAY_NAME = "PID";

		#region -- Base class overrides --

		/// <summary>
		///     Called by the plugin tab when the selected device changes.
		/// </summary>
		/// <param name="aConversation">
		///     Global conversation instance for the newly selected device. This is provided even if the
		///     device is not a servo stage.
		/// </param>
		/// <param name="aServoProperties">
		///     Calculated servo properties of the device. This is only provided if the device is
		///     tunable and the properties can be determined.
		/// </param>
		public override void SetTargetDevice(Conversation aConversation, DeviceProperties aServoProperties)
		{
			SaveUserSettings();
			_lastDevice = aConversation?.Device;
			_deviceProperties = aServoProperties;

			// Recalcuate device unit conversions for the new device.
			_tuner = null;
			_reverseTuner = null;

			if (null != aServoProperties)
			{
				var parms = CreateUserPidParameterSet(aServoProperties);
				for (var i = 0; i < _paramOrder.Length; ++i)
				{
					ConfigureParameterVM(UserParams[i], _help[i], parms, _paramOrder[i]);
				}

				var selector = TunerSelector.Instance;
				var forwardTunerType = selector
								   .FindTunerTypesByAlgorithmAndOutput(PidTunerV3.ALGORITHM_NAME,
																	   _lastDevice
																	   .DeviceType.TuningData?.ControllerVersions)
								   .FirstOrDefault();
				if (null != forwardTunerType)
				{
					_tuner = Activator.CreateInstance(forwardTunerType) as ITuner;
				}

				var reverseTunerType = selector
								   .FindTunerTypesByAlgorithmAndInput(PidReverseTunerV3.ALGORITHM_NAME,
																	  _lastDevice
																	  .DeviceType.TuningData?.ControllerVersions)
								   .FirstOrDefault();
				if (null != reverseTunerType)
				{
					_reverseTuner = Activator.CreateInstance(reverseTunerType) as ITuner;
				}
			}

			LoadUserSettings();

			if (null == _deviceProperties)
			{
				CanRead = CanWrite = false;
				ReadButtonTip = WriteButtonTip = NOT_SUPPORTED_TIP;
			}
			else
			{
				if (null == _tuner)
				{
					CanWrite = false;
					WriteButtonTip = "This tab cannot convert data to a format supported by the device.";
				}
				else
				{
					CanWrite = true;
					WriteButtonTip = DEFAULT_WRITE_TIP;
				}

				if (null == _reverseTuner)
				{
					CanRead = false;
					ReadButtonTip = "This tab cannot convert data from the device's format.";
				}
				else
				{
					CanRead = true;
					ReadButtonTip = DEFAULT_READ_TIP;
				}
			}
		}


		/// <summary>
		///     Get tuning parameters to write to the device, or set parameters that have been read from the device.
		///     Throws an exception (with a human-readable help message) if the operation is not valid at this time or
		///     if there is a calculation error. The expectatation is that get will be called when the user wants to
		///     write their currently edited values to the device, and set will be called when they read from the device.
		///     The device communication is handled by the plugin tab, not the method. Call  <see cref="CanRead" />  to see if
		///     setting this
		///     is legal, and <see cref="CanWrite" /> to see if getting is legal.
		/// </summary>
		public override ITunerParameterSet DeviceParameters
		{
			get => _tuner?.CalculateParameters(_deviceProperties, null, GetParamsFromUI());
			set
			{
				if ((null != value) && (null != _deviceProperties) && (null != _reverseTuner))
				{
					var p = new TunerParameterSet(TuningModeNames.FFPID) { UnitConverter = PidTunerV3.PidUnitTable };
					p.Merge(value);

					var uiParams = _reverseTuner.CalculateParameters(_deviceProperties, null, p);

					SetParamsInUI(uiParams);
				}
			}
		}


		/// <summary>
		///     Invoked when the plugin is closing; allows methods to prompt the user to save data.
		/// </summary>
		public override void OnClosing() => SaveUserSettings();


		/// <summary>
		///     Invoked when user settings are reset. The method should clear all saved settings.
		/// </summary>
		public override void ClearSettings() => Settings.Default.PidMethodSettings?.Clear();

		#endregion

		#region -- Helpers --

		private void LoadUserSettings()
		{
			if (null != _lastDevice)
			{
				var settingsCollection = Settings.Default.PidMethodSettings;
				if (null == settingsCollection)
				{
					settingsCollection = new PidMethodSettingsCollection();
					Settings.Default.PidMethodSettings = settingsCollection;
				}

				var settings = settingsCollection.FindOrCreateSettings(_lastDevice.GetIdentity());

				// Serialization does not preserve custom unit definitions.
				var parms = settings.PidParameters;
				if (null != parms)
				{
					parms.ReplaceUnitConverter(PidTunerV3.PidUnitTable);
					SetParamsInUI(parms);
				}
			}
		}


		private void SaveUserSettings()
		{
			if (null != _lastDevice)
			{
				var settingsCollection = Settings.Default.PidMethodSettings;
				if (null == settingsCollection)
				{
					settingsCollection = new PidMethodSettingsCollection();
					Settings.Default.PidMethodSettings = settingsCollection;
				}

				var settings = settingsCollection.FindOrCreateSettings(_lastDevice.GetIdentity());
				settings.PidParameters = GetParamsFromUI();
				Settings.Default.Save();
			}
		}


		private TunerParameterSet GetParamsFromUI()
		{
			var result = new TunerParameterSet(TuningModeNames.FFPID) { UnitConverter = PidTunerV3.PidUnitTable };

			for (var i = 0; i < _paramOrder.Length; ++i)
			{
				result[_paramOrder[i]] = UserParams[i].Value.Measurement;
			}

			return result;
		}


		// Configures a "user" PID parameter set. User meaning what's displayed by this tuning 
		// method tab - just the four basic parameters, not all the device parameters.
		// This also configures unit conversions for the user parameters.
		private static TunerParameterSet CreateUserPidParameterSet(DeviceProperties aServoProperties = null)
		{
			var result = new TunerParameterSet(TuningModeNames.USERPID);
			var uc = PidTunerV3.PidUnitTable;
			result.UnitConverter = uc;

			result.Add(PidTunerV3.PARAM_KP, new Measurement(0.0, uc.FindUnitBySymbol("N/m")));
			result.Add(PidTunerV3.PARAM_KI, new Measurement(0.0, uc.FindUnitBySymbol("N/m⋅s")));
			result.Add(PidTunerV3.PARAM_KD, new Measurement(0.0, uc.FindUnitBySymbol("N⋅s/m")));
			result.Add(PidTunerV3.PARAM_FC, new Measurement(0.0, uc.FindUnitBySymbol("Hz")));

			if (null != aServoProperties)
			{
				CalculateDeviceUnits(aServoProperties, result);
			}

			return result;
		}


		private static void CalculateDeviceUnits(DeviceProperties aDevice, TunerParameterSet aSet)
		{
			// Create a new unit conversion table for this device.
			var parser = ComposedUnitParser.NewDefaultUnitParser();
			var uc = new UnitConverter(parser, parser.GetUnitRegistry(), PidTunerV3.PidUnitTable);
			aSet.UnitConverter = uc;

			Unit kpUnit = null;
			if (Dimension.Angle == aDevice.PositionUnit.Dimension)
			{
				var kpBaseUnit = uc.FindUnitBySymbol("Nm/°");
				var newtonmeters = uc.FindUnitBySymbol("Nm");
				var degrees = uc.FindUnitBySymbol("°");

				var torqueUnitsInNewtonMeters = aDevice.ForceUnit.GetTransformationTo(newtonmeters).Transform(1.0);
				var positionUnitsInDegrees = aDevice.PositionUnit.GetTransformationTo(degrees).Transform(1.0);
				kpUnit = new ScaledShiftedUnit(PidTunerV3.PARAM_KP_UNITS,
											   "Device-specific gain units",
											   kpBaseUnit,
											   torqueUnitsInNewtonMeters / positionUnitsInDegrees);
			}
			else if (Dimension.Length == aDevice.PositionUnit.Dimension)
			{
				var kpBaseUnit = uc.FindUnitBySymbol("N/m");
				var newtons = uc.FindUnitBySymbol("N");
				var meters = uc.FindUnitBySymbol("m");

				var forceUnitsInNewtons = aDevice.ForceUnit.GetTransformationTo(newtons).Transform(1.0);
				var positionUnitsInMeters = aDevice.PositionUnit.GetTransformationTo(meters).Transform(1.0);
				kpUnit = new ScaledShiftedUnit(PidTunerV3.PARAM_KP_UNITS,
											   "Device-specific gain units",
											   kpBaseUnit,
											   forceUnitsInNewtons / positionUnitsInMeters);
			}
			else
			{
				throw new InvalidOperationException("PID tuning does not support device position type :"
												+ aDevice.PositionUnit.Dimension.Name);
			}

			kpUnit.UseSIPrefixes = false;
			uc.Registry.RegisteredUnits[PidTunerV3.PARAM_KP_UNITS] = kpUnit;

			// Fixup any measurements using the serialization placeholder.
			foreach (var paramName in _paramOrder)
			{
				var m = aSet[paramName];
				if (PidTunerV3.PARAM_KP_UNITS == m.Unit.Abbreviation)
				{
					aSet[paramName] = new Measurement(m.Value, kpUnit);
				}
			}

			// Fix up Kp itself to not use the dimensionless unit.
			var kp = aSet[PidTunerV3.PARAM_KP];
			if (Dimension.None == kp.Unit.Dimension)
			{
				aSet[PidTunerV3.PARAM_KP] = new Measurement(kp.Value, kpUnit);
			}
		}


		private void SetParamsInUI(ITunerParameterSet aParams)
		{
			if (null != aParams)
			{
				for (var i = 0; i < _paramOrder.Length; ++i)
				{
					var vm = UserParams[i].Value;
					var paramName = _paramOrder[i];

					// Try to convert the read-back values to the currently selected
					// units of measure.
					if (null != vm.SelectedUnit)
					{
						var displayValue = aParams[paramName].GetValueAs(vm.SelectedUnit);
						vm.Quantity = (decimal) displayValue;
					}
					else
					{
						vm.Measurement = aParams[paramName];
					}
				}
			}
		}


		private static void ConfigureParameterVM(PidMethodUserParamVM aVM, string aHelpText, ITunerParameterSet aSet,
												 string aParamName)
		{
			var mbox = new MeasurementBoxVM();
			var measurement = aSet[aParamName];
			mbox.UnitConverter = aSet.UnitConverter;
			var dim = measurement.Unit?.Dimension;
			if ((null != dim) && (Dimension.None != dim) && (Dimension.Unknown != dim))
			{
				var units = mbox.UnitConverter.FindUnits(measurement.Unit.Dimension, _usefulPrefixes);
				mbox.PopulateUnits(units);
			}
			else
			{
				mbox.PopulateUnits(new Unit[] { });
			}

			mbox.Measurement = measurement;

			aVM.Name = aParamName;
			aVM.Help = aHelpText;
			aVM.Value = mbox;
		}

		#endregion

		#region -- Fields --

		private static readonly string[] _paramOrder =
		{
			PidTunerV3.PARAM_KP, PidTunerV3.PARAM_KI, PidTunerV3.PARAM_KD, PidTunerV3.PARAM_FC
		};

		private static readonly string[] _help =
		{
			// Help for KP
			Resources.STR_KP_HELP,

			// Help for KI
			Resources.STR_KI_HELP,

			// Help for KD
			Resources.STR_KD_HELP,

			// Help for FC
			Resources.STR_FC_HELP
		};

		private static readonly Prefix[] _usefulPrefixes = { Prefix.Kilo, Prefix.None };

		private static ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private static object _syncRoot = new object();

		private IDispatcher _dispatcher;

		private ZaberDevice _lastDevice;
		private DeviceProperties _deviceProperties;
		private ITuner _tuner;
		private ITuner _reverseTuner;

		#endregion
	}
}

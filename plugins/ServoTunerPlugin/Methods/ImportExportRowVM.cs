﻿using System.Linq;
using Zaber.Tuning.Servo;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.ServoTuner.Methods
{
	/// <summary>
	///     ViewModel for rows in the CSV tuner parameter loader/saver table.
	/// </summary>
	public class ImportExportRowVM : ObservableObject
	{
		#region -- Initialization --

		/// <summary>
		///     Initialize the VM with an existing parameter set.
		/// </summary>
		/// <param name="aParamSet">Parameter set to store in the row.</param>
		/// <param name="aName">Optional name for the set.</param>
		public ImportExportRowVM(ITunerParameterSet aParamSet, string aName = null)
		{
			ParamSet = aParamSet;
			Name = aName;
		}


		/// <summary>
		///     Parameter set associated with this row. May be edited by user
		///     after initialization.
		/// </summary>
		public ITunerParameterSet ParamSet { get; }

		#endregion

		#region -- View data --

		/// <summary>
		///     Name for the row.
		/// </summary>
		public string Name
		{
			get => _name;
			set => Set(ref _name, value, nameof(Name));
		}


		/// <summary>
		///     Name of the servo controller in firmware.
		/// </summary>
		public string Mode => ParamSet.ModeName;


		/// <summary>
		///     Implementation version of the controller mode.
		/// </summary>
		public int ControllerVersion { get; set; }


		/// <summary>
		///     Representation of the parameters of this row.
		/// </summary>
		public string Parameters
		{
			get
			{
				return _parameters
				   ?? (_parameters = string.Join(", ",
												 ParamSet.ParameterNames.OrderBy(n => n)
													  .Select(n => n + " = " + ParamSet[n].ToString())));
			}
		}

		#endregion

		#region -- Fields --

		private string _name;
		private string _parameters;

		#endregion
	}
}

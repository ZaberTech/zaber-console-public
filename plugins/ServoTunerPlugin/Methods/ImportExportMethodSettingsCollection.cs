﻿using System;
using Zaber.Application;

namespace ZaberConsole.Plugins.ServoTuner.Methods
{
	/// <summary>
	///     Per-device saved settings for the CSV calculation method.
	/// </summary>
	[Serializable]
	public class ImportExportMethodSettingsCollection : PerDeviceSettingsCollection<ImportExportMethodSettings>
	{
	}
}

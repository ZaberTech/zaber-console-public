﻿using System;
using Zaber.Application;

namespace ZaberConsole.Plugins.ServoTuner.Methods
{
	/// <summary>
	///     Per-device saved settings for the Simple calculation method.
	/// </summary>
	[Serializable]
	public class SimpleMethodSettingsCollection : PerDeviceSettingsCollection<SimpleMethodSettings>
	{
	}
}

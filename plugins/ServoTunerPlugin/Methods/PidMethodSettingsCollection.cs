﻿using System;
using Zaber.Application;

namespace ZaberConsole.Plugins.ServoTuner.Methods
{
	/// <summary>
	///     Per-device saved settings for the PID calculation method.
	/// </summary>
	[Serializable]
	public class PidMethodSettingsCollection : PerDeviceSettingsCollection<PidMethodSettings>
	{
	}
}

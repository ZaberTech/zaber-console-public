﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Input;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Zaber;
using Zaber.Serialization;
using Zaber.Tuning;
using Zaber.Tuning.Servo;
using Zaber.Units;
using Zaber.Units.UI.WPF.Controls;
using ZaberConsole.Plugins.ServoTuner.Methods.ParameterControls;
using ZaberConsole.Plugins.ServoTuner.Properties;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Dialogs;
using Measurement = Zaber.Units.Measurement;

namespace ZaberConsole.Plugins.ServoTuner.Methods
{
	[DisplayName(DISPLAY_NAME)]
	[SortOrder(1.0)]
	public class SimpleCalculatorMethodVM : CalculatorMethodBase, IDialogClient
	{
		public const string DISPLAY_NAME = "Simple";

		#region -- Setup --

		public SimpleCalculatorMethodVM()
		{
			_dispatcher = DispatcherHelper.Dispatcher;
			LoadInertia = new MeasurementBoxVM(UnitConverter.Default, Dimension.Weight);
			CarriageInertia = new MeasurementBoxVM(UnitConverter.Default, Dimension.Weight);
			NewAcceleration = new MeasurementBoxVM(UnitConverter.Default, Dimension.Acceleration);
			ReadButtonTip =
				"This tuning calculation method is not reversible, so reading back from the device is not supported.";

			// Find all user control types that can be dynamically added.
			foreach (var t in GetType()
						  .Assembly.GetExportedTypes()
						  .Where(t => !t.IsAbstract && typeof(IUserParameterControl).IsAssignableFrom(t)))
			{
				foreach (var attr in t.GetCustomAttributes(typeof(TunerUserParameterControlAttribute), true))
				{
					var pca = attr as TunerUserParameterControlAttribute;
					_userControlTypes[pca.DataType] = t;
				}
			}

			LoadUserSettings();
		}

		#endregion

		#region -- Base class overrides --

		/// <summary>
		///     Called by the plugin tab when the selected device changes.
		/// </summary>
		/// <param name="aConversation">
		///     Global conversation instance for the newly selected device. This is provided even if the
		///     device is not a servo stage.
		/// </param>
		/// <param name="aServoProperties">
		///     Calculated servo properties of the device. This is only provided if the device is
		///     tunable and the properties can be determined.
		/// </param>
		public override void SetTargetDevice(Conversation aConversation, DeviceProperties aServoProperties)
		{
			LoadInertia.PropertyChanged -= OnMassPropertyChanged;
			CarriageInertia.PropertyChanged -= OnMassPropertyChanged;
			NewAcceleration.PropertyChanged -= OnNewAccelerationPropertyChanged;
			SaveUserSettings();

			// Update mass unit options to use most specific unit converter.
			var unitConverter = aConversation?.Device.DeviceType.UnitConverter.UnitConverter;
			if (null != unitConverter)
			{
				LoadInertia.UnitConverter = unitConverter;
				CarriageInertia.UnitConverter = unitConverter;
				NewAcceleration.UnitConverter = unitConverter;

				// TODO (Soleil 2018-04-17): This uses the old unit system for now to avoid 
				// duplicate symbols between the ASCII and Unicode sets. When support for
				// prettyprinting of unit symbols is added, this should be avoidable.
				IEnumerable<UnitOfMeasure> accelerationUnits = null;
				if (aConversation?.Device.DeviceType.IsRotaryDevice ?? false)
				{
					_inertiaUnits = unitConverter.FindUnits(Dimension.RotationalInertia, _siPrefixes).ToList();
					accelerationUnits =
						aConversation.Device.UnitConverter.FindUnits(MotionType.Rotary, MeasurementType.Acceleration);
				}
				else
				{
					_inertiaUnits = unitConverter.FindUnits(Dimension.Inertia, _siPrefixes).ToList();
					accelerationUnits =
						aConversation.Device.UnitConverter.FindUnits(MotionType.Linear, MeasurementType.Acceleration);
				}

				_dispatcher.Invoke(() =>
				{
					LoadInertia.PopulateUnits(_inertiaUnits);
					CarriageInertia.PopulateUnits(_inertiaUnits);
					NewAcceleration.PopulateUnits(accelerationUnits
											  .Where(uom => !uom.Abbreviation.ToLowerInvariant().Contains("step"))
											  .Select(uom => uom.Unit));
				});
			}

			_deviceProperties = aServoProperties;

			// If the device has tuning data in the database, use that to select the tuning algorithm.
			_tuner = null;
			var _newControls = new List<IUserParameterControl>();
			_userControlsByParameterName.Clear();
			var tuningData = aConversation?.Device.DeviceType.TuningData;
			if ((null != tuningData?.GuidanceData) && (null != _deviceProperties))
			{
				JObject j = null;
				try
				{
					using (var reader = new JsonTextReader(new StringReader(tuningData.GuidanceData)))
					{
						j = JObject.Load(reader);
					}
				}
				catch (JsonReaderException aException)
				{
					_log.Error("Database guidance data for the selected device is corrupt.", aException);
				}

				JToken array = null;
				if (null != j)
				{
					array = j["GuidanceData"];
				}

				var tunerSelector = TunerSelector.Instance;

				Type applicableTunerType = null;
				JToken applicableTunerData = null;
				string applicableModeName = null;
				if (null != array)
				{
					foreach (var token in array)
					{
						var alg = token.Value<string>("Algorithm");

						// If this algorithm is known, save it as the best candidate and keep searching.
						// At present there is no mechanism in the UI for selecting between multiple alternatives,
						// so we arbitrary select the first one.
						var applicableType = tunerSelector
										 .FindTunerTypesByAlgorithmAndOutput(alg, tuningData?.ControllerVersions)
										 .FirstOrDefault();
						if (null != applicableType)
						{
							var applicableMode =
								tunerSelector.SelectOutputMode(applicableType, tuningData.ControllerVersions);
							if ((null != applicableType) && !string.IsNullOrEmpty(applicableMode))
							{
								applicableTunerType = applicableType;
								applicableTunerData = token;
								applicableModeName = applicableMode;
							}
						}
					}
				}

				// If we found a candidate algorithm, instantiate it and set up its UI controls.
				if (null != applicableTunerType)
				{
					var create = true;
					if (null != applicableTunerData)
					{
						applicableTunerData = applicableTunerData["UserParameters"];
					}

					if (null != applicableTunerData)
					{
						foreach (var parms in applicableTunerData)
						{
							var controlTypeName = parms.Value<string>("DataType");
							var parameterName = parms.Value<string>("ParameterName");
							if (!_userControlTypes.TryGetValue(controlTypeName, out var controlType))
							{
								_log.Error("Unknown user control data type " + controlTypeName);
								create = false;
								break;
							}

							if (string.IsNullOrEmpty(parameterName))
							{
								_log.Error("User control data type " + controlTypeName + " has no parameter name.");
								create = false;
								break;
							}

							var controls = Activator.CreateInstance(controlType) as IUserParameterControl;
							controls.Configure(parms);
							_newControls.Add(controls);
							_userControlsByParameterName[parameterName] = controls;
						}
					}

					if (create)
					{
						_tuner = Activator.CreateInstance(applicableTunerType) as ITuner;
						_tuningMode = applicableModeName;
					}
					else
					{
						_newControls.Clear();
					}
				}
			}

			_dispatcher.BeginInvoke(() =>
			{
				UserParameterControls =
					new ObservableCollection<IUserParameterControl>(_newControls);
			});

			_lastConversation = aConversation;

			_defaultCarriageInertia = tuningData?.Inertia;
			OnPropertyChanged(nameof(DefaultCarriageInertia));

			LoadUserSettings();
			UpdateCanWrite();

			if ((null != aConversation) && aConversation.Device.IsSingleDevice)
			{
				UpdateAccelerationRecommendation();
				UpdateActualAcceleration();
			}
			else
			{
				CanChangeAcceleration = false;
			}

			LoadInertia.PropertyChanged += OnMassPropertyChanged;
			CarriageInertia.PropertyChanged += OnMassPropertyChanged;
			NewAcceleration.PropertyChanged += OnNewAccelerationPropertyChanged;
		}


		/// <summary>
		///     Get tuning parameters to write to the device, or set parameters that have been read from the device.
		///     Throws an exception (with a human-readable help message) if the operation is not valid at this time or
		///     if there is a calculation error. The expectatation is that get will be called when the user wants to
		///     write their currently edited values to the device, and set will be called when they read from the device.
		///     The device communication is handled by the plugin tab, not the method. Call  <see cref="CanRead" />  to see if
		///     setting this
		///     is legal, and <see cref="CanWrite" /> to see if getting is legal.
		/// </summary>
		public override ITunerParameterSet DeviceParameters
		{
			get
			{
				if (null == _tuner)
				{
					throw new NullReferenceException(
						"Could not find an appropriate mass conversion method for this device.");
				}

				if ((null == CarriageInertia.SelectedUnit) || (null == LoadInertia.SelectedUnit))
				{
					throw new InvalidOperationException("You must select mass units for both the load and carriage.");
				}

				// We use the database carriage inertia and combine the user-entered 
				// carriage inertia into the load in order to avoid incorrect
				// tuning results in the case where users set the carriage 
				// inertia to zero in the GUI. See ticket #6252.
				_deviceProperties.CarriageInertia = new Measurement(_defaultCarriageInertia);

				var loadParams = new LoadProperties
				{
					// This odd expression is meant to make the load intertia reflect the user-entered
					// load inertia plus any difference between the value they entered for carriage intertia
					// and what we are using as the real carriage inertia. This came about because some
					// users had the habit of putting the load value in the carriage box.
					// See ticket #7087.
					LoadInertia = LoadInertia.Measurement + CarriageInertia.Measurement - _deviceProperties.CarriageInertia
				};

				// Merge results from any user control parameters.
				var userParams = new TunerParameterSet(_tuningMode)
				{
					UnitConverter = _lastConversation.Device.UnitConverter.UnitConverter
				};

				foreach (var userControl in UserParameterControls)
				{
					var values = userControl.GetTunerParameters();
					userParams.Merge(values);
				}

				// TODO (Soleil 2018/02/09): Use the input set name of the tuner to verify
				// that the required set of parameters have been assembled at this point.
				// This requires a mapping of set type names to expected parameters somewhere.
				// Currently only the tuner code knows what it expects to find in userParams.

				return _tuner.CalculateParameters(_deviceProperties, loadParams, userParams);
			}
			set => throw new InvalidOperationException(
				"This tuning method does not support reverse conversion from device settings.");
		}


		/// <summary>
		///     Invoked when the plugin is closing; allows methods to prompt the user to save data.
		/// </summary>
		public override void OnClosing() => SaveUserSettings();


		/// <summary>
		///     Invoked when user settings are reset. The method should clear all saved settings.
		/// </summary>
		public override void ClearSettings() => Settings.Default.SimpleMethodSettings?.Clear();

		#endregion

		#region -- IDialogClient implementation --

		/// <summary>
		///     Event to invoke in order to open a dialog box.
		/// </summary>
		public event RequestDialogChange RequestDialogOpen;

		/// <summary>
		///     Event to invoke in order to close an existing dialog box.
		/// </summary>
		#pragma warning disable CS0067
		public event RequestDialogChange RequestDialogClose;
		#pragma warning restore CS0067

		#endregion

		#region -- View properties --

		/// <summary>
		///     Observable property for views to sync LoadMassMeasurement.
		/// </summary>
		public MeasurementBoxVM LoadInertia
		{
			get => _loadInertia;
			private set => Set(ref _loadInertia, value, nameof(LoadInertia));
		}


		/// <summary>
		///     Observable property for views to sync CarriageMassMeasurement.
		/// </summary>
		public MeasurementBoxVM CarriageInertia
		{
			get => _carriageInertia;
			private set => Set(ref _carriageInertia, value, nameof(CarriageInertia));
		}


		/// <summary>
		///     Recommended maximum acceleration based on load.
		/// </summary>
		public string RecommendedAcceleration => _recommendedAcceleration?.ToString("N2");


		/// <summary>
		///     Label text giving the default carriage inertia so users can restore it.
		/// </summary>
		public string DefaultCarriageInertia => _defaultCarriageInertia?.ToString();


		/// <summary>
		///     Observable property for views to sync RestoreDefaultCarriageInertiaCommand.
		/// </summary>
		public ICommand RestoreDefaultCarriageInertiaCommand => OnDemandRelayCommand(
			ref _restoreDefaultCarriageInertiaCommand,
			_ =>
			{
				if ((null != CarriageInertia) && (null != _defaultCarriageInertia))
				{
					CarriageInertia.Measurement = _defaultCarriageInertia;
				}
			});


		/// <summary>
		///     Current device acceleration setting.
		/// </summary>
		public string CurrentAcceleration
		{
			get
			{
				if (null != _currentAcceleration)
				{
					if (_currentAcceleration.Value > 0.0)
					{
						return _currentAcceleration?.ToString("N2");
					}

					return "infinite";
				}

				return null;
			}
		}


		/// <summary>
		///     Current acceleration setting on the device.
		/// </summary>
		public MeasurementBoxVM NewAcceleration
		{
			get => _newAcceleration;
			set => Set(ref _newAcceleration, value, nameof(NewAcceleration));
		}


		/// <summary>
		///     Observable property for views to sync CanChangeAcceleration.
		/// </summary>
		public bool CanChangeAcceleration
		{
			get => _canChangeAcceleration;
			set => Set(ref _canChangeAcceleration, value, nameof(CanChangeAcceleration));
		}


		/// <summary>
		///     Controls for setting user inputs to the tuner.
		/// </summary>
		public ObservableCollection<IUserParameterControl> UserParameterControls
		{
			get => _userParameterControls;
			set => Set(ref _userParameterControls, value, nameof(UserParameterControls));
		}


		/// <summary>
		///     Command invoked by the "apply recommendation" button for acceleration.
		/// </summary>
		public ICommand ChangeAccelerationCommand
			=> OnDemandRelayCommand(ref _changeAccelerationCommand,
									_ =>
									{
										if (null != NewAcceleration?.SelectedUnit)
										{
											try
											{
												var measure =
													new Zaber.Measurement(NewAcceleration
																	  .Measurement.Value,
																		  _lastConversation
																		  .Device.UnitConverter
																		  .FindUnitByAbbreviation(NewAcceleration
																							  .SelectedUnit
																							  .Abbreviation));
												_lastConversation.RequestInUnits("set accel",
																				 measure);
												UpdateActualAcceleration();
											}
											catch (Exception aException)
											{
												var mbvm = MessageBoxParams
												.CreateErrorNotification("Error changing acceleration",
																		 "There was an error when trying to change the device's acceleration setting: "
																	 + aException.Message);

												RequestDialogOpen?.Invoke(this,
																		  mbvm);
											}
										}
									});

		public ICommand CopyRecommendedAccelerationCommand
			=> OnDemandRelayCommand(ref _copyRecommendedAccelerationCommand,
									_ =>
									{
										_dispatcher.BeginInvoke(() =>
										{
											NewAcceleration.Measurement =
												_recommendedAcceleration;
										});
									});

		#endregion

		#region -- Helpers --

		private void LoadUserSettings()
		{
			if (null != _lastConversation)
			{
				var settingsCollection = Settings.Default.SimpleMethodSettings;
				if (null == settingsCollection)
				{
					settingsCollection = new SimpleMethodSettingsCollection();
					Settings.Default.SimpleMethodSettings = settingsCollection;
				}

				var identity = _lastConversation.Device.GetIdentity();
				var settings = settingsCollection.FindSettings(identity);
				if (null != settings) // If settings contain inertia data, use that to populate the controls.
				{
					_dispatcher.Invoke(() =>
					{
						LoadInertia.Quantity = settings.LoadMassQuantity;
						LoadInertia.SelectedUnit =
							LoadInertia.Units.FirstOrDefault(u => u.Abbreviation == settings.LoadMassUnitSymbol)
						?? LoadInertia.Units.FirstOrDefault(u => "g" == u.Abbreviation);
						CarriageInertia.Quantity = settings.CarriageMassQuantity;
						CarriageInertia.SelectedUnit =
							CarriageInertia.Units.FirstOrDefault(u => u.Abbreviation == settings.CarriageMassUnitSymbol)
						?? CarriageInertia.Units.FirstOrDefault(u => "g" == u.Abbreviation);
					});

					// Pass saved settings to dynamically created controls.
					if (null != settings.ControlSettings)
					{
						foreach (var pair in settings.ControlSettings)
						{
							if (_userControlsByParameterName.TryGetValue(pair.Key, out var control))
							{
								control.UserSettings = pair.Value.Select(tup => (Tuple<string, object>) tup);
							}
						}
					}
				}
				else
				{
					// Previous settings didn't exist - create new ones.
					var device = _lastConversation.Device;
					settings = settingsCollection.FindOrCreateSettings(identity);
					var td = _lastConversation.Device.DeviceType.TuningData;

					// If there is information in the device database, use that to populate the inertia controls.
					if (null != td?.Inertia)
					{
						_dispatcher.Invoke(() =>
						{
							CarriageInertia =
								new MeasurementBoxVM(device.UnitConverter.UnitConverter, td.Inertia.Unit.Dimension)
								{
									Measurement = new Measurement(td.Inertia)
								};

							LoadInertia =
								new MeasurementBoxVM(device.UnitConverter.UnitConverter, td.Inertia.Unit.Dimension)
								{
									Measurement = new Measurement(0, td.Inertia.Unit)
								};

							PopulateSettingsFromControls(LoadInertia, CarriageInertia, settings);
						});
					}
					else // No data available about inertia; default.
					{
						Dimension d = null;
						d = Dimension.GetByName(device.DeviceType.IsRotaryDevice ? "Rotational Inertia" : "Inertia");

						_dispatcher.Invoke(() =>
						{
							CarriageInertia = new MeasurementBoxVM(device.UnitConverter.UnitConverter, d);
							LoadInertia = new MeasurementBoxVM(device.UnitConverter.UnitConverter, d);

							var baseUnit = CarriageInertia.Units.Where(u => u.IsCoherent).FirstOrDefault()
									   ?? CarriageInertia.Units.FirstOrDefault()
									   ?? UnitConverter.Default.FindUnits(Dimension.Inertia).FirstOrDefault();

							CarriageInertia.Measurement = new Measurement(100, baseUnit);
							LoadInertia.Measurement = new Measurement(0, baseUnit);
							PopulateSettingsFromControls(LoadInertia, CarriageInertia, settings);
						});
					}

					// In the case where saved settings didn't previously exist, just let the
					// dynamically created controls default themselves.
				}
			}
		}


		private void SaveUserSettings()
		{
			if (null != _lastConversation)
			{
				var settingsCollection = Settings.Default.SimpleMethodSettings;
				if (null == settingsCollection)
				{
					settingsCollection = new SimpleMethodSettingsCollection();
					Settings.Default.SimpleMethodSettings = settingsCollection;
				}

				var settings = settingsCollection.FindOrCreateSettings(_lastConversation.Device.GetIdentity());
				PopulateSettingsFromControls(LoadInertia, CarriageInertia, settings);
				Settings.Default.Save();
			}
		}


		private void PopulateSettingsFromControls(MeasurementBoxVM aLoad, MeasurementBoxVM aCarriage,
												  SimpleMethodSettings aSettings)
		{
			// Save inertia information in settings.
			aSettings.LoadMassQuantity = aLoad.Quantity;
			aSettings.LoadMassUnitSymbol = aLoad.SelectedUnit?.Abbreviation;
			aSettings.CarriageMassQuantity = aCarriage.Quantity;
			aSettings.CarriageMassUnitSymbol = aCarriage.SelectedUnit?.Abbreviation;

			// Save user parameters if any.
			aSettings.ControlSettings = new SerializableDictionary<string, List<SerializableTuple<string, object>>>();
			foreach (var pair in _userControlsByParameterName)
			{
				var controlSettings = pair.Value.UserSettings.Select(tup => (SerializableTuple<string, object>) tup)
									   .ToList();
				if (null != controlSettings)
				{
					aSettings.ControlSettings[pair.Key] = controlSettings;
				}
			}
		}


		private void UpdateCanWrite()
		{
			var can = true;
			var msg = DEFAULT_WRITE_TIP;

			if (null == _deviceProperties)
			{
				can = false;
				msg = NOT_SUPPORTED_TIP;
			}
			else if (null == _tuner)
			{
				can = false;
				msg = "This tab does not currently support the selected device.";
			}
			else if ((LoadInertia.Quantity <= 0) && (CarriageInertia.Quantity <= 0))
			{
				can = false;
				msg =
					"This tuner tab requires a total load inertia greater than zero in order to perform its calculation.";
			}
			else if ((LoadInertia.Quantity > 0) && (null == LoadInertia.SelectedUnit))
			{
				can = false;
				msg = "Please select a unit of measure for the load inertia.";
			}
			else if ((CarriageInertia.Quantity > 0) && (null == CarriageInertia.SelectedUnit))
			{
				can = false;
				msg = "Please select a unit of measure for the carriage inertia.";
			}
			else if (_defaultCarriageInertia is null)
			{
				can = false;
				msg = "The device database does not provide the default carriage inertia for this device.";
			}

			_dispatcher.BeginInvoke(() =>
			{
				CanWrite = can;
				WriteButtonTip = msg;
			});
		}


		/// <summary>
		///     Update the recommended maximum acceleration display when the device or load changes.
		/// </summary>
		private void UpdateAccelerationRecommendation()
		{
			Measurement recommendedAccel = null;

			// Update acceleration display.
			if (null != _deviceProperties?.MaximumForce)
			{
				// TODO (Soleil 2018-04-16): This will need updating for rotary devices.

				// Display maximum recommended acceleration.
				var uc = _lastConversation.Device.UnitConverter.UnitConverter;
				var destUnit = GetSelectedAccelerationUnit();

				if ((null != CarriageInertia.SelectedUnit) && (null != LoadInertia.SelectedUnit) && (null != destUnit))
				{
					// This constant is meant to provide room for acceleration to increase as a result
					// of feedback during a move. It might become a per-device-type value in the database later.
					var MAX_ACCEL_SCALE = 0.60;

					var maxForceInNewtons = _deviceProperties.MaximumForce.GetValueAs(uc.FindUnitBySymbol("N"));
					var totalLoad = CarriageInertia.Measurement + LoadInertia.Measurement;
					var totalLoadInKg = totalLoad.GetValueAs(uc.FindUnitBySymbol("kg"));
					if (totalLoadInKg > 0.0)
					{
						recommendedAccel = new Measurement(MAX_ACCEL_SCALE * (maxForceInNewtons / totalLoadInKg),
														   uc.FindUnitBySymbol("m/s²"));

						// Convert to the selected unit.
						recommendedAccel = new Measurement(recommendedAccel, destUnit.Unit);
					}
				}
			}

			_recommendedAcceleration = recommendedAccel;

			_dispatcher.BeginInvoke(() => { OnPropertyChanged(nameof(RecommendedAcceleration)); });

			UpdateCanChangeAcceleration();
		}


		/// <summary>
		///     Updates the display of the device's current acceleration setting.
		/// </summary>
		private void UpdateActualAcceleration()
		{
			Measurement currentAccel = null;

			// Update acceleration display.
			if (null != _deviceProperties?.MaximumForce)
			{
				// TODO (Soleil 2018-04-16): This will need updating for rotary devices.

				// Display maximum recommended acceleration.
				var uc = _lastConversation.Device.UnitConverter.UnitConverter;
				var destUnit = GetSelectedAccelerationUnit();

				// Display current acceleration.
				if (_lastConversation.Device.DeviceType.GetCommandByText("accel") is SettingInfo accelSetting
				&& (null != accelSetting.RequestUnit))
				{
					try
					{
						var response = _lastConversation.Request("get accel");
						var accel = _lastConversation.Device.ConvertData(response.NumericData, destUnit, accelSetting);
						currentAccel = new Measurement(accel.Value, accel.Unit.Unit);
					}
					catch (ZaberPortErrorException)
					{
					}
					catch (ErrorResponseException)
					{
					}
				}
			}

			_currentAcceleration = currentAccel;
			_dispatcher.BeginInvoke(() => { OnPropertyChanged(nameof(CurrentAcceleration)); });
		}


		/// <summary>
		///     Returns the currently selected unit of measure for acceleration, or a default unit if none is selected.
		/// </summary>
		/// <returns>A unit of measure to use for converting acceleration values for the device.</returns>
		private UnitOfMeasure GetSelectedAccelerationUnit()
		{
			if (null != NewAcceleration.SelectedUnit)
			{
				return _lastConversation.Device.UnitConverter.FindUnitByAbbreviation(
					NewAcceleration.SelectedUnit.Abbreviation);
			}

			return _lastConversation.Device.UnitConverter.FindUnitByAbbreviation("m/s²");
		}


		/// <summary>
		///     Updates the visibility state of the acceleration group in the user interface.
		/// </summary>
		private void UpdateCanChangeAcceleration() => _dispatcher.BeginInvoke(() =>
		{
			CanChangeAcceleration =
				null != _recommendedAcceleration;
		});


		private void OnMassPropertyChanged(object aSenger, PropertyChangedEventArgs aArgs)
		{
			UpdateCanWrite();
			UpdateAccelerationRecommendation();
		}


		private void OnNewAccelerationPropertyChanged(object aSender, PropertyChangedEventArgs aArgs)
		{
			if (nameof(MeasurementBoxVM.SelectedUnit) == aArgs.PropertyName)
			{
				var unit = NewAcceleration.SelectedUnit;
				if (null != unit)
				{
					if (null != _recommendedAcceleration)
					{
						_recommendedAcceleration = new Measurement(_recommendedAcceleration, unit);
					}

					if (null != _currentAcceleration)
					{
						_currentAcceleration = new Measurement(_currentAcceleration, unit);
					}

					_dispatcher.BeginInvoke(() =>
					{
						OnPropertyChanged(nameof(RecommendedAcceleration));
						OnPropertyChanged(nameof(CurrentAcceleration));
					});
				}
			}
		}

		#endregion

		#region -- Fields --

		private readonly IDispatcher _dispatcher;
		private Conversation _lastConversation;
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private readonly Dictionary<string, Type> _userControlTypes = new Dictionary<string, Type>();

		private readonly Dictionary<string, IUserParameterControl> _userControlsByParameterName =
			new Dictionary<string, IUserParameterControl>();

		private DeviceProperties _deviceProperties;
		private ITuner _tuner;
		private string _tuningMode;

		private MeasurementBoxVM _loadInertia;
		private MeasurementBoxVM _carriageInertia;
		private MeasurementBoxVM _newAcceleration;
		private Measurement _recommendedAcceleration;
		private Measurement _currentAcceleration;
		private Measurement _defaultCarriageInertia;
		private bool _canChangeAcceleration;
		private RelayCommand _changeAccelerationCommand;
		private RelayCommand _copyRecommendedAccelerationCommand;
		private RelayCommand _restoreDefaultCarriageInertiaCommand;

		private ObservableCollection<IUserParameterControl> _userParameterControls =
			new ObservableCollection<IUserParameterControl>();

		private static List<Unit> _inertiaUnits;

		private static readonly Prefix[] _siPrefixes = { Prefix.Kilo, Prefix.None, Prefix.Milli };

		#endregion
	}
}

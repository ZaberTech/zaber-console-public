﻿using System;
using Zaber.Application;

namespace ZaberConsole.Plugins.ServoTuner.Methods
{
	/// <summary>
	///     User settings for PID method.
	/// </summary>
	[Serializable]
	public class ImportExportMethodSettings : PerDeviceSettings
	{
		/// <summary>
		///     Last file load/save directory.
		/// </summary>
		public string LastAccessPath { get; set; }
	}
}

﻿using Zaber.Units.UI.WPF.Controls;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.ServoTuner.Methods
{
	/// <summary>
	///     VM for the four user-specified parameters in the PID method user interface.
	///     This class just exists to simplify the XAML.
	/// </summary>
	public class PidMethodUserParamVM : ObservableObject
	{
		/// <summary>
		///     Name of the parameter.
		/// </summary>
		public string Name { get; internal set; }


		/// <summary>
		///     Help text for the help button popup.
		/// </summary>
		public string Help { get; internal set; }


		/// <summary>
		///     Value and unit.
		/// </summary>
		public MeasurementBoxVM Value
		{
			get => _value;
			set => Set(ref _value, value, nameof(Value));
		}


		private MeasurementBoxVM _value;
	}
}

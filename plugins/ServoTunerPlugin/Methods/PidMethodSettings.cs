﻿using System;
using Zaber.Application;
using Zaber.Tuning.Servo;

namespace ZaberConsole.Plugins.ServoTuner.Methods
{
	/// <summary>
	///     User settings for PID method.
	/// </summary>
	[Serializable]
	public class PidMethodSettings : PerDeviceSettings
	{
		/// <summary>
		///     Last entered PID tuning parameters.
		/// </summary>
		public TunerParameterSet PidParameters { get; set; }
	}
}

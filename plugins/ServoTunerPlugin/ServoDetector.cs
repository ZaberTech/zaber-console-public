﻿using System;
using System.Linq;
using Zaber;
using Zaber.PlugIns;
using ZaberConsole.Plugins.ServoTuner.Properties;
using ZaberWpfToolbox.Dialogs;
using ZaberWpfToolbox.Settings;

namespace ZaberConsole.Plugins.ServoTuner
{
	/// <summary>
	///     Invisible plugin that looks for servo devices and prompts the user to display
	///     the tuning tab when there is one.
	/// </summary>
	[PlugIn(Name = "Servo Detector",
		AlwaysActive = true,
		Dockable = false,
		Description = "Detects servo devices and prompts to display the tuning interface.")]
	public class ServoDetector : IDialogClient
	{
		#region -- Plugin implementation --

		/// <summary>
		///     The active port.
		/// </summary>
		[PlugInProperty]
		public ZaberPortFacade PortFacade
		{
			get => _port;
			set
			{
				if (null != _port)
				{
					_port.Opened -= OnPortOpened;
				}

				_port = value;

				if (null != _port)
				{
					_port.Opened += OnPortOpened;
				}
			}
		}


		/// <summary>
		///     Reference to the plugin manager so we can check available tabs.
		/// </summary>
		[PlugInProperty]
		public IPlugInManager PluginManager
		{
			get => _pluginManager;
			set
			{
				if (null != _pluginManager)
				{
					_pluginManager.SettingsResetEvent -= OnSettingsReset;
				}

				_pluginManager = value;

				if (null != _pluginManager)
				{
					_pluginManager.SettingsResetEvent += OnSettingsReset;
				}
			}
		}


		/// <summary>
		///     Saves user settings on exit.
		/// </summary>
		[PlugInMethod(PlugInMethodAttribute.EventType.PluginDeactivated)]
		public void OnClosing() => Settings.Default.Save();

		#endregion

		#region -- IDialogClient implementation --

		/// <summary>
		///     Invoked to display a popup dialog.
		/// </summary>
		public event RequestDialogChange RequestDialogOpen;

		/// <summary>
		///     Invoked to programmatically close a popup dialog.
		/// </summary>
		#pragma warning disable CS0067 // Event is never used.
		public event RequestDialogChange RequestDialogClose;
		#pragma warning restore CS0067

		#endregion

		#region -- Functionality --

		/// <summary>
		///     Called by the plugin manager when the user opts to reset program settings.
		///     Clears the do-not-show-again settings for the show tab prompt.
		/// </summary>
		/// <param name="aSender">The plugin manager.</param>
		/// <param name="aArgs">Ignored.</param>
		private void OnSettingsReset(object aSender, EventArgs aArgs)
			=> Settings.Default.DoNotShowAgainSettings = new DoNotShowAgainSettingsCollection();


		// Called when the port is opened, after devices have been detected.
		private void OnPortOpened(object aSender, EventArgs aArgs)
		{
			if ((null == _port) || (null == PluginManager) || !_port.Port.IsAsciiMode)
			{
				return;
			}

			// Don't do anything if the servo tab is already visible.
			if (PluginManager.GetActivePluginTypes().Contains(typeof(ServoTunerPluginVM)))
			{
				return;
			}

			// Find the do-not-show-again settings for this dialog.
			var settings = Settings.Default.DoNotShowAgainSettings;
			if (null == settings)
			{
				settings = new DoNotShowAgainSettingsCollection();
				Settings.Default.DoNotShowAgainSettings = settings;
			}

			var showTab = false;
			var focus = true;

			var setting = settings.FindOrCreate(SETTING_NAME);
			if (setting.ShowDialog)
			{
				// See if there are any servo devices on the chain.
				Conversation servoConversation = null;
				foreach (var conv in _port.AllConversations)
				{
					var d = conv.Device;
					if (d.IsSingleDevice)
					{
						if (null != d.DeviceType.TuningData)
						{
							servoConversation = conv;
							break;
						}
					}
				}

				// A servo was found - display the popup.
				if (null != servoConversation)
				{
					var mbvm = new CustomMessageBoxVM(
						string.Format(Resources.SERVO_PROMPT_MESSAGE, servoConversation.Device.DeviceType.Name),
						"Add servo tuning tab?")
						{
							DoNotShowAgainSettings = setting,
							DialogResult = 0
						};
					mbvm.AddOption("Yes", 1);
					mbvm.AddOption("No", 0);

					RequestDialogOpen?.Invoke(this, mbvm);

					// Show the tab if the user said OK.
					showTab |= 0 != (int) mbvm.DialogResult;
				}
			}
			else
			{
				showTab = setting.LastSelection is int && ((int) setting.LastSelection != 0);
				focus = false;
			}

			if (showTab)
			{
				PluginManager.ShowPluginTab(typeof(ServoTunerPluginVM), focus);
			}
		}

		#endregion

		#region -- State information --

		private const string SETTING_NAME = "ServoDetectorPrompt";

		private ZaberPortFacade _port;
		private IPlugInManager _pluginManager;

		#endregion
	}
}

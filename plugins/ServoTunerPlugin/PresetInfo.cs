﻿namespace ZaberConsole.Plugins.ServoTuner
{
	/// <summary>
	///     Representation of one device tuning preset (including live set and factory defaults).
	/// </summary>
	public class PresetInfo
	{
		/// <summary>
		///     Firmware's name for the preset - this is used in commands that refer to the preset.
		/// </summary>
		public string PresetId { get; set; }


		/// <summary>
		///     Is this preset writable? Normally true; false for factory presets.
		/// </summary>
		public bool IsWritable { get; set; } = true;


		/// <summary>
		///     How this preset should be displayed in the user interface.
		/// </summary>
		public string DisplayName
		{
			get
			{
				if (!IsWritable || ("staging" == PresetId) || ("live" == PresetId))
				{
					return PresetId;
				}

				return "Preset " + PresetId;
			}
		}
	}
}

﻿using System;
using Zaber.Application;

namespace ZaberConsole.Plugins.ServoTuner
{
	/// <summary>
	///     Class used to remember previously selected tuner settings for each device.
	/// </summary>
	[Serializable]
	public class SavedTunerSettings : PerDeviceSettings
	{
		/// <summary>
		///     Fully qualified type name of the last selected tuning method.
		/// </summary>
		public string MethodName { get; set; }


		/// <summary>
		///     Name of the last selected preset for this device (device's name for it, not display name).
		/// </summary>
		public string ParameterSetName { get; set; }
	}
}

﻿using System;
using Zaber.Application;

namespace ZaberConsole.Plugins.ServoTuner
{
	/// <summary>
	///     Used to save and restore user's tuner settings across program sessions.
	/// </summary>
	[Serializable]
	public class SavedTunerSettingsCollection : PerDeviceSettingsCollection<SavedTunerSettings>
	{
	}
}

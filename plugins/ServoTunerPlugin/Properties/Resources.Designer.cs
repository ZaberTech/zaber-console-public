﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ZaberConsole.Plugins.ServoTuner.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "17.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ZaberConsole.Plugins.ServoTuner.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to We&apos;ve detected a servo device ({0}), which can be tuned to optimize its performance.
        ///
        ///Zaber Console has a tab to assist with servo tuning - would you like to add it to the tab dock now? 
        ///
        ///If not, you can always get to it later by clicking on the &apos;+&apos; tab and selecting &apos;Servo Tuner&apos;..
        /// </summary>
        internal static string SERVO_PROMPT_MESSAGE {
            get {
                return ResourceManager.GetString("SERVO_PROMPT_MESSAGE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Read the device tuning parameters into the editing area above..
        /// </summary>
        internal static string STR_DEFAULT_READ_TIP {
            get {
                return ResourceManager.GetString("STR_DEFAULT_READ_TIP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Write the above tuning parameters to the device..
        /// </summary>
        internal static string STR_DEFAULT_WRITE_TIP {
            get {
                return ResourceManager.GetString("STR_DEFAULT_WRITE_TIP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The selected device is not tunable..
        /// </summary>
        internal static string STR_DEVICE_NOT_TUNABLE {
            get {
                return ResourceManager.GetString("STR_DEVICE_NOT_TUNABLE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Disable driver.
        /// </summary>
        internal static string STR_DISABLE_DRIVER {
            get {
                return ResourceManager.GetString("STR_DISABLE_DRIVER", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This tab does not support tuning the X-DMQ-DE; please see the [product manual](https://www.zaber.com/manuals/X-DMQ-DE#8-13-servo-tuning-2) for help with manual tuning..
        /// </summary>
        internal static string STR_DMQ_NOT_TUNABLE {
            get {
                return ResourceManager.GetString("STR_DMQ_NOT_TUNABLE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enable driver.
        /// </summary>
        internal static string STR_ENABLE_DRIVER {
            get {
                return ResourceManager.GetString("STR_ENABLE_DRIVER", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Failed.
        /// </summary>
        internal static string STR_FAILED {
            get {
                return ResourceManager.GetString("STR_FAILED", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Low pass filter cutoff frequency. Lower values produce more stability and reduce audible noise but also cause slower settling..
        /// </summary>
        internal static string STR_FC_HELP {
            get {
                return ResourceManager.GetString("STR_FC_HELP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Derivative gain. Governs control response in proportion to the rate at which the stage is approaching the target position..
        /// </summary>
        internal static string STR_KD_HELP {
            get {
                return ResourceManager.GetString("STR_KD_HELP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Integral gain. Governs control response in proportion to the error history..
        /// </summary>
        internal static string STR_KI_HELP {
            get {
                return ResourceManager.GetString("STR_KI_HELP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Proportional gain. This controls how much control effort will be applied to correct measured positioning error. A larger number produces stiffer behavior..
        /// </summary>
        internal static string STR_KP_HELP {
            get {
                return ResourceManager.GetString("STR_KP_HELP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The selected parameter set is not writable..
        /// </summary>
        internal static string STR_PRESET_NOT_WRITABLE {
            get {
                return ResourceManager.GetString("STR_PRESET_NOT_WRITABLE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reading....
        /// </summary>
        internal static string STR_READ_IN_PROGRESS {
            get {
                return ResourceManager.GetString("STR_READ_IN_PROGRESS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Scanning....
        /// </summary>
        internal static string STR_SCANNING_PRESETS {
            get {
                return ResourceManager.GetString("STR_SCANNING_PRESETS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Scanning device settings....
        /// </summary>
        internal static string STR_STATUS_SCANNING {
            get {
                return ResourceManager.GetString("STR_STATUS_SCANNING", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Success.
        /// </summary>
        internal static string STR_SUCCESS {
            get {
                return ResourceManager.GetString("STR_SUCCESS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Device interaction is disabled while it is being scanned..
        /// </summary>
        internal static string STR_WAIT_FOR_SCAN_TO_FINISH {
            get {
                return ResourceManager.GetString("STR_WAIT_FOR_SCAN_TO_FINISH", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Writing....
        /// </summary>
        internal static string STR_WRITE_IN_PROGRESS {
            get {
                return ResourceManager.GetString("STR_WRITE_IN_PROGRESS", resourceCulture);
            }
        }
    }
}

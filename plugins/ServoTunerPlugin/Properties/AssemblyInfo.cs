﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Servo Tuner Plugin")]
[assembly: AssemblyDescription("A plugin for Zaber Console to assist with tuning Zaber servo devices.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Zaber Technologies Inc.")]
[assembly: AssemblyProduct("ServoTunerPlugin")]
[assembly: AssemblyCopyright("Copyright © 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("4fc77e2b-21c9-400a-b022-2ad52716af02")]


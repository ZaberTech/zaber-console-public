﻿using System.ComponentModel.Composition;
using System.Windows;
using Zaber.PlugIns;

namespace ZaberConsole.Plugins.ServoTuner
{
	/// <summary>
	///     Code partial of the top-level resource dictionary. This only exists in order to
	///     attach the Export attibute that Zaber Console's Plugin Manager will use to locate
	///     this resource when loading the plugin.
	/// </summary>
	[Export(PlugInAttribute.PLUGIN_RESOURCE_DICTIONARY, typeof(ResourceDictionary))]
	public partial class MainResourceDictionary : ResourceDictionary
	{
		public MainResourceDictionary()
		{
			InitializeComponent();
		}
	}
}

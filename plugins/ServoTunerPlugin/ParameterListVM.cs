﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Zaber.Tuning.Servo;
using Zaber.Units;
using ZaberWpfToolbox;

namespace ZaberConsole.Plugins.ServoTuner
{
	/// <summary>
	///     ViewModel for lists of tuner parameters. Presents a
	///     tuner parameter set as a list of key-value pairs, with
	///     optionally editable values.
	/// </summary>
	public class ParameterListVM : ObservableObject
	{
		#region -- Helpers --

		private void OnParameterValueChanged(object aSender, PropertyChangedEventArgs aArgs)
		{
			if ((aSender is ParameterListRowVM vm)
			&& ((nameof(ParameterListRowVM.Value) == aArgs.PropertyName)
			|| (nameof(ParameterListRowVM.SelectedUnit) == aArgs.PropertyName)))
			{
				_paramSet[vm.Name] = new Measurement(vm.Value, vm.SelectedUnit);
				ParameterEdited?.Invoke(this, vm.Name);
			}
		}

		#endregion

		#region -- Public events --

		/// <summary>
		///     Event handler type for user edits to tuning parameter values..
		/// </summary>
		/// <param name="aSender">The <see cref="ParameterListVM" /> wrapping the modified parameter. </param>
		/// <param name="aParamName">Name of the parameter whose value was edited.</param>
		public delegate void ParameterEditHandler(object aSender, string aParamName);

		/// <summary>
		///     Event handler invoked when the user edits a parameter.
		/// </summary>
		public event ParameterEditHandler ParameterEdited;

		#endregion

		#region -- Initialization --

		/// <summary>
		///     Initialize a new VM from an existing parameter set. The VM will wrap
		///     the given set and update its content if the user edits values.
		/// </summary>
		/// <param name="aParams">Parameter set to present and update.</param>
		public ParameterListVM(ITunerParameterSet aParams)
		{
			var unitConversionsExist = false;
			_paramSet = aParams;
			if (null != aParams)
			{
				foreach (var name in _paramSet.ParameterNames)
				{
					var param = _paramSet[name];
					var row = new ParameterListRowVM(name, (decimal) param.Value);

					var unit = param.Unit;
					if ((null != unit) && (Unit.Dimensionless != unit))
					{
						row.ApplicableUnits = new ObservableCollection<Unit>(
							_paramSet.UnitConverter.FindUnits(unit.Dimension,
															  Prefix.PowersOfThousand.Where(
																  p => Math.Abs(p.Exponent) <= 6)));

						row.SelectedUnit = unit;
						unitConversionsExist |= row.ApplicableUnits.Count > 1;
					}

					row.PropertyChanged += OnParameterValueChanged;
					_parameters[name] = row;
				}
			}

			ShowUnitConversions = unitConversionsExist;
		}


		/// <summary>
		///     Returns a copy of the current view contents and unit selections.
		/// </summary>
		public ITunerParameterSet GetEditedParameters() => _paramSet?.Clone();


		public IList<string> ParameterDisplayOrder
		{
			get => _parameterDisplayOrder;
			set
			{
				_parameterDisplayOrder = value;
				OnPropertyChanged(nameof(Parameters));
			}
		}

		#endregion

		#region -- View properties --

		/// <summary>
		///     Observable property for views to sync Parameters.
		/// </summary>
		public IEnumerable<ParameterListRowVM> Parameters
		{
			get
			{
				if ((null != ParameterDisplayOrder) && (0 < ParameterDisplayOrder.Count))
				{
					return ParameterDisplayOrder.Select(key => _parameters[key]);
				}

				return _parameters.OrderBy(pair => pair.Key).Select(pair => pair.Value);
			}
		}


		/// <summary>
		///     Controls whether or not values can be edited.
		/// </summary>
		public bool IsEditable
		{
			get => _isEditable;
			set => Set(ref _isEditable, value, nameof(IsEditable));
		}


		/// <summary>
		///     Controls whether descriptions are displayed in a column (true) or as tooltips over the names (false).
		/// </summary>
		public bool ShowDescriptions
		{
			get => _showDescriptions;
			set => Set(ref _showDescriptions, value, nameof(ShowDescriptions));
		}


		/// <summary>
		///     Observable property for views to sync ShowUnitConversions.
		/// </summary>
		public bool ShowUnitConversions
		{
			get => _showUnitConversions;
			set => Set(ref _showUnitConversions, value, nameof(ShowUnitConversions));
		}

		#endregion

		#region -- Private fields --

		private readonly ITunerParameterSet _paramSet;

		private readonly Dictionary<string, ParameterListRowVM> _parameters =
			new Dictionary<string, ParameterListRowVM>();

		private bool _isEditable = true;
		private bool _showDescriptions;
		private bool _showUnitConversions;
		private IList<string> _parameterDisplayOrder;

		#endregion
	}
}

﻿using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using Zaber.GCode;

namespace Zaber.GCode.CLI
{
	class Program
	{
		static void Main(string[] args)
		{
            string myNgcFile = args[0];

			// Set up configuration for an A-MCB2 with an ASR-T3
            Configuration config = new Configuration();
            
            config.OutputType = ConfigurationOutputType.StreamingCommands;
            config.StreamNumber = 1;
            config.DeviceAddress = 1;
            config.TraverseRate = 153600;

            config.X.EnabledType = AxisEnabledType.Enabled;
			config.X.AxisNumber = 1;
            config.X.MinPosition = 0;
            config.X.MaxPosition = 640000;
			config.X.MicrostepSize = 0.15625M;

			config.Y.EnabledType = AxisEnabledType.Enabled;
			config.Y.AxisNumber = 2;
            config.Y.MinPosition = 0;
            config.Y.MaxPosition = 768000;
            config.Y.MicrostepSize = 0.15625M;

			// Map M-codes to specific ASCII commands
            // Multiple ASCII commands can be concatenated
            config.Remappings.Add(3, "/1 stream 1 io set do 4 1/1 stream 1 wait 100"); // M3
            config.Remappings.Add(5, "/1 stream 1 io set do 4 0/1 stream 1 wait 100"); // M5

            // Execute the translation
            Translator translator = new Translator(new StreamReader(myNgcFile), config);
            IEnumerable<ZaberCommand> cmds = translator.Translate();

            // Print warning and error logs, if any
            int logCount = translator.Messages.Log.Count;
            if(logCount > 0)
            {
                Console.WriteLine("{0} warnings/errors:", logCount);
                foreach (var log in translator.Messages.Log)
                {
                    Console.WriteLine(log);
                }
            }
            else
            {
                Console.WriteLine("No warning/error.");
            }
			Console.WriteLine();

            // Print ASCII commands if translation is successful
            if (cmds != null)
            {
                Console.WriteLine("Output:");
                foreach (var cmd in cmds)
                {
                    Console.WriteLine("{0}", cmd.ToString());
                }
            }
            else
            {
                Console.WriteLine("No output was produced. Check errors above.");
            }
            Console.WriteLine();

			Console.WriteLine("Press any key to exit...");
			Console.ReadKey();
		}
	}
}
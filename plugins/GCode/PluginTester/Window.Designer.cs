﻿namespace PluginTester
{
	partial class Window
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.mainControl = new Zaber.GCode.Plugin.GCodeTranslator();
			this.SuspendLayout();
			// 
			// mainControl
			// 
			this.mainControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainControl.Location = new System.Drawing.Point(0, 0);
			this.mainControl.Name = "mainControl";
			this.mainControl.Size = new System.Drawing.Size(752, 368);
			this.mainControl.TabIndex = 0;
			// 
			// Window
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(752, 368);
			this.Controls.Add(this.mainControl);
			this.Name = "Window";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}

		#endregion

		private Zaber.GCode.Plugin.GCodeTranslator mainControl;
	}
}


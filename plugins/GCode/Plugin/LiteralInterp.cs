﻿using System;
using System.Collections.Generic;
using System.Text;
using Zaber.GCode.Plugin.Properties;

namespace Zaber.GCode.Plugin
{
	internal static class LiteralInterp
	{
		public static string InterpSrc(Dictionary<string, string> subst, IEnumerable<string> inject)
		{
			var sb = new StringBuilder(Resources.RunWorkerExtract.Length / 2);
			var ignoreCount = 0;
			var emplaceCount = 0;

			foreach (var line in Resources.RunWorkerExtract.Split(new[] { "\r\n", "\n" }, StringSplitOptions.None))
			{
				var cp = line;
				switch (cp)
				{
					case "//LiteralIgnoreBegin":
						ignoreCount = -1;
						continue;
					case "//LiteralIgnoreEnd":
						ignoreCount = 0;
						continue;
					case "//LiteralIgnoreNext":
						ignoreCount = 1;
						continue;
					case "//LiteralOnlyBegin":
						emplaceCount = -1;
						continue;
					case "//LiteralOnlyEnd":
						emplaceCount = 0;
						continue;
					case "//LiteralOnlyNext":
						emplaceCount = 1;
						continue;
					case "//LiteralInjectHere":
						foreach (var ll in inject)
						{
							sb.AppendLine(ll);
						}

						continue;
				}

				foreach (var pair in subst)
				{
					var str = $"${pair.Key}$";
					cp = cp.Replace(str, pair.Value);
				}

				if (ignoreCount != 0)
				{
					if (ignoreCount > 0)
					{
						ignoreCount--;
					}

					continue;
				}

				if ((emplaceCount != 0) && cp.StartsWith("//"))
				{
					if (emplaceCount > 0)
					{
						emplaceCount--;
					}

					sb.AppendLine(cp.Substring(2));
					continue;
				}

				sb.AppendLine(cp);
			}

			return sb.ToString();
		}
	}
}

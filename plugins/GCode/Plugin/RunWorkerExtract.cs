﻿//LiteralIgnoreBegin
//This file is also used as a string resource file so that the code below can appear in two places:
//one, as part of the compiled code for the GCodeTranslator plugin, and
//two, as a template for the generated ASCII scripts.

//DO NOT reformat this document. All spacing and comments must be left intact.
using System;
using System.Windows.Forms;
using System.Collections.Generic;

namespace Zaber.GCode.Plugin
{
	static class RunWorkerExtract
	{
		public static void RunWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
		{
			var args = e.Argument as GCodeTranslator.RunArgs;
			Conversation conv = args.Conversation;
			Queue<ZaberCommand> commandList = args.CommandList;
			object commandListLock = args.CommandListLock;
			int prog = args.Progress;
			GCodeTranslator trans = args.Translator;
//LiteralIgnoreEnd
//LiteralOnlyBegin
//#template(Simple)
//
//Conversation conv = PortFacade.GetConversation($DEVICEID$, 0);
//Queue<string> commandList = new Queue<string>(new[]
//{
//LiteralOnlyEnd
//LiteralInjectHere
//LiteralOnlyNext
//});

TimeoutTimer timer500 = new TimeoutTimer { Timeout = 500 };

string strerror = null;

if (conv.Request("").FlagText == "WR")
{
//LiteralIgnoreBegin
	if (MessageBox.Show("Device is not homed. Home now?", "No Reference Position",
		MessageBoxButtons.YesNo, 
		MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
//LiteralIgnoreEnd
//LiteralOnlyBegin
//	Output.WriteLine("Device is not homed. Home now? [y/N]");
//	if (Input.ReadLine().ToLowerInvariant() == "y")
//LiteralOnlyEnd
	{
		conv.Request("home");
		conv.PollUntilIdle();
	}
	else
	{
//LiteralIgnoreNext
		MessageBox.Show("Aborted.");
//LiteralOnlyNext
//		Output.WriteLine("Aborted.");
		return;
	}
}

//LiteralIgnoreBegin
if (prog == 0)
{
	trans.InitStream(conv);
}
//LiteralIgnoreEnd
//LiteralOnlyBegin
//conv.Request("stop");
//conv.Request("stream $STREAMID$ setup live $AXES$");
//LiteralOnlyEnd

bool busy = true;

//LiteralIgnoreNext
lock (commandListLock)
while (commandList.Count > 0 || busy)
{
//LiteralIgnoreBegin
	if (trans.RunWorker.CancellationPending)
	{
		break;
	}

//LiteralIgnoreEnd
	DeviceMessage msg = null;

//LiteralIgnoreBegin
	if (commandList.Count > 0)
	{
		if (!commandList.Peek().Prefix.StartsWith("/"))
		{
			if (commandList.Count > 0)
			{
				trans.RunWorker.ReportProgress(0, prog++);
				commandList.Dequeue();
			}

			continue; // Ignore passthrough commands.
		}
	}
//LiteralIgnoreEnd

	try
	{
		ConversationTopic topic = conv.StartTopic();
		if (commandList.Count < 1)
		{
			// wait for idle
			conv.Device.SendInUnits("", null, topic.MessageId);
		}
		else
		{
			// request next command
//LiteralIgnoreNext
			conv.Device.SendInUnits(commandList.Peek().Command, null, topic.MessageId);
//LiteralOnlyNext
//			conv.Device.SendInUnits(commandList.Peek(), null, topic.MessageId);
		}

		if (!topic.Wait(timer500))
		{
			throw new RequestTimeoutException();
		}
		msg = topic.Response;
	}
	catch (Exception x)
	{
		strerror = string.Format("Command {0} was rejected: {1}. Aborted.",
			commandList.Count > 0 ? commandList.Peek().ToString() : "polling", x.ToString());
		break;
	}

	if (msg.IsAgain)
	{
		System.Threading.Thread.Sleep(50);
		continue;
	}

	if (msg.IsError)
	{
		strerror = string.Format("Command {0} was rejected: {1}. Execution stopped.",
			commandList.Count > 0 ? commandList.Peek().ToString() : "polling", msg.TextData);
		break;
	}

	if (msg.FlagText == "--" || msg.FlagText == "ND")
	{
		if (commandList.Count > 0)
		{
//LiteralIgnoreNext
			trans.RunWorker.ReportProgress(0, prog++);
			commandList.Dequeue();
		}
		else if (msg.IsIdle)
		{
			// loop can now exit on next cycle
			busy = false;
		}
		continue;
	}

	strerror = string.Format("Device replied with flag {0}. Execution stopped.", msg.FlagText);
	break;
}

conv.Request("stop");

if (strerror != null)
{
//LiteralIgnoreNext
	System.Windows.Forms.MessageBox.Show(strerror);
//LiteralOnlyNext
//	Output.WriteLine(strerror);
}
//LiteralIgnoreBegin
		}
	}
}
//LiteralIgnoreEnd

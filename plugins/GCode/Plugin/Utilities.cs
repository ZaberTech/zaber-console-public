﻿using System;
using System.Collections.Generic;

namespace Zaber.GCode.Plugin
{
	internal static class Utilities
	{
		public static IEnumerable<TResult> Zip<TFirst, TSecond, TResult>(
			this IEnumerable<TFirst> first, IEnumerable<TSecond> second, Func<TFirst, TSecond, TResult> resultSelector)
		{
			if (first is null)
			{
				throw new ArgumentNullException(nameof(first));
			}

			if (second is null)
			{
				throw new ArgumentNullException(nameof(second));
			}

			if (resultSelector is null)
			{
				throw new ArgumentNullException(nameof(resultSelector));
			}

			return ZipIterator(first, second, resultSelector);
		}


		private static IEnumerable<TResult> ZipIterator<TFirst, TSecond, TResult>(IEnumerable<TFirst> first,
																				  IEnumerable<TSecond> second,
																				  Func<TFirst, TSecond, TResult>
																					  resultSelector)
		{
			using (var e1 = first.GetEnumerator())
			{
				using (var e2 = second.GetEnumerator())
				{
					while (e1.MoveNext() && e2.MoveNext())
					{
						yield return resultSelector(e1.Current, e2.Current);
					}
				}
			}
		}
	}
}

﻿namespace Zaber.GCode.Plugin
{
	partial class ConfigDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this._layoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._generalSettingsGroup = new System.Windows.Forms.GroupBox();
            this._generalLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._gWCSValueLabel = new System.Windows.Forms.Label();
            this._gFROverrideBox = new System.Windows.Forms.TextBox();
            this._gDeviceAddrLabel = new System.Windows.Forms.Label();
            this._gDeviceAddrNumeric = new System.Windows.Forms.NumericUpDown();
            this._gStreamIdLabel = new System.Windows.Forms.Label();
            this._gStreamIdNumeric = new System.Windows.Forms.NumericUpDown();
            this._gTraverseRateLabel = new System.Windows.Forms.Label();
            this._gTraverseRateNumeric = new System.Windows.Forms.NumericUpDown();
            this._gFROverrideLabel = new System.Windows.Forms.Label();
            this._gRemappingKeyLabel = new System.Windows.Forms.Label();
            this._gRemappingKeyBox = new System.Windows.Forms.ComboBox();
            this._gRemappingValueLabel = new System.Windows.Forms.Label();
            this._gRemappingValueBox = new System.Windows.Forms.TextBox();
            this._gWCSKeyBox = new System.Windows.Forms.ComboBox();
            this._gWCSKeyLabel = new System.Windows.Forms.Label();
            this._gWCSValueBox = new System.Windows.Forms.TextBox();
            this._gOptDelLabel = new System.Windows.Forms.Label();
            this._gOptDelBox = new System.Windows.Forms.CheckBox();
            this._axisSettingsGroup = new System.Windows.Forms.GroupBox();
            this._axisLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._axisLabel = new System.Windows.Forms.Label();
            this._axisBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this._xMaxPositionNumeric = new System.Windows.Forms.NumericUpDown();
            this._xMinPositionNumeric = new System.Windows.Forms.NumericUpDown();
            this._xMinPositionLabel = new System.Windows.Forms.Label();
            this._xMicrostepSizeLabel = new System.Windows.Forms.Label();
            this._xMicrostepSizeBox = new System.Windows.Forms.TextBox();
            this._xAddrLabel = new System.Windows.Forms.Label();
            this._xEnabledLabel = new System.Windows.Forms.Label();
            this._xAddrNumeric = new System.Windows.Forms.NumericUpDown();
            this._xEnabledBox = new System.Windows.Forms.ComboBox();
            this._presetGroup = new System.Windows.Forms.GroupBox();
            this._presetLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._presetBox = new System.Windows.Forms.ComboBox();
            this._loadButton = new System.Windows.Forms.Button();
            this._saveButton = new System.Windows.Forms.Button();
            this._deleteButton = new System.Windows.Forms.Button();
            this._okButton = new System.Windows.Forms.Button();
            this._importButton = new System.Windows.Forms.Button();
            this._cancelButton = new System.Windows.Forms.Button();
            this._exportButton = new System.Windows.Forms.Button();
            this._loadDeviceButton = new System.Windows.Forms.Button();
            this._deviceNumberBox = new System.Windows.Forms.ComboBox();
            this._ttProvider = new System.Windows.Forms.ToolTip(this.components);
            this._layoutPanel.SuspendLayout();
            this._generalSettingsGroup.SuspendLayout();
            this._generalLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._gDeviceAddrNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._gStreamIdNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._gTraverseRateNumeric)).BeginInit();
            this._axisSettingsGroup.SuspendLayout();
            this._axisLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._xMaxPositionNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._xMinPositionNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._xAddrNumeric)).BeginInit();
            this._presetGroup.SuspendLayout();
            this._presetLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // _layoutPanel
            // 
            this._layoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._layoutPanel.ColumnCount = 4;
            this._layoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._layoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._layoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 280F));
            this._layoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 280F));
            this._layoutPanel.Controls.Add(this._generalSettingsGroup, 0, 1);
            this._layoutPanel.Controls.Add(this._axisSettingsGroup, 2, 1);
            this._layoutPanel.Controls.Add(this._presetGroup, 0, 2);
            this._layoutPanel.Controls.Add(this._okButton, 2, 3);
            this._layoutPanel.Controls.Add(this._importButton, 2, 2);
            this._layoutPanel.Controls.Add(this._cancelButton, 3, 3);
            this._layoutPanel.Controls.Add(this._exportButton, 3, 2);
            this._layoutPanel.Controls.Add(this._loadDeviceButton, 0, 0);
            this._layoutPanel.Controls.Add(this._deviceNumberBox, 1, 0);
            this._layoutPanel.Location = new System.Drawing.Point(26, 25);
            this._layoutPanel.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._layoutPanel.Name = "_layoutPanel";
            this._layoutPanel.RowCount = 4;
            this._layoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this._layoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._layoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this._layoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this._layoutPanel.Size = new System.Drawing.Size(1172, 710);
            this._layoutPanel.TabIndex = 0;
            // 
            // _generalSettingsGroup
            // 
            this._generalSettingsGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._layoutPanel.SetColumnSpan(this._generalSettingsGroup, 2);
            this._generalSettingsGroup.Controls.Add(this._generalLayoutPanel);
            this._generalSettingsGroup.Location = new System.Drawing.Point(6, 62);
            this._generalSettingsGroup.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._generalSettingsGroup.Name = "_generalSettingsGroup";
            this._generalSettingsGroup.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._generalSettingsGroup.Size = new System.Drawing.Size(600, 530);
            this._generalSettingsGroup.TabIndex = 11;
            this._generalSettingsGroup.TabStop = false;
            this._generalSettingsGroup.Text = "General Settings";
            // 
            // _generalLayoutPanel
            // 
            this._generalLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._generalLayoutPanel.ColumnCount = 2;
            this._generalLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._generalLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._generalLayoutPanel.Controls.Add(this._gWCSValueLabel, 0, 10);
            this._generalLayoutPanel.Controls.Add(this._gFROverrideBox, 1, 3);
            this._generalLayoutPanel.Controls.Add(this._gDeviceAddrLabel, 0, 0);
            this._generalLayoutPanel.Controls.Add(this._gDeviceAddrNumeric, 1, 0);
            this._generalLayoutPanel.Controls.Add(this._gStreamIdLabel, 0, 1);
            this._generalLayoutPanel.Controls.Add(this._gStreamIdNumeric, 1, 1);
            this._generalLayoutPanel.Controls.Add(this._gTraverseRateLabel, 0, 2);
            this._generalLayoutPanel.Controls.Add(this._gTraverseRateNumeric, 1, 2);
            this._generalLayoutPanel.Controls.Add(this._gFROverrideLabel, 0, 3);
            this._generalLayoutPanel.Controls.Add(this._gRemappingKeyLabel, 0, 6);
            this._generalLayoutPanel.Controls.Add(this._gRemappingKeyBox, 1, 6);
            this._generalLayoutPanel.Controls.Add(this._gRemappingValueLabel, 0, 7);
            this._generalLayoutPanel.Controls.Add(this._gRemappingValueBox, 1, 7);
            this._generalLayoutPanel.Controls.Add(this._gWCSKeyBox, 1, 9);
            this._generalLayoutPanel.Controls.Add(this._gWCSKeyLabel, 0, 9);
            this._generalLayoutPanel.Controls.Add(this._gWCSValueBox, 1, 10);
            this._generalLayoutPanel.Controls.Add(this._gOptDelLabel, 0, 4);
            this._generalLayoutPanel.Controls.Add(this._gOptDelBox, 1, 4);
            this._generalLayoutPanel.Location = new System.Drawing.Point(12, 37);
            this._generalLayoutPanel.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._generalLayoutPanel.Name = "_generalLayoutPanel";
            this._generalLayoutPanel.RowCount = 12;
            this._generalLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this._generalLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this._generalLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this._generalLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this._generalLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this._generalLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this._generalLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this._generalLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this._generalLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this._generalLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this._generalLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 58F));
            this._generalLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._generalLayoutPanel.Size = new System.Drawing.Size(576, 482);
            this._generalLayoutPanel.TabIndex = 10;
            // 
            // _gWCSValueLabel
            // 
            this._gWCSValueLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._gWCSValueLabel.AutoSize = true;
            this._gWCSValueLabel.Location = new System.Drawing.Point(6, 426);
            this._gWCSValueLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this._gWCSValueLabel.Name = "_gWCSValueLabel";
            this._gWCSValueLabel.Size = new System.Drawing.Size(276, 50);
            this._gWCSValueLabel.TabIndex = 27;
            this._gWCSValueLabel.Text = "Axis Offsets [x, y, z] (G20/G21 units)";
            this._gWCSValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _gFROverrideBox
            // 
            this._gFROverrideBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._gFROverrideBox.Location = new System.Drawing.Point(294, 152);
            this._gFROverrideBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._gFROverrideBox.Name = "_gFROverrideBox";
            this._gFROverrideBox.Size = new System.Drawing.Size(276, 31);
            this._gFROverrideBox.TabIndex = 23;
            this._gFROverrideBox.Text = "100.0";
            this._ttProvider.SetToolTip(this._gFROverrideBox, "The percentage that all feed rates will be scaled by. To restore normal speed, en" +
        "ter 100.");
            this._gFROverrideBox.TextChanged += new System.EventHandler(this.Field_ValueChanged);
            this._gFROverrideBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MaskDecimal_KeyPress);
            // 
            // _gDeviceAddrLabel
            // 
            this._gDeviceAddrLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._gDeviceAddrLabel.AutoSize = true;
            this._gDeviceAddrLabel.Location = new System.Drawing.Point(6, 11);
            this._gDeviceAddrLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this._gDeviceAddrLabel.Name = "_gDeviceAddrLabel";
            this._gDeviceAddrLabel.Size = new System.Drawing.Size(276, 25);
            this._gDeviceAddrLabel.TabIndex = 2;
            this._gDeviceAddrLabel.Text = "Device Address";
            this._gDeviceAddrLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _gDeviceAddrNumeric
            // 
            this._gDeviceAddrNumeric.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._gDeviceAddrNumeric.Location = new System.Drawing.Point(294, 8);
            this._gDeviceAddrNumeric.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._gDeviceAddrNumeric.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this._gDeviceAddrNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._gDeviceAddrNumeric.Name = "_gDeviceAddrNumeric";
            this._gDeviceAddrNumeric.Size = new System.Drawing.Size(276, 31);
            this._gDeviceAddrNumeric.TabIndex = 8;
            this._ttProvider.SetToolTip(this._gDeviceAddrNumeric, "The address of the device that will be used to stream commands.");
            this._gDeviceAddrNumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._gDeviceAddrNumeric.ValueChanged += new System.EventHandler(this.Field_ValueChanged);
            // 
            // _gStreamIdLabel
            // 
            this._gStreamIdLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._gStreamIdLabel.AutoSize = true;
            this._gStreamIdLabel.Location = new System.Drawing.Point(6, 59);
            this._gStreamIdLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this._gStreamIdLabel.Name = "_gStreamIdLabel";
            this._gStreamIdLabel.Size = new System.Drawing.Size(276, 25);
            this._gStreamIdLabel.TabIndex = 9;
            this._gStreamIdLabel.Text = "Stream Number";
            this._gStreamIdLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _gStreamIdNumeric
            // 
            this._gStreamIdNumeric.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._gStreamIdNumeric.Location = new System.Drawing.Point(294, 56);
            this._gStreamIdNumeric.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._gStreamIdNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._gStreamIdNumeric.Name = "_gStreamIdNumeric";
            this._gStreamIdNumeric.Size = new System.Drawing.Size(276, 31);
            this._gStreamIdNumeric.TabIndex = 12;
            this._ttProvider.SetToolTip(this._gStreamIdNumeric, "The ID of the stream that will be used; usually 1.");
            this._gStreamIdNumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._gStreamIdNumeric.ValueChanged += new System.EventHandler(this.Field_ValueChanged);
            // 
            // _gTraverseRateLabel
            // 
            this._gTraverseRateLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._gTraverseRateLabel.AutoSize = true;
            this._gTraverseRateLabel.Location = new System.Drawing.Point(6, 107);
            this._gTraverseRateLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this._gTraverseRateLabel.Name = "_gTraverseRateLabel";
            this._gTraverseRateLabel.Size = new System.Drawing.Size(276, 25);
            this._gTraverseRateLabel.TabIndex = 11;
            this._gTraverseRateLabel.Text = "Traverse Rate (data value)";
            this._gTraverseRateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _gTraverseRateNumeric
            // 
            this._gTraverseRateNumeric.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._gTraverseRateNumeric.Location = new System.Drawing.Point(294, 104);
            this._gTraverseRateNumeric.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._gTraverseRateNumeric.Maximum = new decimal(new int[] {
            4194304,
            0,
            0,
            0});
            this._gTraverseRateNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._gTraverseRateNumeric.Name = "_gTraverseRateNumeric";
            this._gTraverseRateNumeric.Size = new System.Drawing.Size(276, 31);
            this._gTraverseRateNumeric.TabIndex = 21;
            this._ttProvider.SetToolTip(this._gTraverseRateNumeric, "The traverse rate of the device, measured in device units calculated as (data / 1" +
        ".6384) μstep/s");
            this._gTraverseRateNumeric.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this._gTraverseRateNumeric.ValueChanged += new System.EventHandler(this.Field_ValueChanged);
            // 
            // _gFROverrideLabel
            // 
            this._gFROverrideLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._gFROverrideLabel.AutoSize = true;
            this._gFROverrideLabel.Location = new System.Drawing.Point(6, 155);
            this._gFROverrideLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this._gFROverrideLabel.Name = "_gFROverrideLabel";
            this._gFROverrideLabel.Size = new System.Drawing.Size(276, 25);
            this._gFROverrideLabel.TabIndex = 22;
            this._gFROverrideLabel.Text = "Feed Rate Override (%)";
            this._gFROverrideLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _gRemappingKeyLabel
            // 
            this._gRemappingKeyLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._gRemappingKeyLabel.AutoSize = true;
            this._gRemappingKeyLabel.Location = new System.Drawing.Point(6, 270);
            this._gRemappingKeyLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this._gRemappingKeyLabel.Name = "_gRemappingKeyLabel";
            this._gRemappingKeyLabel.Size = new System.Drawing.Size(276, 25);
            this._gRemappingKeyLabel.TabIndex = 15;
            this._gRemappingKeyLabel.Text = "M-Code Remapping";
            this._gRemappingKeyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _gRemappingKeyBox
            // 
            this._gRemappingKeyBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._gRemappingKeyBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._gRemappingKeyBox.FormattingEnabled = true;
            this._gRemappingKeyBox.Location = new System.Drawing.Point(294, 266);
            this._gRemappingKeyBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._gRemappingKeyBox.Name = "_gRemappingKeyBox";
            this._gRemappingKeyBox.Size = new System.Drawing.Size(276, 33);
            this._gRemappingKeyBox.TabIndex = 17;
            this._ttProvider.SetToolTip(this._gRemappingKeyBox, "Select the M-Code to be remapped.");
            this._gRemappingKeyBox.DropDown += new System.EventHandler(this.ComboBox_DropDown);
            this._gRemappingKeyBox.SelectedIndexChanged += new System.EventHandler(this.Field_ValueChanged);
            // 
            // _gRemappingValueLabel
            // 
            this._gRemappingValueLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._gRemappingValueLabel.AutoSize = true;
            this._gRemappingValueLabel.Location = new System.Drawing.Point(6, 318);
            this._gRemappingValueLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this._gRemappingValueLabel.Name = "_gRemappingValueLabel";
            this._gRemappingValueLabel.Size = new System.Drawing.Size(276, 25);
            this._gRemappingValueLabel.TabIndex = 16;
            this._gRemappingValueLabel.Text = "Command";
            this._gRemappingValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _gRemappingValueBox
            // 
            this._gRemappingValueBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._gRemappingValueBox.Location = new System.Drawing.Point(294, 315);
            this._gRemappingValueBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._gRemappingValueBox.Name = "_gRemappingValueBox";
            this._gRemappingValueBox.Size = new System.Drawing.Size(276, 31);
            this._gRemappingValueBox.TabIndex = 18;
            this._ttProvider.SetToolTip(this._gRemappingValueBox, "Enter the command(s) that will be outputted when this M-Code is encountered in th" +
        "e source.");
            this._gRemappingValueBox.TextChanged += new System.EventHandler(this.Field_ValueChanged);
            // 
            // _gWCSKeyBox
            // 
            this._gWCSKeyBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._gWCSKeyBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._gWCSKeyBox.FormattingEnabled = true;
            this._gWCSKeyBox.Location = new System.Drawing.Point(294, 381);
            this._gWCSKeyBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._gWCSKeyBox.Name = "_gWCSKeyBox";
            this._gWCSKeyBox.Size = new System.Drawing.Size(276, 33);
            this._gWCSKeyBox.TabIndex = 24;
            this._ttProvider.SetToolTip(this._gWCSKeyBox, "Select the Work Coordinate System to define.");
            this._gWCSKeyBox.DropDown += new System.EventHandler(this.ComboBox_DropDown);
            this._gWCSKeyBox.SelectedIndexChanged += new System.EventHandler(this.Field_ValueChanged);
            // 
            // _gWCSKeyLabel
            // 
            this._gWCSKeyLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._gWCSKeyLabel.AutoSize = true;
            this._gWCSKeyLabel.Location = new System.Drawing.Point(6, 385);
            this._gWCSKeyLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this._gWCSKeyLabel.Name = "_gWCSKeyLabel";
            this._gWCSKeyLabel.Size = new System.Drawing.Size(276, 25);
            this._gWCSKeyLabel.TabIndex = 25;
            this._gWCSKeyLabel.Text = "Work Coordinate System";
            this._gWCSKeyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _gWCSValueBox
            // 
            this._gWCSValueBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._gWCSValueBox.Enabled = false;
            this._gWCSValueBox.Location = new System.Drawing.Point(294, 435);
            this._gWCSValueBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._gWCSValueBox.Name = "_gWCSValueBox";
            this._gWCSValueBox.Size = new System.Drawing.Size(276, 31);
            this._gWCSValueBox.TabIndex = 26;
            this._ttProvider.SetToolTip(this._gWCSValueBox, "Enter a tuple of offsets to be applied when this WCS is active, in the units defi" +
        "ned by G20 or G21.");
            this._gWCSValueBox.TextChanged += new System.EventHandler(this.Field_ValueChanged);
            this._gWCSValueBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.GWCSValueBox_KeyPress);
            this._gWCSValueBox.Leave += new System.EventHandler(this.GWCSValueBox_Leave);
            // 
            // _gOptDelLabel
            // 
            this._gOptDelLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._gOptDelLabel.AutoSize = true;
            this._gOptDelLabel.Location = new System.Drawing.Point(6, 203);
            this._gOptDelLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this._gOptDelLabel.Name = "_gOptDelLabel";
            this._gOptDelLabel.Size = new System.Drawing.Size(276, 25);
            this._gOptDelLabel.TabIndex = 28;
            this._gOptDelLabel.Text = "Optional Block Delete";
            this._gOptDelLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _gOptDelBox
            // 
            this._gOptDelBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._gOptDelBox.AutoSize = true;
            this._gOptDelBox.Location = new System.Drawing.Point(418, 202);
            this._gOptDelBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._gOptDelBox.Name = "_gOptDelBox";
            this._gOptDelBox.Size = new System.Drawing.Size(28, 27);
            this._gOptDelBox.TabIndex = 29;
            this._ttProvider.SetToolTip(this._gOptDelBox, "If checked, deletes lines starting wtih a \'/\'.");
            this._gOptDelBox.UseVisualStyleBackColor = true;
            this._gOptDelBox.CheckedChanged += new System.EventHandler(this.Field_ValueChanged);
            // 
            // _axisSettingsGroup
            // 
            this._axisSettingsGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._layoutPanel.SetColumnSpan(this._axisSettingsGroup, 2);
            this._axisSettingsGroup.Controls.Add(this._axisLayoutPanel);
            this._axisSettingsGroup.Location = new System.Drawing.Point(618, 62);
            this._axisSettingsGroup.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._axisSettingsGroup.Name = "_axisSettingsGroup";
            this._axisSettingsGroup.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._axisSettingsGroup.Size = new System.Drawing.Size(548, 530);
            this._axisSettingsGroup.TabIndex = 12;
            this._axisSettingsGroup.TabStop = false;
            this._axisSettingsGroup.Text = "Axis Settings";
            // 
            // _axisLayoutPanel
            // 
            this._axisLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._axisLayoutPanel.ColumnCount = 2;
            this._axisLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._axisLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._axisLayoutPanel.Controls.Add(this._axisLabel, 0, 0);
            this._axisLayoutPanel.Controls.Add(this._axisBox, 1, 0);
            this._axisLayoutPanel.Controls.Add(this.label2, 0, 6);
            this._axisLayoutPanel.Controls.Add(this._xMaxPositionNumeric, 1, 6);
            this._axisLayoutPanel.Controls.Add(this._xMinPositionNumeric, 1, 5);
            this._axisLayoutPanel.Controls.Add(this._xMinPositionLabel, 0, 5);
            this._axisLayoutPanel.Controls.Add(this._xMicrostepSizeLabel, 0, 4);
            this._axisLayoutPanel.Controls.Add(this._xMicrostepSizeBox, 1, 4);
            this._axisLayoutPanel.Controls.Add(this._xAddrLabel, 0, 3);
            this._axisLayoutPanel.Controls.Add(this._xEnabledLabel, 0, 2);
            this._axisLayoutPanel.Controls.Add(this._xAddrNumeric, 1, 3);
            this._axisLayoutPanel.Controls.Add(this._xEnabledBox, 1, 2);
            this._axisLayoutPanel.Location = new System.Drawing.Point(12, 37);
            this._axisLayoutPanel.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._axisLayoutPanel.Name = "_axisLayoutPanel";
            this._axisLayoutPanel.RowCount = 8;
            this._axisLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this._axisLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this._axisLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this._axisLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this._axisLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this._axisLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 58F));
            this._axisLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this._axisLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._axisLayoutPanel.Size = new System.Drawing.Size(524, 482);
            this._axisLayoutPanel.TabIndex = 0;
            // 
            // _axisLabel
            // 
            this._axisLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._axisLabel.AutoSize = true;
            this._axisLabel.Location = new System.Drawing.Point(6, 11);
            this._axisLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this._axisLabel.Name = "_axisLabel";
            this._axisLabel.Size = new System.Drawing.Size(250, 25);
            this._axisLabel.TabIndex = 1;
            this._axisLabel.Text = "Axis";
            this._axisLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _axisBox
            // 
            this._axisBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._axisBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._axisBox.FormattingEnabled = true;
            this._axisBox.Items.AddRange(new object[] {
            "X",
            "Y",
            "Z"});
            this._axisBox.Location = new System.Drawing.Point(268, 7);
            this._axisBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._axisBox.Name = "_axisBox";
            this._axisBox.Size = new System.Drawing.Size(250, 33);
            this._axisBox.TabIndex = 0;
            this._ttProvider.SetToolTip(this._axisBox, "Select an axis to modify settings for.");
            this._axisBox.SelectedIndexChanged += new System.EventHandler(this.Field_ValueChanged);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 309);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(250, 25);
            this.label2.TabIndex = 24;
            this.label2.Text = "Max Position (μstep)";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _xMaxPositionNumeric
            // 
            this._xMaxPositionNumeric.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._xMaxPositionNumeric.Enabled = false;
            this._xMaxPositionNumeric.Location = new System.Drawing.Point(268, 306);
            this._xMaxPositionNumeric.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._xMaxPositionNumeric.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this._xMaxPositionNumeric.Minimum = new decimal(new int[] {
            1000000000,
            0,
            0,
            -2147483648});
            this._xMaxPositionNumeric.Name = "_xMaxPositionNumeric";
            this._xMaxPositionNumeric.Size = new System.Drawing.Size(250, 31);
            this._xMaxPositionNumeric.TabIndex = 25;
            this._ttProvider.SetToolTip(this._xMaxPositionNumeric, "The maximum position that the axis can travel to, in μsteps. This can be obtained" +
        " by polling the axis for limit.max.");
            this._xMaxPositionNumeric.ValueChanged += new System.EventHandler(this.Field_ValueChanged);
            // 
            // _xMinPositionNumeric
            // 
            this._xMinPositionNumeric.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._xMinPositionNumeric.Enabled = false;
            this._xMinPositionNumeric.Location = new System.Drawing.Point(268, 253);
            this._xMinPositionNumeric.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._xMinPositionNumeric.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this._xMinPositionNumeric.Minimum = new decimal(new int[] {
            1000000000,
            0,
            0,
            -2147483648});
            this._xMinPositionNumeric.Name = "_xMinPositionNumeric";
            this._xMinPositionNumeric.Size = new System.Drawing.Size(250, 31);
            this._xMinPositionNumeric.TabIndex = 26;
            this._ttProvider.SetToolTip(this._xMinPositionNumeric, "The minimum position that the axis can travel to, in μsteps. This can be obtained" +
        " by polling the axis for limit.min.");
            this._xMinPositionNumeric.ValueChanged += new System.EventHandler(this.Field_ValueChanged);
            // 
            // _xMinPositionLabel
            // 
            this._xMinPositionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._xMinPositionLabel.AutoSize = true;
            this._xMinPositionLabel.Location = new System.Drawing.Point(6, 244);
            this._xMinPositionLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this._xMinPositionLabel.Name = "_xMinPositionLabel";
            this._xMinPositionLabel.Size = new System.Drawing.Size(250, 50);
            this._xMinPositionLabel.TabIndex = 23;
            this._xMinPositionLabel.Text = "Min Position and Machine Home (μstep)";
            this._xMinPositionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _xMicrostepSizeLabel
            // 
            this._xMicrostepSizeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._xMicrostepSizeLabel.AutoSize = true;
            this._xMicrostepSizeLabel.Location = new System.Drawing.Point(6, 203);
            this._xMicrostepSizeLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this._xMicrostepSizeLabel.Name = "_xMicrostepSizeLabel";
            this._xMicrostepSizeLabel.Size = new System.Drawing.Size(250, 25);
            this._xMicrostepSizeLabel.TabIndex = 2;
            this._xMicrostepSizeLabel.Text = "Microstep Size (μm)";
            this._xMicrostepSizeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _xMicrostepSizeBox
            // 
            this._xMicrostepSizeBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._xMicrostepSizeBox.Enabled = false;
            this._xMicrostepSizeBox.Location = new System.Drawing.Point(268, 200);
            this._xMicrostepSizeBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._xMicrostepSizeBox.Name = "_xMicrostepSizeBox";
            this._xMicrostepSizeBox.Size = new System.Drawing.Size(250, 31);
            this._xMicrostepSizeBox.TabIndex = 3;
            this._xMicrostepSizeBox.Text = "1.0";
            this._ttProvider.SetToolTip(this._xMicrostepSizeBox, "The distance in μm that is travelled on each microstep of this axis.");
            this._xMicrostepSizeBox.TextChanged += new System.EventHandler(this.Field_ValueChanged);
            this._xMicrostepSizeBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MaskDecimal_KeyPress);
            // 
            // _xAddrLabel
            // 
            this._xAddrLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._xAddrLabel.AutoSize = true;
            this._xAddrLabel.Location = new System.Drawing.Point(6, 155);
            this._xAddrLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this._xAddrLabel.Name = "_xAddrLabel";
            this._xAddrLabel.Size = new System.Drawing.Size(250, 25);
            this._xAddrLabel.TabIndex = 6;
            this._xAddrLabel.Text = "Axis Number";
            this._xAddrLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _xEnabledLabel
            // 
            this._xEnabledLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._xEnabledLabel.AutoSize = true;
            this._xEnabledLabel.Location = new System.Drawing.Point(6, 107);
            this._xEnabledLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this._xEnabledLabel.Name = "_xEnabledLabel";
            this._xEnabledLabel.Size = new System.Drawing.Size(250, 25);
            this._xEnabledLabel.TabIndex = 4;
            this._xEnabledLabel.Text = "Behaviour";
            this._xEnabledLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _xAddrNumeric
            // 
            this._xAddrNumeric.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._xAddrNumeric.Enabled = false;
            this._xAddrNumeric.Location = new System.Drawing.Point(268, 152);
            this._xAddrNumeric.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._xAddrNumeric.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this._xAddrNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._xAddrNumeric.Name = "_xAddrNumeric";
            this._xAddrNumeric.Size = new System.Drawing.Size(250, 31);
            this._xAddrNumeric.TabIndex = 7;
            this._ttProvider.SetToolTip(this._xAddrNumeric, "The number that this axis is addressed with.");
            this._xAddrNumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._xAddrNumeric.ValueChanged += new System.EventHandler(this.Field_ValueChanged);
            // 
            // _xEnabledBox
            // 
            this._xEnabledBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._xEnabledBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._xEnabledBox.FormattingEnabled = true;
            this._xEnabledBox.Location = new System.Drawing.Point(268, 102);
            this._xEnabledBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._xEnabledBox.Name = "_xEnabledBox";
            this._xEnabledBox.Size = new System.Drawing.Size(250, 33);
            this._xEnabledBox.TabIndex = 27;
            this._ttProvider.SetToolTip(this._xEnabledBox, "Select how the axis will be treated in the source code; disabled axes will raise " +
        "an error if used, and ignored axes will be ignored without any output.");
            this._xEnabledBox.SelectedIndexChanged += new System.EventHandler(this.Field_ValueChanged);
            // 
            // _presetGroup
            // 
            this._presetGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._layoutPanel.SetColumnSpan(this._presetGroup, 2);
            this._presetGroup.Controls.Add(this._presetLayoutPanel);
            this._presetGroup.Location = new System.Drawing.Point(6, 604);
            this._presetGroup.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._presetGroup.Name = "_presetGroup";
            this._presetGroup.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._layoutPanel.SetRowSpan(this._presetGroup, 2);
            this._presetGroup.Size = new System.Drawing.Size(600, 100);
            this._presetGroup.TabIndex = 13;
            this._presetGroup.TabStop = false;
            this._presetGroup.Text = "Presets";
            // 
            // _presetLayoutPanel
            // 
            this._presetLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._presetLayoutPanel.ColumnCount = 4;
            this._presetLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.00501F));
            this._presetLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66166F));
            this._presetLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66166F));
            this._presetLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.67167F));
            this._presetLayoutPanel.Controls.Add(this._presetBox, 0, 0);
            this._presetLayoutPanel.Controls.Add(this._loadButton, 1, 0);
            this._presetLayoutPanel.Controls.Add(this._saveButton, 2, 0);
            this._presetLayoutPanel.Controls.Add(this._deleteButton, 3, 0);
            this._presetLayoutPanel.Location = new System.Drawing.Point(24, 38);
            this._presetLayoutPanel.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._presetLayoutPanel.Name = "_presetLayoutPanel";
            this._presetLayoutPanel.RowCount = 1;
            this._presetLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._presetLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this._presetLayoutPanel.Size = new System.Drawing.Size(558, 50);
            this._presetLayoutPanel.TabIndex = 0;
            // 
            // _presetBox
            // 
            this._presetBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._presetBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._presetBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._presetBox.FormattingEnabled = true;
            this._presetBox.Items.AddRange(new object[] {
            "test1",
            "test2",
            "test3"});
            this._presetBox.Location = new System.Drawing.Point(6, 6);
            this._presetBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._presetBox.Name = "_presetBox";
            this._presetBox.Size = new System.Drawing.Size(267, 33);
            this._presetBox.TabIndex = 3;
            this._ttProvider.SetToolTip(this._presetBox, "Select the name of a preset to save to, load from, or delete.");
            // 
            // _loadButton
            // 
            this._loadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._loadButton.Location = new System.Drawing.Point(281, 4);
            this._loadButton.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this._loadButton.Name = "_loadButton";
            this._loadButton.Size = new System.Drawing.Size(88, 42);
            this._loadButton.TabIndex = 1;
            this._loadButton.Text = "Load";
            this._loadButton.UseVisualStyleBackColor = true;
            this._loadButton.Click += new System.EventHandler(this.LoadButton_Click);
            // 
            // _saveButton
            // 
            this._saveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._saveButton.Location = new System.Drawing.Point(373, 4);
            this._saveButton.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this._saveButton.Name = "_saveButton";
            this._saveButton.Size = new System.Drawing.Size(88, 42);
            this._saveButton.TabIndex = 0;
            this._saveButton.Text = "Save";
            this._saveButton.UseVisualStyleBackColor = true;
            this._saveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // _deleteButton
            // 
            this._deleteButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._deleteButton.Location = new System.Drawing.Point(465, 4);
            this._deleteButton.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this._deleteButton.Name = "_deleteButton";
            this._deleteButton.Size = new System.Drawing.Size(91, 42);
            this._deleteButton.TabIndex = 6;
            this._deleteButton.Text = "Del";
            this._deleteButton.UseVisualStyleBackColor = true;
            this._deleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // _okButton
            // 
            this._okButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._okButton.Location = new System.Drawing.Point(618, 660);
            this._okButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._okButton.Name = "_okButton";
            this._okButton.Size = new System.Drawing.Size(268, 44);
            this._okButton.TabIndex = 7;
            this._okButton.Text = "OK";
            this._okButton.UseVisualStyleBackColor = true;
            // 
            // _importButton
            // 
            this._importButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._importButton.Location = new System.Drawing.Point(618, 604);
            this._importButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._importButton.Name = "_importButton";
            this._importButton.Size = new System.Drawing.Size(268, 44);
            this._importButton.TabIndex = 5;
            this._importButton.Text = "Import From File";
            this._importButton.UseVisualStyleBackColor = true;
            this._importButton.Click += new System.EventHandler(this.ImportButton_Click);
            // 
            // _cancelButton
            // 
            this._cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._cancelButton.Location = new System.Drawing.Point(898, 660);
            this._cancelButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._cancelButton.Name = "_cancelButton";
            this._cancelButton.Size = new System.Drawing.Size(268, 44);
            this._cancelButton.TabIndex = 6;
            this._cancelButton.Text = "Cancel";
            this._cancelButton.UseVisualStyleBackColor = true;
            // 
            // _exportButton
            // 
            this._exportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._exportButton.Location = new System.Drawing.Point(898, 604);
            this._exportButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._exportButton.Name = "_exportButton";
            this._exportButton.Size = new System.Drawing.Size(268, 44);
            this._exportButton.TabIndex = 4;
            this._exportButton.Text = "Export To File";
            this._exportButton.UseVisualStyleBackColor = true;
            this._exportButton.Click += new System.EventHandler(this.ExportButton_Click);
            // 
            // _loadDeviceButton
            // 
            this._loadDeviceButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._loadDeviceButton.Location = new System.Drawing.Point(6, 6);
            this._loadDeviceButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._loadDeviceButton.Name = "_loadDeviceButton";
            this._loadDeviceButton.Size = new System.Drawing.Size(294, 44);
            this._loadDeviceButton.TabIndex = 14;
            this._loadDeviceButton.Text = "Load settings from device";
            this._ttProvider.SetToolTip(this._loadDeviceButton, "Automatically infer settings from the chosen device. M-Code remappings and WCS se" +
        "ttings still need to be manually input. ");
            this._loadDeviceButton.UseVisualStyleBackColor = true;
            this._loadDeviceButton.Click += new System.EventHandler(this.LoadDeviceButton_Click);
            // 
            // _deviceNumberBox
            // 
            this._deviceNumberBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._deviceNumberBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._deviceNumberBox.FormattingEnabled = true;
            this._deviceNumberBox.Location = new System.Drawing.Point(312, 11);
            this._deviceNumberBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this._deviceNumberBox.Name = "_deviceNumberBox";
            this._deviceNumberBox.Size = new System.Drawing.Size(294, 33);
            this._deviceNumberBox.TabIndex = 15;
            // 
            // _ttProvider
            // 
            this._ttProvider.AutomaticDelay = 750;
            // 
            // ConfigDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1222, 758);
            this.Controls.Add(this._layoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.HelpButton = true;
            this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfigDialog";
            this.Text = "Configuration Manager";
            this._layoutPanel.ResumeLayout(false);
            this._generalSettingsGroup.ResumeLayout(false);
            this._generalLayoutPanel.ResumeLayout(false);
            this._generalLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._gDeviceAddrNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._gStreamIdNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._gTraverseRateNumeric)).EndInit();
            this._axisSettingsGroup.ResumeLayout(false);
            this._axisLayoutPanel.ResumeLayout(false);
            this._axisLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._xMaxPositionNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._xMinPositionNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._xAddrNumeric)).EndInit();
            this._presetGroup.ResumeLayout(false);
            this._presetLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel _layoutPanel;
		private System.Windows.Forms.Button _saveButton;
		private System.Windows.Forms.Button _loadButton;
		private System.Windows.Forms.ComboBox _presetBox;
		private System.Windows.Forms.Button _exportButton;
		private System.Windows.Forms.Button _importButton;
		private System.Windows.Forms.Button _cancelButton;
		private System.Windows.Forms.Button _okButton;
		private System.Windows.Forms.GroupBox _generalSettingsGroup;
		private System.Windows.Forms.TableLayoutPanel _generalLayoutPanel;
		private System.Windows.Forms.GroupBox _axisSettingsGroup;
		private System.Windows.Forms.Label _gDeviceAddrLabel;
		private System.Windows.Forms.NumericUpDown _gDeviceAddrNumeric;
		private System.Windows.Forms.Label _gTraverseRateLabel;
		private System.Windows.Forms.Label _gStreamIdLabel;
		private System.Windows.Forms.NumericUpDown _gStreamIdNumeric;
		private System.Windows.Forms.Label _gRemappingKeyLabel;
		private System.Windows.Forms.Label _gRemappingValueLabel;
		private System.Windows.Forms.ComboBox _gRemappingKeyBox;
		private System.Windows.Forms.TextBox _gRemappingValueBox;
		private System.Windows.Forms.NumericUpDown _gTraverseRateNumeric;
		private System.Windows.Forms.TableLayoutPanel _axisLayoutPanel;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label _axisLabel;
		private System.Windows.Forms.ComboBox _axisBox;
		private System.Windows.Forms.Label _xEnabledLabel;
		private System.Windows.Forms.Label _xAddrLabel;
		private System.Windows.Forms.NumericUpDown _xAddrNumeric;
		private System.Windows.Forms.Label _xMicrostepSizeLabel;
		private System.Windows.Forms.TextBox _xMicrostepSizeBox;
		private System.Windows.Forms.Label _xMinPositionLabel;
		private System.Windows.Forms.NumericUpDown _xMaxPositionNumeric;
		private System.Windows.Forms.NumericUpDown _xMinPositionNumeric;
		private System.Windows.Forms.ComboBox _xEnabledBox;
		private System.Windows.Forms.GroupBox _presetGroup;
		private System.Windows.Forms.TableLayoutPanel _presetLayoutPanel;
		private System.Windows.Forms.Button _deleteButton;
		private System.Windows.Forms.Button _loadDeviceButton;
		private System.Windows.Forms.ComboBox _deviceNumberBox;
		private System.Windows.Forms.Label _gFROverrideLabel;
		private System.Windows.Forms.TextBox _gFROverrideBox;
		private System.Windows.Forms.ComboBox _gWCSKeyBox;
		private System.Windows.Forms.Label _gWCSKeyLabel;
		private System.Windows.Forms.Label _gWCSValueLabel;
		private System.Windows.Forms.TextBox _gWCSValueBox;
		private System.Windows.Forms.ToolTip _ttProvider;
		private System.Windows.Forms.Label _gOptDelLabel;
		private System.Windows.Forms.CheckBox _gOptDelBox;
	}
}
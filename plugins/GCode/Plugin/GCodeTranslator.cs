﻿#define FAST

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Zaber.PlugIns;
using Zaber.Telemetry;

namespace Zaber.GCode.Plugin
{
	[PlugIn(Name = "G-Code Translator", Description = "Converts G-Code to Zaber device commands.")]
	public partial class GCodeTranslator : UserControl
	{
		public class RunArgs
		{
			public Conversation Conversation;
			public Queue<ZaberCommand> CommandList;
			public object CommandListLock;
			public int Progress;
			public GCodeTranslator Translator;
		}

		public static readonly string ConfigDir =
			Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
						 "Zaber Technologies\\Zaber Console\\GCodeTranslator");


		/// <summary>
		///     Public ctor
		/// </summary>
		public GCodeTranslator()
		{
			InitializeComponent();
			Directory.CreateDirectory(ConfigDir);

			UpToDateChanged += This_UpToDateChanged;
			RunWorker.DoWork += RunWorkerExtract.RunWorker_DoWork;
			StepWorker.DoWork += StepWorker_DoWork;
			HandleCreated += This_HandleCreated;

			if (FontFamily.Families.Select(x => x.Name).Contains("Consolas"))
			{
				// use Consolas if we have it
				_outputText.Font = new Font("Consolas", 9.25F);
				_inputText.Font = new Font("Consolas", 9.25F);
			}
			else
			{
				_outputText.Font = new Font("Courier New", 9.25F);
				_inputText.Font = new Font("Courier New", 9.25F);
			}

			_exportBox.SelectedIndex = 0;
		}


		public void This_HandleCreated(object sender, EventArgs e)
		{
			try
			{
				using (var r = new StreamReader(ConfigurationFile))
				{
					Config = Configuration.Deserialize(r);
				}
			}
			catch (Exception)
			{
				Config = new Configuration
				{
					X =
					{
						EnabledType = AxisEnabledType.Enabled,
						MaxPosition = 10000
					},
					Y =
					{
						EnabledType = AxisEnabledType.Enabled,
						MaxPosition = 10000
					}
				};

			}

			try
			{
				using (var r = new StreamReader(InputFile))
				{
					UpToDate = bool.Parse(r.ReadLine());
					_loadedFile = r.ReadLine();
					if (_loadedFile == "")
					{
						_loadedFile = null;
					}

					_inputText.Text = r.ReadToEnd();
				}
			}
			catch (Exception)
			{
			}
		}


		[PlugInMethod(PlugInMethodAttribute.EventType.PluginDeactivated)]
		public void Parent_FormClosing()
		{
			using (var w = new StreamWriter(InputFile))
			{
				w.WriteLine(UpToDate);
				w.WriteLine(_loadedFile ?? "");
				w.Write(_inputText.Text);
			}

			if (null != Config)
			{
				using (var w = new StreamWriter(ConfigurationFile))
				{
					Configuration.Serialize(w, Config);
				}
			}
		}


		public void InitStream(Conversation conv)
		{
			conv.Request("stop");
			conv.PollUntilIdle();
			conv.Request(
				$"stream {Config.StreamNumber} setup live {string.Join(" ", Config.EnabledAxes.Select(x => x.AxisNumber.ToString()).ToArray())}");
		}


		/// <summary>
		///     The configuration that the translator will use when translating.
		/// </summary>
		public Configuration Config { get; private set; }

		[PlugInProperty]
		public Conversation Conversation { get; set; }

		[PlugInProperty]
		public ZaberPortFacade ZaberPortFacade { get; set; }

		private bool UpToDate
		{
			get => _upToDate;
			set
			{
				_upToDate = value;

				UpToDateChanged?.Invoke(this, new EventArgs());
			}
		}


		protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
		{
			if (keyData == (Keys.Control | Keys.S))
			{
				SaveButton_Click(this, new EventArgs());
				return true;
			}

			return base.ProcessCmdKey(ref msg, keyData);
		}


		private void This_UpToDateChanged(object sender, EventArgs e) => _saveButton.Invalidate();


		private void SaveButton_Paint(object sender, PaintEventArgs e)
		{
			var size = _saveButton.Size;
			var pen = new Pen(Color.FromArgb(185, UpToDate ? Color.Green : Color.Red)) { Width = 2 };
			e.Graphics.DrawLine(pen, 3, size.Height - 3, size.Width - 3, size.Height - 3);
		}


		private void ExportButton_Click(object sender, EventArgs e)
		{
			if (_commands == null)
			{
				MessageBox.Show("Please translate some code first.",
								"Error",
								MessageBoxButtons.OK,
								MessageBoxIcon.Error);
				return;
			}

			var dialog = new SaveFileDialog
			{
				InitialDirectory = _loadedFile == null
					? Path.GetDirectoryName(_loadedFile)
					: Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
				CheckFileExists = false,
				OverwritePrompt = true
			};

			switch (_exportBox.SelectedIndex)
			{
				case 0:
					dialog.FileName = Path.GetFileName(Path.ChangeExtension(_loadedFile, ".cs"));
					dialog.Filter = "C# scripts|*.cs";
					break;
				case 1:
					dialog.FileName = Path.GetFileName(Path.ChangeExtension(_loadedFile, ".txt"));
					dialog.Filter = "Text files|*.txt";
					break;
			}

			if (dialog.ShowDialog() != DialogResult.OK)
			{
				return;
			}

			using (var output = new StreamWriter(dialog.FileName, false))
			{
				switch (_exportBox.SelectedIndex)
				{
					case 0: // ZC script
						output.Write(LiteralInterp
									 .InterpSrc(new Dictionary<string, string>
												{
													{ "STREAMID", Config.StreamNumber.ToString() },
													{ "DEVICEID", Config.DeviceAddress.ToString() },
													{
														"AXES", string.Join(" ",
																			Config.EnabledAxes.Select(x
																										  => x
																										 .AxisNumber
																										 .ToString())
																			   .ToArray())
													}
												},
												_commands.Select(cmd =>
												{
													if (cmd.Prefix.StartsWith("/"))
													{
														return $"\"{cmd.Command}\",";
													}

													return $"//\"{cmd.Prefix}{cmd.Command}\",";
												})));
						_eventLog.LogEvent("Exported translated G-Code", "To script");
						break;
					case 1: // ASCII commands
						foreach (var cmd in _commands)
						{
							output.Write(cmd.ToString());
						}

						_eventLog.LogEvent("Exported translated G-Code", "As list of commands");
						break;
					default:
						MessageBox.Show("Not supported.");
						break;
				}
			}
		}


		private void SaveButton_Click(object sender, EventArgs e)
		{
			var dialog = new SaveFileDialog
			{
				InitialDirectory = _loadedFile == null
					? Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
					: Path.GetDirectoryName(_loadedFile),
				FileName = Path.GetFileName(_loadedFile ?? ""),
				CheckFileExists = false,
				OverwritePrompt = true
			};

			if (dialog.ShowDialog() != DialogResult.OK)
			{
				return;
			}

			_loadedFile = dialog.FileName;

			using (var output = new StreamWriter(dialog.FileName, false))
			{
				output.Write(_inputText.Text);
			}

			UpToDate = true;
		}


		private void LoadButton_Click(object sender, EventArgs e)
		{
			var dialog = new OpenFileDialog
			{
				InitialDirectory = _loadedFile == null
					? Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
					: Path.GetDirectoryName(_loadedFile),
				Multiselect = false,
				CheckFileExists = true,
				CheckPathExists = true,
				Filter = "All files|*"
			};

			if (dialog.ShowDialog() != DialogResult.OK)
			{
				return;
			}

			_loadedFile = dialog.FileName;
			var input = new StreamReader(dialog.FileName);
			_inputText.Text = input.ReadToEnd();

			input.Close();

			UpToDate = true;
		}


		private void ConvertButton_Click(object sender, EventArgs e)
		{
			if (TranslateWorker.IsBusy)
			{
				_resultLabel.Text = "Cancelling...";
				_cancelTranslate = true;
			}
			else
			{
				_cancelTranslate = false;
				_prevUpToDate = UpToDate;

				_runButton.Enabled = false;
				_stepButton.Enabled = false;
				_outputText.ResetText();
				_convertButton.Text = CancelTxt;

				TranslateWorker.RunWorkerAsync();
				_eventLog.LogEvent("Translate G-Code");
			}
		}


		private void ConfigButton_Click(object sender, EventArgs e)
		{
			var dialog = new ConfigDialog(Config, ZaberPortFacade);

			if (dialog.ShowDialog() == DialogResult.OK)
			{
				Config = dialog.Configuration;

				lock (_commandsListLock)
				{
					if (dialog.Changed)
					{
						_outputText.Text = "";
						_commands = null;
						_commandsList = null;
					}
				}
			}
		}


		private void PreviousMessage_Click(object sender, EventArgs e)
		{
			var start = _inputText.SelectionStart;

			while (_indexToAlert.ContainsKey(--start))
			{
				if (start < 0)
				{
					start = _inputText.TextLength - 1;
				}

				if (start == _inputText.SelectionStart)
				{
					// looped back to initial position
					break;
				}
			}

			while (!_indexToAlert.ContainsKey(--start))
			{
				if (start < 0)
				{
					start = _inputText.TextLength - 1;
				}
			}

			_inputText.SelectionStart = start;
			_inputText.SelectionLength = 1;
			_inputText.ScrollToCaret();
			_inputText.Focus();
		}


		private void NextMessage_Click(object sender, EventArgs e)
		{
			var start = _inputText.SelectionStart;

			while (_indexToAlert.ContainsKey(++start))
			{
				if (start >= _inputText.TextLength)
				{
					start = 0;
				}

				if (start == _inputText.SelectionStart)
				{
					break;
				}
			}

			while (!_indexToAlert.ContainsKey(++start))
			{
				if (start >= _inputText.TextLength)
				{
					start = 0;
				}

				if (start == _inputText.SelectionStart)
				{
					break;
				}
			}

			_inputText.SelectionStart = start;
			_inputText.SelectionLength = 1;
			_inputText.ScrollToCaret();
			_inputText.Focus();
		}


		private void RunButton_Click(object sender, EventArgs e)
		{
			if (ZaberPortFacade == null)
			{
				MessageBox.Show("Please open a serial port.");
				return;
			}

			var conv = ZaberPortFacade.AllConversations
								   .FirstOrDefault(x => x.Device.DeviceNumber == Config.DeviceAddress);

			if (conv == null)
			{
				MessageBox.Show($"Please connect device {Config.DeviceAddress} and rescan the serial port.");
				return;
			}

			var warn = "";

			// validate device settings
			try
			{
				try
				{
					var dat = (int) conv.Request("get stream.numstreams").NumericData;
					if (dat < Config.StreamNumber)
					{
						warn +=
							$"Stream ID used ({Config.StreamNumber}) exceeds number of streams device supports ({dat}). ";
					}
				}
				catch (ErrorResponseException)
				{
					warn += "Device does not support streams. ";
				}

				var maxspeed = conv.Request("get maxspeed");

				if (maxspeed.NumericValues.Count < Config.EnabledAxes.Count())
				{
					warn += string.Format("Number of used axes ({1}) exceeds number of axes device supports ({0}). ",
										  maxspeed.NumericValues.Count,
										  Config.EnabledAxes.Count());
				}
				else
				{
					if (maxspeed.NumericValues.Min() < Config.TraverseRate)
					{
						warn +=
							$"Traversal rate ({Config.TraverseRate}) exceeds lowest maxspeed of all axes ({maxspeed.NumericValues.Min()}). ";
					}

					var actualsize = conv.Request("get peripheralid")
									  .NumericValues
									  .Select(peripheralId =>
										  {
											  if (peripheralId == 0)
											  {
												  return null;
											  }

											  var devType = conv.Device.DeviceType.GetPeripheralById((int)peripheralId);
											  var command = devType.GetCommandByText("get pos");

											  Measurement uM;
											  if (command?.ResponseUnitScale != null)
											  {
												  if (command.ResponseUnit.MotionType != MotionType.Linear)
												  {
													  warn +=
														  "G-Code Translator does not correctly support nonlinear axes.";
												  }

												  var converter = conv.Device.UnitConverter;
												  uM = converter.Convert(
													  new Measurement(1m / command.ResponseUnitScale.Value,
																	  command.ResponseUnit),
													  converter.FindUnitByAbbreviation(UnitOfMeasure.MicrometerSymbol));
											  }
											  else
											  {
												  warn += string.Format(
													  "Could not determine microstep physical size correctly. "
												  + "Please check that the peripheral IDs of your device axes are set correctly.");
												  uM = new Measurement(1.0, UnitOfMeasure.Micrometer);
											  }

											  return uM;
										  })
									  .Zip(conv.Request("get resolution").NumericValues, (unit, resolution) => unit != null ? unit / resolution.Value : null)
									  .ToList();

					var minpos = conv.Request("get limit.min").NumericValues;
					var maxpos = conv.Request("get limit.max").NumericValues;
					
					foreach (var axis in Config.EnabledAxes)
					{
						var axisIndex = axis.AxisNumber - 1;
						if (actualsize[axisIndex] is null)
						{
							warn += $"Axis {axis.AxisNumber} is disabled. ";
							continue;
						}

						if (Math.Abs((actualsize[axisIndex] - new Measurement(axis.MicrostepSize, UnitOfMeasure.Micrometer)).Value)
						> MicrostepSizeTolerance)
						{
							warn +=
								$"Axis {axis.AxisNumber} microstep size ({actualsize[axisIndex] + new Measurement(0, UnitOfMeasure.Micrometer)}) differs from config microstep size ({axis.MicrostepSize}μm). ";
						}

						if (axis.MaxPosition > maxpos[axisIndex])
						{
							warn +=
								$"Axis {axis.AxisNumber} maximum position ({maxpos[axisIndex]}) is less than config maximum position ({axis.MaxPosition}). ";
						}

						if (axis.MinPosition < minpos[axisIndex])
						{
							warn +=
								$"Axis {axis.AxisNumber} minimum position ({minpos[axisIndex]}) is greater than config minimum position ({axis.MinPosition}). ";
						}
					}
				}
			}
			catch (Exception x)
			{
				warn += "An error occurred while validating configuration information: ";
				warn += x.ToString();
			}

			if ((warn == "")
			|| (MessageBox.Show(warn + "Continue?", "Warning", MessageBoxButtons.YesNo) == DialogResult.Yes))
			{
				_stopButton.Enabled = true;
				_runButton.Enabled = false;
				_stepButton.Enabled = false;
				_convertButton.Enabled = false;
				_configButton.Enabled = false;

				RunWorker.RunWorkerAsync(new RunArgs
				{
					Conversation = conv,
					CommandList = _commandsList,
					CommandListLock = _commandsListLock,
					Progress = _commandsProg,
					Translator = this
				});

				_eventLog.LogEvent("Run translated G-Code");
			}
		}


		private void StopButton_Click(object sender, EventArgs e)
		{
			if (RunWorker.IsBusy)
			{
				RunWorker.CancelAsync();
			}
			else if (StepWorker.IsBusy)
			{
				StepWorker.CancelAsync();
			}

			lock (_commandsListLock)
			{
				ResetQueue();
				SelectLine(-1);
			}
		}


		private void StepButton_Click(object sender, EventArgs e)
		{
			if (ZaberPortFacade == null)
			{
				MessageBox.Show("Please open a serial port.");
				return;
			}

			var conv = ZaberPortFacade.AllConversations
								   .FirstOrDefault(x => x.Device.DeviceNumber == Config.DeviceAddress);

			if (conv == null)
			{
				MessageBox.Show($"Please connect device {Config.DeviceAddress} and rescan the serial port.");
				return;
			}

			_stepButton.Enabled = false;
			_runButton.Enabled = false;
			_stopButton.Enabled = true;
			_convertButton.Enabled = false;
			_configButton.Enabled = false;

			StepWorker.RunWorkerAsync(new RunArgs
			{
				Conversation = conv,
				CommandList = _commandsList,
				CommandListLock = _commandsListLock,
				Progress = _commandsProg,
				Translator = this
			});
		}


		private void InputText_MouseMove(object sender, MouseEventArgs e)
		{
			if (_indexToAlert == null)
			{
				return;
			}

			var key = _inputText.GetCharIndexFromPosition(_inputText.PointToClient(Cursor.Position));

			if (_prevKey != key)
			{
				if (_indexToAlert.ContainsKey(key))
				{
					_alertTooltip.Show(string.Join("\n\n",
												   _indexToAlert[key]
												   .Select(x => $"{x.Type}: {x.Message}")
												   .ToArray()),
									   _inputText,
									   _inputText.PointToClient(Cursor.Position) + new Size(5, 5));
				}
				else
				{
					_alertTooltip.Hide(_inputText);
				}

				_prevKey = key;
			}
		}


		private void InputText_TextChanged(object sender, EventArgs e) => UpToDate = false;


		private static void StepWorker_DoWork(object sender, DoWorkEventArgs e)
		{
			var args = e.Argument as RunArgs;
			var conv = args.Conversation;
			var timer500 = new TimeoutTimer { Timeout = 500 };

			if (args.Progress == 0)
			{
				args.Translator.InitStream(conv);
			}

			lock (args.CommandListLock)
			{
				while (true)
				{
					if (args.Translator.StepWorker.CancellationPending)
					{
						conv.Request("stop");
						break;
					}

					DeviceMessage msg = null;

					try
					{
						var topic = conv.StartTopic();
						conv.Device.SendInUnits(args.CommandList.Peek().Command, null, topic.MessageId);

						if (!topic.Wait(timer500))
						{
							throw new RequestTimeoutException();
						}

						topic.Validate();
						msg = topic.Response;
					}
					catch (Exception x)
					{
						conv.Request("stop");
						MessageBox.Show($"Command {args.CommandList.Peek()} was rejected: {x}. Aborted.");
						args.CommandList.Clear();
						args.Translator.StepWorker.ReportProgress(0, args.Progress + 1);
						return;
					}

					// validate 
					if (msg.IsAgain)
					{
						Thread.Sleep(50);
						continue;
					}

					break;
				}

				conv.PollUntilIdle();
				args.CommandList.Dequeue();
				args.Translator.StepWorker.ReportProgress(0, args.Progress + 1);
			}
		}


		private void RunWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			lock (_commandsListLock)
			{
				ResetQueue();
			}
		}


		private void StepWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			lock (_commandsListLock)
			{
				if (_commandsList.Count < 1)
				{
					ResetQueue();
				}
				else
				{
					_stepButton.Enabled = true;
					_runButton.Enabled = true;
					_stopButton.Enabled = true;
				}
			}
		}


		private void TranslateWorker_DoWork(object sender, DoWorkEventArgs e)
		{
			var inputText = (string) EndInvoke(BeginInvoke(new Func<string>(() =>
			{
				_inputText.SelectAll();
				_inputText.SelectionBackColor = Color.White;

				if (!_inputText.Text.EndsWith("\n"))
				{
					_inputText.AppendText("\n");
				}

				_resultLabel.Text = "Translating...";
				_resultLabel.Refresh();

				return _inputText.Text;
			})));

			_lineToIndex = new List<int>();
			_indexToAlert = new Dictionary<int, List<LogMessage>>();

			var position = 0;
			foreach (var str in inputText.Split('\n'))
			{
				_lineToIndex.Add(position);

				position += str.Length + 1;
			}

			var translator = new Translator(new StringReader(inputText), Config);

			_commands = translator.Translate(ref _cancelTranslate);

			if (_commands != null)
			{
				var sb = new StringBuilder();
				foreach (var line in _commands)
				{
					sb.Append(line);
				}

				EndInvoke(BeginInvoke(new Action(() =>
				{
					_outputText.AppendText(sb.ToString());

					lock (_commandsListLock)
					{
						_commandsList = new Queue<ZaberCommand>(_commands);
						_commandsProg = 0;
					}
				})));
			}
			else
			{
				EndInvoke(BeginInvoke(new Action(() =>
				{
					_stepButton.Enabled = false;
					_runButton.Enabled = false;
				})));
			}

			if (!_cancelTranslate)
			{
				EndInvoke(BeginInvoke(new Action(() =>
				{
					_resultLabel.Text =
						$"Highlighting {translator.Messages.Warnings} warnings and {translator.Messages.Errors} errors...";
				})));
			}

			var cap = 1000;

			foreach (var err in translator.Messages.Log.OrderBy(x => x.Type))
			{
				if (_cancelTranslate || (cap-- < 0))
				{
					break;
				}

				var linestart = _lineToIndex[err.Line];
				var index = linestart + err.Start;

				// Set the end to the appropriate value.
				// If no end was specified, the end will be the end of line if no index was specified either,
				// or a single character otherwise.
				int end;

				if ((err.Line + 1) >= _lineToIndex.Count)
				{
					end = _inputText.TextLength;
				}
				else
				{
					end = linestart + err.End;
				}

				for (var i = index; i < end; i++)
				{
					if (_cancelTranslate)
					{
						break;
					}

					#if !FAST // this section makes it so that a space at the end of a word is not highlighted
					if (i == end - 1 && char.IsWhiteSpace(_inputText.Text[i]))
					{
						continue;
					}
#endif

					var list = _indexToAlert.ContainsKey(i)
						? _indexToAlert[i]
						: _indexToAlert[i] = new List<LogMessage>();

					list.Add(err);

					EndInvoke(BeginInvoke(new Action(() =>
					{
						_inputText.Select(i, 1);

						switch (err.Type)
						{
							case LogType.Error:
								_inputText.SelectionBackColor = Color.FromArgb(212, 128, 128);
								break;
							case LogType.Warning:
								_inputText.SelectionBackColor = Color.FromArgb(255, 255, 128);
								break;
						}
					})));
				}
			}

			EndInvoke(BeginInvoke(new Action(() =>
			{
				lock (_commandsListLock)
				{
					_resultLabel.Text =
						$"{(_commands != null ? "Succeeded" : "Failed")} with {translator.Messages.Warnings} warnings and {translator.Messages.Errors} errors.";

					if (_cancelTranslate)
					{
						_resultLabel.Text += " Highlighting cancelled partway.";
					}

					if ((translator.Messages.Warnings > 0) || (translator.Messages.Errors > 0))
					{
						_resultLabel.Text +=
							string.Format("{0}{0}Hover over highlighted code to see details.", Environment.NewLine);
						_previousMessage.Enabled = true;
						_nextMessage.Enabled = true;
					}
					else
					{
						_previousMessage.Enabled = false;
						_nextMessage.Enabled = false;
					}
				}
			})));

			// TODO: for some reason the cancel/translate state machine can get messed up if there is no sleep
			Thread.Sleep(100); 
		}


		private void TranslateWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			if (e.Error != null)
			{
				_resultLabel.Text = "An error occurred.";
				throw e.Error;
			}

			if ((_commandsList == null) && _cancelTranslate)
			{
				_resultLabel.Text = "Cancelled.";
			}

			lock (_commandsListLock)
			{
				_cancelTranslate = false;
				_stepButton.Enabled = _runButton.Enabled = (_commandsList != null) && (_commandsList.Count > 0);
				_convertButton.Text = TranslateTxt;
				UpToDate = _prevUpToDate;
			}
		}


		private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			SelectLine((int) e.UserState);
			_commandsProg = (int) e.UserState;
		}


		/// <summary>
		///     Select a line. If -1 is passed, the box is cleared of all coloring.
		/// </summary>
		/// <param name="line"></param>
		private void SelectLine(int line)
		{
			if (line < 0)
			{
				_outputText.SelectAll();
				_outputText.SelectionBackColor = SystemColors.Control;
				return;
			}

			var start = _outputText.GetFirstCharIndexFromLine(line);
			var end = _outputText.GetFirstCharIndexFromLine(line + 1);

			if (line > 0)
			{
				var before = _outputText.GetFirstCharIndexFromLine(line - 1);
				_outputText.Select(before, start - before);
				_outputText.SelectionBackColor = SystemColors.Control;
			}
			else
			{
				_outputText.SelectAll();
				_outputText.SelectionBackColor = SystemColors.Control;
			}

			_outputText.Select(start, (end > 0 ? end : _outputText.Text.Length) - start);
			_outputText.SelectionBackColor = Color.FromArgb(255, 130, 130, 130);
		}


		private void ResetQueue()
		{
			SelectLine(-1);
			_stepButton.Enabled = true;
			_runButton.Enabled = true;
			_stopButton.Enabled = false;
			_commandsList = new Queue<ZaberCommand>(_commands ?? new ZaberCommand[0]);
			_commandsProg = 0;
			_convertButton.Enabled = true;
			_configButton.Enabled = true;
		}


		/// <summary>
		///     Fired whenever the input text buffer's contents change.
		/// </summary>
		private event EventHandler UpToDateChanged;

		public const string TranslateTxt = "&Translate →";
		public const string CancelTxt = "&Cancel";

		private static readonly string InputFile =
			Path.Combine(ConfigDir, "Input.txt");

		private static readonly string ConfigurationFile =
			Path.Combine(ConfigDir, "Configuration.xml");

		private const int IconMargin = 5;

		/// <summary>
		///     The tolerance between configuration microstep size and the actual microstep size of the device
		///     that the form accepts.
		/// </summary>
		private const decimal MicrostepSizeTolerance = 0.000001M;

		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("G-Code tab");

		private readonly object _commandsListLock = new object();

		/// <summary>
		///     Maps line number to character index in the input box.
		/// </summary>
		private List<int> _lineToIndex;

		/// <summary>
		///     Maps character index to an alert info.
		/// </summary>
		private Dictionary<int, List<LogMessage>> _indexToAlert;

		/// <summary>
		///     Used in InputText_MouseMove.
		/// </summary>
		private int _prevKey = -1;

		/// <summary>
		///     The output of the translate action.
		/// </summary>
		private IEnumerable<ZaberCommand> _commands;

		/// <summary>
		///     Queue of commands that is initialized with the contents of `_commands`.
		/// </summary>
		private Queue<ZaberCommand> _commandsList;

		private int _commandsProg;

		/// <summary>
		///     Whether the input buffer has been saved.
		/// </summary>
		private bool _upToDate = true;

		/// <summary>
		///     The state of _upToDate before a translation action.
		/// </summary>
		private bool _prevUpToDate = true;

		/// <summary>
		///     The previous file that was loaded.
		/// </summary>
		private string _loadedFile;

		/// <summary>
		///     Cancels translation if true.
		/// </summary>
		private bool _cancelTranslate;
	}
}

     ______________________________
    |  ______     _                |
    | |___  /    | |               |
    |    / / __ _| |__   ___ _ __  |
    |   / / / _` | '_ \ / _ \ '__| |
    |  / /_| (_| | |_) |  __/ |    |
    | /_____\__,_|_.__/ \___|_|    |
    |________  _______        _    |__________  _  _________  _  __________
            | |__   __|      | |               | |           (_)           |
            |    | | ___  ___| |__  _ __   ___ | | ___   __ _ _  ___  ___  |
            |    | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __| |
            |    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \ |
            |    |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/ |
            |__________________________________________  __/ | ____________|
                                                        |___/              

                                 G-Code Translator

    === OVERVIEW ===
    Zaber Technologies' G-Code Translator is a utility plugin for Zaber
    Console that allows G-Code (also known as RS-274) commands to interface 
    with Zaber devices using streamed motion commands.

    This distribution includes the following files:
    * README.txt (this file),
    * LICENSE.txt,
    * Zaber.GCode.Core.dll,
    * Zaber.GCode.Plugin.dll, and
    * several example files under the Examples directory
    
    === DOWNLOAD ===
    The latest version of Zaber's G-Code Translator can be found at
    http://zaber.com/support/?tab=Software
    
    === INSTALLATION AND MANUAL ===
    To install, copy Zaber.GCode.Core.dll and Zaber.GCode.Plugin.dll into 
    Zaber Console's plug-in directory. For more information, the full manual
    can be found at:
    http://zaber.com/wiki/Software/Zaber_Console/G-Code_Translator
    
    === USING THE EXAMPLES ===
    Install the plugin as directed in the link above. Navigate to the G-Code
    Translator tab, click the "Open" button, and select one of the .ngc files.
    Click the "Edit Config" button, click the "Import From File" button, and
    select the provided .cfg file in the Examples folder. The configuration
    provided is for an ASR100B120B-T3. Click the "Translate" button, at which
    point you may run the script directly if you have an ASR attached to a
    two-axis controller, or export the output as a script which can be run
    later.
    
    === LICENSE ===
    Zaber Technologies' G-Code Translator is distributed under the Apache 2.0
    License. See LICENSE.txt for more details.
    
    === DEVELOPERS ===
    If you wish to use the G-Code Translator without Zaber Console, the
    library functions provided by Zaber.GCode.dll are free for use and
    documentation can be found at:
    http://zaber.com/wiki/Software/Zaber_Console/G-Code_Translator
    
    === CONTACT AND SUPPORT ===
    For support, Zaber Technologies can be reached in the following ways:
    * email at contact@zaber.com,
    * by telephone at 1-888-276-8033 (toll-free Canada/USA)
    * by telephone at 1-604-569-3780 (direct)

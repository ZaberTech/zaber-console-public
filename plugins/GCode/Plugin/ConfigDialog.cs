﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Zaber.GCode.Plugin
{
	public partial class ConfigDialog : Form
	{
		public static readonly string PresetDir =
			Path.Combine(GCodeTranslator.ConfigDir, "Presets");


		/// <summary>
		///     Public ctor.
		/// </summary>
		/// <param name="config">Configuration to populate values with. Cannot be null.</param>
		/// <exception cref="ArgumentNullException">if `config` is null</exception>
		public ConfigDialog(Configuration config, ZaberPortFacade port)
		{
			if (config is null)
			{
				throw new ArgumentNullException(nameof(config));
			}

			InitializeComponent();

			Directory.CreateDirectory(PresetDir);
			_configDirWatcher = new FileSystemWatcher(PresetDir, "*." + ConfigExt);

			foreach (var i in Enum.GetValues(typeof(AxisEnabledType)))
			{
				_xEnabledBox.Items.Add(i);
			}

			foreach (var i in AllowedRemappings)
			{
				_gRemappingKeyBox.Items.Add("M" + i + (config.Remappings.ContainsKey(i) ? "*" : ""));
			}

			for (var i = 0; i < WCSNames.Length; i++)
			{
				_gWCSKeyBox.Items.Add(WCSNames[i] + (config.CoordinateSystems.ContainsKey(i) ? "*" : ""));
			}

			_configDirWatcher.EnableRaisingEvents = true;
			_configDirWatcher.Changed += PresetDirWatcher_Changed;
			_configDirWatcher.Deleted += PresetDirWatcher_Changed;
			_configDirWatcher.Created += PresetDirWatcher_Changed;
			PresetDirWatcher_Changed(this, null);

			ConfigurationChanged += (sender, e) => ReadFromConfig();
			Configuration = config;
			_axisBox.SelectedIndex = 0;

			Changed = false;

			_deviceNumberBox.Enabled = _loadDeviceButton.Enabled = false;

			if (port != null)
			{
				_conversations = new List<Conversation>(port.Conversations.Where(x => x.Device.DeviceNumber != 0));

				foreach (var i in _conversations)
				{
					_deviceNumberBox.Items.Add($"{i.Device.DeviceNumber} - {i.Device.DeviceType.Name ?? string.Empty}");
				}

				if (_deviceNumberBox.Items.Count > 0)
				{
					_deviceNumberBox.Enabled = _loadDeviceButton.Enabled = true;
					_deviceNumberBox.SelectedIndex = 0;
				}
			}
		}


		public Configuration Configuration
		{
			get => _config;
			private set
			{
				_config = value;

				ConfigurationChanged?.Invoke(this, new EventArgs());
			}
		}

		/// <summary>
		///     Whether the configuration has changed since when it was opened.
		/// </summary>
		public bool Changed { get; private set; }

		private Configuration.Axis CurrentAxis
		{
			get
			{
				switch (_axisBox.SelectedIndex)
				{
					case 0:
						return Configuration.X;
					case 1:
						return Configuration.Y;
					case 2:
						return Configuration.Z;
					default:
						return null;
				}
			}
		}


		private void ReadFromConfig()
		{
			_gDeviceAddrNumeric.Value = Configuration.DeviceAddress;
			_gStreamIdNumeric.Value = Configuration.StreamNumber;
			_gTraverseRateNumeric.Value = Configuration.TraverseRate;
			_gFROverrideBox.Text = (Configuration.FeedRateOverride * 100M).ToString();
			_gOptDelBox.Checked = Configuration.OptionalDelete;

			Field_ValueChanged(_gRemappingKeyBox, new EventArgs());
			Field_ValueChanged(_axisBox, new EventArgs());
		}


		private void SaveButton_Click(object sender, EventArgs e)
		{
			if (_presetBox.Text == "")
			{
				MessageBox.Show("Please enter a configuration name.",
								"Error",
								MessageBoxButtons.OK,
								MessageBoxIcon.Error);
				return;
			}

			var f = new StreamWriter(Path.Combine(PresetDir,
												  Path.GetExtension(_presetBox.Text) == ConfigExt
													  ? _presetBox.Text
													  : Path.ChangeExtension(_presetBox.Text, ConfigExt)));
			Configuration.Serialize(f, Configuration);
			f.Close();
		}


		private void LoadButton_Click(object sender, EventArgs e)
		{
			if (_presetBox.Text == "")
			{
				MessageBox.Show("Please enter a configuration to load.");
				return;
			}

			try
			{
				var f = new StreamReader(Path.Combine(PresetDir,
													  Path.GetExtension(_presetBox.Text) == ConfigExt
														  ? _presetBox.Text
														  : Path.ChangeExtension(_presetBox.Text, ConfigExt)));
				Configuration = Configuration.Deserialize(f);
				f.Close();
			}
			catch (FileNotFoundException)
			{
				MessageBox.Show("Configuration does not exist.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			catch (DirectoryNotFoundException)
			{
				MessageBox.Show("Please enter a configuration to load.",
								"Error",
								MessageBoxButtons.OK,
								MessageBoxIcon.Error);
			}

			ReadFromConfig();
		}


		private void DeleteButton_Click(object sender, EventArgs e)
		{
			if (_presetBox.Text == "")
			{
				MessageBox.Show("Please enter a configuration to delete.");
				return;
			}

			try
			{
				File.Delete(Path.Combine(PresetDir,
										 Path.GetExtension(_presetBox.Text) == ConfigExt
											 ? _presetBox.Text
											 : Path.ChangeExtension(_presetBox.Text, ConfigExt)));
			}
			catch (Exception)
			{
				MessageBox.Show("Configuration does not exist.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}


		private void ExportButton_Click(object sender, EventArgs e)
		{
			var dialog = new SaveFileDialog
			{
				InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
				CheckFileExists = false,
				OverwritePrompt = true,
				Filter = "Configuration file|*.cfg"
			};

			if (dialog.ShowDialog() != DialogResult.OK)
			{
				return;
			}

			TextWriter output = new StreamWriter(dialog.FileName);
			Configuration.Serialize(output, Configuration);
			output.Close();
		}


		private void ImportButton_Click(object sender, EventArgs e)
		{
			var dialog = new OpenFileDialog
			{
				InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
				CheckFileExists = true,
				Filter = "Configuration file|*.cfg"
			};

			if (dialog.ShowDialog() != DialogResult.OK)
			{
				return;
			}

			TextReader input = new StreamReader(dialog.FileName);
			try
			{
				Configuration = Configuration.Deserialize(input);
			}
			catch (Exception)
			{
				MessageBox.Show("Invalid configuration file.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

			input.Close();

			ReadFromConfig();
		}


		private void LoadDeviceButton_Click(object sender, EventArgs e)
		{
			var iDev = _deviceNumberBox.SelectedIndex;
			if ((iDev < 0) || (iDev >= _conversations.Count))
			{
				MessageBox.Show("Invalid selection", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			var conv = _conversations[iDev];

			_gDeviceAddrNumeric.Value = conv.Device.DeviceNumber;
			_presetBox.Text = conv.Device.DeviceType.Name;

			var minSpeed = decimal.MaxValue;

			try
			{
				_gStreamIdNumeric.Value = 1;
				conv.Request("get stream.numstreams");
			}
			catch (ErrorResponseException)
			{
				MessageBox.Show("This device does not support streaming",
								"Warning",
								MessageBoxButtons.OK,
								MessageBoxIcon.Warning);
			}

			try
			{
				// axis parameters
				for (var i = 0; i < _axisBox.Items.Count; i++)
				{
					_axisBox.SelectedIndex = i;

					// set axis to disabled if is initially enabled and beyond device axis count, but keep ignore status
					if (i >= conv.Axes.Count)
					{
						if ((AxisEnabledType) _xEnabledBox.SelectedItem == AxisEnabledType.Enabled)
						{
							_xEnabledBox.SelectedItem = AxisEnabledType.Disabled;
						}

						continue;
					}

					_xEnabledBox.SelectedItem = AxisEnabledType.Enabled;
					_xAddrNumeric.Value = i + 1;
					_xMinPositionNumeric.Value = conv.Axes[i].Request("get limit.min").NumericData;
					_xMaxPositionNumeric.Value = conv.Axes[i].Request("get limit.max").NumericData;

					var devType = conv.Axes[i].Device.DeviceType;
					var command = devType.GetCommandByText("get pos");

					if (command?.ResponseUnitScale != null)
					{
						if (command.ResponseUnit.MotionType != MotionType.Linear)
						{
							throw new InvalidDataException(
								"G-Code Translator does not correctly support nonlinear axes.");
						}

						var divisor = command.ResponseUnitScale.Value
								  * conv.Axes[i].Request("get resolution").NumericData;

						var converter = conv.Axes[i].Device.UnitConverter;
						var uM = converter.Convert(new Measurement(1m / divisor, command.ResponseUnit),
												   converter.FindUnitByAbbreviation(UnitOfMeasure.MicrometerSymbol));

						_xMicrostepSizeBox.Text = uM.Value.ToString();
					}
					else
					{
						throw new InvalidDataException(
							"Could not determine microstep physical size from the Position setting units. "
						+ "Please check that the peripheral IDs of your device axes are set correctly.");
					}

					minSpeed = Math.Min(minSpeed, conv.Axes[i].Request("get maxspeed").NumericData);
				}

				_gTraverseRateNumeric.Value = minSpeed;
			}
			catch (Exception x)
			{
				MessageBox.Show("An error occured while querying device: " + x);
			}
		}


		private void MaskDecimal_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!"0123456789".Contains(e.KeyChar) && !char.IsControl(e.KeyChar) && (e.KeyChar != '.'))
			{
				e.Handled = true;
			}
		}


		private void GWCSValueBox_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!"0123456789".Contains(e.KeyChar)
			&& !char.IsControl(e.KeyChar)
			&& !"., ".Contains(e.KeyChar))
			{
				e.Handled = true;
			}

			var text = _gWCSValueBox.Text;

			if ((e.KeyChar == ',') && (text.Count(x => x == ',') > 1))
			{
				e.Handled = true;
			}

			if ((text.Length > 0)
			&& (text[text.Length - 1] == ',')
			&& "0123456789.".Contains(e.KeyChar))
			{
				_gWCSValueBox.AppendText(" ");
			}
		}


		private void Field_ValueChanged(object sender, EventArgs e)
		{
			if (sender == _xMicrostepSizeBox)
			{
				try
				{
					CurrentAxis.MicrostepSize =
						_xMicrostepSizeBox.Text == "" ? 1M : decimal.Parse(_xMicrostepSizeBox.Text);
					Changed = true;
				}
				catch (Exception)
				{
					MessageBox.Show("Number cannot be parsed.");
					_xMicrostepSizeBox.Text = "1";
				}
			}
			else if (sender == _gFROverrideBox)
			{
				try
				{
					_config.FeedRateOverride =
						_gFROverrideBox.Text == "" ? 1M : decimal.Parse(_gFROverrideBox.Text) / 100M;
					Changed = true;
				}
				catch (Exception)
				{
					MessageBox.Show("Number cannot be parsed.");
					_xMicrostepSizeBox.Text = "100.0";
				}
			}
			else if (sender == _axisBox)
			{
				if (_axisBox.SelectedIndex < 0)
				{
					_xEnabledBox.Enabled = false;
				}
				else
				{
					var axis = CurrentAxis;

					_xEnabledBox.SelectedItem = axis.EnabledType;
					_xAddrNumeric.Value = axis.AxisNumber;
					_xMaxPositionNumeric.Value = axis.MaxPosition;
					_xMinPositionNumeric.Value = axis.MinPosition;
					_xMicrostepSizeBox.Text = axis.MicrostepSize.ToString();

					_xEnabledBox.Enabled = true;
				}
			}
			else if (sender == _xEnabledBox)
			{
				CurrentAxis.EnabledType = (AxisEnabledType) _xEnabledBox.SelectedItem;

				var selectedEnableType = (AxisEnabledType) _xEnabledBox.SelectedItem;
				if (AxisEnabledType.Enabled == selectedEnableType)
				{
					_xAddrNumeric.Enabled = true;
					_xMaxPositionNumeric.Enabled = true;
					_xMinPositionNumeric.Enabled = true;
					_xMicrostepSizeBox.Enabled = true;
				}
				else if (AxisEnabledType.Passthrough == selectedEnableType)
				{
					_xAddrNumeric.Enabled = false;
					_xMaxPositionNumeric.Enabled = true;
					_xMinPositionNumeric.Enabled = true;
					_xMicrostepSizeBox.Enabled = true;
				}
				else
				{
					_xAddrNumeric.Enabled = false;
					_xMaxPositionNumeric.Enabled = false;
					_xMinPositionNumeric.Enabled = false;
					_xMicrostepSizeBox.Enabled = false;
				}

				Changed = true;
			}
			else if (sender == _gRemappingKeyBox)
			{
				if (_gRemappingKeyBox.SelectedIndex < 0)
				{
					_gRemappingValueBox.Enabled = false;
				}
				else
				{
					var key = AllowedRemappings[_gRemappingKeyBox.SelectedIndex];

					_gRemappingValueBox.Text = Configuration.Remappings.ContainsKey(key)
						? Configuration.Remappings[key]
						: "";

					_gRemappingValueBox.Enabled = true;
				}
			}
			else if (sender == _gRemappingValueBox)
			{
				var key = AllowedRemappings[_gRemappingKeyBox.SelectedIndex];

				if (_gRemappingValueBox.Text == "")
				{
					Configuration.Remappings.Remove(key);
				}
				else
				{
					Configuration.Remappings[key] = _gRemappingValueBox.Text;
				}

				_gRemappingPendingUpdate = key;
				Changed = true;
			}
			else if (sender == _gWCSKeyBox)
			{
				if (_gWCSKeyBox.SelectedIndex < 0)
				{
					_gWCSValueBox.Enabled = false;
				}
				else
				{
					_gWCSValueBox.Text = Configuration.CoordinateSystems.ContainsKey(_gWCSKeyBox.SelectedIndex)
						? Configuration.CoordinateSystems[_gWCSKeyBox.SelectedIndex].ToString()
						: "";

					_gWCSValueBox.Enabled = true;
				}
			}
			else if (sender == _gWCSValueBox)
			{
				var key = _gWCSKeyBox.SelectedIndex;
				if (_gWCSValueBox.Text == "")
				{
					Configuration.CoordinateSystems.Remove(key);
				}
				else
				{
					var tup = new Configuration.CoordTuple();
					var toks = _gWCSValueBox.Text.Split(new[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);
					var suc = true;

					if (toks.Length > 0)
					{
						suc &= decimal.TryParse(toks[0], out var parse);
						tup.X = parse;
					}

					if (toks.Length > 1)
					{
						suc &= decimal.TryParse(toks[1], out var parse);
						tup.Y = parse;
					}

					if (toks.Length > 2)
					{
						suc &= decimal.TryParse(toks[2], out var parse);
						tup.Z = parse;
					}

					if (!suc)
					{
						MessageBox.Show("Number cannot be parsed.");
						_gWCSValueBox.Text = "0, 0, 0";
						return;
					}

					Configuration.CoordinateSystems[key] = tup;
				}

				_gWCSPendingUpdate = key;
				Changed = true;
			}
			else if (sender == _gOptDelBox)
			{
				Configuration.OptionalDelete = _gOptDelBox.Checked;
				Changed = true;
			}
			else if (sender == _gTraverseRateNumeric)
			{
				Configuration.TraverseRate = (int) _gTraverseRateNumeric.Value;
				Changed = true;
			}
			else if (sender == _gStreamIdNumeric)
			{
				Configuration.StreamNumber = (int) _gStreamIdNumeric.Value;
				Changed = true;
			}
			else if (sender == _gDeviceAddrNumeric)
			{
				Configuration.DeviceAddress = (int) _gDeviceAddrNumeric.Value;
				Changed = true;
			}
			else if (sender == _xAddrNumeric)
			{
				CurrentAxis.AxisNumber = (int) _xAddrNumeric.Value;
				Changed = true;
			}
			else if (sender == _xMaxPositionNumeric)
			{
				CurrentAxis.MaxPosition = (int) _xMaxPositionNumeric.Value;
				Changed = true;
			}
			else if (sender == _xMinPositionNumeric)
			{
				CurrentAxis.MinPosition = (int) _xMinPositionNumeric.Value;
				Changed = true;
			}
		}


		private void GWCSValueBox_Leave(object sender, EventArgs e) => _gWCSValueBox.Text =
			Configuration.CoordinateSystems.ContainsKey(_gWCSKeyBox.SelectedIndex)
				? Configuration.CoordinateSystems[_gWCSKeyBox.SelectedIndex].ToString()
				: "";


		private void ComboBox_DropDown(object sender, EventArgs e)
		{
			if (sender == _gWCSKeyBox)
			{
				if (_gWCSPendingUpdate >= 0)
				{
					var up = _gWCSPendingUpdate;
					_gWCSPendingUpdate = -1;
					_gWCSKeyBox.Items[up] =
						WCSNames[up] + (_config.CoordinateSystems.ContainsKey(up) ? "*" : "");
				}
			}
			else if (sender == _gRemappingKeyBox)
			{
				if (_gRemappingPendingUpdate >= 0)
				{
					var up = AllowedRemappings[_gRemappingPendingUpdate];
					_gRemappingPendingUpdate = -1;
					_gRemappingKeyBox.Items[up] =
						"M" + AllowedRemappings[up] + (_config.Remappings.ContainsKey(up) ? "*" : "");
				}
			}
		}


		private void PresetDirWatcher_Changed(object sender, FileSystemEventArgs e)
		{
			if (InvokeRequired)
			{
				Invoke((Action) (() => PresetDirWatcher_Changed(sender, e)));
				return;
			}

			//Repopulate save and load boxes.
			_presetBox.Items.Clear();
			foreach (var i in Directory.GetFiles(PresetDir, "*." + ConfigExt))
			{
				_presetBox.Items.Add(Path.GetFileNameWithoutExtension(i));
			}
		}


		private event EventHandler ConfigurationChanged;

		/// <summary>
		///     A list of remappings that are allowed to be used. Note that 2 and 30 are not
		///     included because those cause a program stop.
		/// </summary>
		private static readonly int[] AllowedRemappings =
		{
			0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28,
			29
		};

		private static readonly string[] WCSNames =
		{
			"G54", "G55", "G56", "G57", "G58", "G59", "G59.1", "G59.2", "G59.3", "G54.1 P1", "G54.1 P2", "G54.1 P3",
			"G54.1 P4", "G54.1 P5", "G54.1 P6", "G54.1 P7", "G54.1 P8"
		};

		private const string ConfigExt = "cfg";

		/// <summary>
		///     Watcher for the configuration directory.
		/// </summary>
		private readonly FileSystemWatcher _configDirWatcher;

		/// <summary>
		///     The configuration that this dialog is bound to.
		/// </summary>
		private Configuration _config;

		/// <summary>
		///     Port facade.
		/// </summary>
		private readonly List<Conversation> _conversations;

		/// <summary>
		///     Indexes of ComboBox entries that are pending an update.
		///     This is necessary because updating the entry while it is selected will make the ComboBox freak out.
		/// </summary>
		private int _gWCSPendingUpdate = -1, _gRemappingPendingUpdate = -1;
	}
}

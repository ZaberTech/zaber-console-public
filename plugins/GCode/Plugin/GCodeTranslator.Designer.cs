﻿namespace Zaber.GCode.Plugin
{
	partial class GCodeTranslator
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this._alertTooltip = new System.Windows.Forms.ToolTip(this.components);
			this._runButton = new System.Windows.Forms.Button();
			this._resultLabel = new System.Windows.Forms.Label();
			this._configButton = new System.Windows.Forms.Button();
			this._previousMessage = new System.Windows.Forms.Button();
			this._nextMessage = new System.Windows.Forms.Button();
			this._saveButton = new System.Windows.Forms.Button();
			this._outputText = new System.Windows.Forms.RichTextBox();
			this._inputText = new System.Windows.Forms.RichTextBox();
			this._loadButton = new System.Windows.Forms.Button();
			this._layoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this._exportButton = new System.Windows.Forms.Button();
			this._convertButton = new System.Windows.Forms.Button();
			this._exportBox = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this._stopButton = new System.Windows.Forms.Button();
			this._stepButton = new System.Windows.Forms.Button();
			this.RunWorker = new System.ComponentModel.BackgroundWorker();
			this.StepWorker = new System.ComponentModel.BackgroundWorker();
			this.TranslateWorker = new System.ComponentModel.BackgroundWorker();
			this._layoutPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// _runButton
			// 
			this._runButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._runButton.Enabled = false;
			this._runButton.Location = new System.Drawing.Point(511, 254);
			this._runButton.Name = "_runButton";
			this._runButton.Size = new System.Drawing.Size(39, 23);
			this._runButton.TabIndex = 9;
			this._runButton.Text = "&Run";
			this._runButton.UseVisualStyleBackColor = true;
			this._runButton.Click += new System.EventHandler(this.RunButton_Click);
			// 
			// _resultLabel
			// 
			this._resultLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._resultLabel.AutoSize = true;
			this._layoutPanel.SetColumnSpan(this._resultLabel, 13);
			this._resultLabel.Location = new System.Drawing.Point(3, 280);
			this._resultLabel.Name = "_resultLabel";
			this._resultLabel.Size = new System.Drawing.Size(594, 20);
			this._resultLabel.TabIndex = 8;
			this._resultLabel.Text = "Load a file or type directly into the input box.";
			// 
			// _configButton
			// 
			this._configButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._configButton.Location = new System.Drawing.Point(142, 254);
			this._configButton.Name = "_configButton";
			this._configButton.Size = new System.Drawing.Size(74, 23);
			this._configButton.TabIndex = 12;
			this._configButton.Text = "&Edit Config";
			this._configButton.UseVisualStyleBackColor = true;
			this._configButton.Click += new System.EventHandler(this.ConfigButton_Click);
			// 
			// _previousMessage
			// 
			this._previousMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._previousMessage.Enabled = false;
			this._previousMessage.Location = new System.Drawing.Point(3, 254);
			this._previousMessage.Name = "_previousMessage";
			this._previousMessage.Size = new System.Drawing.Size(29, 23);
			this._previousMessage.TabIndex = 11;
			this._previousMessage.Text = "<<";
			this._previousMessage.UseVisualStyleBackColor = true;
			this._previousMessage.Click += new System.EventHandler(this.PreviousMessage_Click);
			// 
			// _nextMessage
			// 
			this._nextMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._nextMessage.Enabled = false;
			this._nextMessage.Location = new System.Drawing.Point(98, 254);
			this._nextMessage.Name = "_nextMessage";
			this._nextMessage.Size = new System.Drawing.Size(29, 23);
			this._nextMessage.TabIndex = 10;
			this._nextMessage.Text = ">>";
			this._nextMessage.UseVisualStyleBackColor = true;
			this._nextMessage.Click += new System.EventHandler(this.NextMessage_Click);
			// 
			// _saveButton
			// 
			this._saveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._layoutPanel.SetColumnSpan(this._saveButton, 2);
			this._saveButton.Location = new System.Drawing.Point(68, 3);
			this._saveButton.Name = "_saveButton";
			this._saveButton.Size = new System.Drawing.Size(59, 23);
			this._saveButton.TabIndex = 2;
			this._saveButton.Text = "&Save As";
			this._saveButton.UseVisualStyleBackColor = true;
			this._saveButton.Click += new System.EventHandler(this.SaveButton_Click);
			this._saveButton.Paint += new System.Windows.Forms.PaintEventHandler(this.SaveButton_Paint);
			// 
			// _outputText
			// 
			this._outputText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._outputText.BackColor = System.Drawing.SystemColors.Control;
			this._layoutPanel.SetColumnSpan(this._outputText, 6);
			this._outputText.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this._outputText.Location = new System.Drawing.Point(302, 32);
			this._outputText.Name = "_outputText";
			this._outputText.ReadOnly = true;
			this._outputText.Size = new System.Drawing.Size(295, 216);
			this._outputText.TabIndex = 4;
			this._outputText.Text = "Output";
			// 
			// _inputText
			// 
			this._inputText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._layoutPanel.SetColumnSpan(this._inputText, 7);
			this._inputText.DetectUrls = false;
			this._inputText.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this._inputText.Location = new System.Drawing.Point(3, 32);
			this._inputText.Name = "_inputText";
			this._inputText.Size = new System.Drawing.Size(293, 216);
			this._inputText.TabIndex = 3;
			this._inputText.Text = "(G-Code Input)";
			this._inputText.TextChanged += new System.EventHandler(this.InputText_TextChanged);
			this._inputText.MouseMove += new System.Windows.Forms.MouseEventHandler(this.InputText_MouseMove);
			// 
			// _loadButton
			// 
			this._loadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._layoutPanel.SetColumnSpan(this._loadButton, 2);
			this._loadButton.Location = new System.Drawing.Point(3, 3);
			this._loadButton.Name = "_loadButton";
			this._loadButton.Size = new System.Drawing.Size(59, 23);
			this._loadButton.TabIndex = 0;
			this._loadButton.Text = "&Open";
			this._loadButton.UseVisualStyleBackColor = true;
			this._loadButton.Click += new System.EventHandler(this.LoadButton_Click);
			// 
			// _layoutPanel
			// 
			this._layoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._layoutPanel.ColumnCount = 13;
			this._layoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
			this._layoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this._layoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this._layoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
			this._layoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this._layoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
			this._layoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
			this._layoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
			this._layoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
			this._layoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this._layoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
			this._layoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
			this._layoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 46F));
			this._layoutPanel.Controls.Add(this._exportButton, 7, 0);
			this._layoutPanel.Controls.Add(this._loadButton, 0, 0);
			this._layoutPanel.Controls.Add(this._inputText, 0, 1);
			this._layoutPanel.Controls.Add(this._outputText, 7, 1);
			this._layoutPanel.Controls.Add(this._saveButton, 2, 0);
			this._layoutPanel.Controls.Add(this._previousMessage, 0, 2);
			this._layoutPanel.Controls.Add(this._configButton, 5, 2);
			this._layoutPanel.Controls.Add(this._resultLabel, 0, 3);
			this._layoutPanel.Controls.Add(this._convertButton, 6, 2);
			this._layoutPanel.Controls.Add(this._exportBox, 8, 0);
			this._layoutPanel.Controls.Add(this._nextMessage, 3, 2);
			this._layoutPanel.Controls.Add(this.label1, 1, 2);
			this._layoutPanel.Controls.Add(this._stopButton, 12, 2);
			this._layoutPanel.Controls.Add(this._runButton, 11, 2);
			this._layoutPanel.Controls.Add(this._stepButton, 10, 2);
			this._layoutPanel.Location = new System.Drawing.Point(0, 0);
			this._layoutPanel.MinimumSize = new System.Drawing.Size(580, 206);
			this._layoutPanel.Name = "_layoutPanel";
			this._layoutPanel.RowCount = 4;
			this._layoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
			this._layoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this._layoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
			this._layoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this._layoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this._layoutPanel.Size = new System.Drawing.Size(600, 300);
			this._layoutPanel.TabIndex = 1;
			// 
			// _exportButton
			// 
			this._exportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._exportButton.Location = new System.Drawing.Point(302, 3);
			this._exportButton.Name = "_exportButton";
			this._exportButton.Size = new System.Drawing.Size(74, 23);
			this._exportButton.TabIndex = 14;
			this._exportButton.Text = "E&xport as";
			this._exportButton.UseVisualStyleBackColor = true;
			this._exportButton.Click += new System.EventHandler(this.ExportButton_Click);
			// 
			// _convertButton
			// 
			this._convertButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._convertButton.BackColor = System.Drawing.SystemColors.ControlDarkDark;
			this._convertButton.ForeColor = System.Drawing.Color.Transparent;
			this._convertButton.Location = new System.Drawing.Point(222, 254);
			this._convertButton.Name = "_convertButton";
			this._convertButton.Size = new System.Drawing.Size(74, 23);
			this._convertButton.TabIndex = 5;
			this._convertButton.Text = "&Translate →";
			this._convertButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this._convertButton.UseVisualStyleBackColor = false;
			this._convertButton.Click += new System.EventHandler(this.ConvertButton_Click);
			// 
			// _exportBox
			// 
			this._exportBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this._layoutPanel.SetColumnSpan(this._exportBox, 5);
			this._exportBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this._exportBox.FormattingEnabled = true;
			this._exportBox.Items.AddRange(new object[] {
            "Script",
            "ASCII commands"});
			this._exportBox.Location = new System.Drawing.Point(382, 4);
			this._exportBox.Name = "_exportBox";
			this._exportBox.Size = new System.Drawing.Size(215, 21);
			this._exportBox.TabIndex = 15;
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.label1.AutoSize = true;
			this._layoutPanel.SetColumnSpan(this.label1, 2);
			this.label1.Location = new System.Drawing.Point(38, 252);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(54, 26);
			this.label1.TabIndex = 16;
			this.label1.Text = "Warnings Errors";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// _stopButton
			// 
			this._stopButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._stopButton.Enabled = false;
			this._stopButton.Location = new System.Drawing.Point(556, 254);
			this._stopButton.Name = "_stopButton";
			this._stopButton.Size = new System.Drawing.Size(41, 23);
			this._stopButton.TabIndex = 13;
			this._stopButton.Text = "Sto&p";
			this._stopButton.UseVisualStyleBackColor = true;
			this._stopButton.Click += new System.EventHandler(this.StopButton_Click);
			// 
			// _stepButton
			// 
			this._stepButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._stepButton.Enabled = false;
			this._stepButton.Location = new System.Drawing.Point(466, 254);
			this._stepButton.Name = "_stepButton";
			this._stepButton.Size = new System.Drawing.Size(39, 23);
			this._stepButton.TabIndex = 17;
			this._stepButton.Text = "S&tep";
			this._stepButton.UseVisualStyleBackColor = true;
			this._stepButton.Click += new System.EventHandler(this.StepButton_Click);
			// 
			// RunWorker
			// 
			this.RunWorker.WorkerReportsProgress = true;
			this.RunWorker.WorkerSupportsCancellation = true;
			this.RunWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.Worker_ProgressChanged);
			this.RunWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.RunWorker_RunWorkerCompleted);
			// 
			// StepWorker
			// 
			this.StepWorker.WorkerReportsProgress = true;
			this.StepWorker.WorkerSupportsCancellation = true;
			this.StepWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.Worker_ProgressChanged);
			this.StepWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.StepWorker_RunWorkerCompleted);
			// 
			// TranslateWorker
			// 
			this.TranslateWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.TranslateWorker_DoWork);
			this.TranslateWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.TranslateWorker_RunWorkerCompleted);
			// 
			// GCodeTranslator
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this._layoutPanel);
			this.Name = "GCodeTranslator";
			this.Size = new System.Drawing.Size(600, 300);
			this._layoutPanel.ResumeLayout(false);
			this._layoutPanel.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ToolTip _alertTooltip;
		private System.Windows.Forms.Button _convertButton;
		private System.Windows.Forms.Button _runButton;
		private System.Windows.Forms.Label _resultLabel;
		private System.Windows.Forms.TableLayoutPanel _layoutPanel;
		private System.Windows.Forms.Button _stopButton;
		private System.Windows.Forms.Button _loadButton;
		private System.Windows.Forms.RichTextBox _inputText;
		private System.Windows.Forms.RichTextBox _outputText;
		private System.Windows.Forms.Button _saveButton;
		private System.Windows.Forms.Button _nextMessage;
		private System.Windows.Forms.Button _previousMessage;
		private System.Windows.Forms.Button _configButton;
		private System.Windows.Forms.Button _exportButton;
		private System.Windows.Forms.ComboBox _exportBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button _stepButton;
		public System.ComponentModel.BackgroundWorker RunWorker;
		public System.ComponentModel.BackgroundWorker StepWorker;
		public System.ComponentModel.BackgroundWorker TranslateWorker;
	}
}

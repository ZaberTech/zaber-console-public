﻿using System.Collections.Generic;

namespace Zaber.GCode
{
	/// <summary>
	///     A class that contains information about the build process.
	/// </summary>
	public class Logger
	{
		public Logger()
		{
			Block = "";
		}


		internal int Line { get; set; }

		internal string Block { get; set; }

		/// <summary>
		///     A list of messages that are contained in this Log.
		/// </summary>
		public List<LogMessage> Log { get; } = new List<LogMessage>();

		/// <summary>
		///     The number of errors produced.
		/// </summary>
		public int Errors { get; private set; }

		/// <summary>
		///     The number of warnings produced.
		/// </summary>
		public int Warnings { get; private set; }

		internal bool Succeeded => Errors < 1;


		internal void Error(string s)
		{
			Log.Add(new LogMessage(LogType.Error, s, Block, Line, 0, Block.Length));
			Errors++;
		}


		internal void Error(string s, int start, int end)
		{
			Log.Add(new LogMessage(LogType.Error, s, Block, Line, start, end));
			Errors++;
		}


		internal void Error(string s, Word word)
		{
			if (word.Start.HasValue && word.End.HasValue)
			{
				Error(s, word.Start.Value, word.End.Value);
			}
			else
			{
				Error(s);
			}
		}


		internal void Warn(string s)
		{
			Log.Add(new LogMessage(LogType.Warning, s, Block, Line, 0, Block.Length));
			Warnings++;
		}


		internal void Warn(string s, int start, int end)
		{
			Log.Add(new LogMessage(LogType.Warning, s, Block, Line, start, end));
			Warnings++;
		}


		internal void Warn(string s, Word word)
		{
			if (word.Start.HasValue && word.End.HasValue)
			{
				Warn(s, word.Start.Value, word.End.Value);
			}
			else
			{
				Warn(s);
			}
		}


		internal void Clear()
		{
			Log.Clear();
			Line = 0;
			Warnings = 0;
			Warnings = 0;
		}
	}
}

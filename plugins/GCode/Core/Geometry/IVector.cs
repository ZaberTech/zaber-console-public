﻿using System.Collections.Generic;

namespace Zaber.GCode.Geometry
{
	internal interface IVector
	{
		IEnumerable<double> Components { get; }

		int Dimension { get; }
	}
}
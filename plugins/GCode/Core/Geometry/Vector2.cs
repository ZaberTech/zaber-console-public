﻿using System;
using System.Collections.Generic;

namespace Zaber.GCode.Geometry
{
	internal class Vector2 : IVector
	{
		public Vector2(double x, double y)
		{
			X = x;
			Y = y;
		}


		public static Vector2 operator +(Vector2 left, Vector2 right)
			=> new Vector2(left.X + right.X, left.Y + right.Y);


		public static Vector2 operator -(Vector2 left, Vector2 right)
			=> new Vector2(left.X - right.X, left.Y - right.Y);


		public static Vector2 operator *(Vector2 left, double right) => new Vector2(left.X * right, left.Y * right);


		public static Vector2 operator /(Vector2 left, double right) => new Vector2(left.X / right, left.Y / right);


		public static double Dot(Vector2 left, Vector2 right) => (left.X * right.X) + (left.Y * right.Y);


		/// <summary>
		///     Returns the orthogonal component of the cross product between two vector2s.
		/// </summary>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <returns></returns>
		public static double Cross(Vector2 left, Vector2 right) => (left.X * right.Y) - (left.Y * right.X);


		public double X { get; }

		public double Y { get; }

		public double LenSq => (X * X) + (Y * Y);

		public double Len => Math.Sqrt(LenSq);

		/// <summary>
		///     Returns the angle formed by this vector.
		/// </summary>
		/// <returns></returns>
		public double Angle => Math.Atan2(Y, X);

		public IEnumerable<double> Components
		{
			get
			{
				yield return X;
				yield return Y;
			}
		}

		public int Dimension => 2;
	}
}
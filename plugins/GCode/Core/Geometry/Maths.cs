﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Zaber.GCode.Geometry
{
	internal static class Maths
	{
		/// <summary>
		///     Checks whether a line exceeds limits. When enumerated, both IVectors should be in the same order
		///     as Configuration.EnabledAxes.
		/// </summary>
		/// <returns></returns>
		internal static bool LineExceedsBounds(IVector first, IVector second, Configuration conf)
			=> !conf.EnabledAxes.Zip(first.Components,
									 second.Components,
									 (x, y, z) => y >= x.MinPosition - ExceedTolerance
											  && y <= x.MaxPosition + ExceedTolerance
											  && z >= x.MinPosition - ExceedTolerance
											  && z <= x.MaxPosition + ExceedTolerance).All(x => x);


		/// <summary>
		///     Checks whether a vector exceeds limits. When enumerated, the IVector should be in the same order
		///     as Configuration.EnabledAxes.
		/// </summary>
		/// <returns></returns>
		internal static bool VectorExceedsBounds(IVector first, Configuration conf)
			=> !conf.EnabledAxes.Zip(first.Components,
							 (x, y) => y >= x.MinPosition - ExceedTolerance
								   && y <= x.MaxPosition + ExceedTolerance).All(x => x);


		/// <summary>
		///     Tests whether an arc exceeds axis bounds, in a right-handed coordinate system.
		///     <c>centre</c>'s X and Y components map to the axes given by <c>planeX</c> and <c>planeY</c>, resp.
		/// </summary>
		/// <remarks>
		///     The start position of the circle is calculated as (cos theta, sin theta)
		/// </remarks>
		/// <param name="centre">centre of arc</param>
		/// <param name="r">radius of arc</param>
		/// <param name="startangle">arc starting angle</param>
		/// <param name="endangle">arc ending angle</param>
		/// <param name="cw">clockwise or not</param>
		/// <param name="planeX">the plane that maps to <c>centre</c>'s X field</param>
		/// <param name="planeY">the plane that maps to <c>centre</c>'s Y field</param>
		/// <returns></returns>
		internal static bool ArcExceedsBounds(Vector2 centre, double r, double startangle, double endangle, bool cw,
											  Configuration.Axis planeX, Configuration.Axis planeY)
		{
			var list = Limits(centre, r, startangle, endangle, cw).ToList();

			var maxx = list.Max(x => x.X);
			var minx = list.Min(x => x.X);
			var maxy = list.Max(x => x.Y);
			var miny = list.Min(x => x.Y);

			return !(maxx <= planeX.MaxPosition + ExceedTolerance
				 && maxy <= planeY.MaxPosition + ExceedTolerance
				 && minx >= planeX.MinPosition - ExceedTolerance
				 && miny >= planeY.MinPosition - ExceedTolerance);
		}


		/// <summary>
		///     Tests whether a circle exceeds axis bounds.
		/// </summary>
		/// <param name="centre"></param>
		/// <param name="r"></param>
		/// <param name="planeX"></param>
		/// <param name="planeY"></param>
		/// <returns></returns>
		internal static bool CircleExceedsBounds(Vector2 centre, double r, Configuration.Axis planeX,
												 Configuration.Axis planeY)
			=> !(centre.X <= (planeX.MaxPosition - r) + ExceedTolerance
			 && centre.X >= (planeX.MinPosition + r) - ExceedTolerance
			 && centre.Y <= (planeY.MaxPosition - r) + ExceedTolerance
			 && centre.Y >= (planeY.MinPosition + r) - ExceedTolerance);


		/// <summary>
		///     Finds the potential limits of an arc.
		/// </summary>
		/// <returns></returns>
		private static IEnumerable<Vector2> Limits(Vector2 centre, double r, double startangle, double endangle,
												   bool cw)
		{
			yield return (new Vector2(Math.Cos(startangle), Math.Sin(startangle)) * r) + centre;
			yield return (new Vector2(Math.Cos(endangle), Math.Sin(endangle)) * r) + centre;

			if (cw)
			{
				if (ClampAngle(startangle) < ClampAngle(endangle))
				{
					yield return new Vector2(r, 0) + centre;
				}

				if (ClampAngle(startangle - (Math.PI / 2)) < ClampAngle(endangle - (Math.PI / 2)))
				{
					yield return new Vector2(0, r) + centre;
				}

				if (ClampAngle(startangle - Math.PI) < ClampAngle(endangle - Math.PI))
				{
					yield return new Vector2(-r, 0) + centre;
				}

				if (ClampAngle(startangle - ((3 * Math.PI) / 2)) < ClampAngle(endangle - ((3 * Math.PI) / 2)))
				{
					yield return new Vector2(0, -r) + centre;
				}
			}
			else
			{
				if (ClampAngle(startangle) > ClampAngle(endangle))
				{
					yield return new Vector2(r, 0);
				}

				if (ClampAngle(startangle - (Math.PI / 2)) > ClampAngle(endangle - (Math.PI / 2)))
				{
					yield return new Vector2(0, r);
				}

				if (ClampAngle(startangle - Math.PI) > ClampAngle(endangle - Math.PI))
				{
					yield return new Vector2(-r, 0);
				}

				if (ClampAngle(startangle - ((3 * Math.PI) / 2)) > ClampAngle(endangle - ((3 * Math.PI) / 2)))
				{
					yield return new Vector2(0, -r);
				}
			}
		}


		/// <summary>
		///     Clamps an angle between 0 and 2pi.
		/// </summary>
		/// <returns></returns>
		private static double ClampAngle(double theta)
		{
			if (theta < 0)
			{
				while ((theta += Math.PI * 2) < 0)
				{
				}

				return theta;
			}

			if (theta > Math.PI * 2)
			{
				while ((theta -= Math.PI * 2) > Math.PI * 2)
				{
				}

				return theta;
			}

			return theta;
		}


		/// <summary>
		///     The tolerance that we have for exceeding bounds.
		/// </summary>
		private const double ExceedTolerance = 1e-5;
	}
}

﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Zaber.GCode.Tests")]

namespace Zaber.GCode.Geometry
{
	/// <summary>
	///     A vector with an arbitrary number of components.
	/// </summary>
	internal class Vector : List<double>, IVector
	{
		public IEnumerable<double> Components => this;

		public int Dimension => Count;
	}
}

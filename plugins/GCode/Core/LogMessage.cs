﻿using System;

namespace Zaber.GCode
{
	public class LogMessage
	{
		internal LogMessage(LogType type, string message, string block, int line, int start, int end)
		{
			Line = line;
			Type = type;
			Message = message;
			Block = block;
			Start = start;
			End = end;
		}


		public override string ToString() => string.Format("{1} at line {2}:{0}\t{3}:{0}{0}\t{4}{0}\t{5}{0}",
														   Environment.NewLine,
														   Type,
														   Line,
														   Message,
														   Block,
														   new string(' ', Start) + "^");


		/// <summary>
		///     Get the type of the message.
		/// </summary>
		public LogType Type { get; }

		/// <summary>
		///     Get the information contained in this message.
		/// </summary>
		public string Message { get; }

		/// <summary>
		///     Get the string form of the block that triggered this message.
		/// </summary>
		public string Block { get; }

		/// <summary>
		///     Get the starting column of text to which this message applies to.
		/// </summary>
		public int Start { get; }

		/// <summary>
		///     Get the starting column of text to which this message applies to.
		/// </summary>
		public int End { get; }

		/// <summary>
		///     The line number that this message applies to.
		/// </summary>
		public int Line { get; }
	}
}
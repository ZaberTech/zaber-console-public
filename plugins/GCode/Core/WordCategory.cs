﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Zaber.GCode
{
	/// <summary>
	///     A WordCategory groups commands based on their function.
	/// </summary>
	/// <remarks>
	///     A WordCategory is almost like a ModalGroup, except it looks at the word name as well:
	///     X10 and Y10 are the same ModalGroup (NotApplicable), but different WordCategories,
	///     while G0 and G4 are different ModalGroups and also different WordCategories,
	///     and G0 and G1 are the same ModalGroup and the same WordCategory.
	/// </remarks>
	internal class WordCategory
	{
		/// <summary>
		///     Sorts WordCategories based on the order of execution as defined in RS274.
		/// </summary>
		public class Sorter : IComparer<WordCategory>, IComparer<KeyValuePair<WordCategory, Word>>
		{
			public int Compare(KeyValuePair<WordCategory, Word> x, KeyValuePair<WordCategory, Word> y)
				=> Compare(x.Key, y.Key);


			public int Compare(WordCategory x, WordCategory y)
			{
				var xIndex = CategoryOrder.Count;
				var yIndex = xIndex;

				for (var i = 0; i < CategoryOrder.Count; i++)
				{
					if (CategoryOrder[i].Equals(x))
					{
						xIndex = i;
					}

					if (CategoryOrder[i].Equals(y))
					{
						yIndex = i;
					}
				}

				var val = xIndex - yIndex;
				if (val != 0)
				{
					return val;
				}

				if (x.Equals(y))
				{
					return 0;
				}

				return x._word.Command - y._word.Command;
			}


			private static readonly List<WordCategory> CategoryOrder = new List<WordCategory>
			{
				new WordCategory('G', ModalGroup.FeedRate),
				new WordCategory('F'),
				new WordCategory('S'),
				new WordCategory('T'),
				new WordCategory('M', ModalGroup.ToolChange),
				new WordCategory('M', ModalGroup.SpindleTurning),
				new WordCategory('M', ModalGroup.Coolant),
				new WordCategory('M', ModalGroup.OverrideSwitches),
				new WordCategory('G', ModalGroup.Dwell),
				new WordCategory('G', ModalGroup.PlaneSelection),
				new WordCategory('G', ModalGroup.Units),
				new WordCategory('G', ModalGroup.RadiusCompensation),
				new WordCategory('G', ModalGroup.LengthCompensation),
				new WordCategory('G', ModalGroup.CoordinateSystem),
				new WordCategory('G', ModalGroup.PathControl),
				new WordCategory('G', ModalGroup.Distance),
				new WordCategory('G', ModalGroup.ArcDistance),
				new WordCategory('G', ModalGroup.ReturnMode),
				new Word('G', 10).Category,
				new Word('G', 28).Category,
				new Word('G', 30).Category,
				new Word('G', 92).Category,
				new WordCategory('G', ModalGroup.Motion),
				new WordCategory('G', ModalGroup.Stopping)
			};
		}


		/// <summary>
		///     Constructs a WordCategory from a word character and a modal group. Group cannot be one of the
		///     none-types, since those words depend on the number to create a category.
		/// </summary>
		/// <remarks>
		///     It is required to specify the word character as well because NotApplicable ModalGroups are differentiated
		///     by their word character, not their ModalGroup.
		/// </remarks>
		/// <exception cref="ArgumentException">if group is one of the none-types</exception>
		public WordCategory(char word, ModalGroup group = ModalGroup.NotApplicable)
		{
			// The internal _word's number is only used in the case of None and NoneMotion modal groups.
			// This means that we can initialize it to MaxValue without breaking anything,
			// since NoneMotion and None ModalGroups cannot be passed to this constructor at all.
			_word = new Word(word, decimal.MaxValue);
			Group = group;

			if (WordDatabase.GroupedCommands.Contains(word))
			{
				if ((group == ModalGroup.NoneMotion) || (group == ModalGroup.None))
				{
					// We MUST know the Word's number in the case of NoneMotion and None.
					throw new ArgumentException("Internal exception: incomplete category");
				}
			}
			else if (group != ModalGroup.NotApplicable)
			{
				// Non-grouped commands (anything other than G or M) cannot be assigned to any group
				// except for NotApplicable.
				throw new ArgumentException("Internal exception: invalid category");
			}
		}


		/// <summary>
		///     Constructs a WordCategory from an existing Word.
		/// </summary>
		/// <param name="command"></param>
		public WordCategory(Word command)
		{
			// Unlike the above ctor, if `command` is valid, this constructor always succeeds.
			_word = command;
			Group = command.ModalGroup;
		}


		/// <summary>
		///     This function is essential to the correct handling of modal groups.
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		public override bool Equals(object other)
		{
			if (!(other is WordCategory))
			{
				return false;
			}

			var o = other as WordCategory;

			// null commands are never equal
			if ((_word.Command == Word.Null) || (o._word.Command == Word.Null))
			{
				return false;
			}

			// different commands are never equal
			if (o._word.Command != _word.Command)
			{
				return false;
			}

			// different group, can't possibly be same word category
			if (o.Group != Group)
			{
				return false;
			}

			// at this point, the words should be the same, meaning that equality
			// is determined by modal group;
			// if the command is not grouped, they are definitely the same category ("NotApplicable")
			if (WordDatabase.GroupedCommands.Contains(_word.Command))
			{
				if ((Group == ModalGroup.None) || (Group == ModalGroup.NoneMotion))
				{
					// if they are segregated, but modal group is none, each command is its own group
					return _word == o._word;
				}

				return true;
			}

			// same word, and not grouped, so definitely the same category
			return true;
		}


		public static bool operator ==(WordCategory first, WordCategory second) => first.Equals(second);


		public static bool operator !=(WordCategory first, WordCategory second) => !(first == second);


		public override int GetHashCode() => (31 * _word.Command) + (int) Group;


		public override string ToString()
		{
			if (WordDatabase.GroupedCommands.Contains(_word.Command))
			{
				if ((Group != ModalGroup.None) && (Group != ModalGroup.NoneMotion))
				{
					return $"{{{Group}}}";
				}

				return _word.ToString();
			}

			return _word.Command.ToString();
		}


		public char Command => _word.Command;

		public ModalGroup Group { get; }

		/// <summary>
		///     Get the default command for this word category.
		/// </summary>
		/// <param name="config"></param>
		/// <returns></returns>
		public Word DefaultCommand
		{
			get
			{
				if (WordDatabase.Defaults.ContainsKey(this))
				{
					return WordDatabase.Defaults[this];
				}

				return WordDatabase.Groups.First(x => new WordCategory(x.Key) == this).Key;
			}
		}

		/// <summary>
		///     The Word that this WordCategory was constructed with, if any.
		/// </summary>
		private readonly Word _word;
	}
}

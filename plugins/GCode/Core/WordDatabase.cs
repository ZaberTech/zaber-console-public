﻿using System.Collections.Generic;

namespace Zaber.GCode
{
	internal static class WordDatabase
	{
		/// <summary>
		///     If the word appears in this list, duplicate words are detected by comparing the modal group as well.
		///     Otherwise, a word is duplicate regardless of its modal group.
		/// </summary>
		public static readonly char[] GroupedCommands = { 'G', 'M' };

		/// <summary>
		///     A dictionary of the default settings for modal groups.
		/// </summary>
		public static readonly Dictionary<WordCategory, Word> Defaults = new Dictionary<WordCategory, Word>
		{
			{ new WordCategory('G', ModalGroup.Motion), new Word('G', 0M) },
			{ new WordCategory('G', ModalGroup.PlaneSelection), new Word('G', 17M) },
			{ new WordCategory('G', ModalGroup.Units), new Word('G', 21M) },
			{ new WordCategory('G', ModalGroup.Distance), new Word('G', 91M) },
			{ new WordCategory('G', ModalGroup.FeedRate), new Word('G', 94M) },
			{ new WordCategory('G', ModalGroup.ArcDistance), new Word('G', 91.1M) },
			{ new WordCategory('G', ModalGroup.PathControl), new Word('G', 61M) },
			{ new WordCategory('G', ModalGroup.CoordinateSystem), new Word('G', 54M) },
			{ new WordCategory('G', ModalGroup.RadiusCompensation), new Word('G', 40M) },
			{ new WordCategory('G', ModalGroup.LengthCompensation), new Word('G', 49M) }
		};

		/// <summary>
		///     A set for modal groups that are exempt from warnings when no mode is specified and the default is used.
		/// </summary>
		public static readonly HashSet<WordCategory> DefaultWarningExempt = new HashSet<WordCategory>
		{
			new WordCategory('G', ModalGroup.ArcDistance),
			new WordCategory('G', ModalGroup.RadiusCompensation),
			new WordCategory('G', ModalGroup.LengthCompensation),
			new WordCategory('G', ModalGroup.PathControl)
		};

		/// <summary>
		///     A dictionary of commands to modal groups.
		/// </summary>
		public static readonly Dictionary<Word, ModalGroup> Groups = new Dictionary<Word, ModalGroup>
		{
			{ new Word('G', 0M), ModalGroup.Motion },
			{ new Word('G', 1M), ModalGroup.Motion },
			{ new Word('G', 2M), ModalGroup.Motion },
			{ new Word('G', 3M), ModalGroup.Motion },
			{ new Word('G', 4M), ModalGroup.Dwell },
			{ new Word('G', 38.2M), ModalGroup.Motion },
			{ new Word('G', 80M), ModalGroup.Motion },
			{ new Word('G', 81M), ModalGroup.Motion },
			{ new Word('G', 82M), ModalGroup.Motion },
			{ new Word('G', 83M), ModalGroup.Motion },
			{ new Word('G', 84M), ModalGroup.Motion },
			{ new Word('G', 85M), ModalGroup.Motion },
			{ new Word('G', 86M), ModalGroup.Motion },
			{ new Word('G', 87M), ModalGroup.Motion },
			{ new Word('G', 88M), ModalGroup.Motion },
			{ new Word('G', 89M), ModalGroup.Motion },
			{ new Word('G', 17M), ModalGroup.PlaneSelection },
			{ new Word('G', 18M), ModalGroup.PlaneSelection },
			{ new Word('G', 19M), ModalGroup.PlaneSelection },
			{ new Word('G', 20M), ModalGroup.Units },
			{ new Word('G', 21M), ModalGroup.Units },
			{ new Word('G', 28M), ModalGroup.Units },
			{ new Word('G', 90M), ModalGroup.Distance },
			{ new Word('G', 91M), ModalGroup.Distance },
			{ new Word('G', 90.1M), ModalGroup.ArcDistance },
			{ new Word('G', 91.1M), ModalGroup.ArcDistance },
			{ new Word('G', 93M), ModalGroup.FeedRate },
			{ new Word('G', 94M), ModalGroup.FeedRate },
			{ new Word('G', 95M), ModalGroup.FeedRate },
			{ new Word('G', 40M), ModalGroup.RadiusCompensation },
			{ new Word('G', 41M), ModalGroup.RadiusCompensation },
			{ new Word('G', 42M), ModalGroup.RadiusCompensation },
			{ new Word('G', 43M), ModalGroup.LengthCompensation },
			{ new Word('G', 44M), ModalGroup.LengthCompensation },
			{ new Word('G', 49M), ModalGroup.LengthCompensation },
			{ new Word('G', 53M), ModalGroup.CoordinateSystem },
			{ new Word('G', 54M), ModalGroup.CoordinateSystem },
			{ new Word('G', 54.1M), ModalGroup.CoordinateSystem },
			{ new Word('G', 55M), ModalGroup.CoordinateSystem },
			{ new Word('G', 56M), ModalGroup.CoordinateSystem },
			{ new Word('G', 57M), ModalGroup.CoordinateSystem },
			{ new Word('G', 58M), ModalGroup.CoordinateSystem },
			{ new Word('G', 59M), ModalGroup.CoordinateSystem },
			{ new Word('G', 59.1M), ModalGroup.CoordinateSystem },
			{ new Word('G', 59.2M), ModalGroup.CoordinateSystem },
			{ new Word('G', 59.3M), ModalGroup.CoordinateSystem },
			{ new Word('G', 61M), ModalGroup.PathControl },
			{ new Word('G', 64M), ModalGroup.PathControl },
			{ new Word('G', 98M), ModalGroup.ReturnMode },
			{ new Word('G', 99M), ModalGroup.ReturnMode },
			{ new Word('M', 0M), ModalGroup.Stopping },
			{ new Word('M', 1M), ModalGroup.Stopping },
			{ new Word('M', 2M), ModalGroup.Stopping },
			{ new Word('M', 30M), ModalGroup.Stopping }
		};

		/// <summary>
		///     A dictionary of commands to names.
		/// </summary>
		public static readonly Dictionary<Word, string> Names = new Dictionary<Word, string>
		{
			{ new Word('G', 0M), "Linear Interpolated Traverse" },
			{ new Word('G', 1M), "Linear Interpolated Feed" },
			{ new Word('G', 2M), "Circular Clockwise Interpolated Feed" },
			{ new Word('G', 3M), "Circular Counterclockwise Interpolated Feed" },
			{ new Word('G', 4M), "Dwell" },
			{ new Word('G', 17M), "XY Plane Select" },
			{ new Word('G', 18M), "XZ Plane Select" },
			{ new Word('G', 19M), "YZ Plane Select" },
			{ new Word('G', 20M), "Inches Select" },
			{ new Word('G', 21M), "Millimeters Select" },
			{ new Word('G', 28M), "Return to Machine Zero" },
			{ new Word('G', 30M), "Return to Secondary Machine Zero" },
			{ new Word('G', 40M), "Tool Radius Compensation Cancel" },
			{ new Word('G', 41M), "Tool Radius Compensation Left" },
			{ new Word('G', 42M), "Tool Radius Compensation Right" },
			{ new Word('G', 43M), "Tool Height Compensation Positive" },
			{ new Word('G', 44M), "Tool Height Compensation Negative" },
			{ new Word('G', 49M), "Tool Height Compensation Cancel" },
			{ new Word('G', 53M), "Machine Coordinate System" },
			{ new Word('G', 54M), "Work Coordinate System 1 Select" },
			{ new Word('G', 55M), "Work Coordinate System 2 Select" },
			{ new Word('G', 56M), "Work Coordinate System 3 Select" },
			{ new Word('G', 57M), "Work Coordinate System 4 Select" },
			{ new Word('G', 58M), "Work Coordinate System 5 Select" },
			{ new Word('G', 59M), "Work Coordinate System 6 Select" },
			{ new Word('G', 59.1M), "Work Coordinate System 7 Select" },
			{ new Word('G', 59.2M), "Work Coordinate System 8 Select" },
			{ new Word('G', 59.3M), "Work Coordinate System 9 Select" },
			{ new Word('G', 54.1M), "Work Coordinate System 10+ Select" },
			{ new Word('G', 61M), "Exact Stop Select" },
			{ new Word('G', 64M), "Corner Rounding Select" },
			{ new Word('G', 90M), "Absolute Positionining Select" },
			{ new Word('G', 91M), "Incremental Positioning Select" },
			{ new Word('G', 90.1M), "Absolute Arc Positionining Select" },
			{ new Word('G', 91.1M), "Incremental Arc Positionining Select" },
			{ new Word('G', 93M), "Inverse Time Feedrate Select" },
			{ new Word('G', 94M), "Feedrate Per Minute Select" },
			{ new Word('G', 95M), "Feedrate Per Revolution Select" },
			{ new Word('M', 0M), "Compulsory Pause" },
			{ new Word('M', 1M), "Optional Pause" },
			{ new Word('M', 2M), "Program Stop" },
			{ new Word('M', 30M), "Program Stop" },
			{ new Word('M', 60M), "Compulsory Pause" }
		};
	}
}

﻿using System.Collections;
using System.Collections.Generic;

namespace Zaber.GCode.Commands
{
	internal class Raw : IEnumerable<ZaberCommand>
	{
		public Raw(string msg, Configuration config)
		{
			_cmd = new ZaberCommand(msg, config.DeviceAddress, 0);
		}


		public Raw(string msg, Configuration.Axis axis)
		{
			_cmd = new ZaberCommand(msg, axis.DeviceAddress, axis.AxisNumber);
		}


		public Raw(string msg, int dev, int axis)
		{
			_cmd = new ZaberCommand(msg, dev, axis);
		}


		public IEnumerator<ZaberCommand> GetEnumerator()
		{
			yield return _cmd;
		}


		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();


		private readonly ZaberCommand _cmd;
	}
}

﻿using System.Collections.Generic;
using System.Linq;

namespace Zaber.GCode.Commands
{
	internal static class Utils
	{
		public static string GetOnAxesClause(int streamAxisCount, IEnumerable<int> axesIndices)
		{
			// "on" clause not needed for streams with less than three axes (also serves FW6 compatibilty).
			if (streamAxisCount < 3)
			{
				return string.Empty;
			}

			return $"on {string.Join(" ", axesIndices.Select(axis => (char)('a' + axis)))} ";
		}
	}
}

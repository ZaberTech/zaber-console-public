﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Zaber.GCode.Commands
{
	/// <summary>
	///     Represents an arc command, with absolute coordinates.
	/// </summary>
	internal class CircleAt : IEnumerable<ZaberCommand>
	{
		public CircleAt(Configuration config, bool cw, IEnumerable<long> center, IEnumerable<int> axesIndices)
		{
			_cmd = new ZaberCommand(
				$"stream {config.StreamNumber} {Utils.GetOnAxesClause(config.EnabledAxes.Count(), axesIndices)}circle abs {(cw ? "cw" : "ccw")} {string.Join(" ", center.Select(x => x.ToString()).ToArray())}",
				config.DeviceAddress,
				0);
		}


		public IEnumerator<ZaberCommand> GetEnumerator()
		{
			yield return _cmd;
		}


		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();


		private readonly ZaberCommand _cmd;
	}
}

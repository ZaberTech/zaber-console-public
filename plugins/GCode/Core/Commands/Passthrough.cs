﻿using System.Collections;
using System.Collections.Generic;

namespace Zaber.GCode.Commands
{
	/// <summary>
	///     A class that represents partially translated command that is passed through as a non-command.
	/// </summary>
	internal class Passthrough : IEnumerable<ZaberCommand>
	{
		public Passthrough(Configuration aConfig, string aCommand)
		{
			_cmd = new ZaberCommand(aCommand, 0, 0) { Prefix = aConfig.PassthroughPrefix };
		}


		public IEnumerator<ZaberCommand> GetEnumerator()
		{
			yield return _cmd;
		}


		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();


		private readonly ZaberCommand _cmd;
	}
}

﻿using System.Collections;
using System.Collections.Generic;

namespace Zaber.GCode.Commands
{
	internal class StreamAction : IEnumerable<ZaberCommand>
	{
		public StreamAction(string msg, Configuration config)
		{
			_cmd = new ZaberCommand($"stream {config.StreamNumber} {msg}", config.DeviceAddress, 0);
		}


		public IEnumerator<ZaberCommand> GetEnumerator()
		{
			yield return _cmd;
		}


		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();


		private readonly ZaberCommand _cmd;
	}
}

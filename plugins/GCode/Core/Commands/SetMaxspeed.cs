﻿using System.Collections;
using System.Collections.Generic;

namespace Zaber.GCode.Commands
{
	internal class SetMaxspeed : IEnumerable<ZaberCommand>
	{
		public SetMaxspeed(long value, Configuration config)
		{
			_cmd = new ZaberCommand($"stream {config.StreamNumber} set maxspeed {value}",
									config.DeviceAddress,
									0);
		}


		public IEnumerator<ZaberCommand> GetEnumerator()
		{
			yield return _cmd;
		}


		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();


		private readonly ZaberCommand _cmd;
	}
}

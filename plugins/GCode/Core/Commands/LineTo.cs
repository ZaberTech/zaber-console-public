﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Zaber.GCode.Commands
{
	/// <summary>
	///     A class that represents a line to a position in absolute coordinates.
	/// </summary>
	internal class LineTo : IEnumerable<ZaberCommand>
	{
		public LineTo(Configuration config, IEnumerable<long> positions)
		{
			_cmd = new ZaberCommand(
				$"stream {config.StreamNumber} line abs {string.Join(" ", positions.Select(x => x.ToString()).ToArray())}",
				config.DeviceAddress,
				0);
		}


		public IEnumerator<ZaberCommand> GetEnumerator()
		{
			yield return _cmd;
		}


		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();


		private readonly ZaberCommand _cmd;
	}
}

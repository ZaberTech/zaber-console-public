﻿using System.Collections;
using System.Collections.Generic;
using Zaber.GCode.Commands;

namespace Zaber.GCode
{
	/// <summary>
	///     A list of ZaberCommands. This class is responsible for post-processing
	///     actions - such as corner rounding (not implemented yet),
	///     radius compensation (not implemented yet), line simplification (not implemented yet)
	/// </summary>
	internal class ZaberCommandList : ICollection<IEnumerable<ZaberCommand>>
	{
		public IEnumerable<IEnumerable<ZaberCommand>> Commands => _iList;


		/// <summary>
		///     Add a sequence of ZaberCommands to the list.
		/// </summary>
		/// <param name="?"></param>
		public void Add(IEnumerable<ZaberCommand> item)
		{
			if (_iList.Count > 1)
			{
				if (CheckDuplicateSet(item))
				{
					_iList.RemoveLast();
				}

				_iList.AddLast(item);
			}
			else
			{
				_iList.AddLast(item);
			}
		}


		private bool CheckDuplicateSet(IEnumerable<ZaberCommand> item)
		{
			if (item is SetMaxspeed && _iList.Last.Value is SetMaxspeed)
			{
				return true;
			}

			return false;
		}


		/// <summary>
		///     Internal list of actions.
		/// </summary>
		/// <remarks>
		///     The reason that each action is an IEnumerable is so that
		///     is so that "actions" are allowed to vary in their number of commands given.
		///     Elements will only be enumerated once translation finishes.
		/// </remarks>
		/// <remarks>
		///     For example line commands will be composed of multiple
		///     commands when not streaming (although not implemented yet).
		/// </remarks>
		private readonly LinkedList<IEnumerable<ZaberCommand>> _iList = new LinkedList<IEnumerable<ZaberCommand>>();

		#region ICollection<IEnumerable<ZaberCommand>>

		public IEnumerator<IEnumerable<ZaberCommand>> GetEnumerator() => _iList.GetEnumerator();


		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();


		public bool Remove(IEnumerable<ZaberCommand> element) => _iList.Remove(element);


		public bool IsReadOnly => ((ICollection<IEnumerable<ZaberCommand>>) _iList).IsReadOnly;

		public int Count => _iList.Count;


		public void CopyTo(IEnumerable<ZaberCommand>[] dest, int index) => _iList.CopyTo(dest, index);


		public bool Contains(IEnumerable<ZaberCommand> value) => _iList.Contains(value);


		public void Clear() => _iList.Clear();

		#endregion
	}
}

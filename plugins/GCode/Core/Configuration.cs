﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;

namespace Zaber.GCode
{
	public enum ConfigurationOutputType
	{
		StreamingCommands
	}

	public enum AxisEnabledType
	{
		/// <summary>
		///     This axis is disabled.
		/// </summary>
		/// <remarks>
		///     Any reference to this axis will result in an error.
		/// </remarks>
		Disabled,

		/// <summary>
		///     This axis is enabled for translation.
		/// </summary>
		Enabled,

		/// <summary>
		///     This axis is ignored.
		/// </summary>
		/// <remarks>
		///     References to this axis is ignored and will not result in an error.
		/// </remarks>
		Ignored,

		/// <summary>
		///     Moves on this axis will be scaled according to the user-entered microstep size
		///     and passed through as comment lines marked with a user-entered prefix.
		/// </summary>
		Passthrough
	}

	/// <summary>
	///     Contains translator configuration of a specific device.
	/// </summary>
	/// <remarks>
	///     Can be loaded from or saved to file with
	///     <see cref="Serialize(TextWriter, Configuration)" /> and
	///     <see cref="Deserialize(TextReader)" />.
	/// </remarks>
	[DataContract]
	public class Configuration
	{
		[DataContract]
		public class CoordTuple
		{
			public override string ToString() => $"{X}, {Y}, {Z}";


			[DataMember]
			public decimal X { get; set; }

			[DataMember]
			public decimal Y { get; set; }

			[DataMember]
			public decimal Z { get; set; }
		}

		/// <summary>
		///     Holds data about axis-specific configurations.
		/// </summary>
		[DataContract]
		public class Axis
		{
			/// <summary>
			///     Parameterless ctor for serialization.
			/// </summary>
			private Axis()
				: this('\0', '\0', '\0', '\0', null)
			{
			}


			internal Axis(char name, char rotationalName, char arcName, char incrementalName, Configuration config)
			{
				AxisNumber = (name - 'X') + 1;
				MinPosition = 0;
				MicrostepSize = 1.0M;
				EnabledType = AxisEnabledType.Disabled;

				_name = name;
				_rotationalName = rotationalName;
				_arcName = arcName;
				_incrementalName = incrementalName;
				Configuration = config;
			}


			internal int DeviceAddress => Configuration.DeviceAddress;

			/// <summary>
			///     The ID of the axis.
			/// </summary>
			[DataMember]
			public int AxisNumber { get; set; }

			/// <summary>
			///     The maximum position of the axis.
			/// </summary>
			[DataMember]
			public long MaxPosition { get; set; }

			/// <summary>
			///     The minimum position of the axis, and also
			///     the machine home.
			/// </summary>
			[DataMember]
			public long MinPosition { get; set; }

			/// <summary>
			///     Whether the axis is enabled.
			/// </summary>
			[DataMember]
			public AxisEnabledType EnabledType { get; set; }

			/// <summary>
			///     The microstep size in microns.
			/// </summary>
			[DataMember]
			public decimal MicrostepSize { get; set; }

			internal Configuration Configuration { get; set; }

			internal char Name => _name;

			internal char RotationalName => _rotationalName;

			internal char ArcName => _arcName;

			internal char IncrementalName => _incrementalName;


			[DataMember]
			private char _name;

			[DataMember]
			private char _rotationalName;

			[DataMember]
			private char _arcName;

			[DataMember]
			private char _incrementalName;
		}


		/// <summary>
		///     constructor for a bare Configuration.
		/// </summary>
		public Configuration()
		{
			Remappings = new Dictionary<decimal, string>();
			CoordinateSystems = new Dictionary<int, CoordTuple>
			{
				[0] = new CoordTuple
				{
					X = 0,
					Y = 0,
					Z = 0
				}
			};

			X = new Axis('X', 'A', 'I', 'U', this);
			Y = new Axis('Y', 'B', 'J', 'V', this);
			Z = new Axis('Z', 'C', 'K', 'W', this);

			DeviceAddress = 1;
			StreamNumber = 1;
			TraverseRate = 1;
			FeedRateOverride = 1;
			OptionalStop = false;
			OptionalDelete = false;
		}


		/// <summary>
		///     Generate a Configuration from a serialized TextReader
		/// </summary>
		public static Configuration Deserialize(TextReader reader)
		{
			if (reader == null)
			{
				throw new ArgumentNullException(nameof(reader));
			}

			var serializer = new DataContractSerializer(typeof(Configuration));

			var xreader = XmlReader.Create(reader);
			var config = (Configuration) serializer.ReadObject(xreader);
			xreader.Close();

			foreach (var x in config.AllAxes)
			{
				x.Configuration = config;
			}

			if (null == config.PassthroughPrefix)
			{
				config.PassthroughPrefix = "# ";
			}

			return config;
		}


		/// <summary>
		///     Serialize a Configuration into a TextWriter.
		/// </summary>
		public static void Serialize(TextWriter writer, Configuration config)
		{
			if (writer == null)
			{
				throw new ArgumentNullException(nameof(writer));
			}

			if (config == null)
			{
				throw new ArgumentNullException(nameof(config));
			}

			var serializer = new DataContractSerializer(typeof(Configuration));

			var xwriter = XmlWriter.Create(writer,
										   new XmlWriterSettings
										   {
											   Indent = true,
											   IndentChars = "\t"
										   });

			serializer.WriteObject(xwriter, config);

			xwriter.Close();
		}


		/// <summary>
		///     The output type of the Configuration.
		/// </summary>
		[DataMember]
		public ConfigurationOutputType OutputType { get; set; }

		/// <summary>
		///     The address of the device that will carry out streaming.
		/// </summary>
		[DataMember]
		public int DeviceAddress { get; set; }

		/// <summary>
		///     The Stream ID that will be used.
		/// </summary>
		[DataMember]
		public int StreamNumber { get; set; }

		/// <summary>
		///     Whether optional delete blocks are to be deleted.
		/// </summary>
		[DataMember]
		public bool OptionalDelete { get; set; }

		/// <summary>
		///     The X axis of the configuration.
		/// </summary>
		[DataMember]
		public Axis X { get; private set; }

		/// <summary>
		///     The Y axis of the configuration.
		/// </summary>
		[DataMember]
		public Axis Y { get; private set; }

		/// <summary>
		///     The Z axis of the configuration.
		/// </summary>
		[DataMember]
		public Axis Z { get; private set; }

		/// <summary>
		///     Whether optional stops (M01) should be honored. This field is currently unused.
		/// </summary>
		[DataMember]
		public bool OptionalStop { get; set; }

		/// <summary>
		///     The traversal rate, measured in 0.61035 usteps/sec intervals.
		/// </summary>
		[DataMember]
		public decimal TraverseRate { get; set; }

		/// <summary>
		///     The number to multiply all feed rates by; for example,
		///     F150 becomes F300 with a FeedRateOverride of 2.0M.
		/// </summary>
		[DataMember]
		public decimal FeedRateOverride { get; set; }

		/// <summary>
		///     The remappings from M-codes to custom commands.
		/// </summary>
		/// <remarks>
		///     For example, Remappings[1] refers to M1,
		///     and Remappings[5.4M] refers to M5.4.
		/// </remarks>
		[DataMember]
		public Dictionary<decimal, string> Remappings { get; private set; }

		/// <summary>
		///     The Work Coordinate Systems, in units defined by either G21 or G20.
		/// </summary>
		/// <remarks>
		///     CoordinateSystems[0] to CoordinateSystems[5] refer to G54 to G59;
		///     CoordinateSystems[6] to CoordinateSystems[8] refer to G59.1 to G59.3;
		///     CoordinateSystems[9] and onwards refer to G54.1 P(key - 8).
		/// </remarks>
		[DataMember]
		public Dictionary<int, CoordTuple> CoordinateSystems { get; private set; }


		/// <summary>
		///     Prefix prepended to lines in passthrough mode.
		/// </summary>
		[DataMember]
		public string PassthroughPrefix { get; set; }

		/// <summary>
		///     Returns all enabled axes.
		/// </summary>
		public IEnumerable<Axis> EnabledAxes => AllAxes.Where(x => AxisEnabledType.Enabled == x.EnabledType);

		/// <summary>
		///     Returns the X, Y, then Z axis.
		/// </summary>
		public IEnumerable<Axis> AllAxes
		{
			get
			{
				yield return X;
				yield return Y;
				yield return Z;
			}
		}


		/// <summary>
		///     Turns a feedrate in device units (mm|in) / min to a Zaber speed in microsteps/(some ungodly fraction of a second)
		///     This function returns the lowest out of the feedrates calculated per axis.
		/// </summary>
		/// <returns>a speed that can be sent to the device and will work properly</returns>
		internal double GetMicrostepFeedrate(Block block, State state, Translator trans, double num,
											 ZaberCommandList commands)
			=> EnabledAxes.Min(x => x.GetMicrostepFeedrate(block, state, trans, num, commands));


		/// <summary>
		///     Verify that deserialization resulted in a valid object.
		/// </summary>
		internal void VerifyDeserialization()
		{
			foreach (var axis in AllAxes)
			{
				if ((axis.ArcName == '\0')
				|| (axis.Name == '\0')
				|| (axis.RotationalName == '\0')
				|| (axis.IncrementalName == '\0'))
				{
					throw new InvalidDataContractException();
				}
			}
		}
	}
}

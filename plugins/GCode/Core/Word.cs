﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;
using Zaber.GCode.Commands;

namespace Zaber.GCode
{
	/// <summary>
	///     A class representing a Word, its data, and some debugging information as well.
	/// </summary>
	internal class Word
	{
		/// <summary>
		///     Constructor that allows specifying new column bounds.
		/// </summary>
		/// <param name="word"></param>
		/// <param name="start"></param>
		/// <param name="end"></param>
		public Word(Word word, int? start, int? end)
		{
			Command = word.Command;
			Number = word.Number;
			Start = start;
			End = end;
		}


		/// <summary>
		///     Parse a string into a command.
		/// </summary>
		public Word(string token, Translator log, int? start, int? end)
		{
			var part = new StringBuilder();
			Start = start;
			End = end;

			if (!char.IsLetter(token[0]))
			{
				log.Messages.Error($"Unexpected character {token[0]} found as GCode command", this);
			}

			Command = Null;

			try
			{
				// G-code appears to standardize on '.' as the decimal point so we 
				// parse numbers with the invariant culture.
				Number = decimal.Parse(token.Substring(1), CultureInfo.InvariantCulture);
			}
			catch (OverflowException)
			{
				log.Messages.Error($"Specified number for {token[0]} is too large or has too many decimal places",
								   this);
				return;
			}
			catch (FormatException aException)
			{
				log.Messages.Error(string.Format("Number format error (" + aException.Message + ")", token[0]), this);
				return;
			}

			Command = char.ToUpperInvariant(token[0]);
		}


		/// <summary>
		///     Create a Word from a command and a number.
		/// </summary>
		public Word(char word, decimal number)
		{
			Start = null;
			Command = word;
			Number = number;
		}


		/// <summary>
		///     Executes this command, adding the ASCII commands it generates to the ZaberCommandList provided.
		/// </summary>
		public void Execute(State state, Block block, Translator trans, ZaberCommandList commands)
		{
			if ((Command == Callbacks.RemappingCommand) && trans.Configuration.Remappings.ContainsKey(Number))
			{
				var r = trans.Configuration.Remappings[Number];
				foreach (var str in r.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()))
				{
					var split = str.Split(null as char[], 3, StringSplitOptions.RemoveEmptyEntries);
					int axis, device;

					if (split.Length < 1)
					{
						return;
					}

					if (split.Length == 1)
					{
						commands.Add(new Raw(split[0], 0, 0));
					}
					else if (split.Length == 2)
					{
						commands.Add(int.TryParse(split[0], out device)
										 ? new Raw(split[1], device, 0)
										 : new Raw(string.Join(" ", split), 0, 0));
					}
					else if (split.Length == 3)
					{
						if (int.TryParse(split[0], out device))
						{
							commands.Add(int.TryParse(split[1], out axis)
											 ? new Raw(split[2], device, axis)
											 : new Raw(split[1] + " " + split[2], device, 0));
						}
						else
						{
							commands.Add(new Raw(string.Join(" ", split), 0, 0));
						}
					}
				}
			}
			else if (Callbacks.CommandByWord.ContainsKey(this))
			{
				Callbacks.CommandByWord[this](state, block, trans, this, commands);
			}
			else if (Callbacks.CommandByLetter.ContainsKey(Command))
			{
				Callbacks.CommandByLetter[Command](state, block, trans, this, commands);
			}
			else
			{
				trans.Messages.Warn($"Command {this} has no functionality", this);
			}
		}


		public override bool Equals(object other)
		{
			if (other is Word)
			{
				var c = other as Word;

				// equality between decimal here works because of Decimal's base-10 representation of numbers
				return (c.Command == Command) && (c.Number == Number);
			}

			return false;
		}


		public override int GetHashCode() => Command.GetHashCode() ^ Number.GetHashCode();


		public static bool operator ==(Word x, Word y)
		{
			if (ReferenceEquals(x, null))
			{
				return ReferenceEquals(y, null);
			}

			if (ReferenceEquals(y, null))
			{
				return false;
			}

			return x.Equals(y);
		}


		public static bool operator !=(Word x, Word y) => !(x == y);


		public override string ToString() => $"{Command}{Number}{(WordDatabase.Names.ContainsKey(this) ? $" ({WordDatabase.Names[this]})" : "")}";


		/// <summary>
		///     The command character of this Word.
		/// </summary>
		public char Command { get; }

		/// <summary>
		///     The column at which this Word starts, not used in equality check.
		/// </summary>
		public int? Start { get; }

		/// <summary>
		///     The column at which this Word ends, not used in equality check
		/// </summary>
		public int? End { get; }

		/// <summary>
		///     The number that follows this Command
		/// </summary>
		public decimal Number { get; }

		public ModalGroup ModalGroup
		{
			get
			{
				if (WordDatabase.Groups.ContainsKey(this))
				{
					return WordDatabase.Groups[this];
				}

				if (WordDatabase.GroupedCommands.Contains(Command))
				{
					return ModalGroup.None;
				}

				return ModalGroup.NotApplicable;
			}
		}

		public WordCategory Category => new WordCategory(this);

		/// <summary>
		///     Null character for null words.
		/// </summary>
		public const char Null = '\0';
	}
}

﻿using System;
using System.Collections.Generic;

namespace Zaber.GCode
{
	internal static class Utilities
	{
		public static IEnumerable<TResult> Zip<TFirst, TSecond, TResult>(
			this IEnumerable<TFirst> first, IEnumerable<TSecond> second, Func<TFirst, TSecond, TResult> resultSelector)
		{
			if (first is null)
			{
				throw new ArgumentNullException(nameof(first));
			}

			if (second is null)
			{
				throw new ArgumentNullException(nameof(second));
			}

			if (resultSelector is null)
			{
				throw new ArgumentNullException(nameof(resultSelector));
			}

			return ZipIterator(first, second, resultSelector);
		}


		public static IEnumerable<TResult> Zip<TFirst, TSecond, TThird, TResult>(
			this IEnumerable<TFirst> first, IEnumerable<TSecond> second, IEnumerable<TThird> third,
			Func<TFirst, TSecond, TThird, TResult> resultSelector)
		{
			if (first is null)
			{
				throw new ArgumentNullException(nameof(first));
			}

			if (second is null)
			{
				throw new ArgumentNullException(nameof(second));
			}

			if (resultSelector is null)
			{
				throw new ArgumentNullException(nameof(resultSelector));
			}

			return ZipIterator(first, second, third, resultSelector);
		}


		private static IEnumerable<TResult> ZipIterator<TFirst, TSecond, TResult>(IEnumerable<TFirst> first,
																				  IEnumerable<TSecond> second,
																				  Func<TFirst, TSecond, TResult>
																					  resultSelector)
		{
			using (var e1 = first.GetEnumerator())
			{
				using (var e2 = second.GetEnumerator())
				{
					while (e1.MoveNext() && e2.MoveNext())
					{
						yield return resultSelector(e1.Current, e2.Current);
					}
				}
			}
		}


		private static IEnumerable<TResult> ZipIterator<TFirst, TSecond, TThird, TResult>(IEnumerable<TFirst> first,
																						  IEnumerable<TSecond> second,
																						  IEnumerable<TThird> third,
																						  Func<TFirst, TSecond, TThird,
																							  TResult> resultSelector)
		{
			using (var e1 = first.GetEnumerator())
			{
				using (var e2 = second.GetEnumerator())
				{
					using (var e3 = third.GetEnumerator())
					{
						while (e1.MoveNext() && e2.MoveNext() && e3.MoveNext())
						{
							yield return resultSelector(e1.Current, e2.Current, e3.Current);
						}
					}
				}
			}
		}
	}
}

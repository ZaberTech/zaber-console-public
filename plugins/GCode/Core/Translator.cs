﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Zaber.GCode.Tests")]

namespace Zaber.GCode
{
	/// <summary>
	///     The top level class for translating G-code into Zaber ASCII stream commands.
	/// </summary>
	public class Translator
	{
		/// <summary>
		///     Constructs a Translator from a Configuration and a TextReader to read G-code from.
		/// </summary>
		/// <remarks>
		///     The source G-code must be terminated with a new line
		/// </remarks>
		public Translator(TextReader source, Configuration config)
		{
			if ((source is null) || (config is null))
			{
				throw new ArgumentNullException();
			}

			_source = source;
			Configuration = config;
		}


		/// <summary>
		///     Translates the given code into a list of Zaber commands. Returns null
		///     if there were errors.
		/// </summary>
		public List<ZaberCommand> Translate()
		{
			var f = false;
			return Translate(ref f);
		}


		/// <summary>
		///     Translates the given code into a list of Zaber commands. Returns null
		///     if there were errors.
		/// </summary>
		/// <param name="cancel">cancels when set to true</param>
		public List<ZaberCommand> Translate(ref bool cancel)
		{
			_output = new ZaberCommandList();
			var state = new State();

			Messages.Clear();
			state.InitState(Configuration);

			if (Configuration.EnabledAxes.Count() < 1)
			{
				Messages.Error("All axes are disabled", 0, _source.ReadToEnd().Length);
				return null;
			}

			if (!Validate())
			{
				return null;
			}

			for (string line; (line = _source.ReadLine()) != null; Messages.Line++)
			{
				if (cancel)
				{
					return null;
				}

				Messages.Block = line;

				// Ignore empty and % lines
				if (string.IsNullOrEmpty(line))
				{
					continue;
				}

				if (line.StartsWith("%"))
				{
					continue;
				}

				var toks = Lexer.Tokenize(line, this);
				var block = new Block();
				foreach (var token in toks)
				{
					block.AddWord(token, this);
				}

				block.Execute(state, this, _output);

				// Take care of M2 and M30
				if (state.Stopped)
				{
					break;
				}
			}

			if (!Messages.Succeeded)
			{
				return null;
			}

			// Enumerate commandlist only at end of function, as changes may have been made after they were added
			return new List<ZaberCommand>(_output.SelectMany(x => x));
		}


		public Logger Messages { get; } = new Logger();

		public Configuration Configuration { get; }


		private bool Validate()
		{
			foreach (var axis in Configuration.EnabledAxes)
			{
				if (axis.MinPosition > axis.MaxPosition)
				{
					Messages.Error($"Minimum position is greater than maximum position for axis {axis.Name}",
								   0,
								   _source.ReadToEnd().Length);
					return false;
				}

				if (axis.MicrostepSize == 0)
				{
					Messages.Error($"Axis {axis.Name} cannot have a microstep size of 0",
								   0,
								   _source.ReadToEnd().Length);
					return false;
				}
			}

			if (Configuration.FeedRateOverride == 0)
			{
				Messages.Error("Feed rate cannot be overridden to 0",
							   0,
							   _source.ReadToEnd().Length);
				return false;
			}

			return true;
		}


		private readonly TextReader _source;
		private ZaberCommandList _output;
	}
}

﻿using System.Collections.Generic;
using System.Linq;

namespace Zaber.GCode
{
	/// <summary>
	///     Holds information about a line of commands.
	///     A callback would query parameters (such as X or Y) through the GetParameter function.
	/// </summary>
	internal class Block
	{
		/// <summary>
		///     The list of commands that trigger an implicit axis movement based on the current motion mode.
		/// </summary>
		public static readonly List<char> MovementTriggers = new List<char>
		{
			'X',
			'Y',
			'Z',
			'A',
			'B',
			'C',
			'I',
			'J',
			'K'
		};

		/// <summary>
		///     The list of commands that have no functionality on their own - these are used only
		///     as "peripherals" to other codes.
		/// </summary>
		public static readonly List<char> ParameterWords = new List<char>
		{
			'X',
			'Y',
			'Z',
			'A',
			'B',
			'C',
			'I',
			'H',
			'J',
			'K',
			'L',
			'O',
			'P',
			'Q',
			'R',
			'U',
			'V',
			'W',
			'X',
			'Y',
			'Z'
		};


		/// <summary>
		///     Add a word to the current block of code, and logs an error if the modal group is duplicate.
		/// </summary>
		/// <param name="command"></param>
		/// <param name="trans"></param>
		public void AddWord(Word command, Translator trans)
		{
			if (command.Command == Word.Null)
			{
				// don't care about null commands
				return;
			}

			var cmdcat = command.Category;
			if (_words.ContainsKey(cmdcat))
			{
				trans.Messages.Error($"Duplicate command word {command} in modal group {cmdcat}",
									 command);
				return;
			}

			_words[cmdcat] = command;
		}


		/// <summary>
		///     Whether the block contains a certain or not.
		/// </summary>
		/// <param name="param"></param>
		/// <returns></returns>
		public bool HasParameter(char param) => _words.ContainsKey(new WordCategory(param));


		/// <summary>
		///     Whether the block contains a certain word or not.
		/// </summary>
		/// <param name="word"></param>
		/// <returns></returns>
		public bool HasWord(Word word) => _words.ContainsValue(word);


		/// <summary>
		///     Whether the block contains a category for words.
		/// </summary>
		/// <param name="cat"></param>
		/// <returns></returns>
		public bool HasCategory(WordCategory cat) => _words.ContainsKey(cat);


		/// <summary>
		///     Gets the word associated with a category in the current block.
		/// </summary>
		/// <exception cref="KeyNotFoundException">if the category does not exist.</exception>
		public Word GetWordInCategory(WordCategory cat) => _words[cat];


		/// <summary>
		///     Returns a parameter word, and marks that parameter as "used".
		/// </summary>
		/// <param name="param"></param>
		/// <returns></returns>
		public Word GetParameter(char param)
		{
			_unusedParams.Remove(param);
			return _words[new WordCategory(param)];
		}


		/// <summary>
		///     Invoke the block of code, executing on the current state and adding commands to the list provided
		/// </summary>
		/// <param name="state"></param>
		/// <param name="trans"></param>
		public void Execute(State state, Translator trans, ZaberCommandList commands)
		{
			var mtncat = new WordCategory('G', ModalGroup.Motion);

			var nmtncats = new[] { new Word('G', 28).Category, new Word('G', 30).Category };

			if (_words.ContainsKey(mtncat) && nmtncats.Any(_words.ContainsKey))
			{
				// Cannot have both motion and non-modal motion in same block.
				trans.Messages.Error(
					$"Clash between command {_words[mtncat]} and {_words[nmtncats.First(_words.ContainsKey)]} in the same block",
									 _words[mtncat]);
			}

			// TODO: motion mode should not be triggered by commands that take axis parameters such as G92
			if (!_words.ContainsKey(mtncat) && !nmtncats.Any(_words.ContainsKey) && IsMovement)
			{
				// Motion mode is implicitly added if motion is triggered by [XYZABCIJK].
				var result = state.GetValueOrDefault(this, mtncat, trans, commands, false);
				_words.Add(mtncat, new Word(result, null, null));
			}

			var list = _words.ToList();
			list.Sort(new WordCategory.Sorter());

			// Execute words sequentially in their sorted order
			foreach (var i in list.Select(x => x.Value))
			{
				// it's okay to check for unused words here because they should have been sorted to the end
				if (ParameterWords.Contains(i.Command))
				{
					if (_unusedParams.Contains(i.Command))
					{
						trans.Messages.Warn($"Unused parameter word {i}", i);
					}
				}
				else
				{
					i.Execute(state, this, trans, commands);
				}
			}
		}


		public override string ToString() => string.Join(" ", _words.Values.Select(x => x.ToString()).ToArray());


		/// <summary>
		///     Whether this block causes a movement.
		/// </summary>
		public bool IsMovement => _words.Keys.Select(x => x.Command).Count(MovementTriggers.Contains) > 0;

		/// <summary>
		///     A dictionary mapping WordCategories to commands; this is used to detect dupliate words on the same line, and also
		///     to execute words in the correct order.
		/// </summary>
		private readonly Dictionary<WordCategory, Word> _words = new Dictionary<WordCategory, Word>();

		/// <summary>
		///     A hash set of unused parameters so that a warning can be raised if they are not used.
		/// </summary>
		private readonly HashSet<char> _unusedParams = new HashSet<char>(ParameterWords);
	}
}

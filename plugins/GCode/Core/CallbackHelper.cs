﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Zaber.GCode.Commands;
using Zaber.GCode.Geometry;

[assembly: InternalsVisibleTo("Zaber.GCode.Tests")]

namespace Zaber.GCode
{
	internal static class CallbackHelper
	{
		/// <summary>
		///     Return a command that moves at full speed to a point.
		/// </summary>
		/// <param name="state"></param>
		/// <param name="block"></param>
		/// <param name="translator"></param>
		/// <returns></returns>
		public static void G00(State state, Block block, Translator translator, Word word, ZaberCommandList commands)
		{
			ChangeState(state, word);
			if (state.Maxspeed != (double) translator.Configuration.TraverseRate)
			{
				state.Maxspeed = (double) translator.Configuration.TraverseRate;
				commands.Add(new SetMaxspeed((long) translator.Configuration.TraverseRate, translator.Configuration));
			}

			InterpolateLine(state, block, translator, word, commands);
		}


		/// <summary>
		///     Return a command that moves at feed speed to a point.
		/// </summary>
		/// <param name="state"></param>
		/// <param name="block"></param>
		/// <param name="translator"></param>
		/// <returns></returns>
		public static void G01(State state, Block block, Translator translator, Word word, ZaberCommandList commands)
		{
			var feedrate = state.GetValueOrError(block, new WordCategory('F'), translator, Resources.FRUD);
			if (feedrate == null)
			{
				return;
			}

			ChangeState(state, word);
			if (state.Maxspeed != (double) feedrate.Number)
			{
				state.Maxspeed = (double) feedrate.Number;
				commands.Add(new SetMaxspeed((long) feedrate.Number, translator.Configuration));
			}

			InterpolateLine(state, block, translator, word, commands);
		}


		public static void G02(State state, Block block, Translator translator, Word word, ZaberCommandList commands)
		{
			var feedrate = state.GetValueOrError(block, new WordCategory('F'), translator, Resources.FRUD);
			if (feedrate == null)
			{
				return;
			}

			ChangeState(state, word);
			if (state.Maxspeed != (double) feedrate.Number)
			{
				state.Maxspeed = (double) feedrate.Number;
				commands.Add(new SetMaxspeed((long) feedrate.Number, translator.Configuration));
			}

			InterpolateArc(state, block, translator, word, true, commands);
		}


		public static void G03(State state, Block block, Translator translator, Word word, ZaberCommandList commands)
		{
			var feedrate = state.GetValueOrError(block, new WordCategory('F'), translator, Resources.FRUD);
			if (feedrate == null)
			{
				return;
			}

			ChangeState(state, word);
			if (state.Maxspeed != (double) feedrate.Number)
			{
				state.Maxspeed = (double) feedrate.Number;
				commands.Add(new SetMaxspeed((long) feedrate.Number, translator.Configuration));
			}

			InterpolateArc(state, block, translator, word, false, commands);
		}


		public static void G04(State state, Block block, Translator translator, Word word, ZaberCommandList commands)
		{
			if (!block.HasParameter('P'))
			{
				translator.Messages.Error(Resources.DWLNOP);
			}

			var p = block.GetParameter('P');
			var number = (double) p.Number;
			if (number < 0)
			{
				translator.Messages.Error(Resources.DWLNEP, p);
			}

			if (number < 0.001)
			{
				translator.Messages.Warn(Resources.DWLSMP, p);
			}

			commands.Add(new StreamAction("wait " + (int) Math.Round(number * 1000), translator.Configuration));
		}


		public static void F(State state, Block block, Translator translator, Word word, ZaberCommandList commands)
		{
			var mtncat = new WordCategory('G', ModalGroup.Motion);
			var number = (double) word.Number;

			if (number < 0)
			{
				translator.Messages.Error(Resources.FRZP, word);
				return;
			}

			if (number == 0)
			{
				translator.Messages.Warn(Resources.FRZP, word);
				return;
			}

			// If feedrate is changed AND the current motion category is not G0, change the maxspeed.
			if (ChangeState(state,
							new Word('F',
									 (int) Math.Round(translator
												  .Configuration.GetMicrostepFeedrate(block,
																					  state,
																					  translator,
																					  number,
																					  commands))))
			&& state.HasValue(mtncat)
			&& (state.GetValueOrDie(mtncat).Number != 0M))
			{
				commands.Add(new SetMaxspeed((long) state
												.GetValueOrDefault(block,
																   new WordCategory('F'),
																   translator,
																   commands,
																   true)
												.Number,
											 translator.Configuration));
			}
		}


		public static void G28(State state, Block block, Translator translator, Word word, ZaberCommandList commands)
		{
			if (state.Maxspeed != (double) translator.Configuration.TraverseRate)
			{
				state.Maxspeed = (double) translator.Configuration.TraverseRate;
				commands.Add(new SetMaxspeed((long) translator.Configuration.TraverseRate, translator.Configuration));
			}

			var passthrough = false;
			var ptText = new StringBuilder();
			ptText.AppendFormat("{0}{1}", word.Command, word.Number);
			var filter = new Func<Configuration.Axis, bool>(
				x => (AxisEnabledType.Enabled == x.EnabledType) || (AxisEnabledType.Passthrough == x.EnabledType));

			if (translator.Configuration.AllAxes.Where(filter).Any(x => block.HasParameter(x.Name)))
			{
				var mid = new Vector();
				var fin = new Vector();
				foreach (var axis in translator.Configuration.AllAxes.Where(filter))
				{
					if (block.HasParameter(axis.Name))
					{
						var pos = axis.GetMicrostepPosition(block,
															state,
															translator,
															(double) block.GetParameter(axis.Name).Number,
															commands);

						if (double.IsNaN(pos))
						{
							// no reference position
							return;
						}

						if (AxisEnabledType.Passthrough == axis.EnabledType)
						{
							passthrough = true;
							ptText.AppendFormat(" {0}{1}", axis.Name, (long) Math.Round(pos));
						}
						else
						{
							mid.Add(pos);
							fin.Add(axis.MinPosition);
							state.Positions[axis] = axis.MinPosition;
						}
					}
					else if (AxisEnabledType.Enabled == axis.EnabledType)
					{
						if (state.Positions.ContainsKey(axis))
						{
							mid.Add(state.Positions[axis]);
							fin.Add(state.Positions[axis]);
						}
						else
						{
							translator.Messages.Error(Resources.HREFP);
							return;
						}
					}
				}

				if (Maths.VectorExceedsBounds(mid, translator.Configuration))
				{
					translator.Messages.Warn(Resources.LEAB, word);
				}

				if (mid.Count > 0)
				{
					commands.Add(new LineTo(translator.Configuration, mid.Select(x => (long) x)));

					// final position is guaranteed to be inside bounds because it is defined as the lower bound
					commands.Add(new LineTo(translator.Configuration, fin.Select(x => (long) x)));
				}

				if (passthrough)
				{
					commands.Add(new Passthrough(translator.Configuration, ptText.ToString()));
				}
			}
			else
			{
				commands.Add(new LineTo(translator.Configuration,
										translator.Configuration.EnabledAxes.Select(x => x.MinPosition)));
				foreach (var axis in translator.Configuration.AllAxes.Where(filter))
				{
					state.Positions[axis] = axis.MinPosition;
				}
			}
		}


		public static void G54_1(State state, Block block, Translator translator, Word word, ZaberCommandList commands)
		{
			ChangeStateAndTriggerIfChanged(null)(state, block, translator, word, commands);

			if (block.HasParameter('P'))
			{
				translator.Messages.Error(Resources.G54_1NOP, word);
			}
			else
			{
				state.Metadata[word] = block.GetParameter('P');
			}
		}


		public static void WCS(State state, Block block, Translator translator, Word word, ZaberCommandList commands)
		{
			ChangeStateAndTriggerIfChanged(null)(state, block, translator, word, commands);
			if (!translator.Configuration.CoordinateSystems.ContainsKey((int) word.Number - 54))
			{
				translator.Messages.Error(string.Format(Resources.WCSNOF, word), word);
			}
		}


		public static void Pauses(State state, Block block, Translator translator, Word word, ZaberCommandList commands)
			=> translator.Messages.Warn(Resources.WPNS, word);


		public static void Stop(State state, Block block, Translator translator, Word word, ZaberCommandList commands)
			=> state.Stopped = true;


		/// <summary>
		///     Returns an action that sets the passed command as the active state for its modal group and adds the state change
		///     to the list of commands if necessary.
		/// </summary>
		/// <param name="command"></param>
		/// <param name="cmd"> the command to add to the list of commands. If this is null, the command is not added.</param>
		/// <returns></returns>
		public static Callbacks.Callback ChangeStateAndTriggerIfChanged(IEnumerable<ZaberCommand> cmd)
			=> (state, block, trans, word, list) =>
			{
				// if the state does not have the value, the condition short circuits
				if (!state.HasValue(word.Category) || (state.GetValueOrDie(word.Category) != word))
				{
					state.SetValue(word);

					if (cmd != null)
					{
						list.Add(cmd);
					}
				}
			};


		/// <summary>
		///     Do nothing.
		/// </summary>
		/// <returns></returns>
		public static void Nop(State state, Block block, Translator translator, Word word, ZaberCommandList commands)
		{
		}


		/// <summary>
		///     Returns an action that sets the passed command as the active state for its modal group and raises a warning.
		/// </summary>
		/// <param name="command"></param>
		/// <param name="warn">the warning to raise</param>
		/// <returns></returns>
		public static Callbacks.Callback ChangeStateAndWarnIfChanged(string warn)
			=> (state, block, trans, word, list) =>
			{
				// if the state does not have the value, the condition short circuits
				if (!state.HasValue(word.Category) || (state.GetValueOrDie(word.Category) != word))
				{
					state.SetValue(word);

					trans.Messages.Warn(warn, word);
				}
			};


		/// <summary>
		///     Returns an action that does nothing but add a warning.
		/// </summary>
		/// <param name="command"></param>
		/// <param name="warn">the warning to raise</param>
		/// <returns></returns>
		public static Callbacks.Callback WarnOnly(string warn) => (state, block, trans, word, list) =>
		{
			trans.Messages.Warn(warn, word);
		};


		/// <summary>
		///     Returns an action that sets the passed command as the active state for its modal group and raises an error.
		/// </summary>
		/// <param name="warn">the warning to raise</param>
		/// <returns></returns>
		public static Callbacks.Callback ChangeStateAndErrorIfChanged(string err)
			=> (state, block, trans, word, list) =>
			{
				// if the state does not have the value, the condition short circuits so this is fine
				if (!state.HasValue(word.Category) || (state.GetValueOrDie(word.Category) != word))
				{
					state.SetValue(word);

					trans.Messages.Error(err, word);
				}
			};


		/// <summary>
		///     Returns an action that does nothing but raise an error.
		/// </summary>
		/// <returns></returns>
		public static Callbacks.Callback ErrorOnly(string err) => (state, block, trans, word, list) =>
		{
			trans.Messages.Error(err, word);
		};


		/// <summary>
		///     Calculates the absolute microstep position of a given coordinate.
		///     This function takes into account Distance Type, and Units, and Coordinate System..
		/// </summary>
		/// <returns></returns>
		internal static double GetMicrostepPosition(this Configuration.Axis axis, Block block, State state,
													Translator trans, double num, ZaberCommandList commands)
		{
			var distType =
				state.GetValueOrDefault(block, new WordCategory('G', ModalGroup.Distance), trans, commands, true);
			var isRelative = distType.Number == 91M;

			if (isRelative && !state.Positions.ContainsKey(axis))
			{
				trans.Messages.Error(Resources.NRP);
				return 0;
			}

			var scaledDist = axis.GetScaledDistance(block, state, trans, num, commands);

			// Coordinate system offset only applies in absolute mode.
			return isRelative
				? state.Positions[axis] + scaledDist
				: axis.GetMicrostepOffset(block, state, trans, commands) + scaledDist;
		}


		/// <summary>
		///     Calculates the offset applied by coordinate system.
		/// </summary>
		internal static double GetMicrostepOffset(this Configuration.Axis axis, Block block, State state,
												  Translator trans, ZaberCommandList commands)
		{
			var offset = 0.0;
			var distType =
				state.GetValueOrDefault(block, new WordCategory('G', ModalGroup.Distance), trans, commands, true);
			var isRelative = distType.Number == 91M;

			// G53 will cancel coordinate systems
			if (!block.HasWord(new Word('G', 53M)))
			{
				Configuration.CoordTuple val = null;
				if (block.HasWord(new Word('G', 54.1M)))
				{
					if (!state.Metadata.TryGetValue(new Word('G', 54.1M), out var pdata))
					{
						return double.NaN;
					}

					if (!trans.Configuration.CoordinateSystems.TryGetValue((int) (pdata as Word).Number + 8, out val))
					{
						return double.NaN;
					}
				}
				else
				{
					var w = state.GetValueOrDefault(block,
													new WordCategory('G', ModalGroup.CoordinateSystem),
													trans,
													commands,
													true);
					if (w.Number <= 59M)
					{
						if (!trans.Configuration.CoordinateSystems.TryGetValue((int) w.Number - 54, out val))
						{
							return double.NaN;
						}
					}
					else if (!trans.Configuration.CoordinateSystems.TryGetValue(((int) (w.Number * 10) - 591) + 6,
																				out val))
					{
						return double.NaN;
					}
				}

				if (val != null)
				{
					decimal off = 0;
					switch (axis.Name)
					{
						case 'X':
							off = val.X;
							break;
						case 'Y':
							off = val.Y;
							break;
						case 'Z':
							off = val.Z;
							break;
					}

					offset = axis.GetScaledDistance(block, state, trans, (double) off, commands);
				}
			}
			else if (isRelative)
			{
				trans.Messages.Error(Resources.G53ABS);
				return double.NaN;
			}

			return offset;
		}


		/// <summary>
		///     Turns a value in device units to a value in microsteps.
		/// </summary>
		/// <returns></returns>
		internal static double GetScaledDistance(this Configuration.Axis axis, Block block, State state,
												 Translator trans, double num, ZaberCommandList commands)
		{
			var unitType =
				state.GetValueOrDefault(block, new WordCategory('G', ModalGroup.Units), trans, commands, true);

			// since given microstep size is in um
			double scaleFactor = unitType.Number == 20 ? 25400 : 1000;

			return (num * scaleFactor) / (double) axis.MicrostepSize;
		}


		/// <summary>
		///     Turns a feedrate in device units (mm|in) / min to a Zaber speed in microsteps/(some ungodly fraction of a second)
		/// </summary>
		/// <returns>a speed that can be sent to the device and will work properly</returns>
		internal static double GetMicrostepFeedrate(this Configuration.Axis axis, Block block, State state,
													Translator trans, double num, ZaberCommandList commands)
			=> (axis.GetScaledDistance(block, state, trans, num, commands) / 60D)
		   * 1.6384D
		   * (double) trans.Configuration.FeedRateOverride;


		/// <summary>
		///     Add an arc to the command list, called by G02 or G03.
		/// </summary>
		/// <param name="state"></param>
		/// <param name="block"></param>
		/// <param name="translator"></param>
		/// <param name="cw">Clockwise or not.</param>
		/// <param name="commands"></param>
		/// <returns>whether an arc command was added to command list</returns>
		private static bool InterpolateArc(State state, Block block, Translator translator, Word word, bool cw,
										   ZaberCommandList commands)
		{
			CheckAxisAvailability(block, translator);

			var planeType = state.GetValueOrDefault(block,
													new WordCategory('G', ModalGroup.PlaneSelection),
													translator,
													commands,
													true);
			Configuration.Axis plane;
			int[] planarAxesIndices;

			if (planeType.Number == 17)
			{
				plane = translator.Configuration.Z;
				planarAxesIndices = PLANAR_AXES_Z;
			}
			else if (planeType.Number == 18)
			{
				plane = translator.Configuration.Y;
				planarAxesIndices = PLANAR_AXES_Y;
			}
			else if (planeType.Number == 19)
			{
				plane = translator.Configuration.X;
				planarAxesIndices = PLANAR_AXES_X;
			}
			else
			{
				translator.Messages.Error("Internal error, please report this bug. ImpossiblePlaneSelection", word);
				return false;
			}

			if (block.HasParameter(plane.Name))
			{
				var param = block.GetParameter(plane.Name);
				translator.Messages.Warn(string.Format(Resources.CERPHMN, param), param);
				state.Positions[plane] =
					plane.GetMicrostepPosition(block,
											   state,
											   translator,
											   (double) block.GetParameter(plane.Name).Number,
											   commands);
			}

			foreach (var axis in translator.Configuration.AllAxes.Where(
				x => (x != plane) && (AxisEnabledType.Enabled != x.EnabledType)))
			{
				translator.Messages.Error(string.Format(Resources.CERPN_,
														planeType,
														axis.Name),
										  word);
				return false;
			}

			// only look at axes which are in the movement plane
			if (!translator.Configuration.EnabledAxes.Where(x => x != plane).Any(x => block.HasParameter(x.Name)))
			{
				translator.Messages.Warn(Resources.CERPN, word);
				return false;
			}

			var planarAxes = translator.Configuration.EnabledAxes.Where(x => x != plane).ToArray();
			if (Math.Abs(planarAxes[0].MicrostepSize - planarAxes[1].MicrostepSize) > 0.01M)
			{
				translator.Messages.Warn(Resources.CERPNE, word);
				InterpolateLine(state, block, translator, word, commands);
				return false;
			}

			foreach (var axis in planarAxes)
			{
				if (!state.Positions.ContainsKey(axis))
				{
					translator.Messages.Error(Resources.CERPNREF);
					return false;
				}
			}

			// at this point, the setup has been fully checked so the two non-planar axes should both be enabled
			var loopIndex = 0;
			Vector2 current;
			Vector2 final;
			Vector2 centre;
			{
				var cur = new double[2];
				var fin = new double[2];

				foreach (var axis in planarAxes)
				{
					cur[loopIndex] = state.Positions[axis];

					if (block.HasParameter(axis.Name))
					{
						var pos = axis.GetMicrostepPosition(block,
															state,
															translator,
															(double) block.GetParameter(axis.Name).Number,
															commands);
						if (double.IsNaN(pos))
						{
							MarkAxesUsed(translator.Configuration.EnabledAxes, block);
							return false;
						}


						fin[loopIndex] = pos;
						state.Positions[axis] = pos;
					}
					else
					{
						fin[loopIndex] = state.Positions[axis];
					}

					loopIndex++;
				}

				current = new Vector2(cur[0], cur[1]);
				final = new Vector2(fin[0], fin[1]);
			}


			if (block.HasParameter('R'))
			{
				// radius format

				// let the final position be F, start be S, radius be R
				// then the final position is:
				// sqrt(R^2 - (F-S).lensq / 4) * (F-S).rotate90.normalize + (F+S)/2

				var r = translator.Configuration.EnabledAxes.Where(x => x != plane)
							   .First()
							   .GetScaledDistance(block,
												  state,
												  translator,
												  (double) block.GetParameter('R').Number,
												  commands);

				var finalRel = final - current;
				var finalRelLenSq = finalRel.LenSq;
				var finalRelLen = Math.Sqrt(finalRelLenSq);

				if (Math.Abs(r) < (finalRelLen / 2))
				{
					translator.Messages.Error(Resources.CERPRFORMIM, block.GetParameter('R'));
					return false;
				}

				var finalRelCentrePos = (final + current) / 2;

				var orthoDist = Math.Sqrt((r * r) - (finalRelLenSq / 4));

				Vector2 finalRelTransposeNormalized;

				if ((cw && (r > 0)) || (!cw && (r < 0)))
				{
					// the transpose changes depending on radius and clockwiseness
					finalRelTransposeNormalized = new Vector2(-finalRel.Y, finalRel.X) / finalRelLen;
				}
				else
				{
					finalRelTransposeNormalized = new Vector2(finalRel.Y, -finalRel.X) / finalRelLen;
				}

				centre = (finalRelTransposeNormalized * orthoDist) + finalRelCentrePos;

				// arcs travelling for between 165 - 195 degrees, or greater than 345 degrees are very bad
				var centreToFinal = final - centre;
				var centreT = current - centre;

				// a.b / |a||b|
				var cosTheta = Vector2.Dot(centreToFinal, centreT) / (centreToFinal.Len * centreT.Len);

				var theta = Math.Acos(cosTheta);

				if (theta > 2.88)
				{
					translator.Messages.Warn(Resources.CERPRFORMNS, block.GetParameter('R'));
				}
				else if ((theta < 0.26) && (r < 0)) // near-circles are only problems for negative R
				{
					translator.Messages.Warn(Resources.CERPRFORMNC, block.GetParameter('R'));
				}
			}
			else
			{
				// center format
				if (!planarAxes.Any(x => block.HasParameter(x.ArcName)))
				{
					translator.Messages.Error(Resources.CERPNFORM, word);
					return false;
				}

				var arcDist = state.GetValueOrDefault(block,
													  new WordCategory('G', ModalGroup.ArcDistance),
													  translator,
													  commands,
													  true);
				var arcDistRelative = arcDist.Number == 91.1M;

				loopIndex = 0;

				var pos = new double[2];
				foreach (var axis in translator.Configuration.EnabledAxes.Where(x => x != plane))
				{
					if (block.HasParameter(axis.ArcName))
					{
						var msParam = axis.GetScaledDistance(block,
															 state,
															 translator,
															 (double) block.GetParameter(axis.ArcName).Number,
															 commands);
						if (arcDistRelative)
						{
							pos[loopIndex] = current.Components.ElementAt(loopIndex) + msParam;
						}
						else
						{
							pos[loopIndex] = msParam;
						}
					}
					else
					{
						if (arcDistRelative)
						{
							pos[loopIndex] = state.Positions[axis];
						}
						else
						{
							translator.Messages.Error(string.Format(Resources.CERPCFORMABSE, axis.ArcName, arcDist));
							return false;
						}
					}

					loopIndex++;
				}

				centre = new Vector2(pos[0], pos[1]);

				// if the positioning is correct, the projection of the centre onto the line between the destination and
				// the original position lies exactly in the middle of that line
				var centresRel = centre - current;
				var targetRel = final - current;

				// which means that the scalar part is 0.5
				var scalarPart = Vector2.Dot(centresRel, targetRel) / targetRel.LenSq;

				if (double.IsNaN(scalarPart) || double.IsInfinity(scalarPart)
					) // this means that centre and end points are identical
				{
					if (Maths.CircleExceedsBounds(centre, (centre - final).Len, planarAxes[0], planarAxes[1]))
					{
						translator.Messages.Warn(Resources.CEAB, word);
					}

					// is not arc, is circle
					commands.Add(new CircleAt(translator.Configuration,
											  cw,
											  centre.Components.Select(x => (long) Math.Round(x)),
											  planarAxesIndices));
					return true;
				}

				if (centresRel.LenSq > (targetRel.LenSq * 10000))
				{
					translator.Messages.Warn(Resources.CERPCFORMMC);
				}

				// if it's in the centre, the ratio should be around 0.5
				if ((scalarPart < 0.495) || (scalarPart > 0.505))
				{
					translator.Messages.Warn(Resources.CERPCFORMNV, word);
				}
			}

			if (Maths.ArcExceedsBounds(centre,
									   (centre - final).Len,
									   (final - centre).Angle,
									   (current - centre).Angle,
									   cw,
									   planarAxes[0],
									   planarAxes[1]))
			{
				translator.Messages.Warn(Resources.AEAB, word);
			}

			commands.Add(new ArcTo(translator.Configuration,
								   cw,
								   centre.Components.Select(x => (long) Math.Round(x)),
								   final.Components.Select(x => (long) Math.Round(x)),
								   planarAxesIndices));
			return true;
		}


		/// <summary>
		///     Add a line to the command list, called by G00 or G01.
		/// </summary>
		/// <param name="state"></param>
		/// <param name="block"></param>
		/// <param name="translator"></param>
		/// <param name="commands"></param>
		/// <returns>whether the line command was added</returns>
		private static bool InterpolateLine(State state, Block block, Translator translator, Word word,
											ZaberCommandList commands)
		{
			CheckAxisAvailability(block, translator);

			if (!translator.Configuration.AllAxes.Where(x => AxisEnabledType.Disabled != x.EnabledType)
						.Any(x => block.HasParameter(x.Name)))
			{
				translator.Messages.Warn(Resources.LERPN, word);
				return false;
			}

			// This case handles both enabled and passthrough axes.
			var filter = new Func<Configuration.Axis, bool>(
				x => (AxisEnabledType.Enabled == x.EnabledType) || (AxisEnabledType.Passthrough == x.EnabledType));

			// all axes are ignored.
			if (!translator.Configuration.AllAxes.Where(filter).Any(x => block.HasParameter(x.Name)))
			{
				return false;
			}

			var determined = true;
			var passthrough = false;
			var moved = false;
			var ptText = new StringBuilder();
			ptText.AppendFormat("{0}{1}", word.Command, word.Number);

			var current = new Vector();
			var positions = new Vector();
			foreach (var axis in translator.Configuration.AllAxes.Where(filter))
			{
				//bitwise AND does not short circuit
				determined &= state.Positions.TryGetValue(axis, out var curr);

				current.Add(curr);

				if (block.HasParameter(axis.Name))
				{
					var pos = axis.GetMicrostepPosition(block,
														state,
														translator,
														(double) block.GetParameter(axis.Name).Number,
														commands);
					if (double.IsNaN(pos))
					{
						MarkAxesUsed(translator.Configuration.EnabledAxes, block);
						return false;
					}

					if (AxisEnabledType.Passthrough == axis.EnabledType)
					{
						passthrough = true;
						ptText.AppendFormat(" {0}{1}", axis.Name, (long) Math.Round(pos));
					}
					else
					{
						moved |= !state.Positions.ContainsKey(axis) || (state.Positions[axis] != pos);
						positions.Add(pos);
					}

					state.Positions[axis] = pos;
				}
				else if (AxisEnabledType.Enabled == axis.EnabledType)
				{
					if (state.Positions.ContainsKey(axis))
					{
						positions.Add(state.Positions[axis]);
					}
					else
					{
						translator.Messages.Error(Resources.LREFP, word);
						MarkAxesUsed(translator.Configuration.EnabledAxes, block);
						return false;
					}
				}
			}

			if (Maths.VectorExceedsBounds(positions, translator.Configuration))
			{
				translator.Messages.Warn(Resources.LEAB, word);
			}
			else if (determined && Maths.VectorExceedsBounds(current, translator.Configuration))
			{
				translator.Messages.Warn(Resources.LEAB, word);
			}

			if (moved && (positions.Count > 0))
			{
				commands.Add(new LineTo(translator.Configuration, positions.Select(x => (long) Math.Round(x))));
			}

			if (passthrough)
			{
				commands.Add(new Passthrough(translator.Configuration, ptText.ToString()));
			}

			return true;
		}


		/// <summary>
		///     Change the state to a command and return whether it was changed or not.
		/// </summary>
		/// <param name="state"></param>
		/// <param name="command"></param>
		/// <returns></returns>
		private static bool ChangeState(State state, Word command)
		{
			if (!state.HasValue(command.Category) || (state.GetValueOrDie(command.Category) != command))
			{
				state.SetValue(command);
				return true;
			}

			return false;
		}


		/// <summary>
		///     Check axes X, Y, and Z for proper initialization and error if they are not.
		///     Returns false if not successful.
		/// </summary>
		/// <param name="block"></param>
		/// <param name="trans"></param>
		/// <returns></returns>
		private static bool CheckAxisAvailability(Block block, Translator trans)
		{
			var success = true;
			foreach (var axis in trans.Configuration.AllAxes.Where(x => AxisEnabledType.Disabled == x.EnabledType))
			{
				if (block.HasParameter(axis.Name))
				{
					trans.Messages.Error(string.Format(Resources.DXU, axis.Name), block.GetParameter(axis.Name));
					success = false;
				}

				if (block.HasParameter(axis.IncrementalName))
				{
					trans.Messages.Error(string.Format(Resources.DXUI,
													   axis.Name,
													   block.GetParameter(axis.IncrementalName),
													   block.GetParameter(axis.IncrementalName)));
					success = false;
				}

				if (block.HasParameter(axis.RotationalName))
				{
					trans.Messages.Error(string.Format(Resources.DXUI,
													   axis.Name,
													   block.GetParameter(axis.RotationalName),
													   block.GetParameter(axis.RotationalName)));
					success = false;
				}

				if (block.HasParameter(axis.ArcName))
				{
					trans.Messages.Error(string.Format(Resources.DXUI,
													   axis.Name,
													   block.GetParameter(axis.ArcName),
													   block.GetParameter(axis.ArcName)));
					success = false;
				}
			}

			MarkAxesUsed(trans.Configuration.AllAxes.Where(x => AxisEnabledType.Ignored == x.EnabledType), block);
			MarkAxesUsed(trans.Configuration.AllAxes.Where(x => AxisEnabledType.Passthrough == x.EnabledType), block);

			return success;
		}


		/// <summary>
		///     Marks all axes as "used".
		/// </summary>
		/// <param name="?"></param>
		private static void MarkAxesUsed(IEnumerable<Configuration.Axis> aAxisList, Block aBlock)
		{
			// mark ignored axes as "used" to suppress warning
			foreach (var axis in aAxisList)
			{
				if (aBlock.HasParameter(axis.Name))
				{
					aBlock.GetParameter(axis.Name);
				}

				if (aBlock.HasParameter(axis.IncrementalName))
				{
					aBlock.GetParameter(axis.IncrementalName);
				}

				if (aBlock.HasParameter(axis.ArcName))
				{
					aBlock.GetParameter(axis.ArcName);
				}

				if (aBlock.HasParameter(axis.RotationalName))
				{
					aBlock.GetParameter(axis.RotationalName);
				}
			}
		}

		private static readonly int[] PLANAR_AXES_Z = { 0, 1 };
		private static readonly int[] PLANAR_AXES_Y = { 0, 2 };
		private static readonly int[] PLANAR_AXES_X = { 1, 2 };
	}
}

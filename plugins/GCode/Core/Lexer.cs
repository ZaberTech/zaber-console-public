﻿using System.Collections.Generic;
using System.Text;

namespace Zaber.GCode
{
	internal static class Lexer
	{
		/// <summary>
		///     Tokenize a block of GCode into a list of Words.
		/// </summary>
		/// <param name="block">The line to translate.</param>
		/// <param name="line"></param>
		/// <returns></returns>
		public static List<Word> Tokenize(string block, Translator log)
		{
			var tokens = new List<Word>();

			var token = new StringBuilder();
			var comment = false;
			var strstart = 0;

			for (var i = 0; i < block.Length; i++)
			{
				var c = block[i];

				if ((i == 0) && (c == '/'))
				{
					if (log.Configuration.OptionalDelete)
					{
						return new List<Word>();
					}

					continue;
				}

				// deal with comments, all characters in comments are ignored
				if (c == CommentBegin)
				{
					if (comment)
					{
						log.Messages.Error($"Unexpected character {block[i]} inside comment block",
										   i,
										   i + 1);
					}
					else
					{
						comment = true;
					}
				}
				else if (c == CommentEnd)
				{
					if (comment)
					{
						comment = false;
						continue;
					}

					log.Messages.Error($"Unexpected character {block[i]} outside comment block",
									   i,
									   i + 1);
				}

				// whitespace in G-Code is always ignored
				if (char.IsWhiteSpace(c) || comment)
				{
					continue;
				}

				if (c == ';')
				{
					break; //ignore all characters after a semicolon
				}

				// a letter signifies the start of a new word,
				// and the end of the previous word
				if (char.IsLetter(c))
				{
					if (token.Length > 0)
					{
						tokens.Add(new Word(token.ToString(), log, strstart, i));
						strstart = i;
						token.Length = 0;
					}
				}

				token.Append(c);
			}

			// put trailing characters in a new word too
			if (token.Length > 0)
			{
				tokens.Add(new Word(token.ToString(), log, strstart, block.Length));
			}

			return tokens;
		}


		private const char CommentBegin = '(';
		private const char CommentEnd = ')';
	}
}

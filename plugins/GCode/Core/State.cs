﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Zaber.GCode
{
	/// <summary>
	///     Keeps track of the internal state of the machine. All numbers that the state contains are assumed
	///     to be in units of the controller.
	/// </summary>
	internal class State
	{
		/// <summary>
		///     Get the value, or default value of this setting. If the default value is used,
		///     adds to the provided list commands that are necessary to set the default value.
		/// </summary>
		/// <returns></returns>
		public Word GetValueOrDefault(Block block, WordCategory i, Translator trans, ZaberCommandList commands,
									  bool executeCommand)
		{
			if ((i.Group == ModalGroup.NoneMotion) || (i.Group == ModalGroup.Dwell) || (i.Group == ModalGroup.None))
			{
				// cannot get the value of a non-modal command
				throw new InvalidOperationException();
			}

			if (!_modes.ContainsKey(i))
			{
				var cmd = i.DefaultCommand;

				if (!WordDatabase.DefaultWarningExempt.Contains(i))
				{
					trans.Messages.Warn(string.Format("Mode for group {0} was not defined; using {1} as default{2}",
													  i,
													  cmd,
													  block.HasCategory(i)

														  // if the block has the category but hasn't been accessed yet, this means that the sorting
														  // placed it at a lower priority according to the RS274 standard
														  ? $", did you mean to place {block.GetWordInCategory(i)} on the previous line?"
														  : ""));
				}

				_modes[i] = cmd;

				if (executeCommand)
				{
					_modes[i].Execute(this, block, trans, commands);
				}
			}

			return _modes[i];
		}


		/// <summary>
		///     Get the value of this setting, or log an error
		/// </summary>
		/// <returns></returns>
		public Word GetValueOrError(Block block, WordCategory i, Translator trans, string err)
		{
			if ((i.Group == ModalGroup.NoneMotion)
			|| (i.Group == ModalGroup.Dwell)
			|| (WordDatabase.GroupedCommands.Contains(i.Command) && (i.Group == ModalGroup.None)))
			{
				// cannot get the value of a non-modal command
				throw new InvalidOperationException();
			}

			if (!_modes.ContainsKey(i))
			{
				trans.Messages.Error($"Mode for group {i} was not defined; {err}");
				return null;
			}

			return _modes[i];
		}


		/// <summary>
		///     Get the value of this setting, or throw KeyNotFoundException
		/// </summary>
		/// <param name="i"></param>
		/// <returns></returns>
		public Word GetValueOrDie(WordCategory i) => _modes[i];


		/// <summary>
		///     Initialize the state.
		/// </summary>
		/// <param name="config"></param>
		public void InitState(Configuration config)
		{
		}


		/// <summary>
		///     Set the given word as the active word for its category.
		/// </summary>
		/// <param name="value"></param>
		public void SetValue(Word value)
		{
			if ((value.ModalGroup != ModalGroup.None) && (value.ModalGroup != ModalGroup.NoneMotion))
			{
				_modes[value.Category] = value;
			}
		}


		/// <summary>
		///     Whether the state has a value for this category at the current time;
		///     this will always be false during the execution of the first block in the program.
		/// </summary>
		/// <param name="category"></param>
		/// <returns></returns>
		public bool HasValue(WordCategory category) => _modes.ContainsKey(category);


		/// <summary>
		///     The position of each axis, in microsteps.
		/// </summary>
		public Dictionary<Configuration.Axis, double> Positions { get; } = new Dictionary<Configuration.Axis, double>();

		/// <summary>
		///     Metadata that Words can choose to use as part of their state.
		/// </summary>
		public Dictionary<Word, object> Metadata { get; } = new Dictionary<Word, object>();

		/// <summary>
		///     The current stream's maxspeed.
		/// </summary>
		public double Maxspeed { get; set; }

		/// <summary>
		///     Whether M2 or M30 has been called
		/// </summary>
		public bool Stopped { get; set; }

		private readonly Dictionary<WordCategory, Word> _modes = new Dictionary<WordCategory, Word>();
	}
}

﻿using System.Collections.Generic;

namespace Zaber.GCode
{
	internal class Callbacks
	{
		public delegate void Callback(State state, Block block, Translator translator, Word word,
									  ZaberCommandList list);

		/// <summary>
		///     Represents callback functions that are called by a particular command.
		///     The State is the current state. The Block is the current block. The Translator is the current translator.
		///     The Word is the word that is being executed. The ZaberCommandList is the commandlist to add comnmands to.
		/// </summary>
		public static readonly Dictionary<Word, Callback> CommandByWord = new Dictionary<Word, Callback>
		{
			{ new Word('G', 0M), CallbackHelper.G00 },
			{ new Word('G', 1M), CallbackHelper.G01 },
			{ new Word('G', 2M), CallbackHelper.G02 },
			{ new Word('G', 3M), CallbackHelper.G03 },
			{ new Word('G', 4M), CallbackHelper.G04 },
			{ new Word('G', 17M), CallbackHelper.ChangeStateAndTriggerIfChanged(null) },
			{ new Word('G', 18M), CallbackHelper.ChangeStateAndTriggerIfChanged(null) },
			{ new Word('G', 19M), CallbackHelper.ChangeStateAndTriggerIfChanged(null) },
			{ new Word('G', 20M), CallbackHelper.ChangeStateAndTriggerIfChanged(null) },
			{ new Word('G', 21M), CallbackHelper.ChangeStateAndTriggerIfChanged(null) },
			{ new Word('G', 28M), CallbackHelper.G28 },
			{ new Word('G', 40M), CallbackHelper.ChangeStateAndTriggerIfChanged(null) },
			{ new Word('G', 41M), CallbackHelper.WarnOnly(string.Format(Resources.NFRC, "G41")) },
			{ new Word('G', 42M), CallbackHelper.WarnOnly(string.Format(Resources.NFRC, "G42")) },
			{ new Word('G', 49M), CallbackHelper.ChangeStateAndTriggerIfChanged(null) },
			{ new Word('G', 43M), CallbackHelper.WarnOnly(string.Format(Resources.NFLC, "G43")) },
			{ new Word('G', 44M), CallbackHelper.WarnOnly(string.Format(Resources.NFLC, "G44")) },
			{ new Word('G', 53M), CallbackHelper.Nop },
			{ new Word('G', 54M), CallbackHelper.WCS },
			{ new Word('G', 54.1M), CallbackHelper.G54_1 },
			{ new Word('G', 55M), CallbackHelper.WCS },
			{ new Word('G', 56M), CallbackHelper.WCS },
			{ new Word('G', 57M), CallbackHelper.WCS },
			{ new Word('G', 58M), CallbackHelper.WCS },
			{ new Word('G', 59M), CallbackHelper.WCS },
			{ new Word('G', 59.1M), CallbackHelper.WCS },
			{ new Word('G', 59.2M), CallbackHelper.WCS },
			{ new Word('G', 59.3M), CallbackHelper.WCS },
			{ new Word('G', 61M), CallbackHelper.ChangeStateAndTriggerIfChanged(null) },
			{ new Word('G', 64M), CallbackHelper.WarnOnly(Resources.NFCR) },
			{ new Word('G', 90M), CallbackHelper.ChangeStateAndTriggerIfChanged(null) },
			{ new Word('G', 91M), CallbackHelper.ChangeStateAndTriggerIfChanged(null) },
			{ new Word('G', 93M), CallbackHelper.WarnOnly(Resources.NFIF) },
			{ new Word('G', 94M), CallbackHelper.ChangeStateAndTriggerIfChanged(null) },
			{ new Word('G', 95M), CallbackHelper.WarnOnly(Resources.NFUPR) },
			{ new Word('G', 98M), CallbackHelper.ChangeStateAndWarnIfChanged(Resources.NCC) },
			{ new Word('G', 99M), CallbackHelper.ChangeStateAndWarnIfChanged(Resources.NCC) },
			{ new Word('G', 90.1M), CallbackHelper.ChangeStateAndTriggerIfChanged(null) },
			{ new Word('G', 91.1M), CallbackHelper.ChangeStateAndTriggerIfChanged(null) },
			{ new Word('M', 0M), CallbackHelper.Pauses },
			{ new Word('M', 1M), CallbackHelper.Pauses },
			{ new Word('M', 60M), CallbackHelper.Pauses },
			{ new Word('M', 2M), CallbackHelper.Stop },
			{ new Word('M', 30M), CallbackHelper.Stop },
			{
				new Word('M', 3M), CallbackHelper.ChangeStateAndWarnIfChanged(string.Format(Resources.IEXTIO, "M3"))
			},
			{
				new Word('M', 4M), CallbackHelper.ChangeStateAndWarnIfChanged(string.Format(Resources.IEXTIO, "M4"))
			},
			{ new Word('M', 5M), CallbackHelper.ChangeStateAndWarnIfChanged(string.Format(Resources.IEXTIO, "M5")) }
		};

		public static readonly Dictionary<char, Callback> CommandByLetter =
			new Dictionary<char, Callback>
			{
				{ 'N', CallbackHelper.Nop },
				{ 'T', CallbackHelper.ChangeStateAndWarnIfChanged(Resources.NFTC) },
				{ 'S', CallbackHelper.ChangeStateAndWarnIfChanged(Resources.NFSS) },
				{ 'F', CallbackHelper.F }
			};

		public const char RemappingCommand = 'M';
	}
}

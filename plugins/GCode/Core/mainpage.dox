namespace Zaber.GCode {
/**
\mainpage %Zaber G-Code %Translator Library

\tableofcontents

<b>NOTE:</b> This library is deprecated and no longer supported. %Zaber's modern G-Code 
translation offering is part of the [Zaber Motion Library](https://software.zaber.com/motion-library/docs/guides/gcode).
There is also an interactive G-Code application within [Zaber Launcher](https://software.zaber.com/zaber-launcher/download).

The %Zaber G-Code %Translator Library is used by the
[%Zaber Console G-Code %Translator plugin]
(https://www.zaber.com/wiki/Software/Zaber_Console/G-Code_Translator)
to translate G-code into
[%Zaber ASCII stream commands]
(https://www.zaber.com/wiki/Manuals/ASCII_Protocol_Manual#stream).

Please see https://www.zaber.com/wiki/Software/Zaber_Console/G-Code_Translator
for more information on the translator.

\section api Translator API

It is possible to use the translator programmatically by
providing the G-code source and a Configuration object to
Translator.Translator(TextReader, Configuration)
and calling Translator.Translate()

\subsection example Example

This is an example that sets up a Configuration, translates a G-code file,
prints out warnings and errors, and outputs ASCII commands to the screen.

\code
// Set up configuration for an A-MCB2 with an ASR-T3
Configuration config = new Configuration();

config.OutputType = ConfigurationOutputType.StreamingCommands;
config.StreamNumber = 1;
config.DeviceAddress = 1;
config.TraverseRate = 153600;

config.X.EnabledType = AxisEnabledType.Enabled;
config.X.AxisNumber = 1;
config.X.MinPosition = 0;
config.X.MaxPosition = 640000;
config.X.MicrostepSize = 0.15625M;

config.Y.EnabledType = AxisEnabledType.Enabled;
config.Y.AxisNumber = 2;
config.Y.MinPosition = 0;
config.Y.MaxPosition = 768000;
config.Y.MicrostepSize = 0.15625M;

// Map M-codes to specific ASCII commands
// Multiple ASCII commands can be concatenated
config.Remappings.Add(3, "/1 stream 1 io set do 4 1/1 stream 1 wait 100"); // M3
config.Remappings.Add(5, "/1 stream 1 io set do 4 0/1 stream 1 wait 100"); // M5

// Execute the translation
Translator translator = new Translator(new StreamReader("MyGCodeFile.ngc"), config);
IEnumerable<ZaberCommand> cmds = translator.Translate();

// Print warning and error logs, if any
int logCount = translator.Messages.Log.Count;
if(logCount > 0)
{
    Console.WriteLine("{0} warnings/errors:", logCount);
    foreach (var log in translator.Messages.Log)
    {
        Console.WriteLine(log);
    }
}
else
{
    Console.WriteLine("No warning/error.");
}
Console.WriteLine();

// Print ASCII commands if translation is successful
if (cmds != null)
{
    Console.WriteLine("Output:");
    foreach (var cmd in cmds)
    {
        Console.WriteLine("{0}", cmd.ToString());
    }
}
else
{
    Console.WriteLine("No output was produced. Check errors above.");
}
\endcode

\section configuration Configuration

The Configuration object can be saved to and loaded from files with
Configuration.Serialize(TextWriter, Configuration) and Configuration.Deserialize(TextReader).
These files are compatible with the %Zaber Console plugin.

Please refer to the %Configuration section of the [plugin documentation]
(https://www.zaber.com/wiki/Software/Zaber_Console/G-Code_Translator) for descriptions of each setting.

\section support Support

%Zaber support is available at contact@zaber.com.

Telephone and other contact information are also available at https://www.zaber.com/contact/.

*/
}

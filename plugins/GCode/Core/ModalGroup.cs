﻿namespace Zaber.GCode
{
	internal enum ModalGroup : uint
	{
		// G modal groups
		None,
		Dwell, // G4
		NoneMotion, // G28, G30 (non-modal motion)
		Motion,
		PlaneSelection,
		Distance,
		ArcDistance,
		FeedRate,
		Units,
		RadiusCompensation,
		LengthCompensation,
		ReturnMode,
		CoordinateSystem,
		PathControl,

		// M modal groups
		Stopping,
		ToolChange,
		SpindleTurning,
		Coolant,
		OverrideSwitches,

		NotApplicable
	}
}

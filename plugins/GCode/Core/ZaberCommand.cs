﻿namespace Zaber.GCode
{
	/// <summary>
	///     Interface for Zaber commands to implement.
	///     These commands should be static, that is,
	///     their contents should be determined at construction time.
	/// </summary>
	public class ZaberCommand
	{
		internal ZaberCommand(string command, int device, int axis)
		{
			Command = command;
			Axis = axis;
			Device = device;
		}


		public override string ToString()
		{
			if (DEFAULT_PREFIX == Prefix)
			{
				return $"{Prefix}{Device} {Axis} {Command}\r\n";
			}

			return $"{Prefix}{Command}\r\n";
		}


		public string Command { get; }

		public int Axis { get; }

		public int Device { get; }

		public string Prefix { get; internal set; } = DEFAULT_PREFIX;

		private static readonly string DEFAULT_PREFIX = "/";
	}
}

﻿using System.IO;
using NUnit.Framework;

namespace Zaber.GCode.Tests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class TestLexer
	{
		[Test]
		public void TestTokenizeFloat()
		{
			var line = Lexer.Tokenize("G1.1X2.345", TestTranslator);

			Assert.AreEqual(2, line.Count);
			Assert.AreEqual(new Word('G', 1.1M), line[0]);
			Assert.AreEqual(new Word('X', 2.345M), line[1]);
		}


		[Test]
		public void TestTokenizeMultiple()
		{
			var line = Lexer.Tokenize("G1X1Y1Z3", TestTranslator);

			Assert.AreEqual(4, line.Count);
			Assert.AreEqual(new Word('G', 1), line[0]);
			Assert.AreEqual(new Word('X', 1), line[1]);
			Assert.AreEqual(new Word('Y', 1), line[2]);
			Assert.AreEqual(new Word('Z', 3), line[3]);
		}


		[Test]
		public void TestTokenizeOne()
		{
			var line = Lexer.Tokenize("G1", TestTranslator);

			Assert.AreEqual(1, line.Count);
			Assert.AreEqual(new Word('G', 1), line[0]);
		}


		private static readonly Translator TestTranslator = new Translator(new StringReader(""), new Configuration());
	}
}

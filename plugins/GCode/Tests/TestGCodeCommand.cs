﻿using NUnit.Framework;

namespace Zaber.GCode.Tests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class TestGCodeCommand
	{
		[Test]
		public void TestFloat() => Assert.AreEqual(1.234, (double) new Word('G', 1.234M).Number, Epsilon);


		private const double Epsilon = 1E-6;
	}
}

﻿using System;
using NUnit.Framework;

namespace Zaber.GCode.Tests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class TestWordCategory
	{
		[Test]
		public static void TestConstruction()
		{
			Assert.Throws<ArgumentException>(() => { new WordCategory('G', ModalGroup.None); });
			Assert.Throws<ArgumentException>(() => { new WordCategory('G', ModalGroup.NoneMotion); });

			Assert.AreEqual(new Word('G', 20).Category.Group, ModalGroup.Units);
			Assert.AreEqual(new Word('G', 20).Category.Command, 'G');
		}


		[Test]
		public static void TestEquality()
		{
			Assert.AreEqual(new WordCategory('F'), new WordCategory('F'));
			Assert.AreEqual(new Word('F', 10).Category, new Word('F', 20).Category);
			Assert.AreNotEqual(new Word('F', 10).Category, new Word('M', 20).Category);

			Assert.AreNotEqual(new Word('F', 10).Category, new Word('G', 20).Category);
			Assert.AreEqual(new Word('G', 20).Category, new Word('G', 21).Category);
			Assert.AreEqual(new Word('G', 20).Category, new WordCategory('G', ModalGroup.Units));

			Assert.AreNotEqual(new Word('G', 19).Category, new Word('G', 20).Category);
			Assert.AreNotEqual(new Word('G', 19).Category, new WordCategory('G', ModalGroup.Distance));
		}
	}
}

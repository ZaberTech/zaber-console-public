﻿using System.IO;
using System.Collections.Generic;
using NUnit.Framework;

namespace Zaber.GCode.Tests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class TestIntegration
	{
		static string TEST_GCODE_SIMPLE = @"
G17 G20 G90 G94 G54
G0 X0 Y0 Z0.25
X-0.5 Y0.
Z0.1
G01 Z0. F5.
G02 X0. Y0.5 I0.5 J0. F2.5
X0.5 Y0. I0. J-0.5
X0. Y-0.5 I-0.5 J0.
X-0.5 Y0. I0. J0.5
G01 Z0.1 F5.
G00 X0. Y0. Z0.25
".Trim();

		static string TEST_SIMPLE_OUTPUT = @"
/1 0 stream 1 set maxspeed 1
/1 0 stream 1 line abs 0 0 6350
/1 0 stream 1 line abs -12700 0 6350
/1 0 stream 1 line abs -12700 0 2540
/1 0 stream 1 set maxspeed 3468
/1 0 stream 1 line abs -12700 0 0
/1 0 stream 1 set maxspeed 1734
/1 0 stream 1 on a b arc abs cw 0 0 0 12700
/1 0 stream 1 on a b arc abs cw 0 0 12700 0
/1 0 stream 1 on a b arc abs cw 0 0 0 -12700
/1 0 stream 1 on a b arc abs cw 0 0 -12700 0
/1 0 stream 1 set maxspeed 3468
/1 0 stream 1 line abs -12700 0 2540
/1 0 stream 1 set maxspeed 1
/1 0 stream 1 line abs 0 0 6350
".Trim();

		[Test(Description = "Tests a simple program with default configuration and all axes enabled")]
		public void TestCaseSimple()
		{
			var configuration = new Configuration();
			foreach (var axis in configuration.AllAxes)
			{
				axis.EnabledType = AxisEnabledType.Enabled;
			}

			using (var reader = new StringReader(TEST_GCODE_SIMPLE))
			{
				var translator = new Translator(reader, configuration);

				var commands = translator.Translate();
				var lines = string.Join("", commands).Trim();

				Assert.AreEqual(TEST_SIMPLE_OUTPUT, lines);
			}
		}

		static string TEST_GCODE_SIMPLE_2AXIS = @"
G17 G20 G90 G94 G54
G0 X0 Y0
X-0.5 Y0.
G01 F5.
G02 X0. Y0.5 I0.5 J0. F2.5
".Trim();

		static string TEST_SIMPLE_2AXIS_OUTPUT = @"
/1 0 stream 1 set maxspeed 1
/1 0 stream 1 line abs 0 0
/1 0 stream 1 line abs -12700 0
/1 0 stream 1 set maxspeed 1734
/1 0 stream 1 arc abs cw 0 0 0 12700
".Trim();

		[Test(Description = "Tests a simple program with only two axes enabled")]
		public void TestCaseSimple2Axis()
		{
			var configuration = new Configuration();
			configuration.X.EnabledType = AxisEnabledType.Enabled;
			configuration.Y.EnabledType = AxisEnabledType.Enabled;
			configuration.Z.EnabledType = AxisEnabledType.Disabled;

			using (var reader = new StringReader(TEST_GCODE_SIMPLE_2AXIS))
			{
				var translator = new Translator(reader, configuration);

				var commands = translator.Translate();
				var lines = string.Join("", commands).Trim();

				// Key point is that the "arc abs" command should not have the "on a b" clause because
				// only two axes are enabled and they are the first two (FW6 compatibility).
				Assert.AreEqual(TEST_SIMPLE_2AXIS_OUTPUT, lines);
			}
		}

		static string TEST_Z_PASSTHROUGH_OUTPUT = @"
/1 0 stream 1 set maxspeed 1
/1 0 stream 1 line abs 0 0
G0 Z6350
/1 0 stream 1 line abs -12700 0
G0 Z2540
/1 0 stream 1 set maxspeed 3468
G1 Z0
/1 0 stream 1 set maxspeed 1734
/1 0 stream 1 arc abs cw 0 0 0 12700
/1 0 stream 1 arc abs cw 0 0 12700 0
/1 0 stream 1 arc abs cw 0 0 0 -12700
/1 0 stream 1 arc abs cw 0 0 -12700 0
/1 0 stream 1 set maxspeed 3468
G1 Z2540
/1 0 stream 1 set maxspeed 1
/1 0 stream 1 line abs 0 0
G0 Z6350
".Trim();

		[Test(Description = "Tests a simple program with Z axis on passthrough")]
		public void TestCaseZPassthough()
		{
			var configuration = new Configuration();
			foreach (var axis in configuration.AllAxes)
			{
				axis.EnabledType = AxisEnabledType.Enabled;
			}
			configuration.Z.EnabledType = AxisEnabledType.Passthrough;

			using (var reader = new StringReader(TEST_GCODE_SIMPLE))
			{
				var translator = new Translator(reader, configuration);

				var commands = translator.Translate();
				var lines = string.Join("", commands).Trim();

				Assert.AreEqual(TEST_Z_PASSTHROUGH_OUTPUT, lines);
			}
		}

		static string TEST_GCODE_SIMPLE_RELATIVE = @"
G17 G20 G90 G94 G54
G0 X0.5 Y0.5 Z0.25
G01 Z0. F5.
G91
X0.5
Y0.5
X-0.5 Y-0.5
G90
G01 Z0.1 F5.
G00 X0.5 Y0.5 Z0.25
".Trim();

		static string TEST_GCODE_SIMPLE_RELATIVE_OUTPUT = @"
/1 0 stream 1 set maxspeed 1
/1 0 stream 1 line abs 12700 12700 6350
/1 0 stream 1 set maxspeed 3468
/1 0 stream 1 line abs 12700 12700 0
/1 0 stream 1 line abs 25400 12700 0
/1 0 stream 1 line abs 25400 25400 0
/1 0 stream 1 line abs 12700 12700 0
/1 0 stream 1 line abs 12700 12700 2540
/1 0 stream 1 set maxspeed 1
/1 0 stream 1 line abs 12700 12700 6350
".Trim();

		[Test(Description = "Tests a simple program that contains relative motion")]
		public void TestCaseSimpleRelative()
		{
			var configuration = new Configuration();
			foreach (var axis in configuration.AllAxes)
			{
				axis.EnabledType = AxisEnabledType.Enabled;
			}

			using (var reader = new StringReader(TEST_GCODE_SIMPLE_RELATIVE))
			{
				var translator = new Translator(reader, configuration);

				var commands = translator.Translate();
				var lines = string.Join("", commands).Trim();

				Assert.AreEqual(TEST_GCODE_SIMPLE_RELATIVE_OUTPUT, lines);
			}
		}

		static string TEST_GCODE_REMAPPING = @"
G17 G20 G90 G94 G54
M4 M5
G0 X0.5 Y0.5 Z0.25
M2
G01 Z0. F5.
X0 M1
Y0 M1
X0.5 Y0.5 M1
G01 Z0.1 F5.
M3 M5
G00 X0.5 Y0.5 Z0.25
".Trim();

		static string TEST_GCODE_REMAPPING_OUTPUT = @"
/0 0 wait io di 1 == 0
/1 0 stream 1 set maxspeed 1
/1 0 stream 1 line abs 12700 12700 6350
/0 0 io set do 2 1
/1 0 stream 1 set maxspeed 3468
/1 0 stream 1 line abs 12700 12700 0
/1 0 stream 1 line abs 0 12700 0
/0 0 wait 100
/1 0 stream 1 line abs 0 0 0
/0 0 wait 100
/1 0 stream 1 line abs 12700 12700 0
/0 0 wait 100
/1 0 stream 1 line abs 12700 12700 2540
/0 0 io set do 2 0
/1 0 stream 1 set maxspeed 1
/1 0 stream 1 line abs 12700 12700 6350
".Trim();

		[Test(Description = "Tests a simple program that contains relative motion")]
		public void TestCaseRemapping()
		{
			var configuration = new Configuration();
			foreach (var axis in configuration.AllAxes)
			{
				axis.EnabledType = AxisEnabledType.Enabled;
			}
			configuration.Remappings.Add(1, "wait 100");
			configuration.Remappings.Add(2, "io set do 2 1");
			configuration.Remappings.Add(3, "io set do 2 0");
			configuration.Remappings.Add(4, "wait io di 1 == 0");

			using (var reader = new StringReader(TEST_GCODE_REMAPPING))
			{
				var translator = new Translator(reader, configuration);

				var commands = translator.Translate();
				var lines = string.Join("", commands).Trim();

				Assert.AreEqual(TEST_GCODE_REMAPPING_OUTPUT, lines);
			}
		}

		static string TEST_GCODE_PLANE_CHANGE = @"
G17 G20 G90 G94 G54
G0 X0 Y0 Z0.25
X-0.5 Y0.
Z0.1
G01 Z0. F5.
G02 X0. Y0.5 I0.5 J0. F2.5
G19 
G03 Y0 Z0.5 J-0.5 K0
G18
G03 X0.5 Z0 I0 K-0.5
G17 
G03 X0 Y0.5 I-0.5 J0
G01 Z0.1 F5.
G00 X0. Y0. Z0.25
".Trim();
		static string TEST_GCODE_PLANE_CHANGE_OUTPUT = @"
/1 0 stream 1 set maxspeed 1
/1 0 stream 1 line abs 0 0 6350
/1 0 stream 1 line abs -12700 0 6350
/1 0 stream 1 line abs -12700 0 2540
/1 0 stream 1 set maxspeed 3468
/1 0 stream 1 line abs -12700 0 0
/1 0 stream 1 set maxspeed 1734
/1 0 stream 1 on a b arc abs cw 0 0 0 12700
/1 0 stream 1 on b c arc abs ccw 0 0 0 12700
/1 0 stream 1 on a c arc abs ccw 0 0 12700 0
/1 0 stream 1 on a b arc abs ccw 0 0 0 12700
/1 0 stream 1 set maxspeed 3468
/1 0 stream 1 line abs 0 12700 2540
/1 0 stream 1 set maxspeed 1
/1 0 stream 1 line abs 0 0 6350
".Trim();

		[Test(Description = "Tests a program containing arcs and plane changes")]
		public void TestCasePlaneChange()
		{
			var configuration = new Configuration();
			foreach (var axis in configuration.AllAxes)
			{
				axis.EnabledType = AxisEnabledType.Enabled;
			}

			using (var reader = new StringReader(TEST_GCODE_PLANE_CHANGE))
			{
				var translator = new Translator(reader, configuration);

				var commands = translator.Translate();
				var lines = string.Join("", commands).Trim();

				Assert.AreEqual(TEST_GCODE_PLANE_CHANGE_OUTPUT, lines);
			}
		}
	}
}

﻿using System;
using NUnit.Framework;
using Zaber.GCode.Geometry;

namespace Zaber.GCode.Tests
{
	[TestFixture]
	[SetCulture("en-US")]
	public class TestBounds
	{
		[Test]
		public static void ArcBounds()
		{
			var conf = new Configuration
			{
				X =
				{
					MinPosition = 0,
					MaxPosition = 1000,
					EnabledType = AxisEnabledType.Enabled
				},
				Y =
				{
					MinPosition = 0,
					MaxPosition = 1000,
					EnabledType = AxisEnabledType.Enabled
				}
			};

			Assert.IsFalse(Maths.ArcExceedsBounds(new Vector2(0, 0), 500, Math.PI / 2, 0, true, conf.X, conf.Y));

			Assert.IsFalse(Maths.ArcExceedsBounds(new Vector2(0, 0), 500, Math.PI / 2, 0.001, true, conf.X, conf.Y));

			Assert.IsTrue(Maths.ArcExceedsBounds(new Vector2(0, 0), 500, Math.PI / 2, 0, false, conf.X, conf.Y));

			Assert.IsTrue(Maths.ArcExceedsBounds(new Vector2(0, 0), 500, Math.PI / 2, -0.001, true, conf.X, conf.Y));

			Assert.IsTrue(Maths.ArcExceedsBounds(new Vector2(0, 0), 500, Math.PI / 2, Math.PI, true, conf.X, conf.Y));

			Assert.IsTrue(Maths.ArcExceedsBounds(new Vector2(0, 0), 500, Math.PI / 2, Math.PI, false, conf.X, conf.Y));
		}


		[Test]
		public static void LineBounds()
		{
			var conf = new Configuration
			{
				X =
				{
					MinPosition = 0,
					MaxPosition = 1000,
					EnabledType = AxisEnabledType.Enabled
				},
				Y =
				{
					MinPosition = 0,
					MaxPosition = 1000,
					EnabledType = AxisEnabledType.Enabled
				}
			};

			Assert.IsTrue(Maths.LineExceedsBounds(new Vector2(0, 0), new Vector2(-1, -1), conf));

			Assert.IsFalse(Maths.LineExceedsBounds(new Vector2(0, 0), new Vector2(1, 1), conf));

			Assert.IsFalse(Maths.LineExceedsBounds(new Vector2(0, 0), new Vector2(0, 0), conf));

			Assert.IsTrue(Maths.LineExceedsBounds(new Vector2(0, 0), new Vector2(-1, 0), conf));

			Assert.IsTrue(Maths.LineExceedsBounds(new Vector2(0, 0), new Vector2(1001, 0), conf));
		}
	}
}

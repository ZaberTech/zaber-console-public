﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using NUnit.Framework;
using Zaber;
using Zaber.Testing;
using ZaberConsole.Plugins.Oscilloscope;

namespace OscilloscopePluginTests
{
	[TestFixture]
	[SetCulture("en-US")]
	[Description("Tests for the " + nameof(PlotVM) + " class.")]
	public class PlotVMTests
	{
		[SetUp]
		public void Setup() => _vm = new PlotVM();


		[TearDown]
		public void Teardown()
		{
			_listener?.Unlisten(_vm);
			_vm = null;

			if (!string.IsNullOrEmpty(_tempFileName) && File.Exists(_tempFileName))
			{
				File.Delete(_tempFileName);
				_tempFileName = null;
			}
		}


		[Test]
		[Description("Test that default property values are as expected.")]
		public void TestDefaults()
		{
			Assert.IsNull(_vm.Title);
			Assert.IsNotNull(_vm.DataSeries);
		}


		[Test]
		[Description("Test that changing observable properties generates notifications.")]
		public void TestObservableProperties()
		{
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
			_listener.TestObservableProperty(_vm, () => _vm.Title);
			_listener.TestObservableProperty(_vm,
											 () => _vm.DataSeries,
											 ds => new ObservableCollection<ChartSeries>
												 {
													 new ChartSeries(DateTime.Now, 1000)
												 }
											 );
		}


		// TODO (Soleil 2018-02-23): Test disabled because it was failing on the CI machine occasionally. Investigate.
		#if false
		[Test]
		[Description("Test CSV export functionality.")]
		public void TestExport()
		{
			_vm.Title = "Foo";
			var device = DeviceHelper.CreateTestDeviceForCharts();

			var series1 = CreateTestSeries(device, "encoder.pos", 0.0, 1.0, 1.0, 1.0, 4);
			series1.SelectedUnit = series1.Units.Where(u => u.Abbreviation == "mm").First();
			var series2 = CreateTestSeries(device, "system.voltage", 2.0, 12.0, 1.0, 0.1, 4);
			_vm.DataSeries.Add(series1);
			_vm.DataSeries.Add(series2);

			_tempFileName = Path.GetTempFileName();
			_vm.Export(_tempFileName);
			Assert.IsTrue(File.Exists(_tempFileName));
			var refPath =
 Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "ExportRef1.csv");
			Assert.IsTrue(FileUtil.FilesAreEqual(new FileInfo(_tempFileName), new FileInfo(refPath)));
		}
#endif


		private static ChartSeries CreateTestSeries(ZaberDevice aDevice, string aSettingName, double aStartTime,
													double aStartY, double aTimeStep, double aDeltaY, int aSampleCount)
		{
			var series = new ChartSeries(DateTime.Now, 1000);

			series.PopulateSettingsAndUnits(aDevice);

			var data = new List<Tuple<double, double>>();
			var t = aStartTime;
			var y = aStartY;
			for (var i = 0; i < aSampleCount; ++i)
			{
				data.Add(new Tuple<double, double>(t, y));
				t += aTimeStep;
				y += aDeltaY;
			}

			series.Append(data);

			return series;
		}


		private PlotVM _vm;
		private string _tempFileName;
		private ObservableObjectTester _listener;
	}
}

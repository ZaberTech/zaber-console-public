﻿using NUnit.Framework;
using ZaberConsole.Plugins.Oscilloscope;

namespace OscilloscopePluginTests
{
	[TestFixture]
	[SetCulture("en-US")]
	[Description("Tests for the " + nameof(TriggerType) + " class.")]
	public class TriggerTypeTests
	{
		[Test]
		[Description("Test the trigger type default values.")]
		public void TestTriggerTypeDefaults()
		{
			var t = new TriggerType();
			Assert.AreEqual(true, t.IsSelectable);
		}
	}
}

﻿using NUnit.Framework;
using Zaber.Testing;
using ZaberConsole.Plugins.Oscilloscope;

namespace OscilloscopePluginTests
{
	[TestFixture]
	[SetCulture("en-US")]
	[Description("Tests for the " + nameof(SectionHeaderVM) + " class.")]
	public class SectionHeaderVMTests
	{
		[SetUp]
		public void Setup() => _vm = new SectionHeaderVM();


		[TearDown]
		public void Teardown()
		{
			_listener?.Unlisten(_vm);
			_vm = null;
		}


		[Test]
		[Description("Test that default property values are as expected.")]
		public void TestDefaults()
		{
			Assert.IsFalse(_vm.Enabled);
			Assert.IsFalse(_vm.ShowEnableCheckbox);
			Assert.IsTrue(string.IsNullOrEmpty(_vm.Title));
			Assert.IsTrue(string.IsNullOrEmpty(_vm.Help));
		}


		[Test]
		[Description("Test that changing observable properties generates notifications.")]
		public void TestObservableProperties()
		{
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
			_listener.TestObservableProperty(_vm, () => _vm.Enabled);
			_listener.TestObservableProperty(_vm, () => _vm.ShowEnableCheckbox);
			_listener.TestObservableProperty(_vm, () => _vm.Title);
			_listener.TestObservableProperty(_vm, () => _vm.Help);
		}


		private SectionHeaderVM _vm;
		private ObservableObjectTester _listener;
	}
}

﻿using System;
using NUnit.Framework;
using Zaber.Testing;
using ZaberConsole.Plugins.Oscilloscope;

namespace OscilloscopePluginTests
{
	[TestFixture]
	[SetCulture("en-US")]
	[Description("Tests for the " + nameof(ChartSeries) + " class.")]
	public class ChartSeriesTests
	{
		[SetUp]
		public void Setup()
		{
			DispatcherStack.Push(new ImmediateDispatcher());
			_vm = new ChartSeries(DateTime.Now, 2);
			_ovm = new OscilloscopeVM();
		}


		[TearDown]
		public void Teardown()
		{
			_listener?.Unlisten(_vm);
			_vm = null;
			_ovm = null;
			DispatcherStack.Pop();
		}


		[Test]
		[Description("Test that default property values are as expected.")]
		public void TestDefaults()
		{
			Assert.IsNotNull(_vm.GetRawData());
			Assert.IsTrue(_vm.Visible);
			Assert.IsTrue(_vm.DoCapture);
			Assert.IsNull(_ovm.AvailableSettings);
			Assert.IsNull(_vm.SelectedSetting);
			Assert.IsNull(_vm.Units);
			Assert.IsNull(_vm.SelectedUnit);
			Assert.IsNotNull(_vm.GetConvertedData());
		}


		[Test]
		[Description("Test that changing observable properties generates notifications.")]
		public void TestObservableProperties()
		{
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);
			_listener.TestObservableProperty(_vm, () => _vm.Visible);
			_listener.TestObservableProperty(_vm, () => _vm.DoCapture);
		}


		private ChartSeries _vm;
		private OscilloscopeVM _ovm;
		private ObservableObjectTester _listener;
	}
}

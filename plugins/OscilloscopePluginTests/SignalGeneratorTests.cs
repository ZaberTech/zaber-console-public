﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Zaber;
using Zaber.Testing;
using Zaber.Units;
using Zaber.Units.UI.WPF.Controls;
using ZaberConsole.Plugins.Oscilloscope;
using ZaberConsole.Plugins.Oscilloscope.Settings;
using ZaberTest;
using ZaberTest.Testing;
using Measurement = Zaber.Units.Measurement;

namespace OscilloscopePluginTests
{
	[TestFixture]
	[SetCulture("en-US")]
	[Description("Tests for the " + nameof(SignalGeneratorVM) + " class.")]
	public class SignalGeneratorTests
	{
		[SetUp]
		public void Setup()
		{
			SetupPort();
			_vm = new SignalGeneratorVM();
		}


		[TearDown]
		public void Teardown() => _listener?.Unlisten(_vm);


		[Test]
		[Description("Test default state is as expected.")]
		public void TestConstructor()
		{
			_vm.LoadSettings(null);
			Assert.IsFalse(_vm.IsReturnToStartChecked);
			Assert.AreEqual(0, _vm.MovementTypes.Count);
			Assert.AreEqual("1 -- data --", _vm.SelectedSignalStepMeasurement.Measurement.ToString());
			Assert.IsNull(_vm.SelectedMovementType);
		}


		[Test]
		[Description("Test default state is as expected after a conversation is selected.")]
		public void TestDefaultsAfterFirstSelection()
		{
			_vm.Conversation = _portFacade.GetConversation(1);
			_vm.LoadSettings(null);
			Assert.IsFalse(_vm.IsReturnToStartChecked);
			Assert.AreEqual(3, _vm.MovementTypes.Count);
			Assert.AreEqual("1 cm", _vm.SelectedSignalStepMeasurement.Measurement.ToString());
			Assert.IsNotNull(_vm.SelectedMovementType);
		}


		[Test]
		[Description("Test force command removed if device doesn't have it.")]
		public void TestForceRemoved()
		{
			var conv = _portFacade.GetConversation(1);
			var commands = conv.Device.DeviceType.Commands.ToList();
			commands.Remove(commands.First(c => c.TextCommand == "force abs {0}"));
			conv.Device.DeviceType.Commands = commands;
			_vm.Conversation = conv;
			foreach (var mt in _vm.MovementTypes)
			{
				Assert.AreNotEqual(MoveType.Force, mt.MoveMode);
			}
		}


		[Test]
		[Description("Test that changing controls does not affect previously saved settings.")]
		public void TestMeasurementSettingsNotModifiedByDeviceChange()
		{
			_vm.Conversation = _portFacade.GetConversation(1); // LoadSettings needs a unit converter.
			var settings = new OscilloscopeSignalSettings
			{
				StepSizeUnits = "cm",
				AbsMeasurementSettings = new MeasurementSettings
				{
					SavedQuantity = 3,
					SavedUnitAbbreviation = "in"
				}
			};

			_vm.LoadSettings(settings);
			_vm.SelectedMovementType = _vm.MovementTypes.First(mt => MoveType.Absolute == mt.MoveMode);
			Assert.AreEqual(3, _vm.SelectedSignalStepMeasurement.Quantity);
			Assert.AreEqual("in", _vm.SelectedSignalStepMeasurement.SelectedUnit.Abbreviation);

			// Before the fix, changing these would have altered the original values in
			// the loaded settings instance above. 
			_vm.SelectedSignalStepMeasurement.Quantity = 0;
			_vm.SelectedSignalStepMeasurement.SelectedUnit =
				_vm.SelectedSignalStepMeasurement.Units.FirstOrDefault(u => "in" != u.Abbreviation);
			_vm.SelectedMovementType = _vm.MovementTypes.First(mt => MoveType.Relative == mt.MoveMode);

			Assert.AreEqual(3, settings.AbsMeasurementSettings.SavedQuantity);
			Assert.AreEqual("in", settings.AbsMeasurementSettings.SavedUnitAbbreviation);
		}


		[Test]
		[Description("Test observable properties.")]
		public void TestObservableProperties()
		{
			_vm.Conversation = _portFacade.GetConversation(1);
			_vm.LoadSettings(null);
			_listener = new ObservableObjectTester();
			_listener.Listen(_vm);

			_listener.TestObservableProperty(_vm, () => _vm.IsReturnToStartChecked);
			_listener.TestObservableProperty(_vm,
											 () => _vm.SelectedSignalStepMeasurement,
											 m => new MeasurementBoxVM());

			_listener.ClearNotifications();
			_vm.SelectedMovementType = _vm.MovementTypes[1];
			Assert.IsTrue(_listener.GetNumNotifications() > 0);
			_listener.ClearNotifications();
			_vm.SelectedMovementType = _vm.MovementTypes[0];
			Assert.IsTrue(_listener.GetNumNotifications() > 0);
		}


		[Test]
		[Description("Test saving and loading settings.")]
		public void TestSaveAndLoadSettings()
		{
			_vm.Conversation = _portFacade.GetConversation(1); // LoadSettings needs a unit converter.
			_vm.LoadSettings(null);
			var settings1 = _vm.SaveSettings();
			Assert.AreEqual(MoveType.Relative, settings1.MovementType);
			Assert.AreEqual(1m, settings1.StepSize);
			Assert.AreEqual("cm", settings1.StepSizeUnits);
			Assert.IsFalse(settings1.ReturnToStart);
			_vm.SelectedMovementType = _vm.MovementTypes.First(mt => MoveType.Absolute == mt.MoveMode);
			_vm.IsReturnToStartChecked = true;
			_vm.SelectedSignalStepMeasurement.Measurement =
				new Measurement(2, _vm.SelectedSignalStepMeasurement.UnitConverter.FindUnitBySymbol("m"));
			var settings2 = _vm.SaveSettings();
			Assert.AreEqual(MoveType.Absolute, settings2.MovementType);
			Assert.AreEqual(2m, settings2.StepSize);
			Assert.AreEqual("m", settings2.StepSizeUnits);
			Assert.IsTrue(settings2.ReturnToStart);
			_vm.LoadSettings(settings1);
			Assert.AreEqual(MoveType.Relative, _vm.SelectedMovementType.MoveMode);
			Assert.AreEqual("1 cm", _vm.SelectedSignalStepMeasurement.Measurement.ToString());
			Assert.IsFalse(_vm.IsReturnToStartChecked);
			_vm.LoadSettings(settings2);
			Assert.AreEqual(MoveType.Absolute, _vm.SelectedMovementType.MoveMode);
			Assert.AreEqual("2 m", _vm.SelectedSignalStepMeasurement.Measurement.ToString());
			Assert.IsTrue(_vm.IsReturnToStartChecked);
		}


		[Test]
		[Description("Test different move modes.")]
		public void TestStartStopMoveModes()
		{
			_vm.Conversation = _portFacade.GetConversation(1);
			_vm.LoadSettings(null);
			_vm.IsReturnToStartChecked = false;
			_vm.SelectedMovementType = _vm.MovementTypes.First(mt => MoveType.Relative == mt.MoveMode);
			_vm.SelectedSignalStepMeasurement.Measurement =
				new Measurement(1, _vm.SelectedSignalStepMeasurement.UnitConverter.FindUnitBySymbol("cm"));
			_mockPort.ReadExpectationsFromString("/01 0 move rel 10000");
			_mockPort.ReadExpectationsFromString("@01 0 OK IDLE -- 0");
			_vm.Start();
			_vm.Stop();
			_mockPort.Verify();

			_vm.SelectedMovementType = _vm.MovementTypes.First(mt => MoveType.Absolute == mt.MoveMode);
			_vm.SelectedSignalStepMeasurement.Measurement =
				new Measurement(1, _vm.SelectedSignalStepMeasurement.UnitConverter.FindUnitBySymbol("cm"));
			_mockPort.ReadExpectationsFromString("/01 0 move abs 10000");
			_mockPort.ReadExpectationsFromString("@01 0 OK IDLE -- 0");
			_vm.Start();
			_vm.Stop();
			_mockPort.Verify();

			_vm.SelectedMovementType = _vm.MovementTypes.First(mt => MoveType.Force == mt.MoveMode);
			_vm.SelectedSignalStepMeasurement.Measurement =
				new Measurement(1, _vm.SelectedSignalStepMeasurement.UnitConverter.FindUnitBySymbol("N"));
			_mockPort.ReadExpectationsFromString("/01 0 force abs 40");
			_mockPort.ReadExpectationsFromString("@01 0 OK IDLE -- 0");
			_vm.Start();
			_vm.Stop();
			_mockPort.Verify();
		}


		[Test]
		[Description("Test starting and stopping with return to home unchecked.")]
		public void TestStartStopWithoutReturnToHome()
		{
			_vm.Conversation = _portFacade.GetConversation(1);
			_vm.LoadSettings(null);
			_vm.IsReturnToStartChecked = false;
			_vm.SelectedMovementType = _vm.MovementTypes.First();
			_vm.SelectedSignalStepMeasurement.Measurement =
				new Measurement(1, _vm.SelectedSignalStepMeasurement.UnitConverter.FindUnitBySymbol("cm"));
			_mockPort.ReadExpectationsFromString("/01 0 move rel 10000");
			_mockPort.ReadExpectationsFromString("@01 0 OK IDLE -- 0");
			_vm.Start();
			_mockPort.Verify();
		}


		[Test]
		[Description("Test starting and stopping with return to home checked.")]
		public void TestStartStopWithReturnToHome()
		{
			_vm.Conversation = _portFacade.GetConversation(1);
			_vm.LoadSettings(null);
			_vm.IsReturnToStartChecked = true;
			_vm.SelectedMovementType = _vm.MovementTypes.First();
			_vm.SelectedSignalStepMeasurement.Measurement =
				new Measurement(1, _vm.SelectedSignalStepMeasurement.UnitConverter.FindUnitBySymbol("cm"));
			_mockPort.ReadExpectationsFromString("/01 0 get pos");
			_mockPort.ReadExpectationsFromString("@01 0 OK IDLE -- 1000000");
			_mockPort.ReadExpectationsFromString("/01 0 move rel 10000");
			_mockPort.ReadExpectationsFromString("@01 0 OK IDLE -- 0");
			_vm.Prepare();
			_vm.Start();
			_mockPort.Verify();
			_mockPort.ReadExpectationsFromString("/01 0 move abs 1000000");
			_mockPort.ReadExpectationsFromString("@01 0 OK IDLE -- 0");
			_vm.Stop();
			_mockPort.Verify();
		}


		private void SetupPort()
		{
			_mockPort = new MockPort { IsAsciiMode = true };

			var defaultDeviceType = new DeviceType { Commands = new List<CommandInfo>() };

			_portFacade = new ZaberPortFacade
			{
				DefaultDeviceType = defaultDeviceType,
				Port = _mockPort,
				QueryTimeout = 100
			};

			var dmqCommands = new List<CommandInfo>();
			dmqCommands.AddRange(
				AsciiCommandBuilder.ReadCommandDefinitionsFromJsonFile(AsciiCommandBuilder.BASIC_MOVE_COMMAND_FILE));
			dmqCommands.AddRange(
				AsciiCommandBuilder.ReadCommandDefinitionsFromJsonFile(AsciiCommandBuilder.FORCE_COMMAND_FILE));

			var dmq = new DeviceType
			{
				DeviceId = 50419,
				Name = "X-DMQ12P-DE52 Stage",
				MotionType = MotionType.Linear,
				FirmwareVersion = new FirmwareVersion(6, 25),
				Commands = dmqCommands
			};

			_portFacade.AddDeviceType(dmq);

			ZaberPortFacadeTest.SetAsciiExpectations(_portFacade, _mockPort, dmq);

			_portFacade.Open("COM3");
			var conv = _portFacade.GetConversation(1);
			conv.Device.MicrostepResolution = 1;
		}


		private MockPort _mockPort;
		private ZaberPortFacade _portFacade;
		private SignalGeneratorVM _vm;
		private ObservableObjectTester _listener;
	}
}

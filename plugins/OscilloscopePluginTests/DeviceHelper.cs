﻿using System.Collections.Generic;
using System.Linq;
using Zaber;

namespace OscilloscopePluginTests
{
	internal class DeviceHelper
	{
		internal static ZaberDevice CreateTestDeviceForCharts()
		{
			var deviceType = new DeviceType();
			var uc = ConversionTable.Default;

			var commands = new List<CommandInfo>
			{
				new SettingInfo
				{
					TextCommand = "encoder.pos",
					RequestUnit =
						uc.FindUnits(MotionType.Linear, MeasurementType.Position)
					   .Where(u => u.Abbreviation == "m")
					   .First(),
					RequestUnitScale = 1.0m,
					RequestUnitFunction = ScalingFunction.Linear
				},
				new SettingInfo { TextCommand = "system.voltage" }
			};

			deviceType.Commands = commands;

			var device = new ZaberDevice
			{
				DeviceNumber = 1,
				DeviceType = deviceType,
				UnitConverter = uc
			};

			return device;
		}
	}
}

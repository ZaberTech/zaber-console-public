﻿using NUnit.Framework;
using Zaber;
using ZaberConsole.Plugins.Oscilloscope;

namespace OscilloscopePluginTests
{
	[TestFixture]
	[SetCulture("en-US")]
	[Description("Tests for the " + nameof(SettingInfoWrapper) + " class.")]
	public class SettingInfoWrapperTests
	{
		[SetUp]
		public void Setup() => _device = DeviceHelper.CreateTestDeviceForCharts();


		[TearDown]
		public void Teardown() => _device = null;


		[Test]
		[Description(
			"Test that creating a setting info wrapper correctly initializes the default value for the display name (without a description).")]
		public void TestNameSettingDefault()
		{
			var si = _device.DeviceType.Commands[1] as SettingInfo;
			var siw = new SettingInfoWrapper(si) { DisplayName = si.TextCommand };
			Assert.AreEqual("system.voltage", siw.DisplayName);
		}


		[Test]
		[Description(
			"Test that setting the display name works correctly for a setting info wrapper with a description.")]
		public void TestNameSettingEncoderPos()
		{
			var si = _device.DeviceType.Commands[0] as SettingInfo;
			var siw = new SettingInfoWrapper(si) { DisplayName = si.TextCommand };
			siw.SetDisplayName();
			Assert.AreEqual("Measured Position (encoder.pos)", siw.DisplayName);
		}


		[Test]
		[Description(
			"Test that setting the display name works correctly for a setting info wrapper with a description.")]
		public void TestNameSettingSystemVoltage()
		{
			var si = _device.DeviceType.Commands[1] as SettingInfo;
			var siw = new SettingInfoWrapper(si) { DisplayName = si.TextCommand };
			siw.SetDisplayName();
			Assert.AreEqual("Supply Voltage (system.voltage)", siw.DisplayName);
		}


		private ZaberDevice _device;
	}
}

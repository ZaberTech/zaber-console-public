﻿namespace Zaber.SensorLogger
{
	/// <summary>
	///     Interaction logic for LoadStarLoggerView.xaml
	/// </summary>
	public partial class SensorLoggerView
	{
		/// <summary>
		///     Default constructor. Initializes the control.
		/// </summary>
		public SensorLoggerView()
		{
			InitializeComponent();
		}
	}
}

﻿using System.ComponentModel.Composition;
using System.Windows;
using Zaber.PlugIns;

namespace Zaber.SensorLogger
{
	/// <summary>
	///     Top-level resource dictionary class for this plugin. Exists to enable Zaber Console to merge WPF resources
	///     when loading the plugin.
	/// </summary>
	[Export(PlugInAttribute.PLUGIN_RESOURCE_DICTIONARY, typeof(ResourceDictionary))]
	public partial class MainResourceDictionary : ResourceDictionary
	{
		/// <summary>
		///     Default constructor. Initializes the dictionary.
		/// </summary>
		public MainResourceDictionary()
		{
			InitializeComponent();
		}
	}
}

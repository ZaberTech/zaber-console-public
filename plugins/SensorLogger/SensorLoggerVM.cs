﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Input;
using log4net;
using Zaber.PlugIns;
using Zaber.SensorLogger.Properties;
using Zaber.Telemetry;
using ZaberWpfToolbox;
using ZaberWpfToolbox.Dialogs;
using Timer = System.Timers.Timer;

namespace Zaber.SensorLogger
{
	/// <summary>
	///     Main VM for the sensor logger window.
	/// </summary>
	[PlugIn(Name = "Sensor Logger", Description = "Data logger for simple sensors.")]
	public partial class SensorLoggerVM : ObservableObject, IDialogClient
	{
		#region -- Setup --

		/// <summary>
		///     Default constructor. Does nothing.
		/// </summary>
		public SensorLoggerVM()
		{
			_autoStopComparisonTypes = new[]
			{
				new ThresholdComparisonType
				{
					DisplayName = ">=",
					Test = (a, b) => a >= b
				},
				new ThresholdComparisonType
				{
					DisplayName = "<=",
					Test = (a, b) => a <= b
				}
			};

			_autoStopSelectedComparison = _autoStopComparisonTypes[0];

			RestoreSettings();
		}

		#endregion

		#region -- Plugin API --

		/// <summary>
		///     Reference to the Plugin Manager. Used to detect user settings reset events.
		/// </summary>
		[PlugInProperty]
		public IPlugInManager PluginManager
		{
			get => _pluginManager;
			set
			{
				if (null != _pluginManager)
				{
					_pluginManager.SettingsResetEvent -= OnSettingsReset;
				}

				_pluginManager = value;

				if (null != _pluginManager)
				{
					_pluginManager.SettingsResetEvent += OnSettingsReset;
				}
			}
		}


		/// <summary>
		///     The currently active Port Facade, provided by Zaber Console.
		/// </summary>
		[PlugInProperty]
		public ZaberPortFacade PortFacade
		{
			get => _portFacade;
			set
			{
				if (null != _portFacade)
				{
					_portFacade.Closed -= Port_OpenOrClosed;
					_portFacade.Opened -= Port_OpenOrClosed;
				}

				_portFacade = value;

				if (null != _portFacade)
				{
					_portFacade.Closed += Port_OpenOrClosed;
					_portFacade.Opened += Port_OpenOrClosed;
				}
			}
		}


		/// <summary>
		///     Currently selected device conversation in Zaber Console.
		/// </summary>
		[PlugInProperty]
		public Conversation Conversation
		{
			get => _conversation;
			set
			{
				_conversation = value;
				_dispatcher.BeginInvoke(() =>
				{
					OnPropertyChanged(nameof(IsSingleAxisSelected));
					UpdateUnitList();
				});
			}
		}


		/// <summary>
		///     Set by the plugin manager when the user changees the master unit of measure
		///     in the device list; used to update selected units in plugins to match.
		/// </summary>
		[PlugInProperty]
		public UnitOfMeasure MasterUnitOfMeasure
		{
			set => _dispatcher.BeginInvoke(() => { SelectedPositionUnit = value; });
		}


		/// <summary>
		///     Invoked by the plugin manager when the plugin is deactivated.
		/// </summary>
		[PlugInMethod(PlugInMethodAttribute.EventType.PluginDeactivated)]
		public void OnPluginDeactivated()
		{
			ExperimentInProgress = false;
			_doLogging = false;
			SensorThreadStop();
			MotionThreadStop();
			LoggerThreadStop();
			SensorLoggerSettings.Default.Save();
		}

		#endregion

		#region -- View-bound properties --

		/// <summary>
		///     Stage start position for the experiment runs, in whatever the selected unit is.
		/// </summary>
		public decimal StartPosition
		{
			get => _startPos;
			set
			{
				Set(ref _startPos, value, nameof(StartPosition));
				SensorLoggerSettings.Default.StartPosition = value;
			}
		}


		/// <summary>
		///     Stage end position for the experiment runs, in whatever the selected unit is.
		/// </summary>
		public decimal EndPosition
		{
			get => _endPos;
			set
			{
				Set(ref _endPos, value, nameof(EndPosition));
				SensorLoggerSettings.Default.EndPosition = value;
			}
		}


		/// <summary>
		///     Increment to move the stage by at each step of the experiment, in whatever the selected unit is.
		/// </summary>
		public decimal PositionIncrement
		{
			get => _posIncrement;
			set
			{
				Set(ref _posIncrement, value, nameof(PositionIncrement));
				SensorLoggerSettings.Default.PositionIncrement = value;
			}
		}


		/// <summary>
		///     Time to wait between the end of each stage move and recording the sensor reading, in
		///     seconds. Negative values are clamped to zero.
		/// </summary>
		public double SettleTime
		{
			get => _settleTime;
			set
			{
				value = Math.Max(value, 0.0);
				Set(ref _settleTime, value, nameof(SettleTime));
				SensorLoggerSettings.Default.SettleTime = value;
			}
		}


		/// <summary>
		///     Applicable units of measure for the stage position values.
		/// </summary>
		public ObservableCollection<UnitOfMeasure> PositionUnits { get; } = new ObservableCollection<UnitOfMeasure>();


		/// <summary>
		///     Currently selected unit of measure for stage moves.
		/// </summary>
		public UnitOfMeasure SelectedPositionUnit
		{
			get => _selectedUnit;
			set
			{
				Set(ref _selectedUnit, value, nameof(SelectedPositionUnit));
				if (null != value)
				{
					SensorLoggerSettings.Default.PositionUnit = value.Abbreviation;
				}
			}
		}


		/// <summary>
		///     List of serial ports available for the sensor connection. This is the list of all
		///     serial ports on the computer minus the one in use by Zaber Console.
		/// </summary>
		public IEnumerable<string> SerialPorts
		{
			get
			{
				var portNames = SerialPort.GetPortNames().ToList();
				if (PortFacade.IsOpen)
				{
					portNames.Remove(PortFacade.Port.PortName);
				}

				portNames.Sort();

				return portNames;
			}
		}


		/// <summary>
		///     The name of the currently selected serial port for interfacing with the sensor.
		/// </summary>
		public string SelectedPort
		{
			get => _selectedPort;
			set
			{
				Set(ref _selectedPort, value, nameof(SelectedPort));
				SensorLoggerSettings.Default.Port = value;
			}
		}


		/// <summary>
		///     List of all supported baud rates for serial ports. Includes an auto-detect option.
		/// </summary>
		public IEnumerable<string> BaudRates
		{
			get
			{
				var rates = new List<string> { AUTO_BAUD_RATE };

				foreach (var rate in _baudRates)
				{
					rates.Add(rate.ToString());
				}

				return rates;
			}
		}


		/// <summary>
		///     Currently selected baud rate, as a string. Possible values include the auto-detect option.
		/// </summary>
		public string SelectedBaudRate
		{
			get => _selectedBaudRate;
			set
			{
				Set(ref _selectedBaudRate, value, nameof(SelectedBaudRate));
				SensorLoggerSettings.Default.BaudRate = value;
			}
		}


		/// <summary>
		///     Gets or sets the displayed name of the currently connected sensor device.
		/// </summary>
		public string SensorMessage
		{
			get => _sensorMessage;
			set => Set(ref _sensorMessage, value, nameof(SensorMessage));
		}


		/// <summary>
		///     Label displayed on the connect button for the sensor.
		/// </summary>
		public string ConnectButtonLabel
		{
			get => _connectButtonLabel;
			set => Set(ref _connectButtonLabel, value, nameof(ConnectButtonLabel));
		}


		/// <summary>
		///     Indicates if there is currently a recognized sensor attached and returning data.
		/// </summary>
		public bool IsSensorConnected
		{
			get => _isSensorConnected;
			set => Set(ref _isSensorConnected, value, nameof(IsSensorConnected));
		}


		/// <summary>
		///     The current reading from the sensor.
		/// </summary>
		public float SensorReading
		{
			get => _sensorReading;
			set
			{
				_sensorReading = value;
				OnPropertyChanged(nameof(SensorReading));
			}
		}


		/// <summary>
		///     Human-readable name of the units for the sensor output values, or empty string if not known.
		/// </summary>
		public string SensorUnits
		{
			get => _sensorUnits;
			set => Set(ref _sensorUnits, value, nameof(SensorUnits));
		}


		/// <summary>
		///     Sensor threshold for the auto-home behavior.
		/// </summary>
		public float Threshold
		{
			get => _threshold;
			set
			{
				Set(ref _threshold, value, nameof(Threshold));
				SensorLoggerSettings.Default.Threshold = value;
			}
		}


		/// <summary>
		///     Data binding for the home condition checkbox.
		/// </summary>
		public bool EnableAutoStop
		{
			get => _enableAutoStop;
			set
			{
				Set(ref _enableAutoStop, value, nameof(EnableAutoStop));
				SensorLoggerSettings.Default.EnableAutoStop = value;
			}
		}


		/// <summary>
		///     List of options for which devices to stop when threshold condition is met.
		/// </summary>
		public IEnumerable<string> AutoStopDeviceSelections => new[] { "selected device", "all devices" };


		/// <summary>
		///     Currently selection option from the list returned by <cref>AutoStopDeviceSelections</cref>.
		/// </summary>
		public int AutoStopDeviceSelection
		{
			get => _autoStopDeviceSelection;
			set
			{
				Set(ref _autoStopDeviceSelection, value, nameof(AutoStopDeviceSelection));
				SensorLoggerSettings.Default.AutoStopDeviceSelection = value;
			}
		}


		/// <summary>
		///     List of possible threshold comparison tests to do.
		/// </summary>
		public IEnumerable<ThresholdComparisonType> AutoStopComparisonTypes => _autoStopComparisonTypes;


		/// <summary>
		///     Currently selected threshold comparison type.
		/// </summary>
		public ThresholdComparisonType AutoStopSelectedComparison
		{
			get => _autoStopSelectedComparison;
			set
			{
				Set(ref _autoStopSelectedComparison, value, nameof(AutoStopSelectedComparison));

				for (var i = 0; i < _autoStopComparisonTypes.Length; ++i)
				{
					if (_autoStopComparisonTypes[i].DisplayName == value.DisplayName)
					{
						SensorLoggerSettings.Default.ThresholdComparisonTypeIndex = i;
						break;
					}
				}
			}
		}


		/// <summary>
		///     Flag indicating whether estop should be used instead of normal stop.
		/// </summary>
		public bool UseEmergencyStop
		{
			get => _useEmergencyStop;
			set
			{
				Set(ref _useEmergencyStop, value, nameof(UseEmergencyStop));
				SensorLoggerSettings.Default.UseEmergencyStop = value;
			}
		}


		/// <summary>
		///     True if sensor data is being recorded.
		/// </summary>
		public bool ExperimentInProgress
		{
			get => _experimentInProgress;
			set
			{
				_experimentInProgress = value;
				OnPropertyChanged(nameof(ExperimentInProgress));
			}
		}


		/// <summary>
		///     Convenience property for the view to be able to tell if the port is open in ASCII mode.
		/// </summary>
		public bool IsAsciiMode => PortFacade?.Port?.IsAsciiMode ?? false;


		/// <summary>
		///     Convenience property for the view to tell if the port is open.
		/// </summary>
		public bool IsPortOpen => PortFacade?.IsOpen ?? false;


		/// <summary>
		///     Convenience property for the view to tell if a single movement axis is selected.
		/// </summary>
		public bool IsSingleAxisSelected
		{
			get
			{
				if ((null == Conversation)
				|| Conversation is ConversationCollection
				|| !Conversation.Device.IsSingleDevice)
				{
					return false;
				}

				if (Conversation.Device.Axes.Count == 1)
				{
					return MotionType.None != Conversation.Device.Axes[0].DeviceType.MotionType;
				}

				return MotionType.None != Conversation.Device.DeviceType.MotionType;
			}
		}


		/// <summary>
		///     View bound property that enables saving experiment results to CSV files.
		/// </summary>
		public bool EnableSaveResults
		{
			get => _enableSaveResults;
			set
			{
				Set(ref _enableSaveResults, value, nameof(EnableSaveResults));
				SensorLoggerSettings.Default.AutoSaveResults = value;
			}
		}


		/// <summary>
		///     Observable property for views to sync the folder for storing result files.
		/// </summary>
		public string ResultsFolder
		{
			get => _resultsFolder;
			set
			{
				Set(ref _resultsFolder, value, nameof(ResultsFolder));
				OnPropertyChanged(nameof(CanSaveResults));
				SensorLoggerSettings.Default.LastOutputDirectory = value;
			}
		}


		/// <summary>
		///     Observable property for views to sync the base file name.
		/// </summary>
		public string BaseFileName
		{
			get => _baseFileName;
			set
			{
				Set(ref _baseFileName, value, nameof(BaseFileName));
				OnPropertyChanged(nameof(CanSaveResults));
				SensorLoggerSettings.Default.BaseFileName = value;
			}
		}


		/// <summary>
		///     Helper for the view to determine if the output file properties are valid.
		/// </summary>
		public bool CanSaveResults
		{
			get
			{
				if (string.IsNullOrEmpty(ResultsFolder) || !Directory.Exists(ResultsFolder))
				{
					return false;
				}

				if (string.IsNullOrEmpty(BaseFileName))
				{
					return false;
				}

				foreach (var c in Path.GetInvalidFileNameChars())
				{
					if (BaseFileName.Contains(c))
					{
						return false;
					}
				}

				return true;
			}
		}

		/// <summary>
		///     Command invoked by the sensor connect button.
		/// </summary>
		public ICommand ConnectButtonCommand
			=> OnDemandRelayCommand(ref _connectButtonCommand,
									_ => OnConnectClicked(),
									_ => !_experimentInProgress
									 && (((null == _sensorPort)
									  && !string.IsNullOrEmpty(SelectedPort))
									 || (null != _sensorPort)));


		/// <summary>
		///     Command invoked by the tare button.
		/// </summary>
		public ICommand TareButtonCommand 
			=> OnDemandRelayCommand(ref _tareButtonCommand,
								    _ => _sensorThreadCommands.Enqueue(SensorThreadCommand.Tare),
								    _ => !_experimentInProgress && (null != _sensorPort));


		/// <summary>
		///     Command invoked by the start button.
		/// </summary>
		public ICommand StartButtonCommand
			=> OnDemandRelayCommand(ref _startButtonCommand, _ => StartExperiment());


		/// <summary>
		///     Command invoked by the stop button.
		/// </summary>
		public ICommand StopButtonCommand
			=> OnDemandRelayCommand(ref _stopButtonCommand, _ => StopExperiment());


		/// <summary>
		///     Command binding for view use.
		/// </summary>
		public ICommand OpenLastResultsCommand
			=> OnDemandRelayCommand(ref _openLastResultsButtonCommand,
									_ => Process.Start(_lastResultsPath),
									_ => !string.IsNullOrEmpty(_lastResultsPath)
									 && File.Exists(_lastResultsPath));


		/// <summary>
		///     Command binding for view use.
		/// </summary>
		public ICommand ResultsFolderBrowseCommand
			=> OnDemandRelayCommand(ref _resultsFolderButtonCommand, _ => BrowseForResultsFolder());

		#endregion

		#region -- IDialogClient implementation --

		/// <summary>
		///     Delegate this class will use to cause dialog boxes to be displayed.
		/// </summary>
		public event RequestDialogChange RequestDialogOpen;

		/// <summary>
		///     Delegate this class will use to close dialog boxes programmatically.
		/// </summary>
		#pragma warning disable 67 // Never referenced
		public event RequestDialogChange RequestDialogClose;
		#pragma warning restore 67

		#endregion

		#region -- Event handlers --

		private void OnSettingsReset(object aSender, EventArgs aArgs)
		{
			var settings = SensorLoggerSettings.Default;
			settings.Reset();
			RestoreSettings();
		}


		private void Port_OpenOrClosed(object aSender, EventArgs aArgs)
		{
			_unitCache.Clear();
			_dispatcher.BeginInvoke(() =>
			{
				OnPropertyChanged(nameof(SerialPorts));
				OnPropertyChanged(nameof(IsAsciiMode));
				OnPropertyChanged(nameof(IsPortOpen));
				OnPropertyChanged(nameof(IsSingleAxisSelected));
			});
		}


		private void RestoreSettings()
		{
			var settings = SensorLoggerSettings.Default;
			ResultsFolder = settings.LastOutputDirectory;
			if (string.IsNullOrEmpty(ResultsFolder) || !Directory.Exists(ResultsFolder))
			{
				ResultsFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			}

			BaseFileName = settings.BaseFileName;
			EnableSaveResults = settings.AutoSaveResults;
			StartPosition = settings.StartPosition;
			EndPosition = settings.EndPosition;
			PositionIncrement = settings.PositionIncrement;
			SettleTime = settings.SettleTime;
			SelectedPort = settings.Port;
			SelectedBaudRate = settings.BaudRate;
			if (string.IsNullOrEmpty(SelectedBaudRate))
			{
				SelectedBaudRate = AUTO_BAUD_RATE;
			}

			Threshold = settings.Threshold;
			EnableAutoStop = settings.EnableAutoStop;
			AutoStopDeviceSelection = settings.AutoStopDeviceSelection;
			UseEmergencyStop = settings.UseEmergencyStop;

			var index = settings.ThresholdComparisonTypeIndex;
			if ((index < 0) || (index >= _autoStopComparisonTypes.Length))
			{
				index = 0;
			}

			AutoStopSelectedComparison = _autoStopComparisonTypes[index];

			var unit = UnitOfMeasure.FindByAbbreviation(settings.PositionUnit);
			if (null != unit)
			{
				SelectedPositionUnit = unit;
			}
		}

		#endregion

		#region -- Helpers --

		private void UpdateUnitList()
		{
			if (null == Conversation)
			{
				PositionUnits.Clear();
				PositionUnits.Add(UnitOfMeasure.Data);
				SelectedPositionUnit = UnitOfMeasure.Data;
				return;
			}

			// Query the device type for legal units.			
			ZaberDevice unitsDevice = null;
			if ((null != PortFacade)
			&& (1 == PortFacade.GetDevice(Conversation.Device.DeviceNumber).Axes.Count)
			&& (0 == Conversation.Device.AxisNumber))
			{
				// Device with single axis, get UoM from axis
				unitsDevice = Conversation.Device.Axes[0];
			}
			else
			{
				unitsDevice = Conversation.Device;
			}

			// Cache units because the lookup can be expensive, and occurs whenever the selected device changes.
			if (!_unitCache.TryGetValue(unitsDevice.GetIdentity(), out var units))
			{
				units = unitsDevice.GetUnitsOfMeasure(MeasurementType.Position, unitsDevice.DeviceType.Commands.Where(cmd =>
					cmd.Command == Command.MoveAbsolute || cmd.TextCommand == "move abs {0}"));
				_unitCache[unitsDevice.GetIdentity()] = units;
			}

			PositionUnits.Clear();
			foreach (var unit in units)
			{
				PositionUnits.Add(unit);
			}

			if (!PositionUnits.Contains(SelectedPositionUnit))
			{
				SelectedPositionUnit = UnitOfMeasure.Data;
			}
		}


		private void StartExperiment()
		{
			_lastResultsPath = null;
			_autoStopReading = null;
			OnPropertyChanged(nameof(OpenLastResultsCommand));
			ExperimentInProgress = true;
			_recordSampleFlag.Reset();
			_resumeMotionFlag.Reset();
			LoggerThreadStart();
			MotionThreadStart();
			_eventLog.LogEvent("Experiment started");
		}


		private void StopExperiment()
		{
			ExperimentInProgress = false;
			MotionThreadStop();
			LoggerThreadStop();
		}


		private void ClearMessageQueue()
		{
			while (!_sensorPortData.IsEmpty)
			{
				_sensorPortData.TryDequeue(out var dummy);
			}
		}


		private IEnumerable<string> GetSensorResponses(string aCommand)
		{
			ClearMessageQueue();
			_sensorPort.Write(aCommand);

			var timeout = false;
			var timer = new Timer(500.0);
			timer.Elapsed += (aTimerSender, aTimerArgs) =>
			{
				timeout = true;
				timer.Stop();
			};
			timer.Start();

			var results = new List<string>();
			while (!timeout)
			{
				_sensorPortData.TryDequeue(out var message);
				if (null != message)
				{
					results.Add(message);
					timer.Stop();
					timeout = false;
					timer.Start();
				}

				Thread.Sleep(10);
			}

			timer.Stop();

			return results;
		}


		private void BrowseForResultsFolder()
		{
			var dlg = new FolderBrowserDialogParams
			{
				SelectedPath = ResultsFolder,
				Description = "Select location to save CSV files",
				ShowNewFolderButton = true
			};

			RequestDialogOpen?.Invoke(this, dlg);
			if (dlg.DialogResult.HasValue && dlg.DialogResult.Value && Directory.Exists(dlg.SelectedPath))
			{
				ResultsFolder = dlg.SelectedPath;
			}
		}


		private string GenerateFileName()
		{
			var exists = false;
			string path = null;
			var index = 1;

			do
			{
				var time = DateTime.Now;
				var timestamp = time.ToString("yyyy-MM-dd_HH-mm-ss");
				if (exists)
				{
					timestamp += "_" + index;
					++index;
				}

				var fname = BaseFileName + "_" + timestamp + ".csv";
				path = Path.Combine(ResultsFolder, fname);
				exists = File.Exists(path);
			} while (exists);

			return path;
		}

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly IEventLog _eventLog = Router.Instance.GetEventLogger("Sensor Logger tab");

		private enum SensorThreadCommand
		{
			Idle,
			Terminate,
			Tare
		}

		private enum SensorCategory
		{
			Unknown,
			Loadstar
		}

		/// <summary>
		///     Used internally for doing threshold comparisons.
		/// </summary>
		public class ThresholdComparisonType
		{
			/// <summary>
			///     Overload of string conversion.
			/// </summary>
			/// <returns><cref>DisplayName</cref>.</returns>
			public override string ToString() => DisplayName;


			/// <summary>
			///     Human-readable name for the type of comparison.
			/// </summary>
			public string DisplayName { get; set; }

			/// <summary>
			///     Function that actually does the comparison.
			/// </summary>
			public Func<float, float, bool> Test { get; set; }
		}


		private IPlugInManager _pluginManager;
		private readonly IDispatcher _dispatcher = DispatcherHelper.Dispatcher;
		private RelayCommand _connectButtonCommand;
		private RelayCommand _tareButtonCommand;
		private RelayCommand _startButtonCommand;
		private RelayCommand _stopButtonCommand;
		private RelayCommand _openLastResultsButtonCommand;
		private RelayCommand _resultsFolderButtonCommand;
		private ZaberPortFacade _portFacade;
		private Conversation _conversation;
		private SerialPort _sensorPort;
		private readonly ConcurrentQueue<string> _sensorPortData = new ConcurrentQueue<string>();

		private readonly ConcurrentQueue<SensorThreadCommand> _sensorThreadCommands =
			new ConcurrentQueue<SensorThreadCommand>();

		private readonly StringBuilder _sensorMessageBuffer = new StringBuilder();
		private Thread _sensorThread;
		private Thread _motionThread;
		private Thread _loggerThread;
		private readonly ManualResetEvent _recordSampleFlag = new ManualResetEvent(false);
		private readonly ManualResetEvent _resumeMotionFlag = new ManualResetEvent(false);
		private volatile bool _experimentInProgress;

		private static readonly string AUTO_BAUD_RATE = "Auto-detect";

		private static readonly int[] _baudRates =
		{
			256000, 230400, 128000, 115200, 57600, 38400, 19200, 14400, 9600, 4800, 2400, 1200, 600, 300, 110
		};

		private string _selectedBaudRate = AUTO_BAUD_RATE;
		private string _sensorMessage = string.Empty;
		private bool _isSensorConnected;
		private string _selectedPort;
		private string _connectButtonLabel = "Connect";
		private volatile float _sensorReading;
		private float _threshold = 1.0f;
		private SensorCategory _sensorCategory;
		private string _sensorModel;
		private float _sensorUnitScale = 1.0f;
		private string _sensorUnits = string.Empty;
		private string _streamCommand;
		private string _streamEndCommand;
		private string _tareCommand;

		private bool _enableAutoStop;
		private volatile bool _thresholdTriggered;
		private int _autoStopDeviceSelection;
		private float? _autoStopReading;
		private readonly ThresholdComparisonType[] _autoStopComparisonTypes;
		private ThresholdComparisonType _autoStopSelectedComparison;
		private bool _useEmergencyStop;
		private decimal _startPos;
		private decimal _endPos = 10000m;
		private decimal _posIncrement = 1000m;
		private double _settleTime = 0.5;
		private decimal? _devicePosition;

		private readonly Dictionary<DeviceIdentity, ICollection<UnitOfMeasure>> _unitCache =
			new Dictionary<DeviceIdentity, ICollection<UnitOfMeasure>>();

		private UnitOfMeasure _selectedUnit = UnitOfMeasure.Data;

		private readonly List<Tuple<decimal, float>> _logData = new List<Tuple<decimal, float>>();
		private volatile bool _doLogging;
		private bool _enableSaveResults;
		private string _lastResultsPath;
		private string _resultsFolder;
		private string _baseFileName;

		#endregion
	}
}

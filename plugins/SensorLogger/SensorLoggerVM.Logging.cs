﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using DataAccess;

namespace Zaber.SensorLogger
{
	public partial class SensorLoggerVM
	{
		#region -- Data logging thread --

		private void LoggerThreadStart()
		{
			if (null != _loggerThread)
			{
				if (_loggerThread.IsAlive)
				{
					throw new InvalidOperationException(
						"Logger reader thread is already running; cannot start another.");
				}

				_loggerThread = null;
			}

			_doLogging = true;

			// Start the logger thread.
			_loggerThread = new Thread(LoggerThread) { Name = "Sensor Logger Data Recording Thread" };

			_loggerThread.Start();
		}


		private void LoggerThreadStop()
		{
			if (null != _loggerThread)
			{
				if (_loggerThread.IsAlive)
				{
					_doLogging = false;
					_loggerThread.Join();
				}

				_loggerThread = null;
			}
		}


		private void LoggerThread()
		{
			_logData.Clear();

			while (_doLogging)
			{
				if (_recordSampleFlag.WaitOne(50))
				{
					var reading = _sensorReading;

					// For final measurement, use the value that actually caused the stop rather than the current value.
					if (_autoStopReading.HasValue)
					{
						reading = _autoStopReading.Value;
					}

					if (_devicePosition.HasValue)
					{
						_logData.Add(new Tuple<decimal, float>(_devicePosition.Value, reading));
					}

					_recordSampleFlag.Reset();
					_resumeMotionFlag.Set();
				}
			}

			// Record data to file here...
			if (EnableSaveResults && CanSaveResults && (_logData.Count > 0))
			{
				var path = GenerateFileName();

				var data = new MutableDataTable();

				var count = _logData.Count();

				var positionColHeader = "Position";
				if (SelectedPositionUnit != UnitOfMeasure.Data)
				{
					positionColHeader += " (" + SelectedPositionUnit.Abbreviation + ")";
				}

				var dataColHeader = "Sensor Reading";
				if (!string.IsNullOrEmpty(SensorUnits))
				{
					dataColHeader += " (" + SensorUnits + ")";
				}

				var positionCol = new Column(positionColHeader, count);
				var sensorCol = new Column(dataColHeader, count);

				var cols = new List<Column>
				{
					positionCol,
					sensorCol
				};

				positionCol.Values = _logData.Select(t => t.Item1.ToString()).ToArray();
				sensorCol.Values = _logData.Select(t => t.Item2.ToString()).ToArray();

				data.Columns = cols.ToArray();

				data.SaveCSV(path);

				_lastResultsPath = path;

				_eventLog.LogEvent("Results exported");
			}

			_experimentInProgress = false;

			_dispatcher.BeginInvoke(() =>
			{
				OnPropertyChanged(nameof(ExperimentInProgress));
				OnPropertyChanged(nameof(OpenLastResultsCommand));
			});
		}

		#endregion
	}
}

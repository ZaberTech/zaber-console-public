﻿using System;
using System.Threading;

namespace Zaber.SensorLogger
{
	public partial class SensorLoggerVM
	{
		#region -- Stage movement logic --

		private void MotionThreadStart()
		{
			if (null != _motionThread)
			{
				if (_motionThread.IsAlive)
				{
					throw new InvalidOperationException(
						"Motion control thread is already running; cannot start another.");
				}

				_motionThread = null;
			}

			if (null == Conversation)
			{
				throw new InvalidOperationException(
					"Cannot start motion control thread without a selected Conversation.");
			}

			_recordSampleFlag.Reset();

			_motionThread = new Thread(MotionThread) { Name = "Sensor Logger Motion Control Thread" };

			_motionThread.Start();
		}


		private void MotionThreadStop()
		{
			if (null != _motionThread)
			{
				_resumeMotionFlag.Set();
				if (_motionThread.IsAlive)
				{
					_motionThread.Abort();
					_motionThread.Join();
				}

				_motionThread = null;
			}
		}


		private void MotionThread()
		{
			// Save a reference to the selected conversation in case the user accidentally selects 
			// another during the experiment.
			var conv = Conversation;

			var nextPos = _startPos;
			var increment = Math.Abs(_posIncrement);
			if (_endPos < _startPos)
			{
				increment = -increment;
			}

			var stepIndex = 0;

			while (_experimentInProgress)
			{
				// Move to the next sample position.
				if (!MoveTo(conv, nextPos, _selectedUnit))
				{
					_experimentInProgress = false;
					break;
				}

				// Wait for the stage to stop moving. Note could be caused by stall or trigger.
				WaitForIdle(conv);

				// Wait the designated settle time.
				WaitForSettle();
				if (!_experimentInProgress)
				{
					break;
				}

				// Get the current device position.
				_devicePosition = GetDevicePosition(conv, _selectedUnit);

				// Tell the data logging thread to record the measurement.
				_recordSampleFlag.Set();

				// Calculate the next povement position.
				stepIndex++;
				var targetPos = _startPos + (stepIndex * increment);
				targetPos = increment >= 0m ? Math.Min(targetPos, _endPos) : Math.Max(targetPos, _endPos);

				// Wait until the sample has been recorded by the logging thread.
				while (_experimentInProgress)
				{
					if (_resumeMotionFlag.WaitOne(100))
					{
						break;
					}

					if ((null == _sensorThread) || !_sensorThread.IsAlive)
					{
						_experimentInProgress = false;
					}
				}

				_resumeMotionFlag.Reset();

				// Set up to advance to the next position.
				if (targetPos == nextPos) // We've reached the end of the move.
				{
					_experimentInProgress = false;
				}

				nextPos = targetPos;
			}

			// If the movement was aborted by a sensor reading, log the last reading.
			if (EnableAutoStop && _thresholdTriggered)
			{
				_devicePosition = GetDevicePosition(conv, _selectedUnit);
				_recordSampleFlag.Set();
				_resumeMotionFlag.WaitOne(500);
			}

			_doLogging = false;
		}


		private static bool MoveTo(Conversation aConversation, decimal aPosition, UnitOfMeasure aUnits)
		{
			try
			{
				if (aConversation.Device.Port.IsAsciiMode)
				{
					var result = aConversation.RequestInUnits("move abs", aPosition, aUnits);
					if (result.IsError)
					{
						return false;
					}
				}
				else
				{
					var result = aConversation.RequestInUnits(Command.MoveAbsolute, aPosition, aUnits);
					if (result.IsError)
					{
						return false;
					}
				}
			}
			catch (ErrorResponseException aException)
			{
				_log.Error("Experiment aborted because the device rejected a movement command.", aException);
				return false;
			}

			return true;
		}


		private static decimal? GetDevicePosition(Conversation aConversation, UnitOfMeasure aUnits)
		{
			decimal? pos = null;
			var rawData = 0m;
			var dataOk = false;

			if (aConversation.Device.Port.IsAsciiMode)
			{
				var result = aConversation.Request("get pos");
				if (!result.IsError)
				{
					rawData = result.NumericData;
					dataOk = true;
				}
			}
			else
			{
				var result = aConversation.Request(Command.ReturnCurrentPosition);
				if (result.IsError)
				{
					rawData = result.NumericData;
					dataOk = true;
				}
			}

			if (dataOk)
			{
				try
				{
					var converted = aConversation.Device.ConvertData(rawData, aUnits);
					pos = converted.Value;
				}
				catch (InvalidOperationException)
				{
				} // Device can't do unit conversions.
			}

			return pos;
		}


		private void WaitForIdle(Conversation aConv)
		{
			var idle = false;
			while (_experimentInProgress && !idle)
			{
				Thread.Sleep(100);
				if (aConv.Device.Port.IsAsciiMode)
				{
					var result = aConv.Request(string.Empty);
					if (result.Body.Contains("IDLE"))
					{
						idle = true;
					}
				}
				else
				{
					var result = aConv.Request(Command.ReturnStatus);
					var numericData = (int) result.NumericData;
					if ((0 == numericData) || (13 == numericData) || (65 == numericData))
					{
						// ^ Idle                ^ Stalled and stopped  ^ Parked
						idle = true;
					}
				}
			}
		}


		private void WaitForSettle() => Thread.Sleep((int) (1000.0 * _settleTime));

		#endregion
	}
}

﻿using System;
using System.Globalization;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Threading;

namespace Zaber.SensorLogger
{
	public partial class SensorLoggerVM
	{
		#region -- Sensor interface thread --

		private void OnConnectClicked()
		{
			if (null != _sensorPort)
			{
				SensorThreadStop();
				CloseSensorPort();
				SensorMessage = string.Empty;
			}
			else
			{
				SensorThreadStart();
			}
		}


		private void SensorThreadStart()
		{
			if (null != _sensorThread)
			{
				if (_sensorThread.IsAlive)
				{
					throw new InvalidOperationException(
						"Sensor reader thread is already running; cannot start another.");
				}

				_sensorThread = null;
			}

			// Empty the command buffer for the sensor thread.
			while (!_sensorThreadCommands.IsEmpty)
			{
				SensorThreadCommand dummy;
				_sensorThreadCommands.TryDequeue(out dummy);
			}

			// Start the sensor thread.
			_sensorThread = new Thread(SensorThread)
			{
				Name = "Sensor Logger Sensor Logging Thread",
				Priority = ThreadPriority.AboveNormal
			};

			_sensorThread.Start();

			ConnectButtonLabel = "Disconnect";
		}


		private void SensorThreadStop()
		{
			if (null != _sensorThread)
			{
				if (_sensorThread.IsAlive)
				{
					_sensorThreadCommands.Enqueue(SensorThreadCommand.Terminate);
					_sensorThread.Abort();
					_sensorThread.Join();
				}

				_sensorThread = null;
			}
		}


		private void CloseSensorPort()
		{
			if (null != _sensorPort)
			{
				_sensorPort.Close();
				_sensorPort.DataReceived -= Sensor_DataReceived;
				_sensorPort = null;
			}

			ConnectButtonLabel = "Connect";
			IsSensorConnected = false;
		}


		// Serial port callback for when data comes up. Batches up lines of text and queues
		// them for the sensor thread to consume. Note this runs on a random system thread.
		private void Sensor_DataReceived(object aSender, SerialDataReceivedEventArgs aArgs)
		{
			var port = (SerialPort) aSender;
			try
			{
				var buffer = new byte[port.ReadBufferSize];
				var result = port.BaseStream.BeginRead(buffer, 0, buffer.Length, null, null);

				try
				{
					var bytesRead = port.BaseStream.EndRead(result);
					for (var i = 0; i < bytesRead; ++i)
					{
						var c = (char) buffer[i];
						if (('\r' == c) || ('\n' == c))
						{
							var s = _sensorMessageBuffer.ToString();
							if (!string.IsNullOrEmpty(s))
							{
								_sensorPortData.Enqueue(s);
							}

							_sensorMessageBuffer.Clear();
						}
						else
						{
							_sensorMessageBuffer.Append(c);
						}
					}
				}
				catch (IOException ioe)
				{
					_log.Error("Encountered an error while reading.", ioe);
				}
			}
			catch (InvalidOperationException)
			{
				_log.Warn("Port closed while reading.");
			}
		}


		// Main thread for reading sensor data.
		private void SensorThread()
		{
			_thresholdTriggered = false;

			// Detect the sensor type.
			if (!DetectSensor())
			{
				_dispatcher.BeginInvoke(() =>
				{
					SensorMessage = "Failed to detect a supported sensor type.";
					IsSensorConnected = false;
					ConnectButtonLabel = "Connect";
				});

				return;
			}

			// Continuously display sensor readings.
			_dispatcher.BeginInvoke(() => { IsSensorConnected = true; });

			StartSensorOutput();
			var terminate = false;
			var displaying = false;
			while (!terminate)
			{
				_sensorThreadCommands.TryDequeue(out var command);

				switch (command)
				{
					case SensorThreadCommand.Terminate:
						StopSensorOutput();
						terminate = true;
						break;

					case SensorThreadCommand.Tare:
						if (null != _tareCommand)
						{
							StopSensorOutput();
							GetSensorResponses(_tareCommand);
							StartSensorOutput();
						}

						break;

					// Normal operation - display sensor readings.
					default:
					{
						string lastMessage = null;

						// Always take the most recent reading and ignore older ones.
						while (_sensorPortData.TryDequeue(out var message))
						{
							lastMessage = message;
						}

						if (null != lastMessage)
						{
							if (float.TryParse(lastMessage.Trim(),
											   NumberStyles.Float,
											   CultureInfo.InvariantCulture,
											   out var value))
							{
								var reading = value * _sensorUnitScale;
								if (_experimentInProgress)
								{
									TestConditions(reading);
								}

								// Always store the new data.
								_sensorReading = reading;

								// But only update the display as fast as the main UI thread can go.
								if (!displaying)
								{
									displaying = true;
									_dispatcher.BeginInvoke(() =>
									{
										OnPropertyChanged(nameof(SensorReading));
										displaying = false;
									});
								}
							}
						}
					}
						break;
				}

				Thread.Sleep(0);
			}

			_dispatcher.BeginInvoke(() => { IsSensorConnected = false; });
		}


		private bool DetectSensor()
		{
			int[] baudRates = { };
			if (AUTO_BAUD_RATE == SelectedBaudRate)
			{
				baudRates = _baudRates;
			}
			else
			{
				if (int.TryParse(SelectedBaudRate, out var dummy))
				{
					baudRates = new[] { dummy };
				}
			}

			foreach (var baudRate in baudRates)
			{
				_dispatcher.BeginInvoke(() => { SensorMessage = $"Detecting sensors at {baudRate} baud..."; });

				try
				{
					_sensorPort = new SerialPort(SelectedPort, baudRate, Parity.None, 8, StopBits.One);
					_sensorPort.Open();
				}
				catch (Exception aException)
				{
					if (null != _sensorPort)
					{
						_sensorPort.Close();
						_sensorPort = null;
					}

					_dispatcher.BeginInvoke(() => { SensorMessage = "Error opening COM port: " + aException.Message; });

					_log.Error($"Failed to open serial port at {baudRate} baud.", aException);
					continue;
				}

				_sensorPort.DataReceived += Sensor_DataReceived;

				if (DetectLoadstarSensors())
				{
					_dispatcher.BeginInvoke(() => { SelectedBaudRate = baudRate.ToString(); });

					return true;
				}

				_sensorPort.DataReceived += Sensor_DataReceived;
				_sensorPort.Close();
				_sensorPort = null;
			}

			_sensorCategory = SensorCategory.Unknown;

			return false;
		}


		private bool DetectLoadstarSensors()
		{
			var responses = GetSensorResponses("\r\n\r\n").ToList();

			// Sometimes two tries are needed due to broken comms in last session.
			if ((responses is null) || !responses.Contains("A")) 
			{
				responses = GetSensorResponses("\r\n\r\n").ToList();
			}

			if ((responses is null) || !responses.Contains("A"))
			{
				return false;
			}

			string capacityCommand = null;
			string unitsCommand = null;
			string capacity = null;
			string unitStr = null;

			// The DI-1000 in particular is flaky about responding to the MODEL command, so
			// we give it a few chances.
			var model = string.Empty;
			var tries = 0;
			var knownModels = new[] { "DI-1000", "DI-100", "iLoad", "DQ-1000U" };
			while (tries < 3)
			{
				var pings = new[] { "MODEL\r", "SS1\r" };
				foreach (var ping in pings)
				{
					foreach (var response in GetSensorResponses(ping))
					{
						foreach (var subStr in knownModels)
						{
							if (response.Contains(subStr))
							{
								model = response;
								tries = 3;
								break;
							}
						}

						if (!string.IsNullOrEmpty(model))
						{
							break;
						}
					}

					if (!string.IsNullOrEmpty(model))
					{
						break;
					}
				}

				++tries;
			}

			if (model.Contains("DI-1000") || model.Contains("DI-100"))
			{
				_streamCommand = "WC\r";
				_streamEndCommand = "\r";
				unitsCommand = "UNITS\r";
				_tareCommand = "TARE\r";
				_sensorUnitScale = 1.0f;
			}
			else if (model.Contains("iLoad") || model.Contains("DQ-1000U"))
			{
				_streamCommand = "O0W0\r";
				_streamEndCommand = "\r";
				_tareCommand = "CT0\r";
				capacityCommand = "SLC\r";
				_sensorUnitScale = 0.001f;
				unitStr = "lb";
			}
			else
			{
				return false;
			}

			_sensorCategory = SensorCategory.Loadstar;
			_sensorModel = model;
			var resultMessage = model;

			if (null != capacityCommand)
			{
				capacity = GetSensorResponses(capacityCommand).FirstOrDefault();
			}

			if (null != unitsCommand)
			{
				unitStr = GetSensorResponses(unitsCommand).FirstOrDefault();
			}


			if (!string.IsNullOrEmpty(capacity))
			{
				if (float.TryParse(capacity, NumberStyles.Float, CultureInfo.InvariantCulture, out var dummy))
				{
					capacity = dummy.ToString();
				}

				resultMessage += " capacity: " + capacity;
				if (!string.IsNullOrEmpty(unitStr))
				{
					resultMessage += unitStr;
				}
			}

			if (!string.IsNullOrEmpty(unitStr))
			{
				_dispatcher.BeginInvoke(() => { SensorUnits = unitStr; });
			}

			_dispatcher.BeginInvoke(() => { SensorMessage = resultMessage; });

			return true;
		}


		private void StartSensorOutput()
		{
			switch (_sensorCategory)
			{
				case SensorCategory.Loadstar:
					if (null != _streamCommand)
					{
						_sensorPort.Write(_streamCommand);
					}

					break;
			}
		}


		private void StopSensorOutput()
		{
			switch (_sensorCategory)
			{
				case SensorCategory.Loadstar:
					if (null != _streamEndCommand)
					{
						GetSensorResponses(_streamEndCommand);
					}

					break;
			}
		}


		// Evaluate automatic stop conditions, if enabled.
		private void TestConditions(float aSensorReading)
		{
			if (!_experimentInProgress || !EnableAutoStop)
			{
				return;
			}

			if (!AutoStopSelectedComparison.Test(aSensorReading, Threshold))
			{
				return;
			}

			// Tell the motion thread to pack it in and record one last sample.
			_thresholdTriggered = true;
			_autoStopReading = aSensorReading;
			_experimentInProgress = false;

			var conv = Conversation;
			if (1 == AutoStopDeviceSelection) // Stop all devices
			{
				conv = PortFacade?.GetConversation(0);
			}

			// Stop the device(s) immediately.
			if (null != conv)
			{
				if (PortFacade.Port.IsAsciiMode)
				{
					conv.Request(UseEmergencyStop ? "estop" : "stop");
				}
				else
				{
					conv.Request(Command.Stop);
				}
			}

			_dispatcher.BeginInvoke(() => { OnPropertyChanged(nameof(ExperimentInProgress)); });
		}

		#endregion
	}
}

﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("SensorLogger")]
[assembly: AssemblyDescription("Simple sensor data logger for Zaber Console")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Zaber Technologies")]
[assembly: AssemblyProduct("SensorLogger")]
[assembly: AssemblyCopyright("Copyright © 2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("c7df4821-6e95-400e-8655-3079d311e1ca")]

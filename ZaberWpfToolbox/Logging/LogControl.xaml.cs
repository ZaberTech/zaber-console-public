﻿namespace ZaberWpfToolbox.Logging
{
	/// <summary>
	///     Custom control for user-visible program logs.
	/// </summary>
	public partial class LogControl
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor. Initializes the component.
		/// </summary>
		public LogControl()
		{
			InitializeComponent();
		}

		#endregion
	}
}

﻿namespace ZaberWpfToolbox.Logging
{
	/// <summary>
	///     Defines a fairly general way for log strings to be passed between
	///     components of Zaber software, regardless of their intended use.
	/// </summary>
	public interface ILoggable
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Append a line of text to the log. Implementations should add a newline.
		/// </summary>
		/// <param name="aMessage">The text to be logged.</param>
		void AppendLine(string aMessage);

		#endregion
	}
}

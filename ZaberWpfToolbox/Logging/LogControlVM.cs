﻿using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace ZaberWpfToolbox.Logging
{
	/// <summary>
	///     ViewModel for a log window. Manages text provided to it and the
	///     relevant buttons on the log window interface for copying/clearing etc.
	/// </summary>
	public class LogControlVM : ObservableObject, ILoggable
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Create a new instance.
		/// </summary>
		public LogControlVM()
		{
			_updateTimer = new DispatcherTimer(DispatcherPriority.Background, Dispatcher.CurrentDispatcher);
			_updateTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
			_updateTimer.Tick += UpdateTimer_Tick;
			_updateTimer.IsEnabled = true;
			MaxLength = DefaultMaxLength;
		}


		/// <summary>
		///     Add a custom event to the end of the log.
		/// </summary>
		/// <param name="aMessage">The text to add.</param>
		/// <remarks>
		///     This method is thread-safe. It does not have to be called
		///     from the user interface thread. Other threads will be blocked until this returns.
		/// </remarks>
		public void Append(string aMessage)
		{
			if (!IsDisabled)
			{
				_incoming.Enqueue(aMessage);
			}
		}


		/// <summary>
		///     ILoggable implementation. Owner should pass text to be appended to
		///     the log here.
		/// </summary>
		/// <param name="aMessage">The new line of text to be appended to the log.</param>
		/// <remarks>
		///     This method is thread-safe. It does not have to be called
		///     from the user interface thread. Other threads will be blocked until this returns.
		/// </remarks>
		public void AppendLine(string aMessage)
		{
			if (!IsDisabled)
			{
				_incoming.Enqueue(aMessage + Environment.NewLine);
			}
		}


		/// <summary>
		///     Maximum length of the log to keep in memory, in characters.
		/// </summary>
		[Description("Maximum size the log will grow to.")]
		public int MaxLength
		{
			get => _maxLength;
			set
			{
				_maxLength = value;
				AppendAndTrimLog("");
			}
		}


		/// <summary>
		///     Gets or sets a flag that disables log output. If true, AppendLine() will do nothing.
		/// </summary>
		public bool IsDisabled { get; set; }

		/// <summary>
		///     Text to be displayed in the upper part of the control.
		/// </summary>
		public string Text
		{
			get => _displayedText;
			set => Set(ref _displayedText, value, nameof(Text));
		}


		/// <summary>
		///     Controls whether the Copy button can be clicked.
		/// </summary>
		public bool CopyButtonEnabled
		{
			get => _copyButtonEnabled;
			set => Set(ref _copyButtonEnabled, value, nameof(CopyButtonEnabled));
		}


		/// <summary>
		///     Controls visibility of all the buttons - set to false
		///     if you just want the log text with no interactivity.
		/// </summary>
		public bool ButtonsVisible
		{
			get => _buttonsVisible;
			set => Set(ref _buttonsVisible, value, nameof(ButtonsVisible));
		}


		/// <summary>
		///     Command invoked by the Copy button in the view.
		/// </summary>
		public ICommand CopyLogCommand => new RelayCommand(_ =>
		{
			if (!string.IsNullOrEmpty(Text))
			{
				Clipboard.SetDataObject(Text, true);
			}
		});


		/// <summary>
		///     Command invoked by the Clear button in the view.
		/// </summary>
		public ICommand ClearLogCommand => new RelayCommand(_ =>
		{
			_clearLog = true;
			CopyButtonEnabled = false;
		});

		#endregion

		#region -- Non-Public Methods --

		// Periodically updates the displayed text and trims old text.
		private void UpdateTimer_Tick(object sender, EventArgs e)
		{
			var newLogText = new StringBuilder();
			string s = null;
			while (_incoming.TryDequeue(out s))
			{
				newLogText.Append(s);
			}

			if (_clearLog)
			{
				Text = string.Empty;
				_clearLog = false;
			}
			else
			{
				AppendAndTrimLog(newLogText.ToString());
			}
		}


		// Remove old text to keep the total quantity within the limit specified by MaxLength.
		private void AppendAndTrimLog(string newLogText)
		{
			var newLength = newLogText.Length + Text.Length;
			if (newLength > _maxLength)
			{
				var currentText = Text + newLogText;
				var minimumRemoved = newLength - ((_maxLength * 9) / 10);
				var countRemoved = currentText.IndexOf('\n', minimumRemoved - 1) + 1;

				if ((-1 == countRemoved) || (currentText.Length == countRemoved))
				{
					countRemoved = minimumRemoved;
				}

				Text = currentText.Substring(countRemoved);
			}
			else
			{
				Text += newLogText;
			}

			var isNotEmpty = !string.IsNullOrEmpty(Text);
			CopyButtonEnabled = isNotEmpty;
		}

		#endregion

		#region -- Data --

		private const int DefaultMaxLength = 10000;
		private readonly DispatcherTimer _updateTimer;

		private int _maxLength = DefaultMaxLength;
		private readonly ConcurrentQueue<string> _incoming = new ConcurrentQueue<string>();
		private volatile bool _clearLog;

		private string _displayedText = string.Empty;
		private bool _copyButtonEnabled;
		private bool _buttonsVisible = true;

		#endregion
	}
}

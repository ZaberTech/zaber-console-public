﻿using System.Windows;
using System.Windows.Controls;

namespace ZaberWpfToolbox
{
	/// <summary>
	///     Base class for custom controls in Zaber programs. This enables creation of new controls
	///     as UserControls in .xaml/.xaml.cs files, with a little editing. See the <see cref="Logging.LogControl" />
	///     for an example. This class exists to ease global application of styles to controls.
	/// </summary>
	public class BaseControl : ContentControl
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Enable global style pass-through.
		/// </summary>
		static BaseControl()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(BaseControl),
													 new FrameworkPropertyMetadata(typeof(BaseControl)));
		}

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;

namespace ZaberWpfToolbox.Settings
{
	/// <summary>
	///     Helper for storing an arbitrary collection of do-not-show-again dialog
	///     settings in the persistent app settings. Allows easy retrieval and
	///     creation of settings by string names, so adding settings for new dialogs
	///     scales automatically.
	/// </summary>
	[Serializable]
	public class DoNotShowAgainSettingsCollection
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Locate an existing preference entry or create a new one if not found.
		/// </summary>
		/// <param name="aDialogIdentifier">Unique name for the preference.</param>
		/// <returns>Existing or default setting for the preference named.</returns>
		public DoNotShowAgainDialogSettings FindOrCreate(string aDialogIdentifier)
		{
			foreach (var setting in DialogSettings)
			{
				if (aDialogIdentifier == setting.SettingName)
				{
					return setting;
				}
			}

			var newSetting = new DoNotShowAgainDialogSettings();
			newSetting.SettingName = aDialogIdentifier;
			DialogSettings.Add(newSetting);
			return newSetting;
		}


		/// <summary>
		///     Reset all do-not-show-again preferences.
		/// </summary>
		public void Clear() => DialogSettings.Clear();


		/// <summary>
		///     List of saved do-not-show preferences.
		/// </summary>
		public List<DoNotShowAgainDialogSettings> DialogSettings { get; set; } =
			new List<DoNotShowAgainDialogSettings>();

		#endregion
	}
}

﻿using System;

namespace ZaberWpfToolbox.Dialogs
{
	/// <summary>
	///     Convenience class for serializing last-used information for file browser
	///     dialogs.
	/// </summary>
	[Serializable]
	public class FileBrowserUserSettings
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     The last directory selected.
		/// </summary>
		public string Directory { get; set; }


		/// <summary>
		///     Filename (without directory) of the last selected file.
		///     Note that only a single filename is supported.
		/// </summary>
		public string Filename { get; set; }


		/// <summary>
		///     Index of the filter extension last used for selecting a file.
		/// </summary>
		public int FilterIndex { get; set; }

		#endregion
	}
}

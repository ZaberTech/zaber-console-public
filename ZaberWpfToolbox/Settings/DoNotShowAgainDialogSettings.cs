﻿using System;

namespace ZaberWpfToolbox.Settings
{
	/// <summary>
	///     User settings class containing the persistent options for a do not show
	///     again dialog. Code displaying such a dialog should provide a reference to an
	///     instance of this class from the persistent application settings.
	/// </summary>
	[Serializable]
	public class DoNotShowAgainDialogSettings
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     A unique identifier for this setting.
		/// </summary>
		public string SettingName { get; set; }

		/// <summary>
		///     Flag indicating whether the dialog should be shown or the last selection
		///     returned without showing anything.
		/// </summary>
		public bool ShowDialog { get; set; } = true;

		/// <summary>
		///     The user's last selection when the dialog was displayed and an option was
		///     chosed while "do not show again" was checked.
		/// </summary>
		public object LastSelection { get; set; }

		#endregion
	}
}

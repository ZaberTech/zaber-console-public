﻿using System.Windows;

namespace ZaberWpfToolbox.Themes
{
	/// <summary>
	///     Contains static resource key names for Zaber UI components such as colors and brushes.
	///     Note the actual values of these resources will vary between programs and between
	///     themes; these names just exist to create a commmon vocabulary for styling Zaber programs.
	/// </summary>
	public class ResourceKeys
	{
		#region -- Public data --

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey
			Color0Key = new ComponentResourceKey(typeof(ResourceKeys), "Color0");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey
			Color1Key = new ComponentResourceKey(typeof(ResourceKeys), "Color1");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey
			Color2Key = new ComponentResourceKey(typeof(ResourceKeys), "Color2");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey
			Color3Key = new ComponentResourceKey(typeof(ResourceKeys), "Color3");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey
			Color4Key = new ComponentResourceKey(typeof(ResourceKeys), "Color4");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey
			Color5Key = new ComponentResourceKey(typeof(ResourceKeys), "Color5");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey
			Color6Key = new ComponentResourceKey(typeof(ResourceKeys), "Color6");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey
			Color7Key = new ComponentResourceKey(typeof(ResourceKeys), "Color7");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey
			Color8Key = new ComponentResourceKey(typeof(ResourceKeys), "Color8");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey
			Color9Key = new ComponentResourceKey(typeof(ResourceKeys), "Color9");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey Color10Key =
			new ComponentResourceKey(typeof(ResourceKeys), "Color10");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey Color11Key =
			new ComponentResourceKey(typeof(ResourceKeys), "Color11");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey Color12Key =
			new ComponentResourceKey(typeof(ResourceKeys), "Color12");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey Color13Key =
			new ComponentResourceKey(typeof(ResourceKeys), "Color13");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey Color14Key =
			new ComponentResourceKey(typeof(ResourceKeys), "Color14");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey Color15Key =
			new ComponentResourceKey(typeof(ResourceKeys), "Color15");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey Color16Key =
			new ComponentResourceKey(typeof(ResourceKeys), "Color16");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey Color17Key =
			new ComponentResourceKey(typeof(ResourceKeys), "Color17");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey Color18Key =
			new ComponentResourceKey(typeof(ResourceKeys), "Color18");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey Color19Key =
			new ComponentResourceKey(typeof(ResourceKeys), "Color19");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey Color20Key =
			new ComponentResourceKey(typeof(ResourceKeys), "Color20");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey Color21Key =
			new ComponentResourceKey(typeof(ResourceKeys), "Color21");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey Color22Key =
			new ComponentResourceKey(typeof(ResourceKeys), "Color22");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey Color23Key =
			new ComponentResourceKey(typeof(ResourceKeys), "Color23");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey Color24Key =
			new ComponentResourceKey(typeof(ResourceKeys), "Color24");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey Color25Key =
			new ComponentResourceKey(typeof(ResourceKeys), "Color25");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey Color26Key =
			new ComponentResourceKey(typeof(ResourceKeys), "Color26");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey Color27Key =
			new ComponentResourceKey(typeof(ResourceKeys), "Color27");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey Color28Key =
			new ComponentResourceKey(typeof(ResourceKeys), "Color28");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey Color29Key =
			new ComponentResourceKey(typeof(ResourceKeys), "Color29");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey Color30Key =
			new ComponentResourceKey(typeof(ResourceKeys), "Color30");

		/// <summary>
		///     Resource key for a unique color value used in user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey Color31Key =
			new ComponentResourceKey(typeof(ResourceKeys), "Color31");

		/// <summary>
		///     Resource key for distinct color value for use in user interface debugging.
		///     This should be mapped to a non-thematic color that is easily spotted against
		///     the other user interface colors. This color key should never be used in production
		///     user interfaces.
		/// </summary>
		public static readonly ComponentResourceKey DebugColorKey =
			new ComponentResourceKey(typeof(ResourceKeys), "DebugColor");

		/// <summary>
		///     Resource key for the brush to use for window foreground elements.
		/// </summary>
		public static readonly ComponentResourceKey ApplicationForegroundBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "ApplicationForegroundBrush");

		/// <summary>
		///     Resource key for the brush to use for window backgrounds.
		/// </summary>
		public static readonly ComponentResourceKey ApplicationBackgroundBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "ApplicationBackgroundBrush");

		/// <summary>
		///     Resource key for the brush to use for window borders.
		/// </summary>
		public static readonly ComponentResourceKey ApplicationBorderBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "ApplicationBorderBrush");

		/// <summary>
		///     Resource key for the brush to use for selection foreground elements.
		/// </summary>
		public static readonly ComponentResourceKey SelectionForegroundBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "SelectionForegroundBrush");

		/// <summary>
		///     Resource key for the brush to use for selection backgrounds.
		/// </summary>
		public static readonly ComponentResourceKey SelectionBackgroundBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "SelectionBackgroundBrush");

		/// <summary>
		///     Resource key for the brush to use for selection borders.
		/// </summary>
		public static readonly ComponentResourceKey SelectionBorderBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "SelectionBorderBrush");

		/// <summary>
		///     Resource key for the brush to use for plugin UI area backgrounds.
		/// </summary>
		public static readonly ComponentResourceKey PluginBackgroundBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "PluginBackgroundBrush");

		/// <summary>
		///     Resource key for the brush to use for plugin UI area borders.
		/// </summary>
		public static readonly ComponentResourceKey PluginBorderBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "PluginBorderBrush");

		/// <summary>
		///     Resource key for the brush to use for general control Foreground elements.
		/// </summary>
		public static readonly ComponentResourceKey ControlForegroundBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "ControlForegroundBrush");

		/// <summary>
		///     Resource key for the brush to use for general control backgrounds.
		/// </summary>
		public static readonly ComponentResourceKey ControlBackgroundBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "ControlBackgroundBrush");

		/// <summary>
		///     Resource key for the brush to use for general control borders.
		/// </summary>
		public static readonly ComponentResourceKey ControlBorderBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "ControlBorderBrush");

		/// <summary>
		///     Resource key for the brush to use for control data area backgrounds.
		/// </summary>
		public static readonly ComponentResourceKey ControlDataAreaBackgroundBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "ControlDataAreaBackgroundBrush");

		/// <summary>
		///     Resource key for the brush to use for control data area borders.
		/// </summary>
		public static readonly ComponentResourceKey ControlDataAreaBorderBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "ControlDataAreaBorderBrush");

		/// <summary>
		///     Resource key for the brush to use for group content area backgrounds.
		/// </summary>
		public static readonly ComponentResourceKey GroupBackgroundBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "GroupBackgroundBrushKey");

		/// <summary>
		///     Resource key for the brush to use for group header area backgrounds.
		/// </summary>
		public static readonly ComponentResourceKey GroupHeaderBackgroundBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "GroupHeaderBackgroundBrushKey");

		/// <summary>
		///     Resource key for the brush to use for default button foreground elements.
		/// </summary>
		public static readonly ComponentResourceKey ButtonForegroundBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "ButtonForegroundBrush");

		/// <summary>
		///     Resource key for the brush to use for default button backgrounds.
		/// </summary>
		public static readonly ComponentResourceKey ButtonBackgroundBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "ButtonBackgroundBrush");

		/// <summary>
		///     Resource key for the brush to use for default button borders.
		/// </summary>
		public static readonly ComponentResourceKey ButtonBorderBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "ButtonBorderBrush");


		/// <summary>
		///     Resource key for the brush to use for highlighted button foreground elements.
		/// </summary>
		public static readonly ComponentResourceKey ButtonHighlightedForegroundBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "ButtonHighlightedForegroundBrush");

		/// <summary>
		///     Resource key for the brush to use for highlighted button backgrounds.
		/// </summary>
		public static readonly ComponentResourceKey ButtonHighlightedBackgroundBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "ButtonHighlightedBackgroundBrush");

		/// <summary>
		///     Resource key for the brush to use for highlighted button borders.
		/// </summary>
		public static readonly ComponentResourceKey ButtonHighlightedBorderBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "ButtonHighlightedBorderBrush");


		/// <summary>
		///     Resource key for the brush to use for disabled button foreground elements.
		/// </summary>
		public static readonly ComponentResourceKey ButtonDisabledForegroundBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "ButtonDisabledForegroundBrush");

		/// <summary>
		///     Resource key for the brush to use for disabled button backgrounds.
		/// </summary>
		public static readonly ComponentResourceKey ButtonDisabledBackgroundBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "ButtonDisabledBackgroundBrush");

		/// <summary>
		///     Resource key for the brush to use for disabled button borders.
		/// </summary>
		public static readonly ComponentResourceKey ButtonDisabledBorderBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "ButtonDisabledBorderBrush");

		/// <summary>
		///     Resource key for the brush to use for unselected tab body borders.
		/// </summary>
		public static readonly ComponentResourceKey TabBorderBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "TabBorderBrush");

		/// <summary>
		///     Resource key for the brush to use for unselected tab header foreground elements.
		/// </summary>
		public static readonly ComponentResourceKey TabHeaderForegroundBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "TabHeaderForegroundBrush");

		/// <summary>
		///     Resource key for the brush to use for unselected tab header backgrounds.
		/// </summary>
		public static readonly ComponentResourceKey TabHeaderBackgroundBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "TabHeaderBackgroundBrush");

		/// <summary>
		///     Resource key for the brush to use for unselected tab header borders.
		/// </summary>
		public static readonly ComponentResourceKey TabHeaderBorderBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "TabHeaderBorderBrush");

		/// <summary>
		///     Resource key for the brush to use for selected tab body borders.
		/// </summary>
		public static readonly ComponentResourceKey TabBorderSelectedBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "TabBorderSelectedBrush");

		/// <summary>
		///     Resource key for the brush to use for selected tab header foregrounds.
		/// </summary>
		public static readonly ComponentResourceKey TabHeaderSelectedForegroundBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "TabHeaderSelectedForegroundBrush");

		/// <summary>
		///     Resource key for the brush to use for selected tab header backgrounds.
		/// </summary>
		public static readonly ComponentResourceKey TabHeaderSelectedBackgroundBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "TabHeaderSelectedBackgroundBrush");

		/// <summary>
		///     Resource key for the brush to use for selected tab header borders.
		/// </summary>
		public static readonly ComponentResourceKey TabHeaderSelectedBorderBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "TabHeaderSelectedBorderBrush");

		/// <summary>
		///     Resource key for the brush to use for ghost control borders.
		/// </summary>
		public static readonly ComponentResourceKey GhostBorderBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "GhostBorderBrush");

		/// <summary>
		///     Resource key for the brush to use for ghost control backgrounds.
		/// </summary>
		public static readonly ComponentResourceKey GhostBackgroundBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "GhostBackgroundBrush");

		/// <summary>
		///     Resource key for the brush to use for progress bar backgrounds.
		/// </summary>
		public static readonly ComponentResourceKey ProgressBackgroundBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "ProgressBackgroundBrush");

		/// <summary>
		///     Resource key for the brush to use for progress bar foreground (the growing part).
		/// </summary>
		public static readonly ComponentResourceKey ProgressForegroundBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "ProgressForegroundBrush");

		/// <summary>
		///     Resource key for the brush to use to highlight errors.
		/// </summary>
		public static readonly ComponentResourceKey ErrorHighlightBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "ErrorHighlightBrush");

		/// <summary>
		///     Resource key for the brush to use to success or pass conditions.
		/// </summary>
		public static readonly ComponentResourceKey SuccessHighlightBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "SuccessHighlightBrush");

		/// <summary>
		///     Resource key for the brush to use to for attention/info icons.
		/// </summary>
		public static readonly ComponentResourceKey InfoHighlightBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "InfoHighlightBrush");

		/// <summary>
		///     Resource key for the brush to paint dropshadows with.
		/// </summary>
		public static readonly ComponentResourceKey DropshadowBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "DropshadowBrush");

		/// <summary>
		///     Resource key for the brush to use for foreground elements in disabled controls.
		/// </summary>
		public static readonly ComponentResourceKey DisabledControlForegroundBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "DisabledControlForegroundBrush");

		/// <summary>
		///     Resource key for the brush to use for disabled control backgrounds.
		/// </summary>
		public static readonly ComponentResourceKey DisabledControlBackgroundBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "DisabledControlBackgroundBrush");

		/// <summary>
		///     Resource key for the brush to use for disabled borders.
		/// </summary>
		public static readonly ComponentResourceKey DisabledControlBorderBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "DisabledControlBorderBrush");

		/// <summary>
		///     Resource key for the brush to use for warning text.
		/// </summary>
		public static readonly ComponentResourceKey WarningTextForegroundBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "WarningTextForegroundBrush");

		/// <summary>
		///     Resource key for the brush to use for debugging.
		/// </summary>
		public static readonly ComponentResourceKey DebugBrushKey =
			new ComponentResourceKey(typeof(ResourceKeys), "DebugBrush");

		#endregion
	}
}

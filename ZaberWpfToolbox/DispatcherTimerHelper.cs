﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Threading;

namespace ZaberWpfToolbox
{
	/// <summary>
	///     Helper class for getting dispatcher timers.
	/// </summary>
	public static class DispatcherTimerHelper
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Instantiate a new timer.
		/// </summary>
		/// <param name="aPriority">Optional timer priority.</param>
		/// <returns>
		///     An instance of <see cref="IDispatcherTimer" />. Under normal operation this
		///     will be a wrapper around the system DispatcherTimer, using the current dispatcher at
		///     time of call. In a test context this may be a test mock of a timer.
		/// </returns>
		public static IDispatcherTimer CreateTimer(DispatcherPriority aPriority = DispatcherPriority.Normal)
		{
			IDispatcherTimer timer = null;

			lock (_timerStack)
			{
				if (_timerStack.Any())
				{
					timer = _timerStack.Pop();
				}
				else
				{
					timer = new StandardDispatcherTimer(aPriority);
				}
			}

			return timer;
		}

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     For use by tests to add a customer timer instance.
		/// </summary>
		/// <param name="aTimer">Instance to add to the stack.</param>
		internal static void PushTimer(IDispatcherTimer aTimer)
		{
			lock (_timerStack)
			{
				_timerStack.Push(aTimer);
			}
		}


		/// <summary>
		///     For use by tests to verify that all pushed customer timers were used.
		///     Clears the stack of custom timer instances.
		/// </summary>
		/// <returns>Any remaining unconsumed customer timers.</returns>
		internal static IEnumerable<IDispatcherTimer> Reset()
		{
			lock (_timerStack)
			{
				var remainder = _timerStack.ToArray();
				_timerStack.Clear();
				return remainder;
			}
		}

		#endregion

		#region -- Data --

		// Empty under normal operation; in a testing context, contains instances created by the tests.
		private static readonly Stack<IDispatcherTimer> _timerStack = new Stack<IDispatcherTimer>();

		#endregion
	}
}

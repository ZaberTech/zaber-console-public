﻿using System.Windows;
using System.Windows.Input;

using Microsoft.Xaml.Behaviors;

namespace ZaberWpfToolbox.Behaviors
{
	/// <summary>
	///     Attach to a UI element to cause it to translate specified inputs into other inputs.
	///     The new inputs get fired from the top level of the view tree.
	///     WPF keyboard handling is a little wonky; keypress events will always generate a
	///     PreviewKeyDown event, but alphanumeric keys will also generate a TextComposition event.
	///     You can only generate a PreviewKeyDown programmatically for navigational keys. If you want
	///     to generate a keyboard event for alphanumeric keys and have the resulting text appear
	///     in a TextBox (for example) you have to generate a TextComposition event instead.
	/// </summary>
	public class RemapInputBehavior : Behavior<UIElement>
	{
		#region -- Public data --

		/// <summary>
		///     Dependency property for the keyboard keypress to be remapped.
		/// </summary>
		public static readonly DependencyProperty FromKeyProperty =
			DependencyProperty.Register("FromKey", typeof(Key), typeof(RemapInputBehavior));


		/// <summary>
		///     Dependency property for the keyboard keypress to remap to. Use this for UI navigation keys.
		/// </summary>
		public static readonly DependencyProperty ToKeyProperty = DependencyProperty.Register(
			"ToKey",
			typeof(Key),
			typeof(RemapInputBehavior));


		/// <summary>
		///     Dependency property for the textual output to remap to. Use this for non-navigational output.
		/// </summary>
		public static readonly DependencyProperty ToTextProperty =
			DependencyProperty.Register("ToText", typeof(string), typeof(RemapInputBehavior));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Gets or sets the keypress to be remapped.
		/// </summary>
		public Key FromKey
		{
			get => (Key) GetValue(FromKeyProperty);
			set => SetValue(FromKeyProperty, value);
		}


		/// <summary>
		///     Gets ot sets the navigational keypress to remap to.
		/// </summary>
		public Key ToKey
		{
			get => (Key) GetValue(ToKeyProperty);
			set => SetValue(ToKeyProperty, value);
		}


		/// <summary>
		///     Gets or sets the textual typing to remap to.
		/// </summary>
		public string ToText
		{
			get => (string) GetValue(ToTextProperty);
			set => SetValue(ToTextProperty, value);
		}

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Invoked automatically when the behavior is attached to a control.
		/// </summary>
		protected override void OnAttached()
		{
			base.OnAttached();

			if (null != AssociatedObject)
			{
				AssociatedObject.PreviewKeyDown += AssociatedObject_PreviewKeyDown;
			}
		}


		/// <summary>
		///     Invoked automatically when the behavior is detached from a control.
		/// </summary>
		protected override void OnDetaching()
		{
			if (null != AssociatedObject)
			{
				AssociatedObject.PreviewKeyDown -= AssociatedObject_PreviewKeyDown;
			}

			base.OnDetaching();
		}


		// Keyboard event handler - performs the remapping.
		private void AssociatedObject_PreviewKeyDown(object aSender, KeyEventArgs aArgs)
		{
			var uielement = (UIElement) aSender;

			// If we're attached to a control and it has keyboard focus...
			if ((null != uielement) && uielement.IsKeyboardFocused)
			{
				// If the keypress matches (note no modifier handling implemented yet)
				if (aArgs.Key == FromKey)
				{
					// Consume the original event.
					aArgs.Handled = true;

					// Generate the output navigational key event if enabled.
					var toKey = ToKey;
					if (Key.None != toKey)
					{
						var args = new KeyEventArgs(Keyboard.PrimaryDevice,
													Keyboard.PrimaryDevice.ActiveSource,
													0,
													toKey);
						args.RoutedEvent = Keyboard.KeyDownEvent;
						InputManager.Current.ProcessInput(args);
					}

					// Generate the output text composition event if enabled.
					var toText = ToText;
					if (!string.IsNullOrEmpty(toText))
					{
						var compo = new TextComposition(InputManager.Current, Keyboard.FocusedElement, toText);
						var eventArgs = new TextCompositionEventArgs(Keyboard.PrimaryDevice, compo);
						eventArgs.RoutedEvent = UIElement.TextInputEvent;
						InputManager.Current.ProcessInput(eventArgs);
					}
				}
			}
		}

		#endregion
	}
}

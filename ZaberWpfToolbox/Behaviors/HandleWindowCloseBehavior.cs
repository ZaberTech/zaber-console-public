﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

using Microsoft.Xaml.Behaviors;

namespace ZaberWpfToolbox.Behaviors
{
	/// <summary>
	///     WPF behavior to enable ViewModels to inercept close attempts on their
	///     associated Windows by translating the events to ICommands.
	/// </summary>
	public class HandleWindowCloseBehavior : Behavior<Window>
	{
		#region -- Public data --

		/// <summary>
		///     Bindable command for the window closing event. If the command can execute when the
		///     closing event is fired, then window closing is allowed and the command will be executed
		///     when the closed event fires. The CanExecute and Execute methods will be passed the
		///     window's DataContext.
		/// </summary>
		public static DependencyProperty ClosingCommandProperty =
			DependencyProperty.RegisterAttached("ClosingCommand",
												typeof(ICommand),
												typeof(HandleWindowCloseBehavior),
												new UIPropertyMetadata(null));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     ICommand to invoke on row double-click.
		/// </summary>
		public ICommand ClosingCommand
		{
			get => (ICommand) GetValue(ClosingCommandProperty);
			set => SetValue(ClosingCommandProperty, value);
		}

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Called by the base class when the Behavior is associated with a control.
		/// </summary>
		protected override void OnAttached()
		{
			base.OnAttached();

			AssociatedObject.Closing += OnWindowClosing;
			AssociatedObject.Closed += OnWindowClosed;
		}


		/// <summary>
		///     Called by the base class when the Behavior is about to be disconnected from its control.
		/// </summary>
		protected override void OnDetaching()
		{
			base.OnDetaching();

			AssociatedObject.Closed -= OnWindowClosed;
			AssociatedObject.Closing -= OnWindowClosing;
		}


		private void OnWindowClosing(object aSender, CancelEventArgs aArgs)
		{
			var cmd = ClosingCommand;
			if (null != cmd)
			{
				if (!cmd.CanExecute(AssociatedObject.DataContext))
				{
					aArgs.Cancel = true;
				}
			}
		}


		private void OnWindowClosed(object aSender, EventArgs aArgs)
			=> ClosingCommand?.Execute(AssociatedObject.DataContext);

		#endregion
	}
}

﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;

namespace ZaberWpfToolbox.Behaviors
{
	/// <summary>
	///     Attached behavior to automatically make URLs in TextBlocks clickable.
	///     Adapted from https://stackoverflow.com/questions/861409/wpf-making-hyperlinks-clickable
	///     This version requires GitHub-Flavored Markdown syntax for links:
	///     "[link text](link URL)"
	/// </summary>
	public static class TextBlockAutoUrlBehavior
	{
		#region -- Public data --

		/// <summary>
		///     Regular expression used to match hyperlinks enclosed in Markdown wrappers.
		/// </summary>
		public static readonly Regex RE_URL =
			new Regex(
				@"\[(.*)\]\(([a-zA-Z]{2,}:\/\/[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b[-a-zA-Z0-9@:%_\+.~#?&//=]*)\)");


		/// <summary>
		///     Attached property for the text to parse for URLs.
		/// </summary>
		public static readonly DependencyProperty SourceTextProperty
			= DependencyProperty.RegisterAttached("SourceText",
												  typeof(string),
												  typeof(TextBlockAutoUrlBehavior),
												  new PropertyMetadata(null, OnTextChanged));


		/// <summary>
		///     Attached property for the enable control - you might want to programmatically
		///     switch this behavior on or off.
		/// </summary>
		public static readonly DependencyProperty EnableUrlParsingProperty = DependencyProperty.RegisterAttached(
			"EnableUrlParsing",
			typeof(bool),
			typeof(TextBlockAutoUrlBehavior),
			new PropertyMetadata(true, OnEnableChanged));


		/// <summary>
		///     Attached property for command to invoke when a URL is clicked.
		/// </summary>
		public static readonly DependencyProperty ClickActionProperty = DependencyProperty.RegisterAttached(
			"UrlClickCommand",
			typeof(ICommand),
			typeof(TextBlockAutoUrlBehavior),
			new PropertyMetadata(null));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Gets the text to be parsed for URLs and displayed.
		/// </summary>
		/// <param name="aElement">The control associated with this behavior.</param>
		/// <returns>The displayed text.</returns>
		public static string GetSourceText(DependencyObject aElement)
			=> aElement.GetValue(SourceTextProperty) as string;


		/// <summary>
		///     Sets the test to be parsed for URLs and displayed.
		/// </summary>
		/// <param name="aElement">The control that will display the text.</param>
		/// <param name="aValue">The new text to display.</param>
		public static void SetSourceText(DependencyObject aElement, string aValue)
			=> aElement.SetValue(SourceTextProperty, aValue);


		/// <summary>
		///     Gets the url parsing enabled state.
		/// </summary>
		/// <param name="aElement">The control associated with this behavior.</param>
		/// <returns>The enabled state of the URL parsing behavior..</returns>
		public static bool GetEnableUrlParsing(DependencyObject aElement)
			=> (bool) aElement.GetValue(EnableUrlParsingProperty);


		/// <summary>
		///     Sets the url parsing enabled state.
		/// </summary>
		/// <param name="aElement">The control that will display the text.</param>
		/// <param name="aValue">True to enable URL parsing, false to display all plain text.</param>
		public static void SetEnableUrlParsing(DependencyObject aElement, bool aValue)
			=> aElement.SetValue(EnableUrlParsingProperty, aValue);


		/// <summary>
		///     Gets the command to be invoked when a URL is clicked.
		/// </summary>
		/// <param name="aElement">The control associated with this behavior.</param>
		/// <returns>The command for clicked URLs.</returns>
		public static ICommand GetUrlClickCommand(DependencyObject aElement)
			=> aElement.GetValue(ClickActionProperty) as ICommand;


		/// <summary>
		///     Sets the command to be invoked when a URL is clicked. If this is not set, URLs will
		///     be launched using the system browser by default.
		/// </summary>
		/// <param name="aElement">The control that will display the text.</param>
		/// <param name="aValue">The new command for URL clicks. The URL will be passed as the command parameter, as a Uri object.</param>
		public static void SetUrlClickCommand(DependencyObject aElement, ICommand aValue)
			=> aElement.SetValue(ClickActionProperty, aValue);

		#endregion

		#region -- Non-Public Methods --

		// Called when the text is changed.
		private static void OnTextChanged(DependencyObject aElement, DependencyPropertyChangedEventArgs aArgs)
		{
			var block = aElement as TextBlock;
			var str = (string) aArgs.NewValue;
			ParseAndDisplay(block, str);
		}


		private static void OnEnableChanged(DependencyObject aElement, DependencyPropertyChangedEventArgs aArgs)
		{
			var block = aElement as TextBlock;
			var str = GetSourceText(aElement);
			ParseAndDisplay(block, str);
		}


		private static void ParseAndDisplay(TextBlock aTextBlock, string aNewText)
		{
			if (aTextBlock is null)
			{
				return;
			}

			// Clear the previous text.
			aTextBlock.Inlines.Clear();

			if (string.IsNullOrEmpty(aNewText))
			{
				return;
			}

			if (GetEnableUrlParsing(aTextBlock))
			{
				// Find all URLs using a regular expression
				var lastPos = 0;
				foreach (Match match in RE_URL.Matches(aNewText))
				{
					// Copy raw string from the last position up to the match
					if (match.Index != lastPos)
					{
						var rawText = aNewText.Substring(lastPos, match.Index - lastPos);
						aTextBlock.Inlines.Add(new Run(rawText));
					}

					// Create a hyperlink for the match
					var link = new Hyperlink(new Run(match.Groups[1].Value))
					{
						NavigateUri = new Uri(match.Groups[2].Value)
					};
					link.Click += OnUrlClick;

					aTextBlock.Inlines.Add(link);

					// Update the last matched position
					lastPos = match.Index + match.Length;
				}

				// Finally, copy the remainder of the string
				if (lastPos < aNewText.Length)
				{
					aTextBlock.Inlines.Add(new Run(aNewText.Substring(lastPos)));
				}
			}
			else
			{
				// URL parsing not enabled - display plain text only.
				aTextBlock.Inlines.Add(new Run(aNewText));
			}
		}


		private static void OnUrlClick(object aSender, RoutedEventArgs aArgs)
		{
			var link = (Hyperlink) aSender;

			ICommand cmd = null;
			var parent = link.Parent as TextBlock;
			if (null != parent)
			{
				cmd = GetUrlClickCommand(parent);
			}

			if (null != cmd)
			{
				if (cmd.CanExecute(link.NavigateUri))
				{
					cmd.Execute(link.NavigateUri);
				}
			}
			else
			{
				Process.Start(link.NavigateUri.ToString());
			}
		}

		#endregion
	}
}

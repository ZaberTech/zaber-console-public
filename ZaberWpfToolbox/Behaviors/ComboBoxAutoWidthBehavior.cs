﻿using System;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace ZaberWpfToolbox.Behaviors
{
	/// <summary>
	///     Attached behavior to make a ComboBox auto-size its width according to its content.
	///     The code for this class was adapted from
	///     http://stackoverflow.com/questions/1034505/how-can-i-make-a-wpf-combo-box-have-the-width-of-its-widest-element-in-xaml
	///     and is not covered by the same license as regular Zaber code. The applicable
	///     license is CC BY-SA 3.0: http://creativecommons.org/licenses/by-sa/3.0/
	/// </summary>
	/// <remarks>
	///     Do not use this behavior in situations where the combo box could become hidden before the
	///     width calculation finishes. For example, do not use this on combo boxes inside tabs, where
	///     the user could possibly click over to another tab is less than half a second. It causes
	///     a NullReferenceException in ComboBox.CoerceIsSelectionBoxHighlighted with no user code
	///     in the call stack if the combo box is hidden before the width update is finished.
	///     It happens more often if the combo box has focus interaction or is the focused element
	///     when the visibility changes.
	///     See http://stackoverflow.com/questions/29306589/crash-in-combobox-coerce-not-my-code
	/// </remarks>
	public static class ComboBoxAutoWidthBehavior
	{
		#region -- Public data --

		/// <summary>
		///     Dependency property for behavior attachment.
		/// </summary>
		public static readonly DependencyProperty ComboBoxWidthFromItemsProperty =
			DependencyProperty.RegisterAttached("ComboBoxWidthFromItems",
												typeof(bool),
												typeof(ComboBoxAutoWidthBehavior),
												new UIPropertyMetadata(false, OnComboBoxWidthFromItemsPropertyChanged));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Getter for the attached property.
		/// </summary>
		/// <param name="aObject">Object for which to retrieve the value.</param>
		/// <returns>The value of the attached property for the given object.</returns>
		public static bool GetComboBoxWidthFromItems(DependencyObject aObject)
			=> (bool) aObject.GetValue(ComboBoxWidthFromItemsProperty);


		/// <summary>
		///     Setter for the attached property. Controls whether or not the behavior is
		///     active for the associated control.
		/// </summary>
		/// <param name="aObject">Object for which to set the value.</param>
		/// <param name="aValue">New value for the property attached to the object. True to enable the behavior.</param>
		public static void SetComboBoxWidthFromItems(DependencyObject aObject, bool aValue)
			=> aObject.SetValue(ComboBoxWidthFromItemsProperty, aValue);

		#endregion

		#region -- Non-Public Methods --

		// Callback for changes to the attached property.
		private static void OnComboBoxWidthFromItemsPropertyChanged(DependencyObject aDependencyObject,
																	DependencyPropertyChangedEventArgs aArgs)
		{
			var comboBox = aDependencyObject as ComboBox;
			if (comboBox != null)
			{
				if ((bool) aArgs.NewValue)
				{
					comboBox.Loaded += OnComboBoxLoaded;
				}
				else
				{
					comboBox.Loaded -= OnComboBoxLoaded;
				}
			}
		}


		// Set initial width when the combobox is first show.
		private static void OnComboBoxLoaded(object aSender, RoutedEventArgs aArgs)
		{
			var comboBox = aSender as ComboBox;
			comboBox.Dispatcher.BeginInvoke(new Action(() => { SetWidthFromItems(comboBox); }));
		}


		// Calculates the new width for the combobox from its content.
		private static void SetWidthFromItems(ComboBox aComboBox)
		{
			var comboBoxWidth = aComboBox.DesiredSize.Width;

			// Create the peer and provider to expand the comboBox in code behind. 
			var peer = new ComboBoxAutomationPeer(aComboBox);
			var provider = (IExpandCollapseProvider) peer.GetPattern(PatternInterface.ExpandCollapse);
			EventHandler eventHandler = null;

			// This event handler will be invoked whenever the items in the combobox change or
			// the drop-down is opened.
			eventHandler = delegate
			{
				if (aComboBox.IsDropDownOpen
				&& (aComboBox.ItemContainerGenerator.Status == GeneratorStatus.ContainersGenerated))
				{
					double width = 0;
					foreach (var item in aComboBox.Items)
					{
						var comboBoxItem = aComboBox.ItemContainerGenerator.ContainerFromItem(item) as ComboBoxItem;
						comboBoxItem.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));

						if (comboBoxItem.DesiredSize.Width > width)
						{
							width = comboBoxItem.DesiredSize.Width;
						}
					}

					aComboBox.Width = comboBoxWidth + width;

					// Remove the event handler. 
					aComboBox.ItemContainerGenerator.StatusChanged -= eventHandler;
					aComboBox.DropDownOpened -= eventHandler;
					provider.Collapse();
				}
			};

			aComboBox.ItemContainerGenerator.StatusChanged += eventHandler;
			aComboBox.DropDownOpened += eventHandler;

			// Expand the comboBox to generate all its ComboBoxItems. 
			provider.Expand();
		}

		#endregion
	}
}

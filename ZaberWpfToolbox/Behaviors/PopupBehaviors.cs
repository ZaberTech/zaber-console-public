﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace ZaberWpfToolbox.Behaviors
{
	/// <summary>
	///     Adds some common functionality to popups: Close when clicked, auto close after
	///     a delay, don't close while mouse hovering.
	/// </summary>
	/// <remarks>
	///     Based on an example by Aviad P.:
	///     http://www.codeproject.com/Articles/44534/Customizing-a-Popup-s-Auto-Close-Behavior
	/// </remarks>
	public static class PopupBehaviors
	{
		#region -- Public data --

		/// <summary>
		///     Dependency property for binding the boolean that enables closing when the popup is clicked.
		/// </summary>
		public static readonly DependencyProperty ClosesWhenClickedProperty =
			DependencyProperty.RegisterAttached("ClosesWhenClicked",
												typeof(bool),
												typeof(PopupBehaviors),
												new UIPropertyMetadata(false, OnClosesOnInputChanged));


		/// <summary>
		///     Dependency property for binding the auto-close delay value.
		/// </summary>
		public static readonly DependencyProperty AutoCloseDelayProperty =
			DependencyProperty.RegisterAttached("AutoCloseDelay",
												typeof(int),
												typeof(PopupBehaviors),
												new UIPropertyMetadata(0, OnAutoCloseDelayChanged));


		/// <summary>
		///     Dependency property for setting which control to focus when the popup closes.
		/// </summary>
		public static readonly DependencyProperty FocusedElementOnCloseProperty =
			DependencyProperty.RegisterAttached("FocusedElementOnClose",
												typeof(UIElement),
												typeof(PopupBehaviors),
												new UIPropertyMetadata(null, FocusedElementOnCloseChanged));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Get the flag indicating whether the popup should close when clicked on.
		/// </summary>
		/// <param name="aElement">The popup element to get the value for.</param>
		/// <returns>True if the popup should close when clicked.</returns>
		public static bool GetClosesWhenClicked(DependencyObject aElement)
			=> (bool) aElement.GetValue(ClosesWhenClickedProperty);


		/// <summary>
		///     Set the flag indicating whether the popup should close when clicked on.
		/// </summary>
		/// <param name="aElement">The popup element to set the value for.</param>
		/// <param name="aValue">False for default behavior or true to close the popup when clicked.</param>
		public static void SetClosesWhenClicked(DependencyObject aElement, bool aValue)
			=> aElement.SetValue(ClosesWhenClickedProperty, aValue);


		/// <summary>
		///     Get the auto-close delay in milliseconds.
		/// </summary>
		/// <param name="aElement">The popup element to get the value for.</param>
		/// <returns>
		///     The current auto-close delay in milliseconds for this popup.
		///     Zero means no automatic close.
		/// </returns>
		public static int GetAutoCloseDelay(DependencyObject aElement)
			=> (int) aElement.GetValue(AutoCloseDelayProperty);


		/// <summary>
		///     Set the popup's automatic close delay in milliseconds.
		/// </summary>
		/// <param name="aElement">The popup element to set the value for.</param>
		/// <param name="aValue">The number of milliseconds to close the popup after, or zero to disable auto-close.</param>
		public static void SetAutoCloseDelay(DependencyObject aElement, int aValue)
			=> aElement.SetValue(AutoCloseDelayProperty, aValue);


		/// <summary>
		///     Get the name of the element to focus when the popup closes.
		/// </summary>
		/// <param name="aElement">The popup element to get the value for.</param>
		/// <returns>The element that should receive focus when the popul closes.</returns>
		public static UIElement GetFocusedElementOnClose(DependencyObject aElement)
			=> (UIElement) aElement.GetValue(FocusedElementOnCloseProperty);


		/// <summary>
		///     Set the element to focus when the popup closes.
		/// </summary>
		/// <param name="aElement">The popup element to get the value for.</param>
		/// <param name="aValue">The element that should receive focus when the popul closes.</param>
		public static void SetFocusedElementOnClose(DependencyObject aElement, UIElement aValue)
			=> aElement.SetValue(FocusedElementOnCloseProperty, aValue);

		#endregion

		#region -- Non-Public Methods --

		// Hook up events when the closes-on-input dependency property is changed.
		private static void OnClosesOnInputChanged(DependencyObject aPopup,
												   DependencyPropertyChangedEventArgs aPropChangedArgs)
		{
			var popup = aPopup as Popup;

			if (null == popup)
			{
				return;
			}

			var value = (bool) aPropChangedArgs.NewValue;
			var oldValue = (bool) aPropChangedArgs.OldValue;

			if (value && !oldValue)
			{
				// Register for the popup's PreviewMouseUp event.
				popup.PreviewMouseUp += Popup_PreviewMouseUp;

				// Obtain the top level element and register to its PreviewKeyUp event
				// using a context object.
				FindOrCreateAssociation(popup);
			}
			else if (!value && oldValue)
			{
				// Unregister event callbacks from the popup.
				popup.PreviewMouseUp -= Popup_PreviewMouseUp;
				popup.Opened -= Popup_Opened;

				// Remove the context object if no longer needed.
				PopupTopLevelContext assoc = null;
				_associations.TryGetValue(popup, out assoc);

				if ((null != assoc) && (null == assoc.Timer))
				{
					// Tell the context object to unregister from the PreviewKeyUp event
					// of the appropriate element.
					_associations[popup].Release();

					// Disassociate the popup from the context object. The context object
					// is now unreferenced and may be garbage collected.
					_associations.Remove(popup);
				}
			}
		}


		// Hook up events when the auto-close delay dependency property is changed.
		private static void OnAutoCloseDelayChanged(DependencyObject aPopup,
													DependencyPropertyChangedEventArgs aPropChangedArgs)
		{
			var popup = aPopup as Popup;

			if (null == popup)
			{
				return;
			}

			var delayValue = GetAutoCloseDelay(popup);
			PopupTopLevelContext association = null;

			// If the delay value is zero, we only want to find an existing association and
			// disable its timer. Otherwise we want to create a new association if there isn't
			// one, and adjust its timer.
			if (delayValue > 0)
			{
				association = FindOrCreateAssociation(popup);
			}
			else
			{
				_associations.TryGetValue(popup, out association);
			}

			if (null != association)
			{
				// If the new delay is zero, disable any existing timer.
				if (0 == delayValue)
				{
					popup.Opened -= Popup_Opened;
					association.Timer = null;

					// We can also remove the context object if the close on click property is not set.
					if (!GetClosesWhenClicked(popup))
					{
						// Tell the context object to unregister from the PreviewKeyUp event
						// of the appropriate element.
						_associations[popup].Release();

						// Disassociate the popup from the context object. The context object
						// is now unreferenced and may be garbage collected.
						_associations.Remove(popup);
					}
				}
				else
				{
					// Replace any existing timer with a new one for the new delay value.
					var timer = new DispatcherTimer(DispatcherPriority.Normal);
					timer.Interval = new TimeSpan(0, 0, 0, 0, delayValue);
					association.Timer = timer;

					// Set up an event handler to start the timer when the popup is opened.
					popup.Opened += Popup_Opened;
				}
			}
		}


		// Hook up events when the focus-on-close dependency property is changed.
		private static void FocusedElementOnCloseChanged(DependencyObject aPopup,
														 DependencyPropertyChangedEventArgs aPropChangedArgs)
		{
			var popup = aPopup as Popup;

			if (null == popup)
			{
				return;
			}

			var value = (UIElement) aPropChangedArgs.NewValue;
			var oldValue = (UIElement) aPropChangedArgs.OldValue;

			if ((null == oldValue) && (null != value))
			{
				popup.Closed += Popup_Closed;
			}
			else if (null == value)
			{
				popup.Closed -= Popup_Closed;
			}
		}


		// Close the popup on mouse click anywhere.
		private static void Popup_PreviewMouseUp(object aSender, MouseButtonEventArgs aArgs)
		{
			var popup = aSender as Popup;
			if (null != popup)
			{
				popup.SetCurrentValue(Popup.IsOpenProperty, false);
			}
		}


		// Start the timer when a popup is opened.
		private static void Popup_Opened(object aSender, EventArgs aEventArgs)
		{
			var senderPopup = aSender as Popup;
			if (null != senderPopup)
			{
				// If there is still an association and it still has a timer, start
				// the timer.
				PopupTopLevelContext assoc = null;
				_associations.TryGetValue(senderPopup, out assoc);
				if (null != assoc)
				{
					if (null != assoc.Timer)
					{
						assoc.Timer.Start();
					}
				}
			}
		}


		// Set focus when the popup closes.
		private static void Popup_Closed(object aPopup, EventArgs aArgs)
		{
			var popup = aPopup as Popup;

			if (null == popup)
			{
				return;
			}

			var focusedElement = GetFocusedElementOnClose(popup);
			if (null != focusedElement)
			{
				focusedElement.Focus();
			}
		}


		// Retrieves or creates a data container for information about the popup.
		private static PopupTopLevelContext FindOrCreateAssociation(Popup aPopup)
		{
			// Find existing data about this popup; create if missing.
			PopupTopLevelContext association = null;
			if (!_associations.TryGetValue(aPopup, out association))
			{
				var topLevelElement = FindContainer(aPopup);
				association = new PopupTopLevelContext(aPopup, topLevelElement);
				_associations[aPopup] = association;

				// Set up an event handler to disassociate the popup from the parent window
				// when unloaded, for great GC justice!
				RoutedEventHandler onUnloaded = null;
				onUnloaded = delegate(object aSender, RoutedEventArgs aEventArgs)
				{
					var senderPopup = aSender as Popup;
					if (null != senderPopup)
					{
						// Tell the context object to unregister from the PreviewKeyUp event
						// of the appropriate element.
						_associations[senderPopup].Release();

						// Disassociate the popup from the context object. The context object
						// is now unreferenced and may be garbage collected.
						_associations.Remove(senderPopup);

						// Remove this event handler.
						aPopup.Unloaded -= onUnloaded;
					}
				};

				aPopup.Unloaded += onUnloaded;
			}

			return association;
		}


		// Helper to find the window that contains the popup so we can intercept all input.
		private static FrameworkElement FindContainer(Popup aPopup)
		{
			FrameworkElement iterator, nextUp = aPopup;
			do
			{
				iterator = nextUp;
				nextUp = VisualTreeHelper.GetParent(iterator) as FrameworkElement;
			} while (nextUp != null);

			return iterator;
		}

		#endregion

		#region -- Data --

		// Helper class for tracking window/popup associations.
		private class PopupTopLevelContext
		{
			#region -- Public Methods & Properties --

			internal PopupTopLevelContext(Popup aPopup, FrameworkElement aContainer)
			{
				_popup = aPopup;
				_container = aContainer;
				aContainer.PreviewKeyUp += Popup_PreviewKeyUp;
			}


			internal DispatcherTimer Timer
			{
				get => _timer;
				set
				{
					if (null != _timer)
					{
						_timer.Stop();
						_timer.Tick -= Timer_Tick;
					}

					_timer = value;

					if (null != _timer)
					{
						_timer.Tick += Timer_Tick;
					}
				}
			}

			#endregion

			#region -- Non-Public Methods --

			internal void Popup_PreviewKeyUp(object aSender, KeyEventArgs aArgs)
				=> _popup.SetCurrentValue(Popup.IsOpenProperty, false);


			internal void Release()
			{
				_container.PreviewKeyUp -= Popup_PreviewKeyUp;
				if (null != _timer)
				{
					_timer.Stop();
					_timer.Tick -= Timer_Tick;
				}
			}


			private void Timer_Tick(object sender, EventArgs e)
			{
				_popup.SetCurrentValue(Popup.IsOpenProperty, false);
				_timer.Stop();
			}

			#endregion

			#region -- Data --

			private readonly Popup _popup;
			private readonly FrameworkElement _container;
			private DispatcherTimer _timer;

			#endregion
		}


		// Map of all active window/popup associations.
		private static readonly Dictionary<Popup, PopupTopLevelContext> _associations =
			new Dictionary<Popup, PopupTopLevelContext>();

		#endregion
	}
}

﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

using Microsoft.Xaml.Behaviors;

namespace ZaberWpfToolbox.Behaviors
{
	/// <summary>
	///     Attachable behavior for text boxes to filter what characters can be typed in by the user.
	///     The code for this class was adapted from
	///     http://stackoverflow.com/questions/1268552/how-do-i-get-a-textbox-to-only-accept-numeric-input-in-wpf
	///     and is not covered by the normal Zaber code license. The applicable
	///     license is CC BY-SA 3.0: http://creativecommons.org/licenses/by-sa/3.0/
	/// </summary>
	public class TextBoxCharacterFilterBehavior : Behavior<TextBox>
	{
		#region -- Public data --

		/// <summary>
		///     Dependency property registration for the regular expression filter.
		/// </summary>
		public static readonly DependencyProperty RegularExpressionProperty =
			DependencyProperty.Register("RegularExpression",
										typeof(string),
										typeof(TextBoxCharacterFilterBehavior),
										new FrameworkPropertyMetadata(".*"));


		/// <summary>
		///     Dependency property registration for maximum string length.
		/// </summary>
		public static readonly DependencyProperty MaxLengthProperty =
			DependencyProperty.Register("MaxLength",
										typeof(int),
										typeof(TextBoxCharacterFilterBehavior),
										new FrameworkPropertyMetadata(int.MinValue));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Regular expression to filter input with. Defaults to ".*" which accepts all characters.
		/// </summary>
		public string RegularExpression
		{
			get => (string) GetValue(RegularExpressionProperty);
			set => SetValue(RegularExpressionProperty, value);
		}

		/// <summary>
		///     Maximum allowed string length. Defaults to a negative number. Negative values allow any length input.
		/// </summary>
		public int MaxLength
		{
			get => (int) GetValue(MaxLengthProperty);
			set => SetValue(MaxLengthProperty, value);
		}

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Attach paste handler when linked to a control.
		/// </summary>
		protected override void OnAttached()
		{
			base.OnAttached();
			AssociatedObject.PreviewTextInput += OnPreviewTextInput;
			DataObject.AddPastingHandler(AssociatedObject, OnPaste);
		}


		/// <summary>
		///     Remove paste handler when unlinked from a control.
		/// </summary>
		protected override void OnDetaching()
		{
			base.OnDetaching();
			AssociatedObject.PreviewTextInput -= OnPreviewTextInput;
			DataObject.RemovePastingHandler(AssociatedObject, OnPaste);
		}


		// Text paste handler.
		private void OnPaste(object aSender, DataObjectPastingEventArgs aArgs)
		{
			if (aArgs.DataObject.GetDataPresent(DataFormats.Text))
			{
				var text = Convert.ToString(aArgs.DataObject.GetData(DataFormats.Text));

				if (!IsValid(text, true))
				{
					aArgs.CancelCommand();
				}
			}
			else
			{
				aArgs.CancelCommand();
			}
		}


		// Keypress handler.
		private void OnPreviewTextInput(object aSender, TextCompositionEventArgs aArgs)
			=> aArgs.Handled = !IsValid(aArgs.Text, false);


		// Apply regular expression filter to text.
		private bool IsValid(string newText, bool paste)
			=> !ExceedsMaxLength(newText, paste) && Regex.IsMatch(newText, RegularExpression);


		// Apply maximum length filter.
		private bool ExceedsMaxLength(string newText, bool paste)
		{
			if (MaxLength < 1)
			{
				return false;
			}

			return LengthOfModifiedText(newText, paste) > MaxLength;
		}


		// Computes what the new length of the text box's content would be after a user edit.
		private int LengthOfModifiedText(string newText, bool paste)
		{
			var countOfSelectedChars = AssociatedObject.SelectedText.Length;
			var caretIndex = AssociatedObject.CaretIndex;
			var text = AssociatedObject.Text;

			if ((countOfSelectedChars > 0) || paste)
			{
				text = text.Remove(caretIndex, countOfSelectedChars);
				return text.Length + newText.Length;
			}

			var insert = Keyboard.IsKeyToggled(Key.Insert);
			return insert && (caretIndex < text.Length) ? text.Length : text.Length + newText.Length;
		}

		#endregion
	}
}

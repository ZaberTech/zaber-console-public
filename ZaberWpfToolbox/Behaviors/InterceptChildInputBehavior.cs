﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Microsoft.Xaml.Behaviors;

namespace ZaberWpfToolbox.Behaviors
{
	/// <summary>
	///     Attach to a UI element to cause its input bindings to take precedence over
	///     those of its child elements.
	/// </summary>
	public class InterceptChildInputBehavior : Behavior<UIElement>
	{
		#region -- Non-Public Methods --

		/// <summary>
		///     Attaches key event handler when the behavior is connected to the control.
		/// </summary>
		protected override void OnAttached()
		{
			base.OnAttached();

			if (null != AssociatedObject)
			{
				AssociatedObject.PreviewKeyDown += AssociatedObject_PreviewKeyDown;
			}
		}


		/// <summary>
		///     Detaches keyboard event handler on control teardown.
		/// </summary>
		protected override void OnDetaching()
		{
			if (null != AssociatedObject)
			{
				AssociatedObject.PreviewKeyDown -= AssociatedObject_PreviewKeyDown;
			}

			base.OnDetaching();
		}


		private void AssociatedObject_PreviewKeyDown(object aSender, KeyEventArgs aArgs)
		{
			var uielement = (UIElement) aSender;

			if (null != uielement)
			{
				var binding = uielement.InputBindings.OfType<KeyBinding>()
									.FirstOrDefault(aKey =>
										{
											if (aKey.Key == aArgs.Key)
											{
												// In the case where the input keystroke is just a modifier key, we
												// needs to guard against aArgs.ModifierKeys being set when aKey.Modifiers
												// is not - inconsistent mapping of modifiers between KeyBindings and KeyEventArgs.
												if (_modiferKeys.Contains(aKey.Key))
												{
													if ((Key.LeftCtrl == aKey.Key) || (Key.RightCtrl == aKey.Key))
													{
														return ModifierKeys.Control == aArgs.KeyboardDevice.Modifiers;
													}

													if ((Key.LeftAlt == aKey.Key) || (Key.RightAlt == aKey.Key))
													{
														return ModifierKeys.Alt == aArgs.KeyboardDevice.Modifiers;
													}

													if ((Key.LeftShift == aKey.Key) || (Key.RightShift == aKey.Key))
													{
														return ModifierKeys.Shift == aArgs.KeyboardDevice.Modifiers;
													}

													if ((Key.LWin == aKey.Key) || (Key.RWin == aKey.Key))
													{
														return ModifierKeys.Windows == aArgs.KeyboardDevice.Modifiers;
													}
												}

												return aKey.Modifiers == aArgs.KeyboardDevice.Modifiers;
											}

											return false;
										});

				if (null != binding)
				{
					aArgs.Handled = true;

					if (binding.Command.CanExecute(binding.CommandParameter))
					{
						binding.Command.Execute(binding.CommandParameter);
					}
				}
			}
		}

		#endregion

		#region -- Data --

		private static readonly HashSet<Key> _modiferKeys = new HashSet<Key>(new[]
		{
			Key.LeftCtrl, Key.LeftAlt,
			Key.LeftShift, Key.LWin,
			Key.RightAlt, Key.RightCtrl,
			Key.RightShift, Key.RWin
		});

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;

namespace ZaberWpfToolbox.Behaviors
{
	/// <summary>
	///     Enables forcing control focus and selecting text in a TextBox without breaking
	///     the MVVM pattern.
	///     Based on examples in the threads
	///     http://stackoverflow.com/questions/2596757/mvvm-how-can-i-select-text-in-a-textbox
	///     and
	///     https://stackoverflow.com/questions/7808974/initial-focus-and-select-all-behavior
	///     which are not covered by the normal Zaber code license. The applicable license
	///     is CC BY-SA 3.0: http://creativecommons.org/licenses/by-sa/3.0/
	/// </summary>
	public static class TextBoxEventHelper
	{
		#region -- Public data --

		/// <summary>
		///     Callback type used in the ITextBoxController interface.
		/// </summary>
		/// <param name="aSender">The ViewModel invoking the callback.</param>
		public delegate void TextBoxEventHandler(ITextBoxController aSender);


		/// <summary>
		///     Interface to be implemented by ViewModels wishing to manipulate a text box.
		/// </summary>
		public interface ITextBoxController
		{
			#region -- Events --

			/// <summary>
			///     Invoke this event to set cursor focus to the attached TextBox.
			/// </summary>
			event TextBoxEventHandler Focus;

			/// <summary>
			///     Invoke this event to select all text in the attached TextBox.
			/// </summary>
			event TextBoxEventHandler SelectAll;

			#endregion
		}


		/// <summary>
		///     Dependency property for attaching the behavior to a TextBox.
		/// </summary>
		public static readonly DependencyProperty TextBoxControllerProperty = DependencyProperty.RegisterAttached(
			"TextBoxController",
			typeof(ITextBoxController),
			typeof(TextBoxEventHelper),
			new FrameworkPropertyMetadata(null, OnTextBoxControllerChanged));


		/// <summary>
		///     Set this attached property to true to cause the text box contents to be selected
		///     when it gets focus.
		/// </summary>
		public static readonly DependencyProperty SelectAllOnFocusProperty = DependencyProperty.RegisterAttached(
			"SelectAllOnFocus",
			typeof(bool),
			typeof(TextBoxEventHelper),
			new PropertyMetadata(false, SelectAllOnFocusPropertyChanged));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Setter for the attached property.
		/// </summary>
		/// <param name="aTextBox">The TextBox for which to Set the property value.</param>
		/// <param name="aController">The ViewModel property value.</param>
		public static void SetTextBoxController(UIElement aTextBox, ITextBoxController aController)
			=> aTextBox.SetValue(TextBoxControllerProperty, aController);


		/// <summary>
		///     Getter for the attached property.
		/// </summary>
		/// <param name="aTextBox">The TextBow for which to get the associated VM.</param>
		/// <returns>The controller (VM) for the TextBox.</returns>
		public static ITextBoxController GetTextBoxController(UIElement aTextBox)
			=> (ITextBoxController) aTextBox.GetValue(TextBoxControllerProperty);


		/// <summary>
		///     Get the SelectAllOnFocus attached property, which controls whether focusing a text
		///     box causes its content to be selected.
		/// </summary>
		/// <param name="aElement">The control to retrieve the value from.</param>
		/// <returns>The current value of the property.</returns>
		[AttachedPropertyBrowsableForChildren(IncludeDescendants = false)]
		[AttachedPropertyBrowsableForType(typeof(TextBox))]
		[AttachedPropertyBrowsableForType(typeof(RichTextBox))]
		public static bool GetSelectAllOnFocus(DependencyObject aElement)
			=> (bool) aElement.GetValue(SelectAllOnFocusProperty);


		/// <summary>
		///     Set the SelectAllOnFocus attached property, which controls whether focusing a text
		///     box causes its content to be selected.
		/// </summary>
		/// <param name="aElement">The control to change the value for.</param>
		/// <param name="aValue">The new value.</param>
		public static void SetSelectAllOnFocus(DependencyObject aElement, bool aValue)
			=> aElement.SetValue(SelectAllOnFocusProperty, aValue);

		#endregion

		#region -- Non-Public Methods --

		// Detaches and attaches event handlers from a controller when it changes.
		private static void OnTextBoxControllerChanged(DependencyObject aTextBox,
													   DependencyPropertyChangedEventArgs aArgs)
		{
			var tb = aTextBox as TextBox;
			if (null == tb)
			{
				throw new ArgumentNullException("TextBoxEventHelper can only be attached to a WPF TextBox.");
			}

			if (aArgs.OldValue is ITextBoxController oldController)
			{
				if (_elements.ContainsKey(oldController))
				{
					_elements.Remove(oldController);
				}

				oldController.SelectAll -= SelectAll;
				oldController.Focus -= Focus;
			}

			if (aArgs.NewValue is ITextBoxController newController)
			{
				_elements[newController] = tb;
				newController.SelectAll += SelectAll;
				newController.Focus += Focus;
			}
		}


		private static void SelectAllOnFocusPropertyChanged(DependencyObject aElement,
															DependencyPropertyChangedEventArgs aArgs)
		{
			if (aElement is UIElement sender)
			{
				if ((bool) aArgs.NewValue)
				{
					sender.GotKeyboardFocus += OnKeyboardFocusSelectText;
					sender.PreviewMouseLeftButtonDown += OnMouseLeftButtonDownSetFocus;
				}
				else
				{
					sender.GotKeyboardFocus -= OnKeyboardFocusSelectText;
					sender.PreviewMouseLeftButtonDown -= OnMouseLeftButtonDownSetFocus;
				}
			}
		}


		// Event handler for focus requests from user code.
		private static void Focus(ITextBoxController aController)
		{
			if (!_elements.TryGetValue(aController, out var tb))
			{
				throw new ArgumentException("Received a Focus event from an unregistered controller.");
			}

			tb.Focus();
		}


		// Event handler for select all requests from user code.
		private static void SelectAll(ITextBoxController aController)
		{
			if (!_elements.TryGetValue(aController, out var tb))
			{
				throw new ArgumentException("Received a Select All event from an unregistered controller.");
			}

			tb.SelectAll();
		}


		private static void OnKeyboardFocusSelectText(object aSender, KeyboardFocusChangedEventArgs aArgs)
		{
			if (aArgs.OriginalSource is TextBoxBase textBox)
			{
				textBox.SelectAll();
			}
		}


		private static void OnMouseLeftButtonDownSetFocus(object aSender, MouseButtonEventArgs aArgs)
		{
			var tb = FindAncestor<TextBoxBase>((DependencyObject) aArgs.OriginalSource);

			if (null == tb)
			{
				return;
			}

			if (!tb.IsKeyboardFocusWithin)
			{
				tb.Focus();
				aArgs.Handled = true;
			}
		}


		private static T FindAncestor<T>(DependencyObject aElement) where T : DependencyObject
		{
			aElement = VisualTreeHelper.GetParent(aElement);

			while (null != aElement)
			{
				if (aElement is T)
				{
					return (T) aElement;
				}

				aElement = VisualTreeHelper.GetParent(aElement);
			}

			;

			return null;
		}

		#endregion

		#region -- Data --

		private static readonly Dictionary<ITextBoxController, TextBox> _elements =
			new Dictionary<ITextBoxController, TextBox>();

		#endregion
	}
}

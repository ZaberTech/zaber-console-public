﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;

using Microsoft.Xaml.Behaviors;

namespace ZaberWpfToolbox.Behaviors
{
	/// <summary>
	///     Attachable behavior to hide a window's close, minimize and maximize buttons.
	///     They are visible by default so use this to hide them without removing
	///     the window title. To remove all decorations from the top of the window including the title,
	///     set its WindowStyle to None instead of using this.
	/// </summary>
	public class WindowHideSystemMenuBehavior : Behavior<Window>
	{
		#region -- Non-Public Methods --

		/// <summary>
		///     Attaches event handlers on control initialization.
		/// </summary>
		protected override void OnAttached()
		{
			base.OnAttached();
			AssociatedObject.Loaded += OnLoaded;
		}


		/// <summary>
		///     Detaches event handlers on control teardown.
		/// </summary>
		protected override void OnDetaching()
		{
			AssociatedObject.Loaded -= OnLoaded;
			base.OnDetaching();
		}


		private void OnLoaded(object aSender, RoutedEventArgs aArgs)
		{
			var hwnd = new WindowInteropHelper(AssociatedObject).Handle;
			SetWindowLong(hwnd, GWL_STYLE, GetWindowLong(hwnd, GWL_STYLE) & ~WS_SYSMENU);
		}


		[DllImport("user32.dll", SetLastError = true)]
		private static extern int GetWindowLong(IntPtr hWnd, int nIndex);


		[DllImport("user32.dll")]
		private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

		#endregion

		#region -- Data --

		// Constants for use with native calls.
		private const int GWL_STYLE = -16;
		private const int WS_SYSMENU = 0x80000; // Close button.

		#endregion
	}
}

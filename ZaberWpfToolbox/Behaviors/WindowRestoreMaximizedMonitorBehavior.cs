﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Forms;

namespace ZaberWpfToolbox.Behaviors
{
	/// <summary>
	///     Attached behavior that will store the name of the window's current monitor in
	///     a string, and on window load restore the window to that monitor if its state
	///     is maximized. Other mechanisms correctly handle restoring the window position
	///     in non-maximized modes.
	/// </summary>
	public static class WindowRestoreMaximizedMonitorBehavior
	{
		#region -- Public data --

		/// <summary>
		///     Attached property to bind to the place to store the monitor name. This location
		///     must be valid when the window is loading and until the window is closing.
		/// </summary>
		public static readonly DependencyProperty MonitorNameProperty = DependencyProperty.RegisterAttached(
			"MonitorName",
			typeof(string),
			typeof(WindowRestoreMaximizedMonitorBehavior),
			new FrameworkPropertyMetadata("Unlikely", OnMonitorNameBindingChanged));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Set the monitor name property.
		/// </summary>
		/// <param name="aSource">The control to set the monitor name property on.</param>
		/// <param name="aValue">The new name value.</param>
		public static void SetMonitorName(DependencyObject aSource, object aValue)
			=> aSource.SetValue(MonitorNameProperty, aValue);


		/// <summary>
		///     Get the monitor name property.
		/// </summary>
		/// <param name="aSource">The control to get the property value from.</param>
		/// <returns>The current value of the monitor name property.</returns>
		public static object GetMonitorName(DependencyObject aSource) => aSource.GetValue(MonitorNameProperty);

		#endregion

		#region -- Non-Public Methods --

		// Called when the value of the monitor name property changes. The "Unlikely" default name
		// exists to ensure this will get hooked up on first startup, and unless Microsoft starts
		// naming monitors that way it should be reliable on subsequent sessions.
		// It would be better if this behavior could be a subclass of Behavior<Window> but that
		// approach did not work.
		private static void OnMonitorNameBindingChanged(DependencyObject aElement,
														DependencyPropertyChangedEventArgs aArgs)
		{
			var w = aElement as Window;
			if ((w != null) && !_windowsAttached.Contains(w))
			{
				w.Loaded += OnWindowLoaded;
				w.LocationChanged += OnWindowMoved;
				_windowsAttached.Add(w);
			}
		}


		// Called when the window is loaded. Restores its previous monitor location if known
		// and if the window state is maximized. Non-maximized restorations are handled fine elsewhere.
		private static void OnWindowLoaded(object aElement, RoutedEventArgs aArgs)
		{
			var w = aElement as Window;
			if ((null != w) && (WindowState.Maximized == w.WindowState))
			{
				var monitorName = GetMonitorName(w) as string;
				if (!string.IsNullOrEmpty(monitorName))
				{
					var myScreen = _screens.SingleOrDefault(s => s.DeviceName == monitorName);
					if (null != myScreen)
					{
						// Save the non-maximized position.
						var oldLeft = w.Left;
						var oldTop = w.Top;

						// Change the window state to normal so we can move it.
						w.WindowState = WindowState.Normal;

						// Move it to the target monitor.
						w.Left = myScreen.WorkingArea.Left + 10;
						w.Top = myScreen.WorkingArea.Top + 10;

						// Make it maximize on the new monitor.
						w.WindowState = WindowState.Maximized;

						// Restore the saved non-maximized position.
						w.Left = oldLeft;
						w.Top = oldTop;
					}
				}
			}
		}


		// Called when the normal-mode window is moved. Determines what monitor it is
		// on and saves its name. 
		// Note this does not work properly in the Unloaded or Closing events, as the
		// window location is always offscreens in those cases. Unfortunate that we have
		// do to so much extra processing on a window drag.
		private static void OnWindowMoved(object sender, EventArgs e)
		{
			var w = sender as Window;
			if ((null != w) && (WindowState.Normal == w.WindowState))
			{
				var myScreen = _screens.SingleOrDefault(s => s.WorkingArea.Contains((int) w.Left, (int) w.Top));
				if (null != myScreen)
				{
					SetMonitorName(w, myScreen.DeviceName);
				}

				// Keep the old screen name if we failed to detect the new one.
			}
		}

		#endregion

		#region -- Data --

		private static readonly IEnumerable<Screen> _screens = Screen.AllScreens.ToList();
		private static readonly HashSet<Window> _windowsAttached = new HashSet<Window>();

		#endregion
	}
}

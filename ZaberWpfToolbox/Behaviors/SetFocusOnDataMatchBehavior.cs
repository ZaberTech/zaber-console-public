﻿using System.Windows;

namespace ZaberWpfToolbox.Behaviors
{
	/// <summary>
	///     Behavior that allows a control to steal focus when two data values match.
	/// </summary>
	public static class SetFocusOnDataMatchBehavior
	{
		#region -- Public data --

		/// <summary>
		///     Bind to an observable property to form the left-hand side of the comparison.
		/// </summary>
		public static readonly DependencyProperty LValueProperty =
			DependencyProperty.RegisterAttached("LValue",
												typeof(object),
												typeof(SetFocusOnDataMatchBehavior),
												new FrameworkPropertyMetadata(OnValueChanged));

		/// <summary>
		///     Bind to an observable property or constant to form the right-hand side of the comparison.
		/// </summary>
		public static readonly DependencyProperty RValueProperty =
			DependencyProperty.RegisterAttached("RValue",
												typeof(object),
												typeof(SetFocusOnDataMatchBehavior),
												new FrameworkPropertyMetadata(OnValueChanged));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Getter for one of the comparison values.
		/// </summary>
		/// <param name="aElement">The control to get the value for.</param>
		/// <returns>Comparison left-hand value.</returns>
		public static object GetLValue(DependencyObject aElement) => aElement.GetValue(LValueProperty);


		/// <summary>
		///     Setter for one of the comparison values.
		/// </summary>
		/// <param name="aElement">The control to get the value for.</param>
		/// <param name="aValue">The new value for the property.</param>
		public static void SetLValue(DependencyObject aElement, object aValue)
			=> aElement.SetValue(LValueProperty, aValue);


		/// <summary>
		///     Getter for one of the comparison values.
		/// </summary>
		/// <param name="aElement">The control to get the value for.</param>
		/// <returns>Comparison right-hand value.</returns>
		public static object GetRValue(DependencyObject aElement) => aElement.GetValue(RValueProperty);


		/// <summary>
		///     Setter for one of the comparison values.
		/// </summary>
		/// <param name="aElement">The control to get the value for.</param>
		/// <param name="aValue">The new value for the property.</param>
		public static void SetRValue(DependencyObject aElement, object aValue)
			=> aElement.SetValue(RValueProperty, aValue);

		#endregion

		#region -- Non-Public Methods --

		// Called when the value of the bound property changes. If the new value is true, 
		// focus is forced to the associated control.
		private static void OnValueChanged(DependencyObject aElement, DependencyPropertyChangedEventArgs aArgs)
		{
			var element = (FrameworkElement) aElement;

			// Fix attachment problems with controls hidden when initially displayed.
			if (!element.IsVisible)
			{
				element.IsVisibleChanged += OnVisibilityChanged;
			}
			else
			{
				var l = GetLValue(aElement);
				var r = GetRValue(aElement);
				if (l == r)
				{
					element.Focus();
				}
			}
		}


		// Called when control visibility changes - ensures focus is set correctly.
		private static void OnVisibilityChanged(object aElement, DependencyPropertyChangedEventArgs aArgs)
		{
			var element = (FrameworkElement) aElement;
			if (element.IsVisible)
			{
				var l = GetLValue(element);
				var r = GetRValue(element);
				if (l == r)
				{
					element.IsVisibleChanged -= OnVisibilityChanged;
					element.Focus();
				}
			}
		}

		#endregion
	}
}

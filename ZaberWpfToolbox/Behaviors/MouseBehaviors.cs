﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

using Microsoft.Xaml.Behaviors;

namespace ZaberWpfToolbox.Behaviors
{
	/// <summary>
	///     Helper to reroute standard mouse/control interaction events to ICommands.
	/// </summary>
	public class MouseBehaviors : Behavior<Panel>
	{
		#region -- Public data --

		/// <summary>
		///     Dependency property for binding mouse button up events.
		/// </summary>
		public static readonly DependencyProperty MouseButtonUpCommandProperty = DependencyProperty.Register(
			"MouseButtonUpCommand",
			typeof(ICommand),
			typeof(MouseBehaviors));

		/// <summary>
		///     Dependency property for binding mouse button down events.
		/// </summary>
		public static readonly DependencyProperty MouseButtonDownCommandProperty = DependencyProperty.Register(
			"MouseButtonDownCommand",
			typeof(ICommand),
			typeof(MouseBehaviors));

		/// <summary>
		///     Dependency property for binding mouse enter events.
		/// </summary>
		public static readonly DependencyProperty MouseEnterCommandProperty = DependencyProperty.Register(
			"MouseEnterCommand",
			typeof(ICommand),
			typeof(MouseBehaviors));

		/// <summary>
		///     Dependency property for binding mouse leave events.
		/// </summary>
		public static readonly DependencyProperty MouseLeaveCommandProperty = DependencyProperty.Register(
			"MouseLeaveCommand",
			typeof(ICommand),
			typeof(MouseBehaviors));

		/// <summary>
		///     Dependency property for binding mouse wheel events.
		/// </summary>
		public static readonly DependencyProperty MouseWheelCommandProperty = DependencyProperty.Register(
			"MouseWheelCommand",
			typeof(ICommand),
			typeof(MouseBehaviors));

		/// <summary>
		///     Dependency property for binding mouse move events.
		/// </summary>
		public static readonly DependencyProperty MouseMoveCommandProperty =
			DependencyProperty.Register("MouseMoveCommand", typeof(ICommand), typeof(MouseBehaviors));

		/// <summary>
		///     Dependency property for binding to the current mouse position.
		/// </summary>
		public static readonly DependencyProperty MousePositionProperty =
			DependencyProperty.Register("MousePosition", typeof(Point), typeof(MouseBehaviors));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Gets or sets the ICommand to be invoked by mouse button up events.
		/// </summary>
		public ICommand MouseButtonUpCommand
		{
			get => (ICommand) GetValue(MouseButtonUpCommandProperty);
			set => SetValue(MouseButtonUpCommandProperty, value);
		}


		/// <summary>
		///     Gets or sets the ICommand to be invoked by mouse button down events.
		/// </summary>
		public ICommand MouseButtonDownCommand
		{
			get => (ICommand) GetValue(MouseButtonDownCommandProperty);
			set => SetValue(MouseButtonDownCommandProperty, value);
		}


		/// <summary>
		///     Gets or sets the ICommand to be invoked by mouse enter events.
		/// </summary>
		public ICommand MouseEnterCommand
		{
			get => (ICommand) GetValue(MouseEnterCommandProperty);
			set => SetValue(MouseEnterCommandProperty, value);
		}


		/// <summary>
		///     Gets or sets the ICommand to be invoked by mouse leave events.
		/// </summary>
		public ICommand MouseLeaveCommand
		{
			get => (ICommand) GetValue(MouseLeaveCommandProperty);
			set => SetValue(MouseLeaveCommandProperty, value);
		}


		/// <summary>
		///     Gets or sets the ICommand to be invoked by mouse wheel events.
		/// </summary>
		public ICommand MouseWheelCommand
		{
			get => (ICommand) GetValue(MouseWheelCommandProperty);
			set => SetValue(MouseWheelCommandProperty, value);
		}


		/// <summary>
		///     Gets or sets the ICommand to be invoked by mouse move events.
		/// </summary>
		public ICommand MouseMoveCommand
		{
			get => (ICommand) GetValue(MouseMoveCommandProperty);
			set => SetValue(MouseMoveCommandProperty, value);
		}


		/// <summary>
		///     Gets or sets the current mouse position relative to the control.
		/// </summary>
		public Point MousePosition
		{
			get => (Point) GetValue(MousePositionProperty);
			set => SetValue(MousePositionProperty, value);
		}

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Sets up mouse event handlers when the control is initialized.
		/// </summary>
		protected override void OnAttached()
		{
			base.OnAttached();

			AssociatedObject.MouseUp += OnControlMouseUp;
			AssociatedObject.MouseDown += OnControlMouseDown;
			AssociatedObject.MouseWheel += OnControlMouseWheel;

			AssociatedObject.MouseEnter += OnControlMouseEnter;
			AssociatedObject.MouseMove += OnControlMouseMove;
			AssociatedObject.MouseLeave += OnControlMouseLeave;
		}


		private void OnControlMouseUp(object aControl, MouseButtonEventArgs aArgs)
		{
			var command = MouseButtonUpCommand;
			if ((null != command) && command.CanExecute(aArgs))
			{
				command.Execute(aArgs);
			}
		}


		private void OnControlMouseDown(object aControl, MouseButtonEventArgs aArgs)
		{
			var command = MouseButtonDownCommand;
			if ((null != command) && command.CanExecute(aArgs))
			{
				command.Execute(aArgs);
			}
		}


		private void OnControlMouseEnter(object aControl, MouseEventArgs aArgs)
		{
			var command = MouseEnterCommand;
			if ((null != command) && command.CanExecute(aArgs))
			{
				command.Execute(aArgs);
			}
		}


		private void OnControlMouseLeave(object aControl, MouseEventArgs aArgs)
		{
			var command = MouseLeaveCommand;
			if ((null != command) && command.CanExecute(aArgs))
			{
				command.Execute(aArgs);
			}
		}


		private void OnControlMouseWheel(object aControl, MouseWheelEventArgs aArgs)
		{
			var command = MouseWheelCommand;
			if ((null != command) && command.CanExecute(aArgs))
			{
				command.Execute(aArgs);
			}
		}


		private void OnControlMouseMove(object aControl, MouseEventArgs aArgs)
		{
			var pos = aArgs.GetPosition(AssociatedObject);
			MousePosition = pos;

			var command = MouseMoveCommand;
			if ((null != command) && command.CanExecute(aArgs))
			{
				command.Execute(aArgs);
			}
		}

		#endregion
	}
}

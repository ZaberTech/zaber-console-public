﻿using System;
using System.Windows.Controls;

using Microsoft.Xaml.Behaviors;

namespace ZaberWpfToolbox.Behaviors
{
	/// <summary>
	///     WPF behavior for ScrollViewers - automatically scrolls to the bottom
	///     when content or layout is changed.
	/// </summary>
	/// <remarks>
	///     The default behavior could be annoying if the user is trying to scroll while
	///     content is being appended to the ScrollViewer. A possible future enhancement is
	///     to disable the autoscrolling if the user manually scrolls, and re-enable it if
	///     the user then scrolls all the way to the bottom.
	///     Code for this class was taken from
	///     http://geoffwebbercross.blogspot.co.uk/2012/04/scrollviewer-autoscroll-behavior.html
	///     and is not covered by the same license as other Zaber code.
	/// </remarks>
	public class AutoScrollBehavior : Behavior<ScrollViewer>
	{
		#region -- Non-Public Methods --

		/// <summary>
		///     Called by the base class when the Behavior is associated with a control.
		/// </summary>
		protected override void OnAttached()
		{
			base.OnAttached();

			_scrollViewer = AssociatedObject;

			if (null != _scrollViewer)
			{
				_scrollViewer.LayoutUpdated += OnLayoutUpdated;
			}
		}


		/// <summary>
		///     Called by the base class when the Behavior is about to be disconnected from its control.
		/// </summary>
		protected override void OnDetaching()
		{
			base.OnDetaching();

			if (null != _scrollViewer)
			{
				_scrollViewer.LayoutUpdated -= OnLayoutUpdated;
			}
		}


		/// <summary>
		///     Called when the ScrollViewer's layout is changed.
		/// </summary>
		/// <param name="aSender">Unused.</param>
		/// <param name="aArgs">Unused.</param>
		private void OnLayoutUpdated(object aSender, EventArgs aArgs)
		{
			if (_scrollViewer.ExtentHeight != _height)
			{
				_scrollViewer.ScrollToVerticalOffset(_scrollViewer.ExtentHeight);
				_height = _scrollViewer.ExtentHeight;
			}
		}

		#endregion

		#region -- Data --

		private ScrollViewer _scrollViewer;
		private double _height;

		#endregion
	}
}

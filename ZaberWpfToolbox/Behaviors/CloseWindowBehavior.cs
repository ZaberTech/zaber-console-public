﻿using System.Windows;

namespace ZaberWpfToolbox.Behaviors
{
	/// <summary>
	///     WPF behavior to enable ViewModels to close their associated Windows by setting
	///     a bound boolean property to true.
	/// </summary>
	/// <remarks>
	///     It's not the best design to have a public boolean property trigger the window
	///     to close when it is set to true, because setting it to false has no meaning.
	///     We might investigate other ways to accomplish this in future.
	///     The code for this class was adapted from
	///     https://adammills.wordpress.com/2009/07/01/window-close-from-xaml/ and
	///     has no specified license. It is not covered by the default Zaber code license.
	/// </remarks>
	public static class CloseWindowBehavior
	{
		#region -- Public data --

		/// <summary>
		///     Dependency property for attaching the behavior to the window.
		/// </summary>
		public static DependencyProperty CloseProperty =
			DependencyProperty.RegisterAttached("Close",
												typeof(bool),
												typeof(CloseWindowBehavior),
												new UIPropertyMetadata(false, OnClose));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Getter for the attached property that triggers closing.
		/// </summary>
		/// <param name="aTarget">The object from whose property tree to rerieve the value.</param>
		/// <returns>True if the close signal has been given by the ViewModel.</returns>
		public static bool GetClose(DependencyObject aTarget) => (bool) aTarget.GetValue(CloseProperty);


		/// <summary>
		///     Setter for the attached property that triggers closing.
		/// </summary>
		/// <param name="aTarget">The object in whose property tree to set the value.</param>
		/// <param name="aValue">Set to true to trigger window close.</param>
		public static void SetClose(DependencyObject aTarget, bool aValue) => aTarget.SetValue(CloseProperty, aValue);

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Called automatically when the bound close signal property changes.
		/// </summary>
		/// <param name="aSender">Unused.</param>
		/// <param name="aArgs">Information about the property change</param>
		private static void OnClose(DependencyObject aSender, DependencyPropertyChangedEventArgs aArgs)
		{
			if (aArgs.NewValue is bool && (bool) aArgs.NewValue)
			{
				var window = GetWindow(aSender);
				window?.Close();
			}
		}


		// Helper to find the Window associated with this behavior.
		private static Window GetWindow(DependencyObject sender)
		{
			Window window = null;

			if (sender is Window)
			{
				window = (Window) sender;
			}

			if (window == null)
			{
				window = Window.GetWindow(sender);
			}

			return window;
		}

		#endregion
	}
}

﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace ZaberWpfToolbox.Behaviors
{
	/// <summary>
	///     Helper class to fix the WPF WebBrowser control so that its source URL
	///     can be bound to a string or URL property.
	///     The code for this class was adapted from
	///     http://stackoverflow.com/questions/263551/databind-the-source-property-of-the-webbrowser-in-wpf
	///     and is not covered by the normal Zaber code license.
	///     The applicable license is CC BY-SA 3.0: http://creativecommons.org/licenses/by-sa/3.0/
	/// </summary>
	public static class WebBrowserBehaviors
	{
		#region -- Public data --

		/// <summary>
		///     Dependency property for binding the URL to the web browser.
		/// </summary>
		public static readonly DependencyProperty BindableSourceProperty =
			DependencyProperty.RegisterAttached("BindableSource",
												typeof(object),
												typeof(WebBrowserBehaviors),
												new UIPropertyMetadata(null, BindableSourcePropertyChanged));


		/// <summary>
		///     Dependency property for intercepting link clicks.
		/// </summary>
		public static readonly DependencyProperty NavigationFilterProperty =
			DependencyProperty.RegisterAttached("NavigationFilter",
												typeof(Predicate<Uri>),
												typeof(WebBrowserBehaviors),
												new UIPropertyMetadata(null, NavigationFilterPropertyChanged));


		/// <summary>
		///     Dependency property for setting custom headers on browse events.
		/// </summary>
		public static readonly DependencyProperty CustomHeadersProperty =
			DependencyProperty.RegisterAttached("CustomHeaders",
												typeof(string),
												typeof(WebBrowserBehaviors),
												new UIPropertyMetadata(null));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Getter for the URL attached property.
		/// </summary>
		/// <param name="aObject">Control for which to retrieve the value.</param>
		/// <returns>The URL value.</returns>
		public static object GetBindableSource(DependencyObject aObject) => aObject.GetValue(BindableSourceProperty);


		/// <summary>
		///     Setter for the URL attached property.
		/// </summary>
		/// <param name="aObject">Control for which to set the data.</param>
		/// <param name="aValue">New URL value.</param>
		public static void SetBindableSource(DependencyObject aObject, object aValue)
			=> aObject.SetValue(BindableSourceProperty, aValue);


		/// <summary>
		///     Getter for the link filter attached property.
		/// </summary>
		/// <param name="aObject">Browser instance for which to get the filter.</param>
		/// <returns>The current link filter.</returns>
		public static Predicate<Uri> GetNavigationFilter(DependencyObject aObject)
			=> aObject.GetValue(NavigationFilterProperty) as Predicate<Uri>;


		/// <summary>
		///     Setter for the link filter attached property.
		/// </summary>
		/// <param name="aObject">Browser instance for which to set the filter.</param>
		/// <param name="aValue">The new filter predicate to use, or null to not filter.</param>
		public static void SetNavigationFilter(DependencyObject aObject, Predicate<Uri> aValue)
			=> aObject.SetValue(NavigationFilterProperty, aValue);


		/// <summary>
		///     Getter for the custom headers attached property.
		/// </summary>
		/// <param name="aObject">Browser instance for which to get headers.</param>
		/// <returns>The current custom headers.</returns>
		public static string GetCustomHeaders(DependencyObject aObject)
			=> aObject.GetValue(CustomHeadersProperty) as string;


		/// <summary>
		///     Setter for the custom headers attached property.
		/// </summary>
		/// <param name="aObject">Browser instance for which to set the headers.</param>
		/// <param name="aValue">The new custom headers, or null to not filter.</param>
		/// <remarks>Each header should end with "\r\n".</remarks>
		public static void SetCustomHeaders(DependencyObject aObject, string aValue)
			=> aObject.SetValue(CustomHeadersProperty, aValue);

		#endregion

		#region -- Non-Public Methods --

		// Pass changes to the URL property value to the web browser control.
		private static void BindableSourcePropertyChanged(DependencyObject aObj,
														  DependencyPropertyChangedEventArgs aArgs)
		{
			if (!(aObj is WebBrowser browser))
			{
				return;
			}

			Uri url = null;

			switch (aArgs.NewValue)
			{
				case string str:
					url = string.IsNullOrWhiteSpace(str) ? null : new Uri(str);
					break;
				case Uri uri:
					url = uri;
					break;
			}

			var headers = GetCustomHeaders(aObj);
			if (!string.IsNullOrEmpty(headers))
			{
				// Enforce that all headers end with a CR-LF.
				var parts = headers.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
				if (parts.Length > 0)
				{
					headers = string.Join("\r\n", parts) + "\r\n";
				}
				else
				{
					headers = null;
				}
			}

			if ((null != url) && !string.IsNullOrEmpty(url.AbsoluteUri))
			{
				browser.Dispatcher.BeginInvoke(new Action(() =>
				{
					browser.Navigate(url, null, null, headers);
				}));
			}
		}


		// Attach a callback for filtering link clicks.
		private static void NavigationFilterPropertyChanged(DependencyObject aObj,
															DependencyPropertyChangedEventArgs aArgs)
		{
			var browser = aObj as WebBrowser;
			if (null == browser)
			{
				return;
			}

			browser.Navigating -= Browser_Navigating;
			if (null != aArgs.NewValue)
			{
				browser.Navigating += Browser_Navigating;
			}
		}


		// Callback for filtering link clicks. Invokes the filter predicate bound by the user.
		private static void Browser_Navigating(object aSender, NavigatingCancelEventArgs aArgs)
		{
			var filter = GetNavigationFilter(aSender as DependencyObject);
			if (null != filter)
			{
				if (!filter(aArgs.Uri))
				{
					aArgs.Cancel = true;
				}
			}
		}

		#endregion
	}
}

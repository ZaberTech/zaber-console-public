﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Documents;

namespace ZaberWpfToolbox.Behaviors
{
	/// <summary>
	///     Attachable behaviors for the Hyperlink text object.
	/// </summary>
	public static class HyperlinkBehaviors
	{
		#region -- Public data --

		/// <summary>
		///     Dependency property to enable the behavior that launches the hyperlink in an external browser.
		///     If set to true, clicking on a Hyperlink will launch its NavigateUri as a system command.
		/// </summary>
		public static readonly DependencyProperty LaunchExternallyProperty = DependencyProperty.RegisterAttached(
			"LaunchExternally",
			typeof(bool),
			typeof(HyperlinkBehaviors),
			new FrameworkPropertyMetadata(false, OnLaunchExternallyChanged));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Getter for LaunchExternally attached property.
		/// </summary>
		/// <param name="aObject">The Hyperlink to get the associated value from.</param>
		/// <returns>The current value of the property for the given link.</returns>
		public static bool GetLaunchExternally(DependencyObject aObject)
			=> (bool) aObject.GetValue(LaunchExternallyProperty);


		/// <summary>
		///     Setter for LaunchExternally attached property.
		/// </summary>
		/// <param name="aObject">The Hyperlink to set the associated value for.</param>
		/// <param name="aValue">The new value of the property.</param>
		public static void SetLaunchExternally(DependencyObject aObject, bool aValue)
			=> aObject.SetValue(LaunchExternallyProperty, aValue);

		#endregion

		#region -- Non-Public Methods --

		private static void OnLaunchExternallyChanged(DependencyObject aDependencyObject,
													  DependencyPropertyChangedEventArgs aArgs)
		{
			var link = aDependencyObject as Hyperlink;
			if (link != null)
			{
				if ((bool) aArgs.NewValue)
				{
					link.Click += OnLinkClicked;
				}
				else
				{
					link.Click -= OnLinkClicked;
				}
			}
		}


		private static void OnLinkClicked(object sender, RoutedEventArgs e)
		{
			var link = sender as Hyperlink;
			if ((null != link) && GetLaunchExternally(link))
			{
				var cmd = link.NavigateUri.ToString();
				if (!string.IsNullOrEmpty(cmd))
				{
					Process.Start(cmd);
				}
			}
		}

		#endregion
	}
}

﻿using System.Windows;

namespace ZaberWpfToolbox.Behaviors
{
	/// <summary>
	///     Behavior that enables forcing control focus to the control by setting a boolean to true,
	///     and tracking whether or not that control has focus by receiving changes to that boolean property.
	/// </summary>
	/// <remarks>
	///     Adapted from an example found in the thread:
	///     http://stackoverflow.com/questions/1356045/set-focus-on-textbox-in-wpf-from-view-model-c
	///     which is covered by the CC BY-SA 3.0 license: http://creativecommons.org/licenses/by-sa/3.0/
	/// </remarks>
	public static class SetFocusBehavior
	{
		#region -- Public data --

		/// <summary>
		///     Bind HasFocus to an observable boolean property on your VM to be able to force or check
		///     whether the attached control has focus.
		/// </summary>
		public static readonly DependencyProperty HasFocusProperty =
			DependencyProperty.RegisterAttached("HasFocus",
												typeof(bool?),
												typeof(SetFocusBehavior),
												new FrameworkPropertyMetadata(HasFocusChanged));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Getter for the current focus state.
		/// </summary>
		/// <param name="aElement">The control to get the value for.</param>
		/// <returns>Whether or not the control has focus.</returns>
		public static bool? GetHasFocus(DependencyObject aElement) => (bool?) aElement.GetValue(HasFocusProperty);


		/// <summary>
		///     Set whether the control has focus.
		/// </summary>
		/// <param name="aElement">The control to change the value for.</param>
		/// <param name="aNewValue">The new value - true to force focus.</param>
		public static void SetHasFocus(DependencyObject aElement, bool? aNewValue)
			=> aElement.SetValue(HasFocusProperty, aNewValue);

		#endregion

		#region -- Non-Public Methods --

		// Called when the value of the bound property changes. If the new value is true, 
		// focus is forced to the associated control.
		private static void HasFocusChanged(DependencyObject aElement, DependencyPropertyChangedEventArgs aArgs)
		{
			var element = (FrameworkElement) aElement;

			// Attach callbacks if we haven't seen this control before.
			// This is why the dependency property is a nullable bool.
			if (null == aArgs.OldValue)
			{
				element.GotFocus += FrameworkElement_GotFocus;
				element.LostFocus += FrameworkElement_LostFocus;
			}

			// Fix attachment problems with controls hidden when initially displayed.
			if (!element.IsVisible)
			{
				element.IsVisibleChanged += OnVisibilityChanged;
			}

			// Focus the control if the new value is true.
			if ((bool) aArgs.NewValue)
			{
				element.Focus();
			}
		}


		// Called when control visibility changes - ensures focus is set correctly.
		private static void OnVisibilityChanged(object aElement, DependencyPropertyChangedEventArgs aArgs)
		{
			var element = (FrameworkElement) aElement;
			if (element.IsVisible && (bool) element.GetValue(HasFocusProperty))
			{
				element.IsVisibleChanged -= OnVisibilityChanged;
				element.Focus();
			}
		}


		// Pass user-triggered focus changes back to the bound property.
		private static void FrameworkElement_GotFocus(object aElement, RoutedEventArgs aArgs)
			=> ((FrameworkElement) aElement).SetValue(HasFocusProperty, true);


		// Pass user-triggered focus changes back to the bound property.
		private static void FrameworkElement_LostFocus(object aElement, RoutedEventArgs aArgs)
			=> ((FrameworkElement) aElement).SetValue(HasFocusProperty, false);

		#endregion
	}
}

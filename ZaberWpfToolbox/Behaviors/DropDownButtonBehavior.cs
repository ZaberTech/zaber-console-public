﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

using Microsoft.Xaml.Behaviors;

namespace ZaberWpfToolbox.Behaviors
{

	/// <summary>
	///		Attached behavior that causes a button to open its context menu when left-clicked.
	/// </summary>
	/// <remarks>Adapted from an answer in this thread: https://stackoverflow.com/questions/8958946/how-to-open-a-popup-menu-when-a-button-is-clicked</remarks>
	public class DropDownButtonBehavior : Behavior<Button>
	{
		/// <summary>
		/// Dependency property for setting menu placement.
		/// </summary>
		public static DependencyProperty PlacementProperty = DependencyProperty.RegisterAttached(
			"Placement",
			typeof(PlacementMode),
			typeof(DropDownButtonBehavior),
			new PropertyMetadata(PlacementMode.Right));


		/// <summary>
		/// Instance property for menu placement.
		/// </summary>
		public PlacementMode Placement
		{
			get => (PlacementMode)GetValue(PlacementProperty);
			set => SetValue(PlacementProperty, value);
		}


		/// <inheritdoc cref="Behavior.OnAttached"/>
		protected override void OnAttached()
		{
			base.OnAttached();

			AssociatedObject.AddHandler(ButtonBase.ClickEvent, new RoutedEventHandler(AssociatedButton_Click), true);
		}


		/// <inheritdoc cref="Behavior.OnDetaching"/>
		protected override void OnDetaching()
		{
			base.OnDetaching();
			AssociatedObject.RemoveHandler(ButtonBase.ClickEvent, new RoutedEventHandler(AssociatedButton_Click));
		}


		private void AssociatedButton_Click(object aSender, RoutedEventArgs aArgs)
		{
			if ((aSender is Button button) && (button?.ContextMenu != null) && !_isMenuOpen)
			{
				button.ContextMenu.AddHandler(ContextMenu.ClosedEvent, new RoutedEventHandler(ContextMenu_Closed), true);

				button.ContextMenu.PlacementTarget = button;
				button.ContextMenu.Placement = Placement;
				button.ContextMenu.IsOpen = true;
				_isMenuOpen = true;
			}
		}


		private void ContextMenu_Closed(object aSender, RoutedEventArgs aArgs)
		{
			_isMenuOpen = false;
			if (aSender is ContextMenu menu)
			{
				menu.RemoveHandler(ContextMenu.ClosedEvent, new RoutedEventHandler(ContextMenu_Closed));
			}
		}


		private bool _isMenuOpen;
	}
}
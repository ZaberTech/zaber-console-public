﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;

// Adapted from http://stackoverflow.com/questions/320089/how-do-i-bind-a-wpf-datagrid-to-a-variable-number-of-columns

namespace ZaberWpfToolbox.Behaviors
{
	/// <summary>
	///     Attachable behavior to enable data-binding the columns of a DataGrid to a collection.
	/// </summary>
	public class DataGridBindColumnsBehavior
	{
		#region -- Public data --

		/// <summary>
		///     Dependency property for the binding.
		/// </summary>
		public static readonly DependencyProperty BindableColumnsProperty =
			DependencyProperty.RegisterAttached("BindableColumns",
												typeof(ObservableCollection<DataGridColumn>),
												typeof(DataGridBindColumnsBehavior),
												new UIPropertyMetadata(null, OnBindableColumnsPropertyChanged));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Setter for the column collection.
		/// </summary>
		/// <param name="aElement">The DataGrid to change the column collection of.</param>
		/// <param name="aNewValue">The new columns.</param>
		public static void SetBindableColumns(DependencyObject aElement, ObservableCollection<DataGridColumn> aNewValue)
			=> aElement.SetValue(BindableColumnsProperty, aNewValue);


		/// <summary>
		///     Getter for the column collection.
		/// </summary>
		/// <param name="aElement">The DataGrid to get the columns from.</param>
		/// <returns>The current column collection.</returns>
		public static ObservableCollection<DataGridColumn> GetBindableColumns(DependencyObject aElement)
			=> (ObservableCollection<DataGridColumn>) aElement.GetValue(BindableColumnsProperty);

		#endregion

		#region -- Non-Public Methods --

		// Invoked when the column collection itself is changed.
		private static void OnBindableColumnsPropertyChanged(DependencyObject aElement,
															 DependencyPropertyChangedEventArgs aArgs)
		{
			var dataGrid = aElement as DataGrid;
			var columns = aArgs.NewValue as ObservableCollection<DataGridColumn>;

			// Repopulate datagrid columns from new collection.
			dataGrid.Columns.Clear();
			if (null == columns)
			{
				return;
			}

			foreach (var column in columns)
			{
				dataGrid.Columns.Add(column);
			}

			// Attach an event handler for collection content changes.
			columns.CollectionChanged += (aCollection, aChangeArgs) =>
			{
				var ne = aChangeArgs;

				if (ne.Action == NotifyCollectionChangedAction.Reset)
				{
					dataGrid.Columns.Clear();
					foreach (DataGridColumn column in ne.NewItems)
					{
						dataGrid.Columns.Add(column);
					}
				}
				else if (ne.Action == NotifyCollectionChangedAction.Add)
				{
					foreach (DataGridColumn column in ne.NewItems)
					{
						dataGrid.Columns.Add(column);
					}
				}
				else if (ne.Action == NotifyCollectionChangedAction.Move)
				{
					dataGrid.Columns.Move(ne.OldStartingIndex, ne.NewStartingIndex);
				}
				else if (ne.Action == NotifyCollectionChangedAction.Remove)
				{
					foreach (DataGridColumn column in ne.OldItems)
					{
						dataGrid.Columns.Remove(column);
					}
				}
				else if (ne.Action == NotifyCollectionChangedAction.Replace)
				{
					dataGrid.Columns[ne.NewStartingIndex] = ne.NewItems[0] as DataGridColumn;
				}
			};
		}

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace ZaberWpfToolbox.Behaviors
{
	/// <summary>
	///     Enables ViewModels to invoke JavaScript functions in the document displayed
	///     by a WebBrowser, without breaking the MVVM dependency pattern.
	/// </summary>
	public static class WebBrowserEvalBehavior
	{
		#region -- Public data --

		/// <summary>
		///     Callback type used in the controller interface.
		/// </summary>
		/// <param name="aController">The ViewModel invoking the callback.</param>
		/// <param name="aFunctionName">Name of the JavaScript function to invoke.</param>
		/// <param name="aArguments">Positional arguments to pass to the function.</param>
		public delegate void BrowserEvalEventHandler(IWebBrowserController aController, string aFunctionName,
													 object[] aArguments);


		/// <summary>
		///     Interface to be implemented by ViewModels wishing to invoke JavaScript functions.
		/// </summary>
		public interface IWebBrowserController
		{
			#region -- Events --

			/// <summary>
			///     Invoke this event trigger evaluation of a JavaScript function.
			/// </summary>
			event BrowserEvalEventHandler EvalTrigger;

			#endregion
		}


		/// <summary>
		///     Dependency property for attaching the behavior to a WebBrowser.
		/// </summary>
		public static readonly DependencyProperty EvalControllerProperty = DependencyProperty.RegisterAttached(
			"EvalController",
			typeof(IWebBrowserController),
			typeof(WebBrowserEvalBehavior),
			new FrameworkPropertyMetadata(null, OnControllerChanged));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Setter for the attached property.
		/// </summary>
		/// <param name="aBrowser">The WebBrowser for which to Set the property value.</param>
		/// <param name="aController">The ViewModel property value.</param>
		public static void SetEvalController(UIElement aBrowser, IWebBrowserController aController)
			=> aBrowser.SetValue(EvalControllerProperty, aController);


		/// <summary>
		///     Getter for the attached property.
		/// </summary>
		/// <param name="aBrowser">The WebBrowser for which to get the associated VM.</param>
		/// <returns>The controller (VM) for the TextBox.</returns>
		public static IWebBrowserController GetEvalController(UIElement aBrowser)
			=> (IWebBrowserController) aBrowser.GetValue(EvalControllerProperty);

		#endregion

		#region -- Non-Public Methods --

		// Detaches and attaches event handlers from a controller when it changes.
		private static void OnControllerChanged(DependencyObject aBrowser, DependencyPropertyChangedEventArgs aArgs)
		{
			var browser = aBrowser as WebBrowser;
			if (null == browser)
			{
				throw new ArgumentNullException(nameof(WebBrowserEvalBehavior)
											+ " can only be attached to a WebBrowser control.");
			}

			if (aArgs.OldValue is IWebBrowserController oldController)
			{
				if (_controllerToBrowserMap.ContainsKey(oldController))
				{
					_controllerToBrowserMap.Remove(oldController);
				}

				oldController.EvalTrigger -= OnEvalTriggered;
			}

			if (aArgs.NewValue is IWebBrowserController newController)
			{
				_controllerToBrowserMap[newController] = browser;
				newController.EvalTrigger += OnEvalTriggered;
			}
		}


		private static void OnEvalTriggered(IWebBrowserController aController, string aFunctionName,
											object[] aArguments)
		{
			if (!_controllerToBrowserMap.TryGetValue(aController, out var browser))
			{
				throw new ArgumentException("Received an Eval event from an unregistered controller.");
			}

			browser.InvokeScript(aFunctionName, aArguments);
		}

		#endregion

		#region -- Data --

		private static readonly Dictionary<IWebBrowserController, WebBrowser> _controllerToBrowserMap =
			new Dictionary<IWebBrowserController, WebBrowser>();

		#endregion
	}
}

﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

using Microsoft.Xaml.Behaviors;

namespace ZaberWpfToolbox.Behaviors
{
	/// <summary>
	///     DataGrid behavior that adds a double-click ICommand callback for rows.
	/// </summary>
	public class DataGridRowDoubleClickBehavior : Behavior<DataGrid>
	{
		#region -- Public data --

		/// <summary>
		///     Dependency property registration for the ICommand to invoke. The ICommane will
		///     be passed the row's DataContext.
		/// </summary>
		public static readonly DependencyProperty DoubleClickCommandProperty =
			DependencyProperty.Register("DoubleClickCommand", typeof(ICommand), typeof(DataGridRowDoubleClickBehavior));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     ICommand to invoke on row double-click.
		/// </summary>
		public ICommand DoubleClickCommand
		{
			get => (ICommand) GetValue(DoubleClickCommandProperty);
			set => SetValue(DoubleClickCommandProperty, value);
		}

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Called by the base class when the Behavior is associated with a control.
		/// </summary>
		protected override void OnAttached()
		{
			base.OnAttached();

			var dataGrid = AssociatedObject;

			if (null != dataGrid)
			{
				dataGrid.LoadingRow += OnRowAdded;
				dataGrid.UnloadingRow += OnRowRemoved;
			}
		}


		/// <summary>
		///     Called by the base class when the Behavior is about to be disconnected from its control.
		/// </summary>
		protected override void OnDetaching()
		{
			base.OnDetaching();

			var dataGrid = AssociatedObject;

			if (null != dataGrid)
			{
				dataGrid.LoadingRow -= OnRowAdded;
				dataGrid.UnloadingRow -= OnRowRemoved;
			}
		}


		// Callback invoked by the datagrid when a row is added.
		private void OnRowAdded(object aSender, DataGridRowEventArgs aArgs)
			=> aArgs.Row.MouseDoubleClick += OnRowDoubleClick;


		// Callback invoked by the datagrid when a row is removed.
		private void OnRowRemoved(object aSender, DataGridRowEventArgs aArgs)
			=> aArgs.Row.MouseDoubleClick -= OnRowDoubleClick;


		// Actual row double click handler - invokes command.
		private void OnRowDoubleClick(object aSender, MouseButtonEventArgs aArgs)
		{
			var command = DoubleClickCommand;

			if (null != command)
			{
				var row = aSender as DataGridRow;
				var vm = row?.DataContext;

				if (command.CanExecute(vm))
				{
					command.Execute(vm);
				}
			}
		}

		#endregion
	}
}

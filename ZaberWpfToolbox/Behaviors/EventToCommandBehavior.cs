﻿using System;
using System.Reflection;
using System.Windows;
using System.Windows.Input;

using Microsoft.Xaml.Behaviors;

namespace ZaberWpfToolbox.Behaviors
{
	/// <summary>
	///     Behavior that reroutes a UI event to an ICommand invocation.
	///     Based on an example from
	///     http://stackoverflow.com/questions/6205472/mvvm-passing-eventargs-as-command-parameter
	/// </summary>
	public class EventToCommandBehavior : Behavior<FrameworkElement>
	{
		#region -- Public data --

		/// <summary>
		///     Dependency property for the name of the event to be rerouted.
		/// </summary>
		public static readonly DependencyProperty EventProperty
			= DependencyProperty.Register("Event",
										  typeof(string),
										  typeof(EventToCommandBehavior),
										  new PropertyMetadata(null, OnEventChanged));


		/// <summary>
		///     Dependency property for the ICommand to be invoked when the event happens.
		/// </summary>
		public static readonly DependencyProperty CommandProperty
			= DependencyProperty.Register("Command",
										  typeof(ICommand),
										  typeof(EventToCommandBehavior),
										  new PropertyMetadata(null));


		/// <summary>
		///     Dependency property for a flag that controls whether arguments are passed to the ICommand.
		/// </summary>
		public static readonly DependencyProperty PassArgumentsProperty
			= DependencyProperty.Register("PassArguments",
										  typeof(bool),
										  typeof(EventToCommandBehavior),
										  new PropertyMetadata(false));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Gets ot sets the name of the event to be rerouted.
		/// </summary>
		public string Event
		{
			get => (string) GetValue(EventProperty);
			set => SetValue(EventProperty, value);
		}


		/// <summary>
		///     Gets or sets the ICommand to be invoked by the event.
		/// </summary>
		public ICommand Command
		{
			get => (ICommand) GetValue(CommandProperty);
			set => SetValue(CommandProperty, value);
		}


		/// <summary>
		///     Gets ot sets the flag indicating whether to pass arguments to the ICommand.
		/// </summary>
		public bool PassArguments
		{
			get => (bool) GetValue(PassArgumentsProperty);
			set => SetValue(PassArgumentsProperty, value);
		}

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Attach event handler when controls are set up.
		/// </summary>
		protected override void OnAttached() => AttachHandler(Event);


		// Reattach event handler when the event name changes.
		private static void OnEventChanged(DependencyObject aBehavior, DependencyPropertyChangedEventArgs aArgs)
		{
			var beh = (EventToCommandBehavior) aBehavior;

			if ((null != beh) && (null != beh.AssociatedObject))
			{
				beh.AttachHandler((string) aArgs.NewValue);
			}
		}


		// Attaches the handler to the event
		private void AttachHandler(string aEventName)
		{
			// Remove old event handler if any.
			if (null != _oldEvent)
			{
				_oldEvent.RemoveEventHandler(AssociatedObject, _eventHandler);
			}

			// Attach new event handler.
			if (!string.IsNullOrEmpty(aEventName))
			{
				var ei = AssociatedObject.GetType().GetEvent(aEventName);
				if (null != ei)
				{
					var mi = GetType().GetMethod("ExecuteCommand", BindingFlags.Instance | BindingFlags.NonPublic);
					_eventHandler = Delegate.CreateDelegate(ei.EventHandlerType, this, mi);
					ei.AddEventHandler(AssociatedObject, _eventHandler);
					_oldEvent = ei;
				}
				else
				{
					throw new ArgumentException(string.Format("There is no event named '{0}' on type '{1}'",
															  aEventName,
															  AssociatedObject.GetType().Name));
				}
			}
		}


		// Execute the ICommand when the event occurs.
		private void ExecuteCommand(object aSender, EventArgs aArgs)
		{
			object parameter = PassArguments ? aArgs : null;
			if ((null != Command) && Command.CanExecute(parameter))
			{
				Command.Execute(parameter);
			}
		}

		#endregion

		#region -- Data --

		private Delegate _eventHandler;
		private EventInfo _oldEvent;

		#endregion
	}
}

Code under the MvvmDialogs directory is adapted from Mark J. Feldman's article 
"Implementing Dialog Boxes in MVVM" located at: 
http://www.codeproject.com/Articles/820324/Implementing-Dialog-Boxes-in-MVVM

The original code and this derivative are distributed under the Code Project
Open License (CPOL) 1.0: http://www.codeproject.com/info/cpol10.aspx


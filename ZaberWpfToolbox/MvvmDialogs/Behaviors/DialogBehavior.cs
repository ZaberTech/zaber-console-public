﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using MvvmDialogs.Presenters;
using MvvmDialogs.ViewModels;

namespace MvvmDialogs.Behaviors
{
	/// <summary>
	///     This class is adapted from the article "Implementing Dialog Boxes in MVVM"
	///     by Mark J. Feldman:
	///     http://www.codeproject.com/Articles/820324/Implementing-Dialog-Boxes-in-MVVM
	///     And is distributed under different terms than Zaber-originated code.
	///     Please see the license.txt file included at the root folder of this library.
	/// </summary>
	public static class DialogBehavior
	{
		#region -- Public data --

		/// <summary>
		///     Attached property for window closing notification.
		/// </summary>
		public static readonly DependencyProperty ClosingProperty
			= DependencyProperty.RegisterAttached("Closing",
												  typeof(bool),
												  typeof(DialogBehavior),
												  new PropertyMetadata(false));


		/// <summary>
		///     Attached property for window has closed notification.
		/// </summary>
		public static readonly DependencyProperty ClosedProperty
			= DependencyProperty.RegisterAttached("Closed",
												  typeof(bool),
												  typeof(DialogBehavior),
												  new PropertyMetadata(false));

		private static readonly Dictionary<IDialogViewModel, Window> DialogBoxes =
			new Dictionary<IDialogViewModel, Window>();

		private static readonly Dictionary<Window, NotifyCollectionChangedEventHandler> ChangeNotificationHandlers =
			new Dictionary<Window, NotifyCollectionChangedEventHandler>();

		private static readonly Dictionary<ObservableCollection<IDialogViewModel>, List<IDialogViewModel>>
			DialogBoxViewModels = new Dictionary<ObservableCollection<IDialogViewModel>, List<IDialogViewModel>>();

		/// <summary>
		///     Attached property for the view model to display a view dialog for.
		/// </summary>
		public static readonly DependencyProperty DialogViewModelsProperty = DependencyProperty.RegisterAttached(
			"DialogViewModels",
			typeof(object),
			typeof(DialogBehavior),
			new PropertyMetadata(null, OnDialogViewModelsChange));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Set the view model to display.
		/// </summary>
		/// <param name="aSource">The control to set the property for.</param>
		/// <param name="aValue">The new view model to display.</param>
		public static void SetDialogViewModels(DependencyObject aSource, object aValue)
			=> aSource.SetValue(DialogViewModelsProperty, aValue);


		/// <summary>
		///     Get the displayed view model.
		/// </summary>
		/// <param name="aSource">The control to get the property from.</param>
		/// <returns>Current value of the property.</returns>
		public static object GetDialogViewModels(DependencyObject aSource)
			=> aSource.GetValue(DialogViewModelsProperty);

		#endregion

		#region -- Non-Public Methods --

		private static void OnDialogViewModelsChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var parent = d as Window;
			if (null == parent)
			{
				return;
			}

			// when the parent closes we don't need to track it anymore
			parent.Closed += (s, a) => ChangeNotificationHandlers.Remove(parent);

			// otherwise create a handler for it that responds to changes to the supplied collection
			if (!ChangeNotificationHandlers.ContainsKey(parent))
			{
				ChangeNotificationHandlers[parent] = (sender, args) =>
				{
					var collection = sender as ObservableCollection<IDialogViewModel>;
					if (collection != null)
					{
						if ((args.Action == NotifyCollectionChangedAction.Add)
						|| (args.Action == NotifyCollectionChangedAction.Remove)
						|| (args.Action == NotifyCollectionChangedAction.Replace))
						{
							if (args.NewItems != null)
							{
								foreach (IDialogViewModel viewModel in args.NewItems)
								{
									if (!DialogBoxViewModels.ContainsKey(collection))
									{
										DialogBoxViewModels[collection] = new List<IDialogViewModel>();
									}

									DialogBoxViewModels[collection].Add(viewModel);
									AddDialog(viewModel, collection, d as Window);
								}
							}

							if (args.OldItems != null)
							{
								foreach (IDialogViewModel viewModel in args.OldItems)
								{
									RemoveDialog(viewModel);
									DialogBoxViewModels[collection].Remove(viewModel);
									if (DialogBoxViewModels[collection].Count == 0)
									{
										DialogBoxViewModels.Remove(collection);
									}
								}
							}
						}
						else if (args.Action == NotifyCollectionChangedAction.Reset)
						{
							// a reset event is typically generated in response to clearing the collection.
							// unfortunately the framework doesn't provide us with the list of items being
							// removed which is why we have to keep a mirror in DialogBoxViewModels
							if (DialogBoxViewModels.ContainsKey(collection))
							{
								var viewModels = DialogBoxViewModels[collection];
								foreach (var viewModel in DialogBoxViewModels[collection])
								{
									RemoveDialog(viewModel);
								}

								DialogBoxViewModels.Remove(collection);
							}
						}
					}
				};
			}

			// when the collection is first bound to this property we should create any initial
			// dialogs the user may have added in the main view model's constructor
			var newCollection = e.NewValue as ObservableCollection<IDialogViewModel>;
			if (newCollection != null)
			{
				newCollection.CollectionChanged += ChangeNotificationHandlers[parent];
				foreach (var viewModel in newCollection.ToList())
				{
					AddDialog(viewModel, newCollection, d as Window);
				}
			}

			// when we remove the binding we need to shut down any dialogs that have been left open
			var oldCollection = e.OldValue as ObservableCollection<IDialogViewModel>;
			if (oldCollection != null)
			{
				oldCollection.CollectionChanged -= ChangeNotificationHandlers[parent];
				foreach (var viewModel in oldCollection.ToList())
				{
					RemoveDialog(viewModel);
				}
			}
		}


		private static void AddDialog(IDialogViewModel viewModel, ObservableCollection<IDialogViewModel> collection,
									  Window owner)
		{
			// find the global resource that has been keyed to this view model type, walking up
			// the inheritance chain until we reach Window.
			var vmType = viewModel.GetType();
			var resource = Application.Current.TryFindResource(vmType);
			while ((null == resource) && (null != vmType.BaseType) && (typeof(Window) != vmType.BaseType))
			{
				vmType = vmType.BaseType;
				resource = Application.Current.TryFindResource(vmType);
			}

			if (null == resource)
			{
				// Failed to find a view for this VM.
				Debug.WriteLine("No view type found for viewmodel type " + viewModel.GetType().Name);
				return;
			}

			// is this resource a presenter?
			if (IsAssignableToGenericType(resource.GetType(), typeof(IDialogBoxPresenter<>)))
			{
				try
				{
					resource.GetType().GetMethod("Show").Invoke(resource, new object[] { viewModel });
				}
				catch (Exception aException)
				{
					viewModel.Exception = aException;
				}
				finally
				{
					collection.Remove(viewModel);
				}
			}

			// is this resource a dialog box window?
			else if (resource is Window)
			{
				var userViewModel = viewModel as IUserDialogViewModel;
				if (null == userViewModel)
				{
					return;
				}

				// If it's a do not show again dialog and do not show again
				// is selected, just set the result and skip showing it.
				var doNotShowAgain = viewModel as IDoNotShowAgainDialog;
				if ((null != doNotShowAgain)
				&& (null != doNotShowAgain.DoNotShowAgainSettings)
				&& !doNotShowAgain.DoNotShowAgainSettings.ShowDialog)
				{
					doNotShowAgain.DialogResult = doNotShowAgain.DoNotShowAgainSettings.LastSelection;
					return;
				}

				var dialog = resource as Window;
				dialog.DataContext = userViewModel;
				DialogBoxes[userViewModel] = dialog;

				EventHandler bringToFront = (aSender, aArgs) => { BringToFront(resource as Window); };

				userViewModel.BringToFront += bringToFront;

				userViewModel.DialogClosing += (sender, args) =>
				{
					var uvm = sender as IUserDialogViewModel;
					uvm.BringToFront -= bringToFront;
					collection.Remove(uvm);
				};

				dialog.Closing += (sender, args) =>
				{
					if (!(bool) dialog.GetValue(ClosingProperty))
					{
						dialog.SetValue(ClosingProperty, true);
						userViewModel.RequestClose();
						if (!(bool) dialog.GetValue(ClosedProperty))
						{
							args.Cancel = true;
							dialog.SetValue(ClosingProperty, false);
						}
					}
				};

				dialog.Closed += (sender, args) =>
				{
					Debug.Assert(DialogBoxes.ContainsKey(userViewModel));
					DialogBoxes.Remove(userViewModel);
				};

				try
				{
					if (userViewModel.IsModal)
					{
						dialog.Owner = owner;
						dialog.ShowDialog();
					}
					else
					{
						// Owner is not set for non-modal windows so that we can bring the main
						// window to the front.
						dialog.Owner = owner;
						dialog.Show();
					}
				}
				catch (Exception aException)
				{
					userViewModel.Exception = aException;
				}
			}
		}


		private static void RemoveDialog(IDialogViewModel viewModel)
		{
			if (DialogBoxes.ContainsKey(viewModel))
			{
				var dialog = DialogBoxes[viewModel];
				if (!(bool) dialog.GetValue(ClosingProperty))
				{
					dialog.SetValue(ClosingProperty, true);
					DialogBoxes[viewModel].Close();
				}

				dialog.SetValue(ClosedProperty, true);
			}
		}


		// courtesy James Fraumeni/StackOverflow: http://stackoverflow.com/questions/74616/how-to-detect-if-type-is-another-generic-type/1075059#1075059
		private static bool IsAssignableToGenericType(Type givenType, Type genericType)
		{
			var interfaceTypes = givenType.GetInterfaces();

			foreach (var it in interfaceTypes)
			{
				if (it.IsGenericType && (it.GetGenericTypeDefinition() == genericType))
				{
					return true;
				}
			}

			if (givenType.IsGenericType && (givenType.GetGenericTypeDefinition() == genericType))
			{
				return true;
			}

			var baseType = givenType.BaseType;
			if (baseType == null)
			{
				return false;
			}

			return IsAssignableToGenericType(baseType, genericType);
		}


		private static void BringToFront(Window aWindow)
		{
			if (!aWindow.IsVisible)
			{
				aWindow.Show();
			}

			if (WindowState.Minimized == aWindow.WindowState)
			{
				aWindow.WindowState = WindowState.Normal;
			}

			aWindow.Activate();
			aWindow.Topmost = true;
			aWindow.Topmost = false;
			aWindow.Focus();
		}

		#endregion

		#region -- Data --

		#endregion
	}
}

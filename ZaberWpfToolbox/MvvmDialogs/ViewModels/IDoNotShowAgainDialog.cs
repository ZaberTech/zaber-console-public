﻿using ZaberWpfToolbox.Settings;

namespace MvvmDialogs.ViewModels
{
	/// <summary>
	///     Dialogs that implement this interface have Do Not Show Again functionality.
	/// </summary>
	public interface IDoNotShowAgainDialog : IUserDialogViewModel
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     The identifier for this window instance that determines
		///     if it should be shown again if it is a "Do Not Show Again" window.
		/// </summary>
		DoNotShowAgainDialogSettings DoNotShowAgainSettings { get; set; }

		/// <summary>
		///     The result code indicating which option was selected by the user.
		///     Code displaying the dialog should set this to a default value that can be
		///     used to detect that the window was closed without selecting an option. If
		///     the do not show option is enabled, this property will automatically be
		///     filled in with whatever the last selected result was, if the user made a
		///     selection while "do not show again" was checked.
		///     The type assigned to this property must be XML serializable. Null is considered
		///     a valid value. Avoid using complex data as it is persisted in user settings and
		///     referential integrity may not carry over between sessions.
		/// </summary>
		object DialogResult { get; set; }

		#endregion
	}
}

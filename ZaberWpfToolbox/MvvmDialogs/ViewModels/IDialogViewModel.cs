﻿using System;

namespace MvvmDialogs.ViewModels
{
	/// <summary>
	///     Interface to be implemented by VMs that correspond to dialog windows.
	/// </summary>
	public interface IDialogViewModel
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Any exception that was unhandled by the dialog itself.
		/// </summary>
		Exception Exception { get; set; }

		#endregion
	}
}

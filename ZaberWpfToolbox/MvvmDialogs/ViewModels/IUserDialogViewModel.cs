﻿using System;

namespace MvvmDialogs.ViewModels
{
	/// <summary>
	///     Interface to be implemented by VMs that correspond to user dialog windows.
	/// </summary>
	public interface IUserDialogViewModel : IDialogViewModel
	{
		#region -- Events --

		/// <summary>
		///     VM should invoke this event when it is time to close the dialog. The MVVM
		///     dialog system listens to this; normally user code should not.
		/// </summary>
		event EventHandler DialogClosing;


		/// <summary>
		///     Implementers can invoke this event to restore the dialog window and bring it
		///     to the front. This is really only useful for non-model windows.
		/// </summary>
		event EventHandler BringToFront;

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Convenience method to request the dialog to close. Normally implemented
		///     to just invoke the <cref>DialogClosing</cref> event, but can also contain
		///     VM-specific cleanup code.
		/// </summary>
		void RequestClose();


		/// <summary>
		///     Users of a dialog VM can invoke this to bring the window to the front. VM
		///     implementers should implement this to file the <c>BringToFront</c> event.
		/// </summary>
		void RequestBringToFront();


		/// <summary>
		///     Return true if the dialog should be modal.
		/// </summary>
		bool IsModal { get; }

		#endregion
	}
}

﻿using MvvmDialogs.ViewModels;

namespace MvvmDialogs.Presenters
{
	/// <summary>
	///     Interface to be implemented by presenters for system dialogs. This is used for windows
	///     that need to be explicitly invoked by code, such as the system messagebox.
	/// </summary>
	/// <typeparam name="T">The input/output data type for the dialog presenter.</typeparam>
	public interface IDialogBoxPresenter<T> where T : IDialogViewModel
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Display the dialog.
		/// </summary>
		/// <param name="viewModel">Input and output data for the presenter.</param>
		void Show(T viewModel);

		#endregion
	}
}

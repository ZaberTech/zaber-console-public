﻿using System;

namespace ZaberWpfToolbox
{
	/// <summary>
	///     Wrapper interface for DispatcherTimers, to aid testing.
	/// </summary>
	public interface IDispatcherTimer
	{
		#region -- Events --

		/// <summary>
		///     Occurs when the timer interval has elapsed.
		/// </summary>
		event EventHandler Tick;

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Starts the timer.
		/// </summary>
		void Start();


		/// <summary>
		///     Stops the timer.
		/// </summary>
		void Stop();


		/// <summary>
		///     Gets or sets a value that indicates whether the timer is running.
		/// </summary>
		bool IsEnabled { get; set; }


		/// <summary>
		///     Gets or sets the period of time between timer ticks.
		/// </summary>
		TimeSpan Interval { get; set; }

		#endregion
	}
}

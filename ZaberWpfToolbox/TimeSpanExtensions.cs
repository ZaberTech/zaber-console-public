﻿using System;
using System.Collections.Generic;

namespace ZaberWpfToolbox
{
	/// <summary>
	///     Extension methods for TimeSpan.
	/// </summary>
	public static class TimeSpanExtensions
	{
		/// <summary>
		///     Print the value of a TimeSpan in "x h, x m, x s" format.
		/// </summary>
		/// <param name="aTimeSpan">Value to convert to a string.</param>
		/// <returns>
		///     A string containing the nonzero parts of the timespan
		///     followed by _unitSymbols.
		/// </returns>
		public static string PrettyPrint(this TimeSpan aTimeSpan)
		{
			var quantities = new List<int>
			{
				aTimeSpan.Days,
				aTimeSpan.Hours,
				aTimeSpan.Minutes,
				aTimeSpan.Seconds,
				aTimeSpan.Milliseconds
			};

			var parts = new List<string>();

			// TODO (Soleil 2020-01): It would be nice to use Humanize instead, but we'll
			// have to update our .NET dependency first.
			for (var i = 0; i < quantities.Count; i++)
			{
				if (quantities[i] > 0)
				{
					parts.Add($"{quantities[i]} {_unitSymbols[i]}");
				}
			}

			return string.Join(", ", parts);
		}


		private static readonly string[] _unitSymbols =
		{
			"d", "h", "m", "s", "ms"
		};
	}
}
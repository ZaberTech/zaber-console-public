﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;

namespace ZaberWpfToolbox
{
	/// <summary>
	///     Base class for GUI ViewModels - implements the INotifyPropertyChanged bookkeeping.
	/// </summary>
	/// <remarks>
	///     This class is defined locally in order to avoid bringing in the heavy Prism toolkit.
	///     Lambdas are used to identify the changed properties instead of strings so that code
	///     refactoring tools will automatically update property references when the names are changed.
	/// </remarks>
	public class ObservableObject : INotifyPropertyChanged
	{
		#region -- Events --

		/// <summary>
		///     Views automatically hook into this event to be notified of data changes.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Helper to extract the name of a property from the empty lamba that references it.
		/// </summary>
		/// <typeparam name="T">Type of the lamba return value.</typeparam>
		/// <param name="aProperty">The lamba used to name a property.</param>
		/// <returns>The name of the referenced property.</returns>
		public static string NameOf<T>(Expression<Func<T>> aProperty)
			=> ((MemberExpression) aProperty.Body).Member.Name;


		/// <summary>
		///     Helper for creating ICommand handlers only once, on first access. This prevents creating
		///     multiple copies of the same predicate when the code is the same every time.
		/// </summary>
		/// <param name="aField">Reference to the field to store the command handler in.</param>
		/// <param name="aExecute">Code for the command to execute.</param>
		/// <returns></returns>
		public static RelayCommand OnDemandRelayCommand(ref RelayCommand aField, Action<object> aExecute)
		{
			if (null == aField)
			{
				aField = new RelayCommand(aExecute);
			}

			return aField;
		}


		/// <summary>
		///     Helper for creating ICommand handlers only once, on first access. This prevents creating
		///     multiple copies of the same predicate when the code is the same every time.
		/// </summary>
		/// <param name="aField">Reference to the field to store the command handler in.</param>
		/// <param name="aExecute">Code for the command to execute.</param>
		/// <param name="aCanExecute">Code to determine whether the command can be executed.</param>
		/// <returns></returns>
		public static RelayCommand OnDemandRelayCommand(ref RelayCommand aField, Action<object> aExecute,
														Predicate<object> aCanExecute)
		{
			if (null == aField)
			{
				aField = new RelayCommand(aExecute, aCanExecute);
			}

			return aField;
		}

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Subclasses (ie ViewModels) should call this in the setters of bound properties
		///     to update the property values, so that associated Views will automatically be
		///     notified.
		/// </summary>
		/// <param name="aField">Reference to the storage field being updated.</param>
		/// <param name="aValue">The new value to store in aField.</param>
		/// <param name="aPropertyName">
		///     The name of the property being set. Use nameof(property)
		///     to enable refactoring tools to fix the names automatically.
		/// </param>
		protected virtual void Set<T>(ref T aField, T aValue, string aPropertyName)
		{
			// Prevent spurious events for value types, since ReferenceEquals is never true for them.
			if (typeof(T).IsValueType)
			{
				if (!aField.Equals(aValue))
				{
					aField = aValue;
					OnPropertyChanged(aPropertyName);
				}
			}
			else if (!ReferenceEquals(aField, aValue))
			{
				aField = aValue;
				OnPropertyChanged(aPropertyName);
			}
		}


		/// <summary>
		///     Subclasses (ie ViewModels) may call this to notify Views of changes to
		///     bound property values, in cases where one change affects multiple
		///     values. Generally using Set is preferred if possible.
		/// </summary>
		/// <param name="aPropertyName">
		///     An expression that evaluates to the bound property being set. Use "() => PropertyName"
		///     without the quotes.
		/// </param>
		protected virtual void OnPropertyChanged(string aPropertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(aPropertyName));
			}
		}

		#endregion
	}
}

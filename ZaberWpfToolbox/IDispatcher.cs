﻿using System;

// A dispatcher interface that is specified for testing purposes.

namespace ZaberWpfToolbox
{
	/// <summary>
	///     This dispatcher is used in the same way System.Threading.Dispatcher is used
	///     but may be overriden or mocked for testing purposes.
	/// </summary>
	public interface IDispatcher
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     System.Threading.Dispatcher's Invoke method. This causes the action
		///     to run on the main UI thread synchronously and then return to the point of execution
		///     from where it was called.
		/// </summary>
		/// <param name="action"></param>
		void Invoke(Action action);


		/// <summary>
		///     System.Threading.Dispatcher's BeginInvoke method. This causes
		///     the action to run on the main UI thread asynchronously.
		/// </summary>
		/// <param name="action"></param>
		void BeginInvoke(Action action);

		#endregion
	}
}

﻿using System.Globalization;
using System.Windows.Controls;

namespace ZaberWpfToolbox.Validation
{
	/// <summary>
	///     Data validation helper for text boxes that checks for numeric values within a
	///     specified range. The range can be one-sided.
	/// </summary>
	public class RangeValidator : ValidationRule
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Callback for validating entered text.
		/// </summary>
		/// <param name="aValue">The string containing the text being entered.</param>
		/// <param name="aCulture">Current culture for number formatting rules.</param>
		/// <returns></returns>
		public override ValidationResult Validate(object aValue, CultureInfo aCulture)
		{
			var s = aValue as string;
			if ((null != s) && (s.Length > 0))
			{
				var v = 0m;
				if (!decimal.TryParse(s, NumberStyles.Any, aCulture.NumberFormat, out v))
				{
					if (!decimal.TryParse(s, NumberStyles.Any, CultureInfo.InvariantCulture, out v))
					{
						return new ValidationResult(false, "Must be numeric.");
					}
				}

				if (v < Min)
				{
					return new ValidationResult(false, "Minimum value is " + Min.ToString(aCulture.NumberFormat));
				}

				if (v > Max)
				{
					return new ValidationResult(false, "Maximum value is " + Max.ToString(aCulture.NumberFormat));
				}
			}

			return new ValidationResult(true, null);
		}


		/// <summary>
		///     Minimum allowed value for the numeric quantity. Defaults to the greatest
		///     possible negative double value.
		/// </summary>
		public decimal Min { get; set; } = decimal.MinValue;


		/// <summary>
		///     Maximum allowed value for the numeric quantity. Defaults to the greatest
		///     possible positive double value.
		/// </summary>
		public decimal Max { get; set; } = decimal.MaxValue;

		#endregion
	}
}

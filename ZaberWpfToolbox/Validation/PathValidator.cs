﻿using System.Globalization;
using System.IO;
using System.Windows.Controls;

namespace ZaberWpfToolbox.Validation
{
	/// <summary>
	///     Data validation helper for text boxes that checks that the entered text is
	///     a valid path name for a file or directory.
	/// </summary>
	/// <remarks>
	///     Current behavior is to verify that the path points to something that
	///     already exists. Future expansion may include checking whether the string is a valid
	///     path to create something at.
	/// </remarks>
	public class PathValidator : ValidationRule
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Callback for validating entered text.
		/// </summary>
		/// <param name="aValue">The string containing the text being entered.</param>
		/// <param name="aCulture">Current culture for formatting rules.</param>
		/// <returns></returns>
		public override ValidationResult Validate(object aValue, CultureInfo aCulture)
		{
			var result = new ValidationResult(false, "Path is not valid.");
			var s = aValue as string;
			if ((null != s) && (s.Length > 0))
			{
				if (IsFile)
				{
					if (File.Exists(s))
					{
						return new ValidationResult(true, null);
					}

					result = new ValidationResult(false, "Path does not refer to an existing file.");
				}

				if (IsDir)
				{
					if (Directory.Exists(s))
					{
						return new ValidationResult(true, null);
					}

					if (IsFile)
					{
						result = new ValidationResult(false, "Path does not refer to an existing file or directory.");
					}
					else
					{
						result = new ValidationResult(false, "Path does not refer to an existing directory.");
					}
				}
			}

			return result;
		}


		/// <summary>
		///     Entered path is valid if it refers to a file. Defaults to true.
		/// </summary>
		public bool IsFile { get; set; } = true;


		/// <summary>
		///     Entered path is valid if it refers to a directory. Defaults to true.
		/// </summary>
		public bool IsDir { get; set; } = true;

		#endregion
	}
}

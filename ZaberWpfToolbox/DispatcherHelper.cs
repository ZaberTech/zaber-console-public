﻿using System.Windows;

namespace ZaberWpfToolbox
{
	/// <summary>
	///     This class is used to generate a an IDispatcher.
	/// </summary>
	public static class DispatcherHelper
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     The IDispatcher's implementation is usually set to
		///     <see cref="StandardDispatcher" /> which just implements
		///     System.Threading.Dispatcher's Invoke and BeginInvoke methods.
		///     The Dispatcher will be set to Application.Current.Dispatcher if nothing else
		///     is done.
		/// </summary>
		static DispatcherHelper()
		{
			if (Application.Current != null)
			{
				Dispatcher = new StandardDispatcher(Application.Current.Dispatcher);
			}
		}


		/// <summary>
		///     The dispatcher that will be used by a class
		///     that wants to have a mock-able Dispatcher.
		/// </summary>
		public static IDispatcher Dispatcher { get; set; }

		#endregion
	}
}

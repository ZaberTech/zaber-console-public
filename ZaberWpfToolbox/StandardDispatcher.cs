﻿using System;
using System.Windows.Threading;

namespace ZaberWpfToolbox
{
	/// <summary>
	///     A standard dispatcher implementation that simply
	///     uses System.Threading.Dispatcher's BeginInvoke and Invoke methods.
	/// </summary>
	public class StandardDispatcher : IDispatcher
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Wraps around a dispatcher to user its Invoke and BeginInvoke methods.
		/// </summary>
		/// <param name="dispatcher">A dispatcher to wrap around.</param>
		public StandardDispatcher(Dispatcher dispatcher)
		{
			_dispatcher = dispatcher;
		}


		/// <summary>
		///     System.Threading.Dispatcher's BeginInvoke method. Executes
		///     an action asynchronously on the main thread.
		/// </summary>
		/// <param name="action">The action to be executed.</param>
		public void BeginInvoke(Action action) => _dispatcher.BeginInvoke(action);


		/// <summary>
		///     System.Threading.Dispatcher's Invoke method. Executes
		///     an action synchronously on the main thread.
		/// </summary>
		/// <param name="action">The action to be executed.</param>
		public void Invoke(Action action) => _dispatcher.Invoke(action);

		#endregion

		#region -- Data --

		private readonly Dispatcher _dispatcher;

		#endregion
	}
}

﻿using System;
using System.Windows.Threading;

namespace ZaberWpfToolbox
{
	/// <summary>
	///     Implements <see cref="IDispatcherTimer" /> using the default system DispatcherTimer.
	/// </summary>
	public class StandardDispatcherTimer : IDispatcherTimer
	{
		#region -- Events --

		/// <inheritdoc cref="IDispatcherTimer.Tick" />
		public event EventHandler Tick;

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Create a standard dispatcher timer in the ways used by Zaber Console.
		///     This always uses the current dispatcher at time of creation.
		/// </summary>
		/// <param name="aPriority">Priority.</param>
		public StandardDispatcherTimer(DispatcherPriority aPriority = DispatcherPriority.Normal)
		{
			_timer = new DispatcherTimer(aPriority);
			_timer.Tick += Timer_Tick;
		}


		/// <inheritdoc cref="IDispatcherTimer.Start" />
		public void Start() => _timer.Start();


		/// <inheritdoc cref="IDispatcherTimer.Stop" />
		public void Stop() => _timer.Stop();


		/// <inheritdoc cref="IDispatcherTimer.IsEnabled" />
		public bool IsEnabled
		{
			get => _timer.IsEnabled;
			set => _timer.IsEnabled = value;
		}


		/// <inheritdoc cref="IDispatcherTimer.Interval" />
		public TimeSpan Interval
		{
			get => _timer.Interval;
			set => _timer.Interval = value;
		}

		#endregion

		#region -- Non-Public Methods --

		private void Timer_Tick(object aSender, EventArgs aArgs) => Tick?.Invoke(aSender, aArgs);

		#endregion

		#region -- Data --

		private readonly DispatcherTimer _timer;

		#endregion
	}
}

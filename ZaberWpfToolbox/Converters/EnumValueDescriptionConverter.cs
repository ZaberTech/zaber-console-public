﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Data;

namespace ZaberWpfToolbox.Converters
{
	/// <summary>
	///     Converter that returns the descrption attribute content for an enumeration value,
	///     or the value's name if no description is found.
	///     Adapted from code found in the thread:
	///     http://stackoverflow.com/questions/3985876/wpf-binding-a-listbox-to-an-enum-displaying-the-description-attribute
	/// </summary>
	public class EnumValueDescriptionConverter : IValueConverter
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Extract the description from an enum value.
		/// </summary>
		/// <param name="aValue">The enumeration value.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aParameter">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>
		///     The description of the enum value, or the name of the enum value if there is
		///     no description.
		/// </returns>
		public object Convert(object aValue, Type aTargetType, object aParameter, CultureInfo aCulture)
		{
			var result = string.Empty;

			if (aValue is Enum)
			{
				var e = (Enum) aValue;
				result = GetEnumValueDescription(e);
			}

			return result;
		}


		/// <summary>
		///     Reverse conversion is not implemented.
		/// </summary>
		/// <param name="aValue">Unused.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aParameter">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>Nothing.</returns>
		/// <exception cref="NotImplementedException">The method was called.</exception>
		public object ConvertBack(object aValue, Type aTargetType, object aParameter, CultureInfo aCulture)
			=> throw new NotImplementedException();

		#endregion

		#region -- Non-Public Methods --

		private string GetEnumValueDescription(Enum aValue)
		{
			var fieldInfo = aValue.GetType().GetField(aValue.ToString());
			var attrs = fieldInfo.GetCustomAttributes(false);

			if (0 == attrs.Length)
			{
				return aValue.ToString();
			}

			var attrib = attrs[0] as DescriptionAttribute;
			return attrib.Description;
		}

		#endregion
	}
}

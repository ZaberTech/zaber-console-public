﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace ZaberWpfToolbox.Converters
{
	/// <summary>
	///     Improved boolean to visibility converter that has options to invert the boolean
	///     input before converting, and allows the invisible state to be either collapsed or hidden.
	///     Default behavior is the same as the system BooleanToVisibilityConverter.
	/// </summary>
	[ValueConversion(typeof(bool), typeof(Visibility))]
	public class ConfigurableBooleanToVisibilityConverter : IValueConverter
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Convert from boolean to visibility.
		/// </summary>
		/// <param name="aValue">The input boolean.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aParam">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>Visibility.Visible or <c>HiddenState</c> depending on the aValue input and <c>ReverseBehavior</c>.</returns>
		/// <exception cref="InvalidOperationException">The aValue input is not a boolean.</exception>
		public object Convert(object aValue, Type aTargetType, object aParam, CultureInfo aCulture)
		{
			if (!(aValue is bool))
			{
				throw new InvalidOperationException("The input value must be a boolean.");
			}

			var b = (bool) aValue ^ ReverseBehavior;

			return b ? Visibility.Visible : HiddenState;
		}


		/// <summary>
		///     Convert from visibility back to a boolean. This will convert Visibility.Visible to
		///     true and either other possible value to false, or the reverse if <c>ReverseBehavior</c>
		///     is true. The value of <c>HiddenState</c> is ignored.
		/// </summary>
		/// <param name="aValue">Visibility value.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aParam">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>True or false depending on the input and on <c>ReverseBehavior</c>.</returns>
		/// <exception cref="InvalidOperationException">The aValue input is not a Visibilty enum value.</exception>
		public object ConvertBack(object aValue, Type aTargetType, object aParam, CultureInfo aCulture)
		{
			if (!(aValue is Visibility))
			{
				throw new InvalidOperationException("The input value must be a Visibility vlaue.");
			}

			var v = (Visibility) aValue;
			return (Visibility.Visible == v) ^ ReverseBehavior;
		}


		/// <summary>
		///     Set to true to hide in true instead of false.
		/// </summary>
		public bool ReverseBehavior { get; set; }

		/// <summary>
		///     Which state to use for invisible mode. Defaults to Collapsed.
		/// </summary>
		public Visibility HiddenState { get; set; } = Visibility.Collapsed;

		#endregion

		#region -- Data --

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;

namespace ZaberWpfToolbox.Converters
{
	/// <summary>
	///     This converter can be used to chain converters together sequentially.
	///     Adapted from
	///     https://web.archive.org/web/20130622171857/http://www.garethevans.com/linking-multiple-value-converters-in-wpf-and-silverlight
	/// </summary>
	public class SequentialValueConverter : List<IValueConverter>, IValueConverter
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Converts an initial value sequentually by taking the initial value,
		///     sending that through the first converter,
		///     then sending the first converters output into the second converter as the input, and so forth.
		/// </summary>
		/// <param name="value">The value that will be converted.</param>
		/// <param name="targetType">The targettype that will be used by the converters</param>
		/// <param name="parameter">The parameter used by the converters (if used at all)</param>
		/// <param name="culture">The culture used by all of the converters (if used at all)</param>
		/// <returns></returns>
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var returnValue = value;

			foreach (var converter in this)
			{
				returnValue = converter.Convert(returnValue, targetType, parameter, culture);
			}

			return returnValue;
		}


		/// <summary>
		///     Not implemented.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="targetType"></param>
		/// <param name="parameter"></param>
		/// <param name="culture"></param>
		/// <returns></returns>
		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
			=> throw new NotImplementedException();

		#endregion
	}
}

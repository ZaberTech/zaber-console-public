﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ZaberWpfToolbox.Converters
{
	/// <summary>
	///     Boolean value converter that performs a logical OR on all of its inputs.
	///     Input bindings are expected to be boolean; use other converters to make this so.
	/// </summary>
	public class BooleanOrConverter : IMultiValueConverter
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Perform logical OR of all inputs.
		/// </summary>
		/// <param name="aValues">The inputs to OR together. All must be boolean. Non-booleans are ignored.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aReference">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns></returns>
		public object Convert(object[] aValues, Type aTargetType, object aReference, CultureInfo aCulture)
		{
			var result = false;

			foreach (var o in aValues)
			{
				if ((null != o) && o is bool)
				{
					result |= (bool) o;
				}

				// Early-out when definite result is known.
				if (result)
				{
					break;
				}
			}

			return result ^ NegateResult ? TrueValue : FalseValue;
		}


		/// <summary>
		///     Not implemented. Required by the IMultiValueConverter interface.
		/// </summary>
		/// <param name="aValue">Unused.</param>
		/// <param name="aTargetTypes">Unused.</param>
		/// <param name="aParameter">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>Nothing.</returns>
		/// <exception cref="NotImplementedException">The method was invoked.</exception>
		public object[] ConvertBack(object aValue, Type[] aTargetTypes, object aParameter, CultureInfo aCulture)
			=> throw new NotImplementedException();


		/// <summary>
		///     Set to true to invert output value.
		/// </summary>
		public bool NegateResult { get; set; }

		/// <summary>
		///     Object to return when the condition evaluates to true. Defaults to boolean true.
		/// </summary>
		public object TrueValue { get; set; } = true;

		/// <summary>
		///     Object to return when the condition evaluates to false. Defaults to boolean false.
		/// </summary>
		public object FalseValue { get; set; } = false;

		#endregion
	}
}

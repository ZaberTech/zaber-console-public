﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ZaberWpfToolbox.Converters
{
	/// <summary>
	///     WPF value converter that checks objects for nullity.
	/// </summary>
	[ValueConversion(typeof(object), typeof(bool))]
	public class IsNullConverter : IValueConverter
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Check if an object reference is null.
		/// </summary>
		/// <param name="aValue">The object to test.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aParam">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>True if the string is null or empty.</returns>
		public object Convert(object aValue, Type aTargetType, object aParam, CultureInfo aCulture)
			=> (null == aValue) ^ NegateResult;


		/// <summary>
		///     Requred by the IMultiValueConverter interface. Not implemented.
		/// </summary>
		/// <param name="aValue">Unused.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aParam">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>Nothing</returns>
		/// <exception cref="NotSupportedException">The method was invoked.</exception>
		public object ConvertBack(object aValue, Type aTargetType, object aParam, CultureInfo aCulture)
			=> throw new NotSupportedException();


		/// <summary>
		///     Set to true to invert output value.
		/// </summary>
		public bool NegateResult { get; set; }

		#endregion
	}
}

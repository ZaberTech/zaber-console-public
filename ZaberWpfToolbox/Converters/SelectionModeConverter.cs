﻿using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;

namespace ZaberWpfToolbox.Converters
{
	/// <summary>
	///     WPF value converter that converts a boolean to a selection mode
	/// </summary>
	[ValueConversion(typeof(bool), typeof(SelectionMode))]
	public class SelectionModeConverter : IValueConverter
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Convert a boolean to a selection mode.
		/// </summary>
		/// <param name="aValue">The boolean to test.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aParam">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>SelectionMode.Extended if the input was true, or SelectionMode.Single otherwise</returns>
		/// <exception cref="ArgumentException">The aValue input is not a boolean type.</exception>
		public object Convert(object aValue, Type aTargetType, object aParam, CultureInfo aCulture)
		{
			if (!(aValue is bool))
			{
				throw new ArgumentException("Input to SelectionModeConverter.Convert() should be a boolean.");
			}

			var b = (bool) aValue;
			return b ? SelectionMode.Extended : SelectionMode.Single;
		}


		/// <summary>
		///     Convert a SelectionMode to a boolean.
		/// </summary>
		/// <param name="aValue">The SelectionMode value to convert.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aParam">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>True unless the input mode is SelectionMode.Single.</returns>
		/// <exception cref="ArgumentException">The aValue input is not a SelectionMode value.</exception>
		public object ConvertBack(object aValue, Type aTargetType, object aParam, CultureInfo aCulture)
		{
			if (!(aValue is SelectionMode))
			{
				throw new ArgumentException(
					"Input to SelectionModeConverter.ConvertBack() should be a SelectionMode enum value.");
			}

			var mode = (SelectionMode) aValue;
			return mode != SelectionMode.Single;
		}

		#endregion
	}
}

﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Data;

namespace ZaberWpfToolbox.Converters
{
	/// <summary>
	///     Converter that returns the display name of a type or instance. If given a type, it will return the
	///     value of the DisplayName attribute on that type, or the type name if the attribute is not present.
	///     If given a class instance, it will search up the class hierarchy until it finds a DisplayName attribute
	///     and return its value. If none is found it will return the type name of the given class instance.
	/// </summary>
	public class DisplayNameConverter : IValueConverter
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Extract the display name from a type or object.
		/// </summary>
		/// <param name="aValue">The type or instance to get the display name of.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aParameter">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>The display name of aValue, or its type name if there is no display name.</returns>
		public object Convert(object aValue, Type aTargetType, object aParameter, CultureInfo aCulture)
		{
			var result = string.Empty;

			if (null == aValue)
			{
				return null;
			}

			if (aValue is Type)
			{
				var t = aValue as Type;
				result = GetDisplayName(t) ?? t.Name;
			}
			else
			{
				var t = aValue.GetType();
				result = GetDisplayName(t) ?? t.Name;
			}

			return result;
		}


		/// <summary>
		///     Reverse conversion is not implemented.
		/// </summary>
		/// <param name="aValue">Unused.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aParameter">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>Nothing.</returns>
		/// <exception cref="NotImplementedException">The method was called.</exception>
		public object ConvertBack(object aValue, Type aTargetType, object aParameter, CultureInfo aCulture)
			=> throw new NotImplementedException();

		#endregion

		#region -- Non-Public Methods --

		private static string GetDisplayName(Type aType)
		{
			foreach (var attr in aType.GetCustomAttributes(typeof(DisplayNameAttribute), true))
			{
				var dna = attr as DisplayNameAttribute;
				if (null != dna)
				{
					return dna.DisplayName;
				}
			}

			return null;
		}

		#endregion
	}
}

﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace ZaberWpfToolbox.Converters
{
	/// <summary>
	///     Helper for serializing grid measurements to and from strings in user settings.
	/// </summary>
	[ValueConversion(typeof(string), typeof(GridLength))]
	public class GridLengthValueConverter : IValueConverter
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Convert a string to a GridLength. Uses the serialization format of the GridLengthConverter class.
		/// </summary>
		/// <param name="aValue">The string to convert.</param>
		/// <param name="aTargetType">Unused</param>
		/// <param name="aReference">
		///     Optional - if not null, this is expected to be a double value
		///     representing the minimum gridlength to output. This is used to clamp negative lengths.
		/// </param>
		/// <param name="aCulture">Unused</param>
		/// <returns>The string's value as a Gridlenth.</returns>
		/// <exception cref="NotSupportedException">The string cannot be parsed.</exception>
		public object Convert(object aValue, Type aTargetType, object aReference, CultureInfo aCulture)
		{
			var result = (GridLength) _converter.ConvertFromString(aValue as string);

			if (null != aReference)
			{
				var min = 0.0;
				if (aReference is double)
				{
					min = (double) aReference;
				}
				else if (aReference is string)
				{
					double.TryParse(aReference as string, NumberStyles.Float, CultureInfo.InvariantCulture, out min);
				}

				if (result.Value < min)
				{
					result = new GridLength(min, result.GridUnitType);
				}
			}

			return result;
		}


		/// <summary>
		///     Convert a GridLength to a string. Uses the serialization format of the GridLengthConverter class.
		/// </summary>
		/// <param name="aValue">The length to convert.</param>
		/// <param name="aTargetType">Unused</param>
		/// <param name="aReference">Unused</param>
		/// <param name="aCulture">Unused</param>
		/// <returns>The serialized version of the value.</returns>
		/// <exception cref="NotSupportedException">The conversion cannot be performed.</exception>
		public object ConvertBack(object aValue, Type aTargetType, object aReference, CultureInfo aCulture)
			=> _converter.ConvertToString(aValue);

		#endregion

		#region -- Data --

		private static readonly GridLengthConverter _converter = new GridLengthConverter();

		#endregion
	}
}

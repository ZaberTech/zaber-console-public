﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ZaberWpfToolbox.Converters
{
	/// <summary>
	///     WPF value converter that checks strings for content.
	/// </summary>
	[ValueConversion(typeof(string), typeof(bool))]
	public class StringEmptyConverter : IValueConverter
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Check if a string is null or empty.
		/// </summary>
		/// <param name="aValue">The string to test.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aParam">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>True if the string is null or empty.</returns>
		/// <exception cref="InvalidOperationException">The aValue input is not a string type.</exception>
		public object Convert(object aValue, Type aTargetType, object aParam, CultureInfo aCulture)
		{
			var s = aValue as string;
			if ((s == null) && (aValue != null))
			{
				throw new InvalidOperationException("The target must be a string.");
			}

			return string.IsNullOrEmpty(s) ^ NegateResult;
		}


		/// <summary>
		///     Requred by the IMultiValueConverter interface. Not implemented.
		/// </summary>
		/// <param name="aValue">Unused.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aParam">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>Nothing</returns>
		/// <exception cref="NotSupportedException">The method was invoked.</exception>
		public object ConvertBack(object aValue, Type aTargetType, object aParam, CultureInfo aCulture)
			=> throw new NotSupportedException();


		/// <summary>
		///     Set to true to invert output value.
		/// </summary>
		public bool NegateResult { get; set; }

		#endregion
	}
}

﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ZaberWpfToolbox.Converters
{
	/// <summary>
	///     Tests two objects for equality according to the rules:
	///     1) null == null
	///     2) null != not null
	///     3) If both are not null, invoke .Equals on the first one passing in the second.
	/// </summary>
	public class EqualityConverter : IValueConverter
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Check equality of two parameters.
		/// </summary>
		/// <param name="aValue">The first parameter to compare.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aReference">The second parameter to compare.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>True if the two parameters compare equal, false otherwise.</returns>
		public object Convert(object aValue, Type aTargetType, object aReference, CultureInfo aCulture)
		{
			if ((null == aValue) && (null == aReference))
			{
				return true ^ NegateResult;
			}

			if ((null == aValue) ^ (null == aReference))
			{
				return false ^ NegateResult;
			}

			return aValue.Equals(aReference) ^ NegateResult;
		}


		/// <summary>
		///     Requred by the IMultiValueConverter interface. Not implemented.
		/// </summary>
		/// <param name="aValue">Unused.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aReference">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>Nothing</returns>
		/// <exception cref="NotImplementedException">The method was invoked.</exception>
		public object ConvertBack(object aValue, Type aTargetType, object aReference, CultureInfo aCulture)
			=> throw new NotImplementedException();


		/// <summary>
		///     Set to true to change to an inequality to boolean converter.
		/// </summary>
		public bool NegateResult { get; set; }

		#endregion
	}
}

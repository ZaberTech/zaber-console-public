﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ZaberWpfToolbox.Converters
{
	/// <summary>
	///     Compares all input values and returns true if they are all equal to the first.
	/// </summary>
	public class EqualityMultiConverter : IMultiValueConverter
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Test equality of all inputs.
		/// </summary>
		/// <param name="aValues">Input values.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aReference">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>True if all inputs are equal to the first, or if there are no inputs.</returns>
		public object Convert(object[] aValues, Type aTargetType, object aReference, CultureInfo aCulture)
		{
			var result = true;

			if ((null != aValues) && (aValues.Length > 1))
			{
				var first = aValues[0];
				for (int i = 1; i < aValues.Length; i++)
				{
					if (!object.Equals(first, aValues[i]))
					{
						result = false;
						break;
					}
				}
			}

			return result ^ NegateResult ? TrueValue : FalseValue;
		}


		/// <summary>
		///     Requred by the IMultiValueConverter interface. Not implemented.
		/// </summary>
		/// <param name="aValue">Unused.</param>
		/// <param name="aTargetTypes">Unused.</param>
		/// <param name="aReference">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>Nothing</returns>
		/// <exception cref="NotImplementedException">The method was invoked.</exception>
		public object[] ConvertBack(object aValue, Type[] aTargetTypes, object aReference, CultureInfo aCulture)
			=> throw new NotImplementedException();


		/// <summary>
		///     Set to true to invert output.
		/// </summary>
		public bool NegateResult { get; set; }

		/// <summary>
		///     Object to return when the condition evaluates to true. Defaults to boolean true.
		/// </summary>
		public object TrueValue { get; set; } = true;

		/// <summary>
		///     Object to return when the condition evaluates to false. Defaults to boolean false.
		/// </summary>
		public object FalseValue { get; set; } = false;

		#endregion
	}
}

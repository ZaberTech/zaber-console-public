﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;
using ExpressionEvaluator;

namespace ZaberWpfToolbox.Converters
{
	/// <summary>
	///     Multi-value converter that evaluates mathematical expressions on its inputs.
	///     Accepts up to 26 inputs of diverse types; they can be referenced in the expression
	///     string as 'a' through 'z'. The expression and the number of inputs can be changed.
	///     See https://csharpeval.codeplex.com/ for documentation about the expressions.
	///     Example usage in a XAML Style to evaluate Z = X^2 + Y, where X, Y and Z are VM properties:
	///     <Setter Property="Z">
	///         <Setter.Value>
	///             <MultiBinding Converter="{zgtc:ExpressionConverter a * a + b}">
	///                 <Binding Path="X" />
	///                 <Binding Path="Y" />
	///             </MultiBinding>
	///         </Setter.Value>
	///     </Setter>
	/// </summary>
	public class ExpressionConverter : MarkupExtension, IMultiValueConverter
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor - initializes with no expression set.
		/// </summary>
		public ExpressionConverter()
		{
		}


		/// <summary>
		///     Constructor that initializes with an expression.
		/// </summary>
		/// <param name="aExpression">The initial expression to use.</param>
		public ExpressionConverter(string aExpression)
		{
			Expression = aExpression;
		}


		/// <summary>
		///     Perform expression evaluation.
		/// </summary>
		/// <param name="aValues">
		///     Inputs to the expression. These will be labeled 'a' through 'z'. The minimum number is zero, and
		///     the maximum is 26.
		/// </param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aReference">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>The result of the expression.</returns>
		/// <exception cref="ArgumentException">There were more than 26 inputs.</exception>
		public object Convert(object[] aValues, Type aTargetType, object aReference, CultureInfo aCulture)
		{
			if ((null != aValues) && (aValues.Length > 26))
			{
				throw new ArgumentException("ExpressionConverter only supports up to 26 arguments.");
			}

			var reg = new TypeRegistry();
			var varName = 'a';
			if (null != aValues)
			{
				foreach (var val in aValues)
				{
					reg.RegisterSymbol(varName.ToString(), val);
					varName++;
				}
			}

			var compiledExpression = new CompiledExpression(Expression);
			compiledExpression.TypeRegistry = reg;
			compiledExpression.Compile();

			return compiledExpression.Eval();
		}


		/// <summary>
		///     Requred by the IMultiValueConverter interface. Not implemented.
		/// </summary>
		/// <param name="aValue">Unused.</param>
		/// <param name="aTargetTypes">Unused.</param>
		/// <param name="aReference">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>Nothing</returns>
		/// <exception cref="NotImplementedException">The method was invoked.</exception>
		public object[] ConvertBack(object aValue, Type[] aTargetTypes, object aReference, CultureInfo aCulture)
			=> throw new NotImplementedException();


		/// <summary>
		///     Required by the Markup Extension base class.
		/// </summary>
		/// <param name="serviceProvider">Unused.</param>
		/// <returns>this</returns>
		public override object ProvideValue(IServiceProvider serviceProvider) => this;


		/// <summary>
		///     The expression to evaluate.
		/// </summary>
		public string Expression
		{
			get => _expression;
			set
			{
				if (_expression != value)
				{
					_expression = value;
				}
			}
		}

		#endregion

		#region -- Data --

		private string _expression = string.Empty;

		#endregion
	}
}

﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace ZaberWpfToolbox.Converters
{
	/// <summary>
	///     Value converter that returns true if an enum property has a particular value.
	///     Backwards conversion is supported.
	/// </summary>
	public class EnumToBooleanConverter : IValueConverter
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Check to see if an enum has a particular value.
		/// </summary>
		/// <param name="aData">The enum variable to check.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aReferenceValue">The value to check for.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>
		///     True if the enum has the reference value, false otherwise, or DependencyProperty.UnsetValue if aData is not an
		///     enum type.
		/// </returns>
		public object Convert(object aData, Type aTargetType, object aReferenceValue, CultureInfo aCulture)
		{
			if ((null != aData) && aData.GetType().IsEnum)
			{
				return Equals(aData, aReferenceValue) ^ NegateResult;
			}

			return DependencyProperty.UnsetValue;
		}


		/// <summary>
		///     Return the reference enum value if a boolean is true.
		/// </summary>
		/// <param name="aData">Boolean to test.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aReferenceValue">Enumeration value to return on true.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>
		///     aReferenceValue if aData is a true booleam, or DependencyProperty.UnsetValue otherwise.
		///     Opposite results if NegateResult is true.
		/// </returns>
		public object ConvertBack(object aData, Type aTargetType, object aReferenceValue, CultureInfo aCulture)
		{
			if (aData is bool && ((bool) aData ^ NegateResult))
			{
				return aReferenceValue;
			}

			return DependencyProperty.UnsetValue;
		}


		/// <summary>
		///     Set to true to invert output value.
		/// </summary>
		public bool NegateResult { get; set; }

		#endregion
	}
}

﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ZaberWpfToolbox.Converters
{
	/// <summary>
	///     Boolean value converter that performs a logical AND on all of its parameter types.
	///     Input bindings are expected to be boolean; use other converters to make this so.
	/// </summary>
	public class BooleanAndConverter : IMultiValueConverter
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Perform logical AND operation on all inputs.
		/// </summary>
		/// <param name="aValues">Input values; all are expected to be booleans. Non-booleans are treated as false.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aReference">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns></returns>
		public object Convert(object[] aValues, Type aTargetType, object aReference, CultureInfo aCulture)
		{
			var result = true;

			foreach (var o in aValues)
			{
				if ((null != o) && o is bool)
				{
					result &= (bool) o;
				}
				else
				{
					result = false;
				}

				// Early-out when definite result is known.
				if (!result)
				{
					break;
				}
			}

			return result ^ NegateResult ? TrueValue : FalseValue;
		}


		/// <summary>
		///     Requred by the IMultiValueConverter interface. Not implemented.
		/// </summary>
		/// <param name="aValue">Unused.</param>
		/// <param name="aTargetTypes">Unused.</param>
		/// <param name="aReference">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>Nothing</returns>
		/// <exception cref="NotImplementedException">The method was invoked.</exception>
		public object[] ConvertBack(object aValue, Type[] aTargetTypes, object aReference, CultureInfo aCulture)
			=> throw new NotImplementedException();


		/// <summary>
		///     Set to true to invert output.
		/// </summary>
		public bool NegateResult { get; set; }

		/// <summary>
		///     Object to return when the condition evaluates to true. Defaults to boolean true.
		/// </summary>
		public object TrueValue { get; set; } = true;

		/// <summary>
		///     Object to return when the condition evaluates to false. Defaults to boolean false.
		/// </summary>
		public object FalseValue { get; set; } = false;

		#endregion
	}
}

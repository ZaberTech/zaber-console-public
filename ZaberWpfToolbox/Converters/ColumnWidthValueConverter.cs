﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace ZaberWpfToolbox.Converters
{
	/// <summary>
	///     Calculates the width of a grid column given the space available, the number of
	///     columns and a safety margin for spacing. Used to control the behavior of grid
	///     columns inside a ScrollViewer.
	/// </summary>
	[ValueConversion(typeof(double), typeof(GridLength))]
	public class ColumnWidthValueConverter : IValueConverter
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Perform conversion.
		/// </summary>
		/// <param name="aTotalWidth">The space available for grid columns. Must be castable to double.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aParameter">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>aTotalWidth / NumColumns + Offset, as a GridLength.</returns>
		/// <exception cref="InvalidCastException">aTotalWidth could not be cast to double.</exception>
		/// <exception cref="NullReferenceException">aTotalWidth was null.</exception>
		public object Convert(object aTotalWidth, Type aTargetType, object aParameter, CultureInfo aCulture)
		{
			var result = ((double) aTotalWidth / NumColumns) + Offset;
			result = Math.Max(0.0, result);
			return new GridLength(result, GridUnitType.Pixel);
		}


		/// <summary>
		///     Not implemented.
		/// </summary>
		/// <param name="aValue">Unused.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aParameter">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>Nothing.</returns>
		/// <exception cref="NotImplementedException">The method was called.</exception>
		public object ConvertBack(object aValue, Type aTargetType, object aParameter, CultureInfo aCulture)
			=> throw new NotImplementedException();


		/// <summary>
		///     Offset value to add to the result of the calculation.
		///     Defaults to -1.
		/// </summary>
		public double Offset { get; set; } = -1.0;


		/// <summary>
		///     Number of columns to divide the space into. Defaults to 1.
		/// </summary>
		public int NumColumns { get; set; } = 1;

		#endregion
	}
}

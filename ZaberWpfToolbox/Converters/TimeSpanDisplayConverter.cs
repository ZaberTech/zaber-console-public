﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ZaberWpfToolbox.Converters
{
	/// <summary>
	///     WPF value converter that pretty-prints timespans.
	///     Currently only supports English.
	/// </summary>
	[ValueConversion(typeof(TimeSpan), typeof(string))]
	public class TimeSpanDisplayConverter : IValueConverter
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Convert a TimeSpan to a nice human-readable string.
		/// </summary>
		/// <param name="aValue">The TimeSpan value to convert.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aParam">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>A string representation, or null if the input is null or the wrong type.</returns>
		/// <exception cref="InvalidOperationException">The aValue input is not a TimeSpan.</exception>
		public object Convert(object aValue, Type aTargetType, object aParam, CultureInfo aCulture)
		{
			if (aValue is null)
			{
				return null;
			}

			if (!(aValue is TimeSpan ts))
			{
				throw new InvalidOperationException("The target must be a TimeSpan.");
			}

			return ts.PrettyPrint();
		}


		/// <summary>
		///     Requred by the IMultiValueConverter interface. Not implemented.
		/// </summary>
		/// <param name="aValue">Unused.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aParam">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>Nothing</returns>
		/// <exception cref="NotSupportedException">The method was invoked.</exception>
		public object ConvertBack(object aValue, Type aTargetType, object aParam, CultureInfo aCulture)
			=> throw new NotSupportedException();

		#endregion
	}
}
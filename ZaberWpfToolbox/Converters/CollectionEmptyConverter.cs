﻿using System;
using System.Collections;
using System.Globalization;
using System.Windows.Data;

namespace ZaberWpfToolbox.Converters
{
	/// <summary>
	///     WPF value converter that checks enumerables for content.
	/// </summary>
	[ValueConversion(typeof(IEnumerable), typeof(bool))]
	public class CollectionEmptyConverter : IValueConverter
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Check if a collection is null or empty.
		/// </summary>
		/// <param name="aValue">The collection to test.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aParam">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>True if the collection is null or empty.</returns>
		public object Convert(object aValue, Type aTargetType, object aParam, CultureInfo aCulture)
		{
			var collection = aValue as IEnumerable;
			if (null == collection)
			{
				if (null != aValue)
				{
					throw new InvalidOperationException("Object is not an IEnumerable.");
				}

				return true ^ NegateResult;
			}

			var canHaz = false;
			foreach (var o in collection)
			{
				canHaz = true;
				break;
			}

			return !canHaz ^ NegateResult;
		}


		/// <summary>
		///     Requred by the IMultiValueConverter interface. Not implemented.
		/// </summary>
		/// <param name="aValue">Unused.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aParam">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>Nothing</returns>
		/// <exception cref="NotSupportedException">The method was invoked.</exception>
		public object ConvertBack(object aValue, Type aTargetType, object aParam, CultureInfo aCulture)
			=> throw new NotSupportedException();


		/// <summary>
		///     Set to true to invert output value.
		/// </summary>
		public bool NegateResult { get; set; }

		#endregion
	}
}

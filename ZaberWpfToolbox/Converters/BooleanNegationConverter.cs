﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ZaberWpfToolbox.Converters
{
	/// <summary>
	///     WPF value converter that inverts a boolean. Used to trigger
	///     UI changed when a value becomes false.
	/// </summary>
	[ValueConversion(typeof(bool), typeof(bool))]
	public class BooleanNegationConverter : IValueConverter
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Negate a boolean property value.
		/// </summary>
		/// <param name="aValue">The value to negate. Must be a boolean.-</param>
		/// <param name="aTargetType">The type of the property the value comes from. Must be assignable from boolean.</param>
		/// <param name="aParam">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>!aValue</returns>
		/// <exception cref="InvalidOperationException">aValue is not a boolean or aTargetType is not assignable from boolean.</exception>
		public object Convert(object aValue, Type aTargetType, object aParam, CultureInfo aCulture)
		{
			if (!aTargetType.IsAssignableFrom(typeof(bool)))
			{
				throw new InvalidOperationException("The target must be assignable from boolean. Actual type is "
												+ aTargetType.Name);
			}

			return !(bool) aValue;
		}


		/// <summary>
		///     Negate a boolean property value.
		/// </summary>
		/// <param name="aValue">The value that is produced by the binding target.</param>
		/// <param name="aTargetType">The type to convert to.</param>
		/// <param name="aParam">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>!aValue</returns>
		/// <exception cref="InvalidOperationException">aValue is not a boolean or aTargetType is not assignable from boolean.</exception>
		public object ConvertBack(object aValue, Type aTargetType, object aParam, CultureInfo aCulture)
		{
			if (!aTargetType.IsAssignableFrom(typeof(bool)))
			{
				throw new InvalidOperationException("The target must be assignable from boolean. Actual type is "
												+ aTargetType.Name);
			}

			return !(bool) aValue;
		}

		#endregion
	}
}

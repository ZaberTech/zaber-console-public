﻿<!-- View for an About Box window for use in multiple programs. Provides
	 extensive information about the program's .NET operating environment.
-->
<Window x:Class="ZaberWpfToolbox.Dialogs.AboutBox"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:ZaberWpfToolbox.Dialogs"
        xmlns:ztbb="clr-namespace:ZaberWpfToolbox.Behaviors"
        xmlns:ztbc="clr-namespace:ZaberWpfToolbox.Converters"
        xmlns:ztbr="clr-namespace:ZaberWpfToolbox.Resources"
        xmlns:i="http://schemas.microsoft.com/xaml/behaviors"
        xmlns:zaberTheme="clr-namespace:ZaberWpfToolbox.Themes;assembly=ZaberWpfToolbox"
        Width="600"
        Height="400"
        MinWidth="300"
        MinHeight="300"
        WindowStyle="ToolWindow"
        ResizeMode="CanResizeWithGrip"
        Title="{Binding WindowTitle}"
        Icon="{Binding Icon}">

	<i:Interaction.Triggers>
		<i:EventTrigger EventName="Closing">
			<i:InvokeCommandAction Command="{Binding WindowClosingCommand}" />
		</i:EventTrigger>
	</i:Interaction.Triggers>

	<Window.InputBindings>
		<KeyBinding Key="Esc" Command="{Binding WindowClosingCommand}" />
	</Window.InputBindings>

	<Grid Margin="5">
		<Grid.RowDefinitions>
			<RowDefinition Height="Auto" />
			<RowDefinition Height="Auto" />
			<RowDefinition Height="Auto" />
			<RowDefinition Height="*" />
			<RowDefinition Height="5" />
			<RowDefinition Height="Auto" />
		</Grid.RowDefinitions>

		<Grid.ColumnDefinitions>
			<ColumnDefinition Width="Auto" />
			<ColumnDefinition Width="*" />
			<ColumnDefinition Width="Auto" />
			<ColumnDefinition Width="Auto" />
		</Grid.ColumnDefinitions>

		<!-- Top section contains program icon, name and brief description. -->
		<Image Grid.RowSpan="2"
		       Source="{Binding Icon}"
		       Width="32"
		       Height="32"
		       Stretch="Uniform"
		       Margin="15" />

		<TextBlock Grid.Column="1"
		           Grid.ColumnSpan="3"
		           Text="{Binding Title}"
		           VerticalAlignment="Center" />

		<TextBox Grid.Row="1"
		         Grid.Column="1"
		         Grid.ColumnSpan="3"
		         IsReadOnly="True"
		         Text="{Binding Description}"
		         TextWrapping="Wrap"
		         VerticalAlignment="Stretch" />

		<!-- Second section contains program version and build information. -->
		<StackPanel Orientation="Vertical"
		            Grid.Row="2"
		            Grid.ColumnSpan="4"
		            HorizontalAlignment="Stretch">
			<Separator HorizontalAlignment="Stretch" />
			<StackPanel Orientation="Horizontal"
			            FlowDirection="LeftToRight">
				<Label Content="{Binding Version}" />
				<TextBlock />
				<Label>
					<Label.Visibility>
						<Binding Path="ReleaseNotesUrl">
							<Binding.Converter>
								<ztbc:SequentialValueConverter>
									<ztbc:IsNullConverter NegateResult="True" />
									<ztbc:ConfigurableBooleanToVisibilityConverter HiddenState="Collapsed" />
								</ztbc:SequentialValueConverter>
							</Binding.Converter>
						</Binding>
					</Label.Visibility>
					<Hyperlink NavigateUri="{Binding ReleaseNotesUrl}"
					           RequestNavigate="Hyperlink_RequestNavigate">
						<TextBlock Text="Release Notes"
						           ToolTip="{Binding ReleaseNotesUrl.OriginalString}" />
					</Hyperlink>
				</Label>
			</StackPanel>
			<Label Content="{Binding BuildDate}" />
			<Label Content="{Binding CopyrightMessage}" />
		</StackPanel>

		<!-- bottom part of the window is a set of tabs. -->
		<TabControl Grid.Row="3"
		            Grid.ColumnSpan="4"
		            HorizontalAlignment="Stretch"
		            VerticalAlignment="Stretch"
		            SelectedIndex="{Binding SelectedTabIndex, Mode=TwoWay}">
			<TabControl.Items>

				<!-- Tab that displays assembly information about the program. -->
				<TabItem Header="Application">
					<ScrollViewer HorizontalScrollBarVisibility="Auto">
						<DataGrid ItemsSource="{Binding ApplicationInfo}"
						          AutoGenerateColumns="False"
						          CanUserSortColumns="False"
						          CanUserReorderColumns="False"
						          CanUserResizeColumns="True"
						          CanUserResizeRows="False">

							<DataGrid.Columns>
								<DataGridTemplateColumn Header="Application Key"
								                        IsReadOnly="True">
									<DataGridTemplateColumn.CellTemplate>
										<DataTemplate>
											<TextBlock Text="{Binding Item1}" />
										</DataTemplate>
									</DataGridTemplateColumn.CellTemplate>
								</DataGridTemplateColumn>

								<DataGridTemplateColumn Header="Value"
								                        IsReadOnly="True">
									<DataGridTemplateColumn.CellTemplate>
										<DataTemplate>
											<TextBlock Text="{Binding Item2}" />
										</DataTemplate>
									</DataGridTemplateColumn.CellTemplate>
								</DataGridTemplateColumn>
							</DataGrid.Columns>

						</DataGrid>
					</ScrollViewer>
				</TabItem>

				<!-- Tab that displays a list of referenced assemblies. -->
				<TabItem Header="Assemblies">
					<ScrollViewer HorizontalScrollBarVisibility="Auto">
						<DataGrid ItemsSource="{Binding AssemblyList}"
						          SelectedItem="{Binding SelectedAssembly, Mode=TwoWay}"
						          AutoGenerateColumns="False"
						          CanUserAddRows="False"
						          CanUserSortColumns="True"
						          CanUserReorderColumns="False"
						          CanUserResizeColumns="True"
						          CanUserResizeRows="False">

							<i:Interaction.Behaviors>
								<ztbb:DataGridRowDoubleClickBehavior DoubleClickCommand="{Binding AssemblyDoubleClickCommand}" />
							</i:Interaction.Behaviors>

							<DataGrid.InputBindings>
								<KeyBinding Key="Return"
								            Command="{Binding AssemblyDoubleClickCommand}"
								            CommandParameter="{Binding SelectedAssembly}" />
							</DataGrid.InputBindings>

							<DataGrid.Columns>

								<DataGridTemplateColumn Header="Assembly Name"
								                        IsReadOnly="True"
								                        SortMemberPath="AssemblyName">
									<DataGridTemplateColumn.CellTemplate>
										<DataTemplate>
											<TextBlock Text="{Binding AssemblyName}" />
										</DataTemplate>
									</DataGridTemplateColumn.CellTemplate>
								</DataGridTemplateColumn>

								<DataGridTemplateColumn Header="Version"
								                        IsReadOnly="True"
								                        SortMemberPath="Version">
									<DataGridTemplateColumn.CellTemplate>
										<DataTemplate>
											<TextBlock Text="{Binding Version}" />
										</DataTemplate>
									</DataGridTemplateColumn.CellTemplate>
								</DataGridTemplateColumn>

								<DataGridTemplateColumn Header="Build Date"
								                        IsReadOnly="True"
								                        SortMemberPath="BuildDate">
									<DataGridTemplateColumn.CellTemplate>
										<DataTemplate>
											<TextBlock Text="{Binding BuildDate}" />
										</DataTemplate>
									</DataGridTemplateColumn.CellTemplate>
								</DataGridTemplateColumn>

								<DataGridTemplateColumn Header="Location"
								                        IsReadOnly="True"
								                        SortMemberPath="Location">
									<DataGridTemplateColumn.CellTemplate>
										<DataTemplate>
											<TextBlock Text="{Binding Location}" />
										</DataTemplate>
									</DataGridTemplateColumn.CellTemplate>
								</DataGridTemplateColumn>

							</DataGrid.Columns>
						</DataGrid>
					</ScrollViewer>
				</TabItem>

				<!-- Tab that displays information about one referenced assembly. -->
				<TabItem Header="Assembly Details">
					<Grid>
						<Grid.RowDefinitions>
							<RowDefinition Height="Auto" />
							<RowDefinition Height="*" />
						</Grid.RowDefinitions>
						<ComboBox HorizontalAlignment="Stretch"
						          ItemsSource="{Binding AssemblyList}"
						          SelectedItem="{Binding SelectedAssembly, Mode=TwoWay}"
						          ztbb:SetFocusBehavior.HasFocus="{Binding ComboBoxHasFocus, Mode=TwoWay}" />
						<ScrollViewer Grid.Row="1"
						              HorizontalScrollBarVisibility="Auto">
							<DataGrid ItemsSource="{Binding AssemblyInfo}"
							          AutoGenerateColumns="False"
							          CanUserSortColumns="False"
							          CanUserReorderColumns="False"
							          CanUserResizeColumns="True"
							          CanUserResizeRows="False">
								<DataGrid.Columns>

									<DataGridTemplateColumn Header="Assembly Key"
									                        IsReadOnly="True">
										<DataGridTemplateColumn.CellTemplate>
											<DataTemplate>
												<TextBlock Text="{Binding Item1}" />
											</DataTemplate>
										</DataGridTemplateColumn.CellTemplate>
									</DataGridTemplateColumn>

									<DataGridTemplateColumn Header="Value"
									                        IsReadOnly="True">
										<DataGridTemplateColumn.CellTemplate>
											<DataTemplate>
												<TextBlock Text="{Binding Item2}" />
											</DataTemplate>
										</DataGridTemplateColumn.CellTemplate>
									</DataGridTemplateColumn>

								</DataGrid.Columns>
							</DataGrid>
						</ScrollViewer>
					</Grid>
				</TabItem>
			</TabControl.Items>
		</TabControl>

		<!-- Second-last row is empty for spacing. -->

		<Button Grid.Row="5"
		        Grid.Column="2"
		        Content="Launch System Information Tool"
		        HorizontalAlignment="Right"
		        DockPanel.Dock="Left"
		        Command="{Binding MSInfoCommand}" />

		<Button Grid.Row="5"
		        Grid.Column="3"
		        Content="OK"
		        HorizontalAlignment="Right"
		        DockPanel.Dock="Right"
		        Command="{Binding WindowClosingCommand}"
		        ztbb:SetFocusBehavior.HasFocus="True" />

	</Grid>
</Window>
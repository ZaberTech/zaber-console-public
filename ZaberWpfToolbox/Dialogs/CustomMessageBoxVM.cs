﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MvvmDialogs.ViewModels;
using ZaberWpfToolbox.Settings;

namespace ZaberWpfToolbox.Dialogs
{
	/// <summary>
	///     ViewModel for a message box dialog that displays scrollable text and a variable number
	///     of buttons representing different possible user responses. Optionally shows a
	///     "do not show again" checkbox.
	/// </summary>
	public class CustomMessageBoxVM : ObservableObject, IDoNotShowAgainDialog
	{
		#region -- Public data --

		/// <summary>
		///     Representation of an option for the user to select.
		/// </summary>
		public class Option
		{
			#region -- Public Methods & Properties --

			/// <summary>
			///     Displayed name of the option.
			/// </summary>
			public string Label { get; internal set; }

			/// <summary>
			///     Caller-defined identifier for the option.
			/// </summary>
			public object Identifier { get; internal set; }

			#endregion
		}

		#endregion

		#region -- Events --

		/// <summary>
		///     Event for the dialog manager system to hook into.
		///     Fires when the dialog is closing.
		/// </summary>
		public event EventHandler DialogClosing;


		/// <summary>
		///     Event fired when code requests the window be brought to the front.
		/// </summary>
		public event EventHandler BringToFront;

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Create a new instance and set the message and title. You must call AddOption()
		///     at least once after this to create some buttons for the user to click on.
		/// </summary>
		/// <param name="aMessage">The message to be shown in the window's scrollable area.</param>
		/// <param name="aTitle">The title of the window.</param>
		public CustomMessageBoxVM(string aMessage, string aTitle)
		{
			aMessage = aMessage.Replace("\r", "").Replace("\n", Environment.NewLine);

			_message = aMessage;
			_title = aTitle;
		}


		/// <summary>
		///     Create a new instance and set the message and title and one or two buttons.
		///     This initializes the left button to produce the result MessageBoxResult.Yes and the
		///     right button to output MessageBoxResult.No. The default result is MessageBoxResult.None.
		///     This is a legacy helper and is not recommended for future use.
		/// </summary>
		/// <param name="aMessage">The message to be shown in the window's scrollable area.</param>
		/// <param name="aTitle">The title of the window.</param>
		/// <param name="aLeftButtonLabel">
		///     The text of the button with MessageBoxResult "Yes", or null to only display the other
		///     button.
		/// </param>
		/// <param name="aRightButtonLabel">The text of the button with MessageBoxResult "No". Required.</param>
		public CustomMessageBoxVM(string aMessage, string aTitle, string aLeftButtonLabel, string aRightButtonLabel)
		{
			aMessage = aMessage.Replace("\r", "").Replace("\n", Environment.NewLine);

			DialogResult = MessageBoxResult.None;

			_message = aMessage;
			_title = aTitle;

			if (!string.IsNullOrEmpty(aLeftButtonLabel))
			{
				AddOption(aLeftButtonLabel, MessageBoxResult.Yes);
			}

			AddOption(aRightButtonLabel, MessageBoxResult.No);
		}


		/// <summary>
		///     Add an option button to click on. Buttons are populated left to right in the order added.
		/// </summary>
		/// <param name="aLabel">Text to display in the button.</param>
		/// <param name="aIdentifier">
		///     A unique identifier for the button. This value will
		///     be assigned to <cref>DialogResult</cref> if the button is clicked. The value
		///     should be different from the default value of DialogResult so that you can
		///     identify when the dialog was closed without an option being selected.
		///     Avoid using complex types as these identifiers can persist in user settings
		///     when do-not-show-again mode is used.
		/// </param>
		public void AddOption(string aLabel, object aIdentifier) => _options.Add(new Option
		{
			Label = aLabel,
			Identifier = aIdentifier
		});


		/// <summary>
		///     Load the <see cref="Icon"/> from assembly resources given its URI.
		/// </summary>
		/// <param name="aUri">Resource URI to load from.</param>
		public void LoadBitmapIcon(string aUri) => Icon = new BitmapImage(new Uri(aUri));


		/// <summary>
		///     Reqest the dialog to close.
		/// </summary>
		public void RequestClose()
		{
			// If this method was called, then the dialog is either being closed programmatically
			// or the user clicked on the X icon to close it without selecting anything.
			// In this case we should reset the do not show again settings.
			if (null != DoNotShowAgainSettings)
			{
				DoNotShowAgainSettings.ShowDialog = true;
				DoNotShowAgainSettings.LastSelection = null;
			}

			DialogClosing?.Invoke(this, null);
		}


		/// <summary>
		///     Call this to bring the window to the front.
		/// </summary>
		public void RequestBringToFront() => BringToFront?.Invoke(this, null);


		/// <summary>
		///     Result code to be checked after the dialog closes.
		///     This will have one of the identifier values passed to AddOption if the
		///     user clicked one of the option buttons, or will retain its initial value if the
		///     user closed the dialog window without selecting an option.
		/// </summary>
		public object DialogResult { get; set; }

		/// <summary>
		///     Window title text.
		/// </summary>
		public string Title
		{
			get => _title;
			set => Set(ref _title, value, nameof(Title));
		}


		/// <summary>
		///     Message displayed in the scrollable area of the dialog.
		/// </summary>
		public string Message
		{
			get => _message;
			set => Set(ref _message, value, nameof(Message));
		}


		/// <summary>
		///     Optional icon to be displayed next to the message text.
		/// </summary>
		public ImageSource Icon
		{
			get => _icon;
			set => Set(ref _icon, value, nameof(Icon));
		}


		/// <summary>
		///     For view binding only - list of buttons to display.
		/// </summary>
		public IEnumerable<Option> Options => _options;


		/// <summary>
		///     Returns the button label corresponding to the default option, if any.
		///     The view uses this to set focus to the default options.
		/// </summary>
		public string FocusedButtonLabel
		{
			get
			{
				var match = _options.Where(opt => Equals(opt.Identifier, DialogResult)).FirstOrDefault();
				return match?.Label ?? null;
			}
		}


		/// <summary>
		///     Enable automatic parsing of URLs from displayed text. On by default.
		/// </summary>
		public bool EnableUrlParsing
		{
			get => _enableUrlParsing;
			set => Set(ref _enableUrlParsing, value, nameof(EnableUrlParsing));
		}


		/// <summary>
		///     Optional command to be invoked when a parsed URL is clicked. If not set, the
		///     default behavior is to launch the URL as a system command (ie open it in the
		///     default web browser).
		/// </summary>
		public ICommand UrlClickCommand
		{
			get => _urlClickCommand;
			set => Set(ref _urlClickCommand, value, nameof(UrlClickCommand));
		}


		/// <summary>
		///     Optional - set this to a non-null value to enable the "do not show again"
		///     behavior. If set, and the "do not show again" checkbox was checked the last time
		///     the user selected a valid option in the dialog associated with these settings,
		///     then the last selection will be set in <cref>DialogResult</cref> without the
		///     dialog actually being displayed.
		/// </summary>
		public DoNotShowAgainDialogSettings DoNotShowAgainSettings { get; set; }


		/// <summary>
		///     Command invoked by the left button.
		/// </summary>
		public ICommand ButtonClickCommand => new RelayCommand(aIdentifier =>
		{
			DialogResult = aIdentifier;

			// Only set the last selection in the do not show again settings
			// if the user actually selected something.
			if ((null != DoNotShowAgainSettings) && !DoNotShowAgainSettings.ShowDialog)
			{
				DoNotShowAgainSettings.LastSelection = aIdentifier;
			}

			// Close the dialog without clearing the do not show again setting.
			DialogClosing?.Invoke(this, null);
		});

		/// <summary>
		///     Controls whether this message box is a modal dialog. Defaults to true.
		/// </summary>
		public bool IsModal { get; set; } = true;


		/// <summary>
		///     Any exception that was unhandled by the dialog itself.
		/// </summary>
		public Exception Exception { get; set; }

		#endregion

		#region -- Data --

		private string _title = "Message";
		private string _message;
		private readonly List<Option> _options = new List<Option>();
		private bool _enableUrlParsing = true;
		private ICommand _urlClickCommand;
		private ImageSource _icon;

		#endregion
	}
}

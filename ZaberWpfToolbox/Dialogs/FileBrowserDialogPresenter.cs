﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Win32;
using MvvmDialogs.Presenters;

namespace ZaberWpfToolbox.Dialogs
{
	/// <summary>
	///     Helper class for opening system OpenFileDialog windows for selecting files.
	/// </summary>
	public class FileBrowserDialogPresenter : IDialogBoxPresenter<FileBrowserDialogParams>
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Show an OpenFileDialog with the settings provided in aVM. Invoked by the
		///     dialog subsystem. Returns the user interaction result in aVM.DialogResult
		///     and aVM.Filename.
		/// </summary>
		/// <param name="aVM">Inputs and outputs from the dialog window.</param>
		public void Show(FileBrowserDialogParams aVM)
		{
			FileDialog dialog = null;

			switch (aVM.Action)
			{
				case FileBrowserDialogParams.FileActionType.Open:
				{
					var dlg = new OpenFileDialog();
					dialog = dlg;
					dlg.Multiselect = aVM.MultiSelect;
					break;
				}
				case FileBrowserDialogParams.FileActionType.Save:
				{
					var dlg = new SaveFileDialog();
					dialog = dlg;
					dlg.OverwritePrompt = aVM.PromptForOverwrite;
					break;
				}
				default:
					throw new InvalidProgramException("Unsupported file dialog mode " + aVM.Action);
			}

			dialog.Filter = aVM.Filter;
			dialog.FilterIndex = aVM.FilterIndex;
			dialog.RestoreDirectory = true;
			dialog.AddExtension = aVM.AddExtension;
			dialog.CheckFileExists = aVM.CheckFileExists;
			dialog.FileName = aVM.Filename;
			dialog.DefaultExt = aVM.DefaultExtension;
			dialog.Title = aVM.Title;

			var initialDir = aVM.InitialDirectory;
			if (string.IsNullOrEmpty(initialDir) || !Directory.Exists(initialDir))
			{
				initialDir = aVM.DefaultDirectory;
			}

			dialog.InitialDirectory = initialDir;

			aVM.DialogResult = dialog.ShowDialog();

			if (!aVM.MultiSelect)
			{
				aVM.Filename = dialog.FileName;
			}
			else
			{
				aVM.Filenames = new List<string>(dialog.FileNames);
			}

			aVM.FilterIndex = dialog.FilterIndex;
		}

		#endregion
	}
}

﻿namespace ZaberWpfToolbox.Dialogs
{
	/// <summary>
	///     Custom dialog box implementation. Displays clickable URLs, a variable number of buttons and
	///     an optional "do not show again" checkbox. See <see cref="CustomMessageBoxVM" />.
	/// </summary>
	public partial class CustomMessageBox
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor. Initializes the component.
		/// </summary>
		public CustomMessageBox()
		{
			InitializeComponent();
		}

		#endregion
	}
}

﻿using System;
using System.Windows.Input;
using MvvmDialogs.ViewModels;

namespace ZaberWpfToolbox.Dialogs
{
	/// <summary>
	///     ViewModel for a basic modal dialog with a progress bar and message output.
	///     Normal use case is to subclass this and override OnWindowLoaded() to start the
	///     work task in a background thread.
	/// </summary>
	public class ProgressDialogVM : ObservableObject, IUserDialogViewModel
	{
		#region -- Events --

		/// <summary>
		///     Event hook used by the dialog system to detect window close requests.
		/// </summary>
		public event EventHandler DialogClosing;


		/// <summary>
		///     Event invoked to bring the window to the front.
		/// </summary>
		public event EventHandler BringToFront;

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Initialize the progress dialog to display an indefinite progress bar.
		///     Updating the Progress property will have no effect in this case.
		/// </summary>
		public ProgressDialogVM()
		{
		}


		/// <summary>
		///     Initialize the progress dialog to display a progress bar with a custom range.
		/// </summary>
		/// <param name="aMin">The starting progress value for the progress bar.</param>
		/// <param name="aMax">The finishing progress value for the progress bar.</param>
		public ProgressDialogVM(double aMin, double aMax)
		{
			_minValue = aMin;
			_maxValue = aMax;
			_message = string.Empty;
			_progress = aMin;
			_hasDefiniteValue = true;
		}


		/// <summary>
		///     Append text to the message area without a newline.
		/// </summary>
		/// <param name="aMessage"></param>
		public void Append(string aMessage) => Message = Message + aMessage;


		/// <summary>
		///     Append text to the messages displayed. A newline is added automatically.
		/// </summary>
		/// <param name="aMessage"></param>
		public void AppendLine(string aMessage) => Message = Message + aMessage + Environment.NewLine;


		/// <summary>
		///     Subclasses can call this to close the dialog when the work is complete.
		/// </summary>
		public void RequestClose() => DialogClosing?.Invoke(this, null);


		/// <summary>
		///     Call this to request the dialog window be brought to the front.
		/// </summary>
		public void RequestBringToFront() => BringToFront?.Invoke(this, null);


		/// <summary>
		///     Gets or sets the currently displayed message(s) in their entirety.
		/// </summary>
		public string Message
		{
			get => _message;
			set => Set(ref _message, value, nameof(Message));
		}


		/// <summary>
		///     Minimum progress value, as passed to the constructor.
		/// </summary>
		public double MinValue
		{
			get => _minValue;
			set => Set(ref _minValue, value, nameof(MinValue));
		}


		/// <summary>
		///     Maximum progress value, as passed to the constructor.
		/// </summary>
		public double MaxValue
		{
			get => _maxValue;
			set => Set(ref _maxValue, value, nameof(MaxValue));
		}


		/// <summary>
		///     Get or set the current progress bar value. Has no effect if HasDefiniteValue = false;
		/// </summary>
		public double Progress
		{
			get => _progress;
			set => Set(ref _progress, value, nameof(Progress));
		}


		/// <summary>
		///     Control whether the progress bar response to changes to the Progress property
		///     or displays an animation. If false, an animated, repeating progress bar is displayed
		///     and changing the Progress value will have no effect. Defaults to true.
		/// </summary>
		public bool HasDefiniteValue
		{
			get => _hasDefiniteValue;
			set => Set(ref _hasDefiniteValue, value, nameof(HasDefiniteValue));
		}


		/// <summary>
		///     Command invoked by the view when the window becomes visible.
		/// </summary>
		public ICommand WindowLoadedCommand => new RelayCommand(_ => OnWindowLoaded());

		/// <summary>
		///     Required by the IUserDialogViewModel interface. Sets this to be a modal dialog.
		/// </summary>
		public bool IsModal => true;


		/// <summary>
		///     Any exception that was unhandled by the dialog itself.
		/// </summary>
		public Exception Exception { get; set; }

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Called when the view becomes visible on screen. Subclasses should overload
		///     this to initiate the work for which progress and messages are being displayed.
		/// </summary>
		protected virtual void OnWindowLoaded()
		{
		}

		#endregion

		#region -- Data --

		private double _minValue;
		private double _maxValue = 100.0;
		private double _progress;
		private bool _hasDefiniteValue;
		private string _message = string.Empty;

		#endregion
	}
}

﻿using System.Windows;
using MvvmDialogs.Presenters;

namespace ZaberWpfToolbox.Dialogs
{
	/// <summary>
	///     MVVM presenter for system message boxes. This gets invoked via the
	///     DialogBehavior of a window when a MessageBoxParams is added to the window's
	///     dialog list.
	/// </summary>
	public class MessageBoxPresenter : IDialogBoxPresenter<MessageBoxParams>
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Displays a message box and passes the result code back to the associated ViewModel.
		/// </summary>
		/// <param name="aVM">
		///     A <cref>NessageBoxParams</cref> containing inputs for the message box and a place
		///     to store the result code.
		/// </param>
		public void Show(MessageBoxParams aVM)
			=> aVM.Result = MessageBox.Show(aVM.Message, aVM.Caption, aVM.Buttons, aVM.Icon);

		#endregion
	}
}

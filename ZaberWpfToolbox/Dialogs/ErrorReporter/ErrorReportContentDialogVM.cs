﻿using System;
using MvvmDialogs.ViewModels;

namespace ZaberWpfToolbox.Dialogs.ErrorReporter
{
	internal class ErrorReportContentDialogVM : ObservableObject, IUserDialogViewModel
	{
		#region -- Public data --

		public static bool IsOpen = false;

		#endregion

		#region -- Events --

		public event EventHandler DialogClosing;

		public event EventHandler BringToFront;

		#endregion

		#region -- Public Methods & Properties --

		public ErrorReportContentDialogVM(ErrorReportInformation aInformation)
		{
			Information = aInformation;
		}


		public void RequestClose() => DialogClosing?.Invoke(this, null);


		public void RequestBringToFront() => BringToFront?.Invoke(this, null);


		public bool IsModal => false;

		public ErrorReportInformation Information
		{
			get => _information;
			set => Set(ref _information, value, nameof(Information));
		}


		/// <summary>
		///     Any exception that was unhandled by the dialog itself.
		/// </summary>
		public Exception Exception { get; set; }

		#endregion

		#region -- Data --

		private ErrorReportInformation _information;

		#endregion
	}
}

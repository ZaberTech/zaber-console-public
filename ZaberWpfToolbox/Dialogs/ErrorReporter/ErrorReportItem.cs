﻿namespace ZaberWpfToolbox.Dialogs.ErrorReporter
{
	/// <summary>
	///     Model for one entry in an error report.
	/// </summary>
	public class ErrorReportItem
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Constructor - initalizes fields.
		/// </summary>
		/// <param name="aName">Category name for the new item.</param>
		/// <param name="aDescription">Description or content for the item.</param>
		public ErrorReportItem(string aName, string aDescription)
		{
			Name = aName;
			Description = aDescription;
		}


		/// <summary>
		///     Category name of the item.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		///     Description or content of the item.
		/// </summary>
		public string Description { get; set; }

		#endregion
	}
}

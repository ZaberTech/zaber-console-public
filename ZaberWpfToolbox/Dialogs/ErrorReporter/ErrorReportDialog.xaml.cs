﻿using System.Windows;

namespace ZaberWpfToolbox.Dialogs.ErrorReporter
{
	/// <summary>
	///     Interaction logic for ErrorReportDialog.xaml
	/// </summary>
	public partial class ErrorReportDialog : Window
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor. Initializes the component.
		/// </summary>
		public ErrorReportDialog()
		{
			InitializeComponent();
		}

		#endregion
	}
}

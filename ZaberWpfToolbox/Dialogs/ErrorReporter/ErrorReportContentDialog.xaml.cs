﻿using System.Windows;

namespace ZaberWpfToolbox.Dialogs.ErrorReporter
{
	/// <summary>
	///     Interaction logic for ErrorReportContentDialog.xaml
	/// </summary>
	public partial class ErrorReportContentDialog : Window
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor. Initializes the component.
		/// </summary>
		public ErrorReportContentDialog()
		{
			InitializeComponent();
		}

		#endregion
	}
}

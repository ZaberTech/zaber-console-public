﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;

namespace ZaberWpfToolbox.Dialogs.ErrorReporter
{
	/// <summary>
	///     Information to be submitted with the error report
	/// </summary>
	public class ErrorReportInformation : List<ErrorReportItem>
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Add an item to the the error report information list.
		/// </summary>
		/// <param name="aItem">ErrorReportItems.Item enum to add</param>
		/// <param name="aDescription">Item description</param>
		/// <param name="aTrim">Choose if aDescription string should be trimmed</param>
		public void Add(Enum aItem, string aDescription, bool aTrim = true)
		{
			if (!string.IsNullOrEmpty(aDescription) && aTrim)
			{
				aDescription = aDescription.Trim();
			}

			Add(new ErrorReportItem(ErrorReportItems.Description(aItem), aDescription));
		}


		/// <summary>
		///     Get an item from the error report information list.
		/// </summary>
		/// <param name="aItem">ErrorReportItems.Item enum to get</param>
		/// <returns>ErrorReportItem paired with the specified ErrorReportItems.Item</returns>
		public ErrorReportItem Get(Enum aItem)
			=> this.First(item => item.Name.Equals(ErrorReportItems.Description(aItem)));


		/// <summary>
		///     Anonymize ErrorReportInformation by removing/replacing personal information
		/// </summary>
		/// <returns>An anonymized version of ErrorReportInformation</returns>
		public ErrorReportInformation Anonymize()
		{
			if (!string.IsNullOrEmpty(Get(ErrorReportItems.Item.Email).Description))
			{
				ClearItem(ErrorReportItems.Item.Guid);
			}

			ClearItemsWithEmptyDescriptions();

			return this;
		}


		/// <summary>
		///     Convert ErrorReportInformation to a JSON string
		/// </summary>
		/// <returns>JSON string representing ErrorReportInformation</returns>
		public string ToJson()
		{
			var dictionary = this.ToDictionary(item => VariableNameFor(item.Name),
											   item => item.Description);

			return new JavaScriptSerializer().Serialize(dictionary);
		}


		/// <summary>
		///     Human-readable summar[y,ies] of the error(s), to be displayed in the popup dialog but
		///     not included in the filed report.
		/// </summary>
		public IEnumerable<string> ErrorConditions { get; set; } = new List<string>();

		#endregion

		#region -- Non-Public Methods --

		private string VariableNameFor(string aString)
		{
			aString = aString.Trim().ToLower();
			aString = Regex.Replace(aString, @"[\s]+", "_");
			return Regex.Replace(aString, @"[^\w\d]", string.Empty);
		}


		private void ClearItem(Enum aItem)
		{
			for (var i = 0; i < Count; ++i)
			{
				if (this[i].Name.Equals(ErrorReportItems.Description(aItem)))
				{
					this[i].Description = null;
				}
			}
		}


		private void ClearItemsWithEmptyDescriptions()
		{
			for (var i = 0; i < Count; ++i)
			{
				if (string.IsNullOrEmpty(this[i].Description?.Trim()))
				{
					this[i].Description = null;
				}
			}
		}

		#endregion
	}
}

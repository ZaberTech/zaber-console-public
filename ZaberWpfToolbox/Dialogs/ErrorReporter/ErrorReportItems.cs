﻿using System;
using System.ComponentModel;

namespace ZaberWpfToolbox.Dialogs.ErrorReporter
{
	/// <summary>
	///     Enum with string descriptions for each item.
	///     This class is implemented to avoid directly using strings as item names.
	/// </summary>
	public class ErrorReportItems
	{
		#region -- Public data --

		/// <summary>
		///     Enumeration of all the different category names for error report items.
		/// </summary>
		public enum Item
		{
			/// <summary>
			///     User's email address.
			/// </summary>
			Email,

			/// <summary>
			///     Item description or content.
			/// </summary>
			Description,

			/// <summary>
			///     Timestamp for the report.
			/// </summary>
			Date,

			/// <summary>
			///     Exception message, if any.
			/// </summary>
			Exception,

			/// <summary>
			///     Name of the program making the report.
			/// </summary>
			[Description("Program name")]
			ProgramName,

			/// <summary>
			///     Version of the program making the report.
			/// </summary>
			[Description("Program version")]
			ProgramVersion,

			/// <summary>
			///     Unique identifier for the computer involved.
			/// </summary>
			[Description("Machine GUID")]
			Guid,

			/// <summary>
			///     Name of the operating system in use.
			/// </summary>
			[Description("Operating system")]
			OperatingSystem,

			/// <summary>
			///     Information about the screen(s) attached to the computer.
			/// </summary>
			Screen,

			/// <summary>
			///     List of attached devices.
			/// </summary>
			[Description("Connected devices")]
			Devices,

			/// <summary>
			///     Info about the currently open port and prorotol, if any.
			/// </summary>
			[Description("Port mode")]
			PortMode,

			/// <summary>
			///     List of active plugins or tabs in the program.
			/// </summary>
			[Description("Open tabs")]
			OpenTabs,

			/// <summary>
			///     The plugin or tab in use at the time of the error.
			/// </summary>
			[Description("Active tab")]
			ActiveTab,

			/// <summary>
			///     Amount of memory available.
			/// </summary>
			[Description("Total physical memory")]
			Memory,

			/// <summary>
			///     Display language setting from the OS.
			/// </summary>
			[Description("UI culture language")]
			Language,

			/// <summary>
			///     Name of the installer used, if known.
			/// </summary>
			Installer,

			/// <summary>
			///     List of all loaded DLLs.
			/// </summary>
			[Description("DLLs loaded")]
			Assemblies,

			/// <summary>
			///     Contents of the user message log, if any.
			/// </summary>
			[Description("Message log")]
			MessageLog,

			/// <summary>
			///     Contents of the application log file.
			/// </summary>
			[Description("Application log")]
			ApplicationLog,

			/// <summary>
			///     Dump of the application-level settings.
			/// </summary>
			[Description("Application settings")]
			ApplicationSettings,

			/// <summary>
			///     Dump of the per-user settings.
			/// </summary>
			[Description("User settings")]
			UserSettings
		}

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Return the description of an enum item. If no description is specified, the variable's name is used instead.
		/// </summary>
		/// <param name="aValue"></param>
		/// <returns>String description of enum item</returns>
		public static string Description(Enum aValue)
		{
			var fieldInfo = aValue.GetType().GetField(aValue.ToString());

			var attributes =
				(DescriptionAttribute[]) fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);

			return (null != attributes) && (0 < attributes.Length) ? attributes[0].Description : aValue.ToString();
		}

		#endregion
	}
}

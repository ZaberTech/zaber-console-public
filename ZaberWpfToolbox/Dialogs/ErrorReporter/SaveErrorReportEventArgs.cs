﻿using System;

namespace ZaberWpfToolbox.Dialogs.ErrorReporter
{
	/// <summary>
	///     Custom event arguments for error reporting.
	/// </summary>
	public class SaveErrorReportEventArgs : EventArgs
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Carries report information to event handlers.
		/// </summary>
		public ErrorReportInformation Information { get; set; }

		#endregion
	}
}

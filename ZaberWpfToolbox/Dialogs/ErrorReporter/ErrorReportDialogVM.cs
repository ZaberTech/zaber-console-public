﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using MvvmDialogs.ViewModels;

namespace ZaberWpfToolbox.Dialogs.ErrorReporter
{
	/// <summary>
	///     ViewModel logic for ErrorReportDialog
	/// </summary>
	public class ErrorReportDialogVM : ObservableObject, IUserDialogViewModel, IDialogClient
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     ErrorReportDialogVM constructor.
		/// </summary>
		/// <param name="aInformation">ErrorReportInformation to be sent to Zaber.</param>
		/// <param name="aTriggeredByException">
		///     Specify if this dialog is being triggered as the result
		///     of an exception being thrown (true) or whether the user has chosen to manually submit the
		///     report (false).
		/// </param>
		public ErrorReportDialogVM(ErrorReportInformation aInformation, bool aTriggeredByException)
		{
			Information = aInformation;
			_triggeredByException = aTriggeredByException;
			ErrorReporterDialogOpen = true;
		}

		#endregion

		#region -- Public properties --

		/// <summary>
		///     Observer design pattern used to control the "Report a Problem..." button enabled state.
		///     If the error report dialog is open the button is disabled (and vice versa).
		/// </summary>
		public class ErrorReporterDialogOpenProvider : IObservable<bool>
		{
			#region -- Public Methods & Properties --

			/// <summary>
			///     ErrorReporterDialogOpenProvider constructor
			/// </summary>
			public ErrorReporterDialogOpenProvider()
			{
				observers = new HashSet<IObserver<bool>>();
			}


			/// <summary>
			///     Subscribe to an observer monitoring whether the dialog is open.
			/// </summary>
			/// <param name="observer">Observer monitoring dialog open/closed state.</param>
			/// <returns>Unsubscriber for observer.</returns>
			public IDisposable Subscribe(IObserver<bool> observer)
			{
				observers.Add(observer);
				return new Unsubscriber(observers, observer);
			}


			/// <summary>
			///     Notify observers that the dialog is open or closed.
			/// </summary>
			/// <param name="aIsErrorReportDialogOpen">Open (true), closed (false).</param>
			public void NotifyObservers(bool aIsErrorReportDialogOpen)
			{
				foreach (var observer in observers)
				{
					observer.OnNext(aIsErrorReportDialogOpen);
				}
			}

			#endregion

			#region -- Data --

			private class Unsubscriber : IDisposable
			{
				#region -- Public Methods & Properties --

				public Unsubscriber(HashSet<IObserver<bool>> observers, IObserver<bool> observer)
				{
					_observers = observers;
					_observer = observer;
				}


				public void Dispose()
				{
					if (null != _observer)
					{
						_observers.Remove(_observer);
					}
				}

				#endregion

				#region -- Data --

				private readonly HashSet<IObserver<bool>> _observers;
				private readonly IObserver<bool> _observer;

				#endregion
			}


			private readonly HashSet<IObserver<bool>> observers;

			#endregion
		}


		/// <summary>
		///     Set to specify whether the dialog is open or closed and notifies observers when set.
		/// </summary>
		public static bool ErrorReporterDialogOpen
		{
			get => _errorReporterDialogOpen;
			internal set
			{
				_errorReporterDialogOpen = value;
				ErrorReporterDialogOpenProviderInstance.NotifyObservers(_errorReporterDialogOpen);
			}
		}


		/// <summary>
		///     Message to be shown at the top of the error report dialog.
		/// </summary>
		public string ErrorReportMessage
		{
			get
			{
				var message = new StringBuilder();

				if (_triggeredByException)
				{
					message.Append("Zaber Console has encountered a problem and might need to be restarted. ");
				}

				message.Append("Please save your work and submit a report so we can fix the problem.");

				if ((null != Information.ErrorConditions) && (Information.ErrorConditions.Count() > 0))
				{
					message.AppendLine();
					message.AppendLine();
					if (Information.ErrorConditions.Count() > 1)
					{
						message.AppendLine("Error messages:");

						foreach (var msg in Information.ErrorConditions)
						{
							message.AppendLine("   " + msg);
						}
					}
					else
					{
						message.AppendLine("Error message: " + Information.ErrorConditions.First());
					}
				}

				return message.ToString();
			}
		}

		/// <summary>
		///     User-provided email address.
		/// </summary>
		public string Email
		{
			get => _email;
			set => Set(ref _email, value, nameof(Email));
		}


		/// <summary>
		///     User-provided error description.
		/// </summary>
		public string Description
		{
			get => _description;
			set => Set(ref _description, value, nameof(Description));
		}


		/// <summary>
		///     Generated error report information.
		/// </summary>
		public ErrorReportInformation Information
		{
			get => _information;
			set => Set(ref _information, value, nameof(Information));
		}


		/// <summary>
		///     Returns whether the dialog is modal.
		/// </summary>
		public bool IsModal => true;


		/// <summary>
		///     An instance of the ErrorReporterDialogOpenProvider used to notify observers when the dialog is opened or closed.
		/// </summary>
		public static ErrorReporterDialogOpenProvider ErrorReporterDialogOpenProviderInstance =
			new ErrorReporterDialogOpenProvider();

		/// <summary>
		///     Event handler to save the error report.
		/// </summary>
		public event EventHandler<SaveErrorReportEventArgs> SaveErrorReport;

		/// <summary>
		///     Event invoked when it is time to close the dialog.
		/// </summary>
		public event EventHandler DialogClosing;

		/// <summary>
		///     Event invoked to restore the dialog window and bring it to the front.
		/// </summary>
		public event EventHandler BringToFront;

		/// <summary>
		///     Event fired when the main window should open the supplied dialog.
		/// </summary>
		public event RequestDialogChange RequestDialogOpen;

		/// <summary>
		///     Event fired when the main window should close the supplied dialog.
		/// </summary>
		#pragma warning disable CS0067 // Event is never used.
		public event RequestDialogChange RequestDialogClose;
		#pragma warning restore CS0067


		/// <summary>
		///     Any exception that was unhandled by the dialog itself.
		/// </summary>
		public Exception Exception { get; set; }

		#endregion

		#region -- Public methods

		/// <summary>
		///     Close the dialog.
		/// </summary>
		public void RequestClose()
		{
			ErrorReporterDialogOpen = false;
			DialogClosing?.Invoke(this, null);
		}


		/// <summary>
		///     Bring dialog to front.
		/// </summary>
		public void RequestBringToFront() => BringToFront?.Invoke(this, null);

		#endregion

		#region -- ICommands --

		/// <summary>
		///     ICommand triggered when "See what this report contains" text is clicked.
		/// </summary>
		public ICommand SeeWhatThisReportContainsCommand => new RelayCommand(_ => SeeWhatThisReportContains());

		/// <summary>
		///     ICommand triggered when "Send" button is pressed.
		/// </summary>
		public ICommand SendCommand => new RelayCommand(_ => SubmitReport());

		/// <summary>
		///     ICommand triggered when "Don't send" button is pressed.
		/// </summary>
		public ICommand DontSendCommand => new RelayCommand(_ => RequestClose());

		#endregion

		#region -- Private helper methods --

		private void SeeWhatThisReportContains()
		{
			if (ErrorReportContentDialogVM.IsOpen)
			{
				return;
			}

			var report = new ErrorReportInformation
			{
				{ ErrorReportItems.Item.Email, Email },
				{ ErrorReportItems.Item.Description, Description }
			};
			report.AddRange(Information);

			var errorReportDialogContent = new ErrorReportContentDialogVM(report);
			ErrorReportContentDialogVM.IsOpen = true;
			errorReportDialogContent.DialogClosing += OnErrorReportDialogContnetClosing;
			RequestDialogOpen.Invoke(this, errorReportDialogContent);
		}


		private static void OnErrorReportDialogContnetClosing(object aSender, EventArgs aEventArgs)
			=> ErrorReportContentDialogVM.IsOpen = false;


		private void SubmitReport()
		{
			var report = new ErrorReportInformation
			{
				{ ErrorReportItems.Item.Email, Email },
				{ ErrorReportItems.Item.Description, Description }
			};
			report.AddRange(Information);

			var eventArgs = new SaveErrorReportEventArgs();
			eventArgs.Information = report;

			SaveErrorReport?.Invoke(this, eventArgs);

			RequestClose();
		}

		#endregion

		#region -- Private member data --

		private static bool _errorReporterDialogOpen;
		private readonly bool _triggeredByException;

		private string _email;
		private string _description;

		private ErrorReportInformation _information;

		#endregion
	}
}

﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using Microsoft.Win32;
using MvvmDialogs.ViewModels;

namespace ZaberWpfToolbox.Dialogs
{
	/// <summary>
	///     ViewModel for program About Boxes.
	/// </summary>
	public class AboutBoxVM : ObservableObject, IUserDialogViewModel, IDialogClient
	{
		#region -- Events --

		/// <summary>
		///     Event fired to cause the dialog to close.
		/// </summary>
		public event EventHandler DialogClosing;

		/// <summary>
		///     Event fired when a programmatic request is made to bring the dialog to the front.
		/// </summary>
		public event EventHandler BringToFront;

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Initialize the VM with default information about the program.
		/// </summary>
		public AboutBoxVM()
		{
			_dispatcher = Dispatcher.CurrentDispatcher;
			_assemblyHelper = new AssemblyInfoHelper();

			Title = _assemblyHelper.GetEntryAssemblyAttribute("title");
			WindowTitle = "About " + Title;
			Description = _assemblyHelper.GetEntryAssemblyAttribute("description");
			Version = "Version " + _assemblyHelper.GetEntryAssemblyAttribute("version");
			BuildDate = "Built on " + _assemblyHelper.GetEntryAssemblyAttribute("builddate");
			CopyrightMessage =
				$"Copyright © {DateTime.Now.Year.ToString()}, {_assemblyHelper.GetEntryAssemblyAttribute("company")}";

			_detailsWorker = new BackgroundWorker { WorkerSupportsCancellation = true };

			_detailsWorker.DoWork += (aSender, aArgs) => { PopulateDetailInfo(); };

			_detailsWorker.RunWorkerCompleted += (aSender, aArgs) => { _detailsWorker = null; };

			_detailsWorker.RunWorkerAsync();
		}


		/// <summary>
		///     Request that the dialog close. Just fires the DialogClosing event, which is
		///     handled by the dialog system.
		/// </summary>
		public void RequestClose()
		{
			if (null != _detailsWorker)
			{
				_detailsWorker.CancelAsync();
			}

			DialogClosing?.Invoke(this, null);
		}


		/// <summary>
		///     Call thsi to bring the dialog window to the front.
		/// </summary>
		public void RequestBringToFront() => BringToFront?.Invoke(this, null);


		// These properties do not need to fire OnPropertyChanged because they
		// are never changed after initialization.

		/// <summary>
		///     Title for the About Box window.
		///     The VM constructor initializes this with a default value.
		/// </summary>
		public string WindowTitle { get; set; }

		/// <summary>
		///     Name of the program.
		///     The VM constructor initializes this with a default value.
		/// </summary>
		public string Title { get; set; }

		/// <summary>
		///     Description of the program.
		///     The VM constructor initializes this with a default value.
		/// </summary>
		public string Description { get; set; }

		/// <summary>
		///     Version number of the program.
		///     The VM constructor initializes this with a default value.
		/// </summary>
		public string Version { get; set; }

		/// <summary>
		///     Compile date of the program.
		///     The VM constructor initializes this with a default value.
		/// </summary>
		public string BuildDate { get; set; }

		/// <summary>
		///     Copyright message about the program.
		///     The VM constructor initializes this with a default value.
		/// </summary>
		public string CopyrightMessage { get; set; }

		/// <summary>
		///     Program icon to display on the window decoration and next to the title.
		///     User must provide this.
		/// </summary>
		public ImageSource Icon { get; set; }

		/// <summary>
		///     Link to the release notes for the program version.
		/// </summary>
		public Uri ReleaseNotesUrl { get; set; }

		/// <summary>
		///     Information about the application.
		/// </summary>
		public ObservableCollection<Tuple<string, string>> ApplicationInfo
		{
			get => _applicationInfo;
			private set => Set(ref _applicationInfo, value, nameof(ApplicationInfo));
		}


		/// <summary>
		///     List of referenced assemblies.
		/// </summary>
		public ObservableCollection<AssemblyInfoHelper.AssemblyProperties> AssemblyList
		{
			get => _assemblyList;
			private set => Set(ref _assemblyList, value, nameof(AssemblyList));
		}


		/// <summary>
		///     Currently selected assembly.
		/// </summary>
		public AssemblyInfoHelper.AssemblyProperties SelectedAssembly
		{
			get => _selectedAssembly;
			set
			{
				Set(ref _selectedAssembly, value, nameof(SelectedAssembly));

				if (null != _selectedAssembly)
				{
					PopulateAssemblyDetails(_selectedAssembly.AssemblyName);
				}
				else
				{
					AssemblyInfo.Clear();
				}
			}
		}


		/// <summary>
		///     Information about the selected assembly.
		/// </summary>
		public ObservableCollection<Tuple<string, string>> AssemblyInfo
		{
			get => _assemblyInfo;
			private set => Set(ref _assemblyInfo, value, nameof(AssemblyInfo));
		}


		/// <summary>
		///     Controls which tab is currently displayed, when the tab control is visible.
		/// </summary>
		public int SelectedTabIndex
		{
			get => _selectedTabindex;
			set => Set(ref _selectedTabindex, value, nameof(SelectedTabIndex));
		}


		/// <summary>
		///     Set to true to force control focus to the combobox on the third tab.
		/// </summary>
		public bool ComboBoxHasFocus
		{
			get => _comboBoxHasFocus;
			set => Set(ref _comboBoxHasFocus, value, nameof(ComboBoxHasFocus));
		}


		/// <summary>
		///     Command invoked when the Launch button is pressed.
		/// </summary>
		public ICommand MSInfoCommand => new RelayCommand(_ => MSInfo());


		/// <summary>
		///     Command invoked when the Close button is pressed.
		/// </summary>
		public ICommand WindowClosingCommand => new RelayCommand(_ => RequestClose());


		/// <summary>
		///     Command invoked when a user double-clicks a row in the middle tab.
		/// </summary>
		public ICommand AssemblyDoubleClickCommand => new RelayCommand(row =>
		{
			_dispatcher.BeginInvoke(new Action(() =>
			{
				SelectedAssembly = row as AssemblyInfoHelper.AssemblyProperties;
				SelectedTabIndex = 2;
				ComboBoxHasFocus = true;
			}));
		});

		/// <summary>
		///     Required by IUserDialogViewModel - indicates this is a modal dialog window.
		/// </summary>
		public bool IsModal => true;


		/// <summary>
		///     Any exception that was unhandled by the dialog itself.
		/// </summary>
		public Exception Exception { get; set; }

		#endregion

		#region -- Non-Public Methods --

		// Switch to tab view on first click, then open system information window on second click.
		private void PopulateDetailInfo()
		{
			var assemblies = _assemblyHelper.PopulateAssemblies();

			_dispatcher.BeginInvoke(new Action(() =>
			{
				AssemblyList = new ObservableCollection<AssemblyInfoHelper.AssemblyProperties>(assemblies);
				SelectedAssembly = AssemblyList.FirstOrDefault();
			}));

			var appInfo = _assemblyHelper.PopulateAppInfo();

			_dispatcher.BeginInvoke(new Action(() =>
			{
				ApplicationInfo =
					new ObservableCollection<Tuple<string, string>>(appInfo);
			}));
		}


		private void MSInfo()
		{
			var strSysInfoPath = RegistryHklmValue(@"SOFTWARE\Microsoft\Shared Tools Location", "MSINFO");
			if (strSysInfoPath == "")
			{
				strSysInfoPath = RegistryHklmValue(@"SOFTWARE\Microsoft\Shared Tools\MSINFO", "PATH");
			}

			if (strSysInfoPath == "")
			{
				var mbvm = new MessageBoxParams
				{
					Message = "System Information is unavailable at this time."
						  + Environment.NewLine
						  + Environment.NewLine
						  + "(couldn't find path for Microsoft System Information Tool in the registry.)",
					Caption = WindowTitle,
					Buttons = MessageBoxButton.OK,
					Icon = MessageBoxImage.Warning
				};

				RequestDialogOpen?.Invoke(this, mbvm);
				return;
			}

			try
			{
				Process.Start(strSysInfoPath);
			}
			catch (Exception)
			{
				var mbvm = new MessageBoxParams
				{
					Message = "System Information is unavailable at this time."
						  + Environment.NewLine
						  + Environment.NewLine
						  + "(couldn't launch '"
						  + strSysInfoPath
						  + "')",
					Caption = WindowTitle,
					Buttons = MessageBoxButton.OK,
					Icon = MessageBoxImage.Stop
				};

				RequestDialogOpen?.Invoke(this, mbvm);
			}
		}

		#endregion

		#region -- IDialogClient implementation --

		/// <summary>
		///     Required by the IDialogClient interface. Fired to tell the
		///     dialog system to open a new dialog window.
		/// </summary>
		public event RequestDialogChange RequestDialogOpen;

		/// <summary>
		///     Required by the IDialogClient interface. Fired to tell the
		///     dialog system to close a currently open window.
		/// </summary>
		#pragma warning disable CS0067 // Event is never used.
		public event RequestDialogChange RequestDialogClose;
		#pragma warning restore CS0067

		#endregion

		#region -- Private functionality --

		// Gather data about a referenced assembly.
		private void PopulateAssemblyDetails(string aAssemblyName) => AssemblyInfo =
			new ObservableCollection<Tuple<string, string>>(_assemblyHelper.PopulateAssemblyDetails(aAssemblyName));


		// Helper for getting registry values.
		private static string RegistryHklmValue(string keyName, string subKeyRef)
		{
			try
			{
				var rk = Registry.LocalMachine.OpenSubKey(keyName);
				return (string) rk.GetValue(subKeyRef, "");
			}
			catch (Exception)
			{
				return "";
			}
		}

		#endregion

		#region -- Data --

		private BackgroundWorker _detailsWorker;
		private readonly Dispatcher _dispatcher;
		private readonly AssemblyInfoHelper _assemblyHelper;
		private int _selectedTabindex;
		private bool _comboBoxHasFocus;

		private ObservableCollection<Tuple<string, string>> _applicationInfo =
			new ObservableCollection<Tuple<string, string>>();

		private ObservableCollection<AssemblyInfoHelper.AssemblyProperties> _assemblyList =
			new ObservableCollection<AssemblyInfoHelper.AssemblyProperties>();

		private AssemblyInfoHelper.AssemblyProperties _selectedAssembly;

		private ObservableCollection<Tuple<string, string>> _assemblyInfo =
			new ObservableCollection<Tuple<string, string>>();

		#endregion
	}
}

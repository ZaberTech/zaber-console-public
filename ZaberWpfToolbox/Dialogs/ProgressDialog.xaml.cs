﻿namespace ZaberWpfToolbox.Dialogs
{
	/// <summary>
	///     Custom control for progress dialog boxes. See <see cref="ProgressDialogVM" />.
	/// </summary>
	public partial class ProgressDialog
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor. Initializes the component.
		/// </summary>
		public ProgressDialog()
		{
			InitializeComponent();
		}

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using MvvmDialogs.ViewModels;

namespace ZaberWpfToolbox.Dialogs
{
	/// <summary>
	///     Input parameters and outputs for file browser dialogs.
	/// </summary>
	public class FileBrowserDialogParams : IDialogViewModel
	{
		#region -- Public data --

		/// <summary>
		///     Dialog modes for <see cref="Action" />.
		/// </summary>
		public enum FileActionType
		{
			/// <summary>
			///     The user is being prompted to select file(s) to open.
			/// </summary>
			Open,

			/// <summary>
			///     The user is being propted to give a file name to save data to.
			/// </summary>
			Save
		}

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Creates a new instance with default values.
		/// </summary>
		public FileBrowserDialogParams()
		{
			Title = "Select File";
			Filter = "All files (*.*)|*.*";
			CheckFileExists = true;
		}


		/// <summary>
		///     Creates a new instance populated with data from user settings.
		///     Note the dialog never updates user settings itself; the application
		///     is responsible for deciding what to save.
		/// </summary>
		public FileBrowserDialogParams(FileBrowserUserSettings aSettings)
			: this()
		{
			InitialDirectory = aSettings.Directory;
			Filename = aSettings.Filename;
			FilterIndex = aSettings.FilterIndex;
		}


		/// <summary>
		///     Title for the file browser window.
		/// </summary>
		public string Title { get; set; }

		/// <summary>
		///     Initial directory to show.
		/// </summary>
		public string InitialDirectory { get; set; }

		/// <summary>
		///		Default directory to use if <see cref="InitialDirectory"/> doesn't exist.
		/// </summary>
		public string DefaultDirectory { get; set; } = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

		/// <summary>
		///     Default filter for file types.
		/// </summary>
		public string Filter { get; set; }

		/// <summary>
		///     Initially selected file type filter index, if there is more than one.
		/// </summary>
		public int FilterIndex { get; set; }

		/// <summary>
		///     Set to true to automatically add extensions to new filenames.
		/// </summary>
		public bool AddExtension { get; set; }

		/// <summary>
		///     Default filename extension to add if not specified by the user.
		/// </summary>
		public string DefaultExtension { get; set; }

		/// <summary>
		///     Set to false to allow entry of nonexistent filenames.
		/// </summary>
		public bool CheckFileExists { get; set; }

		/// <summary>
		///     When saving and the file exists, prompt the user to confirm overwrite.
		/// </summary>
		public bool PromptForOverwrite { get; set; } = true;

		/// <summary>
		///     Enable or disable selection of multiple files.
		/// </summary>
		public bool MultiSelect { get; set; }

		/// <summary>
		///     What type of operation the user is being prompted for - ie open
		///     or save. Controls some layout aspects of the dialog.
		/// </summary>
		public FileActionType Action { get; set; } = FileActionType.Open;

		/// <summary>
		///     Get or set a single filename when <c>MultiSelect</c> = false. This just returns the
		///     first element of <c>Filenames</c> or sets <c>Filenames</c> to a new list containing
		///     the provided value, which has the effect of having that file initially selected
		///     when the dialog opens.
		/// </summary>
		public string Filename
		{
			get => Filenames?.FirstOrDefault() ?? null;
			set
			{
				List<string> list = null;
				if (null != value)
				{
					list = new List<string>();
					list.Add(value);
				}

				Filenames = list;
			}
		}

		/// <summary>
		///     As an input, suggests filename to show initially. When the dialog closes, this
		///     is an output giving the user-selected filename(s). If MultiSelect = false, there
		///     will be at most one file name here.
		/// </summary>
		public IEnumerable<string> Filenames { get; set; }

		/// <summary>
		///     Result when the dialog closes. False if the user hit cancel, true if the user
		///     authorizes the operation, or null if the dialog was closed without choosing.
		/// </summary>
		public bool? DialogResult { get; set; }


		/// <summary>
		///     Any exception that was unhandled by the dialog itself.
		/// </summary>
		public Exception Exception { get; set; }

		#endregion
	}
}

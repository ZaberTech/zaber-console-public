﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Navigation;

namespace ZaberWpfToolbox.Dialogs
{
	/// <summary>
	///     Special window type for About Boxes.
	/// </summary>
	public partial class AboutBox : Window
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor. Intializes the component.
		/// </summary>
		public AboutBox()
		{
			InitializeComponent();
		}

		#endregion

		#region -- Non-Public Methods --

		private void Hyperlink_RequestNavigate(object aSender, RequestNavigateEventArgs aArgs)
			=> Process.Start(aArgs.Uri.ToString());

		#endregion
	}
}

﻿using System;
using MvvmDialogs.ViewModels;

namespace ZaberWpfToolbox.Dialogs
{
	/// <summary>
	///     Contains inputs and outputs for FolderBrowserDialogs.
	///     This is not an ObservableObject since values are not changed
	///     programmatically while the dialog is open.
	/// </summary>
	public class FolderBrowserDialogParams : IDialogViewModel
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor. Sets properties to default values.
		/// </summary>
		public FolderBrowserDialogParams()
		{
			Description = "Select Folder";
			ShowNewFolderButton = true;
			DialogResult = null;
		}


		/// <summary>
		///     Path selected by the user. Can be initialized before
		///     opening the dialog to set the initial folder.
		/// </summary>
		public string SelectedPath { get; set; }

		/// <summary>
		/// Default path to use if <see cref="SelectedPath"/> does not exist.
		/// </summary>
		public string DefaultPath { get; set; } = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

		/// <summary>
		///     Title for the dialog.
		/// </summary>
		public string Description { get; set; }

		/// <summary>
		///     Controls whether the user can create new folders via the dialog.
		///     Defaults to true.
		/// </summary>
		public bool ShowNewFolderButton { get; set; }

		/// <summary>
		///     Result returned by the dialog on closing. True to proceed,
		///     false to cancel, or null if the dialog was closed without choosing.
		/// </summary>
		public bool? DialogResult { get; set; }


		/// <summary>
		///     Any exception that was unhandled by the dialog itself.
		/// </summary>
		public Exception Exception { get; set; }

		#endregion
	}
}

﻿using MvvmDialogs.ViewModels;

namespace ZaberWpfToolbox.Dialogs
{
	/// <summary>
	/// Interface implemented by the top-level dialog manager class so it can
	/// pass itself into the constructor of non-VM objects that may need to
	/// open dialogs, without creating a dependency.
	/// </summary>
	public interface IDialogManager
	{
		/// <summary>
		/// Event handler type for the events in <cref>IDialogClient</cref>.
		/// </summary>
		/// <param name="aSender">ViewModel requesting the change.</param>
		/// <param name="aVM">ViewModel for the dialog to be changed.</param>
		void OpenDialog(object aSender, IDialogViewModel aVM);

		/// <summary>
		/// Event handler type for the events in <cref>IDialogClient</cref>.
		/// </summary>
		/// <param name="aSender">ViewModel requesting the change.</param>
		/// <param name="aVM">ViewModel for the dialog to be changed.</param>
		void CloseDialog(object aSender, IDialogViewModel aVM);
	}
}

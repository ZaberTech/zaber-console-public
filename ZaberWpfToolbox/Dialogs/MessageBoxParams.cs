﻿using System;
using System.Windows;
using MvvmDialogs.ViewModels;

namespace ZaberWpfToolbox.Dialogs
{
	/// <summary>
	///     Inputs used to invoke the system MessageBox via the MVVM pattern.
	///     Does not inherit from ObservableObject because message boxes have no
	///     interactable data; all visible data should be populated before the
	///     dialog is shown, and the result is not consumed until the dialog is closed.
	/// </summary>
	public class MessageBoxParams : IDialogViewModel
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor. Initializes properties to default values.
		/// </summary>
		public MessageBoxParams()
		{
			Caption = "Confirm Action";
			Buttons = MessageBoxButton.OKCancel;
			Icon = MessageBoxImage.Question;
			Result = MessageBoxResult.None;
		}


		/// <summary>
		///     Shortcut for creating an error notification message box, with only an OK button.
		/// </summary>
		/// <param name="aCaption">Window title for the dialog box.</param>
		/// <param name="aMessage">Message body.</param>
		/// <returns>A new instance of <see cref="MessageBoxParams" />.</returns>
		public static MessageBoxParams CreateErrorNotification(string aCaption, string aMessage) => new MessageBoxParams
		{
			Buttons = MessageBoxButton.OK,
			Caption = aCaption,
			Icon = MessageBoxImage.Exclamation,
			Message = aMessage
		};


		/// <summary>
		///     Shortcut for creating a yes/no dialog box.
		/// </summary>
		/// <param name="aCaption">Window title for the dialog box.</param>
		/// <param name="aMessage">Message body.</param>
		/// <returns>A new instance of <see cref="MessageBoxParams" />.</returns>
		public static MessageBoxParams CreateYesNoPrompt(string aCaption, string aMessage) => new MessageBoxParams
		{
			Buttons = MessageBoxButton.YesNo,
			Caption = aCaption,
			Icon = MessageBoxImage.Question,
			Message = aMessage
		};


		/// <summary>
		///     Shortcut for creating OK only notification dialog.
		/// </summary>
		/// <param name="aCaption">Window title for the dialog box.</param>
		/// <param name="aMessage">Message body.</param>
		/// <returns>A new instance of <see cref="MessageBoxParams" />.</returns>
		public static MessageBoxParams CreateInfoNotification(string aCaption, string aMessage) => new MessageBoxParams
		{
			Buttons = MessageBoxButton.OK,
			Caption = aCaption,
			Icon = MessageBoxImage.Information,
			Message = aMessage
		};


		/// <summary>
		///     The message to display.
		/// </summary>
		public string Message { get; set; }

		/// <summary>
		///     Window title. Defaults to "Confirm Action".
		/// </summary>
		public string Caption { get; set; }

		/// <summary>
		///     What buttons to show on the dialog. Defaults to OK and Cancel.
		/// </summary>
		public MessageBoxButton Buttons { get; set; }

		/// <summary>
		///     What icon to display in the dialog. Defaults to the question mark.
		/// </summary>
		public MessageBoxImage Icon { get; set; }

		/// <summary>
		///     Result code. Valid after the dialog is closed. Value depends on
		///     what button the user clicked. Defaults to None if the window was
		///     closed without clicking a button.
		/// </summary>
		public MessageBoxResult Result { get; set; }


		/// <summary>
		///     Any exception that was unhandled by the dialog itself.
		/// </summary>
		public Exception Exception { get; set; }

		#endregion
	}
}

﻿using System.IO;
using System.Windows.Forms;
using MvvmDialogs.Presenters;

namespace ZaberWpfToolbox.Dialogs
{
	/// <summary>
	///     MVVM presenter for system folder browser dialogs. This gets invoked via the
	///     DialogBehavior of a window when a FolderBrowserDialogParams is added to the window's
	///     dialog list.
	/// </summary>
	public class FolderBrowserDialogPresenter : IDialogBoxPresenter<FolderBrowserDialogParams>
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Displays a folder browser dialog and passes the results back to the associated ViewModel.
		/// </summary>
		/// <param name="aVM">
		///     A <cref>FolderBrowsserDialogParms</cref> containing inputs for the message
		///     box and a place to store the result code.
		/// </param>
		public void Show(FolderBrowserDialogParams aVM)
		{
			var initialPath = aVM.SelectedPath;
			if (string.IsNullOrEmpty(initialPath) || !Directory.Exists(initialPath))
			{
				initialPath = aVM.DefaultPath;
			}

			var dialog = new FolderBrowserDialog();
			dialog.SelectedPath = initialPath;
			dialog.Description = aVM.Description;
			dialog.ShowNewFolderButton = aVM.ShowNewFolderButton;
			var result = dialog.ShowDialog();
			if ((DialogResult.OK == result) || (DialogResult.Yes == result))
			{
				aVM.SelectedPath = dialog.SelectedPath;
				aVM.DialogResult = true;
			}
			else if ((DialogResult.Cancel == result) || (DialogResult.No == result))
			{
				aVM.DialogResult = false;
			}

			// Leave result indeterminate for other cases.
		}

		#endregion
	}
}

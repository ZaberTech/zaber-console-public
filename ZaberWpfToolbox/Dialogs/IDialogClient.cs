﻿using MvvmDialogs.ViewModels;

namespace ZaberWpfToolbox.Dialogs
{
	/// <summary>
	///     Event handler type for the events in <cref>IDialogClient</cref>.
	/// </summary>
	/// <param name="aSender">ViewModel requesting the change.</param>
	/// <param name="aVM">ViewModel for the dialog to be changed.</param>
	public delegate void RequestDialogChange(object aSender, IDialogViewModel aVM);

	/// <summary>
	///     ViewModels that want to show dialog boxes should implement this interface.
	///     The ViewModel for the main window should subscribe to these events in order to
	///     display dialogs on behalf of child controls, as the MvvmDialogs sub-library
	///     is specific to window-level dialog VM collections.
	/// </summary>
	public interface IDialogClient
	{
		#region -- Events --

		/// <summary>
		///     Implementing VM should fire this event when it wants the main window
		///     to open a dialog using the VM it supplies.
		/// </summary>
		event RequestDialogChange RequestDialogOpen;

		/// <summary>
		///     Implementing VM should fire this event if it wants to programmatically
		///     close the dialog it previously opened.
		/// </summary>
		event RequestDialogChange RequestDialogClose;

		#endregion
	}
}

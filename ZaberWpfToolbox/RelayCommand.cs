﻿using System;
using System.Windows.Input;

namespace ZaberWpfToolbox
{
	/// <summary>
	///     Implementation of ICommand used for many Zaber Console ViewModels.
	/// </summary>
	public class RelayCommand : ICommand
	{
		#region -- Events --

		/// <summary>
		///     Occurs when changes occur that affect whether or not the command should execute.
		/// </summary>
		public event EventHandler CanExecuteChanged
		{
			add
			{
				CommandManager.RequerySuggested += value;
				_canExecuteChangedRef += value;
			}
			remove
			{
				_canExecuteChangedRef -= value;
				CommandManager.RequerySuggested -= value;
			}
		}

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Initializes a new instance of the <see cref="RelayCommand" /> class.
		/// </summary>
		/// <param name="aExecute">The execute action.</param>
		/// <param name="aCanExecute">The can execute predicate.</param>
		public RelayCommand(Action<object> aExecute, Predicate<object> aCanExecute)
		{
			if (aCanExecute == null)
			{
				throw new ArgumentNullException("aCanExecute must not be null.");
			}

			if (aExecute == null)
			{
				throw new ArgumentNullException("aExecute must not be null.");
			}

			mCanExecutePredicate = aCanExecute;
			mExecuteAction = aExecute;
		}


		/// <summary>
		///     Initializes a new instance of the RelayCommand class that can always execute.
		/// </summary>
		/// <param name="aExecute">The execute action.</param>
		public RelayCommand(Action<object> aExecute)
			: this(aExecute, _ => true)
		{
		}


		/// <summary>
		///     Defines the method that determines whether the command can execute in its current state.
		/// </summary>
		/// <param name="aParam">
		///     Data used by the command.  If the command does not require data to be passed, this object can be
		///     set to null.
		/// </param>
		/// <returns>
		///     true if this command can be executed; otherwise, false.
		/// </returns>
		public bool CanExecute(object aParam) => mCanExecutePredicate(aParam);


		/// <summary>
		///     Defines the method to be called when the command is invoked.
		/// </summary>
		/// <param name="aParam">
		///     Data used by the command.  If the command does not require data to be passed, this object can be
		///     set to null.
		/// </param>
		public void Execute(object aParam) => mExecuteAction(aParam);


		/// <summary>
		///     Delierately invoke the <see cref="CanExecuteChanged" /> to cause associated controls to re-evaluate their enabled
		///     state.
		/// </summary>
		public void FireCanExecuteChanged() => _canExecuteChangedRef?.Invoke(this, EventArgs.Empty);


		/// <summary>
		///     An ICommand that can execute but does nothing, to be used as a default.
		/// </summary>
		public static ICommand DoNothing { get; } = new RelayCommand(_ => { });


		/// <summary>
		///     An ICommand that does nothing and cannot execute, to be used as a default.
		/// </summary>
		public static ICommand CanDoNothing { get; } = new RelayCommand(_ => { }, _ => false);

		#endregion

		#region -- Miscellaneous --

		// This is just for lifetime management and never actually fires.
		// See https://stackoverflow.com/questions/2281566/is-josh-smiths-implementation-of-the-relaycommand-flawed
		private event EventHandler _canExecuteChangedRef;

		#endregion

		#region -- Data --

		private readonly Predicate<object> mCanExecutePredicate;
		private readonly Action<object> mExecuteAction;

		#endregion
	}
}

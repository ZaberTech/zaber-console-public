﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using log4net;

namespace ZaberWpfToolbox
{
	/// <summary>
	///     Helper for gathering information about the assemblies used in the program.
	///     Used by the About Box VM for about box dialogs.
	///     This should work with any .NET program and will return information about the main
	///     assembly.
	/// </summary>
	public class AssemblyInfoHelper
	{
		#region -- Public data --

		/// <summary>
		///     Data structure for detailed assembly properties.
		/// </summary>
		public class AssemblyProperties
		{
			#region -- Public Methods & Properties --

			/// <summary>
			///     Combines the AssemblyName and Annotation strings.
			/// </summary>
			/// <returns>A pretty-printed name for the assembly.</returns>
			public override string ToString()
			{
				if (string.IsNullOrEmpty(Annotation))
				{
					return AssemblyName;
				}

				return AssemblyName + Annotation;
			}


			/// <summary>
			///     Base name of the assembly.
			/// </summary>
			public string AssemblyName { get; set; }

			/// <summary>
			///     Assembly version string.
			/// </summary>
			public string Version { get; set; }

			/// <summary>
			///     Compile date of the assembly.
			/// </summary>
			public string BuildDate { get; set; }

			/// <summary>
			///     Disk path to the assembly .DLL or .EXE.
			/// </summary>
			public string Location { get; set; }

			/// <summary>
			///     Annotations about the assembly - ie whether it is
			///     the executing or calling assembly.
			/// </summary>
			public string Annotation { get; set; }

			#endregion
		}

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Initialize an instance and populate data about the main assembly.
		/// </summary>
		public AssemblyInfoHelper()
		{
			_appEntryAssembly = Assembly.GetEntryAssembly();

			if (_appEntryAssembly == null)
			{
				_appEntryAssembly = Assembly.GetExecutingAssembly();
			}

			_executingAssemblyName = Assembly.GetExecutingAssembly().GetName().Name;
			_callingAssemblyName = Assembly.GetCallingAssembly().GetName().Name;
			try
			{
				// for web hosted apps, GetEntryAssembly = nothing
				_entryAssemblyName = Assembly.GetEntryAssembly().GetName().Name;
			}
			catch (Exception)
			{
			}

			// get entry assembly attribs
			_entryAssemblyAttribCollection = AssemblyAttribs(_appEntryAssembly);
		}


		/// <summary>
		///     Retrieve all known attribute names for the entry assembly. These are all
		///     the keys that will return valid values when passed to GetEntryAssemblyAttribute().
		/// </summary>
		/// <returns>Set of all valid attribute names from the entry assembly.</returns>
		public IEnumerable<string> GetEntryAssemblyAttributeNames()
			=> new List<string>(_entryAssemblyAttribCollection.AllKeys);


		/// <summary>
		///     Get an attribute of the main assembly by name. There is a
		///     fixed list of recognized names - call GetEntryAssemblyAttributeNames
		///     to list them.
		/// </summary>
		/// <param name="aName">Name of the attribute to retrieve.</param>
		/// <returns>Value of the attribute, or a default string if not found.</returns>
		public string GetEntryAssemblyAttribute(string aName)
		{
			if (_entryAssemblyAttribCollection[aName] == null)
			{
				return "<Assembly: Assembly" + aName + "(\"\")>";
			}

			return _entryAssemblyAttribCollection[aName];
		}


		/// <summary>
		///     Gets information about the application's domain.
		/// </summary>
		/// <returns>A list of key-value pairs harvested from AppDomain.CurrentDomain.</returns>
		public IEnumerable<Tuple<string, string>> PopulateAppInfo()
		{
			var result = new List<Tuple<string, string>>();
			var dom = AppDomain.CurrentDomain;

			result.Add(new Tuple<string, string>("Application Name", dom.SetupInformation.ApplicationName));
			result.Add(new Tuple<string, string>("Application Base", dom.SetupInformation.ApplicationBase));
			result.Add(new Tuple<string, string>("Cache Path", dom.SetupInformation.CachePath));
			result.Add(new Tuple<string, string>("Configuration File", dom.SetupInformation.ConfigurationFile));
			result.Add(new Tuple<string, string>("Dynamic Base", dom.SetupInformation.DynamicBase));
			result.Add(new Tuple<string, string>("Friendly Name", dom.FriendlyName));
			result.Add(new Tuple<string, string>("License File", dom.SetupInformation.LicenseFile));
			result.Add(new Tuple<string, string>("Private Bin Path", dom.SetupInformation.PrivateBinPath));
			result.Add(new Tuple<string, string>("Shadow Copy Directories",
												 dom.SetupInformation.ShadowCopyDirectories));
			result.Add(new Tuple<string, string>(" ", " "));
			result.Add(new Tuple<string, string>("Entry Assembly", _entryAssemblyName));
			result.Add(new Tuple<string, string>("Executing Assembly", _executingAssemblyName));
			result.Add(new Tuple<string, string>("Calling Assembly", _callingAssemblyName));

			return result;
		}


		/// <summary>
		///     Gathers information about all assemblies in use by the current application.
		/// </summary>
		/// <returns>a set of AssemblyProperties data structures about all active assemblies.</returns>
		public IEnumerable<AssemblyProperties> PopulateAssemblies()
		{
			var result = new List<AssemblyProperties>();

			foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
			{
				try
				{
					result.Add(PopulateAssemblySummary(a));
				}
				catch (FileLoadException aException)
				{
					_log.Warn($"Failed to populate assembly information for {a.Location}.", aException);
				}
			}

			result.Sort((a, b) => { return a.AssemblyName.CompareTo(b.AssemblyName); });

			return result;
		}


		/// <summary>
		///     Get detailed properties of one assembly, by assembly name.
		/// </summary>
		/// <param name="aAssemblyName">The base name of the assembly to query.</param>
		/// <returns>A set of key-value pairs containing data about the assembly.</returns>
		public IEnumerable<Tuple<string, string>> PopulateAssemblyDetails(string aAssemblyName)
		{
			var result = new List<Tuple<string, string>>();

			var asm = MatchAssemblyByName(aAssemblyName);
			if (asm != null)
			{
				// this assembly property is only available in framework versions 1.1+
				result.Add(new Tuple<string, string>("Image Runtime Version", asm.ImageRuntimeVersion));
				result.Add(new Tuple<string, string>("Loaded from GAC", asm.GlobalAssemblyCache.ToString()));

				var nvc = AssemblyAttribs(asm);
				foreach (string strKey in nvc)
				{
					result.Add(new Tuple<string, string>(strKey, nvc[strKey]));
				}
			}

			return result;
		}

		#endregion

		#region -- Non-Public Methods --

		// <summary>
		// returns string name / string value pair of all attribs
		// for specified assembly
		// </summary>
		// <remarks>
		// note that Assembly* values are pulled from AssemblyInfo file in project folder
		//
		// Trademark       = AssemblyTrademark string
		// Debuggable      = true
		// GUID            = 7FDF68D5-8C6F-44C9-B391-117B5AFB5467
		// CLSCompliant    = true
		// Product         = AssemblyProduct string
		// Copyright       = AssemblyCopyright string
		// Company         = AssemblyCompany string
		// Description     = AssemblyDescription string
		// Title           = AssemblyTitle string
		// </remarks>
		private static NameValueCollection AssemblyAttribs(Assembly aAsm)
		{
			var nvc = new NameValueCollection();
			var r = new Regex(@"(\.Assembly|\.)(?<Name>[^.]*)Attribute$", RegexOptions.IgnoreCase);

			foreach (var attrib in aAsm.GetCustomAttributes(false))
			{
				var typeName = attrib.GetType().ToString();
				var name = r.Match(typeName).Groups["Name"].ToString();
				string value;

				switch (typeName)
				{
					case "System.CLSCompliantAttribute":
						value = ((CLSCompliantAttribute) attrib).IsCompliant.ToString();
						break;
					case "System.Diagnostics.DebuggableAttribute":
						value = ((DebuggableAttribute) attrib).IsJITTrackingEnabled.ToString();
						break;
					case "System.Reflection.AssemblyCompanyAttribute":
						value = ((AssemblyCompanyAttribute) attrib).Company;
						break;
					case "System.Reflection.AssemblyConfigurationAttribute":
						value = ((AssemblyConfigurationAttribute) attrib).Configuration;
						break;
					case "System.Reflection.AssemblyCopyrightAttribute":
						value = ((AssemblyCopyrightAttribute) attrib).Copyright;
						break;
					case "System.Reflection.AssemblyDefaultAliasAttribute":
						value = ((AssemblyDefaultAliasAttribute) attrib).DefaultAlias;
						break;
					case "System.Reflection.AssemblyDelaySignAttribute":
						value = ((AssemblyDelaySignAttribute) attrib).DelaySign.ToString();
						break;
					case "System.Reflection.AssemblyDescriptionAttribute":
						value = ((AssemblyDescriptionAttribute) attrib).Description;
						break;
					case "System.Reflection.AssemblyInformationalVersionAttribute":
						value = ((AssemblyInformationalVersionAttribute) attrib).InformationalVersion;
						break;
					case "System.Reflection.AssemblyKeyFileAttribute":
						value = ((AssemblyKeyFileAttribute) attrib).KeyFile;
						break;
					case "System.Reflection.AssemblyProductAttribute":
						value = ((AssemblyProductAttribute) attrib).Product;
						break;
					case "System.Reflection.AssemblyTrademarkAttribute":
						value = ((AssemblyTrademarkAttribute) attrib).Trademark;
						break;
					case "System.Reflection.AssemblyTitleAttribute":
						value = ((AssemblyTitleAttribute) attrib).Title;
						break;
					case "System.Resources.NeutralResourcesLanguageAttribute":
						value = ((NeutralResourcesLanguageAttribute) attrib).CultureName;
						break;
					case "System.Resources.SatelliteContractVersionAttribute":
						value = ((SatelliteContractVersionAttribute) attrib).Version;
						break;
					case "System.Runtime.InteropServices.ComCompatibleVersionAttribute":
					{
						ComCompatibleVersionAttribute x;
						x = (ComCompatibleVersionAttribute) attrib;
						value = x.MajorVersion + "." + x.MinorVersion + "." + x.RevisionNumber + "." + x.BuildNumber;
						break;
					}
					case "System.Runtime.InteropServices.ComVisibleAttribute":
						value = ((ComVisibleAttribute) attrib).Value.ToString();
						break;
					case "System.Runtime.InteropServices.GuidAttribute":
						value = ((GuidAttribute) attrib).Value;
						break;
					case "System.Runtime.InteropServices.TypeLibVersionAttribute":
					{
						TypeLibVersionAttribute x;
						x = (TypeLibVersionAttribute) attrib;
						value = x.MajorVersion + "." + x.MinorVersion;
						break;
					}
					case "System.Security.AllowPartiallyTrustedCallersAttribute":
						value = "(Present)";
						break;
					default:

						// debug.writeline("** unknown assembly attribute '" + TypeName + "'")
						value = typeName;
						break;
				}

				if (nvc[name] == null)
				{
					nvc.Add(name, value);
				}
			}

			// add some extra values that are not in the AssemblyInfo, but nice to have
			// codebase
			try
			{
				nvc.Add("CodeBase", aAsm.CodeBase.Replace("file:///", ""));
			}
			catch (NotSupportedException)
			{
				nvc.Add("CodeBase", "(not supported)");
			}

			// build date
			var dt = AssemblyBuildDate(aAsm, false);
			if (dt == DateTime.MaxValue)
			{
				nvc.Add("BuildDate", "(unknown)");
			}
			else
			{
				nvc.Add("BuildDate", dt.ToString("yyyy-MM-dd hh:mm tt"));
			}

			// location
			try
			{
				nvc.Add("Location", aAsm.Location);
			}
			catch (NotSupportedException)
			{
				nvc.Add("Location", "(not supported)");
			}

			// version
			try
			{
				if ((aAsm.GetName().Version.Major == 0) && (aAsm.GetName().Version.Minor == 0))
				{
					nvc.Add("Version", "(unknown)");
				}
				else
				{
					nvc.Add("Version", aAsm.GetName().Version.ToString());
				}
			}
			catch (Exception)
			{
				nvc.Add("Version", "(unknown)");
			}

			nvc.Add("FullName", aAsm.FullName);

			return nvc;
		}


		// <summary>
		// returns DateTime this Assembly was last built. Will attempt to calculate from build number, if possible. 
		// If not, the actual LastWriteTime on the assembly file will be returned.
		// </summary>
		// <param name="a">Assembly to get build date for</param>
		// <param name="ForceFileDate">Don't attempt to use the build number to calculate the date</param>
		// <returns>DateTime this assembly was last built</returns>
		private static DateTime AssemblyBuildDate(Assembly aAsm, bool aForceFileDate)
		{
			var assemblyVersion = aAsm.GetName().Version;
			DateTime dt;

			if (aForceFileDate)
			{
				dt = AssemblyLastWriteTime(aAsm);
			}
			else
			{
				dt = DateTime.Parse("01/01/2000", CultureInfo.InvariantCulture)
						  .AddDays(assemblyVersion.Build)
						  .AddSeconds(assemblyVersion.Revision * 2);
				if (TimeZone.IsDaylightSavingTime(dt, TimeZone.CurrentTimeZone.GetDaylightChanges(dt.Year)))
				{
					dt = dt.AddHours(1);
				}

				if ((dt > DateTime.Now) || (assemblyVersion.Build < 730) || (assemblyVersion.Revision == 0))
				{
					dt = AssemblyLastWriteTime(aAsm);
				}
			}

			return dt;
		}


		// <summary>
		// exception-safe retrieval of LastWriteTime for this assembly.
		// </summary>
		// <returns>File.GetLastWriteTime, or DateTime.MaxValue if exception was encountered.</returns>
		private static DateTime AssemblyLastWriteTime(Assembly aAsm)
		{
			try
			{
				if (string.IsNullOrEmpty(aAsm.Location))
				{
					return DateTime.MaxValue;
				}

				return File.GetLastWriteTime(aAsm.Location);
			}
			catch (Exception)
			{
				return DateTime.MaxValue;
			}
		}


		// <summary>
		// populate Assembly Information listview with summary view for a specific assembly
		// </summary>
		private AssemblyProperties PopulateAssemblySummary(Assembly aAsm)
		{
			var nvc = AssemblyAttribs(aAsm);

			var strAssemblyName = aAsm.GetName().Name;

			var lvi = new AssemblyProperties();
			lvi.AssemblyName = strAssemblyName;

			if (strAssemblyName == _callingAssemblyName)
			{
				lvi.Annotation += " (calling)";
			}

			if (strAssemblyName == _executingAssemblyName)
			{
				lvi.Annotation += " (executing)";
			}

			if (strAssemblyName == _entryAssemblyName)
			{
				lvi.Annotation += " (entry)";
			}

			lvi.Version = nvc["version"];
			lvi.BuildDate = nvc["builddate"];
			lvi.Location = nvc["codebase"];

			return lvi;
		}


		// <summary>
		// matches assembly by Assembly.GetName.Name; returns nothing if no match
		// </summary>
		private static Assembly MatchAssemblyByName(string aAssemblyName)
		{
			foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
			{
				if (a.GetName().Name == aAssemblyName)
				{
					return a;
				}
			}

			return null;
		}

		#endregion

		#region -- Data --

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private readonly string _entryAssemblyName;
		private readonly string _callingAssemblyName;
		private readonly string _executingAssemblyName;
		private readonly Assembly _appEntryAssembly;
		private readonly NameValueCollection _entryAssemblyAttribCollection;

		#endregion
	}
}

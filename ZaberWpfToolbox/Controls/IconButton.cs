﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ZaberWpfToolbox.Controls
{
	/// <summary>
	///     Icon Button - button subclass that only displays a small icon.
	/// </summary>
	public class IconButton : Button
	{
		#region -- Public data --

		/// <summary>
		///     Enables setting the icon via URI. Changing this populates <see cref="Icon" /> from the URI.
		/// </summary>
		public static readonly DependencyProperty IconUriProperty =
			DependencyProperty.RegisterAttached("IconUri",
												typeof(Uri),
												typeof(IconButton),
												new FrameworkPropertyMetadata(null, OnIconUriChanged));


		/// <summary>
		///     Enables setting the icon as an ImageBrush. Changing this directly does not clear the value of
		///     <see cref="IconUri" />.
		/// </summary>
		public static readonly DependencyProperty IconProperty =
			DependencyProperty.RegisterAttached("Icon",
												typeof(ImageBrush),
												typeof(IconButton));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Enables local style override since this control inherits directly from a standard control.
		/// </summary>
		static IconButton()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(IconButton),
													 new FrameworkPropertyMetadata(typeof(IconButton)));
		}


		/// <summary>
		///     Get or set the icon URI.
		/// </summary>
		public Uri IconUri
		{
			get => GetValue(IconUriProperty) as Uri;
			set => SetValue(IconUriProperty, value);
		}


		/// <summary>
		///     Get or set the help content.
		/// </summary>
		public ImageBrush Icon
		{
			get => GetValue(IconProperty) as ImageBrush;
			set => SetValue(IconProperty, value);
		}

		#endregion

		#region -- Non-Public Methods --

		private static void OnIconUriChanged(object aSender, DependencyPropertyChangedEventArgs aArgs)
		{
			var me = aSender as IconButton;
			if (null != me)
			{
				ImageBrush img = null;
				var uri = aArgs.NewValue as Uri;
				if (null != uri)
				{
					img = new ImageBrush(new BitmapImage(uri));
				}

				me.Icon = img;
			}
		}

		#endregion
	}
}

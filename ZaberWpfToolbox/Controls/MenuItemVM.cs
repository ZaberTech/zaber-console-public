﻿using System.Windows.Input;

namespace ZaberWpfToolbox.Controls
{
	/// <summary>
	///     ViewModel for menu items.
	/// </summary>
	public class MenuItemVM : ObservableObject
	{
		/// <summary>
		///		Menu item text.
		/// </summary>
		public string Title
		{
			get => _title;
			set => Set(ref _title, value, nameof(Title));
		}


		/// <summary>
		///		Tooltip for the menu item and icon.
		/// </summary>
		public string ToolTip
		{
			get => _toolTip;
			set => Set(ref _toolTip, value, nameof(ToolTip));
		}


		/// <summary>
		///		URL for the menu item's icon, if any.
		/// </summary>
		public string IconUrl
		{
			get => _iconUrl;
			set => Set(ref _iconUrl, value, nameof(IconUrl));
		}


		/// <summary>
		///		Command invoked by the menu item.
		/// </summary>
		public ICommand Command
		{
			get => _command;
			set => Set(ref _command, value, nameof(Command));
		}


		/// <summary>
		///		Optional parameter for the menu item's command.
		/// </summary>
		public object CommandParameter
		{
			get => _commandParameter;
			set => Set(ref _commandParameter, value, nameof(CommandParameter));
		}


		private string _title;
		private string _toolTip;
		private string _iconUrl;
		private ICommand _command;
		private object _commandParameter;
	}
}

﻿using System.Windows;

namespace ZaberWpfToolbox.Controls
{
	/// <summary>
	///     Interaction logic for HelpButton.xaml
	/// </summary>
	public partial class HelpButton
	{
		#region -- Public data --

		/// <summary>
		///     Bind HelpContent to the content to be displayed in the popup when the button is clicked.
		///     This can be a string or controls, such as a TextBlock with link spans to open additional help.
		/// </summary>
		public static readonly DependencyProperty HelpContentProperty =
			DependencyProperty.RegisterAttached("HelpContent",
												typeof(object),
												typeof(HelpButton));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Initializer for the help button control.
		/// </summary>
		public HelpButton()
		{
			InitializeComponent();
		}


		/// <summary>
		///     Get or set the help content.
		/// </summary>
		public object HelpContent
		{
			get => GetValue(HelpContentProperty);
			set => SetValue(HelpContentProperty, value);
		}

		#endregion

		#region -- Non-Public Methods --

		private void OnClick(object aSender, RoutedEventArgs aArgs)
		{
			_popupContent.Content = HelpContent;
			_popup.IsOpen = true;
		}

		#endregion
	}
}

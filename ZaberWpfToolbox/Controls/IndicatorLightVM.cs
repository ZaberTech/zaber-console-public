﻿using System;
using System.Windows.Media;

namespace ZaberWpfToolbox.Controls
{
	/// <summary>
	///     Base ViewModel for the indicator light control, which simulates a flashing LED.
	/// </summary>
	public class IndicatorLightVM : ObservableObject
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Intialize the VM with default values.
		/// </summary>
		public IndicatorLightVM()
		{
			_dispatcher = DispatcherHelper.Dispatcher;
			_timer = DispatcherTimerHelper.CreateTimer();
			_timer.IsEnabled = false;
			LitPeriod = UnlitPeriod = 250;
			_timer.Interval = new TimeSpan(0, 0, 0, 0, LitPeriod);
			_timer.Tick += _timer_Tick;
		}


		/// <summary>
		///     Turn the light on or off. If FlashWhenOn is set, will start the light flashing.
		///     Otherwise will turn the light on momentarily or continuously depending on the
		///     value of LitPeriod.
		/// </summary>
		public bool On
		{
			get => _on;
			set
			{
				_on = value;
				_dispatcher.BeginInvoke(() =>
				{
					Lit = value;
					_timer.Stop();

					if (value && (LitPeriod > 0))
					{
						_timer.Interval = new TimeSpan(0, 0, 0, 0, LitPeriod);
						_timer.IsEnabled = true;
						_timer.Start();
					}
				});
			}
		}


		/// <summary>
		///     The duration the light will remain lit after Lit is set to true, in milliseconds.
		///     Set to zero to make the light stay on continuously as long as On is true.
		///     Default is 250mS.
		/// </summary>
		public int LitPeriod { get; set; }


		/// <summary>
		///     The duration the light will remain dark during each flashing period.
		///     Default is 250mS.
		/// </summary>
		public int UnlitPeriod { get; set; }


		/// <summary>
		///     Set to true to flash the light continuously with the duty cycle defined by
		///     LitPeriod and DarkPeriod.
		/// </summary>
		public bool FlashWhenOn { get; set; }

		/// <summary>
		///     Whether on not the light is currently on. Do not change this value directly;
		///     use On instead.
		/// </summary>
		public bool Lit
		{
			get => _lit;
			set => Set(ref _lit, value, nameof(Lit));
		}


		/// <summary>
		///     Text to display beside the light.
		/// </summary>
		public string Text
		{
			get => _text;
			set => Set(ref _text, value, nameof(Text));
		}


		/// <summary>
		///     Tooltip for the light.
		/// </summary>
		public string ToolTip
		{
			get => _toolTip;
			set => Set(ref _toolTip, value, nameof(ToolTip));
		}


		/// <summary>
		///     The color of the light when it is not lit.
		///     The default is black.
		/// </summary>
		public Color UnlitColor
		{
			get => _unlitColor;
			set => Set(ref _unlitColor, value, nameof(UnlitColor));
		}


		/// <summary>
		///     The color of the light when it is lit.
		///     The default is lime green.
		/// </summary>
		public Color LitColor
		{
			get => _litColor;
			set => Set(ref _litColor, value, nameof(LitColor));
		}

		#endregion

		#region -- Non-Public Methods --

		private void _timer_Tick(object sender, EventArgs e)
		{
			if (!On)
			{
				_timer.IsEnabled = false;
				_timer.Stop();
				Lit = false;
				return;
			}

			if (!FlashWhenOn)
			{
				// Turn off after a single flash.
				if (Lit)
				{
					Lit = false;
					_on = false;
					_timer.IsEnabled = false;
					_timer.Stop();
				}
			}
			else
			{
				// Toggle between lit and unlit.
				if (Lit)
				{
					Lit = false;
					_timer.Interval = new TimeSpan(0, 0, 0, 0, UnlitPeriod);
				}
				else
				{
					Lit = true;
					_timer.Interval = new TimeSpan(0, 0, 0, 0, LitPeriod);
				}
			}
		}

		#endregion

		#region -- Data --

		private bool _on;
		private bool _lit;
		private string _text = string.Empty;
		private string _toolTip = string.Empty;
		private Color _unlitColor = Colors.Black;
		private Color _litColor = Colors.Lime;
		private readonly IDispatcher _dispatcher;
		private readonly IDispatcherTimer _timer;

		#endregion
	}
}

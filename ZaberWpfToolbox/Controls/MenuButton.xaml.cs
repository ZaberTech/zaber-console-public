﻿namespace ZaberWpfToolbox.Controls
{
	/// <summary>
	/// Interaction logic for MenuButton.xaml
	/// </summary>
	public partial class MenuButton
	{
		/// <summary>
		///     Initializes styles for this component.
		/// </summary>
		public MenuButton()
		{
			InitializeComponent();
		}
	}
}

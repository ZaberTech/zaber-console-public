﻿namespace ZaberWpfToolbox.Controls
{
	/// <summary>
	///     Interaction logic for IndicatorLight.xaml
	/// </summary>
	public partial class IndicatorLight : BaseControl
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor. Initializes the component.
		/// </summary>
		public IndicatorLight()
		{
			InitializeComponent();
		}

		#endregion
	}
}

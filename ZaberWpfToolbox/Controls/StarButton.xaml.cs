﻿using System.Windows;

namespace ZaberWpfToolbox.Controls
{
	/// <summary>
	///     Interaction logic for StarButton.xaml
	/// </summary>
	public partial class StarButton
	{
		#region -- Public data --

		/// <summary>
		///     Attached property for whether or not the star is lit.
		/// </summary>
		public static readonly DependencyProperty StarredProperty =
			DependencyProperty.RegisterAttached("Starred",
												typeof(bool),
												typeof(StarButton),
												new UIPropertyMetadata(false, OnStarredChanged));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Type initalizer. Enables style pass-through since this doesn't inherit from BaseControl.
		/// </summary>
		static StarButton()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(StarButton),
													 new FrameworkPropertyMetadata(typeof(StarButton)));
		}


		/// <summary>
		///     Default constructor. Intializes the component.
		/// </summary>
		public StarButton()
		{
			InitializeComponent();
			_hollowStar.Visibility = Visibility.Visible;
			_filledStar.Visibility = Visibility.Hidden;
		}


		/// <summary>
		///     Get or set the star's lit state.
		/// </summary>
		public bool Starred
		{
			get => (bool) GetValue(StarredProperty);
			set => SetValue(StarredProperty, value);
		}

		#endregion

		#region -- Non-Public Methods --

		private static void OnStarredChanged(DependencyObject aControl, DependencyPropertyChangedEventArgs aArgs)
		{
			var button = aControl as StarButton;
			if (null == button)
			{
				return;
			}

			var starred = (bool) aArgs.NewValue;
			if (starred)
			{
				button._hollowStar.Visibility = Visibility.Hidden;
				button._filledStar.Visibility = Visibility.Visible;
			}
			else
			{
				button._filledStar.Visibility = Visibility.Hidden;
				button._hollowStar.Visibility = Visibility.Visible;
			}
		}

		#endregion
	}
}

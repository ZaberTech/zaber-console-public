using System.Collections.ObjectModel;

namespace ZaberWpfToolbox.Controls
{
	/// <summary>
	/// This is the base viewmodel class for devices used in the Device List control. 
	/// It contains all the properties used by the view,
	/// but none of the logic since that would introduce a dependency on types from Zaber.dll.
	/// </summary>
	public class BaseDeviceVM : ObservableObject
	{
		#region -- View-bound properties --

		/// <summary>
		/// List of all units of measure applicable to this device.
		/// </summary>
		public ObservableCollection<object> UnitsOfMeasure
		{
			get { return _applicableUnits; }
			set
			{
				Set(ref _applicableUnits, value, () => UnitsOfMeasure);
			}
		}


		/// <summary>
		/// The currently selected unit of measure.
		/// </summary>
		public object SelectedUnit
		{
			get { return _selectedUnit; }
			set
			{
				Set(ref _selectedUnit, value, () => SelectedUnit);
			}
		}


		/// <summary>
		/// The device number (its address in the serial chain).
		/// </summary>
		public string DeviceNumber
		{
			get { return _deviceNumber; }
			set
			{
				Set(ref _deviceNumber, value, () => DeviceNumber);
			}
		}


		/// <summary>
		/// A human-readable string describing the device.
		/// </summary>
		public string DeviceType
		{
			get { return _deviceType; }
			set
			{
				Set(ref _deviceType, value, () => DeviceType);
			}
		}


		/// <summary>
		/// Error message(s) related to this device. Displayed as a ToolTip.
		/// </summary>
		public string ErrorMessage
		{
			get { return _errorMessage; }
			set
			{
				Set(ref _errorMessage, value, () => ErrorMessage);
			}
		}


		public string Notification
		{
			get { return _notification; }
			set
			{
				Set(ref _notification, value, () => Notification);
			}
		} 


		public bool DisplayNotification
		{
			get { return _displayNotification; }
			set
			{
				Set(ref _displayNotification, value, () => DisplayNotification);
			}
		}


		/// <summary>
		/// The current position of the device, if known.
		/// </summary>
		public string Position
		{
			get { return _position; }
			set
			{
				Set(ref _position, value, () => Position);
			}
		}

		#endregion
		#region -- Data members --

		private ObservableCollection<object> _applicableUnits;
		private string _deviceNumber;
		private string _deviceType;
		private string _errorMessage;
		private string _notification;
		private bool _displayNotification;
		private string _position;
		private object _selectedUnit;

		#endregion
	}
}

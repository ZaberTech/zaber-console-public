﻿using System.Collections.ObjectModel;

namespace ZaberWpfToolbox.Controls
{
	/// <summary>
	///     ViewModel for the MenuButton control - an icon that shows a
	///     flat menu of commands when clicked.
	/// </summary>
	public class MenuButtonVM : ObservableObject
	{
		/// <summary>
		///     Path to image to use as the button icon.
		/// </summary>
		public string IconUrl
		{
			get => _iconUrl;
			set => Set(ref _iconUrl, value, nameof(IconUrl));
		}


		/// <summary>
		///     Tooltip message for the button.
		/// </summary>
		public string ToolTip
		{
			get => _toolTip;
			set => Set(ref _toolTip, value, nameof(ToolTip));
		}


		/// <summary>
		///     Controls whether the alert dot is drawn over the icon.
		/// </summary>
		public bool ShowAlert
		{
			get => _showAlert;
			set => Set(ref _showAlert, value, nameof(ShowAlert));
		}


		/// <summary>
		///     Items in the context menu.
		/// </summary>
		public ObservableCollection<MenuItemVM> MenuItems
		{
			get => _menuItems;
			set => Set(ref _menuItems, value, nameof(MenuItems));
		}


		private string _iconUrl;
		private bool _showAlert;
		private string _toolTip;
		private ObservableCollection<MenuItemVM> _menuItems = new ObservableCollection<MenuItemVM>();
	}
}

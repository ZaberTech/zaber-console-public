﻿using System.Windows;
using System.Windows.Forms;

namespace ZaberWpfToolbox.Controls
{
	/// <summary>
	///     Helper control to enable embedding WinForms controls in a WPF window in an
	///     MVVM-compatible way. Basically just enables setting the WindoewsFormHost's
	///     Child property via a data binding.
	/// </summary>
	public partial class WinFormsHostHelper
	{
		#region -- Public data --

		/// <summary>
		///     Dependency property for binding the content to a Windows Forms control.
		/// </summary>
		public static readonly DependencyProperty ControlProperty
			= DependencyProperty.Register("Control",
										  typeof(Control),
										  typeof(WinFormsHostHelper),
										  new FrameworkPropertyMetadata(OnContentChanged));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor - initializes view.
		/// </summary>
		public WinFormsHostHelper()
		{
			InitializeComponent();
		}


		/// <summary>
		///     Get or set the Windows Forms control embedded in this control.
		/// </summary>
		public Control Control
		{
			get => (Control) GetValue(ControlProperty);
			set => SetValue(ControlProperty, value);
		}

		#endregion

		#region -- Non-Public Methods --

		// Updates the content of the WindowsFormsHost when the bound property changes.
		private static void OnContentChanged(DependencyObject aSender, DependencyPropertyChangedEventArgs aArgs)
		{
			var host = (WinFormsHostHelper) aSender;
			if (null != host)
			{
				var newControl = (Control) aArgs.NewValue;
				host._winFormsHost.Child = newControl;
			}
		}

		#endregion
	}
}

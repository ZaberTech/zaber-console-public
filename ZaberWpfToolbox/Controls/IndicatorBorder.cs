﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ZaberWpfToolbox.Controls
{
	/// <summary>
	///    Border that is invisible until a message is assigned, and then
	///    becomes visible and provides the message as a tooltip.
	/// </summary>
	public partial class IndicatorBorder : Border
	{
		#region -- Public data --

		/// <summary>
		///     Dependency property for the text to display.
		/// </summary>
		public static readonly DependencyProperty MessageProperty =
			DependencyProperty.RegisterAttached("Message",
												typeof(string),
												typeof(IndicatorBorder),
												new FrameworkPropertyMetadata(string.Empty, OnPropertyChanged));

		/// <summary>
		///     Dependency property for the brush to use for the border when the message is set.
		/// </summary>
		public static readonly DependencyProperty IndicationBrushProperty =
			DependencyProperty.RegisterAttached("IndicationBrush",
												typeof(Brush),
												typeof(IndicatorBorder),
												new FrameworkPropertyMetadata(Brushes.Red, OnPropertyChanged));

		#endregion
		#region -- Public methods --

		/// <summary>
		///     Initialize the control.
		/// </summary>
		public IndicatorBorder()
		{
			Background = Brushes.Transparent;
			BorderBrush = Brushes.Transparent;
			BorderThickness = new Thickness(1.0);
			Padding = new Thickness(5.0);
		}


		/// <summary>
		///     Text to display as a tooltip. If null or empty, the border is
		///     invisible and the tooltip is disabled.
		/// </summary>
		public string Message
		{
			get => GetValue(MessageProperty) as string;
			set => SetValue(MessageProperty, value);
		}


		/// <summary>
		///    Brush to use for the border when a message is set.
		/// </summary>
		public Brush IndicationBrush
		{
			get => GetValue(IndicationBrushProperty) as Brush;
			set => SetValue(IndicationBrushProperty, value);
		}

		#endregion
		#region -- Event handlers --

		private static void OnPropertyChanged(DependencyObject aControl, DependencyPropertyChangedEventArgs aArgs)
		{
			if (!(aControl is IndicatorBorder border))
			{
				return;
			}

			// This is implemented in code instead of a style because doing it as a style
			// breaks the ability to contain named controls for some reason.
			if (string.IsNullOrEmpty(border.Message))
			{
				border.BorderBrush = Brushes.Transparent;
				border.ToolTip = null;
				ToolTipService.SetIsEnabled(border, false);
			}
			else
			{
				border.BorderBrush = border.IndicationBrush;
				border.ToolTip = border.Message;
				ToolTipService.SetIsEnabled(border, true);
			}
		}

		#endregion
	}
}

﻿using System.Windows;
using System.Windows.Input;

namespace ZaberWpfToolbox.Controls
{
	/// <summary>
	///     Interaction logic for simple VCR-like movement controls.
	/// </summary>
	public partial class SimpleMovementControl
	{
		#region -- Public data --

		/// <summary>
		///     Dependency property for binding the command for the third move left button.
		/// </summary>
		public static readonly DependencyProperty MoveLeftToEndCommandProperty =
			DependencyProperty.RegisterAttached("MoveLeftToEndCommand",
												typeof(ICommand),
												typeof(SimpleMovementControl));


		/// <summary>
		///     Dependency property for binding the command for the second move left button.
		/// </summary>
		public static readonly DependencyProperty MoveLeftCommandProperty =
			DependencyProperty.RegisterAttached("MoveLeftCommand",
												typeof(ICommand),
												typeof(SimpleMovementControl));


		/// <summary>
		///     Dependency property for binding the command for the first move left button.
		/// </summary>
		public static readonly DependencyProperty JogLeftCommandProperty =
			DependencyProperty.RegisterAttached("JogLeftCommand",
												typeof(ICommand),
												typeof(SimpleMovementControl));


		/// <summary>
		///     Dependency property for binding the command for the stop button.
		/// </summary>
		public static readonly DependencyProperty StopCommandProperty =
			DependencyProperty.RegisterAttached("StopCommand",
												typeof(ICommand),
												typeof(SimpleMovementControl));


		/// <summary>
		///     Dependency property for binding the command for the first move right button.
		/// </summary>
		public static readonly DependencyProperty JogRightCommandProperty =
			DependencyProperty.RegisterAttached("JogRightCommand",
												typeof(ICommand),
												typeof(SimpleMovementControl));


		/// <summary>
		///     Dependency property for binding the command for the second move right button.
		/// </summary>
		public static readonly DependencyProperty MoveRightCommandProperty =
			DependencyProperty.RegisterAttached("MoveRightCommand",
												typeof(ICommand),
												typeof(SimpleMovementControl));


		/// <summary>
		///     Dependency property for binding the command for the third move right button.
		/// </summary>
		public static readonly DependencyProperty MoveRightToEndCommandProperty =
			DependencyProperty.RegisterAttached("MoveRightToEndCommand",
												typeof(ICommand),
												typeof(SimpleMovementControl));


		/// <summary>
		///     Dependency property for binding the command for the home button.
		/// </summary>
		public static readonly DependencyProperty HomeCommandProperty =
			DependencyProperty.RegisterAttached("HomeCommand",
												typeof(ICommand),
												typeof(SimpleMovementControl));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Constructor. Initializes the control.
		/// </summary>
		public SimpleMovementControl()
		{
			InitializeComponent();
		}


		/// <summary>
		///     Get or set the command for the third move left button.
		/// </summary>
		public ICommand MoveLeftToEndCommand
		{
			get => GetValue(MoveLeftToEndCommandProperty) as ICommand;
			set => SetValue(MoveLeftToEndCommandProperty, value);
		}


		/// <summary>
		///     Get or set the command for the second move left button.
		/// </summary>
		public ICommand MoveLeftCommand
		{
			get => GetValue(MoveLeftCommandProperty) as ICommand;
			set => SetValue(MoveLeftCommandProperty, value);
		}


		/// <summary>
		///     Get or set the command for the first move left button.
		/// </summary>
		public ICommand JogLeftCommand
		{
			get => GetValue(JogLeftCommandProperty) as ICommand;
			set => SetValue(JogLeftCommandProperty, value);
		}


		/// <summary>
		///     Get or set the command for the stop button.
		/// </summary>
		public ICommand StopCommand
		{
			get => GetValue(StopCommandProperty) as ICommand;
			set => SetValue(StopCommandProperty, value);
		}


		/// <summary>
		///     Get or set the command for the first move right button.
		/// </summary>
		public ICommand JogRightCommand
		{
			get => GetValue(JogRightCommandProperty) as ICommand;
			set => SetValue(JogRightCommandProperty, value);
		}


		/// <summary>
		///     Get or set the command for the second move right button.
		/// </summary>
		public ICommand MoveRightCommand
		{
			get => GetValue(MoveRightCommandProperty) as ICommand;
			set => SetValue(MoveRightCommandProperty, value);
		}


		/// <summary>
		///     Get or set the command for the third move right button.
		/// </summary>
		public ICommand MoveRightToEndCommand
		{
			get => GetValue(MoveRightToEndCommandProperty) as ICommand;
			set => SetValue(MoveRightToEndCommandProperty, value);
		}


		/// <summary>
		///     Get or set the command for the home button.
		/// </summary>
		public ICommand HomeCommand
		{
			get => GetValue(HomeCommandProperty) as ICommand;
			set => SetValue(HomeCommandProperty, value);
		}

		#endregion

		#region -- Non-Public Methods --

		private void Button_Click(object aSender, RoutedEventArgs aArgs)
		{
			if (aSender == _stopButton)
			{
				TryInvoke(StopCommand);
			}
			else if (aSender == _homeButton)
			{
				TryInvoke(HomeCommand);
			}
			else if (aSender == _leftButton3)
			{
				TryInvoke(MoveLeftToEndCommand);
			}
			else if (aSender == _leftButton2)
			{
				TryInvoke(MoveLeftCommand);
			}
			else if (aSender == _leftButton1)
			{
				TryInvoke(JogLeftCommand);
			}
			else if (aSender == _rightButton1)
			{
				TryInvoke(JogRightCommand);
			}
			else if (aSender == _rightButton2)
			{
				TryInvoke(MoveRightCommand);
			}
			else if (aSender == _rightButton3)
			{
				TryInvoke(MoveRightToEndCommand);
			}
		}


		private void TryInvoke(ICommand aCommand)
		{
			if (null != aCommand)
			{
				if (aCommand.CanExecute(DataContext))
				{
					aCommand.Execute(DataContext);
				}
			}
		}

		#endregion
	}
}

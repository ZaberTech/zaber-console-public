﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using ScintillaNET;
using KeyBinding = System.Windows.Input.KeyBinding;
using KeyEventArgs = System.Windows.Forms.KeyEventArgs;

// Disable the "Obsolete" warning for Scintilla.NET.
#pragma warning disable 0618

namespace ZaberWpfToolbox.Controls
{
	/// <summary>
	///     Helper control for embedding the Scintilla code editor in a WPF window. This
	///     enables us to use the WinForms Scintilla error in a WPF program in an
	///     MVVM-compatible way.
	///     Provides dependency properties for the data that Zaber Console makes use of.
	/// </summary>
	public partial class ScintillaWpfHost
	{
		#region -- Public data --

		/// <summary>
		///     Dependency property to allow binding of the editor code language selection, which
		///     affects syntax highlighting.
		/// </summary>
		public static readonly DependencyProperty ScriptLanguageProperty
			= DependencyProperty.Register("ScriptLanguage",
										  typeof(string),
										  typeof(ScintillaWpfHost),
										  new FrameworkPropertyMetadata(null, OnScriptLanguageChanged));


		/// <summary>
		///     Dependency property to allow binding of the textual content of the editor.
		/// </summary>
		public static readonly DependencyProperty TextProperty
			= DependencyProperty.Register("Text",
										  typeof(string),
										  typeof(ScintillaWpfHost),
										  new FrameworkPropertyMetadata(string.Empty,
																		OnTextChanged));


		/// <summary>
		///     Dependency property to allow binding of the error highlight lines to the editor.
		/// </summary>
		public static readonly DependencyProperty ErrorLinesProperty
			= DependencyProperty.Register("ErrorLines",
										  typeof(int[]),
										  typeof(ScintillaWpfHost),
										  new FrameworkPropertyMetadata(null, OnErrorLinesChanged));


		/// <summary>
		///     Dependency property to allow attaching an ICommand to be invoked when the user edits the text.
		/// </summary>
		public static readonly DependencyProperty DocumentChangedCommandProperty
			= DependencyProperty.Register("DocumentChangedCommand",
										  typeof(ICommand),
										  typeof(ScintillaWpfHost));


		/// <summary>
		///     Dependency property that allows overriding of keyboard inputs normally consumed by the Scintilla control.
		///     Bind this to the InputBindings of another control (usually the container) to ensure that its
		///     inputs will take precedence over the Scintilla input handling.
		/// </summary>
		public static readonly DependencyProperty InputOverridesProperty
			= DependencyProperty.Register("InputOverrides",
										  typeof(InputBindingCollection),
										  typeof(ScintillaWpfHost));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Default constructor. Initializes fonts and event handlers.
		/// </summary>
		public ScintillaWpfHost()
		{
			InitializeComponent();

			var scin = ScintillaControl;
			scin.Scrolling.ScrollBars = ScrollBars.Both;
			scin.Scrolling.HorizontalScrollTracking = true;

			scin.Styles.BraceBad.FontName = "Verdana\0\0\0\0\0\0\0\0\0\0\0\0\0";
			scin.Styles.BraceLight.FontName = "Verdana\0\0\0\0\0\0\0\0\0\0\0\0\0";
			scin.Styles.CallTip.FontName = "Tahoma\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
			scin.Styles.ControlChar.FontName = "Verdana\0\0\0\0\0\0\0\0\0\0\0\0\0";
			scin.Styles.Default.FontName = "Verdana\0\0\0\0\0\0\0\0\0\0\0\0\0";
			scin.Styles.IndentGuide.FontName = "Verdana\0\0\0\0\0\0\0\0\0\0\0\0\0";
			scin.Styles.LastPredefined.FontName = "Verdana\0\0\0\0\0\0\0\0\0\0\0\0\0";
			scin.Styles.LineNumber.FontName = "Verdana\0\0\0\0\0\0\0\0\0\0\0\0\0";
			scin.Styles.Max.FontName = "Verdana\0\0\0\0\0\0\0\0\0\0\0\0\0";

			if (scin.Margins.Count > 1)
			{
				scin.Margins[0].Type = MarginType.Number;
				scin.Margins[1].Width = 8;
				UpdateLineNumberWidth();

				_errorMarker = scin.Markers[3];
				_errorMarker.Symbol = MarkerSymbol.LeftRectangle;
				_errorMarker.BackColor = Color.Red;
				_errorMarker.ForeColor = Color.Black;
			}

			scin.DocumentChange += ScintillaControl_DocumentChange;
			scin.KeyDown += ScintillaControl_KeyDown;
		}


		/// <summary>
		///     The current Scintilla code language selection. Affects syntax highlighting.
		///     This string should be one of the Scintilla lexer language codes, such as "cpp" or "cs".
		/// </summary>
		public string ScriptLanguage
		{
			get => (string) GetValue(ScriptLanguageProperty);
			set => SetValue(ScriptLanguageProperty, value);
		}


		/// <summary>
		///     Gets or sets the text currently displayed in the editor.
		/// </summary>
		public string Text
		{
			get => (string) GetValue(TextProperty);
			set => SetValue(TextProperty, value);
		}


		/// <summary>
		///     Gets or sets the line numbers currently highlighted as having errors.
		/// </summary>
		public int[] ErrorLines
		{
			get => (int[]) GetValue(ErrorLinesProperty);
			set => SetValue(ErrorLinesProperty, value);
		}


		/// <summary>
		///     Gets or sets an ICommand to be invoked when the user makes an edit.
		/// </summary>
		public ICommand DocumentChangedCommand
		{
			get => (ICommand) GetValue(DocumentChangedCommandProperty);
			set => SetValue(DocumentChangedCommandProperty, value);
		}


		/// <summary>
		///     Gets or sets an overriding set of input bindings for global editor commands.
		/// </summary>
		public InputBindingCollection InputOverrides
		{
			get => (InputBindingCollection) GetValue(InputOverridesProperty);
			set => SetValue(InputOverridesProperty, value);
		}

		#endregion

		#region -- Non-Public Methods --

		// Updates the language selection in the Scintilla control when the bound property changes value.
		private static void OnScriptLanguageChanged(DependencyObject aSender, DependencyPropertyChangedEventArgs aArgs)
		{
			var host = (ScintillaWpfHost) aSender;
			if (null != host)
			{
				try
				{
					host.ScintillaControl.ConfigurationManager.Language = (string) aArgs.NewValue;
				}
				catch (AccessViolationException)
				{
					// Scintilla's got some bugs. 
					// Sometimes it throws an AccessViolationException.
					// Ignore it, since we can't control it.
				}
			}
		}


		// Updates the text in the Scintilla control when the bound property changes value.
		private static void OnTextChanged(DependencyObject aSender, DependencyPropertyChangedEventArgs aArgs)
		{
			var host = (ScintillaWpfHost) aSender;
			if (null != host)
			{
				if (host._inDocumentChanged)
				{
					return;
				}

				host.ScintillaControl.Text = (string) aArgs.NewValue;
				host.UpdateLineNumberWidth();
			}
		}


		// Updates the error markers in the Scintilla control when the bound property changes value.
		private static void OnErrorLinesChanged(DependencyObject aSender, DependencyPropertyChangedEventArgs aArgs)
		{
			var host = (ScintillaWpfHost) aSender;
			if (null != host)
			{
				if (host._inDocumentChanged)
				{
					return;
				}

				var lines = aArgs.NewValue as int[];
				if (null == lines)
				{
					lines = new int[0];
				}

				if (null != host._errorMarker)
				{
					var errorLines = new HashSet<int>(lines);
					foreach (Line line in host.ScintillaControl.Lines)
					{
						line.DeleteAllMarkers();
						if (errorLines.Contains(line.Number))
						{
							line.AddMarker(host._errorMarker);
						}
					}
				}
			}
		}


		// Invokes the Document Changed callback when the Scintilla control reports an edit.
		private void ScintillaControl_DocumentChange(object aSender, NativeScintillaEventArgs aArgs)
		{
			if (_inDocumentChanged)
			{
				return;
			}

			_inDocumentChanged = true;

			UpdateLineNumberWidth();

			var editor = (Scintilla) aSender;
			if (Text != editor.Text)
			{
				Text = editor.Text;
			}

			var cmd = DocumentChangedCommand;
			if ((null != cmd) && cmd.CanExecute(aArgs.SCNotification.modificationType))
			{
				cmd.Execute(aArgs.SCNotification.modificationType);
			}

			_inDocumentChanged = false;
		}


		private void ScintillaControl_KeyDown(object aSender, KeyEventArgs aArgs)
		{
			var bindings = InputOverrides;
			if (null != bindings)
			{
				var binding = bindings.OfType<KeyBinding>()
								   .FirstOrDefault(kb => (kb.Key == KeyInterop.KeyFromVirtualKey((int) aArgs.KeyCode))
													 && (kb.Modifiers == WinFormsModiferKeysToWpf(aArgs)));

				if (null != binding)
				{
					if (binding.Command.CanExecute(binding.CommandParameter))
					{
						aArgs.Handled = true;
						binding.Command.Execute(binding.CommandParameter);
					}
				}
			}
		}


		private ModifierKeys WinFormsModiferKeysToWpf(KeyEventArgs aArgs)
		{
			var result = ModifierKeys.None;

			if (aArgs.Shift)
			{
				result |= ModifierKeys.Shift;
			}

			if (aArgs.Control)
			{
				result |= ModifierKeys.Control;
			}

			if (aArgs.Alt)
			{
				result |= ModifierKeys.Alt;
			}

			if ((Keys.LWin == aArgs.KeyCode) || (Keys.RWin == aArgs.KeyCode))
			{
				result |= ModifierKeys.Windows;
			}

			return result;
		}


		private void UpdateLineNumberWidth()
		{
			var editor = ScintillaControl;

			if (editor.Margins.Count > 0)
			{
				var numChars = editor.Lines.Count.ToString().Length;
				if (numChars != _lineNumberDigits)
				{
					_lineNumberDigits = numChars;
					editor.Margins[0].Width =
						editor.Styles[StylesCommon.LineNumber].GetTextWidth(new string('9', _lineNumberDigits + 1)) + 2;
				}
			}
		}

		#endregion

		#region -- Data --

		private readonly Marker _errorMarker;

		private volatile bool _inDocumentChanged;
		private int _lineNumberDigits;

		#endregion
	}
}

#pragma warning restore 0618

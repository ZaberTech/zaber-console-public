﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using log4net;

namespace ZaberWpfToolbox.Controls
{
	/// <summary>
	///     ViewModel for a marquee control that displays multiple one-line strings, one at a time,
	///     in a rotating sequence.
	/// </summary>
	public class MarqueeVM : ObservableObject
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Initializes the object with a reference to the UI dispatcher.
		///     This assumes the object is being constructed in the UI thread.
		/// </summary>
		public MarqueeVM()
		{
			_dispatcher = DispatcherHelper.Dispatcher;
			_expiryTimer = DispatcherTimerHelper.CreateTimer();
			_expiryTimer.IsEnabled = false;
			_expiryTimer.Tick += ExpiryTimer_Tick;

			_updateTimer = DispatcherTimerHelper.CreateTimer();
			_updateTimer.IsEnabled = false;
			_updateTimer.Interval = TimeSpan.FromSeconds(2);
			_updateTimer.Tick += UpdateTimer_Tick;
		}


		/// <summary>
		///     Add a message to the marquee. Messages are shown in the order added.
		/// </summary>
		/// <param name="aOwner">Owner of the message - used as a key to clear messages early.</param>
		/// <param name="aMessage">Message to display.</param>
		/// <param name="aDuration">
		///     Duration to display the message for, in seconds.
		///     This is wall clock time, not accumulated screen time. This will be clamped to a
		///     minimum of one second.
		/// </param>
		public void AddMessage(object aOwner, string aMessage, int aDuration)
		{
			_expiryTimer.Stop();

			aDuration = Math.Max(aDuration, 1);

			lock (_messages)
			{
				var added = _messages.AddLast(new MessageInfo
				{
					Owner = aOwner,
					Message = aMessage,
					Expiry = DateTime.Now + TimeSpan.FromSeconds(aDuration)
				});

				if (_currentMessage is null)
				{
					_currentMessage = added;
				}
			}

			_log.DebugFormat("Added '{0}", aMessage);

			UpdateText();

			ScheduleExpiry();
			ScheduleUpdate();
		}


		/// <summary>
		///     Clear all messages from one source.
		/// </summary>
		/// <param name="aOwner">Owner of all messages to remove.</param>
		public void ClearMessages(object aOwner)
		{
			_expiryTimer.Stop();

			lock (_messages)
			{
				var node = _messages.First;
				while (null != node)
				{
					if (node.Value.Owner == aOwner)
					{
						_log.DebugFormat("Cleared '{0}'", node.Value.Message);
						var prev = node.Previous;
						if (_currentMessage == node)
						{
							_currentMessage = prev?.Next;
						}

						_messages.Remove(node);
						node = prev is null ? _messages.First : prev.Next;
					}
					else
					{
						node = node.Next;
					}
				}
			}

			UpdateText();

			ScheduleExpiry();
			ScheduleUpdate();
		}


		/// <summary>
		///     Main message currently being displayed.
		/// </summary>
		public string CurrentText
		{
			get => _currentText;
			set => Set(ref _currentText, value, nameof(CurrentText));
		}


		/// <summary>
		///     Concatenation of all currently active messages.
		/// </summary>
		public string AllText
		{
			get => _allText;
			set => Set(ref _allText, value, nameof(AllText));
		}

		#endregion

		#region -- Non-Public Methods --

		private void ScheduleExpiry()
		{
			lock (_messages)
			{
				if (_messages.Any())
				{
					var nextUpdate = _messages
								 .Select(m => m.Expiry)
								 .Min();

					var interval = nextUpdate - DateTime.Now;
					if (interval.TotalMilliseconds < 50)
					{
						interval = TimeSpan.FromMilliseconds(50);
					}

					_expiryTimer.Interval = interval;
					_expiryTimer.Start();
				}
			}
		}


		private void ScheduleUpdate() => _updateTimer.IsEnabled = _messages.Any();


		private void ExpiryTimer_Tick(object aSender, EventArgs aArgs)
		{
			_expiryTimer.Stop();
			PurgeExpiredMessages();
			ScheduleExpiry();
			ScheduleUpdate();
		}


		private void UpdateTimer_Tick(object aSender, EventArgs aArgs)
		{
			_currentMessage = _currentMessage?.Next ?? _messages.First;
			_dispatcher.BeginInvoke(() => { CurrentText = _currentMessage?.Value.Message; });
		}


		private void PurgeExpiredMessages()
		{
			lock (_messages)
			{
				var node = _messages.First;
				while (null != node)
				{
					if (node.Value.Expiry < DateTime.Now)
					{
						_log.DebugFormat("Expired '{0}'", node.Value.Message);
						var prev = node.Previous;
						if (_currentMessage == node)
						{
							_currentMessage = prev?.Next;
						}

						_messages.Remove(node);
						node = prev is null ? _messages.First : prev.Next;
					}
					else
					{
						node = node.Next;
					}
				}
			}

			UpdateText();
		}


		private void UpdateText()
		{
			var allText = string.Join(Environment.NewLine, _messages.Select(n => n.Message).ToArray());

			if (_currentMessage is null)
			{
				_currentMessage = _messages.First;
			}

			_dispatcher.BeginInvoke(() =>
			{
				CurrentText = _currentMessage?.Value.Message;
				AllText = allText;
			});
		}

		#endregion

		#region -- Data --

		private struct MessageInfo
		{
			public object Owner { get; set; }

			public string Message { get; set; }

			public DateTime Expiry { get; set; }
		}


		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private readonly IDispatcher _dispatcher;

		private string _currentText;
		private string _allText;

		private readonly LinkedList<MessageInfo> _messages = new LinkedList<MessageInfo>();
		private LinkedListNode<MessageInfo> _currentMessage;
		private readonly IDispatcherTimer _expiryTimer;
		private readonly IDispatcherTimer _updateTimer;

		#endregion
	}
}

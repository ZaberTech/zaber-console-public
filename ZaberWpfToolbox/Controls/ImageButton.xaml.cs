﻿using System.Windows;
using System.Windows.Controls;

namespace ZaberWpfToolbox.Controls
{
	/// <summary>
	///     Interaction logic for ImageButton.xaml
	/// </summary>
	public partial class ImageButton : Button
	{
		#region -- Public data --

		/// <summary>
		///     Attached property for the label text.
		/// </summary>
		public static readonly DependencyProperty LabelProperty =
			DependencyProperty.RegisterAttached("Label",
												typeof(string),
												typeof(ImageButton),
												new UIPropertyMetadata(string.Empty, OnLabelChanged));


		/// <summary>
		///     Attached property for the keyboard shortcut label.
		/// </summary>
		public static readonly DependencyProperty ShortcutLabelProperty =
			DependencyProperty.RegisterAttached("ShortcutLabel",
												typeof(string),
												typeof(ImageButton),
												new UIPropertyMetadata(string.Empty, OnShortcutLabelChanged));


		/// <summary>
		///     Attached property for the icon image.
		/// </summary>
		public static readonly DependencyProperty ImageProperty =
			DependencyProperty.RegisterAttached("Image",
												typeof(object),
												typeof(ImageButton),
												new UIPropertyMetadata(null, OnImageChanged));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Type initializer. Applies style pass-though for this type, since it doesn't inherit from BaseControl.
		/// </summary>
		static ImageButton()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(ImageButton),
													 new FrameworkPropertyMetadata(typeof(ImageButton)));
		}


		/// <summary>
		///     Default constructor. Initializes the component.
		/// </summary>
		public ImageButton()
		{
			InitializeComponent();
		}


		/// <summary>
		///     Get or set the button label.
		/// </summary>
		public string Label
		{
			get => GetValue(LabelProperty) as string;
			set => SetValue(LabelProperty, value);
		}


		/// <summary>
		///     Get or set the shortcut text.
		/// </summary>
		public string ShortcutLabel
		{
			get => GetValue(ShortcutLabelProperty) as string;
			set => SetValue(ShortcutLabelProperty, value);
		}


		/// <summary>
		///     Get or set the icon.
		/// </summary>
		public object Image
		{
			get => GetValue(ImageProperty);
			set => SetValue(ImageProperty, value);
		}

		#endregion

		#region -- Non-Public Methods --

		private static void OnLabelChanged(DependencyObject aControl, DependencyPropertyChangedEventArgs aArgs)
		{
			var button = aControl as ImageButton;
			if (null == button)
			{
				return;
			}

			var text = aArgs.NewValue as string;
			button._label.Text = text;
			button._label.Visibility = string.IsNullOrEmpty(text) ? Visibility.Collapsed : Visibility.Visible;
		}


		private static void OnShortcutLabelChanged(DependencyObject aControl, DependencyPropertyChangedEventArgs aArgs)
		{
			var button = aControl as ImageButton;
			if (null == button)
			{
				return;
			}

			var text = aArgs.NewValue as string;
			button._shortcutLabel.Text = text;
			button._shortcutLabel.Visibility = string.IsNullOrEmpty(text) ? Visibility.Collapsed : Visibility.Visible;
		}


		private static void OnImageChanged(DependencyObject aControl, DependencyPropertyChangedEventArgs aArgs)
		{
			var button = aControl as ImageButton;
			if (null == button)
			{
				return;
			}

			button._image.Content = aArgs.NewValue;
		}

		#endregion
	}
}

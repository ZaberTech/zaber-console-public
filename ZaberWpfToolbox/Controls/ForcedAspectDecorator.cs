﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace ZaberWpfToolbox.Controls
{
	/// <summary>
	///     Layout border that sizes itself according to its content, and enforces a specified
	///     aspect ratio on itself.
	/// </summary>
	/// <remarks>
	///     Adapted from https://coding4life.wordpress.com/2012/10/15/wpf-resize-maintain-aspect-ratio
	///     which does not specify a license but clearly intends it to be used by readers.
	/// </remarks>
	public class ForcedAspectDecorator : Decorator
	{
		#region -- Public data --

		/// <summary>
		///     Dependency property for binding aspect ratio.
		/// </summary>
		public static readonly DependencyProperty AspectRatioProperty
			= DependencyProperty.Register("AspectRatio",
										  typeof(double),
										  typeof(ForcedAspectDecorator),
										  new FrameworkPropertyMetadata(1.0,
																		FrameworkPropertyMetadataOptions
																		.AffectsMeasure),
										  ValidateAspectRatio);

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Adjust a size according to the current aspect ratio.
		/// </summary>
		/// <param name="aSize">The size to fix up.</param>
		/// <param name="aAllowExpansion">True to achieve the correct ratio by expanding, false for contraction.</param>
		/// <returns>A size adjusted to fit the current ratio.</returns>
		public Size SizeToRatio(Size aSize, bool aAllowExpansion)
		{
			var ratio = AspectRatio;

			var height = aSize.Width / ratio;
			var width = aSize.Height * ratio;

			if (aAllowExpansion)
			{
				width = Math.Max(width, aSize.Width);
				height = Math.Max(height, aSize.Height);
			}
			else
			{
				width = Math.Min(width, aSize.Width);
				height = Math.Min(height, aSize.Height);
			}

			return new Size(width, height);
		}


		/// <summary>
		///     Get or set the aspect ratio to be enforced.
		/// </summary>
		public double AspectRatio
		{
			get => (double) GetValue(AspectRatioProperty);
			set => SetValue(AspectRatioProperty, value);
		}

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Override the measure behavior for the decorator.
		/// </summary>
		/// <param name="aConstraint">Sizing constraint information from the layout system.</param>
		/// <returns>Measured size for the control.</returns>
		protected override Size MeasureOverride(Size aConstraint)
		{
			if (null != Child)
			{
				aConstraint = SizeToRatio(aConstraint, false);
				Child.Measure(aConstraint);

				if (double.IsInfinity(aConstraint.Width) || double.IsInfinity(aConstraint.Height))
				{
					return SizeToRatio(Child.DesiredSize, true);
				}

				return aConstraint;
			}

			// we don't have a child, so we don't need any space
			return new Size(0, 0);
		}


		/// <summary>
		///     Override the arrangement behavior for the control.
		/// </summary>
		/// <param name="aArrangeSize">Target size.</param>
		/// <returns>Adjusted size.</returns>
		protected override Size ArrangeOverride(Size aArrangeSize)
		{
			if (null != Child)
			{
				var newSize = SizeToRatio(aArrangeSize, false);

				var widthDelta = aArrangeSize.Width - newSize.Width;
				var heightDelta = aArrangeSize.Height - newSize.Height;

				double top = 0;
				double left = 0;

				if (!double.IsNaN(widthDelta) && !double.IsInfinity(widthDelta))
				{
					left = widthDelta / 2;
				}

				if (!double.IsNaN(heightDelta) && !double.IsInfinity(heightDelta))
				{
					top = heightDelta / 2;
				}

				var finalRect = new Rect(new Point(left, top), newSize);
				Child.Arrange(finalRect);
			}

			return aArrangeSize;
		}


		private static bool ValidateAspectRatio(object aValue)
		{
			if (!(aValue is double))
			{
				return false;
			}

			var aspectRatio = (double) aValue;

			return (aspectRatio > 0.0)
			   && !double.IsInfinity(aspectRatio)
			   && !double.IsNaN(aspectRatio);
		}

		#endregion
	}
}

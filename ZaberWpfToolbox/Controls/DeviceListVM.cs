﻿using System.Collections.ObjectModel;

namespace ZaberWpfToolbox.Controls
{
	/// <summary>
	/// ViewModel for the list of devices. Also tracks the currently
	/// selected device.
	/// </summary>
	public class DeviceListVM : ObservableObject
	{
		/// <summary>
		/// List of available devices to display. User can either modify
		/// the content or replace it with a new list to update the display.
		/// </summary>
		public ObservableCollection<BaseDeviceVM> Devices
		{
			get { return _devices; }
			set
			{
				Set(ref _devices, value, () => Devices);
			}
		}


		/// <summary>
		/// The currently delected device from the Devices list, or null if none selected.
		/// </summary>
		public BaseDeviceVM SelectedDevice
		{
			get { return _selectedDevice; }
			set
			{
				Set(ref _selectedDevice, value, () => SelectedDevice);
			}
		}


		/// <summary>
		/// Controls whether the device list is interactable or greyed out.
		/// </summary>
		public bool Enabled
		{
			get { return _enabled; }
			set
			{
				Set(ref _enabled, value, () => Enabled);
			}
		}


		private ObservableCollection<BaseDeviceVM> _devices = new ObservableCollection<BaseDeviceVM>();
		private BaseDeviceVM _selectedDevice = null;
		private bool _enabled = true;
	}
}

﻿namespace ZaberWpfToolbox.Controls
{
	/// <summary>
	///     Interaction logic for Marquee.xaml
	/// </summary>
	public partial class Marquee
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Constructor. Initializes the control.
		/// </summary>
		public Marquee()
		{
			InitializeComponent();
		}

		#endregion
	}
}

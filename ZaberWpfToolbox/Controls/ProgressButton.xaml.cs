﻿using System.Windows;

namespace ZaberWpfToolbox.Controls
{
	/// <summary>
	///     A button that displays an optional icon to the left of a label, with an
	///     optional progress bar as the background for the label.
	/// </summary>
	public partial class ProgressButton
	{
		#region -- Public data --

		/// <summary>
		///     Attached property for the icon image.
		/// </summary>
		public static readonly DependencyProperty IconProperty =
			DependencyProperty.RegisterAttached("Icon",
												typeof(object),
												typeof(ProgressButton),
												new UIPropertyMetadata(null, OnIconChanged));


		/// <summary>
		///     Attached property for the label text.
		/// </summary>
		public static readonly DependencyProperty LabelProperty =
			DependencyProperty.RegisterAttached("Label",
												typeof(string),
												typeof(ProgressButton),
												new UIPropertyMetadata(string.Empty, OnLabelChanged));


		/// <summary>
		///     Attached property for the progress bar visibility.
		/// </summary>
		public static readonly DependencyProperty ProgressVisibleProperty =
			DependencyProperty.RegisterAttached("ProgressVisible",
												typeof(bool),
												typeof(ProgressButton),
												new UIPropertyMetadata(false, OnProgressVisibilityChanged));


		/// <summary>
		///     Attached property for the progress bar minimum value.
		/// </summary>
		public static readonly DependencyProperty ProgressMinimumProperty =
			DependencyProperty.RegisterAttached("ProgressMinimum",
												typeof(double),
												typeof(ProgressButton),
												new UIPropertyMetadata(0.0, OnProgressMinMaxChanged));


		/// <summary>
		///     Attached property for the progress bar maximum value.
		/// </summary>
		public static readonly DependencyProperty ProgressMaximumProperty =
			DependencyProperty.RegisterAttached("ProgressMaximum",
												typeof(double),
												typeof(ProgressButton),
												new UIPropertyMetadata(1.0, OnProgressMinMaxChanged));


		/// <summary>
		///     Attached property for the progress bar value.
		/// </summary>
		public static readonly DependencyProperty ProgressProperty =
			DependencyProperty.RegisterAttached("Progress",
												typeof(double),
												typeof(ProgressButton),
												new UIPropertyMetadata(0.0, OnProgressChanged));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Type initializer. Applies style pass-though for this type, since it doesn't inherit from BaseControl.
		/// </summary>
		static ProgressButton()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(ProgressButton),
													 new FrameworkPropertyMetadata(typeof(ProgressButton)));
		}


		/// <summary>
		///     Default constructor. Initializes the component.
		/// </summary>
		public ProgressButton()
		{
			InitializeComponent();
			_progressBar.Visibility = ProgressVisible ? Visibility.Visible : Visibility.Hidden;
		}


		/// <summary>
		///     Get or set the icon.
		/// </summary>
		public object Icon
		{
			get => GetValue(IconProperty);
			set => SetValue(IconProperty, value);
		}


		/// <summary>
		///     Get or set the button label.
		/// </summary>
		public string Label
		{
			get => GetValue(LabelProperty) as string;
			set => SetValue(LabelProperty, value);
		}


		/// <summary>
		///     Get or set the progress bar visibilithy.
		/// </summary>
		public bool ProgressVisible
		{
			get => (bool) GetValue(ProgressVisibleProperty);
			set => SetValue(ProgressVisibleProperty, value);
		}


		/// <summary>
		///     Get or set the progress bar minimum value.
		/// </summary>
		public double ProgressMinimum
		{
			get => (double) GetValue(ProgressMinimumProperty);
			set => SetValue(ProgressMinimumProperty, value);
		}


		/// <summary>
		///     Get or set the progress bar maximum value.
		/// </summary>
		public double ProgressMaximum
		{
			get => (double) GetValue(ProgressMaximumProperty);
			set => SetValue(ProgressMaximumProperty, value);
		}


		/// <summary>
		///     Get or set the progress bar value.
		/// </summary>
		public double Progress
		{
			get => (double) GetValue(ProgressMaximumProperty);
			set => SetValue(ProgressMaximumProperty, value);
		}

		#endregion

		#region -- Non-Public Methods --

		private static void OnIconChanged(DependencyObject aControl, DependencyPropertyChangedEventArgs aArgs)
		{
			if (aControl is ProgressButton button)
			{
				button._icon.Content = aArgs.NewValue;
			}
		}


		private static void OnLabelChanged(DependencyObject aControl, DependencyPropertyChangedEventArgs aArgs)
		{
			if (aControl is ProgressButton button)
			{
				var text = aArgs.NewValue as string;
				button._label.Text = text;
			}
		}


		private static void OnProgressVisibilityChanged(DependencyObject aControl,
														DependencyPropertyChangedEventArgs aArgs)
		{
			if (aControl is ProgressButton button)
			{
				var visible = (bool) aArgs.NewValue;
				button._progressBar.Visibility = visible ? Visibility.Visible : Visibility.Hidden;
			}
		}


		private static void OnProgressMinMaxChanged(DependencyObject aControl, DependencyPropertyChangedEventArgs aArgs)
		{
			if (aControl is ProgressButton button)
			{
				button._progressBar.Minimum = button.ProgressMinimum;
				button._progressBar.Maximum = button.ProgressMaximum;
			}
		}


		private static void OnProgressChanged(DependencyObject aControl, DependencyPropertyChangedEventArgs aArgs)
		{
			if (aControl is ProgressButton button)
			{
				button._progressBar.Value = (double) aArgs.NewValue;
			}
		}

		#endregion
	}
}

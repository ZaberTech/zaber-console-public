﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ZaberWpfToolbox.Controls
{
	/// <summary>
	///     Interaction logic for AutocompleteBox.xaml
	/// </summary>
	public partial class AutocompleteBox : BaseControl
	{
		#region -- Public data --

		/// <summary>
		///     Dependency property for CommandList.
		/// </summary>
		public static readonly DependencyProperty CommandListProperty =
			DependencyProperty.RegisterAttached("CommandList",
												typeof(ObservableCollection<string>),
												typeof(AutocompleteBox),
												new PropertyMetadata(default(ObservableCollection<string>),
																	 OnCommandListChanged));

		/// <summary>
		///     Dependency property for the previously selected item.
		///     If set, this item will always be present at the bottom of the list, and highlighted.
		/// </summary>
		public static readonly DependencyProperty PreviousSelectionProperty =
			DependencyProperty.RegisterAttached("PreviousSelection",
				typeof(string),
				typeof(AutocompleteBox),
				new PropertyMetadata(default(string)));


		/// <summary>
		///     Bind HasFocus to an observable boolean property on your VM to be able to force or check
		///     whether the attached control has focus.
		/// </summary>
		public static readonly DependencyProperty HasFocusProperty =
			DependencyProperty.RegisterAttached("HasFocus",
												typeof(bool?),
												typeof(AutocompleteBox),
												new FrameworkPropertyMetadata(HasFocusChanged));


		/// <summary>
		///     Dependency property for EnteredText.
		/// </summary>
		public static readonly DependencyProperty EnteredTextProperty =
			DependencyProperty.Register("EnteredText",
										typeof(string),
										typeof(AutocompleteBox),
										new FrameworkPropertyMetadata(EnteredTextChanged));


		/// <summary>
		///     Dependency property for UnselectedText.
		/// </summary>
		public static readonly DependencyProperty UnselectedTextProperty =
			DependencyProperty.Register("UnselectedText",
										typeof(string),
										typeof(AutocompleteBox),
										new PropertyMetadata(null));


		/// <summary>
		///     Dependency property for the prefix regular expression.
		/// </summary>
		public static readonly DependencyProperty PrefixProperty =
			DependencyProperty.Register("Prefix",
										typeof(string),
										typeof(AutocompleteBox),
										new FrameworkPropertyMetadata(PrefixPropertyChanged));


		/// <summary>
		///     Dependency property for the case sensitivity setting.
		/// </summary>
		public static readonly DependencyProperty CaseSensitivityProperty =
			DependencyProperty.Register("CaseSensitive",
										typeof(bool),
										typeof(AutocompleteBox),
										new FrameworkPropertyMetadata(true));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     AutocompleteBox constructor
		/// </summary>
		public AutocompleteBox()
		{
			InitializeComponent();
		}


		/// <summary>
		///     List of commands available to the AutocompleteBox.
		/// </summary>
		public ObservableCollection<string> CommandList
		{
			get => GetValue(CommandListProperty) as ObservableCollection<string>;
			set => SetValue(CommandListProperty, value);
		}


		/// <summary>
		///     Suggestion to be highlighted in completion list.
		/// </summary>
		public string PreviousSelection
		{
			get => GetValue(PreviousSelectionProperty) as string;
			set => SetValue(PreviousSelectionProperty, value);
		}


		/// <summary>
		///     Get or set the current focus state.
		/// </summary>
		public bool? HasFocus
		{
			get => GetValue(HasFocusProperty) as bool?;
			set => SetValue(HasFocusProperty, value);
		}


		/// <summary>
		///     Text entered in Autocomplete text field.
		/// </summary>
		public string EnteredText
		{
			get => GetValue(EnteredTextProperty) as string;
			set => SetValue(EnteredTextProperty, value);
		}


		/// <summary>
		///     Text entered in Autocomplete text field that is not selected.
		/// </summary>
		public string UnselectedText
		{
			get => GetValue(UnselectedTextProperty) as string;
			set => SetValue(UnselectedTextProperty, value);
		}


		/// <summary>
		///     Prefix regular expression. If set, any text that matches the expression at the start
		///     of the typed string will be ignored when searching for completions, and will be prepended
		///     to all possible completions. This expression should not include the start of string match
		///     or any capture groups; those will be added automatically.
		///     Example: "(\/\s*)?(?:\d+\s+){0,2}" will handle the addressing part of Zaber ASCII commands.
		/// </summary>
		public string Prefix
		{
			get => GetValue(UnselectedTextProperty) as string;
			set => SetValue(UnselectedTextProperty, value);
		}


		/// <summary>
		///     Should completion matching be case sensitive? Defaults to true.
		/// </summary>
		public bool CaseSensitive
		{
			get => (bool) GetValue(CaseSensitivityProperty);
			set => SetValue(CaseSensitivityProperty, value);
		}

		#endregion

		#region -- Non-Public Methods --

		private static void OnCommandListChanged(DependencyObject aControl, DependencyPropertyChangedEventArgs aArgs)
		{
			var list = aControl as AutocompleteBox;

			if (null == list)
			{
				return;
			}

			list._completionList = aArgs.NewValue as ObservableCollection<string>;
			list.MatchPrefix();
		}


		// Called when the value of the bound property changes. If the new value is true, 
		// focus is forced to the associated control.
		private static void HasFocusChanged(DependencyObject aElement, DependencyPropertyChangedEventArgs aArgs)
		{
			var me = aElement as AutocompleteBox;

			if (null == me)
			{
				return;
			}

			var tb = me._textBox;

			// Attach callbacks if we haven't seen this control before.
			// This is why the dependency property is a nullable bool.
			if (null == aArgs.OldValue)
			{
				tb.GotFocus += TextBox_GotFocus;
				tb.LostFocus += TextBox_LostFocus;
			}


			// Fix attachment problems with controls hidden when initially displayed.
			if (!me.IsVisible)
			{
				me.IsVisibleChanged += OnVisibilityChanged;
			}

			// Focus the control if the new value is true.
			if ((Visibility.Visible == tb.Visibility)
			&& tb.IsEnabled)
			{
				if (aArgs.NewValue is bool focus && focus)
				{
					tb.Focus();
				}
			}
		}


		private static void PrefixPropertyChanged(DependencyObject aControl, DependencyPropertyChangedEventArgs aArgs)
		{
			var me = aControl as AutocompleteBox;

			if (null == me)
			{
				return;
			}

			me._prefixExpression = null;
			var newExpr = aArgs.NewValue as string;
			if (!string.IsNullOrEmpty(newExpr))
			{
				me._prefixExpression = new Regex("\\A(" + newExpr + ")", RegexOptions.CultureInvariant);
			}

			me.MatchPrefix();
		}


		private static void EnteredTextChanged(DependencyObject aControl, DependencyPropertyChangedEventArgs aArgs)
		{
			var me = aControl as AutocompleteBox;

			if (null == me)
			{
				return;
			}

			if (!me._localChange)
			{
				me._textBox.Text = aArgs.NewValue as string ?? string.Empty;
				me.MatchPrefix();
			}
		}


		// Called when control visibility changes - ensures focus is set correctly.
		private static void OnVisibilityChanged(object aElement, DependencyPropertyChangedEventArgs aArgs)
		{
			var me = aElement as AutocompleteBox;

			if (null == me)
			{
				return;
			}

			var tb = me._textBox;

			if ((null != tb) && tb.IsVisible && (bool) me.GetValue(HasFocusProperty))
			{
				me.IsVisibleChanged -= OnVisibilityChanged;
				tb.Focus();
			}
		}


		// Pass user-triggered focus changes back to the bound property.
		private static void TextBox_GotFocus(object aElement, RoutedEventArgs aArgs)
		{
			var me = aElement as AutocompleteBox;
			if (null != me)
			{
				me.SetValue(HasFocusProperty, true);
			}
		}


		// Pass user-triggered focus changes back to the bound property.
		private static void TextBox_LostFocus(object aElement, RoutedEventArgs aArgs)
		{
			var me = aElement as AutocompleteBox;
			if (null != me)
			{
				me.SetValue(HasFocusProperty, true);
			}
		}


		private static string ExtractUnselectedText(TextBox aTextBox)
		{
			var selectedText = aTextBox.SelectedText;

			if (string.IsNullOrEmpty(selectedText))
			{
				return aTextBox.Text;
			}

			return aTextBox.Text.Substring(0, aTextBox.Text.IndexOf(selectedText));
		}


		private void OnTextBox_TextChanged(object aSender, TextChangedEventArgs aEventArgs)
		{
			MatchPrefix();
			var typedString = _textBox.Text;
			SetEnteredText(typedString);
			UnselectedText = ExtractUnselectedText(_textBox);

			if (string.IsNullOrEmpty(typedString))
			{
				_listBox.ItemsSource = null;
				_listBox.Visibility = Visibility.Collapsed;
			}

			if (ShouldSkipRecursiveCall())
			{
				return;
			}

			var suggestions = FilteredSuggestionsFromText(UnselectedText);
			_listBox.ItemsSource = suggestions;

			if (!IsCaretAtEndOfTextBox(_textBox))
			{
				return;
			}

			if (suggestions.Count == 0)
			{
				_textBox.Text = typedString;
				_listBox.ItemsSource = null;
				_listBox.Visibility = Visibility.Collapsed;
				return;
			}

			if (WasTextDeleted(aEventArgs.Changes))
			{
				return;
			}

			var textToFill = typedString + NextAutocompleteWord(typedString, TopSuggestion(suggestions));
			SkipRecursiveCalls(textToFill, TopSuggestion(suggestions));

			_textBox.Text = textToFill;
			_textBox.Select(typedString.Length, textToFill.Length);
			UnselectedText = ExtractUnselectedText(_textBox);

			_listBox.Visibility = Visibility.Visible;
			_popup.IsOpen = true;
		}


		private void OnTextBox_PreviewKeyDown(object aSender, KeyEventArgs aEventArgs)
		{
			DoNotSkipRecursiveCalls();

			switch (aEventArgs.Key)
			{
				case Key.Return:
					SaveHistoryCommand();
					_textBox.SelectAll();
					break;
				case Key.Up:
					aEventArgs.Handled = true;
					HandleTextBoxUpKey();
					break;
				case Key.Down:
					aEventArgs.Handled = true;
					HandleTextBoxDownKey();
					break;
				case Key.Tab:
					aEventArgs.Handled = HandleTextBoxTabKey();
					break;
				case Key.Escape:
					if (Visibility.Visible == _listBox.Visibility)
					{
						_listBox.Visibility = Visibility.Collapsed;
					}
					else
					{
						_textBox.Text = string.Empty;
					}

					break;
			}
		}


		private void OnSuggestionList_PreviewKeyDown(object aSender, KeyEventArgs aEventArgs)
		{
			if (ReferenceEquals(aSender, _listBox))
			{
				switch (aEventArgs.Key)
				{
					case Key.Enter:
						aEventArgs.Handled = true;
						AcceptSelectedSuggestion();
						break;
					case Key.Tab:
						aEventArgs.Handled = true;
						HandleSuggestionListTabKey();
						break;
					case Key.Right:
						aEventArgs.Handled = true;
						AcceptSelectedSuggestion();
						break;
					case Key.Down:
						aEventArgs.Handled = true;
						HandleSuggestionListDownKey();
						break;
					case Key.Up:
						aEventArgs.Handled = true;
						HandleSuggestionListUpKey();
						break;
					case Key.Escape:
						aEventArgs.Handled = true;
						_listBox.Visibility = Visibility.Collapsed;
						break;
					case Key.Back:
						aEventArgs.Handled = true;
						_listBox.Visibility = Visibility.Collapsed;
						break;
				}
			}

			if (_listBox.Visibility == Visibility.Collapsed)
			{
				_textBox.Focus();
			}
		}


		private void OnSuggestionList_MouseClick(object aSender, MouseEventArgs aEventArgs)
		{
			aEventArgs.Handled = true;
			AcceptSelectedSuggestion();
		}


		private void MatchPrefix()
		{
			_prefixMatch = string.Empty;

			if (null != _prefixExpression)
			{
				var match = _prefixExpression.Match(_textBox.Text);
				if (match.Success && (match.Captures.Count > 0))
				{
					_prefixMatch = match.Captures[0].Value;
				}
			}
		}


		private List<string> FilteredSuggestionsFromText(string aText)
		{
			var autoList = new List<string>();

			if (aText.StartsWith(_prefixMatch))
			{
				aText = aText.Substring(_prefixMatch.Length);
			}

			if (null != _completionList)
			{
				foreach (var suggestion in _completionList)
				{
					if (string.IsNullOrEmpty(_textBox.Text) || !ShouldRowGetFiltered(suggestion, aText))
					{
						autoList.Add(_prefixMatch + suggestion);
					}
				}
			}

			// If the previous selection is set, always put it at the bottom of the list
			// even if it also appears in the list.
			var previous = PreviousSelection;
			if (!string.IsNullOrEmpty(previous))
			{
				autoList.Remove(previous);
				autoList.Add(previous);
			}

			return autoList;
		}


		private bool ShouldRowGetFiltered(string aSuggestion, string aTextFilter)
		{
			var suggestionArgs = aSuggestion.Split();
			var textFilterArgs = aTextFilter.Split();

			var comparisonMode = CaseSensitive
				? StringComparison.InvariantCulture
				: StringComparison.InvariantCultureIgnoreCase;

			for (var i = 0; i < textFilterArgs.Length; ++i)
			{
				if (i >= suggestionArgs.Length)
				{
					return true;
				}

				if (Regex.IsMatch(suggestionArgs[i], $"^{RE_PARAM}$"))
				{
					// If we encounter a repeated parameter, consider it to 
					// potentially match everything else since we don't currently
					// have the ability to check word validity for params.
					// This means some suggestions will stay in the list longer
					// than desired, but only when repeated params occur in 
					// the middle of a suggestion. See tickets #6346 and #6351.
					if (Regex.IsMatch(suggestionArgs[i], $"^{RE_MULTI_PARAM}$"))
					{
						return false;
					}

					continue;
				}

				if (((1 < textFilterArgs.Length)
				 && (i < (textFilterArgs.Length - 1))
				 && !suggestionArgs[i].Equals(textFilterArgs[i], comparisonMode))
				|| !suggestionArgs[i].StartsWith(textFilterArgs[i], comparisonMode))
				{
					return true;
				}
			}

			return false;
		}


		private string TopSuggestion(ICollection<string> aSuggestions)
			=> aSuggestions.Count == 0 ? string.Empty : aSuggestions.FirstOrDefault();


		private string TopSuggestion(ListBox aSuggestionList)
			=> aSuggestionList.HasItems ? aSuggestionList.Items[0].ToString() : string.Empty;


		private string NextAutocompleteWord(string aInput, string aSuggestion)
		{
			var inputArgs = aInput.Split();
			var suggestionArgs = aSuggestion.Split();

			if ((suggestionArgs.Length < inputArgs.Length)
			|| string.IsNullOrEmpty(aInput)
			|| string.IsNullOrEmpty(aSuggestion))
			{
				return string.Empty;
			}

			var comparisonMode = CaseSensitive
				? StringComparison.InvariantCulture
				: StringComparison.InvariantCultureIgnoreCase;

			for (var i = 0; i < inputArgs.Length; ++i)
			{
				if (string.IsNullOrEmpty(inputArgs[i]))
				{
					return suggestionArgs[i];
				}

				if (suggestionArgs[i].Equals(inputArgs[i]) || Regex.IsMatch(suggestionArgs[i], $"^{RE_PARAM}$"))
				{
					continue;
				}

				if (((inputArgs.Length - 1) == i) && suggestionArgs[i].StartsWith(inputArgs[i], comparisonMode))
				{
					return suggestionArgs[i].Substring(inputArgs[i].Length);
				}

				return string.Empty;
			}

			if (inputArgs.Length < suggestionArgs.Length)
			{
				return char.IsWhiteSpace(aInput[aInput.Length - 1])
					? suggestionArgs[inputArgs.Length]
					: " " + suggestionArgs[inputArgs.Length];
			}

			return string.Empty;
		}


		// Note to maintainer: The OnTextBox_TextChanged method is invoked every time the autocomplete box's 
		//                     text field is modified (either by the user or programmatically). Sometimes it is 
		//                     necessary to modify the text field from within OnTextBox_TextChanged. To avoid
		//                     OnTextBox_TextChanged calling itself, we break from the method using the 
		//                     following methods:
		private bool ShouldSkipRecursiveCall()
		{
			--_skipRecursiveCallCount;
			return -1 < _skipRecursiveCallCount;
		}


		private void SkipRecursiveCalls(string aText, string aSuggestion) => _skipRecursiveCallCount =
			aText.Split().Length < aSuggestion.Split().Length ? aSuggestion.Substring(aText.Length).Split().Length : 1;


		private void DoNotSkipRecursiveCalls() => _skipRecursiveCallCount = 0;


		private bool WasTextDeleted(ICollection<TextChange> aChanges)
		{
			foreach (var change in aChanges)
			{
				if ((0 < change.RemovedLength) && (0 == change.AddedLength))
				{
					return true;
				}
			}

			return false;
		}


		private bool IsCaretAtEndOfTextBox(TextBox aAutocompleteTextBox)
			=> aAutocompleteTextBox.Text.Length == aAutocompleteTextBox.CaretIndex;


		private void AcceptHighlightedSuggestion(TextBox aTextBox)
		{
			if (Regex.IsMatch(aTextBox.SelectedText, $"^\\s{RE_PARAM}$"))
			{
				SelectWordInSquareBrackets(aTextBox);
				return;
			}

			if (!IsAnyTextSelected(aTextBox))
			{
				aTextBox.Select(0, 0);
				aTextBox.CaretIndex = aTextBox.Text.Length;
			}

			UnselectedText = aTextBox.Text;
		}


		private void HighlightNextSuggestion(TextBox aTextBox, ListBox aSuggestionList)
		{
			if (Regex.Matches(aTextBox.Text, RE_PARAM).Count > 0)
			{
				SelectWordInSquareBrackets(aTextBox);
				return;
			}

			if (aSuggestionList.HasItems)
			{
				var selectionStartIndex = aTextBox.Text.Length;
				var next = NextAutocompleteWord(aTextBox.Text, TopSuggestion(aSuggestionList));

				if (string.IsNullOrEmpty(next))
				{
					return;
				}

				SkipRecursiveCalls(aTextBox.Text, aTextBox.Text + next);
				aTextBox.Text += next;

				aTextBox.Select(selectionStartIndex + 1, aTextBox.Text.Length);
			}
		}


		private static bool IsAnyTextSelected(TextBox aTextBox) => string.IsNullOrEmpty(aTextBox.SelectedText);


		private bool IsSuggestionListVisible() => Visibility.Visible == _listBox.Visibility;


		private void SelectWordInSquareBrackets(TextBox aTextBox)
		{
			var matches = Regex.Matches(aTextBox.Text, RE_PARAM);
			if ((0 == matches.Count) || Regex.IsMatch(aTextBox.Text, $"^{RE_PARAM}$"))
			{
				return;
			}

			aTextBox.Select(matches[0].Index, matches[0].Value.Length);
			aTextBox.Focus();
		}


		private void AcceptSelectedSuggestion()
		{
			var selectedItem = _listBox.SelectedItem;
			if (selectedItem == null)
			{
				return;
			}

			var enteredArgs = _textBox.Text.Split();
			var suggestionArgs = selectedItem.ToString().Split();
			var commonLength = Math.Min(enteredArgs.Length, suggestionArgs.Length);

			for (var i = 0; i < commonLength; ++i)
			{
				if (Regex.IsMatch(suggestionArgs[i], $"^{RE_PARAM}$"))
				{
					suggestionArgs[i] = enteredArgs[i];
				}
			}

			_textBox.Text = string.Join(" ", suggestionArgs);
			_listBox.Visibility = Visibility.Collapsed;

			SelectWordInSquareBrackets(_textBox);

			if (string.IsNullOrEmpty(_textBox.SelectedText))
			{
				_textBox.CaretIndex = _textBox.Text.Length;
				_textBox.Focus();
			}
		}


		private void HandleTextBoxUpKey() => ShowPreviousHistoryCommand();


		private void HandleTextBoxDownKey()
		{
			if (_historyIndex.HasValue && (_historyIndex.Value < _history.Count))
			{
				// If we're navigating history and haven't got to the end, show next from history.
				ShowNextHistoryCommand();
			}
			else if (IsSuggestionListVisible())
			{
				// If we're not navigating history or have got to the end and the suggestion list is
				// visible, jump to navigating the suggestion list.
				_textBox.Text = UnselectedText;
				_listBox.Focus();
				_listBox.SelectedIndex = 0;
			}
		}


		private bool HandleTextBoxTabKey()
		{
			if (0 == _textBox.Text.Length)
			{
				return false;
			}

			AcceptHighlightedSuggestion(_textBox);
			HighlightNextSuggestion(_textBox, _listBox);
			UnselectedText = ExtractUnselectedText(_textBox);
			_listBox.ItemsSource = FilteredSuggestionsFromText(UnselectedText);
			return true;
		}


		private void SaveHistoryCommand()
		{
			var command = _textBox.Text;
			if (string.IsNullOrEmpty(command.Trim()))
			{
				return;
			}

			_history.Add(command);
			if (_historyIndex.HasValue)
			{
				var i = _historyIndex.Value;
				if ((i < (_history.Count - 1)) && (command == _history[i + 1])) // Issued copy of next cmd in history.
				{
					_historyIndex = i + 1;
					if (_historyIndex == (_history.Count - 1))
					{
						_historyIndex = null;
					}
				}
				else if (command != _history[i]) // History command was edited, making a new command.
				{
					_historyIndex = null;
				}
			}
		}


		private void ShowPreviousHistoryCommand()
		{
			var command = _textBox.Text;

			var i = _historyIndex ?? (_history.Count - 1);
			if (!string.IsNullOrEmpty(command.Trim())
			&& (i == (_history.Count - 1))
			&& (i >= 0)
			&& (command != _history[i]))
			{
				// If we were editing a new command before using the up arrow, and were not modifying
				// a historical command, save the edit in progress at tne end of the history list.
				_history.Add(command);
			}

			if (!_historyIndex.HasValue) // If we're just starting to move up, start at the last index.
			{
				i++;
			}

			i = Math.Max(0, i - 1);
			_historyIndex = i;
			if ((i >= 0) && (i < _history.Count))
			{
				command = _history[i];
				_textBox.Text = command;
				_textBox.SelectAll();
			}
		}


		private void ShowNextHistoryCommand()
		{
			var command = _textBox.Text;

			var i = _historyIndex ?? (_history.Count - 1);
			if (!string.IsNullOrEmpty(command.Trim()) && (i >= 0) && (i < _history.Count) && (command != _history[i]))
			{
				_history[i] = command;
			}

			i++;
			if (i >= _history.Count)
			{
				_historyIndex = null;
				command = string.Empty;
			}
			else
			{
				_historyIndex = i;
				command = _history[i];
			}

			_textBox.Text = command;
			_textBox.SelectAll();
		}


		private void HandleSuggestionListTabKey()
		{
			var text = _textBox.Text + NextAutocompleteWord(_textBox.Text, _listBox.SelectedItem?.ToString());
			SkipRecursiveCalls(text, _listBox.SelectedItem?.ToString());
			_textBox.Text = text;
			_textBox.CaretIndex = _textBox.Text.Length;
			_listBox.ItemsSource = FilteredSuggestionsFromText(EnteredText);

			if (_listBox.Items.Count < 2)
			{
				_textBox.Focus();
			}

			SelectWordInSquareBrackets(_textBox);
		}


		private void HandleSuggestionListDownKey()
		{
			++_listBox.SelectedIndex;
			_listBox.ScrollIntoView(_listBox.SelectedItem);
		}


		private void HandleSuggestionListUpKey()
		{
			if (_listBox.SelectedIndex < 1)
			{
				_textBox.CaretIndex = _textBox.Text.Length;
				_textBox.Focus();
			}

			_listBox.SelectedIndex = _listBox.SelectedIndex >= 0 ? _listBox.SelectedIndex - 1 : _listBox.SelectedIndex;
			_listBox.ScrollIntoView(_listBox.SelectedItem);
		}


		private void SetEnteredText(string aNewValue)
		{
			_localChange = true;
			EnteredText = aNewValue;
			_localChange = false;
		}

		#endregion

		#region -- Data --

		private const string RE_PARAM = @"\[(.*?)\]";
		private const string RE_MULTI_PARAM = @"\[(.*?)\.\.\.\]";

		private ObservableCollection<string> _completionList;
		private readonly List<string> _history = new List<string>();
		private int? _historyIndex;
		private int _skipRecursiveCallCount;
		private Regex _prefixExpression;
		private string _prefixMatch = string.Empty;
		private bool _localChange;

		#endregion
	}
}

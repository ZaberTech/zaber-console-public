﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ZaberWpfToolbox.Controls
{
	/// <summary>
	///     Checkbox subclass that uses data-bound icon images for its checked and unchecked states.
	/// </summary>
	public class IconCheckbox : CheckBox
	{
		#region -- Public data --

		/// <summary>
		///     Binding to the image to use for the unchecked state.
		/// </summary>
		public static readonly DependencyProperty CheckedImageProperty =
			DependencyProperty.RegisterAttached("CheckedImage",
												typeof(ImageBrush),
												typeof(IconCheckbox));


		/// <summary>
		///     Binding to the image to use for the unchecked state.
		/// </summary>
		public static readonly DependencyProperty UncheckedImageProperty =
			DependencyProperty.RegisterAttached("UncheckedImage",
												typeof(ImageBrush),
												typeof(IconCheckbox));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Enables local style override since this control inherits directly from a standard control.
		/// </summary>
		static IconCheckbox()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(IconCheckbox),
													 new FrameworkPropertyMetadata(typeof(IconCheckbox)));
		}


		/// <summary>
		///     Get or set the help content.
		/// </summary>
		public ImageBrush CheckedImage
		{
			get => GetValue(CheckedImageProperty) as ImageBrush;
			set => SetValue(CheckedImageProperty, value);
		}


		/// <summary>
		///     Get or set the help content.
		/// </summary>
		public ImageBrush UncheckedImage
		{
			get => GetValue(UncheckedImageProperty) as ImageBrush;
			set => SetValue(UncheckedImageProperty, value);
		}

		#endregion
	}
}

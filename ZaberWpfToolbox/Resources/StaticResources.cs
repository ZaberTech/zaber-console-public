﻿using System.Windows;
using ZaberWpfToolbox.Converters;

namespace ZaberWpfToolbox.Resources
{
	/// <summary>
	///     Static resources for XAML clients of this library. Putting these resources
	///     in code instead of a merged dictionary makes them a little more reliable since
	///     this way they are referenced by names in code rather than by keys, which are
	///     much easier to break accidentally.
	///     Reference these resources in XAML with "{x:Static res:StaticResources.ResourceName}",
	///     remembering to define the res namespace to be this namespace.
	/// </summary>
	public static class StaticResources
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Static constructor - initializes resource instances.
		/// </summary>
		static StaticResources()
		{
			ConvertBoolToVisibility = new ConfigurableBooleanToVisibilityConverter();
			ConvertBoolToHidden = new ConfigurableBooleanToVisibilityConverter { HiddenState = Visibility.Hidden };

			ConvertNegatedBoolToCollapsed = new ConfigurableBooleanToVisibilityConverter { ReverseBehavior = true };

			ConvertNegatedBoolToHidden = new ConfigurableBooleanToVisibilityConverter
			{
				ReverseBehavior = true,
				HiddenState = Visibility.Hidden
			};

			NegateBool = new BooleanNegationConverter();
			DetectEmptyString = new StringEmptyConverter();
			DetectNonemptyString = new StringEmptyConverter { NegateResult = true };
			ConvertEnumToBoolean = new EnumToBooleanConverter();
			ConvertEnumToNegatedBoolean = new EnumToBooleanConverter { NegateResult = true };
			BooleanAnd = new BooleanAndConverter();
			BooleanNand = new BooleanAndConverter { NegateResult = true };
			BooleanOr = new BooleanOrConverter();
			BooleanNor = new BooleanOrConverter { NegateResult = true };
			Equality = new EqualityConverter();
			MultiEquality = new EqualityMultiConverter();
			Inequality = new EqualityConverter { NegateResult = true };
			GridLengthSerializer = new GridLengthValueConverter();
			CollectionEmpty = new CollectionEmptyConverter();
			CollectionNotEmpty = new CollectionEmptyConverter { NegateResult = true };

			EnableExtendedSelectionModeConverter = new SelectionModeConverter();
			EnumValueDescriptionConverter = new EnumValueDescriptionConverter();

			IsNull = new IsNullConverter();
			IsNotNull = new IsNullConverter { NegateResult = true };

			GetDisplayName = new DisplayNameConverter();
		}


		/// <summary>
		///     Converts boolean true to Visibility.Visible and false to Visibility.Collapsed.
		/// </summary>
		public static ConfigurableBooleanToVisibilityConverter ConvertBoolToVisibility { get; }


		/// <summary>
		///     Converts boolean true to Visibility.Visible and false to Visibility.Hidden.
		/// </summary>
		public static ConfigurableBooleanToVisibilityConverter ConvertBoolToHidden { get; }


		/// <summary>
		///     Converts boolean false to Visibility.Visible and true to Visibility.Collapsed.
		/// </summary>
		public static ConfigurableBooleanToVisibilityConverter ConvertNegatedBoolToCollapsed { get; }


		/// <summary>
		///     Converts boolean false to Visibility.Visible and true to Visibility.Hidden.
		/// </summary>
		public static ConfigurableBooleanToVisibilityConverter ConvertNegatedBoolToHidden { get; }


		/// <summary>
		///     Negates boolean values.
		/// </summary>
		public static BooleanNegationConverter NegateBool { get; }


		/// <summary>
		///     Returns true if a string property is empty or null.
		/// </summary>
		public static StringEmptyConverter DetectEmptyString { get; }


		/// <summary>
		///     Returns true if a string property is not empty.
		/// </summary>
		public static StringEmptyConverter DetectNonemptyString { get; }


		/// <summary>
		///     Returns true if an enum property equals the ConvertParameter.
		/// </summary>
		public static EnumToBooleanConverter ConvertEnumToBoolean { get; }


		/// <summary>
		///     Returns false if an enum property equals its ConvertParameter.
		/// </summary>
		public static EnumToBooleanConverter ConvertEnumToNegatedBoolean { get; }


		/// <summary>
		///     Performs logical AND on multiple boolean inputs.
		/// </summary>
		public static BooleanAndConverter BooleanAnd { get; }


		/// <summary>
		///     Performs logical NAND on multiple boolean inputs.
		/// </summary>
		public static BooleanAndConverter BooleanNand { get; }


		/// <summary>
		///     Performs logical OR on multiple boolean inputs.
		/// </summary>
		public static BooleanOrConverter BooleanOr { get; }


		/// <summary>
		///     Performs logical NOR on multiple boolean inputs.
		/// </summary>
		public static BooleanOrConverter BooleanNor { get; }


		/// <summary>
		///     Compares two objects of any types for equality.
		/// </summary>
		public static EqualityConverter Equality { get; }


		/// <summary>
		///     Compares many objects of any types for equality.
		/// </summary>
		public static EqualityMultiConverter MultiEquality { get; }


		/// <summary>
		///     Compares two objects of any types for inequality.
		/// </summary>
		public static EqualityConverter Inequality { get; }


		/// <summary>
		///     Converts between GridLengths and their string serialized form.
		/// </summary>
		public static GridLengthValueConverter GridLengthSerializer { get; }


		/// <summary>
		///     Converts null or empty collections to boolean true.
		/// </summary>
		public static CollectionEmptyConverter CollectionEmpty { get; }


		/// <summary>
		///     Converts collections with content to boolean true.
		/// </summary>
		public static CollectionEmptyConverter CollectionNotEmpty { get; }


		/// <summary>
		///     Enables extended multiple selection mode on true input.
		/// </summary>
		public static SelectionModeConverter EnableExtendedSelectionModeConverter { get; }


		/// <summary>
		///     Returns the description attributes of enumeration values.
		/// </summary>
		public static EnumValueDescriptionConverter EnumValueDescriptionConverter { get; }


		/// <summary>
		///     Returns true if the value is null.
		/// </summary>
		public static IsNullConverter IsNull { get; }


		/// <summary>
		///     Returns true if the value is not null.
		/// </summary>
		public static IsNullConverter IsNotNull { get; }


		/// <summary>
		///     Returns the display name of a type or object, or its type name if none.
		/// </summary>
		public static DisplayNameConverter GetDisplayName { get; }

		#endregion
	}
}

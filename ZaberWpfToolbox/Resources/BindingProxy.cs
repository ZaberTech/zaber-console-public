﻿using System.Windows;

namespace ZaberWpfToolbox.Resources
{
	/// <summary>
	///     Proxy object to provide datacontext access to controls that are outside the visual
	///     tree. For example, putting one of these in the resources section of a DataGrid will
	///     enable the grid columns to bind to the datagrid's data context.
	/// </summary>
	/// <remarks>
	///     Based on an example from:
	///     http://stackoverflow.com/questions/22073740/binding-visibility-for-datagridcolumn-in-wpf
	/// </remarks>
	public class BindingProxy : Freezable
	{
		#region -- Public data --

		/// <summary>
		///     Attached property for the data to route to the control.
		/// </summary>
		public static readonly DependencyProperty DataProperty = DependencyProperty.Register("Data",
																							 typeof(object),
																							 typeof(BindingProxy));

		#endregion

		#region -- Public Methods & Properties --

		/// <summary>
		///     Data to store for child elements to reference.
		/// </summary>
		public object Data
		{
			get => GetValue(DataProperty);
			set => SetValue(DataProperty, value);
		}

		#endregion

		#region -- Non-Public Methods --

		/// <summary>
		///     Base class override. Creates a new instance of this type.
		/// </summary>
		/// <returns>A new instance of BindingProxy.</returns>
		protected override Freezable CreateInstanceCore() => new BindingProxy();

		#endregion
	}
}

﻿using System.Diagnostics;
using System.Text;
using System.Windows;

namespace ZaberWpfToolbox.Resources
{
	/// <summary>
	///     Debugging helper for finding WPF binding errors. Adapted from the thread:
	///     http://stackoverflow.com/questions/8480569/how-to-locate-the-source-of-a-binding-error
	///     which origially got it from the now offline:
	///     http://www.switchonthecode.com/tutorials/wpf-snippet-detecting-binding-errors
	/// </summary>
	public class BindingErrorTracer : DefaultTraceListener
	{
		#region -- Public Methods & Properties --

		// Instance constructor is private - static public API.
		private BindingErrorTracer()
		{
		}


		/// <summary>
		///     Start listening for binding errors.
		/// </summary>
		public static void SetTrace() => SetTrace(SourceLevels.Error, TraceOptions.None);


		/// <summary>
		///     Start listening for binding problems.
		/// </summary>
		/// <param name="aLevel">Minimum severity of the problem to listen for.</param>
		/// <param name="aOptions">Bit mask of output options.</param>
		public static void SetTrace(SourceLevels aLevel, TraceOptions aOptions)
		{
			if (null == _Listener)
			{
				_Listener = new BindingErrorTracer();
				PresentationTraceSources.DataBindingSource.Listeners.Add(_Listener);
			}

			_Listener.TraceOutputOptions = aOptions;
			PresentationTraceSources.DataBindingSource.Switch.Level = aLevel;
		}


		/// <summary>
		///     Stop listening.
		/// </summary>
		public static void CloseTrace()
		{
			if (null == _Listener)
			{
				return;
			}

			_Listener.Flush();
			_Listener.Close();
			PresentationTraceSources.DataBindingSource.Listeners.Remove(_Listener);
			_Listener = null;
		}


		/// <summary>
		///     Callback for reporting partial messages. Does not display them.
		/// </summary>
		/// <param name="aMessage">The message to output.</param>
		public override void Write(string aMessage) => _Message.Append(aMessage);


		/// <summary>
		///     Callback for reporting messages. Displays the message in a MessageBox.
		/// </summary>
		/// <param name="aMessage">The message to output.</param>
		public override void WriteLine(string aMessage)
		{
			_Message.Append(aMessage);

			var final = _Message.ToString();
			_Message.Length = 0;

			MessageBox.Show(final, "Binding Error", MessageBoxButton.OK, MessageBoxImage.Error);
		}

		#endregion

		#region -- Data --

		private static BindingErrorTracer _Listener;
		private readonly StringBuilder _Message = new StringBuilder();

		#endregion
	}
}

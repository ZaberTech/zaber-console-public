﻿using System;
using System.Text;
using System.Windows;
using System.Windows.Markup;
using System.Xml;

namespace ZaberWpfToolbox.Resources
{
	/// <summary>
	///     Helper class to extract default control templates.
	///     Not used in production code, but useful to keep around for
	///     implementation time.
	/// </summary>
	public static class ControlTemplateExtractor
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Save the control template of a control to a text file on disk.
		/// </summary>
		/// <param name="aControlType">The type of control to get the template of.</param>
		/// <param name="aFilePath">Path to the file to save the template in.</param>
		public static void SaveDefaultTemplate(Type aControlType, string aFilePath)
		{
			var control = Application.Current.FindResource(aControlType);
			using (var writer = new XmlTextWriter(aFilePath, Encoding.UTF8))
			{
				writer.Formatting = Formatting.Indented;
				XamlWriter.Save(control, writer);
			}
		}

		#endregion
	}
}
